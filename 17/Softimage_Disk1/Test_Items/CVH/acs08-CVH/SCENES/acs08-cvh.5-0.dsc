SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.5-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       cvh-cube1.2-0 ; 
       cvh-cube2.1-0 ; 
       cvh-cube3.1-0 ; 
       cvh-cube4_1.1-0 ROOT ; 
       cvh-cube5.1-0 ; 
       cvh-cube7.1-0 ; 
       cvh-cyl5.1-0 ; 
       cvh-cyl6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-cvh.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       4 5 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       7 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 2.329999 -2.85306 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 99 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
