SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       new_lighting-bool1_5_1.22-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.59-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       new_lighting-bool1_5_1.23-0 ROOT ; 
       new_lighting-control_tower.2-0 ; 
       new_lighting-east_bay_7.7-0 ; 
       new_lighting-east_bay_9.1-0 ; 
       new_lighting-east_bay_antenna.1-0 ; 
       new_lighting-east_bay_antenna1.1-0 ; 
       new_lighting-east_bay_strut.1-0 ; 
       new_lighting-east_bay_strut_1.1-0 ; 
       new_lighting-east_bay_strut1.1-0 ; 
       new_lighting-east_bay_strut1_1.3-0 ; 
       new_lighting-main_antenna.1-0 ; 
       new_lighting-south_block.1-0 ; 
       new_lighting-south_block5.1-0 ; 
       new_lighting-south_block6.1-0 ; 
       new_lighting-south_hull_1.3-0 ; 
       new_lighting-south_hull_2.1-0 ; 
       new_lighting-SSc1.1-0 ; 
       new_lighting-SSc2.1-0 ; 
       new_lighting-SSc3.1-0 ; 
       new_lighting-SSc4.1-0 ; 
       new_lighting-SSc5.1-0 ; 
       new_lighting-SSc6.1-0 ; 
       new_lighting-turret.1-0 ; 
       new_lighting-turret1.1-0 ; 
       new_lighting-turret2.1-0 ; 
       new_lighting-turret4.1-0 ; 
       new_lighting-utl28a_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/test/PICTURES/ss27 ; 
       E:/Pete_Data2/test/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss27-add_turrets.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 2 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       26 0 110 ; 
       22 3 110 ; 
       23 14 110 ; 
       24 15 110 ; 
       25 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 63.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 60 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 45 -2 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 71.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 86.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 101.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 113.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 95 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 80 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 110 -4 0 MPRFLG 0 ; 
       25 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 119 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
