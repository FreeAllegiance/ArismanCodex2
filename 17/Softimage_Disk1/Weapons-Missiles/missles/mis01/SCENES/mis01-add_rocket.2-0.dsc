SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_rocket-cam_int1.2-0 ROOT ; 
       add_rocket-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       add_rocket-mat1.1-0 ; 
       add_rocket-mat10.1-0 ; 
       add_rocket-mat11.1-0 ; 
       add_rocket-mat12.1-0 ; 
       add_rocket-mat13.1-0 ; 
       add_rocket-mat15.1-0 ; 
       add_rocket-mat16.1-0 ; 
       add_rocket-mat17.1-0 ; 
       add_rocket-mat7.1-0 ; 
       add_rocket-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       add_rocket-front_fins.1-0 ; 
       add_rocket-frontfin4.1-0 ; 
       add_rocket-frontfin4_1.2-0 ; 
       add_rocket-frontfin5.1-0 ; 
       add_rocket-frontfin7.1-0 ; 
       add_rocket-main_body.1-0 ; 
       add_rocket-Mis06.1-0 ROOT ; 
       add_rocket-rear_fin5.1-0 ; 
       add_rocket-rear_fin5_1.2-0 ; 
       add_rocket-rear_fin6.1-0 ; 
       add_rocket-rear_fin7.1-0 ; 
       add_rocket-rear_fins.1-0 ; 
       add_rocket-rocket.1-0 ; 
       add_rocket-smoke.1-0 ; 
       add_rocket-thrust.1-0 ; 
       add_rocket-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage_Archive/Softimage_Disk1/Weapons-Missiles/missles/mis01/PICTURES/mis01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mis01-add_rocket.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       add_rocket-t2d1.1-0 ; 
       add_rocket-t2d10.1-0 ; 
       add_rocket-t2d11.1-0 ; 
       add_rocket-t2d12.1-0 ; 
       add_rocket-t2d13.1-0 ; 
       add_rocket-t2d15.1-0 ; 
       add_rocket-t2d16.1-0 ; 
       add_rocket-t2d17.1-0 ; 
       add_rocket-t2d7.1-0 ; 
       add_rocket-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 6 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       12 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       2 0 300 ; 
       3 2 300 ; 
       4 3 300 ; 
       5 9 300 ; 
       5 1 300 ; 
       7 6 300 ; 
       8 8 300 ; 
       9 7 300 ; 
       10 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 9 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 18.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 26.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 35 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
