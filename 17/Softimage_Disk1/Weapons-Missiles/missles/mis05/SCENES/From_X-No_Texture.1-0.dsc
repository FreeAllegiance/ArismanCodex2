SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       No_Texture-obj22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       No_Texture-cam_int1.1-0 ROOT ; 
       No_Texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       No_Texture-mat1.1-0 ; 
       No_Texture-mat10.1-0 ; 
       No_Texture-mat2.1-0 ; 
       No_Texture-mat3.1-0 ; 
       No_Texture-mat4.1-0 ; 
       No_Texture-mat5.1-0 ; 
       No_Texture-mat6.1-0 ; 
       No_Texture-mat7.1-0 ; 
       No_Texture-mat8.1-0 ; 
       No_Texture-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       No_Texture-body.1-0 ; 
       No_Texture-frontfin1.1-0 ; 
       No_Texture-frontfin4.1-0 ; 
       No_Texture-frontfin5.1-0 ; 
       No_Texture-frontfin6.1-0 ; 
       No_Texture-obj22.1-0 ROOT ; 
       No_Texture-obj23.1-0 ; 
       No_Texture-obj24.1-0 ; 
       No_Texture-rear_fin3.1-0 ; 
       No_Texture-rear_fin5.1-0 ; 
       No_Texture-rear_fin6.1-0 ; 
       No_Texture-rear_fin7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-No_Texture.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 5 110 ; 
       2 6 110 ; 
       1 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       10 7 110 ; 
       9 7 110 ; 
       11 7 110 ; 
       0 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       1 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       8 5 300 ; 
       10 6 300 ; 
       9 7 300 ; 
       11 8 300 ; 
       0 9 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       0 SCHEM 23.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
