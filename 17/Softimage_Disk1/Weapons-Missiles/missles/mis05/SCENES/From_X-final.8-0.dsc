SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       final-cam_int1.8-0 ROOT ; 
       final-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       final-mat1.1-0 ; 
       final-mat10.1-0 ; 
       final-mat11.1-0 ; 
       final-mat12.1-0 ; 
       final-mat13.1-0 ; 
       final-mat15.1-0 ; 
       final-mat16.1-0 ; 
       final-mat17.1-0 ; 
       final-mat7.1-0 ; 
       final-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       Mis05-body.1-0 ; 
       Mis05-front_fins.1-0 ; 
       Mis05-frontfin4.1-0 ; 
       Mis05-frontfin4_1.2-0 ; 
       Mis05-frontfin5.1-0 ; 
       Mis05-frontfin7.1-0 ; 
       Mis05-Mis05.11-0 ROOT ; 
       Mis05-rear_fin5.1-0 ; 
       Mis05-rear_fin5_1.2-0 ; 
       Mis05-rear_fin6.1-0 ; 
       Mis05-rear_fin7.1-0 ; 
       Mis05-rear_fins.1-0 ; 
       Mis05-thrust.1-0 ; 
       Mis05-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis05/PICTURES/mis05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-final.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       final-t2d1.1-0 ; 
       final-t2d10.1-0 ; 
       final-t2d11.1-0 ; 
       final-t2d12.1-0 ; 
       final-t2d13.1-0 ; 
       final-t2d15.1-0 ; 
       final-t2d16.1-0 ; 
       final-t2d17.1-0 ; 
       final-t2d7.1-0 ; 
       final-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 6 110 ; 
       12 6 110 ; 
       0 6 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 1 300 ; 
       2 4 300 ; 
       3 0 300 ; 
       4 2 300 ; 
       5 3 300 ; 
       7 6 300 ; 
       8 8 300 ; 
       9 7 300 ; 
       10 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 9 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 29.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 32 -2 0 MPRFLG 0 ; 
       0 SCHEM 5.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 13.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 17 -4 0 MPRFLG 0 ; 
       3 SCHEM 9.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 14.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12 -4 0 MPRFLG 0 ; 
       6 SCHEM 18.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 19.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 27 -4 0 MPRFLG 0 ; 
       10 SCHEM 22 -4 0 MPRFLG 0 ; 
       11 SCHEM 23.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 22 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
