SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.3-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       text-mat1.3-0 ; 
       text-mat10.3-0 ; 
       text-mat7.3-0 ; 
       text-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       Mis05-body.1-0 ; 
       Mis05-front_fins.1-0 ; 
       Mis05-Mis05.2-0 ROOT ; 
       Mis05-rear_fins.1-0 ; 
       text-frontfin4.2-0 ROOT ; 
       text-rear_fin5.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis05/PICTURES/mis05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-text.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       text-t2d1.2-0 ; 
       text-t2d10.2-0 ; 
       text-t2d7.2-0 ; 
       text-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 2 110 ; 
       0 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 0 300 ; 
       5 2 300 ; 
       0 3 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 2 401 ; 
       3 1 401 ; 
       1 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 2.745197 MPRFLG 0 ; 
       5 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 -3.52362 MPRFLG 0 ; 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
