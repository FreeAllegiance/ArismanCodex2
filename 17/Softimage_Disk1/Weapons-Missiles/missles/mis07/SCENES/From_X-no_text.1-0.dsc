SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       no_text-cam_int1.1-0 ROOT ; 
       no_text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       no_text-mat1.1-0 ; 
       no_text-mat10.1-0 ; 
       no_text-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       no_text-cyl2.1-0 ; 
       no_text-frontfin4.1-0 ROOT ; 
       no_text-obj4.1-0 ROOT ; 
       no_text-rear_fin4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-no_text.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 2 300 ; 
       3 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0.2698405 MPRFLG 0 ; 
       3 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 -0.4013462 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
