SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.2-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       text-mat1.1-0 ; 
       text-mat10.1-0 ; 
       text-mat11.1-0 ; 
       text-mat12.1-0 ; 
       text-mat13.1-0 ; 
       text-mat14.1-0 ; 
       text-mat15.1-0 ; 
       text-mat16.1-0 ; 
       text-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       Mis07-body.1-0 ; 
       Mis07-frontfin4.1-0 ; 
       Mis07-frontfin4_1.1-0 ; 
       Mis07-frontfin5.1-0 ; 
       Mis07-frontfin6.1-0 ; 
       Mis07-Mis07.1-0 ROOT ; 
       Mis07-null3.1-0 ; 
       Mis07-null4.1-0 ; 
       Mis07-rear_fin4.1-0 ; 
       Mis07-rear_fin4_1.1-0 ; 
       Mis07-rear_fin5.1-0 ; 
       Mis07-rear_fin6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis07/PICTURES/mis07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-text.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       text-t2d1.1-0 ; 
       text-t2d2.1-0 ; 
       text-t2d3.1-0 ; 
       text-t2d4.1-0 ; 
       text-t2d5.1-0 ; 
       text-t2d6.1-0 ; 
       text-t2d7.1-0 ; 
       text-t2d8.1-0 ; 
       text-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 6 110 ; 
       10 7 110 ; 
       4 6 110 ; 
       2 6 110 ; 
       11 7 110 ; 
       9 7 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       0 5 110 ; 
       1 6 110 ; 
       8 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       10 3 300 ; 
       4 4 300 ; 
       2 5 300 ; 
       11 6 300 ; 
       9 7 300 ; 
       0 0 300 ; 
       1 8 300 ; 
       8 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 3 401 ; 
       8 1 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       1 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
