SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.10-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       text-mat1.1-0 ; 
       text-mat10.1-0 ; 
       text-mat11.1-0 ; 
       text-mat12.1-0 ; 
       text-mat13.1-0 ; 
       text-mat14.1-0 ; 
       text-mat15.1-0 ; 
       text-mat16.1-0 ; 
       text-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       Mis07-back_fins.1-0 ; 
       Mis07-body.1-0 ; 
       Mis07-front_fins.1-0 ; 
       Mis07-frontfin4.1-0 ; 
       Mis07-frontfin4_1.1-0 ; 
       Mis07-frontfin5.1-0 ; 
       Mis07-frontfin6.1-0 ; 
       Mis07-Mis07.7-0 ROOT ; 
       Mis07-rear_fin4.1-0 ; 
       Mis07-rear_fin4_1.1-0 ; 
       Mis07-rear_fin5.1-0 ; 
       Mis07-rear_fin6.1-0 ; 
       Mis07-smoke.1-0 ; 
       Mis07-thrust.1-0 ; 
       Mis07-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis07/PICTURES/mis07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mis07-final.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       text-t2d1.1-0 ; 
       text-t2d2.1-0 ; 
       text-t2d3.1-0 ; 
       text-t2d4.1-0 ; 
       text-t2d5.1-0 ; 
       text-t2d6.1-0 ; 
       text-t2d7.1-0 ; 
       text-t2d8.1-0 ; 
       text-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 7 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       12 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       3 8 300 ; 
       4 5 300 ; 
       5 2 300 ; 
       6 4 300 ; 
       8 1 300 ; 
       9 7 300 ; 
       10 3 300 ; 
       11 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
