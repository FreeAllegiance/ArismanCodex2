SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture-cam_int1.2-0 ROOT ; 
       texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       texture-mat1.1-0 ; 
       texture-mat10.1-0 ; 
       texture-mat11.1-0 ; 
       texture-mat12.1-0 ; 
       texture-mat13.1-0 ; 
       texture-mat14.1-0 ; 
       texture-mat15.1-0 ; 
       texture-mat16.1-0 ; 
       texture-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       Mis08-back_fins.1-0 ; 
       Mis08-body.1-0 ; 
       Mis08-front_fins.1-0 ; 
       Mis08-frontfin4.1-0 ; 
       Mis08-frontfin4_1.1-0 ; 
       Mis08-frontfin5.1-0 ; 
       Mis08-frontfin6.1-0 ; 
       Mis08-Mis08.2-0 ROOT ; 
       Mis08-rear_fin4.1-0 ; 
       Mis08-rear_fin4_1.1-0 ; 
       Mis08-rear_fin5.1-0 ; 
       Mis08-rear_fin6.1-0 ; 
       Mis08-thrust.1-0 ; 
       Mis08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis08/PICTURES/mis08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-final.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       texture-t2d1.1-0 ; 
       texture-t2d2.1-0 ; 
       texture-t2d3.1-0 ; 
       texture-t2d4.1-0 ; 
       texture-t2d5.1-0 ; 
       texture-t2d6.1-0 ; 
       texture-t2d7.1-0 ; 
       texture-t2d8.1-0 ; 
       texture-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 7 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 7 110 ; 
       13 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       3 8 300 ; 
       4 5 300 ; 
       5 2 300 ; 
       6 4 300 ; 
       8 1 300 ; 
       9 7 300 ; 
       10 3 300 ; 
       11 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 24.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 18.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 17 -6 0 MPRFLG 0 ; 
       4 SCHEM 22 -6 0 MPRFLG 0 ; 
       5 SCHEM 19.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 14.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 22 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 24.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 32 -6 0 MPRFLG 0 ; 
       10 SCHEM 27 -6 0 MPRFLG 0 ; 
       11 SCHEM 29.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 9.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 12 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 22 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
