SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       no_texture-cam_int1.2-0 ROOT ; 
       no_texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       no_texture-mat1.2-0 ; 
       no_texture-mat10.2-0 ; 
       no_texture-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       Mis10-Body.1-0 ; 
       Mis10-Mis10.2-0 ROOT ; 
       no_texture-frontfin1.2-0 ROOT ; 
       no_texture-Rear_Fin1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-no_texture.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       2 2 300 ; 
       3 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 SRT 1 1 1 0 0 -1.570796 0 0 0.1011267 MPRFLG 0 ; 
       3 SCHEM 5 0 0 SRT 1 1 1 0 0 -1.570796 0 0 -0.6642937 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
