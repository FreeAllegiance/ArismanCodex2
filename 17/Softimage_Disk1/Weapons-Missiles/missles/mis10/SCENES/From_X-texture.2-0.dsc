SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture-cam_int1.2-0 ROOT ; 
       texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       texture-mat1.1-0 ; 
       texture-mat10.1-0 ; 
       texture-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       Mis09-frontfin1.1-0 ROOT ; 
       Mis09-rear_fin1.1-0 ROOT ; 
       Mis10-body.1-0 ; 
       Mis10-Mis10.3-0 ROOT ; 
       Mis10-thrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis10/PICTURES/mis10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       texture-t2d1.1-0 ; 
       texture-t2d2.2-0 ; 
       texture-t2d3.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 3 110 ; 
       4 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       0 2 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 SRT 1.7088 1.7088 1.7088 0 0 0 0 -1.403697e-016 0.2231713 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 1 2 SRT 1.680588 1.680588 1.680588 0 0 0 0.0009326653 -9.312274e-016 -0.6668323 MPRFLG 0 ; 
       3 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
