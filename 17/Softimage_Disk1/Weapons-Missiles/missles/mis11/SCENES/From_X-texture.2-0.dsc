SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture-cam_int1.2-0 ROOT ; 
       texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       texture-mat1.1-0 ; 
       texture-mat10.1-0 ; 
       texture-mat11.1-0 ; 
       texture-mat12.1-0 ; 
       texture-mat13.1-0 ; 
       texture-mat14.1-0 ; 
       texture-mat15.1-0 ; 
       texture-mat16.1-0 ; 
       texture-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       Mis11-back_fins.1-0 ; 
       Mis11-body.1-0 ; 
       Mis11-front_fins.1-0 ; 
       Mis11-frontfin1.1-0 ; 
       Mis11-frontfin1_1.2-0 ; 
       Mis11-frontfin2.1-0 ; 
       Mis11-frontfin3.1-0 ; 
       Mis11-Mis11.1-0 ROOT ; 
       Mis11-rear_fin1.1-0 ; 
       Mis11-rear_fin1_1.2-0 ; 
       Mis11-rear_fin2.1-0 ; 
       Mis11-rear_fin3.1-0 ; 
       Mis11-thrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis11/PICTURES/mis11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       texture-t2d1.1-0 ; 
       texture-t2d2.1-0 ; 
       texture-t2d3.1-0 ; 
       texture-t2d4.1-0 ; 
       texture-t2d5.1-0 ; 
       texture-t2d6.1-0 ; 
       texture-t2d7.1-0 ; 
       texture-t2d8.1-0 ; 
       texture-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       1 7 110 ; 
       10 0 110 ; 
       4 2 110 ; 
       11 0 110 ; 
       6 2 110 ; 
       3 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       2 1 110 ; 
       0 1 110 ; 
       12 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 2 300 ; 
       1 0 300 ; 
       10 3 300 ; 
       4 8 300 ; 
       11 4 300 ; 
       6 5 300 ; 
       3 6 300 ; 
       8 7 300 ; 
       9 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 3 401 ; 
       0 0 401 ; 
       3 4 401 ; 
       1 2 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 13.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       0 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
