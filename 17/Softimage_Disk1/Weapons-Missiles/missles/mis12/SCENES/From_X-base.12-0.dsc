SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       base-lwingz1.3-0 ; 
       mis12-mis12.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       base-cam_int1.12-0 ROOT ; 
       base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       base-mat1.2-0 ; 
       base-mat2.2-0 ; 
       base-mat3.2-0 ; 
       base-mat4.2-0 ; 
       base-mat6.1-0 ; 
       base-mat8.1-0 ; 
       base-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       base-lwingz1.1-0 ROOT ; 
       mis12-alfinz.1-0 ; 
       mis12-lwingz.1-0 ; 
       mis12-mfuselg.1-0 ; 
       mis12-mis12.8-0 ROOT ; 
       mis12-tafinz.1-0 ; 
       mis12-tafuselg.1-0 ; 
       mis12-thrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis12/PICTURES/mis12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-base.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       base-t2d1.1-0 ; 
       base-t2d2.1-0 ; 
       base-t2d3.1-0 ; 
       base-t2d4.1-0 ; 
       base-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 4 110 ; 
       3 4 110 ; 
       6 3 110 ; 
       5 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       6 2 300 ; 
       5 3 300 ; 
       1 4 300 ; 
       2 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 1 15000 ; 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 4 401 ; 
       6 3 401 ; 
       5 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 23.75 0 0 SRT 1 1 1 2.427355e-008 -2.04205 3.894897e-008 0 -0.09987 0.07000499 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
