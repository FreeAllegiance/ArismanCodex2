SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       mis12-mis12.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.7-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       text-mat1.2-0 ; 
       text-mat10.2-0 ; 
       text-mat2.2-0 ; 
       text-mat3.2-0 ; 
       text-mat4.2-0 ; 
       text-mat6.2-0 ; 
       text-mat8.2-0 ; 
       text-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       mis12-alfinz.1-0 ; 
       mis12-alfinz1.1-0 ; 
       mis12-lwingz.1-0 ; 
       mis12-lwingz1.1-0 ; 
       mis12-mfuselg.1-0 ; 
       mis12-mis12.14-0 ROOT ; 
       mis12-tafinz.1-0 ; 
       mis12-tafuselg.1-0 ; 
       mis12-thrust.1-0 ; 
       mis12-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis12/PICTURES/mis12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-final.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       text-t2d1.2-0 ; 
       text-t2d2.4-0 ; 
       text-t2d3.3-0 ; 
       text-t2d4.3-0 ; 
       text-t2d5.2-0 ; 
       text-t2d6.2-0 ; 
       text-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       1 1 300 ; 
       2 6 300 ; 
       3 7 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       6 4 300 ; 
       7 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 2 401 ; 
       7 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17 -4 0 MPRFLG 0 ; 
       1 SCHEM 27 -4 0 MPRFLG 0 ; 
       2 SCHEM 19.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 23.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 23.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 14.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 22 -4 0 MPRFLG 0 ; 
       8 SCHEM 9.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 32 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 37 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
