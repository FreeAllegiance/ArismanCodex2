SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       mis13-mis13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       texture-cam_int1.2-0 ROOT ; 
       texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       texture-mat1.1-0 ; 
       texture-mat10.1-0 ; 
       texture-mat2.1-0 ; 
       texture-mat3.1-0 ; 
       texture-mat4.1-0 ; 
       texture-mat6.1-0 ; 
       texture-mat8.1-0 ; 
       texture-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       mis13-alfinz.1-0 ; 
       mis13-alfinz1.1-0 ; 
       mis13-lwingz.1-0 ; 
       mis13-lwingz1.1-0 ; 
       mis13-mfuselg.1-0 ; 
       mis13-mis13.1-0 ROOT ; 
       mis13-tafinz.1-0 ; 
       mis13-tafuselg.1-0 ; 
       mis13-thrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/missles/mis13/PICTURES/mis13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       From_X-texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       texture-t2d1.1-0 ; 
       texture-t2d2.1-0 ; 
       texture-t2d3.1-0 ; 
       texture-t2d4.1-0 ; 
       texture-t2d5.1-0 ; 
       texture-t2d6.1-0 ; 
       texture-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       1 1 300 ; 
       2 6 300 ; 
       3 7 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       6 4 300 ; 
       7 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 2 401 ; 
       7 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
