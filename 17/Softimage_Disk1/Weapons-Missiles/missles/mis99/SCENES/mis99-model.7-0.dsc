SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       model-DXF_141.2-0 ; 
       model-mat107.1-0 ; 
       model-mat108.1-0 ; 
       model-mat109.1-0 ; 
       model-mat112.1-0 ; 
       model-mat113.1-0 ; 
       utl202_belter_apc-mat106.1-0 ; 
       utl202_belter_apc-mat83.1-0 ; 
       utl202_belter_apc-mat84.1-0 ; 
       utl202_belter_apc-mat85.1-0 ; 
       utl202_belter_apc-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       model-cyl1_1.1-0 ; 
       model-cyl1_1_1.1-0 ; 
       model-dxf-scene_0.1-0-default.6-0 ROOT ; 
       model-extru13.1-0 ; 
       model-extru6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/Weapons-Missiles/missles/mis99/PICTURES/mis99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mis99-model.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       model-t2d27.2-0 ; 
       model-t2d28.2-0 ; 
       model-t2d29.2-0 ; 
       model-t2d32.2-0 ; 
       model-t2d33.2-0 ; 
       utl202_belter_apc-t2d11.2-0 ; 
       utl202_belter_apc-t2d26.2-0 ; 
       utl202_belter_apc-t2d3.2-0 ; 
       utl202_belter_apc-t2d4.2-0 ; 
       utl202_belter_apc-t2d5.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       1 4 110 ; 
       0 3 110 ; 
       4 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       0 10 300 ; 
       0 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 15 0 0 SRT 4.47 4.47 4.47 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       0 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
