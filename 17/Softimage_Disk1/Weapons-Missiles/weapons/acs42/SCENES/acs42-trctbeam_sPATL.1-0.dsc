SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       trctbeam_sPATL-light1.1-0 ROOT ; 
       trctbeam_sPATL-light2.1-0 ROOT ; 
       trctbeam_sPATL-light3.1-0 ROOT ; 
       trctbeam_sPATL-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       trctbeam_sPATL-mat1.1-0 ; 
       trctbeam_sPATL-mat10.1-0 ; 
       trctbeam_sPATL-mat12.1-0 ; 
       trctbeam_sPATL-mat13.1-0 ; 
       trctbeam_sPATL-mat14.1-0 ; 
       trctbeam_sPATL-mat15.1-0 ; 
       trctbeam_sPATL-mat16.1-0 ; 
       trctbeam_sPATL-mat17.1-0 ; 
       trctbeam_sPATL-mat18.1-0 ; 
       trctbeam_sPATL-mat19.1-0 ; 
       trctbeam_sPATL-mat2.1-0 ; 
       trctbeam_sPATL-mat20.1-0 ; 
       trctbeam_sPATL-mat21.1-0 ; 
       trctbeam_sPATL-mat3.1-0 ; 
       trctbeam_sPATL-mat4.1-0 ; 
       trctbeam_sPATL-mat6.1-0 ; 
       trctbeam_sPATL-mat7.1-0 ; 
       trctbeam_sPATL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       acs42-barrel.1-0 ; 
       acs42-body.1-0 ; 
       acs42-energy.1-0 ; 
       acs42-grav_mod1.1-0 ; 
       acs42-grav_mod2.1-0 ; 
       acs42-grav_mod3.1-0 ; 
       acs42-tractor_beam.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/acs42/PICTURES/acs42 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs42-trctbeam_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       trctbeam_sPATL-t2d1.1-0 ; 
       trctbeam_sPATL-t2d11.1-0 ; 
       trctbeam_sPATL-t2d12.1-0 ; 
       trctbeam_sPATL-t2d13.1-0 ; 
       trctbeam_sPATL-t2d14.1-0 ; 
       trctbeam_sPATL-t2d15.1-0 ; 
       trctbeam_sPATL-t2d16.1-0 ; 
       trctbeam_sPATL-t2d17.1-0 ; 
       trctbeam_sPATL-t2d18.1-0 ; 
       trctbeam_sPATL-t2d19.1-0 ; 
       trctbeam_sPATL-t2d20.1-0 ; 
       trctbeam_sPATL-t2d3.1-0 ; 
       trctbeam_sPATL-t2d5.1-0 ; 
       trctbeam_sPATL-t2d6.1-0 ; 
       trctbeam_sPATL-t2d8.1-0 ; 
       trctbeam_sPATL-t2d9.1-0 ; 
       trctbeam_sPATL-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 10 300 ; 
       0 12 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 11 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       4 17 300 ; 
       4 1 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       6 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 15 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 16 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -40.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -42.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -44.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -46.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.5 -31.5 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -14.5 0 MPRFLG 0 ; 
       2 SCHEM 7 -20.5 0 MPRFLG 0 ; 
       3 SCHEM 7 -37.5 0 MPRFLG 0 ; 
       4 SCHEM 7 -33.5 0 MPRFLG 0 ; 
       5 SCHEM 7 -29.5 0 MPRFLG 0 ; 
       6 SCHEM 0 -21.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -32.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -22.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -20.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -18.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7 -26.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7 -24.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10.5 -38.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10.5 -36.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 10.5 -30.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 10.5 -28.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10.5 -34.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10.5 -26.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -22.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -20.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -18.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -16.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10.5 -24.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -36.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -30.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -28.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -34.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -32.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -38.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
