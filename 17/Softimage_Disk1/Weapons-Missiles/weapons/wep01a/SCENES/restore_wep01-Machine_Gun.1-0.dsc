SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep01-wep01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       destroy-inf_light2_1_3_1_2_1.1-0 ROOT ; 
       destroy-inf_light4_1_3_1_2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       Machine_Gun-mat1.1-0 ; 
       Machine_Gun-mat10.1-0 ; 
       Machine_Gun-mat11.1-0 ; 
       Machine_Gun-mat12.1-0 ; 
       Machine_Gun-mat13.1-0 ; 
       Machine_Gun-mat14.1-0 ; 
       Machine_Gun-mat15.1-0 ; 
       Machine_Gun-mat16.1-0 ; 
       Machine_Gun-mat17.1-0 ; 
       Machine_Gun-mat18.1-0 ; 
       Machine_Gun-mat19.1-0 ; 
       Machine_Gun-mat2.1-0 ; 
       Machine_Gun-mat20.1-0 ; 
       Machine_Gun-mat21.1-0 ; 
       Machine_Gun-mat22.1-0 ; 
       Machine_Gun-mat23.1-0 ; 
       Machine_Gun-mat24.1-0 ; 
       Machine_Gun-mat25.1-0 ; 
       Machine_Gun-mat26.1-0 ; 
       Machine_Gun-mat27.1-0 ; 
       Machine_Gun-mat28.1-0 ; 
       Machine_Gun-mat29.1-0 ; 
       Machine_Gun-mat3.1-0 ; 
       Machine_Gun-mat30.1-0 ; 
       Machine_Gun-mat31.1-0 ; 
       Machine_Gun-mat32.1-0 ; 
       Machine_Gun-mat33.1-0 ; 
       Machine_Gun-mat4.1-0 ; 
       Machine_Gun-mat5.1-0 ; 
       Machine_Gun-mat6.1-0 ; 
       Machine_Gun-mat7.1-0 ; 
       Machine_Gun-mat8.1-0 ; 
       Machine_Gun-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       wep01-bwepbar.1-0 ; 
       wep01-wep01.1-0 ROOT ; 
       wep01-wepbar1.1-0 ; 
       wep01-wepbas1.1-0 ; 
       wep01-wepbas2.1-0 ; 
       wep01-wepbas3.1-0 ; 
       wep01-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/weapons/wep01a/PICTURES/wep01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       restore_wep01-Machine_Gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       Machine_Gun-t2d1.1-0 ; 
       Machine_Gun-t2d10.1-0 ; 
       Machine_Gun-t2d11.1-0 ; 
       Machine_Gun-t2d12.1-0 ; 
       Machine_Gun-t2d13.1-0 ; 
       Machine_Gun-t2d14.1-0 ; 
       Machine_Gun-t2d15.1-0 ; 
       Machine_Gun-t2d16.1-0 ; 
       Machine_Gun-t2d17.1-0 ; 
       Machine_Gun-t2d18.1-0 ; 
       Machine_Gun-t2d19.1-0 ; 
       Machine_Gun-t2d2.1-0 ; 
       Machine_Gun-t2d20.1-0 ; 
       Machine_Gun-t2d21.1-0 ; 
       Machine_Gun-t2d22.1-0 ; 
       Machine_Gun-t2d23.1-0 ; 
       Machine_Gun-t2d24.1-0 ; 
       Machine_Gun-t2d25.1-0 ; 
       Machine_Gun-t2d26.1-0 ; 
       Machine_Gun-t2d27.1-0 ; 
       Machine_Gun-t2d6.1-0 ; 
       Machine_Gun-t2d7.1-0 ; 
       Machine_Gun-t2d8.1-0 ; 
       Machine_Gun-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       2 22 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       3 0 300 ; 
       3 11 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 22 401 ; 
       3 23 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       10 6 401 ; 
       11 0 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 11 401 ; 
       31 20 401 ; 
       32 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 19 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 19 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 9.431816 -37.5 0 USR MPRFLG 0 ; 
       1 SCHEM 2.5 -25.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5.96293 -27.5 0 USR MPRFLG 0 ; 
       3 SCHEM 6 -24.5 0 USR MPRFLG 0 ; 
       4 SCHEM 9.5 -23.5 0 USR MPRFLG 0 ; 
       5 SCHEM 9.5 -25.5 0 USR MPRFLG 0 ; 
       6 SCHEM 9.46293 -42.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9.5 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.96293 -34.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.96293 -36.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.96293 -38.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.96293 -40.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9.5 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9.5 -2.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9.5 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9.5 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9.5 -18.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9.5 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9.5 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9.5 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9.5 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13 -22.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13 -24.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13 -26.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13 -28.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13 -20.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 13 -22.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13 -24.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 13 -26.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9.46293 -20.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9.46293 -30.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9.46293 -28.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9.46293 -26.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9.680729 -24.34443 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9.46293 -18.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9.46293 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9.46293 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9.46293 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9.46293 -22.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9.46293 -32.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 13 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.46293 -40.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 13 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 13 -0.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 13 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 13 -16.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -24.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -26.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 12.96293 -18.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -28.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -22.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -24.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -26.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.96293 -30.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 12.96293 -28.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 12.96293 -26.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 12.96293 -24.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.96293 -22.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.96293 -32.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.46293 -36.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.46293 -38.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6 -5.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
