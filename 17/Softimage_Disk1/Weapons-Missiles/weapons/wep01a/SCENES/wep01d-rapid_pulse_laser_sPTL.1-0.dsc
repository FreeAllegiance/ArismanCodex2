SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep01d-wep01d.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       destroy-inf_light2_1_3_1_2_1_1.1-0 ROOT ; 
       destroy-inf_light4_1_3_1_2_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       rapid_pulse_laser_sPTL-mat10.1-0 ; 
       rapid_pulse_laser_sPTL-mat11.1-0 ; 
       rapid_pulse_laser_sPTL-mat12.1-0 ; 
       rapid_pulse_laser_sPTL-mat13.1-0 ; 
       rapid_pulse_laser_sPTL-mat3.1-0 ; 
       rapid_pulse_laser_sPTL-mat30.1-0 ; 
       rapid_pulse_laser_sPTL-mat31.1-0 ; 
       rapid_pulse_laser_sPTL-mat32.1-0 ; 
       rapid_pulse_laser_sPTL-mat33.1-0 ; 
       rapid_pulse_laser_sPTL-mat4.1-0 ; 
       rapid_pulse_laser_sPTL-mat5.1-0 ; 
       rapid_pulse_laser_sPTL-mat6.1-0 ; 
       rapid_pulse_laser_sPTL-mat7.1-0 ; 
       rapid_pulse_laser_sPTL-mat8.1-0 ; 
       rapid_pulse_laser_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       wep01d-bwepbar.1-0 ; 
       wep01d-wep01d.1-0 ROOT ; 
       wep01d-wepbar1.1-0 ; 
       wep01d-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep01a/PICTURES/wep01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep01d-rapid_pulse_laser_sPTL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       rapid_pulse_laser_sPTL-t2d10.1-0 ; 
       rapid_pulse_laser_sPTL-t2d2.1-0 ; 
       rapid_pulse_laser_sPTL-t2d24.1-0 ; 
       rapid_pulse_laser_sPTL-t2d25.1-0 ; 
       rapid_pulse_laser_sPTL-t2d26.1-0 ; 
       rapid_pulse_laser_sPTL-t2d27.1-0 ; 
       rapid_pulse_laser_sPTL-t2d6.1-0 ; 
       rapid_pulse_laser_sPTL-t2d7.1-0 ; 
       rapid_pulse_laser_sPTL-t2d8.1-0 ; 
       rapid_pulse_laser_sPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       3 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       2 4 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 8 401 ; 
       2 9 401 ; 
       3 0 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 1 401 ; 
       13 6 401 ; 
       14 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -10.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -12.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.96293 -6.5 0 MPRFLG 0 ; 
       1 SCHEM 0 -5.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 3.46293 -7.5 0 USR MPRFLG 0 ; 
       3 SCHEM 6.96293 -8.5 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10.46293 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10.46293 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10.46293 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.46293 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.96293 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 13.96293 -6.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10.46293 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13.96293 -6.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 13.96293 -6.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -4.5 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 3 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
