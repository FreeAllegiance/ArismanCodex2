SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep02d-wep02d.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2_1.1-0 ROOT ; 
       pulse_laser_F-inf_light1.1-0 ROOT ; 
       pulse_laser_F-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       pulse_laser_F-mat43.1-0 ; 
       pulse_laser_F-mat45.1-0 ; 
       pulse_laser_F-mat46.1-0 ; 
       pulse_laser_F-mat47.1-0 ; 
       pulse_laser_F-mat48.1-0 ; 
       pulse_laser_P-mat16.1-0 ; 
       pulse_laser_P-mat17.1-0 ; 
       pulse_laser_P-mat18.1-0 ; 
       pulse_laser_P-mat39.1-0 ; 
       pulse_laser_P-mat40.1-0 ; 
       pulse_laser_P-mat41.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       wep02d-bwepbar.1-0 ; 
       wep02d-twepbar.1-0 ; 
       wep02d-wep02d.1-0 ROOT ; 
       wep02d-wepbar1.1-0 ; 
       wep02d-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/weapons/wep02a/PICTURES/wep02b ; 
       D:/Pete_Data/Softimage/weapons/wep02a/PICTURES/wep02c ; 
       D:/Pete_Data/Softimage/weapons/wep02a/PICTURES/wep09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep02d-pulse_laser_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       pulse_laser_F-t2d33.1-0 ; 
       pulse_laser_F-t2d34.1-0 ; 
       pulse_laser_F-t2d36.1-0 ; 
       pulse_laser_F-t2d37.1-0 ; 
       pulse_laser_F-t2d38.1-0 ; 
       pulse_laser_F-t2d39.1-0 ; 
       pulse_laser_F-t2d40.1-0 ; 
       pulse_laser_P-t2d12.1-0 ; 
       pulse_laser_P-t2d13.1-0 ; 
       pulse_laser_P-t2d30.1-0 ; 
       pulse_laser_P-t2d31.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       3 2 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 3 300 ; 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 0 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 2 401 ; 
       9 9 401 ; 
       10 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 7.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 USR MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 USR MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 11 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
