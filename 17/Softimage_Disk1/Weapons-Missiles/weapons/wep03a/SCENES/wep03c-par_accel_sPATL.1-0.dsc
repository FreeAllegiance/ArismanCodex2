SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep03-wep03.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4.1-0 ROOT ; 
       destroy-lite3_4.1-0 ROOT ; 
       par_accel_sPATL-light1.1-0 ROOT ; 
       par_accel_sPATL-lite1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       par_accel_sPATL-mat1.1-0 ; 
       par_accel_sPATL-mat10.1-0 ; 
       par_accel_sPATL-mat11.1-0 ; 
       par_accel_sPATL-mat12.1-0 ; 
       par_accel_sPATL-mat13.1-0 ; 
       par_accel_sPATL-mat14.1-0 ; 
       par_accel_sPATL-mat15.1-0 ; 
       par_accel_sPATL-mat16.1-0 ; 
       par_accel_sPATL-mat17.1-0 ; 
       par_accel_sPATL-mat2.1-0 ; 
       par_accel_sPATL-mat3.1-0 ; 
       par_accel_sPATL-mat4.1-0 ; 
       par_accel_sPATL-mat5.1-0 ; 
       par_accel_sPATL-mat6.1-0 ; 
       par_accel_sPATL-mat7.1-0 ; 
       par_accel_sPATL-mat8.1-0 ; 
       par_accel_sPATL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       wep03-wep03.1-0 ROOT ; 
       wep03-wepbar1.1-0 ; 
       wep03-wepbar2.1-0 ; 
       wep03-wepbar3.1-0 ; 
       wep03-wepbas1.1-0 ; 
       wep03-wepbas2.1-0 ; 
       wep03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep03a/PICTURES/Wep03a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep03c-par_accel_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       par_accel_sPATL-t2d1.1-0 ; 
       par_accel_sPATL-t2d10.1-0 ; 
       par_accel_sPATL-t2d11.1-0 ; 
       par_accel_sPATL-t2d12.1-0 ; 
       par_accel_sPATL-t2d13.1-0 ; 
       par_accel_sPATL-t2d14.1-0 ; 
       par_accel_sPATL-t2d15.1-0 ; 
       par_accel_sPATL-t2d16.1-0 ; 
       par_accel_sPATL-t2d2.1-0 ; 
       par_accel_sPATL-t2d3.1-0 ; 
       par_accel_sPATL-t2d4.1-0 ; 
       par_accel_sPATL-t2d5.1-0 ; 
       par_accel_sPATL-t2d6.1-0 ; 
       par_accel_sPATL-t2d7.1-0 ; 
       par_accel_sPATL-t2d8.1-0 ; 
       par_accel_sPATL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 16 300 ; 
       1 1 300 ; 
       1 7 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 14 401 ; 
       2 10 401 ; 
       3 11 401 ; 
       4 0 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 15 401 ; 
       8 13 401 ; 
       9 6 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
       14 5 401 ; 
       15 7 401 ; 
       16 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 13 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
