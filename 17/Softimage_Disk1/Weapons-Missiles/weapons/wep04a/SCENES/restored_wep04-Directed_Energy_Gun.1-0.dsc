SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep04-wep04.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.2-0 ROOT ; 
       destroy-lite3.2-0 ROOT ; 
       Directed_Energy_Gun-lite1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       Directed_Energy_Gun-mat1.1-0 ; 
       Directed_Energy_Gun-mat2.1-0 ; 
       Directed_Energy_Gun-mat3.1-0 ; 
       Directed_Energy_Gun-mat4.1-0 ; 
       Directed_Energy_Gun-mat5.1-0 ; 
       Directed_Energy_Gun-mat6.1-0 ; 
       Directed_Energy_Gun-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       wep04-wep04.1-0 ROOT ; 
       wep04-wepbar1.1-0 ; 
       wep04-wepbar2.1-0 ; 
       wep04-wepbar3.1-0 ; 
       wep04-wepbar4.1-0 ; 
       wep04-wepbar5.1-0 ; 
       wep04-wepbas1.1-0 ; 
       wep04-wepbas2.1-0 ; 
       wep04-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/weapons/wep04a/PICTURES/wep04b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       restored_wep04-Directed_Energy_Gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       Directed_Energy_Gun-t2d1.1-0 ; 
       Directed_Energy_Gun-t2d2.1-0 ; 
       Directed_Energy_Gun-t2d3.1-0 ; 
       Directed_Energy_Gun-t2d4.1-0 ; 
       Directed_Energy_Gun-t2d5.1-0 ; 
       Directed_Energy_Gun-t2d6.1-0 ; 
       Directed_Energy_Gun-t2d7.1-0 ; 
       Directed_Energy_Gun-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       5 2 300 ; 
       6 0 300 ; 
       7 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 2 400 ; 
       2 3 400 ; 
       5 1 400 ; 
       6 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 4 401 ; 
       3 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 0 -14.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 13 -0.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 13 -2.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -8.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -9.5 0 MPRFLG 0 ; 
       2 SCHEM 7 -6.5 0 MPRFLG 0 ; 
       3 SCHEM 7 -8.5 0 MPRFLG 0 ; 
       4 SCHEM 7 -10.5 0 MPRFLG 0 ; 
       5 SCHEM 7 -12.5 0 MPRFLG 0 ; 
       6 SCHEM 3.5 -4.5 0 MPRFLG 0 ; 
       7 SCHEM 7 -4.5 0 MPRFLG 0 ; 
       8 SCHEM 10.5 -6.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -12.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -12.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -2.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
