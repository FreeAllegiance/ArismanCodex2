SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep04d-wep04d.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       boosted_pl_sPATL-lite1.1-0 ROOT ; 
       destroy-lite2.1-0 ROOT ; 
       destroy-lite3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       boosted_pl_sPATL-mat3.1-0 ; 
       boosted_pl_sPATL-mat4.1-0 ; 
       boosted_pl_sPATL-mat5.1-0 ; 
       boosted_pl_sPATL-mat6.1-0 ; 
       boosted_pl_sPATL-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       wep04d-wep04d.1-0 ROOT ; 
       wep04d-wepbar1.1-0 ; 
       wep04d-wepbar2.1-0 ; 
       wep04d-wepbar3.1-0 ; 
       wep04d-wepbar4.1-0 ; 
       wep04d-wepbar5.1-0 ; 
       wep04d-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep04a/PICTURES/wep04b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep04d-boosted_pl_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       boosted_pl_sPATL-t2d2.1-0 ; 
       boosted_pl_sPATL-t2d3.1-0 ; 
       boosted_pl_sPATL-t2d4.1-0 ; 
       boosted_pl_sPATL-t2d6.1-0 ; 
       boosted_pl_sPATL-t2d7.1-0 ; 
       boosted_pl_sPATL-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       5 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 1 400 ; 
       2 2 400 ; 
       5 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 13 -0.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 13 -2.5 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -7.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -7.5 0 DISPLAY 3 2 MPRFLG 0 ; 
       2 SCHEM 7 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7 -6.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7 -8.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7 -10.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10.5 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10.5 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10.5 -10.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -2.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
