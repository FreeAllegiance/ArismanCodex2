SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep05d-wep05d.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2_1_2.1-0 ROOT ; 
       destroy-lite3_1.1-0 ROOT ; 
       super_pulse_laser-lite1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       super_pulse_laser-mat3.1-0 ; 
       super_pulse_laser-mat5.1-0 ; 
       super_pulse_laser-mat6.1-0 ; 
       super_pulse_laser-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       wep05d-bwepbar1.1-0 ; 
       wep05d-bwepbar2.1-0 ; 
       wep05d-twepbar1.1-0 ; 
       wep05d-twepbar2.1-0 ; 
       wep05d-wep05d.1-0 ROOT ; 
       wep05d-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep05a/PICTURES/wep05b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep05d-super_pl_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       super_pulse_laser-t2d4.1-0 ; 
       super_pulse_laser-t2d5.1-0 ; 
       super_pulse_laser-t2d6.1-0 ; 
       super_pulse_laser-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 0 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 2 300 ; 
       2 1 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
