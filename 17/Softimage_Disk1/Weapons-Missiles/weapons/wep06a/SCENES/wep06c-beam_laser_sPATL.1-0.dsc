SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep06-wep06.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       beam_laser_T-cam_int1.1-0 ROOT ; 
       beam_laser_T-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       beam_laser_T-light1_1.1-0 ROOT ; 
       beam_laser_T-light2_1.1-0 ROOT ; 
       beam_laser_T-light3_1.1-0 ROOT ; 
       beam_laser_T-light4_1.1-0 ROOT ; 
       destroy-lite2_4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       beam_laser_T-mat1.1-0 ; 
       beam_laser_T-mat10.1-0 ; 
       beam_laser_T-mat11.1-0 ; 
       beam_laser_T-mat12.1-0 ; 
       beam_laser_T-mat13.1-0 ; 
       beam_laser_T-mat14.1-0 ; 
       beam_laser_T-mat15.1-0 ; 
       beam_laser_T-mat16.1-0 ; 
       beam_laser_T-mat2.1-0 ; 
       beam_laser_T-mat3.1-0 ; 
       beam_laser_T-mat4.1-0 ; 
       beam_laser_T-mat5.1-0 ; 
       beam_laser_T-mat6.1-0 ; 
       beam_laser_T-mat7.1-0 ; 
       beam_laser_T-mat8.1-0 ; 
       beam_laser_T-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       wep06-wep06.1-0 ROOT ; 
       wep06-wepbar.1-0 ; 
       wep06-wepbas1.1-0 ; 
       wep06-wepbas2.1-0 ; 
       wep06-wepbas3.1-0 ; 
       wep06-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep06a/PICTURES/Wep06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep06c-beam_laser_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       beam_laser_T-t2d1.1-0 ; 
       beam_laser_T-t2d10.1-0 ; 
       beam_laser_T-t2d11.1-0 ; 
       beam_laser_T-t2d12.1-0 ; 
       beam_laser_T-t2d13.1-0 ; 
       beam_laser_T-t2d14.1-0 ; 
       beam_laser_T-t2d15.1-0 ; 
       beam_laser_T-t2d16.1-0 ; 
       beam_laser_T-t2d2.1-0 ; 
       beam_laser_T-t2d3.1-0 ; 
       beam_laser_T-t2d4.1-0 ; 
       beam_laser_T-t2d5.1-0 ; 
       beam_laser_T-t2d6.1-0 ; 
       beam_laser_T-t2d7.1-0 ; 
       beam_laser_T-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 7 300 ; 
       3 9 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       4 8 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 7 401 ; 
       8 0 401 ; 
       9 11 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -10.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -12.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -14.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -16.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -4.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -6.5 0 MPRFLG 0 ; 
       2 SCHEM 3.5 -3.5 0 MPRFLG 0 ; 
       3 SCHEM 7 -2.5 0 MPRFLG 0 ; 
       4 SCHEM 7 -4.5 0 MPRFLG 0 ; 
       5 SCHEM 7 -6.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -0.75 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 5 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
