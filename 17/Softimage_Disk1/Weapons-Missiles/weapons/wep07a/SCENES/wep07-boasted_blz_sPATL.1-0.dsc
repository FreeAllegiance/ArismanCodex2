SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep07-wep07.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       boasted_blz_sPATL-cam_int1.1-0 ROOT ; 
       boasted_blz_sPATL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       boasted_blz_sPATL-light1.1-0 ROOT ; 
       boasted_blz_sPATL-light2.1-0 ROOT ; 
       boasted_blz_sPATL-light3.1-0 ROOT ; 
       boasted_blz_sPATL-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       boasted_blz_sPATL-mat1.1-0 ; 
       boasted_blz_sPATL-mat10.1-0 ; 
       boasted_blz_sPATL-mat2.1-0 ; 
       boasted_blz_sPATL-mat3.1-0 ; 
       boasted_blz_sPATL-mat4.1-0 ; 
       boasted_blz_sPATL-mat5.1-0 ; 
       boasted_blz_sPATL-mat6.1-0 ; 
       boasted_blz_sPATL-mat7.1-0 ; 
       boasted_blz_sPATL-mat8.1-0 ; 
       boasted_blz_sPATL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       wep07-wep07.1-0 ROOT ; 
       wep07-wepbar1.1-0 ; 
       wep07-wepbar2.1-0 ; 
       wep07-wepbas.1-0 ; 
       wep07-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep07a/PICTURES/Wep07a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep07-boasted_blz_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       boasted_blz_sPATL-t2d1.1-0 ; 
       boasted_blz_sPATL-t2d2.1-0 ; 
       boasted_blz_sPATL-t2d3.1-0 ; 
       boasted_blz_sPATL-t2d4.1-0 ; 
       boasted_blz_sPATL-t2d5.1-0 ; 
       boasted_blz_sPATL-t2d6.1-0 ; 
       boasted_blz_sPATL-t2d7.1-0 ; 
       boasted_blz_sPATL-t2d8.1-0 ; 
       boasted_blz_sPATL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 3 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 2 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 8 401 ; 
       2 4 401 ; 
       3 2 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 3 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -6.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -8.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -10.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -12.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -3.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7 -2.5 0 MPRFLG 0 ; 
       2 SCHEM 3.5 -2.5 0 MPRFLG 0 ; 
       3 SCHEM 3.5 -4.5 0 MPRFLG 0 ; 
       4 SCHEM 10.5 -2.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -0.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 14 12 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
