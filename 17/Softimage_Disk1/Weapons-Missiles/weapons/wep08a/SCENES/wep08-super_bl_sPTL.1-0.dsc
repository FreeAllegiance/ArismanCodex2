SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       super_bl_sPTL-cam_int1.1-0 ROOT ; 
       super_bl_sPTL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       super_bl_sPTL-light1.1-0 ROOT ; 
       super_bl_sPTL-light2.1-0 ROOT ; 
       super_bl_sPTL-light3.1-0 ROOT ; 
       super_bl_sPTL-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       super_bl_sPTL-mat1.1-0 ; 
       super_bl_sPTL-mat2.1-0 ; 
       super_bl_sPTL-mat3.1-0 ; 
       super_bl_sPTL-mat4.1-0 ; 
       super_bl_sPTL-mat5.1-0 ; 
       super_bl_sPTL-mat6.1-0 ; 
       super_bl_sPTL-mat7.1-0 ; 
       super_bl_sPTL-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       wep08-wep08.1-0 ROOT ; 
       wep08-wepbar1.1-0 ; 
       wep08-wepbar2.1-0 ; 
       wep08-wepbas1.1-0 ; 
       wep08-wepbas2.1-0 ; 
       wep08-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep08a/PICTURES/Wep08a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep08-super_bl_sPTL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       super_bl_sPTL-t2d1.1-0 ; 
       super_bl_sPTL-t2d2.1-0 ; 
       super_bl_sPTL-t2d3.1-0 ; 
       super_bl_sPTL-t2d4.1-0 ; 
       super_bl_sPTL-t2d5.1-0 ; 
       super_bl_sPTL-t2d6.1-0 ; 
       super_bl_sPTL-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 2 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 1 300 ; 
       4 3 300 ; 
       4 6 300 ; 
       4 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 4 401 ; 
       7 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -6.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -8.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -10.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -12.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -3.5 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -4.5 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 3.5 -2.5 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7 -2.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 10.5 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
