SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep09-wep09.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       il_heavyfighter_sAT-spot1_11.1-0 ; 
       il_heavyfighter_sAT-spot1_int_1_1_1_3.1-0 ROOT ; 
       meson_cannon_sPATL-light1_4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       meson_cannon_sPATL-mat1.1-0 ; 
       meson_cannon_sPATL-mat10.1-0 ; 
       meson_cannon_sPATL-mat11.1-0 ; 
       meson_cannon_sPATL-mat12.1-0 ; 
       meson_cannon_sPATL-mat13.1-0 ; 
       meson_cannon_sPATL-mat14.1-0 ; 
       meson_cannon_sPATL-mat15.1-0 ; 
       meson_cannon_sPATL-mat16_1.1-0 ; 
       meson_cannon_sPATL-mat17_1.1-0 ; 
       meson_cannon_sPATL-mat18_1.1-0 ; 
       meson_cannon_sPATL-mat19_1.1-0 ; 
       meson_cannon_sPATL-mat2.1-0 ; 
       meson_cannon_sPATL-mat20_1.1-0 ; 
       meson_cannon_sPATL-mat21_1.1-0 ; 
       meson_cannon_sPATL-mat22_1.1-0 ; 
       meson_cannon_sPATL-mat23_1.1-0 ; 
       meson_cannon_sPATL-mat24_1.1-0 ; 
       meson_cannon_sPATL-mat25_1.1-0 ; 
       meson_cannon_sPATL-mat26_1.1-0 ; 
       meson_cannon_sPATL-mat27_1.1-0 ; 
       meson_cannon_sPATL-mat28_1.1-0 ; 
       meson_cannon_sPATL-mat29_1.1-0 ; 
       meson_cannon_sPATL-mat3.1-0 ; 
       meson_cannon_sPATL-mat30_1.1-0 ; 
       meson_cannon_sPATL-mat31_1.1-0 ; 
       meson_cannon_sPATL-mat32_1.1-0 ; 
       meson_cannon_sPATL-mat33_1.1-0 ; 
       meson_cannon_sPATL-mat34_1.1-0 ; 
       meson_cannon_sPATL-mat35_1.1-0 ; 
       meson_cannon_sPATL-mat36_1.1-0 ; 
       meson_cannon_sPATL-mat37_1.1-0 ; 
       meson_cannon_sPATL-mat38.1-0 ; 
       meson_cannon_sPATL-mat39_1.1-0 ; 
       meson_cannon_sPATL-mat4.1-0 ; 
       meson_cannon_sPATL-mat5.1-0 ; 
       meson_cannon_sPATL-mat6.1-0 ; 
       meson_cannon_sPATL-mat7.1-0 ; 
       meson_cannon_sPATL-mat8.1-0 ; 
       meson_cannon_sPATL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       wep09-wep09.1-0 ROOT ; 
       wep09-wepbar1.1-0 ; 
       wep09-wepbar2.1-0 ; 
       wep09-wepbar3.1-0 ; 
       wep09-wepbar4.1-0 ; 
       wep09-wepbar5.1-0 ; 
       wep09-wepbas1.1-0 ; 
       wep09-wepbas2.1-0 ; 
       wep09-wepbas3.1-0 ; 
       wep09-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep09a/PICTURES/wep09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep09-meson_cannon_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       meson_cannon_sPATL-t2d1.1-0 ; 
       meson_cannon_sPATL-t2d10.1-0 ; 
       meson_cannon_sPATL-t2d11.1-0 ; 
       meson_cannon_sPATL-t2d12_1.1-0 ; 
       meson_cannon_sPATL-t2d13_1.1-0 ; 
       meson_cannon_sPATL-t2d14_1.1-0 ; 
       meson_cannon_sPATL-t2d15_1.1-0 ; 
       meson_cannon_sPATL-t2d16_1.1-0 ; 
       meson_cannon_sPATL-t2d17_1.1-0 ; 
       meson_cannon_sPATL-t2d18_1.1-0 ; 
       meson_cannon_sPATL-t2d19_1.1-0 ; 
       meson_cannon_sPATL-t2d2.1-0 ; 
       meson_cannon_sPATL-t2d20_1.1-0 ; 
       meson_cannon_sPATL-t2d21_1.1-0 ; 
       meson_cannon_sPATL-t2d22_1.1-0 ; 
       meson_cannon_sPATL-t2d23_1.1-0 ; 
       meson_cannon_sPATL-t2d24_1.1-0 ; 
       meson_cannon_sPATL-t2d25_1.1-0 ; 
       meson_cannon_sPATL-t2d26_1.1-0 ; 
       meson_cannon_sPATL-t2d27_1.1-0 ; 
       meson_cannon_sPATL-t2d28_1.1-0 ; 
       meson_cannon_sPATL-t2d29.1-0 ; 
       meson_cannon_sPATL-t2d3.1-0 ; 
       meson_cannon_sPATL-t2d30_1.1-0 ; 
       meson_cannon_sPATL-t2d4.1-0 ; 
       meson_cannon_sPATL-t2d5.1-0 ; 
       meson_cannon_sPATL-t2d6.1-0 ; 
       meson_cannon_sPATL-t2d7.1-0 ; 
       meson_cannon_sPATL-t2d8.1-0 ; 
       meson_cannon_sPATL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 4 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 32 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 30 300 ; 
       5 10 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       6 0 300 ; 
       6 11 300 ; 
       6 22 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 31 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       8 1 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 28 401 ; 
       2 29 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       11 0 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       21 13 401 ; 
       22 11 401 ; 
       23 14 401 ; 
       24 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 23 401 ; 
       33 22 401 ; 
       34 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       38 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -8 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 15 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
