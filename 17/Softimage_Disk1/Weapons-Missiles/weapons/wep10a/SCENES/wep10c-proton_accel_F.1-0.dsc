SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep10c-wep10c.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-inf_light2_1_3_1_2_1_2_1.1-0 ROOT ; 
       destroy-inf_light4_1_3_1_2_1_2_1.1-0 ROOT ; 
       il_heavyfighter_sAT-spot1.1-0 ; 
       il_heavyfighter_sAT-spot1_int_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       proton_accel_F-mat10.1-0 ; 
       proton_accel_F-mat11.1-0 ; 
       proton_accel_F-mat12.1-0 ; 
       proton_accel_F-mat13.1-0 ; 
       proton_accel_F-mat14.1-0 ; 
       proton_accel_F-mat15.1-0 ; 
       proton_accel_F-mat16.1-0 ; 
       proton_accel_F-mat17.1-0 ; 
       proton_accel_F-mat18.1-0 ; 
       proton_accel_F-mat19.1-0 ; 
       proton_accel_F-mat20.1-0 ; 
       proton_accel_F-mat21.1-0 ; 
       proton_accel_F-mat22.1-0 ; 
       proton_accel_F-mat23.1-0 ; 
       proton_accel_F-mat24.1-0 ; 
       proton_accel_F-mat25.1-0 ; 
       proton_accel_F-mat26.1-0 ; 
       proton_accel_F-mat27.1-0 ; 
       proton_accel_F-mat28.1-0 ; 
       proton_accel_F-mat29.1-0 ; 
       proton_accel_F-mat30.1-0 ; 
       proton_accel_F-mat31.1-0 ; 
       proton_accel_F-mat32.1-0 ; 
       proton_accel_F-mat33.1-0 ; 
       proton_accel_F-mat34.1-0 ; 
       proton_accel_F-mat35.1-0 ; 
       proton_accel_F-mat36.1-0 ; 
       proton_accel_F-mat37.1-0 ; 
       proton_accel_F-mat4.1-0 ; 
       proton_accel_F-mat5.1-0 ; 
       proton_accel_F-mat52.1-0 ; 
       proton_accel_F-mat53.1-0 ; 
       proton_accel_F-mat54.1-0 ; 
       proton_accel_F-mat55.1-0 ; 
       proton_accel_F-mat56.1-0 ; 
       proton_accel_F-mat57.1-0 ; 
       proton_accel_F-mat58.1-0 ; 
       proton_accel_F-mat59.1-0 ; 
       proton_accel_F-mat6.1-0 ; 
       proton_accel_F-mat60.1-0 ; 
       proton_accel_F-mat61.1-0 ; 
       proton_accel_F-mat62.1-0 ; 
       proton_accel_F-mat63.1-0 ; 
       proton_accel_F-mat64.1-0 ; 
       proton_accel_F-mat65.1-0 ; 
       proton_accel_F-mat66.1-0 ; 
       proton_accel_F-mat67.1-0 ; 
       proton_accel_F-mat68.1-0 ; 
       proton_accel_F-mat69.1-0 ; 
       proton_accel_F-mat7.1-0 ; 
       proton_accel_F-mat70.1-0 ; 
       proton_accel_F-mat71.1-0 ; 
       proton_accel_F-mat72.1-0 ; 
       proton_accel_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       wep10c-wep10c.1-0 ROOT ; 
       wep10c-wepbar0.1-0 ; 
       wep10c-wepbar1.1-0 ; 
       wep10c-wepbar2.1-0 ; 
       wep10c-wepbar3.1-0 ; 
       wep10c-wepbar4.1-0 ; 
       wep10c-wepbar5.1-0 ; 
       wep10c-wepbar6.1-0 ; 
       wep10c-wepbar7.1-0 ; 
       wep10c-wepbar8.1-0 ; 
       wep10c-wepbar9.1-0 ; 
       wep10c-wepbas1.1-0 ; 
       wep10c-wepbas2.1-0 ; 
       wep10c-wepbas3.1-0 ; 
       wep10c-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep10a/PICTURES/wep10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep10c-proton_accel_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       proton_accel_F-t2d10.1-0 ; 
       proton_accel_F-t2d11.1-0 ; 
       proton_accel_F-t2d12.1-0 ; 
       proton_accel_F-t2d13.1-0 ; 
       proton_accel_F-t2d14.1-0 ; 
       proton_accel_F-t2d15.1-0 ; 
       proton_accel_F-t2d16.1-0 ; 
       proton_accel_F-t2d17.1-0 ; 
       proton_accel_F-t2d18.1-0 ; 
       proton_accel_F-t2d19.1-0 ; 
       proton_accel_F-t2d20.1-0 ; 
       proton_accel_F-t2d21.1-0 ; 
       proton_accel_F-t2d22.1-0 ; 
       proton_accel_F-t2d23.1-0 ; 
       proton_accel_F-t2d24.1-0 ; 
       proton_accel_F-t2d25.1-0 ; 
       proton_accel_F-t2d26.1-0 ; 
       proton_accel_F-t2d27.1-0 ; 
       proton_accel_F-t2d28.1-0 ; 
       proton_accel_F-t2d29.1-0 ; 
       proton_accel_F-t2d3.1-0 ; 
       proton_accel_F-t2d4.1-0 ; 
       proton_accel_F-t2d40.1-0 ; 
       proton_accel_F-t2d41.1-0 ; 
       proton_accel_F-t2d42.1-0 ; 
       proton_accel_F-t2d43.1-0 ; 
       proton_accel_F-t2d44.1-0 ; 
       proton_accel_F-t2d45.1-0 ; 
       proton_accel_F-t2d46.1-0 ; 
       proton_accel_F-t2d47.1-0 ; 
       proton_accel_F-t2d48.1-0 ; 
       proton_accel_F-t2d49.1-0 ; 
       proton_accel_F-t2d5.1-0 ; 
       proton_accel_F-t2d50.1-0 ; 
       proton_accel_F-t2d51.1-0 ; 
       proton_accel_F-t2d52.1-0 ; 
       proton_accel_F-t2d53.1-0 ; 
       proton_accel_F-t2d54.1-0 ; 
       proton_accel_F-t2d7.1-0 ; 
       proton_accel_F-t2d8.1-0 ; 
       proton_accel_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 10 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 28 300 ; 
       2 29 300 ; 
       2 38 300 ; 
       2 49 300 ; 
       3 53 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       10 27 300 ; 
       11 34 300 ; 
       11 35 300 ; 
       11 36 300 ; 
       11 37 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       11 42 300 ; 
       11 43 300 ; 
       11 44 300 ; 
       11 45 300 ; 
       12 50 300 ; 
       12 51 300 ; 
       12 52 300 ; 
       13 46 300 ; 
       13 47 300 ; 
       13 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 38 401 ; 
       1 39 401 ; 
       2 40 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       29 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 21 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       45 33 401 ; 
       47 34 401 ; 
       48 35 401 ; 
       49 32 401 ; 
       51 36 401 ; 
       52 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -2 0 SRT 1 1 1 0 0 -4.701977e-038 0 0 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 0.9418535 -8 0 USR MPRFLG 0 ; 
       3 SCHEM 3.858609 -8 0 USR MPRFLG 0 ; 
       4 SCHEM 6.532918 -8 0 USR MPRFLG 0 ; 
       5 SCHEM 9.247035 -8 0 USR MPRFLG 0 ; 
       6 SCHEM 12.11158 -8 0 USR MPRFLG 0 ; 
       7 SCHEM 14.90002 -7.980977 0 USR MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 USR MPRFLG 0 ; 
       9 SCHEM 0 -6 0 USR MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 USR MPRFLG 0 ; 
       11 SCHEM 21.25 -4 0 USR MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 0.9418535 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.858609 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.858609 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.858609 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5.532918 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5.532918 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5.532918 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5.532918 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 8.247035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.247035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 8.247035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 8.247035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.11158 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.11158 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.11158 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.11158 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13.90002 -9.980977 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13.90002 -9.980977 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13.90002 -9.980977 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13.90002 -9.980977 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2.441854 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 2.441854 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 2.441854 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 2.441854 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 2.858609 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5.532918 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5.532918 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5.532918 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 8.247035 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 8.247035 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 8.247035 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.11158 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.11158 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.11158 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 13.90002 -11.98098 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 13.90002 -11.98098 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 13.90002 -11.98098 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 2.441854 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 2.441854 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 2.441854 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 2.858609 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 2.858609 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 2.858609 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 24 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 15 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
