SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep11-wep11.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2_1.1-0 ROOT ; 
       destroy-lite3_1.1-0 ROOT ; 
       iso_proton_disrupt_sPATL-lite1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       iso_proton_disrupt_sPATL-mat1.1-0 ; 
       iso_proton_disrupt_sPATL-mat2.1-0 ; 
       iso_proton_disrupt_sPATL-mat3.1-0 ; 
       iso_proton_disrupt_sPATL-mat4.1-0 ; 
       iso_proton_disrupt_sPATL-mat5.1-0 ; 
       iso_proton_disrupt_sPATL-mat6.1-0 ; 
       iso_proton_disrupt_sPATL-mat7.1-0 ; 
       iso_proton_disrupt_sPATL-mat8.1-0 ; 
       iso_proton_disrupt_sPATL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       wep11-wep11.1-0 ROOT ; 
       wep11-wepbar1.1-0 ; 
       wep11-wepbar2.1-0 ; 
       wep11-wepbas1.1-0 ; 
       wep11-wepbas2.1-0 ; 
       wep11-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep11a/PICTURES/wep11B ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep11-iso_proton_disrupt_sPATL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       iso_proton_disrupt_sPATL-t2d1.1-0 ; 
       iso_proton_disrupt_sPATL-t2d2.1-0 ; 
       iso_proton_disrupt_sPATL-t2d3.1-0 ; 
       iso_proton_disrupt_sPATL-t2d4.1-0 ; 
       iso_proton_disrupt_sPATL-t2d5.1-0 ; 
       iso_proton_disrupt_sPATL-t2d6.1-0 ; 
       iso_proton_disrupt_sPATL-t2d7.1-0 ; 
       iso_proton_disrupt_sPATL-t2d8.1-0 ; 
       iso_proton_disrupt_sPATL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       4 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -10.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -12.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -14.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7 -5.5 0 MPRFLG 0 ; 
       2 SCHEM 3.5 -5.5 0 MPRFLG 0 ; 
       3 SCHEM 3.5 -7.5 0 USR DISPLAY 3 2 MPRFLG 0 ; 
       4 SCHEM 7 -7.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 10.5 -5.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10.5 -3.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -3.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -3.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -5.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7 -5.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7 -5.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -5.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7 -5.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -7.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -3.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -3.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -3.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -5.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -5.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -5.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -5.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -5.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -7.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -3.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 15 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
