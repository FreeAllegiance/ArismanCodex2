SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep18-wep18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       Lightning_Bolt-light1.1-0 ROOT ; 
       Lightning_Bolt-light2.1-0 ROOT ; 
       Lightning_Bolt-light3.1-0 ROOT ; 
       Lightning_Bolt-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       Lightning_Bolt-mat1.1-0 ; 
       Lightning_Bolt-mat10.1-0 ; 
       Lightning_Bolt-mat11.1-0 ; 
       Lightning_Bolt-mat12.1-0 ; 
       Lightning_Bolt-mat13.1-0 ; 
       Lightning_Bolt-mat14.1-0 ; 
       Lightning_Bolt-mat15.1-0 ; 
       Lightning_Bolt-mat16.1-0 ; 
       Lightning_Bolt-mat2.1-0 ; 
       Lightning_Bolt-mat3.1-0 ; 
       Lightning_Bolt-mat4.1-0 ; 
       Lightning_Bolt-mat5.1-0 ; 
       Lightning_Bolt-mat6.1-0 ; 
       Lightning_Bolt-mat7.1-0 ; 
       Lightning_Bolt-mat8.1-0 ; 
       Lightning_Bolt-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       wep18-wep18.1-0 ROOT ; 
       wep18-wepbar1.1-0 ; 
       wep18-wepbar2.1-0 ; 
       wep18-wepbar3.1-0 ; 
       wep18-wepbas1.1-0 ; 
       wep18-wepbas2.1-0 ; 
       wep18-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/weapons/wep18a/PICTURES/Wep18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       restore_wep18-Lightning_Bolt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       Lightning_Bolt-t2d1.1-0 ; 
       Lightning_Bolt-t2d10.1-0 ; 
       Lightning_Bolt-t2d11.1-0 ; 
       Lightning_Bolt-t2d12.1-0 ; 
       Lightning_Bolt-t2d13.1-0 ; 
       Lightning_Bolt-t2d14.1-0 ; 
       Lightning_Bolt-t2d15.1-0 ; 
       Lightning_Bolt-t2d2.1-0 ; 
       Lightning_Bolt-t2d3.1-0 ; 
       Lightning_Bolt-t2d4.1-0 ; 
       Lightning_Bolt-t2d5.1-0 ; 
       Lightning_Bolt-t2d6.1-0 ; 
       Lightning_Bolt-t2d7.1-0 ; 
       Lightning_Bolt-t2d8.1-0 ; 
       Lightning_Bolt-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 15 300 ; 
       1 1 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 14 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 0 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -2 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 -0.05876711 -0.06054794 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 0 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
