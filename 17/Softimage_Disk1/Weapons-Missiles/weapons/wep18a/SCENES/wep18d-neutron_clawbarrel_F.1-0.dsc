SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       wep18d-wep18d.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       neutron_clawbarrel_F-light1.1-0 ROOT ; 
       neutron_clawbarrel_F-light2.1-0 ROOT ; 
       neutron_clawbarrel_F-light3.1-0 ROOT ; 
       neutron_clawbarrel_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       neutron_clawbarrel_F-mat1.1-0 ; 
       neutron_clawbarrel_F-mat10.1-0 ; 
       neutron_clawbarrel_F-mat11.1-0 ; 
       neutron_clawbarrel_F-mat12.1-0 ; 
       neutron_clawbarrel_F-mat2.1-0 ; 
       neutron_clawbarrel_F-mat3.1-0 ; 
       neutron_clawbarrel_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       wep18d-wep18d.1-0 ROOT ; 
       wep18d-wepbar1.1-0 ; 
       wep18d-wepbar2.1-0 ; 
       wep18d-wepbar5.1-0 ; 
       wep18d-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/weapons/wep18a/PICTURES/Wep18a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep18d-neutron_clawbarrel_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       neutron_clawbarrel_F-t2d1.1-0 ; 
       neutron_clawbarrel_F-t2d10.1-0 ; 
       neutron_clawbarrel_F-t2d11.1-0 ; 
       neutron_clawbarrel_F-t2d2.1-0 ; 
       neutron_clawbarrel_F-t2d8.1-0 ; 
       neutron_clawbarrel_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 6 300 ; 
       1 1 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 2 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 5 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 0 401 ; 
       5 3 401 ; 
       6 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -20.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -22.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -24.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -26.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -11.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -12.5 0 MPRFLG 0 ; 
       2 SCHEM 7 -15.5 0 MPRFLG 0 ; 
       3 SCHEM 7 -11.5 0 MPRFLG 0 ; 
       4 SCHEM 7 -18.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -16.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -4.75 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 15 0 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
