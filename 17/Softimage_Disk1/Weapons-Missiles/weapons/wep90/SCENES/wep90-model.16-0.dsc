SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.16-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       model-mat1.1-0 ; 
       model-mat10.1-0 ; 
       model-mat11.1-0 ; 
       model-mat12.1-0 ; 
       model-mat6.1-0 ; 
       model-mat7.2-0 ; 
       model-mat8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       model-cube1.1-0 ; 
       model-cyl4.1-0 ; 
       model-cyl5.1-0 ; 
       model-cyl6.1-0 ; 
       model-cyl7.1-0 ; 
       model-cyl8.1-0 ; 
       model-null1.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/Weapons-Missiles/weapons/wep90/PICTURES/wep90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep90-model.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       model-t2d1.2-0 ; 
       model-t2d2.1-0 ; 
       model-t2d4.1-0 ; 
       model-t2d5.1-0 ; 
       model-t2d6.2-0 ; 
       model-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 6 110 ; 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 5 300 ; 
       0 4 300 ; 
       6 0 300 ; 
       1 2 300 ; 
       2 1 300 ; 
       3 3 300 ; 
       4 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 4 401 ; 
       6 0 401 ; 
       3 5 401 ; 
       1 1 401 ; 
       4 3 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
