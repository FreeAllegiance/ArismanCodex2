SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.6-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       re_app-mat35.1-0 ; 
       re_app-mat36.1-0 ; 
       re_app-mat37.1-0 ; 
       re_app-mat38.1-0 ; 
       re_app-mat39.1-0 ; 
       re_app-mat40.1-0 ; 
       ss300_outpost-white_strobe1_46.1-0 ; 
       ss300_outpost-white_strobe1_47.1-0 ; 
       ss300_outpost-white_strobe1_48.1-0 ; 
       ss300_outpost-white_strobe1_49.1-0 ; 
       ss300_outpost-white_strobe1_50.1-0 ; 
       ss300_outpost-white_strobe1_51.1-0 ; 
       ss300_outpost-white_strobe1_52.1-0 ; 
       ss301_garrison-white_strobe1_33.1-0 ; 
       ss301_garrison-white_strobe1_34.1-0 ; 
       ss304_research-white_strobe1_35.1-0 ; 
       ss304_research-white_strobe1_36.1-0 ; 
       ss304_research-white_strobe1_37.1-0 ; 
       ss304_research-white_strobe1_38.1-0 ; 
       ss304_research-white_strobe1_39.1-0 ; 
       ss304_research-white_strobe1_40.1-0 ; 
       ss304_research-white_strobe1_41.1-0 ; 
       ss304_research-white_strobe1_42.1-0 ; 
       ss304_research-white_strobe1_43.1-0 ; 
       ss304_research-white_strobe1_44.1-0 ; 
       ss304_research-white_strobe1_45.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss307_miningplat-mat10.1-0 ; 
       ss307_miningplat-mat12.1-0 ; 
       ss307_miningplat-mat13.1-0 ; 
       ss307_miningplat-mat14.1-0 ; 
       ss307_miningplat-mat15.1-0 ; 
       ss307_miningplat-mat16.1-0 ; 
       ss307_miningplat-mat25.1-0 ; 
       ss307_miningplat-mat26.1-0 ; 
       ss307_miningplat-mat27.1-0 ; 
       ss307_miningplat-mat28.1-0 ; 
       ss307_miningplat-mat29.1-0 ; 
       ss307_miningplat-mat30.1-0 ; 
       ss307_miningplat-mat32.1-0 ; 
       ss307_miningplat-mat33.1-0 ; 
       ss307_miningplat-mat34.1-0 ; 
       ss307_miningplat-mat7.1-0 ; 
       ss307_miningplat-mat8.1-0 ; 
       ss307_miningplat-mat9.1-0 ; 
       ss307_miningplat-white_strobe1_53.1-0 ; 
       ss307_miningplat-white_strobe1_54.1-0 ; 
       ss307_miningplat-white_strobe1_55.1-0 ; 
       ss307_miningplat-white_strobe1_56.1-0 ; 
       ss307_miningplat-white_strobe1_57.1-0 ; 
       ss307_miningplat-white_strobe1_58.1-0 ; 
       ss307_miningplat-white_strobe1_59.1-0 ; 
       ss307_miningplat-white_strobe1_60.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       root-cube1.2-0 ; 
       root-cube14.1-0 ; 
       root-cube17.1-0 ; 
       root-cube17_1.1-0 ; 
       root-cube22.1-0 ; 
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube25.1-0 ; 
       root-cube26.1-0 ; 
       root-cube27.1-0 ; 
       root-cube28.1-0 ; 
       root-cube3.1-0 ; 
       root-cube5.1-0 ; 
       root-cube6.1-0 ; 
       root-cube7.1-0 ; 
       root-cube9.1-0 ; 
       root-cyl1.1-0 ; 
       root-cyl1_1.1-0 ; 
       root-flag_mount.1-0 ; 
       root-null18.1-0 ; 
       root-null18_1.1-0 ; 
       root-null19.1-0 ; 
       root-null19_1.1-0 ; 
       root-null20.1-0 ; 
       root-null21.1-0 ; 
       root-root_1_1.3-0 ROOT ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_33_1.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_34_1.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_35_1.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_36_1.1-0 ; 
       root-SS_40.1-0 ; 
       root-SS_41.1-0 ; 
       root-SS_42.1-0 ; 
       root-SS_42_1.1-0 ; 
       root-SS_43.1-0 ; 
       root-SS_44.1-0 ; 
       root-SS_45.1-0 ; 
       root-SS_46.1-0 ; 
       root-SS_47.1-0 ; 
       root-SS_48.1-0 ; 
       root-SS_49.1-0 ; 
       root-SS_50.1-0 ; 
       root-SS_51.1-0 ; 
       root-SS_52.1-0 ; 
       root-SS_53.1-0 ; 
       root-SS_53_1.1-0 ; 
       root-SS_54.1-0 ; 
       root-SS_55.1-0 ; 
       root-SS_56.1-0 ; 
       root-SS_57.1-0 ; 
       root-SS_58.1-0 ; 
       root-SS_59.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_61.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/ss307 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Belters_Platform-ROTATE.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       re_app-t2d33.1-0 ; 
       re_app-t2d34.1-0 ; 
       re_app-t2d35.1-0 ; 
       re_app-t2d36.1-0 ; 
       re_app-t2d37.1-0 ; 
       re_app-t2d38.1-0 ; 
       ss307_miningplat-t2d10.1-0 ; 
       ss307_miningplat-t2d12.1-0 ; 
       ss307_miningplat-t2d13.1-0 ; 
       ss307_miningplat-t2d14.1-0 ; 
       ss307_miningplat-t2d15.1-0 ; 
       ss307_miningplat-t2d16.1-0 ; 
       ss307_miningplat-t2d17.1-0 ; 
       ss307_miningplat-t2d18.1-0 ; 
       ss307_miningplat-t2d19.1-0 ; 
       ss307_miningplat-t2d20.1-0 ; 
       ss307_miningplat-t2d21.1-0 ; 
       ss307_miningplat-t2d22.1-0 ; 
       ss307_miningplat-t2d28.1-0 ; 
       ss307_miningplat-t2d29.1-0 ; 
       ss307_miningplat-t2d30.1-0 ; 
       ss307_miningplat-t2d31.1-0 ; 
       ss307_miningplat-t2d32.1-0 ; 
       ss307_miningplat-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 25 110 ; 
       1 14 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 0 110 ; 
       10 9 110 ; 
       11 0 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 0 110 ; 
       15 11 110 ; 
       16 0 110 ; 
       17 11 110 ; 
       18 25 110 ; 
       19 25 110 ; 
       20 25 110 ; 
       21 25 110 ; 
       22 25 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 21 110 ; 
       29 22 110 ; 
       30 21 110 ; 
       31 22 110 ; 
       32 21 110 ; 
       33 22 110 ; 
       34 21 110 ; 
       35 22 110 ; 
       36 20 110 ; 
       37 20 110 ; 
       38 21 110 ; 
       39 22 110 ; 
       40 23 110 ; 
       41 23 110 ; 
       42 23 110 ; 
       43 23 110 ; 
       44 19 110 ; 
       45 19 110 ; 
       46 19 110 ; 
       47 19 110 ; 
       48 19 110 ; 
       49 19 110 ; 
       50 21 110 ; 
       51 22 110 ; 
       52 24 110 ; 
       53 24 110 ; 
       54 24 110 ; 
       55 24 110 ; 
       56 24 110 ; 
       57 24 110 ; 
       58 20 110 ; 
       59 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 43 300 ; 
       0 42 300 ; 
       1 32 300 ; 
       1 36 300 ; 
       2 40 300 ; 
       3 41 300 ; 
       4 37 300 ; 
       5 0 300 ; 
       6 1 300 ; 
       7 2 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       10 5 300 ; 
       11 44 300 ; 
       11 34 300 ; 
       11 35 300 ; 
       12 30 300 ; 
       13 31 300 ; 
       14 29 300 ; 
       14 38 300 ; 
       15 28 300 ; 
       16 33 300 ; 
       16 39 300 ; 
       17 45 300 ; 
       26 27 300 ; 
       27 27 300 ; 
       28 26 300 ; 
       29 10 300 ; 
       30 26 300 ; 
       31 9 300 ; 
       32 26 300 ; 
       33 8 300 ; 
       34 26 300 ; 
       35 7 300 ; 
       36 14 300 ; 
       37 13 300 ; 
       38 15 300 ; 
       39 11 300 ; 
       40 16 300 ; 
       41 17 300 ; 
       42 18 300 ; 
       43 19 300 ; 
       44 20 300 ; 
       45 21 300 ; 
       46 22 300 ; 
       47 23 300 ; 
       48 24 300 ; 
       49 25 300 ; 
       50 6 300 ; 
       51 12 300 ; 
       52 46 300 ; 
       53 47 300 ; 
       54 48 300 ; 
       55 49 300 ; 
       56 50 300 ; 
       57 51 300 ; 
       58 52 300 ; 
       59 53 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       28 16 401 ; 
       29 12 401 ; 
       30 10 401 ; 
       31 11 401 ; 
       32 13 401 ; 
       33 6 401 ; 
       34 8 401 ; 
       35 9 401 ; 
       36 14 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 19 401 ; 
       40 20 401 ; 
       41 21 401 ; 
       42 22 401 ; 
       43 23 401 ; 
       44 7 401 ; 
       45 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 5 -6 0 MPRFLG 0 ; 
       18 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 108.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 81.25 -2 0 MPRFLG 0 ; 
       24 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       25 SCHEM 58.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 75 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 80 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 35 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 45 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 30 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 55 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 40 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 77.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 85 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 87.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 60 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 62.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 65 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 70 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 72.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 42.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 95 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 90 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 97.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 92.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 100 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 102.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 109 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 96.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 99 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 104 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 114 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
