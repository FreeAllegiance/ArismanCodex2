SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.6-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       re_app-cube14.1-0 ; 
       re_app-cube15.1-0 ; 
       re_app-cube27.1-0 ; 
       re_app-cube7.5-0 ROOT ; 
       re_app-cyl13.1-0 ; 
       re_app-cyl14.1-0 ; 
       re_app-cyl15.1-0 ; 
       re_app-cyl16.1-0 ; 
       re_app-cyl17.1-0 ; 
       re_app-cyl18.1-0 ; 
       re_app-cyl19.1-0 ; 
       re_app-cyl7.1-0 ; 
       re_app-flag_mount.1-0 ; 
       re_app-null1.1-0 ; 
       re_app-null18.1-0 ; 
       re_app-null32.1-0 ; 
       re_app-SS_01.1-0 ; 
       re_app-SS_02.1-0 ; 
       re_app-SS_03.1-0 ; 
       re_app-SS_04.1-0 ; 
       re_app-SS_29.1-0 ; 
       re_app-SS_30.1-0 ; 
       re_app-SS_31.1-0 ; 
       re_app-SS_32.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/ss107 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Bios_Plat-re_app.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 3 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       12 3 110 ; 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       11 13 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 MPRFLG 0 ; 
       0 SCHEM 25 -2 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 -7.236394 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       14 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 30 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 35 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
