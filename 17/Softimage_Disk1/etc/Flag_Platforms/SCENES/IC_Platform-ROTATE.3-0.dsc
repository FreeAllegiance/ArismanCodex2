SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.21-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       ROTATE-mat1.1-0 ; 
       ROTATE-mat40.1-0 ; 
       ROTATE-mat41.1-0 ; 
       ROTATE-mat42.1-0 ; 
       ROTATE-mat43.1-0 ; 
       ROTATE-mat44.1-0 ; 
       ROTATE-mat47.1-0 ; 
       ROTATE-mat48.1-0 ; 
       ROTATE-mat49.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       ss05-bfuselg0.1-0 ; 
       ss05-bmfuselg.1-0 ; 
       ss05-flag_mount.1-0 ; 
       ss05-fuselg.1-0 ; 
       ss05-lndpad1.1-0 ; 
       ss05-lndpad2.1-0 ; 
       ss05-lndpad3.1-0 ; 
       ss05-lndpad4.1-0 ; 
       ss05-rantenn.1-0 ; 
       ss05-rantenn2.1-0 ; 
       ss05-ss05_2.14-0 ROOT ; 
       ss05-SS1_1.1-0 ; 
       ss05-SS1_3.1-0 ; 
       ss05-SSt1.2-0 ; 
       ss05-SSt10.1-0 ; 
       ss05-SSt11.1-0 ; 
       ss05-SSt12.1-0 ; 
       ss05-SSt13.1-0 ; 
       ss05-SSt14.1-0 ; 
       ss05-SSt15.1-0 ; 
       ss05-SSt16.1-0 ; 
       ss05-SSt17.1-0 ; 
       ss05-SSt18.1-0 ; 
       ss05-SSt19.1-0 ; 
       ss05-SSt2.3-0 ; 
       ss05-SSt20.1-0 ; 
       ss05-SSt21.1-0 ; 
       ss05-SSt22.1-0 ; 
       ss05-SSt23.1-0 ; 
       ss05-SSt24.1-0 ; 
       ss05-SSt3.2-0 ; 
       ss05-SSt4.2-0 ; 
       ss05-SSt5.1-0 ; 
       ss05-SSt6.1-0 ; 
       ss05-SSt7.1-0 ; 
       ss05-SSt8.1-0 ; 
       ss05-SSt9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/IC_FlagPlat ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Platform-ROTATE.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       ROTATE-t2d1.1-0 ; 
       ROTATE-t2d2.1-0 ; 
       ROTATE-t2d3.1-0 ; 
       ROTATE-t2d5.1-0 ; 
       ROTATE-t2d6.1-0 ; 
       ROTATE-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 1 110 ; 
       11 8 110 ; 
       13 4 110 ; 
       14 24 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 30 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 5 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 31 110 ; 
       28 31 110 ; 
       29 31 110 ; 
       30 6 110 ; 
       31 7 110 ; 
       32 13 110 ; 
       33 13 110 ; 
       34 13 110 ; 
       35 13 110 ; 
       36 13 110 ; 
       9 1 110 ; 
       12 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 6 300 ; 
       3 0 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       8 5 300 ; 
       11 1 300 ; 
       14 2 300 ; 
       15 2 300 ; 
       16 2 300 ; 
       17 2 300 ; 
       18 2 300 ; 
       19 2 300 ; 
       20 2 300 ; 
       21 2 300 ; 
       22 2 300 ; 
       23 2 300 ; 
       25 2 300 ; 
       26 2 300 ; 
       27 2 300 ; 
       28 2 300 ; 
       29 2 300 ; 
       32 2 300 ; 
       33 2 300 ; 
       34 2 300 ; 
       35 2 300 ; 
       36 2 300 ; 
       9 7 300 ; 
       12 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       7 5 401 ; 
       6 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 56.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 55 -4 0 MPRFLG 0 ; 
       10 SCHEM 31.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 -3.901442 0 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 20 -8 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 25 -8 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 MPRFLG 0 ; 
       20 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 35 -8 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 40 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 45 -8 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 50 -8 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 35 -6 0 MPRFLG 0 ; 
       31 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 5 -8 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 10 -8 0 MPRFLG 0 ; 
       35 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -7.948547 -32.31512 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -7.948547 -34.31512 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 8 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
