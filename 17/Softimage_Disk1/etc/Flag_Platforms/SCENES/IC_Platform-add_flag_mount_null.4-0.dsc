SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.12-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       add_flag_mount_null-mat1.2-0 ; 
       add_flag_mount_null-mat40.2-0 ; 
       add_flag_mount_null-mat41.2-0 ; 
       add_flag_mount_null-mat42.2-0 ; 
       add_flag_mount_null-mat43.2-0 ; 
       add_flag_mount_null-mat44.2-0 ; 
       add_flag_mount_null-mat45.2-0 ; 
       add_flag_mount_null-mat46.2-0 ; 
       add_flag_mount_null-mat47.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       ss05-bfuselg0.1-0 ; 
       ss05-bmfuselg.1-0 ; 
       ss05-flag_mount.1-0 ; 
       ss05-fuselg.1-0 ; 
       ss05-lndpad1.1-0 ; 
       ss05-lndpad2.1-0 ; 
       ss05-lndpad3.1-0 ; 
       ss05-lndpad4.1-0 ; 
       ss05-rantenn.1-0 ; 
       ss05-rantenn1.1-0 ; 
       ss05-ss05_2.11-0 ROOT ; 
       ss05-SS1_1.1-0 ; 
       ss05-SS1_2.1-0 ; 
       ss05-SSt1.2-0 ; 
       ss05-SSt10.1-0 ; 
       ss05-SSt11.1-0 ; 
       ss05-SSt12.1-0 ; 
       ss05-SSt13.1-0 ; 
       ss05-SSt14.1-0 ; 
       ss05-SSt15.1-0 ; 
       ss05-SSt16.1-0 ; 
       ss05-SSt17.1-0 ; 
       ss05-SSt18.1-0 ; 
       ss05-SSt19.1-0 ; 
       ss05-SSt2.3-0 ; 
       ss05-SSt20.1-0 ; 
       ss05-SSt21.1-0 ; 
       ss05-SSt22.1-0 ; 
       ss05-SSt23.1-0 ; 
       ss05-SSt24.1-0 ; 
       ss05-SSt3.2-0 ; 
       ss05-SSt4.2-0 ; 
       ss05-SSt5.1-0 ; 
       ss05-SSt6.1-0 ; 
       ss05-SSt7.1-0 ; 
       ss05-SSt8.1-0 ; 
       ss05-SSt9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/IC_FlagPlat ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Platform-add_flag_mount_null.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       add_flag_mount_null-t2d1.3-0 ; 
       add_flag_mount_null-t2d2.3-0 ; 
       add_flag_mount_null-t2d3.2-0 ; 
       add_flag_mount_null-t2d4.2-0 ; 
       add_flag_mount_null-t2d5.3-0 ; 
       add_flag_mount_null-t2d6.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       11 8 110 ; 
       12 9 110 ; 
       13 4 110 ; 
       14 24 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 30 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 5 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 31 110 ; 
       28 31 110 ; 
       29 31 110 ; 
       30 6 110 ; 
       31 7 110 ; 
       32 13 110 ; 
       33 13 110 ; 
       34 13 110 ; 
       35 13 110 ; 
       36 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 8 300 ; 
       3 0 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       8 5 300 ; 
       9 6 300 ; 
       11 1 300 ; 
       12 7 300 ; 
       14 2 300 ; 
       15 2 300 ; 
       16 2 300 ; 
       17 2 300 ; 
       18 2 300 ; 
       19 2 300 ; 
       20 2 300 ; 
       21 2 300 ; 
       22 2 300 ; 
       23 2 300 ; 
       25 2 300 ; 
       26 2 300 ; 
       27 2 300 ; 
       28 2 300 ; 
       29 2 300 ; 
       32 2 300 ; 
       33 2 300 ; 
       34 2 300 ; 
       35 2 300 ; 
       36 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.948547 -2 0 MPRFLG 0 ; 
       1 SCHEM 66.69855 -2 0 MPRFLG 0 ; 
       2 SCHEM 75.44855 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.69855 -2 0 MPRFLG 0 ; 
       4 SCHEM 10.44855 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.94855 -4 0 MPRFLG 0 ; 
       6 SCHEM 35.44855 -4 0 MPRFLG 0 ; 
       7 SCHEM 47.94855 -4 0 MPRFLG 0 ; 
       8 SCHEM 61.69855 -4 0 MPRFLG 0 ; 
       9 SCHEM 66.69855 -4 0 MPRFLG 0 ; 
       10 SCHEM 39.19855 0 0 SRT 1 1 1 -1.570796 0 0 0 -3.901442 0 MPRFLG 0 ; 
       11 SCHEM 60.44855 -6 0 MPRFLG 0 ; 
       12 SCHEM 65.44855 -6 0 MPRFLG 0 ; 
       13 SCHEM 10.44855 -6 0 MPRFLG 0 ; 
       14 SCHEM 17.94855 -8 0 MPRFLG 0 ; 
       15 SCHEM 20.44855 -8 0 MPRFLG 0 ; 
       16 SCHEM 22.94855 -8 0 MPRFLG 0 ; 
       17 SCHEM 25.44855 -8 0 MPRFLG 0 ; 
       18 SCHEM 27.94855 -8 0 MPRFLG 0 ; 
       19 SCHEM 30.44855 -8 0 MPRFLG 0 ; 
       20 SCHEM 32.94855 -8 0 MPRFLG 0 ; 
       21 SCHEM 35.44855 -8 0 MPRFLG 0 ; 
       22 SCHEM 37.94855 -8 0 MPRFLG 0 ; 
       23 SCHEM 40.44855 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.94855 -6 0 MPRFLG 0 ; 
       25 SCHEM 42.94855 -8 0 MPRFLG 0 ; 
       26 SCHEM 45.44855 -8 0 MPRFLG 0 ; 
       27 SCHEM 47.94855 -8 0 MPRFLG 0 ; 
       28 SCHEM 50.44855 -8 0 MPRFLG 0 ; 
       29 SCHEM 52.94855 -8 0 MPRFLG 0 ; 
       30 SCHEM 35.44855 -6 0 MPRFLG 0 ; 
       31 SCHEM 47.94855 -6 0 MPRFLG 0 ; 
       32 SCHEM 5.448547 -8 0 MPRFLG 0 ; 
       33 SCHEM 7.948547 -8 0 MPRFLG 0 ; 
       34 SCHEM 10.44855 -8 0 MPRFLG 0 ; 
       35 SCHEM 12.94855 -8 0 MPRFLG 0 ; 
       36 SCHEM 15.44855 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -32.31512 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60.44855 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.94855 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 55.44855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 57.94855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.94855 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.94855 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 65.44855 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55.44855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.94855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 62.94855 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 67.94855 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -34.31512 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 8 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
