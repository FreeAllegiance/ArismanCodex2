SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.4-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       optimize-mat1.2-0 ; 
       optimize-mat40.1-0 ; 
       optimize-mat41.1-0 ; 
       optimize-mat42.1-0 ; 
       optimize-mat43.1-0 ; 
       optimize-mat44.1-0 ; 
       optimize-mat45.1-0 ; 
       optimize-mat46.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       ss05-bfuselg0.1-0 ; 
       ss05-bfuselg1.1-0 ROOT ; 
       ss05-bfuselg2.1-0 ROOT ; 
       ss05-bmfuselg.1-0 ; 
       ss05-fuselg.1-0 ; 
       ss05-fuselg3.1-0 ROOT ; 
       ss05-fuselg4.1-0 ROOT ; 
       ss05-lndpad1.1-0 ; 
       ss05-lndpad2.1-0 ; 
       ss05-lndpad3.1-0 ; 
       ss05-lndpad4.1-0 ; 
       ss05-rantenn.1-0 ; 
       ss05-rantenn1.1-0 ; 
       ss05-ss05_2.4-0 ROOT ; 
       ss05-SS1_1.1-0 ; 
       ss05-SS1_2.1-0 ; 
       ss05-SSt1.2-0 ; 
       ss05-SSt10.1-0 ; 
       ss05-SSt11.1-0 ; 
       ss05-SSt12.1-0 ; 
       ss05-SSt13.1-0 ; 
       ss05-SSt14.1-0 ; 
       ss05-SSt15.1-0 ; 
       ss05-SSt16.1-0 ; 
       ss05-SSt17.1-0 ; 
       ss05-SSt18.1-0 ; 
       ss05-SSt19.1-0 ; 
       ss05-SSt2.3-0 ; 
       ss05-SSt20.1-0 ; 
       ss05-SSt21.1-0 ; 
       ss05-SSt22.1-0 ; 
       ss05-SSt23.1-0 ; 
       ss05-SSt24.1-0 ; 
       ss05-SSt3.2-0 ; 
       ss05-SSt4.2-0 ; 
       ss05-SSt5.1-0 ; 
       ss05-SSt6.1-0 ; 
       ss05-SSt7.1-0 ; 
       ss05-SSt8.1-0 ; 
       ss05-SSt9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/ss5 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Platform-optimize.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       optimize-t2d1.1-0 ; 
       optimize-t2d2.1-0 ; 
       optimize-t2d3.1-0 ; 
       optimize-t2d4.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 3 110 ; 
       14 11 110 ; 
       16 7 110 ; 
       17 27 110 ; 
       18 27 110 ; 
       19 27 110 ; 
       20 27 110 ; 
       21 27 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 33 110 ; 
       25 33 110 ; 
       26 33 110 ; 
       27 8 110 ; 
       28 34 110 ; 
       29 34 110 ; 
       30 34 110 ; 
       31 34 110 ; 
       32 34 110 ; 
       33 9 110 ; 
       34 10 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 16 110 ; 
       39 16 110 ; 
       12 3 110 ; 
       15 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       2 0 300 ; 
       3 0 300 ; 
       4 0 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       5 0 300 ; 
       6 0 300 ; 
       11 5 300 ; 
       14 1 300 ; 
       17 2 300 ; 
       18 2 300 ; 
       19 2 300 ; 
       20 2 300 ; 
       21 2 300 ; 
       22 2 300 ; 
       23 2 300 ; 
       24 2 300 ; 
       25 2 300 ; 
       26 2 300 ; 
       28 2 300 ; 
       29 2 300 ; 
       30 2 300 ; 
       31 2 300 ; 
       32 2 300 ; 
       35 2 300 ; 
       36 2 300 ; 
       37 2 300 ; 
       38 2 300 ; 
       39 2 300 ; 
       12 6 300 ; 
       15 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 3 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 0 -8 0 SRT 0.7600001 0.2046001 0.7600001 5.960464e-007 -1.509958e-007 -9.000051e-014 -5.517859 -0.1326304 5.490437 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 SRT 0.7600001 0.2046001 0.7600001 -5.960464e-007 -3.141593 9.000051e-014 5.517859 -0.1326304 5.490437 MPRFLG 0 ; 
       3 SCHEM 68.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 SRT 0.7600001 0.2046001 0.7600001 5.960464e-007 1.509958e-007 9.000051e-014 -5.517859 -0.1326239 -5.490434 MPRFLG 0 ; 
       6 SCHEM 7.5 -8 0 DISPLAY 1 2 SRT 0.7600001 0.2046001 0.7600001 -5.960464e-007 3.141593 -9.000051e-014 5.517859 -0.1326239 -5.490434 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 40 -8 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 66.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 36.25 -4 0 SRT 1 1 1 -1.570796 0 0 0 -3.901442 0 MPRFLG 0 ; 
       14 SCHEM 65 -10 0 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 25 -12 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       20 SCHEM 30 -12 0 MPRFLG 0 ; 
       21 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       22 SCHEM 35 -12 0 MPRFLG 0 ; 
       23 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       24 SCHEM 40 -12 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       26 SCHEM 45 -12 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 50 -12 0 MPRFLG 0 ; 
       30 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       31 SCHEM 55 -12 0 MPRFLG 0 ; 
       32 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       33 SCHEM 40 -10 0 MPRFLG 0 ; 
       34 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       35 SCHEM 10 -12 0 MPRFLG 0 ; 
       36 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 15 -12 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 20 -12 0 MPRFLG 0 ; 
       12 SCHEM 71.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 70 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.301449 -36.31512 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
