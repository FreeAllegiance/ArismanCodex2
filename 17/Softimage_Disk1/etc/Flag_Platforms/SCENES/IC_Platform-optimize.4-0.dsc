SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.5-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       optimize-mat1.3-0 ; 
       optimize-mat40.1-0 ; 
       optimize-mat41.1-0 ; 
       optimize-mat42.1-0 ; 
       optimize-mat43.1-0 ; 
       optimize-mat44.1-0 ; 
       optimize-mat45.1-0 ; 
       optimize-mat46.1-0 ; 
       optimize-mat47.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       ss05-bfuselg0.1-0 ; 
       ss05-bmfuselg.1-0 ; 
       ss05-fuselg.1-0 ; 
       ss05-lndpad1.1-0 ; 
       ss05-lndpad2.1-0 ; 
       ss05-lndpad3.1-0 ; 
       ss05-lndpad4.1-0 ; 
       ss05-rantenn.1-0 ; 
       ss05-rantenn1.1-0 ; 
       ss05-ss05_2.5-0 ROOT ; 
       ss05-SS1_1.1-0 ; 
       ss05-SS1_2.1-0 ; 
       ss05-SSt1.2-0 ; 
       ss05-SSt10.1-0 ; 
       ss05-SSt11.1-0 ; 
       ss05-SSt12.1-0 ; 
       ss05-SSt13.1-0 ; 
       ss05-SSt14.1-0 ; 
       ss05-SSt15.1-0 ; 
       ss05-SSt16.1-0 ; 
       ss05-SSt17.1-0 ; 
       ss05-SSt18.1-0 ; 
       ss05-SSt19.1-0 ; 
       ss05-SSt2.3-0 ; 
       ss05-SSt20.1-0 ; 
       ss05-SSt21.1-0 ; 
       ss05-SSt22.1-0 ; 
       ss05-SSt23.1-0 ; 
       ss05-SSt24.1-0 ; 
       ss05-SSt3.2-0 ; 
       ss05-SSt4.2-0 ; 
       ss05-SSt5.1-0 ; 
       ss05-SSt6.1-0 ; 
       ss05-SSt7.1-0 ; 
       ss05-SSt8.1-0 ; 
       ss05-SSt9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/ss5 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Platform-optimize.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       optimize-t2d1.2-0 ; 
       optimize-t2d2.2-0 ; 
       optimize-t2d3.1-0 ; 
       optimize-t2d4.1-0 ; 
       optimize-t2d5.1-0 ; 
       optimize-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 1 110 ; 
       10 7 110 ; 
       12 3 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 29 110 ; 
       19 29 110 ; 
       20 29 110 ; 
       21 29 110 ; 
       22 29 110 ; 
       23 4 110 ; 
       24 30 110 ; 
       25 30 110 ; 
       26 30 110 ; 
       27 30 110 ; 
       28 30 110 ; 
       29 5 110 ; 
       30 6 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 12 110 ; 
       34 12 110 ; 
       35 12 110 ; 
       8 1 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 8 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       7 5 300 ; 
       10 1 300 ; 
       13 2 300 ; 
       14 2 300 ; 
       15 2 300 ; 
       16 2 300 ; 
       17 2 300 ; 
       18 2 300 ; 
       19 2 300 ; 
       20 2 300 ; 
       21 2 300 ; 
       22 2 300 ; 
       24 2 300 ; 
       25 2 300 ; 
       26 2 300 ; 
       27 2 300 ; 
       28 2 300 ; 
       31 2 300 ; 
       32 2 300 ; 
       33 2 300 ; 
       34 2 300 ; 
       35 2 300 ; 
       8 6 300 ; 
       11 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       6 3 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       8 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5.448547 -2 0 MPRFLG 0 ; 
       1 SCHEM 67.94855 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 34.19855 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.94855 -4 0 MPRFLG 0 ; 
       4 SCHEM 25.44855 -4 0 MPRFLG 0 ; 
       5 SCHEM 37.94855 -4 0 MPRFLG 0 ; 
       6 SCHEM 50.44855 -4 0 MPRFLG 0 ; 
       7 SCHEM 64.19855 -4 0 MPRFLG 0 ; 
       9 SCHEM 39.19855 0 0 SRT 1 1 1 -1.570796 0 0 0 -3.901442 0 MPRFLG 0 ; 
       10 SCHEM 62.94855 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.94855 -6 0 MPRFLG 0 ; 
       13 SCHEM 20.44855 -8 0 MPRFLG 0 ; 
       14 SCHEM 22.94855 -8 0 MPRFLG 0 ; 
       15 SCHEM 25.44855 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.94855 -8 0 MPRFLG 0 ; 
       17 SCHEM 30.44855 -8 0 MPRFLG 0 ; 
       18 SCHEM 32.94855 -8 0 MPRFLG 0 ; 
       19 SCHEM 35.44855 -8 0 MPRFLG 0 ; 
       20 SCHEM 37.94855 -8 0 MPRFLG 0 ; 
       21 SCHEM 40.44855 -8 0 MPRFLG 0 ; 
       22 SCHEM 42.94855 -8 0 MPRFLG 0 ; 
       23 SCHEM 25.44855 -6 0 MPRFLG 0 ; 
       24 SCHEM 45.44855 -8 0 MPRFLG 0 ; 
       25 SCHEM 47.94855 -8 0 MPRFLG 0 ; 
       26 SCHEM 50.44855 -8 0 MPRFLG 0 ; 
       27 SCHEM 52.94855 -8 0 MPRFLG 0 ; 
       28 SCHEM 55.44855 -8 0 MPRFLG 0 ; 
       29 SCHEM 37.94855 -6 0 MPRFLG 0 ; 
       30 SCHEM 50.44855 -6 0 MPRFLG 0 ; 
       31 SCHEM 7.948547 -8 0 MPRFLG 0 ; 
       32 SCHEM 10.44855 -8 0 MPRFLG 0 ; 
       33 SCHEM 12.94855 -8 0 MPRFLG 0 ; 
       34 SCHEM 15.44855 -8 0 MPRFLG 0 ; 
       35 SCHEM 17.94855 -8 0 MPRFLG 0 ; 
       8 SCHEM 69.19855 -4 0 MPRFLG 0 ; 
       11 SCHEM 67.94855 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -32.31512 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70.44855 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 62.94855 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20.44855 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.94855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60.44855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65.44855 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.94855 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.94855 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.94855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60.44855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 65.44855 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 70.44855 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -34.31512 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 75.44855 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
