SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.1-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       forg-mat3.1-0 ; 
       forg-mat31.1-0 ; 
       forg-mat5.1-0 ; 
       forg-mat58.1-0 ; 
       forg-mat59.1-0 ; 
       forg-mat6.1-0 ; 
       forg-mat60.1-0 ; 
       forg-mat61.1-0 ; 
       forg-mat62.1-0 ; 
       forg-shaft.1-0 ; 
       forg-shaft1.1-0 ; 
       forg-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       forg-flang_mount.1-0 ; 
       forg-fuselg6.1-0 ROOT ; 
       forg-ss01.1-0 ; 
       forg-ss2.1-0 ; 
       forg-ss3.1-0 ; 
       forg-ss4.1-0 ; 
       forg-ss5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/Flag_Platforms/PICTURES/ss94 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       rix_flag-forg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       forg-t2d27.1-0 ; 
       forg-t2d28.1-0 ; 
       forg-t2d29.1-0 ; 
       forg-t2d4.1-0 ; 
       forg-t2d5.1-0 ; 
       forg-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 9 300 ; 
       1 2 300 ; 
       1 5 300 ; 
       1 11 300 ; 
       1 10 300 ; 
       1 1 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 2 401 ; 
       2 4 401 ; 
       5 5 401 ; 
       9 3 401 ; 
       10 1 401 ; 
       11 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 667.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
