SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture-cam_int1.4-0 ROOT ; 
       texture-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CLUSTERS NBELEM 2     
       model1-sphere2cls0.4-0 ; 
       texture-sphere2cls0.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       texture-mat1.1-0 ; 
       texture-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       model1-sphere2.4-0 ROOT ; 
       texture-sphere2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/etc/Flags_and_Flag_Related/Flags/PICTURES/artifact ; 
       //research/root/federation/shared_art_files/SoftImage/etc/Flags_and_Flag_Related/Flags/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       artifact-texture.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       texture-3DMarble_Bullseye1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       texture-artifact.1-0 ; 
       texture-RenderMap1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       texture-gold1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER CLUSTERS 
       0 0 700 ; 
       1 1 700 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 0 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
