SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       acs01-acs01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       gen_lifeboat_F-cam_int1.1-0 ROOT ; 
       gen_lifeboat_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       gen_lifeboat_F-light3.1-0 ROOT ; 
       gen_lifeboat_F-light4.1-0 ROOT ; 
       gen_lifeboat_F-light5.1-0 ROOT ; 
       gen_lifeboat_F-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       gen_lifeboat_F-mat1.1-0 ; 
       gen_lifeboat_F-mat10.1-0 ; 
       gen_lifeboat_F-mat12.1-0 ; 
       gen_lifeboat_F-mat14.1-0 ; 
       gen_lifeboat_F-mat15.1-0 ; 
       gen_lifeboat_F-mat16.1-0 ; 
       gen_lifeboat_F-mat17.1-0 ; 
       gen_lifeboat_F-mat18.1-0 ; 
       gen_lifeboat_F-mat19.1-0 ; 
       gen_lifeboat_F-mat20.1-0 ; 
       gen_lifeboat_F-mat21.1-0 ; 
       gen_lifeboat_F-mat22.1-0 ; 
       gen_lifeboat_F-mat23.1-0 ; 
       gen_lifeboat_F-mat24.1-0 ; 
       gen_lifeboat_F-mat25.1-0 ; 
       gen_lifeboat_F-mat26.1-0 ; 
       gen_lifeboat_F-mat27.1-0 ; 
       gen_lifeboat_F-mat3.1-0 ; 
       gen_lifeboat_F-mat4.1-0 ; 
       gen_lifeboat_F-mat7.1-0 ; 
       gen_lifeboat_F-mat8.1-0 ; 
       gen_lifeboat_F-mat9.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-1.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-10.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-2.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-3.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-4.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-5.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-6.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-7.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-8.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       acs01-acs01.1-0 ROOT ; 
       acs01-engine1.1-0 ; 
       acs01-engine2.1-0 ; 
       acs01-engine3.1-0 ; 
       acs01-fuselg.1-0 ; 
       acs01-portal1.1-0 ; 
       acs01-portal2.1-0 ; 
       acs01-protal3.1-0 ; 
       acs01-SS1.1-0 ; 
       acs01-SS2.1-0 ; 
       acs01-SS3.1-0 ; 
       acs01-SSb.1-0 ; 
       acs01-SSt1.1-0 ; 
       acs01-SSt2.1-0 ; 
       acs01-SSt3.1-0 ; 
       acs01-SSt4.1-0 ; 
       acs01-SSt5.1-0 ; 
       acs01-SSt6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/cargo_and_utility/acs01/PICTURES/acs01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs01-gen_lifeboat_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       gen_lifeboat_F-t2d10.1-0 ; 
       gen_lifeboat_F-t2d11.1-0 ; 
       gen_lifeboat_F-t2d12.1-0 ; 
       gen_lifeboat_F-t2d13.1-0 ; 
       gen_lifeboat_F-t2d14.1-0 ; 
       gen_lifeboat_F-t2d15.1-0 ; 
       gen_lifeboat_F-t2d16.1-0 ; 
       gen_lifeboat_F-t2d17.1-0 ; 
       gen_lifeboat_F-t2d18.1-0 ; 
       gen_lifeboat_F-t2d19.1-0 ; 
       gen_lifeboat_F-t2d2.1-0 ; 
       gen_lifeboat_F-t2d20.1-0 ; 
       gen_lifeboat_F-t2d21.1-0 ; 
       gen_lifeboat_F-t2d22.1-0 ; 
       gen_lifeboat_F-t2d24.1-0 ; 
       gen_lifeboat_F-t2d3.1-0 ; 
       gen_lifeboat_F-t2d5.1-0 ; 
       gen_lifeboat_F-t2d6.1-0 ; 
       gen_lifeboat_F-t2d7.1-0 ; 
       gen_lifeboat_F-t2d8.1-0 ; 
       gen_lifeboat_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 1 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 22 300 ; 
       9 25 300 ; 
       10 26 300 ; 
       11 23 300 ; 
       12 27 300 ; 
       13 28 300 ; 
       14 29 300 ; 
       15 30 300 ; 
       16 31 300 ; 
       17 24 300 ; 
       0 0 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       2 19 300 ; 
       2 21 300 ; 
       3 20 300 ; 
       3 1 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       5 4 300 ; 
       6 2 300 ; 
       6 16 300 ; 
       7 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 19 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 20 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 10 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 16 401 ; 
       21 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 4.23284 -7.710817 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 4.23284 -13.71081 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 4.23284 -11.71081 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 4.23284 -9.710814 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 14 -23.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 14 -25.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 14 -27.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7 -21.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7 -29.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 7 -31.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 7 -33.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 7 -35.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 7 -37.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 7 -39.59001 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 0 -30.59001 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10.5 -23.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 10.5 -25.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10.5 -27.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 3.5 -30.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7 -23.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7 -25.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 7 -27.59001 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -19.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -28.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -26.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -28.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 10.5 -26.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -24.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -26.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -28.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -26.84001 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -26.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10.5 -39.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.39689 -42.18092 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -28.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -30.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10.5 -38.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10.5 -40.17476 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10.5 -36.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 10.44654 -32.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 10.5 -34.59001 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -28.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -26.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -26.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -28.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -26.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -26.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -28.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -24.84001 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -19.84001 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
