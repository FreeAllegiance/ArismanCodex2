SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.1-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       tanker_F-light1.1-0 ROOT ; 
       tanker_F-light2.1-0 ROOT ; 
       tanker_F-light3.1-0 ROOT ; 
       tanker_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       tanker_F-mat9.1-0 ; 
       utl12_wtext-mat1.1-0 ; 
       utl12_wtext-mat2.1-0 ; 
       utl12_wtext-mat3.1-0 ; 
       utl12_wtext-mat4.1-0 ; 
       utl12_wtext-mat5.1-0 ; 
       utl12_wtext-mat6.1-0 ; 
       utl12_wtext-mat7.1-0 ; 
       utl12_wtext-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       utl12-doccon.1-0 ; 
       utl12-fuselg.2-0 ; 
       utl12-utl12.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/cargo_and_utility/utl12/PICTURES/utl12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl12-tanker-F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       tanker_F-t2d8.1-0 ; 
       tanker_F-t2d9.1-0 ; 
       utl12_wtext-t2d1.1-0 ; 
       utl12_wtext-t2d2.1-0 ; 
       utl12_wtext-t2d4.1-0 ; 
       utl12_wtext-t2d5.1-0 ; 
       utl12_wtext-t2d6.1-0 ; 
       utl12_wtext-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 1 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 19 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 19 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 19 -4.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 19 -6.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13 -18.5 0 MPRFLG 0 ; 
       1 SCHEM 9.5 -9.5 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 6 -9.5 0 SRT 1 1 1 0 0 0 0 0 -12.20696 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 13 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 13 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13 -2.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 13 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 13 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 13 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 13 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -0.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -16.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
