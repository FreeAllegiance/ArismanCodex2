SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utann_tug_F-cam_int1.1-0 ROOT ; 
       utann_tug_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       cargo-light7_3_1_1.1-0 ROOT ; 
       cargo-light7_3_1_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       transport_F-mat1.1-0 ; 
       transport_F-mat2.1-0 ; 
       transport_F-mat3.1-0 ; 
       transport_F-mat4.1-0 ; 
       transport_F-mat6.1-0 ; 
       transport_F-mat7.1-0 ; 
       transport_F-mat8.1-0 ; 
       transport_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       utl13-doccon.1-0 ; 
       utl13-fuselg.2-0 ; 
       utl13-utl13.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/cargo_and_utility/utl13/PICTURES/utl13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl13-transport_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       transport_F-t2d1.1-0 ; 
       transport_F-t2d2.1-0 ; 
       transport_F-t2d4.1-0 ; 
       transport_F-t2d5.1-0 ; 
       transport_F-t2d6.1-0 ; 
       transport_F-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 -8.614734e-009 -12.54515 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
