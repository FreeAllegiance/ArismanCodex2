SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl26-utl26.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.1-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pass_cont_F-light1.1-0 ROOT ; 
       pass_cont_F-light2.1-0 ROOT ; 
       pass_cont_F-light3.1-0 ROOT ; 
       pass_cont_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       pass_cont_F-mat35.1-0 ; 
       pass_cont_F-mat36.1-0 ; 
       pass_cont_F-mat37.1-0 ; 
       pass_cont_F-mat38.1-0 ; 
       pass_cont_F-mat39.1-0 ; 
       pass_cont_F-mat40.1-0 ; 
       pass_cont_F-mat41.1-0 ; 
       pass_cont_F-mat42.1-0 ; 
       pass_cont_F-mat43.1-0 ; 
       pass_cont_F-mat44.1-0 ; 
       pass_cont_F-mat45.1-0 ; 
       pass_cont_F-mat46.1-0 ; 
       pass_cont_F-mat47.1-0 ; 
       pass_cont_F-mat48.1-0 ; 
       pass_cont_F-mat49.1-0 ; 
       pass_cont_F-mat50.1-0 ; 
       pass_cont_F-mat51.1-0 ; 
       pass_cont_F-mat52.1-0 ; 
       pass_cont_F-mat53.1-0 ; 
       pass_cont_F-mat54.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       passenger_container-DECAL-box.1-0 ; 
       passenger_container-DECAL-box1.1-0 ; 
       passenger_container-DECAL-box2.1-0 ; 
       passenger_container-DECAL-box3.1-0 ; 
       passenger_container-DECAL-box4.1-0 ; 
       passenger_container-DECAL-box5.1-0 ; 
       passenger_container-STENCIL-utl26.1-0 ROOT ; 
       utl26-antenn1.1-0 ; 
       utl26-antenn2.1-0 ; 
       utl26-fuselg.1-0 ; 
       utl26-turratt.1-0 ; 
       utl26-utl26.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/cargo_and_utility/utl26/PICTURES/utl26 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl26-pass_cont_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       pass_cont_F-t2d33.1-0 ; 
       pass_cont_F-t2d34.1-0 ; 
       pass_cont_F-t2d35.1-0 ; 
       pass_cont_F-t2d36.1-0 ; 
       pass_cont_F-t2d37.1-0 ; 
       pass_cont_F-t2d38.1-0 ; 
       pass_cont_F-t2d39.1-0 ; 
       pass_cont_F-t2d40.1-0 ; 
       pass_cont_F-t2d41.1-0 ; 
       pass_cont_F-t2d42.1-0 ; 
       pass_cont_F-t2d43.1-0 ; 
       pass_cont_F-t2d44.1-0 ; 
       pass_cont_F-t2d45.1-0 ; 
       pass_cont_F-t2d46.1-0 ; 
       pass_cont_F-t2d47.1-0 ; 
       pass_cont_F-t2d48.1-0 ; 
       pass_cont_F-t2d49.1-0 ; 
       pass_cont_F-t2d50.1-0 ; 
       pass_cont_F-t2d51.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       0 0 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       0 0 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       1 6 110 ; 
       1 1 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       1 1 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       2 6 110 ; 
       2 2 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       2 2 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       3 6 110 ; 
       3 3 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       3 3 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       4 6 110 ; 
       4 4 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       4 4 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       5 6 110 ; 
       5 5 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       5 5 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       7 9 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 0 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       8 0 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 14 300 ; 
       9 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       11 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1.999152 1.999152 1.999152 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 3.75 0 0 SRT 1 1 1 0 3.141593 0 0.01007271 -1.189506 9.992488 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
