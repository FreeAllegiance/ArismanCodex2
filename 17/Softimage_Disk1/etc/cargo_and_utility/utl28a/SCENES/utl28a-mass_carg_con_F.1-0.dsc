SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28a-utl28a.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       plan_surv_vesR6-light3_2_1_1.1-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_1.1-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       mass_carg_con_F-mat110.1-0 ; 
       mass_carg_con_F-mat111.1-0 ; 
       mass_carg_con_F-mat112.1-0 ; 
       mass_carg_con_F-mat120.1-0 ; 
       mass_carg_con_F-mat124.1-0 ; 
       mass_carg_con_F-mat125.1-0 ; 
       mass_carg_con_F-mat126.1-0 ; 
       mass_carg_con_F-mat131.1-0 ; 
       mass_carg_con_F-mat135.1-0 ; 
       mass_carg_con_F-mat136.1-0 ; 
       mass_carg_con_F-mat137.1-0 ; 
       mass_carg_con_F-mat218.1-0 ; 
       mass_carg_con_F-mat219.1-0 ; 
       mass_carg_con_F-mat220.1-0 ; 
       mass_carg_con_F-mat221.1-0 ; 
       mass_carg_con_F-mat222.1-0 ; 
       mass_carg_con_F-mat66.1-0 ; 
       mass_carg_con_F-mat92.1-0 ; 
       mass_carg_con_F-nose_white-center12.1-0 ; 
       mass_carg_con_F-nose_white-center19.1-0 ; 
       mass_carg_con_F-nose_white-center21.1-0 ; 
       mass_carg_con_F-nose_white-center22.1-0 ; 
       mass_carg_con_F-nose_white-center6.1-0 ; 
       mass_carg_con_F-nose_white-center8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       utl28a-crgatt.1-0 ; 
       utl28a-ffuselg.1-0 ; 
       utl28a-fuselg1.1-0 ; 
       utl28a-fuselg2.1-0 ; 
       utl28a-fuselg3.1-0 ; 
       utl28a-SSc1.1-0 ; 
       utl28a-SSc2.1-0 ; 
       utl28a-SSc3.1-0 ; 
       utl28a-SSc4.1-0 ; 
       utl28a-SSc5.1-0 ; 
       utl28a-SSc6.1-0 ; 
       utl28a-utl28a.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/cargo_and_utility/utl28a/PICTURES/utl14a ; 
       D:/Pete_Data/Softimage/cargo_and_utility/utl28a/PICTURES/utl14b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl28a-mass_carg_con_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       mass_carg_con_F-t2d105.1-0 ; 
       mass_carg_con_F-t2d106.1-0 ; 
       mass_carg_con_F-t2d107.1-0 ; 
       mass_carg_con_F-t2d119.1-0 ; 
       mass_carg_con_F-t2d120.1-0 ; 
       mass_carg_con_F-t2d121.1-0 ; 
       mass_carg_con_F-t2d129.1-0 ; 
       mass_carg_con_F-t2d130.1-0 ; 
       mass_carg_con_F-t2d131.1-0 ; 
       mass_carg_con_F-t2d205.1-0 ; 
       mass_carg_con_F-t2d206.1-0 ; 
       mass_carg_con_F-t2d207.1-0 ; 
       mass_carg_con_F-t2d208.1-0 ; 
       mass_carg_con_F-t2d209.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       6 2 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       0 1 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 23 300 ; 
       6 18 300 ; 
       7 22 300 ; 
       8 19 300 ; 
       9 20 300 ; 
       10 21 300 ; 
       1 17 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 13 300 ; 
       3 16 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 14 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       11 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 27.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 45 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 0 -8 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 33.75 -4 0 SRT 1 1 1 0 -3.141593 0 0 -3.576279e-007 19.26218 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
