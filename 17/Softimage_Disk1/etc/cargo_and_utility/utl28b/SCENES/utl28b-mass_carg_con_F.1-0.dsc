SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl28b-utl28b.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       mass_carg_con_F-cam_int1.1-0 ROOT ; 
       mass_carg_con_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       mass_carg_con_F-inf_light1.1-0 ROOT ; 
       mass_carg_con_F-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       mass_carg_con_F-mat1.1-0 ; 
       mass_carg_con_F-mat13.1-0 ; 
       mass_carg_con_F-mat14.1-0 ; 
       mass_carg_con_F-mat15.1-0 ; 
       mass_carg_con_F-mat16.1-0 ; 
       mass_carg_con_F-mat21.1-0 ; 
       mass_carg_con_F-mat22.1-0 ; 
       mass_carg_con_F-mat23.1-0 ; 
       mass_carg_con_F-mat24.1-0 ; 
       mass_carg_con_F-mat25.1-0 ; 
       mass_carg_con_F-mat26.1-0 ; 
       mass_carg_con_F-mat27.1-0 ; 
       mass_carg_con_F-mat28.1-0 ; 
       mass_carg_con_F-mat29.1-0 ; 
       mass_carg_con_F-mat5.1-0 ; 
       mass_carg_con_F-mat6.1-0 ; 
       mass_carg_con_F-mat7.1-0 ; 
       mass_carg_con_F-mat8.1-0 ; 
       mass_carg_con_F-nose_white-center1.1-0 ; 
       mass_carg_con_F-nose_white-center2.1-0 ; 
       mass_carg_con_F-nose_white-center3.1-0 ; 
       mass_carg_con_F-nose_white-center4.1-0 ; 
       mass_carg_con_F-nose_white-center5.1-0 ; 
       mass_carg_con_F-nose_white-center6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       utl28b-crgatt.1-0 ; 
       utl28b-ffuselg.1-0 ; 
       utl28b-fuselg1.1-0 ; 
       utl28b-fuselg2.1-0 ; 
       utl28b-fuselg3.1-0 ; 
       utl28b-SSc10.1-0 ; 
       utl28b-SSc11.1-0 ; 
       utl28b-SSc12.1-0 ; 
       utl28b-SSc7.1-0 ; 
       utl28b-SSc8.1-0 ; 
       utl28b-SSc9.1-0 ; 
       utl28b-utl28b.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/cargo_and_utility/utl28b/PICTURES/utl14b ; 
       D:/Pete_Data/Softimage/cargo_and_utility/utl28b/PICTURES/utl14c ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl28b-mass_carg_con_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       mass_carg_con_F-t2d12.1-0 ; 
       mass_carg_con_F-t2d13.1-0 ; 
       mass_carg_con_F-t2d14.1-0 ; 
       mass_carg_con_F-t2d19.1-0 ; 
       mass_carg_con_F-t2d20.1-0 ; 
       mass_carg_con_F-t2d21.1-0 ; 
       mass_carg_con_F-t2d23.1-0 ; 
       mass_carg_con_F-t2d24.1-0 ; 
       mass_carg_con_F-t2d25.1-0 ; 
       mass_carg_con_F-t2d26.1-0 ; 
       mass_carg_con_F-t2d27.1-0 ; 
       mass_carg_con_F-t2d5.1-0 ; 
       mass_carg_con_F-t2d6.1-0 ; 
       mass_carg_con_F-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 3 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       0 1 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 23 300 ; 
       6 18 300 ; 
       7 19 300 ; 
       8 20 300 ; 
       9 21 300 ; 
       10 22 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       2 17 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 11 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 12 300 ; 
       4 0 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       11 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 10 0 0 SRT 6 6 6 0 -3.141593 0 0 1.192093e-007 19.29676 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 19 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
