SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       re_texture-light1.1-0 ROOT ; 
       re_texture-light2.1-0 ROOT ; 
       re_texture-light3.1-0 ROOT ; 
       re_texture-light4.1-0 ROOT ; 
       re_texture-light5.1-0 ROOT ; 
       re_texture-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       acs05_bound-bound1.1-0 ; 
       acs05_bound-bound2.2-0 ; 
       acs05_bound-bound3.1-0 ; 
       acs05_bound-bound4.1-0 ; 
       acs05_bound-bound5.1-0 ; 
       acs05_bound-root.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs05-BOUND.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
