SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       flash-cam_int1.8-0 ROOT ; 
       flash-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       flash-mat127.1-0 ; 
       flash-mat128.1-0 ; 
       flash-mat129.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       muzzle2-Back.1-0 ; 
       muzzle2-Horz.1-0 ; 
       muzzle2-root.3-0 ROOT ; 
       muzzle2-Vert.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/muzzle_flash/PICTURES/flash ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       flash-flash.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       flash-t2d4.2-0 ; 
       flash-t2d5.2-0 ; 
       flash-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       0 2 110 ; 
       3 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       0 0 300 ; 
       3 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
