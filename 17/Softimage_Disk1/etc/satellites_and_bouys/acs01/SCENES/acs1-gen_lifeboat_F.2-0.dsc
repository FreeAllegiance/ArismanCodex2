SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       gen_lifeboat_F-cam_int1.2-0 ROOT ; 
       gen_lifeboat_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       gen_lifeboat_F-mat1.1-0 ; 
       gen_lifeboat_F-mat10.1-0 ; 
       gen_lifeboat_F-mat12.1-0 ; 
       gen_lifeboat_F-mat14.1-0 ; 
       gen_lifeboat_F-mat15.1-0 ; 
       gen_lifeboat_F-mat16.1-0 ; 
       gen_lifeboat_F-mat17.1-0 ; 
       gen_lifeboat_F-mat18.1-0 ; 
       gen_lifeboat_F-mat19.1-0 ; 
       gen_lifeboat_F-mat20.1-0 ; 
       gen_lifeboat_F-mat21.1-0 ; 
       gen_lifeboat_F-mat22.1-0 ; 
       gen_lifeboat_F-mat23.1-0 ; 
       gen_lifeboat_F-mat24.1-0 ; 
       gen_lifeboat_F-mat25.1-0 ; 
       gen_lifeboat_F-mat26.1-0 ; 
       gen_lifeboat_F-mat27.1-0 ; 
       gen_lifeboat_F-mat3.1-0 ; 
       gen_lifeboat_F-mat4.1-0 ; 
       gen_lifeboat_F-mat7.1-0 ; 
       gen_lifeboat_F-mat8.1-0 ; 
       gen_lifeboat_F-mat9.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-1.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-10.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-2.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-3.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-4.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-5.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-6.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-7.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-8.1-0 ; 
       gen_lifeboat_F-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       acs01-acs01.1-0 ROOT ; 
       acs01-engine1.1-0 ; 
       acs01-engine2.1-0 ; 
       acs01-engine3.1-0 ; 
       acs01-fuselg.1-0 ; 
       acs01-portal1.1-0 ; 
       acs01-portal2.1-0 ; 
       acs01-protal3.1-0 ; 
       acs01-SS1.1-0 ; 
       acs01-SS2.1-0 ; 
       acs01-SS3.1-0 ; 
       acs01-SSb.1-0 ; 
       acs01-SSt1.1-0 ; 
       acs01-SSt2.1-0 ; 
       acs01-SSt3.1-0 ; 
       acs01-SSt4.1-0 ; 
       acs01-SSt5.1-0 ; 
       acs01-SSt6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/etc/satellites_and_bouys/acs01/PICTURES/acs01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs1-gen_lifeboat_F.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       gen_lifeboat_F-t2d10.1-0 ; 
       gen_lifeboat_F-t2d11.1-0 ; 
       gen_lifeboat_F-t2d12.1-0 ; 
       gen_lifeboat_F-t2d13.1-0 ; 
       gen_lifeboat_F-t2d14.1-0 ; 
       gen_lifeboat_F-t2d15.1-0 ; 
       gen_lifeboat_F-t2d16.1-0 ; 
       gen_lifeboat_F-t2d17.1-0 ; 
       gen_lifeboat_F-t2d18.1-0 ; 
       gen_lifeboat_F-t2d19.1-0 ; 
       gen_lifeboat_F-t2d2.1-0 ; 
       gen_lifeboat_F-t2d20.1-0 ; 
       gen_lifeboat_F-t2d21.1-0 ; 
       gen_lifeboat_F-t2d22.1-0 ; 
       gen_lifeboat_F-t2d24.1-0 ; 
       gen_lifeboat_F-t2d3.1-0 ; 
       gen_lifeboat_F-t2d5.1-0 ; 
       gen_lifeboat_F-t2d6.1-0 ; 
       gen_lifeboat_F-t2d7.1-0 ; 
       gen_lifeboat_F-t2d8.1-0 ; 
       gen_lifeboat_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 1 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       3 7 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 22 300 ; 
       9 25 300 ; 
       10 26 300 ; 
       11 23 300 ; 
       12 27 300 ; 
       13 28 300 ; 
       14 29 300 ; 
       15 30 300 ; 
       16 31 300 ; 
       17 24 300 ; 
       0 0 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       2 19 300 ; 
       2 21 300 ; 
       3 20 300 ; 
       3 1 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       5 4 300 ; 
       6 2 300 ; 
       6 16 300 ; 
       7 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 19 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 20 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 10 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 16 401 ; 
       21 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 45 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 50 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 35 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 MPRFLG 0 ; 
       4 SCHEM 40 -2 0 MPRFLG 0 ; 
       5 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 75 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
