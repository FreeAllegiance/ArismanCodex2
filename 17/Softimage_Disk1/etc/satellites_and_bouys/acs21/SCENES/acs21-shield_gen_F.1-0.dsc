SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shield_gen_F-cam_int1.1-0 ROOT ; 
       shield_gen_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       shield_gen_F-spot1.1-0 ; 
       shield_gen_F-spot1_int.1-0 ROOT ; 
       shield_gen_F-spot2.1-0 ; 
       shield_gen_F-spot2_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       shield_gen_F-mat1.1-0 ; 
       shield_gen_F-mat10.1-0 ; 
       shield_gen_F-mat2.1-0 ; 
       shield_gen_F-mat3.1-0 ; 
       shield_gen_F-mat4.1-0 ; 
       shield_gen_F-mat5.1-0 ; 
       shield_gen_F-mat6.1-0 ; 
       shield_gen_F-mat7.1-0 ; 
       shield_gen_F-mat8.1-0 ; 
       shield_gen_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       acs21-acs21.1-0 ROOT ; 
       acs21-bar1.1-0 ; 
       acs21-bar2.1-0 ; 
       acs21-doccon.1-0 ; 
       acs21-genbody.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/satellites_and_bouys/acs21/PICTURES/acs21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs21-shield_gen_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       shield_gen_F-t2d1.1-0 ; 
       shield_gen_F-t2d10.1-0 ; 
       shield_gen_F-t2d2.1-0 ; 
       shield_gen_F-t2d3.1-0 ; 
       shield_gen_F-t2d4.1-0 ; 
       shield_gen_F-t2d5.1-0 ; 
       shield_gen_F-t2d6.1-0 ; 
       shield_gen_F-t2d7.1-0 ; 
       shield_gen_F-t2d8.1-0 ; 
       shield_gen_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.26118 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.26118 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 24.76118 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 24.76118 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.13961 0 0 SRT 1 1 1 1.570796 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 USR MPRFLG 0 ; 
       2 SCHEM 8.51118 -4 0 USR MPRFLG 0 ; 
       3 SCHEM 8.532052 -6 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 5.88961 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19.63961 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14.76118 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.26118 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19.76118 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.26118 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.26118 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9.76118 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.26118 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19.63961 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14.76118 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.26118 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19.76118 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.26118 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.26118 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9.76118 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.26118 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
