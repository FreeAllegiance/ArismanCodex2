SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl04-utl04.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       gen_sat_F-cam_int1.1-0 ROOT ; 
       gen_sat_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       gen_sat_F-light3.1-0 ROOT ; 
       gen_sat_F-light4.1-0 ROOT ; 
       gen_sat_F-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       gen_sat_F-blutan1.1-0 ; 
       gen_sat_F-default1.1-0 ; 
       gen_sat_F-mat10.1-0 ; 
       gen_sat_F-mat11.1-0 ; 
       gen_sat_F-mat12.1-0 ; 
       gen_sat_F-mat13.1-0 ; 
       gen_sat_F-mat14.1-0 ; 
       gen_sat_F-mat15.1-0 ; 
       gen_sat_F-mat16.1-0 ; 
       gen_sat_F-mat17.1-0 ; 
       gen_sat_F-mat18.1-0 ; 
       gen_sat_F-mat19.1-0 ; 
       gen_sat_F-mat20.1-0 ; 
       gen_sat_F-mat21.1-0 ; 
       gen_sat_F-mat22.1-0 ; 
       gen_sat_F-mat23.1-0 ; 
       gen_sat_F-mat24.1-0 ; 
       gen_sat_F-mat25.1-0 ; 
       gen_sat_F-mat26.1-0 ; 
       gen_sat_F-mat27.1-0 ; 
       gen_sat_F-mat28.1-0 ; 
       gen_sat_F-mat4.1-0 ; 
       gen_sat_F-mat5.1-0 ; 
       gen_sat_F-mat6.1-0 ; 
       gen_sat_F-mat8.1-0 ; 
       gen_sat_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       utl04-bengine.1-0 ; 
       utl04-bfuselg.1-0 ; 
       utl04-fuselg.1-0 ; 
       utl04-slrpan.1-0 ; 
       utl04-SS1.1-0 ; 
       utl04-SS2.1-0 ; 
       utl04-SS3.1-0 ; 
       utl04-SS4.1-0 ; 
       utl04-SS5.1-0 ; 
       utl04-SS6.1-0 ; 
       utl04-tengine.1-0 ; 
       utl04-tfuselg.1-0 ; 
       utl04-utl04.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/satellites_and_bouys/utl04/PICTURES/utl04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl04-gen_sat_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       gen_sat_F-t2d10.1-0 ; 
       gen_sat_F-t2d11.1-0 ; 
       gen_sat_F-t2d12.1-0 ; 
       gen_sat_F-t2d13.1-0 ; 
       gen_sat_F-t2d14.1-0 ; 
       gen_sat_F-t2d15.1-0 ; 
       gen_sat_F-t2d16.1-0 ; 
       gen_sat_F-t2d17.1-0 ; 
       gen_sat_F-t2d18.1-0 ; 
       gen_sat_F-t2d19.1-0 ; 
       gen_sat_F-t2d20.1-0 ; 
       gen_sat_F-t2d3.1-0 ; 
       gen_sat_F-t2d4.1-0 ; 
       gen_sat_F-t2d6.1-0 ; 
       gen_sat_F-t2d7.1-0 ; 
       gen_sat_F-t2d8.1-0 ; 
       gen_sat_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       0 1 110 ; 
       1 2 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       10 11 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 15 300 ; 
       5 16 300 ; 
       6 17 300 ; 
       7 18 300 ; 
       8 19 300 ; 
       9 20 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 14 300 ; 
       1 9 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 10 300 ; 
       3 0 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 13 300 ; 
       11 8 300 ; 
       12 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       12 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 15 401 ; 
       3 16 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       22 11 401 ; 
       23 12 401 ; 
       24 13 401 ; 
       25 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 -0.07839248 0.321239 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 21.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 24 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
