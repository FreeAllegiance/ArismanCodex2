SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl05-utl05.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       Vscom_sat_F-cam_int1.1-0 ROOT ; 
       Vscom_sat_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       Vscom_sat_F-light6.1-0 ROOT ; 
       Vscom_sat_F-light7.1-0 ROOT ; 
       Vscom_sat_F-light8.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       Vscom_sat_F-blutan2.1-0 ; 
       Vscom_sat_F-default1.1-0 ; 
       Vscom_sat_F-grey2.1-0 ; 
       Vscom_sat_F-grey3.1-0 ; 
       Vscom_sat_F-mat1.1-0 ; 
       Vscom_sat_F-mat10.1-0 ; 
       Vscom_sat_F-mat12.1-0 ; 
       Vscom_sat_F-mat13.1-0 ; 
       Vscom_sat_F-mat14.1-0 ; 
       Vscom_sat_F-mat15.1-0 ; 
       Vscom_sat_F-mat16.1-0 ; 
       Vscom_sat_F-mat17.1-0 ; 
       Vscom_sat_F-mat18.1-0 ; 
       Vscom_sat_F-mat19.1-0 ; 
       Vscom_sat_F-mat2.1-0 ; 
       Vscom_sat_F-mat20.1-0 ; 
       Vscom_sat_F-mat21.1-0 ; 
       Vscom_sat_F-mat22.1-0 ; 
       Vscom_sat_F-mat23.1-0 ; 
       Vscom_sat_F-mat24.1-0 ; 
       Vscom_sat_F-mat25.1-0 ; 
       Vscom_sat_F-mat27.1-0 ; 
       Vscom_sat_F-mat28.1-0 ; 
       Vscom_sat_F-mat29.1-0 ; 
       Vscom_sat_F-mat3.1-0 ; 
       Vscom_sat_F-mat30.1-0 ; 
       Vscom_sat_F-mat31.1-0 ; 
       Vscom_sat_F-mat32.1-0 ; 
       Vscom_sat_F-mat33.1-0 ; 
       Vscom_sat_F-mat34.1-0 ; 
       Vscom_sat_F-mat35.1-0 ; 
       Vscom_sat_F-mat36.1-0 ; 
       Vscom_sat_F-mat37.1-0 ; 
       Vscom_sat_F-mat38.1-0 ; 
       Vscom_sat_F-mat39.1-0 ; 
       Vscom_sat_F-mat4.1-0 ; 
       Vscom_sat_F-mat40.1-0 ; 
       Vscom_sat_F-mat41.1-0 ; 
       Vscom_sat_F-mat42.1-0 ; 
       Vscom_sat_F-mat43.1-0 ; 
       Vscom_sat_F-mat5.1-0 ; 
       Vscom_sat_F-mat6.1-0 ; 
       Vscom_sat_F-mat7.1-0 ; 
       Vscom_sat_F-mat8.1-0 ; 
       Vscom_sat_F-mat9.1-0 ; 
       Vscom_sat_F-obj1.1-0 ; 
       Vscom_sat_F-silver5.1-0 ; 
       Vscom_sat_F-silver7.1-0 ; 
       Vscom_sat_F-white1.1-0 ; 
       Vscom_sat_F-white2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl05-afuselg1.4-0 ; 
       utl05-afuselg2.1-0 ; 
       utl05-antenn.1-0 ; 
       utl05-doccon.1-0 ; 
       utl05-engine.1-0 ; 
       utl05-ffuselg.1-0 ; 
       utl05-mfuselg1.1-0 ; 
       utl05-mfuselg2.1-0 ; 
       utl05-slrpan1.1-0 ; 
       utl05-slrpan2.1-0 ; 
       utl05-SSa1.1-0 ; 
       utl05-SSa2.1-0 ; 
       utl05-SSa3.1-0 ; 
       utl05-SSa4.1-0 ; 
       utl05-SSf1.1-0 ; 
       utl05-SSf2.1-0 ; 
       utl05-SSt.1-0 ; 
       utl05-utl05.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/satellites_and_bouys/utl05/PICTURES/utl05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl05-Vscom_sat_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       Vscom_sat_F-t2d1.1-0 ; 
       Vscom_sat_F-t2d11.1-0 ; 
       Vscom_sat_F-t2d12.1-0 ; 
       Vscom_sat_F-t2d13.1-0 ; 
       Vscom_sat_F-t2d14.1-0 ; 
       Vscom_sat_F-t2d15.1-0 ; 
       Vscom_sat_F-t2d16.1-0 ; 
       Vscom_sat_F-t2d17.1-0 ; 
       Vscom_sat_F-t2d18.1-0 ; 
       Vscom_sat_F-t2d19.1-0 ; 
       Vscom_sat_F-t2d2.1-0 ; 
       Vscom_sat_F-t2d20.1-0 ; 
       Vscom_sat_F-t2d21.1-0 ; 
       Vscom_sat_F-t2d22.1-0 ; 
       Vscom_sat_F-t2d24.1-0 ; 
       Vscom_sat_F-t2d25.1-0 ; 
       Vscom_sat_F-t2d26.1-0 ; 
       Vscom_sat_F-t2d27.1-0 ; 
       Vscom_sat_F-t2d28.1-0 ; 
       Vscom_sat_F-t2d29.1-0 ; 
       Vscom_sat_F-t2d3.1-0 ; 
       Vscom_sat_F-t2d30.1-0 ; 
       Vscom_sat_F-t2d31.1-0 ; 
       Vscom_sat_F-t2d32.1-0 ; 
       Vscom_sat_F-t2d33.1-0 ; 
       Vscom_sat_F-t2d4.1-0 ; 
       Vscom_sat_F-t2d5.1-0 ; 
       Vscom_sat_F-t2d6.1-0 ; 
       Vscom_sat_F-t2d7.1-0 ; 
       Vscom_sat_F-t2d8.1-0 ; 
       Vscom_sat_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 2 110 ; 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 5 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 17 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 33 300 ; 
       11 34 300 ; 
       12 36 300 ; 
       13 37 300 ; 
       14 38 300 ; 
       15 39 300 ; 
       16 32 300 ; 
       0 3 300 ; 
       0 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 31 300 ; 
       1 2 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 22 300 ; 
       2 46 300 ; 
       2 20 300 ; 
       2 29 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       5 48 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 21 300 ; 
       5 23 300 ; 
       6 47 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 15 300 ; 
       7 49 300 ; 
       7 4 300 ; 
       7 14 300 ; 
       7 24 300 ; 
       7 35 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 5 300 ; 
       8 0 300 ; 
       8 6 300 ; 
       9 45 300 ; 
       9 30 300 ; 
       17 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       17 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 30 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 0 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 10 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       35 20 401 ; 
       40 25 401 ; 
       41 26 401 ; 
       42 27 401 ; 
       43 28 401 ; 
       44 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 MPRFLG 0 ; 
       16 SCHEM 30 -6 0 MPRFLG 0 ; 
       0 SCHEM 25 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0.0003437735 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 31.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 2 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
