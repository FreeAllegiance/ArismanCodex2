SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 49     
       utann_heavy_fighter_land-utann_hvy_fighter_4.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_11.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_5_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_23.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_23_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_11.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_47.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_5.2-0 ; 
       utl23-utl23.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.2-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       generic_buoy_sAt-mat71.1-0 ; 
       generic_buoy_sAt-mat72.1-0 ; 
       generic_buoy_sAt-mat73.1-0 ; 
       generic_buoy_sAt-mat75.1-0 ; 
       generic_buoy_sAt-mat76.1-0 ; 
       marker_buoy_F-default7.1-0 ; 
       marker_buoy_F-mat106.1-0 ; 
       marker_buoy_F-mat107.1-0 ; 
       marker_buoy_F-mat108.1-0 ; 
       marker_buoy_F-mat109.1-0 ; 
       marker_buoy_F-mat110.1-0 ; 
       marker_buoy_F-mat111.1-0 ; 
       marker_buoy_F-mat112.1-0 ; 
       marker_buoy_F-mat113.1-0 ; 
       marker_buoy_F-mat114.1-0 ; 
       marker_buoy_F-mat115.1-0 ; 
       marker_buoy_F-mat116.1-0 ; 
       marker_buoy_F-mat117.1-0 ; 
       marker_buoy_F-mat118.1-0 ; 
       marker_buoy_F-mat119.1-0 ; 
       marker_buoy_F-mat120.1-0 ; 
       marker_buoy_F-mat121.1-0 ; 
       marker_mod-mat1_1.1-0 ; 
       marker_mod-mat2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       utl23-beacon1.1-0 ; 
       utl23-beacon2.1-0 ; 
       utl23-fuselg.1-0 ; 
       utl23-pwrpak.1-0 ; 
       utl23-SSl.1-0 ; 
       utl23-SSm1.1-0 ; 
       utl23-SSm2.1-0 ; 
       utl23-SSr.1-0 ; 
       utl23-utl23.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/satellites_and_bouys/utl23/PICTURES/utl01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl23-marker_buoy_F.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       generic_buoy_sAt-t2d22.1-0 ; 
       generic_buoy_sAt-t2d23.1-0 ; 
       generic_buoy_sAt-t2d24.1-0 ; 
       generic_buoy_sAt-t2d32.1-0 ; 
       generic_buoy_sAt-t2d33.1-0 ; 
       marker_buoy_F-t2d66.1-0 ; 
       marker_buoy_F-t2d67.1-0 ; 
       marker_buoy_F-t2d68.1-0 ; 
       marker_buoy_F-t2d69.1-0 ; 
       marker_buoy_F-t2d70.1-0 ; 
       marker_buoy_F-t2d71.1-0 ; 
       marker_buoy_F-t2d72.1-0 ; 
       marker_buoy_F-t2d73.1-0 ; 
       marker_buoy_F-t2d74.1-0 ; 
       marker_buoy_F-t2d75.1-0 ; 
       marker_buoy_F-t2d76.1-0 ; 
       marker_buoy_F-t2d77.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 0 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 3 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 4 300 ; 
       2 5 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 17 300 ; 
       4 20 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       7 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       1 4 400 ; 
       2 11 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 48 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 6.25 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 28.69619 -2 0 USR MPRFLG 0 ; 
       4 SCHEM 28.69619 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 6.25 0 0 WIRECOL 7 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 30.19619 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.69619 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 30.19619 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 166.1417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 166.8802 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 165.1302 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 158.7018 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 176.948 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 146.7018 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 164.948 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 183.3763 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 185.1263 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 157.9518 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 176.198 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 148.4518 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 166.698 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 165.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 183.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 164.1417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 164.8917 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 159.0352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 177.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 154.5352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 172.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 183.1378 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 182.3878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 163.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 181.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 153.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 168.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 186.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 164.8917 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 164.1417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 163.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 181.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 153.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 182.3878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 183.1378 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 159.0352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 177.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 154.5352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 172.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 184.3878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 163.7249 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 159.5352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 177.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 155.8686 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 174.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 181.9711 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 30.19619 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 89 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
