SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 49     
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_11.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_5_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_23.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_23_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_11.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_47.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_5.3-0 ; 
       utl23-utl23.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.3-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       generic_buoy_sAt-mat71.1-0 ; 
       generic_buoy_sAt-mat72.1-0 ; 
       generic_buoy_sAt-mat73.1-0 ; 
       generic_buoy_sAt-mat75.1-0 ; 
       generic_buoy_sAt-mat76.1-0 ; 
       marker_mod-mat1_1.1-0 ; 
       marker_mod-mat2_1.1-0 ; 
       static-default7.1-0 ; 
       static-mat106.1-0 ; 
       static-mat107.1-0 ; 
       static-mat108.1-0 ; 
       static-mat109.1-0 ; 
       static-mat110.1-0 ; 
       static-mat111.1-0 ; 
       static-mat112.1-0 ; 
       static-mat113.1-0 ; 
       static-mat114.1-0 ; 
       static-mat115.1-0 ; 
       static-mat116.1-0 ; 
       static-mat117.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       utl23-beacon1.1-0 ; 
       utl23-beacon2.1-0 ; 
       utl23-fuselg.1-0 ; 
       utl23-pwrpak.1-0 ; 
       utl23-utl23.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/satellites_and_bouys/utl23/PICTURES/utl01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl23-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       generic_buoy_sAt-t2d22.1-0 ; 
       generic_buoy_sAt-t2d23.1-0 ; 
       generic_buoy_sAt-t2d24.1-0 ; 
       generic_buoy_sAt-t2d32.1-0 ; 
       generic_buoy_sAt-t2d33.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
       static-t2d68.1-0 ; 
       static-t2d69.1-0 ; 
       static-t2d70.1-0 ; 
       static-t2d71.1-0 ; 
       static-t2d72.1-0 ; 
       static-t2d73.1-0 ; 
       static-t2d74.1-0 ; 
       static-t2d75.1-0 ; 
       static-t2d76.1-0 ; 
       static-t2d77.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 0 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 4 300 ; 
       2 7 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       1 4 400 ; 
       2 11 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 48 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 0 0 WIRECOL 7 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 163.6417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 164.3802 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 162.6302 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 156.2018 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 174.448 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 144.2018 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 162.448 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 180.8763 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 182.6263 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 155.4518 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 173.698 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 145.9518 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 164.198 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 162.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 181.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 161.6417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 162.3917 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 156.5352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 174.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 152.0352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 170.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 180.6378 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 179.8878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 160.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 179.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 151.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 169.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 165.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 184.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 162.3917 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 161.6417 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 160.7852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 179.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 151.2852 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 169.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 179.8878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 180.6378 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 156.5352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 174.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 152.0352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 170.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 181.8878 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 161.2249 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 157.0352 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 175.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 153.3686 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 171.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 179.4711 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 9 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 89 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
