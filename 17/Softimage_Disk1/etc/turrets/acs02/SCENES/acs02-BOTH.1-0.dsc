SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       BOTH-mat10.1-0 ; 
       BOTH-mat11.1-0 ; 
       BOTH-mat12.1-0 ; 
       BOTH-mat13.1-0 ; 
       BOTH-mat8.1-0 ; 
       BOTH-mat9.1-0 ; 
       rixturret_sm_F-mat1.3-0 ; 
       rixturret_sm_F-mat1_1.1-0 ; 
       rixturret_sm_F-mat2.3-0 ; 
       rixturret_sm_F-mat2_1.1-0 ; 
       rixturret_sm_F-mat3.3-0 ; 
       rixturret_sm_F-mat3_1.1-0 ; 
       rixturret_sm_F-mat4.3-0 ; 
       rixturret_sm_F-mat4_1.1-0 ; 
       rixturret_sm_F-mat5.3-0 ; 
       rixturret_sm_F-mat5_1.1-0 ; 
       rixturret_sm_F-mat6.3-0 ; 
       rixturret_sm_F-mat6_1.1-0 ; 
       rixturret_sm_F-mat7.3-0 ; 
       rixturret_sm_F-mat7_1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       acs02-acs02.6-0 ROOT ; 
       acs02-acs02_1.1-0 ROOT ; 
       acs02-bfusegl3.1-0 ; 
       acs02-bfusegl3_1.1-0 ; 
       acs02-bfuselg1.1-0 ; 
       acs02-bfuselg1_1.1-0 ; 
       acs02-bfuselg2.1-0 ; 
       acs02-bfuselg2_1.1-0 ; 
       acs02-bfuselg4.1-0 ; 
       acs02-bfuselg4_1.1-0 ; 
       acs02-fuselg.1-0 ; 
       acs02-fuselg_1.1-0 ; 
       acs02-fuselg_1_1.1-0 ; 
       acs02-fuselg_3.1-0 ; 
       acs02-lwepbas.1-0 ; 
       acs02-lwepbas_1.1-0 ; 
       acs02-lwepmnt.1-0 ; 
       acs02-lwepmnt_1.1-0 ; 
       acs02-missemt1.1-0 ; 
       acs02-missemt2.1-0 ; 
       acs02-rwepbas.1-0 ; 
       acs02-rwepbas_1.1-0 ; 
       acs02-rwepmnt.1-0 ; 
       acs02-rwepmnt_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/etc/turrets/acs02/PICTURES/acs02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs02-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       BOTH-t2d10.1-0 ; 
       BOTH-t2d11.1-0 ; 
       BOTH-t2d12.1-0 ; 
       BOTH-t2d7.1-0 ; 
       BOTH-t2d8.1-0 ; 
       BOTH-t2d9.1-0 ; 
       rixturret_sm_F-t2d1.3-0 ; 
       rixturret_sm_F-t2d1_1.1-0 ; 
       rixturret_sm_F-t2d2.3-0 ; 
       rixturret_sm_F-t2d2_1.1-0 ; 
       rixturret_sm_F-t2d3.3-0 ; 
       rixturret_sm_F-t2d3_1.1-0 ; 
       rixturret_sm_F-t2d4.3-0 ; 
       rixturret_sm_F-t2d4_1.1-0 ; 
       rixturret_sm_F-t2d5.3-0 ; 
       rixturret_sm_F-t2d5_1.1-0 ; 
       rixturret_sm_F-t2d6.3-0 ; 
       rixturret_sm_F-t2d6_1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       4 10 110 ; 
       6 10 110 ; 
       8 10 110 ; 
       10 11 110 ; 
       11 0 110 ; 
       14 11 110 ; 
       16 14 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 11 110 ; 
       22 20 110 ; 
       3 7 110 ; 
       5 13 110 ; 
       7 13 110 ; 
       9 13 110 ; 
       13 12 110 ; 
       12 1 110 ; 
       15 12 110 ; 
       17 15 110 ; 
       21 12 110 ; 
       23 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       2 3 300 ; 
       4 1 300 ; 
       6 2 300 ; 
       8 0 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       11 6 300 ; 
       11 8 300 ; 
       14 6 300 ; 
       14 12 300 ; 
       14 14 300 ; 
       14 16 300 ; 
       20 6 300 ; 
       20 10 300 ; 
       20 18 300 ; 
       1 7 300 ; 
       3 23 300 ; 
       5 21 300 ; 
       7 22 300 ; 
       9 20 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       12 7 300 ; 
       12 9 300 ; 
       15 7 300 ; 
       15 13 300 ; 
       15 15 300 ; 
       15 17 300 ; 
       21 7 300 ; 
       21 11 300 ; 
       21 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       8 6 401 ; 
       10 8 401 ; 
       12 10 401 ; 
       14 12 401 ; 
       16 14 401 ; 
       18 16 401 ; 
       9 7 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       15 13 401 ; 
       17 15 401 ; 
       19 17 401 ; 
       20 23 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 MPRFLG 0 ; 
       14 SCHEM 5 -4 0 MPRFLG 0 ; 
       16 SCHEM 5 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 32 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 34.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 32 -6 0 MPRFLG 0 ; 
       7 SCHEM 34.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 37 -6 0 MPRFLG 0 ; 
       13 SCHEM 34.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 32 -2 0 MPRFLG 0 ; 
       15 SCHEM 27 -4 0 MPRFLG 0 ; 
       17 SCHEM 27 -6 0 MPRFLG 0 ; 
       21 SCHEM 29.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 29.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 38.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 28.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 28.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 28.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 33.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 38.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 38.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 28.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 28.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 28.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 33.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 38.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 38.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
