SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.6-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       rixturret_sm_F-mat1.3-0 ; 
       rixturret_sm_F-mat2.3-0 ; 
       rixturret_sm_F-mat3.3-0 ; 
       rixturret_sm_F-mat4.3-0 ; 
       rixturret_sm_F-mat5.3-0 ; 
       rixturret_sm_F-mat6.3-0 ; 
       rixturret_sm_F-mat7.3-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       acs02-acs02.5-0 ROOT ; 
       acs02-bfusegl3.1-0 ; 
       acs02-bfuselg1.1-0 ; 
       acs02-bfuselg2.1-0 ; 
       acs02-bfuselg4.1-0 ; 
       acs02-fuselg.1-0 ; 
       acs02-fuselg_1.1-0 ; 
       acs02-lwepbas.1-0 ; 
       acs02-lwepmnt.1-0 ; 
       acs02-rwepbas.1-0 ; 
       acs02-rwepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/acs02/PICTURES/acs02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs02-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       rixturret_sm_F-t2d1.3-0 ; 
       rixturret_sm_F-t2d2.3-0 ; 
       rixturret_sm_F-t2d3.3-0 ; 
       rixturret_sm_F-t2d4.3-0 ; 
       rixturret_sm_F-t2d5.3-0 ; 
       rixturret_sm_F-t2d6.3-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 6 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 10 300 ; 
       2 8 300 ; 
       3 9 300 ; 
       4 7 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       0 0 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       7 0 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       9 0 300 ; 
       9 2 300 ; 
       9 6 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       7 11 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
