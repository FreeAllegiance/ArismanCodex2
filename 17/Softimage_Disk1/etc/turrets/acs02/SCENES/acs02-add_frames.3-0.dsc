SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.5-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       add_frames-mat10.1-0 ; 
       add_frames-mat11.1-0 ; 
       add_frames-mat12.1-0 ; 
       add_frames-mat13.1-0 ; 
       add_frames-mat8.1-0 ; 
       add_frames-mat9.1-0 ; 
       rixturret_sm_F-mat1.3-0 ; 
       rixturret_sm_F-mat2.3-0 ; 
       rixturret_sm_F-mat3.3-0 ; 
       rixturret_sm_F-mat4.3-0 ; 
       rixturret_sm_F-mat5.3-0 ; 
       rixturret_sm_F-mat6.3-0 ; 
       rixturret_sm_F-mat7.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       acs02-acs02.4-0 ROOT ; 
       acs02-bfusegl3.1-0 ; 
       acs02-bfuselg1.1-0 ; 
       acs02-bfuselg2.1-0 ; 
       acs02-bfuselg4.1-0 ; 
       acs02-fuselg.1-0 ; 
       acs02-fuselg_1.1-0 ; 
       acs02-lwepbas.1-0 ; 
       acs02-lwepmnt.1-0 ; 
       acs02-missemt1.1-0 ; 
       acs02-missemt2.1-0 ; 
       acs02-rwepbas.1-0 ; 
       acs02-rwepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/acs02/PICTURES/acs02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs02-add_frames.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       add_frames-t2d10.1-0 ; 
       add_frames-t2d11.1-0 ; 
       add_frames-t2d12.1-0 ; 
       add_frames-t2d7.1-0 ; 
       add_frames-t2d8.1-0 ; 
       add_frames-t2d9.1-0 ; 
       rixturret_sm_F-t2d1.3-0 ; 
       rixturret_sm_F-t2d2.3-0 ; 
       rixturret_sm_F-t2d3.3-0 ; 
       rixturret_sm_F-t2d4.3-0 ; 
       rixturret_sm_F-t2d5.3-0 ; 
       rixturret_sm_F-t2d6.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       9 0 110 ; 
       5 6 110 ; 
       10 0 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       11 6 110 ; 
       12 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 1 300 ; 
       3 2 300 ; 
       4 0 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       0 6 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 6 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       11 6 300 ; 
       11 8 300 ; 
       11 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       0 5 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
