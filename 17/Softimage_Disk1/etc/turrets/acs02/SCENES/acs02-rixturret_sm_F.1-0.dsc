SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rixturret_sm_F-cam_int1.1-0 ROOT ; 
       rixturret_sm_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_1.1-0 ROOT ; 
       destroy-lite3_4_1.1-0 ROOT ; 
       ion_cannon_sPA-light1_1.1-0 ROOT ; 
       ion_cannon_sPA-lite1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       rixturret_sm_F-mat1.1-0 ; 
       rixturret_sm_F-mat2.1-0 ; 
       rixturret_sm_F-mat3.1-0 ; 
       rixturret_sm_F-mat4.1-0 ; 
       rixturret_sm_F-mat5.1-0 ; 
       rixturret_sm_F-mat6.1-0 ; 
       rixturret_sm_F-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       acs02-acs02.1-0 ROOT ; 
       acs02-fuselg.1-0 ; 
       acs02-lwepbas.1-0 ; 
       acs02-lwepmnt.1-0 ; 
       acs02-rwepbas.1-0 ; 
       acs02-rwepmnt.1-0 ; 
       rixturret_sm_F-DECAL-lgunbase.1-0 ; 
       rixturret_sm_F-DECAL-lgunbase1.1-0 ; 
       rixturret_sm_F-DECAL-lgunbase2.1-0 ; 
       rixturret_sm_F-DECAL-rgunbase.1-0 ; 
       rixturret_sm_F-DECAL-rgunbase2.1-0 ; 
       rixturret_sm_F-DECAL-sphere2.1-0 ; 
       rixturret_sm_F-STENCIL-acs02.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/turrets/acs02/PICTURES/acs02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs02-rixturret_sm_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       rixturret_sm_F-t2d1.1-0 ; 
       rixturret_sm_F-t2d2.1-0 ; 
       rixturret_sm_F-t2d3.1-0 ; 
       rixturret_sm_F-t2d4.1-0 ; 
       rixturret_sm_F-t2d5.1-0 ; 
       rixturret_sm_F-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 4 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 12 110 ; 
       10 12 110 ; 
       10 10 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       10 10 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       11 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       4 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 10 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 USR MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 USR MPRFLG 0 ; 
       6 SCHEM 12.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 10 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 0 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 6.25 -12 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 214 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
