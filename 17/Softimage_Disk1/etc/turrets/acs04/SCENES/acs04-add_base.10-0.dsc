SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.11-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2.11-0 ROOT ; 
       destroy-lite3_4_1_2.11-0 ROOT ; 
       ion_cannon_sPA-light1_1_2.11-0 ROOT ; 
       ion_cannon_sPA-lite1_1_2.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       add_base-mat25.3-0 ; 
       add_base-mat26.1-0 ; 
       add_base-mat27.1-0 ; 
       add_base-mat28.1-0 ; 
       smfedtur_sPTLN-mat1.2-0 ; 
       smfedtur_sPTLN-mat10.2-0 ; 
       smfedtur_sPTLN-mat11.2-0 ; 
       smfedtur_sPTLN-mat17.2-0 ; 
       smfedtur_sPTLN-mat18.2-0 ; 
       smfedtur_sPTLN-mat19.2-0 ; 
       smfedtur_sPTLN-mat2.2-0 ; 
       smfedtur_sPTLN-mat20.2-0 ; 
       smfedtur_sPTLN-mat21.2-0 ; 
       smfedtur_sPTLN-mat22.2-0 ; 
       smfedtur_sPTLN-mat23.2-0 ; 
       smfedtur_sPTLN-mat24.2-0 ; 
       smfedtur_sPTLN-mat3.2-0 ; 
       smfedtur_sPTLN-mat4.2-0 ; 
       smfedtur_sPTLN-mat5.2-0 ; 
       smfedtur_sPTLN-mat6.2-0 ; 
       smfedtur_sPTLN-mat7.2-0 ; 
       smfedtur_sPTLN-mat8.2-0 ; 
       smfedtur_sPTLN-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       acs04-acs04.6-0 ROOT ; 
       acs04-cyl1.1-0 ; 
       acs04-lwepbas.1-0 ; 
       acs04-lwepmnt.1-0 ; 
       acs04-rwepbas.1-0 ; 
       acs04-rwepmnt.1-0 ; 
       acs04-tetra1.1-0 ; 
       acs04-turret.1-0 ; 
       add_base-cube1.3-0 ROOT ; 
       add_base-face2.1-0 ; 
       add_base-null1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs04/PICTURES/acs04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs04-add_base.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       add_base-t2d17.1-0 ; 
       add_base-t2d18.1-0 ; 
       add_base-t2d19.2-0 ; 
       smfedtur_sPTLN-t2d1.2-0 ; 
       smfedtur_sPTLN-t2d10.2-0 ; 
       smfedtur_sPTLN-t2d11.2-0 ; 
       smfedtur_sPTLN-t2d12.2-0 ; 
       smfedtur_sPTLN-t2d13.2-0 ; 
       smfedtur_sPTLN-t2d14.2-0 ; 
       smfedtur_sPTLN-t2d15.2-0 ; 
       smfedtur_sPTLN-t2d16.2-0 ; 
       smfedtur_sPTLN-t2d2.2-0 ; 
       smfedtur_sPTLN-t2d3.2-0 ; 
       smfedtur_sPTLN-t2d4.2-0 ; 
       smfedtur_sPTLN-t2d5.2-0 ; 
       smfedtur_sPTLN-t2d6.2-0 ; 
       smfedtur_sPTLN-t2d7.2-0 ; 
       smfedtur_sPTLN-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 7 110 ; 
       6 1 110 ; 
       9 10 110 ; 
       2 7 110 ; 
       3 2 110 ; 
       4 7 110 ; 
       5 4 110 ; 
       7 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 3 300 ; 
       6 1 300 ; 
       9 2 300 ; 
       0 4 300 ; 
       2 16 300 ; 
       2 20 300 ; 
       2 22 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 15 300 ; 
       4 10 300 ; 
       4 19 300 ; 
       4 21 300 ; 
       4 5 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       7 17 300 ; 
       7 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       18 3 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       0 0 401 ; 
       2 2 401 ; 
       3 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 0 0 MPRFLG 0 ; 
       8 SCHEM 50 0 0 DISPLAY 1 2 SRT 0.1940001 0.1940001 0.0388 0 0 0 8.272757 -0.7240803 0 MPRFLG 0 ; 
       9 SCHEM 30 0 0 MPRFLG 0 ; 
       10 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 SRT 19.3865 19.3865 19.3865 0 0 0 0.003738761 -0.005320858 -8.098781e-006 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 40 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
