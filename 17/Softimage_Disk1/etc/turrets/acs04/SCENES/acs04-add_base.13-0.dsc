SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.14-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2.14-0 ROOT ; 
       destroy-lite3_4_1_2.14-0 ROOT ; 
       ion_cannon_sPA-light1_1_2.14-0 ROOT ; 
       ion_cannon_sPA-lite1_1_2.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       add_base-mat25.3-0 ; 
       add_base-mat26.2-0 ; 
       add_base-mat27.1-0 ; 
       add_base-mat28.1-0 ; 
       add_base-mat29.1-0 ; 
       add_base-mat30.1-0 ; 
       add_base-mat31.1-0 ; 
       smfedtur_sPTLN-mat1.2-0 ; 
       smfedtur_sPTLN-mat10.2-0 ; 
       smfedtur_sPTLN-mat11.2-0 ; 
       smfedtur_sPTLN-mat17.2-0 ; 
       smfedtur_sPTLN-mat18.2-0 ; 
       smfedtur_sPTLN-mat19.2-0 ; 
       smfedtur_sPTLN-mat2.2-0 ; 
       smfedtur_sPTLN-mat20.2-0 ; 
       smfedtur_sPTLN-mat21.2-0 ; 
       smfedtur_sPTLN-mat22.2-0 ; 
       smfedtur_sPTLN-mat23.2-0 ; 
       smfedtur_sPTLN-mat24.2-0 ; 
       smfedtur_sPTLN-mat3.2-0 ; 
       smfedtur_sPTLN-mat4.2-0 ; 
       smfedtur_sPTLN-mat5.2-0 ; 
       smfedtur_sPTLN-mat6.2-0 ; 
       smfedtur_sPTLN-mat7.2-0 ; 
       smfedtur_sPTLN-mat8.2-0 ; 
       smfedtur_sPTLN-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       acs04-acs04.8-0 ROOT ; 
       acs04-cube1.4-0 ; 
       acs04-cube2.1-0 ; 
       acs04-cyl1.1-0 ; 
       acs04-face2.1-0 ; 
       acs04-face3.1-0 ; 
       acs04-lwepbas.1-0 ; 
       acs04-lwepmnt.1-0 ; 
       acs04-null1.6-0 ; 
       acs04-null2.1-0 ; 
       acs04-rwepbas.1-0 ; 
       acs04-rwepmnt.1-0 ; 
       acs04-tetra1.1-0 ; 
       acs04-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs04/PICTURES/acs04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs04-add_base.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       add_base-t2d17.1-0 ; 
       add_base-t2d18.1-0 ; 
       add_base-t2d19.2-0 ; 
       add_base-t2d20.2-0 ; 
       add_base-t2d21.1-0 ; 
       add_base-t2d22.1-0 ; 
       add_base-t2d23.1-0 ; 
       smfedtur_sPTLN-t2d1.2-0 ; 
       smfedtur_sPTLN-t2d10.2-0 ; 
       smfedtur_sPTLN-t2d11.2-0 ; 
       smfedtur_sPTLN-t2d12.2-0 ; 
       smfedtur_sPTLN-t2d13.2-0 ; 
       smfedtur_sPTLN-t2d14.2-0 ; 
       smfedtur_sPTLN-t2d15.2-0 ; 
       smfedtur_sPTLN-t2d16.2-0 ; 
       smfedtur_sPTLN-t2d2.2-0 ; 
       smfedtur_sPTLN-t2d3.2-0 ; 
       smfedtur_sPTLN-t2d4.2-0 ; 
       smfedtur_sPTLN-t2d5.2-0 ; 
       smfedtur_sPTLN-t2d6.2-0 ; 
       smfedtur_sPTLN-t2d7.2-0 ; 
       smfedtur_sPTLN-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 13 110 ; 
       12 3 110 ; 
       1 3 110 ; 
       4 8 110 ; 
       8 1 110 ; 
       2 3 110 ; 
       6 13 110 ; 
       7 6 110 ; 
       10 13 110 ; 
       11 10 110 ; 
       13 0 110 ; 
       9 2 110 ; 
       5 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       3 3 300 ; 
       12 1 300 ; 
       1 4 300 ; 
       4 2 300 ; 
       2 5 300 ; 
       0 7 300 ; 
       6 19 300 ; 
       6 23 300 ; 
       6 25 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 18 300 ; 
       10 13 300 ; 
       10 22 300 ; 
       10 24 300 ; 
       10 8 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       5 6 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 19 401 ; 
       9 20 401 ; 
       10 21 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       21 7 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       0 0 401 ; 
       1 6 401 ; 
       2 2 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 SRT 19.3865 19.3865 19.3865 0 0 0 0.003738761 -0.005320858 -8.098781e-006 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 0 -6 0 MPRFLG 0 ; 
       11 SCHEM 0 -8 0 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -10 0 MPRFLG 0 ; 
       5 SCHEM 10 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
