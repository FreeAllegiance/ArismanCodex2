SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.27-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.27-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2.27-0 ROOT ; 
       destroy-lite3_4_1_2.27-0 ROOT ; 
       ion_cannon_sPA-light1_1_2.27-0 ROOT ; 
       ion_cannon_sPA-lite1_1_2.27-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       add_guns-mat25.1-0 ; 
       add_guns-mat26.1-0 ; 
       add_guns-mat28.1-0 ; 
       add_guns-mat29.1-0 ; 
       add_guns-mat30.1-0 ; 
       add_guns-mat31.1-0 ; 
       add_guns-mat32.1-0 ; 
       add_guns-mat33.1-0 ; 
       add_guns-mat34.1-0 ; 
       smfedtur_sPTLN-mat1.2-0 ; 
       smfedtur_sPTLN-mat10.2-0 ; 
       smfedtur_sPTLN-mat11.2-0 ; 
       smfedtur_sPTLN-mat17.2-0 ; 
       smfedtur_sPTLN-mat18.2-0 ; 
       smfedtur_sPTLN-mat19.2-0 ; 
       smfedtur_sPTLN-mat2.2-0 ; 
       smfedtur_sPTLN-mat20.2-0 ; 
       smfedtur_sPTLN-mat21.2-0 ; 
       smfedtur_sPTLN-mat22.2-0 ; 
       smfedtur_sPTLN-mat23.2-0 ; 
       smfedtur_sPTLN-mat24.2-0 ; 
       smfedtur_sPTLN-mat3.2-0 ; 
       smfedtur_sPTLN-mat4.2-0 ; 
       smfedtur_sPTLN-mat5.2-0 ; 
       smfedtur_sPTLN-mat6.2-0 ; 
       smfedtur_sPTLN-mat7.2-0 ; 
       smfedtur_sPTLN-mat8.2-0 ; 
       smfedtur_sPTLN-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       acs04-acs04.16-0 ROOT ; 
       acs04-cube1.4-0 ; 
       acs04-cube2.1-0 ; 
       acs04-cube3.1-0 ; 
       acs04-cube4.1-0 ; 
       acs04-cyl1.1-0 ; 
       acs04-cyl2.2-0 ; 
       acs04-lwepbas.1-0 ; 
       acs04-lwepemt.1-0 ; 
       acs04-rwepbas.1-0 ; 
       acs04-rwepemt.1-0 ; 
       acs04-tetra1.1-0 ; 
       acs04-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs04/PICTURES/acs04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs04-add_guns.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       add_guns-t2d17.1-0 ; 
       add_guns-t2d18.1-0 ; 
       add_guns-t2d20.1-0 ; 
       add_guns-t2d21.1-0 ; 
       add_guns-t2d23.1-0 ; 
       add_guns-t2d24.1-0 ; 
       add_guns-t2d25.1-0 ; 
       add_guns-t2d26.1-0 ; 
       smfedtur_sPTLN-t2d1.3-0 ; 
       smfedtur_sPTLN-t2d10.3-0 ; 
       smfedtur_sPTLN-t2d11.3-0 ; 
       smfedtur_sPTLN-t2d12.3-0 ; 
       smfedtur_sPTLN-t2d13.3-0 ; 
       smfedtur_sPTLN-t2d14.3-0 ; 
       smfedtur_sPTLN-t2d15.3-0 ; 
       smfedtur_sPTLN-t2d16.3-0 ; 
       smfedtur_sPTLN-t2d2.3-0 ; 
       smfedtur_sPTLN-t2d3.3-0 ; 
       smfedtur_sPTLN-t2d4.3-0 ; 
       smfedtur_sPTLN-t2d5.3-0 ; 
       smfedtur_sPTLN-t2d6.3-0 ; 
       smfedtur_sPTLN-t2d7.3-0 ; 
       smfedtur_sPTLN-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 9 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 12 110 ; 
       7 12 110 ; 
       8 7 110 ; 
       9 12 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 8 300 ; 
       0 9 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       5 7 300 ; 
       7 21 300 ; 
       7 25 300 ; 
       7 27 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 20 300 ; 
       9 15 300 ; 
       9 24 300 ; 
       9 26 300 ; 
       9 10 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       11 1 300 ; 
       12 22 300 ; 
       12 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 4 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 22 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       23 8 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 0 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 SRT 0.75 0.75 0.75 0 0 0 0.003738761 -0.005320858 -8.098781e-006 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 10 -10 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
