SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.20-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2.20-0 ROOT ; 
       destroy-lite3_4_1_2.20-0 ROOT ; 
       ion_cannon_sPA-light1_1_2.20-0 ROOT ; 
       ion_cannon_sPA-lite1_1_2.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       final-mat25.2-0 ; 
       final-mat26.1-0 ; 
       final-mat28.2-0 ; 
       final-mat29.1-0 ; 
       final-mat30.1-0 ; 
       final-mat31.2-0 ; 
       final-mat32.1-0 ; 
       final-mat33.1-0 ; 
       smfedtur_sPTLN-mat1.2-0 ; 
       smfedtur_sPTLN-mat10.2-0 ; 
       smfedtur_sPTLN-mat11.2-0 ; 
       smfedtur_sPTLN-mat17.2-0 ; 
       smfedtur_sPTLN-mat18.2-0 ; 
       smfedtur_sPTLN-mat19.2-0 ; 
       smfedtur_sPTLN-mat2.2-0 ; 
       smfedtur_sPTLN-mat20.2-0 ; 
       smfedtur_sPTLN-mat21.2-0 ; 
       smfedtur_sPTLN-mat22.2-0 ; 
       smfedtur_sPTLN-mat23.2-0 ; 
       smfedtur_sPTLN-mat24.2-0 ; 
       smfedtur_sPTLN-mat3.2-0 ; 
       smfedtur_sPTLN-mat4.2-0 ; 
       smfedtur_sPTLN-mat5.2-0 ; 
       smfedtur_sPTLN-mat6.2-0 ; 
       smfedtur_sPTLN-mat7.2-0 ; 
       smfedtur_sPTLN-mat8.2-0 ; 
       smfedtur_sPTLN-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       acs04-acs04.12-0 ROOT ; 
       acs04-cube1.4-0 ; 
       acs04-cube2.1-0 ; 
       acs04-cube3.1-0 ; 
       acs04-cube4.1-0 ; 
       acs04-cyl1.1-0 ; 
       acs04-lwepbas.1-0 ; 
       acs04-lwepemt.1-0 ; 
       acs04-rwepbas.1-0 ; 
       acs04-rwepemt.1-0 ; 
       acs04-tetra1.1-0 ; 
       acs04-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs04/PICTURES/acs04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs04-final.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       final-t2d17.2-0 ; 
       final-t2d18.2-0 ; 
       final-t2d20.1-0 ; 
       final-t2d21.1-0 ; 
       final-t2d23.1-0 ; 
       final-t2d24.1-0 ; 
       final-t2d25.1-0 ; 
       final-t2d26.1-0 ; 
       smfedtur_sPTLN-t2d1.2-0 ; 
       smfedtur_sPTLN-t2d10.2-0 ; 
       smfedtur_sPTLN-t2d11.2-0 ; 
       smfedtur_sPTLN-t2d12.2-0 ; 
       smfedtur_sPTLN-t2d13.2-0 ; 
       smfedtur_sPTLN-t2d14.2-0 ; 
       smfedtur_sPTLN-t2d15.2-0 ; 
       smfedtur_sPTLN-t2d16.2-0 ; 
       smfedtur_sPTLN-t2d2.2-0 ; 
       smfedtur_sPTLN-t2d3.2-0 ; 
       smfedtur_sPTLN-t2d4.2-0 ; 
       smfedtur_sPTLN-t2d5.2-0 ; 
       smfedtur_sPTLN-t2d6.2-0 ; 
       smfedtur_sPTLN-t2d7.2-0 ; 
       smfedtur_sPTLN-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 11 110 ; 
       10 5 110 ; 
       1 5 110 ; 
       4 2 110 ; 
       2 5 110 ; 
       6 11 110 ; 
       3 1 110 ; 
       8 11 110 ; 
       7 6 110 ; 
       11 0 110 ; 
       9 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       5 2 300 ; 
       5 7 300 ; 
       10 1 300 ; 
       1 3 300 ; 
       4 6 300 ; 
       2 4 300 ; 
       0 8 300 ; 
       6 20 300 ; 
       6 24 300 ; 
       6 26 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 19 300 ; 
       3 5 300 ; 
       8 14 300 ; 
       8 23 300 ; 
       8 25 300 ; 
       8 9 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       11 21 300 ; 
       11 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 20 401 ; 
       10 21 401 ; 
       11 22 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       22 8 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       0 0 401 ; 
       1 4 401 ; 
       5 5 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       6 6 401 ; 
       7 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       0 SCHEM 10 0 0 SRT 19.3865 19.3865 19.3865 0 0 0 0.003738761 -0.005320858 -8.098781e-006 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
