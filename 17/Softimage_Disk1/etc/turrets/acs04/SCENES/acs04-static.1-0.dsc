SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.32-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.32-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       smfedtur_sPTLN-mat1.2-0 ; 
       smfedtur_sPTLN-mat10.2-0 ; 
       smfedtur_sPTLN-mat11.2-0 ; 
       smfedtur_sPTLN-mat17.2-0 ; 
       smfedtur_sPTLN-mat18.2-0 ; 
       smfedtur_sPTLN-mat19.2-0 ; 
       smfedtur_sPTLN-mat2.2-0 ; 
       smfedtur_sPTLN-mat20.2-0 ; 
       smfedtur_sPTLN-mat21.2-0 ; 
       smfedtur_sPTLN-mat22.2-0 ; 
       smfedtur_sPTLN-mat23.2-0 ; 
       smfedtur_sPTLN-mat24.2-0 ; 
       smfedtur_sPTLN-mat3.2-0 ; 
       smfedtur_sPTLN-mat4.2-0 ; 
       smfedtur_sPTLN-mat5.2-0 ; 
       smfedtur_sPTLN-mat6.2-0 ; 
       smfedtur_sPTLN-mat7.2-0 ; 
       smfedtur_sPTLN-mat8.2-0 ; 
       smfedtur_sPTLN-mat9.2-0 ; 
       static-mat25.1-0 ; 
       static-mat28.1-0 ; 
       static-mat29.1-0 ; 
       static-mat30.1-0 ; 
       static-mat31.1-0 ; 
       static-mat32.1-0 ; 
       static-mat33.1-0 ; 
       static-mat34.1-0 ; 
       static-mat35.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       acs04-acs04.20-0 ROOT ; 
       acs04-cube1.4-0 ; 
       acs04-cube2.1-0 ; 
       acs04-cube3.1-0 ; 
       acs04-cube4.1-0 ; 
       acs04-cyl1.1-0 ; 
       acs04-cyl2.2-0 ; 
       acs04-cyl3.1-0 ; 
       acs04-lwepbas.1-0 ; 
       acs04-rwepbas.1-0 ; 
       acs04-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs04/PICTURES/acs04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs04-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       smfedtur_sPTLN-t2d1.3-0 ; 
       smfedtur_sPTLN-t2d10.3-0 ; 
       smfedtur_sPTLN-t2d11.3-0 ; 
       smfedtur_sPTLN-t2d12.3-0 ; 
       smfedtur_sPTLN-t2d13.3-0 ; 
       smfedtur_sPTLN-t2d14.3-0 ; 
       smfedtur_sPTLN-t2d15.3-0 ; 
       smfedtur_sPTLN-t2d16.3-0 ; 
       smfedtur_sPTLN-t2d2.3-0 ; 
       smfedtur_sPTLN-t2d3.3-0 ; 
       smfedtur_sPTLN-t2d4.3-0 ; 
       smfedtur_sPTLN-t2d5.3-0 ; 
       smfedtur_sPTLN-t2d6.3-0 ; 
       smfedtur_sPTLN-t2d7.3-0 ; 
       smfedtur_sPTLN-t2d9.3-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d21.1-0 ; 
       static-t2d24.1-0 ; 
       static-t2d25.1-0 ; 
       static-t2d26.1-0 ; 
       static-t2d27.1-0 ; 
       static-t2d28.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 5 110 ; 
       3 1 110 ; 
       4 2 110 ; 
       5 10 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 21 300 ; 
       2 22 300 ; 
       3 23 300 ; 
       4 24 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       5 25 300 ; 
       6 26 300 ; 
       7 27 300 ; 
       8 12 300 ; 
       8 16 300 ; 
       8 18 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 11 300 ; 
       9 6 300 ; 
       9 15 300 ; 
       9 17 300 ; 
       9 1 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       10 13 300 ; 
       10 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       1 12 401 ; 
       2 13 401 ; 
       3 14 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       14 0 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 0.75 0.75 0.75 0 0 0 0.003738761 -0.005320858 -8.098781e-006 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       19 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
