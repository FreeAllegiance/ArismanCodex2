SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.21-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.21-0 ROOT ; 
       destroy-lite2_4.21-0 ROOT ; 
       destroy-lite2_4_1_3.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       add_guns-mat90.1-0 ; 
       add_guns-mat91.1-0 ; 
       add_guns-mat94.1-0 ; 
       add_guns-mat95.1-0 ; 
       add_guns-mat96.1-0 ; 
       add_guns-mat97.1-0 ; 
       add_guns-mat98.1-0 ; 
       mdfedtur_sPTLN-mat53_1.3-0 ; 
       mdfedtur_sPTLN-mat54.2-0 ; 
       mdfedtur_sPTLN-mat55.3-0 ; 
       mdfedtur_sPTLN-mat56.3-0 ; 
       mdfedtur_sPTLN-mat57.3-0 ; 
       mdfedtur_sPTLN-mat58.2-0 ; 
       mdfedtur_sPTLN-mat59.2-0 ; 
       mdfedtur_sPTLN-mat60.2-0 ; 
       mdfedtur_sPTLN-mat61.2-0 ; 
       mdfedtur_sPTLN-mat62.2-0 ; 
       mdfedtur_sPTLN-mat63.2-0 ; 
       mdfedtur_sPTLN-mat64.2-0 ; 
       mdfedtur_sPTLN-mat65.2-0 ; 
       mdfedtur_sPTLN-mat66.2-0 ; 
       mdfedtur_sPTLN-mat67.2-0 ; 
       mdfedtur_sPTLN-mat68.2-0 ; 
       mdfedtur_sPTLN-mat69.2-0 ; 
       mdfedtur_sPTLN-mat70.2-0 ; 
       mdfedtur_sPTLN-mat71.2-0 ; 
       mdfedtur_sPTLN-mat72.2-0 ; 
       mdfedtur_sPTLN-mat73.2-0 ; 
       mdfedtur_sPTLN-mat74.2-0 ; 
       mdfedtur_sPTLN-mat75.2-0 ; 
       mdfedtur_sPTLN-mat76.2-0 ; 
       mdfedtur_sPTLN-mat77.2-0 ; 
       mdfedtur_sPTLN-mat78.2-0 ; 
       mdfedtur_sPTLN-mat79.2-0 ; 
       mdfedtur_sPTLN-mat80.2-0 ; 
       mdfedtur_sPTLN-mat81.2-0 ; 
       mdfedtur_sPTLN-mat82.2-0 ; 
       mdfedtur_sPTLN-mat83.2-0 ; 
       mdfedtur_sPTLN-mat84.2-0 ; 
       mdfedtur_sPTLN-mat85.2-0 ; 
       mdfedtur_sPTLN-mat86.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       acs63-acs63.17-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-cube1.2-0 ; 
       acs63-cube2.1-0 ; 
       acs63-cube5.1-0 ; 
       acs63-cube6.1-0 ; 
       acs63-cyl4.1-0 ; 
       acs63-cyl5.1-0 ; 
       acs63-lmissemt.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rmissemt.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs63/PICTURES/acs63 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs63-add_guns.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       add_guns-t2d34.1-0 ; 
       add_guns-t2d35.1-0 ; 
       add_guns-t2d38.1-0 ; 
       add_guns-t2d39.1-0 ; 
       add_guns-t2d40.1-0 ; 
       add_guns-t2d41.1-0 ; 
       add_guns-t2d42.1-0 ; 
       mdfedtur_sPTLN-t2d1.5-0 ; 
       mdfedtur_sPTLN-t2d10.4-0 ; 
       mdfedtur_sPTLN-t2d11.4-0 ; 
       mdfedtur_sPTLN-t2d12.4-0 ; 
       mdfedtur_sPTLN-t2d13.4-0 ; 
       mdfedtur_sPTLN-t2d14.4-0 ; 
       mdfedtur_sPTLN-t2d15.4-0 ; 
       mdfedtur_sPTLN-t2d16.4-0 ; 
       mdfedtur_sPTLN-t2d17.5-0 ; 
       mdfedtur_sPTLN-t2d18.4-0 ; 
       mdfedtur_sPTLN-t2d19.4-0 ; 
       mdfedtur_sPTLN-t2d2.5-0 ; 
       mdfedtur_sPTLN-t2d20.4-0 ; 
       mdfedtur_sPTLN-t2d21.4-0 ; 
       mdfedtur_sPTLN-t2d22.4-0 ; 
       mdfedtur_sPTLN-t2d23.4-0 ; 
       mdfedtur_sPTLN-t2d24.4-0 ; 
       mdfedtur_sPTLN-t2d25.4-0 ; 
       mdfedtur_sPTLN-t2d26.4-0 ; 
       mdfedtur_sPTLN-t2d27.4-0 ; 
       mdfedtur_sPTLN-t2d28.4-0 ; 
       mdfedtur_sPTLN-t2d29.4-0 ; 
       mdfedtur_sPTLN-t2d3.5-0 ; 
       mdfedtur_sPTLN-t2d30.4-0 ; 
       mdfedtur_sPTLN-t2d4.4-0 ; 
       mdfedtur_sPTLN-t2d5.4-0 ; 
       mdfedtur_sPTLN-t2d6.4-0 ; 
       mdfedtur_sPTLN-t2d7.4-0 ; 
       mdfedtur_sPTLN-t2d8.4-0 ; 
       mdfedtur_sPTLN-t2d9.4-0 ; 
       mdfedtur_sPTLN-z.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 14 110 ; 
       9 11 110 ; 
       1 12 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 16 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       10 9 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 8 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 5 300 ; 
       9 6 300 ; 
       0 8 300 ; 
       1 7 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       1 31 300 ; 
       1 39 300 ; 
       2 7 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       3 7 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 40 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 3 300 ; 
       7 4 300 ; 
       11 7 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       12 7 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       14 7 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       14 17 300 ; 
       15 7 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       16 7 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       16 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       7 15 401 ; 
       9 7 401 ; 
       10 18 401 ; 
       11 29 401 ; 
       12 31 401 ; 
       13 32 401 ; 
       14 33 401 ; 
       15 34 401 ; 
       16 35 401 ; 
       17 36 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       39 30 401 ; 
       40 37 401 ; 
       5 5 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 11.25 0 0 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 10 -4 0 MPRFLG 0 ; 
       16 SCHEM 11.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
