SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.24-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       mdfedtur_sPTLN-mat53_1.3-0 ; 
       mdfedtur_sPTLN-mat54.2-0 ; 
       mdfedtur_sPTLN-mat55.3-0 ; 
       mdfedtur_sPTLN-mat56.3-0 ; 
       mdfedtur_sPTLN-mat57.3-0 ; 
       mdfedtur_sPTLN-mat58.2-0 ; 
       mdfedtur_sPTLN-mat59.2-0 ; 
       mdfedtur_sPTLN-mat60.2-0 ; 
       mdfedtur_sPTLN-mat61.2-0 ; 
       mdfedtur_sPTLN-mat62.2-0 ; 
       mdfedtur_sPTLN-mat63.2-0 ; 
       mdfedtur_sPTLN-mat64.2-0 ; 
       mdfedtur_sPTLN-mat65.2-0 ; 
       mdfedtur_sPTLN-mat66.2-0 ; 
       mdfedtur_sPTLN-mat67.2-0 ; 
       mdfedtur_sPTLN-mat68.2-0 ; 
       mdfedtur_sPTLN-mat69.2-0 ; 
       mdfedtur_sPTLN-mat70.2-0 ; 
       mdfedtur_sPTLN-mat71.2-0 ; 
       mdfedtur_sPTLN-mat72.2-0 ; 
       mdfedtur_sPTLN-mat73.2-0 ; 
       static-mat90.1-0 ; 
       static-mat91.1-0 ; 
       static-mat94.1-0 ; 
       static-mat95.1-0 ; 
       static-mat96.1-0 ; 
       static-mat97.1-0 ; 
       static-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       acs63-acs63.18-0 ROOT ; 
       acs63-cube1.2-0 ; 
       acs63-cube2.1-0 ; 
       acs63-cube5.1-0 ; 
       acs63-cube6.1-0 ; 
       acs63-cyl4.1-0 ; 
       acs63-cyl5.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs63/PICTURES/acs63 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs63-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       mdfedtur_sPTLN-t2d1.5-0 ; 
       mdfedtur_sPTLN-t2d10.4-0 ; 
       mdfedtur_sPTLN-t2d11.4-0 ; 
       mdfedtur_sPTLN-t2d12.4-0 ; 
       mdfedtur_sPTLN-t2d13.4-0 ; 
       mdfedtur_sPTLN-t2d14.4-0 ; 
       mdfedtur_sPTLN-t2d15.4-0 ; 
       mdfedtur_sPTLN-t2d16.4-0 ; 
       mdfedtur_sPTLN-t2d17.5-0 ; 
       mdfedtur_sPTLN-t2d18.4-0 ; 
       mdfedtur_sPTLN-t2d19.4-0 ; 
       mdfedtur_sPTLN-t2d2.5-0 ; 
       mdfedtur_sPTLN-t2d3.5-0 ; 
       mdfedtur_sPTLN-t2d4.4-0 ; 
       mdfedtur_sPTLN-t2d5.4-0 ; 
       mdfedtur_sPTLN-t2d6.4-0 ; 
       mdfedtur_sPTLN-t2d7.4-0 ; 
       mdfedtur_sPTLN-t2d8.4-0 ; 
       mdfedtur_sPTLN-t2d9.4-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d38.1-0 ; 
       static-t2d39.1-0 ; 
       static-t2d40.1-0 ; 
       static-t2d41.1-0 ; 
       static-t2d42.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 11 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 9 110 ; 
       6 7 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 21 300 ; 
       2 22 300 ; 
       3 24 300 ; 
       4 25 300 ; 
       5 26 300 ; 
       6 27 300 ; 
       7 0 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       8 0 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       9 0 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 10 300 ; 
       10 0 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       11 0 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       0 8 401 ; 
       2 0 401 ; 
       3 11 401 ; 
       4 12 401 ; 
       5 13 401 ; 
       6 14 401 ; 
       7 15 401 ; 
       8 16 401 ; 
       9 17 401 ; 
       10 18 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       19 9 401 ; 
       20 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 0 0 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 11.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
