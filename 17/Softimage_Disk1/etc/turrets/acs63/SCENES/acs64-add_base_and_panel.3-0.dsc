SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.4-0 ROOT ; 
       destroy-lite2_4.4-0 ROOT ; 
       destroy-lite2_4_1_3.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       add_base_and_panel-mat90.1-0 ; 
       add_base_and_panel-mat91.1-0 ; 
       add_base_and_panel-mat92.1-0 ; 
       mdfedtur_sPTLN-mat53_1.1-0 ; 
       mdfedtur_sPTLN-mat54.1-0 ; 
       mdfedtur_sPTLN-mat55.1-0 ; 
       mdfedtur_sPTLN-mat56.1-0 ; 
       mdfedtur_sPTLN-mat57.1-0 ; 
       mdfedtur_sPTLN-mat58.1-0 ; 
       mdfedtur_sPTLN-mat59.1-0 ; 
       mdfedtur_sPTLN-mat60.1-0 ; 
       mdfedtur_sPTLN-mat61.1-0 ; 
       mdfedtur_sPTLN-mat62.1-0 ; 
       mdfedtur_sPTLN-mat63.1-0 ; 
       mdfedtur_sPTLN-mat64.1-0 ; 
       mdfedtur_sPTLN-mat65.1-0 ; 
       mdfedtur_sPTLN-mat66.1-0 ; 
       mdfedtur_sPTLN-mat67.1-0 ; 
       mdfedtur_sPTLN-mat68.1-0 ; 
       mdfedtur_sPTLN-mat69.1-0 ; 
       mdfedtur_sPTLN-mat70.1-0 ; 
       mdfedtur_sPTLN-mat71.1-0 ; 
       mdfedtur_sPTLN-mat72.1-0 ; 
       mdfedtur_sPTLN-mat73.1-0 ; 
       mdfedtur_sPTLN-mat74.1-0 ; 
       mdfedtur_sPTLN-mat75.1-0 ; 
       mdfedtur_sPTLN-mat76.1-0 ; 
       mdfedtur_sPTLN-mat77.1-0 ; 
       mdfedtur_sPTLN-mat78.1-0 ; 
       mdfedtur_sPTLN-mat79.1-0 ; 
       mdfedtur_sPTLN-mat80.1-0 ; 
       mdfedtur_sPTLN-mat81.1-0 ; 
       mdfedtur_sPTLN-mat82.1-0 ; 
       mdfedtur_sPTLN-mat83.1-0 ; 
       mdfedtur_sPTLN-mat84.1-0 ; 
       mdfedtur_sPTLN-mat85.1-0 ; 
       mdfedtur_sPTLN-mat86.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       acs63-acs63.3-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-cube1.2-0 ; 
       acs63-cube2.1-0 ; 
       acs63-face2.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwepmnt.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwepmnt.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs63/PICTURES/acs63 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-add_base_and_panel.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       add_base_and_panel-t2d34.1-0 ; 
       add_base_and_panel-t2d35.1-0 ; 
       add_base_and_panel-t2d36.1-0 ; 
       mdfedtur_sPTLN-t2d1.1-0 ; 
       mdfedtur_sPTLN-t2d10.1-0 ; 
       mdfedtur_sPTLN-t2d11.1-0 ; 
       mdfedtur_sPTLN-t2d12.1-0 ; 
       mdfedtur_sPTLN-t2d13.1-0 ; 
       mdfedtur_sPTLN-t2d14.1-0 ; 
       mdfedtur_sPTLN-t2d15.1-0 ; 
       mdfedtur_sPTLN-t2d16.1-0 ; 
       mdfedtur_sPTLN-t2d17.1-0 ; 
       mdfedtur_sPTLN-t2d18.1-0 ; 
       mdfedtur_sPTLN-t2d19.1-0 ; 
       mdfedtur_sPTLN-t2d2.1-0 ; 
       mdfedtur_sPTLN-t2d20.1-0 ; 
       mdfedtur_sPTLN-t2d21.1-0 ; 
       mdfedtur_sPTLN-t2d22.1-0 ; 
       mdfedtur_sPTLN-t2d23.1-0 ; 
       mdfedtur_sPTLN-t2d24.1-0 ; 
       mdfedtur_sPTLN-t2d25.1-0 ; 
       mdfedtur_sPTLN-t2d26.1-0 ; 
       mdfedtur_sPTLN-t2d27.1-0 ; 
       mdfedtur_sPTLN-t2d28.1-0 ; 
       mdfedtur_sPTLN-t2d29.1-0 ; 
       mdfedtur_sPTLN-t2d3.1-0 ; 
       mdfedtur_sPTLN-t2d30.1-0 ; 
       mdfedtur_sPTLN-t2d4.1-0 ; 
       mdfedtur_sPTLN-t2d5.1-0 ; 
       mdfedtur_sPTLN-t2d6.1-0 ; 
       mdfedtur_sPTLN-t2d7.1-0 ; 
       mdfedtur_sPTLN-t2d8.1-0 ; 
       mdfedtur_sPTLN-t2d9.1-0 ; 
       mdfedtur_sPTLN-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 5 110 ; 
       4 13 110 ; 
       5 4 110 ; 
       1 9 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       7 13 110 ; 
       8 7 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 10 110 ; 
       12 13 110 ; 
       13 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 2 300 ; 
       0 4 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       1 3 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 35 300 ; 
       2 3 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       3 3 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 36 300 ; 
       7 3 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       9 3 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       10 3 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       10 13 300 ; 
       12 3 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       13 3 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 11 401 ; 
       5 3 401 ; 
       6 14 401 ; 
       7 25 401 ; 
       8 27 401 ; 
       9 28 401 ; 
       10 29 401 ; 
       11 30 401 ; 
       12 31 401 ; 
       13 32 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       35 26 401 ; 
       36 33 401 ; 
       2 2 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 5 -8 0 MPRFLG 0 ; 
       0 SCHEM 10 0 0 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
