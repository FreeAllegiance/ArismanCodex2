SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.10-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.10-0 ROOT ; 
       destroy-lite2_4.10-0 ROOT ; 
       destroy-lite2_4_1_3.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       final-mat90.1-0 ; 
       final-mat91.1-0 ; 
       final-mat92.1-0 ; 
       final-mat94.1-0 ; 
       final-mat95.1-0 ; 
       mdfedtur_sPTLN-mat53_1.2-0 ; 
       mdfedtur_sPTLN-mat54.1-0 ; 
       mdfedtur_sPTLN-mat55.2-0 ; 
       mdfedtur_sPTLN-mat56.2-0 ; 
       mdfedtur_sPTLN-mat57.2-0 ; 
       mdfedtur_sPTLN-mat58.1-0 ; 
       mdfedtur_sPTLN-mat59.1-0 ; 
       mdfedtur_sPTLN-mat60.1-0 ; 
       mdfedtur_sPTLN-mat61.1-0 ; 
       mdfedtur_sPTLN-mat62.1-0 ; 
       mdfedtur_sPTLN-mat63.1-0 ; 
       mdfedtur_sPTLN-mat64.1-0 ; 
       mdfedtur_sPTLN-mat65.1-0 ; 
       mdfedtur_sPTLN-mat66.1-0 ; 
       mdfedtur_sPTLN-mat67.1-0 ; 
       mdfedtur_sPTLN-mat68.1-0 ; 
       mdfedtur_sPTLN-mat69.1-0 ; 
       mdfedtur_sPTLN-mat70.1-0 ; 
       mdfedtur_sPTLN-mat71.1-0 ; 
       mdfedtur_sPTLN-mat72.1-0 ; 
       mdfedtur_sPTLN-mat73.1-0 ; 
       mdfedtur_sPTLN-mat74.1-0 ; 
       mdfedtur_sPTLN-mat75.1-0 ; 
       mdfedtur_sPTLN-mat76.1-0 ; 
       mdfedtur_sPTLN-mat77.1-0 ; 
       mdfedtur_sPTLN-mat78.1-0 ; 
       mdfedtur_sPTLN-mat79.1-0 ; 
       mdfedtur_sPTLN-mat80.1-0 ; 
       mdfedtur_sPTLN-mat81.1-0 ; 
       mdfedtur_sPTLN-mat82.1-0 ; 
       mdfedtur_sPTLN-mat83.1-0 ; 
       mdfedtur_sPTLN-mat84.1-0 ; 
       mdfedtur_sPTLN-mat85.1-0 ; 
       mdfedtur_sPTLN-mat86.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       acs63-acs63.8-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-cube1.2-0 ; 
       acs63-cube2.1-0 ; 
       acs63-face2.1-0 ; 
       acs63-face4.1-0 ; 
       acs63-lmissemt.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rmissemt.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs63/PICTURES/acs63 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-final.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       final-t2d34.1-0 ; 
       final-t2d35.1-0 ; 
       final-t2d36.1-0 ; 
       final-t2d38.1-0 ; 
       final-t2d39.1-0 ; 
       mdfedtur_sPTLN-t2d1.3-0 ; 
       mdfedtur_sPTLN-t2d10.2-0 ; 
       mdfedtur_sPTLN-t2d11.2-0 ; 
       mdfedtur_sPTLN-t2d12.2-0 ; 
       mdfedtur_sPTLN-t2d13.2-0 ; 
       mdfedtur_sPTLN-t2d14.2-0 ; 
       mdfedtur_sPTLN-t2d15.2-0 ; 
       mdfedtur_sPTLN-t2d16.2-0 ; 
       mdfedtur_sPTLN-t2d17.3-0 ; 
       mdfedtur_sPTLN-t2d18.2-0 ; 
       mdfedtur_sPTLN-t2d19.2-0 ; 
       mdfedtur_sPTLN-t2d2.3-0 ; 
       mdfedtur_sPTLN-t2d20.2-0 ; 
       mdfedtur_sPTLN-t2d21.2-0 ; 
       mdfedtur_sPTLN-t2d22.2-0 ; 
       mdfedtur_sPTLN-t2d23.2-0 ; 
       mdfedtur_sPTLN-t2d24.2-0 ; 
       mdfedtur_sPTLN-t2d25.2-0 ; 
       mdfedtur_sPTLN-t2d26.2-0 ; 
       mdfedtur_sPTLN-t2d27.2-0 ; 
       mdfedtur_sPTLN-t2d28.2-0 ; 
       mdfedtur_sPTLN-t2d29.2-0 ; 
       mdfedtur_sPTLN-t2d3.3-0 ; 
       mdfedtur_sPTLN-t2d30.2-0 ; 
       mdfedtur_sPTLN-t2d4.2-0 ; 
       mdfedtur_sPTLN-t2d5.2-0 ; 
       mdfedtur_sPTLN-t2d6.2-0 ; 
       mdfedtur_sPTLN-t2d7.2-0 ; 
       mdfedtur_sPTLN-t2d8.2-0 ; 
       mdfedtur_sPTLN-t2d9.2-0 ; 
       mdfedtur_sPTLN-z.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 5 110 ; 
       11 12 110 ; 
       8 9 110 ; 
       7 5 110 ; 
       4 14 110 ; 
       5 4 110 ; 
       1 10 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       14 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 2 300 ; 
       0 6 300 ; 
       7 4 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       1 5 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 37 300 ; 
       2 5 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       3 5 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 38 300 ; 
       9 5 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       10 5 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       12 5 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 15 300 ; 
       13 5 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       14 5 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 13 401 ; 
       7 5 401 ; 
       8 16 401 ; 
       9 27 401 ; 
       10 29 401 ; 
       11 30 401 ; 
       12 31 401 ; 
       13 32 401 ; 
       14 33 401 ; 
       15 34 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       37 28 401 ; 
       38 35 401 ; 
       2 2 401 ; 
       4 4 401 ; 
       3 3 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 5 -8 0 MPRFLG 0 ; 
       0 SCHEM 11.25 0 0 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
       14 SCHEM 11.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
