SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.10-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2_1.10-0 ROOT ; 
       destroy-lite3_4_1_3.10-0 ROOT ; 
       ion_cannon_sPA-light1_1_1.10-0 ROOT ; 
       ion_cannon_sPA-lite1_1_3.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       add_base-mat52.1-0 ; 
       add_base-mat53.1-0 ; 
       add_base-mat54.1-0 ; 
       add_base-mat55.1-0 ; 
       add_base-mat56.1-0 ; 
       add_base-mat57.1-0 ; 
       add_base-mat58.1-0 ; 
       add_base-mat59.1-0 ; 
       add_base-mat60.1-0 ; 
       add_base-mat61.1-0 ; 
       add_base-mat62.1-0 ; 
       add_base-mat63.1-0 ; 
       add_base-mat64.1-0 ; 
       add_base-mat65.1-0 ; 
       add_base-mat66.1-0 ; 
       add_base-mat67.1-0 ; 
       add_base-mat68.1-0 ; 
       add_base-mat69.1-0 ; 
       add_base-mat70.1-0 ; 
       add_base-mat71.1-0 ; 
       add_base-mat72.1-0 ; 
       add_base-mat73.1-0 ; 
       add_base-mat74.1-0 ; 
       add_base-mat75.1-0 ; 
       add_base-mat76.1-0 ; 
       add_base-mat77.1-0 ; 
       add_base-mat78.1-0 ; 
       add_base-mat79.1-0 ; 
       add_base-mat80.1-0 ; 
       add_base-mat81.1-0 ; 
       add_base-mat82.1-0 ; 
       add_base-mat83.1-0 ; 
       add_base-mat84.1-0 ; 
       add_base-mat85.1-0 ; 
       add_base-mat86.1-0 ; 
       add_base-mat87.1-0 ; 
       add_base-mat88.1-0 ; 
       add_base-mat89.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       acs64-acs64.6-0 ROOT ; 
       acs64-antenn1.1-0 ; 
       acs64-antenn2.1-0 ; 
       acs64-antenn3.1-0 ; 
       acs64-cyl1.2-0 ; 
       acs64-cyl2.1-0 ; 
       acs64-lwingzz.1-0 ; 
       acs64-rwingzz.1-0 ; 
       acs64-turret1.1-0 ; 
       acs64-wepbas.1-0 ; 
       acs64-wepmnt.1-0 ; 
       add_base-cube1.2-0 ROOT ; 
       add_base-cube2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs64/PICTURES/acs64 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-add_base.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_base-t2d1.1-0 ; 
       add_base-t2d10.1-0 ; 
       add_base-t2d11.1-0 ; 
       add_base-t2d12.1-0 ; 
       add_base-t2d13.1-0 ; 
       add_base-t2d14.1-0 ; 
       add_base-t2d15.1-0 ; 
       add_base-t2d16.1-0 ; 
       add_base-t2d17.1-0 ; 
       add_base-t2d18.1-0 ; 
       add_base-t2d19.1-0 ; 
       add_base-t2d2.1-0 ; 
       add_base-t2d20.1-0 ; 
       add_base-t2d21.1-0 ; 
       add_base-t2d22.1-0 ; 
       add_base-t2d23.1-0 ; 
       add_base-t2d24.1-0 ; 
       add_base-t2d25.1-0 ; 
       add_base-t2d26.1-0 ; 
       add_base-t2d27.1-0 ; 
       add_base-t2d28.1-0 ; 
       add_base-t2d29.1-0 ; 
       add_base-t2d3.1-0 ; 
       add_base-t2d30.1-0 ; 
       add_base-t2d31.1-0 ; 
       add_base-t2d32.1-0 ; 
       add_base-t2d33.1-0 ; 
       add_base-t2d4.1-0 ; 
       add_base-t2d5.1-0 ; 
       add_base-t2d6.1-0 ; 
       add_base-t2d7.1-0 ; 
       add_base-t2d8.1-0 ; 
       add_base-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 6 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       4 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 36 300 ; 
       5 37 300 ; 
       1 9 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 20 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 13 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       7 8 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 19 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       9 0 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       4 34 300 ; 
       4 35 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 11 401 ; 
       4 22 401 ; 
       5 27 401 ; 
       6 28 401 ; 
       7 29 401 ; 
       10 30 401 ; 
       11 31 401 ; 
       12 32 401 ; 
       13 1 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       21 8 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 12 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       28 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       33 21 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       11 SCHEM 25 0 0 SRT 0.1499999 0.1499999 0.1499999 0 0 0 0 -0.749113 0 MPRFLG 0 ; 
       12 SCHEM 27.5 0 0 SRT 0.2087737 0.02044395 0.02044395 0 0 0 0 -2.457112 0 MPRFLG 0 ; 
       0 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
