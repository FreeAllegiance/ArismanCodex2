SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2_1.4-0 ROOT ; 
       destroy-lite3_4_1_3.4-0 ROOT ; 
       ion_cannon_sPA-light1_1_1.4-0 ROOT ; 
       ion_cannon_sPA-lite1_1_3.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       add_guns-mat52.2-0 ; 
       add_guns-mat53.2-0 ; 
       add_guns-mat54.2-0 ; 
       add_guns-mat55.2-0 ; 
       add_guns-mat56.2-0 ; 
       add_guns-mat57.2-0 ; 
       add_guns-mat58.2-0 ; 
       add_guns-mat59.2-0 ; 
       add_guns-mat60.2-0 ; 
       add_guns-mat61.2-0 ; 
       add_guns-mat62.2-0 ; 
       add_guns-mat63.2-0 ; 
       add_guns-mat64.2-0 ; 
       add_guns-mat65.2-0 ; 
       add_guns-mat66.2-0 ; 
       add_guns-mat67.2-0 ; 
       add_guns-mat68.2-0 ; 
       add_guns-mat69.2-0 ; 
       add_guns-mat70.2-0 ; 
       add_guns-mat71.2-0 ; 
       add_guns-mat72.2-0 ; 
       add_guns-mat73.2-0 ; 
       add_guns-mat74.2-0 ; 
       add_guns-mat75.2-0 ; 
       add_guns-mat76.2-0 ; 
       add_guns-mat77.2-0 ; 
       add_guns-mat78.2-0 ; 
       add_guns-mat79.2-0 ; 
       add_guns-mat80.2-0 ; 
       add_guns-mat81.2-0 ; 
       add_guns-mat82.2-0 ; 
       add_guns-mat83.2-0 ; 
       add_guns-mat84.2-0 ; 
       add_guns-mat85.2-0 ; 
       add_guns-mat86.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       acs64-acs64.4-0 ROOT ; 
       acs64-antenn1.1-0 ; 
       acs64-antenn2.1-0 ; 
       acs64-antenn3.1-0 ; 
       acs64-cyl1.2-0 ; 
       acs64-lwingzz.1-0 ; 
       acs64-rwingzz.1-0 ; 
       acs64-turret1.1-0 ; 
       acs64-wepbas.1-0 ; 
       acs64-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs64/PICTURES/acs64 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-add_guns.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_guns-t2d1.2-0 ; 
       add_guns-t2d10.2-0 ; 
       add_guns-t2d11.2-0 ; 
       add_guns-t2d12.2-0 ; 
       add_guns-t2d13.2-0 ; 
       add_guns-t2d14.2-0 ; 
       add_guns-t2d15.2-0 ; 
       add_guns-t2d16.2-0 ; 
       add_guns-t2d17.2-0 ; 
       add_guns-t2d18.2-0 ; 
       add_guns-t2d19.2-0 ; 
       add_guns-t2d2.2-0 ; 
       add_guns-t2d20.2-0 ; 
       add_guns-t2d21.2-0 ; 
       add_guns-t2d22.2-0 ; 
       add_guns-t2d23.2-0 ; 
       add_guns-t2d24.2-0 ; 
       add_guns-t2d25.2-0 ; 
       add_guns-t2d26.2-0 ; 
       add_guns-t2d27.2-0 ; 
       add_guns-t2d28.2-0 ; 
       add_guns-t2d29.2-0 ; 
       add_guns-t2d3.2-0 ; 
       add_guns-t2d30.1-0 ; 
       add_guns-t2d4.2-0 ; 
       add_guns-t2d5.2-0 ; 
       add_guns-t2d6.2-0 ; 
       add_guns-t2d7.2-0 ; 
       add_guns-t2d8.2-0 ; 
       add_guns-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       4 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 9 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 20 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 13 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 8 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 19 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       8 0 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       4 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 11 401 ; 
       4 22 401 ; 
       5 24 401 ; 
       6 25 401 ; 
       7 26 401 ; 
       10 27 401 ; 
       11 28 401 ; 
       12 29 401 ; 
       13 1 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       21 8 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 12 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       28 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       33 21 401 ; 
       34 23 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
