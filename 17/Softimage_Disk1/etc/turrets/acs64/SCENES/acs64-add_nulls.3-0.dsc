SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.16-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2_1.16-0 ROOT ; 
       destroy-lite3_4_1_3.16-0 ROOT ; 
       ion_cannon_sPA-light1_1_1.16-0 ROOT ; 
       ion_cannon_sPA-lite1_1_3.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       add_nulls-mat52.1-0 ; 
       add_nulls-mat53.1-0 ; 
       add_nulls-mat54.1-0 ; 
       add_nulls-mat55.1-0 ; 
       add_nulls-mat56.1-0 ; 
       add_nulls-mat57.1-0 ; 
       add_nulls-mat58.1-0 ; 
       add_nulls-mat59.1-0 ; 
       add_nulls-mat60.1-0 ; 
       add_nulls-mat61.1-0 ; 
       add_nulls-mat62.1-0 ; 
       add_nulls-mat63.1-0 ; 
       add_nulls-mat64.1-0 ; 
       add_nulls-mat65.1-0 ; 
       add_nulls-mat66.1-0 ; 
       add_nulls-mat67.1-0 ; 
       add_nulls-mat68.1-0 ; 
       add_nulls-mat69.1-0 ; 
       add_nulls-mat70.1-0 ; 
       add_nulls-mat71.1-0 ; 
       add_nulls-mat72.1-0 ; 
       add_nulls-mat73.1-0 ; 
       add_nulls-mat74.1-0 ; 
       add_nulls-mat75.1-0 ; 
       add_nulls-mat76.1-0 ; 
       add_nulls-mat77.1-0 ; 
       add_nulls-mat78.1-0 ; 
       add_nulls-mat79.1-0 ; 
       add_nulls-mat80.1-0 ; 
       add_nulls-mat81.1-0 ; 
       add_nulls-mat82.1-0 ; 
       add_nulls-mat83.1-0 ; 
       add_nulls-mat84.1-0 ; 
       add_nulls-mat85.1-0 ; 
       add_nulls-mat86.1-0 ; 
       add_nulls-mat87.1-0 ; 
       add_nulls-mat88.1-0 ; 
       add_nulls-mat89.1-0 ; 
       add_nulls-mat90.1-0 ; 
       add_nulls-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       acs64-acs64.9-0 ROOT ; 
       acs64-antenn1.1-0 ; 
       acs64-antenn2.1-0 ; 
       acs64-antenn3.1-0 ; 
       acs64-cube1.2-0 ; 
       acs64-cube2.1-0 ; 
       acs64-cyl1.2-0 ; 
       acs64-cyl2.1-0 ; 
       acs64-lwepemt.1-0 ; 
       acs64-lwingzz.1-0 ; 
       acs64-rwepemt.1-0 ; 
       acs64-rwingzz.1-0 ; 
       acs64-turret1.1-0 ; 
       acs64-wepbas.1-0 ; 
       acs64-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs64/PICTURES/acs64 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-add_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       add_nulls-t2d1.1-0 ; 
       add_nulls-t2d10.1-0 ; 
       add_nulls-t2d11.1-0 ; 
       add_nulls-t2d12.1-0 ; 
       add_nulls-t2d13.1-0 ; 
       add_nulls-t2d14.1-0 ; 
       add_nulls-t2d15.1-0 ; 
       add_nulls-t2d16.1-0 ; 
       add_nulls-t2d17.1-0 ; 
       add_nulls-t2d18.1-0 ; 
       add_nulls-t2d19.1-0 ; 
       add_nulls-t2d2.1-0 ; 
       add_nulls-t2d20.1-0 ; 
       add_nulls-t2d21.1-0 ; 
       add_nulls-t2d22.1-0 ; 
       add_nulls-t2d23.1-0 ; 
       add_nulls-t2d24.1-0 ; 
       add_nulls-t2d25.1-0 ; 
       add_nulls-t2d26.1-0 ; 
       add_nulls-t2d27.1-0 ; 
       add_nulls-t2d28.1-0 ; 
       add_nulls-t2d29.1-0 ; 
       add_nulls-t2d3.1-0 ; 
       add_nulls-t2d30.1-0 ; 
       add_nulls-t2d31.1-0 ; 
       add_nulls-t2d32.1-0 ; 
       add_nulls-t2d33.1-0 ; 
       add_nulls-t2d34.1-0 ; 
       add_nulls-t2d35.1-0 ; 
       add_nulls-t2d4.1-0 ; 
       add_nulls-t2d5.1-0 ; 
       add_nulls-t2d6.1-0 ; 
       add_nulls-t2d7.1-0 ; 
       add_nulls-t2d8.1-0 ; 
       add_nulls-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 7 110 ; 
       10 6 110 ; 
       1 9 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       6 11 110 ; 
       7 9 110 ; 
       9 12 110 ; 
       11 12 110 ; 
       12 0 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       4 12 110 ; 
       5 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 9 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 20 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 13 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       11 8 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 19 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       13 0 300 ; 
       13 3 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       4 38 300 ; 
       5 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 11 401 ; 
       4 22 401 ; 
       5 29 401 ; 
       6 30 401 ; 
       7 31 401 ; 
       10 32 401 ; 
       11 33 401 ; 
       12 34 401 ; 
       13 1 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       21 8 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 12 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       28 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       33 21 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
