SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.20-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-lite2_4_1_2_1.20-0 ROOT ; 
       destroy-lite3_4_1_3.20-0 ROOT ; 
       ion_cannon_sPA-light1_1_1.20-0 ROOT ; 
       ion_cannon_sPA-lite1_1_3.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       final-bottom1.1-0 ; 
       final-mat53.1-0 ; 
       final-mat54.1-0 ; 
       final-mat60.1-0 ; 
       final-mat61.1-0 ; 
       final-mat62.1-0 ; 
       final-mat63.1-0 ; 
       final-mat64.1-0 ; 
       final-mat65.1-0 ; 
       final-mat66.1-0 ; 
       final-mat67.1-0 ; 
       final-mat68.1-0 ; 
       final-mat69.1-0 ; 
       final-mat70.1-0 ; 
       final-mat71.1-0 ; 
       final-mat72.1-0 ; 
       final-mat73.1-0 ; 
       final-mat74.1-0 ; 
       final-mat75.1-0 ; 
       final-mat76.1-0 ; 
       final-mat77.1-0 ; 
       final-mat78.1-0 ; 
       final-mat79.1-0 ; 
       final-mat80.1-0 ; 
       final-mat81.1-0 ; 
       final-mat82.1-0 ; 
       final-mat83.1-0 ; 
       final-mat84.1-0 ; 
       final-mat85.1-0 ; 
       final-mat86.1-0 ; 
       final-mat87.1-0 ; 
       final-mat88.1-0 ; 
       final-mat89.1-0 ; 
       final-mat90.1-0 ; 
       final-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       acs64-acs64.11-0 ROOT ; 
       acs64-antenn1.1-0 ; 
       acs64-antenn2.1-0 ; 
       acs64-antenn3.1-0 ; 
       acs64-cube1.2-0 ; 
       acs64-cube2.1-0 ; 
       acs64-cyl1.2-0 ; 
       acs64-cyl2.1-0 ; 
       acs64-lwepemt.1-0 ; 
       acs64-lwingzz.1-0 ; 
       acs64-rwepemt.1-0 ; 
       acs64-rwingzz.1-0 ; 
       acs64-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/turrets/acs64/PICTURES/acs64 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs64-final.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       final-t2d1.1-0 ; 
       final-t2d10.1-0 ; 
       final-t2d11.1-0 ; 
       final-t2d12.1-0 ; 
       final-t2d13.1-0 ; 
       final-t2d14.1-0 ; 
       final-t2d15.1-0 ; 
       final-t2d16.1-0 ; 
       final-t2d17.1-0 ; 
       final-t2d18.1-0 ; 
       final-t2d19.1-0 ; 
       final-t2d20.1-0 ; 
       final-t2d21.1-0 ; 
       final-t2d22.1-0 ; 
       final-t2d23.1-0 ; 
       final-t2d24.1-0 ; 
       final-t2d25.1-0 ; 
       final-t2d26.1-0 ; 
       final-t2d27.1-0 ; 
       final-t2d28.1-0 ; 
       final-t2d29.1-0 ; 
       final-t2d30.1-0 ; 
       final-t2d31.1-0 ; 
       final-t2d32.1-0 ; 
       final-t2d33.1-0 ; 
       final-t2d34.1-0 ; 
       final-t2d35.1-0 ; 
       final-t2d36.1-0 ; 
       final-t2d7.1-0 ; 
       final-t2d8.1-0 ; 
       final-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 12 110 ; 
       5 4 110 ; 
       6 11 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       9 12 110 ; 
       10 6 110 ; 
       11 12 110 ; 
       12 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 15 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       4 33 300 ; 
       5 34 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 8 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       11 3 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 14 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       12 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 27 401 ; 
       2 0 401 ; 
       5 28 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
