SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.10-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.10-0 ROOT ; 
       destroy-lite2_4.10-0 ROOT ; 
       destroy-lite2_4_1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       mdRIXtur_F-mat53_1.1-0 ; 
       mdRIXtur_F-mat55.1-0 ; 
       mdRIXtur_F-mat56.1-0 ; 
       mdRIXtur_F-mat57.1-0 ; 
       mdRIXtur_F-mat58.1-0 ; 
       mdRIXtur_F-mat59.1-0 ; 
       mdRIXtur_F-mat60.1-0 ; 
       mdRIXtur_F-mat61.1-0 ; 
       mdRIXtur_F-mat62.1-0 ; 
       mdRIXtur_F-mat63.1-0 ; 
       mdRIXtur_F-mat64.1-0 ; 
       mdRIXtur_F-mat65.1-0 ; 
       mdRIXtur_F-mat66.1-0 ; 
       mdRIXtur_F-mat67.1-0 ; 
       mdRIXtur_F-mat68.1-0 ; 
       mdRIXtur_F-mat69.1-0 ; 
       mdRIXtur_F-mat70.1-0 ; 
       mdRIXtur_F-mat71.1-0 ; 
       mdRIXtur_F-mat72.1-0 ; 
       mdRIXtur_F-mat73.1-0 ; 
       mdRIXtur_F-mat87.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       acs63-bfusegl3.1-0 ; 
       acs63-bfuselg1.1-0 ; 
       acs63-bfuselg2.1-0 ; 
       acs63-fuselg.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/acs65/PICTURES/acs65 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs65-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       mdRIXtur_F-t2d1.1-0 ; 
       mdRIXtur_F-t2d10.1-0 ; 
       mdRIXtur_F-t2d11.1-0 ; 
       mdRIXtur_F-t2d12.1-0 ; 
       mdRIXtur_F-t2d13.1-0 ; 
       mdRIXtur_F-t2d14.1-0 ; 
       mdRIXtur_F-t2d15.1-0 ; 
       mdRIXtur_F-t2d16.1-0 ; 
       mdRIXtur_F-t2d17.1-0 ; 
       mdRIXtur_F-t2d18.1-0 ; 
       mdRIXtur_F-t2d19.1-0 ; 
       mdRIXtur_F-t2d2.1-0 ; 
       mdRIXtur_F-t2d3.1-0 ; 
       mdRIXtur_F-t2d4.1-0 ; 
       mdRIXtur_F-t2d5.1-0 ; 
       mdRIXtur_F-t2d6.1-0 ; 
       mdRIXtur_F-t2d7.1-0 ; 
       mdRIXtur_F-t2d8.1-0 ; 
       mdRIXtur_F-t2d9.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 23 300 ; 
       1 21 300 ; 
       2 22 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       4 0 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       5 0 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       6 0 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 9 300 ; 
       7 0 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       7 20 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       1 0 401 ; 
       2 11 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 17 401 ; 
       7 18 401 ; 
       8 16 401 ; 
       9 15 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 6 401 ; 
       18 4 401 ; 
       19 1 401 ; 
       20 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 11.25 0 0 SRT 3.440571 3.440571 3.440571 0 0 0 3.55596e-014 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
