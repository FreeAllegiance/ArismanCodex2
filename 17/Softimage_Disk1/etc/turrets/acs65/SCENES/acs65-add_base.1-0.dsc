SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.2-0 ROOT ; 
       destroy-lite2_4.2-0 ROOT ; 
       destroy-lite2_4_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       add_base-mat11.1-0 ; 
       add_base-mat12.1-0 ; 
       add_base-mat13.1-0 ; 
       add_base-mat8.1-0 ; 
       add_base-mat9.1-0 ; 
       mdRIXtur_F-mat53_1.1-0 ; 
       mdRIXtur_F-mat55.1-0 ; 
       mdRIXtur_F-mat56.1-0 ; 
       mdRIXtur_F-mat57.1-0 ; 
       mdRIXtur_F-mat58.1-0 ; 
       mdRIXtur_F-mat59.1-0 ; 
       mdRIXtur_F-mat60.1-0 ; 
       mdRIXtur_F-mat61.1-0 ; 
       mdRIXtur_F-mat62.1-0 ; 
       mdRIXtur_F-mat63.1-0 ; 
       mdRIXtur_F-mat64.1-0 ; 
       mdRIXtur_F-mat65.1-0 ; 
       mdRIXtur_F-mat66.1-0 ; 
       mdRIXtur_F-mat67.1-0 ; 
       mdRIXtur_F-mat68.1-0 ; 
       mdRIXtur_F-mat69.1-0 ; 
       mdRIXtur_F-mat70.1-0 ; 
       mdRIXtur_F-mat71.1-0 ; 
       mdRIXtur_F-mat72.1-0 ; 
       mdRIXtur_F-mat73.1-0 ; 
       mdRIXtur_F-mat74.1-0 ; 
       mdRIXtur_F-mat75.1-0 ; 
       mdRIXtur_F-mat76.1-0 ; 
       mdRIXtur_F-mat77.1-0 ; 
       mdRIXtur_F-mat78.1-0 ; 
       mdRIXtur_F-mat79.1-0 ; 
       mdRIXtur_F-mat80.1-0 ; 
       mdRIXtur_F-mat81.1-0 ; 
       mdRIXtur_F-mat82.1-0 ; 
       mdRIXtur_F-mat83.1-0 ; 
       mdRIXtur_F-mat84.1-0 ; 
       mdRIXtur_F-mat85.1-0 ; 
       mdRIXtur_F-mat86.1-0 ; 
       mdRIXtur_F-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-bfusegl3.1-0 ; 
       acs63-bfuselg1.1-0 ; 
       acs63-bfuselg2.1-0 ; 
       acs63-fuselg.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/turrets/acs65/PICTURES/acs02 ; 
       F:/Pete_Data3/turrets/acs65/PICTURES/acs65 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs65-add_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_base-t2d10.1-0 ; 
       add_base-t2d11.1-0 ; 
       add_base-t2d12.1-0 ; 
       add_base-t2d7.1-0 ; 
       add_base-t2d8.1-0 ; 
       mdRIXtur_F-t2d1.1-0 ; 
       mdRIXtur_F-t2d10.1-0 ; 
       mdRIXtur_F-t2d11.1-0 ; 
       mdRIXtur_F-t2d12.1-0 ; 
       mdRIXtur_F-t2d13.1-0 ; 
       mdRIXtur_F-t2d14.1-0 ; 
       mdRIXtur_F-t2d15.1-0 ; 
       mdRIXtur_F-t2d16.1-0 ; 
       mdRIXtur_F-t2d17.1-0 ; 
       mdRIXtur_F-t2d18.1-0 ; 
       mdRIXtur_F-t2d19.1-0 ; 
       mdRIXtur_F-t2d2.1-0 ; 
       mdRIXtur_F-t2d20.1-0 ; 
       mdRIXtur_F-t2d21.1-0 ; 
       mdRIXtur_F-t2d22.1-0 ; 
       mdRIXtur_F-t2d23.1-0 ; 
       mdRIXtur_F-t2d24.1-0 ; 
       mdRIXtur_F-t2d25.1-0 ; 
       mdRIXtur_F-t2d26.1-0 ; 
       mdRIXtur_F-t2d27.1-0 ; 
       mdRIXtur_F-t2d28.1-0 ; 
       mdRIXtur_F-t2d29.1-0 ; 
       mdRIXtur_F-t2d3.1-0 ; 
       mdRIXtur_F-t2d30.1-0 ; 
       mdRIXtur_F-t2d31.1-0 ; 
       mdRIXtur_F-t2d4.1-0 ; 
       mdRIXtur_F-t2d5.1-0 ; 
       mdRIXtur_F-t2d6.1-0 ; 
       mdRIXtur_F-t2d7.1-0 ; 
       mdRIXtur_F-t2d8.1-0 ; 
       mdRIXtur_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 5 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 11 110 ; 
       0 8 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       0 5 300 ; 
       0 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 36 300 ; 
       1 5 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       2 5 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 37 300 ; 
       7 5 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       8 5 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       9 5 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 14 300 ; 
       10 5 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 38 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       6 5 401 ; 
       7 16 401 ; 
       8 27 401 ; 
       9 30 401 ; 
       10 31 401 ; 
       11 34 401 ; 
       12 35 401 ; 
       13 33 401 ; 
       14 32 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 11 401 ; 
       23 9 401 ; 
       24 6 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       36 25 401 ; 
       37 26 401 ; 
       38 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.33056 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 24.83056 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.33056 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 19.83056 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.33056 -4 0 MPRFLG 0 ; 
       5 SCHEM 19.83056 -4 0 MPRFLG 0 ; 
       6 SCHEM 18.58056 -2 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 14.83056 -2 0 USR MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.33056 -2 0 USR MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 11.25 0 0 SRT 3.440571 3.440571 3.440571 0 0 0 3.55596e-014 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 18.83056 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.33056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.33056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.33056 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.33056 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.33056 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.33056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.33056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.33056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 18.83056 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.33056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.33056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.33056 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.33056 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.33056 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.33056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.33056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.33056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
