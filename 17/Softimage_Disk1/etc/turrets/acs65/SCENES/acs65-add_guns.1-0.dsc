SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.5-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.5-0 ROOT ; 
       destroy-lite2_4.5-0 ROOT ; 
       destroy-lite2_4_1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       add_guns-mat100.1-0 ; 
       add_guns-mat101.1-0 ; 
       add_guns-mat102.1-0 ; 
       add_guns-mat103.1-0 ; 
       add_guns-mat104.1-0 ; 
       add_guns-mat105.1-0 ; 
       add_guns-mat106.1-0 ; 
       add_guns-mat107.1-0 ; 
       add_guns-mat108.1-0 ; 
       add_guns-mat109.1-0 ; 
       add_guns-mat11.1-0 ; 
       add_guns-mat110.1-0 ; 
       add_guns-mat111.1-0 ; 
       add_guns-mat112.1-0 ; 
       add_guns-mat113.1-0 ; 
       add_guns-mat114.1-0 ; 
       add_guns-mat115.1-0 ; 
       add_guns-mat116.1-0 ; 
       add_guns-mat117.1-0 ; 
       add_guns-mat118.1-0 ; 
       add_guns-mat119.1-0 ; 
       add_guns-mat12.1-0 ; 
       add_guns-mat13.1-0 ; 
       add_guns-mat53_2.1-0 ; 
       add_guns-mat53_3.1-0 ; 
       add_guns-mat53_4.1-0 ; 
       add_guns-mat53_5.1-0 ; 
       add_guns-mat53_6.1-0 ; 
       add_guns-mat53_7.1-0 ; 
       add_guns-mat53_8.1-0 ; 
       add_guns-mat53_9.1-0 ; 
       add_guns-mat8.1-0 ; 
       add_guns-mat88.1-0 ; 
       add_guns-mat89.1-0 ; 
       add_guns-mat9.1-0 ; 
       add_guns-mat90.1-0 ; 
       add_guns-mat91.1-0 ; 
       add_guns-mat92.1-0 ; 
       add_guns-mat93.1-0 ; 
       add_guns-mat94.1-0 ; 
       add_guns-mat95.1-0 ; 
       add_guns-mat96.1-0 ; 
       add_guns-mat97.1-0 ; 
       add_guns-mat98.1-0 ; 
       add_guns-mat99.1-0 ; 
       mdRIXtur_F-mat53_1.1-0 ; 
       mdRIXtur_F-mat55.1-0 ; 
       mdRIXtur_F-mat56.1-0 ; 
       mdRIXtur_F-mat57.1-0 ; 
       mdRIXtur_F-mat58.1-0 ; 
       mdRIXtur_F-mat59.1-0 ; 
       mdRIXtur_F-mat60.1-0 ; 
       mdRIXtur_F-mat61.1-0 ; 
       mdRIXtur_F-mat62.1-0 ; 
       mdRIXtur_F-mat63.1-0 ; 
       mdRIXtur_F-mat64.1-0 ; 
       mdRIXtur_F-mat65.1-0 ; 
       mdRIXtur_F-mat66.1-0 ; 
       mdRIXtur_F-mat67.1-0 ; 
       mdRIXtur_F-mat68.1-0 ; 
       mdRIXtur_F-mat69.1-0 ; 
       mdRIXtur_F-mat70.1-0 ; 
       mdRIXtur_F-mat71.1-0 ; 
       mdRIXtur_F-mat72.1-0 ; 
       mdRIXtur_F-mat73.1-0 ; 
       mdRIXtur_F-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       acs63-antenn10.1-0 ; 
       acs63-antenn11.1-0 ; 
       acs63-antenn4.1-0 ; 
       acs63-antenn5.1-0 ; 
       acs63-antenn6.1-0 ; 
       acs63-antenn7.1-0 ; 
       acs63-antenn8.1-0 ; 
       acs63-antenn9.1-0 ; 
       acs63-bfusegl3.1-0 ; 
       acs63-bfuselg1.1-0 ; 
       acs63-bfuselg2.1-0 ; 
       acs63-fuselg.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/acs65/PICTURES/acs65 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs65-add_guns.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       add_guns-t2d10.1-0 ; 
       add_guns-t2d11.1-0 ; 
       add_guns-t2d12.1-0 ; 
       add_guns-t2d32.1-0 ; 
       add_guns-t2d33.1-0 ; 
       add_guns-t2d34.1-0 ; 
       add_guns-t2d35.1-0 ; 
       add_guns-t2d36.1-0 ; 
       add_guns-t2d37.1-0 ; 
       add_guns-t2d38.1-0 ; 
       add_guns-t2d39.1-0 ; 
       add_guns-t2d40.1-0 ; 
       add_guns-t2d41.1-0 ; 
       add_guns-t2d42.1-0 ; 
       add_guns-t2d43.1-0 ; 
       add_guns-t2d44.1-0 ; 
       add_guns-t2d45.1-0 ; 
       add_guns-t2d46.1-0 ; 
       add_guns-t2d47.1-0 ; 
       add_guns-t2d48.1-0 ; 
       add_guns-t2d49.1-0 ; 
       add_guns-t2d50.1-0 ; 
       add_guns-t2d51.1-0 ; 
       add_guns-t2d52.1-0 ; 
       add_guns-t2d53.1-0 ; 
       add_guns-t2d54.1-0 ; 
       add_guns-t2d55.1-0 ; 
       add_guns-t2d56.1-0 ; 
       add_guns-t2d57.1-0 ; 
       add_guns-t2d58.1-0 ; 
       add_guns-t2d59.1-0 ; 
       add_guns-t2d7.1-0 ; 
       add_guns-t2d8.1-0 ; 
       mdRIXtur_F-t2d1.1-0 ; 
       mdRIXtur_F-t2d10.1-0 ; 
       mdRIXtur_F-t2d11.1-0 ; 
       mdRIXtur_F-t2d12.1-0 ; 
       mdRIXtur_F-t2d13.1-0 ; 
       mdRIXtur_F-t2d14.1-0 ; 
       mdRIXtur_F-t2d15.1-0 ; 
       mdRIXtur_F-t2d16.1-0 ; 
       mdRIXtur_F-t2d17.1-0 ; 
       mdRIXtur_F-t2d18.1-0 ; 
       mdRIXtur_F-t2d19.1-0 ; 
       mdRIXtur_F-t2d2.1-0 ; 
       mdRIXtur_F-t2d3.1-0 ; 
       mdRIXtur_F-t2d4.1-0 ; 
       mdRIXtur_F-t2d5.1-0 ; 
       mdRIXtur_F-t2d6.1-0 ; 
       mdRIXtur_F-t2d7.1-0 ; 
       mdRIXtur_F-t2d8.1-0 ; 
       mdRIXtur_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 12 110 ; 
       8 10 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       4 12 110 ; 
       11 16 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 14 110 ; 
       7 6 110 ; 
       0 14 110 ; 
       1 0 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 23 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 35 300 ; 
       2 36 300 ; 
       8 22 300 ; 
       9 10 300 ; 
       10 21 300 ; 
       4 25 300 ; 
       4 41 300 ; 
       4 42 300 ; 
       4 43 300 ; 
       4 44 300 ; 
       11 31 300 ; 
       11 34 300 ; 
       3 24 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 39 300 ; 
       3 40 300 ; 
       5 26 300 ; 
       5 0 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       6 27 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 28 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       0 29 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 30 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       12 45 300 ; 
       12 51 300 ; 
       12 52 300 ; 
       12 53 300 ; 
       13 45 300 ; 
       13 57 300 ; 
       13 58 300 ; 
       13 59 300 ; 
       13 60 300 ; 
       13 61 300 ; 
       14 45 300 ; 
       14 49 300 ; 
       14 50 300 ; 
       14 54 300 ; 
       15 45 300 ; 
       15 55 300 ; 
       15 56 300 ; 
       15 62 300 ; 
       15 63 300 ; 
       15 64 300 ; 
       15 65 300 ; 
       16 45 300 ; 
       16 46 300 ; 
       16 47 300 ; 
       16 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       32 3 401 ; 
       33 4 401 ; 
       35 5 401 ; 
       36 6 401 ; 
       37 7 401 ; 
       38 8 401 ; 
       10 0 401 ; 
       21 1 401 ; 
       22 2 401 ; 
       31 31 401 ; 
       34 32 401 ; 
       46 33 401 ; 
       47 44 401 ; 
       48 45 401 ; 
       49 46 401 ; 
       50 47 401 ; 
       51 50 401 ; 
       52 51 401 ; 
       53 49 401 ; 
       54 48 401 ; 
       55 35 401 ; 
       56 36 401 ; 
       57 40 401 ; 
       58 41 401 ; 
       59 42 401 ; 
       60 43 401 ; 
       61 39 401 ; 
       63 37 401 ; 
       64 34 401 ; 
       41 10 401 ; 
       42 11 401 ; 
       43 12 401 ; 
       44 13 401 ; 
       0 14 401 ; 
       1 15 401 ; 
       3 16 401 ; 
       4 17 401 ; 
       5 18 401 ; 
       65 38 401 ; 
       40 9 401 ; 
       6 19 401 ; 
       7 20 401 ; 
       8 21 401 ; 
       9 22 401 ; 
       12 23 401 ; 
       13 24 401 ; 
       14 25 401 ; 
       15 26 401 ; 
       16 27 401 ; 
       17 28 401 ; 
       18 29 401 ; 
       20 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -4 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 MPRFLG 0 ; 
       16 SCHEM 13.75 0 0 SRT 3.440571 3.440571 3.440571 0 0 0 3.55596e-014 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
