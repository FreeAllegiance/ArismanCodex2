SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       destroy-lite2.1-0 ROOT ; 
       destroy-lite2_4.1-0 ROOT ; 
       destroy-lite2_4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       mdRIXtur_F-mat53_1.1-0 ; 
       mdRIXtur_F-mat54.1-0 ; 
       mdRIXtur_F-mat55.1-0 ; 
       mdRIXtur_F-mat56.1-0 ; 
       mdRIXtur_F-mat57.1-0 ; 
       mdRIXtur_F-mat58.1-0 ; 
       mdRIXtur_F-mat59.1-0 ; 
       mdRIXtur_F-mat60.1-0 ; 
       mdRIXtur_F-mat61.1-0 ; 
       mdRIXtur_F-mat62.1-0 ; 
       mdRIXtur_F-mat63.1-0 ; 
       mdRIXtur_F-mat64.1-0 ; 
       mdRIXtur_F-mat65.1-0 ; 
       mdRIXtur_F-mat66.1-0 ; 
       mdRIXtur_F-mat67.1-0 ; 
       mdRIXtur_F-mat68.1-0 ; 
       mdRIXtur_F-mat69.1-0 ; 
       mdRIXtur_F-mat70.1-0 ; 
       mdRIXtur_F-mat71.1-0 ; 
       mdRIXtur_F-mat72.1-0 ; 
       mdRIXtur_F-mat73.1-0 ; 
       mdRIXtur_F-mat74.1-0 ; 
       mdRIXtur_F-mat75.1-0 ; 
       mdRIXtur_F-mat76.1-0 ; 
       mdRIXtur_F-mat77.1-0 ; 
       mdRIXtur_F-mat78.1-0 ; 
       mdRIXtur_F-mat79.1-0 ; 
       mdRIXtur_F-mat80.1-0 ; 
       mdRIXtur_F-mat81.1-0 ; 
       mdRIXtur_F-mat82.1-0 ; 
       mdRIXtur_F-mat83.1-0 ; 
       mdRIXtur_F-mat84.1-0 ; 
       mdRIXtur_F-mat85.1-0 ; 
       mdRIXtur_F-mat86.1-0 ; 
       mdRIXtur_F-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       acs63-acs63.1-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwepmnt.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwepmnt.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
       mdRIXtur_F-DECAL-antenn1.1-0 ; 
       mdRIXtur_F-DECAL-antenn1_1.1-0 ; 
       mdRIXtur_F-DECAL-antenn1_2.1-0 ; 
       mdRIXtur_F-DECAL-antenn1_3.1-0 ; 
       mdRIXtur_F-DECAL-antenn1_5.1-0 ; 
       mdRIXtur_F-DECAL-antenn2.1-0 ; 
       mdRIXtur_F-DECAL-antenn2_1.1-0 ; 
       mdRIXtur_F-DECAL-antenn2_2.1-0 ; 
       mdRIXtur_F-DECAL-antenn2_3.1-0 ; 
       mdRIXtur_F-DECAL-antenn3.1-0 ; 
       mdRIXtur_F-DECAL-antenn3_1.1-0 ; 
       mdRIXtur_F-DECAL-antenn3_2.1-0 ; 
       mdRIXtur_F-DECAL-antenn3_3.1-0 ; 
       mdRIXtur_F-DECAL-antenn3_4.1-0 ; 
       mdRIXtur_F-DECAL-lwepbas.1-0 ; 
       mdRIXtur_F-DECAL-lwepbas_1.1-0 ; 
       mdRIXtur_F-DECAL-lwepbas_2.1-0 ; 
       mdRIXtur_F-DECAL-lwingzz.1-0 ; 
       mdRIXtur_F-DECAL-lwingzz_1.1-0 ; 
       mdRIXtur_F-DECAL-lwingzz_2.1-0 ; 
       mdRIXtur_F-DECAL-lwingzz_3.1-0 ; 
       mdRIXtur_F-DECAL-lwingzz_4.1-0 ; 
       mdRIXtur_F-DECAL-rwepbas.1-0 ; 
       mdRIXtur_F-DECAL-rwepbas_1.1-0 ; 
       mdRIXtur_F-DECAL-rwepbas_2.1-0 ; 
       mdRIXtur_F-DECAL-rwingzz.1-0 ; 
       mdRIXtur_F-DECAL-rwingzz_1.1-0 ; 
       mdRIXtur_F-DECAL-rwingzz_2.1-0 ; 
       mdRIXtur_F-DECAL-rwingzz_3.1-0 ; 
       mdRIXtur_F-DECAL-rwingzz_4.1-0 ; 
       mdRIXtur_F-DECAL-turret1.1-0 ; 
       mdRIXtur_F-DECAL-turret1_1.1-0 ; 
       mdRIXtur_F-DECAL-turret1_2.1-0 ; 
       mdRIXtur_F-STENCIL-acs65.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/turrets/acs65/PICTURES/acs65 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs65-mdRIXtur_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       mdRIXtur_F-t2d1.1-0 ; 
       mdRIXtur_F-t2d10.1-0 ; 
       mdRIXtur_F-t2d11.1-0 ; 
       mdRIXtur_F-t2d12.1-0 ; 
       mdRIXtur_F-t2d13.1-0 ; 
       mdRIXtur_F-t2d14.1-0 ; 
       mdRIXtur_F-t2d15.1-0 ; 
       mdRIXtur_F-t2d16.1-0 ; 
       mdRIXtur_F-t2d17.1-0 ; 
       mdRIXtur_F-t2d18.1-0 ; 
       mdRIXtur_F-t2d19.1-0 ; 
       mdRIXtur_F-t2d2.1-0 ; 
       mdRIXtur_F-t2d20.1-0 ; 
       mdRIXtur_F-t2d21.1-0 ; 
       mdRIXtur_F-t2d22.1-0 ; 
       mdRIXtur_F-t2d23.1-0 ; 
       mdRIXtur_F-t2d24.1-0 ; 
       mdRIXtur_F-t2d25.1-0 ; 
       mdRIXtur_F-t2d26.1-0 ; 
       mdRIXtur_F-t2d27.1-0 ; 
       mdRIXtur_F-t2d28.1-0 ; 
       mdRIXtur_F-t2d29.1-0 ; 
       mdRIXtur_F-t2d3.1-0 ; 
       mdRIXtur_F-t2d30.1-0 ; 
       mdRIXtur_F-t2d31.1-0 ; 
       mdRIXtur_F-t2d4.1-0 ; 
       mdRIXtur_F-t2d5.1-0 ; 
       mdRIXtur_F-t2d6.1-0 ; 
       mdRIXtur_F-t2d7.1-0 ; 
       mdRIXtur_F-t2d8.1-0 ; 
       mdRIXtur_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 10 110 ; 
       5 4 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 0 110 ; 
       11 44 110 ; 
       11 11 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       11 11 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       12 44 110 ; 
       12 12 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       12 12 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       13 44 110 ; 
       13 13 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       13 13 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       14 44 110 ; 
       14 14 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       14 14 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       15 44 110 ; 
       15 15 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       15 15 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       16 44 110 ; 
       16 16 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       16 16 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       17 44 110 ; 
       17 17 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       17 17 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       18 44 110 ; 
       18 18 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       18 18 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       19 44 110 ; 
       19 19 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       19 19 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       20 44 110 ; 
       20 20 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       20 20 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       21 44 110 ; 
       21 21 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       21 21 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       22 44 110 ; 
       22 22 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       22 22 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       23 44 110 ; 
       23 23 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       23 23 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       24 44 110 ; 
       24 24 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       24 24 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       25 44 110 ; 
       25 25 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       25 25 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       26 44 110 ; 
       26 26 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       26 26 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       27 44 110 ; 
       27 27 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       27 27 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       28 44 110 ; 
       28 28 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       28 28 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       29 44 110 ; 
       29 29 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       29 29 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       30 44 110 ; 
       30 30 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       30 30 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       31 44 110 ; 
       31 31 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       31 31 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       32 44 110 ; 
       32 32 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       32 32 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       33 44 110 ; 
       33 33 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       33 33 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       34 44 110 ; 
       34 34 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       34 34 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       35 44 110 ; 
       35 35 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       35 35 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       36 44 110 ; 
       36 36 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       36 36 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       37 44 110 ; 
       37 37 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       37 37 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       38 44 110 ; 
       38 38 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       38 38 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       39 44 110 ; 
       39 39 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       39 39 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       40 44 110 ; 
       40 40 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       40 40 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       41 44 110 ; 
       41 41 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       41 41 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       42 44 110 ; 
       42 42 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       42 42 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       43 44 110 ; 
       43 43 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       43 43 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 32 300 ; 
       2 0 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       3 0 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 33 300 ; 
       4 0 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       6 0 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 0 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 10 300 ; 
       9 0 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 34 300 ; 
       10 0 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       10 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 11 401 ; 
       4 22 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 29 401 ; 
       8 30 401 ; 
       9 28 401 ; 
       10 27 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 6 401 ; 
       19 4 401 ; 
       20 1 401 ; 
       21 16 401 ; 
       22 17 401 ; 
       23 18 401 ; 
       24 19 401 ; 
       25 12 401 ; 
       26 13 401 ; 
       27 14 401 ; 
       28 15 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       32 20 401 ; 
       33 21 401 ; 
       34 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -2 0 DISPLAY 1 2 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       4 SCHEM 7.330564 -6 0 USR MPRFLG 0 ; 
       5 SCHEM 7.330564 -8 0 USR MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 4.830559 -6 0 USR MPRFLG 0 ; 
       8 SCHEM 4.830559 -8 0 USR MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 60 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 62.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 65 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 52.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 50 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 55 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 77.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 72.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 75 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 70 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 80 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 20 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 15 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 40 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 35 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 37.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 45 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 10 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 12.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 27.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 22.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 30 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 32.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 0 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 40 -14 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
