SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2.1-0 ; 
       utl01-utl01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.1-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       gen_buoy_F-light1_2.1-0 ROOT ; 
       gen_buoy_F-light2_2.1-0 ROOT ; 
       marker-inf_light4_3.1-0 ROOT ; 
       turret-inf_light3_5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       gen_buoy_F-default3.1-0 ; 
       gen_buoy_F-default5.1-0 ; 
       gen_buoy_F-default6.1-0 ; 
       gen_buoy_F-mat100.1-0 ; 
       gen_buoy_F-mat101.1-0 ; 
       gen_buoy_F-mat102.1-0 ; 
       gen_buoy_F-mat103.1-0 ; 
       gen_buoy_F-mat104.1-0 ; 
       gen_buoy_F-mat105.1-0 ; 
       gen_buoy_F-mat106.1-0 ; 
       gen_buoy_F-mat107.1-0 ; 
       gen_buoy_F-mat108.1-0 ; 
       gen_buoy_F-mat109.1-0 ; 
       gen_buoy_F-mat111.1-0 ; 
       gen_buoy_F-mat112.1-0 ; 
       gen_buoy_F-mat113.1-0 ; 
       gen_buoy_F-mat114.1-0 ; 
       gen_buoy_F-mat115.1-0 ; 
       gen_buoy_F-mat116.1-0 ; 
       gen_buoy_F-mat64.1-0 ; 
       gen_buoy_F-mat65.1-0 ; 
       gen_buoy_F-mat66.1-0 ; 
       gen_buoy_F-mat67.1-0 ; 
       gen_buoy_F-mat68.1-0 ; 
       gen_buoy_F-mat69.1-0 ; 
       gen_buoy_F-mat70.1-0 ; 
       gen_buoy_F-mat71.1-0 ; 
       gen_buoy_F-mat72.1-0 ; 
       gen_buoy_F-mat73.1-0 ; 
       gen_buoy_F-mat74.1-0 ; 
       gen_buoy_F-mat75.1-0 ; 
       gen_buoy_F-mat76.1-0 ; 
       gen_buoy_F-mat77.1-0 ; 
       gen_buoy_F-mat78.1-0 ; 
       gen_buoy_F-mat82.1-0 ; 
       gen_buoy_F-mat83.1-0 ; 
       gen_buoy_F-mat84.1-0 ; 
       gen_buoy_F-mat85.1-0 ; 
       gen_buoy_F-mat86.1-0 ; 
       gen_buoy_F-mat87.1-0 ; 
       gen_buoy_F-mat88.1-0 ; 
       gen_buoy_F-mat89.1-0 ; 
       gen_buoy_F-mat90.1-0 ; 
       gen_buoy_F-mat91.1-0 ; 
       gen_buoy_F-mat92.1-0 ; 
       gen_buoy_F-mat93.1-0 ; 
       gen_buoy_F-mat94.1-0 ; 
       gen_buoy_F-mat95.1-0 ; 
       gen_buoy_F-mat96.1-0 ; 
       gen_buoy_F-mat98.1-0 ; 
       gen_buoy_F-mat99.1-0 ; 
       generic_buoy_sA-mat49.1-0 ; 
       generic_buoy_sA-mat50.1-0 ; 
       generic_buoy_sA-mat51.1-0 ; 
       generic_buoy_sA-mat52.1-0 ; 
       generic_buoy_sA-mat53.1-0 ; 
       generic_buoy_sA-mat54.1-0 ; 
       generic_buoy_sA-mat55.1-0 ; 
       generic_buoy_sA-mat56.1-0 ; 
       generic_buoy_sA-mat57.1-0 ; 
       generic_buoy_sA-mat58.1-0 ; 
       generic_buoy_sA-mat59.1-0 ; 
       generic_buoy_sA-mat60.1-0 ; 
       generic_buoy_sA-mat61.1-0 ; 
       generic_buoy_sA-mat62.1-0 ; 
       generic_buoy_sA-mat63.1-0 ; 
       marker_mod-mat1.1-0 ; 
       marker_mod-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl01-beacon1.1-0 ; 
       utl01-beacon2.1-0 ; 
       utl01-bfuselg.1-0 ; 
       utl01-blfuselg.1-0 ; 
       utl01-lfuselg1.4-0 ; 
       utl01-lfuselg2.2-0 ; 
       utl01-lfuselg3.1-0 ; 
       utl01-lfuselg4.1-0 ; 
       utl01-lslrpan1.1-0 ; 
       utl01-pwrpak.1-0 ; 
       utl01-rfuselg1.1-0 ; 
       utl01-rfuselg2.1-0 ; 
       utl01-rfuselg3.3-0 ; 
       utl01-shield.1-0 ; 
       utl01-solar_panel1.1-0 ; 
       utl01-SSl1.1-0 ; 
       utl01-SSl2.1-0 ; 
       utl01-SSl4.1-0 ; 
       utl01-SSl5.1-0 ; 
       utl01-SSm1.1-0 ; 
       utl01-SSm2.1-0 ; 
       utl01-SSr1.1-0 ; 
       utl01-SSr2.1-0 ; 
       utl01-SSr4.1-0 ; 
       utl01-SSr5.1-0 ; 
       utl01-tlfuselg.1-0 ; 
       utl01-trfuselg.1-0 ; 
       utl01-utl01.1-0 ROOT ; 
       utl01-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/utl01/PICTURES/utl01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01-gen_buoy_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       gen_buoy_F-t2d18.1-0 ; 
       gen_buoy_F-t2d19.1-0 ; 
       gen_buoy_F-t2d20.1-0 ; 
       gen_buoy_F-t2d21.1-0 ; 
       gen_buoy_F-t2d22.1-0 ; 
       gen_buoy_F-t2d23.1-0 ; 
       gen_buoy_F-t2d24.1-0 ; 
       gen_buoy_F-t2d29.1-0 ; 
       gen_buoy_F-t2d30.1-0 ; 
       gen_buoy_F-t2d31.1-0 ; 
       gen_buoy_F-t2d32.1-0 ; 
       gen_buoy_F-t2d33.1-0 ; 
       gen_buoy_F-t2d34.1-0 ; 
       gen_buoy_F-t2d35.1-0 ; 
       gen_buoy_F-t2d36.1-0 ; 
       gen_buoy_F-t2d37.1-0 ; 
       gen_buoy_F-t2d38.1-0 ; 
       gen_buoy_F-t2d39.1-0 ; 
       gen_buoy_F-t2d40.1-0 ; 
       gen_buoy_F-t2d41.1-0 ; 
       gen_buoy_F-t2d42.1-0 ; 
       gen_buoy_F-t2d43.1-0 ; 
       gen_buoy_F-t2d44.1-0 ; 
       gen_buoy_F-t2d46.1-0 ; 
       gen_buoy_F-t2d47.1-0 ; 
       gen_buoy_F-t2d48.1-0 ; 
       gen_buoy_F-t2d49.1-0 ; 
       gen_buoy_F-t2d50.1-0 ; 
       gen_buoy_F-t2d51.1-0 ; 
       gen_buoy_F-t2d52.1-0 ; 
       gen_buoy_F-t2d53.1-0 ; 
       gen_buoy_F-t2d56.1-0 ; 
       gen_buoy_F-t2d57.1-0 ; 
       gen_buoy_F-t2d58.1-0 ; 
       gen_buoy_F-t2d59.1-0 ; 
       gen_buoy_F-t2d60.1-0 ; 
       gen_buoy_F-t2d61.1-0 ; 
       gen_buoy_F-t2d62.1-0 ; 
       gen_buoy_F-t2d63.1-0 ; 
       gen_buoy_F-t2d64.1-0 ; 
       gen_buoy_F-t2d65.1-0 ; 
       generic_buoy_sA-t2d1.1-0 ; 
       generic_buoy_sA-t2d10.1-0 ; 
       generic_buoy_sA-t2d11.1-0 ; 
       generic_buoy_sA-t2d14.1-0 ; 
       generic_buoy_sA-t2d4.1-0 ; 
       generic_buoy_sA-t2d5.1-0 ; 
       generic_buoy_sA-t2d6.1-0 ; 
       generic_buoy_sA-t2d7.1-0 ; 
       generic_buoy_sA-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 8 110 ; 
       16 8 110 ; 
       17 25 110 ; 
       18 3 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 26 110 ; 
       24 2 110 ; 
       0 27 110 ; 
       1 0 110 ; 
       2 10 110 ; 
       3 4 110 ; 
       4 27 110 ; 
       5 27 110 ; 
       6 27 110 ; 
       7 27 110 ; 
       8 5 110 ; 
       9 27 110 ; 
       10 27 110 ; 
       11 27 110 ; 
       12 27 110 ; 
       13 27 110 ; 
       14 11 110 ; 
       25 4 110 ; 
       26 10 110 ; 
       28 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 12 300 ; 
       16 9 300 ; 
       17 13 300 ; 
       18 14 300 ; 
       19 17 300 ; 
       20 18 300 ; 
       21 10 300 ; 
       22 11 300 ; 
       23 15 300 ; 
       24 16 300 ; 
       0 30 300 ; 
       1 66 300 ; 
       1 67 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 31 300 ; 
       2 61 300 ; 
       2 62 300 ; 
       3 59 300 ; 
       3 60 300 ; 
       4 51 300 ; 
       4 53 300 ; 
       5 41 300 ; 
       6 1 300 ; 
       6 49 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       8 63 300 ; 
       8 64 300 ; 
       8 20 300 ; 
       8 23 300 ; 
       8 25 300 ; 
       8 32 300 ; 
       9 19 300 ; 
       9 45 300 ; 
       9 46 300 ; 
       9 47 300 ; 
       9 48 300 ; 
       9 50 300 ; 
       9 3 300 ; 
       10 52 300 ; 
       10 55 300 ; 
       10 56 300 ; 
       11 29 300 ; 
       12 42 300 ; 
       12 43 300 ; 
       12 44 300 ; 
       13 65 300 ; 
       13 34 300 ; 
       13 35 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       13 39 300 ; 
       13 40 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       14 24 300 ; 
       14 33 300 ; 
       25 54 300 ; 
       25 57 300 ; 
       25 58 300 ; 
       26 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 10 400 ; 
       1 11 400 ; 
       2 8 400 ; 
       3 9 400 ; 
       4 41 400 ; 
       5 22 400 ; 
       6 31 400 ; 
       7 37 400 ; 
       10 45 400 ; 
       11 29 400 ; 
       25 14 400 ; 
       26 7 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       27 8 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 34 401 ; 
       4 35 401 ; 
       5 36 401 ; 
       6 38 401 ; 
       7 39 401 ; 
       8 40 401 ; 
       22 0 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 4 401 ; 
       27 5 401 ; 
       28 6 401 ; 
       32 12 401 ; 
       33 13 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 19 401 ; 
       39 20 401 ; 
       40 21 401 ; 
       42 23 401 ; 
       43 24 401 ; 
       44 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       53 48 401 ; 
       55 46 401 ; 
       56 47 401 ; 
       57 49 401 ; 
       58 42 401 ; 
       60 43 401 ; 
       64 44 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 5 -6 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 25 -6 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 10 -6 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 35 -6 0 MPRFLG 0 ; 
       0 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 35 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 MPRFLG 0 ; 
       4 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       8 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 3.301749 MPRFLG 0 ; 
       28 SCHEM 37.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 -44.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 0 -46.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 0 -48.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 0 -50.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 0 -52.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 0 -54.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 0 -56.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 0 -58.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 39 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 120 1 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
