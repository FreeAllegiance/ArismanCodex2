SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_2.3-0 ; 
       utl01-utl01.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.3-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       marker-inf_light4_3.3-0 ROOT ; 
       stait-light1_2.1-0 ROOT ; 
       stait-light2_2.1-0 ROOT ; 
       turret-inf_light3_5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       generic_buoy_sA-mat49.1-0 ; 
       generic_buoy_sA-mat50.1-0 ; 
       generic_buoy_sA-mat51.1-0 ; 
       generic_buoy_sA-mat52.1-0 ; 
       generic_buoy_sA-mat53.1-0 ; 
       generic_buoy_sA-mat54.1-0 ; 
       generic_buoy_sA-mat55.1-0 ; 
       generic_buoy_sA-mat56.1-0 ; 
       generic_buoy_sA-mat57.1-0 ; 
       generic_buoy_sA-mat58.1-0 ; 
       generic_buoy_sA-mat59.1-0 ; 
       generic_buoy_sA-mat60.1-0 ; 
       generic_buoy_sA-mat61.1-0 ; 
       generic_buoy_sA-mat62.1-0 ; 
       generic_buoy_sA-mat63.1-0 ; 
       marker_mod-mat1.1-0 ; 
       marker_mod-mat2.1-0 ; 
       stait-default3.1-0 ; 
       stait-default5.1-0 ; 
       stait-default6.1-0 ; 
       stait-mat100.1-0 ; 
       stait-mat101.1-0 ; 
       stait-mat102.1-0 ; 
       stait-mat103.1-0 ; 
       stait-mat104.1-0 ; 
       stait-mat105.1-0 ; 
       stait-mat64.1-0 ; 
       stait-mat65.1-0 ; 
       stait-mat66.1-0 ; 
       stait-mat67.1-0 ; 
       stait-mat68.1-0 ; 
       stait-mat69.1-0 ; 
       stait-mat70.1-0 ; 
       stait-mat71.1-0 ; 
       stait-mat72.1-0 ; 
       stait-mat73.1-0 ; 
       stait-mat74.1-0 ; 
       stait-mat75.1-0 ; 
       stait-mat76.1-0 ; 
       stait-mat77.1-0 ; 
       stait-mat78.1-0 ; 
       stait-mat82.1-0 ; 
       stait-mat83.1-0 ; 
       stait-mat84.1-0 ; 
       stait-mat85.1-0 ; 
       stait-mat86.1-0 ; 
       stait-mat87.1-0 ; 
       stait-mat88.1-0 ; 
       stait-mat89.1-0 ; 
       stait-mat90.1-0 ; 
       stait-mat91.1-0 ; 
       stait-mat92.1-0 ; 
       stait-mat93.1-0 ; 
       stait-mat94.1-0 ; 
       stait-mat95.1-0 ; 
       stait-mat96.1-0 ; 
       stait-mat98.1-0 ; 
       stait-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl01-beacon1.1-0 ; 
       utl01-beacon2.1-0 ; 
       utl01-bfuselg.1-0 ; 
       utl01-blfuselg.1-0 ; 
       utl01-lfuselg1.4-0 ; 
       utl01-lfuselg2.2-0 ; 
       utl01-lfuselg3.1-0 ; 
       utl01-lfuselg4.1-0 ; 
       utl01-lslrpan1.1-0 ; 
       utl01-pwrpak.1-0 ; 
       utl01-rfuselg1.1-0 ; 
       utl01-rfuselg2.1-0 ; 
       utl01-rfuselg3.3-0 ; 
       utl01-shield.1-0 ; 
       utl01-solar_panel1.1-0 ; 
       utl01-tlfuselg.1-0 ; 
       utl01-trfuselg.1-0 ; 
       utl01-utl01.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/utl01/PICTURES/utl01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01-stait.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       generic_buoy_sA-t2d1.1-0 ; 
       generic_buoy_sA-t2d10.1-0 ; 
       generic_buoy_sA-t2d11.1-0 ; 
       generic_buoy_sA-t2d14.1-0 ; 
       generic_buoy_sA-t2d4.1-0 ; 
       generic_buoy_sA-t2d5.1-0 ; 
       generic_buoy_sA-t2d6.1-0 ; 
       generic_buoy_sA-t2d7.1-0 ; 
       generic_buoy_sA-t2d9.1-0 ; 
       stait-t2d18.1-0 ; 
       stait-t2d19.1-0 ; 
       stait-t2d20.1-0 ; 
       stait-t2d21.1-0 ; 
       stait-t2d22.1-0 ; 
       stait-t2d23.1-0 ; 
       stait-t2d24.1-0 ; 
       stait-t2d29.1-0 ; 
       stait-t2d30.1-0 ; 
       stait-t2d31.1-0 ; 
       stait-t2d32.1-0 ; 
       stait-t2d33.1-0 ; 
       stait-t2d34.1-0 ; 
       stait-t2d35.1-0 ; 
       stait-t2d36.1-0 ; 
       stait-t2d37.1-0 ; 
       stait-t2d38.1-0 ; 
       stait-t2d39.1-0 ; 
       stait-t2d40.1-0 ; 
       stait-t2d41.1-0 ; 
       stait-t2d42.1-0 ; 
       stait-t2d43.1-0 ; 
       stait-t2d44.1-0 ; 
       stait-t2d46.1-0 ; 
       stait-t2d47.1-0 ; 
       stait-t2d48.1-0 ; 
       stait-t2d49.1-0 ; 
       stait-t2d50.1-0 ; 
       stait-t2d51.1-0 ; 
       stait-t2d52.1-0 ; 
       stait-t2d53.1-0 ; 
       stait-t2d56.1-0 ; 
       stait-t2d57.1-0 ; 
       stait-t2d58.1-0 ; 
       stait-t2d59.1-0 ; 
       stait-t2d60.1-0 ; 
       stait-t2d61.1-0 ; 
       stait-t2d62.1-0 ; 
       stait-t2d63.1-0 ; 
       stait-t2d64.1-0 ; 
       stait-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 0 110 ; 
       2 10 110 ; 
       3 4 110 ; 
       4 17 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 5 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 11 110 ; 
       15 4 110 ; 
       16 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 37 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       1 35 300 ; 
       1 38 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       5 48 300 ; 
       6 18 300 ; 
       6 56 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       7 19 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 27 300 ; 
       8 30 300 ; 
       8 32 300 ; 
       8 39 300 ; 
       9 26 300 ; 
       9 52 300 ; 
       9 53 300 ; 
       9 54 300 ; 
       9 55 300 ; 
       9 57 300 ; 
       9 20 300 ; 
       10 1 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       11 36 300 ; 
       12 49 300 ; 
       12 50 300 ; 
       12 51 300 ; 
       13 14 300 ; 
       13 41 300 ; 
       13 42 300 ; 
       13 43 300 ; 
       13 44 300 ; 
       13 45 300 ; 
       13 46 300 ; 
       13 47 300 ; 
       14 28 300 ; 
       14 29 300 ; 
       14 31 300 ; 
       14 40 300 ; 
       15 3 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       16 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 19 400 ; 
       1 20 400 ; 
       2 17 400 ; 
       3 18 400 ; 
       4 0 400 ; 
       5 31 400 ; 
       6 40 400 ; 
       7 46 400 ; 
       10 4 400 ; 
       11 38 400 ; 
       15 23 400 ; 
       16 16 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       17 8 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 43 401 ; 
       21 44 401 ; 
       22 45 401 ; 
       23 47 401 ; 
       24 48 401 ; 
       25 49 401 ; 
       29 9 401 ; 
       30 10 401 ; 
       31 11 401 ; 
       32 12 401 ; 
       33 13 401 ; 
       34 14 401 ; 
       35 15 401 ; 
       39 21 401 ; 
       40 22 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       51 34 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       55 39 401 ; 
       56 41 401 ; 
       57 42 401 ; 
       2 7 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 8 401 ; 
       7 1 401 ; 
       9 2 401 ; 
       13 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       10 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 5 -2 0 MPRFLG 0 ; 
       12 SCHEM 30 -2 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 5 -4 0 MPRFLG 0 ; 
       15 SCHEM 20 -4 0 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 MPRFLG 0 ; 
       17 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 3.301749 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       17 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 -44.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 0 -46.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 0 -48.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 0 -50.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 0 -52.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 0 -54.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 0 -56.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 0 -58.5 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 120 1 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
