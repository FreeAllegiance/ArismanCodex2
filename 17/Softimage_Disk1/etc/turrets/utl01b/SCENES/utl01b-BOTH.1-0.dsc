SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.3-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       BOTH-mat1.1-0 ; 
       BOTH-mat10.1-0 ; 
       BOTH-mat11.1-0 ; 
       BOTH-mat12.1-0 ; 
       BOTH-mat13.1-0 ; 
       BOTH-mat14.1-0 ; 
       BOTH-mat15.1-0 ; 
       BOTH-mat16.1-0 ; 
       BOTH-mat17.1-0 ; 
       BOTH-mat18.1-0 ; 
       BOTH-mat19.1-0 ; 
       BOTH-mat2.1-0 ; 
       BOTH-mat20.1-0 ; 
       BOTH-mat21.1-0 ; 
       BOTH-mat22.1-0 ; 
       BOTH-mat23.1-0 ; 
       BOTH-mat3.1-0 ; 
       BOTH-mat4.1-0 ; 
       BOTH-mat5.1-0 ; 
       BOTH-mat6.1-0 ; 
       BOTH-mat7.1-0 ; 
       BOTH-mat8.1-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       BOTH-cube1.3-0 ; 
       BOTH-cube2.2-0 ; 
       BOTH-cube4.1-0 ; 
       BOTH-cube6.1-0 ; 
       BOTH-cyl2.1-0 ; 
       BOTH-cyl3.1-0 ; 
       BOTH-null1.1-0 ROOT ; 
       BOTH-range_finder.1-0 ; 
       BOTH-ss01.1-0 ; 
       BOTH-ss02.1-0 ; 
       BOTH-ss03.1-0 ; 
       BOTH-wepemt.1-0 ; 
       STATIC-cube1.3-0 ; 
       STATIC-cube2.2-0 ; 
       STATIC-cube4.1-0 ; 
       STATIC-cube6.1-0 ; 
       STATIC-cyl2.1-0 ; 
       STATIC-cyl3.1-0 ; 
       STATIC-null1.2-0 ROOT ; 
       STATIC-range_finder.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/etc/turrets/utl01b/PICTURES/utl01b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01b-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       BOTH-t2d1.1-0 ; 
       BOTH-t2d10.1-0 ; 
       BOTH-t2d11.1-0 ; 
       BOTH-t2d12.1-0 ; 
       BOTH-t2d13.1-0 ; 
       BOTH-t2d14.1-0 ; 
       BOTH-t2d15.1-0 ; 
       BOTH-t2d16.1-0 ; 
       BOTH-t2d17.1-0 ; 
       BOTH-t2d18.1-0 ; 
       BOTH-t2d19.1-0 ; 
       BOTH-t2d2.1-0 ; 
       BOTH-t2d20.1-0 ; 
       BOTH-t2d21.1-0 ; 
       BOTH-t2d22.1-0 ; 
       BOTH-t2d3.1-0 ; 
       BOTH-t2d4.1-0 ; 
       BOTH-t2d5.1-0 ; 
       BOTH-t2d6.1-0 ; 
       BOTH-t2d7.1-0 ; 
       BOTH-t2d8.1-0 ; 
       BOTH-t2d9.1-0 ; 
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       7 0 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 17 110 ; 
       13 18 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 13 110 ; 
       19 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       1 20 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 21 300 ; 
       3 8 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       5 19 300 ; 
       7 0 300 ; 
       7 11 300 ; 
       12 40 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 26 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       13 42 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       13 34 300 ; 
       13 35 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       14 43 300 ; 
       15 30 300 ; 
       16 38 300 ; 
       16 39 300 ; 
       17 41 300 ; 
       19 22 300 ; 
       19 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 16 401 ; 
       1 17 401 ; 
       2 18 401 ; 
       3 19 401 ; 
       4 20 401 ; 
       5 21 401 ; 
       6 1 401 ; 
       7 3 401 ; 
       8 5 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 0 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 15 401 ; 
       17 11 401 ; 
       18 2 401 ; 
       19 6 401 ; 
       20 14 401 ; 
       21 4 401 ; 
       22 38 401 ; 
       23 39 401 ; 
       24 40 401 ; 
       25 41 401 ; 
       26 42 401 ; 
       27 43 401 ; 
       28 23 401 ; 
       29 25 401 ; 
       30 27 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 22 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 37 401 ; 
       39 33 401 ; 
       40 24 401 ; 
       41 28 401 ; 
       42 36 401 ; 
       43 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0.4724897 3.088477 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 30 -8 0 MPRFLG 0 ; 
       16 SCHEM 25 -8 0 MPRFLG 0 ; 
       17 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0.4724897 3.088477 MPRFLG 0 ; 
       19 SCHEM 22.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
