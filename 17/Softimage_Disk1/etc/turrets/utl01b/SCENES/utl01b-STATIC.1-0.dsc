SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.1-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       STATIC-cube1.3-0 ; 
       STATIC-cube2.2-0 ; 
       STATIC-cube4.1-0 ; 
       STATIC-cube6.1-0 ; 
       STATIC-cyl2.1-0 ; 
       STATIC-cyl3.1-0 ; 
       STATIC-null1.1-0 ROOT ; 
       STATIC-range_finder.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/utl01b/PICTURES/utl01b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01b-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       7 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       1 20 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 21 300 ; 
       3 8 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       5 19 300 ; 
       7 0 300 ; 
       7 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 16 401 ; 
       1 17 401 ; 
       2 18 401 ; 
       3 19 401 ; 
       4 20 401 ; 
       5 21 401 ; 
       6 1 401 ; 
       7 3 401 ; 
       8 5 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 0 401 ; 
       16 15 401 ; 
       17 11 401 ; 
       18 2 401 ; 
       19 6 401 ; 
       20 14 401 ; 
       21 4 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 MPRFLG 0 ; 
       6 SCHEM 33.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
