SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.10-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       model-cube1.3-0 ; 
       model-cube2.2-0 ; 
       model-cube4.1-0 ROOT ; 
       model-cyl2.1-0 ; 
       model-cyl2_1.1-0 ; 
       model-cyl3.1-0 ; 
       model-null1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01b-model.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       4 0 110 ; 
       3 0 110 ; 
       5 1 110 ; 
       1 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -2.536773 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
