SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.9-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       model-cube1.3-0 ; 
       model-cube2.2-0 ; 
       model-cyl2.1-0 ; 
       model-cyl2_1.1-0 ; 
       model-cyl3.1-0 ; 
       model-null1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl01b-model.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       3 0 110 ; 
       2 0 110 ; 
       4 1 110 ; 
       1 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
