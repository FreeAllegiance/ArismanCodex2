SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl02-utl02_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.5-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       BOTH-mat1.1-0 ; 
       BOTH-mat10.1-0 ; 
       BOTH-mat11.1-0 ; 
       BOTH-mat12.1-0 ; 
       BOTH-mat13.1-0 ; 
       BOTH-mat14.1-0 ; 
       BOTH-mat15.1-0 ; 
       BOTH-mat16.1-0 ; 
       BOTH-mat17.1-0 ; 
       BOTH-mat18.1-0 ; 
       BOTH-mat19.1-0 ; 
       BOTH-mat2.1-0 ; 
       BOTH-mat20.1-0 ; 
       BOTH-mat21.1-0 ; 
       BOTH-mat22.1-0 ; 
       BOTH-mat23.1-0 ; 
       BOTH-mat24.1-0 ; 
       BOTH-mat25.1-0 ; 
       BOTH-mat26.1-0 ; 
       BOTH-mat27.1-0 ; 
       BOTH-mat28.1-0 ; 
       BOTH-mat29.1-0 ; 
       BOTH-mat3.1-0 ; 
       BOTH-mat30.1-0 ; 
       BOTH-mat31.1-0 ; 
       BOTH-mat32.1-0 ; 
       BOTH-mat33.1-0 ; 
       BOTH-mat34.1-0 ; 
       BOTH-mat35.1-0 ; 
       BOTH-mat36.1-0 ; 
       BOTH-mat37.1-0 ; 
       BOTH-mat4.1-0 ; 
       BOTH-mat5.1-0 ; 
       BOTH-mat6.1-0 ; 
       BOTH-mat7.1-0 ; 
       BOTH-mat8.1-0 ; 
       BOTH-mat9.1-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat24.1-0 ; 
       STATIC-mat25.1-0 ; 
       STATIC-mat26.1-0 ; 
       STATIC-mat27.1-0 ; 
       STATIC-mat28.1-0 ; 
       STATIC-mat29.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat36.1-0 ; 
       STATIC-mat37.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       utl02-bfusegl3.1-0 ; 
       utl02-bfusegl3_1.1-0 ; 
       utl02-bfuselg1.1-0 ; 
       utl02-bfuselg1_1.1-0 ; 
       utl02-bfuselg2.1-0 ; 
       utl02-bfuselg2_1.1-0 ; 
       utl02-bfuselg4.1-0 ; 
       utl02-bfuselg4_1.1-0 ; 
       utl02-cube1.1-0 ; 
       utl02-cube1_1.1-0 ; 
       utl02-fuselg.1-0 ; 
       utl02-fuselg_1.1-0 ; 
       utl02-lfuselg.1-0 ; 
       utl02-lfuselg_1.1-0 ; 
       utl02-mfuselg.1-0 ; 
       utl02-mfuselg_1.1-0 ; 
       utl02-rfuselg.1-0 ; 
       utl02-rfuselg_1.1-0 ; 
       utl02-SSa.1-0 ; 
       utl02-SSal.1-0 ; 
       utl02-SSar.1-0 ; 
       utl02-SSb.1-0 ; 
       utl02-SSft.1-0 ; 
       utl02-SSt.1-0 ; 
       utl02-tetra1.1-0 ; 
       utl02-tetra1_1.1-0 ; 
       utl02-tfuselg.1-0 ; 
       utl02-tfuselg_1.1-0 ; 
       utl02-utl02.3-0 ROOT ; 
       utl02-utl02_1.3-0 ROOT ; 
       utl02-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/etc/turrets/utl02/PICTURES/utl02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl02-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 62     
       BOTH-t2d1.1-0 ; 
       BOTH-t2d10.1-0 ; 
       BOTH-t2d11.1-0 ; 
       BOTH-t2d12.1-0 ; 
       BOTH-t2d13.1-0 ; 
       BOTH-t2d14.1-0 ; 
       BOTH-t2d15.1-0 ; 
       BOTH-t2d16.1-0 ; 
       BOTH-t2d17.1-0 ; 
       BOTH-t2d18.1-0 ; 
       BOTH-t2d19.1-0 ; 
       BOTH-t2d2.1-0 ; 
       BOTH-t2d20.1-0 ; 
       BOTH-t2d21.1-0 ; 
       BOTH-t2d22.1-0 ; 
       BOTH-t2d23.1-0 ; 
       BOTH-t2d24.1-0 ; 
       BOTH-t2d25.1-0 ; 
       BOTH-t2d26.1-0 ; 
       BOTH-t2d27.1-0 ; 
       BOTH-t2d28.1-0 ; 
       BOTH-t2d29.1-0 ; 
       BOTH-t2d3.1-0 ; 
       BOTH-t2d30.1-0 ; 
       BOTH-t2d31.1-0 ; 
       BOTH-t2d4.1-0 ; 
       BOTH-t2d5.1-0 ; 
       BOTH-t2d6.1-0 ; 
       BOTH-t2d7.1-0 ; 
       BOTH-t2d8.1-0 ; 
       BOTH-t2d9.1-0 ; 
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d23.1-0 ; 
       STATIC-t2d24.1-0 ; 
       STATIC-t2d25.1-0 ; 
       STATIC-t2d26.1-0 ; 
       STATIC-t2d27.1-0 ; 
       STATIC-t2d28.1-0 ; 
       STATIC-t2d29.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d30.1-0 ; 
       STATIC-t2d31.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 10 110 ; 
       4 10 110 ; 
       6 10 110 ; 
       8 29 110 ; 
       10 29 110 ; 
       12 14 110 ; 
       14 29 110 ; 
       16 14 110 ; 
       18 14 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 0 110 ; 
       22 26 110 ; 
       23 10 110 ; 
       24 8 110 ; 
       26 14 110 ; 
       30 29 110 ; 
       1 5 110 ; 
       3 11 110 ; 
       5 11 110 ; 
       7 11 110 ; 
       9 28 110 ; 
       11 28 110 ; 
       13 15 110 ; 
       15 28 110 ; 
       17 15 110 ; 
       25 9 110 ; 
       27 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 32 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 19 300 ; 
       4 3 300 ; 
       4 18 300 ; 
       6 6 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       8 29 300 ; 
       10 22 300 ; 
       10 31 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       12 35 300 ; 
       16 36 300 ; 
       16 1 300 ; 
       16 2 300 ; 
       18 27 300 ; 
       19 28 300 ; 
       20 24 300 ; 
       21 23 300 ; 
       22 26 300 ; 
       23 25 300 ; 
       24 30 300 ; 
       26 0 300 ; 
       26 11 300 ; 
       26 20 300 ; 
       26 21 300 ; 
       1 63 300 ; 
       3 41 300 ; 
       3 42 300 ; 
       3 53 300 ; 
       3 54 300 ; 
       3 56 300 ; 
       5 40 300 ; 
       5 55 300 ; 
       7 43 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       9 60 300 ; 
       11 59 300 ; 
       11 62 300 ; 
       11 44 300 ; 
       11 45 300 ; 
       11 46 300 ; 
       11 47 300 ; 
       11 49 300 ; 
       11 50 300 ; 
       13 64 300 ; 
       13 65 300 ; 
       13 66 300 ; 
       17 67 300 ; 
       17 38 300 ; 
       17 39 300 ; 
       25 61 300 ; 
       27 37 300 ; 
       27 48 300 ; 
       27 57 300 ; 
       27 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 30 401 ; 
       2 1 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 2 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 29 401 ; 
       37 31 401 ; 
       38 61 401 ; 
       39 32 401 ; 
       40 34 401 ; 
       41 35 401 ; 
       42 36 401 ; 
       43 37 401 ; 
       44 38 401 ; 
       45 39 401 ; 
       46 40 401 ; 
       47 41 401 ; 
       48 42 401 ; 
       49 43 401 ; 
       50 44 401 ; 
       51 45 401 ; 
       52 46 401 ; 
       53 47 401 ; 
       54 48 401 ; 
       55 49 401 ; 
       56 50 401 ; 
       57 51 401 ; 
       58 52 401 ; 
       59 53 401 ; 
       60 54 401 ; 
       61 55 401 ; 
       62 56 401 ; 
       63 33 401 ; 
       64 57 401 ; 
       65 58 401 ; 
       66 59 401 ; 
       67 60 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 MPRFLG 0 ; 
       8 SCHEM 0 -4 0 MPRFLG 0 ; 
       10 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 0 -6 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 13.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42 -8 0 MPRFLG 0 ; 
       3 SCHEM 39.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 42 -6 0 MPRFLG 0 ; 
       7 SCHEM 44.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 29.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 42 -4 0 MPRFLG 0 ; 
       13 SCHEM 32 -6 0 MPRFLG 0 ; 
       15 SCHEM 34.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 37 -6 0 MPRFLG 0 ; 
       25 SCHEM 29.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 34.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 37 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 28.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 41 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 41 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 43.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 38.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 38.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 43.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 43.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 43.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 38.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 38.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 43.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 38.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 31 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 28.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
