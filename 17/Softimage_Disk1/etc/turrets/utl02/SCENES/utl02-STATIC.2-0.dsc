SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       rix_sentry_sPa-inf_light1.2-0 ROOT ; 
       rix_sentry_sPa-inf_light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat24.1-0 ; 
       STATIC-mat25.1-0 ; 
       STATIC-mat26.1-0 ; 
       STATIC-mat27.1-0 ; 
       STATIC-mat28.1-0 ; 
       STATIC-mat29.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat36.1-0 ; 
       STATIC-mat37.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       utl02-bfusegl3.1-0 ; 
       utl02-bfuselg1.1-0 ; 
       utl02-bfuselg2.1-0 ; 
       utl02-bfuselg4.1-0 ; 
       utl02-cube1.1-0 ; 
       utl02-fuselg.1-0 ; 
       utl02-lfuselg.1-0 ; 
       utl02-mfuselg.1-0 ; 
       utl02-rfuselg.1-0 ; 
       utl02-tetra1.1-0 ; 
       utl02-tfuselg.1-0 ; 
       utl02-utl02.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/utl02/PICTURES/utl02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl02-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d23.1-0 ; 
       STATIC-t2d24.1-0 ; 
       STATIC-t2d25.1-0 ; 
       STATIC-t2d26.1-0 ; 
       STATIC-t2d27.1-0 ; 
       STATIC-t2d28.1-0 ; 
       STATIC-t2d29.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d30.1-0 ; 
       STATIC-t2d31.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 7 110 ; 
       7 11 110 ; 
       8 7 110 ; 
       9 4 110 ; 
       10 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 26 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 19 300 ; 
       2 3 300 ; 
       2 18 300 ; 
       3 6 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       4 23 300 ; 
       5 22 300 ; 
       5 25 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       8 30 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       9 24 300 ; 
       10 0 300 ; 
       10 11 300 ; 
       10 20 300 ; 
       10 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 30 401 ; 
       2 1 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 2 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 0 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
