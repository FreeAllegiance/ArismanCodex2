SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl02-utl02_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       rix_sentry_sPa-inf_light1_1.1-0 ROOT ; 
       rix_sentry_sPa-inf_light2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       add_frames-mat1.1-0 ; 
       add_frames-mat10.1-0 ; 
       add_frames-mat11.1-0 ; 
       add_frames-mat12.1-0 ; 
       add_frames-mat13.1-0 ; 
       add_frames-mat14.1-0 ; 
       add_frames-mat15.1-0 ; 
       add_frames-mat16.1-0 ; 
       add_frames-mat17.1-0 ; 
       add_frames-mat18.1-0 ; 
       add_frames-mat19.1-0 ; 
       add_frames-mat2.1-0 ; 
       add_frames-mat20.1-0 ; 
       add_frames-mat21.1-0 ; 
       add_frames-mat22.1-0 ; 
       add_frames-mat23.1-0 ; 
       add_frames-mat24.1-0 ; 
       add_frames-mat25.1-0 ; 
       add_frames-mat26.1-0 ; 
       add_frames-mat27.1-0 ; 
       add_frames-mat28.1-0 ; 
       add_frames-mat29.1-0 ; 
       add_frames-mat3.1-0 ; 
       add_frames-mat30.1-0 ; 
       add_frames-mat31.1-0 ; 
       add_frames-mat32.1-0 ; 
       add_frames-mat33.1-0 ; 
       add_frames-mat34.1-0 ; 
       add_frames-mat35.1-0 ; 
       add_frames-mat36.1-0 ; 
       add_frames-mat37.1-0 ; 
       add_frames-mat4.1-0 ; 
       add_frames-mat5.1-0 ; 
       add_frames-mat6.1-0 ; 
       add_frames-mat7.1-0 ; 
       add_frames-mat8.1-0 ; 
       add_frames-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       utl02-bfusegl3.1-0 ; 
       utl02-bfuselg1.1-0 ; 
       utl02-bfuselg2.1-0 ; 
       utl02-bfuselg4.1-0 ; 
       utl02-cube1.1-0 ; 
       utl02-fuselg.1-0 ; 
       utl02-lfuselg.1-0 ; 
       utl02-mfuselg.1-0 ; 
       utl02-rfuselg.1-0 ; 
       utl02-SSa.1-0 ; 
       utl02-SSal.1-0 ; 
       utl02-SSar.1-0 ; 
       utl02-SSb.1-0 ; 
       utl02-SSft.1-0 ; 
       utl02-SSt.1-0 ; 
       utl02-tetra1.1-0 ; 
       utl02-tfuselg.1-0 ; 
       utl02-utl02_1.1-0 ROOT ; 
       utl02-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/turrets/utl02/PICTURES/utl02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl02-add_frames.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_frames-t2d1.1-0 ; 
       add_frames-t2d10.1-0 ; 
       add_frames-t2d11.1-0 ; 
       add_frames-t2d12.1-0 ; 
       add_frames-t2d13.1-0 ; 
       add_frames-t2d14.1-0 ; 
       add_frames-t2d15.1-0 ; 
       add_frames-t2d16.1-0 ; 
       add_frames-t2d17.1-0 ; 
       add_frames-t2d18.1-0 ; 
       add_frames-t2d19.1-0 ; 
       add_frames-t2d2.1-0 ; 
       add_frames-t2d20.1-0 ; 
       add_frames-t2d21.1-0 ; 
       add_frames-t2d22.1-0 ; 
       add_frames-t2d23.1-0 ; 
       add_frames-t2d24.1-0 ; 
       add_frames-t2d25.1-0 ; 
       add_frames-t2d26.1-0 ; 
       add_frames-t2d27.1-0 ; 
       add_frames-t2d28.1-0 ; 
       add_frames-t2d29.1-0 ; 
       add_frames-t2d3.1-0 ; 
       add_frames-t2d30.1-0 ; 
       add_frames-t2d31.1-0 ; 
       add_frames-t2d4.1-0 ; 
       add_frames-t2d5.1-0 ; 
       add_frames-t2d6.1-0 ; 
       add_frames-t2d7.1-0 ; 
       add_frames-t2d8.1-0 ; 
       add_frames-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 17 110 ; 
       5 17 110 ; 
       6 7 110 ; 
       7 17 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 0 110 ; 
       13 16 110 ; 
       14 5 110 ; 
       15 4 110 ; 
       16 7 110 ; 
       18 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 32 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 19 300 ; 
       2 3 300 ; 
       2 18 300 ; 
       3 6 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       4 29 300 ; 
       5 22 300 ; 
       5 31 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       8 36 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       9 27 300 ; 
       10 28 300 ; 
       11 24 300 ; 
       12 23 300 ; 
       13 26 300 ; 
       14 25 300 ; 
       15 30 300 ; 
       16 0 300 ; 
       16 11 300 ; 
       16 20 300 ; 
       16 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       17 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 30 401 ; 
       2 1 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 2 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 MPRFLG 0 ; 
       4 SCHEM 0 -4 0 MPRFLG 0 ; 
       5 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 0 -6 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -2 0 SRT 1 1 1 0 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
