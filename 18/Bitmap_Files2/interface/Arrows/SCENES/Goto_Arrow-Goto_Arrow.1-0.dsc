SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Goto_Arrow-cam_int1.1-0 ROOT ; 
       Goto_Arrow-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       Goto_Arrow-basech1.1-0 ; 
       Goto_Arrow-basech2.1-0 ; 
       Goto_Arrow-basech3.1-0 ; 
       Goto_Arrow-basech4.1-0 ; 
       Goto_Arrow-chr_1.1-0 ; 
       Goto_Arrow-chr_2.1-0 ; 
       Goto_Arrow-chr_3.1-0 ; 
       Goto_Arrow-chr_4.1-0 ; 
       Goto_Arrow-spl_1.1-0 ; 
       Goto_Arrow-str_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Goto_Arrow-Goto_Arrow.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 9 110 ; 
       0 9 110 ; 
       4 0 110 ; 
       1 9 110 ; 
       5 1 110 ; 
       2 9 110 ; 
       6 2 110 ; 
       3 9 110 ; 
       7 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
