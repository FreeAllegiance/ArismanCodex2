SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Goto_Arrow-cam_int1.2-0 ROOT ; 
       Goto_Arrow-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       Goto_Arrow-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       Goto_Arrow-basech1.1-0 ; 
       Goto_Arrow-basech2.1-0 ; 
       Goto_Arrow-basech3.1-0 ; 
       Goto_Arrow-basech4.1-0 ; 
       Goto_Arrow-chr_1.1-0 ; 
       Goto_Arrow-chr_2.1-0 ; 
       Goto_Arrow-chr_3.1-0 ; 
       Goto_Arrow-chr_4.1-0 ; 
       Goto_Arrow-extru1.1-0 ; 
       Goto_Arrow-extru2.1-0 ; 
       Goto_Arrow-extru3.1-0 ; 
       Goto_Arrow-extru4.1-0 ; 
       Goto_Arrow-extru5.1-0 ; 
       Goto_Arrow-face1.1-0 ; 
       Goto_Arrow-null1.1-0 ROOT ; 
       Goto_Arrow-str_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Goto_Arrow-Goto_Arrow.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 14 110 ; 
       13 15 110 ; 
       0 15 110 ; 
       4 0 110 ; 
       1 15 110 ; 
       5 1 110 ; 
       2 15 110 ; 
       6 2 110 ; 
       3 15 110 ; 
       7 3 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 15 -2 0 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 MPRFLG 0 ; 
       14 SCHEM 21.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
