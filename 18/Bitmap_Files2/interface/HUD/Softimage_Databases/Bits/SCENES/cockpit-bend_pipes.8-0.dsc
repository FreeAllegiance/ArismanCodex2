SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bend_pipes-obj1.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       bend_pipes-cam_int1.8-0 ROOT ; 
       bend_pipes-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       bend_pipes-mat10.1-0 ; 
       bend_pipes-mat11.1-0 ; 
       bend_pipes-mat12.1-0 ; 
       bend_pipes-mat13.1-0 ; 
       bend_pipes-mat14.1-0 ; 
       bend_pipes-mat15.1-0 ; 
       bend_pipes-mat16.1-0 ; 
       bend_pipes-mat6.1-0 ; 
       bend_pipes-mat7.1-0 ; 
       bend_pipes-mat8.1-0 ; 
       bend_pipes-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       bend_pipes-bmerge3.1-0 ; 
       bend_pipes-cube1.1-0 ; 
       bend_pipes-cube2.1-0 ; 
       bend_pipes-cube4.2-0 ; 
       bend_pipes-obj1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       G:/HUD/Softimage_Databases/Bits/PICTURES/pit_fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cockpit-bend_pipes.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       bend_pipes-t2d10.2-0 ; 
       bend_pipes-t2d11.2-0 ; 
       bend_pipes-t2d12.2-0 ; 
       bend_pipes-t2d13.1-0 ; 
       bend_pipes-t2d14.1-0 ; 
       bend_pipes-t2d15.2-0 ; 
       bend_pipes-t2d6.2-0 ; 
       bend_pipes-t2d7.2-0 ; 
       bend_pipes-t2d8.2-0 ; 
       bend_pipes-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       0 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       2 5 300 ; 
       3 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 3 401 ; 
       0 9 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
