SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       retext-obj1.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       retext-cam_int1.7-0 ROOT ; 
       retext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       retext-mat1.1-0 ; 
       retext-mat10.1-0 ; 
       retext-mat11.1-0 ; 
       retext-mat12.1-0 ; 
       retext-mat13.1-0 ; 
       retext-mat14.1-0 ; 
       retext-mat2.1-0 ; 
       retext-mat3.1-0 ; 
       retext-mat4.1-0 ; 
       retext-mat5.1-0 ; 
       retext-mat6.1-0 ; 
       retext-mat7.1-0 ; 
       retext-mat8.1-0 ; 
       retext-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       retext-bmerge3.1-0 ; 
       retext-crossbar.1-0 ; 
       retext-lbar.1-0 ; 
       retext-obj1.7-0 ROOT ; 
       retext-rbar.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       G:/HUD/Softimage_Databases/Bits/PICTURES/pit_fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cockpit-retext.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       retext-t2d1.1-0 ; 
       retext-t2d10.7-0 ; 
       retext-t2d11.7-0 ; 
       retext-t2d12.7-0 ; 
       retext-t2d13.7-0 ; 
       retext-t2d2.1-0 ; 
       retext-t2d3.1-0 ; 
       retext-t2d4.1-0 ; 
       retext-t2d5.1-0 ; 
       retext-t2d6.7-0 ; 
       retext-t2d7.7-0 ; 
       retext-t2d8.7-0 ; 
       retext-t2d9.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 9 300 ; 
       4 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 12 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
