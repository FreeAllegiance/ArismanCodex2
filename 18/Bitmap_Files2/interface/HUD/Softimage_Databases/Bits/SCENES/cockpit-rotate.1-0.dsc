SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rotate-cam_int1.1-0 ROOT ; 
       rotate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       rotate-mat10.1-0 ; 
       rotate-mat11.1-0 ; 
       rotate-mat12.1-0 ; 
       rotate-mat13.1-0 ; 
       rotate-mat14.1-0 ; 
       rotate-mat15.1-0 ; 
       rotate-mat16.1-0 ; 
       rotate-mat6.1-0 ; 
       rotate-mat7.1-0 ; 
       rotate-mat8.1-0 ; 
       rotate-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       rotate-bmerge3.1-0 ; 
       rotate-cube1.1-0 ; 
       rotate-cube2.1-0 ; 
       rotate-cube4.2-0 ; 
       rotate-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       G:/HUD/Softimage_Databases/Bits/PICTURES/fig02pit ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cockpit-rotate.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       rotate-t2d10.1-0 ; 
       rotate-t2d11.1-0 ; 
       rotate-t2d12.1-0 ; 
       rotate-t2d13.1-0 ; 
       rotate-t2d14.1-0 ; 
       rotate-t2d15.1-0 ; 
       rotate-t2d6.1-0 ; 
       rotate-t2d7.1-0 ; 
       rotate-t2d8.1-0 ; 
       rotate-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       2 5 300 ; 
       3 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
