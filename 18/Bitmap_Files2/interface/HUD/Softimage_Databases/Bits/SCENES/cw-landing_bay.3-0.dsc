SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.3-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 13     
       landing_bay-light1.3-0 ROOT ; 
       landing_bay-light3.3-0 ROOT ; 
       landing_bay-light4.3-0 ROOT ; 
       landing_bay-spot1.1-0 ; 
       landing_bay-spot1_int.3-0 ROOT ; 
       landing_bay-spot2.1-0 ; 
       landing_bay-spot2_int.3-0 ROOT ; 
       landing_bay-spot3.1-0 ; 
       landing_bay-spot3_int.3-0 ROOT ; 
       landing_bay-spot4.1-0 ; 
       landing_bay-spot4_int.3-0 ROOT ; 
       landing_bay-spot5.1-0 ; 
       landing_bay-spot5_int.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       landing_bay-default6.1-0 ; 
       landing_bay-mat1.2-0 ; 
       landing_bay-mat2.1-0 ; 
       landing_bay-mat3.1-0 ; 
       landing_bay-mat4.1-0 ; 
       landing_bay-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       landing_bay-grid1.2-0 ROOT ; 
       landing_bay-grid2.1-0 ROOT ; 
       landing_bay-grid3.1-0 ROOT ; 
       landing_bay1-grid1.1-0 ROOT ; 
       landing_bay2-grid2.1-0 ROOT ; 
       redo4-nurbs6_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       G:/HUD/Softimage_Databases/Bits/PICTURES/Grid512c ; 
       G:/HUD/Softimage_Databases/Bits/PICTURES/cwlogo ; 
       G:/HUD/Softimage_Databases/Bits/PICTURES/ground ; 
       G:/HUD/Softimage_Databases/Bits/PICTURES/wall ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cw-landing_bay.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       landing_bay-t2d1.1-0 ; 
       landing_bay-t2d2.1-0 ; 
       landing_bay-t2d3.1-0 ; 
       landing_bay-t2d4.1-0 ; 
       landing_bay-t2d5.1-0 ; 
       landing_bay-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       3 3 300 ; 
       2 4 300 ; 
       4 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       0 2 400 ; 
       1 0 400 ; 
       3 3 400 ; 
       2 4 400 ; 
       4 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
       5 6 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       11 12 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 28.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 28.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 31 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 31 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 33.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 33.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 36 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 36 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 38.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 38.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 41 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 46 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 43.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 6 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 11 0 0 SRT 3.2721 1.31 5.599999 0 0 0 0.8604943 -8.67362 5.335064 MPRFLG 0 ; 
       1 SCHEM 17.25 0 0 SRT 1.262 1 1.4796 1.570796 0 0 0.5782413 -0.7709885 33.90347 MPRFLG 0 ; 
       3 SCHEM 23.5 0 0 SRT 3.2721 1.31 5.599999 0 0 0 0.8604943 7.03527 5.335064 MPRFLG 0 ; 
       2 SCHEM 48.5 0 0 SRT 1.60393 1 5.805877 0 0 1.570796 17.35425 -0.7993335 5.96865 MPRFLG 0 ; 
       4 SCHEM 56 0 0 SRT 3.341775 1 1.4796 1.570796 0 0 0.5782413 -0.7709885 34.41977 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 8.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 51 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 18.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 23.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 53.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 63.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
