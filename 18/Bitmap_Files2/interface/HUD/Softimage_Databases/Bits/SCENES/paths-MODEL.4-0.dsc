SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       MODEL-cam_int1.4-0 ROOT ; 
       MODEL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       MODEL-light1.1-0 ROOT ; 
       MODEL-spot2.1-0 ; 
       MODEL-spot2_int.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       MODEL-mat1.1-0 ; 
       MODEL-METALS-copper01.1-1.1-0 ; 
       MODEL-METALS-silver02.1-1.1-0 ; 
       MODEL-OTHERS-rubber.1-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       MODEL-bmerge1.1-0 ; 
       MODEL-bmerge2.1-0 ; 
       MODEL-cyl1.1-0 ; 
       MODEL-cyl2.1-0 ; 
       MODEL-extru4.1-0 ; 
       MODEL-extru5.1-0 ; 
       MODEL-extru6.1-0 ; 
       MODEL-extru7.1-0 ; 
       MODEL-extru8.1-0 ; 
       MODEL-extru9.1-0 ; 
       MODEL-face3.3-0 ROOT ; 
       MODEL-null14.1-0 ; 
       MODEL-null15.1-0 ; 
       MODEL-null16.1-0 ; 
       MODEL-null17.1-0 ; 
       MODEL-null18.1-0 ; 
       MODEL-null19.1-0 ; 
       MODEL-PIPE.6-0 ; 
       MODEL-PIPE_1.2-0 ; 
       MODEL-PIPE_2.1-0 ; 
       MODEL-PIPE_3.1-0 ; 
       MODEL-PIPE_4.1-0 ; 
       MODEL-sphere1.1-0 ; 
       MODEL-sphere10.1-0 ; 
       MODEL-sphere11.1-0 ; 
       MODEL-sphere12.1-0 ; 
       MODEL-sphere13.1-0 ; 
       MODEL-sphere14.1-0 ; 
       MODEL-sphere16.1-0 ; 
       MODEL-sphere17.1-0 ; 
       MODEL-sphere18.1-0 ; 
       MODEL-sphere19.1-0 ; 
       MODEL-sphere2.1-0 ; 
       MODEL-sphere20.1-0 ; 
       MODEL-sphere21.1-0 ; 
       MODEL-sphere22.1-0 ; 
       MODEL-sphere23.1-0 ; 
       MODEL-sphere24.1-0 ; 
       MODEL-sphere25.1-0 ; 
       MODEL-sphere26.1-0 ; 
       MODEL-sphere27.1-0 ; 
       MODEL-sphere28.1-0 ; 
       MODEL-sphere29.1-0 ROOT ; 
       MODEL-sphere3.1-0 ; 
       MODEL-sphere4.1-0 ; 
       MODEL-sphere5.1-0 ; 
       MODEL-sphere6.1-0 ; 
       MODEL-sphere7.1-0 ; 
       MODEL-sphere8.1-0 ; 
       MODEL-sphere9.1-0 ; 
       MODEL-torus3.1-0 ; 
       MODEL-torus5.1-0 ; 
       MODEL-torus6.1-0 ; 
       MODEL-torus7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       G:/HUD/Softimage_Databases/Bits/PICTURES/TRANSPORT-shuttle4 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       paths-MODEL.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       MODEL-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 11 110 ; 
       21 10 110 ; 
       12 10 110 ; 
       0 11 110 ; 
       11 10 110 ; 
       13 10 110 ; 
       1 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       22 14 110 ; 
       32 14 110 ; 
       43 14 110 ; 
       44 14 110 ; 
       45 14 110 ; 
       46 14 110 ; 
       47 14 110 ; 
       48 14 110 ; 
       49 14 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       28 15 110 ; 
       29 15 110 ; 
       30 15 110 ; 
       31 15 110 ; 
       33 15 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 15 110 ; 
       37 15 110 ; 
       38 15 110 ; 
       39 15 110 ; 
       40 15 110 ; 
       41 15 110 ; 
       16 10 110 ; 
       52 50 110 ; 
       53 51 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       50 2 110 ; 
       51 3 110 ; 
       4 11 110 ; 
       5 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 2 300 ; 
       0 2 300 ; 
       11 2 300 ; 
       13 2 300 ; 
       1 2 300 ; 
       7 2 300 ; 
       8 2 300 ; 
       9 2 300 ; 
       22 1 300 ; 
       32 1 300 ; 
       43 1 300 ; 
       44 1 300 ; 
       45 1 300 ; 
       46 1 300 ; 
       47 1 300 ; 
       48 1 300 ; 
       49 1 300 ; 
       23 1 300 ; 
       24 1 300 ; 
       25 1 300 ; 
       26 1 300 ; 
       27 1 300 ; 
       14 1 300 ; 
       15 1 300 ; 
       42 0 300 ; 
       28 1 300 ; 
       29 1 300 ; 
       30 1 300 ; 
       31 1 300 ; 
       33 1 300 ; 
       34 1 300 ; 
       35 1 300 ; 
       36 1 300 ; 
       37 1 300 ; 
       38 1 300 ; 
       39 1 300 ; 
       40 1 300 ; 
       41 1 300 ; 
       16 1 300 ; 
       10 1 300 ; 
       17 2 300 ; 
       50 3 300 ; 
       51 3 300 ; 
       4 2 300 ; 
       5 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       42 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 2 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 112.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 112.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 120 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 36.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 107.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 105 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 102.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 100 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 97.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 95 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 92.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 90 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 87.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 85 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 82.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 80 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 77.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 91.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 116.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 72.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 70 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 67.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 65 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 62.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 60 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 55 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 52.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 50 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 47.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 45 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 42.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 25 -8 0 MPRFLG 0 ; 
       53 SCHEM 30 -8 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 MPRFLG 0 ; 
       10 SCHEM 56.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 10 -2 0 MPRFLG 0 ; 
       18 SCHEM 5 -2 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       50 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       51 SCHEM 30 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 43.48817 -2.550733 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 117.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
