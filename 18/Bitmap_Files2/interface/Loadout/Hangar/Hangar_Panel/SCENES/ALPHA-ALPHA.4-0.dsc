SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ALPHA-null25.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       mul_ceiling_lights-cam_int1.65-0 ROOT ; 
       mul_ceiling_lights-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       ALPHA-Constant_Black2.1-0 ; 
       done_notch-chrome_pipe1.3-0 ; 
       done_notch-Copper_Pipe1.3-0 ; 
       done_notch-Door_Bars.2-0 ; 
       done_notch-Door_Chrome1.2-0 ; 
       done_notch-Door_Chrome4.2-0 ; 
       done_notch-Door_Chrome5.2-0 ; 
       done_notch-mat27.2-0 ; 
       done_notch-mat28.2-0 ; 
       done_notch-METALS-PLATINUM.1-0.2-0 ; 
       done_notch-portal_edge1.2-0 ; 
       done_notch-sign_Above_Door.2-0 ; 
       mul_ceiling_lights-Ceiling_Metal_Bars.3-0 ; 
       mul_ceiling_lights-Ceiling1.3-0 ; 
       mul_ceiling_lights-Constant_Black1.3-0 ; 
       mul_ceiling_lights-Door_Frame.3-0 ; 
       mul_ceiling_lights-Floor2.3-0 ; 
       mul_ceiling_lights-Metal_Bars.3-0 ; 
       mul_ceiling_lights-TV_Plastic.2-0 ; 
       mul_ceiling_lights-TV_Screen.2-0 ; 
       mul_ceiling_lights-Wall1.4-0 ; 
       mul_ceiling_lights-window_Glass.4-0 ; 
       render-mat30.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 166     
       ALPHA-Above_Ceiling.1-0 ; 
       ALPHA-Above_Ceiling1.1-0 ; 
       ALPHA-Base.11-0 ; 
       ALPHA-base.7-0 ; 
       ALPHA-base_1.9-0 ; 
       ALPHA-beam1.1-0 ; 
       ALPHA-beam1_1.1-0 ; 
       ALPHA-beam144.1-0 ; 
       ALPHA-beam144_1.1-0 ; 
       ALPHA-beam145.1-0 ; 
       ALPHA-beam145_1.1-0 ; 
       ALPHA-beam146.1-0 ; 
       ALPHA-beam146_1.1-0 ; 
       ALPHA-beam147.1-0 ; 
       ALPHA-beam147_1.1-0 ; 
       ALPHA-beam148.1-0 ; 
       ALPHA-beam148_1.1-0 ; 
       ALPHA-beam149.1-0 ; 
       ALPHA-beam149_1.1-0 ; 
       ALPHA-beam150.1-0 ; 
       ALPHA-beam150_1.1-0 ; 
       ALPHA-beam151.1-0 ; 
       ALPHA-beam151_1.1-0 ; 
       ALPHA-beam152.1-0 ; 
       ALPHA-beam152_1.1-0 ; 
       ALPHA-beam153.1-0 ; 
       ALPHA-beam153_1.1-0 ; 
       ALPHA-beam154.1-0 ; 
       ALPHA-beam154_1.1-0 ; 
       ALPHA-beam155.1-0 ; 
       ALPHA-beam155_1.1-0 ; 
       ALPHA-beam156.1-0 ; 
       ALPHA-beam156_1.1-0 ; 
       ALPHA-beam157.1-0 ; 
       ALPHA-beam157_1.1-0 ; 
       ALPHA-beam158.1-0 ; 
       ALPHA-beam158_1.1-0 ; 
       ALPHA-beam159.1-0 ; 
       ALPHA-beam159_1.1-0 ; 
       ALPHA-beam196.1-0 ; 
       ALPHA-beam196_1.1-0 ; 
       ALPHA-beam197.1-0 ; 
       ALPHA-beam197_1.1-0 ; 
       ALPHA-beam198.1-0 ; 
       ALPHA-beam198_1.1-0 ; 
       ALPHA-beam199.1-0 ; 
       ALPHA-beam199_1.1-0 ; 
       ALPHA-beam200.1-0 ; 
       ALPHA-beam200_1.1-0 ; 
       ALPHA-beam201.1-0 ; 
       ALPHA-beam201_1.1-0 ; 
       ALPHA-beam202.1-0 ; 
       ALPHA-beam202_1.1-0 ; 
       ALPHA-beam203.1-0 ; 
       ALPHA-beam203_1.1-0 ; 
       ALPHA-beam204.1-0 ; 
       ALPHA-beam204_1.1-0 ; 
       ALPHA-beam205.1-0 ; 
       ALPHA-beam205_1.1-0 ; 
       ALPHA-beam206.1-0 ; 
       ALPHA-beam206_1.1-0 ; 
       ALPHA-beam207.1-0 ; 
       ALPHA-beam207_1.1-0 ; 
       ALPHA-beam208.1-0 ; 
       ALPHA-beam208_1.1-0 ; 
       ALPHA-beam209.1-0 ; 
       ALPHA-beam209_1.1-0 ; 
       ALPHA-beam210.1-0 ; 
       ALPHA-beam210_1.1-0 ; 
       ALPHA-beam211.1-0 ; 
       ALPHA-beam211_1.1-0 ; 
       ALPHA-beam212.1-0 ; 
       ALPHA-beam212_1.1-0 ; 
       ALPHA-beam213.1-0 ; 
       ALPHA-beam213_1.1-0 ; 
       ALPHA-beam214.1-0 ; 
       ALPHA-beam214_1.1-0 ; 
       ALPHA-Bottom_Panel.1-0 ; 
       ALPHA-Brace1.1-0 ; 
       ALPHA-Brace2.1-0 ; 
       ALPHA-Ceiling.1-0 ; 
       ALPHA-Central_Cylinder.2-0 ; 
       ALPHA-close_side_border.1-0 ; 
       ALPHA-Closing_Edge.1-0 ; 
       ALPHA-cube10.1-0 ; 
       ALPHA-cube11.1-0 ; 
       ALPHA-cube12.1-0 ; 
       ALPHA-cube17.1-0 ; 
       ALPHA-cube18.1-0 ; 
       ALPHA-cube19.1-0 ; 
       ALPHA-cube2.1-0 ; 
       ALPHA-cube20.1-0 ; 
       ALPHA-cube21.1-0 ; 
       ALPHA-cube22.1-0 ; 
       ALPHA-cube23.1-0 ; 
       ALPHA-cube24.1-0 ; 
       ALPHA-cube25.1-0 ; 
       ALPHA-cube26.1-0 ; 
       ALPHA-cube27.1-0 ; 
       ALPHA-cube28.1-0 ; 
       ALPHA-cube29.1-0 ; 
       ALPHA-cube3.1-0 ; 
       ALPHA-cube30.1-0 ; 
       ALPHA-cube31.1-0 ; 
       ALPHA-cube4.1-0 ; 
       ALPHA-cube5.1-0 ; 
       ALPHA-cube6.1-0 ; 
       ALPHA-cube7.1-0 ; 
       ALPHA-cube8.1-0 ; 
       ALPHA-cube9.1-0 ; 
       ALPHA-Detail.1-0 ; 
       ALPHA-Door_1.1-0 ; 
       ALPHA-Door_4.6-0 ; 
       ALPHA-Door_Frame.1-0 ; 
       ALPHA-Door_Line.1-0 ; 
       ALPHA-down_wall.1-0 ; 
       ALPHA-Floor.1-0 ; 
       ALPHA-Floor_Bars_Root.13-0 ; 
       ALPHA-Floor_Edge_Root.13-0 ; 
       ALPHA-Handrail.1-0 ; 
       ALPHA-Hangar_Root.13-0 ; 
       ALPHA-Left_Bar.1-0 ; 
       ALPHA-Monitor_Root.18-0 ; 
       ALPHA-null12.1-0 ; 
       ALPHA-null12_1.1-0 ; 
       ALPHA-null25.1-0 ROOT ; 
       ALPHA-nurbs15.1-0 ; 
       ALPHA-pit_guard.1-0 ; 
       ALPHA-Platform.1-0 ; 
       ALPHA-Portal.1-0 ; 
       ALPHA-portal_Edge.1-0 ; 
       ALPHA-Right_Bar.1-0 ; 
       ALPHA-Room_Root.72-0 ; 
       ALPHA-Root.14-0 ; 
       ALPHA-Screen_1.1-0 ; 
       ALPHA-Screen_3.1-0 ; 
       ALPHA-smoke_grid.1-0 ; 
       ALPHA-Stairs_Root.1-0 ; 
       ALPHA-subfloor.1-0 ; 
       ALPHA-Top_Panel.1-0 ; 
       ALPHA-torus15.1-0 ; 
       ALPHA-torus15_1.1-0 ; 
       ALPHA-torus19.1-0 ; 
       ALPHA-torus19_1.1-0 ; 
       ALPHA-torus21.1-0 ; 
       ALPHA-torus21_1.1-0 ; 
       ALPHA-torus23.1-0 ; 
       ALPHA-torus23_1.1-0 ; 
       ALPHA-torus24.1-0 ; 
       ALPHA-torus24_1.1-0 ; 
       ALPHA-torus27.1-0 ; 
       ALPHA-torus27_1.1-0 ; 
       ALPHA-torus28.1-0 ; 
       ALPHA-torus28_1.1-0 ; 
       ALPHA-torus29.1-0 ; 
       ALPHA-torus29_1.1-0 ; 
       ALPHA-torus30.1-0 ; 
       ALPHA-torus30_1.1-0 ; 
       ALPHA-torus31.1-0 ; 
       ALPHA-torus31_1.1-0 ; 
       ALPHA-torus32.1-0 ; 
       ALPHA-torus32_1.1-0 ; 
       ALPHA-Wall.1-0 ; 
       ALPHA-warning_sign_over_door.1-0 ; 
       ALPHA-window_border.1-0 ; 
       ALPHA-window_pane.1-0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       mul_ceiling_lights-hangar_field1.12-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 14     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Airlock_Decal ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/EXTING ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ILLogo ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/above_Door ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blight ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blue_funk ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/chrome3 ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/door_shit1 ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/pipe ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/wall_Panels ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ALPHA-ALPHA.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       done_notch-above_Door1.2-0 ; 
       done_notch-door_Refmap1.2-0 ; 
       done_notch-door_Shit.2-0 ; 
       done_notch-EXTING1.2-0 ; 
       done_notch-pipe1.3-0 ; 
       done_notch-t2d16.2-0 ; 
       done_notch-t2d33.2-0 ; 
       mul_ceiling_lights-blight1.5-0 ; 
       mul_ceiling_lights-blue_crap1.5-0 ; 
       mul_ceiling_lights-Door_Frame_Hazzard.4-0 ; 
       mul_ceiling_lights-IL_Logo1.5-0 ; 
       mul_ceiling_lights-Panels1.5-0 ; 
       mul_ceiling_lights-Reflection_Map1.3-0 ; 
       mul_ceiling_lights-t2d1.3-0 ; 
       mul_ceiling_lights-t2d2.3-0 ; 
       render-t2d35.2-0 ; 
       render-t2d36.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 125 110 ; 
       6 124 110 ; 
       7 124 110 ; 
       10 124 110 ; 
       12 124 110 ; 
       13 124 110 ; 
       15 124 110 ; 
       18 124 110 ; 
       20 124 110 ; 
       22 124 110 ; 
       24 124 110 ; 
       25 124 110 ; 
       27 124 110 ; 
       30 124 110 ; 
       31 124 110 ; 
       34 124 110 ; 
       35 124 110 ; 
       37 124 110 ; 
       39 124 110 ; 
       42 124 110 ; 
       43 124 110 ; 
       45 124 110 ; 
       47 124 110 ; 
       49 124 110 ; 
       51 124 110 ; 
       54 124 110 ; 
       56 124 110 ; 
       58 124 110 ; 
       59 124 110 ; 
       62 124 110 ; 
       63 124 110 ; 
       66 124 110 ; 
       67 124 110 ; 
       70 124 110 ; 
       72 124 110 ; 
       74 124 110 ; 
       76 124 110 ; 
       124 133 110 ; 
       133 125 110 ; 
       141 133 110 ; 
       142 133 110 ; 
       144 133 110 ; 
       146 133 110 ; 
       148 133 110 ; 
       150 133 110 ; 
       152 133 110 ; 
       154 133 110 ; 
       157 133 110 ; 
       159 133 110 ; 
       160 133 110 ; 
       5 123 110 ; 
       8 123 110 ; 
       9 123 110 ; 
       11 123 110 ; 
       14 123 110 ; 
       16 123 110 ; 
       17 123 110 ; 
       19 123 110 ; 
       21 123 110 ; 
       23 123 110 ; 
       26 123 110 ; 
       28 123 110 ; 
       29 123 110 ; 
       32 123 110 ; 
       33 123 110 ; 
       36 123 110 ; 
       38 123 110 ; 
       40 123 110 ; 
       41 123 110 ; 
       44 123 110 ; 
       46 123 110 ; 
       48 123 110 ; 
       50 123 110 ; 
       52 123 110 ; 
       53 123 110 ; 
       55 123 110 ; 
       57 123 110 ; 
       60 123 110 ; 
       61 123 110 ; 
       64 123 110 ; 
       65 123 110 ; 
       68 123 110 ; 
       69 123 110 ; 
       71 123 110 ; 
       73 123 110 ; 
       75 123 110 ; 
       117 125 110 ; 
       123 117 110 ; 
       140 117 110 ; 
       143 117 110 ; 
       145 117 110 ; 
       147 117 110 ; 
       149 117 110 ; 
       151 117 110 ; 
       153 117 110 ; 
       155 117 110 ; 
       156 117 110 ; 
       158 117 110 ; 
       161 117 110 ; 
       84 118 110 ; 
       85 118 110 ; 
       86 118 110 ; 
       87 118 110 ; 
       88 118 110 ; 
       89 118 110 ; 
       90 118 110 ; 
       91 118 110 ; 
       92 118 110 ; 
       93 118 110 ; 
       94 118 110 ; 
       95 118 110 ; 
       96 118 110 ; 
       97 118 110 ; 
       98 118 110 ; 
       99 118 110 ; 
       100 118 110 ; 
       101 118 110 ; 
       102 118 110 ; 
       103 118 110 ; 
       104 118 110 ; 
       105 118 110 ; 
       106 118 110 ; 
       107 118 110 ; 
       108 118 110 ; 
       109 118 110 ; 
       118 125 110 ; 
       2 125 110 ; 
       110 2 110 ; 
       78 128 110 ; 
       79 78 110 ; 
       119 128 110 ; 
       120 125 110 ; 
       128 137 110 ; 
       137 120 110 ; 
       3 122 110 ; 
       4 122 110 ; 
       122 125 110 ; 
       134 3 110 ; 
       135 4 110 ; 
       0 80 110 ; 
       77 111 110 ; 
       80 132 110 ; 
       81 132 110 ; 
       82 111 110 ; 
       83 111 110 ; 
       111 112 110 ; 
       112 113 110 ; 
       113 162 110 ; 
       114 162 110 ; 
       115 138 110 ; 
       116 132 110 ; 
       121 111 110 ; 
       126 127 110 ; 
       127 116 110 ; 
       129 111 110 ; 
       130 129 110 ; 
       131 111 110 ; 
       132 125 110 ; 
       136 113 110 ; 
       138 116 110 ; 
       139 111 110 ; 
       162 132 110 ; 
       163 113 110 ; 
       164 162 110 ; 
       165 164 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       133 12 300 ; 
       117 17 300 ; 
       2 1 300 ; 
       110 2 300 ; 
       3 18 300 ; 
       4 18 300 ; 
       134 19 300 ; 
       135 19 300 ; 
       0 14 300 ; 
       77 6 300 ; 
       80 13 300 ; 
       82 4 300 ; 
       83 9 300 ; 
       111 7 300 ; 
       113 15 300 ; 
       121 3 300 ; 
       127 16 300 ; 
       129 8 300 ; 
       130 10 300 ; 
       131 3 300 ; 
       136 22 300 ; 
       138 16 300 ; 
       139 5 300 ; 
       162 20 300 ; 
       163 11 300 ; 
       165 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 4 400 ; 
       110 15 400 ; 
       110 16 400 ; 
       134 14 400 ; 
       135 13 400 ; 
       83 6 400 ; 
       111 2 400 ; 
       113 9 400 ; 
       129 1 400 ; 
       139 5 400 ; 
       162 10 400 ; 
       162 8 400 ; 
       162 11 400 ; 
       162 7 400 ; 
       162 3 400 ; 
       163 0 400 ; 
       165 12 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       125 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 55 -2 0 MPRFLG 0 ; 
       125 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 122.5 -6 1 MPRFLG 0 ; 
       7 SCHEM 80 -6 1 MPRFLG 0 ; 
       10 SCHEM 82.5 -6 1 MPRFLG 0 ; 
       12 SCHEM 85 -6 1 MPRFLG 0 ; 
       13 SCHEM 87.5 -6 1 MPRFLG 0 ; 
       15 SCHEM 90 -6 1 MPRFLG 0 ; 
       18 SCHEM 92.5 -6 1 MPRFLG 0 ; 
       20 SCHEM 95 -6 1 MPRFLG 0 ; 
       22 SCHEM 97.5 -6 1 MPRFLG 0 ; 
       24 SCHEM 100 -6 1 MPRFLG 0 ; 
       25 SCHEM 102.5 -6 1 MPRFLG 0 ; 
       27 SCHEM 105 -6 1 MPRFLG 0 ; 
       30 SCHEM 107.5 -6 1 MPRFLG 0 ; 
       31 SCHEM 110 -6 1 MPRFLG 0 ; 
       34 SCHEM 112.5 -6 1 MPRFLG 0 ; 
       35 SCHEM 115 -6 1 MPRFLG 0 ; 
       37 SCHEM 117.5 -6 1 MPRFLG 0 ; 
       39 SCHEM 120 -6 1 MPRFLG 0 ; 
       42 SCHEM 77.5 -6 1 MPRFLG 0 ; 
       43 SCHEM 125 -6 1 MPRFLG 0 ; 
       45 SCHEM 127.5 -6 1 MPRFLG 0 ; 
       47 SCHEM 130 -6 1 MPRFLG 0 ; 
       49 SCHEM 132.5 -6 1 MPRFLG 0 ; 
       51 SCHEM 135 -6 1 MPRFLG 0 ; 
       54 SCHEM 137.5 -6 1 MPRFLG 0 ; 
       56 SCHEM 140 -6 1 MPRFLG 0 ; 
       58 SCHEM 142.5 -6 1 MPRFLG 0 ; 
       59 SCHEM 145 -6 1 MPRFLG 0 ; 
       62 SCHEM 147.5 -6 1 MPRFLG 0 ; 
       63 SCHEM 150 -6 1 MPRFLG 0 ; 
       66 SCHEM 152.5 -6 1 MPRFLG 0 ; 
       67 SCHEM 155 -6 1 MPRFLG 0 ; 
       70 SCHEM 157.5 -6 1 MPRFLG 0 ; 
       72 SCHEM 160 -6 1 MPRFLG 0 ; 
       74 SCHEM 162.5 -6 1 MPRFLG 0 ; 
       76 SCHEM 165 -6 1 MPRFLG 0 ; 
       124 SCHEM 121.25 -4 1 MPRFLG 0 ; 
       133 SCHEM 50 -2 1 MPRFLG 0 ; 
       141 SCHEM 75 -4 1 MPRFLG 0 ; 
       142 SCHEM 65 -4 1 MPRFLG 0 ; 
       144 SCHEM 52.5 -4 1 MPRFLG 0 ; 
       146 SCHEM 55 -4 1 MPRFLG 0 ; 
       148 SCHEM 57.5 -4 1 MPRFLG 0 ; 
       150 SCHEM 60 -4 1 MPRFLG 0 ; 
       152 SCHEM 50 -4 1 MPRFLG 0 ; 
       154 SCHEM 62.5 -4 1 MPRFLG 0 ; 
       157 SCHEM 67.5 -4 1 MPRFLG 0 ; 
       159 SCHEM 72.5 -4 1 MPRFLG 0 ; 
       160 SCHEM 70 -4 1 MPRFLG 0 ; 
       5 SCHEM 107.5 -6 1 MPRFLG 0 ; 
       8 SCHEM 65 -6 1 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 1 MPRFLG 0 ; 
       11 SCHEM 70 -6 1 MPRFLG 0 ; 
       14 SCHEM 72.5 -6 1 MPRFLG 0 ; 
       16 SCHEM 75 -6 1 MPRFLG 0 ; 
       17 SCHEM 77.5 -6 1 MPRFLG 0 ; 
       19 SCHEM 80 -6 1 MPRFLG 0 ; 
       21 SCHEM 82.5 -6 1 MPRFLG 0 ; 
       23 SCHEM 85 -6 1 MPRFLG 0 ; 
       26 SCHEM 87.5 -6 1 MPRFLG 0 ; 
       28 SCHEM 90 -6 1 MPRFLG 0 ; 
       29 SCHEM 92.5 -6 1 MPRFLG 0 ; 
       32 SCHEM 95 -6 1 MPRFLG 0 ; 
       33 SCHEM 97.5 -6 1 MPRFLG 0 ; 
       36 SCHEM 100 -6 1 MPRFLG 0 ; 
       38 SCHEM 102.5 -6 1 MPRFLG 0 ; 
       40 SCHEM 105 -6 1 MPRFLG 0 ; 
       41 SCHEM 62.5 -6 1 MPRFLG 0 ; 
       44 SCHEM 110 -6 1 MPRFLG 0 ; 
       46 SCHEM 112.5 -6 1 MPRFLG 0 ; 
       48 SCHEM 115 -6 1 MPRFLG 0 ; 
       50 SCHEM 117.5 -6 1 MPRFLG 0 ; 
       52 SCHEM 120 -6 1 MPRFLG 0 ; 
       53 SCHEM 122.5 -6 1 MPRFLG 0 ; 
       55 SCHEM 125 -6 1 MPRFLG 0 ; 
       57 SCHEM 127.5 -6 1 MPRFLG 0 ; 
       60 SCHEM 130 -6 1 MPRFLG 0 ; 
       61 SCHEM 132.5 -6 1 MPRFLG 0 ; 
       64 SCHEM 135 -6 1 MPRFLG 0 ; 
       65 SCHEM 137.5 -6 1 MPRFLG 0 ; 
       68 SCHEM 140 -6 1 MPRFLG 0 ; 
       69 SCHEM 142.5 -6 1 MPRFLG 0 ; 
       71 SCHEM 145 -6 1 MPRFLG 0 ; 
       73 SCHEM 147.5 -6 1 MPRFLG 0 ; 
       75 SCHEM 150 -6 1 MPRFLG 0 ; 
       117 SCHEM 47.5 -2 1 MPRFLG 0 ; 
       123 SCHEM 106.25 -4 1 MPRFLG 0 ; 
       140 SCHEM 50 -4 1 MPRFLG 0 ; 
       143 SCHEM 52.5 -4 1 MPRFLG 0 ; 
       145 SCHEM 55 -4 1 MPRFLG 0 ; 
       147 SCHEM 57.5 -4 1 MPRFLG 0 ; 
       149 SCHEM 60 -4 1 MPRFLG 0 ; 
       151 SCHEM 47.5 -4 1 MPRFLG 0 ; 
       153 SCHEM 152.5 -4 1 MPRFLG 0 ; 
       155 SCHEM 155 -4 1 MPRFLG 0 ; 
       156 SCHEM 157.5 -4 1 MPRFLG 0 ; 
       158 SCHEM 160 -4 1 MPRFLG 0 ; 
       161 SCHEM 162.5 -4 1 MPRFLG 0 ; 
       84 SCHEM 85 -4 1 MPRFLG 0 ; 
       85 SCHEM 55 -4 1 MPRFLG 0 ; 
       86 SCHEM 57.5 -4 1 MPRFLG 0 ; 
       87 SCHEM 60 -4 1 MPRFLG 0 ; 
       88 SCHEM 62.5 -4 1 MPRFLG 0 ; 
       89 SCHEM 65 -4 1 MPRFLG 0 ; 
       90 SCHEM 67.5 -4 1 MPRFLG 0 ; 
       91 SCHEM 70 -4 1 MPRFLG 0 ; 
       92 SCHEM 72.5 -4 1 MPRFLG 0 ; 
       93 SCHEM 75 -4 1 MPRFLG 0 ; 
       94 SCHEM 77.5 -4 1 MPRFLG 0 ; 
       95 SCHEM 80 -4 1 MPRFLG 0 ; 
       96 SCHEM 82.5 -4 1 MPRFLG 0 ; 
       97 SCHEM 52.5 -4 1 MPRFLG 0 ; 
       98 SCHEM 87.5 -4 1 MPRFLG 0 ; 
       99 SCHEM 90 -4 1 MPRFLG 0 ; 
       100 SCHEM 92.5 -4 1 MPRFLG 0 ; 
       101 SCHEM 95 -4 1 MPRFLG 0 ; 
       102 SCHEM 97.5 -4 1 MPRFLG 0 ; 
       103 SCHEM 100 -4 1 MPRFLG 0 ; 
       104 SCHEM 102.5 -4 1 MPRFLG 0 ; 
       105 SCHEM 105 -4 1 MPRFLG 0 ; 
       106 SCHEM 107.5 -4 1 MPRFLG 0 ; 
       107 SCHEM 110 -4 1 MPRFLG 0 ; 
       108 SCHEM 112.5 -4 1 MPRFLG 0 ; 
       109 SCHEM 115 -4 1 MPRFLG 0 ; 
       118 SCHEM 52.5 -2 1 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       110 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       78 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       79 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       119 SCHEM 5 -8 0 MPRFLG 0 ; 
       120 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       128 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       137 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 MPRFLG 0 ; 
       122 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       134 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       135 SCHEM 60 -6 0 MPRFLG 0 ; 
       0 SCHEM 45 -6 0 MPRFLG 0 ; 
       77 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       80 SCHEM 45 -4 0 MPRFLG 0 ; 
       81 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       82 SCHEM 35 -12 0 MPRFLG 0 ; 
       83 SCHEM 30 -12 0 MPRFLG 0 ; 
       111 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       112 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       113 SCHEM 30 -6 0 MPRFLG 0 ; 
       114 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       115 SCHEM 10 -8 0 MPRFLG 0 ; 
       116 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       121 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       126 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       127 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       129 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       130 SCHEM 32.5 -14 0 MPRFLG 0 ; 
       131 SCHEM 20 -12 0 MPRFLG 0 ; 
       132 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       136 SCHEM 40 -8 0 MPRFLG 0 ; 
       138 SCHEM 10 -6 0 MPRFLG 0 ; 
       139 SCHEM 25 -12 0 MPRFLG 0 ; 
       162 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       163 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       164 SCHEM 15 -6 0 MPRFLG 0 ; 
       165 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.86975 -4.07901 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -33.19415 -6.286673 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -30.69415 -6.286673 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20.88935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 18.38935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25.88935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 28.38935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15.88935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 33.38936 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30.88935 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 38.38936 -11.67275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -11.50735 -46.01818 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9.257183 -8.046623 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.869751 -8.07901 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4.998672 -10.06399 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4.438347 -7.210381 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -10.44706 -51.30468 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.841492 -9.165932 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.975296 -7.330811 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -127.0499 79.66614 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -132.1127 79.61006 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.88936 -11.67275 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -28.19415 -6.286673 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 8.991646 -12.09978 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 3.991646 -12.09978 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.758003 -11.07792 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9.079666 -10.99906 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.491646 -12.09978 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 3.071587 -11.05971 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -146.6607 82.96746 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -149.1607 82.96746 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 11 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
