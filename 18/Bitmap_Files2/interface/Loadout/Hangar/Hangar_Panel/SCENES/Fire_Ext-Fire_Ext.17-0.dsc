SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Fire_Ext-cam_int1.17-0 ROOT ; 
       Fire_Ext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       Fire_Ext-Fire_Ext_Black.2-0 ; 
       Fire_Ext-Fire_Ext_Chrome1.2-0 ; 
       Fire_Ext-Fire_Ext_Gauge.1-0 ; 
       Fire_Ext-Fire_Ext_Red1.1-0 ; 
       Fire_Ext-tag.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       Fire_Ext-cube1.1-0 ; 
       Fire_Ext-cube4.1-0 ; 
       Fire_Ext-cyl1.1-0 ; 
       Fire_Ext-cyl2.1-0 ; 
       Fire_Ext-cyl3.1-0 ; 
       Fire_Ext-cyl4.1-0 ; 
       Fire_Ext-cyl5.1-0 ; 
       Fire_Ext-Fire_Ext.1-0 ROOT ; 
       Fire_Ext-grid1.1-0 ; 
       Fire_Ext-PIPE.2-0 ; 
       Fire_Ext-PIPE_2.4-0 ; 
       Fire_Ext-spline1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Fire_Ext_label ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_gauge ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_tag ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Fire_Ext-Fire_Ext.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       Fire_Ext-t2d1.1-0 ; 
       Fire_Ext-t2d2.1-0 ; 
       Fire_Ext-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 4 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 1 300 ; 
       2 1 300 ; 
       3 1 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       8 4 300 ; 
       9 0 300 ; 
       7 3 300 ; 
       10 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 0 400 ; 
       8 1 400 ; 
       7 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
