SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_wall-cam_int1.23-0 ROOT ; 
       texture_wall-Camera.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       Add_Fire_Ext-Fire_Ext_Black.1-0 ; 
       Add_Fire_Ext-Fire_Ext_Chrome1.1-0 ; 
       Add_Fire_Ext-Fire_Ext_Gauge.1-0 ; 
       Add_Fire_Ext-Fire_Ext_Red1.1-0 ; 
       Add_Fire_Ext-tag.1-0 ; 
       try1-Ceiling1.2-0 ; 
       try1-Constant_Black1.2-0 ; 
       try1-Door_Frame1.2-0 ; 
       try1-Door1.2-0 ; 
       try1-Floor1.2-0 ; 
       try1-Floor2.2-0 ; 
       try1-Wall1.2-0 ; 
       try1-window_Glass1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       Add_Fire_Ext-cube1.1-0 ; 
       Add_Fire_Ext-cube4.1-0 ; 
       Add_Fire_Ext-cyl1.1-0 ; 
       Add_Fire_Ext-cyl2.1-0 ; 
       Add_Fire_Ext-cyl3.1-0 ; 
       Add_Fire_Ext-cyl4.1-0 ; 
       Add_Fire_Ext-cyl5.1-0 ; 
       Add_Fire_Ext-Fire_Ext.1-0 ROOT ; 
       Add_Fire_Ext-grid1.1-0 ; 
       Add_Fire_Ext-PIPE.2-0 ; 
       Add_Fire_Ext-PIPE_2.4-0 ; 
       Add_Fire_Ext-spline1.2-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Central_Cylinder.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ; 
       Room_Base-Door_Line.1-0 ; 
       Room_Base-face1.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.38-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       Room_Base-window_border.1-0 ; 
       Room_Base-window_pane.1-0 ; 
       try1-angle_line.1-0 ; 
       try1-cube1.4-0 ROOT ; 
       try1-cyl1.3-0 ROOT ; 
       try1-cyl2.3-0 ROOT ; 
       try1-cyl2_1.1-0 ; 
       try1-cyl2_1_1.2-0 ; 
       try1-cyl3.1-0 ; 
       try1-cyl4.1-0 ; 
       try1-null1.3-0 ROOT ; 
       try1-null2.4-0 ROOT ; 
       try1-nurbs10.3-0 ; 
       try1-nurbs12.1-0 ; 
       try1-nurbs14.3-0 ROOT ; 
       try1-nurbs7.1-0 ; 
       try1-nurbs8.4-0 ROOT ; 
       try1-PIPE.1-0 ; 
       try1-PIPE_1.1-0 ; 
       try1-PIPE_10.1-0 ; 
       try1-PIPE_12.2-0 ; 
       try1-PIPE_2.1-0 ; 
       try1-PIPE_3.1-0 ; 
       try1-PIPE_4.3-0 ; 
       try1-PIPE_5.2-0 ; 
       try1-PIPE_6.3-0 ROOT ; 
       try1-PIPE_7.1-0 ; 
       try1-PIPE_9.1-0 ; 
       try3-PIPE_6.3-0 ROOT ; 
       try4-cube1.2-0 ROOT ; 
       try4-PIPE_10.1-0 ; 
       try4-PIPE_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Fire_Ext_label ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ILLogo ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blight ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blue_funk ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_gauge ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_tag ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/wall_Panels ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Pipes-Add_Fire_Ext.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       Add_Fire_Ext-t2d1.1-0 ; 
       Add_Fire_Ext-t2d2.1-0 ; 
       Add_Fire_Ext-t2d3.1-0 ; 
       try1-blight1.2-0 ; 
       try1-blue_crap1.2-0 ; 
       try1-Door_Frame_Hazzard1.2-0 ; 
       try1-IL_Logo1.2-0 ; 
       try1-Panels1.2-0 ; 
       try1-Reflection_Map1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 4 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 7 110 ; 
       12 13 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       15 16 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 15 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       22 19 110 ; 
       23 21 110 ; 
       24 23 110 ; 
       25 24 110 ; 
       26 34 110 ; 
       30 38 110 ; 
       31 36 110 ; 
       32 40 110 ; 
       33 39 110 ; 
       36 30 110 ; 
       37 33 110 ; 
       39 40 110 ; 
       41 34 110 ; 
       42 34 110 ; 
       43 27 110 ; 
       44 31 110 ; 
       45 34 110 ; 
       46 34 110 ; 
       47 35 110 ; 
       48 35 110 ; 
       50 47 110 ; 
       51 27 110 ; 
       54 53 110 ; 
       55 53 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 1 300 ; 
       2 1 300 ; 
       3 1 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       8 4 300 ; 
       9 0 300 ; 
       7 3 300 ; 
       10 0 300 ; 
       12 6 300 ; 
       13 5 300 ; 
       15 8 300 ; 
       16 7 300 ; 
       20 10 300 ; 
       22 9 300 ; 
       23 11 300 ; 
       25 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 0 400 ; 
       8 1 400 ; 
       7 2 400 ; 
       16 5 400 ; 
       23 6 400 ; 
       23 4 400 ; 
       23 7 400 ; 
       23 3 400 ; 
       25 8 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 0 0 SRT 3.544473 3.544473 3.544473 0 -2.559998 0 45.54572 1.98552 -27.5324 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 37 -4 0 MPRFLG 0 ; 
       13 SCHEM 38.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 47 -2 0 MPRFLG 0 ; 
       15 SCHEM 55.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 58.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 64.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 54.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 43.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 42 -4 0 MPRFLG 0 ; 
       21 SCHEM 57 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 44.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 63.25 -2 0 MPRFLG 0 ; 
       24 SCHEM 50.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 50.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 39.66711 15.44105 0 MPRFLG 0 ; 
       27 SCHEM 75.118 19.98875 0 USR SRT 0.6319999 0.6319999 0.6319999 0 -0.46 0 22.68504 -2.582028 -46.08094 MPRFLG 0 ; 
       28 SCHEM 98.90504 17.44106 0 SRT 1.892671 0.2244774 1.892671 0 0 0 44.5927 0 -30.62145 MPRFLG 0 ; 
       29 SCHEM 101.7965 17.48999 0 USR SRT 1.892671 0.2244774 1.892671 1.570796 -2.590017 0 46.25054 15.21868 -27.89786 MPRFLG 0 ; 
       30 SCHEM 100.5318 12.73035 0 USR MPRFLG 0 ; 
       31 SCHEM 100.4756 10.79311 0 USR MPRFLG 0 ; 
       32 SCHEM 69.37462 12.47615 0 MPRFLG 0 ; 
       33 SCHEM 71.87462 10.47615 0 MPRFLG 0 ; 
       34 SCHEM 44.66711 17.44106 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 82.12348 17.44106 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 100.3331 11.90357 0 USR MPRFLG 0 ; 
       37 SCHEM 71.87462 8.47615 0 MPRFLG 0 ; 
       38 SCHEM 98.88568 13.63933 0 USR SRT 1 1 1 0 0 0 -1.65044 -24.49304 -0.247566 MPRFLG 0 ; 
       39 SCHEM 71.87462 12.47615 0 MPRFLG 0 ; 
       40 SCHEM 70.62462 14.47615 0 USR SRT 0.9999999 0.9999999 0.9999999 0 -4.912643e-008 0 -0.8182016 -24.49304 0.5741778 MPRFLG 0 ; 
       41 SCHEM 42.16711 15.44105 0 MPRFLG 0 ; 
       42 SCHEM 49.66711 15.44105 0 MPRFLG 0 ; 
       43 SCHEM 76.368 17.98875 0 MPRFLG 0 ; 
       44 SCHEM 100.1445 9.313616 0 USR MPRFLG 0 ; 
       45 SCHEM 44.66711 15.44105 0 MPRFLG 0 ; 
       46 SCHEM 47.16711 15.44105 0 MPRFLG 0 ; 
       47 SCHEM 83.37348 15.44106 0 MPRFLG 0 ; 
       48 SCHEM 80.87348 15.44106 0 MPRFLG 0 ; 
       49 SCHEM 88.90504 17.4214 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       50 SCHEM 83.37348 13.44106 0 MPRFLG 0 ; 
       51 SCHEM 73.868 17.98875 0 MPRFLG 0 ; 
       52 SCHEM 85.87348 17.44106 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       53 SCHEM 105.5465 19.98875 0 USR SRT 0.4070079 0.4070079 0.4070079 0 -1.905992 0 49.70624 -15.35827 17.57039 MPRFLG 0 ; 
       54 SCHEM 106.7965 17.98875 0 MPRFLG 0 ; 
       55 SCHEM 104.2965 17.98875 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 77 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 72 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 69.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 52 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
