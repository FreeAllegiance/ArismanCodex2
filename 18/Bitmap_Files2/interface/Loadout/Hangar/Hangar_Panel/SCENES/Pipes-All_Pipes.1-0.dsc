SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_wall-cam_int1.24-0 ROOT ; 
       texture_wall-Camera.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       All_Pipes-Fire_Ext_Black.1-0 ; 
       All_Pipes-Fire_Ext_Chrome1.1-0 ; 
       All_Pipes-Fire_Ext_Gauge.1-0 ; 
       All_Pipes-Fire_Ext_Red1.1-0 ; 
       All_Pipes-tag.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       try1-angle_line.1-0 ; 
       try1-cube1.1-0 ; 
       try1-cube1_1.4-0 ; 
       try1-cube1_2.2-0 ; 
       try1-cube4.1-0 ; 
       try1-cyl1.1-0 ; 
       try1-cyl1_1.3-0 ; 
       try1-cyl2.1-0 ; 
       try1-cyl2_1.1-0 ; 
       try1-cyl2_1_1.2-0 ; 
       try1-cyl2_3.3-0 ; 
       try1-cyl3.1-0 ; 
       try1-cyl3_1.1-0 ; 
       try1-cyl4.1-0 ; 
       try1-cyl4_1.1-0 ; 
       try1-cyl5.1-0 ; 
       try1-Fire_Ext.1-0 ; 
       try1-grid1.1-0 ; 
       try1-Holder.1-0 ; 
       try1-null1.3-0 ; 
       try1-null2.5-0 ROOT ; 
       try1-nurbs10.3-0 ; 
       try1-nurbs12.1-0 ; 
       try1-nurbs14.3-0 ; 
       try1-nurbs7.1-0 ; 
       try1-nurbs8.4-0 ; 
       try1-PIPE.1-0 ; 
       try1-PIPE_1.1-0 ; 
       try1-PIPE_10.1-0 ; 
       try1-PIPE_10_1.1-0 ; 
       try1-PIPE_12.2-0 ; 
       try1-PIPE_14.2-0 ; 
       try1-PIPE_2.1-0 ; 
       try1-PIPE_2_1.4-0 ; 
       try1-PIPE_3.1-0 ; 
       try1-PIPE_4.3-0 ; 
       try1-PIPE_5.2-0 ; 
       try1-PIPE_7.1-0 ; 
       try1-PIPE_9.1-0 ; 
       try1-PIPE_9_1.1-0 ; 
       try1-spline1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Fire_Ext_label ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ILLogo ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blight ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blue_funk ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_gauge ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_tag ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/wall_Panels ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Pipes-All_Pipes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       All_Pipes-t2d1.1-0 ; 
       All_Pipes-t2d2.1-0 ; 
       All_Pipes-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 16 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       7 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       15 12 110 ; 
       17 16 110 ; 
       31 40 110 ; 
       16 18 110 ; 
       33 40 110 ; 
       40 16 110 ; 
       0 19 110 ; 
       2 20 110 ; 
       6 20 110 ; 
       10 20 110 ; 
       8 23 110 ; 
       9 21 110 ; 
       11 25 110 ; 
       14 24 110 ; 
       19 20 110 ; 
       21 8 110 ; 
       22 14 110 ; 
       23 20 110 ; 
       24 25 110 ; 
       25 20 110 ; 
       26 19 110 ; 
       27 19 110 ; 
       28 2 110 ; 
       30 9 110 ; 
       32 19 110 ; 
       34 19 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 35 110 ; 
       38 2 110 ; 
       3 23 110 ; 
       29 3 110 ; 
       39 3 110 ; 
       18 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       4 1 300 ; 
       5 1 300 ; 
       7 1 300 ; 
       12 0 300 ; 
       13 1 300 ; 
       15 2 300 ; 
       17 4 300 ; 
       31 0 300 ; 
       16 3 300 ; 
       33 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 0 400 ; 
       17 1 400 ; 
       16 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 70 -6 0 MPRFLG 0 ; 
       4 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 65 -8 0 MPRFLG 0 ; 
       17 SCHEM 80 -6 0 MPRFLG 0 ; 
       31 SCHEM 75 -8 0 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 MPRFLG 0 ; 
       33 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 76.25 -6 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 55 -2 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 MPRFLG 0 ; 
       19 SCHEM 25 -2 0 MPRFLG 0 ; 
       20 SCHEM 50 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 35 -8 0 MPRFLG 0 ; 
       23 SCHEM 50 -2 0 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 MPRFLG 0 ; 
       25 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       26 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 30 -4 0 MPRFLG 0 ; 
       28 SCHEM 40 -4 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 25 -4 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 45 -2 0 MPRFLG 0 ; 
       36 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       37 SCHEM 45 -4 0 MPRFLG 0 ; 
       38 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 50 -6 0 MPRFLG 0 ; 
       18 SCHEM 70 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 79 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 81.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
