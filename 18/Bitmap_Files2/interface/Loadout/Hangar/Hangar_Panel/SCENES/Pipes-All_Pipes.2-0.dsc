SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_wall-cam_int1.25-0 ROOT ; 
       texture_wall-Camera.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       All_Pipes-Fire_Ext_Black.1-0 ; 
       All_Pipes-Fire_Ext_Chrome1.1-0 ; 
       All_Pipes-Fire_Ext_Gauge.1-0 ; 
       All_Pipes-Fire_Ext_Red1.1-0 ; 
       All_Pipes-tag.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       All_Pipes-All_Pipes.1-0 ROOT ; 
       All_Pipes-angle_line.1-0 ; 
       All_Pipes-cube1.1-0 ; 
       All_Pipes-cube1_1.4-0 ; 
       All_Pipes-cube1_2.2-0 ; 
       All_Pipes-cube4.1-0 ; 
       All_Pipes-cyl1.1-0 ; 
       All_Pipes-cyl1_1.3-0 ; 
       All_Pipes-cyl2.1-0 ; 
       All_Pipes-cyl2_1.1-0 ; 
       All_Pipes-cyl2_1_1.2-0 ; 
       All_Pipes-cyl2_3.3-0 ; 
       All_Pipes-cyl3.1-0 ; 
       All_Pipes-cyl3_1.1-0 ; 
       All_Pipes-cyl4.1-0 ; 
       All_Pipes-cyl4_1.1-0 ; 
       All_Pipes-cyl5.1-0 ; 
       All_Pipes-Fire_Ext.1-0 ; 
       All_Pipes-grid1.1-0 ; 
       All_Pipes-Holder.1-0 ; 
       All_Pipes-null1.3-0 ; 
       All_Pipes-nurbs10.3-0 ; 
       All_Pipes-nurbs12.1-0 ; 
       All_Pipes-nurbs14.3-0 ; 
       All_Pipes-nurbs7.1-0 ; 
       All_Pipes-nurbs8.4-0 ; 
       All_Pipes-PIPE.1-0 ; 
       All_Pipes-PIPE_1.1-0 ; 
       All_Pipes-PIPE_10.1-0 ; 
       All_Pipes-PIPE_10_1.1-0 ; 
       All_Pipes-PIPE_12.2-0 ; 
       All_Pipes-PIPE_14.2-0 ; 
       All_Pipes-PIPE_2.1-0 ; 
       All_Pipes-PIPE_2_1.4-0 ; 
       All_Pipes-PIPE_3.1-0 ; 
       All_Pipes-PIPE_4.3-0 ; 
       All_Pipes-PIPE_5.2-0 ; 
       All_Pipes-PIPE_7.1-0 ; 
       All_Pipes-PIPE_9.1-0 ; 
       All_Pipes-PIPE_9_1.1-0 ; 
       All_Pipes-spline1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Fire_Ext_label ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_gauge ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/fire_ext_tag ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Pipes-All_Pipes.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       All_Pipes-t2d1.1-0 ; 
       All_Pipes-t2d2.1-0 ; 
       All_Pipes-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 17 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       8 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       16 13 110 ; 
       18 17 110 ; 
       31 40 110 ; 
       17 19 110 ; 
       33 40 110 ; 
       40 17 110 ; 
       1 20 110 ; 
       3 0 110 ; 
       7 0 110 ; 
       11 0 110 ; 
       9 23 110 ; 
       10 21 110 ; 
       12 25 110 ; 
       15 24 110 ; 
       20 0 110 ; 
       21 9 110 ; 
       22 15 110 ; 
       23 0 110 ; 
       24 25 110 ; 
       25 0 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 3 110 ; 
       30 10 110 ; 
       32 20 110 ; 
       34 20 110 ; 
       35 0 110 ; 
       36 0 110 ; 
       37 35 110 ; 
       38 3 110 ; 
       4 23 110 ; 
       29 4 110 ; 
       39 4 110 ; 
       19 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 1 300 ; 
       5 1 300 ; 
       6 1 300 ; 
       8 1 300 ; 
       13 0 300 ; 
       14 1 300 ; 
       16 2 300 ; 
       18 4 300 ; 
       31 0 300 ; 
       17 3 300 ; 
       33 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       16 0 400 ; 
       18 1 400 ; 
       17 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 70 -6 0 MPRFLG 0 ; 
       5 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 60 -6 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 65 -6 0 MPRFLG 0 ; 
       14 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 MPRFLG 0 ; 
       18 SCHEM 80 -6 0 MPRFLG 0 ; 
       31 SCHEM 75 -8 0 MPRFLG 0 ; 
       17 SCHEM 70 -4 0 MPRFLG 0 ; 
       33 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 76.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 55 -2 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 35 -6 0 MPRFLG 0 ; 
       20 SCHEM 25 -2 0 MPRFLG 0 ; 
       0 SCHEM 50 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 35 -8 0 MPRFLG 0 ; 
       23 SCHEM 50 -2 0 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 MPRFLG 0 ; 
       25 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       26 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 30 -4 0 MPRFLG 0 ; 
       28 SCHEM 40 -4 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 25 -4 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 45 -2 0 MPRFLG 0 ; 
       36 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       37 SCHEM 45 -4 0 MPRFLG 0 ; 
       38 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 50 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 79 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 81.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
