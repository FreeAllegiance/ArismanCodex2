SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.11-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       model-mat1.2-0 ; 
       model-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       spock_viewer-cube2.1-0 ; 
       spock_viewer-nurbs1.1-0 ; 
       spock_viewer-skin1.1-0 ; 
       spock_viewer-spock_viewer.1-0 ROOT ; 
       spock_viewer-torus2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/radar ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Spock_viewer-model.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       model-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 4 110 ; 
       0 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 1 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 10 1.134752 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 -4.978246 0 MPRFLG 0 ; 
       1 SCHEM 11.25 -2.865248 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -0.8652482 0 MPRFLG 0 ; 
       2 SCHEM 5 -0.8652482 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -0.8652482 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4.865248 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -0.8652482 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -4.865248 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
