SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       window_trim-cam_int1.1-0 ROOT ; 
       window_trim-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       PIPE-PIPE.1-0 ROOT ; 
       window_trim-Window_trim_base.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       window_trim-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       create-window_trim.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
