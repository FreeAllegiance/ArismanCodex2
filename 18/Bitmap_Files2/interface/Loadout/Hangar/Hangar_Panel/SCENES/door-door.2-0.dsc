SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       door-cam_int1.2-0 ROOT ; 
       door-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       door-Door_Frame1.1-0 ; 
       door-Door1.1-0 ; 
       door-mat1.1-0 ; 
       door-mat2.1-0 ; 
       door-portal_edge1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       Door-Door.2-0 ROOT ; 
       door-face3.1-0 ; 
       door-null1.1-0 ROOT ; 
       Door-Portal.1-0 ; 
       Door-portal_Edge.1-0 ; 
       door-square2.1-0 ; 
       door-square3.1-0 ; 
       door-square4.1-0 ; 
       door-square5.1-0 ; 
       door-square6.1-0 ; 
       door-square7.1-0 ; 
       door-square8.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ROOT ; 
       Room_Base-face1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       door-door.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       door-Door_Frame_Hazzard1.1-0 ; 
       door-door_Refmap1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 13 110 ; 
       14 12 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 4 300 ; 
       0 2 300 ; 
       13 0 300 ; 
       12 1 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       13 0 400 ; 
       3 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 7.5 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 14.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.21654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 19.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 22.21654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 24.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 12.21654 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 9.861725 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 27.30761 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       12 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 29.80761 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34.80761 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39.80761 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 37.30761 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
