SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       door-cam_int1.3-0 ROOT ; 
       door-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       door-light1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       door-Door_Chrome1.1-0 ; 
       door-Door_Frame1.1-0 ; 
       door-Door1.1-0 ; 
       door-mat1.2-0 ; 
       door-mat2.1-0 ; 
       door-portal_edge1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       Door-cube1.1-0 ; 
       Door-cube2.1-0 ; 
       Door-cube3.1-0 ; 
       Door-cube4.1-0 ; 
       Door-cube5.1-0 ; 
       Door-cube6.1-0 ; 
       Door-Door.3-0 ROOT ; 
       Door-Door_1.2-0 ; 
       door-face3.1-0 ; 
       door-null1.1-0 ROOT ; 
       Door-Portal.1-0 ; 
       Door-portal_Edge.1-0 ; 
       door-square2.1-0 ; 
       door-square3.1-0 ; 
       door-square4.1-0 ; 
       door-square5.1-0 ; 
       door-square6.1-0 ; 
       door-square7.1-0 ; 
       door-square8.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ROOT ; 
       Room_Base-face1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/door_shit1 ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       door-door.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       door-Door_Frame_Hazzard1.1-0 ; 
       door-door_Refmap1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 9 110 ; 
       11 10 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 9 110 ; 
       1 7 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       7 6 110 ; 
       0 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       19 20 110 ; 
       21 19 110 ; 
       10 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 5 300 ; 
       1 0 300 ; 
       7 3 300 ; 
       0 0 300 ; 
       2 0 300 ; 
       3 0 300 ; 
       4 0 300 ; 
       5 0 300 ; 
       20 1 300 ; 
       19 2 300 ; 
       10 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       20 0 400 ; 
       10 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 64.8076 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 12.5 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 34.80761 -6 0 MPRFLG 0 ; 
       12 SCHEM 19.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 22.21654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 24.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 27.21654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 29.71654 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 44.80761 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.21654 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 14.86172 -6.106377 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 48.55761 -2 0 MPRFLG 0 ; 
       9 SCHEM 32.30761 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 42.30761 -4 0 MPRFLG 0 ; 
       6 SCHEM 48.55761 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -1.195866 5.007269 -24.60189 MPRFLG 0 ; 
       2 SCHEM 47.30761 -4 0 MPRFLG 0 ; 
       3 SCHEM 49.80761 -4 0 MPRFLG 0 ; 
       4 SCHEM 52.30761 -4 0 MPRFLG 0 ; 
       5 SCHEM 54.80761 -4 0 MPRFLG 0 ; 
       20 SCHEM 6.25 0 0 DISPLAY 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       19 SCHEM 3.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.30761 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 57.30761 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.30761 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34.80761 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 48.18835 -7.216215 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 39.80761 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
