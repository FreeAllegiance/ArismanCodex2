SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_wall-cam_int1.21-0 ROOT ; 
       texture_wall-Camera.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       try1-Ceiling1.2-0 ; 
       try1-Constant_Black1.2-0 ; 
       try1-Door_Frame1.2-0 ; 
       try1-Door1.2-0 ; 
       try1-Floor1.2-0 ; 
       try1-Floor2.2-0 ; 
       try1-Wall1.2-0 ; 
       try1-window_Glass1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Central_Cylinder.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ; 
       Room_Base-Door_Line.1-0 ; 
       Room_Base-face1.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.37-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       Room_Base-window_border.1-0 ; 
       Room_Base-window_pane.1-0 ; 
       try1-angle_line.1-0 ; 
       try1-cube1.3-0 ROOT ; 
       try1-cyl1.2-0 ROOT ; 
       try1-cyl2.2-0 ROOT ; 
       try1-cyl2_1.1-0 ; 
       try1-cyl2_1_1.2-0 ; 
       try1-cyl3.1-0 ; 
       try1-cyl4.1-0 ; 
       try1-null1.2-0 ROOT ; 
       try1-null2.3-0 ROOT ; 
       try1-nurbs10.3-0 ; 
       try1-nurbs12.1-0 ; 
       try1-nurbs14.2-0 ROOT ; 
       try1-nurbs7.1-0 ; 
       try1-nurbs8.3-0 ROOT ; 
       try1-PIPE.1-0 ; 
       try1-PIPE_1.1-0 ; 
       try1-PIPE_10.1-0 ; 
       try1-PIPE_12.2-0 ; 
       try1-PIPE_2.1-0 ; 
       try1-PIPE_3.1-0 ; 
       try1-PIPE_4.3-0 ; 
       try1-PIPE_5.2-0 ; 
       try1-PIPE_6.2-0 ROOT ; 
       try1-PIPE_7.1-0 ; 
       try1-PIPE_9.1-0 ; 
       try3-PIPE_6.2-0 ROOT ; 
       try4-cube1.1-0 ROOT ; 
       try4-PIPE_10.1-0 ; 
       try4-PIPE_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/Door_Hazzard ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ILLogo ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blight ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/blue_funk ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/wall_Panels ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       make_Pipes-try1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       try1-blight1.2-0 ; 
       try1-blue_crap1.2-0 ; 
       try1-Door_Frame_Hazzard1.2-0 ; 
       try1-IL_Logo1.2-0 ; 
       try1-Panels1.2-0 ; 
       try1-Reflection_Map1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 4 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 3 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       10 7 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 22 110 ; 
       19 24 110 ; 
       20 28 110 ; 
       21 27 110 ; 
       24 18 110 ; 
       25 21 110 ; 
       27 28 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 15 110 ; 
       32 19 110 ; 
       33 22 110 ; 
       34 22 110 ; 
       35 23 110 ; 
       36 23 110 ; 
       38 35 110 ; 
       39 15 110 ; 
       42 41 110 ; 
       18 26 110 ; 
       43 41 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
       3 3 300 ; 
       4 2 300 ; 
       8 5 300 ; 
       10 4 300 ; 
       11 6 300 ; 
       13 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 2 400 ; 
       11 3 400 ; 
       11 1 400 ; 
       11 4 400 ; 
       11 0 400 ; 
       13 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 21.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 30 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 20 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 28.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 5.167112 15.44105 0 MPRFLG 0 ; 
       15 SCHEM 40.618 19.98875 0 USR SRT 0.6319999 0.6319999 0.6319999 0 -0.46 0 22.68504 -2.582028 -46.08094 MPRFLG 0 ; 
       16 SCHEM 64.40504 17.44106 0 SRT 1.892671 0.2244774 1.892671 0 0 0 44.5927 0 -30.62145 MPRFLG 0 ; 
       17 SCHEM 67.29654 17.48999 0 USR SRT 1.892671 0.2244774 1.892671 1.570796 -2.590017 0 46.25054 15.21868 -27.89786 MPRFLG 0 ; 
       19 SCHEM 65.97564 10.79311 0 USR MPRFLG 0 ; 
       20 SCHEM 34.87462 12.47615 0 MPRFLG 0 ; 
       21 SCHEM 37.37462 10.47615 0 MPRFLG 0 ; 
       22 SCHEM 10.16711 17.44106 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 47.62348 17.44106 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 65.83313 11.90357 0 USR MPRFLG 0 ; 
       25 SCHEM 37.37462 8.47615 0 MPRFLG 0 ; 
       41 SCHEM 71.04654 19.98875 0 USR SRT 0.4070079 0.4070079 0.4070079 0 -1.905992 0 49.70624 -15.35827 17.57039 MPRFLG 0 ; 
       27 SCHEM 37.37462 12.47615 0 MPRFLG 0 ; 
       28 SCHEM 36.12462 14.47615 0 USR SRT 0.9999999 0.9999999 0.9999999 0 -4.912643e-008 0 -0.8182016 -24.49304 0.5741778 MPRFLG 0 ; 
       29 SCHEM 7.667112 15.44105 0 MPRFLG 0 ; 
       30 SCHEM 15.16711 15.44105 0 MPRFLG 0 ; 
       31 SCHEM 41.868 17.98875 0 MPRFLG 0 ; 
       32 SCHEM 65.6445 9.313616 0 USR MPRFLG 0 ; 
       33 SCHEM 10.16711 15.44105 0 MPRFLG 0 ; 
       34 SCHEM 12.66711 15.44105 0 MPRFLG 0 ; 
       35 SCHEM 48.87348 15.44106 0 MPRFLG 0 ; 
       36 SCHEM 46.37348 15.44106 0 MPRFLG 0 ; 
       37 SCHEM 54.40504 17.4214 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 48.87348 13.44106 0 MPRFLG 0 ; 
       39 SCHEM 39.368 17.98875 0 MPRFLG 0 ; 
       40 SCHEM 51.37348 17.44106 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 72.29654 17.98875 0 MPRFLG 0 ; 
       26 SCHEM 64.38568 13.63933 0 USR SRT 1 1 1 0 0 0 -1.65044 -24.49304 -0.247566 MPRFLG 0 ; 
       18 SCHEM 66.03182 12.73035 0 USR MPRFLG 0 ; 
       43 SCHEM 69.79654 17.98875 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
