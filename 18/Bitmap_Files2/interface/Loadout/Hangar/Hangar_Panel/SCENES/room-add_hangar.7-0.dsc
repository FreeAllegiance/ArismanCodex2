SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_hangar-cam_int1Cam.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_hangar-cam_int1.7-0 ROOT ; 
       add_hangar-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS NBELEM 1     
       add_hangar-Bionic_Lens1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       add_hangar-light1.7-0 ROOT ; 
       add_hangar-light2.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       add_hangar-Ceiling1.1-0 ; 
       add_hangar-Constant_Black.1-0 ; 
       add_hangar-Floor1.1-0 ; 
       add_hangar-mat3.1-0 ; 
       add_hangar-mat4.1-0 ; 
       add_hangar-mat5.1-0 ; 
       add_hangar-mat6.1-0 ; 
       add_hangar-mat7.1-0 ; 
       add_hangar-mat8.1-0 ; 
       add_hangar-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 66     
       add_hangar-bionic_volume.1-0 ROOT ; 
       add_hangar-cube1.1-0 ROOT ; 
       add_hangar-cube10.1-0 ; 
       add_hangar-cube11.1-0 ; 
       add_hangar-cube12.1-0 ; 
       add_hangar-cube17.1-0 ; 
       add_hangar-cube18.1-0 ; 
       add_hangar-cube19.1-0 ; 
       add_hangar-cube2.1-0 ; 
       add_hangar-cube20.1-0 ; 
       add_hangar-cube21.1-0 ; 
       add_hangar-cube22.1-0 ; 
       add_hangar-cube23.1-0 ; 
       add_hangar-cube24.1-0 ; 
       add_hangar-cube25.1-0 ; 
       add_hangar-cube26.1-0 ; 
       add_hangar-cube27.1-0 ; 
       add_hangar-cube28.1-0 ; 
       add_hangar-cube29.1-0 ; 
       add_hangar-cube3.1-0 ; 
       add_hangar-cube30.1-0 ; 
       add_hangar-cube31.1-0 ; 
       add_hangar-cube32.2-0 ROOT ; 
       add_hangar-cube33.1-0 ; 
       add_hangar-cube4.1-0 ; 
       add_hangar-cube5.1-0 ; 
       add_hangar-cube6.1-0 ; 
       add_hangar-cube7.1-0 ; 
       add_hangar-cube8.1-0 ; 
       add_hangar-cube9.1-0 ; 
       add_hangar-cyl20.1-0 ; 
       add_hangar-cyl21.1-0 ; 
       add_hangar-cyl3.1-0 ; 
       add_hangar-grid1.1-0 ; 
       add_hangar-grid2.1-0 ; 
       add_hangar-grid3.1-0 ; 
       add_hangar-grid4.2-0 ROOT ; 
       add_hangar-null1.1-0 ; 
       add_hangar-null2.4-0 ROOT ; 
       add_hangar-null8.1-0 ROOT ; 
       Monitor-base.4-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.2-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-below_floor.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-Room_Root.12-0 ROOT ; 
       Room_Base-Wall.1-0 ; 
       screens-Cammand_View.2-0 ; 
       screens-Game_State.2-0 ; 
       screens-Investment.2-0 ; 
       screens-Loadout.4-0 ; 
       screens-screens.3-0 ROOT ; 
       screens-Sector_Overview.4-0 ; 
       screens-Team_Panel.2-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-extru1.1-0 ; 
       standins-face1.1-0 ; 
       standins-nurbs1.1-0 ; 
       standins-sphere1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-add_hangar.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       add_hangar-t2d1.1-0 ; 
       add_hangar-t2d2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       add_hangar-Bionic_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       58 65 110 ; 
       63 65 110 ; 
       61 59 110 ; 
       60 61 110 ; 
       62 59 110 ; 
       37 39 110 ; 
       8 37 110 ; 
       19 37 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       2 37 110 ; 
       56 55 110 ; 
       52 55 110 ; 
       53 55 110 ; 
       51 55 110 ; 
       57 55 110 ; 
       59 65 110 ; 
       54 55 110 ; 
       64 65 110 ; 
       44 46 110 ; 
       45 48 110 ; 
       46 49 110 ; 
       48 49 110 ; 
       50 49 110 ; 
       3 37 110 ; 
       4 37 110 ; 
       33 38 110 ; 
       23 38 110 ; 
       34 38 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       15 37 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 37 110 ; 
       20 37 110 ; 
       21 37 110 ; 
       47 50 110 ; 
       35 38 110 ; 
       32 36 110 ; 
       41 40 110 ; 
       43 42 110 ; 
       30 36 110 ; 
       31 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       55 3 300 ; 
       0 4 300 ; 
       44 1 300 ; 
       45 1 300 ; 
       46 0 300 ; 
       48 2 300 ; 
       50 9 300 ; 
       40 5 300 ; 
       41 6 300 ; 
       42 7 300 ; 
       43 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       41 0 400 ; 
       43 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER ANIMATION 
       0 0 15007 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       4 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER CAMERA_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -1.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 139.9334 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 142.4334 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       58 SCHEM 27.43336 -2 0 MPRFLG 0 ; 
       63 SCHEM 29.93336 -2 0 MPRFLG 0 ; 
       65 SCHEM 24.93336 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       55 SCHEM 39.93336 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       61 SCHEM 22.43336 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 22.43336 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 49.93336 0 0 DISPLAY 0 0 SRT 11.25 11.25 11.25 0 0 0 0 0 0 MPRFLG 0 ; 
       62 SCHEM 19.93336 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 52.43336 0 0 SRT 1 1 1 0 -0.8726646 0 0 0 0 MPRFLG 0 ; 
       37 SCHEM 103.6834 -2 0 MPRFLG 0 ; 
       8 SCHEM 99.93336 -4 0 MPRFLG 0 ; 
       19 SCHEM 74.93336 -4 0 MPRFLG 0 ; 
       24 SCHEM 77.43336 -4 0 MPRFLG 0 ; 
       25 SCHEM 79.93336 -4 0 MPRFLG 0 ; 
       26 SCHEM 82.43336 -4 0 MPRFLG 0 ; 
       27 SCHEM 84.93336 -4 0 MPRFLG 0 ; 
       28 SCHEM 87.43336 -4 0 MPRFLG 0 ; 
       29 SCHEM 89.93336 -4 0 MPRFLG 0 ; 
       2 SCHEM 92.43336 -4 0 MPRFLG 0 ; 
       56 SCHEM 34.93336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 37.43336 -2 0 MPRFLG 0 ; 
       53 SCHEM 39.93336 -2 0 MPRFLG 0 ; 
       51 SCHEM 42.43336 -2 0 MPRFLG 0 ; 
       57 SCHEM 44.93336 -2 0 MPRFLG 0 ; 
       59 SCHEM 21.18336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 32.43336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 24.93336 -2 0 MPRFLG 0 ; 
       44 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       45 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       46 SCHEM 10 -2 0 MPRFLG 0 ; 
       48 SCHEM 5 -2 0 MPRFLG 0 ; 
       49 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       50 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 94.93336 -4 0 MPRFLG 0 ; 
       4 SCHEM 97.43336 -4 0 MPRFLG 0 ; 
       33 SCHEM 62.43336 -2 0 MPRFLG 0 ; 
       23 SCHEM 64.93336 -2 0 MPRFLG 0 ; 
       34 SCHEM 67.43336 -2 0 MPRFLG 0 ; 
       38 SCHEM 66.18336 0 0 SRT 1.166 1.66738 1.166 0 0 0 181.1734 -0.0535363 -128.4584 MPRFLG 0 ; 
       5 SCHEM 72.43336 -4 0 MPRFLG 0 ; 
       6 SCHEM 102.4334 -4 0 MPRFLG 0 ; 
       7 SCHEM 104.9334 -4 0 MPRFLG 0 ; 
       9 SCHEM 107.4334 -4 0 MPRFLG 0 ; 
       10 SCHEM 109.9334 -4 0 MPRFLG 0 ; 
       11 SCHEM 112.4334 -4 0 MPRFLG 0 ; 
       12 SCHEM 114.9334 -4 0 MPRFLG 0 ; 
       13 SCHEM 117.4334 -4 0 MPRFLG 0 ; 
       14 SCHEM 119.9334 -4 0 MPRFLG 0 ; 
       15 SCHEM 122.4334 -4 0 MPRFLG 0 ; 
       16 SCHEM 124.9334 -4 0 MPRFLG 0 ; 
       17 SCHEM 127.4334 -4 0 MPRFLG 0 ; 
       18 SCHEM 129.9334 -4 0 MPRFLG 0 ; 
       20 SCHEM 132.4334 -4 0 MPRFLG 0 ; 
       21 SCHEM 134.9334 -4 0 MPRFLG 0 ; 
       47 SCHEM 14.93336 -3.835222 0 USR MPRFLG 0 ; 
       22 SCHEM 54.93336 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       35 SCHEM 69.93336 -2 0 MPRFLG 0 ; 
       36 SCHEM 57.43336 0 0 SRT 1.42 1 2.037903 0 0.2792527 0 67.10641 -18.64181 -18.7254 MPRFLG 0 ; 
       40 SCHEM 157.4334 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       32 SCHEM 59.93336 0 0 MPRFLG 0 ; 
       41 SCHEM 156.1834 -2 0 MPRFLG 0 ; 
       42 SCHEM 162.4334 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       43 SCHEM 161.1834 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       30 SCHEM 137.4334 -4 0 MPRFLG 0 ; 
       39 SCHEM 103.6834 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 144.9334 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 8.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.43336 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.43336 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49.93336 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 159.9334 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 154.9334 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 164.9334 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 159.9334 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 157.4334 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 162.4334 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 1.25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
