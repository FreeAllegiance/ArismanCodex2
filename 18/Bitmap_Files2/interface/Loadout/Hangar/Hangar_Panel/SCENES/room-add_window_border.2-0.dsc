SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_window_border-cam_int1.2-0 ROOT ; 
       add_window_border-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 42     
       add_window_border-from_space_to_hangar2.1-0 ; 
       add_window_border-from_space_to_hangar2_int.2-0 ROOT ; 
       add_window_border-spocklight1.2-0 ROOT ; 
       bounce-bounce_off_beam.44-0 ROOT ; 
       Fill-Ceiling.1-0 ; 
       Fill-Ceiling_int.11-0 ROOT ; 
       fill-inside_hangar_fill1.40-0 ROOT ; 
       Fill-Shine_on_backwall.1-0 ; 
       Fill-Shine_on_backwall_int.13-0 ROOT ; 
       Fill-Shine_on_sidewall.1-0 ; 
       Fill-Shine_on_sidewall_int.12-0 ROOT ; 
       Point-Alarm1.47-0 ROOT ; 
       Spot-hilight_logo.1-0 ; 
       Spot-hilight_logo_int.11-0 ROOT ; 
       Vol-floor1.1-0 ; 
       Vol-floor1_int.27-0 ROOT ; 
       Vol-floor10.1-0 ; 
       Vol-floor10_int.24-0 ROOT ; 
       Vol-floor11.1-0 ; 
       Vol-floor11_int.24-0 ROOT ; 
       Vol-floor12.1-0 ; 
       Vol-floor12_int.24-0 ROOT ; 
       Vol-floor3.1-0 ; 
       Vol-floor3_int.27-0 ROOT ; 
       Vol-floor4.1-0 ; 
       Vol-floor4_int.27-0 ROOT ; 
       Vol-floor5.1-0 ; 
       Vol-floor5_int.27-0 ROOT ; 
       Vol-floor6.1-0 ; 
       Vol-floor6_int.27-0 ROOT ; 
       Vol-floor7.1-0 ; 
       Vol-floor7_int.27-0 ROOT ; 
       Vol-floor8.1-0 ; 
       Vol-floor8_int.24-0 ROOT ; 
       Vol-floor9.1-0 ; 
       Vol-floor9_int.24-0 ROOT ; 
       Vol-from_hangar1.1-0 ; 
       Vol-from_hangar1_int.27-0 ROOT ; 
       Vol-Shine_on_Door.1-0 ; 
       Vol-Shine_on_Door_int.14-0 ROOT ; 
       Vol-Under_Hole.1-0 ; 
       Vol-Under_Hole_int.27-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 17     
       add_window_border-alarm1.1-0 ; 
       add_window_border-floor1.1-0 ; 
       add_window_border-floor10.1-0 ; 
       add_window_border-floor11.1-0 ; 
       add_window_border-floor2.1-0 ; 
       add_window_border-floor3.1-0 ; 
       add_window_border-floor4.1-0 ; 
       add_window_border-floor5.1-0 ; 
       add_window_border-floor6.1-0 ; 
       add_window_border-floor7.1-0 ; 
       add_window_border-floor8.1-0 ; 
       add_window_border-floor9.1-0 ; 
       add_window_border-from_hangar1.1-0 ; 
       add_window_border-From_Hole_Bottom1.1-0 ; 
       add_window_border-From_Hole_Bottom2.1-0 ; 
       add_window_border-from_space1.1-0 ; 
       add_window_border-light_on_Door1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       add_window_border-Ceiling_Metal_Bars.1-0 ; 
       add_window_border-Ceiling1.1-0 ; 
       add_window_border-Constant_Black1.1-0 ; 
       add_window_border-Floor2.1-0 ; 
       add_window_border-mat13.1-0 ; 
       add_window_border-mat14.1-0 ; 
       add_window_border-mat15.1-0 ; 
       add_window_border-mat16.1-0 ; 
       add_window_border-mat17.1-0 ; 
       add_window_border-mat18.1-0 ; 
       add_window_border-mat21.1-0 ; 
       add_window_border-mat22.1-0 ; 
       add_window_border-mat23.1-0 ; 
       add_window_border-mat24.1-0 ; 
       add_window_border-mat25.1-0 ; 
       add_window_border-mat26.1-0 ; 
       add_window_border-mat4.1-0 ; 
       add_window_border-mat5.1-0 ; 
       add_window_border-mat6.1-0 ; 
       add_window_border-mat7.1-0 ; 
       add_window_border-mat8.1-0 ; 
       add_window_border-Metal_Bars.1-0 ; 
       add_window_border-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 214     
       add_window_border-bionic_volume.1-0 ROOT ; 
       add_window_border-ceiling_light_1.1-0 ROOT ; 
       add_window_border-cube1.1-0 ROOT ; 
       add_window_border-cube10.1-0 ; 
       add_window_border-cube11.1-0 ; 
       add_window_border-cube12.1-0 ; 
       add_window_border-cube17.1-0 ; 
       add_window_border-cube18.1-0 ; 
       add_window_border-cube19.1-0 ; 
       add_window_border-cube2.1-0 ; 
       add_window_border-cube20.1-0 ; 
       add_window_border-cube21.1-0 ; 
       add_window_border-cube22.1-0 ; 
       add_window_border-cube23.1-0 ; 
       add_window_border-cube24.1-0 ; 
       add_window_border-cube25.1-0 ; 
       add_window_border-cube26.1-0 ; 
       add_window_border-cube27.1-0 ; 
       add_window_border-cube28.1-0 ; 
       add_window_border-cube29.1-0 ; 
       add_window_border-cube3.1-0 ; 
       add_window_border-cube30.1-0 ; 
       add_window_border-cube31.1-0 ; 
       add_window_border-cube32.1-0 ROOT ; 
       add_window_border-cube33.1-0 ; 
       add_window_border-cube34.1-0 ; 
       add_window_border-cube34_1.1-0 ; 
       add_window_border-cube34_2.1-0 ROOT ; 
       add_window_border-cube35.1-0 ; 
       add_window_border-cube36.1-0 ; 
       add_window_border-cube37.1-0 ; 
       add_window_border-cube38.1-0 ; 
       add_window_border-cube39.1-0 ; 
       add_window_border-cube4.1-0 ; 
       add_window_border-cube40.1-0 ; 
       add_window_border-cube41.1-0 ; 
       add_window_border-cube42.1-0 ; 
       add_window_border-cube43.1-0 ; 
       add_window_border-cube44.1-0 ; 
       add_window_border-cube45.1-0 ; 
       add_window_border-cube46.1-0 ; 
       add_window_border-cube47.1-0 ; 
       add_window_border-cube48.1-0 ; 
       add_window_border-cube49.1-0 ; 
       add_window_border-cube5.1-0 ; 
       add_window_border-cube50.1-0 ; 
       add_window_border-cube51.1-0 ; 
       add_window_border-cube52.1-0 ; 
       add_window_border-cube53.1-0 ; 
       add_window_border-cube54.1-0 ; 
       add_window_border-cube55.1-0 ; 
       add_window_border-cube56.1-0 ; 
       add_window_border-cube57.1-0 ; 
       add_window_border-cube58.1-0 ; 
       add_window_border-cube59.1-0 ; 
       add_window_border-cube6.1-0 ; 
       add_window_border-cube60.1-0 ; 
       add_window_border-cube61.1-0 ; 
       add_window_border-cube62.1-0 ; 
       add_window_border-cube63.1-0 ; 
       add_window_border-cube64.1-0 ; 
       add_window_border-cube65.1-0 ; 
       add_window_border-cube66.1-0 ; 
       add_window_border-cube67.1-0 ; 
       add_window_border-cube68.1-0 ; 
       add_window_border-cube7.1-0 ; 
       add_window_border-cube8.1-0 ; 
       add_window_border-cube9.1-0 ; 
       add_window_border-cyl20.1-0 ; 
       add_window_border-cyl21.1-0 ; 
       add_window_border-cyl22.1-0 ROOT ; 
       add_window_border-cyl3.1-0 ; 
       add_window_border-Door.1-0 ROOT ; 
       add_window_border-face1.1-0 ; 
       add_window_border-grid1.1-0 ; 
       add_window_border-grid2.1-0 ; 
       add_window_border-grid3.1-0 ; 
       add_window_border-grid4.1-0 ROOT ; 
       add_window_border-hangar_force_field.1-0 ROOT ; 
       add_window_border-null1.1-0 ; 
       add_window_border-null13.1-0 ROOT ; 
       add_window_border-null14.1-0 ROOT ; 
       add_window_border-null2.1-0 ROOT ; 
       add_window_border-null8.1-0 ROOT ; 
       Ceiling_Bars-beam1.1-0 ; 
       Ceiling_Bars-beam144.1-0 ; 
       Ceiling_Bars-beam145.1-0 ; 
       Ceiling_Bars-beam146.1-0 ; 
       Ceiling_Bars-beam147.1-0 ; 
       Ceiling_Bars-beam148.1-0 ; 
       Ceiling_Bars-beam149.1-0 ; 
       Ceiling_Bars-beam150.1-0 ; 
       Ceiling_Bars-beam151.1-0 ; 
       Ceiling_Bars-beam152.1-0 ; 
       Ceiling_Bars-beam153.1-0 ; 
       Ceiling_Bars-beam154.1-0 ; 
       Ceiling_Bars-beam155.1-0 ; 
       Ceiling_Bars-beam156.1-0 ; 
       Ceiling_Bars-beam157.1-0 ; 
       Ceiling_Bars-beam158.1-0 ; 
       Ceiling_Bars-beam159.1-0 ; 
       Ceiling_Bars-beam196.1-0 ; 
       Ceiling_Bars-beam197.1-0 ; 
       Ceiling_Bars-beam198.1-0 ; 
       Ceiling_Bars-beam199.1-0 ; 
       Ceiling_Bars-beam200.1-0 ; 
       Ceiling_Bars-beam201.1-0 ; 
       Ceiling_Bars-beam202.1-0 ; 
       Ceiling_Bars-beam203.1-0 ; 
       Ceiling_Bars-beam204.1-0 ; 
       Ceiling_Bars-beam205.1-0 ; 
       Ceiling_Bars-beam206.1-0 ; 
       Ceiling_Bars-beam207.1-0 ; 
       Ceiling_Bars-beam208.1-0 ; 
       Ceiling_Bars-beam209.1-0 ; 
       Ceiling_Bars-beam210.1-0 ; 
       Ceiling_Bars-beam211.1-0 ; 
       Ceiling_Bars-beam212.1-0 ; 
       Ceiling_Bars-beam213.1-0 ; 
       Ceiling_Bars-beam214.1-0 ; 
       Ceiling_Bars-null12.1-0 ; 
       Ceiling_Bars-Root.2-0 ROOT ; 
       Ceiling_Bars-torus15.1-0 ; 
       Ceiling_Bars-torus19.1-0 ; 
       Ceiling_Bars-torus21.1-0 ; 
       Ceiling_Bars-torus23.1-0 ; 
       Ceiling_Bars-torus24.1-0 ; 
       Ceiling_Bars-torus27.1-0 ; 
       Ceiling_Bars-torus28.1-0 ; 
       Ceiling_Bars-torus29.1-0 ; 
       Ceiling_Bars-torus30.1-0 ; 
       Ceiling_Bars-torus31.1-0 ; 
       Ceiling_Bars-torus32.1-0 ; 
       celing_lights-torus10.1-0 ; 
       celing_lights-tubes.6-0 ROOT ; 
       Floor_Bars-beam1.1-0 ; 
       Floor_Bars-beam144.1-0 ; 
       Floor_Bars-beam145.1-0 ; 
       Floor_Bars-beam146.1-0 ; 
       Floor_Bars-beam147.1-0 ; 
       Floor_Bars-beam148.1-0 ; 
       Floor_Bars-beam149.1-0 ; 
       Floor_Bars-beam150.1-0 ; 
       Floor_Bars-beam151.1-0 ; 
       Floor_Bars-beam152.1-0 ; 
       Floor_Bars-beam153.1-0 ; 
       Floor_Bars-beam154.1-0 ; 
       Floor_Bars-beam155.1-0 ; 
       Floor_Bars-beam156.1-0 ; 
       Floor_Bars-beam157.1-0 ; 
       Floor_Bars-beam158.1-0 ; 
       Floor_Bars-beam159.1-0 ; 
       Floor_Bars-beam196.1-0 ; 
       Floor_Bars-beam197.1-0 ; 
       Floor_Bars-beam198.1-0 ; 
       Floor_Bars-beam199.1-0 ; 
       Floor_Bars-beam200.1-0 ; 
       Floor_Bars-beam201.1-0 ; 
       Floor_Bars-beam202.1-0 ; 
       Floor_Bars-beam203.1-0 ; 
       Floor_Bars-beam204.1-0 ; 
       Floor_Bars-beam205.1-0 ; 
       Floor_Bars-beam206.1-0 ; 
       Floor_Bars-beam207.1-0 ; 
       Floor_Bars-beam208.1-0 ; 
       Floor_Bars-beam209.1-0 ; 
       Floor_Bars-beam210.1-0 ; 
       Floor_Bars-beam211.1-0 ; 
       Floor_Bars-beam212.1-0 ; 
       Floor_Bars-beam213.1-0 ; 
       Floor_Bars-beam214.1-0 ; 
       Floor_Bars-null12.1-0 ; 
       Floor_Bars-Root.2-0 ROOT ; 
       Floor_Bars-torus15.1-0 ; 
       Floor_Bars-torus19.1-0 ; 
       Floor_Bars-torus21.1-0 ; 
       Floor_Bars-torus23.1-0 ; 
       Floor_Bars-torus24.1-0 ; 
       Floor_Bars-torus27.1-0 ; 
       Floor_Bars-torus28.1-0 ; 
       Floor_Bars-torus29.1-0 ; 
       Floor_Bars-torus30.1-0 ; 
       Floor_Bars-torus31.1-0 ; 
       Floor_Bars-torus32.1-0 ; 
       floor_grate1-cube34.5-0 ROOT ; 
       Monitor-base.9-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.7-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Monitor6-base.9-0 ROOT ; 
       Monitor6-Screen.1-0 ; 
       Monitor7-base.8-0 ROOT ; 
       Monitor7-Screen.1-0 ; 
       Monitor8-base.9-0 ROOT ; 
       Monitor8-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-border.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.23-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       spock_viewer-cube2.1-0 ; 
       spock_viewer-nurbs1.1-0 ; 
       spock_viewer-skin1.1-0 ; 
       spock_viewer-spock_viewer.5-0 ROOT ; 
       spock_viewer-torus2.1-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-nurbs1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       add_window_border-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/radar ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-add_window_border.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       add_window_border-t2d1.1-0 ; 
       add_window_border-t2d2.1-0 ; 
       add_window_border-t2d3.1-0 ; 
       add_window_border-t2d4.1-0 ; 
       add_window_border-t2d5.1-0 ; 
       add_window_border-t2d7.1-0 ; 
       add_window_border-t2d8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       add_window_border-Bionic_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       84 120 110 ; 
       85 120 110 ; 
       86 120 110 ; 
       87 120 110 ; 
       88 120 110 ; 
       89 120 110 ; 
       90 120 110 ; 
       91 120 110 ; 
       92 120 110 ; 
       93 120 110 ; 
       94 120 110 ; 
       95 120 110 ; 
       96 120 110 ; 
       97 120 110 ; 
       98 120 110 ; 
       99 120 110 ; 
       100 120 110 ; 
       101 120 110 ; 
       102 120 110 ; 
       103 120 110 ; 
       104 120 110 ; 
       105 120 110 ; 
       106 120 110 ; 
       107 120 110 ; 
       108 120 110 ; 
       109 120 110 ; 
       110 120 110 ; 
       111 120 110 ; 
       112 120 110 ; 
       113 120 110 ; 
       114 120 110 ; 
       115 120 110 ; 
       116 120 110 ; 
       117 120 110 ; 
       118 120 110 ; 
       119 120 110 ; 
       120 121 110 ; 
       122 121 110 ; 
       123 121 110 ; 
       124 121 110 ; 
       125 121 110 ; 
       126 121 110 ; 
       127 121 110 ; 
       128 121 110 ; 
       129 121 110 ; 
       130 121 110 ; 
       131 121 110 ; 
       132 121 110 ; 
       133 134 110 ; 
       135 171 110 ; 
       136 171 110 ; 
       137 171 110 ; 
       138 171 110 ; 
       139 171 110 ; 
       140 171 110 ; 
       141 171 110 ; 
       142 171 110 ; 
       143 171 110 ; 
       144 171 110 ; 
       145 171 110 ; 
       146 171 110 ; 
       147 171 110 ; 
       148 171 110 ; 
       149 171 110 ; 
       150 171 110 ; 
       151 171 110 ; 
       152 171 110 ; 
       153 171 110 ; 
       154 171 110 ; 
       155 171 110 ; 
       156 171 110 ; 
       157 171 110 ; 
       158 171 110 ; 
       159 171 110 ; 
       160 171 110 ; 
       161 171 110 ; 
       162 171 110 ; 
       163 171 110 ; 
       164 171 110 ; 
       165 171 110 ; 
       166 171 110 ; 
       167 171 110 ; 
       168 171 110 ; 
       169 171 110 ; 
       170 171 110 ; 
       171 172 110 ; 
       173 172 110 ; 
       174 172 110 ; 
       175 172 110 ; 
       176 172 110 ; 
       177 172 110 ; 
       178 172 110 ; 
       179 172 110 ; 
       180 172 110 ; 
       181 172 110 ; 
       182 172 110 ; 
       183 172 110 ; 
       186 185 110 ; 
       188 187 110 ; 
       190 189 110 ; 
       192 191 110 ; 
       194 193 110 ; 
       195 197 110 ; 
       197 201 110 ; 
       198 203 110 ; 
       199 201 110 ; 
       200 199 110 ; 
       202 199 110 ; 
       203 201 110 ; 
       204 207 110 ; 
       205 208 110 ; 
       206 207 110 ; 
       208 207 110 ; 
       209 213 110 ; 
       210 213 110 ; 
       211 210 110 ; 
       212 213 110 ; 
       3 79 110 ; 
       4 79 110 ; 
       5 79 110 ; 
       6 79 110 ; 
       7 79 110 ; 
       8 79 110 ; 
       9 79 110 ; 
       10 79 110 ; 
       11 79 110 ; 
       12 79 110 ; 
       13 79 110 ; 
       14 79 110 ; 
       15 79 110 ; 
       16 79 110 ; 
       17 79 110 ; 
       18 79 110 ; 
       19 79 110 ; 
       20 79 110 ; 
       21 79 110 ; 
       22 79 110 ; 
       24 82 110 ; 
       25 81 110 ; 
       26 80 110 ; 
       28 80 110 ; 
       29 80 110 ; 
       30 80 110 ; 
       31 80 110 ; 
       32 80 110 ; 
       33 79 110 ; 
       34 80 110 ; 
       35 80 110 ; 
       36 80 110 ; 
       37 80 110 ; 
       38 80 110 ; 
       39 80 110 ; 
       40 80 110 ; 
       41 80 110 ; 
       42 80 110 ; 
       43 80 110 ; 
       44 79 110 ; 
       45 80 110 ; 
       46 80 110 ; 
       47 81 110 ; 
       48 81 110 ; 
       49 81 110 ; 
       50 81 110 ; 
       51 81 110 ; 
       52 81 110 ; 
       53 81 110 ; 
       54 81 110 ; 
       55 79 110 ; 
       56 81 110 ; 
       57 81 110 ; 
       58 81 110 ; 
       59 81 110 ; 
       60 81 110 ; 
       61 81 110 ; 
       62 81 110 ; 
       63 81 110 ; 
       64 81 110 ; 
       65 79 110 ; 
       66 79 110 ; 
       67 79 110 ; 
       68 77 110 ; 
       69 71 110 ; 
       71 77 110 ; 
       73 72 110 ; 
       74 82 110 ; 
       75 82 110 ; 
       76 82 110 ; 
       79 83 110 ; 
       196 203 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       121 0 300 ; 
       134 11 300 ; 
       172 21 300 ; 
       185 17 300 ; 
       186 18 300 ; 
       187 19 300 ; 
       188 20 300 ; 
       189 4 300 ; 
       190 5 300 ; 
       191 6 300 ; 
       192 7 300 ; 
       193 8 300 ; 
       194 9 300 ; 
       195 2 300 ; 
       197 1 300 ; 
       200 3 300 ; 
       202 3 300 ; 
       203 22 300 ; 
       205 15 300 ; 
       207 14 300 ; 
       0 16 300 ; 
       1 10 300 ; 
       78 12 300 ; 
       81 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       186 0 400 ; 
       188 1 400 ; 
       190 2 400 ; 
       192 3 400 ; 
       194 4 400 ; 
       205 6 400 ; 
       1 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       4 5 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       12 13 2110 ; 
       0 1 2110 ; 
       14 15 2110 ; 
       16 17 2110 ; 
       18 19 2110 ; 
       20 21 2110 ; 
       22 23 2110 ; 
       24 25 2110 ; 
       26 27 2110 ; 
       28 29 2110 ; 
       30 31 2110 ; 
       32 33 2110 ; 
       34 35 2110 ; 
       36 37 2110 ; 
       38 39 2110 ; 
       40 41 2110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       5 70 2200 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       16 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       5 14 550 ; 
       11 0 550 ; 
       1 15 550 ; 
       15 1 550 ; 
       17 11 550 ; 
       19 2 550 ; 
       21 3 550 ; 
       23 4 550 ; 
       25 5 550 ; 
       27 6 550 ; 
       29 7 550 ; 
       31 8 550 ; 
       33 9 550 ; 
       35 10 550 ; 
       37 12 550 ; 
       39 16 550 ; 
       41 13 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 78 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 14 551 1 ; 
       0 16 551 1 ; 
       0 18 551 1 ; 
       0 20 551 1 ; 
       0 22 551 1 ; 
       0 24 551 1 ; 
       0 26 551 1 ; 
       0 28 551 1 ; 
       0 30 551 1 ; 
       0 32 551 1 ; 
       0 34 551 1 ; 
       0 36 551 1 ; 
       0 38 551 1 ; 
       0 40 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 360 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 407.5 -0.634279 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 407.5 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 365 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 400 -0.634279 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 400 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 402.5 -0.634279 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 402.5 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 355 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 405 -0.634279 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 405 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 362.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 362.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 395 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 367.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 367.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 387.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 387.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 390 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 390 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 392.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 392.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 370 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM 370 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       24 SCHEM 372.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 372.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       26 SCHEM 375 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 375 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 377.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       29 SCHEM 377.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       30 SCHEM 380 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       31 SCHEM 380 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       32 SCHEM 382.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       33 SCHEM 382.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       34 SCHEM 385 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       35 SCHEM 385 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       36 SCHEM 357.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 357.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       38 SCHEM 397.5 -0.6342791 0 WIRECOL 7 7 MPRFLG 0 ; 
       39 SCHEM 397.5 1.365721 0 WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 352.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       41 SCHEM 352.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       84 SCHEM 467.9645 -3.975205 1 MPRFLG 0 ; 
       85 SCHEM 427.9645 -3.975205 1 MPRFLG 0 ; 
       86 SCHEM 430.4645 -3.975205 1 MPRFLG 0 ; 
       87 SCHEM 432.9645 -3.975205 1 MPRFLG 0 ; 
       88 SCHEM 435.4645 -3.975205 1 MPRFLG 0 ; 
       89 SCHEM 437.9645 -3.975205 1 MPRFLG 0 ; 
       90 SCHEM 440.4645 -3.975205 1 MPRFLG 0 ; 
       91 SCHEM 442.9645 -3.975205 1 MPRFLG 0 ; 
       92 SCHEM 445.4645 -3.975205 1 MPRFLG 0 ; 
       93 SCHEM 447.9645 -3.975205 1 MPRFLG 0 ; 
       94 SCHEM 450.4645 -3.975205 1 MPRFLG 0 ; 
       95 SCHEM 452.9645 -3.975205 1 MPRFLG 0 ; 
       96 SCHEM 455.4645 -3.975205 1 MPRFLG 0 ; 
       97 SCHEM 457.9645 -3.975205 1 MPRFLG 0 ; 
       98 SCHEM 460.4645 -3.975205 1 MPRFLG 0 ; 
       99 SCHEM 462.9645 -3.975205 1 MPRFLG 0 ; 
       100 SCHEM 465.4645 -3.975205 1 MPRFLG 0 ; 
       101 SCHEM 470.4645 -3.975205 1 MPRFLG 0 ; 
       102 SCHEM 472.9645 -3.975205 1 MPRFLG 0 ; 
       103 SCHEM 475.4645 -3.975205 1 MPRFLG 0 ; 
       104 SCHEM 477.9645 -3.975205 1 MPRFLG 0 ; 
       105 SCHEM 480.4645 -3.975205 1 MPRFLG 0 ; 
       106 SCHEM 482.9645 -3.975205 1 MPRFLG 0 ; 
       107 SCHEM 485.4645 -3.975205 1 MPRFLG 0 ; 
       108 SCHEM 487.9645 -3.975205 1 MPRFLG 0 ; 
       109 SCHEM 490.4645 -3.975205 1 MPRFLG 0 ; 
       110 SCHEM 492.9645 -3.975205 1 MPRFLG 0 ; 
       111 SCHEM 495.4645 -3.975205 1 MPRFLG 0 ; 
       112 SCHEM 497.9645 -3.975205 1 MPRFLG 0 ; 
       113 SCHEM 500.4645 -3.975205 1 MPRFLG 0 ; 
       114 SCHEM 502.9645 -3.975205 1 MPRFLG 0 ; 
       115 SCHEM 505.4646 -3.975205 1 MPRFLG 0 ; 
       116 SCHEM 507.9646 -3.975205 1 MPRFLG 0 ; 
       117 SCHEM 510.4646 -3.975205 1 MPRFLG 0 ; 
       118 SCHEM 512.9646 -3.975205 1 MPRFLG 0 ; 
       119 SCHEM 515.4646 -3.975205 1 MPRFLG 0 ; 
       120 SCHEM 471.7145 -1.975205 1 MPRFLG 0 ; 
       121 SCHEM 147.1959 -11.39112 1 USR SRT 1 1 1 0 0 0 0 35.92136 0 MPRFLG 0 ; 
       122 SCHEM 400.4645 -1.975205 1 MPRFLG 0 ; 
       123 SCHEM 402.9645 -1.975205 1 MPRFLG 0 ; 
       124 SCHEM 405.4645 -1.975205 1 MPRFLG 0 ; 
       125 SCHEM 407.9645 -1.975205 1 MPRFLG 0 ; 
       126 SCHEM 410.4645 -1.975205 1 MPRFLG 0 ; 
       127 SCHEM 412.9645 -1.975205 1 MPRFLG 0 ; 
       128 SCHEM 415.4645 -1.975205 1 MPRFLG 0 ; 
       129 SCHEM 417.9645 -1.975205 1 MPRFLG 0 ; 
       130 SCHEM 420.4645 -1.975205 1 MPRFLG 0 ; 
       131 SCHEM 422.9645 -1.975205 1 MPRFLG 0 ; 
       132 SCHEM 425.4645 -1.975205 1 MPRFLG 0 ; 
       133 SCHEM 112.5 -2 0 MPRFLG 0 ; 
       134 SCHEM 112.5 0 0 SRT 1 1 1 0 -0.4 0 0 17.30421 0 MPRFLG 0 ; 
       135 SCHEM 212.4255 -15.38843 1 MPRFLG 0 ; 
       136 SCHEM 172.4255 -15.38843 1 MPRFLG 0 ; 
       137 SCHEM 174.9255 -15.38843 1 MPRFLG 0 ; 
       138 SCHEM 177.4255 -15.38843 1 MPRFLG 0 ; 
       139 SCHEM 179.9255 -15.38843 1 MPRFLG 0 ; 
       140 SCHEM 182.4255 -15.38843 1 MPRFLG 0 ; 
       141 SCHEM 184.9255 -15.38843 1 MPRFLG 0 ; 
       142 SCHEM 187.4255 -15.38843 1 MPRFLG 0 ; 
       143 SCHEM 189.9255 -15.38843 1 MPRFLG 0 ; 
       144 SCHEM 192.4255 -15.38843 1 MPRFLG 0 ; 
       145 SCHEM 194.9255 -15.38843 1 MPRFLG 0 ; 
       146 SCHEM 197.4255 -15.38843 1 MPRFLG 0 ; 
       147 SCHEM 199.9255 -15.38843 1 MPRFLG 0 ; 
       148 SCHEM 202.4255 -15.38843 1 MPRFLG 0 ; 
       149 SCHEM 204.9255 -15.38843 1 MPRFLG 0 ; 
       150 SCHEM 207.4255 -15.38843 1 MPRFLG 0 ; 
       151 SCHEM 209.9255 -15.38843 1 MPRFLG 0 ; 
       152 SCHEM 214.9255 -15.38843 1 MPRFLG 0 ; 
       153 SCHEM 217.4255 -15.38843 1 MPRFLG 0 ; 
       154 SCHEM 219.9255 -15.38843 1 MPRFLG 0 ; 
       155 SCHEM 222.4255 -15.38843 1 MPRFLG 0 ; 
       156 SCHEM 224.9255 -15.38843 1 MPRFLG 0 ; 
       157 SCHEM 227.4255 -15.38843 1 MPRFLG 0 ; 
       158 SCHEM 229.9255 -15.38843 1 MPRFLG 0 ; 
       159 SCHEM 232.4255 -15.38843 1 MPRFLG 0 ; 
       160 SCHEM 234.9255 -15.38843 1 MPRFLG 0 ; 
       161 SCHEM 237.4255 -15.38843 1 MPRFLG 0 ; 
       162 SCHEM 239.9255 -15.38843 1 MPRFLG 0 ; 
       163 SCHEM 242.4255 -15.38843 1 MPRFLG 0 ; 
       164 SCHEM 244.9255 -15.38843 1 MPRFLG 0 ; 
       165 SCHEM 247.4255 -15.38843 1 MPRFLG 0 ; 
       166 SCHEM 249.9255 -15.38843 1 MPRFLG 0 ; 
       167 SCHEM 252.4255 -15.38843 1 MPRFLG 0 ; 
       168 SCHEM 254.9255 -15.38843 1 MPRFLG 0 ; 
       169 SCHEM 257.4255 -15.38843 1 MPRFLG 0 ; 
       170 SCHEM 259.9255 -15.38843 1 MPRFLG 0 ; 
       171 SCHEM 216.1755 -13.38843 1 MPRFLG 0 ; 
       172 SCHEM 144.9255 -11.38843 1 USR SRT 1 1 1 0 0 0 0 -0.5 0 MPRFLG 0 ; 
       173 SCHEM 144.9255 -13.38843 1 MPRFLG 0 ; 
       174 SCHEM 147.4255 -13.38843 1 MPRFLG 0 ; 
       175 SCHEM 149.9255 -13.38843 1 MPRFLG 0 ; 
       176 SCHEM 152.4255 -13.38843 1 MPRFLG 0 ; 
       177 SCHEM 154.9255 -13.38843 1 MPRFLG 0 ; 
       178 SCHEM 157.4255 -13.38843 1 MPRFLG 0 ; 
       179 SCHEM 159.9255 -13.38843 1 MPRFLG 0 ; 
       180 SCHEM 162.4255 -13.38843 1 MPRFLG 0 ; 
       181 SCHEM 164.9255 -13.38843 1 MPRFLG 0 ; 
       182 SCHEM 167.4255 -13.38843 1 MPRFLG 0 ; 
       183 SCHEM 169.9255 -13.38843 1 MPRFLG 0 ; 
       184 SCHEM 295 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       185 SCHEM 95 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       186 SCHEM 95 -2 0 MPRFLG 0 ; 
       187 SCHEM 97.5 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       188 SCHEM 97.5 -2 0 MPRFLG 0 ; 
       189 SCHEM 100 0 0 SRT 1.471141 0.9625919 0.9625919 0.4977454 0.5151278 -0.00364445 8.855516 6.308922 9.169995 MPRFLG 0 ; 
       190 SCHEM 100 -2 0 MPRFLG 0 ; 
       191 SCHEM 102.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3516835 0.2844428 0.001876728 10.20939 -8.125242 8.293537 MPRFLG 0 ; 
       192 SCHEM 102.5 -2 0 MPRFLG 0 ; 
       193 SCHEM 105 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3081128 -0.06048232 -3.158986 -5.119568 5.063054 13.15584 MPRFLG 0 ; 
       194 SCHEM 105 -2 0 MPRFLG 0 ; 
       195 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       197 SCHEM 122.5 -2 0 MPRFLG 0 ; 
       198 SCHEM 128.75 -4 0 MPRFLG 0 ; 
       199 SCHEM 118.75 -2 0 MPRFLG 0 ; 
       200 SCHEM 117.5 -4 0 MPRFLG 0 ; 
       201 SCHEM 121.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       202 SCHEM 120 -4 0 MPRFLG 0 ; 
       203 SCHEM 128.75 -2 0 MPRFLG 0 ; 
       204 SCHEM 345 -2 0 MPRFLG 0 ; 
       205 SCHEM 347.5 -4 0 MPRFLG 0 ; 
       206 SCHEM 350 -2 0 MPRFLG 0 ; 
       207 SCHEM 347.5 0 0 SRT 1 1 1 0.6699998 0.152 0 -2.159692 -10.97795 12.63122 MPRFLG 0 ; 
       208 SCHEM 347.5 -2 0 MPRFLG 0 ; 
       209 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       210 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       211 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       212 SCHEM 5 -2 0 MPRFLG 0 ; 
       213 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 352.6028 1.365721 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 110 0 0 SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       2 SCHEM 10 0 0 SRT 1 1 1 0 -0.8726646 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -4 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 55 -4 0 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 MPRFLG 0 ; 
       7 SCHEM 60 -4 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 65 -4 0 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 70 -4 0 MPRFLG 0 ; 
       13 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 75 -4 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 80 -4 0 MPRFLG 0 ; 
       17 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 85 -4 0 MPRFLG 0 ; 
       19 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 90 -4 0 MPRFLG 0 ; 
       22 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 12.5 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       24 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       25 SCHEM 320 -2 0 MPRFLG 0 ; 
       26 SCHEM 272.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 247.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 252.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 255 -2 0 MPRFLG 0 ; 
       30 SCHEM 257.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 260 -2 0 MPRFLG 0 ; 
       32 SCHEM 262.5 -2 0 MPRFLG 0 ; 
       33 SCHEM 35 -4 0 MPRFLG 0 ; 
       34 SCHEM 265 -2 0 MPRFLG 0 ; 
       35 SCHEM 267.5 -2 0 MPRFLG 0 ; 
       36 SCHEM 270 -2 0 MPRFLG 0 ; 
       37 SCHEM 250 -2 0 MPRFLG 0 ; 
       38 SCHEM 275 -2 0 MPRFLG 0 ; 
       39 SCHEM 277.5 -2 0 MPRFLG 0 ; 
       40 SCHEM 280 -2 0 MPRFLG 0 ; 
       41 SCHEM 282.5 -2 0 MPRFLG 0 ; 
       42 SCHEM 285 -2 0 MPRFLG 0 ; 
       43 SCHEM 287.5 -2 0 MPRFLG 0 ; 
       44 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       45 SCHEM 290 -2 0 MPRFLG 0 ; 
       46 SCHEM 292.5 -2 0 MPRFLG 0 ; 
       47 SCHEM 300 -2 0 MPRFLG 0 ; 
       48 SCHEM 302.5 -2 0 MPRFLG 0 ; 
       49 SCHEM 305 -2 0 MPRFLG 0 ; 
       50 SCHEM 307.5 -2 0 MPRFLG 0 ; 
       51 SCHEM 310 -2 0 MPRFLG 0 ; 
       52 SCHEM 312.5 -2 0 MPRFLG 0 ; 
       53 SCHEM 315 -2 0 MPRFLG 0 ; 
       54 SCHEM 317.5 -2 0 MPRFLG 0 ; 
       55 SCHEM 40 -4 0 MPRFLG 0 ; 
       56 SCHEM 297.5 -2 0 MPRFLG 0 ; 
       57 SCHEM 322.5 -2 0 MPRFLG 0 ; 
       58 SCHEM 325 -2 0 MPRFLG 0 ; 
       59 SCHEM 327.5 -2 0 MPRFLG 0 ; 
       60 SCHEM 330 -2 0 MPRFLG 0 ; 
       61 SCHEM 332.5 -2 0 MPRFLG 0 ; 
       62 SCHEM 335 -2 0 MPRFLG 0 ; 
       63 SCHEM 337.5 -2 0 MPRFLG 0 ; 
       64 SCHEM 340 -2 0 MPRFLG 0 ; 
       65 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       66 SCHEM 45 -4 0 MPRFLG 0 ; 
       67 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       68 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       69 SCHEM 15 -4 0 MPRFLG 0 ; 
       70 SCHEM 107.5 0 0 SRT 6.050002 11.52707 6.050002 0 0 0 0 0 0 MPRFLG 0 ; 
       71 SCHEM 15 -2 0 MPRFLG 0 ; 
       72 SCHEM 342.5 0 0 SRT 1.082 1.082 1.082 0 -1.34 0 51.55545 0 -14.8918 MPRFLG 0 ; 
       73 SCHEM 342.5 -2 0 MPRFLG 0 ; 
       74 SCHEM 20 -2 0 MPRFLG 0 ; 
       75 SCHEM 25 -2 0 MPRFLG 0 ; 
       76 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       77 SCHEM 16.25 0 0 SRT 1.42 1 2.037903 0 0.2792527 0 67.10641 -18.64181 -18.7254 MPRFLG 0 ; 
       78 SCHEM 115 0 0 SRT 1 1 1 0 0 0 147.0213 -22.28944 -177.9266 MPRFLG 0 ; 
       79 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       80 SCHEM 271.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       81 SCHEM 318.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       82 SCHEM 23.75 0 0 SRT 1.166 1.66738 1.166 0 0 0 181.1734 -0.0535363 -128.4584 MPRFLG 0 ; 
       83 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       196 SCHEM 126.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 413.4329 2.63785 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 124 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 121.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 116.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 101.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 104 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 106.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 109 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 114 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 114 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 341.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 351.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 346.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 154 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 96.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 99 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 109.0895 -10.30617 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 94 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 96.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 99 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 101.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 104 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 109 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 346.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       15 SCHEM 0 0 0 ; 
       16 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
