SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_window_pane-cam_int1.3-0 ROOT ; 
       add_window_pane-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 43     
       add_window_pane-from_space_to_hangar2.1-0 ; 
       add_window_pane-from_space_to_hangar2_int.3-0 ROOT ; 
       add_window_pane-spocklight1.3-0 ROOT ; 
       bounce-bounce_off_beam.47-0 ROOT ; 
       Fill-Ceiling.1-0 ; 
       Fill-Ceiling_int.14-0 ROOT ; 
       fill-inside_hangar_fill1.43-0 ROOT ; 
       Fill-Shine_on_backwall.1-0 ; 
       Fill-Shine_on_backwall_int.16-0 ROOT ; 
       Fill-Shine_on_sidewall.1-0 ; 
       Fill-Shine_on_sidewall_int.15-0 ROOT ; 
       Glass-Reflector.1-0 ROOT ; 
       Point-Alarm1.50-0 ROOT ; 
       Spot-hilight_logo.1-0 ; 
       Spot-hilight_logo_int.14-0 ROOT ; 
       Vol-floor1.1-0 ; 
       Vol-floor1_int.30-0 ROOT ; 
       Vol-floor10.1-0 ; 
       Vol-floor10_int.27-0 ROOT ; 
       Vol-floor11.1-0 ; 
       Vol-floor11_int.27-0 ROOT ; 
       Vol-floor12.1-0 ; 
       Vol-floor12_int.27-0 ROOT ; 
       Vol-floor3.1-0 ; 
       Vol-floor3_int.30-0 ROOT ; 
       Vol-floor4.1-0 ; 
       Vol-floor4_int.30-0 ROOT ; 
       Vol-floor5.1-0 ; 
       Vol-floor5_int.30-0 ROOT ; 
       Vol-floor6.1-0 ; 
       Vol-floor6_int.30-0 ROOT ; 
       Vol-floor7.1-0 ; 
       Vol-floor7_int.30-0 ROOT ; 
       Vol-floor8.1-0 ; 
       Vol-floor8_int.27-0 ROOT ; 
       Vol-floor9.1-0 ; 
       Vol-floor9_int.27-0 ROOT ; 
       Vol-from_hangar1.1-0 ; 
       Vol-from_hangar1_int.30-0 ROOT ; 
       Vol-Shine_on_Door.1-0 ; 
       Vol-Shine_on_Door_int.17-0 ROOT ; 
       Vol-Under_Hole.1-0 ; 
       Vol-Under_Hole_int.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 17     
       add_window_pane-alarm1.1-0 ; 
       add_window_pane-floor1.1-0 ; 
       add_window_pane-floor10.1-0 ; 
       add_window_pane-floor11.1-0 ; 
       add_window_pane-floor2.1-0 ; 
       add_window_pane-floor3.1-0 ; 
       add_window_pane-floor4.1-0 ; 
       add_window_pane-floor5.1-0 ; 
       add_window_pane-floor6.1-0 ; 
       add_window_pane-floor7.1-0 ; 
       add_window_pane-floor8.1-0 ; 
       add_window_pane-floor9.1-0 ; 
       add_window_pane-from_hangar1.1-0 ; 
       add_window_pane-From_Hole_Bottom1.1-0 ; 
       add_window_pane-From_Hole_Bottom2.1-0 ; 
       add_window_pane-from_space1.1-0 ; 
       add_window_pane-light_on_Door1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       add_window_pane-Ceiling_Metal_Bars.1-0 ; 
       add_window_pane-Ceiling1.1-0 ; 
       add_window_pane-Constant_Black1.1-0 ; 
       add_window_pane-Floor2.1-0 ; 
       add_window_pane-mat13.1-0 ; 
       add_window_pane-mat14.1-0 ; 
       add_window_pane-mat15.1-0 ; 
       add_window_pane-mat16.1-0 ; 
       add_window_pane-mat17.1-0 ; 
       add_window_pane-mat18.1-0 ; 
       add_window_pane-mat21.1-0 ; 
       add_window_pane-mat23.1-0 ; 
       add_window_pane-mat24.1-0 ; 
       add_window_pane-mat25.1-0 ; 
       add_window_pane-mat26.1-0 ; 
       add_window_pane-mat4.1-0 ; 
       add_window_pane-mat5.1-0 ; 
       add_window_pane-mat6.1-0 ; 
       add_window_pane-mat7.1-0 ; 
       add_window_pane-mat8.1-0 ; 
       add_window_pane-Metal_Bars.1-0 ; 
       add_window_pane-Wall1.1-0 ; 
       add_window_pane-window_Glass.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 210     
       add_window_pane-bionic_volume.1-0 ROOT ; 
       add_window_pane-ceiling_light_1.1-0 ROOT ; 
       add_window_pane-cube10.1-0 ; 
       add_window_pane-cube11.1-0 ; 
       add_window_pane-cube12.1-0 ; 
       add_window_pane-cube17.1-0 ; 
       add_window_pane-cube18.1-0 ; 
       add_window_pane-cube19.1-0 ; 
       add_window_pane-cube2.1-0 ; 
       add_window_pane-cube20.1-0 ; 
       add_window_pane-cube21.1-0 ; 
       add_window_pane-cube22.1-0 ; 
       add_window_pane-cube23.1-0 ; 
       add_window_pane-cube24.1-0 ; 
       add_window_pane-cube25.1-0 ; 
       add_window_pane-cube26.1-0 ; 
       add_window_pane-cube27.1-0 ; 
       add_window_pane-cube28.1-0 ; 
       add_window_pane-cube29.1-0 ; 
       add_window_pane-cube3.1-0 ; 
       add_window_pane-cube30.1-0 ; 
       add_window_pane-cube31.1-0 ; 
       add_window_pane-cube34.1-0 ; 
       add_window_pane-cube34_1.1-0 ; 
       add_window_pane-cube34_2.1-0 ROOT ; 
       add_window_pane-cube35.1-0 ; 
       add_window_pane-cube36.1-0 ; 
       add_window_pane-cube37.1-0 ; 
       add_window_pane-cube38.1-0 ; 
       add_window_pane-cube39.1-0 ; 
       add_window_pane-cube4.1-0 ; 
       add_window_pane-cube40.1-0 ; 
       add_window_pane-cube41.1-0 ; 
       add_window_pane-cube42.1-0 ; 
       add_window_pane-cube43.1-0 ; 
       add_window_pane-cube44.1-0 ; 
       add_window_pane-cube45.1-0 ; 
       add_window_pane-cube46.1-0 ; 
       add_window_pane-cube47.1-0 ; 
       add_window_pane-cube48.1-0 ; 
       add_window_pane-cube49.1-0 ; 
       add_window_pane-cube5.1-0 ; 
       add_window_pane-cube50.1-0 ; 
       add_window_pane-cube51.1-0 ; 
       add_window_pane-cube52.1-0 ; 
       add_window_pane-cube53.1-0 ; 
       add_window_pane-cube54.1-0 ; 
       add_window_pane-cube55.1-0 ; 
       add_window_pane-cube56.1-0 ; 
       add_window_pane-cube57.1-0 ; 
       add_window_pane-cube58.1-0 ; 
       add_window_pane-cube59.1-0 ; 
       add_window_pane-cube6.1-0 ; 
       add_window_pane-cube60.1-0 ; 
       add_window_pane-cube61.1-0 ; 
       add_window_pane-cube62.1-0 ; 
       add_window_pane-cube63.1-0 ; 
       add_window_pane-cube64.1-0 ; 
       add_window_pane-cube65.1-0 ; 
       add_window_pane-cube66.1-0 ; 
       add_window_pane-cube67.1-0 ; 
       add_window_pane-cube68.1-0 ; 
       add_window_pane-cube7.1-0 ; 
       add_window_pane-cube8.1-0 ; 
       add_window_pane-cube9.1-0 ; 
       add_window_pane-cyl22.2-0 ROOT ; 
       add_window_pane-Door.1-0 ROOT ; 
       add_window_pane-face1.1-0 ; 
       add_window_pane-hangar_force_field.1-0 ROOT ; 
       add_window_pane-null1.1-0 ; 
       add_window_pane-null13.1-0 ROOT ; 
       add_window_pane-null14.1-0 ROOT ; 
       add_window_pane-null8.1-0 ROOT ; 
       Ceiling_Bars-beam1.1-0 ; 
       Ceiling_Bars-beam144.1-0 ; 
       Ceiling_Bars-beam145.1-0 ; 
       Ceiling_Bars-beam146.1-0 ; 
       Ceiling_Bars-beam147.1-0 ; 
       Ceiling_Bars-beam148.1-0 ; 
       Ceiling_Bars-beam149.1-0 ; 
       Ceiling_Bars-beam150.1-0 ; 
       Ceiling_Bars-beam151.1-0 ; 
       Ceiling_Bars-beam152.1-0 ; 
       Ceiling_Bars-beam153.1-0 ; 
       Ceiling_Bars-beam154.1-0 ; 
       Ceiling_Bars-beam155.1-0 ; 
       Ceiling_Bars-beam156.1-0 ; 
       Ceiling_Bars-beam157.1-0 ; 
       Ceiling_Bars-beam158.1-0 ; 
       Ceiling_Bars-beam159.1-0 ; 
       Ceiling_Bars-beam196.1-0 ; 
       Ceiling_Bars-beam197.1-0 ; 
       Ceiling_Bars-beam198.1-0 ; 
       Ceiling_Bars-beam199.1-0 ; 
       Ceiling_Bars-beam200.1-0 ; 
       Ceiling_Bars-beam201.1-0 ; 
       Ceiling_Bars-beam202.1-0 ; 
       Ceiling_Bars-beam203.1-0 ; 
       Ceiling_Bars-beam204.1-0 ; 
       Ceiling_Bars-beam205.1-0 ; 
       Ceiling_Bars-beam206.1-0 ; 
       Ceiling_Bars-beam207.1-0 ; 
       Ceiling_Bars-beam208.1-0 ; 
       Ceiling_Bars-beam209.1-0 ; 
       Ceiling_Bars-beam210.1-0 ; 
       Ceiling_Bars-beam211.1-0 ; 
       Ceiling_Bars-beam212.1-0 ; 
       Ceiling_Bars-beam213.1-0 ; 
       Ceiling_Bars-beam214.1-0 ; 
       Ceiling_Bars-null12.1-0 ; 
       Ceiling_Bars-Root.2-0 ROOT ; 
       Ceiling_Bars-torus15.1-0 ; 
       Ceiling_Bars-torus19.1-0 ; 
       Ceiling_Bars-torus21.1-0 ; 
       Ceiling_Bars-torus23.1-0 ; 
       Ceiling_Bars-torus24.1-0 ; 
       Ceiling_Bars-torus27.1-0 ; 
       Ceiling_Bars-torus28.1-0 ; 
       Ceiling_Bars-torus29.1-0 ; 
       Ceiling_Bars-torus30.1-0 ; 
       Ceiling_Bars-torus31.1-0 ; 
       Ceiling_Bars-torus32.1-0 ; 
       Floor_Bars-beam1.1-0 ; 
       Floor_Bars-beam144.1-0 ; 
       Floor_Bars-beam145.1-0 ; 
       Floor_Bars-beam146.1-0 ; 
       Floor_Bars-beam147.1-0 ; 
       Floor_Bars-beam148.1-0 ; 
       Floor_Bars-beam149.1-0 ; 
       Floor_Bars-beam150.1-0 ; 
       Floor_Bars-beam151.1-0 ; 
       Floor_Bars-beam152.1-0 ; 
       Floor_Bars-beam153.1-0 ; 
       Floor_Bars-beam154.1-0 ; 
       Floor_Bars-beam155.1-0 ; 
       Floor_Bars-beam156.1-0 ; 
       Floor_Bars-beam157.1-0 ; 
       Floor_Bars-beam158.1-0 ; 
       Floor_Bars-beam159.1-0 ; 
       Floor_Bars-beam196.1-0 ; 
       Floor_Bars-beam197.1-0 ; 
       Floor_Bars-beam198.1-0 ; 
       Floor_Bars-beam199.1-0 ; 
       Floor_Bars-beam200.1-0 ; 
       Floor_Bars-beam201.1-0 ; 
       Floor_Bars-beam202.1-0 ; 
       Floor_Bars-beam203.1-0 ; 
       Floor_Bars-beam204.1-0 ; 
       Floor_Bars-beam205.1-0 ; 
       Floor_Bars-beam206.1-0 ; 
       Floor_Bars-beam207.1-0 ; 
       Floor_Bars-beam208.1-0 ; 
       Floor_Bars-beam209.1-0 ; 
       Floor_Bars-beam210.1-0 ; 
       Floor_Bars-beam211.1-0 ; 
       Floor_Bars-beam212.1-0 ; 
       Floor_Bars-beam213.1-0 ; 
       Floor_Bars-beam214.1-0 ; 
       Floor_Bars-null12.1-0 ; 
       Floor_Bars-Root.2-0 ROOT ; 
       Floor_Bars-torus15.1-0 ; 
       Floor_Bars-torus19.1-0 ; 
       Floor_Bars-torus21.1-0 ; 
       Floor_Bars-torus23.1-0 ; 
       Floor_Bars-torus24.1-0 ; 
       Floor_Bars-torus27.1-0 ; 
       Floor_Bars-torus28.1-0 ; 
       Floor_Bars-torus29.1-0 ; 
       Floor_Bars-torus30.1-0 ; 
       Floor_Bars-torus31.1-0 ; 
       Floor_Bars-torus32.1-0 ; 
       floor_grate1-cube34.5-0 ROOT ; 
       Hangar-Brace1.1-0 ; 
       Hangar-Brace2.1-0 ; 
       Hangar-Ceiling.1-0 ; 
       Hangar-Field_Edge.1-0 ; 
       Hangar-Floor.1-0 ; 
       Hangar-Handrail.1-0 ; 
       Hangar-Hangar_Room_Root.2-0 ; 
       Hangar-Platform.1-0 ; 
       Hangar-Root.1-0 ROOT ; 
       Hangar-Stairs_Root.1-0 ; 
       Hangar-Wall.1-0 ; 
       Monitor-base.9-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.7-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Monitor6-base.9-0 ROOT ; 
       Monitor6-Screen.1-0 ; 
       Monitor7-base.8-0 ROOT ; 
       Monitor7-Screen.1-0 ; 
       Monitor8-base.9-0 ROOT ; 
       Monitor8-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.26-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       Room_Base-window_border.1-0 ; 
       Room_Base-window_pane.1-0 ; 
       spock_viewer-cube2.1-0 ; 
       spock_viewer-nurbs1.1-0 ; 
       spock_viewer-skin1.1-0 ; 
       spock_viewer-spock_viewer.5-0 ROOT ; 
       spock_viewer-torus2.1-0 ; 
       standins-Fore_Cylinder.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       add_window_pane-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/radar ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-add_window_pane.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       add_window_pane-Reflection_Map1.1-0 ; 
       add_window_pane-t2d1.1-0 ; 
       add_window_pane-t2d2.1-0 ; 
       add_window_pane-t2d3.1-0 ; 
       add_window_pane-t2d4.1-0 ; 
       add_window_pane-t2d5.1-0 ; 
       add_window_pane-t2d7.1-0 ; 
       add_window_pane-t2d8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       add_window_pane-Bionic_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       181 180 110 ; 
       73 109 110 ; 
       74 109 110 ; 
       75 109 110 ; 
       76 109 110 ; 
       77 109 110 ; 
       78 109 110 ; 
       79 109 110 ; 
       80 109 110 ; 
       81 109 110 ; 
       82 109 110 ; 
       83 109 110 ; 
       84 109 110 ; 
       85 109 110 ; 
       86 109 110 ; 
       87 109 110 ; 
       88 109 110 ; 
       89 109 110 ; 
       90 109 110 ; 
       91 109 110 ; 
       92 109 110 ; 
       93 109 110 ; 
       94 109 110 ; 
       95 109 110 ; 
       96 109 110 ; 
       97 109 110 ; 
       98 109 110 ; 
       99 109 110 ; 
       100 109 110 ; 
       101 109 110 ; 
       102 109 110 ; 
       103 109 110 ; 
       104 109 110 ; 
       105 109 110 ; 
       106 109 110 ; 
       107 109 110 ; 
       108 109 110 ; 
       109 110 110 ; 
       111 110 110 ; 
       112 110 110 ; 
       113 110 110 ; 
       114 110 110 ; 
       115 110 110 ; 
       116 110 110 ; 
       117 110 110 ; 
       118 110 110 ; 
       119 110 110 ; 
       120 110 110 ; 
       121 110 110 ; 
       122 158 110 ; 
       123 158 110 ; 
       124 158 110 ; 
       125 158 110 ; 
       126 158 110 ; 
       127 158 110 ; 
       128 158 110 ; 
       129 158 110 ; 
       130 158 110 ; 
       131 158 110 ; 
       132 158 110 ; 
       133 158 110 ; 
       134 158 110 ; 
       135 158 110 ; 
       136 158 110 ; 
       137 158 110 ; 
       138 158 110 ; 
       139 158 110 ; 
       140 158 110 ; 
       141 158 110 ; 
       142 158 110 ; 
       143 158 110 ; 
       144 158 110 ; 
       145 158 110 ; 
       146 158 110 ; 
       147 158 110 ; 
       148 158 110 ; 
       149 158 110 ; 
       150 158 110 ; 
       151 158 110 ; 
       152 158 110 ; 
       153 158 110 ; 
       154 158 110 ; 
       155 158 110 ; 
       156 158 110 ; 
       157 158 110 ; 
       158 159 110 ; 
       160 159 110 ; 
       161 159 110 ; 
       162 159 110 ; 
       163 159 110 ; 
       164 159 110 ; 
       165 159 110 ; 
       166 159 110 ; 
       167 159 110 ; 
       168 159 110 ; 
       169 159 110 ; 
       170 159 110 ; 
       184 183 110 ; 
       186 185 110 ; 
       188 187 110 ; 
       190 189 110 ; 
       192 191 110 ; 
       193 194 110 ; 
       194 199 110 ; 
       195 201 110 ; 
       197 199 110 ; 
       198 197 110 ; 
       200 197 110 ; 
       201 199 110 ; 
       204 207 110 ; 
       205 208 110 ; 
       206 207 110 ; 
       208 207 110 ; 
       2 69 110 ; 
       3 69 110 ; 
       4 69 110 ; 
       5 69 110 ; 
       6 69 110 ; 
       7 69 110 ; 
       8 69 110 ; 
       9 69 110 ; 
       10 69 110 ; 
       11 69 110 ; 
       12 69 110 ; 
       13 69 110 ; 
       14 69 110 ; 
       15 69 110 ; 
       16 69 110 ; 
       17 69 110 ; 
       18 69 110 ; 
       19 69 110 ; 
       20 69 110 ; 
       21 69 110 ; 
       196 201 110 ; 
       175 178 110 ; 
       22 71 110 ; 
       23 70 110 ; 
       25 70 110 ; 
       26 70 110 ; 
       27 70 110 ; 
       28 70 110 ; 
       29 70 110 ; 
       30 69 110 ; 
       31 70 110 ; 
       32 70 110 ; 
       33 70 110 ; 
       34 70 110 ; 
       35 70 110 ; 
       36 70 110 ; 
       37 70 110 ; 
       38 70 110 ; 
       39 70 110 ; 
       40 70 110 ; 
       41 69 110 ; 
       42 70 110 ; 
       43 70 110 ; 
       44 71 110 ; 
       45 71 110 ; 
       46 71 110 ; 
       47 71 110 ; 
       48 71 110 ; 
       49 71 110 ; 
       50 71 110 ; 
       51 71 110 ; 
       52 69 110 ; 
       53 71 110 ; 
       54 71 110 ; 
       55 71 110 ; 
       56 71 110 ; 
       57 71 110 ; 
       58 71 110 ; 
       59 71 110 ; 
       60 71 110 ; 
       61 71 110 ; 
       62 69 110 ; 
       63 69 110 ; 
       64 69 110 ; 
       172 179 110 ; 
       173 172 110 ; 
       177 179 110 ; 
       67 66 110 ; 
       174 178 110 ; 
       176 178 110 ; 
       182 178 110 ; 
       179 181 110 ; 
       69 72 110 ; 
       178 180 110 ; 
       202 201 110 ; 
       203 202 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       110 0 300 ; 
       159 20 300 ; 
       183 16 300 ; 
       184 17 300 ; 
       185 18 300 ; 
       186 19 300 ; 
       187 4 300 ; 
       188 5 300 ; 
       189 6 300 ; 
       190 7 300 ; 
       191 8 300 ; 
       192 9 300 ; 
       193 2 300 ; 
       194 1 300 ; 
       198 3 300 ; 
       200 3 300 ; 
       201 21 300 ; 
       205 14 300 ; 
       207 13 300 ; 
       0 15 300 ; 
       1 10 300 ; 
       68 11 300 ; 
       71 12 300 ; 
       203 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       184 1 400 ; 
       186 2 400 ; 
       188 3 400 ; 
       190 4 400 ; 
       192 5 400 ; 
       205 7 400 ; 
       1 6 400 ; 
       203 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       4 5 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       13 14 2110 ; 
       0 1 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
       21 22 2110 ; 
       23 24 2110 ; 
       25 26 2110 ; 
       27 28 2110 ; 
       29 30 2110 ; 
       31 32 2110 ; 
       33 34 2110 ; 
       35 36 2110 ; 
       37 38 2110 ; 
       39 40 2110 ; 
       41 42 2110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       5 65 2200 ; 
       11 203 2200 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       15 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       5 14 550 ; 
       12 0 550 ; 
       1 15 550 ; 
       16 1 550 ; 
       18 11 550 ; 
       20 2 550 ; 
       22 3 550 ; 
       24 4 550 ; 
       26 5 550 ; 
       28 6 550 ; 
       30 7 550 ; 
       32 8 550 ; 
       34 9 550 ; 
       36 10 550 ; 
       38 12 550 ; 
       40 16 550 ; 
       42 13 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 68 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 15 551 1 ; 
       0 17 551 1 ; 
       0 19 551 1 ; 
       0 21 551 1 ; 
       0 23 551 1 ; 
       0 25 551 1 ; 
       0 27 551 1 ; 
       0 29 551 1 ; 
       0 31 551 1 ; 
       0 33 551 1 ; 
       0 35 551 1 ; 
       0 37 551 1 ; 
       0 39 551 1 ; 
       0 41 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 587.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 634.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 634.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 592.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 627.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 627.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 629.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 629.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 582.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 632.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 632.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 589.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 589.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 622.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 594.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 594.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 614.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 614.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 617.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 617.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 619.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 619.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM 597.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       24 SCHEM 597.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 599.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       26 SCHEM 599.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 602.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 602.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       29 SCHEM 604.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       30 SCHEM 604.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       31 SCHEM 607.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       32 SCHEM 607.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       33 SCHEM 609.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       34 SCHEM 609.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       35 SCHEM 612.073 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       36 SCHEM 612.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 584.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       38 SCHEM 584.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       39 SCHEM 624.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 624.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       41 SCHEM 579.573 -2 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       42 SCHEM 579.573 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 577.073 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       181 SCHEM 632.073 12.02897 0 MPRFLG 0 ; 
       180 SCHEM 637.073 14.02897 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       73 SCHEM 307.336 -4 1 MPRFLG 0 ; 
       74 SCHEM 267.336 -4 1 MPRFLG 0 ; 
       75 SCHEM 269.836 -4 1 MPRFLG 0 ; 
       76 SCHEM 272.336 -4 1 MPRFLG 0 ; 
       77 SCHEM 274.836 -4 1 MPRFLG 0 ; 
       78 SCHEM 277.336 -4 1 MPRFLG 0 ; 
       79 SCHEM 279.836 -4 1 MPRFLG 0 ; 
       80 SCHEM 282.336 -4 1 MPRFLG 0 ; 
       81 SCHEM 284.836 -4 1 MPRFLG 0 ; 
       82 SCHEM 287.336 -4 1 MPRFLG 0 ; 
       83 SCHEM 289.836 -4 1 MPRFLG 0 ; 
       84 SCHEM 292.336 -4 1 MPRFLG 0 ; 
       85 SCHEM 294.836 -4 1 MPRFLG 0 ; 
       86 SCHEM 297.336 -4 1 MPRFLG 0 ; 
       87 SCHEM 299.836 -4 1 MPRFLG 0 ; 
       88 SCHEM 302.336 -4 1 MPRFLG 0 ; 
       89 SCHEM 304.836 -4 1 MPRFLG 0 ; 
       90 SCHEM 309.836 -4 1 MPRFLG 0 ; 
       91 SCHEM 312.336 -4 1 MPRFLG 0 ; 
       92 SCHEM 314.836 -4 1 MPRFLG 0 ; 
       93 SCHEM 317.336 -4 1 MPRFLG 0 ; 
       94 SCHEM 319.836 -4 1 MPRFLG 0 ; 
       95 SCHEM 322.336 -4 1 MPRFLG 0 ; 
       96 SCHEM 324.836 -4 1 MPRFLG 0 ; 
       97 SCHEM 327.336 -4 1 MPRFLG 0 ; 
       98 SCHEM 329.836 -4 1 MPRFLG 0 ; 
       99 SCHEM 332.336 -4 1 MPRFLG 0 ; 
       100 SCHEM 334.836 -4 1 MPRFLG 0 ; 
       101 SCHEM 337.336 -4 1 MPRFLG 0 ; 
       102 SCHEM 339.836 -4 1 MPRFLG 0 ; 
       103 SCHEM 342.336 -4 1 MPRFLG 0 ; 
       104 SCHEM 344.836 -4 1 MPRFLG 0 ; 
       105 SCHEM 347.336 -4 1 MPRFLG 0 ; 
       106 SCHEM 349.836 -4 1 MPRFLG 0 ; 
       107 SCHEM 352.336 -4 1 MPRFLG 0 ; 
       108 SCHEM 354.836 -4 1 MPRFLG 0 ; 
       109 SCHEM 311.086 -2 1 MPRFLG 0 ; 
       110 SCHEM 239.836 0 1 SRT 1 1 1 0 0 0 0 35.92136 0 MPRFLG 0 ; 
       111 SCHEM 239.836 -2 1 MPRFLG 0 ; 
       112 SCHEM 242.336 -2 1 MPRFLG 0 ; 
       113 SCHEM 244.836 -2 1 MPRFLG 0 ; 
       114 SCHEM 247.336 -2 1 MPRFLG 0 ; 
       115 SCHEM 249.836 -2 1 MPRFLG 0 ; 
       116 SCHEM 252.336 -2 1 MPRFLG 0 ; 
       117 SCHEM 254.836 -2 1 MPRFLG 0 ; 
       118 SCHEM 257.336 -2 1 MPRFLG 0 ; 
       119 SCHEM 259.836 -2 1 MPRFLG 0 ; 
       120 SCHEM 262.336 -2 1 MPRFLG 0 ; 
       121 SCHEM 264.836 -2 1 MPRFLG 0 ; 
       122 SCHEM 304.836 -4 1 MPRFLG 0 ; 
       123 SCHEM 264.836 -4 1 MPRFLG 0 ; 
       124 SCHEM 267.336 -4 1 MPRFLG 0 ; 
       125 SCHEM 269.836 -4 1 MPRFLG 0 ; 
       126 SCHEM 272.336 -4 1 MPRFLG 0 ; 
       127 SCHEM 274.836 -4 1 MPRFLG 0 ; 
       128 SCHEM 277.336 -4 1 MPRFLG 0 ; 
       129 SCHEM 279.836 -4 1 MPRFLG 0 ; 
       130 SCHEM 282.336 -4 1 MPRFLG 0 ; 
       131 SCHEM 284.836 -4 1 MPRFLG 0 ; 
       132 SCHEM 287.336 -4 1 MPRFLG 0 ; 
       133 SCHEM 289.836 -4 1 MPRFLG 0 ; 
       134 SCHEM 292.336 -4 1 MPRFLG 0 ; 
       135 SCHEM 294.836 -4 1 MPRFLG 0 ; 
       136 SCHEM 297.336 -4 1 MPRFLG 0 ; 
       137 SCHEM 299.836 -4 1 MPRFLG 0 ; 
       138 SCHEM 302.336 -4 1 MPRFLG 0 ; 
       139 SCHEM 307.336 -4 1 MPRFLG 0 ; 
       140 SCHEM 309.836 -4 1 MPRFLG 0 ; 
       141 SCHEM 312.336 -4 1 MPRFLG 0 ; 
       142 SCHEM 314.836 -4 1 MPRFLG 0 ; 
       143 SCHEM 317.336 -4 1 MPRFLG 0 ; 
       144 SCHEM 319.836 -4 1 MPRFLG 0 ; 
       145 SCHEM 322.336 -4 1 MPRFLG 0 ; 
       146 SCHEM 324.836 -4 1 MPRFLG 0 ; 
       147 SCHEM 327.336 -4 1 MPRFLG 0 ; 
       148 SCHEM 329.836 -4 1 MPRFLG 0 ; 
       149 SCHEM 332.336 -4 1 MPRFLG 0 ; 
       150 SCHEM 334.836 -4 1 MPRFLG 0 ; 
       151 SCHEM 337.336 -4 1 MPRFLG 0 ; 
       152 SCHEM 339.836 -4 1 MPRFLG 0 ; 
       153 SCHEM 342.336 -4 1 MPRFLG 0 ; 
       154 SCHEM 344.836 -4 1 MPRFLG 0 ; 
       155 SCHEM 347.336 -4 1 MPRFLG 0 ; 
       156 SCHEM 349.836 -4 1 MPRFLG 0 ; 
       157 SCHEM 352.336 -4 1 MPRFLG 0 ; 
       158 SCHEM 308.586 -2 1 MPRFLG 0 ; 
       159 SCHEM 237.336 0 1 SRT 1 1 1 0 0 0 0 -0.5 0 MPRFLG 0 ; 
       160 SCHEM 237.336 -2 1 MPRFLG 0 ; 
       161 SCHEM 239.836 -2 1 MPRFLG 0 ; 
       162 SCHEM 242.336 -2 1 MPRFLG 0 ; 
       163 SCHEM 244.836 -2 1 MPRFLG 0 ; 
       164 SCHEM 247.336 -2 1 MPRFLG 0 ; 
       165 SCHEM 249.836 -2 1 MPRFLG 0 ; 
       166 SCHEM 252.336 -2 1 MPRFLG 0 ; 
       167 SCHEM 254.836 -2 1 MPRFLG 0 ; 
       168 SCHEM 257.336 -2 1 MPRFLG 0 ; 
       169 SCHEM 259.836 -2 1 MPRFLG 0 ; 
       170 SCHEM 262.336 -2 1 MPRFLG 0 ; 
       171 SCHEM 508.573 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       183 SCHEM 87.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       184 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       185 SCHEM 95 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       186 SCHEM 93.75 -2 0 MPRFLG 0 ; 
       187 SCHEM 102.5 0 0 SRT 1.471141 0.9625919 0.9625919 0.4977454 0.5151278 -0.00364445 8.855516 6.308922 9.169995 MPRFLG 0 ; 
       188 SCHEM 101.25 -2 0 MPRFLG 0 ; 
       189 SCHEM 110 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3516835 0.2844428 0.001876728 10.20939 -8.125242 8.293537 MPRFLG 0 ; 
       190 SCHEM 108.75 -2 0 MPRFLG 0 ; 
       191 SCHEM 117.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3081128 -0.06048232 -3.158986 -5.119568 5.063054 13.15584 MPRFLG 0 ; 
       192 SCHEM 116.25 -2 0 MPRFLG 0 ; 
       193 SCHEM 137.5 -4 0 MPRFLG 0 ; 
       194 SCHEM 138.75 -2 0 MPRFLG 0 ; 
       195 SCHEM 147.5 -4 0 MPRFLG 0 ; 
       197 SCHEM 133.75 -2 0 MPRFLG 0 ; 
       198 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       199 SCHEM 142.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       200 SCHEM 135 -4 0 MPRFLG 0 ; 
       201 SCHEM 147.5 -2 0 MPRFLG 0 ; 
       204 SCHEM 561.073 -2 0 USR MPRFLG 0 ; 
       205 SCHEM 564.823 -4 0 USR MPRFLG 0 ; 
       206 SCHEM 568.573 -2 0 USR MPRFLG 0 ; 
       207 SCHEM 566.073 0 0 SRT 1 1 1 0.6699998 0.152 0 -2.159692 -10.97795 12.63122 MPRFLG 0 ; 
       208 SCHEM 564.823 -2 0 USR MPRFLG 0 ; 
       209 SCHEM 2.5 0 0 SRT 1 7.550001 1 0.03600001 0 -0.6040002 11.92827 -7.458397 44.58141 MPRFLG 0 ; 
       0 SCHEM 574.573 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 126.25 0 0 SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       2 SCHEM 40 -4 0 MPRFLG 0 ; 
       3 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 45 -4 0 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 MPRFLG 0 ; 
       6 SCHEM 50 -4 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 55 -4 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 MPRFLG 0 ; 
       12 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 65 -4 0 MPRFLG 0 ; 
       14 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 70 -4 0 MPRFLG 0 ; 
       16 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 75 -4 0 MPRFLG 0 ; 
       18 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 80 -4 0 MPRFLG 0 ; 
       21 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       196 SCHEM 150 -4 0 MPRFLG 0 ; 
       175 SCHEM 638.323 10.02897 0 MPRFLG 0 ; 
       22 SCHEM 533.573 -2 0 USR MPRFLG 0 ; 
       23 SCHEM 179 -2 0 USR MPRFLG 0 ; 
       24 SCHEM 154 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 159 -2 0 USR MPRFLG 0 ; 
       26 SCHEM 161.5 -2 0 USR MPRFLG 0 ; 
       27 SCHEM 164 -2 0 USR MPRFLG 0 ; 
       28 SCHEM 166.5 -2 0 USR MPRFLG 0 ; 
       29 SCHEM 169 -2 0 USR MPRFLG 0 ; 
       30 SCHEM 25 -4 0 MPRFLG 0 ; 
       31 SCHEM 171.5 -2 0 USR MPRFLG 0 ; 
       32 SCHEM 174 -2 0 USR MPRFLG 0 ; 
       33 SCHEM 176.5 -2 0 USR MPRFLG 0 ; 
       34 SCHEM 156.5 -2 0 USR MPRFLG 0 ; 
       35 SCHEM 181.5 -2 0 USR MPRFLG 0 ; 
       36 SCHEM 184 -2 0 USR MPRFLG 0 ; 
       37 SCHEM 186.5 -2 0 USR MPRFLG 0 ; 
       38 SCHEM 189 -2 0 USR MPRFLG 0 ; 
       39 SCHEM 191.5 -2 0 USR MPRFLG 0 ; 
       40 SCHEM 194 -2 0 USR MPRFLG 0 ; 
       41 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       42 SCHEM 196.5 -2 0 USR MPRFLG 0 ; 
       43 SCHEM 199 -2 0 USR MPRFLG 0 ; 
       44 SCHEM 513.573 -2 0 USR MPRFLG 0 ; 
       45 SCHEM 516.073 -2 0 USR MPRFLG 0 ; 
       46 SCHEM 518.573 -2 0 USR MPRFLG 0 ; 
       47 SCHEM 521.073 -2 0 USR MPRFLG 0 ; 
       48 SCHEM 523.573 -2 0 USR MPRFLG 0 ; 
       49 SCHEM 526.073 -2 0 USR MPRFLG 0 ; 
       50 SCHEM 528.573 -2 0 USR MPRFLG 0 ; 
       51 SCHEM 531.073 -2 0 USR MPRFLG 0 ; 
       52 SCHEM 30 -4 0 MPRFLG 0 ; 
       53 SCHEM 511.073 -2 0 USR MPRFLG 0 ; 
       54 SCHEM 536.073 -2 0 USR MPRFLG 0 ; 
       55 SCHEM 538.573 -2 0 USR MPRFLG 0 ; 
       56 SCHEM 541.073 -2 0 USR MPRFLG 0 ; 
       57 SCHEM 543.573 -2 0 USR MPRFLG 0 ; 
       58 SCHEM 546.073 -2 0 USR MPRFLG 0 ; 
       59 SCHEM 548.573 -2 0 USR MPRFLG 0 ; 
       60 SCHEM 551.073 -2 0 USR MPRFLG 0 ; 
       61 SCHEM 553.573 -2 0 USR MPRFLG 0 ; 
       62 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       63 SCHEM 35 -4 0 MPRFLG 0 ; 
       64 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       172 SCHEM 633.323 8.02897 0 MPRFLG 0 ; 
       173 SCHEM 633.323 6.02897 0 MPRFLG 0 ; 
       65 SCHEM 122.5 0 0 SRT 6.050002 11.52707 6.050002 0 0 0 0 0 0 MPRFLG 0 ; 
       177 SCHEM 630.823 8.02897 0 MPRFLG 0 ; 
       66 SCHEM 558.573 0 0 SRT 1.082 1.082 1.082 0 -1.34 0 51.55545 0 -14.8918 MPRFLG 0 ; 
       67 SCHEM 558.573 -2 0 USR MPRFLG 0 ; 
       174 SCHEM 635.823 10.02897 0 MPRFLG 0 ; 
       176 SCHEM 640.823 10.02897 0 MPRFLG 0 ; 
       182 SCHEM 643.323 10.02897 0 MPRFLG 0 ; 
       179 SCHEM 632.073 10.02897 0 MPRFLG 0 ; 
       68 SCHEM 130 0 0 SRT 1 1 1 0 0 0 147.0213 -22.28944 -177.9266 MPRFLG 0 ; 
       69 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       70 SCHEM 177.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       71 SCHEM 533.573 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       178 SCHEM 639.573 12.02897 0 MPRFLG 0 ; 
       72 SCHEM 51.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       202 SCHEM 143.75 -4 0 MPRFLG 0 ; 
       203 SCHEM 143.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 506.073 14.02897 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 139 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 127.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 556.073 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 571.073 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 566.073 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 573.573 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 201.5 1.082259 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 151.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 141.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 85 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 107.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 115 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 125 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 563.573 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 143 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       15 SCHEM 0 0 0 ; 
       16 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
