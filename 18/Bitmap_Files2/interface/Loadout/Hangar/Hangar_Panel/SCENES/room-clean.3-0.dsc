SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       clean-cam_int1.3-0 ROOT ; 
       clean-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 43     
       Bounce-Alarm1.1-0 ROOT ; 
       bounce-Fore_beam.1-0 ROOT ; 
       Bounce-Spock.1-0 ROOT ; 
       Fill-Ceiling.1-0 ; 
       Fill-Ceiling_int.17-0 ROOT ; 
       Fill-from_space_to_hangar2.1-0 ; 
       Fill-from_space_to_hangar2_int.1-0 ROOT ; 
       Fill-hilight_logo.1-0 ; 
       Fill-hilight_logo_int.1-0 ROOT ; 
       fill-inside_hangar_fill1.46-0 ROOT ; 
       Fill-Shine_on_backwall.1-0 ; 
       Fill-Shine_on_backwall_int.19-0 ROOT ; 
       Fill-Shine_on_sidewall.1-0 ; 
       Fill-Shine_on_sidewall_int.18-0 ROOT ; 
       Glass-Reflector.4-0 ROOT ; 
       Vol-floor1.1-0 ; 
       Vol-floor1_int.33-0 ROOT ; 
       Vol-floor10.1-0 ; 
       Vol-floor10_int.30-0 ROOT ; 
       Vol-floor11.1-0 ; 
       Vol-floor11_int.30-0 ROOT ; 
       Vol-floor12.1-0 ; 
       Vol-floor12_int.30-0 ROOT ; 
       Vol-floor3.1-0 ; 
       Vol-floor3_int.33-0 ROOT ; 
       Vol-floor4.1-0 ; 
       Vol-floor4_int.33-0 ROOT ; 
       Vol-floor5.1-0 ; 
       Vol-floor5_int.33-0 ROOT ; 
       Vol-floor6.1-0 ; 
       Vol-floor6_int.33-0 ROOT ; 
       Vol-floor7.1-0 ; 
       Vol-floor7_int.33-0 ROOT ; 
       Vol-floor8.1-0 ; 
       Vol-floor8_int.30-0 ROOT ; 
       Vol-floor9.1-0 ; 
       Vol-floor9_int.30-0 ROOT ; 
       Vol-from_hangar1.1-0 ; 
       Vol-from_hangar1_int.33-0 ROOT ; 
       Vol-Shine_on_Door.1-0 ; 
       Vol-Shine_on_Door_int.20-0 ROOT ; 
       Vol-Under_Hole.1-0 ; 
       Vol-Under_Hole_int.33-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 17     
       clean-alarm1.1-0 ; 
       clean-floor1.1-0 ; 
       clean-floor10.1-0 ; 
       clean-floor11.1-0 ; 
       clean-floor2.1-0 ; 
       clean-floor3.1-0 ; 
       clean-floor4.1-0 ; 
       clean-floor5.1-0 ; 
       clean-floor6.1-0 ; 
       clean-floor7.1-0 ; 
       clean-floor8.1-0 ; 
       clean-floor9.1-0 ; 
       clean-from_hangar1.1-0 ; 
       clean-From_Hole_Bottom1.1-0 ; 
       clean-From_Hole_Bottom2.1-0 ; 
       clean-from_space1.1-0 ; 
       clean-light_on_Door1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       clean-Bionic_Volume.1-0 ; 
       clean-Bottom_Lights.1-0 ; 
       clean-Ceiling_Metal_Bars.1-0 ; 
       clean-Ceiling1.1-0 ; 
       clean-Constant_Black1.1-0 ; 
       clean-Floor2.1-0 ; 
       clean-mat13.1-0 ; 
       clean-mat14.1-0 ; 
       clean-mat15.1-0 ; 
       clean-mat16.1-0 ; 
       clean-mat17.1-0 ; 
       clean-mat18.1-0 ; 
       clean-mat21.1-0 ; 
       clean-mat23.1-0 ; 
       clean-mat25.1-0 ; 
       clean-mat26.1-0 ; 
       clean-mat5.1-0 ; 
       clean-mat6.1-0 ; 
       clean-mat7.1-0 ; 
       clean-mat8.1-0 ; 
       clean-Metal_Bars.1-0 ; 
       clean-Wall1.1-0 ; 
       clean-window_Glass.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 209     
       Bionic_Volume-Bionic_Volume.1-0 ROOT ; 
       Bottom_Lights-Bottom_light_Root.1-0 ROOT ; 
       Bottom_Lights-cube34.1-0 ; 
       Bottom_Lights-cube34_1.1-0 ; 
       Bottom_Lights-cube35.1-0 ; 
       Bottom_Lights-cube36.1-0 ; 
       Bottom_Lights-cube37.1-0 ; 
       Bottom_Lights-cube38.1-0 ; 
       Bottom_Lights-cube39.1-0 ; 
       Bottom_Lights-cube40.1-0 ; 
       Bottom_Lights-cube41.1-0 ; 
       Bottom_Lights-cube42.1-0 ; 
       Bottom_Lights-cube43.1-0 ; 
       Bottom_Lights-cube44.1-0 ; 
       Bottom_Lights-cube45.1-0 ; 
       Bottom_Lights-cube46.1-0 ; 
       Bottom_Lights-cube47.1-0 ; 
       Bottom_Lights-cube48.1-0 ; 
       Bottom_Lights-cube49.1-0 ; 
       Bottom_Lights-cube50.1-0 ; 
       Bottom_Lights-cube51.1-0 ; 
       Bottom_Lights-cube52.1-0 ; 
       Bottom_Lights-cube53.1-0 ; 
       Bottom_Lights-cube54.1-0 ; 
       Bottom_Lights-cube55.1-0 ; 
       Bottom_Lights-cube56.1-0 ; 
       Bottom_Lights-cube57.1-0 ; 
       Bottom_Lights-cube58.1-0 ; 
       Bottom_Lights-cube59.1-0 ; 
       Bottom_Lights-cube60.1-0 ; 
       Bottom_Lights-cube61.1-0 ; 
       Bottom_Lights-cube62.1-0 ; 
       Bottom_Lights-cube63.1-0 ; 
       Bottom_Lights-cube64.1-0 ; 
       Bottom_Lights-cube65.1-0 ; 
       Bottom_Lights-cube66.1-0 ; 
       Bottom_Lights-cube67.1-0 ; 
       Bottom_Lights-cube68.1-0 ; 
       Bottom_Lights-inside_Track.1-0 ; 
       Bottom_Lights-outside_Track.1-0 ; 
       Ceiling_Bars-beam1.1-0 ; 
       Ceiling_Bars-beam144.1-0 ; 
       Ceiling_Bars-beam145.1-0 ; 
       Ceiling_Bars-beam146.1-0 ; 
       Ceiling_Bars-beam147.1-0 ; 
       Ceiling_Bars-beam148.1-0 ; 
       Ceiling_Bars-beam149.1-0 ; 
       Ceiling_Bars-beam150.1-0 ; 
       Ceiling_Bars-beam151.1-0 ; 
       Ceiling_Bars-beam152.1-0 ; 
       Ceiling_Bars-beam153.1-0 ; 
       Ceiling_Bars-beam154.1-0 ; 
       Ceiling_Bars-beam155.1-0 ; 
       Ceiling_Bars-beam156.1-0 ; 
       Ceiling_Bars-beam157.1-0 ; 
       Ceiling_Bars-beam158.1-0 ; 
       Ceiling_Bars-beam159.1-0 ; 
       Ceiling_Bars-beam196.1-0 ; 
       Ceiling_Bars-beam197.1-0 ; 
       Ceiling_Bars-beam198.1-0 ; 
       Ceiling_Bars-beam199.1-0 ; 
       Ceiling_Bars-beam200.1-0 ; 
       Ceiling_Bars-beam201.1-0 ; 
       Ceiling_Bars-beam202.1-0 ; 
       Ceiling_Bars-beam203.1-0 ; 
       Ceiling_Bars-beam204.1-0 ; 
       Ceiling_Bars-beam205.1-0 ; 
       Ceiling_Bars-beam206.1-0 ; 
       Ceiling_Bars-beam207.1-0 ; 
       Ceiling_Bars-beam208.1-0 ; 
       Ceiling_Bars-beam209.1-0 ; 
       Ceiling_Bars-beam210.1-0 ; 
       Ceiling_Bars-beam211.1-0 ; 
       Ceiling_Bars-beam212.1-0 ; 
       Ceiling_Bars-beam213.1-0 ; 
       Ceiling_Bars-beam214.1-0 ; 
       Ceiling_Bars-null12.1-0 ; 
       Ceiling_Bars-Root.2-0 ROOT ; 
       Ceiling_Bars-torus15.1-0 ; 
       Ceiling_Bars-torus19.1-0 ; 
       Ceiling_Bars-torus21.1-0 ; 
       Ceiling_Bars-torus23.1-0 ; 
       Ceiling_Bars-torus24.1-0 ; 
       Ceiling_Bars-torus27.1-0 ; 
       Ceiling_Bars-torus28.1-0 ; 
       Ceiling_Bars-torus29.1-0 ; 
       Ceiling_Bars-torus30.1-0 ; 
       Ceiling_Bars-torus31.1-0 ; 
       Ceiling_Bars-torus32.1-0 ; 
       clean-ceiling_light_1.1-0 ROOT ; 
       Floor_Bars-beam1.1-0 ; 
       Floor_Bars-beam144.1-0 ; 
       Floor_Bars-beam145.1-0 ; 
       Floor_Bars-beam146.1-0 ; 
       Floor_Bars-beam147.1-0 ; 
       Floor_Bars-beam148.1-0 ; 
       Floor_Bars-beam149.1-0 ; 
       Floor_Bars-beam150.1-0 ; 
       Floor_Bars-beam151.1-0 ; 
       Floor_Bars-beam152.1-0 ; 
       Floor_Bars-beam153.1-0 ; 
       Floor_Bars-beam154.1-0 ; 
       Floor_Bars-beam155.1-0 ; 
       Floor_Bars-beam156.1-0 ; 
       Floor_Bars-beam157.1-0 ; 
       Floor_Bars-beam158.1-0 ; 
       Floor_Bars-beam159.1-0 ; 
       Floor_Bars-beam196.1-0 ; 
       Floor_Bars-beam197.1-0 ; 
       Floor_Bars-beam198.1-0 ; 
       Floor_Bars-beam199.1-0 ; 
       Floor_Bars-beam200.1-0 ; 
       Floor_Bars-beam201.1-0 ; 
       Floor_Bars-beam202.1-0 ; 
       Floor_Bars-beam203.1-0 ; 
       Floor_Bars-beam204.1-0 ; 
       Floor_Bars-beam205.1-0 ; 
       Floor_Bars-beam206.1-0 ; 
       Floor_Bars-beam207.1-0 ; 
       Floor_Bars-beam208.1-0 ; 
       Floor_Bars-beam209.1-0 ; 
       Floor_Bars-beam210.1-0 ; 
       Floor_Bars-beam211.1-0 ; 
       Floor_Bars-beam212.1-0 ; 
       Floor_Bars-beam213.1-0 ; 
       Floor_Bars-beam214.1-0 ; 
       Floor_Bars-Floor_Bars_Root.1-0 ROOT ; 
       Floor_Bars-null12.1-0 ; 
       Floor_Bars-torus15.1-0 ; 
       Floor_Bars-torus19.1-0 ; 
       Floor_Bars-torus21.1-0 ; 
       Floor_Bars-torus23.1-0 ; 
       Floor_Bars-torus24.1-0 ; 
       Floor_Bars-torus27.1-0 ; 
       Floor_Bars-torus28.1-0 ; 
       Floor_Bars-torus29.1-0 ; 
       Floor_Bars-torus30.1-0 ; 
       Floor_Bars-torus31.1-0 ; 
       Floor_Bars-torus32.1-0 ; 
       Floor_Edge-cube10.1-0 ; 
       Floor_Edge-cube11.1-0 ; 
       Floor_Edge-cube12.1-0 ; 
       Floor_Edge-cube17.1-0 ; 
       Floor_Edge-cube18.1-0 ; 
       Floor_Edge-cube19.1-0 ; 
       Floor_Edge-cube2.1-0 ; 
       Floor_Edge-cube20.1-0 ; 
       Floor_Edge-cube21.1-0 ; 
       Floor_Edge-cube22.1-0 ; 
       Floor_Edge-cube23.1-0 ; 
       Floor_Edge-cube24.1-0 ; 
       Floor_Edge-cube25.1-0 ; 
       Floor_Edge-cube26.1-0 ; 
       Floor_Edge-cube27.1-0 ; 
       Floor_Edge-cube28.1-0 ; 
       Floor_Edge-cube29.1-0 ; 
       Floor_Edge-cube3.1-0 ; 
       Floor_Edge-cube30.1-0 ; 
       Floor_Edge-cube31.1-0 ; 
       Floor_Edge-cube4.1-0 ; 
       Floor_Edge-cube5.1-0 ; 
       Floor_Edge-cube6.1-0 ; 
       Floor_Edge-cube7.1-0 ; 
       Floor_Edge-cube8.1-0 ; 
       Floor_Edge-cube9.1-0 ; 
       Floor_Edge-Floor_Edge_Root.1-0 ROOT ; 
       Hangar-Brace1.1-0 ; 
       Hangar-Brace2.1-0 ; 
       Hangar-Ceiling.1-0 ; 
       Hangar-Field_Edge.1-0 ; 
       Hangar-Floor.1-0 ; 
       Hangar-Handrail.1-0 ; 
       Hangar-hangar_force_field.1-0 ; 
       Hangar-Hangar_Room_Root.2-0 ; 
       Hangar-Hangar_Root.1-0 ROOT ; 
       Hangar-Platform.1-0 ; 
       Hangar-Stairs_Root.1-0 ; 
       Hangar-Wall.1-0 ; 
       Monitors-base.7-0 ; 
       Monitors-base_1.9-0 ; 
       Monitors-base_2.9-0 ; 
       Monitors-base_3.8-0 ; 
       Monitors-base_4.9-0 ; 
       Monitors-cube2.1-0 ; 
       Monitors-Monitor_Root.1-0 ROOT ; 
       Monitors-nurbs1.1-0 ; 
       Monitors-Screen.1-0 ; 
       Monitors-Screen_1.1-0 ; 
       Monitors-Screen_2.1-0 ; 
       Monitors-Screen_3.1-0 ; 
       Monitors-Screen_4.1-0 ; 
       Monitors-skin1.1-0 ; 
       Monitors-spock_viewer.1-0 ROOT ; 
       Monitors-torus2.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Central_Cylinder.2-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Door_Frame.1-0 ; 
       Room_Base-Door_Line.1-0 ; 
       Room_Base-face1.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.27-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       Room_Base-window_border.1-0 ; 
       Room_Base-window_pane.1-0 ; 
       standins-Fore_Cylinder.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       clean-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/radar ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/reflection ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-clean.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       clean-Reflection_Map1.1-0 ; 
       clean-t2d1.1-0 ; 
       clean-t2d2.1-0 ; 
       clean-t2d3.1-0 ; 
       clean-t2d4.1-0 ; 
       clean-t2d5.1-0 ; 
       clean-t2d7.1-0 ; 
       clean-t2d8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       clean-Bionic_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       176 174 110 ; 
       40 76 110 ; 
       41 76 110 ; 
       42 76 110 ; 
       43 76 110 ; 
       44 76 110 ; 
       45 76 110 ; 
       46 76 110 ; 
       47 76 110 ; 
       48 76 110 ; 
       49 76 110 ; 
       50 76 110 ; 
       51 76 110 ; 
       52 76 110 ; 
       53 76 110 ; 
       54 76 110 ; 
       55 76 110 ; 
       56 76 110 ; 
       57 76 110 ; 
       58 76 110 ; 
       59 76 110 ; 
       60 76 110 ; 
       61 76 110 ; 
       62 76 110 ; 
       63 76 110 ; 
       64 76 110 ; 
       65 76 110 ; 
       66 76 110 ; 
       67 76 110 ; 
       68 76 110 ; 
       69 76 110 ; 
       70 76 110 ; 
       71 76 110 ; 
       72 76 110 ; 
       73 76 110 ; 
       74 76 110 ; 
       75 76 110 ; 
       76 77 110 ; 
       78 77 110 ; 
       79 77 110 ; 
       80 77 110 ; 
       81 77 110 ; 
       82 77 110 ; 
       83 77 110 ; 
       84 77 110 ; 
       85 77 110 ; 
       86 77 110 ; 
       87 77 110 ; 
       88 77 110 ; 
       90 127 110 ; 
       91 127 110 ; 
       92 127 110 ; 
       93 127 110 ; 
       94 127 110 ; 
       95 127 110 ; 
       96 127 110 ; 
       97 127 110 ; 
       98 127 110 ; 
       99 127 110 ; 
       100 127 110 ; 
       101 127 110 ; 
       102 127 110 ; 
       103 127 110 ; 
       104 127 110 ; 
       105 127 110 ; 
       106 127 110 ; 
       107 127 110 ; 
       108 127 110 ; 
       109 127 110 ; 
       110 127 110 ; 
       111 127 110 ; 
       112 127 110 ; 
       113 127 110 ; 
       114 127 110 ; 
       115 127 110 ; 
       116 127 110 ; 
       117 127 110 ; 
       118 127 110 ; 
       119 127 110 ; 
       120 127 110 ; 
       121 127 110 ; 
       122 127 110 ; 
       123 127 110 ; 
       124 127 110 ; 
       125 127 110 ; 
       127 126 110 ; 
       128 126 110 ; 
       129 126 110 ; 
       130 126 110 ; 
       131 126 110 ; 
       132 126 110 ; 
       133 126 110 ; 
       134 126 110 ; 
       135 126 110 ; 
       136 126 110 ; 
       137 126 110 ; 
       138 126 110 ; 
       179 184 110 ; 
       189 179 110 ; 
       178 184 110 ; 
       187 178 110 ; 
       180 184 110 ; 
       188 180 110 ; 
       181 184 110 ; 
       190 181 110 ; 
       182 184 110 ; 
       186 182 110 ; 
       194 195 110 ; 
       195 203 110 ; 
       199 205 110 ; 
       201 203 110 ; 
       202 201 110 ; 
       204 201 110 ; 
       205 203 110 ; 
       183 192 110 ; 
       185 193 110 ; 
       191 192 110 ; 
       193 192 110 ; 
       139 165 110 ; 
       140 165 110 ; 
       141 165 110 ; 
       142 165 110 ; 
       143 165 110 ; 
       144 165 110 ; 
       145 165 110 ; 
       146 165 110 ; 
       147 165 110 ; 
       148 165 110 ; 
       149 165 110 ; 
       150 165 110 ; 
       151 165 110 ; 
       152 165 110 ; 
       153 165 110 ; 
       154 165 110 ; 
       155 165 110 ; 
       156 165 110 ; 
       157 165 110 ; 
       158 165 110 ; 
       198 205 110 ; 
       169 173 110 ; 
       2 38 110 ; 
       3 39 110 ; 
       4 39 110 ; 
       5 39 110 ; 
       6 39 110 ; 
       7 39 110 ; 
       8 39 110 ; 
       159 165 110 ; 
       9 39 110 ; 
       10 39 110 ; 
       11 39 110 ; 
       12 39 110 ; 
       13 39 110 ; 
       14 39 110 ; 
       15 39 110 ; 
       16 39 110 ; 
       17 39 110 ; 
       18 39 110 ; 
       160 165 110 ; 
       19 39 110 ; 
       20 39 110 ; 
       21 38 110 ; 
       22 38 110 ; 
       23 38 110 ; 
       24 38 110 ; 
       25 38 110 ; 
       26 38 110 ; 
       27 38 110 ; 
       28 38 110 ; 
       161 165 110 ; 
       29 38 110 ; 
       30 38 110 ; 
       31 38 110 ; 
       32 38 110 ; 
       33 38 110 ; 
       34 38 110 ; 
       35 38 110 ; 
       36 38 110 ; 
       37 38 110 ; 
       162 165 110 ; 
       163 165 110 ; 
       164 165 110 ; 
       166 175 110 ; 
       167 166 110 ; 
       196 203 110 ; 
       171 175 110 ; 
       197 198 110 ; 
       200 197 110 ; 
       168 173 110 ; 
       170 173 110 ; 
       177 173 110 ; 
       175 176 110 ; 
       172 169 110 ; 
       39 1 110 ; 
       38 1 110 ; 
       173 174 110 ; 
       206 205 110 ; 
       207 206 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       77 2 300 ; 
       1 1 300 ; 
       126 20 300 ; 
       179 16 300 ; 
       189 17 300 ; 
       178 18 300 ; 
       187 19 300 ; 
       180 6 300 ; 
       188 7 300 ; 
       181 8 300 ; 
       190 9 300 ; 
       182 10 300 ; 
       186 11 300 ; 
       194 4 300 ; 
       195 3 300 ; 
       202 5 300 ; 
       204 5 300 ; 
       205 21 300 ; 
       185 15 300 ; 
       192 14 300 ; 
       0 0 300 ; 
       89 12 300 ; 
       172 13 300 ; 
       207 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       189 1 400 ; 
       187 2 400 ; 
       188 3 400 ; 
       190 4 400 ; 
       186 5 400 ; 
       185 7 400 ; 
       89 6 400 ; 
       207 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
       10 11 2110 ; 
       12 13 2110 ; 
       7 8 2110 ; 
       5 6 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
       21 22 2110 ; 
       23 24 2110 ; 
       25 26 2110 ; 
       27 28 2110 ; 
       29 30 2110 ; 
       31 32 2110 ; 
       33 34 2110 ; 
       35 36 2110 ; 
       37 38 2110 ; 
       39 40 2110 ; 
       41 42 2110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       4 196 2200 ; 
       14 207 2200 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       4 14 550 ; 
       0 0 550 ; 
       6 15 550 ; 
       16 1 550 ; 
       18 11 550 ; 
       20 2 550 ; 
       22 3 550 ; 
       24 4 550 ; 
       26 5 550 ; 
       28 6 550 ; 
       30 7 550 ; 
       32 8 550 ; 
       34 9 550 ; 
       36 10 550 ; 
       38 12 550 ; 
       40 16 550 ; 
       42 13 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 172 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 15 551 1 ; 
       0 17 551 1 ; 
       0 19 551 1 ; 
       0 21 551 1 ; 
       0 23 551 1 ; 
       0 25 551 1 ; 
       0 27 551 1 ; 
       0 29 551 1 ; 
       0 31 551 1 ; 
       0 33 551 1 ; 
       0 35 551 1 ; 
       0 37 551 1 ; 
       0 39 551 1 ; 
       0 41 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -8.199468 -42.9393 0 USR MPRFLG 0 ; 
       1 SCHEM -8.199468 -44.9393 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 10.83598 -30.15323 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM -4.973569 -16.40984 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM -4.973569 -14.40984 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM -7.433966 -30.19182 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM -4.981926 -32.19182 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM -4.981926 -30.19182 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM -2.385413 -32.19182 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM -2.385413 -30.19182 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 8.502513 -30.1745 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 0.1196743 -32.14378 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0.1196743 -30.14377 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.568331 -32.14378 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 2.568331 -30.14377 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 13.16175 -30.12043 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM -0.5213227 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM -0.5213227 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 4.478675 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 4.478675 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 9.478683 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 9.478683 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM -7.577133 -27.78249 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM -7.577133 -25.78249 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM -5.333353 -27.75672 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       24 SCHEM -5.333353 -25.75672 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 14.82899 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       26 SCHEM 14.82899 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 17.329 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 17.329 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       29 SCHEM 12.329 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       30 SCHEM 12.329 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       31 SCHEM 6.978683 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       32 SCHEM 6.978683 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       33 SCHEM 1.978678 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       34 SCHEM 1.978678 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       35 SCHEM -3.021325 -27.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       36 SCHEM -3.021325 -25.76073 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM -3.008871 -24.7221 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       38 SCHEM -3.008871 -22.7221 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       39 SCHEM -5.271611 -24.70826 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM -5.271611 -22.70826 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       41 SCHEM -7.615487 -24.67058 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       42 SCHEM -7.615487 -22.67058 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM -7.265627 -14.4891 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       176 SCHEM -4.834857 42.95493 0 USR MPRFLG 0 ; 
       174 SCHEM 0.165144 44.95493 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 9.635521 -4.230892 1 USR MPRFLG 0 ; 
       41 SCHEM -32.86448 -4.230892 1 USR MPRFLG 0 ; 
       42 SCHEM -30.36448 -4.230892 1 USR MPRFLG 0 ; 
       43 SCHEM -27.86448 -4.230892 1 USR MPRFLG 0 ; 
       44 SCHEM -25.36446 -4.230892 1 USR MPRFLG 0 ; 
       45 SCHEM -22.86446 -4.230892 1 USR MPRFLG 0 ; 
       46 SCHEM -20.36448 -4.230892 1 USR MPRFLG 0 ; 
       47 SCHEM -17.86448 -4.230892 1 USR MPRFLG 0 ; 
       48 SCHEM -15.36446 -4.230892 1 USR MPRFLG 0 ; 
       49 SCHEM -12.86446 -4.230892 1 USR MPRFLG 0 ; 
       50 SCHEM -10.36448 -4.230892 1 USR MPRFLG 0 ; 
       51 SCHEM -7.864475 -4.230892 1 USR MPRFLG 0 ; 
       52 SCHEM -5.364475 -4.230892 1 USR MPRFLG 0 ; 
       53 SCHEM -2.864475 -4.230892 1 USR MPRFLG 0 ; 
       54 SCHEM -0.3644753 -4.230892 1 USR MPRFLG 0 ; 
       55 SCHEM 2.135525 -4.230892 1 USR MPRFLG 0 ; 
       56 SCHEM 4.635529 -4.230892 1 USR MPRFLG 0 ; 
       57 SCHEM 7.135521 -4.230892 1 USR MPRFLG 0 ; 
       58 SCHEM -35.36448 -4.230892 1 USR MPRFLG 0 ; 
       59 SCHEM 12.13552 -4.230892 1 USR MPRFLG 0 ; 
       60 SCHEM 14.63554 -4.230892 1 USR MPRFLG 0 ; 
       61 SCHEM 17.13554 -4.230892 1 USR MPRFLG 0 ; 
       62 SCHEM 19.63553 -4.230892 1 USR MPRFLG 0 ; 
       63 SCHEM 22.13553 -4.230892 1 USR MPRFLG 0 ; 
       64 SCHEM 24.63553 -4.230892 1 USR MPRFLG 0 ; 
       65 SCHEM 27.13552 -4.230892 1 USR MPRFLG 0 ; 
       66 SCHEM 29.63552 -4.230892 1 USR MPRFLG 0 ; 
       67 SCHEM 32.13552 -4.230892 1 USR MPRFLG 0 ; 
       68 SCHEM 34.63552 -4.230892 1 USR MPRFLG 0 ; 
       69 SCHEM 37.13552 -4.230892 1 USR MPRFLG 0 ; 
       70 SCHEM 39.63552 -4.230892 1 USR MPRFLG 0 ; 
       71 SCHEM 42.13553 -4.230892 1 USR MPRFLG 0 ; 
       72 SCHEM 44.63553 -4.230892 1 USR MPRFLG 0 ; 
       73 SCHEM 47.13553 -4.230892 1 USR MPRFLG 0 ; 
       74 SCHEM 49.63553 -4.230892 1 USR MPRFLG 0 ; 
       75 SCHEM 52.13553 -4.230892 1 USR MPRFLG 0 ; 
       76 SCHEM 8.385521 -2.230893 1 USR MPRFLG 0 ; 
       77 SCHEM 84.89288 43.78729 1 USR SRT 1 1 1 0 0 0 0 35.92136 0 MPRFLG 0 ; 
       78 SCHEM -37.86448 -2.230893 1 USR MPRFLG 0 ; 
       79 SCHEM -47.86448 -2.230893 1 USR MPRFLG 0 ; 
       80 SCHEM -60.36448 -2.230893 1 USR MPRFLG 0 ; 
       81 SCHEM -57.86448 -2.230893 1 USR MPRFLG 0 ; 
       82 SCHEM -55.36448 -2.230893 1 USR MPRFLG 0 ; 
       83 SCHEM -52.86448 -2.230893 1 USR MPRFLG 0 ; 
       84 SCHEM -62.86448 -2.230893 1 USR MPRFLG 0 ; 
       85 SCHEM -50.36448 -2.230893 1 USR MPRFLG 0 ; 
       86 SCHEM -45.36448 -2.230893 1 USR MPRFLG 0 ; 
       87 SCHEM -40.36448 -2.230893 1 USR MPRFLG 0 ; 
       88 SCHEM -42.86448 -2.230893 1 USR MPRFLG 0 ; 
       184 SCHEM 18.21782 33.66244 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 42.08976 -40.50394 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       90 SCHEM -2.279182 -9.611547 1 USR MPRFLG 0 ; 
       91 SCHEM -44.77916 -9.611547 1 USR MPRFLG 0 ; 
       92 SCHEM -42.27916 -9.611547 1 USR MPRFLG 0 ; 
       93 SCHEM -39.77916 -9.611547 1 USR MPRFLG 0 ; 
       94 SCHEM -37.27915 -9.611547 1 USR MPRFLG 0 ; 
       95 SCHEM -34.77915 -9.611547 1 USR MPRFLG 0 ; 
       96 SCHEM -32.27915 -9.611547 1 USR MPRFLG 0 ; 
       97 SCHEM -29.77915 -9.611547 1 USR MPRFLG 0 ; 
       98 SCHEM -27.27915 -9.611547 1 USR MPRFLG 0 ; 
       99 SCHEM -24.77916 -9.611547 1 USR MPRFLG 0 ; 
       100 SCHEM -22.27916 -9.611547 1 USR MPRFLG 0 ; 
       101 SCHEM -19.77919 -9.611547 1 USR MPRFLG 0 ; 
       102 SCHEM -17.27919 -9.611547 1 USR MPRFLG 0 ; 
       103 SCHEM -14.77919 -9.611547 1 USR MPRFLG 0 ; 
       104 SCHEM -12.27919 -9.611547 1 USR MPRFLG 0 ; 
       105 SCHEM -9.779182 -9.611547 1 USR MPRFLG 0 ; 
       106 SCHEM -7.279182 -9.611547 1 USR MPRFLG 0 ; 
       107 SCHEM -4.779182 -9.611547 1 USR MPRFLG 0 ; 
       108 SCHEM -47.27916 -9.611547 1 USR MPRFLG 0 ; 
       109 SCHEM 0.2208176 -9.611547 1 USR MPRFLG 0 ; 
       110 SCHEM 2.720821 -9.611547 1 USR MPRFLG 0 ; 
       111 SCHEM 5.220821 -9.611547 1 USR MPRFLG 0 ; 
       112 SCHEM 7.720829 -9.611547 1 USR MPRFLG 0 ; 
       113 SCHEM 10.22083 -9.611547 1 USR MPRFLG 0 ; 
       114 SCHEM 12.72083 -9.611547 1 USR MPRFLG 0 ; 
       115 SCHEM 15.22083 -9.611547 1 USR MPRFLG 0 ; 
       116 SCHEM 17.72083 -9.611547 1 USR MPRFLG 0 ; 
       117 SCHEM 20.22082 -9.611547 1 USR MPRFLG 0 ; 
       118 SCHEM 22.72082 -9.611547 1 USR MPRFLG 0 ; 
       119 SCHEM 25.22082 -9.611547 1 USR MPRFLG 0 ; 
       120 SCHEM 27.72081 -9.611547 1 USR MPRFLG 0 ; 
       121 SCHEM 30.22081 -9.611547 1 USR MPRFLG 0 ; 
       122 SCHEM 32.72081 -9.611547 1 USR MPRFLG 0 ; 
       123 SCHEM 35.22081 -9.611547 1 USR MPRFLG 0 ; 
       124 SCHEM 37.72081 -9.611547 1 USR MPRFLG 0 ; 
       125 SCHEM 40.22081 -9.611547 1 USR MPRFLG 0 ; 
       127 SCHEM -3.529182 -7.611546 1 USR MPRFLG 0 ; 
       126 SCHEM 81.91788 43.69313 1 USR SRT 1 1 1 0 0 0 0 -0.5 0 MPRFLG 0 ; 
       128 SCHEM -59.77918 -7.611546 1 USR MPRFLG 0 ; 
       129 SCHEM -57.27919 -7.611546 1 USR MPRFLG 0 ; 
       130 SCHEM -54.77916 -7.611546 1 USR MPRFLG 0 ; 
       131 SCHEM -52.27916 -7.611546 1 USR MPRFLG 0 ; 
       132 SCHEM -49.77916 -7.611546 1 USR MPRFLG 0 ; 
       133 SCHEM -62.27919 -7.611546 1 USR MPRFLG 0 ; 
       134 SCHEM 42.72082 -7.611546 1 USR MPRFLG 0 ; 
       135 SCHEM 45.22082 -7.611546 1 USR MPRFLG 0 ; 
       136 SCHEM 47.72082 -7.611546 1 USR MPRFLG 0 ; 
       137 SCHEM 50.22082 -7.611546 1 USR MPRFLG 0 ; 
       138 SCHEM 52.72082 -7.611546 1 USR MPRFLG 0 ; 
       179 SCHEM 39.46782 31.66244 0 USR MPRFLG 0 ; 
       189 SCHEM 38.21782 29.66244 0 USR MPRFLG 0 ; 
       178 SCHEM -3.032205 31.66244 0 USR MPRFLG 0 ; 
       187 SCHEM -4.282207 29.66244 0 USR MPRFLG 0 ; 
       180 SCHEM 16.96782 31.66244 0 USR MPRFLG 0 ; 
       188 SCHEM 15.71782 29.66244 0 USR MPRFLG 0 ; 
       181 SCHEM 24.46782 31.66244 0 USR MPRFLG 0 ; 
       190 SCHEM 23.21783 29.66244 0 USR MPRFLG 0 ; 
       182 SCHEM 31.96781 31.66244 0 USR MPRFLG 0 ; 
       186 SCHEM 30.71781 29.66244 0 USR MPRFLG 0 ; 
       194 SCHEM 30.56861 40.95077 0 USR MPRFLG 0 ; 
       195 SCHEM 31.81861 42.95076 0 USR MPRFLG 0 ; 
       199 SCHEM 23.06861 40.95077 0 USR MPRFLG 0 ; 
       201 SCHEM 11.8186 42.95076 0 USR MPRFLG 0 ; 
       202 SCHEM 13.06861 40.95077 0 USR MPRFLG 0 ; 
       203 SCHEM 21.81861 44.95076 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       204 SCHEM 10.56861 40.95077 0 USR MPRFLG 0 ; 
       205 SCHEM 20.56861 42.95076 0 USR MPRFLG 0 ; 
       183 SCHEM 9.467813 29.66244 0 USR MPRFLG 0 ; 
       185 SCHEM 5.717796 27.66244 0 USR MPRFLG 0 ; 
       191 SCHEM 1.967799 29.66244 0 USR MPRFLG 0 ; 
       192 SCHEM 6.967797 31.66244 0 USR SRT 1 1 1 0.6699998 0.152 0 -2.159692 -10.97795 12.63122 MPRFLG 0 ; 
       193 SCHEM 5.717796 29.66244 0 USR MPRFLG 0 ; 
       208 SCHEM -5.058892 15.62115 0 USR SRT 1 7.550001 1 0.03600001 0 -0.6040002 11.92827 -7.458397 44.58141 MPRFLG 0 ; 
       0 SCHEM 54.5033 -29.99257 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       89 SCHEM 41.81418 -37.23277 0 USR SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       139 SCHEM 21.07891 6.487391 1 USR MPRFLG 0 ; 
       140 SCHEM -8.921074 6.487391 1 USR MPRFLG 0 ; 
       141 SCHEM -6.421074 6.487391 1 USR MPRFLG 0 ; 
       142 SCHEM -3.921074 6.487391 1 USR MPRFLG 0 ; 
       143 SCHEM -1.421074 6.487391 1 USR MPRFLG 0 ; 
       144 SCHEM 1.078926 6.487391 1 USR MPRFLG 0 ; 
       145 SCHEM 3.578922 6.487391 1 USR MPRFLG 0 ; 
       146 SCHEM 6.078907 6.487391 1 USR MPRFLG 0 ; 
       147 SCHEM 8.578907 6.487391 1 USR MPRFLG 0 ; 
       148 SCHEM 11.07891 6.487391 1 USR MPRFLG 0 ; 
       149 SCHEM 13.57891 6.487391 1 USR MPRFLG 0 ; 
       150 SCHEM 16.07891 6.487391 1 USR MPRFLG 0 ; 
       151 SCHEM 18.57891 6.487391 1 USR MPRFLG 0 ; 
       152 SCHEM -11.42108 6.487391 1 USR MPRFLG 0 ; 
       153 SCHEM 23.57891 6.487391 1 USR MPRFLG 0 ; 
       154 SCHEM 26.07891 6.487391 1 USR MPRFLG 0 ; 
       155 SCHEM 28.57891 6.487391 1 USR MPRFLG 0 ; 
       156 SCHEM 31.07891 6.487391 1 USR MPRFLG 0 ; 
       157 SCHEM 33.57891 6.487391 1 USR MPRFLG 0 ; 
       158 SCHEM 36.07892 6.487391 1 USR MPRFLG 0 ; 
       198 SCHEM 20.56862 40.95077 0 USR MPRFLG 0 ; 
       169 SCHEM 6.415141 40.95493 0 MPRFLG 0 ; 
       2 SCHEM 19.58974 -44.50392 0 USR MPRFLG 0 ; 
       3 SCHEM 64.58973 -44.50392 0 USR MPRFLG 0 ; 
       4 SCHEM 44.58976 -44.50392 0 USR MPRFLG 0 ; 
       5 SCHEM 47.08976 -44.50392 0 USR MPRFLG 0 ; 
       6 SCHEM 49.58976 -44.50392 0 USR MPRFLG 0 ; 
       7 SCHEM 52.08976 -44.50392 0 USR MPRFLG 0 ; 
       8 SCHEM 54.58974 -44.50392 0 USR MPRFLG 0 ; 
       159 SCHEM 38.57892 6.487391 1 USR MPRFLG 0 ; 
       9 SCHEM 57.08974 -44.50392 0 USR MPRFLG 0 ; 
       10 SCHEM 59.58973 -44.50392 0 USR MPRFLG 0 ; 
       11 SCHEM 62.08973 -44.50392 0 USR MPRFLG 0 ; 
       12 SCHEM 42.08976 -44.50392 0 USR MPRFLG 0 ; 
       13 SCHEM 67.08973 -44.50392 0 USR MPRFLG 0 ; 
       14 SCHEM 69.58973 -44.50392 0 USR MPRFLG 0 ; 
       15 SCHEM 72.08966 -44.50392 0 USR MPRFLG 0 ; 
       16 SCHEM 74.58966 -44.50392 0 USR MPRFLG 0 ; 
       17 SCHEM 77.08966 -44.50392 0 USR MPRFLG 0 ; 
       18 SCHEM 79.58966 -44.50392 0 USR MPRFLG 0 ; 
       160 SCHEM 41.07892 6.487391 1 USR MPRFLG 0 ; 
       19 SCHEM 82.08966 -44.50392 0 USR MPRFLG 0 ; 
       20 SCHEM 84.58966 -44.50392 0 USR MPRFLG 0 ; 
       21 SCHEM -0.4102842 -44.50392 0 USR MPRFLG 0 ; 
       22 SCHEM 2.0897 -44.50392 0 USR MPRFLG 0 ; 
       23 SCHEM 4.589701 -44.50392 0 USR MPRFLG 0 ; 
       24 SCHEM 7.089688 -44.50392 0 USR MPRFLG 0 ; 
       25 SCHEM 9.589689 -44.50392 0 USR MPRFLG 0 ; 
       26 SCHEM 12.08969 -44.50392 0 USR MPRFLG 0 ; 
       27 SCHEM 14.58969 -44.50392 0 USR MPRFLG 0 ; 
       28 SCHEM 17.08974 -44.50392 0 USR MPRFLG 0 ; 
       161 SCHEM 43.57892 6.487391 1 USR MPRFLG 0 ; 
       29 SCHEM -2.910303 -44.50392 0 USR MPRFLG 0 ; 
       30 SCHEM 22.08974 -44.50392 0 USR MPRFLG 0 ; 
       31 SCHEM 24.58974 -44.50392 0 USR MPRFLG 0 ; 
       32 SCHEM 27.08974 -44.50392 0 USR MPRFLG 0 ; 
       33 SCHEM 29.58975 -44.50392 0 USR MPRFLG 0 ; 
       34 SCHEM 32.08975 -44.50392 0 USR MPRFLG 0 ; 
       35 SCHEM 34.58975 -44.50392 0 USR MPRFLG 0 ; 
       36 SCHEM 37.08975 -44.50392 0 USR MPRFLG 0 ; 
       37 SCHEM 39.58976 -44.50392 0 USR MPRFLG 0 ; 
       162 SCHEM 46.07892 6.487391 1 USR MPRFLG 0 ; 
       163 SCHEM 48.57891 6.487391 1 USR MPRFLG 0 ; 
       164 SCHEM 51.07891 6.487391 1 USR MPRFLG 0 ; 
       166 SCHEM -3.584857 38.95493 0 USR MPRFLG 0 ; 
       167 SCHEM -3.584857 36.95494 0 USR MPRFLG 0 ; 
       196 SCHEM 28.06862 42.95076 0 USR MPRFLG 0 ; 
       171 SCHEM -6.084857 38.95493 0 USR MPRFLG 0 ; 
       197 SCHEM 20.56862 38.95077 0 USR MPRFLG 0 ; 
       200 SCHEM 20.56862 36.95078 0 USR MPRFLG 0 ; 
       168 SCHEM -1.084854 40.95493 0 USR MPRFLG 0 ; 
       170 SCHEM 1.415141 40.95493 0 USR MPRFLG 0 ; 
       177 SCHEM 3.915141 40.95493 0 USR MPRFLG 0 ; 
       175 SCHEM -4.834857 40.95493 0 USR MPRFLG 0 ; 
       172 SCHEM 6.415141 38.95493 0 MPRFLG 0 ; 
       165 SCHEM 88.12183 43.65878 1 USR SRT 1 1 1 0 -0.02200001 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 63.33973 -42.50394 0 USR MPRFLG 0 ; 
       38 SCHEM 18.33974 -42.50394 0 USR MPRFLG 0 ; 
       173 SCHEM 2.66514 42.95493 0 USR MPRFLG 0 ; 
       206 SCHEM 16.81862 40.95077 0 USR MPRFLG 0 ; 
       207 SCHEM 16.81862 38.95077 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 54.63553 -2.230893 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 33.06861 40.95077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30.56861 38.95077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 13.06861 38.95077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 18.46782 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14.96782 27.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25.96781 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.46783 27.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 33.46782 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29.96781 27.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 43.06418 -39.23277 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.54677 37.77004 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 86.08972 -42.50394 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.96781 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.967797 25.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 54.5033 -31.99256 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40.9678 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.46782 27.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -1.532205 29.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -5.032206 27.66244 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55.22082 -7.611546 1 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25.5686 40.95077 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 18.0686 36.95078 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 35.96782 27.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -6.532221 27.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 13.46781 27.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20.96782 27.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 28.46781 27.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40.56418 -39.23277 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4.4678 25.66244 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 15.56862 36.95078 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       15 SCHEM 0 0 0 ; 
       16 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
