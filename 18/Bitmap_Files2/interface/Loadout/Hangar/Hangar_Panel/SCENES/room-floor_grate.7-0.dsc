SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       floor_grate-cam_int1.7-0 ROOT ; 
       floor_grate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 31     
       bounce-bounce_off_beam.22-0 ROOT ; 
       fill-inside_hangar_fill1.18-0 ROOT ; 
       floor_grate-from_space_to_hangar2.1-0 ; 
       floor_grate-from_space_to_hangar2_int.7-0 ROOT ; 
       Point-Alarm1.25-0 ROOT ; 
       Vol-floor1.1-0 ; 
       Vol-floor1_int.5-0 ROOT ; 
       Vol-floor10.1-0 ; 
       Vol-floor10_int.2-0 ROOT ; 
       Vol-floor11.1-0 ; 
       Vol-floor11_int.2-0 ROOT ; 
       Vol-floor12.1-0 ; 
       Vol-floor12_int.2-0 ROOT ; 
       Vol-floor3.1-0 ; 
       Vol-floor3_int.5-0 ROOT ; 
       Vol-floor4.1-0 ; 
       Vol-floor4_int.5-0 ROOT ; 
       Vol-floor5.1-0 ; 
       Vol-floor5_int.5-0 ROOT ; 
       Vol-floor6.1-0 ; 
       Vol-floor6_int.5-0 ROOT ; 
       Vol-floor7.1-0 ; 
       Vol-floor7_int.5-0 ROOT ; 
       Vol-floor8.1-0 ; 
       Vol-floor8_int.2-0 ROOT ; 
       Vol-floor9.1-0 ; 
       Vol-floor9_int.2-0 ROOT ; 
       Vol-from_hangar1.1-0 ; 
       Vol-from_hangar1_int.5-0 ROOT ; 
       Vol-Under_Hole.1-0 ; 
       Vol-Under_Hole_int.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 15     
       floor_grate-alarm1.1-0 ; 
       floor_grate-floor1.3-0 ; 
       floor_grate-floor10.2-0 ; 
       floor_grate-floor11.2-0 ; 
       floor_grate-floor2.3-0 ; 
       floor_grate-floor3.3-0 ; 
       floor_grate-floor4.3-0 ; 
       floor_grate-floor5.3-0 ; 
       floor_grate-floor6.3-0 ; 
       floor_grate-floor7.2-0 ; 
       floor_grate-floor8.2-0 ; 
       floor_grate-floor9.2-0 ; 
       floor_grate-from_hangar1.1-0 ; 
       floor_grate-From_Hole_Bottom1.2-0 ; 
       floor_grate-from_space1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       floor_grate-Ceiling1.1-0 ; 
       floor_grate-Constant_Black1.1-0 ; 
       floor_grate-Floor2.2-0 ; 
       floor_grate-mat13.1-0 ; 
       floor_grate-mat14.1-0 ; 
       floor_grate-mat15.1-0 ; 
       floor_grate-mat16.1-0 ; 
       floor_grate-mat17.1-0 ; 
       floor_grate-mat18.1-0 ; 
       floor_grate-mat19.1-0 ; 
       floor_grate-mat20.1-0 ; 
       floor_grate-mat21.1-0 ; 
       floor_grate-mat22.1-0 ; 
       floor_grate-mat23.1-0 ; 
       floor_grate-mat24.2-0 ; 
       floor_grate-mat4.1-0 ; 
       floor_grate-mat5.1-0 ; 
       floor_grate-mat6.1-0 ; 
       floor_grate-mat7.1-0 ; 
       floor_grate-mat8.1-0 ; 
       floor_grate-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 161     
       celing_lights-torus10.1-0 ; 
       celing_lights-tubes.2-0 ROOT ; 
       floor_grate-beam1.1-0 ; 
       floor_grate-beam144.1-0 ; 
       floor_grate-beam145.1-0 ; 
       floor_grate-beam146.1-0 ; 
       floor_grate-beam147.1-0 ; 
       floor_grate-beam148.1-0 ; 
       floor_grate-beam149.1-0 ; 
       floor_grate-beam150.1-0 ; 
       floor_grate-beam151.1-0 ; 
       floor_grate-beam152.1-0 ; 
       floor_grate-beam153.1-0 ; 
       floor_grate-beam154.1-0 ; 
       floor_grate-beam155.1-0 ; 
       floor_grate-beam156.1-0 ; 
       floor_grate-beam157.1-0 ; 
       floor_grate-beam158.1-0 ; 
       floor_grate-beam159.1-0 ; 
       floor_grate-beam196.1-0 ; 
       floor_grate-beam197.1-0 ; 
       floor_grate-beam198.1-0 ; 
       floor_grate-beam199.1-0 ; 
       floor_grate-beam200.1-0 ; 
       floor_grate-beam201.1-0 ; 
       floor_grate-beam202.1-0 ; 
       floor_grate-beam203.1-0 ; 
       floor_grate-beam204.1-0 ; 
       floor_grate-beam205.1-0 ; 
       floor_grate-beam206.1-0 ; 
       floor_grate-beam207.1-0 ; 
       floor_grate-beam208.1-0 ; 
       floor_grate-beam209.1-0 ; 
       floor_grate-beam210.1-0 ; 
       floor_grate-beam211.1-0 ; 
       floor_grate-beam212.1-0 ; 
       floor_grate-beam213.1-0 ; 
       floor_grate-beam214.1-0 ; 
       floor_grate-bionic_volume.1-0 ROOT ; 
       floor_grate-ceiling_light_1.1-0 ROOT ; 
       floor_grate-cube1.1-0 ROOT ; 
       floor_grate-cube10.1-0 ; 
       floor_grate-cube11.1-0 ; 
       floor_grate-cube12.1-0 ; 
       floor_grate-cube17.1-0 ; 
       floor_grate-cube18.1-0 ; 
       floor_grate-cube19.1-0 ; 
       floor_grate-cube2.1-0 ; 
       floor_grate-cube20.1-0 ; 
       floor_grate-cube21.1-0 ; 
       floor_grate-cube22.1-0 ; 
       floor_grate-cube23.1-0 ; 
       floor_grate-cube24.1-0 ; 
       floor_grate-cube25.1-0 ; 
       floor_grate-cube26.1-0 ; 
       floor_grate-cube27.1-0 ; 
       floor_grate-cube28.1-0 ; 
       floor_grate-cube29.1-0 ; 
       floor_grate-cube3.1-0 ; 
       floor_grate-cube30.1-0 ; 
       floor_grate-cube31.1-0 ; 
       floor_grate-cube32.1-0 ROOT ; 
       floor_grate-cube33.1-0 ; 
       floor_grate-cube34.1-0 ; 
       floor_grate-cube34_1.1-0 ; 
       floor_grate-cube34_2.1-0 ROOT ; 
       floor_grate-cube35.1-0 ; 
       floor_grate-cube36.1-0 ; 
       floor_grate-cube37.1-0 ; 
       floor_grate-cube38.1-0 ; 
       floor_grate-cube39.1-0 ; 
       floor_grate-cube4.1-0 ; 
       floor_grate-cube40.1-0 ; 
       floor_grate-cube41.1-0 ; 
       floor_grate-cube42.1-0 ; 
       floor_grate-cube43.1-0 ; 
       floor_grate-cube44.1-0 ; 
       floor_grate-cube45.1-0 ; 
       floor_grate-cube46.1-0 ; 
       floor_grate-cube47.1-0 ; 
       floor_grate-cube48.1-0 ; 
       floor_grate-cube49.1-0 ; 
       floor_grate-cube5.1-0 ; 
       floor_grate-cube50.1-0 ; 
       floor_grate-cube51.1-0 ; 
       floor_grate-cube52.1-0 ; 
       floor_grate-cube53.1-0 ; 
       floor_grate-cube54.1-0 ; 
       floor_grate-cube55.1-0 ; 
       floor_grate-cube56.1-0 ; 
       floor_grate-cube57.1-0 ; 
       floor_grate-cube58.1-0 ; 
       floor_grate-cube59.1-0 ; 
       floor_grate-cube6.1-0 ; 
       floor_grate-cube60.1-0 ; 
       floor_grate-cube61.1-0 ; 
       floor_grate-cube62.1-0 ; 
       floor_grate-cube63.1-0 ; 
       floor_grate-cube64.1-0 ; 
       floor_grate-cube65.1-0 ; 
       floor_grate-cube66.1-0 ; 
       floor_grate-cube67.1-0 ; 
       floor_grate-cube68.1-0 ; 
       floor_grate-cube7.1-0 ; 
       floor_grate-cube8.1-0 ; 
       floor_grate-cube9.1-0 ; 
       floor_grate-cyl20.1-0 ; 
       floor_grate-cyl21.1-0 ; 
       floor_grate-cyl22.1-0 ROOT ; 
       floor_grate-cyl3.1-0 ; 
       floor_grate-Door.1-0 ROOT ; 
       floor_grate-face1.1-0 ; 
       floor_grate-grid1.1-0 ; 
       floor_grate-grid2.1-0 ; 
       floor_grate-grid3.1-0 ; 
       floor_grate-grid4.1-0 ROOT ; 
       floor_grate-hangar_force_field.1-0 ROOT ; 
       floor_grate-null1.1-0 ; 
       floor_grate-null12.1-0 ROOT ; 
       floor_grate-null13.2-0 ROOT ; 
       floor_grate-null14.2-0 ROOT ; 
       floor_grate-null2.1-0 ROOT ; 
       floor_grate-null8.1-0 ROOT ; 
       floor_grate-null9.2-0 ROOT ; 
       floor_grate-torus15.1-0 ; 
       floor_grate-torus19.1-0 ; 
       floor_grate-torus21.1-0 ; 
       floor_grate-torus23.1-0 ; 
       floor_grate-torus24.1-0 ; 
       floor_grate-torus27.1-0 ; 
       floor_grate-torus28.1-0 ; 
       floor_grate-torus29.1-0 ; 
       floor_grate-torus30.1-0 ; 
       floor_grate-torus31.1-0 ; 
       floor_grate-torus32.1-0 ; 
       floor_grate1-cube34.1-0 ROOT ; 
       Monitor-base.5-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.3-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Monitor6-base.5-0 ROOT ; 
       Monitor6-Screen.1-0 ; 
       Monitor7-base.4-0 ROOT ; 
       Monitor7-Screen.1-0 ; 
       Monitor8-base.5-0 ROOT ; 
       Monitor8-Screen.1-0 ; 
       Monitor9-base.5-0 ROOT ; 
       Monitor9-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.16-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-nurbs1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       floor_grate-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-floor_grate.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       floor_grate-t2d1.1-0 ; 
       floor_grate-t2d2.1-0 ; 
       floor_grate-t2d3.1-0 ; 
       floor_grate-t2d4.1-0 ; 
       floor_grate-t2d5.1-0 ; 
       floor_grate-t2d6.1-0 ; 
       floor_grate-t2d7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       floor_grate-Bionic_Volume1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       41 117 110 ; 
       42 117 110 ; 
       43 117 110 ; 
       44 117 110 ; 
       45 117 110 ; 
       46 117 110 ; 
       47 117 110 ; 
       48 117 110 ; 
       49 117 110 ; 
       50 117 110 ; 
       51 117 110 ; 
       52 117 110 ; 
       53 117 110 ; 
       54 117 110 ; 
       55 117 110 ; 
       56 117 110 ; 
       57 117 110 ; 
       58 117 110 ; 
       59 117 110 ; 
       60 117 110 ; 
       62 121 110 ; 
       71 117 110 ; 
       82 117 110 ; 
       93 117 110 ; 
       103 117 110 ; 
       104 117 110 ; 
       105 117 110 ; 
       106 115 110 ; 
       107 109 110 ; 
       109 115 110 ; 
       112 121 110 ; 
       113 121 110 ; 
       114 121 110 ; 
       117 122 110 ; 
       0 1 110 ; 
       137 136 110 ; 
       139 138 110 ; 
       141 140 110 ; 
       143 142 110 ; 
       145 144 110 ; 
       147 146 110 ; 
       148 149 110 ; 
       152 151 110 ; 
       149 153 110 ; 
       150 155 110 ; 
       151 153 110 ; 
       155 153 110 ; 
       156 160 110 ; 
       157 160 110 ; 
       158 157 110 ; 
       159 160 110 ; 
       154 151 110 ; 
       126 123 110 ; 
       132 123 110 ; 
       127 123 110 ; 
       124 123 110 ; 
       125 123 110 ; 
       128 123 110 ; 
       134 123 110 ; 
       129 123 110 ; 
       130 123 110 ; 
       131 123 110 ; 
       133 123 110 ; 
       64 119 110 ; 
       66 119 110 ; 
       67 119 110 ; 
       68 119 110 ; 
       69 119 110 ; 
       70 119 110 ; 
       72 119 110 ; 
       73 119 110 ; 
       74 119 110 ; 
       75 119 110 ; 
       76 119 110 ; 
       77 119 110 ; 
       78 119 110 ; 
       79 119 110 ; 
       80 119 110 ; 
       81 119 110 ; 
       83 119 110 ; 
       84 119 110 ; 
       63 120 110 ; 
       85 120 110 ; 
       86 120 110 ; 
       87 120 110 ; 
       88 120 110 ; 
       89 120 110 ; 
       90 120 110 ; 
       91 120 110 ; 
       92 120 110 ; 
       94 120 110 ; 
       95 120 110 ; 
       96 120 110 ; 
       97 120 110 ; 
       98 120 110 ; 
       99 120 110 ; 
       100 120 110 ; 
       101 120 110 ; 
       102 120 110 ; 
       111 110 110 ; 
       2 118 110 ; 
       3 118 110 ; 
       4 118 110 ; 
       5 118 110 ; 
       6 118 110 ; 
       7 118 110 ; 
       8 118 110 ; 
       9 118 110 ; 
       10 118 110 ; 
       11 118 110 ; 
       12 118 110 ; 
       13 118 110 ; 
       14 118 110 ; 
       15 118 110 ; 
       16 118 110 ; 
       17 118 110 ; 
       18 118 110 ; 
       19 118 110 ; 
       20 118 110 ; 
       21 118 110 ; 
       22 118 110 ; 
       23 118 110 ; 
       24 118 110 ; 
       25 118 110 ; 
       26 118 110 ; 
       27 118 110 ; 
       28 118 110 ; 
       29 118 110 ; 
       30 118 110 ; 
       31 118 110 ; 
       32 118 110 ; 
       33 118 110 ; 
       34 118 110 ; 
       35 118 110 ; 
       36 118 110 ; 
       37 118 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       38 15 300 ; 
       39 11 300 ; 
       116 13 300 ; 
       1 12 300 ; 
       136 16 300 ; 
       137 17 300 ; 
       138 18 300 ; 
       139 19 300 ; 
       140 3 300 ; 
       141 4 300 ; 
       142 5 300 ; 
       143 6 300 ; 
       144 7 300 ; 
       145 8 300 ; 
       146 9 300 ; 
       147 10 300 ; 
       148 1 300 ; 
       152 2 300 ; 
       149 0 300 ; 
       155 20 300 ; 
       154 2 300 ; 
       120 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       39 6 400 ; 
       137 0 400 ; 
       139 1 400 ; 
       141 2 400 ; 
       143 3 400 ; 
       145 4 400 ; 
       147 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       23 24 2110 ; 
       25 26 2110 ; 
       27 28 2110 ; 
       29 30 2110 ; 
       5 6 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
       21 22 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       11 12 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       15 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       3 14 550 ; 
       24 9 550 ; 
       4 0 550 ; 
       28 12 550 ; 
       30 13 550 ; 
       6 1 550 ; 
       26 10 550 ; 
       14 4 550 ; 
       16 5 550 ; 
       18 6 550 ; 
       20 7 550 ; 
       22 8 550 ; 
       8 11 550 ; 
       10 2 550 ; 
       12 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 116 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 5 551 1 ; 
       0 7 551 1 ; 
       0 9 551 1 ; 
       0 11 551 1 ; 
       0 13 551 1 ; 
       0 15 551 1 ; 
       0 17 551 1 ; 
       0 19 551 1 ; 
       0 21 551 1 ; 
       0 23 551 1 ; 
       0 25 551 1 ; 
       0 27 551 1 ; 
       0 29 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 401 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 401 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM 421 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       24 SCHEM 421 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 423.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 398.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 403.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 393.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 396 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 396 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       29 SCHEM 391 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       30 SCHEM 391 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 406 -2 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 406 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       26 SCHEM 423.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 408.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 408.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 411 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 411 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 413.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 413.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 416 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 416 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 418.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 418.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 426 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 426 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 428.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 428.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 431 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 431 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       38 SCHEM 198.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 143.75 0 0 SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       40 SCHEM 10 0 0 SRT 1 1 1 0 -0.8726646 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 50 -4 0 MPRFLG 0 ; 
       42 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       43 SCHEM 55 -4 0 MPRFLG 0 ; 
       44 SCHEM 30 -4 0 MPRFLG 0 ; 
       45 SCHEM 60 -4 0 MPRFLG 0 ; 
       46 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       47 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       48 SCHEM 65 -4 0 MPRFLG 0 ; 
       49 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       50 SCHEM 70 -4 0 MPRFLG 0 ; 
       51 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       52 SCHEM 75 -4 0 MPRFLG 0 ; 
       53 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       54 SCHEM 80 -4 0 MPRFLG 0 ; 
       55 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       56 SCHEM 85 -4 0 MPRFLG 0 ; 
       57 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       58 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       59 SCHEM 90 -4 0 MPRFLG 0 ; 
       60 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       61 SCHEM 12.5 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       62 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       71 SCHEM 35 -4 0 MPRFLG 0 ; 
       82 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       93 SCHEM 40 -4 0 MPRFLG 0 ; 
       103 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       104 SCHEM 45 -4 0 MPRFLG 0 ; 
       105 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       106 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       107 SCHEM 15 -4 0 MPRFLG 0 ; 
       108 SCHEM 140 0 0 SRT 6.050002 11.52707 6.050002 0 0 0 0 0 0 MPRFLG 0 ; 
       109 SCHEM 15 -2 0 MPRFLG 0 ; 
       112 SCHEM 20 -2 0 MPRFLG 0 ; 
       113 SCHEM 25 -2 0 MPRFLG 0 ; 
       114 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       115 SCHEM 16.25 0 0 SRT 1.42 1 2.037903 0 0.2792527 0 67.10641 -18.64181 -18.7254 MPRFLG 0 ; 
       116 SCHEM 152.5 0 0 SRT 1 1 1 0 0 0 147.0213 -22.28944 -177.9266 MPRFLG 0 ; 
       117 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       121 SCHEM 23.75 0 0 SRT 1.166 1.66738 1.166 0 0 0 181.1734 -0.0535363 -128.4584 MPRFLG 0 ; 
       122 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 147.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 148.75 0 0 SRT 1 1 1 0 -0.4 0 0 17.30421 0 MPRFLG 0 ; 
       136 SCHEM 97.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       137 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       138 SCHEM 105 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       139 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       140 SCHEM 112.5 0 0 SRT 1.471141 0.9625919 0.9625919 0.4977454 0.5151278 -0.00364445 8.855516 6.308922 9.169995 MPRFLG 0 ; 
       141 SCHEM 111.25 -2 0 MPRFLG 0 ; 
       142 SCHEM 120 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3516835 0.2844428 0.001876728 10.20939 -8.125242 8.293537 MPRFLG 0 ; 
       143 SCHEM 118.75 -2 0 MPRFLG 0 ; 
       144 SCHEM 127.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3081128 -0.06048232 -3.158986 -5.119568 5.063054 13.15584 MPRFLG 0 ; 
       145 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       146 SCHEM 135 0 0 SRT 1.471141 0.9625919 0.9625919 -0.02179375 -0.1563777 -3.104043 -5.474182 -6.385276 14.13102 MPRFLG 0 ; 
       147 SCHEM 133.75 -2 0 MPRFLG 0 ; 
       148 SCHEM 160 -4 0 MPRFLG 0 ; 
       152 SCHEM 155 -4 0 MPRFLG 0 ; 
       149 SCHEM 161.25 -2 0 MPRFLG 0 ; 
       150 SCHEM 165 -4 0 MPRFLG 0 ; 
       151 SCHEM 156.25 -2 0 MPRFLG 0 ; 
       153 SCHEM 161.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       155 SCHEM 166.25 -2 0 MPRFLG 0 ; 
       156 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       157 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       158 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       159 SCHEM 5 -2 0 MPRFLG 0 ; 
       160 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       154 SCHEM 157.5 -4 0 MPRFLG 0 ; 
       126 SCHEM 175 -2 0 MPRFLG 0 ; 
       132 SCHEM 190 -2 0 MPRFLG 0 ; 
       127 SCHEM 177.5 -2 0 MPRFLG 0 ; 
       124 SCHEM 170 -2 0 MPRFLG 0 ; 
       125 SCHEM 172.5 -2 0 MPRFLG 0 ; 
       123 SCHEM 182.5 0 0 SRT 1 1 1 0 0 0 0 -0.5 0 MPRFLG 0 ; 
       128 SCHEM 180 -2 0 MPRFLG 0 ; 
       134 SCHEM 195 -2 0 MPRFLG 0 ; 
       129 SCHEM 182.5 -2 0 MPRFLG 0 ; 
       130 SCHEM 185 -2 0 MPRFLG 0 ; 
       131 SCHEM 187.5 -2 0 MPRFLG 0 ; 
       133 SCHEM 192.5 -2 0 MPRFLG 0 ; 
       65 SCHEM 291 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       119 SCHEM 314.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       64 SCHEM 316 -2 0 MPRFLG 0 ; 
       66 SCHEM 296 -2 0 MPRFLG 0 ; 
       67 SCHEM 298.5 -2 0 MPRFLG 0 ; 
       68 SCHEM 301 -2 0 MPRFLG 0 ; 
       69 SCHEM 303.5 -2 0 MPRFLG 0 ; 
       70 SCHEM 306 -2 0 MPRFLG 0 ; 
       72 SCHEM 308.5 -2 0 MPRFLG 0 ; 
       73 SCHEM 311 -2 0 MPRFLG 0 ; 
       74 SCHEM 313.5 -2 0 MPRFLG 0 ; 
       75 SCHEM 293.5 -2 0 MPRFLG 0 ; 
       76 SCHEM 318.5 -2 0 MPRFLG 0 ; 
       77 SCHEM 321 -2 0 MPRFLG 0 ; 
       78 SCHEM 323.5 -2 0 MPRFLG 0 ; 
       79 SCHEM 326 -2 0 MPRFLG 0 ; 
       80 SCHEM 328.5 -2 0 MPRFLG 0 ; 
       81 SCHEM 331 -2 0 MPRFLG 0 ; 
       83 SCHEM 333.5 -2 0 MPRFLG 0 ; 
       84 SCHEM 336 -2 0 MPRFLG 0 ; 
       135 SCHEM 338.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       120 SCHEM 363.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       63 SCHEM 363.5 -2 0 MPRFLG 0 ; 
       85 SCHEM 343.5 -2 0 MPRFLG 0 ; 
       86 SCHEM 346 -2 0 MPRFLG 0 ; 
       87 SCHEM 348.5 -2 0 MPRFLG 0 ; 
       88 SCHEM 351 -2 0 MPRFLG 0 ; 
       89 SCHEM 353.5 -2 0 MPRFLG 0 ; 
       90 SCHEM 356 -2 0 MPRFLG 0 ; 
       91 SCHEM 358.5 -2 0 MPRFLG 0 ; 
       92 SCHEM 361 -2 0 MPRFLG 0 ; 
       94 SCHEM 341 -2 0 MPRFLG 0 ; 
       95 SCHEM 366 -2 0 MPRFLG 0 ; 
       96 SCHEM 368.5 -2 0 MPRFLG 0 ; 
       97 SCHEM 371 -2 0 MPRFLG 0 ; 
       98 SCHEM 373.5 -2 0 MPRFLG 0 ; 
       99 SCHEM 376 -2 0 MPRFLG 0 ; 
       100 SCHEM 378.5 -2 0 MPRFLG 0 ; 
       101 SCHEM 381 -2 0 MPRFLG 0 ; 
       102 SCHEM 383.5 -2 0 MPRFLG 0 ; 
       111 SCHEM 388.5 -2 0 MPRFLG 0 ; 
       110 SCHEM 388.5 0 0 SRT 1.082 1.082 1.082 0 -1.34 0 51.55545 0 -14.8918 MPRFLG 0 ; 
       118 SCHEM 244.75 0 0 SRT 1 0.2900001 1 0 0 0 0 -12.54536 0 MPRFLG 0 ; 
       2 SCHEM 241 -2 0 MPRFLG 0 ; 
       3 SCHEM 201 -2 0 MPRFLG 0 ; 
       4 SCHEM 203.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 206 -2 0 MPRFLG 0 ; 
       6 SCHEM 208.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 211 -2 0 MPRFLG 0 ; 
       8 SCHEM 213.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 216 -2 0 MPRFLG 0 ; 
       10 SCHEM 218.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 221 -2 0 MPRFLG 0 ; 
       12 SCHEM 223.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 226 -2 0 MPRFLG 0 ; 
       14 SCHEM 228.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 231 -2 0 MPRFLG 0 ; 
       16 SCHEM 233.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 236 -2 0 MPRFLG 0 ; 
       18 SCHEM 238.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 243.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 246 -2 0 MPRFLG 0 ; 
       21 SCHEM 248.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 251 -2 0 MPRFLG 0 ; 
       23 SCHEM 253.5 -2 0 MPRFLG 0 ; 
       24 SCHEM 256 -2 0 MPRFLG 0 ; 
       25 SCHEM 258.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 261 -2 0 MPRFLG 0 ; 
       27 SCHEM 263.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 266 -2 0 MPRFLG 0 ; 
       29 SCHEM 268.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 271 -2 0 MPRFLG 0 ; 
       31 SCHEM 273.5 -2 0 MPRFLG 0 ; 
       32 SCHEM 276 -2 0 MPRFLG 0 ; 
       33 SCHEM 278.5 -2 0 MPRFLG 0 ; 
       34 SCHEM 281 -2 0 MPRFLG 0 ; 
       35 SCHEM 283.5 -2 0 MPRFLG 0 ; 
       36 SCHEM 286 -2 0 MPRFLG 0 ; 
       37 SCHEM 288.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       20 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 122.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 152.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 197.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 386 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 110 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 117.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 132.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
