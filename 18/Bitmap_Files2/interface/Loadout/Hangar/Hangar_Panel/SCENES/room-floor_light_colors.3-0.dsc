SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       floor_light_colors-cam_int1.3-0 ROOT ; 
       floor_light_colors-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 31     
       bounce-bounce_off_beam.26-0 ROOT ; 
       fill-inside_hangar_fill1.22-0 ROOT ; 
       floor_light_colors-from_space_to_hangar2.1-0 ; 
       floor_light_colors-from_space_to_hangar2_int.3-0 ROOT ; 
       Point-Alarm1.29-0 ROOT ; 
       Vol-floor1.1-0 ; 
       Vol-floor1_int.9-0 ROOT ; 
       Vol-floor10.1-0 ; 
       Vol-floor10_int.6-0 ROOT ; 
       Vol-floor11.1-0 ; 
       Vol-floor11_int.6-0 ROOT ; 
       Vol-floor12.1-0 ; 
       Vol-floor12_int.6-0 ROOT ; 
       Vol-floor3.1-0 ; 
       Vol-floor3_int.9-0 ROOT ; 
       Vol-floor4.1-0 ; 
       Vol-floor4_int.9-0 ROOT ; 
       Vol-floor5.1-0 ; 
       Vol-floor5_int.9-0 ROOT ; 
       Vol-floor6.1-0 ; 
       Vol-floor6_int.9-0 ROOT ; 
       Vol-floor7.1-0 ; 
       Vol-floor7_int.9-0 ROOT ; 
       Vol-floor8.1-0 ; 
       Vol-floor8_int.6-0 ROOT ; 
       Vol-floor9.1-0 ; 
       Vol-floor9_int.6-0 ROOT ; 
       Vol-from_hangar1.1-0 ; 
       Vol-from_hangar1_int.9-0 ROOT ; 
       Vol-Under_Hole.1-0 ; 
       Vol-Under_Hole_int.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 15     
       floor_light_colors-alarm1.1-0 ; 
       floor_light_colors-floor1.1-0 ; 
       floor_light_colors-floor10.1-0 ; 
       floor_light_colors-floor11.1-0 ; 
       floor_light_colors-floor2.1-0 ; 
       floor_light_colors-floor3.1-0 ; 
       floor_light_colors-floor4.1-0 ; 
       floor_light_colors-floor5.1-0 ; 
       floor_light_colors-floor6.1-0 ; 
       floor_light_colors-floor7.1-0 ; 
       floor_light_colors-floor8.1-0 ; 
       floor_light_colors-floor9.1-0 ; 
       floor_light_colors-from_hangar1.1-0 ; 
       floor_light_colors-From_Hole_Bottom1.1-0 ; 
       floor_light_colors-from_space1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       floor_light_colors-Ceiling1.1-0 ; 
       floor_light_colors-Constant_Black1.1-0 ; 
       floor_light_colors-Floor2.1-0 ; 
       floor_light_colors-mat13.1-0 ; 
       floor_light_colors-mat14.1-0 ; 
       floor_light_colors-mat15.1-0 ; 
       floor_light_colors-mat16.1-0 ; 
       floor_light_colors-mat17.1-0 ; 
       floor_light_colors-mat18.1-0 ; 
       floor_light_colors-mat19.1-0 ; 
       floor_light_colors-mat20.1-0 ; 
       floor_light_colors-mat21.1-0 ; 
       floor_light_colors-mat22.1-0 ; 
       floor_light_colors-mat23.1-0 ; 
       floor_light_colors-mat24.1-0 ; 
       floor_light_colors-mat4.1-0 ; 
       floor_light_colors-mat5.1-0 ; 
       floor_light_colors-mat6.1-0 ; 
       floor_light_colors-mat7.1-0 ; 
       floor_light_colors-mat8.1-0 ; 
       floor_light_colors-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 161     
       celing_lights-torus10.1-0 ; 
       celing_lights-tubes.2-0 ROOT ; 
       floor_grate1-cube34.1-0 ROOT ; 
       floor_light_colors-beam1.1-0 ; 
       floor_light_colors-beam144.1-0 ; 
       floor_light_colors-beam145.1-0 ; 
       floor_light_colors-beam146.1-0 ; 
       floor_light_colors-beam147.1-0 ; 
       floor_light_colors-beam148.1-0 ; 
       floor_light_colors-beam149.1-0 ; 
       floor_light_colors-beam150.1-0 ; 
       floor_light_colors-beam151.1-0 ; 
       floor_light_colors-beam152.1-0 ; 
       floor_light_colors-beam153.1-0 ; 
       floor_light_colors-beam154.1-0 ; 
       floor_light_colors-beam155.1-0 ; 
       floor_light_colors-beam156.1-0 ; 
       floor_light_colors-beam157.1-0 ; 
       floor_light_colors-beam158.1-0 ; 
       floor_light_colors-beam159.1-0 ; 
       floor_light_colors-beam196.1-0 ; 
       floor_light_colors-beam197.1-0 ; 
       floor_light_colors-beam198.1-0 ; 
       floor_light_colors-beam199.1-0 ; 
       floor_light_colors-beam200.1-0 ; 
       floor_light_colors-beam201.1-0 ; 
       floor_light_colors-beam202.1-0 ; 
       floor_light_colors-beam203.1-0 ; 
       floor_light_colors-beam204.1-0 ; 
       floor_light_colors-beam205.1-0 ; 
       floor_light_colors-beam206.1-0 ; 
       floor_light_colors-beam207.1-0 ; 
       floor_light_colors-beam208.1-0 ; 
       floor_light_colors-beam209.1-0 ; 
       floor_light_colors-beam210.1-0 ; 
       floor_light_colors-beam211.1-0 ; 
       floor_light_colors-beam212.1-0 ; 
       floor_light_colors-beam213.1-0 ; 
       floor_light_colors-beam214.1-0 ; 
       floor_light_colors-bionic_volume.1-0 ROOT ; 
       floor_light_colors-ceiling_light_1.1-0 ROOT ; 
       floor_light_colors-cube1.1-0 ROOT ; 
       floor_light_colors-cube10.1-0 ; 
       floor_light_colors-cube11.1-0 ; 
       floor_light_colors-cube12.1-0 ; 
       floor_light_colors-cube17.1-0 ; 
       floor_light_colors-cube18.1-0 ; 
       floor_light_colors-cube19.1-0 ; 
       floor_light_colors-cube2.1-0 ; 
       floor_light_colors-cube20.1-0 ; 
       floor_light_colors-cube21.1-0 ; 
       floor_light_colors-cube22.1-0 ; 
       floor_light_colors-cube23.1-0 ; 
       floor_light_colors-cube24.1-0 ; 
       floor_light_colors-cube25.1-0 ; 
       floor_light_colors-cube26.1-0 ; 
       floor_light_colors-cube27.1-0 ; 
       floor_light_colors-cube28.1-0 ; 
       floor_light_colors-cube29.1-0 ; 
       floor_light_colors-cube3.1-0 ; 
       floor_light_colors-cube30.1-0 ; 
       floor_light_colors-cube31.1-0 ; 
       floor_light_colors-cube32.1-0 ROOT ; 
       floor_light_colors-cube33.1-0 ; 
       floor_light_colors-cube34.1-0 ; 
       floor_light_colors-cube34_1.1-0 ; 
       floor_light_colors-cube34_2.1-0 ROOT ; 
       floor_light_colors-cube35.1-0 ; 
       floor_light_colors-cube36.1-0 ; 
       floor_light_colors-cube37.1-0 ; 
       floor_light_colors-cube38.1-0 ; 
       floor_light_colors-cube39.1-0 ; 
       floor_light_colors-cube4.1-0 ; 
       floor_light_colors-cube40.1-0 ; 
       floor_light_colors-cube41.1-0 ; 
       floor_light_colors-cube42.1-0 ; 
       floor_light_colors-cube43.1-0 ; 
       floor_light_colors-cube44.1-0 ; 
       floor_light_colors-cube45.1-0 ; 
       floor_light_colors-cube46.1-0 ; 
       floor_light_colors-cube47.1-0 ; 
       floor_light_colors-cube48.1-0 ; 
       floor_light_colors-cube49.1-0 ; 
       floor_light_colors-cube5.1-0 ; 
       floor_light_colors-cube50.1-0 ; 
       floor_light_colors-cube51.1-0 ; 
       floor_light_colors-cube52.1-0 ; 
       floor_light_colors-cube53.1-0 ; 
       floor_light_colors-cube54.1-0 ; 
       floor_light_colors-cube55.1-0 ; 
       floor_light_colors-cube56.1-0 ; 
       floor_light_colors-cube57.1-0 ; 
       floor_light_colors-cube58.1-0 ; 
       floor_light_colors-cube59.1-0 ; 
       floor_light_colors-cube6.1-0 ; 
       floor_light_colors-cube60.1-0 ; 
       floor_light_colors-cube61.1-0 ; 
       floor_light_colors-cube62.1-0 ; 
       floor_light_colors-cube63.1-0 ; 
       floor_light_colors-cube64.1-0 ; 
       floor_light_colors-cube65.1-0 ; 
       floor_light_colors-cube66.1-0 ; 
       floor_light_colors-cube67.1-0 ; 
       floor_light_colors-cube68.1-0 ; 
       floor_light_colors-cube7.1-0 ; 
       floor_light_colors-cube8.1-0 ; 
       floor_light_colors-cube9.1-0 ; 
       floor_light_colors-cyl20.1-0 ; 
       floor_light_colors-cyl21.1-0 ; 
       floor_light_colors-cyl22.1-0 ROOT ; 
       floor_light_colors-cyl3.1-0 ; 
       floor_light_colors-Door.1-0 ROOT ; 
       floor_light_colors-face1.1-0 ; 
       floor_light_colors-grid1.1-0 ; 
       floor_light_colors-grid2.1-0 ; 
       floor_light_colors-grid3.1-0 ; 
       floor_light_colors-grid4.1-0 ROOT ; 
       floor_light_colors-hangar_force_field.1-0 ROOT ; 
       floor_light_colors-null1.1-0 ; 
       floor_light_colors-null12.1-0 ROOT ; 
       floor_light_colors-null13.1-0 ROOT ; 
       floor_light_colors-null14.1-0 ROOT ; 
       floor_light_colors-null2.1-0 ROOT ; 
       floor_light_colors-null8.1-0 ROOT ; 
       floor_light_colors-null9.1-0 ROOT ; 
       floor_light_colors-torus15.1-0 ; 
       floor_light_colors-torus19.1-0 ; 
       floor_light_colors-torus21.1-0 ; 
       floor_light_colors-torus23.1-0 ; 
       floor_light_colors-torus24.1-0 ; 
       floor_light_colors-torus27.1-0 ; 
       floor_light_colors-torus28.1-0 ; 
       floor_light_colors-torus29.1-0 ; 
       floor_light_colors-torus30.1-0 ; 
       floor_light_colors-torus31.1-0 ; 
       floor_light_colors-torus32.1-0 ; 
       Monitor-base.5-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.3-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Monitor6-base.5-0 ROOT ; 
       Monitor6-Screen.1-0 ; 
       Monitor7-base.4-0 ROOT ; 
       Monitor7-Screen.1-0 ; 
       Monitor8-base.5-0 ROOT ; 
       Monitor8-Screen.1-0 ; 
       Monitor9-base.5-0 ROOT ; 
       Monitor9-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-pit_guard.1-0 ; 
       Room_Base-Room_Root.16-0 ROOT ; 
       Room_Base-subfloor.1-0 ; 
       Room_Base-Wall.1-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-nurbs1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       floor_light_colors-hangar_field1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-floor_light_colors.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       floor_light_colors-t2d1.1-0 ; 
       floor_light_colors-t2d2.1-0 ; 
       floor_light_colors-t2d3.1-0 ; 
       floor_light_colors-t2d4.1-0 ; 
       floor_light_colors-t2d5.1-0 ; 
       floor_light_colors-t2d6.1-0 ; 
       floor_light_colors-t2d7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       floor_light_colors-Bionic_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       42 118 110 ; 
       43 118 110 ; 
       44 118 110 ; 
       45 118 110 ; 
       46 118 110 ; 
       47 118 110 ; 
       48 118 110 ; 
       49 118 110 ; 
       50 118 110 ; 
       51 118 110 ; 
       52 118 110 ; 
       53 118 110 ; 
       54 118 110 ; 
       55 118 110 ; 
       56 118 110 ; 
       57 118 110 ; 
       58 118 110 ; 
       59 118 110 ; 
       60 118 110 ; 
       61 118 110 ; 
       63 122 110 ; 
       72 118 110 ; 
       83 118 110 ; 
       94 118 110 ; 
       104 118 110 ; 
       105 118 110 ; 
       106 118 110 ; 
       107 116 110 ; 
       108 110 110 ; 
       110 116 110 ; 
       113 122 110 ; 
       114 122 110 ; 
       115 122 110 ; 
       118 123 110 ; 
       0 1 110 ; 
       137 136 110 ; 
       139 138 110 ; 
       141 140 110 ; 
       143 142 110 ; 
       145 144 110 ; 
       147 146 110 ; 
       148 149 110 ; 
       152 151 110 ; 
       149 153 110 ; 
       150 155 110 ; 
       151 153 110 ; 
       155 153 110 ; 
       156 160 110 ; 
       157 160 110 ; 
       158 157 110 ; 
       159 160 110 ; 
       154 151 110 ; 
       127 124 110 ; 
       133 124 110 ; 
       128 124 110 ; 
       125 124 110 ; 
       126 124 110 ; 
       129 124 110 ; 
       135 124 110 ; 
       130 124 110 ; 
       131 124 110 ; 
       132 124 110 ; 
       134 124 110 ; 
       65 120 110 ; 
       67 120 110 ; 
       68 120 110 ; 
       69 120 110 ; 
       70 120 110 ; 
       71 120 110 ; 
       73 120 110 ; 
       74 120 110 ; 
       75 120 110 ; 
       76 120 110 ; 
       77 120 110 ; 
       78 120 110 ; 
       79 120 110 ; 
       80 120 110 ; 
       81 120 110 ; 
       82 120 110 ; 
       84 120 110 ; 
       85 120 110 ; 
       64 121 110 ; 
       86 121 110 ; 
       87 121 110 ; 
       88 121 110 ; 
       89 121 110 ; 
       90 121 110 ; 
       91 121 110 ; 
       92 121 110 ; 
       93 121 110 ; 
       95 121 110 ; 
       96 121 110 ; 
       97 121 110 ; 
       98 121 110 ; 
       99 121 110 ; 
       100 121 110 ; 
       101 121 110 ; 
       102 121 110 ; 
       103 121 110 ; 
       112 111 110 ; 
       3 119 110 ; 
       4 119 110 ; 
       5 119 110 ; 
       6 119 110 ; 
       7 119 110 ; 
       8 119 110 ; 
       9 119 110 ; 
       10 119 110 ; 
       11 119 110 ; 
       12 119 110 ; 
       13 119 110 ; 
       14 119 110 ; 
       15 119 110 ; 
       16 119 110 ; 
       17 119 110 ; 
       18 119 110 ; 
       19 119 110 ; 
       20 119 110 ; 
       21 119 110 ; 
       22 119 110 ; 
       23 119 110 ; 
       24 119 110 ; 
       25 119 110 ; 
       26 119 110 ; 
       27 119 110 ; 
       28 119 110 ; 
       29 119 110 ; 
       30 119 110 ; 
       31 119 110 ; 
       32 119 110 ; 
       33 119 110 ; 
       34 119 110 ; 
       35 119 110 ; 
       36 119 110 ; 
       37 119 110 ; 
       38 119 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       39 15 300 ; 
       40 11 300 ; 
       117 13 300 ; 
       1 12 300 ; 
       136 16 300 ; 
       137 17 300 ; 
       138 18 300 ; 
       139 19 300 ; 
       140 3 300 ; 
       141 4 300 ; 
       142 5 300 ; 
       143 6 300 ; 
       144 7 300 ; 
       145 8 300 ; 
       146 9 300 ; 
       147 10 300 ; 
       148 1 300 ; 
       152 2 300 ; 
       149 0 300 ; 
       155 20 300 ; 
       154 2 300 ; 
       121 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       40 6 400 ; 
       137 0 400 ; 
       139 1 400 ; 
       141 2 400 ; 
       143 3 400 ; 
       145 4 400 ; 
       147 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
       23 24 2110 ; 
       25 26 2110 ; 
       27 28 2110 ; 
       29 30 2110 ; 
       5 6 2110 ; 
       13 14 2110 ; 
       15 16 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
       21 22 2110 ; 
       7 8 2110 ; 
       9 10 2110 ; 
       11 12 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       15 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       3 14 550 ; 
       24 9 550 ; 
       4 0 550 ; 
       28 12 550 ; 
       30 13 550 ; 
       6 1 550 ; 
       26 10 550 ; 
       14 4 550 ; 
       16 5 550 ; 
       18 6 550 ; 
       20 7 550 ; 
       22 8 550 ; 
       8 11 550 ; 
       10 2 550 ; 
       12 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 117 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 5 551 1 ; 
       0 7 551 1 ; 
       0 9 551 1 ; 
       0 11 551 1 ; 
       0 13 551 1 ; 
       0 15 551 1 ; 
       0 17 551 1 ; 
       0 19 551 1 ; 
       0 21 551 1 ; 
       0 23 551 1 ; 
       0 25 551 1 ; 
       0 27 551 1 ; 
       0 29 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 401 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 401 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       23 SCHEM 421 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       24 SCHEM 421 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 423.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 398.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 403.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 393.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       27 SCHEM 396 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 396 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       29 SCHEM 391 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       30 SCHEM 391 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 406 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 406 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 423.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 408.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 408.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 411 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 411 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 413.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 413.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 416 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 416 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       21 SCHEM 418.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 418.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 426 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 426 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 428.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 428.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 431 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 431 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       39 SCHEM 198.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 143.75 0 0 SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       41 SCHEM 10 0 0 SRT 1 1 1 0 -0.8726646 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 50 -4 0 MPRFLG 0 ; 
       43 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       44 SCHEM 55 -4 0 MPRFLG 0 ; 
       45 SCHEM 30 -4 0 MPRFLG 0 ; 
       46 SCHEM 60 -4 0 MPRFLG 0 ; 
       47 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       48 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       49 SCHEM 65 -4 0 MPRFLG 0 ; 
       50 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       51 SCHEM 70 -4 0 MPRFLG 0 ; 
       52 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       53 SCHEM 75 -4 0 MPRFLG 0 ; 
       54 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       55 SCHEM 80 -4 0 MPRFLG 0 ; 
       56 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       57 SCHEM 85 -4 0 MPRFLG 0 ; 
       58 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       59 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       60 SCHEM 90 -4 0 MPRFLG 0 ; 
       61 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       62 SCHEM 12.5 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       63 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       72 SCHEM 35 -4 0 MPRFLG 0 ; 
       83 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       94 SCHEM 40 -4 0 MPRFLG 0 ; 
       104 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       105 SCHEM 45 -4 0 MPRFLG 0 ; 
       106 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       107 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       108 SCHEM 15 -4 0 MPRFLG 0 ; 
       109 SCHEM 140 0 0 SRT 6.050002 11.52707 6.050002 0 0 0 0 0 0 MPRFLG 0 ; 
       110 SCHEM 15 -2 0 MPRFLG 0 ; 
       113 SCHEM 20 -2 0 MPRFLG 0 ; 
       114 SCHEM 25 -2 0 MPRFLG 0 ; 
       115 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       116 SCHEM 16.25 0 0 SRT 1.42 1 2.037903 0 0.2792527 0 67.10641 -18.64181 -18.7254 MPRFLG 0 ; 
       117 SCHEM 152.5 0 0 SRT 1 1 1 0 0 0 147.0213 -22.28944 -177.9266 MPRFLG 0 ; 
       118 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       122 SCHEM 23.75 0 0 SRT 1.166 1.66738 1.166 0 0 0 181.1734 -0.0535363 -128.4584 MPRFLG 0 ; 
       123 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 147.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 148.75 0 0 SRT 1 1 1 0 -0.4 0 0 17.30421 0 MPRFLG 0 ; 
       136 SCHEM 97.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       137 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       138 SCHEM 105 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       139 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       140 SCHEM 112.5 0 0 SRT 1.471141 0.9625919 0.9625919 0.4977454 0.5151278 -0.00364445 8.855516 6.308922 9.169995 MPRFLG 0 ; 
       141 SCHEM 111.25 -2 0 MPRFLG 0 ; 
       142 SCHEM 120 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3516835 0.2844428 0.001876728 10.20939 -8.125242 8.293537 MPRFLG 0 ; 
       143 SCHEM 118.75 -2 0 MPRFLG 0 ; 
       144 SCHEM 127.5 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3081128 -0.06048232 -3.158986 -5.119568 5.063054 13.15584 MPRFLG 0 ; 
       145 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       146 SCHEM 135 0 0 SRT 1.471141 0.9625919 0.9625919 -0.02179375 -0.1563777 -3.104043 -5.474182 -6.385276 14.13102 MPRFLG 0 ; 
       147 SCHEM 133.75 -2 0 MPRFLG 0 ; 
       148 SCHEM 160 -4 0 MPRFLG 0 ; 
       152 SCHEM 155 -4 0 MPRFLG 0 ; 
       149 SCHEM 161.25 -2 0 MPRFLG 0 ; 
       150 SCHEM 165 -4 0 MPRFLG 0 ; 
       151 SCHEM 156.25 -2 0 MPRFLG 0 ; 
       153 SCHEM 161.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       155 SCHEM 166.25 -2 0 MPRFLG 0 ; 
       156 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       157 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       158 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       159 SCHEM 5 -2 0 MPRFLG 0 ; 
       160 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       154 SCHEM 157.5 -4 0 MPRFLG 0 ; 
       127 SCHEM 175 -2 0 MPRFLG 0 ; 
       133 SCHEM 190 -2 0 MPRFLG 0 ; 
       128 SCHEM 177.5 -2 0 MPRFLG 0 ; 
       125 SCHEM 170 -2 0 MPRFLG 0 ; 
       126 SCHEM 172.5 -2 0 MPRFLG 0 ; 
       124 SCHEM 182.5 0 0 SRT 1 1 1 0 0 0 0 -0.5 0 MPRFLG 0 ; 
       129 SCHEM 180 -2 0 MPRFLG 0 ; 
       135 SCHEM 195 -2 0 MPRFLG 0 ; 
       130 SCHEM 182.5 -2 0 MPRFLG 0 ; 
       131 SCHEM 185 -2 0 MPRFLG 0 ; 
       132 SCHEM 187.5 -2 0 MPRFLG 0 ; 
       134 SCHEM 192.5 -2 0 MPRFLG 0 ; 
       66 SCHEM 291 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       120 SCHEM 314.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       65 SCHEM 316 -2 0 MPRFLG 0 ; 
       67 SCHEM 296 -2 0 MPRFLG 0 ; 
       68 SCHEM 298.5 -2 0 MPRFLG 0 ; 
       69 SCHEM 301 -2 0 MPRFLG 0 ; 
       70 SCHEM 303.5 -2 0 MPRFLG 0 ; 
       71 SCHEM 306 -2 0 MPRFLG 0 ; 
       73 SCHEM 308.5 -2 0 MPRFLG 0 ; 
       74 SCHEM 311 -2 0 MPRFLG 0 ; 
       75 SCHEM 313.5 -2 0 MPRFLG 0 ; 
       76 SCHEM 293.5 -2 0 MPRFLG 0 ; 
       77 SCHEM 318.5 -2 0 MPRFLG 0 ; 
       78 SCHEM 321 -2 0 MPRFLG 0 ; 
       79 SCHEM 323.5 -2 0 MPRFLG 0 ; 
       80 SCHEM 326 -2 0 MPRFLG 0 ; 
       81 SCHEM 328.5 -2 0 MPRFLG 0 ; 
       82 SCHEM 331 -2 0 MPRFLG 0 ; 
       84 SCHEM 333.5 -2 0 MPRFLG 0 ; 
       85 SCHEM 336 -2 0 MPRFLG 0 ; 
       2 SCHEM 338.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       121 SCHEM 363.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       64 SCHEM 363.5 -2 0 MPRFLG 0 ; 
       86 SCHEM 343.5 -2 0 MPRFLG 0 ; 
       87 SCHEM 346 -2 0 MPRFLG 0 ; 
       88 SCHEM 348.5 -2 0 MPRFLG 0 ; 
       89 SCHEM 351 -2 0 MPRFLG 0 ; 
       90 SCHEM 353.5 -2 0 MPRFLG 0 ; 
       91 SCHEM 356 -2 0 MPRFLG 0 ; 
       92 SCHEM 358.5 -2 0 MPRFLG 0 ; 
       93 SCHEM 361 -2 0 MPRFLG 0 ; 
       95 SCHEM 341 -2 0 MPRFLG 0 ; 
       96 SCHEM 366 -2 0 MPRFLG 0 ; 
       97 SCHEM 368.5 -2 0 MPRFLG 0 ; 
       98 SCHEM 371 -2 0 MPRFLG 0 ; 
       99 SCHEM 373.5 -2 0 MPRFLG 0 ; 
       100 SCHEM 376 -2 0 MPRFLG 0 ; 
       101 SCHEM 378.5 -2 0 MPRFLG 0 ; 
       102 SCHEM 381 -2 0 MPRFLG 0 ; 
       103 SCHEM 383.5 -2 0 MPRFLG 0 ; 
       112 SCHEM 388.5 -2 0 MPRFLG 0 ; 
       111 SCHEM 388.5 0 0 SRT 1.082 1.082 1.082 0 -1.34 0 51.55545 0 -14.8918 MPRFLG 0 ; 
       119 SCHEM 244.75 0 0 SRT 1 0.2900001 1 0 0 0 0 -12.54536 0 MPRFLG 0 ; 
       3 SCHEM 241 -2 0 MPRFLG 0 ; 
       4 SCHEM 201 -2 0 MPRFLG 0 ; 
       5 SCHEM 203.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 206 -2 0 MPRFLG 0 ; 
       7 SCHEM 208.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 211 -2 0 MPRFLG 0 ; 
       9 SCHEM 213.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 216 -2 0 MPRFLG 0 ; 
       11 SCHEM 218.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 221 -2 0 MPRFLG 0 ; 
       13 SCHEM 223.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 226 -2 0 MPRFLG 0 ; 
       15 SCHEM 228.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 231 -2 0 MPRFLG 0 ; 
       17 SCHEM 233.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 236 -2 0 MPRFLG 0 ; 
       19 SCHEM 238.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 243.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 246 -2 0 MPRFLG 0 ; 
       22 SCHEM 248.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 251 -2 0 MPRFLG 0 ; 
       24 SCHEM 253.5 -2 0 MPRFLG 0 ; 
       25 SCHEM 256 -2 0 MPRFLG 0 ; 
       26 SCHEM 258.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 261 -2 0 MPRFLG 0 ; 
       28 SCHEM 263.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 266 -2 0 MPRFLG 0 ; 
       30 SCHEM 268.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 271 -2 0 MPRFLG 0 ; 
       32 SCHEM 273.5 -2 0 MPRFLG 0 ; 
       33 SCHEM 276 -2 0 MPRFLG 0 ; 
       34 SCHEM 278.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 281 -2 0 MPRFLG 0 ; 
       36 SCHEM 283.5 -2 0 MPRFLG 0 ; 
       37 SCHEM 286 -2 0 MPRFLG 0 ; 
       38 SCHEM 288.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       20 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 122.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 152.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 197.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 386 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 110 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 117.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 132.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
