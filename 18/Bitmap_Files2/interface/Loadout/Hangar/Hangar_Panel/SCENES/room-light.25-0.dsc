SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       light-cam_int1.25-0 ROOT ; 
       light-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       bounce-bounce_off_beam.12-0 ROOT ; 
       fill-inside_hangar_fill1.8-0 ROOT ; 
       light-from_space_to_hangar2.1-0 ; 
       light-from_space_to_hangar2_int.8-0 ROOT ; 
       light-top_of_hole1.12-0 ROOT ; 
       light-top_of_hole1_1.12-0 ROOT ; 
       light-top_of_hole1_2.12-0 ROOT ; 
       Point-Alarm1.15-0 ROOT ; 
       spot-from_hangar1.1-0 ; 
       spot-from_hangar1_int.12-0 ROOT ; 
       Spot-Under_Hole.1-0 ; 
       Spot-Under_Hole_int.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 4     
       light-alarm1.1-0 ; 
       light-from_hangar1.4-0 ; 
       light-From_Hole_Bottom1.1-0 ; 
       light-from_space1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       light-Ceiling1.2-0 ; 
       light-Constant_Black.2-0 ; 
       light-Floor1.2-0 ; 
       light-mat13.1-0 ; 
       light-mat14.1-0 ; 
       light-mat15.1-0 ; 
       light-mat16.1-0 ; 
       light-mat17.1-0 ; 
       light-mat18.1-0 ; 
       light-mat19.1-0 ; 
       light-mat20.1-0 ; 
       light-mat21.1-0 ; 
       light-mat22.1-0 ; 
       light-mat23.1-0 ; 
       light-mat3.2-0 ; 
       light-mat4.2-0 ; 
       light-mat5.1-0 ; 
       light-mat6.1-0 ; 
       light-mat7.1-0 ; 
       light-mat8.1-0 ; 
       light-Wall1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       celing_lights-torus10.1-0 ; 
       celing_lights-tubes.1-0 ROOT ; 
       light-bionic_volume.5-0 ROOT ; 
       light-ceiling_light_1.1-0 ROOT ; 
       light-cube1.1-0 ROOT ; 
       light-cube10.1-0 ; 
       light-cube11.1-0 ; 
       light-cube12.1-0 ; 
       light-cube17.1-0 ; 
       light-cube18.1-0 ; 
       light-cube19.1-0 ; 
       light-cube2.1-0 ; 
       light-cube20.1-0 ; 
       light-cube21.1-0 ; 
       light-cube22.1-0 ; 
       light-cube23.1-0 ; 
       light-cube24.1-0 ; 
       light-cube25.1-0 ; 
       light-cube26.1-0 ; 
       light-cube27.1-0 ; 
       light-cube28.1-0 ; 
       light-cube29.1-0 ; 
       light-cube3.1-0 ; 
       light-cube30.1-0 ; 
       light-cube31.1-0 ; 
       light-cube32.1-0 ROOT ; 
       light-cube33.1-0 ; 
       light-cube4.1-0 ; 
       light-cube5.1-0 ; 
       light-cube6.1-0 ; 
       light-cube7.1-0 ; 
       light-cube8.1-0 ; 
       light-cube9.1-0 ; 
       light-cyl20.1-0 ; 
       light-cyl21.1-0 ; 
       light-cyl22.2-0 ROOT ; 
       light-cyl3.1-0 ; 
       light-grid1.1-0 ; 
       light-grid2.1-0 ; 
       light-grid3.1-0 ; 
       light-grid4.1-0 ROOT ; 
       light-hangar_force_field.1-0 ROOT ; 
       light-null1.1-0 ; 
       light-null2.2-0 ROOT ; 
       light-null8.1-0 ROOT ; 
       Monitor-base.4-0 ROOT ; 
       Monitor-Screen.1-0 ; 
       Monitor1-base.2-0 ROOT ; 
       Monitor1-Screen.1-0 ; 
       Monitor6-base.4-0 ROOT ; 
       Monitor6-Screen.1-0 ; 
       Monitor7-base.3-0 ROOT ; 
       Monitor7-Screen.1-0 ; 
       Monitor8-base.4-0 ROOT ; 
       Monitor8-Screen.1-0 ; 
       Monitor9-base.4-0 ROOT ; 
       Monitor9-Screen.1-0 ; 
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-below_floor.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Door.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-Room_Root.12-0 ROOT ; 
       Room_Base-Wall.1-0 ; 
       screens-Cammand_View.2-0 ; 
       screens-Game_State.2-0 ; 
       screens-Investment.2-0 ; 
       screens-Loadout.4-0 ; 
       screens-screens.3-0 ROOT ; 
       screens-Sector_Overview.4-0 ; 
       screens-Team_Panel.2-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-extru1.1-0 ; 
       standins-face1.1-0 ; 
       standins-nurbs1.1-0 ; 
       standins-sphere1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       light-hangar_field1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/ceiling_light_Source ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/gamestate_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/sector_screen ; 
       F:/Pete_Data3/Panels/Hangar_Panel/PICTURES/teampanel_screen ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-light.25-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       light-t2d1.1-0 ; 
       light-t2d2.1-0 ; 
       light-t2d3.1-0 ; 
       light-t2d4.1-0 ; 
       light-t2d5.1-0 ; 
       light-t2d6.1-0 ; 
       light-t2d7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       light-Bionic_Volume1.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       71 78 110 ; 
       76 78 110 ; 
       74 72 110 ; 
       73 74 110 ; 
       75 72 110 ; 
       42 44 110 ; 
       11 42 110 ; 
       22 42 110 ; 
       27 42 110 ; 
       28 42 110 ; 
       29 42 110 ; 
       30 42 110 ; 
       31 42 110 ; 
       32 42 110 ; 
       5 42 110 ; 
       69 68 110 ; 
       65 68 110 ; 
       66 68 110 ; 
       64 68 110 ; 
       70 68 110 ; 
       72 78 110 ; 
       67 68 110 ; 
       77 78 110 ; 
       57 59 110 ; 
       58 61 110 ; 
       59 62 110 ; 
       61 62 110 ; 
       63 62 110 ; 
       6 42 110 ; 
       7 42 110 ; 
       37 43 110 ; 
       26 43 110 ; 
       38 43 110 ; 
       8 42 110 ; 
       9 42 110 ; 
       10 42 110 ; 
       12 42 110 ; 
       13 42 110 ; 
       14 42 110 ; 
       15 42 110 ; 
       16 42 110 ; 
       17 42 110 ; 
       18 42 110 ; 
       19 42 110 ; 
       20 42 110 ; 
       21 42 110 ; 
       23 42 110 ; 
       24 42 110 ; 
       60 63 110 ; 
       39 43 110 ; 
       36 40 110 ; 
       46 45 110 ; 
       48 47 110 ; 
       50 49 110 ; 
       52 51 110 ; 
       54 53 110 ; 
       56 55 110 ; 
       33 40 110 ; 
       34 36 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       68 14 300 ; 
       2 15 300 ; 
       57 1 300 ; 
       58 1 300 ; 
       59 0 300 ; 
       61 2 300 ; 
       63 20 300 ; 
       45 16 300 ; 
       46 17 300 ; 
       47 18 300 ; 
       48 19 300 ; 
       3 11 300 ; 
       41 13 300 ; 
       49 3 300 ; 
       50 4 300 ; 
       51 5 300 ; 
       52 6 300 ; 
       53 7 300 ; 
       54 8 300 ; 
       55 9 300 ; 
       56 10 300 ; 
       1 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       46 0 400 ; 
       48 1 400 ; 
       3 6 400 ; 
       50 2 400 ; 
       52 3 400 ; 
       54 4 400 ; 
       56 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       10 11 2110 ; 
       8 9 2110 ; 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       15 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       7 0 550 ; 
       11 2 550 ; 
       9 1 550 ; 
       3 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 41 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 8 551 1 ; 
       0 2 551 1 ; 
       0 10 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       7 SCHEM 196.1834 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 198.6834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 193.6834 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 193.6834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 201.1834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 203.6834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 206.1834 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 206.1834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 208.6834 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 211.348 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 211.348 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 213.848 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       71 SCHEM 26.18336 -2 0 MPRFLG 0 ; 
       76 SCHEM 28.68336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 23.68336 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       68 SCHEM 38.68336 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       74 SCHEM 21.18336 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 21.18336 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 208.848 -1.475872 0 USR SRT 11.25 11.25 11.25 0 0 0 0 0 0 MPRFLG 0 ; 
       75 SCHEM 18.68336 -4 0 MPRFLG 0 ; 
       4 SCHEM 48.68336 0 0 SRT 1 1 1 0 -0.8726646 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 99.93336 -2 0 MPRFLG 0 ; 
       11 SCHEM 96.18336 -4 0 MPRFLG 0 ; 
       22 SCHEM 71.18336 -4 0 MPRFLG 0 ; 
       27 SCHEM 73.68336 -4 0 MPRFLG 0 ; 
       28 SCHEM 76.18336 -4 0 MPRFLG 0 ; 
       29 SCHEM 78.68336 -4 0 MPRFLG 0 ; 
       30 SCHEM 81.18336 -4 0 MPRFLG 0 ; 
       31 SCHEM 83.68336 -4 0 MPRFLG 0 ; 
       32 SCHEM 86.18336 -4 0 MPRFLG 0 ; 
       5 SCHEM 88.68336 -4 0 MPRFLG 0 ; 
       69 SCHEM 33.68336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 36.18336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 38.68336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 41.18336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 43.68336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 19.93336 -2 0 MPRFLG 0 ; 
       67 SCHEM 31.18336 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 23.68336 -2 0 MPRFLG 0 ; 
       57 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       58 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       59 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       61 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       62 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       63 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 91.18336 -4 0 MPRFLG 0 ; 
       7 SCHEM 93.68336 -4 0 MPRFLG 0 ; 
       37 SCHEM 58.68336 -2 0 MPRFLG 0 ; 
       26 SCHEM 61.18336 -2 0 MPRFLG 0 ; 
       38 SCHEM 63.68336 -2 0 MPRFLG 0 ; 
       43 SCHEM 62.43336 0 0 SRT 1.166 1.66738 1.166 0 0 0 181.1734 -0.0535363 -128.4584 MPRFLG 0 ; 
       8 SCHEM 68.68336 -4 0 MPRFLG 0 ; 
       9 SCHEM 98.68336 -4 0 MPRFLG 0 ; 
       10 SCHEM 101.1834 -4 0 MPRFLG 0 ; 
       12 SCHEM 103.6834 -4 0 MPRFLG 0 ; 
       13 SCHEM 106.1834 -4 0 MPRFLG 0 ; 
       14 SCHEM 108.6834 -4 0 MPRFLG 0 ; 
       15 SCHEM 111.1834 -4 0 MPRFLG 0 ; 
       16 SCHEM 113.6834 -4 0 MPRFLG 0 ; 
       17 SCHEM 116.1834 -4 0 MPRFLG 0 ; 
       18 SCHEM 118.6834 -4 0 MPRFLG 0 ; 
       19 SCHEM 121.1834 -4 0 MPRFLG 0 ; 
       20 SCHEM 123.6834 -4 0 MPRFLG 0 ; 
       21 SCHEM 126.1834 -4 0 MPRFLG 0 ; 
       23 SCHEM 128.6834 -4 0 MPRFLG 0 ; 
       24 SCHEM 131.1834 -4 0 MPRFLG 0 ; 
       60 SCHEM 13.68336 -3.835222 0 USR MPRFLG 0 ; 
       25 SCHEM 51.18336 0 0 SRT 1 1 0.410176 0 -1.332488 0 28.16133 0 -7.441764 MPRFLG 0 ; 
       39 SCHEM 66.18336 -2 0 MPRFLG 0 ; 
       40 SCHEM 54.93336 0 0 SRT 1.42 1 2.037903 0 0.2792527 0 67.10641 -18.64181 -18.7254 MPRFLG 0 ; 
       45 SCHEM 136.1834 0 0 SRT 1.471141 0.9625919 0.9625919 -0.5821127 -1.486002 0 26.72663 -6.86913 35.46216 MPRFLG 0 ; 
       36 SCHEM 53.68336 -2 0 MPRFLG 0 ; 
       46 SCHEM 134.9334 -2 0 MPRFLG 0 ; 
       47 SCHEM 143.6834 0 0 SRT 1.471141 0.9625919 0.9625919 0.5133368 -1.344847 -0.01415699 26.79786 6.308922 35.46216 MPRFLG 0 ; 
       48 SCHEM 142.4334 -2 0 MPRFLG 0 ; 
       3 SCHEM 184.9334 0 0 SRT 0.362 0.2476079 1.178 0 0.23 0 45.7519 17.62612 -12.52787 MPRFLG 0 ; 
       41 SCHEM 216.348 0 0 SRT 1 1 1 0 0 0 147.0213 -22.28944 -177.9266 MPRFLG 0 ; 
       49 SCHEM 151.1834 0 0 SRT 1.471141 0.9625919 0.9625919 0.4977454 0.5151278 -0.00364445 8.855516 6.308922 9.169995 MPRFLG 0 ; 
       50 SCHEM 149.9334 -2 0 MPRFLG 0 ; 
       51 SCHEM 158.6834 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3516835 0.2844428 0.001876728 10.20939 -8.125242 8.293537 MPRFLG 0 ; 
       52 SCHEM 157.4334 -2 0 MPRFLG 0 ; 
       53 SCHEM 166.1834 0 0 SRT 1.471141 0.9625919 0.9625919 -0.3081128 -0.06048232 -3.158986 -5.119568 5.063054 13.15584 MPRFLG 0 ; 
       54 SCHEM 164.9334 -2.000001 0 USR MPRFLG 0 ; 
       55 SCHEM 173.6834 0 0 SRT 1.471141 0.9625919 0.9625919 -0.02179375 -0.1563777 -3.104043 -5.474182 -6.385276 14.13102 MPRFLG 0 ; 
       56 SCHEM 172.4334 -2.000001 0 USR MPRFLG 0 ; 
       35 SCHEM 178.6834 0 0 SRT 6.050002 11.52707 6.050002 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 189.9334 0 0 SRT 1 1 1 0 -0.4 0 0 17.30421 0 MPRFLG 0 ; 
       33 SCHEM 56.18336 -2 0 MPRFLG 0 ; 
       44 SCHEM 99.93336 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 53.68336 -4 0 MPRFLG 0 ; 
       0 SCHEM 188.6834 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.18336 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.18336 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 208.848 -3.475872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 138.6834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 136.1834 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 146.1834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 143.6834 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 183.6834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 191.1834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 218.848 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 153.6834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 151.1834 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 161.1834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 158.6834 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 168.6834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 166.1834 -4.000001 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 176.1834 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 173.6834 -4.000001 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 133.6834 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 141.1834 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 148.6834 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 156.1834 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 163.6834 -4.000001 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 171.1834 -4.000001 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 186.1834 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       2 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
