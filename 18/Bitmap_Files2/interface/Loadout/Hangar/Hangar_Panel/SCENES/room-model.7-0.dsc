SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.7-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       model-Ceiling1.1-0 ; 
       model-Constant_Black.1-0 ; 
       model-Floor1.1-0 ; 
       model-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-below_floor.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-Room_Root.4-0 ROOT ; 
       Room_Base-Wall.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-model.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 3 110 ; 
       3 4 110 ; 
       2 4 110 ; 
       5 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 1 300 ; 
       3 2 300 ; 
       2 0 300 ; 
       5 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
