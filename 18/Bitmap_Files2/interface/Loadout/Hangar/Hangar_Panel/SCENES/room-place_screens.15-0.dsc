SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       place_screens-cam_int1Cam.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       place_screens-cam_int1.15-0 ROOT ; 
       place_screens-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       place_screens-Ceiling1.1-0 ; 
       place_screens-Constant_Black.1-0 ; 
       place_screens-Floor1.1-0 ; 
       place_screens-mat3.2-0 ; 
       place_screens-Wall1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       Room_Base-Above_Ceiling.1-0 ; 
       Room_Base-below_floor.1-0 ; 
       Room_Base-Ceiling.1-0 ; 
       Room_Base-Floor.1-0 ; 
       Room_Base-Room_Root.9-0 ROOT ; 
       Room_Base-Wall.1-0 ; 
       screens-Cammand_View.2-0 ; 
       screens-Game_State.2-0 ; 
       screens-Investment.2-0 ; 
       screens-Loadout.4-0 ; 
       screens-screens.2-0 ROOT ; 
       screens-Sector_Overview.4-0 ; 
       screens-Team_Panel.2-0 ; 
       standins-cyl1.2-0 ; 
       standins-Door_Outline.4-0 ; 
       standins-extru1.1-0 ; 
       standins-face1.1-0 ; 
       standins-sphere1.1-0 ; 
       standins-sphere1_1.2-0 ; 
       standins-standins.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       room-place_screens.15-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 19 110 ; 
       17 19 110 ; 
       16 14 110 ; 
       15 16 110 ; 
       11 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       6 10 110 ; 
       12 10 110 ; 
       14 19 110 ; 
       9 10 110 ; 
       18 19 110 ; 
       0 2 110 ; 
       1 3 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 3 300 ; 
       0 1 300 ; 
       1 1 300 ; 
       2 0 300 ; 
       3 2 300 ; 
       5 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER ANIMATION 
       0 0 15007 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 20 -2 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 15.55776 -3.611821 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 15.55776 -5.611821 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 1.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
