SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       destroy-default5_1_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat14_1.1-0 ; 
       destroy-mat15_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat36_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       destroy-mat40_1.1-0 ; 
       destroy-mat41_1.1-0 ; 
       destroy-mat42_1.1-0 ; 
       destroy-mat43_1.1-0 ; 
       static-mat46.1-0 ; 
       static-mat47.1-0 ; 
       static-mat64.1-0 ; 
       static-mat65.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       static-afuselg.1-0 ; 
       static-ffuselg.1-0 ; 
       static-fig02.1-0 ROOT ; 
       static-finzzz0.1-0 ; 
       static-finzzz1.1-0 ; 
       static-lbooster.1-0 ; 
       static-lfinzzz.1-0 ; 
       static-lwingzz0.1-0 ; 
       static-lwingzz1.1-0 ; 
       static-lwingzz2.1-0 ; 
       static-rbooster.1-0 ; 
       static-rfinzzz.1-0 ; 
       static-rwingzz0.1-0 ; 
       static-rwingzz1.1-0 ; 
       static-rwingzz2.1-0 ; 
       static-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Pete_Data3/Panels/Screens/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.1-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.1-0 ; 
       destroy-t2d3_1.1-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.1-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.1-0 ; 
       destroy-t2d6_1.1-0 ; 
       destroy-t2d7_1.1-0 ; 
       destroy-t2d8_1.1-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       static-t2d42.1-0 ; 
       static-t2d43.1-0 ; 
       static-t2d44.1-0 ; 
       static-t2d45.1-0 ; 
       static-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 15 110 ; 
       6 3 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 15 110 ; 
       11 3 110 ; 
       12 2 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       4 18 300 ; 
       5 1 300 ; 
       5 20 300 ; 
       5 25 300 ; 
       6 3 300 ; 
       6 27 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 24 300 ; 
       10 2 300 ; 
       10 19 300 ; 
       10 26 300 ; 
       11 4 300 ; 
       11 28 300 ; 
       14 0 300 ; 
       14 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       5 10 400 ; 
       6 5 400 ; 
       10 11 400 ; 
       11 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 16 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 0 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       22 14 401 ; 
       23 17 401 ; 
       24 25 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 27 401 ; 
       28 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 SRT 0.1528094 0.1528094 0.1528094 6.264869e-005 -1.569883 0.002353534 0 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 0 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 1.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -1 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
