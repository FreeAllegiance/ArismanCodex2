SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       create-null2.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       setup-cam_int1.25-0 ROOT ; 
       setup-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       static-light1.7-0 ROOT ; 
       static-light2.7-0 ROOT ; 
       static-light3.7-0 ROOT ; 
       static-light4.7-0 ROOT ; 
       static-light5.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       create-mat1.1-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       static-mat46.2-0 ; 
       static-mat47.2-0 ; 
       static-mat64.2-0 ; 
       static-mat65.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       create-afuselg.1-0 ; 
       create-ffuselg.1-0 ; 
       create-fig02.1-0 ; 
       create-finzzz0.1-0 ; 
       create-finzzz1.1-0 ; 
       create-grid1.1-0 ROOT ; 
       create-inst.1-0 ; 
       create-inst_1.1-0 ; 
       create-inst_2.1-0 ; 
       create-lbooster.1-0 ; 
       create-lfinzzz.1-0 ; 
       create-lwingzz0.1-0 ; 
       create-lwingzz1.1-0 ; 
       create-lwingzz2.1-0 ; 
       create-null1.3-0 ; 
       create-null2.3-0 ROOT ; 
       create-rbooster.1-0 ; 
       create-rfinzzz.1-0 ; 
       create-rwingzz0.1-0 ; 
       create-rwingzz1.1-0 ; 
       create-rwingzz2.1-0 ; 
       create-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Panels/Screens/PICTURES/fig02 ; 
       F:/Pete_Data3/Panels/Screens/PICTURES/invest.1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       invest_Screen-create.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       create-t2d1.1-0 ; 
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.2-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.2-0 ; 
       destroy-t2d3_1.2-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.2-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.2-0 ; 
       destroy-t2d6_1.2-0 ; 
       destroy-t2d7_1.2-0 ; 
       destroy-t2d8_1.2-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       static-t2d42.1-0 ; 
       static-t2d43.1-0 ; 
       static-t2d44.1-0 ; 
       static-t2d45.1-0 ; 
       static-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 15 110 ; 
       6 14 20002 ; 
       0 1 110 ; 
       1 2 110 ; 
       2 14 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       9 21 110 ; 
       10 3 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 15 110 ; 
       16 21 110 ; 
       17 3 110 ; 
       18 2 110 ; 
       19 18 110 ; 
       20 19 110 ; 
       21 2 110 ; 
       7 15 110 ; 
       7 14 20002 ; 
       8 15 110 ; 
       8 14 20002 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       0 6 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       4 19 300 ; 
       9 2 300 ; 
       9 21 300 ; 
       9 26 300 ; 
       10 4 300 ; 
       10 28 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       13 25 300 ; 
       16 3 300 ; 
       16 20 300 ; 
       16 27 300 ; 
       17 5 300 ; 
       17 29 300 ; 
       20 1 300 ; 
       20 24 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 0 400 ; 
       0 23 400 ; 
       1 2 400 ; 
       4 8 400 ; 
       9 11 400 ; 
       10 6 400 ; 
       16 12 400 ; 
       17 7 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       15 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 16 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 17 401 ; 
       11 19 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 22 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 1 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       23 15 401 ; 
       24 18 401 ; 
       25 26 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 28 401 ; 
       29 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 17.5 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 17.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 8.75 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 5 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 0 -12 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 15 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 15 -14 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 8.75 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       16 SCHEM 2.5 -12 0 WIRECOL 8 7 DISPLAY 1 2 MPRFLG 0 ; 
       17 SCHEM 10 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       18 SCHEM 12.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 12.5 -12 0 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 12.5 -14 0 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 1.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 12.5 -4 0 DISPLAY 1 2 SRT 1 1 1 -1.570796 -9.315215e-008 6.283185 -9.889747 23.16489 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -1 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM -1 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -1 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -1 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 16 16 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
