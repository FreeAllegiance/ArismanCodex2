SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ready-null1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       setup-cam_int1.8-0 ROOT ; 
       setup-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       ready-light1.4-0 ROOT ; 
       ready-light2.4-0 ROOT ; 
       ready-light3.4-0 ROOT ; 
       ready-light4.4-0 ROOT ; 
       ready-light5.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       destroy-default5_1_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       ready-mat46.1-0 ; 
       ready-mat47.1-0 ; 
       ready-mat64.1-0 ; 
       ready-mat65.1-0 ; 
       setup-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ready-afuselg.1-0 ; 
       ready-ffuselg.1-0 ; 
       ready-fig02.1-0 ; 
       ready-finzzz0.1-0 ; 
       ready-finzzz1.1-0 ; 
       ready-lbooster.1-0 ; 
       ready-lfinzzz.1-0 ; 
       ready-lwingzz0.1-0 ; 
       ready-lwingzz1.1-0 ; 
       ready-lwingzz2.1-0 ; 
       ready-null1.3-0 ROOT ; 
       ready-rbooster.1-0 ; 
       ready-rfinzzz.1-0 ; 
       ready-rwingzz0.1-0 ; 
       ready-rwingzz1.1-0 ; 
       ready-rwingzz2.1-0 ; 
       ready-thrust0.1-0 ; 
       setup-grid1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Panels/Screens/PICTURES/fig02 ; 
       F:/Pete_Data3/Panels/Screens/PICTURES/loadout ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ready-ready.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.2-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.2-0 ; 
       destroy-t2d3_1.2-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.2-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.2-0 ; 
       destroy-t2d6_1.2-0 ; 
       destroy-t2d7_1.2-0 ; 
       destroy-t2d8_1.2-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       ready-t2d42.1-0 ; 
       ready-t2d43.1-0 ; 
       ready-t2d44.1-0 ; 
       ready-t2d45.1-0 ; 
       ready-zt2d1.1-0 ; 
       setup-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 10 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 16 110 ; 
       6 3 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       11 16 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       17 29 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       4 18 300 ; 
       5 1 300 ; 
       5 20 300 ; 
       5 25 300 ; 
       6 3 300 ; 
       6 27 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 24 300 ; 
       11 2 300 ; 
       11 19 300 ; 
       11 26 300 ; 
       12 4 300 ; 
       12 28 300 ; 
       15 0 300 ; 
       15 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       17 28 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       5 10 400 ; 
       6 5 400 ; 
       11 11 400 ; 
       12 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 16 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 0 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       22 14 401 ; 
       23 17 401 ; 
       24 25 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 27 401 ; 
       28 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 35 -6 0 MPRFLG 0 ; 
       7 SCHEM 55 -4 0 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 MPRFLG 0 ; 
       9 SCHEM 55 -8 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 52.5 0 0 DISPLAY 1 2 SRT 0.9999999 1 1 -1.570796 0 0 -1.305764 23.18021 -5.244612 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 97.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 16 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
