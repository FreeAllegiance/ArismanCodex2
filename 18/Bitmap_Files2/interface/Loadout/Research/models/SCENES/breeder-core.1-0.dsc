SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       core-cam_int1.1-0 ROOT ; 
       core-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       core-spot1.1-0 ; 
       core-spot1_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       core-mat1.1-0 ; 
       core-mat11.1-0 ; 
       core-mat12.1-0 ; 
       core-mat13.1-0 ; 
       core-mat14.1-0 ; 
       core-mat15.1-0 ; 
       core-mat16.1-0 ; 
       core-mat17.1-0 ; 
       core-mat18.1-0 ; 
       core-mat6.1-0 ; 
       core-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       core-cyl1.1-0 ROOT ; 
       core-null2.1-0 ROOT ; 
       core-nurbs2.1-0 ; 
       core-nurbs5.1-0 ROOT ; 
       core-rrr.1-0 ; 
       core-torus10.1-0 ; 
       core-torus11.1-0 ; 
       core-torus13.1-0 ; 
       core-torus14.1-0 ; 
       core-torus16.1-0 ; 
       core-torus17.1-0 ; 
       core-torus23.1-0 ; 
       core-torus24.1-0 ; 
       core-torus25.1-0 ; 
       core-torus26.1-0 ; 
       core-torus27.1-0 ; 
       core-torus7.1-0 ; 
       core-torus8.1-0 ; 
       core-torus9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       core-DGlow1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       breeder-core.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       core-March_Fractal1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 1 110 ; 
       4 3 110 ; 
       2 3 110 ; 
       11 1 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       12 1 110 ; 
       9 3 110 ; 
       13 1 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 3 110 ; 
       14 1 110 ; 
       15 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       10 3 300 ; 
       4 1 300 ; 
       2 9 300 ; 
       3 9 300 ; 
       11 4 300 ; 
       5 0 300 ; 
       6 0 300 ; 
       7 10 300 ; 
       8 10 300 ; 
       12 5 300 ; 
       9 10 300 ; 
       13 6 300 ; 
       16 0 300 ; 
       17 0 300 ; 
       18 0 300 ; 
       14 7 300 ; 
       15 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       2 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 4 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 0 0 SRT 1.408 1.702238 1.408 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 41.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -1.228092 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 35 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 15 -2 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
