SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       we-cam_int1.2-0 ROOT ; 
       we-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       we-1.1-0 ; 
       we-mat1.1-0 ; 
       we-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       we-cyl3.1-0 ROOT ; 
       we-cyl4.1-0 ; 
       we-cyl5.1-0 ; 
       we-cyl6.1-0 ; 
       we-cyl8.1-0 ; 
       we-cyl9.1-0 ; 
       we-null1.1-0 ROOT ; 
       we-null2.1-0 ; 
       we-sphere6.2-0 ROOT ; 
       we-sphere7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       we-bg_color1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       chamber-we.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 8 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       7 6 110 ; 
       4 7 110 ; 
       5 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 1 300 ; 
       0 0 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       -1 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 SRT 0.9099997 2.6 2.6 0 0 0 6.268312 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 20 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
