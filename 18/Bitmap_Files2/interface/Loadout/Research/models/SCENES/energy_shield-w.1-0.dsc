SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       w-cam_int1.1-0 ROOT ; 
       w-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       w-light1.1-0 ROOT ; 
       w-light2.1-0 ROOT ; 
       w-light3.1-0 ROOT ; 
       w-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       w-mat1.1-0 ; 
       w-METALS-chrome002.1-1.1-0 ; 
       w-METALS-chrome01.1-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       w-cyl10.1-0 ROOT ; 
       w-null1.1-0 ROOT ; 
       w-sphere8.1-0 ROOT ; 
       w-sphere9.1-0 ROOT ; 
       w-torus2.1-0 ; 
       w-torus3.1-0 ; 
       w-torus4.1-0 ; 
       w-torus5.1-0 ; 
       w-torus6.1-0 ; 
       w-torus7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy_shield-w.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 2     
       w-lightning1.1-0 ; 
       w-MF_blue_neon1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       0 2 300 ; 
       2 1 300 ; 
       3 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       2 0 550 ; 
       0 1 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER MODELS 
       0 2 551 1 ; 
       0 3 551 2 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       1 2 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 3.036806 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 21.25 0 0 DISPLAY 1 2 SRT 1 1.326 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 SRT 0.254 0.254 0.254 0 0 0 0 3.981296 0 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 SRT 0.254 0.254 0.254 0 0 0 0 -3.360002 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
