SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       w-cam_int1.2-0 ROOT ; 
       w-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       w-light1.2-0 ROOT ; 
       w-light2.2-0 ROOT ; 
       w-light3.2-0 ROOT ; 
       w-light4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       w-mat1.2-0 ; 
       w-mat2.1-0 ; 
       w-METALS-chrome002.1-1.1-0 ; 
       w-METALS-chrome01.1-1.1-0 ; 
       w-METALS-SILVER.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       w-bmerge1.1-0 ROOT ; 
       w-cube1.1-0 ; 
       w-cyl10.1-0 ROOT ; 
       w-cyl11.1-0 ROOT ; 
       w-cyl12.1-0 ROOT ; 
       w-root.1-0 ROOT ; 
       w-sphere8.1-0 ROOT ; 
       w-sphere9.1-0 ROOT ; 
       w-torus2.1-0 ; 
       w-torus3.1-0 ; 
       w-torus4.1-0 ; 
       w-torus5.1-0 ; 
       w-torus6.1-0 ; 
       w-torus7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       w-DGlow1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       energy_shield-w.2-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 2     
       w-lightning1.1-0 ; 
       w-MF_blue_neon1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 1 300 ; 
       5 0 300 ; 
       2 3 300 ; 
       4 1 300 ; 
       6 2 300 ; 
       7 2 300 ; 
       0 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       3 0 550 ; 
       0 1 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       -1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER MODELS 
       0 6 551 1 ; 
       0 7 551 2 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 5 551 1 ; 
       0 8 551 1 ; 
       0 13 551 1 ; 
       0 12 551 1 ; 
       0 11 551 1 ; 
       0 10 551 1 ; 
       0 9 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       1 2 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 32.5 0 0 SRT 0.2739999 0.2739999 0.2739999 0 0 0 0 5.39098 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 3.036806 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 35 0 0 SRT 1 1.326 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 27.5 0 0 SRT 0.2739999 0.2739999 0.2739999 0 0 0 0 -4.740343 0 MPRFLG 0 ; 
       6 SCHEM 30 0 0 SRT 0.254 0.254 0.254 0 0 0 0 3.981296 0 MPRFLG 0 ; 
       7 SCHEM 37.5 0 0 SRT 0.254 0.254 0.254 0 0 0 0 -3.360002 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 2.624601 0.3253177 0.0352606 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
