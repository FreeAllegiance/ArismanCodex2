SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       no_namewe-cam_int1.10-0 ROOT ; 
       no_namewe-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       no_namewe-light1.9-0 ROOT ; 
       no_namewe-light2.9-0 ROOT ; 
       no_namewe-light3.9-0 ROOT ; 
       no_namewe-spot4.1-0 ; 
       no_namewe-spot4_int.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       no_namewe-mat1.3-0 ; 
       no_namewe-mat3.2-0 ; 
       no_namewe-mat4.2-0 ; 
       no_namewe-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       no_namewe-ball.1-0 ROOT ; 
       no_namewe-cube10.1-0 ; 
       no_namewe-cube11.1-0 ; 
       no_namewe-cube2.1-0 ; 
       no_namewe-cube3.1-0 ; 
       no_namewe-cube4.1-0 ; 
       no_namewe-cube5.1-0 ; 
       no_namewe-cube6.1-0 ; 
       no_namewe-cube7.1-0 ; 
       no_namewe-cube8.1-0 ; 
       no_namewe-cube9.1-0 ; 
       no_namewe-cyl1.1-0 ; 
       no_namewe-cyl2.1-0 ; 
       no_namewe-null1.3-0 ROOT ; 
       no_namewe-sphere3.2-0 ROOT ; 
       no_namewe-sphere4.1-0 ; 
       no_namewe-sphere5.1-0 ; 
       no_namewe-spline1.3-0 ROOT ; 
       no_namewe-tube.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 2     
       no_namewe-bg_color1.2-0 ; 
       no_namewe-DGlow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       magnetic_repulsor-no_namewe.10-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       no_namewe-March_Fractal1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 14 110 ; 
       16 14 110 ; 
       11 15 110 ; 
       12 16 110 ; 
       18 17 220 RELDATA SCLE 0.366 5.00638 0.366 ROLL 0 TRNS 0 12.88235 2.980232e-008 EndOfRELDATA ; 
       18 17 220 2 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
       18 1 300 ; 
       13 3 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       -1 1 550 ; 
       -1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       1 0 551 1 ; 
       1 18 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 3 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 45 -2 0 MPRFLG 0 ; 
       4 SCHEM 45 0 0 MPRFLG 0 ; 
       0 SCHEM 47.5 0 0 MPRFLG 0 ; 
       1 SCHEM 50 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 30 -2 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 32.5 0 0 SRT 1.392 0.686 2.6 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 15 0 0 DISPLAY 3 2 SRT 1 0.4408521 1 0 0 0 0 0 -6.546165 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 40 0 0 SRT 0.594 0.594 0.594 0 0 0 0 0 12.5265 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
