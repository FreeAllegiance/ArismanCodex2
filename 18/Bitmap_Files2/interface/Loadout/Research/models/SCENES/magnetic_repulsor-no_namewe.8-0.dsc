SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       no_namewe-cam_int1.8-0 ROOT ; 
       no_namewe-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       no_namewe-light1.7-0 ROOT ; 
       no_namewe-light2.7-0 ROOT ; 
       no_namewe-light3.7-0 ROOT ; 
       no_namewe-spot4.1-0 ; 
       no_namewe-spot4_int.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       no_namewe-mat1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       no_namewe-cube10.1-0 ; 
       no_namewe-cube11.1-0 ; 
       no_namewe-cube2.1-0 ; 
       no_namewe-cube3.1-0 ; 
       no_namewe-cube4.1-0 ; 
       no_namewe-cube5.1-0 ; 
       no_namewe-cube6.1-0 ; 
       no_namewe-cube7.1-0 ; 
       no_namewe-cube8.1-0 ; 
       no_namewe-cube9.1-0 ; 
       no_namewe-cyl1.1-0 ; 
       no_namewe-cyl2.1-0 ; 
       no_namewe-cyl3.2-0 ROOT ; 
       no_namewe-null1.1-0 ROOT ; 
       no_namewe-sphere3.2-0 ROOT ; 
       no_namewe-sphere4.1-0 ; 
       no_namewe-sphere5.1-0 ; 
       no_namewe-sphere6.1-0 ROOT ; 
       no_namewe-spline1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       no_namewe-bg_color1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       magnetic_repulsor-no_namewe.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 14 110 ; 
       16 14 110 ; 
       10 15 110 ; 
       11 16 110 ; 
       12 18 220 RELDATA SCLE 0.366 5.00638 0.366 ROLL 0 TRNS 0 12.88235 2.980232e-008 EndOfRELDATA ; 
       12 18 220 2 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       0 13 110 ; 
       1 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       -1 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 42.5 0 0 MPRFLG 0 ; 
       0 SCHEM 45 0 0 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 MPRFLG 0 ; 
       2 SCHEM 50 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 60 0 0 MPRFLG 0 ; 
       16 SCHEM 62.5 0 0 MPRFLG 0 ; 
       10 SCHEM 65 0 0 MPRFLG 0 ; 
       11 SCHEM 67.5 0 0 MPRFLG 0 ; 
       14 SCHEM 57.5 0 0 DISPLAY 1 2 SRT 1.392 0.686 2.6 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 70 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 86.25 0 0 SRT 1 0.4408521 1 0 0 0 0 0 -6.546165 MPRFLG 0 ; 
       2 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 80 -2 0 MPRFLG 0 ; 
       5 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 85 -2 0 MPRFLG 0 ; 
       7 SCHEM 75 -2 0 MPRFLG 0 ; 
       8 SCHEM 90 -2 0 MPRFLG 0 ; 
       9 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 95 -2 0 MPRFLG 0 ; 
       1 SCHEM 97.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 102.5 0 0 SRT 0.594 0.594 0.594 0 0 0 0 0 12.5265 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 105 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
