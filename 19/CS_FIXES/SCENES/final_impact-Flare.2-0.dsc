SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       Flare-null11_2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.7-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS NBELEM 1     
       Flare-Flares1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       vol-Thruster1_2.7-0 ROOT ; 
       vol-Thruster2_2.7-0 ROOT ; 
       vol-Thruster3_2.7-0 ROOT ; 
       vol-Thruster4_2.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 4     
       asteroid_plate-asteroid_thruster1.2-0 ; 
       asteroid_plate-asteroid_thruster2.2-0 ; 
       asteroid_plate-asteroid_thruster3.2-0 ; 
       asteroid_plate-asteroid_thruster4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       asteroid_plate-METAL1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       Flare-cyl39.1-0 ; 
       Flare-cyl40.1-0 ; 
       Flare-cyl41.1-0 ; 
       Flare-cyl42.1-0 ; 
       Flare-null10.1-0 ; 
       Flare-null11_2.2-0 ROOT ; 
       Flare-null4.1-0 ; 
       Flare-null5.4-0 ; 
       Flare-null6.1-0 ; 
       Flare-null7.1-0 ; 
       Flare-null8.1-0 ; 
       Flare-null9.1-0 ; 
       setup-spline1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final_impact-Flare.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 3 110 ; 
       5 12 112 ; 
       5 12 112 2 ; 
       6 7 110 ; 
       7 5 110 ; 
       8 6 110 ; 
       9 2 110 ; 
       10 0 110 ; 
       11 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 0 300 ; 
       2 0 300 ; 
       3 0 300 ; 
       6 0 300 ; 
       7 0 300 ; 
       8 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       0 9 2111 ; 
       1 10 2111 ; 
       2 11 2111 ; 
       3 4 2111 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       0 0 550 ; 
       1 1 550 ; 
       2 2 550 ; 
       3 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER CAMERA_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -10.11606 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -10.11606 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -10.11606 0 MPRFLG 0 ; 
       3 SCHEM 10 -10.11606 0 MPRFLG 0 ; 
       4 SCHEM 10 -12.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 9.065376 0 0 SRT 1 1 1 -0.2420717 0 4.565933 16.17545 7.803852 471.3979 MPRFLG 0 ; 
       6 SCHEM 7.5 -6.116063 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4.116063 0 USR MPRFLG 0 ; 
       8 SCHEM 7.5 -8.116063 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -12.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 5 -12.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -12.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 15 0 0 SRT 1 1 1 0 -2.906 0 14.3675 12.28784 341.4336 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -47.5458 -18.96095 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 14 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 209 979 774 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
