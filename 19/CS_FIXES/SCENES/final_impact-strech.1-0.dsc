SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       strech-null11_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.1-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS NBELEM 1     
       strech-Flares1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       Lighting_Template-inf_light1_2.1-0 ROOT ; 
       Lighting_Template-inf_light2_2.1-0 ROOT ; 
       vol-Thruster1_2.1-0 ROOT ; 
       vol-Thruster2_2.1-0 ROOT ; 
       vol-Thruster3_2.1-0 ROOT ; 
       vol-Thruster4_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 4     
       asteroid_plate-asteroid_thruster1.1-0 ; 
       asteroid_plate-asteroid_thruster2.1-0 ; 
       asteroid_plate-asteroid_thruster3.1-0 ; 
       asteroid_plate-asteroid_thruster4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       asteroid_plate-mat1.1-0 ; 
       asteroid_plate-METAL1.1-0 ; 
       asteroid_plate-volume1.1-0 ; 
       turr_utl-mat245.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       setup-spline1.1-0 ROOT ; 
       strech-cyl10.1-0 ; 
       strech-cyl11.1-0 ; 
       strech-cyl12.1-0 ; 
       strech-cyl13.1-0 ; 
       strech-cyl14.1-0 ; 
       strech-cyl15.1-0 ; 
       strech-cyl16.1-0 ; 
       strech-cyl17.1-0 ; 
       strech-cyl18.1-0 ; 
       strech-cyl19.1-0 ; 
       strech-cyl20.1-0 ; 
       strech-cyl21.1-0 ; 
       strech-cyl22.1-0 ; 
       strech-cyl23.1-0 ; 
       strech-cyl24.1-0 ; 
       strech-cyl25.1-0 ; 
       strech-cyl26.1-0 ; 
       strech-cyl27.1-0 ; 
       strech-cyl28.1-0 ; 
       strech-cyl29.1-0 ; 
       strech-cyl3.1-0 ; 
       strech-cyl30.1-0 ; 
       strech-cyl31.1-0 ; 
       strech-cyl32.1-0 ; 
       strech-cyl33.1-0 ; 
       strech-cyl34.1-0 ; 
       strech-cyl35.1-0 ; 
       strech-cyl36.1-0 ; 
       strech-cyl37.1-0 ; 
       strech-cyl38.1-0 ; 
       strech-cyl39.1-0 ; 
       strech-cyl4.1-0 ; 
       strech-cyl40.1-0 ; 
       strech-cyl41.1-0 ; 
       strech-cyl42.1-0 ; 
       strech-cyl5.1-0 ; 
       strech-cyl6.1-0 ; 
       strech-cyl7.1-0 ; 
       strech-cyl8.1-0 ; 
       strech-cyl9.1-0 ; 
       strech-null1.1-0 ; 
       strech-null10.1-0 ; 
       strech-null11_2.1-0 ROOT ; 
       strech-null2.1-0 ; 
       strech-null3.1-0 ; 
       strech-null4.1-0 ; 
       strech-null5.4-0 ; 
       strech-null6.1-0 ; 
       strech-null7.1-0 ; 
       strech-null8.1-0 ; 
       strech-null9.1-0 ; 
       strech-skin2.5-0 ; 
       strech-sphere2.1-0 ; 
       strech-sphere3.1-0 ; 
       strech-sphere4.1-0 ; 
       strech-volume.1-0 ; 
       turr_utl-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       D:/CS_FIXES/PICTURES/MoonBump ; 
       D:/CS_FIXES/PICTURES/MoonMap ; 
       D:/CS_FIXES/PICTURES/bottom_of_features ; 
       D:/CS_FIXES/PICTURES/inside_large_crater ; 
       D:/CS_FIXES/PICTURES/large_crater ; 
       D:/CS_FIXES/PICTURES/main_feature ; 
       D:/CS_FIXES/PICTURES/main_shell ; 
       D:/CS_FIXES/PICTURES/map1 ; 
       D:/CS_FIXES/PICTURES/tri_spec ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final_impact-strech.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       asteroid_plate-FeatureBumps1.1-0 ; 
       asteroid_plate-FeatureMap1.1-0 ; 
       asteroid_plate-Inside_large_Crater1.1-0 ; 
       asteroid_plate-Large_Crater1.1-0 ; 
       asteroid_plate-MoonBump.1-0 ; 
       asteroid_plate-MoonMap.1-0 ; 
       asteroid_plate-Shell1.1-0 ; 
       asteroid_plate-t2d1.1-0 ; 
       asteroid_plate-t2d2.1-0 ; 
       asteroid_plate-t2d3.1-0 ; 
       asteroid_plate-t2d4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid_plate-rock01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       asteroid_plate-Asteroid_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 41 110 ; 
       2 44 110 ; 
       3 44 110 ; 
       4 44 110 ; 
       5 44 110 ; 
       6 44 110 ; 
       7 44 110 ; 
       8 44 110 ; 
       9 44 110 ; 
       10 45 110 ; 
       11 45 110 ; 
       12 45 110 ; 
       13 45 110 ; 
       14 45 110 ; 
       15 45 110 ; 
       16 45 110 ; 
       17 45 110 ; 
       18 45 110 ; 
       19 45 110 ; 
       20 45 110 ; 
       21 41 110 ; 
       22 45 110 ; 
       23 41 110 ; 
       24 41 110 ; 
       25 41 110 ; 
       26 41 110 ; 
       27 44 110 ; 
       28 44 110 ; 
       29 44 110 ; 
       30 44 110 ; 
       31 48 110 ; 
       32 41 110 ; 
       33 48 110 ; 
       34 48 110 ; 
       35 48 110 ; 
       36 41 110 ; 
       37 41 110 ; 
       38 41 110 ; 
       39 41 110 ; 
       40 41 110 ; 
       41 46 110 ; 
       42 35 110 ; 
       43 0 112 ; 
       43 0 112 2 ; 
       44 46 110 ; 
       45 46 110 ; 
       46 47 110 ; 
       47 43 110 ; 
       48 46 110 ; 
       49 34 110 ; 
       50 31 110 ; 
       51 33 110 ; 
       52 47 110 ; 
       53 41 110 ; 
       54 44 110 ; 
       55 48 110 ; 
       56 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 1 300 ; 
       3 1 300 ; 
       4 1 300 ; 
       5 1 300 ; 
       6 1 300 ; 
       7 1 300 ; 
       8 1 300 ; 
       9 1 300 ; 
       10 1 300 ; 
       11 1 300 ; 
       12 1 300 ; 
       13 1 300 ; 
       14 1 300 ; 
       15 1 300 ; 
       16 1 300 ; 
       17 1 300 ; 
       18 1 300 ; 
       19 1 300 ; 
       20 1 300 ; 
       21 1 300 ; 
       22 1 300 ; 
       23 1 300 ; 
       24 1 300 ; 
       25 1 300 ; 
       26 1 300 ; 
       27 1 300 ; 
       28 1 300 ; 
       29 1 300 ; 
       30 1 300 ; 
       31 1 300 ; 
       32 1 300 ; 
       33 1 300 ; 
       34 1 300 ; 
       35 1 300 ; 
       36 1 300 ; 
       37 1 300 ; 
       38 1 300 ; 
       39 1 300 ; 
       40 1 300 ; 
       41 1 300 ; 
       44 1 300 ; 
       45 1 300 ; 
       46 1 300 ; 
       47 1 300 ; 
       48 1 300 ; 
       52 0 300 ; 
       53 1 300 ; 
       54 1 300 ; 
       55 1 300 ; 
       56 2 300 ; 
       57 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       52 4 400 ; 
       52 5 400 ; 
       52 6 400 ; 
       52 3 400 ; 
       52 2 400 ; 
       52 0 400 ; 
       52 1 400 ; 
       53 8 400 ; 
       54 9 400 ; 
       55 10 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       52 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       43 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       2 49 2111 ; 
       3 50 2111 ; 
       4 51 2111 ; 
       5 42 2111 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       2 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       2 0 550 ; 
       3 1 550 ; 
       4 2 550 ; 
       5 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER CAMERA_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 2 551 1 ; 
       0 3 551 1 ; 
       0 4 551 1 ; 
       0 5 551 1 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS CHAPTER LIGHTS 
       0 2 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 -29.9197 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 7.166333 -29.9197 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -29.9197 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 15 -29.9197 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -29.9197 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 20 -29.9197 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 47.5 -14.11606 0 MPRFLG 0 ; 
       2 SCHEM 70 -14.11606 0 MPRFLG 0 ; 
       3 SCHEM 67.5 -14.11606 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -14.11606 0 MPRFLG 0 ; 
       5 SCHEM 65 -14.11606 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -14.11606 0 MPRFLG 0 ; 
       7 SCHEM 75 -14.11606 0 MPRFLG 0 ; 
       8 SCHEM 77.5 -14.11606 0 MPRFLG 0 ; 
       9 SCHEM 80 -14.11606 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -14.11606 0 MPRFLG 0 ; 
       11 SCHEM 5 -14.11606 0 MPRFLG 0 ; 
       12 SCHEM 0 -14.11606 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -14.11606 0 MPRFLG 0 ; 
       14 SCHEM 10 -14.11606 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -14.11606 0 MPRFLG 0 ; 
       16 SCHEM 15 -14.11606 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -14.11606 0 MPRFLG 0 ; 
       18 SCHEM 20 -14.11606 0 MPRFLG 0 ; 
       19 SCHEM 25 -14.11606 0 MPRFLG 0 ; 
       20 SCHEM 27.5 -14.11606 0 MPRFLG 0 ; 
       21 SCHEM 35 -14.11606 0 MPRFLG 0 ; 
       22 SCHEM 22.5 -14.11606 0 MPRFLG 0 ; 
       23 SCHEM 55 -14.11606 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -14.11606 0 MPRFLG 0 ; 
       25 SCHEM 60 -14.11606 0 MPRFLG 0 ; 
       26 SCHEM 57.5 -14.11606 0 MPRFLG 0 ; 
       27 SCHEM 92.5 -14.11606 0 MPRFLG 0 ; 
       28 SCHEM 87.5 -14.11606 0 MPRFLG 0 ; 
       29 SCHEM 85 -14.11606 0 MPRFLG 0 ; 
       30 SCHEM 90 -14.11606 0 MPRFLG 0 ; 
       31 SCHEM 97.5 -14.11606 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -14.11606 0 MPRFLG 0 ; 
       33 SCHEM 100 -14.11606 0 MPRFLG 0 ; 
       34 SCHEM 95 -14.11606 0 MPRFLG 0 ; 
       35 SCHEM 102.5 -14.11606 0 MPRFLG 0 ; 
       36 SCHEM 30 -14.11606 0 MPRFLG 0 ; 
       37 SCHEM 32.5 -14.11606 0 MPRFLG 0 ; 
       38 SCHEM 45 -14.11606 0 MPRFLG 0 ; 
       39 SCHEM 40 -14.11606 0 MPRFLG 0 ; 
       40 SCHEM 42.5 -14.11606 0 MPRFLG 0 ; 
       41 SCHEM 45 -12.11606 0 MPRFLG 0 ; 
       42 SCHEM 102.5 -16.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 66.56538 -4 0 SRT 1 1 1 -0.0913973 0 1.103901 19.20452 1.307889 783.1678 MPRFLG 0 ; 
       44 SCHEM 77.5 -12.11606 0 MPRFLG 0 ; 
       45 SCHEM 13.75 -12.11606 0 MPRFLG 0 ; 
       46 SCHEM 52.5 -10.11606 0 MPRFLG 0 ; 
       47 SCHEM 65 -8.116063 0 USR MPRFLG 0 ; 
       48 SCHEM 100 -12.11606 0 MPRFLG 0 ; 
       49 SCHEM 95 -16.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 97.5 -16.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 100 -16.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 117.5 -10.11606 0 MPRFLG 0 ; 
       53 SCHEM 50 -14.11606 0 MPRFLG 0 ; 
       54 SCHEM 82.5 -14.11606 0 MPRFLG 0 ; 
       55 SCHEM 105 -14.11606 0 MPRFLG 0 ; 
       56 SCHEM 130 -10.11606 0 WIRECOL 4 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -29.9197 0 SRT 1 1 1 0 -2.906 0 15.05321 0 553.9581 MPRFLG 0 ; 
       57 SCHEM 0 -29.9197 0 DISPLAY 0 0 SRT 53.23993 53.23993 53.23993 0 0 0 -2.608452 -13.79824 -465.16 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 117.5 -12.11606 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44.9542 -22.96095 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 130 -12.11606 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 0 -31.9197 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 122.5 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 125 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 120 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 107.5 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 112.5 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 115 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 117.5 -14.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60.4719 -27.9197 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 82.5 -16.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 105 -16.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 127.5 -12.11606 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 131.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 145 400 317 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
