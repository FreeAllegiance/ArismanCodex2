THE BIOS

Bios technology is considerably cheaper than for other factions, but it
takes some time to complete their development. Planning ahead is critical.<p<p

Advantages

Research buildings cost $15,000 each, but they don't need to be upgraded.
Garrison can be upgraded to Starbase.
Stations repair 25% faster.
Shields regenerate 25% faster.
All development costs 50% less.
Ships have 15% higher turn rates.
Ships have 15% more scan range.
Ships have 15% lower signature.
Fighters, scouts, and interceptors can carry the heavy cloak (but this will reduce their maneuverability).
Eject pods can teleport.

Disadvantages

Research buildings take 5 minutes to build.
Stations have 25% less shield strength and armor.
All development takes 10 minutes to complete.
Eject pods have a maximum speed of 40 meters per second.