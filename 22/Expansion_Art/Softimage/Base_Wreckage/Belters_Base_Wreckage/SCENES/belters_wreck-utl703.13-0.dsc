SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.14-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       ss300_outpost-mat58.3-0 ; 
       ss300_outpost-mat59.2-0 ; 
       ss300_outpost-mat60.1-0 ; 
       ss300_outpost-mat61.2-0 ; 
       ss300_outpost-mat73.3-0 ; 
       ss300_outpost-mat76.2-0 ; 
       ss306_ripcord-mat5.1-0 ; 
       utl703-mat77.1-0 ; 
       utl703-mat78.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       root-cube23.1-0 ; 
       root-cube24.1-0 ; 
       root-cube4.1-0 ; 
       root-root.13-0 ROOT ; 
       root-sphere6.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/Belters_Base_Wreckage/PICTURES/bgrnd03 ; 
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/Belters_Base_Wreckage/PICTURES/utl703 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       belters_wreck-utl703.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       ss300_outpost-t2d58.6-0 ; 
       ss300_outpost-t2d59.6-0 ; 
       ss300_outpost-t2d68.7-0 ; 
       ss300_outpost-t2d72.2-0 ; 
       ss300_outpost-t2d73.7-0 ; 
       ss300_outpost-t2d76.6-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
       utl703-t2d77.1-0 ; 
       utl703-t2d78.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 8 300 ; 
       1 1 300 ; 
       1 3 300 ; 
       1 5 300 ; 
       1 7 300 ; 
       2 2 300 ; 
       4 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 0 401 ; 
       2 3 401 ; 
       3 1 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
