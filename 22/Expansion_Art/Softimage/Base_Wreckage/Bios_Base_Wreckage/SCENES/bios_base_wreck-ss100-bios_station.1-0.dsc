SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.2-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       ss100_bios_station-cube1_27.1-0 ; 
       ss100_bios_station-cube1_29.1-0 ; 
       ss100_bios_station-cube1_30.1-0 ; 
       ss100_bios_station-cube1_31.1-0 ; 
       ss100_bios_station-cube20.1-0 ; 
       ss100_bios_station-cube22_2_2.1-0 ; 
       ss100_bios_station-cube22_2_24.1-0 ; 
       ss100_bios_station-cube22_2_25.1-0 ; 
       ss100_bios_station-cube22_2_28.1-0 ; 
       ss100_bios_station-cube5_1.13-0 ; 
       ss100_bios_station-east_bay_11_10.1-0 ; 
       ss100_bios_station-east_bay_11_6.1-0 ; 
       ss100_bios_station-east_bay_11_8.1-0 ; 
       ss100_bios_station-east_bay_11_9.1-0 ; 
       ss100_bios_station-extru15.1-0 ; 
       ss100_bios_station-extru22.1-0 ; 
       ss100_bios_station-extru31.1-0 ; 
       ss100_bios_station-extru34.1-0 ; 
       ss100_bios_station-extru35.1-0 ; 
       ss100_bios_station-extru36.1-0 ; 
       ss100_bios_station-extru37.1-0 ; 
       ss100_bios_station-extru38.1-0 ; 
       ss100_bios_station-extru39.1-0 ; 
       ss100_bios_station-extru40.1-0 ; 
       ss100_bios_station-extru41.1-0 ; 
       ss100_bios_station-extru42.1-0 ; 
       ss100_bios_station-extru43.1-0 ; 
       ss100_bios_station-extru44.1-0 ; 
       ss100_bios_station-extru45.1-0 ; 
       ss100_bios_station-extru9.1-0 ; 
       ss100_bios_station-null21.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/Bios_Base_Wreckage/PICTURES/biosbay ; 
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/Bios_Base_Wreckage/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bios_base_wreck-ss100-bios_station.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 30 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
       26 4 110 ; 
       27 4 110 ; 
       28 4 110 ; 
       29 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 MPRFLG 0 ; 
       18 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -6 0 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 60 -6 0 MPRFLG 0 ; 
       23 SCHEM 35 -6 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 40 -6 0 MPRFLG 0 ; 
       26 SCHEM 45 -6 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 50 -6 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
