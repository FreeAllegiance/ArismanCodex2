SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.30-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       new-mat2_1.4-0 ; 
       re_do-mat268_1.4-0 ; 
       re_do-mat269.6-0 ; 
       re_do-mat274.5-0 ; 
       re_do-mat277.5-0 ; 
       re_do-mat278.5-0 ; 
       re_do-mat280.6-0 ; 
       re_do-mat283.4-0 ; 
       re_do-mat320.4-0 ; 
       re_do-mat321.4-0 ; 
       utl704-mat323.4-0 ; 
       utl704-mat324.3-0 ; 
       utl704-mat325.3-0 ; 
       utl704-mat326.3-0 ; 
       utl704-mat327.2-0 ; 
       utl704-mat334.1-0 ; 
       utl704-mat335.1-0 ; 
       utl704-mat336.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.1-0 ; 
       ss24a-cyl6.1-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.26-0 ROOT ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/GC_Base_Wreckage/PICTURES/bgrnd03 ; 
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/GC_Base_Wreckage/PICTURES/utl704 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       GC_base_wreckage-utl704.30-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       new-t2d2_1.5-0 ; 
       re_do-t2d268.7-0 ; 
       re_do-t2d271.7-0 ; 
       re_do-t2d272.7-0 ; 
       re_do-t2d274.8-0 ; 
       re_do-t2d277.5-0 ; 
       re_do-t2d280.6-0 ; 
       re_do-t2d281.6-0 ; 
       utl704-t2d283.7-0 ; 
       utl704-t2d284.5-0 ; 
       utl704-t2d285.6-0 ; 
       utl704-t2d286.5-0 ; 
       utl704-t2d287.4-0 ; 
       utl704-t2d288.3-0 ; 
       utl704-t2d294.1-0 ; 
       utl704-t2d295.1-0 ; 
       utl704-t2d296.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 9 110 ; 
       3 10 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 9 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       10 7 110 ; 
       2 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 14 300 ; 
       3 3 300 ; 
       3 13 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       5 6 300 ; 
       5 12 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 7 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 8 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 9 401 ; 
       11 12 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 45 -2 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
