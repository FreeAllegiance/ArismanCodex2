SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.2-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       base-bool5.2-0 ROOT ; 
       base-cube1.1-0 ; 
       base-cube10.1-0 ; 
       base-cube11.1-0 ; 
       base-cube12.1-0 ; 
       base-cube13.1-0 ; 
       base-cube14.1-0 ; 
       base-cube15.1-0 ; 
       base-cube16.1-0 ; 
       base-cube17.1-0 ; 
       base-cube18.1-0 ; 
       base-cube19.1-0 ; 
       base-cube2.1-0 ; 
       base-cube20.1-0 ; 
       base-cube21.1-0 ; 
       base-cube22.1-0 ; 
       base-cube23.1-0 ; 
       base-cube24.1-0 ; 
       base-cube25.1-0 ; 
       base-cube26.1-0 ; 
       base-cube27.1-0 ; 
       base-cube28.1-0 ; 
       base-cube29.1-0 ; 
       base-cube3.1-0 ; 
       base-cube30.1-0 ; 
       base-cube31.1-0 ; 
       base-cube32.1-0 ; 
       base-cube4.1-0 ; 
       base-cube5.1-0 ; 
       base-cube6.1-0 ; 
       base-cube7.1-0 ; 
       base-cube8.1-0 ; 
       base-cube9.1-0 ; 
       base-null1.1-0 ; 
       base-null2.1-0 ; 
       base-null3.1-0 ; 
       base-null4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Wreckage-base.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 35 110 ; 
       12 35 110 ; 
       23 35 110 ; 
       27 35 110 ; 
       28 33 110 ; 
       29 33 110 ; 
       30 33 110 ; 
       31 33 110 ; 
       32 33 110 ; 
       2 33 110 ; 
       3 33 110 ; 
       33 0 110 ; 
       4 36 110 ; 
       5 36 110 ; 
       6 36 110 ; 
       7 36 110 ; 
       8 36 110 ; 
       9 36 110 ; 
       10 36 110 ; 
       34 0 110 ; 
       11 33 110 ; 
       13 33 110 ; 
       14 33 110 ; 
       15 33 110 ; 
       16 33 110 ; 
       17 33 110 ; 
       18 33 110 ; 
       19 34 110 ; 
       20 34 110 ; 
       21 34 110 ; 
       22 34 110 ; 
       24 34 110 ; 
       25 34 110 ; 
       26 34 110 ; 
       35 0 110 ; 
       36 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 -6.956963e-015 0.5968131 2.135077e-008 MPRFLG 0 ; 
       12 SCHEM 75 -4 0 MPRFLG 0 ; 
       23 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 80 -4 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 5 -4 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 10 -4 0 MPRFLG 0 ; 
       32 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 55 -4 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 -4 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 65 -4 0 MPRFLG 0 ; 
       9 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 MPRFLG 0 ; 
       34 SCHEM 45 -2 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -4 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 35 -4 0 MPRFLG 0 ; 
       19 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 MPRFLG 0 ; 
       21 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 45 -4 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 50 -4 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       36 SCHEM 62.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
