SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.21-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       texture-bool5.11-0 ROOT ; 
       texture-cube1.1-0 ; 
       texture-cube2.1-0 ; 
       texture-cube33.1-0 ; 
       texture-cube34.2-0 ; 
       texture-cube35.2-0 ; 
       texture-cube36.2-0 ; 
       texture-cube37.2-0 ; 
       texture-cube38.2-0 ; 
       texture-cube39.2-0 ; 
       texture-cube40.2-0 ; 
       texture-cube41.2-0 ; 
       texture-cube42.2-0 ; 
       texture-cube43.2-0 ; 
       texture-cube44.2-0 ; 
       texture-cube45.2-0 ; 
       texture-cube46.2-0 ; 
       texture-cube47.2-0 ; 
       texture-cube48.2-0 ; 
       texture-cube49.2-0 ; 
       texture-cube50.2-0 ; 
       texture-cube51.2-0 ; 
       texture-cube52.2-0 ; 
       texture-cube53.2-0 ; 
       texture-cube54.2-0 ; 
       texture-cube55.2-0 ; 
       texture-cube56.2-0 ; 
       texture-cube57.2-0 ; 
       texture-cube58.2-0 ; 
       texture-cube59.2-0 ; 
       texture-cube60.2-0 ; 
       texture-cube61.2-0 ; 
       texture-cube62.1-0 ; 
       texture-cube63.1-0 ; 
       texture-cube64.1-0 ; 
       texture-cube65.1-0 ; 
       texture-null3.1-0 ; 
       texture-null4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/PICTURES/IC_Base_wreckage ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Wreckage-texture.15-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 36 110 ; 
       2 36 110 ; 
       32 37 110 ; 
       3 37 110 ; 
       33 37 110 ; 
       34 37 110 ; 
       35 37 110 ; 
       37 0 110 ; 
       36 0 110 ; 
       4 37 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       15 37 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 37 110 ; 
       19 37 110 ; 
       20 37 110 ; 
       21 37 110 ; 
       22 37 110 ; 
       23 37 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 37 110 ; 
       31 37 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       0 SCHEM 52.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       32 SCHEM 80 -8 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 85 -8 0 MPRFLG 0 ; 
       35 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 25 -8 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 30 -8 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 35 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 40 -8 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 45 -8 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 50 -8 0 MPRFLG 0 ; 
       17 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 55 -8 0 MPRFLG 0 ; 
       19 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 60 -8 0 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 65 -8 0 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 70 -8 0 MPRFLG 0 ; 
       25 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 75 -8 0 MPRFLG 0 ; 
       27 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 10 -8 0 MPRFLG 0 ; 
       30 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
