SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.23-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       utl701-bool5.1-0 ROOT ; 
       utl701-cube1.1-0 ; 
       utl701-cube2.1-0 ; 
       utl701-cube33.1-0 ; 
       utl701-cube34.2-0 ; 
       utl701-cube35.2-0 ; 
       utl701-cube36.2-0 ; 
       utl701-cube37.2-0 ; 
       utl701-cube38.2-0 ; 
       utl701-cube39.2-0 ; 
       utl701-cube40.2-0 ; 
       utl701-cube41.2-0 ; 
       utl701-cube42.2-0 ; 
       utl701-cube43.2-0 ; 
       utl701-cube44.2-0 ; 
       utl701-cube45.2-0 ; 
       utl701-cube46.2-0 ; 
       utl701-cube47.2-0 ; 
       utl701-cube48.2-0 ; 
       utl701-cube49.2-0 ; 
       utl701-cube50.2-0 ; 
       utl701-cube51.2-0 ; 
       utl701-cube52.2-0 ; 
       utl701-cube53.2-0 ; 
       utl701-cube54.2-0 ; 
       utl701-cube55.2-0 ; 
       utl701-cube56.2-0 ; 
       utl701-cube57.2-0 ; 
       utl701-cube58.2-0 ; 
       utl701-cube59.2-0 ; 
       utl701-cube60.2-0 ; 
       utl701-cube61.2-0 ; 
       utl701-cube62.1-0 ; 
       utl701-cube63.1-0 ; 
       utl701-cube64.1-0 ; 
       utl701-cube65.1-0 ; 
       utl701-null3.1-0 ; 
       utl701-null4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Base_Wreckage/PICTURES/IC_Base_wreckage ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_Wreckage-utl701.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 36 110 ; 
       2 36 110 ; 
       3 37 110 ; 
       4 37 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 37 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       15 37 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 37 110 ; 
       19 37 110 ; 
       20 37 110 ; 
       21 37 110 ; 
       22 37 110 ; 
       23 37 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 37 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       33 37 110 ; 
       34 37 110 ; 
       35 37 110 ; 
       36 0 110 ; 
       37 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 55 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 45 -4 0 MPRFLG 0 ; 
       14 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 MPRFLG 0 ; 
       16 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 55 -4 0 MPRFLG 0 ; 
       18 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 60 -4 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 65 -4 0 MPRFLG 0 ; 
       22 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 70 -4 0 MPRFLG 0 ; 
       24 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 75 -4 0 MPRFLG 0 ; 
       26 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 80 -4 0 MPRFLG 0 ; 
       28 SCHEM 10 -4 0 MPRFLG 0 ; 
       29 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 15 -4 0 MPRFLG 0 ; 
       31 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 85 -4 0 MPRFLG 0 ; 
       34 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 90 -4 0 MPRFLG 0 ; 
       36 SCHEM 5 -2 0 MPRFLG 0 ; 
       37 SCHEM 50 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
