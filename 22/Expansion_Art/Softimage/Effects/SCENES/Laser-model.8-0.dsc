SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.8-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       model-mat1.1-0 ; 
       model-mat2.2-0 ; 
       model-mat3.2-0 ; 
       model-mat4.2-0 ; 
       model-mat5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       laser-bolt.3-0 ROOT ; 
       laser-bottom.1-0 ; 
       laser-left.1-0 ; 
       laser-right.1-0 ; 
       laser-top.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       I:/Effects/PICTURES/laser ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Laser-model.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       model-t2d1.2-0 ; 
       model-t2d2.3-0 ; 
       model-t2d3.2-0 ; 
       model-t2d4.2-0 ; 
       model-t2d5.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       4 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 2 300 ; 
       4 1 300 ; 
       2 3 300 ; 
       3 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
