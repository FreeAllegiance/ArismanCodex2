SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig34_interceptor-null4_1_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.4-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       demo-mat127.1-0 ; 
       demo-mat128.1-0 ; 
       demo-mat129.1-0 ; 
       demo-mat132.1-0 ; 
       demo-mat133.1-0 ; 
       demo-mat134.1-0 ; 
       edit_nulls-mat70_1.1-0 ; 
       fig20_biofighter-mat71_1.1-0 ; 
       fig20_biofighter-mat75_1.1-0 ; 
       fig20_biofighter-mat77_1.1-0 ; 
       fig20_biofighter-mat78_1.1-0 ; 
       fig20_biofighter-mat80_1.1-0 ; 
       fig30_belter_ftr-mat100.1-0 ; 
       fig30_belter_ftr-mat101.1-0 ; 
       fig30_belter_ftr-mat102.1-0 ; 
       fig30_belter_ftr-mat103.1-0 ; 
       fig30_belter_ftr-mat113.1-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat81_1.1-0 ; 
       fig30_belter_ftr-mat82_1.1-0 ; 
       fig30_belter_ftr-mat83.1-0 ; 
       fig30_belter_ftr-mat84.1-0 ; 
       fig30_belter_ftr-mat87.1-0 ; 
       fig30_belter_ftr-mat88.1-0 ; 
       fig30_belter_ftr-mat89.1-0 ; 
       fig30_belter_ftr-mat90.1-0 ; 
       fig30_belter_ftr-mat91.1-0 ; 
       fig30_belter_ftr-mat92.1-0 ; 
       fig30_belter_ftr-mat98.1-0 ; 
       fig30_belter_ftr-mat99.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig34_interceptor-mat123.1-0 ; 
       fig34_interceptor-mat124.1-0 ; 
       fig34_interceptor-mat127.1-0 ; 
       fig34_interceptor-mat128.1-0 ; 
       fig34_interceptor-mat129.1-0 ; 
       fig34_interceptor-mat130.1-0 ; 
       fig34_interceptor-mat131.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       fig34_interceptor-Back.1-0 ; 
       fig34_interceptor-Back_1.1-0 ; 
       fig34_interceptor-cockpt.1-0 ; 
       fig34_interceptor-cube1.1-0 ; 
       fig34_interceptor-cube10.1-0 ; 
       fig34_interceptor-cube14.1-0 ; 
       fig34_interceptor-cube20.1-0 ; 
       fig34_interceptor-cube21.1-0 ; 
       fig34_interceptor-cube24.1-0 ; 
       fig34_interceptor-cube27.1-0 ; 
       fig34_interceptor-cube28.1-0 ; 
       fig34_interceptor-cube30.1-0 ; 
       fig34_interceptor-cube4.1-0 ; 
       fig34_interceptor-cube6.1-0 ; 
       fig34_interceptor-cube7.1-0 ; 
       fig34_interceptor-cyl1.1-0 ; 
       fig34_interceptor-cyl1_1.1-0 ; 
       fig34_interceptor-cyl1_2.1-0 ; 
       fig34_interceptor-cyl14.1-0 ; 
       fig34_interceptor-extru2.1-0 ; 
       fig34_interceptor-extru3.2-0 ; 
       fig34_interceptor-fwepemt.1-0 ; 
       fig34_interceptor-Horz.1-0 ; 
       fig34_interceptor-Horz_1.1-0 ; 
       fig34_interceptor-lthrust.1-0 ; 
       fig34_interceptor-lwepemt.1-0 ; 
       fig34_interceptor-missemt.1-0 ; 
       fig34_interceptor-msmoke.1-0 ; 
       fig34_interceptor-mthrust.1-0 ; 
       fig34_interceptor-null4_1_1.3-0 ROOT ; 
       fig34_interceptor-root.1-0 ; 
       fig34_interceptor-root_1.4-0 ; 
       fig34_interceptor-rsmoke.1-0 ; 
       fig34_interceptor-rthrust.1-0 ; 
       fig34_interceptor-rwepemt.1-0 ; 
       fig34_interceptor-SS01.1-0 ; 
       fig34_interceptor-SS02.1-0 ; 
       fig34_interceptor-SS03.1-0 ; 
       fig34_interceptor-SS04.1-0 ; 
       fig34_interceptor-SS05.1-0 ; 
       fig34_interceptor-SS06.1-0 ; 
       fig34_interceptor-SS07.1-0 ; 
       fig34_interceptor-SS08.1-0 ; 
       fig34_interceptor-trail.1-0 ; 
       fig34_interceptor-Vert.1-0 ; 
       fig34_interceptor-Vert_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/Effects/PICTURES/fig34 ; 
       //research/root/federation/Expansion_Art/Softimage/Effects/PICTURES/flash ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       muzzle_flash-demo.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       demo-t2d4.1-0 ; 
       demo-t2d5.1-0 ; 
       demo-t2d51.1-0 ; 
       demo-t2d52.1-0 ; 
       demo-t2d53.1-0 ; 
       demo-t2d6.1-0 ; 
       fig30_belter_ftr-t2d1_1.1-0 ; 
       fig30_belter_ftr-t2d10_1.1-0 ; 
       fig30_belter_ftr-t2d16_1.1-0 ; 
       fig30_belter_ftr-t2d17_1.1-0 ; 
       fig30_belter_ftr-t2d18_1.1-0 ; 
       fig30_belter_ftr-t2d19.1-0 ; 
       fig30_belter_ftr-t2d2_1.1-0 ; 
       fig30_belter_ftr-t2d20.1-0 ; 
       fig30_belter_ftr-t2d21_1.1-0 ; 
       fig30_belter_ftr-t2d32_1.1-0 ; 
       fig30_belter_ftr-t2d33_1.1-0 ; 
       fig30_belter_ftr-t2d34_1.1-0 ; 
       fig30_belter_ftr-t2d39_1.1-0 ; 
       fig30_belter_ftr-t2d5_1.1-0 ; 
       fig30_belter_ftr-t2d6_1.1-0 ; 
       fig30_belter_ftr-t2d7.1-0 ; 
       fig30_belter_ftr-t2d8_1.1-0 ; 
       fig30_belter_ftr-t2d9_1.1-0 ; 
       fig32_belter_stealth-t2d12_1.1-0 ; 
       fig32_belter_stealth-t2d16_1.1-0 ; 
       fig32_belter_stealth-t2d29_1.1-0 ; 
       fig32_belter_stealth-t2d30_1.1-0 ; 
       fig32_belter_stealth-t2d31_1.1-0 ; 
       fig34_interceptor-t2d42_1.1-0 ; 
       fig34_interceptor-t2d43_1.1-0 ; 
       fig34_interceptor-t2d46_1.1-0 ; 
       fig34_interceptor-t2d47_1.1-0 ; 
       fig34_interceptor-t2d48.1-0 ; 
       fig34_interceptor-t2d49.1-0 ; 
       fig34_interceptor-t2d50.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 31 110 ; 
       1 30 110 ; 
       2 19 110 ; 
       3 19 110 ; 
       4 3 110 ; 
       5 19 110 ; 
       6 20 110 ; 
       7 3 110 ; 
       8 19 110 ; 
       9 20 110 ; 
       10 3 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 3 110 ; 
       14 20 110 ; 
       15 20 110 ; 
       16 8 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 29 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 31 110 ; 
       23 30 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 19 110 ; 
       27 19 110 ; 
       28 19 110 ; 
       30 18 110 ; 
       31 17 110 ; 
       32 19 110 ; 
       33 19 110 ; 
       34 19 110 ; 
       35 19 110 ; 
       36 19 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 19 110 ; 
       40 19 110 ; 
       41 19 110 ; 
       42 19 110 ; 
       43 19 110 ; 
       44 31 110 ; 
       45 30 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 5 300 ; 
       3 28 300 ; 
       3 12 300 ; 
       3 18 300 ; 
       4 29 300 ; 
       4 15 300 ; 
       4 43 300 ; 
       5 16 300 ; 
       6 30 300 ; 
       7 31 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       9 39 300 ; 
       10 40 300 ; 
       11 41 300 ; 
       11 42 300 ; 
       12 32 300 ; 
       13 27 300 ; 
       14 25 300 ; 
       15 26 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       16 35 300 ; 
       16 36 300 ; 
       17 33 300 ; 
       18 34 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       19 19 300 ; 
       20 24 300 ; 
       20 17 300 ; 
       22 1 300 ; 
       23 4 300 ; 
       35 6 300 ; 
       36 7 300 ; 
       37 9 300 ; 
       38 8 300 ; 
       39 11 300 ; 
       40 10 300 ; 
       41 20 300 ; 
       42 21 300 ; 
       44 2 300 ; 
       45 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 5 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       22 6 401 ; 
       23 12 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 7 401 ; 
       30 8 401 ; 
       31 9 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 33 401 ; 
       42 34 401 ; 
       43 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 27.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 95 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 112.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 105 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 72.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 112.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 115 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 126.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 110 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 70 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 80 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 6.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 21.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 31.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 70 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 78.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 130 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 30 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 52.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 100 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 97.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 55 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 67.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 70 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 -0.31 0 0 0 MPRFLG 0 ; 
       30 SCHEM 30 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 20 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 60 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 62.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 65 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 42.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 40 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 47.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 45 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 57.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 50 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 92.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 90 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 15 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 22.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 32.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 140 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
