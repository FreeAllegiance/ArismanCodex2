SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       EVA01-cam_int1.5-0 ROOT ; 
       EVA01-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       EVA01-mat1.3-0 ; 
       EVA01-mat10.1-0 ; 
       EVA01-mat11.1-0 ; 
       EVA01-mat12.1-0 ; 
       EVA01-mat13.1-0 ; 
       EVA01-mat14.1-0 ; 
       EVA01-mat15.1-0 ; 
       EVA01-mat16.1-0 ; 
       EVA01-mat17.1-0 ; 
       EVA01-mat18.1-0 ; 
       EVA01-mat19.1-0 ; 
       EVA01-mat2.2-0 ; 
       EVA01-mat20.1-0 ; 
       EVA01-mat21.1-0 ; 
       EVA01-mat22.1-0 ; 
       EVA01-mat23.1-0 ; 
       EVA01-mat24.1-0 ; 
       EVA01-mat25.1-0 ; 
       EVA01-mat26.1-0 ; 
       EVA01-mat27.1-0 ; 
       EVA01-mat28.1-0 ; 
       EVA01-mat29.1-0 ; 
       EVA01-mat3.1-0 ; 
       EVA01-mat30.2-0 ; 
       EVA01-mat4.1-0 ; 
       EVA01-mat5.1-0 ; 
       EVA01-mat6.1-0 ; 
       EVA01-mat7.1-0 ; 
       EVA01-mat8.1-0 ; 
       EVA01-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       EVA01-bmerge2.4-0 ROOT ; 
       EVA01-bmerge3.1-0 ; 
       EVA01-cube11.1-0 ; 
       EVA01-cube12.1-0 ; 
       EVA01-cube14.1-0 ; 
       EVA01-cube17.1-0 ; 
       EVA01-cube19.1-0 ; 
       EVA01-cube2.1-0 ; 
       EVA01-cube20.1-0 ; 
       EVA01-cube21.1-0 ; 
       EVA01-cube22.1-0 ; 
       EVA01-cube23.1-0 ; 
       EVA01-cube3.1-0 ; 
       EVA01-cube8.1-0 ; 
       EVA01-cube9.1-0 ; 
       EVA01-cyl13.1-0 ; 
       EVA01-cyl14.1-0 ; 
       EVA01-cyl16.1-0 ; 
       EVA01-cyl17.1-0 ; 
       EVA01-cyl18.1-0 ; 
       EVA01-cyl19.1-0 ; 
       EVA01-cyl7.1-0 ; 
       EVA01-cyl8.1-0 ; 
       EVA01-sphere10.1-0 ; 
       EVA01-sphere11.1-0 ; 
       EVA01-sphere12.1-0 ; 
       EVA01-sphere13.1-0 ; 
       EVA01-sphere5.1-0 ; 
       EVA01-sphere8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/eva_suit/PICTURES/EVA01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-EVA01.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       EVA01-t2d1.2-0 ; 
       EVA01-t2d2.2-0 ; 
       EVA01-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 16 110 ; 
       3 12 110 ; 
       4 3 110 ; 
       5 12 110 ; 
       6 18 110 ; 
       7 0 110 ; 
       8 20 110 ; 
       9 8 110 ; 
       10 12 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       13 22 110 ; 
       14 13 110 ; 
       15 7 110 ; 
       16 23 110 ; 
       17 7 110 ; 
       18 24 110 ; 
       19 25 110 ; 
       20 26 110 ; 
       21 27 110 ; 
       22 28 110 ; 
       23 15 110 ; 
       24 17 110 ; 
       25 0 110 ; 
       26 19 110 ; 
       27 0 110 ; 
       28 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       1 0 300 ; 
       1 23 300 ; 
       2 21 300 ; 
       3 9 300 ; 
       4 10 300 ; 
       5 12 300 ; 
       6 17 300 ; 
       7 13 300 ; 
       8 27 300 ; 
       9 28 300 ; 
       10 7 300 ; 
       11 8 300 ; 
       12 6 300 ; 
       13 4 300 ; 
       14 5 300 ; 
       15 18 300 ; 
       16 20 300 ; 
       17 14 300 ; 
       18 16 300 ; 
       19 24 300 ; 
       20 26 300 ; 
       21 1 300 ; 
       22 3 300 ; 
       23 19 300 ; 
       24 15 300 ; 
       25 22 300 ; 
       26 25 300 ; 
       27 29 300 ; 
       28 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       11 2 401 ; 
       23 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 -4.768372e-007 12.44336 -0.8646591 MPRFLG 0 ; 
       1 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 55 -12 0 MPRFLG 0 ; 
       10 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       14 SCHEM 40 -12 0 MPRFLG 0 ; 
       15 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 60 -4 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 45 -4 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 MPRFLG 0 ; 
       24 SCHEM 30 -6 0 MPRFLG 0 ; 
       25 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       26 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       28 SCHEM 43.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
