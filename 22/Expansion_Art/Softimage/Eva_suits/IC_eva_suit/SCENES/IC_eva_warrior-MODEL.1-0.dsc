SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       MODEL-cam_int1.1-0 ROOT ; 
       MODEL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       MODEL-mat1.1-0 ; 
       MODEL-mat10.1-0 ; 
       MODEL-mat11.1-0 ; 
       MODEL-mat12.1-0 ; 
       MODEL-mat13.1-0 ; 
       MODEL-mat14.1-0 ; 
       MODEL-mat15.1-0 ; 
       MODEL-mat16.1-0 ; 
       MODEL-mat17.1-0 ; 
       MODEL-mat18.1-0 ; 
       MODEL-mat19.1-0 ; 
       MODEL-mat2.1-0 ; 
       MODEL-mat20.1-0 ; 
       MODEL-mat21.1-0 ; 
       MODEL-mat22.1-0 ; 
       MODEL-mat23.1-0 ; 
       MODEL-mat24.1-0 ; 
       MODEL-mat25.1-0 ; 
       MODEL-mat26.1-0 ; 
       MODEL-mat27.1-0 ; 
       MODEL-mat28.1-0 ; 
       MODEL-mat29.1-0 ; 
       MODEL-mat3.1-0 ; 
       MODEL-mat4.1-0 ; 
       MODEL-mat5.1-0 ; 
       MODEL-mat6.1-0 ; 
       MODEL-mat7.1-0 ; 
       MODEL-mat8.1-0 ; 
       MODEL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       MODEL-bmerge2.1-0 ROOT ; 
       MODEL-bmerge3.1-0 ; 
       MODEL-cube11.1-0 ; 
       MODEL-cube12.1-0 ; 
       MODEL-cube14.1-0 ; 
       MODEL-cube17.1-0 ; 
       MODEL-cube19.1-0 ; 
       MODEL-cube2.1-0 ; 
       MODEL-cube20.1-0 ; 
       MODEL-cube21.1-0 ; 
       MODEL-cube22.1-0 ; 
       MODEL-cube23.1-0 ; 
       MODEL-cube3.1-0 ; 
       MODEL-cube8.1-0 ; 
       MODEL-cube9.1-0 ; 
       MODEL-cyl13.1-0 ; 
       MODEL-cyl14.1-0 ; 
       MODEL-cyl16.1-0 ; 
       MODEL-cyl17.1-0 ; 
       MODEL-cyl18.1-0 ; 
       MODEL-cyl19.1-0 ; 
       MODEL-cyl7.1-0 ; 
       MODEL-cyl8.1-0 ; 
       MODEL-sphere10.1-0 ; 
       MODEL-sphere11.1-0 ; 
       MODEL-sphere12.1-0 ; 
       MODEL-sphere13.1-0 ; 
       MODEL-sphere5.1-0 ; 
       MODEL-sphere8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-MODEL.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 0 110 ; 
       27 0 110 ; 
       19 25 110 ; 
       26 19 110 ; 
       20 26 110 ; 
       28 21 110 ; 
       23 15 110 ; 
       8 20 110 ; 
       9 8 110 ; 
       1 0 110 ; 
       12 0 110 ; 
       3 12 110 ; 
       4 3 110 ; 
       5 12 110 ; 
       10 12 110 ; 
       17 7 110 ; 
       24 17 110 ; 
       18 24 110 ; 
       6 18 110 ; 
       11 10 110 ; 
       21 27 110 ; 
       22 28 110 ; 
       13 22 110 ; 
       14 13 110 ; 
       7 0 110 ; 
       15 7 110 ; 
       16 23 110 ; 
       2 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 22 300 ; 
       27 28 300 ; 
       19 23 300 ; 
       0 11 300 ; 
       26 24 300 ; 
       20 25 300 ; 
       28 2 300 ; 
       23 19 300 ; 
       8 26 300 ; 
       9 27 300 ; 
       1 0 300 ; 
       12 6 300 ; 
       3 9 300 ; 
       4 10 300 ; 
       5 12 300 ; 
       10 7 300 ; 
       17 14 300 ; 
       24 15 300 ; 
       18 16 300 ; 
       6 17 300 ; 
       11 8 300 ; 
       21 1 300 ; 
       22 3 300 ; 
       13 4 300 ; 
       14 5 300 ; 
       7 13 300 ; 
       15 18 300 ; 
       16 20 300 ; 
       2 21 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       25 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       27 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 60 -4 0 MPRFLG 0 ; 
       0 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 -4.768372e-007 12.44336 -0.8646591 MPRFLG 0 ; 
       26 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 MPRFLG 0 ; 
       8 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 55 -12 0 MPRFLG 0 ; 
       1 SCHEM 70 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 30 -6 0 MPRFLG 0 ; 
       18 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       21 SCHEM 45 -4 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       14 SCHEM 40 -12 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
