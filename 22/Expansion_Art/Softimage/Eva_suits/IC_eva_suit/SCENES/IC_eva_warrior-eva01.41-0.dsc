SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       EVA01-cam_int1.6-0 ROOT ; 
       EVA01-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       EVA01-mat1.3-0 ; 
       EVA01-mat15.4-0 ; 
       EVA01-mat18.2-0 ; 
       EVA01-mat19.2-0 ; 
       EVA01-mat2.2-0 ; 
       EVA01-mat20.4-0 ; 
       EVA01-mat21.2-0 ; 
       EVA01-mat22.2-0 ; 
       EVA01-mat23.2-0 ; 
       EVA01-mat24.2-0 ; 
       EVA01-mat25.3-0 ; 
       EVA01-mat3.2-0 ; 
       EVA01-mat30.2-0 ; 
       EVA01-mat4.2-0 ; 
       eva01-mat43.1-0 ; 
       eva01-mat44.1-0 ; 
       eva01-mat45.1-0 ; 
       eva01-mat46.1-0 ; 
       eva01-mat47.1-0 ; 
       eva01-mat48.1-0 ; 
       eva01-mat49.2-0 ; 
       EVA01-mat5.2-0 ; 
       eva01-mat50.2-0 ; 
       eva01-mat51.1-0 ; 
       eva01-mat52.1-0 ; 
       eva01-mat53.1-0 ; 
       eva01-mat54.1-0 ; 
       eva01-mat55.1-0 ; 
       eva01-mat56.1-0 ; 
       eva01-mat57.1-0 ; 
       eva01-mat58.1-0 ; 
       eva01-mat59.1-0 ; 
       EVA01-mat6.2-0 ; 
       eva01-mat60.1-0 ; 
       eva01-mat61.1-0 ; 
       eva01-mat62.1-0 ; 
       eva01-mat63.1-0 ; 
       eva01-mat64.1-0 ; 
       eva01-mat65.1-0 ; 
       EVA01-mat7.2-0 ; 
       EVA01-mat8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       EVA01-bmerge2.39-0 ROOT ; 
       EVA01-bmerge3.1-0 ; 
       EVA01-cockpt.1-0 ; 
       EVA01-cone1.1-0 ; 
       EVA01-cone2.1-0 ; 
       EVA01-cube12.1-0 ; 
       EVA01-cube14.1-0 ; 
       EVA01-cube17.1-0 ; 
       EVA01-cube19.1-0 ; 
       EVA01-cube2.1-0 ; 
       EVA01-cube20.1-0 ; 
       EVA01-cube21.1-0 ; 
       EVA01-cube3.1-0 ; 
       EVA01-cube30.1-0 ; 
       EVA01-cube31.1-0 ; 
       EVA01-cube32.1-0 ; 
       EVA01-cube35.1-0 ; 
       EVA01-cube36.1-0 ; 
       EVA01-cyl16.1-0 ; 
       EVA01-cyl17.1-0 ; 
       EVA01-cyl18.1-0 ; 
       EVA01-cyl19.1-0 ; 
       EVA01-cyl26.1-0 ; 
       EVA01-cyl27.1-0 ; 
       EVA01-cyl30.1-0 ; 
       EVA01-cyl31.1-0 ; 
       EVA01-lsmoke.1-0 ; 
       EVA01-lthrust.1-0 ; 
       EVA01-rsmoke.1-0 ; 
       EVA01-rthrust.1-0 ; 
       EVA01-sphere11.1-0 ; 
       EVA01-sphere12.1-0 ; 
       EVA01-sphere13.1-0 ; 
       EVA01-sphere18.1-0 ; 
       EVA01-sphere21.1-0 ; 
       EVA01-sphere22.1-0 ; 
       EVA01-SS1.1-0 ; 
       EVA01-SS2.1-0 ; 
       EVA01-SS3.1-0 ; 
       EVA01-trail.1-0 ; 
       EVA01-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/eva_suit/PICTURES/EVA01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-eva01.41-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       EVA01-t2d1.8-0 ; 
       EVA01-t2d13.6-0 ; 
       EVA01-t2d14.5-0 ; 
       EVA01-t2d15.5-0 ; 
       EVA01-t2d16.5-0 ; 
       EVA01-t2d17.5-0 ; 
       EVA01-t2d18.5-0 ; 
       EVA01-t2d2.9-0 ; 
       EVA01-t2d25.4-0 ; 
       EVA01-t2d26.4-0 ; 
       EVA01-t2d29.7-0 ; 
       EVA01-t2d3.4-0 ; 
       EVA01-t2d30.8-0 ; 
       eva01-t2d31.3-0 ; 
       eva01-t2d32.3-0 ; 
       eva01-t2d33.4-0 ; 
       eva01-t2d34.3-0 ; 
       eva01-t2d35.2-0 ; 
       eva01-t2d36.2-0 ; 
       eva01-t2d37.4-0 ; 
       eva01-t2d38.5-0 ; 
       eva01-t2d39.4-0 ; 
       EVA01-t2d4.4-0 ; 
       eva01-t2d40.4-0 ; 
       eva01-t2d41.2-0 ; 
       eva01-t2d42.2-0 ; 
       eva01-t2d43.2-0 ; 
       eva01-t2d44.2-0 ; 
       eva01-t2d45.2-0 ; 
       eva01-t2d46.2-0 ; 
       eva01-t2d47.2-0 ; 
       eva01-t2d48.2-0 ; 
       eva01-t2d49.2-0 ; 
       EVA01-t2d5.3-0 ; 
       eva01-t2d50.1-0 ; 
       EVA01-t2d6.6-0 ; 
       EVA01-t2d7.5-0 ; 
       EVA01-t2d8.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 12 110 ; 
       4 12 110 ; 
       5 12 110 ; 
       6 5 110 ; 
       7 12 110 ; 
       8 19 110 ; 
       9 0 110 ; 
       10 21 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       13 23 110 ; 
       14 12 110 ; 
       15 14 110 ; 
       16 25 110 ; 
       17 16 110 ; 
       18 9 110 ; 
       19 30 110 ; 
       20 31 110 ; 
       21 32 110 ; 
       22 9 110 ; 
       23 33 110 ; 
       24 34 110 ; 
       25 35 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 18 110 ; 
       31 0 110 ; 
       32 20 110 ; 
       33 22 110 ; 
       34 0 110 ; 
       35 24 110 ; 
       36 0 110 ; 
       37 0 110 ; 
       38 0 110 ; 
       39 0 110 ; 
       40 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 0 300 ; 
       1 12 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       5 2 300 ; 
       6 3 300 ; 
       7 5 300 ; 
       7 22 300 ; 
       7 37 300 ; 
       8 10 300 ; 
       9 6 300 ; 
       10 39 300 ; 
       11 40 300 ; 
       12 1 300 ; 
       12 20 300 ; 
       12 38 300 ; 
       13 17 300 ; 
       14 18 300 ; 
       15 19 300 ; 
       16 31 300 ; 
       17 33 300 ; 
       18 7 300 ; 
       19 9 300 ; 
       20 13 300 ; 
       21 32 300 ; 
       22 14 300 ; 
       23 16 300 ; 
       24 28 300 ; 
       25 30 300 ; 
       30 8 300 ; 
       31 11 300 ; 
       32 21 300 ; 
       33 15 300 ; 
       34 27 300 ; 
       35 29 300 ; 
       36 34 300 ; 
       37 35 300 ; 
       38 36 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 10 401 ; 
       2 8 401 ; 
       3 9 401 ; 
       4 11 401 ; 
       5 12 401 ; 
       6 22 401 ; 
       7 33 401 ; 
       8 37 401 ; 
       9 35 401 ; 
       10 36 401 ; 
       11 6 401 ; 
       12 0 401 ; 
       13 2 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 1 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 3 401 ; 
       33 31 401 ; 
       37 32 401 ; 
       38 34 401 ; 
       39 4 401 ; 
       40 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 75.12927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 61.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 60 -12 0 MPRFLG 0 ; 
       12 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 20 -6 0 MPRFLG 0 ; 
       16 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       17 SCHEM 75 -12 0 MPRFLG 0 ; 
       18 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 65 -4 0 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 80 -4 0 MPRFLG 0 ; 
       25 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 70.12927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 72.62927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 67.62927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -6 0 MPRFLG 0 ; 
       31 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       32 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 50 -6 0 MPRFLG 0 ; 
       34 SCHEM 81.25 -2 0 MPRFLG 0 ; 
       35 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       36 SCHEM 77.62927 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 80.12927 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 82.62927 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 85.12927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 65.12927 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 87.62927 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 77.62927 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 80.12927 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 82.62927 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 87.62927 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 75 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
