SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       reduce-cam_int1.10-0 ROOT ; 
       reduce-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       reduce-bmerge2.1-0 ROOT ; 
       reduce-bmerge3.1-0 ROOT ; 
       reduce-cube11.1-0 ; 
       reduce-cube12.1-0 ; 
       reduce-cube13.1-0 ; 
       reduce-cube14.1-0 ; 
       reduce-cube15.1-0 ; 
       reduce-cube17.1-0 ; 
       reduce-cube19.1-0 ; 
       reduce-cube2.1-0 ; 
       reduce-cube20.1-0 ; 
       reduce-cube21.1-0 ; 
       reduce-cube3.1-0 ; 
       reduce-cube8.1-0 ; 
       reduce-cube9.1-0 ; 
       reduce-cyl1.1-0 ; 
       reduce-cyl13.1-0 ; 
       reduce-cyl14.1-0 ; 
       reduce-cyl15.1-0 ; 
       reduce-cyl16.1-0 ; 
       reduce-cyl17.1-0 ; 
       reduce-cyl18.1-0 ; 
       reduce-cyl19.1-0 ; 
       reduce-cyl7.1-0 ; 
       reduce-cyl8.1-0 ; 
       reduce-sphere1.3-0 ROOT ; 
       reduce-sphere10.1-0 ; 
       reduce-sphere11.1-0 ; 
       reduce-sphere12.1-0 ; 
       reduce-sphere13.1-0 ; 
       reduce-sphere3.1-0 ; 
       reduce-sphere5.1-0 ; 
       reduce-sphere8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-reduce.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       28 0 110 ; 
       31 0 110 ; 
       21 28 110 ; 
       29 21 110 ; 
       22 29 110 ; 
       32 23 110 ; 
       26 16 110 ; 
       10 22 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       3 12 110 ; 
       5 3 110 ; 
       4 12 110 ; 
       6 4 110 ; 
       7 12 110 ; 
       18 7 110 ; 
       19 9 110 ; 
       27 19 110 ; 
       20 27 110 ; 
       8 20 110 ; 
       23 31 110 ; 
       24 32 110 ; 
       13 24 110 ; 
       14 13 110 ; 
       9 0 110 ; 
       16 9 110 ; 
       17 26 110 ; 
       2 17 110 ; 
       15 25 110 ; 
       30 25 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       28 SCHEM 28.91241 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 26.41241 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 28.91241 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -4.768372e-007 12.44336 -0.8646591 MPRFLG 0 ; 
       29 SCHEM 28.91241 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 28.91241 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 26.41241 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 15 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 28.91241 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 28.91241 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 33.91241 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 2.384186e-007 25.11824 0.6552038 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 26.41241 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 26.41241 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.41241 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.41241 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 16.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -4 0 DISPLAY 0 0 SRT 0.8725408 1.016 1.016 0 0 0 0 23.8797 -2.134982 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
