SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       reduce-cam_int1.11-0 ROOT ; 
       reduce-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       reduce-bmerge2.2-0 ROOT ; 
       reduce-bmerge3.1-0 ; 
       reduce-cube11.1-0 ; 
       reduce-cube12.1-0 ; 
       reduce-cube14.1-0 ; 
       reduce-cube17.1-0 ; 
       reduce-cube19.1-0 ; 
       reduce-cube2.1-0 ; 
       reduce-cube20.1-0 ; 
       reduce-cube21.1-0 ; 
       reduce-cube22.1-0 ; 
       reduce-cube23.1-0 ; 
       reduce-cube3.1-0 ; 
       reduce-cube8.1-0 ; 
       reduce-cube9.1-0 ; 
       reduce-cyl13.1-0 ; 
       reduce-cyl14.1-0 ; 
       reduce-cyl16.1-0 ; 
       reduce-cyl17.1-0 ; 
       reduce-cyl18.1-0 ; 
       reduce-cyl19.1-0 ; 
       reduce-cyl7.1-0 ; 
       reduce-cyl8.1-0 ; 
       reduce-sphere10.1-0 ; 
       reduce-sphere11.1-0 ; 
       reduce-sphere12.1-0 ; 
       reduce-sphere13.1-0 ; 
       reduce-sphere5.1-0 ; 
       reduce-sphere8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-reduce.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 0 110 ; 
       27 0 110 ; 
       19 25 110 ; 
       26 19 110 ; 
       20 26 110 ; 
       28 21 110 ; 
       23 15 110 ; 
       8 20 110 ; 
       9 8 110 ; 
       1 0 110 ; 
       12 0 110 ; 
       3 12 110 ; 
       4 3 110 ; 
       5 12 110 ; 
       10 12 110 ; 
       17 7 110 ; 
       24 17 110 ; 
       18 24 110 ; 
       6 18 110 ; 
       11 10 110 ; 
       21 27 110 ; 
       22 28 110 ; 
       13 22 110 ; 
       14 13 110 ; 
       7 0 110 ; 
       15 7 110 ; 
       16 23 110 ; 
       2 16 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       25 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 -4.768372e-007 12.44336 -0.8646591 MPRFLG 0 ; 
       26 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 15 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 15 -4 0 MPRFLG 0 ; 
       22 SCHEM 15 -8 0 MPRFLG 0 ; 
       13 SCHEM 15 -10 0 MPRFLG 0 ; 
       14 SCHEM 15 -12 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 10 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       2 SCHEM 10 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
