SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       reduce-cam_int1.9-0 ROOT ; 
       reduce-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       reduce-bmerge2.1-0 ROOT ; 
       reduce-cube11.1-0 ; 
       reduce-cube12.1-0 ; 
       reduce-cube13.1-0 ; 
       reduce-cube14.1-0 ; 
       reduce-cube15.1-0 ; 
       reduce-cube17.1-0 ; 
       reduce-cube19.1-0 ; 
       reduce-cube2.1-0 ; 
       reduce-cube20.1-0 ; 
       reduce-cube21.1-0 ; 
       reduce-cube3.1-0 ; 
       reduce-cube8.1-0 ; 
       reduce-cube9.1-0 ; 
       reduce-cyl1.1-0 ; 
       reduce-cyl13.1-0 ; 
       reduce-cyl14.1-0 ; 
       reduce-cyl15.1-0 ; 
       reduce-cyl16.1-0 ; 
       reduce-cyl17.1-0 ; 
       reduce-cyl18.1-0 ; 
       reduce-cyl19.1-0 ; 
       reduce-cyl7.1-0 ; 
       reduce-cyl8.1-0 ; 
       reduce-sphere1.2-0 ROOT ; 
       reduce-sphere10.1-0 ; 
       reduce-sphere11.1-0 ; 
       reduce-sphere12.1-0 ; 
       reduce-sphere13.1-0 ; 
       reduce-sphere3.1-0 ; 
       reduce-sphere5.1-0 ; 
       reduce-sphere8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-reduce.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       27 0 110 ; 
       30 0 110 ; 
       20 27 110 ; 
       28 20 110 ; 
       21 28 110 ; 
       31 22 110 ; 
       25 15 110 ; 
       9 21 110 ; 
       10 9 110 ; 
       11 0 110 ; 
       2 11 110 ; 
       4 2 110 ; 
       3 11 110 ; 
       5 3 110 ; 
       6 11 110 ; 
       17 6 110 ; 
       18 8 110 ; 
       26 18 110 ; 
       19 26 110 ; 
       7 19 110 ; 
       22 30 110 ; 
       23 31 110 ; 
       12 23 110 ; 
       13 12 110 ; 
       8 0 110 ; 
       15 8 110 ; 
       16 25 110 ; 
       1 16 110 ; 
       14 24 110 ; 
       29 24 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       27 SCHEM 28.91241 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 26.41241 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 28.91241 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -4.768372e-007 12.44336 -0.8646591 MPRFLG 0 ; 
       28 SCHEM 28.91241 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 28.91241 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 26.41241 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 28.91241 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 28.91241 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 26.41241 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 26.41241 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.41241 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.41241 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 15 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 3.75 -4 0 SRT 0.8725408 1.016 1.016 0 0 0 0 23.8797 -2.134982 MPRFLG 0 ; 
       14 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
