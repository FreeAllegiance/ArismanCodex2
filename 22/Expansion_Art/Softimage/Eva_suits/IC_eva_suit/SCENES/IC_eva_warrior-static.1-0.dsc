SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       EVA01-cam_int1.6-0 ROOT ; 
       EVA01-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       EVA01-mat1.3-0 ; 
       EVA01-mat15.4-0 ; 
       EVA01-mat18.2-0 ; 
       EVA01-mat19.2-0 ; 
       EVA01-mat2.2-0 ; 
       EVA01-mat20.4-0 ; 
       EVA01-mat21.2-0 ; 
       EVA01-mat22.2-0 ; 
       EVA01-mat23.2-0 ; 
       EVA01-mat24.2-0 ; 
       EVA01-mat25.3-0 ; 
       EVA01-mat3.2-0 ; 
       EVA01-mat30.2-0 ; 
       EVA01-mat4.2-0 ; 
       EVA01-mat5.2-0 ; 
       EVA01-mat6.2-0 ; 
       EVA01-mat7.2-0 ; 
       EVA01-mat8.2-0 ; 
       static-mat43.1-0 ; 
       static-mat44.1-0 ; 
       static-mat45.1-0 ; 
       static-mat46.1-0 ; 
       static-mat47.1-0 ; 
       static-mat48.1-0 ; 
       static-mat49.1-0 ; 
       static-mat50.1-0 ; 
       static-mat51.1-0 ; 
       static-mat52.1-0 ; 
       static-mat53.1-0 ; 
       static-mat54.1-0 ; 
       static-mat55.1-0 ; 
       static-mat56.1-0 ; 
       static-mat57.1-0 ; 
       static-mat58.1-0 ; 
       static-mat59.1-0 ; 
       static-mat60.1-0 ; 
       static-mat64.1-0 ; 
       static-mat65.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       EVA01-bmerge2.40-0 ROOT ; 
       EVA01-bmerge3.1-0 ; 
       EVA01-cone1.1-0 ; 
       EVA01-cone2.1-0 ; 
       EVA01-cube12.1-0 ; 
       EVA01-cube14.1-0 ; 
       EVA01-cube17.1-0 ; 
       EVA01-cube19.1-0 ; 
       EVA01-cube2.1-0 ; 
       EVA01-cube20.1-0 ; 
       EVA01-cube21.1-0 ; 
       EVA01-cube3.1-0 ; 
       EVA01-cube30.1-0 ; 
       EVA01-cube31.1-0 ; 
       EVA01-cube32.1-0 ; 
       EVA01-cube35.1-0 ; 
       EVA01-cube36.1-0 ; 
       EVA01-cyl16.1-0 ; 
       EVA01-cyl17.1-0 ; 
       EVA01-cyl18.1-0 ; 
       EVA01-cyl19.1-0 ; 
       EVA01-cyl26.1-0 ; 
       EVA01-cyl27.1-0 ; 
       EVA01-cyl30.1-0 ; 
       EVA01-cyl31.1-0 ; 
       EVA01-sphere11.1-0 ; 
       EVA01-sphere12.1-0 ; 
       EVA01-sphere13.1-0 ; 
       EVA01-sphere18.1-0 ; 
       EVA01-sphere21.1-0 ; 
       EVA01-sphere22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/eva_suit/PICTURES/EVA01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       IC_eva_warrior-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       EVA01-t2d1.8-0 ; 
       EVA01-t2d13.6-0 ; 
       EVA01-t2d14.5-0 ; 
       EVA01-t2d15.5-0 ; 
       EVA01-t2d16.5-0 ; 
       EVA01-t2d17.5-0 ; 
       EVA01-t2d18.5-0 ; 
       EVA01-t2d2.9-0 ; 
       EVA01-t2d25.4-0 ; 
       EVA01-t2d26.4-0 ; 
       EVA01-t2d29.7-0 ; 
       EVA01-t2d3.4-0 ; 
       EVA01-t2d30.8-0 ; 
       EVA01-t2d4.4-0 ; 
       EVA01-t2d5.3-0 ; 
       EVA01-t2d6.6-0 ; 
       EVA01-t2d7.5-0 ; 
       EVA01-t2d8.4-0 ; 
       static-t2d31.1-0 ; 
       static-t2d32.1-0 ; 
       static-t2d33.1-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d36.1-0 ; 
       static-t2d37.1-0 ; 
       static-t2d38.1-0 ; 
       static-t2d39.1-0 ; 
       static-t2d40.1-0 ; 
       static-t2d41.1-0 ; 
       static-t2d42.1-0 ; 
       static-t2d43.1-0 ; 
       static-t2d44.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d46.1-0 ; 
       static-t2d47.1-0 ; 
       static-t2d48.1-0 ; 
       static-t2d49.1-0 ; 
       static-t2d50.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 4 110 ; 
       6 11 110 ; 
       7 18 110 ; 
       8 0 110 ; 
       9 20 110 ; 
       10 9 110 ; 
       11 0 110 ; 
       12 22 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 24 110 ; 
       16 15 110 ; 
       17 8 110 ; 
       18 25 110 ; 
       19 26 110 ; 
       20 27 110 ; 
       21 8 110 ; 
       22 28 110 ; 
       23 29 110 ; 
       24 30 110 ; 
       25 17 110 ; 
       26 0 110 ; 
       27 19 110 ; 
       28 21 110 ; 
       29 0 110 ; 
       30 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 0 300 ; 
       1 12 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       4 2 300 ; 
       5 3 300 ; 
       6 5 300 ; 
       6 25 300 ; 
       6 36 300 ; 
       7 10 300 ; 
       8 6 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       11 1 300 ; 
       11 24 300 ; 
       11 37 300 ; 
       12 21 300 ; 
       13 22 300 ; 
       14 23 300 ; 
       15 34 300 ; 
       16 35 300 ; 
       17 7 300 ; 
       18 9 300 ; 
       19 13 300 ; 
       20 15 300 ; 
       21 18 300 ; 
       22 20 300 ; 
       23 31 300 ; 
       24 33 300 ; 
       25 8 300 ; 
       26 11 300 ; 
       27 14 300 ; 
       28 19 300 ; 
       29 30 300 ; 
       30 32 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 10 401 ; 
       2 8 401 ; 
       3 9 401 ; 
       4 11 401 ; 
       5 12 401 ; 
       6 13 401 ; 
       7 14 401 ; 
       8 17 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 6 401 ; 
       12 0 401 ; 
       13 2 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       14 1 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       15 3 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       16 4 401 ; 
       17 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -10 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 20 -10 0 MPRFLG 0 ; 
       10 SCHEM 20 -12 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
       14 SCHEM 10 -6 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       17 SCHEM 15 -4 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 MPRFLG 0 ; 
       19 SCHEM 20 -4 0 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 MPRFLG 0 ; 
       26 SCHEM 20 -2 0 MPRFLG 0 ; 
       27 SCHEM 20 -6 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 22.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
