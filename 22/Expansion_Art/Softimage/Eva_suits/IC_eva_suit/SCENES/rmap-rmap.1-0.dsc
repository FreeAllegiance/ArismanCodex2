SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       EVA01-cam_int1.6-0 ROOT ; 
       EVA01-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       EVA01-mat1.4-0 ; 
       EVA01-mat30.3-0 ; 
       rmap-blue4.1-0 ; 
       rmap-bone1.1-0 ; 
       rmap-default1.1-0 ; 
       rmap-flblack1.1-0 ; 
       rmap-flbrown1.1-0 ; 
       rmap-flesh3.1-0 ; 
       rmap-flwhite1.1-0 ; 
       rmap-mat31.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       EVA01-bmerge3.1-0 ROOT ; 
       rmap-brow.1-0 ; 
       rmap-eyewhite.1-0 ; 
       rmap-hair.1-0 ; 
       rmap-iris.1-0 ; 
       rmap-Poly_Male_Hero.1-0 ROOT ; 
       rmap-pupil.1-0 ; 
       rmap-skin.1-0 ; 
       rmap-sphere1.1-0 ROOT ; 
       rmap-torso.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/eva_suit/PICTURES/EVA01 ; 
       //research/root/federation/Expansion_Art/Softimage/eva_suit/PICTURES/orion ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       rmap-rmap.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       EVA01-t2d1.7-0 ; 
       EVA01-t2d2.7-0 ; 
       rmap-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 5 110 ; 
       4 5 110 ; 
       2 5 110 ; 
       1 5 110 ; 
       3 5 110 ; 
       7 5 110 ; 
       9 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 4 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       6 5 300 ; 
       8 9 300 ; 
       2 8 300 ; 
       1 6 300 ; 
       3 3 300 ; 
       7 7 300 ; 
       9 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       9 2 401 ; 
       1 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 16.25 0 0 SRT 0.2099405 0.2099405 0.2099405 0 0 0 0 0.08784868 0.655989 MPRFLG 0 ; 
       0 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 -3.583337e-007 3.418166 0.7030395 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 27.5 0 0 SRT 10.35 10.35 10.35 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
