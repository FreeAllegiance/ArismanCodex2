SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.34-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       model-bmerge5.3-0 ; 
       model-bmerge6.4-0 ROOT ; 
       model-cube1.10-0 ; 
       model-cube6.1-0 ; 
       model-cube7.1-0 ; 
       model-cube8.1-0 ; 
       model-cyl13.1-0 ; 
       model-cyl14.1-0 ; 
       model-cyl15.1-0 ; 
       model-cyl16.1-0 ; 
       model-cyl17.1-0 ; 
       model-cyl18.1-0 ; 
       model-cyl2.3-0 ; 
       model-cyl4.1-0 ; 
       model-Head.3-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       eva02-model.34-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 1 110 ; 
       6 1 110 ; 
       11 10 110 ; 
       7 6 110 ; 
       2 13 110 ; 
       3 7 110 ; 
       12 0 110 ; 
       13 12 110 ; 
       8 0 110 ; 
       9 8 110 ; 
       14 1 110 ; 
       4 9 110 ; 
       5 11 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 -13.34468 1.603473 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
