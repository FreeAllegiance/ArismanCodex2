SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.59-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       new-gold1.2-0 ; 
       new-gold2.2-0 ; 
       new-gold3.2-0 ; 
       new-gold4.2-0 ; 
       new-gold5.2-0 ; 
       new-gold6.2-0 ; 
       new-gold7.2-0 ; 
       new-mat22.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       rendermap-mat16.5-0 ; 
       rendermap-mat19.2-0 ; 
       rendermap-mat20.2-0 ; 
       rendermap-mat21.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       new-bmerge12.1-0 ; 
       new-bmerge13.1-0 ; 
       new-bmerge14.1-0 ; 
       new-bmerge5.3-0 ; 
       new-bmerge6_1.4-0 ROOT ; 
       new-bmerge9.1-0 ; 
       new-cube10.3-0 ; 
       new-cube12.1-0 ; 
       new-cube13.1-0 ; 
       new-cube9.1-0 ; 
       new-fuselg7.1-0 ; 
       new-fuselg8.1-0 ; 
       new-Head.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Eva_suits/Rix_Eva/PICTURES/eva02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       eva02-new.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       new-t2d1.3-0 ; 
       new-t2d13.1-0 ; 
       new-t2d14.1-0 ; 
       new-t2d15.1-0 ; 
       new-t2d16.1-0 ; 
       new-t2d17.2-0 ; 
       new-t2d3.2-0 ; 
       new-t2d6.1-0 ; 
       new-t2d8.1-0 ; 
       new-t2d9.1-0 ; 
       rendermap-t2d10.4-0 ; 
       rendermap-t2d11.4-0 ; 
       rendermap-t2d12.5-0 ; 
       rendermap-t2d5.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 4 110 ; 
       2 3 110 ; 
       3 4 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       3 2 300 ; 
       4 0 300 ; 
       5 4 300 ; 
       6 11 300 ; 
       7 13 300 ; 
       8 7 300 ; 
       9 12 300 ; 
       10 10 300 ; 
       11 8 300 ; 
       12 1 300 ; 
       12 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 13 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       0 6 401 ; 
       1 0 401 ; 
       2 8 401 ; 
       3 9 401 ; 
       4 7 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 -13.34468 1.603473 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 8.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       10 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
