SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.47-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rendermap-light1.18-0 ROOT ; 
       rendermap-light2.18-0 ROOT ; 
       rendermap-light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       rendermap-gold1.4-0 ; 
       rendermap-gold2.3-0 ; 
       rendermap-gold3.3-0 ; 
       rendermap-gold4.2-0 ; 
       rendermap-gold5.2-0 ; 
       rendermap-mat16.4-0 ; 
       rendermap-mat19.1-0 ; 
       rendermap-mat20.1-0 ; 
       rendermap-mat21.1-0 ; 
       rendermap-mat22.3-0 ; 
       rendermap-mat23.2-0 ; 
       rendermap-mat24.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       rendermap-bmerge12.1-0 ; 
       rendermap-bmerge5.3-0 ; 
       rendermap-bmerge6_1.7-0 ROOT ; 
       rendermap-bmerge9.1-0 ; 
       rendermap-cube10.3-0 ; 
       rendermap-cube12.1-0 ; 
       rendermap-cube9.1-0 ; 
       rendermap-fuselg7.1-0 ; 
       rendermap-Head.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Eva_suits/Rix_Eva/PICTURES/eva02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       eva02-rendermap.18-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       rendermap-t2d1.7-0 ; 
       rendermap-t2d10.4-0 ; 
       rendermap-t2d11.4-0 ; 
       rendermap-t2d12.5-0 ; 
       rendermap-t2d3.12-0 ; 
       rendermap-t2d5.4-0 ; 
       rendermap-t2d6.6-0 ; 
       rendermap-t2d8.7-0 ; 
       rendermap-t2d9.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       rendermap-brain1.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 6 110 ; 
       3 2 110 ; 
       0 1 110 ; 
       1 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       8 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 5 300 ; 
       3 4 300 ; 
       0 3 300 ; 
       1 2 300 ; 
       1 11 300 ; 
       2 0 300 ; 
       2 9 300 ; 
       4 6 300 ; 
       5 8 300 ; 
       6 7 300 ; 
       8 1 300 ; 
       8 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       3 0 500 ; 
       0 0 500 ; 
       1 0 500 ; 
       2 0 500 ; 
       8 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 5 401 ; 
       6 1 401 ; 
       4 6 401 ; 
       7 2 401 ; 
       3 8 401 ; 
       8 3 401 ; 
       2 7 401 ; 
       1 0 401 ; 
       0 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 36 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 38.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 41 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 19.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 17 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 9.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 12 -2 0 USR MPRFLG 0 ; 
       2 SCHEM 18.25 0 0 SRT 1 1 1 0 0 0 0 -13.34468 1.603473 MPRFLG 0 ; 
       4 SCHEM 23.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 20.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 4.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 18.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 33.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 3.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 25.35571 -24.00262 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
