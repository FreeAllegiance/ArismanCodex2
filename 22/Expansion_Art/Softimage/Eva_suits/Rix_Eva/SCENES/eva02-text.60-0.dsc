SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.54-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       rendermap-mat16.5-0 ; 
       rendermap-mat19.2-0 ; 
       rendermap-mat20.2-0 ; 
       rendermap-mat21.2-0 ; 
       text-gold1.2-0 ; 
       text-gold2.2-0 ; 
       text-gold3.2-0 ; 
       text-gold4.2-0 ; 
       text-gold5.2-0 ; 
       text-gold6.2-0 ; 
       text-gold7.2-0 ; 
       text-mat22.2-0 ; 
       text-mat23.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       text-bmerge12.1-0 ; 
       text-bmerge13.1-0 ; 
       text-bmerge14.1-0 ; 
       text-bmerge5.3-0 ; 
       text-bmerge6_1.33-0 ROOT ; 
       text-bmerge9.1-0 ; 
       text-cube10.3-0 ; 
       text-cube12.1-0 ; 
       text-cube13.1-0 ; 
       text-cube9.1-0 ; 
       text-fuselg7.1-0 ; 
       text-fuselg8.1-0 ; 
       text-Head.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Eva_suits/Rix_Eva/PICTURES/eva02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       eva02-text.60-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       rendermap-t2d10.4-0 ; 
       rendermap-t2d11.4-0 ; 
       rendermap-t2d12.5-0 ; 
       rendermap-t2d5.4-0 ; 
       text-t2d1.11-0 ; 
       text-t2d13.3-0 ; 
       text-t2d14.2-0 ; 
       text-t2d15.1-0 ; 
       text-t2d16.1-0 ; 
       text-t2d3.9-0 ; 
       text-t2d6.7-0 ; 
       text-t2d8.12-0 ; 
       text-t2d9.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 4 110 ; 
       2 3 110 ; 
       3 4 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 9 300 ; 
       2 10 300 ; 
       3 6 300 ; 
       4 4 300 ; 
       5 8 300 ; 
       6 1 300 ; 
       7 3 300 ; 
       8 11 300 ; 
       9 2 300 ; 
       10 0 300 ; 
       11 12 300 ; 
       12 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 9 401 ; 
       5 4 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 10 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 -13.34468 1.603473 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
