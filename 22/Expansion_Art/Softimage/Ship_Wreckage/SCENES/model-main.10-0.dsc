SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       main-cam_int1.10-0 ROOT ; 
       main-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       main-mat1.2-0 ; 
       main-mat10.1-0 ; 
       main-mat11.1-0 ; 
       main-mat12.1-0 ; 
       main-mat14.1-0 ; 
       main-mat15.1-0 ; 
       main-mat16.1-0 ; 
       main-mat17.1-0 ; 
       main-mat18.1-0 ; 
       main-mat19.1-0 ; 
       main-mat2.1-0 ; 
       main-mat20.1-0 ; 
       main-mat21.1-0 ; 
       main-mat22.1-0 ; 
       main-mat23.1-0 ; 
       main-mat24.1-0 ; 
       main-mat25.1-0 ; 
       main-mat26.1-0 ; 
       main-mat27.1-0 ; 
       main-mat28.1-0 ; 
       main-mat3.1-0 ; 
       main-mat30.1-0 ; 
       main-mat31.1-0 ; 
       main-mat33.1-0 ; 
       main-mat34.1-0 ; 
       main-mat35.1-0 ; 
       main-mat36.1-0 ; 
       main-mat38.1-0 ; 
       main-mat39.1-0 ; 
       main-mat4.1-0 ; 
       main-mat40.1-0 ; 
       main-mat41.1-0 ; 
       main-mat43.1-0 ; 
       main-mat5.1-0 ; 
       main-mat6.1-0 ; 
       main-mat7.1-0 ; 
       main-mat8.1-0 ; 
       main-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       main-cone1.1-0 ; 
       main-cube1.2-0 ; 
       main-cube10.1-0 ; 
       main-cube12.1-0 ; 
       main-cube13.1-0 ; 
       main-cube14.1-0 ; 
       main-cube15.1-0 ; 
       main-cube16.1-0 ; 
       main-cube17.1-0 ; 
       main-cube18.1-0 ; 
       main-cube19.1-0 ; 
       main-cube20.1-0 ; 
       main-cube21.1-0 ; 
       main-cube22.1-0 ; 
       main-cube23.1-0 ; 
       main-cube24.1-0 ; 
       main-cube25.1-0 ; 
       main-cube26.1-0 ; 
       main-cube28.1-0 ; 
       main-cube29.1-0 ; 
       main-cube3.1-0 ; 
       main-cube31.1-0 ; 
       main-cube32.1-0 ; 
       main-cube33.1-0 ; 
       main-cube34.1-0 ; 
       main-cube35.1-0 ; 
       main-cube37.1-0 ; 
       main-cube38.1-0 ; 
       main-cube4.1-0 ; 
       main-cube40.1-0 ; 
       main-cube41.1-0 ; 
       main-cube42.1-0 ; 
       main-cube43.1-0 ; 
       main-cube43_1.1-0 ; 
       main-cube5.1-0 ; 
       main-cube6.1-0 ; 
       main-cube7.1-0 ; 
       main-cube8.1-0 ; 
       main-cube9.1-0 ; 
       main-cyl1.6-0 ; 
       main-null1.1-0 ; 
       main-null10.1-0 ; 
       main-null11.1-0 ; 
       main-null12.1-0 ; 
       main-null13.2-0 ROOT ; 
       main-null14.1-0 ; 
       main-null15.2-0 ROOT ; 
       main-null2.1-0 ; 
       main-null3.1-0 ; 
       main-null4.1-0 ; 
       main-null5.1-0 ; 
       main-null6.1-0 ; 
       main-null7.1-0 ; 
       main-null8.1-0 ; 
       main-null9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Ship_Wreckage/PICTURES/ic_shipwreck ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-main.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       main-t2d1.3-0 ; 
       main-t2d10.1-0 ; 
       main-t2d11.1-0 ; 
       main-t2d12.1-0 ; 
       main-t2d14.1-0 ; 
       main-t2d15.1-0 ; 
       main-t2d16.1-0 ; 
       main-t2d17.1-0 ; 
       main-t2d18.1-0 ; 
       main-t2d19.1-0 ; 
       main-t2d2.1-0 ; 
       main-t2d20.1-0 ; 
       main-t2d21.1-0 ; 
       main-t2d22.1-0 ; 
       main-t2d23.1-0 ; 
       main-t2d24.1-0 ; 
       main-t2d25.1-0 ; 
       main-t2d26.1-0 ; 
       main-t2d27.1-0 ; 
       main-t2d28.1-0 ; 
       main-t2d3.2-0 ; 
       main-t2d30.1-0 ; 
       main-t2d31.1-0 ; 
       main-t2d33.1-0 ; 
       main-t2d34.1-0 ; 
       main-t2d35.1-0 ; 
       main-t2d36.1-0 ; 
       main-t2d38.1-0 ; 
       main-t2d39.1-0 ; 
       main-t2d4.1-0 ; 
       main-t2d40.1-0 ; 
       main-t2d41.1-0 ; 
       main-t2d43.1-0 ; 
       main-t2d5.1-0 ; 
       main-t2d6.1-0 ; 
       main-t2d7.1-0 ; 
       main-t2d8.1-0 ; 
       main-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       39 44 110 ; 
       0 39 110 ; 
       1 40 110 ; 
       34 40 110 ; 
       20 40 110 ; 
       40 45 110 ; 
       28 40 110 ; 
       35 47 110 ; 
       36 47 110 ; 
       37 47 110 ; 
       38 47 110 ; 
       47 45 110 ; 
       2 48 110 ; 
       3 48 110 ; 
       4 48 110 ; 
       48 45 110 ; 
       49 45 110 ; 
       5 49 110 ; 
       6 49 110 ; 
       7 49 110 ; 
       8 49 110 ; 
       50 45 110 ; 
       9 50 110 ; 
       10 50 110 ; 
       11 50 110 ; 
       12 50 110 ; 
       51 45 110 ; 
       13 51 110 ; 
       14 51 110 ; 
       15 51 110 ; 
       16 51 110 ; 
       17 52 110 ; 
       31 46 110 ; 
       18 52 110 ; 
       19 52 110 ; 
       52 54 110 ; 
       53 54 110 ; 
       32 46 110 ; 
       21 53 110 ; 
       22 53 110 ; 
       23 53 110 ; 
       54 45 110 ; 
       41 45 110 ; 
       42 41 110 ; 
       24 42 110 ; 
       25 42 110 ; 
       33 46 110 ; 
       26 42 110 ; 
       43 41 110 ; 
       27 43 110 ; 
       29 43 110 ; 
       30 43 110 ; 
       45 44 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       39 0 300 ; 
       39 10 300 ; 
       0 20 300 ; 
       1 29 300 ; 
       34 35 300 ; 
       20 33 300 ; 
       28 34 300 ; 
       35 36 300 ; 
       36 37 300 ; 
       37 1 300 ; 
       38 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       4 5 300 ; 
       5 6 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       8 9 300 ; 
       9 11 300 ; 
       10 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       13 15 300 ; 
       14 16 300 ; 
       15 17 300 ; 
       16 18 300 ; 
       17 19 300 ; 
       18 21 300 ; 
       19 22 300 ; 
       21 23 300 ; 
       22 24 300 ; 
       23 25 300 ; 
       24 30 300 ; 
       25 31 300 ; 
       26 32 300 ; 
       27 26 300 ; 
       29 27 300 ; 
       30 28 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       10 10 401 ; 
       20 20 401 ; 
       29 29 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       39 SCHEM 5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 55 -6 0 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 MPRFLG 0 ; 
       40 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 90 -6 0 MPRFLG 0 ; 
       36 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 95 -6 0 MPRFLG 0 ; 
       47 SCHEM 91.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 MPRFLG 0 ; 
       48 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       49 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       50 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       51 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 35 -6 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 97.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 60 -8 0 MPRFLG 0 ; 
       19 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       52 SCHEM 60 -6 0 MPRFLG 0 ; 
       53 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 100 -2 0 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 65 -8 0 MPRFLG 0 ; 
       23 SCHEM 70 -8 0 MPRFLG 0 ; 
       54 SCHEM 63.75 -4 0 MPRFLG 0 ; 
       41 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       42 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 85 -8 0 MPRFLG 0 ; 
       25 SCHEM 80 -8 0 MPRFLG 0 ; 
       33 SCHEM 102.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 75 -6 0 MPRFLG 0 ; 
       27 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 75 -8 0 MPRFLG 0 ; 
       44 SCHEM 48.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       45 SCHEM 52.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
