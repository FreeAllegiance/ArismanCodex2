SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       main-cam_int1.12-0 ROOT ; 
       main-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       main-mat1.2-0 ; 
       main-mat10.1-0 ; 
       main-mat11.1-0 ; 
       main-mat12.1-0 ; 
       main-mat14.1-0 ; 
       main-mat15.1-0 ; 
       main-mat16.1-0 ; 
       main-mat17.1-0 ; 
       main-mat18.1-0 ; 
       main-mat2.1-0 ; 
       main-mat20.1-0 ; 
       main-mat21.1-0 ; 
       main-mat22.1-0 ; 
       main-mat23.1-0 ; 
       main-mat24.1-0 ; 
       main-mat25.1-0 ; 
       main-mat26.1-0 ; 
       main-mat27.1-0 ; 
       main-mat28.1-0 ; 
       main-mat3.1-0 ; 
       main-mat30.1-0 ; 
       main-mat31.1-0 ; 
       main-mat33.1-0 ; 
       main-mat34.1-0 ; 
       main-mat35.1-0 ; 
       main-mat36.1-0 ; 
       main-mat38.1-0 ; 
       main-mat39.1-0 ; 
       main-mat4.1-0 ; 
       main-mat40.1-0 ; 
       main-mat41.1-0 ; 
       main-mat43.1-0 ; 
       main-mat44.1-0 ; 
       main-mat45.1-0 ; 
       main-mat46.1-0 ; 
       main-mat47.1-0 ; 
       main-mat48.1-0 ; 
       main-mat5.1-0 ; 
       main-mat6.1-0 ; 
       main-mat7.1-0 ; 
       main-mat8.1-0 ; 
       main-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       main-cone1.1-0 ; 
       main-cone2.1-0 ; 
       main-cube1.2-0 ; 
       main-cube10.1-0 ; 
       main-cube12.1-0 ; 
       main-cube13.1-0 ; 
       main-cube14.1-0 ; 
       main-cube15.1-0 ; 
       main-cube16.1-0 ; 
       main-cube18.1-0 ; 
       main-cube19.1-0 ; 
       main-cube20.1-0 ; 
       main-cube21.1-0 ; 
       main-cube22.1-0 ; 
       main-cube23.1-0 ; 
       main-cube24.1-0 ; 
       main-cube25.1-0 ; 
       main-cube26.1-0 ; 
       main-cube28.1-0 ; 
       main-cube29.1-0 ; 
       main-cube3.1-0 ; 
       main-cube31.1-0 ; 
       main-cube32.1-0 ; 
       main-cube33.1-0 ; 
       main-cube34.1-0 ; 
       main-cube35.1-0 ; 
       main-cube37.1-0 ; 
       main-cube38.1-0 ; 
       main-cube4.1-0 ; 
       main-cube40.1-0 ; 
       main-cube41.1-0 ; 
       main-cube42.1-0 ; 
       main-cube43_1.1-0 ; 
       main-cube43_2.1-0 ; 
       main-cube43_2_1.1-0 ; 
       main-cube5.1-0 ; 
       main-cube6.1-0 ; 
       main-cube7.1-0 ; 
       main-cube8.1-0 ; 
       main-cube9.1-0 ; 
       main-cyl1.6-0 ; 
       main-null1.1-0 ; 
       main-null10.1-0 ; 
       main-null11.1-0 ; 
       main-null12.1-0 ; 
       main-null13.4-0 ROOT ; 
       main-null14.1-0 ; 
       main-null15.2-0 ; 
       main-null2.1-0 ; 
       main-null3.1-0 ; 
       main-null4.1-0 ; 
       main-null5.1-0 ; 
       main-null6.1-0 ; 
       main-null7.1-0 ; 
       main-null8.1-0 ; 
       main-null9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Ship_Wreckage/PICTURES/ic_shipwreck ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-main.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       main-t2d1.3-0 ; 
       main-t2d10.1-0 ; 
       main-t2d11.1-0 ; 
       main-t2d12.1-0 ; 
       main-t2d14.1-0 ; 
       main-t2d15.1-0 ; 
       main-t2d16.1-0 ; 
       main-t2d17.1-0 ; 
       main-t2d18.1-0 ; 
       main-t2d2.1-0 ; 
       main-t2d20.1-0 ; 
       main-t2d21.1-0 ; 
       main-t2d22.1-0 ; 
       main-t2d23.1-0 ; 
       main-t2d24.1-0 ; 
       main-t2d25.1-0 ; 
       main-t2d26.1-0 ; 
       main-t2d27.1-0 ; 
       main-t2d28.1-0 ; 
       main-t2d3.2-0 ; 
       main-t2d30.1-0 ; 
       main-t2d31.1-0 ; 
       main-t2d33.1-0 ; 
       main-t2d34.1-0 ; 
       main-t2d35.1-0 ; 
       main-t2d36.1-0 ; 
       main-t2d38.1-0 ; 
       main-t2d39.1-0 ; 
       main-t2d4.1-0 ; 
       main-t2d40.1-0 ; 
       main-t2d41.1-0 ; 
       main-t2d43.1-0 ; 
       main-t2d44.1-0 ; 
       main-t2d5.1-0 ; 
       main-t2d6.1-0 ; 
       main-t2d7.1-0 ; 
       main-t2d8.1-0 ; 
       main-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       40 45 110 ; 
       0 40 110 ; 
       2 41 110 ; 
       35 41 110 ; 
       20 41 110 ; 
       41 46 110 ; 
       28 41 110 ; 
       36 48 110 ; 
       37 48 110 ; 
       38 48 110 ; 
       39 48 110 ; 
       48 46 110 ; 
       3 49 110 ; 
       47 45 110 ; 
       4 49 110 ; 
       5 49 110 ; 
       49 46 110 ; 
       50 46 110 ; 
       6 50 110 ; 
       7 50 110 ; 
       8 50 110 ; 
       51 46 110 ; 
       9 51 110 ; 
       10 51 110 ; 
       11 51 110 ; 
       12 51 110 ; 
       52 46 110 ; 
       13 52 110 ; 
       14 52 110 ; 
       15 52 110 ; 
       16 52 110 ; 
       17 53 110 ; 
       31 47 110 ; 
       18 53 110 ; 
       19 53 110 ; 
       53 55 110 ; 
       54 55 110 ; 
       34 47 110 ; 
       21 54 110 ; 
       22 54 110 ; 
       23 54 110 ; 
       55 46 110 ; 
       42 46 110 ; 
       43 42 110 ; 
       24 43 110 ; 
       25 43 110 ; 
       32 47 110 ; 
       26 43 110 ; 
       44 42 110 ; 
       27 44 110 ; 
       1 45 110 ; 
       29 44 110 ; 
       30 44 110 ; 
       46 45 110 ; 
       33 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       40 0 300 ; 
       40 9 300 ; 
       0 19 300 ; 
       2 28 300 ; 
       35 39 300 ; 
       20 37 300 ; 
       28 38 300 ; 
       36 40 300 ; 
       37 41 300 ; 
       38 1 300 ; 
       39 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       9 10 300 ; 
       10 11 300 ; 
       11 12 300 ; 
       12 13 300 ; 
       13 14 300 ; 
       14 15 300 ; 
       15 16 300 ; 
       16 17 300 ; 
       17 18 300 ; 
       31 34 300 ; 
       18 20 300 ; 
       19 21 300 ; 
       34 36 300 ; 
       21 22 300 ; 
       22 23 300 ; 
       23 24 300 ; 
       24 29 300 ; 
       25 30 300 ; 
       32 35 300 ; 
       26 31 300 ; 
       27 25 300 ; 
       1 32 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       33 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       9 9 401 ; 
       19 19 401 ; 
       28 28 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       32 32 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       35 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 50 -6 0 MPRFLG 0 ; 
       36 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 90 -6 0 MPRFLG 0 ; 
       38 SCHEM 85 -6 0 MPRFLG 0 ; 
       39 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       47 SCHEM 101.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       49 SCHEM 40 -4 0 MPRFLG 0 ; 
       50 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       52 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 MPRFLG 0 ; 
       31 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 60 -8 0 MPRFLG 0 ; 
       53 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 65 -6 0 MPRFLG 0 ; 
       34 SCHEM 105 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 65 -8 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       42 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       43 SCHEM 80 -6 0 MPRFLG 0 ; 
       24 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 100 -4 0 MPRFLG 0 ; 
       26 SCHEM 80 -8 0 MPRFLG 0 ; 
       44 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 95 -2 0 MPRFLG 0 ; 
       29 SCHEM 70 -8 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       45 SCHEM 53.75 0 0 SRT 1 1 1 0 0 0 7.491362e-009 0 -4.30002e-009 MPRFLG 0 ; 
       46 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       33 SCHEM 102.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
