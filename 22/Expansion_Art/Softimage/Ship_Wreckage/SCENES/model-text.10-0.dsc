SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.10-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       text-mat1.1-0 ; 
       text-mat10.1-0 ; 
       text-mat11.1-0 ; 
       text-mat12.1-0 ; 
       text-mat14.1-0 ; 
       text-mat15.1-0 ; 
       text-mat16.1-0 ; 
       text-mat17.1-0 ; 
       text-mat18.1-0 ; 
       text-mat2.1-0 ; 
       text-mat20.1-0 ; 
       text-mat21.1-0 ; 
       text-mat22.1-0 ; 
       text-mat23.1-0 ; 
       text-mat24.1-0 ; 
       text-mat25.1-0 ; 
       text-mat26.1-0 ; 
       text-mat27.1-0 ; 
       text-mat28.1-0 ; 
       text-mat3.1-0 ; 
       text-mat30.1-0 ; 
       text-mat31.1-0 ; 
       text-mat33.1-0 ; 
       text-mat34.1-0 ; 
       text-mat35.1-0 ; 
       text-mat36.1-0 ; 
       text-mat38.1-0 ; 
       text-mat39.1-0 ; 
       text-mat4.1-0 ; 
       text-mat40.1-0 ; 
       text-mat41.1-0 ; 
       text-mat43.1-0 ; 
       text-mat44.2-0 ; 
       text-mat45.1-0 ; 
       text-mat46.1-0 ; 
       text-mat47.1-0 ; 
       text-mat48.1-0 ; 
       text-mat49.1-0 ; 
       text-mat5.1-0 ; 
       text-mat6.1-0 ; 
       text-mat7.1-0 ; 
       text-mat8.1-0 ; 
       text-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       text-cone1.1-0 ; 
       text-cone2.1-0 ; 
       text-cube1.2-0 ; 
       text-cube10.1-0 ; 
       text-cube12.1-0 ; 
       text-cube13.1-0 ; 
       text-cube14.1-0 ; 
       text-cube15.1-0 ; 
       text-cube16.1-0 ; 
       text-cube18.1-0 ; 
       text-cube19.1-0 ; 
       text-cube20.1-0 ; 
       text-cube21.1-0 ; 
       text-cube22.1-0 ; 
       text-cube23.1-0 ; 
       text-cube24.1-0 ; 
       text-cube25.1-0 ; 
       text-cube26.1-0 ; 
       text-cube28.1-0 ; 
       text-cube29.1-0 ; 
       text-cube3.1-0 ; 
       text-cube31.1-0 ; 
       text-cube32.1-0 ; 
       text-cube33.1-0 ; 
       text-cube34.1-0 ; 
       text-cube35.1-0 ; 
       text-cube37.1-0 ; 
       text-cube38.1-0 ; 
       text-cube4.1-0 ; 
       text-cube40.1-0 ; 
       text-cube41.1-0 ; 
       text-cube42.1-0 ; 
       text-cube43_1.1-0 ; 
       text-cube43_2.1-0 ; 
       text-cube43_2_1.1-0 ; 
       text-cube5.1-0 ; 
       text-cube6.1-0 ; 
       text-cube7.1-0 ; 
       text-cube8.1-0 ; 
       text-cube9.1-0 ; 
       text-cyl1.6-0 ; 
       text-null1.1-0 ; 
       text-null10.1-0 ; 
       text-null11.1-0 ; 
       text-null12.1-0 ; 
       text-null13.7-0 ROOT ; 
       text-null14.1-0 ; 
       text-null15.2-0 ; 
       text-null2.1-0 ; 
       text-null3.1-0 ; 
       text-null4.1-0 ; 
       text-null5.1-0 ; 
       text-null6.1-0 ; 
       text-null7.1-0 ; 
       text-null8.1-0 ; 
       text-null9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/Ship_Wreckage/PICTURES/ic_shipwreck ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-text.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       text-t2d1.1-0 ; 
       text-t2d10.1-0 ; 
       text-t2d11.1-0 ; 
       text-t2d12.1-0 ; 
       text-t2d14.1-0 ; 
       text-t2d15.1-0 ; 
       text-t2d16.1-0 ; 
       text-t2d17.1-0 ; 
       text-t2d18.1-0 ; 
       text-t2d2.1-0 ; 
       text-t2d20.1-0 ; 
       text-t2d21.1-0 ; 
       text-t2d22.1-0 ; 
       text-t2d23.1-0 ; 
       text-t2d24.1-0 ; 
       text-t2d25.1-0 ; 
       text-t2d26.1-0 ; 
       text-t2d27.1-0 ; 
       text-t2d28.1-0 ; 
       text-t2d3.1-0 ; 
       text-t2d30.1-0 ; 
       text-t2d31.1-0 ; 
       text-t2d33.1-0 ; 
       text-t2d34.1-0 ; 
       text-t2d35.1-0 ; 
       text-t2d36.1-0 ; 
       text-t2d38.1-0 ; 
       text-t2d39.1-0 ; 
       text-t2d4.2-0 ; 
       text-t2d40.1-0 ; 
       text-t2d41.1-0 ; 
       text-t2d43.1-0 ; 
       text-t2d44.3-0 ; 
       text-t2d45.2-0 ; 
       text-t2d46.2-0 ; 
       text-t2d47.2-0 ; 
       text-t2d48.2-0 ; 
       text-t2d49.2-0 ; 
       text-t2d5.1-0 ; 
       text-t2d6.1-0 ; 
       text-t2d7.1-0 ; 
       text-t2d8.1-0 ; 
       text-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       40 45 110 ; 
       0 40 110 ; 
       2 41 110 ; 
       35 41 110 ; 
       20 41 110 ; 
       41 46 110 ; 
       28 41 110 ; 
       36 48 110 ; 
       37 48 110 ; 
       38 48 110 ; 
       39 48 110 ; 
       48 46 110 ; 
       3 49 110 ; 
       47 45 110 ; 
       4 49 110 ; 
       5 49 110 ; 
       49 46 110 ; 
       50 46 110 ; 
       6 50 110 ; 
       7 50 110 ; 
       8 50 110 ; 
       51 46 110 ; 
       9 51 110 ; 
       10 51 110 ; 
       11 51 110 ; 
       12 51 110 ; 
       52 46 110 ; 
       13 52 110 ; 
       14 52 110 ; 
       15 52 110 ; 
       16 52 110 ; 
       17 53 110 ; 
       31 47 110 ; 
       18 53 110 ; 
       19 53 110 ; 
       53 55 110 ; 
       54 55 110 ; 
       34 47 110 ; 
       21 54 110 ; 
       22 54 110 ; 
       23 54 110 ; 
       55 46 110 ; 
       42 46 110 ; 
       43 42 110 ; 
       24 43 110 ; 
       25 43 110 ; 
       32 47 110 ; 
       26 43 110 ; 
       44 42 110 ; 
       27 44 110 ; 
       1 45 110 ; 
       29 44 110 ; 
       30 44 110 ; 
       46 45 110 ; 
       33 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       40 0 300 ; 
       40 9 300 ; 
       0 19 300 ; 
       2 28 300 ; 
       35 40 300 ; 
       20 38 300 ; 
       28 39 300 ; 
       36 41 300 ; 
       37 42 300 ; 
       38 1 300 ; 
       39 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       9 10 300 ; 
       10 11 300 ; 
       11 12 300 ; 
       12 13 300 ; 
       13 14 300 ; 
       14 15 300 ; 
       15 16 300 ; 
       16 17 300 ; 
       17 18 300 ; 
       31 34 300 ; 
       18 20 300 ; 
       19 21 300 ; 
       34 36 300 ; 
       21 22 300 ; 
       22 23 300 ; 
       23 24 300 ; 
       24 29 300 ; 
       25 30 300 ; 
       32 35 300 ; 
       26 31 300 ; 
       27 25 300 ; 
       1 32 300 ; 
       1 37 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       33 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       9 9 401 ; 
       19 19 401 ; 
       28 28 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       41 41 401 ; 
       42 42 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       32 32 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       33 36 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       34 34 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       35 35 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       36 33 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       37 37 401 ; 
       31 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       35 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 50 -6 0 MPRFLG 0 ; 
       36 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 90 -6 0 MPRFLG 0 ; 
       38 SCHEM 85 -6 0 MPRFLG 0 ; 
       39 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       47 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       49 SCHEM 40 -4 0 MPRFLG 0 ; 
       50 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       52 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 MPRFLG 0 ; 
       31 SCHEM 100 -4 0 MPRFLG 0 ; 
       18 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 60 -8 0 MPRFLG 0 ; 
       53 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 65 -6 0 MPRFLG 0 ; 
       34 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 65 -8 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       42 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       43 SCHEM 80 -6 0 MPRFLG 0 ; 
       24 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 80 -8 0 MPRFLG 0 ; 
       44 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       29 SCHEM 70 -8 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       45 SCHEM 55 0 0 SRT 1 1 1 0 0 0 7.491362e-009 0 -4.30002e-009 MPRFLG 0 ; 
       46 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       33 SCHEM 105 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
