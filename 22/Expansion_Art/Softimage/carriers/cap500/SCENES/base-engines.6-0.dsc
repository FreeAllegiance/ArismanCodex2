SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       engines-antenna.1-0 ; 
       engines-Body.1-0 ROOT ; 
       engines-L_engine_pod.3-0 ; 
       engines-R_engine_pod.1-0 ; 
       engines-tower.1-0 ; 
       engines-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-engines.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       0 4 110 ; 
       5 1 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       0 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 274.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 272.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 269.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 304.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 282.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 289.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 264.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 294.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 292.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 302.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 277.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 284.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 267.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 297.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 299.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 279.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 287.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 262.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
