SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       engines-antenna.1-0 ; 
       engines-Body.1-0 ROOT ; 
       engines-L_engine_pod.3-0 ; 
       engines-R_engine_pod.1-0 ; 
       engines-tower.1-0 ; 
       engines-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-engines.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       0 4 110 ; 
       5 1 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       0 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
