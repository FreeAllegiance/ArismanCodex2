SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.11-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       mat-Bottom1.1-0 ; 
       mat-mat1.1-0 ; 
       mat-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       mat-antenna.1-0 ; 
       mat-Body.1-0 ROOT ; 
       mat-L_engine_pod.3-0 ; 
       mat-R_engine_pod.1-0 ; 
       mat-tower.1-0 ; 
       mat-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-mat.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 5 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       1 2 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 12.5 0 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
