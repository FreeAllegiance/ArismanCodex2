SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.70-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       text-Antenna1.2-0 ; 
       text-Bottom_inside1.5-0 ; 
       text-Bottom1.12-0 ; 
       text-Bottom3.2-0 ; 
       text-Bottom4.2-0 ; 
       text-engine_glow3.2-0 ; 
       text-engine_glow4.2-0 ; 
       text-inside1.7-0 ; 
       text-intake1.8-0 ; 
       text-mat1.7-0 ; 
       text-mat2.2-0 ; 
       text-Side.7-0 ; 
       text-Side3.2-0 ; 
       text-Side4.2-0 ; 
       text-stripes1.1-0 ; 
       text-top.11-0 ; 
       text-tower_base.3-0 ; 
       text-tower1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       text-antenna.1-0 ; 
       text-Body_1.13-0 ROOT ; 
       text-cube1.1-0 ; 
       text-cube3.1-0 ; 
       text-cube4.3-0 ROOT ; 
       text-L_engine_pod.1-0 ; 
       text-r_engine_pod.1-0 ; 
       text-tower.1-0 ; 
       text-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/IC_Carrier/PICTURES/cap500 ; 
       I:/IC_Carrier/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-text.65-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       text-bottom.21-0 ; 
       text-intake1.19-0 ; 
       text-t2d10.4-0 ; 
       text-t2d11.2-0 ; 
       text-t2d12.3-0 ; 
       text-t2d13.3-0 ; 
       text-t2d14.3-0 ; 
       text-t2d15.3-0 ; 
       text-t2d16.11-0 ; 
       text-t2d17.11-0 ; 
       text-t2d18.1-0 ; 
       text-t2d19.1-0 ; 
       text-t2d2.17-0 ; 
       text-t2d20.4-0 ; 
       text-t2d8.4-0 ; 
       text-t2d9.4-0 ; 
       text-top.22-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 8 110 ; 
       8 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 11 300 ; 
       1 15 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 14 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       5 3 300 ; 
       5 5 300 ; 
       5 12 300 ; 
       6 4 300 ; 
       6 6 300 ; 
       6 13 300 ; 
       7 17 300 ; 
       8 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 10 400 ; 
       3 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 0 401 ; 
       3 14 401 ; 
       4 5 401 ; 
       5 15 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 1 401 ; 
       11 12 401 ; 
       12 2 401 ; 
       13 7 401 ; 
       15 16 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       14 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 0.5 0.231 0.5 0 0 0 0 1.386098 -4.735185 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
