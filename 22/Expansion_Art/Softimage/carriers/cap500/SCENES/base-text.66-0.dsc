SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.71-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       text-Antenna1.2-0 ; 
       text-Bottom_inside1.5-0 ; 
       text-Bottom1.12-0 ; 
       text-Bottom3.2-0 ; 
       text-Bottom4.2-0 ; 
       text-engine_glow3.2-0 ; 
       text-engine_glow4.2-0 ; 
       text-inside1.7-0 ; 
       text-intake1.8-0 ; 
       text-mat1.7-0 ; 
       text-mat2.2-0 ; 
       text-Side.7-0 ; 
       text-Side3.2-0 ; 
       text-Side4.2-0 ; 
       text-stripes1.1-0 ; 
       text-top.11-0 ; 
       text-tower_base.3-0 ; 
       text-tower1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       text-antenna.1-0 ; 
       text-Body_1.14-0 ROOT ; 
       text-cube1.1-0 ; 
       text-cube3.1-0 ; 
       text-garage1A.1-0 ; 
       text-garage1B.1-0 ; 
       text-garage1C.1-0 ; 
       text-garage1D.1-0 ; 
       text-garage1E.1-0 ; 
       text-L_engine_pod.1-0 ; 
       text-launch1.1-0 ; 
       text-r_engine_pod.1-0 ; 
       text-tower.1-0 ; 
       text-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/IC_Carrier/PICTURES/cap500 ; 
       I:/IC_Carrier/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-text.66-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       text-bottom.21-0 ; 
       text-intake1.19-0 ; 
       text-t2d10.4-0 ; 
       text-t2d11.2-0 ; 
       text-t2d12.3-0 ; 
       text-t2d13.3-0 ; 
       text-t2d14.3-0 ; 
       text-t2d15.3-0 ; 
       text-t2d16.11-0 ; 
       text-t2d17.11-0 ; 
       text-t2d18.1-0 ; 
       text-t2d19.1-0 ; 
       text-t2d2.17-0 ; 
       text-t2d20.4-0 ; 
       text-t2d8.4-0 ; 
       text-t2d9.4-0 ; 
       text-top.22-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       9 1 110 ; 
       11 1 110 ; 
       12 13 110 ; 
       13 1 110 ; 
       10 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 11 300 ; 
       1 15 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 14 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       9 3 300 ; 
       9 5 300 ; 
       9 12 300 ; 
       11 4 300 ; 
       11 6 300 ; 
       11 13 300 ; 
       12 17 300 ; 
       13 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 10 400 ; 
       3 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 0 401 ; 
       3 14 401 ; 
       4 5 401 ; 
       5 15 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 1 401 ; 
       11 12 401 ; 
       12 2 401 ; 
       13 7 401 ; 
       15 16 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       14 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 15 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
