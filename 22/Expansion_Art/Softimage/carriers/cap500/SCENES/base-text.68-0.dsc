SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.73-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       text-Antenna1.2-0 ; 
       text-Bottom_inside1.5-0 ; 
       text-Bottom1.12-0 ; 
       text-Bottom3.2-0 ; 
       text-Bottom4.2-0 ; 
       text-engine_glow3.2-0 ; 
       text-engine_glow4.2-0 ; 
       text-inside1.7-0 ; 
       text-intake1.8-0 ; 
       text-mat1.7-0 ; 
       text-mat2.2-0 ; 
       text-Side.7-0 ; 
       text-Side3.2-0 ; 
       text-Side4.2-0 ; 
       text-stripes1.1-0 ; 
       text-top.11-0 ; 
       text-tower_base.3-0 ; 
       text-tower1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 78     
       text-antenna.1-0 ; 
       text-Body_1.16-0 ROOT ; 
       text-cockpit.1-0 ; 
       text-cube1.1-0 ; 
       text-cube3.1-0 ; 
       text-garage1A.1-0 ; 
       text-garage1B.1-0 ; 
       text-garage1C.1-0 ; 
       text-garage1D.1-0 ; 
       text-garage1E.1-0 ; 
       text-L_engine_pod.1-0 ; 
       text-launch1.1-0 ; 
       text-r_engine_pod.1-0 ; 
       text-SS01.1-0 ; 
       text-SS10.1-0 ; 
       text-SS11.1-0 ; 
       text-SS12.1-0 ; 
       text-SS13.1-0 ; 
       text-SS14.1-0 ; 
       text-SS15.1-0 ; 
       text-SS16.1-0 ; 
       text-SS17.1-0 ; 
       text-SS18.1-0 ; 
       text-SS19.1-0 ; 
       text-SS2.1-0 ; 
       text-SS20.1-0 ; 
       text-SS21.1-0 ; 
       text-SS22.1-0 ; 
       text-SS23.1-0 ; 
       text-SS24.1-0 ; 
       text-SS25.1-0 ; 
       text-SS26.1-0 ; 
       text-SS27.1-0 ; 
       text-SS28.1-0 ; 
       text-SS29.1-0 ; 
       text-SS3.1-0 ; 
       text-SS30.1-0 ; 
       text-SS31.1-0 ; 
       text-SS32.1-0 ; 
       text-SS33.1-0 ; 
       text-SS34.1-0 ; 
       text-SS35.1-0 ; 
       text-SS36.1-0 ; 
       text-SS37.1-0 ; 
       text-SS38.1-0 ; 
       text-SS39.1-0 ; 
       text-SS4.1-0 ; 
       text-SS40.1-0 ; 
       text-SS41.1-0 ; 
       text-SS42.1-0 ; 
       text-SS43.1-0 ; 
       text-SS44.1-0 ; 
       text-SS45.1-0 ; 
       text-SS46.1-0 ; 
       text-SS47.1-0 ; 
       text-SS48.1-0 ; 
       text-SS49.1-0 ; 
       text-SS5.1-0 ; 
       text-SS50.1-0 ; 
       text-SS51.1-0 ; 
       text-SS52.1-0 ; 
       text-SS53.1-0 ; 
       text-SS54.1-0 ; 
       text-SS55.1-0 ; 
       text-SS56.1-0 ; 
       text-SS57.1-0 ; 
       text-SS58.1-0 ; 
       text-SS59.1-0 ; 
       text-SS6.1-0 ; 
       text-SS60.1-0 ; 
       text-SS61.1-0 ; 
       text-SS7.1-0 ; 
       text-SS8.1-0 ; 
       text-SS9.1-0 ; 
       text-thrust1.1-0 ; 
       text-thrust2.1-0 ; 
       text-tower.1-0 ; 
       text-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/IC_Carrier/PICTURES/cap500 ; 
       I:/IC_Carrier/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-text.68-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       text-bottom.21-0 ; 
       text-intake1.19-0 ; 
       text-t2d10.4-0 ; 
       text-t2d11.2-0 ; 
       text-t2d12.3-0 ; 
       text-t2d13.3-0 ; 
       text-t2d14.3-0 ; 
       text-t2d15.3-0 ; 
       text-t2d16.11-0 ; 
       text-t2d17.11-0 ; 
       text-t2d18.1-0 ; 
       text-t2d19.1-0 ; 
       text-t2d2.17-0 ; 
       text-t2d20.4-0 ; 
       text-t2d8.4-0 ; 
       text-t2d9.4-0 ; 
       text-top.22-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 76 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       10 1 110 ; 
       12 1 110 ; 
       76 77 110 ; 
       77 1 110 ; 
       2 1 110 ; 
       11 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       74 1 110 ; 
       75 1 110 ; 
       13 1 110 ; 
       24 1 110 ; 
       35 1 110 ; 
       46 1 110 ; 
       57 1 110 ; 
       68 1 110 ; 
       71 1 110 ; 
       72 1 110 ; 
       73 1 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       26 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       25 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       41 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       36 1 110 ; 
       40 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       42 1 110 ; 
       43 1 110 ; 
       66 1 110 ; 
       44 1 110 ; 
       45 1 110 ; 
       47 1 110 ; 
       48 1 110 ; 
       53 1 110 ; 
       49 1 110 ; 
       50 1 110 ; 
       51 1 110 ; 
       52 1 110 ; 
       54 1 110 ; 
       55 1 110 ; 
       56 1 110 ; 
       67 1 110 ; 
       58 1 110 ; 
       59 1 110 ; 
       60 1 110 ; 
       61 1 110 ; 
       69 1 110 ; 
       62 1 110 ; 
       63 1 110 ; 
       64 1 110 ; 
       65 1 110 ; 
       70 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 11 300 ; 
       1 15 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 14 300 ; 
       3 9 300 ; 
       4 10 300 ; 
       10 3 300 ; 
       10 5 300 ; 
       10 12 300 ; 
       12 4 300 ; 
       12 6 300 ; 
       12 13 300 ; 
       76 17 300 ; 
       77 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 10 400 ; 
       4 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 0 401 ; 
       3 14 401 ; 
       4 5 401 ; 
       5 15 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 1 401 ; 
       11 12 401 ; 
       12 2 401 ; 
       13 7 401 ; 
       15 16 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       14 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 95 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       76 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       77 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       11 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       74 SCHEM 32.5 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       75 SCHEM 35 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       13 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 80 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 85 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 90 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 92.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 70 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 95 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 97.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 100 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 120 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 102.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 105 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 117.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 110 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 112.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 115 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 122.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 125 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 177.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 127.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 130 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 132.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 135 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 147.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 137.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 140 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 142.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 145 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 150 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 152.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 155 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 180 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 157.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 160 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 162.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 165 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 167.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 170 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 172.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 175 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 189 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 189 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
