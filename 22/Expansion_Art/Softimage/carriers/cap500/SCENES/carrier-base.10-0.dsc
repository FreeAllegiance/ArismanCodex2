SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.99-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       base-Antenna1.2-0 ; 
       base-Bottom_inside1.2-0 ; 
       base-Bottom1.2-0 ; 
       base-Bottom3.2-0 ; 
       base-Bottom4.2-0 ; 
       base-engine_glow3.2-0 ; 
       base-engine_glow4.2-0 ; 
       base-inside1.2-0 ; 
       base-intake1.2-0 ; 
       base-mat1.2-0 ; 
       base-mat16.2-0 ; 
       base-mat2.2-0 ; 
       base-mat3.2-0 ; 
       base-mat35.2-0 ; 
       base-mat59.2-0 ; 
       base-Side.2-0 ; 
       base-Side3.2-0 ; 
       base-Side4.2-0 ; 
       base-stripes1.2-0 ; 
       base-top.2-0 ; 
       base-tower_base.2-0 ; 
       base-tower1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 83     
       base-antenna.1-0 ; 
       base-Body_1.9-0 ROOT ; 
       base-cockpit.1-0 ; 
       base-cube1.1-0 ; 
       base-cube3.1-0 ; 
       base-garage1A.1-0 ; 
       base-garage1B.1-0 ; 
       base-garage1C.1-0 ; 
       base-garage1D.1-0 ; 
       base-garage1E.1-0 ; 
       base-L_engine_pod.1-0 ; 
       base-launch1.1-0 ; 
       base-r_engine_pod.1-0 ; 
       base-SS01.1-0 ; 
       base-SS10.1-0 ; 
       base-SS11.1-0 ; 
       base-SS12.1-0 ; 
       base-SS13.1-0 ; 
       base-SS14.1-0 ; 
       base-SS15.1-0 ; 
       base-SS16.1-0 ; 
       base-SS17.1-0 ; 
       base-SS18.1-0 ; 
       base-SS19.1-0 ; 
       base-SS2.1-0 ; 
       base-SS20.1-0 ; 
       base-SS21.1-0 ; 
       base-SS22.1-0 ; 
       base-SS23.1-0 ; 
       base-SS24.1-0 ; 
       base-SS25.1-0 ; 
       base-SS26.1-0 ; 
       base-SS27.1-0 ; 
       base-SS28.1-0 ; 
       base-SS29.1-0 ; 
       base-SS3.1-0 ; 
       base-SS30.1-0 ; 
       base-SS31.1-0 ; 
       base-SS32.1-0 ; 
       base-SS33.1-0 ; 
       base-SS34.1-0 ; 
       base-SS35.1-0 ; 
       base-SS36.1-0 ; 
       base-SS37.1-0 ; 
       base-SS38.1-0 ; 
       base-SS39.1-0 ; 
       base-SS4.1-0 ; 
       base-SS40.1-0 ; 
       base-SS41.1-0 ; 
       base-SS42.1-0 ; 
       base-SS43.1-0 ; 
       base-SS44.1-0 ; 
       base-SS45.1-0 ; 
       base-SS46.1-0 ; 
       base-SS47.1-0 ; 
       base-SS48.1-0 ; 
       base-SS49.1-0 ; 
       base-SS5.1-0 ; 
       base-SS50.1-0 ; 
       base-SS51.1-0 ; 
       base-SS52.1-0 ; 
       base-SS53.1-0 ; 
       base-SS54.1-0 ; 
       base-SS55.1-0 ; 
       base-SS56.1-0 ; 
       base-SS57.1-0 ; 
       base-SS58.1-0 ; 
       base-SS59.1-0 ; 
       base-SS6.1-0 ; 
       base-SS60.1-0 ; 
       base-SS61.1-0 ; 
       base-SS7.1-0 ; 
       base-SS8.1-0 ; 
       base-SS9.1-0 ; 
       base-thrust1.1-0 ; 
       base-thrust2.1-0 ; 
       base-thrust3.1-0 ; 
       base-thrust4.1-0 ; 
       base-thrust5.1-0 ; 
       base-thrust6.1-0 ; 
       base-tower.1-0 ; 
       base-tower_base.1-0 ; 
       base-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap500/PICTURES/cap500 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap500/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       carrier-base.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       base-bottom.6-0 ; 
       base-intake1.6-0 ; 
       base-t2d10.6-0 ; 
       base-t2d11.6-0 ; 
       base-t2d12.6-0 ; 
       base-t2d13.6-0 ; 
       base-t2d14.6-0 ; 
       base-t2d15.6-0 ; 
       base-t2d16.6-0 ; 
       base-t2d17.6-0 ; 
       base-t2d18.6-0 ; 
       base-t2d19.6-0 ; 
       base-t2d2.6-0 ; 
       base-t2d20.6-0 ; 
       base-t2d8.6-0 ; 
       base-t2d9.6-0 ; 
       base-top.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       78 1 110 ; 
       79 1 110 ; 
       0 80 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       40 1 110 ; 
       41 1 110 ; 
       42 1 110 ; 
       43 1 110 ; 
       44 1 110 ; 
       45 1 110 ; 
       46 1 110 ; 
       47 1 110 ; 
       48 1 110 ; 
       49 1 110 ; 
       50 1 110 ; 
       51 1 110 ; 
       52 1 110 ; 
       53 1 110 ; 
       54 1 110 ; 
       55 1 110 ; 
       56 1 110 ; 
       57 1 110 ; 
       58 1 110 ; 
       59 1 110 ; 
       60 1 110 ; 
       61 1 110 ; 
       62 1 110 ; 
       63 1 110 ; 
       64 1 110 ; 
       65 1 110 ; 
       66 1 110 ; 
       67 1 110 ; 
       68 1 110 ; 
       69 1 110 ; 
       70 1 110 ; 
       71 1 110 ; 
       72 1 110 ; 
       73 1 110 ; 
       74 1 110 ; 
       75 1 110 ; 
       80 81 110 ; 
       81 1 110 ; 
       82 1 110 ; 
       76 1 110 ; 
       77 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 15 300 ; 
       1 19 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 18 300 ; 
       3 9 300 ; 
       4 11 300 ; 
       10 3 300 ; 
       10 5 300 ; 
       10 16 300 ; 
       12 4 300 ; 
       12 6 300 ; 
       12 17 300 ; 
       13 14 300 ; 
       14 12 300 ; 
       15 12 300 ; 
       16 10 300 ; 
       17 10 300 ; 
       18 10 300 ; 
       19 10 300 ; 
       20 10 300 ; 
       21 10 300 ; 
       22 10 300 ; 
       23 10 300 ; 
       24 12 300 ; 
       25 10 300 ; 
       26 10 300 ; 
       27 12 300 ; 
       28 12 300 ; 
       29 12 300 ; 
       30 10 300 ; 
       31 10 300 ; 
       32 10 300 ; 
       33 10 300 ; 
       34 10 300 ; 
       35 12 300 ; 
       36 10 300 ; 
       37 10 300 ; 
       38 10 300 ; 
       39 10 300 ; 
       40 13 300 ; 
       41 13 300 ; 
       42 13 300 ; 
       43 13 300 ; 
       44 13 300 ; 
       45 13 300 ; 
       46 12 300 ; 
       47 13 300 ; 
       48 13 300 ; 
       49 13 300 ; 
       50 13 300 ; 
       51 13 300 ; 
       52 13 300 ; 
       53 13 300 ; 
       54 13 300 ; 
       55 13 300 ; 
       56 13 300 ; 
       57 12 300 ; 
       58 13 300 ; 
       59 13 300 ; 
       60 13 300 ; 
       61 13 300 ; 
       62 13 300 ; 
       63 13 300 ; 
       64 13 300 ; 
       65 13 300 ; 
       66 14 300 ; 
       67 14 300 ; 
       68 12 300 ; 
       69 14 300 ; 
       70 14 300 ; 
       71 12 300 ; 
       72 12 300 ; 
       73 12 300 ; 
       80 21 300 ; 
       81 20 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 10 400 ; 
       4 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 0 401 ; 
       3 14 401 ; 
       4 5 401 ; 
       5 15 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 1 401 ; 
       15 12 401 ; 
       16 2 401 ; 
       17 7 401 ; 
       18 13 401 ; 
       19 16 401 ; 
       20 3 401 ; 
       21 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       78 SCHEM 42.5 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       79 SCHEM 45 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 101.25 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.01478578 0.01493197 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 200 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 80 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 102.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 105 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 100 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 95 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 85 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 90 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 77.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 107.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 110 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 112.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 115 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 117.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 120 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 122.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 125 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 127.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 130 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 132.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 135 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 137.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 140 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 142.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 145 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 147.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 150 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 152.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 155 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 157.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 160 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 162.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 165 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 167.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 170 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 172.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 175 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 177.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 180 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 182.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 185 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 187.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 192.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 195 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 197.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 72.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 32.5 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       75 SCHEM 35 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       80 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       81 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       82 SCHEM 47.5 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       76 SCHEM 37.5 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
       77 SCHEM 40 -2 0 WIRECOL 8 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 120.7471 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 70.74709 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 180.7471 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 199 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 201.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 201.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
