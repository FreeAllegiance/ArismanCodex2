SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.76-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       static-Antenna1.1-0 ; 
       static-Bottom_inside1.1-0 ; 
       static-Bottom1.1-0 ; 
       static-Bottom3.1-0 ; 
       static-Bottom4.1-0 ; 
       static-engine_glow3.1-0 ; 
       static-engine_glow4.1-0 ; 
       static-inside1.1-0 ; 
       static-intake1.1-0 ; 
       static-mat1.1-0 ; 
       static-mat2.1-0 ; 
       static-Side.1-0 ; 
       static-Side3.1-0 ; 
       static-Side4.1-0 ; 
       static-stripes1.1-0 ; 
       static-top.1-0 ; 
       static-tower_base.1-0 ; 
       static-tower1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       static-antenna.1-0 ; 
       static-Body_1.1-0 ROOT ; 
       static-cube1.1-0 ; 
       static-cube3.1-0 ; 
       static-L_engine_pod.1-0 ; 
       static-r_engine_pod.1-0 ; 
       static-tower.1-0 ; 
       static-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/IC_Carrier/PICTURES/cap500 ; 
       I:/IC_Carrier/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       carrier-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       static-bottom.1-0 ; 
       static-intake1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d19.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
       static-top.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 11 300 ; 
       1 15 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       1 14 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       4 3 300 ; 
       4 5 300 ; 
       4 12 300 ; 
       5 4 300 ; 
       5 6 300 ; 
       5 13 300 ; 
       6 17 300 ; 
       7 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 10 400 ; 
       3 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 0 401 ; 
       3 14 401 ; 
       4 5 401 ; 
       5 15 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 1 401 ; 
       11 12 401 ; 
       12 2 401 ; 
       13 7 401 ; 
       15 16 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       14 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 -2.384186e-007 0.2449718 1.309875 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
