SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.94-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       static-Bottom_inside1.3-0 ; 
       static-Bottom1.3-0 ; 
       static-Bottom3.3-0 ; 
       static-Bottom4.3-0 ; 
       static-engine_glow3.3-0 ; 
       static-engine_glow4.3-0 ; 
       static-inside1.3-0 ; 
       static-intake1.3-0 ; 
       static-mat1.3-0 ; 
       static-mat2.3-0 ; 
       static-Side.3-0 ; 
       static-Side3.3-0 ; 
       static-Side4.3-0 ; 
       static-stripes1.3-0 ; 
       static-top.3-0 ; 
       static-tower_base.3-0 ; 
       static-tower1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       static-Body_1.4-0 ROOT ; 
       static-cube1.1-0 ; 
       static-cube3.1-0 ; 
       static-L_engine_pod.1-0 ; 
       static-r_engine_pod.1-0 ; 
       static-tower.1-0 ; 
       static-tower_base.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap500/PICTURES/cap500 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap500/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       carrier-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       static-bottom.4-0 ; 
       static-intake1.4-0 ; 
       static-t2d10.4-0 ; 
       static-t2d11.4-0 ; 
       static-t2d12.4-0 ; 
       static-t2d13.4-0 ; 
       static-t2d14.4-0 ; 
       static-t2d15.4-0 ; 
       static-t2d16.4-0 ; 
       static-t2d17.4-0 ; 
       static-t2d18.4-0 ; 
       static-t2d19.4-0 ; 
       static-t2d2.4-0 ; 
       static-t2d20.4-0 ; 
       static-t2d8.4-0 ; 
       static-t2d9.4-0 ; 
       static-top.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 6 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 10 300 ; 
       0 14 300 ; 
       0 1 300 ; 
       0 7 300 ; 
       0 6 300 ; 
       0 0 300 ; 
       0 13 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       3 2 300 ; 
       3 4 300 ; 
       3 11 300 ; 
       4 3 300 ; 
       4 5 300 ; 
       4 12 300 ; 
       5 16 300 ; 
       6 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 10 400 ; 
       2 11 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 0 401 ; 
       2 14 401 ; 
       3 5 401 ; 
       4 15 401 ; 
       5 6 401 ; 
       6 8 401 ; 
       7 1 401 ; 
       10 12 401 ; 
       11 2 401 ; 
       12 7 401 ; 
       13 13 401 ; 
       14 16 401 ; 
       15 3 401 ; 
       16 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 -2.384186e-007 0.01478578 0.01493197 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
