SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.31-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       belters_carrier-mat10.1-0 ; 
       belters_carrier-mat11.1-0 ; 
       belters_carrier-mat12.1-0 ; 
       belters_carrier-mat15.1-0 ; 
       belters_carrier-mat18.1-0 ; 
       belters_carrier-mat19.1-0 ; 
       belters_carrier-mat20.1-0 ; 
       belters_carrier-mat21.1-0 ; 
       belters_carrier-mat22.2-0 ; 
       belters_carrier-mat23.2-0 ; 
       belters_carrier-mat24.1-0 ; 
       belters_carrier-mat3.4-0 ; 
       belters_carrier-mat4.2-0 ; 
       belters_carrier-mat5.1-0 ; 
       belters_carrier-mat6.1-0 ; 
       belters_carrier-mat7.1-0 ; 
       belters_carrier-mat8.1-0 ; 
       belters_carrier-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       3-cube1.1-0 ; 
       3-cube1_1.2-0 ; 
       3-cube1_2.1-0 ; 
       3-cube1_3.1-0 ; 
       3-cube2.3-0 ; 
       3-engine_shpere1.1-0 ; 
       3-engine_shpere1_1.1-0 ; 
       3-engine_shpere1_2.1-0 ; 
       3-engine_shpere1_3.1-0 ; 
       3-nozzle1_1.2-0 ; 
       3-nozzle1_3.1-0 ; 
       3-null1.1-0 ; 
       3-skin2.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/beltersbay ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/cap501 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap501-belters_carrier.32-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       belters_carrier-t2d10.1-0 ; 
       belters_carrier-t2d11.1-0 ; 
       belters_carrier-t2d12.1-0 ; 
       belters_carrier-t2d15.1-0 ; 
       belters_carrier-t2d18.1-0 ; 
       belters_carrier-t2d19.1-0 ; 
       belters_carrier-t2d20.1-0 ; 
       belters_carrier-t2d21.1-0 ; 
       belters_carrier-t2d22.2-0 ; 
       belters_carrier-t2d23.2-0 ; 
       belters_carrier-t2d24.1-0 ; 
       belters_carrier-t2d3.10-0 ; 
       belters_carrier-t2d4.5-0 ; 
       belters_carrier-t2d5.3-0 ; 
       belters_carrier-t2d6.2-0 ; 
       belters_carrier-t2d7.3-0 ; 
       belters_carrier-t2d8.3-0 ; 
       belters_carrier-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 1 110 ; 
       6 3 110 ; 
       10 6 110 ; 
       7 0 110 ; 
       9 5 110 ; 
       8 2 110 ; 
       11 12 110 ; 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       4 12 110 ; 
       3 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 14 300 ; 
       6 17 300 ; 
       10 0 300 ; 
       10 1 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       12 13 300 ; 
       7 2 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       8 3 300 ; 
       0 6 300 ; 
       1 4 300 ; 
       2 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       3 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 15 401 ; 
       17 17 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 15 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 23.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 13.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 28.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 35 -2 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 40 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       11 SCHEM 40 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 45 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 15 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 25 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
