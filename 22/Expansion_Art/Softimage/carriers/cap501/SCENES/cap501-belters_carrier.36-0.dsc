SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.35-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       belters_carrier-mat10.1-0 ; 
       belters_carrier-mat11.1-0 ; 
       belters_carrier-mat12.1-0 ; 
       belters_carrier-mat15.1-0 ; 
       belters_carrier-mat18.1-0 ; 
       belters_carrier-mat19.1-0 ; 
       belters_carrier-mat20.1-0 ; 
       belters_carrier-mat21.1-0 ; 
       belters_carrier-mat22.2-0 ; 
       belters_carrier-mat23.2-0 ; 
       belters_carrier-mat24.1-0 ; 
       belters_carrier-mat25.1-0 ; 
       belters_carrier-mat26.1-0 ; 
       belters_carrier-mat27.1-0 ; 
       belters_carrier-mat28.1-0 ; 
       belters_carrier-mat29.1-0 ; 
       belters_carrier-mat3.4-0 ; 
       belters_carrier-mat30.1-0 ; 
       belters_carrier-mat31.1-0 ; 
       belters_carrier-mat32.1-0 ; 
       belters_carrier-mat4.2-0 ; 
       belters_carrier-mat5.1-0 ; 
       belters_carrier-mat6.1-0 ; 
       belters_carrier-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       3-cube1.1-0 ; 
       3-cube1_1.2-0 ; 
       3-cube1_2.1-0 ; 
       3-cube1_3.1-0 ; 
       3-cube2.3-0 ; 
       3-cube3.1-0 ; 
       3-cube4.1-0 ; 
       3-engine_shpere1.1-0 ; 
       3-engine_shpere1_1.1-0 ; 
       3-engine_shpere1_2.1-0 ; 
       3-engine_shpere1_3.1-0 ; 
       3-nozzle1_3.1-0 ; 
       3-nozzle1_4.1-0 ; 
       3-nozzle1_5.1-0 ; 
       3-nozzle1_6.1-0 ; 
       3-null1.1-0 ; 
       3-skin2.18-0 ROOT ; 
       3-sphere2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/beltersbay ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/cap501 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap501-belters_carrier.36-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       belters_carrier-t2d10.2-0 ; 
       belters_carrier-t2d11.2-0 ; 
       belters_carrier-t2d12.1-0 ; 
       belters_carrier-t2d15.1-0 ; 
       belters_carrier-t2d18.1-0 ; 
       belters_carrier-t2d19.1-0 ; 
       belters_carrier-t2d20.1-0 ; 
       belters_carrier-t2d21.1-0 ; 
       belters_carrier-t2d22.2-0 ; 
       belters_carrier-t2d23.2-0 ; 
       belters_carrier-t2d24.1-0 ; 
       belters_carrier-t2d25.1-0 ; 
       belters_carrier-t2d26.1-0 ; 
       belters_carrier-t2d27.1-0 ; 
       belters_carrier-t2d28.1-0 ; 
       belters_carrier-t2d29.1-0 ; 
       belters_carrier-t2d3.10-0 ; 
       belters_carrier-t2d30.1-0 ; 
       belters_carrier-t2d31.1-0 ; 
       belters_carrier-t2d32.1-0 ; 
       belters_carrier-t2d4.5-0 ; 
       belters_carrier-t2d5.3-0 ; 
       belters_carrier-t2d6.2-0 ; 
       belters_carrier-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 15 110 ; 
       4 16 110 ; 
       7 1 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 2 110 ; 
       11 8 110 ; 
       12 7 110 ; 
       13 10 110 ; 
       14 9 110 ; 
       15 16 110 ; 
       5 16 110 ; 
       17 5 110 ; 
       6 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       1 4 300 ; 
       2 7 300 ; 
       3 5 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       7 22 300 ; 
       8 23 300 ; 
       9 2 300 ; 
       10 3 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       13 13 300 ; 
       13 14 300 ; 
       14 15 300 ; 
       14 17 300 ; 
       16 16 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       5 18 300 ; 
       17 19 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       18 18 401 ; 
       19 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 45 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       16 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 50 -4 0 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
