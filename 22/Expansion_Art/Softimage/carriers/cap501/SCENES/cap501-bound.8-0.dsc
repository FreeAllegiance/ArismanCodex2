SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.58-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       cap501-bound01.1-0 ; 
       cap501-bound02.1-0 ; 
       cap501-bound03.1-0 ; 
       cap501-bound05.1-0 ; 
       cap501-bound06.1-0 ; 
       cap501-bound07.1-0 ; 
       cap501-bound4.1-0 ; 
       cap501-bounding_model.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap501-bound.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 0 0 DISPLAY 3 2 SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
