SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.62-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       new_back-mat10.1-0 ; 
       new_back-mat11.1-0 ; 
       new_back-mat12.1-0 ; 
       new_back-mat15.1-0 ; 
       new_back-mat16.1-0 ; 
       new_back-mat18.1-0 ; 
       new_back-mat19.1-0 ; 
       new_back-mat20.1-0 ; 
       new_back-mat21.1-0 ; 
       new_back-mat22.1-0 ; 
       new_back-mat23.1-0 ; 
       new_back-mat24.1-0 ; 
       new_back-mat25.1-0 ; 
       new_back-mat26.1-0 ; 
       new_back-mat27.1-0 ; 
       new_back-mat28.1-0 ; 
       new_back-mat29.1-0 ; 
       new_back-mat3.1-0 ; 
       new_back-mat3_2.1-0 ; 
       new_back-mat3_3.3-0 ; 
       new_back-mat30.1-0 ; 
       new_back-mat31.1-0 ; 
       new_back-mat32.1-0 ; 
       new_back-mat33.1-0 ; 
       new_back-mat34.1-0 ; 
       new_back-mat35.1-0 ; 
       new_back-mat36.1-0 ; 
       new_back-mat37.1-0 ; 
       new_back-mat38.1-0 ; 
       new_back-mat39.1-0 ; 
       new_back-mat40.1-0 ; 
       new_back-mat41.1-0 ; 
       new_back-mat42.1-0 ; 
       new_back-mat43.1-0 ; 
       new_back-mat44.1-0 ; 
       new_back-mat45.1-0 ; 
       new_back-mat46.1-0 ; 
       new_back-mat47.1-0 ; 
       new_back-mat48.1-0 ; 
       new_back-mat49.1-0 ; 
       new_back-mat50.1-0 ; 
       new_back-mat51.1-0 ; 
       new_back-mat52.1-0 ; 
       new_back-mat53.1-0 ; 
       new_back-mat54.1-0 ; 
       new_back-mat55.1-0 ; 
       new_back-mat56.1-0 ; 
       new_back-mat57.1-0 ; 
       new_back-mat58.1-0 ; 
       new_back-mat59.1-0 ; 
       new_back-mat6.1-0 ; 
       new_back-mat60.1-0 ; 
       new_back-mat61.1-0 ; 
       new_back-mat64.3-0 ; 
       new_back-mat65.3-0 ; 
       new_back-mat66.1-0 ; 
       new_back-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 62     
       Cap501-cockpt.1-0 ; 
       Cap501-cube1.1-0 ; 
       Cap501-cube1_1.2-0 ; 
       Cap501-cube1_2.1-0 ; 
       Cap501-cube1_3.1-0 ; 
       Cap501-cube2.3-0 ; 
       Cap501-cube3.1-0 ; 
       Cap501-cube4.1-0 ; 
       Cap501-cube5.1-0 ; 
       Cap501-engine_shpere1.1-0 ; 
       Cap501-engine_shpere1_1.1-0 ; 
       Cap501-engine_shpere1_2.1-0 ; 
       Cap501-engine_shpere1_3.1-0 ; 
       Cap501-garage1A.1-0 ; 
       Cap501-garage1B.1-0 ; 
       Cap501-garage1C.1-0 ; 
       Cap501-garage1D.1-0 ; 
       Cap501-garage1E.1-0 ; 
       Cap501-launch.1-0 ; 
       Cap501-nozzle1_3.1-0 ; 
       Cap501-nozzle1_4.1-0 ; 
       Cap501-nozzle1_5.1-0 ; 
       Cap501-nozzle1_6.1-0 ; 
       Cap501-null1.1-0 ; 
       Cap501-root.5-0 ROOT ; 
       Cap501-skin2.1-0 ; 
       Cap501-skin2_1.1-0 ; 
       Cap501-sphere2.1-0 ; 
       Cap501-SS10.1-0 ; 
       Cap501-SS13.1-0 ; 
       Cap501-SS13_1.1-0 ; 
       Cap501-SS13_10.1-0 ; 
       Cap501-SS13_11.1-0 ; 
       Cap501-SS13_12.1-0 ; 
       Cap501-SS13_2.1-0 ; 
       Cap501-SS13_3.1-0 ; 
       Cap501-SS13_4.1-0 ; 
       Cap501-SS13_5.1-0 ; 
       Cap501-SS13_6.1-0 ; 
       Cap501-SS13_7.1-0 ; 
       Cap501-SS13_8.1-0 ; 
       Cap501-SS13_9.1-0 ; 
       Cap501-SS43.1-0 ; 
       Cap501-SS43_1.1-0 ; 
       Cap501-SS43_2.1-0 ; 
       Cap501-SS43_3.1-0 ; 
       Cap501-SS43_4.1-0 ; 
       Cap501-SS43_5.1-0 ; 
       Cap501-SS43_6.1-0 ; 
       Cap501-SS44.1-0 ; 
       Cap501-SS45.1-0 ; 
       Cap501-SS46.1-0 ; 
       Cap501-SS47.1-0 ; 
       Cap501-SS48.1-0 ; 
       Cap501-SS49.1-0 ; 
       Cap501-SS50.1-0 ; 
       Cap501-SS51.1-0 ; 
       Cap501-thrust1.1-0 ; 
       Cap501-thrust2.1-0 ; 
       Cap501-thrust3.1-0 ; 
       Cap501-thrust4.1-0 ; 
       Cap501-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/beltersbay ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/cap501 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap501-new_back.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       new_back-t2d10.1-0 ; 
       new_back-t2d11.1-0 ; 
       new_back-t2d12.1-0 ; 
       new_back-t2d15.1-0 ; 
       new_back-t2d18.1-0 ; 
       new_back-t2d19.1-0 ; 
       new_back-t2d20.1-0 ; 
       new_back-t2d21.1-0 ; 
       new_back-t2d22.1-0 ; 
       new_back-t2d23.1-0 ; 
       new_back-t2d24.1-0 ; 
       new_back-t2d25.1-0 ; 
       new_back-t2d26.1-0 ; 
       new_back-t2d27.1-0 ; 
       new_back-t2d28.1-0 ; 
       new_back-t2d29.1-0 ; 
       new_back-t2d30.1-0 ; 
       new_back-t2d31.1-0 ; 
       new_back-t2d32.1-0 ; 
       new_back-t2d33.1-0 ; 
       new_back-t2d34.1-0 ; 
       new_back-t2d38.3-0 ; 
       new_back-t2d39.3-0 ; 
       new_back-t2d40.3-0 ; 
       new_back-t2d41.1-0 ; 
       new_back-t2d42.1-0 ; 
       new_back-t2d6.1-0 ; 
       new_back-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 23 110 ; 
       2 23 110 ; 
       3 23 110 ; 
       4 23 110 ; 
       5 24 110 ; 
       6 24 110 ; 
       7 24 110 ; 
       8 24 110 ; 
       9 2 110 ; 
       10 4 110 ; 
       11 1 110 ; 
       12 3 110 ; 
       13 24 110 ; 
       14 24 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 10 110 ; 
       20 9 110 ; 
       21 12 110 ; 
       22 11 110 ; 
       23 24 110 ; 
       25 24 110 ; 
       26 25 110 ; 
       27 6 110 ; 
       28 24 110 ; 
       29 24 110 ; 
       30 24 110 ; 
       31 24 110 ; 
       32 24 110 ; 
       33 24 110 ; 
       34 24 110 ; 
       35 24 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 24 110 ; 
       39 24 110 ; 
       40 24 110 ; 
       41 24 110 ; 
       42 24 110 ; 
       43 24 110 ; 
       44 24 110 ; 
       45 24 110 ; 
       46 24 110 ; 
       47 24 110 ; 
       48 24 110 ; 
       49 24 110 ; 
       50 24 110 ; 
       51 24 110 ; 
       52 24 110 ; 
       53 24 110 ; 
       54 24 110 ; 
       55 24 110 ; 
       56 24 110 ; 
       57 24 110 ; 
       58 24 110 ; 
       59 24 110 ; 
       60 24 110 ; 
       61 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       2 5 300 ; 
       3 8 300 ; 
       4 6 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       6 21 300 ; 
       7 23 300 ; 
       8 24 300 ; 
       9 50 300 ; 
       10 56 300 ; 
       11 2 300 ; 
       12 3 300 ; 
       19 0 300 ; 
       19 1 300 ; 
       20 12 300 ; 
       20 13 300 ; 
       21 14 300 ; 
       21 15 300 ; 
       22 16 300 ; 
       22 20 300 ; 
       25 19 300 ; 
       25 53 300 ; 
       25 54 300 ; 
       25 55 300 ; 
       26 18 300 ; 
       27 22 300 ; 
       28 17 300 ; 
       29 4 300 ; 
       30 34 300 ; 
       31 43 300 ; 
       32 44 300 ; 
       33 45 300 ; 
       34 35 300 ; 
       35 36 300 ; 
       36 37 300 ; 
       37 38 300 ; 
       38 39 300 ; 
       39 40 300 ; 
       40 41 300 ; 
       41 42 300 ; 
       42 25 300 ; 
       43 48 300 ; 
       44 46 300 ; 
       45 47 300 ; 
       46 49 300 ; 
       47 51 300 ; 
       48 52 300 ; 
       49 26 300 ; 
       50 27 300 ; 
       51 28 300 ; 
       52 29 300 ; 
       53 30 300 ; 
       54 31 300 ; 
       55 32 300 ; 
       56 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       18 24 401 ; 
       19 21 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       50 26 401 ; 
       53 22 401 ; 
       54 23 401 ; 
       56 27 401 ; 
       55 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 MPRFLG 0 ; 
       6 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 55 -2 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -6 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       13 SCHEM 77.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 80 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 82.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 85 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 75 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       21 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       23 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       24 SCHEM 86.25 0 0 SRT 1 1 1 0 0 0 2.834778e-008 0 -8.09988e-008 MPRFLG 0 ; 
       25 SCHEM 166.25 -2 0 MPRFLG 0 ; 
       26 SCHEM 162.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 50 -4 0 MPRFLG 0 ; 
       28 SCHEM 107.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 140 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 112.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 147.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 130 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 135 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 115 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 117.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 120 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 122.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 127.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 132.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 137.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 142.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 125 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 152.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 145 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 150 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 155 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 157.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 160 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 90 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 92.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 95 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 97.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 100 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 102.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 105 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 110 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 139 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 106.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 124 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 89 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 91.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 94 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 96.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 99 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 101.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 104 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 109 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 111.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 114 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 116.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 119 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 121.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 126.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 131.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 136.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 141.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 146.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 129 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 134 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 144 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 149 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 151.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 154 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 156.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 159 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 172.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 175 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
