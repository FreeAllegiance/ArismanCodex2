SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.64-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       static-mat10.3-0 ; 
       static-mat11.3-0 ; 
       static-mat12.3-0 ; 
       static-mat15.3-0 ; 
       static-mat18.3-0 ; 
       static-mat19.3-0 ; 
       static-mat20.3-0 ; 
       static-mat21.3-0 ; 
       static-mat22.3-0 ; 
       static-mat23.3-0 ; 
       static-mat24.3-0 ; 
       static-mat25.3-0 ; 
       static-mat26.3-0 ; 
       static-mat27.3-0 ; 
       static-mat28.3-0 ; 
       static-mat29.3-0 ; 
       static-mat3_2.2-0 ; 
       static-mat3_3.2-0 ; 
       static-mat30.3-0 ; 
       static-mat6.3-0 ; 
       static-mat64.2-0 ; 
       static-mat65.2-0 ; 
       static-mat66.1-0 ; 
       static-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       Cap501-cube1.1-0 ; 
       Cap501-cube1_1.2-0 ; 
       Cap501-cube1_2.1-0 ; 
       Cap501-cube1_3.1-0 ; 
       Cap501-cube2.3-0 ; 
       Cap501-engine_shpere1.1-0 ; 
       Cap501-engine_shpere1_1.1-0 ; 
       Cap501-engine_shpere1_2.1-0 ; 
       Cap501-engine_shpere1_3.1-0 ; 
       Cap501-nozzle1_3.1-0 ; 
       Cap501-nozzle1_4.1-0 ; 
       Cap501-nozzle1_5.1-0 ; 
       Cap501-nozzle1_6.1-0 ; 
       Cap501-null1.1-0 ; 
       Cap501-root.6-0 ROOT ; 
       Cap501-skin2.1-0 ; 
       Cap501-skin2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/beltersbay ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap501/PICTURES/cap501 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap501-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       static-t2d10.3-0 ; 
       static-t2d11.3-0 ; 
       static-t2d12.3-0 ; 
       static-t2d15.3-0 ; 
       static-t2d18.3-0 ; 
       static-t2d19.3-0 ; 
       static-t2d20.3-0 ; 
       static-t2d21.3-0 ; 
       static-t2d22.3-0 ; 
       static-t2d23.3-0 ; 
       static-t2d24.3-0 ; 
       static-t2d25.3-0 ; 
       static-t2d26.3-0 ; 
       static-t2d27.3-0 ; 
       static-t2d28.3-0 ; 
       static-t2d29.3-0 ; 
       static-t2d30.3-0 ; 
       static-t2d38.2-0 ; 
       static-t2d39.2-0 ; 
       static-t2d40.2-0 ; 
       static-t2d41.2-0 ; 
       static-t2d42.1-0 ; 
       static-t2d6.3-0 ; 
       static-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 14 110 ; 
       5 1 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 2 110 ; 
       9 6 110 ; 
       10 5 110 ; 
       11 8 110 ; 
       12 7 110 ; 
       13 14 110 ; 
       15 14 110 ; 
       16 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       1 4 300 ; 
       2 7 300 ; 
       3 5 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       5 19 300 ; 
       6 23 300 ; 
       7 2 300 ; 
       8 3 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       12 15 300 ; 
       12 18 300 ; 
       15 17 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       15 22 300 ; 
       16 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 20 401 ; 
       17 17 401 ; 
       18 16 401 ; 
       19 22 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       23 23 401 ; 
       22 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 MPRFLG 0 ; 
       13 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 2.834778e-008 0 -8.09988e-008 MPRFLG 0 ; 
       15 SCHEM 15 -2 0 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
