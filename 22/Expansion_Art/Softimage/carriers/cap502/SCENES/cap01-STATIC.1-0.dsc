SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.1-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 79     
       debug_turr_prob-mat100.1-0 ; 
       debug_turr_prob-mat101.1-0 ; 
       debug_turr_prob-mat103.1-0 ; 
       debug_turr_prob-mat104.1-0 ; 
       debug_turr_prob-mat105.1-0 ; 
       debug_turr_prob-mat106.1-0 ; 
       debug_turr_prob-mat107.1-0 ; 
       debug_turr_prob-mat108.1-0 ; 
       debug_turr_prob-mat115.1-0 ; 
       debug_turr_prob-mat116.1-0 ; 
       debug_turr_prob-mat117.1-0 ; 
       debug_turr_prob-mat118.1-0 ; 
       debug_turr_prob-mat119.1-0 ; 
       debug_turr_prob-mat120.1-0 ; 
       debug_turr_prob-mat121.1-0 ; 
       debug_turr_prob-mat122.1-0 ; 
       debug_turr_prob-mat123.1-0 ; 
       debug_turr_prob-mat124.1-0 ; 
       debug_turr_prob-mat125.1-0 ; 
       debug_turr_prob-mat126.1-0 ; 
       debug_turr_prob-mat98.1-0 ; 
       debug_turr_prob-mat99.1-0 ; 
       kez_frigate_F-mat1.1-0 ; 
       kez_frigate_F-mat10.1-0 ; 
       kez_frigate_F-mat11.1-0 ; 
       kez_frigate_F-mat12.1-0 ; 
       kez_frigate_F-mat13.1-0 ; 
       kez_frigate_F-mat14.1-0 ; 
       kez_frigate_F-mat15.1-0 ; 
       kez_frigate_F-mat17.1-0 ; 
       kez_frigate_F-mat18.1-0 ; 
       kez_frigate_F-mat19.1-0 ; 
       kez_frigate_F-mat2.1-0 ; 
       kez_frigate_F-mat20.1-0 ; 
       kez_frigate_F-mat21.1-0 ; 
       kez_frigate_F-mat22.1-0 ; 
       kez_frigate_F-mat23.1-0 ; 
       kez_frigate_F-mat24.1-0 ; 
       kez_frigate_F-mat25.1-0 ; 
       kez_frigate_F-mat26.1-0 ; 
       kez_frigate_F-mat28.1-0 ; 
       kez_frigate_F-mat3.1-0 ; 
       kez_frigate_F-mat30.1-0 ; 
       kez_frigate_F-mat32.1-0 ; 
       kez_frigate_F-mat33.1-0 ; 
       kez_frigate_F-mat34.1-0 ; 
       kez_frigate_F-mat35.1-0 ; 
       kez_frigate_F-mat36.1-0 ; 
       kez_frigate_F-mat37.1-0 ; 
       kez_frigate_F-mat38.1-0 ; 
       kez_frigate_F-mat39.1-0 ; 
       kez_frigate_F-mat4.1-0 ; 
       kez_frigate_F-mat40.1-0 ; 
       kez_frigate_F-mat41.1-0 ; 
       kez_frigate_F-mat43.1-0 ; 
       kez_frigate_F-mat44.1-0 ; 
       kez_frigate_F-mat45.1-0 ; 
       kez_frigate_F-mat46.1-0 ; 
       kez_frigate_F-mat48.1-0 ; 
       kez_frigate_F-mat49.1-0 ; 
       kez_frigate_F-mat5.1-0 ; 
       kez_frigate_F-mat50.1-0 ; 
       kez_frigate_F-mat51.1-0 ; 
       kez_frigate_F-mat52.1-0 ; 
       kez_frigate_F-mat53.1-0 ; 
       kez_frigate_F-mat6.1-0 ; 
       kez_frigate_F-mat60.1-0 ; 
       kez_frigate_F-mat66.1-0 ; 
       kez_frigate_F-mat69.1-0 ; 
       kez_frigate_F-mat7.1-0 ; 
       kez_frigate_F-mat70.1-0 ; 
       kez_frigate_F-mat71.1-0 ; 
       kez_frigate_F-mat72.1-0 ; 
       kez_frigate_F-mat74.1-0 ; 
       kez_frigate_F-mat8.1-0 ; 
       kez_frigate_F-mat9.1-0 ; 
       kez_frigate_F-mat95.1-0 ; 
       kez_frigate_F-port_red-left.1-0.1-0 ; 
       kez_frigate_F-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01_2.1-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-slrsal0.1-0 ROOT ; 
       cap01-SSal.1-0 ; 
       cap01-SSar.1-0 ; 
       cap01-SSat.1-0 ; 
       cap01-turloc1.1-0 ; 
       cap01-turloc3.1-0 ; 
       cap01-turloc4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/xfiles/Carriers/cap502/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 67     
       debug_turr_prob-t2d100.1-0 ; 
       debug_turr_prob-t2d101.1-0 ; 
       debug_turr_prob-t2d102.1-0 ; 
       debug_turr_prob-t2d79.1-0 ; 
       debug_turr_prob-t2d80.1-0 ; 
       debug_turr_prob-t2d81.1-0 ; 
       debug_turr_prob-t2d82.1-0 ; 
       debug_turr_prob-t2d83.1-0 ; 
       debug_turr_prob-t2d84.1-0 ; 
       debug_turr_prob-t2d85.1-0 ; 
       debug_turr_prob-t2d86.1-0 ; 
       debug_turr_prob-t2d87.1-0 ; 
       debug_turr_prob-t2d93.1-0 ; 
       debug_turr_prob-t2d94.1-0 ; 
       debug_turr_prob-t2d95.1-0 ; 
       debug_turr_prob-t2d96.1-0 ; 
       debug_turr_prob-t2d97.1-0 ; 
       debug_turr_prob-t2d98.1-0 ; 
       debug_turr_prob-t2d99.1-0 ; 
       kez_frigate_F-t2d12.1-0 ; 
       kez_frigate_F-t2d14.1-0 ; 
       kez_frigate_F-t2d15.1-0 ; 
       kez_frigate_F-t2d16.1-0 ; 
       kez_frigate_F-t2d17.1-0 ; 
       kez_frigate_F-t2d18.1-0 ; 
       kez_frigate_F-t2d19.1-0 ; 
       kez_frigate_F-t2d2.1-0 ; 
       kez_frigate_F-t2d20.1-0 ; 
       kez_frigate_F-t2d21.1-0 ; 
       kez_frigate_F-t2d22.1-0 ; 
       kez_frigate_F-t2d24.1-0 ; 
       kez_frigate_F-t2d26.1-0 ; 
       kez_frigate_F-t2d27.1-0 ; 
       kez_frigate_F-t2d28.1-0 ; 
       kez_frigate_F-t2d29.1-0 ; 
       kez_frigate_F-t2d30.1-0 ; 
       kez_frigate_F-t2d31.1-0 ; 
       kez_frigate_F-t2d32.1-0 ; 
       kez_frigate_F-t2d33.1-0 ; 
       kez_frigate_F-t2d34.1-0 ; 
       kez_frigate_F-t2d35.1-0 ; 
       kez_frigate_F-t2d36.1-0 ; 
       kez_frigate_F-t2d37.1-0 ; 
       kez_frigate_F-t2d38.1-0 ; 
       kez_frigate_F-t2d4.1-0 ; 
       kez_frigate_F-t2d40.1-0 ; 
       kez_frigate_F-t2d41.1-0 ; 
       kez_frigate_F-t2d42.1-0 ; 
       kez_frigate_F-t2d43.1-0 ; 
       kez_frigate_F-t2d44.1-0 ; 
       kez_frigate_F-t2d45.1-0 ; 
       kez_frigate_F-t2d55.1-0 ; 
       kez_frigate_F-t2d58.1-0 ; 
       kez_frigate_F-t2d59.1-0 ; 
       kez_frigate_F-t2d60.1-0 ; 
       kez_frigate_F-t2d61.1-0 ; 
       kez_frigate_F-t2d62.1-0 ; 
       kez_frigate_F-t2d7.1-0 ; 
       kez_frigate_F-t2d70.1-0 ; 
       kez_frigate_F-t2d71.1-0 ; 
       kez_frigate_F-t2d72.1-0 ; 
       kez_frigate_F-t2d73.1-0 ; 
       kez_frigate_F-t2d74.1-0 ; 
       kez_frigate_F-t2d75.1-0 ; 
       kez_frigate_F-t2d76.1-0 ; 
       kez_frigate_F-t2d8.1-0 ; 
       kez_frigate_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 21 110 ; 
       8 21 110 ; 
       9 21 110 ; 
       10 21 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 3 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 2 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 52 300 ; 
       0 53 300 ; 
       0 54 300 ; 
       0 55 300 ; 
       0 56 300 ; 
       0 57 300 ; 
       0 58 300 ; 
       0 59 300 ; 
       0 61 300 ; 
       0 62 300 ; 
       0 63 300 ; 
       0 64 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       1 43 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 21 300 ; 
       2 46 300 ; 
       2 47 300 ; 
       2 48 300 ; 
       2 49 300 ; 
       2 50 300 ; 
       2 20 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       4 42 300 ; 
       5 31 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       7 65 300 ; 
       8 60 300 ; 
       9 51 300 ; 
       10 41 300 ; 
       11 32 300 ; 
       12 22 300 ; 
       13 66 300 ; 
       13 67 300 ; 
       13 68 300 ; 
       13 70 300 ; 
       13 71 300 ; 
       13 72 300 ; 
       13 73 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       14 26 300 ; 
       15 69 300 ; 
       16 74 300 ; 
       17 75 300 ; 
       18 23 300 ; 
       19 24 300 ; 
       20 25 300 ; 
       22 77 300 ; 
       23 78 300 ; 
       24 76 300 ; 
       25 2 300 ; 
       25 3 300 ; 
       25 4 300 ; 
       25 5 300 ; 
       25 6 300 ; 
       25 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       26 10 300 ; 
       26 11 300 ; 
       26 12 300 ; 
       26 13 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       27 16 300 ; 
       27 17 300 ; 
       27 18 300 ; 
       27 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 23 400 ; 
       9 44 400 ; 
       11 26 400 ; 
       14 57 400 ; 
       15 65 400 ; 
       16 66 400 ; 
       19 19 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       3 7 401 ; 
       4 8 401 ; 
       5 9 401 ; 
       6 10 401 ; 
       7 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 0 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 64 401 ; 
       23 59 401 ; 
       25 58 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       40 30 401 ; 
       41 63 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       53 39 401 ; 
       54 40 401 ; 
       55 41 401 ; 
       56 42 401 ; 
       57 43 401 ; 
       58 45 401 ; 
       59 46 401 ; 
       60 62 401 ; 
       61 47 401 ; 
       62 48 401 ; 
       63 49 401 ; 
       64 50 401 ; 
       65 61 401 ; 
       67 51 401 ; 
       68 52 401 ; 
       70 53 401 ; 
       71 54 401 ; 
       72 55 401 ; 
       73 56 401 ; 
       75 60 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 50 0 0 SRT 1 1 1 -0.0003636462 -0.01099198 -0.006247859 0 0 -0.1383678 MPRFLG 0 ; 
       4 SCHEM 50 -8 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 MPRFLG 0 ; 
       7 SCHEM 30 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 40 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 MPRFLG 0 ; 
       14 SCHEM 26.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 10 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 26.25 -8 0 DISPLAY 0 0 SRT 1 1 1 0.1613091 1.57306 0.1550885 -0.006310462 -0.3395127 -2.02184 MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 60 -2 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 65 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 39 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 44 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
