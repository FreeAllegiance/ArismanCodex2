SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.2-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       debug_turr_prob-mat100.1-0 ; 
       debug_turr_prob-mat101.1-0 ; 
       debug_turr_prob-mat98.1-0 ; 
       debug_turr_prob-mat99.1-0 ; 
       kez_frigate_F-mat14.1-0 ; 
       kez_frigate_F-mat15.1-0 ; 
       kez_frigate_F-mat17.1-0 ; 
       kez_frigate_F-mat18.1-0 ; 
       kez_frigate_F-mat19.1-0 ; 
       kez_frigate_F-mat20.1-0 ; 
       kez_frigate_F-mat21.1-0 ; 
       kez_frigate_F-mat22.1-0 ; 
       kez_frigate_F-mat23.1-0 ; 
       kez_frigate_F-mat24.1-0 ; 
       kez_frigate_F-mat25.1-0 ; 
       kez_frigate_F-mat26.1-0 ; 
       kez_frigate_F-mat28.1-0 ; 
       kez_frigate_F-mat30.1-0 ; 
       kez_frigate_F-mat32.1-0 ; 
       kez_frigate_F-mat33.1-0 ; 
       kez_frigate_F-mat34.1-0 ; 
       kez_frigate_F-mat35.1-0 ; 
       kez_frigate_F-mat36.1-0 ; 
       kez_frigate_F-mat37.1-0 ; 
       kez_frigate_F-mat38.1-0 ; 
       kez_frigate_F-mat39.1-0 ; 
       kez_frigate_F-mat40.1-0 ; 
       kez_frigate_F-mat41.1-0 ; 
       kez_frigate_F-mat43.1-0 ; 
       kez_frigate_F-mat44.1-0 ; 
       kez_frigate_F-mat45.1-0 ; 
       kez_frigate_F-mat46.1-0 ; 
       kez_frigate_F-mat48.1-0 ; 
       kez_frigate_F-mat49.1-0 ; 
       kez_frigate_F-mat50.1-0 ; 
       kez_frigate_F-mat51.1-0 ; 
       kez_frigate_F-mat52.1-0 ; 
       kez_frigate_F-mat53.1-0 ; 
       kez_frigate_F-mat60.1-0 ; 
       kez_frigate_F-mat66.1-0 ; 
       kez_frigate_F-mat69.1-0 ; 
       kez_frigate_F-mat70.1-0 ; 
       kez_frigate_F-mat71.1-0 ; 
       kez_frigate_F-mat72.1-0 ; 
       kez_frigate_F-mat74.1-0 ; 
       STATIC-mat127.1-0 ; 
       STATIC-mat128.1-0 ; 
       STATIC-mat129.1-0 ; 
       STATIC-mat130.1-0 ; 
       STATIC-mat131.1-0 ; 
       STATIC-mat132.1-0 ; 
       STATIC-mat133.1-0 ; 
       STATIC-mat134.1-0 ; 
       STATIC-mat135.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01_2.2-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-mfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/xfiles/Carriers/cap502/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       debug_turr_prob-t2d79.1-0 ; 
       debug_turr_prob-t2d80.1-0 ; 
       debug_turr_prob-t2d81.1-0 ; 
       debug_turr_prob-t2d82.1-0 ; 
       kez_frigate_F-t2d14.1-0 ; 
       kez_frigate_F-t2d15.1-0 ; 
       kez_frigate_F-t2d16.1-0 ; 
       kez_frigate_F-t2d17.1-0 ; 
       kez_frigate_F-t2d18.1-0 ; 
       kez_frigate_F-t2d19.1-0 ; 
       kez_frigate_F-t2d20.1-0 ; 
       kez_frigate_F-t2d21.1-0 ; 
       kez_frigate_F-t2d22.1-0 ; 
       kez_frigate_F-t2d24.1-0 ; 
       kez_frigate_F-t2d26.1-0 ; 
       kez_frigate_F-t2d27.1-0 ; 
       kez_frigate_F-t2d28.1-0 ; 
       kez_frigate_F-t2d29.1-0 ; 
       kez_frigate_F-t2d30.1-0 ; 
       kez_frigate_F-t2d31.1-0 ; 
       kez_frigate_F-t2d32.1-0 ; 
       kez_frigate_F-t2d33.1-0 ; 
       kez_frigate_F-t2d34.1-0 ; 
       kez_frigate_F-t2d35.1-0 ; 
       kez_frigate_F-t2d36.1-0 ; 
       kez_frigate_F-t2d37.1-0 ; 
       kez_frigate_F-t2d38.1-0 ; 
       kez_frigate_F-t2d40.1-0 ; 
       kez_frigate_F-t2d41.1-0 ; 
       kez_frigate_F-t2d42.1-0 ; 
       kez_frigate_F-t2d43.1-0 ; 
       kez_frigate_F-t2d44.1-0 ; 
       kez_frigate_F-t2d45.1-0 ; 
       kez_frigate_F-t2d55.1-0 ; 
       kez_frigate_F-t2d58.1-0 ; 
       kez_frigate_F-t2d59.1-0 ; 
       kez_frigate_F-t2d60.1-0 ; 
       kez_frigate_F-t2d61.1-0 ; 
       kez_frigate_F-t2d62.1-0 ; 
       STATIC-t2d103.1-0 ; 
       STATIC-t2d104.1-0 ; 
       STATIC-t2d105.1-0 ; 
       STATIC-t2d106.1-0 ; 
       STATIC-t2d107.1-0 ; 
       STATIC-t2d108.1-0 ; 
       STATIC-t2d109.1-0 ; 
       STATIC-t2d110.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 7 110 ; 
       0 7 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 45 300 ; 
       8 46 300 ; 
       8 47 300 ; 
       8 48 300 ; 
       8 49 300 ; 
       8 50 300 ; 
       8 51 300 ; 
       8 52 300 ; 
       8 53 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 3 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 2 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 0 300 ; 
       7 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
       46 39 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       47 40 401 ; 
       48 41 401 ; 
       49 42 401 ; 
       50 43 401 ; 
       51 44 401 ; 
       52 45 401 ; 
       39 33 401 ; 
       40 34 401 ; 
       41 35 401 ; 
       42 36 401 ; 
       43 37 401 ; 
       44 38 401 ; 
       53 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 0 0 SRT 1 1 1 -0.0003636462 -0.01099198 -0.006247859 0 0 -0.1383678 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 USR MPRFLG 0 ; 
       5 SCHEM 5 -8 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       39 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
