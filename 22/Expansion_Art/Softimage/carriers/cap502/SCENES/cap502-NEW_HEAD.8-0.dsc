SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       NEW_HEAD-cam_int1.8-0 ROOT ; 
       NEW_HEAD-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       enginnes-default1.1-0 ; 
       enginnes-mat100.1-0 ; 
       enginnes-mat101.1-0 ; 
       enginnes-mat102.1-0 ; 
       enginnes-mat103.1-0 ; 
       enginnes-mat104.1-0 ; 
       enginnes-mat108.2-0 ; 
       enginnes-mat271.1-0 ; 
       enginnes-mat275.1-0 ; 
       enginnes-mat276.1-0 ; 
       enginnes-mat277.1-0 ; 
       enginnes-mat278.1-0 ; 
       enginnes-mat279.1-0 ; 
       enginnes-mat281.1-0 ; 
       enginnes-mat282.1-0 ; 
       enginnes-mat283.1-0 ; 
       enginnes-mat284.1-0 ; 
       enginnes-mat285.1-0 ; 
       enginnes-mat286.1-0 ; 
       enginnes-mat287.1-0 ; 
       enginnes-mat288.1-0 ; 
       enginnes-mat289.1-0 ; 
       enginnes-mat290.1-0 ; 
       enginnes-mat291.1-0 ; 
       enginnes-mat292.1-0 ; 
       enginnes-mat293.1-0 ; 
       kez_frigate_F-mat19.8-0 ; 
       kez_frigate_F-mat55.4-0 ; 
       kez_frigate_F-mat57.4-0 ; 
       kez_frigate_F-mat60.3-0 ; 
       kez_frigate_F-mat71.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       enginnes-afuselg2.5-0 ; 
       enginnes-afuselg3.5-0 ; 
       enginnes-bmerge2.12-0 ROOT ; 
       enginnes-bmerge3.9-0 ; 
       enginnes-bmerge4.2-0 ; 
       enginnes-cube1.3-0 ; 
       enginnes-cube2.2-0 ; 
       enginnes-cube3.1-0 ; 
       enginnes-cube6.1-0 ; 
       enginnes-cube7.1-0 ; 
       enginnes-engine2.9-0 ; 
       enginnes-ffuselg.1-0 ; 
       enginnes-mfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cap502 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cwbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap502-NEW_HEAD.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       enginnes-t2d239.2-0 ; 
       enginnes-t2d243.1-0 ; 
       enginnes-t2d244.1-0 ; 
       enginnes-t2d245.1-0 ; 
       enginnes-t2d246.1-0 ; 
       enginnes-t2d248.1-0 ; 
       enginnes-t2d249.1-0 ; 
       enginnes-t2d250.1-0 ; 
       enginnes-t2d251.1-0 ; 
       enginnes-t2d252.1-0 ; 
       enginnes-t2d253.1-0 ; 
       enginnes-t2d254.1-0 ; 
       enginnes-t2d255.1-0 ; 
       enginnes-t2d256.1-0 ; 
       enginnes-t2d257.1-0 ; 
       enginnes-t2d258.1-0 ; 
       enginnes-t2d259.1-0 ; 
       enginnes-t2d260.1-0 ; 
       enginnes-t2d261.1-0 ; 
       enginnes-t2d262.1-0 ; 
       enginnes-t2d81.1-0 ; 
       enginnes-t2d82.1-0 ; 
       enginnes-t2d83.1-0 ; 
       enginnes-t2d84.1-0 ; 
       enginnes-t2d85.1-0 ; 
       enginnes-t2d90.3-0 ; 
       enginnes-t2d91.3-0 ; 
       kez_frigate_F-t2d60.4-0 ; 
       NEW_HEAD-t2d263.2-0 ; 
       NEW_HEAD-t2d264.4-0 ; 
       NEW_HEAD-t2d265.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       11 12 110 ; 
       12 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 4 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       2 0 300 ; 
       3 5 300 ; 
       3 7 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       7 8 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       10 26 300 ; 
       10 6 300 ; 
       10 25 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       12 29 300 ; 
       12 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 20 401 ; 
       1 21 401 ; 
       2 22 401 ; 
       3 23 401 ; 
       4 24 401 ; 
       5 16 401 ; 
       6 25 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 8 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 26 401 ; 
       27 30 401 ; 
       28 29 401 ; 
       29 28 401 ; 
       30 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0.001528643 0.03424564 1.173152 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -4 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 2.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       29 SCHEM 2.75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 2.75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 5.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 5.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
