SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_points-cam_int1.1-0 ROOT ; 
       add_points-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       enginnes-default1.1-0 ; 
       enginnes-mat100.1-0 ; 
       enginnes-mat101.1-0 ; 
       enginnes-mat102.1-0 ; 
       enginnes-mat103.1-0 ; 
       enginnes-mat104.1-0 ; 
       enginnes-mat108.2-0 ; 
       enginnes-mat271.1-0 ; 
       enginnes-mat275.1-0 ; 
       enginnes-mat276.1-0 ; 
       enginnes-mat277.1-0 ; 
       enginnes-mat278.1-0 ; 
       enginnes-mat279.1-0 ; 
       enginnes-mat281.1-0 ; 
       enginnes-mat282.1-0 ; 
       enginnes-mat283.1-0 ; 
       enginnes-mat284.1-0 ; 
       enginnes-mat285.1-0 ; 
       enginnes-mat286.1-0 ; 
       enginnes-mat287.1-0 ; 
       enginnes-mat288.1-0 ; 
       enginnes-mat289.1-0 ; 
       enginnes-mat290.1-0 ; 
       enginnes-mat291.1-0 ; 
       enginnes-mat292.1-0 ; 
       enginnes-mat293.1-0 ; 
       kez_frigate_F-mat19.8-0 ; 
       kez_frigate_F-mat55.4-0 ; 
       kez_frigate_F-mat57.4-0 ; 
       kez_frigate_F-mat60.3-0 ; 
       kez_frigate_F-mat71.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       cap502-afuselg2.5-0 ; 
       cap502-afuselg3.5-0 ; 
       cap502-bmerge3.9-0 ; 
       cap502-bmerge4.2-0 ; 
       cap502-cockpt.1-0 ; 
       cap502-cube1.3-0 ; 
       cap502-cube2.2-0 ; 
       cap502-cube3.1-0 ; 
       cap502-cube6.1-0 ; 
       cap502-cube7.1-0 ; 
       cap502-engine2.9-0 ; 
       cap502-ffuselg.1-0 ; 
       cap502-garage1A.1-0 ; 
       cap502-garage1B.1-0 ; 
       cap502-garage1C.1-0 ; 
       cap502-garage1D.1-0 ; 
       cap502-garage1E.1-0 ; 
       cap502-garage2A.1-0 ; 
       cap502-garage2B.1-0 ; 
       cap502-garage2C.1-0 ; 
       cap502-garage2D.1-0 ; 
       cap502-garage2E.1-0 ; 
       cap502-launch1.1-0 ; 
       cap502-launch2.1-0 ; 
       cap502-mfuselg.1-0 ; 
       cap502-root.3-0 ROOT ; 
       cap502-thrust1.1-0 ; 
       cap502-thrust2.1-0 ; 
       cap502-thrust3.1-0 ; 
       cap502-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cap502 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cwbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap502-add_points.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_points-t2d263.1-0 ; 
       add_points-t2d264.1-0 ; 
       add_points-t2d265.1-0 ; 
       enginnes-t2d239.2-0 ; 
       enginnes-t2d243.1-0 ; 
       enginnes-t2d244.1-0 ; 
       enginnes-t2d245.1-0 ; 
       enginnes-t2d246.1-0 ; 
       enginnes-t2d248.1-0 ; 
       enginnes-t2d249.1-0 ; 
       enginnes-t2d250.1-0 ; 
       enginnes-t2d251.1-0 ; 
       enginnes-t2d252.1-0 ; 
       enginnes-t2d253.1-0 ; 
       enginnes-t2d254.1-0 ; 
       enginnes-t2d255.1-0 ; 
       enginnes-t2d256.1-0 ; 
       enginnes-t2d257.1-0 ; 
       enginnes-t2d258.1-0 ; 
       enginnes-t2d259.1-0 ; 
       enginnes-t2d260.1-0 ; 
       enginnes-t2d261.1-0 ; 
       enginnes-t2d262.1-0 ; 
       enginnes-t2d81.1-0 ; 
       enginnes-t2d82.1-0 ; 
       enginnes-t2d83.1-0 ; 
       enginnes-t2d84.1-0 ; 
       enginnes-t2d85.1-0 ; 
       enginnes-t2d90.3-0 ; 
       enginnes-t2d91.3-0 ; 
       kez_frigate_F-t2d60.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 25 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       5 6 110 ; 
       6 25 110 ; 
       7 25 110 ; 
       8 25 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       29 25 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       4 25 110 ; 
       22 25 110 ; 
       23 25 110 ; 
       13 25 110 ; 
       14 25 110 ; 
       15 25 110 ; 
       16 25 110 ; 
       12 25 110 ; 
       21 25 110 ; 
       20 25 110 ; 
       19 25 110 ; 
       18 25 110 ; 
       11 24 110 ; 
       17 25 110 ; 
       24 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 4 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       25 0 300 ; 
       2 5 300 ; 
       2 7 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       7 8 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       10 26 300 ; 
       10 6 300 ; 
       10 25 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       24 29 300 ; 
       24 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 23 401 ; 
       1 24 401 ; 
       2 25 401 ; 
       3 26 401 ; 
       4 27 401 ; 
       5 19 401 ; 
       6 28 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 11 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 29 401 ; 
       27 2 401 ; 
       28 1 401 ; 
       29 0 401 ; 
       30 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       25 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0.001528643 0.03424564 1.173152 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       29 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 35 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 55 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 50 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       17 SCHEM 45 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
