SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_points-cam_int1.7-0 ROOT ; 
       add_points-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       cap502-bound01.1-0 ; 
       cap502-bound02.1-0 ; 
       cap502-bound03.1-0 ; 
       cap502-bound04.1-0 ; 
       cap502-bound05.1-0 ; 
       cap502-bound06.1-0 ; 
       cap502-bound07.1-0 ; 
       cap502-bound08.8-0 ; 
       cap502-bound09.1-0 ; 
       cap502-bound10.1-0 ; 
       cap502-bounding_model.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap502-bound.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 10 110 ; 
       9 10 110 ; 
       0 10 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 13.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
