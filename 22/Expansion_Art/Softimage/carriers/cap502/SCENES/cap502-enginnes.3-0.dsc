SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.35-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       enginnes-default1.1-0 ; 
       enginnes-mat100.1-0 ; 
       enginnes-mat101.1-0 ; 
       enginnes-mat102.1-0 ; 
       enginnes-mat103.1-0 ; 
       enginnes-mat104.1-0 ; 
       enginnes-mat108.1-0 ; 
       enginnes-mat264.1-0 ; 
       enginnes-mat265.1-0 ; 
       enginnes-mat269.1-0 ; 
       enginnes-mat271.1-0 ; 
       enginnes-mat275.1-0 ; 
       enginnes-mat276.1-0 ; 
       enginnes-mat277.1-0 ; 
       enginnes-mat278.1-0 ; 
       enginnes-mat279.1-0 ; 
       enginnes-mat281.1-0 ; 
       enginnes-mat282.1-0 ; 
       enginnes-mat283.1-0 ; 
       enginnes-mat284.1-0 ; 
       enginnes-mat285.1-0 ; 
       enginnes-mat286.1-0 ; 
       enginnes-mat287.1-0 ; 
       enginnes-mat288.1-0 ; 
       enginnes-mat289.1-0 ; 
       enginnes-mat290.1-0 ; 
       enginnes-mat291.1-0 ; 
       enginnes-mat292.1-0 ; 
       kez_frigate_F-mat19.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       enginnes-afuselg2.5-0 ; 
       enginnes-afuselg3.5-0 ; 
       enginnes-bmerge2.3-0 ROOT ; 
       enginnes-bmerge3.9-0 ; 
       enginnes-bmerge4.2-0 ROOT ; 
       enginnes-cube1.3-0 ; 
       enginnes-cube2.2-0 ; 
       enginnes-cube3.1-0 ; 
       enginnes-cube6.1-0 ; 
       enginnes-cube7.1-0 ; 
       enginnes-engine2.9-0 ; 
       enginnes-fuselg4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cap502 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cwbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap502-enginnes.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       enginnes-t2d232.1-0 ; 
       enginnes-t2d236.1-0 ; 
       enginnes-t2d239.2-0 ; 
       enginnes-t2d243.1-0 ; 
       enginnes-t2d244.1-0 ; 
       enginnes-t2d245.1-0 ; 
       enginnes-t2d246.1-0 ; 
       enginnes-t2d248.1-0 ; 
       enginnes-t2d249.1-0 ; 
       enginnes-t2d250.1-0 ; 
       enginnes-t2d251.1-0 ; 
       enginnes-t2d252.1-0 ; 
       enginnes-t2d253.1-0 ; 
       enginnes-t2d254.1-0 ; 
       enginnes-t2d255.1-0 ; 
       enginnes-t2d256.1-0 ; 
       enginnes-t2d257.1-0 ; 
       enginnes-t2d258.1-0 ; 
       enginnes-t2d259.1-0 ; 
       enginnes-t2d260.1-0 ; 
       enginnes-t2d261.1-0 ; 
       enginnes-t2d81.1-0 ; 
       enginnes-t2d82.1-0 ; 
       enginnes-t2d83.1-0 ; 
       enginnes-t2d84.1-0 ; 
       enginnes-t2d85.1-0 ; 
       enginnes-t2d90.2-0 ; 
       enginnes-t2d91.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       3 10 110 ; 
       5 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       11 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 26 300 ; 
       4 27 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       2 0 300 ; 
       3 5 300 ; 
       3 10 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       7 11 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       10 28 300 ; 
       10 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 21 401 ; 
       1 22 401 ; 
       2 23 401 ; 
       3 24 401 ; 
       4 25 401 ; 
       5 18 401 ; 
       6 26 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       26 19 401 ; 
       10 2 401 ; 
       27 20 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 10 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       17 8 401 ; 
       18 9 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       28 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 67.5 0 0 SRT 0.6069559 0.6069559 0.6069559 3.011593 5.654728e-009 1.570796 -0.01214046 -0.6331102 -3.094305 MPRFLG 0 ; 
       0 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       1 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 SRT 1 1 1 0 0 0 0.001528643 0.03424564 1.173152 MPRFLG 0 ; 
       3 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 31.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 66.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 68.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 66.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 68.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
