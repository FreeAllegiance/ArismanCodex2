SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.32-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       gc_carrier-default1.4-0 ; 
       gc_carrier-mat100.3-0 ; 
       gc_carrier-mat101.3-0 ; 
       gc_carrier-mat102.4-0 ; 
       gc_carrier-mat103.3-0 ; 
       gc_carrier-mat104.7-0 ; 
       gc_carrier-mat108.3-0 ; 
       gc_carrier-mat264.2-0 ; 
       gc_carrier-mat265.2-0 ; 
       gc_carrier-mat269.2-0 ; 
       gc_carrier-mat270.2-0 ; 
       gc_carrier-mat271.1-0 ; 
       gc_carrier-mat272.1-0 ; 
       gc_carrier-mat273.1-0 ; 
       gc_carrier-mat274.1-0 ; 
       gc_carrier-mat275.2-0 ; 
       gc_carrier-mat276.2-0 ; 
       gc_carrier-mat277.1-0 ; 
       gc_carrier-mat278.3-0 ; 
       gc_carrier-mat279.3-0 ; 
       gc_carrier-mat281.2-0 ; 
       gc_carrier-mat282.1-0 ; 
       gc_carrier-mat283.1-0 ; 
       gc_carrier-mat284.2-0 ; 
       gc_carrier-mat285.2-0 ; 
       gc_carrier-mat286.2-0 ; 
       gc_carrier-mat287.2-0 ; 
       gc_carrier-mat288.2-0 ; 
       gc_carrier-mat289.2-0 ; 
       gc_carrier-mat290.2-0 ; 
       kez_frigate_F-mat19.7-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       gc_carrier-afuselg2.5-0 ; 
       gc_carrier-afuselg3.5-0 ; 
       gc_carrier-bmerge2.10-0 ROOT ; 
       gc_carrier-bmerge3.9-0 ; 
       gc_carrier-bmerge6.1-0 ; 
       gc_carrier-cube1.3-0 ; 
       gc_carrier-cube2.2-0 ; 
       gc_carrier-cube3.1-0 ; 
       gc_carrier-cube6.1-0 ; 
       gc_carrier-cube7.1-0 ; 
       gc_carrier-engine2.9-0 ; 
       gc_carrier-fuselg4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cap502 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap502/PICTURES/cwbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap502-gc_carrier.56-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       gc_carrier-t2d232.4-0 ; 
       gc_carrier-t2d236.4-0 ; 
       gc_carrier-t2d237.3-0 ; 
       gc_carrier-t2d238.2-0 ; 
       gc_carrier-t2d239.1-0 ; 
       gc_carrier-t2d240.1-0 ; 
       gc_carrier-t2d241.1-0 ; 
       gc_carrier-t2d242.1-0 ; 
       gc_carrier-t2d243.5-0 ; 
       gc_carrier-t2d244.3-0 ; 
       gc_carrier-t2d245.2-0 ; 
       gc_carrier-t2d246.9-0 ; 
       gc_carrier-t2d248.8-0 ; 
       gc_carrier-t2d249.6-0 ; 
       gc_carrier-t2d250.5-0 ; 
       gc_carrier-t2d251.4-0 ; 
       gc_carrier-t2d252.2-0 ; 
       gc_carrier-t2d253.2-0 ; 
       gc_carrier-t2d254.2-0 ; 
       gc_carrier-t2d255.2-0 ; 
       gc_carrier-t2d256.2-0 ; 
       gc_carrier-t2d257.2-0 ; 
       gc_carrier-t2d258.2-0 ; 
       gc_carrier-t2d81.2-0 ; 
       gc_carrier-t2d82.4-0 ; 
       gc_carrier-t2d83.4-0 ; 
       gc_carrier-t2d84.4-0 ; 
       gc_carrier-t2d85.3-0 ; 
       gc_carrier-t2d90.3-0 ; 
       gc_carrier-t2d91.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 0 110 ; 
       0 1 110 ; 
       1 2 110 ; 
       3 10 110 ; 
       8 2 110 ; 
       5 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       11 7 110 ; 
       4 10 110 ; 
       9 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 30 300 ; 
       10 6 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       2 0 300 ; 
       3 5 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 15 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       9 29 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 23 401 ; 
       1 24 401 ; 
       2 25 401 ; 
       3 26 401 ; 
       4 27 401 ; 
       5 2 401 ; 
       10 3 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       6 28 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       30 29 401 ; 
       11 4 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 15 401 ; 
       19 11 401 ; 
       23 16 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       0 SCHEM 56.25 -8 0 MPRFLG 0 ; 
       1 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 37.5 -4 0 SRT 1 1 1 0 0 0 0.001528643 0.03424564 1.173152 MPRFLG 0 ; 
       3 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 77.5 0 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 55 -12 0 MPRFLG 0 ; 
       9 SCHEM 75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 85 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
