SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.19-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       bio_carrier-mat132.5-0 ; 
       bio_carrier-mat146.2-0 ; 
       bio_carrier-mat156.2-0 ; 
       bio_carrier-mat164.2-0 ; 
       bio_carrier-mat165.2-0 ; 
       bio_carrier-mat166.1-0 ; 
       bio_carrier-mat167.1-0 ; 
       bio_carrier-mat168.1-0 ; 
       bio_carrier-mat169.1-0 ; 
       bio_carrier-mat170.1-0 ; 
       bio_carrier-mat178.1-0 ; 
       bio_carrier-mat179.1-0 ; 
       bio_carrier-mat180.1-0 ; 
       bio_carrier-mat181.1-0 ; 
       bio_carrier-mat182.1-0 ; 
       bio_carrier-mat183.1-0 ; 
       bio_carrier-mat184.1-0 ; 
       bio_carrier-mat185.1-0 ; 
       bio_carrier-mat186.1-0 ; 
       bio_carrier-mat187.1-0 ; 
       bio_carrier-mat188.1-0 ; 
       bio_carrier-mat189.1-0 ; 
       bio_carrier-mat190.1-0 ; 
       bio_carrier-mat191.1-0 ; 
       bio_carrier-mat192.1-0 ; 
       bio_carrier-mat193.1-0 ; 
       bio_carrier-mat194.1-0 ; 
       bio_carrier-mat195.1-0 ; 
       bio_carrier-mat196.1-0 ; 
       biocruiser_cap102-mat95.4-0 ; 
       biofrigate_cap101-mat4.1-0 ; 
       biofrigate_cap101-mat5.1-0 ; 
       biofrigate_cap101-mat6.1-0 ; 
       biofrigate_cap101-mat7.1-0 ; 
       biofrigate_cap101-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       cruiser-cyl13.17-0 ROOT ; 
       cruiser-extru12.1-0 ; 
       cruiser-extru14.1-0 ; 
       cruiser-extru15.1-0 ; 
       cruiser-extru2.1-0 ; 
       cruiser-extru9_8_1.2-0 ; 
       cruiser-extru9_8_5.1-0 ; 
       cruiser-extru9_8_7.1-0 ; 
       cruiser-extru9_8_9.1-0 ; 
       cruiser-sphere15.1-0 ; 
       cruiser-sphere17.1-0 ; 
       cruiser-sphere18.1-0 ; 
       cruiser-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap503/PICTURES/cap503 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap503-bio_carrier.22-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       bio_carrier-t2d26.2-0 ; 
       bio_carrier-t2d36.4-0 ; 
       bio_carrier-t2d37.2-0 ; 
       bio_carrier-t2d45.3-0 ; 
       bio_carrier-t2d46.3-0 ; 
       bio_carrier-t2d47.2-0 ; 
       bio_carrier-t2d48.2-0 ; 
       bio_carrier-t2d49.2-0 ; 
       bio_carrier-t2d50.2-0 ; 
       bio_carrier-t2d51.2-0 ; 
       bio_carrier-t2d59.1-0 ; 
       bio_carrier-t2d60.1-0 ; 
       bio_carrier-t2d61.1-0 ; 
       bio_carrier-t2d62.1-0 ; 
       bio_carrier-t2d63.1-0 ; 
       bio_carrier-t2d64.1-0 ; 
       bio_carrier-t2d65.1-0 ; 
       bio_carrier-t2d66.1-0 ; 
       bio_carrier-t2d67.1-0 ; 
       bio_carrier-t2d68.1-0 ; 
       bio_carrier-t2d69.1-0 ; 
       bio_carrier-t2d70.1-0 ; 
       bio_carrier-t2d71.1-0 ; 
       bio_carrier-t2d72.1-0 ; 
       bio_carrier-t2d73.1-0 ; 
       bio_carrier-t2d74.1-0 ; 
       bio_carrier-t2d75.1-0 ; 
       bio_carrier-t2d76.1-0 ; 
       bio_carrier-t2d77.1-0 ; 
       biocruiser_cap102-t2d15.4-0 ; 
       biofrigate_cap101-t2d4.3-0 ; 
       biofrigate_cap101-t2d5.3-0 ; 
       biofrigate_cap101-t2d6.3-0 ; 
       biofrigate_cap101-t2d7.2-0 ; 
       biofrigate_cap101-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 5 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       5 0 110 ; 
       1 6 110 ; 
       2 7 110 ; 
       12 4 110 ; 
       9 1 110 ; 
       10 2 110 ; 
       8 0 110 ; 
       3 8 110 ; 
       11 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 29 300 ; 
       0 1 300 ; 
       0 28 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       6 19 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       5 10 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       11 26 300 ; 
       11 27 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       20 20 401 ; 
       11 11 401 ; 
       21 21 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       2 2 401 ; 
       17 17 401 ; 
       29 29 401 ; 
       1 0 401 ; 
       25 25 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       26 26 401 ; 
       10 10 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       27 27 401 ; 
       28 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 12.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 15 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
