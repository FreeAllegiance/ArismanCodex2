SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.23-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       bio_carrier-mat132.6-0 ; 
       bio_carrier-mat146.3-0 ; 
       bio_carrier-mat156.3-0 ; 
       bio_carrier-mat164.3-0 ; 
       bio_carrier-mat165.3-0 ; 
       bio_carrier-mat166.2-0 ; 
       bio_carrier-mat167.2-0 ; 
       bio_carrier-mat168.2-0 ; 
       bio_carrier-mat169.2-0 ; 
       bio_carrier-mat170.2-0 ; 
       bio_carrier-mat178.2-0 ; 
       bio_carrier-mat179.2-0 ; 
       bio_carrier-mat180.2-0 ; 
       bio_carrier-mat181.2-0 ; 
       bio_carrier-mat182.2-0 ; 
       bio_carrier-mat183.2-0 ; 
       bio_carrier-mat184.2-0 ; 
       bio_carrier-mat185.2-0 ; 
       bio_carrier-mat186.2-0 ; 
       bio_carrier-mat187.2-0 ; 
       bio_carrier-mat188.2-0 ; 
       bio_carrier-mat189.2-0 ; 
       bio_carrier-mat190.2-0 ; 
       bio_carrier-mat191.2-0 ; 
       bio_carrier-mat192.2-0 ; 
       bio_carrier-mat193.2-0 ; 
       bio_carrier-mat194.2-0 ; 
       bio_carrier-mat195.2-0 ; 
       bio_carrier-mat196.2-0 ; 
       biocruiser_cap102-mat95.5-0 ; 
       biofrigate_cap101-mat4.2-0 ; 
       biofrigate_cap101-mat5.2-0 ; 
       biofrigate_cap101-mat6.2-0 ; 
       biofrigate_cap101-mat7.2-0 ; 
       biofrigate_cap101-mat8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       cruiser-cyl13.20-0 ROOT ; 
       cruiser-extru12.1-0 ; 
       cruiser-extru14.1-0 ; 
       cruiser-extru15.1-0 ; 
       cruiser-extru2.1-0 ; 
       cruiser-extru9_8_1.2-0 ; 
       cruiser-extru9_8_5.1-0 ; 
       cruiser-extru9_8_7.1-0 ; 
       cruiser-extru9_8_9.1-0 ; 
       cruiser-sphere15.1-0 ; 
       cruiser-sphere17.1-0 ; 
       cruiser-sphere18.1-0 ; 
       cruiser-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap503/PICTURES/cap503 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap503-bio_carrier.26-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       bio_carrier-t2d36.5-0 ; 
       bio_carrier-t2d37.3-0 ; 
       bio_carrier-t2d45.4-0 ; 
       bio_carrier-t2d46.4-0 ; 
       bio_carrier-t2d47.3-0 ; 
       bio_carrier-t2d48.3-0 ; 
       bio_carrier-t2d49.3-0 ; 
       bio_carrier-t2d50.3-0 ; 
       bio_carrier-t2d51.3-0 ; 
       bio_carrier-t2d59.2-0 ; 
       bio_carrier-t2d60.2-0 ; 
       bio_carrier-t2d61.2-0 ; 
       bio_carrier-t2d62.2-0 ; 
       bio_carrier-t2d63.2-0 ; 
       bio_carrier-t2d64.2-0 ; 
       bio_carrier-t2d65.2-0 ; 
       bio_carrier-t2d66.2-0 ; 
       bio_carrier-t2d67.2-0 ; 
       bio_carrier-t2d68.2-0 ; 
       bio_carrier-t2d69.2-0 ; 
       bio_carrier-t2d70.2-0 ; 
       bio_carrier-t2d71.2-0 ; 
       bio_carrier-t2d72.2-0 ; 
       bio_carrier-t2d73.2-0 ; 
       bio_carrier-t2d74.2-0 ; 
       bio_carrier-t2d75.2-0 ; 
       bio_carrier-t2d76.2-0 ; 
       biofrigate_cap101-t2d4.4-0 ; 
       biofrigate_cap101-t2d5.4-0 ; 
       biofrigate_cap101-t2d6.4-0 ; 
       biofrigate_cap101-t2d7.3-0 ; 
       biofrigate_cap101-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 7 110 ; 
       3 8 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 1 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 29 300 ; 
       0 1 300 ; 
       0 28 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       5 10 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       6 19 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       12 33 300 ; 
       12 34 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
