SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.31-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       bio_carrier-mat132.6-0 ; 
       bio_carrier-mat146.3-0 ; 
       bio_carrier-mat156.3-0 ; 
       bio_carrier-mat164.3-0 ; 
       bio_carrier-mat165.3-0 ; 
       bio_carrier-mat166.2-0 ; 
       bio_carrier-mat167.2-0 ; 
       bio_carrier-mat168.2-0 ; 
       bio_carrier-mat169.2-0 ; 
       bio_carrier-mat170.2-0 ; 
       bio_carrier-mat178.2-0 ; 
       bio_carrier-mat179.2-0 ; 
       bio_carrier-mat180.2-0 ; 
       bio_carrier-mat181.2-0 ; 
       bio_carrier-mat182.2-0 ; 
       bio_carrier-mat183.2-0 ; 
       bio_carrier-mat184.2-0 ; 
       bio_carrier-mat185.2-0 ; 
       bio_carrier-mat186.2-0 ; 
       bio_carrier-mat187.2-0 ; 
       bio_carrier-mat188.2-0 ; 
       bio_carrier-mat189.2-0 ; 
       bio_carrier-mat190.2-0 ; 
       bio_carrier-mat191.2-0 ; 
       bio_carrier-mat192.2-0 ; 
       bio_carrier-mat193.2-0 ; 
       bio_carrier-mat194.2-0 ; 
       bio_carrier-mat195.2-0 ; 
       bio_carrier-mat196.2-0 ; 
       biocruiser_cap102-mat95.5-0 ; 
       biofrigate_cap101-mat4.2-0 ; 
       biofrigate_cap101-mat5.2-0 ; 
       biofrigate_cap101-mat6.2-0 ; 
       biofrigate_cap101-mat7.2-0 ; 
       biofrigate_cap101-mat8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       cruiser-cockpt.1-0 ; 
       cruiser-cyl13.28-0 ROOT ; 
       cruiser-extru12.1-0 ; 
       cruiser-extru14.1-0 ; 
       cruiser-extru15.1-0 ; 
       cruiser-extru2.1-0 ; 
       cruiser-extru9_8_1.2-0 ; 
       cruiser-extru9_8_5.1-0 ; 
       cruiser-extru9_8_7.1-0 ; 
       cruiser-extru9_8_9.1-0 ; 
       cruiser-garage1A.1-0 ; 
       cruiser-garage1B.1-0 ; 
       cruiser-garage1C.1-0 ; 
       cruiser-garage1D.1-0 ; 
       cruiser-garage1E.1-0 ; 
       cruiser-launch1.1-0 ; 
       cruiser-sphere15.1-0 ; 
       cruiser-sphere17.1-0 ; 
       cruiser-sphere18.1-0 ; 
       cruiser-sphere3.1-0 ; 
       cruiser-thrust1.1-0 ; 
       cruiser-thrust10.1-0 ; 
       cruiser-thrust2.1-0 ; 
       cruiser-thrust3.1-0 ; 
       cruiser-thrust4.1-0 ; 
       cruiser-thrust5.1-0 ; 
       cruiser-thrust6.1-0 ; 
       cruiser-thrust9.1-0 ; 
       cruiser-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap503/PICTURES/cap503 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap503-bio_carrier.34-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       bio_carrier-t2d36.6-0 ; 
       bio_carrier-t2d37.4-0 ; 
       bio_carrier-t2d45.5-0 ; 
       bio_carrier-t2d46.5-0 ; 
       bio_carrier-t2d47.5-0 ; 
       bio_carrier-t2d48.5-0 ; 
       bio_carrier-t2d49.5-0 ; 
       bio_carrier-t2d50.5-0 ; 
       bio_carrier-t2d51.5-0 ; 
       bio_carrier-t2d59.3-0 ; 
       bio_carrier-t2d60.3-0 ; 
       bio_carrier-t2d61.3-0 ; 
       bio_carrier-t2d62.3-0 ; 
       bio_carrier-t2d63.4-0 ; 
       bio_carrier-t2d64.4-0 ; 
       bio_carrier-t2d65.4-0 ; 
       bio_carrier-t2d66.4-0 ; 
       bio_carrier-t2d67.4-0 ; 
       bio_carrier-t2d68.3-0 ; 
       bio_carrier-t2d69.3-0 ; 
       bio_carrier-t2d70.3-0 ; 
       bio_carrier-t2d71.3-0 ; 
       bio_carrier-t2d72.4-0 ; 
       bio_carrier-t2d73.4-0 ; 
       bio_carrier-t2d74.4-0 ; 
       bio_carrier-t2d75.4-0 ; 
       bio_carrier-t2d76.4-0 ; 
       bio_carrier-t2d77.8-0 ; 
       bio_carrier-t2d78.6-0 ; 
       bio_carrier-t2d79.6-0 ; 
       biofrigate_cap101-t2d4.6-0 ; 
       biofrigate_cap101-t2d5.6-0 ; 
       biofrigate_cap101-t2d6.6-0 ; 
       biofrigate_cap101-t2d7.5-0 ; 
       biofrigate_cap101-t2d8.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 7 110 ; 
       3 8 110 ; 
       4 9 110 ; 
       5 6 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       16 2 110 ; 
       17 3 110 ; 
       18 4 110 ; 
       19 5 110 ; 
       28 1 110 ; 
       20 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       0 1 110 ; 
       10 1 110 ; 
       15 1 110 ; 
       27 1 110 ; 
       21 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 29 300 ; 
       1 1 300 ; 
       1 28 300 ; 
       2 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       5 30 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       6 0 300 ; 
       6 2 300 ; 
       6 10 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       7 19 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       18 26 300 ; 
       18 27 300 ; 
       19 33 300 ; 
       19 34 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 28 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 MPRFLG 0 ; 
       16 SCHEM 5 -6 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 45 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 50 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
