SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.33-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       bound-cube1.1-0 ; 
       bound-cube10.1-0 ; 
       bound-cube3.1-0 ; 
       bound-cube5.1-0 ; 
       bound-cube6.1-0 ; 
       bound-cube7.1-0 ; 
       bound-cube8.1-0 ; 
       bound-cube9.1-0 ; 
       bound-cyl15.1-0 ; 
       bound-null2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap503-bound.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 9 110 ; 
       8 9 110 ; 
       0 9 110 ; 
       2 9 110 ; 
       6 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       7 9 110 ; 
       1 9 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
