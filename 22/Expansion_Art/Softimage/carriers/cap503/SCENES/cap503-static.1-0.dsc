SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.32-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       biocruiser_cap102-mat95.5-0 ; 
       biofrigate_cap101-mat4.2-0 ; 
       biofrigate_cap101-mat5.2-0 ; 
       biofrigate_cap101-mat6.2-0 ; 
       biofrigate_cap101-mat7.2-0 ; 
       biofrigate_cap101-mat8.2-0 ; 
       static-mat132.1-0 ; 
       static-mat146.1-0 ; 
       static-mat156.1-0 ; 
       static-mat164.1-0 ; 
       static-mat165.1-0 ; 
       static-mat166.1-0 ; 
       static-mat167.1-0 ; 
       static-mat168.1-0 ; 
       static-mat169.1-0 ; 
       static-mat170.1-0 ; 
       static-mat178.1-0 ; 
       static-mat179.1-0 ; 
       static-mat180.1-0 ; 
       static-mat181.1-0 ; 
       static-mat182.1-0 ; 
       static-mat183.1-0 ; 
       static-mat184.1-0 ; 
       static-mat185.1-0 ; 
       static-mat186.1-0 ; 
       static-mat187.1-0 ; 
       static-mat188.1-0 ; 
       static-mat189.1-0 ; 
       static-mat190.1-0 ; 
       static-mat191.1-0 ; 
       static-mat192.1-0 ; 
       static-mat193.1-0 ; 
       static-mat194.1-0 ; 
       static-mat195.1-0 ; 
       static-mat196.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       cruiser-cyl13.29-0 ROOT ; 
       cruiser-extru12.1-0 ; 
       cruiser-extru14.1-0 ; 
       cruiser-extru15.1-0 ; 
       cruiser-extru2.1-0 ; 
       cruiser-extru9_8_1.2-0 ; 
       cruiser-extru9_8_5.1-0 ; 
       cruiser-extru9_8_7.1-0 ; 
       cruiser-extru9_8_9.1-0 ; 
       cruiser-sphere15.1-0 ; 
       cruiser-sphere17.1-0 ; 
       cruiser-sphere18.1-0 ; 
       cruiser-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap503/PICTURES/cap503 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap503-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       biofrigate_cap101-t2d4.6-0 ; 
       biofrigate_cap101-t2d5.6-0 ; 
       biofrigate_cap101-t2d6.6-0 ; 
       biofrigate_cap101-t2d7.5-0 ; 
       biofrigate_cap101-t2d8.5-0 ; 
       static-t2d36.1-0 ; 
       static-t2d37.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d46.1-0 ; 
       static-t2d47.1-0 ; 
       static-t2d48.1-0 ; 
       static-t2d49.1-0 ; 
       static-t2d50.1-0 ; 
       static-t2d51.1-0 ; 
       static-t2d59.1-0 ; 
       static-t2d60.1-0 ; 
       static-t2d61.1-0 ; 
       static-t2d62.1-0 ; 
       static-t2d63.1-0 ; 
       static-t2d64.1-0 ; 
       static-t2d65.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
       static-t2d68.1-0 ; 
       static-t2d69.1-0 ; 
       static-t2d70.1-0 ; 
       static-t2d71.1-0 ; 
       static-t2d72.1-0 ; 
       static-t2d73.1-0 ; 
       static-t2d74.1-0 ; 
       static-t2d75.1-0 ; 
       static-t2d76.1-0 ; 
       static-t2d77.1-0 ; 
       static-t2d78.1-0 ; 
       static-t2d79.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 7 110 ; 
       3 8 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 1 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 7 300 ; 
       0 34 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       5 6 300 ; 
       5 8 300 ; 
       5 16 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 25 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       8 28 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       11 32 300 ; 
       11 33 300 ; 
       12 4 300 ; 
       12 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 5 401 ; 
       7 33 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       0 34 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       32 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
