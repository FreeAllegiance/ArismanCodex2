SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       mets-cam_int1.1-0 ROOT ; 
       mets-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       mets-ball1.1-0 ; 
       mets-ball10.1-0 ; 
       mets-ball11.1-0 ; 
       mets-ball12.1-0 ; 
       mets-ball13.1-0 ; 
       mets-ball14.1-0 ; 
       mets-ball15.1-0 ; 
       mets-ball16.1-0 ; 
       mets-ball17.1-0 ; 
       mets-ball18.1-0 ; 
       mets-ball19.1-0 ; 
       mets-ball2.1-0 ; 
       mets-ball20.1-0 ; 
       mets-ball3.1-0 ; 
       mets-ball4.1-0 ; 
       mets-ball5.1-0 ; 
       mets-ball6.1-0 ; 
       mets-ball7.1-0 ; 
       mets-ball8.1-0 ; 
       mets-ball9.1-0 ; 
       mets-fromMeta1.1-0 ROOT ; 
       mets-meta1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Base-mets.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 21 110 ; 
       0 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       1 21 110 ; 
       2 21 110 ; 
       3 21 110 ; 
       4 21 110 ; 
       5 21 110 ; 
       6 21 110 ; 
       7 21 110 ; 
       8 21 110 ; 
       9 21 110 ; 
       10 21 110 ; 
       12 21 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 5 -2 0 MPRFLG 0 ; 
       21 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 15 -2 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 20 -2 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 45 -2 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 MPRFLG 0 ; 
       20 SCHEM 52.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
