SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_strobe-cam_int1.2-0 ROOT ; 
       add_strobe-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_strobe-default7.1-0 ; 
       add_strobe-default8.1-0 ; 
       add_strobe-mat17.1-0 ; 
       add_strobe-mat19.1-0 ; 
       add_strobe-mat20.1-0 ; 
       add_strobe-mat21.1-0 ; 
       add_strobe-mat22.1-0 ; 
       add_strobe-mat23.1-0 ; 
       add_strobe-mat304.1-0 ; 
       add_strobe-mat373.1-0 ; 
       add_strobe-mat374.1-0 ; 
       add_strobe-mat375.1-0 ; 
       add_strobe-mat376.1-0 ; 
       add_strobe-mat377.1-0 ; 
       add_strobe-mat378.1-0 ; 
       add_strobe-mat379.1-0 ; 
       add_strobe-mat380.1-0 ; 
       add_strobe-mat381.1-0 ; 
       add_strobe-mat382.1-0 ; 
       add_strobe-mat383.1-0 ; 
       add_strobe-mat396.1-0 ; 
       add_strobe-mat397.1-0 ; 
       add_strobe-mat398.1-0 ; 
       add_strobe-mat399.1-0 ; 
       add_strobe-mat400.1-0 ; 
       add_strobe-mat401.1-0 ; 
       add_strobe-mat402.1-0 ; 
       add_strobe-mat403.1-0 ; 
       add_strobe-mat404.1-0 ; 
       add_strobe-mat405.1-0 ; 
       add_strobe-mat406.1-0 ; 
       add_strobe-mat407.1-0 ; 
       add_strobe-mat409.1-0 ; 
       add_strobe-mat411.1-0 ; 
       add_strobe-mat413.1-0 ; 
       add_strobe-mat418.1-0 ; 
       add_strobe-mat420.1-0 ; 
       add_strobe-mat422.1-0 ; 
       add_strobe-mat424.1-0 ; 
       add_strobe-mat426.1-0 ; 
       add_strobe-mat428.1-0 ; 
       add_strobe-mat430.1-0 ; 
       add_strobe-mat432.1-0 ; 
       add_strobe-mat434.1-0 ; 
       add_strobe-mat435.1-0 ; 
       add_strobe-mat437.1-0 ; 
       add_strobe-mat439.1-0 ; 
       add_strobe-mat441.1-0 ; 
       add_strobe-mat443.1-0 ; 
       add_strobe-mat444.1-0 ; 
       add_strobe-mat445.1-0 ; 
       add_strobe-mat446.1-0 ; 
       add_strobe-mat447.1-0 ; 
       add_strobe-mat448.1-0 ; 
       add_strobe-mat449.1-0 ; 
       add_strobe-mat450.1-0 ; 
       add_strobe-mat451.1-0 ; 
       add_strobe-mat452.1-0 ; 
       add_strobe-mat453.1-0 ; 
       add_strobe-mat454.1-0 ; 
       add_strobe-mat455.1-0 ; 
       add_strobe-mat456.1-0 ; 
       add_strobe-mat457.1-0 ; 
       add_strobe-mat458.1-0 ; 
       add_strobe-mat459.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       cruiser4-cockpt.1-0 ; 
       cruiser4-garage1A.1-0 ; 
       cruiser4-garage1B.1-0 ; 
       cruiser4-garage1C.1-0 ; 
       cruiser4-garage1D.1-0 ; 
       cruiser4-garage1E.1-0 ; 
       cruiser4-head.2-0 ; 
       cruiser4-launch1.1-0 ; 
       cruiser4-launch2.1-0 ; 
       cruiser4-SS43_12.1-0 ; 
       cruiser4-SS43_12_1.1-0 ; 
       cruiser4-SS43_12_2.1-0 ; 
       cruiser4-SS43_12_3.1-0 ; 
       cruiser4-SS43_12_4.1-0 ; 
       cruiser4-SS43_12_5.1-0 ; 
       cruiser4-SS43_12_6.1-0 ; 
       cruiser4-SS43_12_7.1-0 ; 
       cruiser4-SS43_12_8.1-0 ; 
       cruiser4-SS43_13.1-0 ; 
       cruiser4-SS43_14.1-0 ; 
       cruiser4-SS43_15.1-0 ; 
       cruiser4-SS43_16.1-0 ; 
       cruiser4-SS43_17.1-0 ; 
       cruiser4-SS43_18.1-0 ; 
       cruiser4-SS44.1-0 ; 
       cruiser4-SS44_19.1-0 ; 
       cruiser4-SS44_19_1.1-0 ; 
       cruiser4-SS44_19_10.1-0 ; 
       cruiser4-SS44_19_11.1-0 ; 
       cruiser4-SS44_19_2.1-0 ; 
       cruiser4-SS44_19_23.1-0 ; 
       cruiser4-SS44_19_24.1-0 ; 
       cruiser4-SS44_19_25.1-0 ; 
       cruiser4-SS44_19_26.1-0 ; 
       cruiser4-SS44_19_27.1-0 ; 
       cruiser4-SS44_19_28.1-0 ; 
       cruiser4-SS44_19_29.1-0 ; 
       cruiser4-SS44_19_3.1-0 ; 
       cruiser4-SS44_19_30.1-0 ; 
       cruiser4-SS44_19_31.1-0 ; 
       cruiser4-SS44_19_32.1-0 ; 
       cruiser4-SS44_19_33.1-0 ; 
       cruiser4-SS44_19_4.1-0 ; 
       cruiser4-SS44_19_5.1-0 ; 
       cruiser4-SS44_19_6.1-0 ; 
       cruiser4-SS44_19_7.1-0 ; 
       cruiser4-SS44_19_8.1-0 ; 
       cruiser4-SS44_19_9.1-0 ; 
       cruiser4-SS44_21_2.1-0 ; 
       cruiser4-SS44_21_4.1-0 ; 
       cruiser4-SS44_21_6.1-0 ; 
       cruiser4-SS44_23.1-0 ; 
       cruiser4-SS44_25.1-0 ; 
       cruiser4-SS44_27.1-0 ; 
       cruiser4-SS44_29.1-0 ; 
       cruiser4-SS44_31.1-0 ; 
       cruiser4-SS44_33.1-0 ; 
       cruiser4-SS44_35.1-0 ; 
       cruiser4-SS44_37.1-0 ; 
       cruiser4-SS44_39.1-0 ; 
       cruiser4-SS44_41.1-0 ; 
       cruiser4-SS44_42.1-0 ; 
       cruiser4-SS44_44.1-0 ; 
       cruiser4-SS44_46.1-0 ; 
       cruiser4-SS44_48.1-0 ; 
       cruiser4-SS44_50.1-0 ; 
       cruiser4-thrust1.1-0 ; 
       cruiser4-thrust2.1-0 ; 
       cruiser4-torso.5-0 ROOT ; 
       cruiser4-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-add_strobe.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       add_strobe-t2d11.1-0 ; 
       add_strobe-t2d12.1-0 ; 
       add_strobe-t2d13.1-0 ; 
       add_strobe-t2d14.1-0 ; 
       add_strobe-t2d15.1-0 ; 
       add_strobe-t2d16.1-0 ; 
       add_strobe-t2d17.1-0 ; 
       add_strobe-t2d18.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 68 110 ; 
       1 68 110 ; 
       2 68 110 ; 
       3 68 110 ; 
       4 68 110 ; 
       5 68 110 ; 
       6 68 110 ; 
       7 68 110 ; 
       8 68 110 ; 
       9 68 110 ; 
       10 68 110 ; 
       11 68 110 ; 
       12 68 110 ; 
       13 68 110 ; 
       14 68 110 ; 
       15 68 110 ; 
       16 68 110 ; 
       17 68 110 ; 
       18 68 110 ; 
       19 68 110 ; 
       20 68 110 ; 
       21 68 110 ; 
       22 68 110 ; 
       23 68 110 ; 
       25 68 110 ; 
       26 68 110 ; 
       27 68 110 ; 
       28 68 110 ; 
       29 68 110 ; 
       30 68 110 ; 
       31 68 110 ; 
       32 68 110 ; 
       33 68 110 ; 
       34 68 110 ; 
       35 68 110 ; 
       36 68 110 ; 
       37 68 110 ; 
       38 68 110 ; 
       39 68 110 ; 
       40 68 110 ; 
       41 68 110 ; 
       42 68 110 ; 
       43 68 110 ; 
       44 68 110 ; 
       45 68 110 ; 
       46 68 110 ; 
       47 68 110 ; 
       48 68 110 ; 
       49 68 110 ; 
       50 68 110 ; 
       51 68 110 ; 
       52 68 110 ; 
       53 68 110 ; 
       54 68 110 ; 
       55 68 110 ; 
       56 68 110 ; 
       57 68 110 ; 
       58 68 110 ; 
       59 68 110 ; 
       60 68 110 ; 
       61 68 110 ; 
       62 68 110 ; 
       63 68 110 ; 
       64 68 110 ; 
       65 68 110 ; 
       66 68 110 ; 
       67 68 110 ; 
       69 68 110 ; 
       24 68 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       9 56 300 ; 
       10 50 300 ; 
       11 49 300 ; 
       12 51 300 ; 
       13 52 300 ; 
       14 53 300 ; 
       15 54 300 ; 
       16 55 300 ; 
       17 57 300 ; 
       18 58 300 ; 
       19 59 300 ; 
       20 60 300 ; 
       21 61 300 ; 
       22 62 300 ; 
       23 63 300 ; 
       25 8 300 ; 
       26 9 300 ; 
       27 18 300 ; 
       28 19 300 ; 
       29 10 300 ; 
       30 20 300 ; 
       31 21 300 ; 
       32 22 300 ; 
       33 23 300 ; 
       34 24 300 ; 
       35 26 300 ; 
       36 27 300 ; 
       37 12 300 ; 
       38 28 300 ; 
       39 29 300 ; 
       40 30 300 ; 
       41 31 300 ; 
       42 11 300 ; 
       43 13 300 ; 
       44 14 300 ; 
       45 15 300 ; 
       46 16 300 ; 
       47 17 300 ; 
       48 32 300 ; 
       49 33 300 ; 
       50 34 300 ; 
       51 25 300 ; 
       52 35 300 ; 
       53 36 300 ; 
       54 37 300 ; 
       55 38 300 ; 
       56 39 300 ; 
       57 40 300 ; 
       58 41 300 ; 
       59 42 300 ; 
       60 43 300 ; 
       61 44 300 ; 
       62 45 300 ; 
       63 46 300 ; 
       64 47 300 ; 
       65 48 300 ; 
       68 0 300 ; 
       68 4 300 ; 
       68 5 300 ; 
       68 6 300 ; 
       68 7 300 ; 
       24 64 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 7 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 6 401 ; 
       6 5 401 ; 
       7 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 152.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 137.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 135 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 140 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 142.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 145 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 147.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 150 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 155 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 157.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 160 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 162.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 165 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 167.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 170 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 72.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 80 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 85 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 87.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 90 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 92.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 95 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 97.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 100 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 102.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 105 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 107.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 110 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 112.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 115 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 117.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 120 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 122.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 125 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 127.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 130 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 132.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 87.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       69 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 172.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 75.74904 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 109 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 111.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 116.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 119 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 121.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 124 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 126.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 129 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 131.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 134 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 136.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 139 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 141.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 144 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 146.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 149 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 151.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 154 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 156.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 159 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 161.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 164 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 128.249 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 111.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 114 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 116.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 119 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 124 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 129 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 159 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 161.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 164 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 166.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 169 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 171.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 174 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 176.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 179 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 181.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 184 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 186.5 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 189 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 191.5 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 194 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 196.5 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
