SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       pre_mrege-cam_int1.45-0 ROOT ; 
       pre_mrege-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       new_body-default1.1-0 ; 
       new_body-default2.1-0 ; 
       new_body-default3.1-0 ; 
       new_body-default4.1-0 ; 
       new_body-default5.1-0 ; 
       new_body-default6.1-0 ; 
       new_body-default7.1-0 ; 
       new_body-mat12.1-0 ; 
       new_body-mat13.1-0 ; 
       new_body-mat14.1-0 ; 
       new_body-mat15.1-0 ; 
       new_body-mat16.1-0 ; 
       new_body-mat17.1-0 ; 
       new_body-mat18.1-0 ; 
       new_body-mat19.1-0 ; 
       new_body-mat20.1-0 ; 
       new_body-mat21.1-0 ; 
       new_body-mat22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       new_body-bmerge1.4-0 ROOT ; 
       new_body-bmerge2.1-0 ROOT ; 
       new_body-bmerge3.2-0 ROOT ; 
       new_body-cap504.1-0 ROOT ; 
       new_body-cap505.1-0 ROOT ; 
       new_body1-bmerge1.1-0 ROOT ; 
       rix_carrier1-cap504.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-new_body.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       new_body-t2d11.1-0 ; 
       new_body-t2d12.1-0 ; 
       new_body-t2d13.1-0 ; 
       new_body-t2d14.1-0 ; 
       new_body-t2d15.1-0 ; 
       new_body-t2d16.1-0 ; 
       new_body-t2d17.1-0 ; 
       new_body-t2d18.1-0 ; 
       new_body-t2d19.1-0 ; 
       new_body-t2d20.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       6 1 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       4 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       5 5 300 ; 
       2 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 35 0 0 DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 38.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 10.61186 0 10.29925 MPRFLG 0 ; 
       1 SCHEM 42.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 10.61186 0 -10.29925 MPRFLG 0 ; 
       5 SCHEM 45 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 10.61186 0 10.29925 MPRFLG 0 ; 
       2 SCHEM 48.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 10.61186 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 41.25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
