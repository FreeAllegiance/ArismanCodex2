SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       redo-cam_int1.21-0 ROOT ; 
       redo-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       redo-default7.5-0 ; 
       redo-default8.1-0 ; 
       redo-mat17.1-0 ; 
       redo-mat19.1-0 ; 
       redo-mat20.3-0 ; 
       redo-mat21.1-0 ; 
       redo-mat22.1-0 ; 
       redo-mat23.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       new_body-head.2-0 ; 
       new_body-torso.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-redo.21-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       redo-t2d11.3-0 ; 
       redo-t2d12.3-0 ; 
       redo-t2d13.7-0 ; 
       redo-t2d14.4-0 ; 
       redo-t2d15.2-0 ; 
       redo-t2d16.2-0 ; 
       redo-t2d17.2-0 ; 
       redo-t2d18.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 3 401 ; 
       5 6 401 ; 
       6 5 401 ; 
       7 4 401 ; 
       0 2 401 ; 
       1 7 401 ; 
       2 0 401 ; 
       3 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 10.61186 0 0 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
