SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       pre_mrege-cam_int1.36-0 ROOT ; 
       pre_mrege-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       rix_carrier-default1.8-0 ; 
       rix_carrier-mat1.7-0 ; 
       rix_carrier-mat10.1-0 ; 
       rix_carrier-mat2.6-0 ; 
       rix_carrier-mat3.5-0 ; 
       rix_carrier-mat4.5-0 ; 
       rix_carrier-mat5.5-0 ; 
       rix_carrier-mat6.5-0 ; 
       rix_carrier-mat7.4-0 ; 
       rix_carrier-mat8.3-0 ; 
       rix_carrier-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       rix_carrier-cap504.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-rix_carrier.25-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       rix_carrier-t2d1.14-0 ; 
       rix_carrier-t2d10.1-0 ; 
       rix_carrier-t2d2.13-0 ; 
       rix_carrier-t2d3.12-0 ; 
       rix_carrier-t2d4.12-0 ; 
       rix_carrier-t2d5.11-0 ; 
       rix_carrier-t2d6.9-0 ; 
       rix_carrier-t2d7.5-0 ; 
       rix_carrier-t2d8.5-0 ; 
       rix_carrier-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
