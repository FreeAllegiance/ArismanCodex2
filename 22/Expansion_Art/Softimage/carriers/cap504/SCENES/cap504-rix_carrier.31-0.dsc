SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_carrier-cam_int1.4-0 ROOT ; 
       rix_carrier-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       rix_carrier-default7.1-0 ; 
       rix_carrier-default8.1-0 ; 
       rix_carrier-mat17.1-0 ; 
       rix_carrier-mat19.1-0 ; 
       rix_carrier-mat20.1-0 ; 
       rix_carrier-mat21.1-0 ; 
       rix_carrier-mat22.1-0 ; 
       rix_carrier-mat23.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       new_body-cockpt.1-0 ; 
       new_body-garage1A.1-0 ; 
       new_body-garage1B.1-0 ; 
       new_body-garage1C.1-0 ; 
       new_body-garage1D.1-0 ; 
       new_body-garage1E.1-0 ; 
       new_body-head.2-0 ; 
       new_body-launch1.1-0 ; 
       new_body-launch2.1-0 ; 
       new_body-thrust1.1-0 ; 
       new_body-thrust2.1-0 ; 
       new_body-torso.13-0 ROOT ; 
       new_body-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-rix_carrier.31-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       rix_carrier-t2d11.2-0 ; 
       rix_carrier-t2d12.2-0 ; 
       rix_carrier-t2d13.2-0 ; 
       rix_carrier-t2d14.2-0 ; 
       rix_carrier-t2d15.2-0 ; 
       rix_carrier-t2d16.2-0 ; 
       rix_carrier-t2d17.2-0 ; 
       rix_carrier-t2d18.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 11 110 ; 
       12 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       0 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       2 11 110 ; 
       1 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       11 0 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 7 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 6 401 ; 
       6 5 401 ; 
       7 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
