SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_carrier-cam_int1.9-0 ROOT ; 
       rix_carrier-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       rix_carrier-default7.1-0 ; 
       rix_carrier-default8.1-0 ; 
       rix_carrier-mat17.1-0 ; 
       rix_carrier-mat19.1-0 ; 
       rix_carrier-mat20.1-0 ; 
       rix_carrier-mat21.1-0 ; 
       rix_carrier-mat22.1-0 ; 
       rix_carrier-mat23.1-0 ; 
       rix_carrier-mat304.1-0 ; 
       rix_carrier-mat373.1-0 ; 
       rix_carrier-mat374.1-0 ; 
       rix_carrier-mat375.1-0 ; 
       rix_carrier-mat376.1-0 ; 
       rix_carrier-mat377.1-0 ; 
       rix_carrier-mat378.1-0 ; 
       rix_carrier-mat379.1-0 ; 
       rix_carrier-mat380.1-0 ; 
       rix_carrier-mat381.1-0 ; 
       rix_carrier-mat382.1-0 ; 
       rix_carrier-mat383.1-0 ; 
       rix_carrier-mat396.1-0 ; 
       rix_carrier-mat397.1-0 ; 
       rix_carrier-mat398.1-0 ; 
       rix_carrier-mat399.1-0 ; 
       rix_carrier-mat400.1-0 ; 
       rix_carrier-mat401.1-0 ; 
       rix_carrier-mat402.1-0 ; 
       rix_carrier-mat403.1-0 ; 
       rix_carrier-mat404.1-0 ; 
       rix_carrier-mat405.1-0 ; 
       rix_carrier-mat406.1-0 ; 
       rix_carrier-mat407.1-0 ; 
       rix_carrier-mat409.1-0 ; 
       rix_carrier-mat411.1-0 ; 
       rix_carrier-mat413.1-0 ; 
       rix_carrier-mat418.1-0 ; 
       rix_carrier-mat420.1-0 ; 
       rix_carrier-mat422.1-0 ; 
       rix_carrier-mat424.1-0 ; 
       rix_carrier-mat426.1-0 ; 
       rix_carrier-mat428.1-0 ; 
       rix_carrier-mat430.1-0 ; 
       rix_carrier-mat432.1-0 ; 
       rix_carrier-mat434.1-0 ; 
       rix_carrier-mat435.1-0 ; 
       rix_carrier-mat437.1-0 ; 
       rix_carrier-mat439.1-0 ; 
       rix_carrier-mat441.1-0 ; 
       rix_carrier-mat443.1-0 ; 
       rix_carrier-mat444.1-0 ; 
       rix_carrier-mat445.1-0 ; 
       rix_carrier-mat446.1-0 ; 
       rix_carrier-mat447.1-0 ; 
       rix_carrier-mat448.1-0 ; 
       rix_carrier-mat449.1-0 ; 
       rix_carrier-mat450.1-0 ; 
       rix_carrier-mat451.1-0 ; 
       rix_carrier-mat452.1-0 ; 
       rix_carrier-mat453.1-0 ; 
       rix_carrier-mat454.1-0 ; 
       rix_carrier-mat455.1-0 ; 
       rix_carrier-mat456.1-0 ; 
       rix_carrier-mat457.1-0 ; 
       rix_carrier-mat458.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 69     
       cruiser4-cockpt.1-0 ; 
       cruiser4-garage1A.1-0 ; 
       cruiser4-garage1B.1-0 ; 
       cruiser4-garage1C.1-0 ; 
       cruiser4-garage1D.1-0 ; 
       cruiser4-garage1E.1-0 ; 
       cruiser4-head.2-0 ; 
       cruiser4-launch1.1-0 ; 
       cruiser4-launch2.1-0 ; 
       cruiser4-SS43_12.1-0 ; 
       cruiser4-SS43_12_1.1-0 ; 
       cruiser4-SS43_12_2.1-0 ; 
       cruiser4-SS43_12_3.1-0 ; 
       cruiser4-SS43_12_4.1-0 ; 
       cruiser4-SS43_12_5.1-0 ; 
       cruiser4-SS43_12_6.1-0 ; 
       cruiser4-SS43_12_7.1-0 ; 
       cruiser4-SS43_12_8.1-0 ; 
       cruiser4-SS43_13.1-0 ; 
       cruiser4-SS43_14.1-0 ; 
       cruiser4-SS43_15.1-0 ; 
       cruiser4-SS43_16.1-0 ; 
       cruiser4-SS43_17.1-0 ; 
       cruiser4-SS43_18.1-0 ; 
       cruiser4-SS44_19.1-0 ; 
       cruiser4-SS44_19_1.1-0 ; 
       cruiser4-SS44_19_10.1-0 ; 
       cruiser4-SS44_19_11.1-0 ; 
       cruiser4-SS44_19_2.1-0 ; 
       cruiser4-SS44_19_23.1-0 ; 
       cruiser4-SS44_19_24.1-0 ; 
       cruiser4-SS44_19_25.1-0 ; 
       cruiser4-SS44_19_26.1-0 ; 
       cruiser4-SS44_19_27.1-0 ; 
       cruiser4-SS44_19_28.1-0 ; 
       cruiser4-SS44_19_29.1-0 ; 
       cruiser4-SS44_19_3.1-0 ; 
       cruiser4-SS44_19_30.1-0 ; 
       cruiser4-SS44_19_31.1-0 ; 
       cruiser4-SS44_19_32.1-0 ; 
       cruiser4-SS44_19_33.1-0 ; 
       cruiser4-SS44_19_4.1-0 ; 
       cruiser4-SS44_19_5.1-0 ; 
       cruiser4-SS44_19_6.1-0 ; 
       cruiser4-SS44_19_7.1-0 ; 
       cruiser4-SS44_19_8.1-0 ; 
       cruiser4-SS44_19_9.1-0 ; 
       cruiser4-SS44_21_2.1-0 ; 
       cruiser4-SS44_21_4.1-0 ; 
       cruiser4-SS44_21_6.1-0 ; 
       cruiser4-SS44_23.1-0 ; 
       cruiser4-SS44_25.1-0 ; 
       cruiser4-SS44_27.1-0 ; 
       cruiser4-SS44_29.1-0 ; 
       cruiser4-SS44_31.1-0 ; 
       cruiser4-SS44_33.1-0 ; 
       cruiser4-SS44_35.1-0 ; 
       cruiser4-SS44_37.1-0 ; 
       cruiser4-SS44_39.1-0 ; 
       cruiser4-SS44_41.1-0 ; 
       cruiser4-SS44_42.1-0 ; 
       cruiser4-SS44_44.1-0 ; 
       cruiser4-SS44_46.1-0 ; 
       cruiser4-SS44_48.1-0 ; 
       cruiser4-SS44_50.1-0 ; 
       cruiser4-thrust1.1-0 ; 
       cruiser4-thrust2.1-0 ; 
       cruiser4-torso.4-0 ROOT ; 
       cruiser4-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-rix_carrier.36-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       rix_carrier-t2d11.3-0 ; 
       rix_carrier-t2d12.3-0 ; 
       rix_carrier-t2d13.3-0 ; 
       rix_carrier-t2d14.3-0 ; 
       rix_carrier-t2d15.3-0 ; 
       rix_carrier-t2d16.3-0 ; 
       rix_carrier-t2d17.3-0 ; 
       rix_carrier-t2d18.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 67 110 ; 
       1 67 110 ; 
       2 67 110 ; 
       3 67 110 ; 
       4 67 110 ; 
       5 67 110 ; 
       6 67 110 ; 
       7 67 110 ; 
       8 67 110 ; 
       9 67 110 ; 
       10 67 110 ; 
       11 67 110 ; 
       12 67 110 ; 
       13 67 110 ; 
       14 67 110 ; 
       15 67 110 ; 
       16 67 110 ; 
       17 67 110 ; 
       18 67 110 ; 
       19 67 110 ; 
       20 67 110 ; 
       21 67 110 ; 
       22 67 110 ; 
       23 67 110 ; 
       24 67 110 ; 
       25 67 110 ; 
       26 67 110 ; 
       27 67 110 ; 
       28 67 110 ; 
       29 67 110 ; 
       30 67 110 ; 
       31 67 110 ; 
       32 67 110 ; 
       33 67 110 ; 
       34 67 110 ; 
       35 67 110 ; 
       36 67 110 ; 
       37 67 110 ; 
       38 67 110 ; 
       39 67 110 ; 
       40 67 110 ; 
       41 67 110 ; 
       42 67 110 ; 
       43 67 110 ; 
       44 67 110 ; 
       45 67 110 ; 
       46 67 110 ; 
       47 67 110 ; 
       48 67 110 ; 
       49 67 110 ; 
       50 67 110 ; 
       51 67 110 ; 
       52 67 110 ; 
       53 67 110 ; 
       54 67 110 ; 
       55 67 110 ; 
       56 67 110 ; 
       57 67 110 ; 
       58 67 110 ; 
       59 67 110 ; 
       60 67 110 ; 
       61 67 110 ; 
       62 67 110 ; 
       63 67 110 ; 
       64 67 110 ; 
       65 67 110 ; 
       66 67 110 ; 
       68 67 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       9 56 300 ; 
       10 50 300 ; 
       11 49 300 ; 
       12 51 300 ; 
       13 52 300 ; 
       14 53 300 ; 
       15 54 300 ; 
       16 55 300 ; 
       17 57 300 ; 
       18 58 300 ; 
       19 59 300 ; 
       20 60 300 ; 
       21 61 300 ; 
       22 62 300 ; 
       23 63 300 ; 
       24 8 300 ; 
       25 9 300 ; 
       26 18 300 ; 
       27 19 300 ; 
       28 10 300 ; 
       29 20 300 ; 
       30 21 300 ; 
       31 22 300 ; 
       32 23 300 ; 
       33 24 300 ; 
       34 26 300 ; 
       35 27 300 ; 
       36 12 300 ; 
       37 28 300 ; 
       38 29 300 ; 
       39 30 300 ; 
       40 31 300 ; 
       41 11 300 ; 
       42 13 300 ; 
       43 14 300 ; 
       44 15 300 ; 
       45 16 300 ; 
       46 17 300 ; 
       47 32 300 ; 
       48 33 300 ; 
       49 34 300 ; 
       50 25 300 ; 
       51 35 300 ; 
       52 36 300 ; 
       53 37 300 ; 
       54 38 300 ; 
       55 39 300 ; 
       56 40 300 ; 
       57 41 300 ; 
       58 42 300 ; 
       59 43 300 ; 
       60 44 300 ; 
       61 45 300 ; 
       62 46 300 ; 
       63 47 300 ; 
       64 48 300 ; 
       67 0 300 ; 
       67 4 300 ; 
       67 5 300 ; 
       67 6 300 ; 
       67 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 7 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 6 401 ; 
       6 5 401 ; 
       7 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 155 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 140 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 137.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 142.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 145 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 147.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 150 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 152.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 157.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 160 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 162.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 165 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 167.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 170 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 172.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 80 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 85 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 87.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 90 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 95 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 97.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 100 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 102.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 105 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 107.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 110 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 112.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 115 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 117.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 120 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 122.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 125 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 127.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 130 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 132.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 135 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       65 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 87.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       68 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 174 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 75.74904 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 109 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 111.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 116.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 119 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 121.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 124 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 126.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 129 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 131.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 134 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 136.5 0 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 139 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 141.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 144 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 146.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 149 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 151.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 154 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 156.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 159 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 161.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 164 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 128.249 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 99 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 111.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 114 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 116.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 119 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 124 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 129 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 134 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 159 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 161.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 164 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 166.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 169 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 171.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 174 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 176.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 179 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 181.5 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 184 4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 186.5 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 189 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 191.5 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 194 6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 174 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
