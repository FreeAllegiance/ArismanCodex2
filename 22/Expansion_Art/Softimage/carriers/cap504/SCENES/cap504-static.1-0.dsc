SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.1-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       static-default7.1-0 ; 
       static-default8.1-0 ; 
       static-mat17.1-0 ; 
       static-mat19.1-0 ; 
       static-mat20.1-0 ; 
       static-mat21.1-0 ; 
       static-mat22.1-0 ; 
       static-mat23.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       new_body-head.2-0 ; 
       new_body-torso.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/cap504 ; 
       //research/root/federation/Expansion_Art/Softimage/carriers/cap504/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap504-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 0 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 7 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 6 401 ; 
       6 5 401 ; 
       7 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
