SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       from_x-cam_int1.1-0 ROOT ; 
       from_x-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       from_x-mat1.1-0 ; 
       from_x-mat2.1-0 ; 
       from_x-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       from_x-cube1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       pointer-from_x.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
