SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom03_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       check_nulls-bottom.1-0 ; 
       check_nulls-cockpit_side.1-0 ; 
       check_nulls-cockpit_top.1-0 ; 
       check_nulls-engine1.1-0 ; 
       check_nulls-exterior_intake_duct1.1-0 ; 
       check_nulls-glows.1-0 ; 
       check_nulls-inside_landing_bays.1-0 ; 
       check_nulls-intake_ducts_top-n-bottom.1-0 ; 
       check_nulls-intake1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat11.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat6.1-0 ; 
       check_nulls-mat7.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-Rear.1-0 ; 
       check_nulls-side_glow1.1-0 ; 
       check_nulls-top.1-0 ; 
       check_nulls-wing.1-0 ; 
       tweak-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.3-0 ROOT ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       check_nulls-t2d10.1-0 ; 
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d16.1-0 ; 
       check_nulls-t2d17.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d19.1-0 ; 
       check_nulls-t2d2_1.1-0 ; 
       check_nulls-t2d20.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d22.1-0 ; 
       check_nulls-t2d23.1-0 ; 
       check_nulls-t2d24.1-0 ; 
       check_nulls-t2d4.1-0 ; 
       check_nulls-t2d5.1-0 ; 
       check_nulls-t2d6.1-0 ; 
       check_nulls-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 0 110 ; 
       5 2 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 12 110 ; 
       9 12 110 ; 
       10 2 110 ; 
       11 0 110 ; 
       12 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 19 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 17 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 20 300 ; 
       3 15 300 ; 
       5 14 300 ; 
       6 10 300 ; 
       7 11 300 ; 
       8 12 300 ; 
       9 13 300 ; 
       10 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 17 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 16 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 10 401 ; 
       13 6 401 ; 
       14 9 401 ; 
       15 11 401 ; 
       16 7 401 ; 
       17 3 401 ; 
       18 19 401 ; 
       19 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       11 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 30 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
