SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 78     
       animation-back_shelf.1-0 ; 
       animation-bottom.1-0 ; 
       animation-default10.1-0 ; 
       animation-default11.1-0 ; 
       animation-default12.1-0 ; 
       animation-default13.1-0 ; 
       animation-global.1-0 ; 
       animation-lower_back_panel.1-0 ; 
       animation-mat171.1-0 ; 
       animation-mat173.1-0 ; 
       animation-mat174.1-0 ; 
       animation-mat175.1-0 ; 
       animation-mat180.1-0 ; 
       animation-mat181.1-0 ; 
       animation-mat182.1-0 ; 
       animation-mat185.1-0 ; 
       animation-mat186.1-0 ; 
       animation-mat192.1-0 ; 
       animation-mat193.1-0 ; 
       animation-mat194.1-0 ; 
       animation-mat195.1-0 ; 
       animation-mat196.1-0 ; 
       animation-mat198.1-0 ; 
       animation-mat199.1-0 ; 
       animation-mat200.1-0 ; 
       animation-mat201.1-0 ; 
       animation-mat202.1-0 ; 
       animation-mat203.1-0 ; 
       animation-mat209.1-0 ; 
       animation-mat210.1-0 ; 
       animation-mat211.1-0 ; 
       animation-mat212.1-0 ; 
       animation-mat213.1-0 ; 
       animation-mat214.1-0 ; 
       animation-mat215.1-0 ; 
       animation-mat216.1-0 ; 
       animation-mat217.1-0 ; 
       animation-mat218.1-0 ; 
       animation-mat219.1-0 ; 
       animation-mat220.1-0 ; 
       animation-mat221.1-0 ; 
       animation-mat222.1-0 ; 
       animation-mat236.1-0 ; 
       animation-mat237.1-0 ; 
       animation-mat239.1-0 ; 
       animation-mat240.1-0 ; 
       animation-mat242.1-0 ; 
       animation-mat255.1-0 ; 
       animation-mat257.1-0 ; 
       animation-mat258.1-0 ; 
       animation-mat263.1-0 ; 
       animation-mat264.1-0 ; 
       animation-mat266.1-0 ; 
       animation-mat267.1-0 ; 
       animation-mat268.1-0 ; 
       animation-mat269.1-0 ; 
       animation-mat270.1-0 ; 
       animation-mat271.1-0 ; 
       animation-mat272.1-0 ; 
       animation-mat273.1-0 ; 
       animation-mat274.1-0 ; 
       animation-mat275.1-0 ; 
       animation-mat276.1-0 ; 
       animation-mat277.1-0 ; 
       animation-mat278.1-0 ; 
       animation-mat279.1-0 ; 
       animation-mat280.1-0 ; 
       animation-mat281.1-0 ; 
       animation-mat46.1-0 ; 
       animation-mat69.1-0 ; 
       animation-side1.1-0 ; 
       animation-top.1-0 ; 
       animation-under_nose.1-0 ; 
       il_light_bomber_sT-mat99.1-0 ; 
       mess_with_landing_gear-mat283.1-0 ; 
       mess_with_landing_gear-mat284.1-0 ; 
       static-mat285.1-0 ; 
       static-mat286.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       bom07-afuselg0.1-0 ; 
       bom07-bom07_3_2.2-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-l-gun.1-0 ; 
       bom07-lbaydoor.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-lthruster.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-r-gun.1-0 ; 
       bom07-rbaydoor.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthruster.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       animation-t2d101.1-0 ; 
       animation-t2d102.1-0 ; 
       animation-t2d105.1-0 ; 
       animation-t2d106.1-0 ; 
       animation-t2d111.1-0 ; 
       animation-t2d112.1-0 ; 
       animation-t2d113.1-0 ; 
       animation-t2d114.1-0 ; 
       animation-t2d115.1-0 ; 
       animation-t2d116.1-0 ; 
       animation-t2d117.1-0 ; 
       animation-t2d118.1-0 ; 
       animation-t2d119.1-0 ; 
       animation-t2d120.1-0 ; 
       animation-t2d121.1-0 ; 
       animation-t2d125.1-0 ; 
       animation-t2d127.1-0 ; 
       animation-t2d128.1-0 ; 
       animation-t2d129.1-0 ; 
       animation-t2d130.1-0 ; 
       animation-t2d131.1-0 ; 
       animation-t2d132.1-0 ; 
       animation-t2d133.1-0 ; 
       animation-t2d134.1-0 ; 
       animation-t2d135.1-0 ; 
       animation-t2d149.1-0 ; 
       animation-t2d150.1-0 ; 
       animation-t2d151.1-0 ; 
       animation-t2d153.1-0 ; 
       animation-t2d154.1-0 ; 
       animation-t2d155.1-0 ; 
       animation-t2d168.1-0 ; 
       animation-t2d169.1-0 ; 
       animation-t2d170.1-0 ; 
       animation-t2d175.1-0 ; 
       animation-t2d176.1-0 ; 
       animation-t2d177.1-0 ; 
       animation-t2d178.1-0 ; 
       animation-t2d179.1-0 ; 
       animation-t2d180.1-0 ; 
       animation-t2d181.1-0 ; 
       animation-t2d182.1-0 ; 
       animation-t2d183.1-0 ; 
       animation-t2d184.1-0 ; 
       animation-t2d185.1-0 ; 
       animation-t2d186.1-0 ; 
       animation-t2d187.1-0 ; 
       animation-t2d188.1-0 ; 
       animation-t2d189.1-0 ; 
       animation-t2d66.1-0 ; 
       animation-t2d83.1-0 ; 
       animation-t2d85.1-0 ; 
       animation-t2d95.1-0 ; 
       animation-t2d96.1-0 ; 
       animation-t2d97.1-0 ; 
       mess_with_landing_gear-t2d191.1-0 ; 
       mess_with_landing_gear-t2d192.1-0 ; 
       static-t2d193.1-0 ; 
       static-t2d194.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 30 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 4 110 ; 
       6 9 110 ; 
       7 15 110 ; 
       8 2 110 ; 
       9 3 110 ; 
       10 4 110 ; 
       11 9 110 ; 
       12 14 110 ; 
       13 32 110 ; 
       14 13 110 ; 
       15 0 110 ; 
       16 19 110 ; 
       17 15 110 ; 
       18 2 110 ; 
       19 3 110 ; 
       20 4 110 ; 
       21 19 110 ; 
       22 24 110 ; 
       23 32 110 ; 
       24 23 110 ; 
       25 10 110 ; 
       26 20 110 ; 
       27 3 110 ; 
       28 11 110 ; 
       29 21 110 ; 
       30 0 110 ; 
       31 30 110 ; 
       32 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 48 300 ; 
       2 52 300 ; 
       3 6 300 ; 
       3 71 300 ; 
       3 1 300 ; 
       3 0 300 ; 
       3 7 300 ; 
       3 72 300 ; 
       3 70 300 ; 
       5 3 300 ; 
       5 44 300 ; 
       6 69 300 ; 
       7 74 300 ; 
       9 68 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 45 300 ; 
       10 76 300 ; 
       11 5 300 ; 
       11 63 300 ; 
       11 64 300 ; 
       11 65 300 ; 
       12 2 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 42 300 ; 
       13 35 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       14 39 300 ; 
       14 40 300 ; 
       14 41 300 ; 
       15 47 300 ; 
       15 49 300 ; 
       15 50 300 ; 
       15 51 300 ; 
       16 66 300 ; 
       17 75 300 ; 
       19 14 300 ; 
       19 15 300 ; 
       19 16 300 ; 
       19 46 300 ; 
       20 77 300 ; 
       21 4 300 ; 
       21 60 300 ; 
       21 61 300 ; 
       21 62 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       22 20 300 ; 
       22 21 300 ; 
       22 43 300 ; 
       23 30 300 ; 
       23 31 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       24 28 300 ; 
       24 29 300 ; 
       24 34 300 ; 
       25 54 300 ; 
       26 55 300 ; 
       27 53 300 ; 
       28 57 300 ; 
       29 56 300 ; 
       30 73 300 ; 
       30 58 300 ; 
       30 59 300 ; 
       30 67 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       76 57 401 ; 
       77 58 401 ; 
       0 8 401 ; 
       1 50 401 ; 
       7 25 401 ; 
       9 52 401 ; 
       10 53 401 ; 
       11 54 401 ; 
       12 0 401 ; 
       13 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 12 401 ; 
       26 13 401 ; 
       27 14 401 ; 
       29 15 401 ; 
       31 16 401 ; 
       32 17 401 ; 
       33 18 401 ; 
       34 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 29 401 ; 
       46 30 401 ; 
       47 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 37 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       63 44 401 ; 
       64 45 401 ; 
       65 46 401 ; 
       66 47 401 ; 
       67 48 401 ; 
       69 51 401 ; 
       70 38 401 ; 
       71 49 401 ; 
       72 36 401 ; 
       74 56 401 ; 
       75 55 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -2 0 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 SRT 1 1 1 0.08377581 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -6 0 MPRFLG 0 ; 
       15 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 MPRFLG 0 ; 
       18 SCHEM 35 -8 0 MPRFLG 0 ; 
       19 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       20 SCHEM 15 -4 0 MPRFLG 0 ; 
       21 SCHEM 20 -6 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 15 -6 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 25 -8 0 MPRFLG 0 ; 
       29 SCHEM 20 -8 0 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 40 -6 0 MPRFLG 0 ; 
       32 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       76 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       57 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 41.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
