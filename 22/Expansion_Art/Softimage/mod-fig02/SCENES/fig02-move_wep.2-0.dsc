SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       move_wep-fig02.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       destroy-default5_1_1.1-0 ; 
       destroy-mat10_1.1-0 ; 
       destroy-mat11_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat14_1.1-0 ; 
       destroy-mat15_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat36_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       destroy-mat40_1.1-0 ; 
       destroy-mat41_1.1-0 ; 
       destroy-mat42_1.1-0 ; 
       destroy-mat43_1.1-0 ; 
       destroy-mat5_1.1-0 ; 
       destroy-mat6_1.1-0 ; 
       done_lite-.5.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat44.1-0 ; 
       move_wep-mat46.1-0 ; 
       move_wep-mat47.1-0 ; 
       move_wep-mat63.1-0 ; 
       move_wep-mat64.1-0 ; 
       move_wep-mat65.1-0 ; 
       move_wep-mat66.1-0 ; 
       move_wep-mat69.1-0 ; 
       move_wep-mat70.1-0 ; 
       move_wep-mat71.1-0 ; 
       move_wep-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       move_wep-afuselg.1-0 ; 
       move_wep-cockpt.1-0 ; 
       move_wep-ffuselg.1-0 ; 
       move_wep-fgun.1-0 ; 
       move_wep-fig02.1-0 ROOT ; 
       move_wep-finzzz0.1-0 ; 
       move_wep-finzzz1.1-0 ; 
       move_wep-fwepemt.1-0 ; 
       move_wep-fwepmnt.1-0 ; 
       move_wep-l-gun.1-0 ; 
       move_wep-lbooster.1-0 ; 
       move_wep-lfinzzz.1-0 ; 
       move_wep-lsmoke.1-0 ; 
       move_wep-lthrust.1-0 ; 
       move_wep-lwepatt.1-0 ; 
       move_wep-lwepemt.1-0 ; 
       move_wep-lwingzz0.1-0 ; 
       move_wep-lwingzz1.1-0 ; 
       move_wep-lwingzz2.1-0 ; 
       move_wep-lwingzz3.1-0 ; 
       move_wep-lwingzz4.1-0 ; 
       move_wep-missemt.1-0 ; 
       move_wep-r-gun.1-0 ; 
       move_wep-rbooster.1-0 ; 
       move_wep-rfinzzz.1-0 ; 
       move_wep-rsmoke.1-0 ; 
       move_wep-rthrust.1-0 ; 
       move_wep-rwepatt.1-0 ; 
       move_wep-rwepemt.1-0 ; 
       move_wep-rwingzz0.1-0 ; 
       move_wep-rwingzz1.1-0 ; 
       move_wep-rwingzz2.1-0 ; 
       move_wep-rwingzz3.1-0 ; 
       move_wep-rwingzz4.1-0 ; 
       move_wep-SSal.1-0 ; 
       move_wep-SSar.1-0 ; 
       move_wep-SSf.1-0 ; 
       move_wep-SSr.1-0 ; 
       move_wep-SSr1.1-0 ; 
       move_wep-tgun.1-0 ; 
       move_wep-thrust0.1-0 ; 
       move_wep-trail.1-0 ; 
       move_wep-twepemt.1-0 ; 
       move_wep-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-move_wep.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.1-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.1-0 ; 
       destroy-t2d3_1.1-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d34_1.1-0 ; 
       destroy-t2d35_1.1-0 ; 
       destroy-t2d36_1.1-0 ; 
       destroy-t2d37_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.1-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.1-0 ; 
       destroy-t2d6_1.1-0 ; 
       destroy-t2d7_1.1-0 ; 
       destroy-t2d8_1.1-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       move_wep-t2d42.1-0 ; 
       move_wep-t2d43.1-0 ; 
       move_wep-t2d44.1-0 ; 
       move_wep-t2d45.1-0 ; 
       move_wep-t2d85.1-0 ; 
       move_wep-t2d86.1-0 ; 
       move_wep-t2d87.1-0 ; 
       move_wep-t2d88.1-0 ; 
       move_wep-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 40 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 10 110 ; 
       14 4 110 ; 
       15 9 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 4 110 ; 
       22 2 110 ; 
       23 40 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 23 110 ; 
       27 4 110 ; 
       28 22 110 ; 
       29 4 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 11 110 ; 
       35 24 110 ; 
       36 2 110 ; 
       37 31 110 ; 
       38 18 110 ; 
       39 2 110 ; 
       40 4 110 ; 
       41 4 110 ; 
       42 39 110 ; 
       43 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       3 41 300 ; 
       6 20 300 ; 
       9 38 300 ; 
       10 3 300 ; 
       10 22 300 ; 
       10 32 300 ; 
       11 5 300 ; 
       11 35 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 26 300 ; 
       19 28 300 ; 
       20 27 300 ; 
       22 39 300 ; 
       23 4 300 ; 
       23 21 300 ; 
       23 33 300 ; 
       24 6 300 ; 
       24 36 300 ; 
       31 0 300 ; 
       31 25 300 ; 
       32 1 300 ; 
       33 2 300 ; 
       34 31 300 ; 
       35 30 300 ; 
       36 29 300 ; 
       37 34 300 ; 
       38 37 300 ; 
       39 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 26 400 ; 
       2 1 400 ; 
       6 7 400 ; 
       10 10 400 ; 
       11 5 400 ; 
       19 15 400 ; 
       20 14 400 ; 
       23 11 400 ; 
       24 6 400 ; 
       32 16 400 ; 
       33 17 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 0 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 18 401 ; 
       25 21 401 ; 
       26 29 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 35 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 61.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 53.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 66.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 8.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 36.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       21 SCHEM 68.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 11.25 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       24 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 63.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       28 SCHEM 51.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       30 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       33 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       34 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       35 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       36 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       38 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       39 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       40 SCHEM 10 -2 0 MPRFLG 0 ; 
       41 SCHEM 58.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 48.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
