SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       static-fig02.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       destroy-default5_1_1.1-0 ; 
       destroy-mat10_1.1-0 ; 
       destroy-mat11_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat14_1.1-0 ; 
       destroy-mat15_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat36_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       destroy-mat40_1.1-0 ; 
       destroy-mat41_1.1-0 ; 
       destroy-mat42_1.1-0 ; 
       destroy-mat43_1.1-0 ; 
       destroy-mat5_1.1-0 ; 
       destroy-mat6_1.1-0 ; 
       done_lite-.5.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat44.1-0 ; 
       static-mat46.1-0 ; 
       static-mat47.1-0 ; 
       static-mat63.1-0 ; 
       static-mat64.1-0 ; 
       static-mat65.1-0 ; 
       static-mat66.1-0 ; 
       static-mat69.1-0 ; 
       static-mat70.1-0 ; 
       static-mat71.1-0 ; 
       static-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       static-afuselg.1-0 ; 
       static-ffuselg.1-0 ; 
       static-fgun.1-0 ; 
       static-fig02.1-0 ROOT ; 
       static-finzzz0.1-0 ; 
       static-finzzz1.1-0 ; 
       static-fwepmnt.1-0 ; 
       static-l-gun.1-0 ; 
       static-lbooster.1-0 ; 
       static-lfinzzz.1-0 ; 
       static-lwepatt.1-0 ; 
       static-lwingzz0.1-0 ; 
       static-lwingzz1.1-0 ; 
       static-lwingzz2.1-0 ; 
       static-lwingzz3.1-0 ; 
       static-lwingzz4.1-0 ; 
       static-r-gun.1-0 ; 
       static-rbooster.1-0 ; 
       static-rfinzzz.1-0 ; 
       static-rwepatt.1-0 ; 
       static-rwingzz0.1-0 ; 
       static-rwingzz1.1-0 ; 
       static-rwingzz2.1-0 ; 
       static-rwingzz3.1-0 ; 
       static-rwingzz4.1-0 ; 
       static-SSal.1-0 ; 
       static-SSar.1-0 ; 
       static-SSf.1-0 ; 
       static-SSr.1-0 ; 
       static-SSr1.1-0 ; 
       static-tgun.1-0 ; 
       static-thrust0.1-0 ; 
       static-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.1-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.1-0 ; 
       destroy-t2d3_1.1-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d34_1.1-0 ; 
       destroy-t2d35_1.1-0 ; 
       destroy-t2d36_1.1-0 ; 
       destroy-t2d37_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.1-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.1-0 ; 
       destroy-t2d6_1.1-0 ; 
       destroy-t2d7_1.1-0 ; 
       destroy-t2d8_1.1-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       static-t2d42.1-0 ; 
       static-t2d43.1-0 ; 
       static-t2d44.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d85.1-0 ; 
       static-t2d86.1-0 ; 
       static-t2d87.1-0 ; 
       static-t2d88.1-0 ; 
       static-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 31 110 ; 
       9 4 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 1 110 ; 
       17 31 110 ; 
       18 4 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 9 110 ; 
       26 18 110 ; 
       27 1 110 ; 
       28 22 110 ; 
       29 13 110 ; 
       30 1 110 ; 
       31 3 110 ; 
       32 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 41 300 ; 
       5 20 300 ; 
       7 38 300 ; 
       8 3 300 ; 
       8 22 300 ; 
       8 32 300 ; 
       9 5 300 ; 
       9 35 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       13 26 300 ; 
       14 28 300 ; 
       15 27 300 ; 
       16 39 300 ; 
       17 4 300 ; 
       17 21 300 ; 
       17 33 300 ; 
       18 6 300 ; 
       18 36 300 ; 
       22 0 300 ; 
       22 25 300 ; 
       23 1 300 ; 
       24 2 300 ; 
       25 31 300 ; 
       26 30 300 ; 
       27 29 300 ; 
       28 34 300 ; 
       29 37 300 ; 
       30 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 26 400 ; 
       1 1 400 ; 
       5 7 400 ; 
       8 10 400 ; 
       9 5 400 ; 
       14 15 400 ; 
       15 14 400 ; 
       17 11 400 ; 
       18 6 400 ; 
       23 16 400 ; 
       24 17 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 0 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 18 401 ; 
       25 21 401 ; 
       26 29 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 35 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -4 0 MPRFLG 0 ; 
       1 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 40 -4 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 51.25 -6.194867 0 USR MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 25 -2 0 MPRFLG 0 ; 
       21 SCHEM 25 -4 0 MPRFLG 0 ; 
       22 SCHEM 25 -6 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 MPRFLG 0 ; 
       27 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 35 -8 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       32 SCHEM 50 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 41.06975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 55.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 55.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 41.06975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 55.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 70.25 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
