SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       move_points-mat73.1-0 ; 
       move_points-mat74.1-0 ; 
       move_points-mat75.1-0 ; 
       move_points-mat79.1-0 ; 
       move_points-mat80.1-0 ; 
       move_points-mat81.1-0 ; 
       move_points-mat82.1-0 ; 
       move_points-mat83.1-0 ; 
       move_points-mat86.1-0 ; 
       move_points-mat89.1-0 ; 
       rix_fighter_sPt-default1.1-0 ; 
       rix_fighter_sPt-mat1.1-0 ; 
       rix_fighter_sPt-mat10.1-0 ; 
       rix_fighter_sPt-mat11.1-0 ; 
       rix_fighter_sPt-mat2.1-0 ; 
       rix_fighter_sPt-mat3.1-0 ; 
       rix_fighter_sPt-mat4.1-0 ; 
       rix_fighter_sPt-mat48.1-0 ; 
       rix_fighter_sPt-mat49.1-0 ; 
       rix_fighter_sPt-mat5.1-0 ; 
       rix_fighter_sPt-mat50.1-0 ; 
       rix_fighter_sPt-mat51.1-0 ; 
       rix_fighter_sPt-mat52.1-0 ; 
       rix_fighter_sPt-mat53.1-0 ; 
       rix_fighter_sPt-mat54.1-0 ; 
       rix_fighter_sPt-mat55.1-0 ; 
       rix_fighter_sPt-mat56.1-0 ; 
       rix_fighter_sPt-mat57.1-0 ; 
       rix_fighter_sPt-mat58.1-0 ; 
       rix_fighter_sPt-mat6.1-0 ; 
       rix_fighter_sPt-mat65.1-0 ; 
       rix_fighter_sPt-mat66.1-0 ; 
       rix_fighter_sPt-mat67.1-0 ; 
       rix_fighter_sPt-mat68.1-0 ; 
       rix_fighter_sPt-mat69.1-0 ; 
       rix_fighter_sPt-mat7.1-0 ; 
       rix_fighter_sPt-mat70.1-0 ; 
       rix_fighter_sPt-mat71.1-0 ; 
       rix_fighter_sPt-mat72.1-0 ; 
       rix_fighter_sPt-mat8.1-0 ; 
       rix_fighter_sPt-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       fig04-cockpt.1-0 ; 
       fig04-fig04.4-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-missemt.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepemt2.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-smoke.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-thrust.1-0 ; 
       fig04-trail.1-0 ; 
       fig04-wingzz0.1-0 ; 
       move_points-cube2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-move_points.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       move_points-t2d66.1-0 ; 
       move_points-t2d67.1-0 ; 
       move_points-t2d68.1-0 ; 
       move_points-t2d71.1-0 ; 
       rix_fighter_sPt-t2d1.1-0 ; 
       rix_fighter_sPt-t2d10.1-0 ; 
       rix_fighter_sPt-t2d11.1-0 ; 
       rix_fighter_sPt-t2d2.1-0 ; 
       rix_fighter_sPt-t2d3.1-0 ; 
       rix_fighter_sPt-t2d4.1-0 ; 
       rix_fighter_sPt-t2d44.1-0 ; 
       rix_fighter_sPt-t2d45.1-0 ; 
       rix_fighter_sPt-t2d46.1-0 ; 
       rix_fighter_sPt-t2d47.1-0 ; 
       rix_fighter_sPt-t2d48.1-0 ; 
       rix_fighter_sPt-t2d49.1-0 ; 
       rix_fighter_sPt-t2d5.1-0 ; 
       rix_fighter_sPt-t2d50.1-0 ; 
       rix_fighter_sPt-t2d51.1-0 ; 
       rix_fighter_sPt-t2d52.1-0 ; 
       rix_fighter_sPt-t2d53.1-0 ; 
       rix_fighter_sPt-t2d54.1-0 ; 
       rix_fighter_sPt-t2d55.1-0 ; 
       rix_fighter_sPt-t2d56.1-0 ; 
       rix_fighter_sPt-t2d57.1-0 ; 
       rix_fighter_sPt-t2d58.1-0 ; 
       rix_fighter_sPt-t2d59.1-0 ; 
       rix_fighter_sPt-t2d6.1-0 ; 
       rix_fighter_sPt-t2d60.1-0 ; 
       rix_fighter_sPt-t2d61.1-0 ; 
       rix_fighter_sPt-t2d62.1-0 ; 
       rix_fighter_sPt-t2d63.1-0 ; 
       rix_fighter_sPt-t2d64.1-0 ; 
       rix_fighter_sPt-t2d65.1-0 ; 
       rix_fighter_sPt-t2d7.1-0 ; 
       rix_fighter_sPt-t2d8.1-0 ; 
       rix_fighter_sPt-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 3 110 ; 
       8 4 110 ; 
       9 2 110 ; 
       10 31 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 4 110 ; 
       15 2 110 ; 
       16 4 110 ; 
       17 15 110 ; 
       18 2 110 ; 
       19 31 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 1 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 12 110 ; 
       28 21 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 36 300 ; 
       2 10 300 ; 
       2 38 300 ; 
       2 0 300 ; 
       2 7 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 19 300 ; 
       6 8 300 ; 
       7 11 300 ; 
       7 14 300 ; 
       8 29 300 ; 
       8 35 300 ; 
       8 39 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 37 300 ; 
       15 9 300 ; 
       16 40 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       20 17 300 ; 
       20 18 300 ; 
       21 20 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       21 23 300 ; 
       23 3 300 ; 
       24 4 300 ; 
       25 5 300 ; 
       26 6 300 ; 
       27 1 300 ; 
       28 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 23 400 ; 
       8 30 400 ; 
       11 25 400 ; 
       12 22 400 ; 
       16 31 400 ; 
       20 24 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 16 401 ; 
       20 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 15 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 27 401 ; 
       30 33 401 ; 
       32 32 401 ; 
       33 26 401 ; 
       34 28 401 ; 
       35 34 401 ; 
       37 21 401 ; 
       38 29 401 ; 
       39 35 401 ; 
       40 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 SRT 1 1 1 0 0 -1.570796 0 0 1.390758 MPRFLG 0 ; 
       2 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       17 SCHEM 38.73292 -7.242239 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 MPRFLG 0 ; 
       22 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 25 -4 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 20 -4 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 55 -8 0 MPRFLG 0 ; 
       29 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       32 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
