SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.7-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       rix_fighter_sPt-default1.1-0 ; 
       rix_fighter_sPt-mat1.1-0 ; 
       rix_fighter_sPt-mat10.1-0 ; 
       rix_fighter_sPt-mat11.1-0 ; 
       rix_fighter_sPt-mat2.1-0 ; 
       rix_fighter_sPt-mat3.1-0 ; 
       rix_fighter_sPt-mat4.1-0 ; 
       rix_fighter_sPt-mat48.1-0 ; 
       rix_fighter_sPt-mat49.1-0 ; 
       rix_fighter_sPt-mat5.1-0 ; 
       rix_fighter_sPt-mat50.1-0 ; 
       rix_fighter_sPt-mat51.1-0 ; 
       rix_fighter_sPt-mat52.1-0 ; 
       rix_fighter_sPt-mat53.1-0 ; 
       rix_fighter_sPt-mat54.1-0 ; 
       rix_fighter_sPt-mat55.1-0 ; 
       rix_fighter_sPt-mat56.1-0 ; 
       rix_fighter_sPt-mat57.1-0 ; 
       rix_fighter_sPt-mat58.1-0 ; 
       rix_fighter_sPt-mat6.1-0 ; 
       rix_fighter_sPt-mat65.1-0 ; 
       rix_fighter_sPt-mat66.1-0 ; 
       rix_fighter_sPt-mat67.1-0 ; 
       rix_fighter_sPt-mat68.1-0 ; 
       rix_fighter_sPt-mat69.1-0 ; 
       rix_fighter_sPt-mat7.1-0 ; 
       rix_fighter_sPt-mat70.1-0 ; 
       rix_fighter_sPt-mat71.1-0 ; 
       rix_fighter_sPt-mat72.1-0 ; 
       rix_fighter_sPt-mat8.1-0 ; 
       rix_fighter_sPt-mat9.1-0 ; 
       static-mat73.1-0 ; 
       static-mat83.1-0 ; 
       static-mat86.1-0 ; 
       static-mat89.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       fig04-fig04_1.3-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       rix_fighter_sPt-t2d1.1-0 ; 
       rix_fighter_sPt-t2d10.1-0 ; 
       rix_fighter_sPt-t2d11.1-0 ; 
       rix_fighter_sPt-t2d2.1-0 ; 
       rix_fighter_sPt-t2d3.1-0 ; 
       rix_fighter_sPt-t2d4.1-0 ; 
       rix_fighter_sPt-t2d44.1-0 ; 
       rix_fighter_sPt-t2d45.1-0 ; 
       rix_fighter_sPt-t2d46.1-0 ; 
       rix_fighter_sPt-t2d47.1-0 ; 
       rix_fighter_sPt-t2d48.1-0 ; 
       rix_fighter_sPt-t2d49.1-0 ; 
       rix_fighter_sPt-t2d5.1-0 ; 
       rix_fighter_sPt-t2d50.1-0 ; 
       rix_fighter_sPt-t2d51.1-0 ; 
       rix_fighter_sPt-t2d52.1-0 ; 
       rix_fighter_sPt-t2d53.1-0 ; 
       rix_fighter_sPt-t2d54.1-0 ; 
       rix_fighter_sPt-t2d55.1-0 ; 
       rix_fighter_sPt-t2d56.1-0 ; 
       rix_fighter_sPt-t2d57.1-0 ; 
       rix_fighter_sPt-t2d58.1-0 ; 
       rix_fighter_sPt-t2d59.1-0 ; 
       rix_fighter_sPt-t2d6.1-0 ; 
       rix_fighter_sPt-t2d60.1-0 ; 
       rix_fighter_sPt-t2d61.1-0 ; 
       rix_fighter_sPt-t2d62.1-0 ; 
       rix_fighter_sPt-t2d63.1-0 ; 
       rix_fighter_sPt-t2d64.1-0 ; 
       rix_fighter_sPt-t2d65.1-0 ; 
       rix_fighter_sPt-t2d7.1-0 ; 
       rix_fighter_sPt-t2d8.1-0 ; 
       rix_fighter_sPt-t2d9.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
       static-t2d68.1-0 ; 
       static-t2d71.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 2 110 ; 
       6 3 110 ; 
       7 15 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 1 110 ; 
       11 3 110 ; 
       12 15 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 26 300 ; 
       1 0 300 ; 
       1 28 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       4 33 300 ; 
       5 1 300 ; 
       5 4 300 ; 
       6 19 300 ; 
       6 25 300 ; 
       6 29 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 27 300 ; 
       10 34 300 ; 
       11 30 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 19 400 ; 
       6 26 400 ; 
       8 21 400 ; 
       9 18 400 ; 
       11 27 400 ; 
       13 20 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       31 33 401 ; 
       32 34 401 ; 
       33 35 401 ; 
       34 36 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 12 401 ; 
       10 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 23 401 ; 
       20 29 401 ; 
       22 28 401 ; 
       23 22 401 ; 
       24 24 401 ; 
       25 30 401 ; 
       27 17 401 ; 
       28 25 401 ; 
       29 31 401 ; 
       30 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 -1.570796 0 0 1.390758 MPRFLG 0 ; 
       1 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 MPRFLG 0 ; 
       15 SCHEM 26.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       31 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       33 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 31.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
