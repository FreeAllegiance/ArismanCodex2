SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.7-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       add_grear-back6.2-0 ; 
       add_grear-engine7.2-0 ; 
       add_grear-fiiped_decal1.2-0 ; 
       add_grear-inside5.1-0 ; 
       add_grear-inside6.1-0 ; 
       add_grear-mat1.1-0 ; 
       add_grear-mat2.1-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-outside7.1-0 ; 
       add_grear-sides5.2-0 ; 
       add_grear-tur3.1-0 ; 
       add_grear-wing_bottom7.1-0 ; 
       add_grear-wing_bottom8.1-0 ; 
       add_grear-wing_top8.1-0 ; 
       add_grear-wing_top9.1-0 ; 
       static-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig16-fgun.1-0 ; 
       fig16-lstablat.1-0 ; 
       fig16-lwing.1-0 ; 
       fig16-lwingpod.4-0 ; 
       fig16-root_1.2-0 ROOT ; 
       fig16-rstablat.1-0 ; 
       fig16-rwing.1-0 ; 
       fig16-rwingpod.1-0 ; 
       fig16-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/mod-fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig16-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       add_grear-engine_map5_1.1-0 ; 
       add_grear-outside5_1.1-0 ; 
       add_grear-outside6_1.1-0 ; 
       add_grear-t2d27_1.1-0 ; 
       add_grear-t2d28_1.1-0 ; 
       add_grear-t2d29_1.1-0 ; 
       add_grear-t2d30_1.1-0 ; 
       add_grear-t2d31_1.1-0 ; 
       add_grear-t2d32_1.1-0 ; 
       add_grear-t2d33_1.1-0 ; 
       add_grear-t2d35_1.1-0 ; 
       add_grear-wing_bottom8_1.1-0 ; 
       add_grear-wing_bottom9_1.1-0 ; 
       add_grear-wing_top8_1.1-0 ; 
       add_grear-wing_top9_1.1-0 ; 
       static-t2d33_2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       1 4 300 ; 
       1 8 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       3 6 300 ; 
       4 9 300 ; 
       4 1 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       5 3 300 ; 
       5 7 300 ; 
       6 11 300 ; 
       6 13 300 ; 
       7 15 300 ; 
       8 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 10 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 7 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
