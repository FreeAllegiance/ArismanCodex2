SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.11-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       bounding_model-bound1.1-0 ; 
       bounding_model-bound10.2-0 ; 
       bounding_model-bound11.2-0 ; 
       bounding_model-bound12.1-0 ; 
       bounding_model-bound13.1-0 ; 
       bounding_model-bound14.1-0 ; 
       bounding_model-bound15.1-0 ; 
       bounding_model-bound16.1-0 ; 
       bounding_model-bound2.1-0 ; 
       bounding_model-bound3.1-0 ; 
       bounding_model-bound4.1-0 ; 
       bounding_model-bound5.1-0 ; 
       bounding_model-bound6.1-0 ; 
       bounding_model-bound7.2-0 ; 
       bounding_model-bound8.1-0 ; 
       bounding_model-bound9.2-0 ; 
       bounding_model-bounding_model_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss98_ordinance-bound.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 37.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 21.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
