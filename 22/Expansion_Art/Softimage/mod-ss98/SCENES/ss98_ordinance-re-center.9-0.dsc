SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.13-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       dowager_sPtL-inf_light5_1_2.9-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       done_text-mat151.1-0 ; 
       done_text-mat153.1-0 ; 
       done_text-mat162.1-0 ; 
       done_text-mat164.1-0 ; 
       done_text-mat165.1-0 ; 
       done_text-mat166.1-0 ; 
       done_text-mat167.1-0 ; 
       done_text-mat173.1-0 ; 
       done_text-mat174.1-0 ; 
       done_text-mat201.1-0 ; 
       done_text-mat202.1-0 ; 
       done_text-mat203.1-0 ; 
       done_text-mat208.1-0 ; 
       done_text-mat209.1-0 ; 
       done_text-mat210.1-0 ; 
       done_text-mat211.1-0 ; 
       done_text-mat212.1-0 ; 
       done_text-mat213.1-0 ; 
       done_text-mat214.1-0 ; 
       done_text-mat215.1-0 ; 
       done_text-mat216.1-0 ; 
       done_text-mat217.1-0 ; 
       done_text-mat218.1-0 ; 
       done_text-mat219.1-0 ; 
       done_text-mat220.1-0 ; 
       done_text-mat221.1-0 ; 
       done_text-mat222.1-0 ; 
       done_text-mat223.1-0 ; 
       done_text-mat224.1-0 ; 
       done_text-mat225.1-0 ; 
       done_text-mat226.1-0 ; 
       done_text-mat227.1-0 ; 
       done_text-mat228.1-0 ; 
       done_text-mat229.1-0 ; 
       re_center-mat337.1-0 ; 
       re_center-mat338.1-0 ; 
       re_center-mat339.1-0 ; 
       re_center-mat340.1-0 ; 
       ROTATE-mat230.1-0 ; 
       ROTATE-mat231.1-0 ; 
       ROTATE-mat232.1-0 ; 
       ROTATE-mat233.1-0 ; 
       ROTATE-mat234.1-0 ; 
       ROTATE-mat235.1-0 ; 
       ROTATE-mat236.1-0 ; 
       ROTATE-mat237.1-0 ; 
       ROTATE-mat238.1-0 ; 
       ROTATE-mat239.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       done_text-bcorrdr1.1-0 ; 
       done_text-bcorrdr2.1-0 ; 
       done_text-bcorrdr3.1-0 ; 
       done_text-bcorrdr4.1-0 ; 
       done_text-fuselg7.1-0 ; 
       done_text-fuselg7_1.1-0 ; 
       done_text-fuselg7_2.1-0 ; 
       done_text-fuselg7_5.1-0 ; 
       done_text-fuselg9.1-0 ; 
       done_text-fuselg9_1.1-0 ; 
       done_text-garage1A.1-0 ; 
       done_text-garage1B.1-0 ; 
       done_text-garage1C.1-0 ; 
       done_text-garage1D.1-0 ; 
       done_text-garage1E.1-0 ; 
       done_text-launch1.1-0 ; 
       done_text-null1.1-0 ; 
       done_text-root_1.2-0 ; 
       done_text-root_2.1-0 ; 
       done_text-sphere1.7-0 ROOT ; 
       done_text-ss01.1-0 ; 
       done_text-ss10.1-0 ; 
       done_text-ss11.1-0 ; 
       done_text-ss11_1.1-0 ; 
       done_text-ss11_2.1-0 ; 
       done_text-ss11_3.1-0 ; 
       done_text-ss2.1-0 ; 
       done_text-ss3.1-0 ; 
       done_text-ss4.1-0 ; 
       done_text-ss5.1-0 ; 
       done_text-ss6.1-0 ; 
       done_text-ss7.1-0 ; 
       done_text-ss8.1-0 ; 
       done_text-ss9.1-0 ; 
       done_text-torus1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Expansion_Art/Softimage/mod-ss98/PICTURES/rixbay ; 
       //research/root/federation/Expansion_Art/Softimage/mod-ss98/PICTURES/ss98 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss98_ordinance-re-center.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       done_text-t2d101.1-0 ; 
       done_text-t2d102.1-0 ; 
       done_text-t2d103.1-0 ; 
       done_text-t2d104.1-0 ; 
       done_text-t2d105.1-0 ; 
       done_text-t2d106.1-0 ; 
       done_text-t2d107.1-0 ; 
       done_text-t2d108.1-0 ; 
       done_text-t2d109.1-0 ; 
       done_text-t2d110.1-0 ; 
       done_text-t2d111.1-0 ; 
       done_text-t2d112.1-0 ; 
       done_text-t2d113.1-0 ; 
       done_text-t2d114.1-0 ; 
       done_text-t2d115.1-0 ; 
       done_text-t2d116.1-0 ; 
       done_text-t2d117.1-0 ; 
       done_text-t2d118.1-0 ; 
       done_text-t2d119.1-0 ; 
       done_text-t2d120.1-0 ; 
       done_text-t2d15.1-0 ; 
       done_text-t2d17.1-0 ; 
       done_text-t2d18.1-0 ; 
       done_text-t2d19.1-0 ; 
       done_text-t2d20.1-0 ; 
       done_text-t2d26.1-0 ; 
       done_text-t2d27.1-0 ; 
       done_text-t2d3.1-0 ; 
       done_text-t2d5.1-0 ; 
       done_text-t2d91.1-0 ; 
       done_text-t2d92.1-0 ; 
       done_text-t2d93.1-0 ; 
       done_text-t2d98.1-0 ; 
       done_text-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 19 110 ; 
       2 19 110 ; 
       3 19 110 ; 
       4 17 110 ; 
       5 18 110 ; 
       6 19 110 ; 
       7 34 110 ; 
       8 17 110 ; 
       9 18 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 19 110 ; 
       15 19 110 ; 
       16 19 110 ; 
       17 34 110 ; 
       18 34 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 19 110 ; 
       27 19 110 ; 
       28 19 110 ; 
       29 19 110 ; 
       30 19 110 ; 
       31 19 110 ; 
       32 19 110 ; 
       33 19 110 ; 
       34 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       4 0 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       8 1 300 ; 
       9 7 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       19 11 300 ; 
       19 33 300 ; 
       20 38 300 ; 
       21 47 300 ; 
       22 34 300 ; 
       23 37 300 ; 
       24 36 300 ; 
       25 35 300 ; 
       26 39 300 ; 
       27 40 300 ; 
       28 41 300 ; 
       29 42 300 ; 
       30 43 300 ; 
       31 44 300 ; 
       32 45 300 ; 
       33 46 300 ; 
       34 12 300 ; 
       34 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 27 400 ; 
       5 26 400 ; 
       8 28 400 ; 
       9 25 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 20 401 ; 
       3 21 401 ; 
       4 22 401 ; 
       5 23 401 ; 
       6 24 401 ; 
       9 29 401 ; 
       10 30 401 ; 
       11 31 401 ; 
       12 32 401 ; 
       13 33 401 ; 
       14 0 401 ; 
       15 1 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       26 12 401 ; 
       27 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 80 -6 0 MPRFLG 0 ; 
       2 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 95 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       6 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 100 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 102.5 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 105 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 107.5 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 110 -6 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 112.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 MPRFLG 0 ; 
       18 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 71.25 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 137.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 75 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 70 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 117.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 120 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 122.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 125 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 132.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 127.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 130 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 135 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 26.25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
