SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       swarmer-cam_int1.3-0 ROOT ; 
       swarmer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       add_rocket-mat1.1-0 ; 
       add_rocket-mat2.1-0 ; 
       add_rocket-mat3.1-0 ; 
       add_rocket-mat4.1-0 ; 
       add_rocket-mat5.1-0 ; 
       add_rocket-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       add_rocket-cube1.1-0 ; 
       add_rocket-cube5.1-0 ; 
       add_rocket-cube6.1-0 ; 
       add_rocket-cube7.1-0 ; 
       add_rocket-cyl1.1-0 ROOT ; 
       add_rocket-rocket.1-0 ; 
       add_rocket-smoke.1-0 ; 
       add_rocket-thrust.1-0 ; 
       add_rocket-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/new_missiles/mis02/PICTURES/mis02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mis02-add_rocket.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       add_rocket-t2d1.1-0 ; 
       add_rocket-t2d2.1-0 ; 
       add_rocket-t2d3.1-0 ; 
       add_rocket-t2d4.1-0 ; 
       add_rocket-t2d5.1-0 ; 
       add_rocket-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       5 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       4 0 300 ; 
       4 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13.75 0 0 DISPLAY 3 2 SRT 0.617596 0.617596 0.617596 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
