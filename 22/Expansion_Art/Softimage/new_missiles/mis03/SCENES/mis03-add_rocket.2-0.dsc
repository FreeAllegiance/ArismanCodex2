SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_rocket-cam_int1.2-0 ROOT ; 
       add_rocket-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       add_rocket-mat1.1-0 ; 
       add_rocket-mat10.1-0 ; 
       add_rocket-mat11.1-0 ; 
       add_rocket-mat12.1-0 ; 
       add_rocket-mat13.1-0 ; 
       add_rocket-mat15.1-0 ; 
       add_rocket-mat16.1-0 ; 
       add_rocket-mat17.1-0 ; 
       add_rocket-mat7.1-0 ; 
       add_rocket-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       Mis03-front_fins.1-0 ; 
       Mis03-frontfin4.1-0 ; 
       Mis03-frontfin4_1.2-0 ; 
       Mis03-frontfin5.1-0 ; 
       Mis03-frontfin7.1-0 ; 
       Mis03-main_body.1-0 ; 
       Mis03-rear_fin5.1-0 ; 
       Mis03-rear_fin5_1.2-0 ; 
       Mis03-rear_fin6.1-0 ; 
       Mis03-rear_fin7.1-0 ; 
       Mis03-rear_fins.1-0 ; 
       Mis03-rocket.1-0 ; 
       Mis03-root.2-0 ROOT ; 
       Mis03-thrust.1-0 ; 
       Mis03-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/new_missiles/mis03/PICTURES/mis03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mis03-add_rocket.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       add_rocket-t2d1.1-0 ; 
       add_rocket-t2d10.1-0 ; 
       add_rocket-t2d11.1-0 ; 
       add_rocket-t2d12.1-0 ; 
       add_rocket-t2d13.1-0 ; 
       add_rocket-t2d15.1-0 ; 
       add_rocket-t2d16.1-0 ; 
       add_rocket-t2d17.1-0 ; 
       add_rocket-t2d7.1-0 ; 
       add_rocket-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 12 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 12 110 ; 
       11 12 110 ; 
       13 12 110 ; 
       14 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       2 0 300 ; 
       3 2 300 ; 
       4 3 300 ; 
       5 9 300 ; 
       5 1 300 ; 
       6 6 300 ; 
       7 8 300 ; 
       8 7 300 ; 
       9 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 9 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 22 -6 0 MPRFLG 0 ; 
       2 SCHEM 14.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 19.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 17 -6 0 MPRFLG 0 ; 
       5 SCHEM 10.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 29.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 24.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 32 -6 0 MPRFLG 0 ; 
       9 SCHEM 27 -6 0 MPRFLG 0 ; 
       10 SCHEM 28.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 34.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 19.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 7 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 22 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 32 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
