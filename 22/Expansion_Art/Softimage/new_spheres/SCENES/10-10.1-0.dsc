SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.10-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       10-mat2.1-0 ; 
       10-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       2-sphere3.4-0 ROOT ; 
       3-sphere3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/map ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/map_light ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/orion-tile ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       10-10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       10-t2d4.1-0 ; 
       10-t2d5.1-0 ; 
       10-t2d6.1-0 ; 
       10-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       0 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
