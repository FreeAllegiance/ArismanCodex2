SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.1-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       from_shared_art-Bottom.1-0 ; 
       from_shared_art-mat10.1-0 ; 
       from_shared_art-mat8.1-0 ; 
       from_shared_art-mat9.1-0 ; 
       from_shared_art-Strip.1-0 ; 
       from_shared_art-Top.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       from_shared_art-Mapper.1-0 ROOT ; 
       from_shared_art-Reciever.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/dust_strip ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/environtest ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/lagoon ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/map ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/mapbottom ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/orion ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/pieces ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-from_shared_art.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       from_shared_art-Belt3.1-0 ; 
       from_shared_art-Horse_Head3.1-0 ; 
       from_shared_art-orion3.1-0 ; 
       from_shared_art-strip.1-0 ; 
       from_shared_art-strip1.1-0 ; 
       from_shared_art-t2d20.1-0 ; 
       from_shared_art-t2d21.1-0 ; 
       from_shared_art-t2d23.1-0 ; 
       from_shared_art-t2d24.1-0 ; 
       from_shared_art-t2d25.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       0 1 300 ; 
       1 5 300 ; 
       1 0 300 ; 
       1 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       0 0 400 ; 
       0 2 400 ; 
       0 5 400 ; 
       0 9 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 3 401 ; 
       2 6 401 ; 
       4 4 401 ; 
       5 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 25 0 0 DISPLAY 0 0 SRT 1500 1500 1500 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
