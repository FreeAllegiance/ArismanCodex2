SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.2-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       create-Bottom.1-0 ; 
       create-mat10.1-0 ; 
       create-mat8.1-0 ; 
       create-mat9.1-0 ; 
       create-Strip.1-0 ; 
       create-Top.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       create-Mapper.1-0 ROOT ; 
       create-Reciever.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/dust_strip ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/environtest ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/lagoon ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/map ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/mapbottom ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/orion ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/orion-tile ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/pieces ; 
       //research/root/federation/Expansion_Art/Softimage/new_spheres/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_sphere_1-create.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       create-Belt3.1-0 ; 
       create-orion3.1-0 ; 
       create-strip.1-0 ; 
       create-strip1.1-0 ; 
       create-t2d21.1-0 ; 
       create-t2d23.1-0 ; 
       create-t2d24.1-0 ; 
       create-t2d25.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       0 1 300 ; 
       1 5 300 ; 
       1 0 300 ; 
       1 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       0 1 400 ; 
       0 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       1 2 401 ; 
       2 4 401 ; 
       4 3 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 DISPLAY 0 0 SRT 1500 1500 1500 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
