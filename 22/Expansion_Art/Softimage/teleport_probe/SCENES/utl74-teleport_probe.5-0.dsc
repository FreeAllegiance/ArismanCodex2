SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.5-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       auto_turret2_mod-mat46_1.1-0 ; 
       done_lite-mat11_1.1-0 ; 
       generic_buoy_sA-mat49_1.1-0 ; 
       generic_buoy_sA-mat51_1.1-0 ; 
       generic_buoy_sA-mat52_1.1-0 ; 
       generic_buoy_sA-mat55_1.1-0 ; 
       generic_buoy_sA-mat56_1.1-0 ; 
       generic_buoy_sA-mat57.1-0 ; 
       generic_buoy_sA-mat58.1-0 ; 
       generic_buoy_sA-mat61.1-0 ; 
       generic_buoy_sA-mat62.1-0 ; 
       generic_buoy_sAT-default6.1-0 ; 
       generic_buoy_sAT-mat103.1-0 ; 
       generic_buoy_sAT-mat104.1-0 ; 
       generic_buoy_sAT-mat105.1-0 ; 
       generic_buoy_sAt-mat65.1-0 ; 
       generic_buoy_sAt-mat68.1-0 ; 
       generic_buoy_sAt-mat70.1-0 ; 
       generic_buoy_sAt-mat77.1-0 ; 
       generic_buoy_sAt-mat89.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       mb_utl01-defense_5_6.4-0 ; 
       mb_utl01-door1_1.1-0 ; 
       mb_utl01-door2_1.1-0 ; 
       mb_utl01-generic.4-0 ROOT ; 
       mb_utl01-solar_panel.1-0 ; 
       mb_utl01-solar_pod_4_6.2-0 ; 
       mb_utl01-SS01.1-0 ; 
       mb_utl01-SS02.1-0 ; 
       mb_utl01-thrust_pod_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/teleport_probe/PICTURES/utl74 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl74-teleport_probe.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       generic_buoy_sA-t2d1_1.2-0 ; 
       generic_buoy_sA-t2d10.2-0 ; 
       generic_buoy_sA-t2d11.2-0 ; 
       generic_buoy_sA-t2d14.3-0 ; 
       generic_buoy_sA-t2d7.2-0 ; 
       generic_buoy_sA-t2d9.2-0 ; 
       generic_buoy_sAt-t2d19.3-0 ; 
       generic_buoy_sAt-t2d21.3-0 ; 
       generic_buoy_sAt-t2d31.2-0 ; 
       generic_buoy_sAt-t2d34.3-0 ; 
       generic_buoy_sAt-t2d36.2-0 ; 
       generic_buoy_sAt-t2d44.2-0 ; 
       generic_buoy_sAT-t2d62.2-0 ; 
       generic_buoy_sAT-t2d63.2-0 ; 
       generic_buoy_sAT-t2d64.2-0 ; 
       generic_buoy_sAT-t2d65.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       4 5 110 ; 
       5 3 110 ; 
       7 4 110 ; 
       6 4 110 ; 
       8 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 19 300 ; 
       7 1 300 ; 
       6 0 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       1 10 400 ; 
       2 8 400 ; 
       5 11 400 ; 
       8 12 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 4 401 ; 
       5 5 401 ; 
       6 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 48.75 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 48.75 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 WIRECOL 7 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -12 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 210 210 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
