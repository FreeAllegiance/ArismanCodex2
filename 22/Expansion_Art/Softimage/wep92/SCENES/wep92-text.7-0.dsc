SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.7-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       text-front-back1.1-0 ; 
       text-mat1.1-0 ; 
       text-mat2.1-0 ; 
       text-mat3.2-0 ; 
       text-mat4.2-0 ; 
       text-sides1.1-0 ; 
       text-top-bottom1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       wep92-cube1.1-0 ; 
       wep92-cyl10.1-0 ; 
       wep92-cyl11.1-0 ; 
       wep92-cyl9.1-0 ; 
       wep92-root.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Expansion_Art/Softimage/wep92/PICTURES/wep92 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wep92-text.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       text-t2d1.2-0 ; 
       text-t2d2.2-0 ; 
       text-t2d4.2-0 ; 
       text-t2d5.2-0 ; 
       text-t2d6.2-0 ; 
       text-t2d7.1-0 ; 
       text-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       1 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 6 300 ; 
       0 0 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 1 300 ; 
       1 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 5 401 ; 
       4 2 401 ; 
       6 3 401 ; 
       0 4 401 ; 
       5 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
