SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       venus-cam_int1.6-0 ROOT ; 
       venus-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       venus-inf_light1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       venus-mat1.1-0 ; 
       venus-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       venus-sphere2.2-0 ROOT ; 
       venus1-sphere2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/Expansion_Art/Softimage/new_nebs/PICTURES/CloudMap ; 
       //research/root/federation/Expansion_Art/Softimage/new_nebs/PICTURES/CloudMapTrans ; 
       //research/root/federation/Expansion_Art/Softimage/new_nebs/PICTURES/cut ; 
       //research/root/federation/Expansion_Art/Softimage/new_nebs/PICTURES/venus_map ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       venus-venus.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       venus-t2d1.1-0 ; 
       venus-t2d3.3-0 ; 
       venus-t2d4.3-0 ; 
       venus-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       1 1 400 ; 
       1 2 400 ; 
       1 3 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 11.25 0 0 DISPLAY 1 2 SRT 1.008 1.008 1.008 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
