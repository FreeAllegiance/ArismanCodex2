SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       M_Class-cam_int1.2-0 ROOT ; 
       M_Class-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       M_Class-inf_light1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       M_Class-mat2.1-0 ; 
       M_Class-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       M_Class-sphere1.2-0 ROOT ; 
       M_Class-sphere3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Planets/PICTURES/plnt01_Clouds ; 
       F:/Pete_Data3/Planets/PICTURES/plnt32 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt32_ground ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-Gas_Giant.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       M_Class-bump.2-0 ; 
       M_Class-map.2-0 ; 
       M_Class-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       1 1 400 ; 
       1 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 1 2 SRT 1.044405 1.044405 1.044405 0 0.306 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
