SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       M_Class-cam_int1.4-0 ROOT ; 
       M_Class-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       M_Class-inf_light1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       Gas_Giant-mat4.1-0 ; 
       M_Class-mat2.2-0 ; 
       M_Class-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       M_Class-sphere1.4-0 ROOT ; 
       M_Class-sphere3.2-0 ; 
       M_Class-sphere4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Planets/PICTURES/plnt19 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt22 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt39 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-Gas_Giant.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       Gas_Giant-map1.2-0 ; 
       M_Class-map.4-0 ; 
       M_Class-t2d2.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 2 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       1 1 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
