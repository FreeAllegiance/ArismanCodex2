SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       p1-cam_int1.1-0 ROOT ; 
       p1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       p1-inf_light1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       p1-mat2.1-0 ; 
       p1-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       p1-sphere1.1-0 ROOT ; 
       p1-sphere3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Planets/PICTURES/plnt01 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt01_Clouds ; 
       F:/Pete_Data3/Planets/PICTURES/plnt01_ground ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-p1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       p1-bump.1-0 ; 
       p1-map.1-0 ; 
       p1-spec1.1-0 ; 
       p1-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       1 1 400 ; 
       1 0 400 ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1.044405 1.044405 1.044405 0 0.306 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
