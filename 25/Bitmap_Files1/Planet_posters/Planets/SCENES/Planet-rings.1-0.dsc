SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       M_Class-cam_int1.6-0 ROOT ; 
       M_Class-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       M_Class-inf_light1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       M_Class-mat2.2-0 ; 
       M_Class-mat3.3-0 ; 
       rings-mat4.1-0 ; 
       rings-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       M_Class-sphere1.6-0 ROOT ; 
       M_Class-sphere3.2-0 ; 
       M_Class-sphere4.1-0 ; 
       rings-grid1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Pete_Data3/Planets/PICTURES/RING4 ; 
       F:/Pete_Data3/Planets/PICTURES/neptune ; 
       F:/Pete_Data3/Planets/PICTURES/plnt22 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt36 ; 
       F:/Pete_Data3/Planets/PICTURES/plnt42 ; 
       F:/Pete_Data3/Planets/PICTURES/ring3 ; 
       F:/Pete_Data3/Planets/PICTURES/saturn ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-rings.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       M_Class-map.5-0 ; 
       M_Class-t2d2.3-0 ; 
       rings-map1.1-0 ; 
       rings-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 0 400 ; 
       2 2 400 ; 
       3 3 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 21.25 0 0 DISPLAY 1 2 SRT 2.030082 2.030082 2.030082 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
