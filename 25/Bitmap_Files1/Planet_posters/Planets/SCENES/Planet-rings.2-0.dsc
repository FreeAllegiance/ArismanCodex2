SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       M_Class-cam_int1.9-0 ROOT ; 
       M_Class-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       M_Class-inf_light1.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       M_Class-mat3.5-0 ; 
       rings-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       M_Class-sphere3.3-0 ROOT ; 
       M_Class-sphere4.1-0 ; 
       rings-circle2.1-0 ROOT ; 
       rings-c_ring.1-0 ROOT ; 
       rings-face1.1-0 ROOT ; 
       rings-face2.1-0 ROOT ; 
       rings-face3.1-0 ROOT ; 
       rings-torus1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Planets/PICTURES/saturn ; 
       F:/Pete_Data3/Planets/PICTURES/venusclouds ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-rings.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       M_Class-map.7-0 ; 
       rings-map1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       1 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 8.749999 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 0.001 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 6.25 -2 0 DISPLAY 0 0 SRT 0.9909402 0.9909402 0.9909402 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 0 0 SRT 9.2 9.2 9.2 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15.01757 -2.724622 0 USR DISPLAY 1 2 SRT 12.22 12.22 12.22 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 16.97264 0.06054715 0 USR SRT 9.839 9.839 9.839 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 17.51757 0 0 USR SRT 11.75 11.75 11.75 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 20.01757 0 0 USR SRT 13.357 13.357 13.357 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
