SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       M_Class-cam_int1.10-0 ROOT ; 
       M_Class-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       M_Class-inf_light1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       M_Class-mat3.5-0 ; 
       rings-mat10.1-0 ; 
       rings-mat12.1-0 ; 
       rings-mat4.2-0 ; 
       rings-mat5.2-0 ; 
       rings-mat6.1-0 ; 
       rings-mat7.1-0 ; 
       rings-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       M_Class-sphere3.3-0 ROOT ; 
       M_Class-sphere4.1-0 ; 
       rings-c_ring.1-0 ; 
       rings-face1.1-0 ; 
       rings-face2.1-0 ; 
       rings-face3.1-0 ; 
       rings-face4.1-0 ; 
       rings-face5.1-0 ; 
       rings-null1.1-0 ROOT ; 
       rings-torus1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Planets/PICTURES/saturn ; 
       F:/Pete_Data3/Planets/PICTURES/venusclouds ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Planet-rings.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       M_Class-map.7-0 ; 
       rings-map1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       6 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       1 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 0.001 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 DISPLAY 0 0 SRT 0.9909402 0.9909402 0.9909402 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 21.25 0 0 SRT 0.188118 0.188118 0.188118 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
