SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.1-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 17     
       11-spot1.1-0 ; 
       11-spot1_int.1-0 ROOT ; 
       setup-light1.1-0 ROOT ; 
       setup-light10.1-0 ROOT ; 
       setup-light11.1-0 ROOT ; 
       setup-light12.1-0 ROOT ; 
       setup-light13.1-0 ROOT ; 
       setup-light14.1-0 ROOT ; 
       setup-light15.1-0 ROOT ; 
       setup-light2.1-0 ROOT ; 
       setup-light3.1-0 ROOT ; 
       setup-light4.1-0 ROOT ; 
       setup-light5.1-0 ROOT ; 
       setup-light6.1-0 ROOT ; 
       setup-light7.1-0 ROOT ; 
       setup-light8.1-0 ROOT ; 
       setup-light9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       setup-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       root-gar444age1.1-0 ROOT ; 
       setup-face2.1-0 ; 
       setup-face3.1-0 ; 
       setup-face4.1-0 ; 
       setup-face5.1-0 ; 
       setup-face6.1-0 ; 
       setup-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       setup-setup.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       1 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
