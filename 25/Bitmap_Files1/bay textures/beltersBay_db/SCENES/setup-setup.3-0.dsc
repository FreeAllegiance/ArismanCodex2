SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss306_ripcord-cam_int1.3-0 ROOT ; 
       ss306_ripcord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 21     
       11-spot1.1-0 ; 
       11-spot1_int.3-0 ROOT ; 
       setup-light1.3-0 ROOT ; 
       setup-light10.3-0 ROOT ; 
       setup-light11.3-0 ROOT ; 
       setup-light12.3-0 ROOT ; 
       setup-light13.3-0 ROOT ; 
       setup-light14.3-0 ROOT ; 
       setup-light15.3-0 ROOT ; 
       setup-light2.3-0 ROOT ; 
       setup-light3.3-0 ROOT ; 
       setup-light4.3-0 ROOT ; 
       setup-light5.3-0 ROOT ; 
       setup-light6.3-0 ROOT ; 
       setup-light7.3-0 ROOT ; 
       setup-light8.3-0 ROOT ; 
       setup-light9.3-0 ROOT ; 
       setup-spot6.1-0 ; 
       setup-spot6_int1.2-0 ROOT ; 
       setup-spot7.1-0 ; 
       setup-spot7_int1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       setup-mat1.1-0 ; 
       setup-mat2.1-0 ; 
       setup-mat3.1-0 ; 
       setup-mat4.1-0 ; 
       setup-mat5.1-0 ; 
       setup-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       root-gar444age1.1-0 ROOT ; 
       setup-face2.1-0 ; 
       setup-face3.1-0 ; 
       setup-face4.1-0 ; 
       setup-face5.1-0 ; 
       setup-face6.1-0 ; 
       setup-null1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/Bitmap_Files/bay textures/beltersBay_db/PICTURES/borg ; 
       //research/root/federation/shared_art_files/Bitmap_Files/bay textures/beltersBay_db/PICTURES/dayDecal ; 
       //research/root/federation/shared_art_files/Bitmap_Files/bay textures/beltersBay_db/PICTURES/rust ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       setup-setup.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       setup-t2d1.1-0 ; 
       setup-t2d2.1-0 ; 
       setup-t2d3.1-0 ; 
       setup-t2d4.1-0 ; 
       setup-t2d5.1-0 ; 
       setup-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       1 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       1 1 300 ; 
       3 4 300 ; 
       4 5 300 ; 
       5 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 1 400 ; 
       1 0 400 ; 
       1 5 400 ; 
       3 3 400 ; 
       4 4 400 ; 
       5 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       17 18 2110 ; 
       19 20 2110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       17 SCHEM 35 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       18 SCHEM 35 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       19 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       20 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 30 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
