SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_Decal-null3_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.15-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_Decal-default1.1-0 ; 
       add_Decal-default3.1-0 ; 
       add_Decal-default4.1-0 ; 
       add_Decal-mat1.1-0 ; 
       add_Decal-mat10.1-0 ; 
       add_Decal-mat12.1-0 ; 
       add_Decal-mat13.1-0 ; 
       add_Decal-mat14.1-0 ; 
       add_Decal-mat15.1-0 ; 
       add_Decal-mat16.1-0 ; 
       add_Decal-mat18.1-0 ; 
       add_Decal-mat19.1-0 ; 
       add_Decal-mat2.1-0 ; 
       add_Decal-mat20.1-0 ; 
       add_Decal-mat21.1-0 ; 
       add_Decal-mat22.1-0 ; 
       add_Decal-mat23.1-0 ; 
       add_Decal-mat24.1-0 ; 
       add_Decal-mat25.1-0 ; 
       add_Decal-mat3.1-0 ; 
       add_Decal-mat34.1-0 ; 
       add_Decal-mat4.1-0 ; 
       add_Decal-mat44.1-0 ; 
       add_Decal-mat45.1-0 ; 
       add_Decal-mat46.1-0 ; 
       add_Decal-mat47.1-0 ; 
       add_Decal-mat48.1-0 ; 
       add_Decal-mat49.1-0 ; 
       add_Decal-mat50.1-0 ; 
       add_Decal-mat51.1-0 ; 
       add_Decal-mat52.1-0 ; 
       add_Decal-mat53.1-0 ; 
       add_Decal-mat54.1-0 ; 
       add_Decal-mat55.1-0 ; 
       add_Decal-mat56.1-0 ; 
       add_Decal-mat57.1-0 ; 
       add_Decal-mat58.1-0 ; 
       add_Decal-mat59.1-0 ; 
       add_Decal-mat63.1-0 ; 
       add_Decal-mat64.1-0 ; 
       add_Decal-mat65.1-0 ; 
       add_Decal-mat66.1-0 ; 
       add_Decal-mat67.1-0 ; 
       add_Decal-mat69.1-0 ; 
       add_Decal-mat70.1-0 ; 
       add_Decal-mat71.1-0 ; 
       add_Decal-mat8.1-0 ; 
       add_Decal-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       add_Decal-abfuselg.1-0 ; 
       add_Decal-afuselg.1-0 ; 
       add_Decal-alengine.1-0 ; 
       add_Decal-alengine1.1-0 ; 
       add_Decal-atfuselg.1-0 ; 
       add_Decal-awepmnt.1-0 ; 
       add_Decal-bom01a_1.8-0 ; 
       add_Decal-cockpt.3-0 ; 
       add_Decal-ffuselg.11-0 ; 
       add_Decal-finzzz0.1-0 ; 
       add_Decal-finzzz1.1-0 ; 
       add_Decal-fuselg0.1-0 ; 
       add_Decal-fwepmnt.1-0 ; 
       add_Decal-lbwepemt.1-0 ; 
       add_Decal-lfinzzz.1-0 ; 
       add_Decal-lgun.1-0 ; 
       add_Decal-LL0.1-0 ; 
       add_Decal-LLf.1-0 ; 
       add_Decal-LLl.1-0 ; 
       add_Decal-LLr.1-0 ; 
       add_Decal-lthrust.1-0 ; 
       add_Decal-lthrust1.1-0 ; 
       add_Decal-lturbine.1-0 ; 
       add_Decal-lwepbar.1-0 ; 
       add_Decal-lwepbas.1-0 ; 
       add_Decal-lwepemt1.1-0 ; 
       add_Decal-lwepemt2.1-0 ; 
       add_Decal-lwepemt3.1-0 ; 
       add_Decal-null2.1-0 ; 
       add_Decal-null3_1.2-0 ROOT ; 
       add_Decal-rbwepemt.1-0 ; 
       add_Decal-rfinzzz.1-0 ; 
       add_Decal-rgun.1-0 ; 
       add_Decal-rthrust.1-0 ; 
       add_Decal-rthrust12.1-0 ; 
       add_Decal-rturbine.1-0 ; 
       add_Decal-rwepbar.1-0 ; 
       add_Decal-rwepbas.1-0 ; 
       add_Decal-rwepemt1.1-0 ; 
       add_Decal-rwepemt2.1-0 ; 
       add_Decal-rwepemt3.1-0 ; 
       add_Decal-SSal.1-0 ; 
       add_Decal-SSar.1-0 ; 
       add_Decal-SSf.1-0 ; 
       add_Decal-thrust0.1-0 ; 
       add_Decal-trail.1-0 ; 
       add_Decal-tturatt.1-0 ; 
       add_Decal-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-add_Decal.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_Decal-t2d11.1-0 ; 
       add_Decal-t2d12.1-0 ; 
       add_Decal-t2d13.1-0 ; 
       add_Decal-t2d14.1-0 ; 
       add_Decal-t2d15.1-0 ; 
       add_Decal-t2d17.1-0 ; 
       add_Decal-t2d18.1-0 ; 
       add_Decal-t2d2.1-0 ; 
       add_Decal-t2d21.1-0 ; 
       add_Decal-t2d3.1-0 ; 
       add_Decal-t2d30.1-0 ; 
       add_Decal-t2d31.1-0 ; 
       add_Decal-t2d53.1-0 ; 
       add_Decal-t2d54.1-0 ; 
       add_Decal-t2d55.1-0 ; 
       add_Decal-t2d56.1-0 ; 
       add_Decal-t2d58.1-0 ; 
       add_Decal-t2d59.1-0 ; 
       add_Decal-t2d61.1-0 ; 
       add_Decal-t2d62.1-0 ; 
       add_Decal-t2d63.1-0 ; 
       add_Decal-t2d64.1-0 ; 
       add_Decal-t2d65.1-0 ; 
       add_Decal-t2d69.1-0 ; 
       add_Decal-t2d7.1-0 ; 
       add_Decal-t2d70.1-0 ; 
       add_Decal-t2d71.1-0 ; 
       add_Decal-t2d72.1-0 ; 
       add_Decal-t2d73.1-0 ; 
       add_Decal-t2d74.1-0 ; 
       add_Decal-t2d75.1-0 ; 
       add_Decal-t2d76.1-0 ; 
       add_Decal-t2d77.1-0 ; 
       add_Decal-t2d78.1-0 ; 
       add_Decal-t2d79.1-0 ; 
       add_Decal-t2d8.1-0 ; 
       add_Decal-t2d80.1-0 ; 
       add_Decal-t2d81.1-0 ; 
       add_Decal-t2d82.1-0 ; 
       add_Decal-t2d83.1-0 ; 
       add_Decal-t2d84.1-0 ; 
       add_Decal-t2d85.1-0 ; 
       add_Decal-t2d86.1-0 ; 
       add_Decal-t2d87.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 28 110 ; 
       7 29 110 ; 
       8 11 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 6 110 ; 
       12 8 110 ; 
       13 23 110 ; 
       14 9 110 ; 
       15 21 110 ; 
       16 6 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 44 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 47 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 15 110 ; 
       28 29 110 ; 
       30 36 110 ; 
       31 9 110 ; 
       32 34 110 ; 
       33 35 110 ; 
       34 44 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       37 47 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 32 110 ; 
       41 14 110 ; 
       42 31 110 ; 
       43 8 110 ; 
       44 6 110 ; 
       45 29 110 ; 
       46 8 110 ; 
       47 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 9 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       6 3 300 ; 
       8 20 300 ; 
       8 22 300 ; 
       8 37 300 ; 
       10 11 300 ; 
       14 10 300 ; 
       15 43 300 ; 
       17 30 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       18 1 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       19 2 300 ; 
       19 35 300 ; 
       19 36 300 ; 
       21 46 300 ; 
       22 12 300 ; 
       22 19 300 ; 
       22 21 300 ; 
       23 23 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 38 300 ; 
       24 29 300 ; 
       31 45 300 ; 
       32 44 300 ; 
       34 47 300 ; 
       35 40 300 ; 
       35 41 300 ; 
       35 42 300 ; 
       36 26 300 ; 
       36 27 300 ; 
       36 28 300 ; 
       36 39 300 ; 
       37 0 300 ; 
       41 16 300 ; 
       42 17 300 ; 
       43 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 32 401 ; 
       1 36 401 ; 
       2 37 401 ; 
       4 21 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       45 43 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 11 401 ; 
       13 19 401 ; 
       14 20 401 ; 
       15 8 401 ; 
       19 7 401 ; 
       20 10 401 ; 
       21 9 401 ; 
       22 18 401 ; 
       23 31 401 ; 
       24 30 401 ; 
       25 29 401 ; 
       26 28 401 ; 
       27 27 401 ; 
       28 26 401 ; 
       29 33 401 ; 
       30 34 401 ; 
       31 12 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 22 401 ; 
       38 23 401 ; 
       39 25 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       46 24 401 ; 
       47 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 148.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 132.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 130 -12 0 MPRFLG 0 ; 
       4 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 147.5 -12 0 MPRFLG 0 ; 
       6 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 162.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       8 SCHEM 116.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 40 -8 0 MPRFLG 0 ; 
       11 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 120 -10 0 MPRFLG 0 ; 
       13 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 MPRFLG 0 ; 
       18 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 MPRFLG 0 ; 
       20 SCHEM 10 -12 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       23 SCHEM 80 -10 0 MPRFLG 0 ; 
       24 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       25 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 85 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 48.75 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       32 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       33 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 30 -8 0 MPRFLG 0 ; 
       35 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       36 SCHEM 95 -10 0 MPRFLG 0 ; 
       37 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       38 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       43 SCHEM 105 -10 0 MPRFLG 0 ; 
       44 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       45 SCHEM 165 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 88.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 140 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 150 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 166.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
