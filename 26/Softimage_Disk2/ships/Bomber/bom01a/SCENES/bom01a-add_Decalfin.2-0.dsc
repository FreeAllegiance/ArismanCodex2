SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_Decalfin-null3_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.17-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_Decalfin-default1.1-0 ; 
       add_Decalfin-default3.1-0 ; 
       add_Decalfin-default4.1-0 ; 
       add_Decalfin-mat1.1-0 ; 
       add_Decalfin-mat10.1-0 ; 
       add_Decalfin-mat12.1-0 ; 
       add_Decalfin-mat13.1-0 ; 
       add_Decalfin-mat14.1-0 ; 
       add_Decalfin-mat15.1-0 ; 
       add_Decalfin-mat16.1-0 ; 
       add_Decalfin-mat19.1-0 ; 
       add_Decalfin-mat2.1-0 ; 
       add_Decalfin-mat20.1-0 ; 
       add_Decalfin-mat21.1-0 ; 
       add_Decalfin-mat22.1-0 ; 
       add_Decalfin-mat23.1-0 ; 
       add_Decalfin-mat24.1-0 ; 
       add_Decalfin-mat25.1-0 ; 
       add_Decalfin-mat3.1-0 ; 
       add_Decalfin-mat34.1-0 ; 
       add_Decalfin-mat4.1-0 ; 
       add_Decalfin-mat44.1-0 ; 
       add_Decalfin-mat45.1-0 ; 
       add_Decalfin-mat46.1-0 ; 
       add_Decalfin-mat47.1-0 ; 
       add_Decalfin-mat48.1-0 ; 
       add_Decalfin-mat49.1-0 ; 
       add_Decalfin-mat50.1-0 ; 
       add_Decalfin-mat51.1-0 ; 
       add_Decalfin-mat52.1-0 ; 
       add_Decalfin-mat53.1-0 ; 
       add_Decalfin-mat54.1-0 ; 
       add_Decalfin-mat55.1-0 ; 
       add_Decalfin-mat56.1-0 ; 
       add_Decalfin-mat57.1-0 ; 
       add_Decalfin-mat58.1-0 ; 
       add_Decalfin-mat59.1-0 ; 
       add_Decalfin-mat63.1-0 ; 
       add_Decalfin-mat64.1-0 ; 
       add_Decalfin-mat65.1-0 ; 
       add_Decalfin-mat66.1-0 ; 
       add_Decalfin-mat67.1-0 ; 
       add_Decalfin-mat69.1-0 ; 
       add_Decalfin-mat70.1-0 ; 
       add_Decalfin-mat71.1-0 ; 
       add_Decalfin-mat72.1-0 ; 
       add_Decalfin-mat8.1-0 ; 
       add_Decalfin-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       add_Decalfin-abfuselg.1-0 ; 
       add_Decalfin-afuselg.1-0 ; 
       add_Decalfin-alengine.1-0 ; 
       add_Decalfin-alengine1.1-0 ; 
       add_Decalfin-atfuselg.1-0 ; 
       add_Decalfin-awepmnt.1-0 ; 
       add_Decalfin-bom01a_1.8-0 ; 
       add_Decalfin-cockpt.3-0 ; 
       add_Decalfin-ffuselg.11-0 ; 
       add_Decalfin-finzzz0.1-0 ; 
       add_Decalfin-finzzz1.1-0 ; 
       add_Decalfin-fuselg0.1-0 ; 
       add_Decalfin-fwepmnt.1-0 ; 
       add_Decalfin-lbwepemt.1-0 ; 
       add_Decalfin-Lfinzzz.1-0 ; 
       add_Decalfin-lgun.1-0 ; 
       add_Decalfin-LL0.1-0 ; 
       add_Decalfin-LLf.1-0 ; 
       add_Decalfin-LLl.1-0 ; 
       add_Decalfin-LLr.1-0 ; 
       add_Decalfin-lthrust.1-0 ; 
       add_Decalfin-lthrust1.1-0 ; 
       add_Decalfin-lturbine.1-0 ; 
       add_Decalfin-lwepbar.1-0 ; 
       add_Decalfin-lwepbas.1-0 ; 
       add_Decalfin-lwepemt1.1-0 ; 
       add_Decalfin-lwepemt2.1-0 ; 
       add_Decalfin-lwepemt3.1-0 ; 
       add_Decalfin-null2.1-0 ; 
       add_Decalfin-null3_1.2-0 ROOT ; 
       add_Decalfin-rbwepemt.1-0 ; 
       add_Decalfin-rfinzzz.1-0 ; 
       add_Decalfin-rgun.1-0 ; 
       add_Decalfin-rthrust.1-0 ; 
       add_Decalfin-rthrust12.1-0 ; 
       add_Decalfin-rturbine.1-0 ; 
       add_Decalfin-rwepbar.1-0 ; 
       add_Decalfin-rwepbas.1-0 ; 
       add_Decalfin-rwepemt1.1-0 ; 
       add_Decalfin-rwepemt2.1-0 ; 
       add_Decalfin-rwepemt3.1-0 ; 
       add_Decalfin-SSal.1-0 ; 
       add_Decalfin-SSar.1-0 ; 
       add_Decalfin-SSf.1-0 ; 
       add_Decalfin-thrust0.1-0 ; 
       add_Decalfin-trail.1-0 ; 
       add_Decalfin-tturatt.1-0 ; 
       add_Decalfin-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-add_Decalfin.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_Decalfin-t2d11.1-0 ; 
       add_Decalfin-t2d12.1-0 ; 
       add_Decalfin-t2d13.1-0 ; 
       add_Decalfin-t2d14.1-0 ; 
       add_Decalfin-t2d15.1-0 ; 
       add_Decalfin-t2d18.1-0 ; 
       add_Decalfin-t2d2.1-0 ; 
       add_Decalfin-t2d21.1-0 ; 
       add_Decalfin-t2d3.1-0 ; 
       add_Decalfin-t2d30.1-0 ; 
       add_Decalfin-t2d31.1-0 ; 
       add_Decalfin-t2d53.1-0 ; 
       add_Decalfin-t2d54.1-0 ; 
       add_Decalfin-t2d55.1-0 ; 
       add_Decalfin-t2d56.1-0 ; 
       add_Decalfin-t2d58.1-0 ; 
       add_Decalfin-t2d59.1-0 ; 
       add_Decalfin-t2d61.1-0 ; 
       add_Decalfin-t2d62.1-0 ; 
       add_Decalfin-t2d63.1-0 ; 
       add_Decalfin-t2d64.1-0 ; 
       add_Decalfin-t2d65.1-0 ; 
       add_Decalfin-t2d69.1-0 ; 
       add_Decalfin-t2d7.1-0 ; 
       add_Decalfin-t2d70.1-0 ; 
       add_Decalfin-t2d71.1-0 ; 
       add_Decalfin-t2d72.1-0 ; 
       add_Decalfin-t2d73.1-0 ; 
       add_Decalfin-t2d74.1-0 ; 
       add_Decalfin-t2d75.1-0 ; 
       add_Decalfin-t2d76.1-0 ; 
       add_Decalfin-t2d77.1-0 ; 
       add_Decalfin-t2d78.1-0 ; 
       add_Decalfin-t2d79.1-0 ; 
       add_Decalfin-t2d8.1-0 ; 
       add_Decalfin-t2d80.1-0 ; 
       add_Decalfin-t2d81.1-0 ; 
       add_Decalfin-t2d82.1-0 ; 
       add_Decalfin-t2d83.1-0 ; 
       add_Decalfin-t2d84.1-0 ; 
       add_Decalfin-t2d85.1-0 ; 
       add_Decalfin-t2d86.1-0 ; 
       add_Decalfin-t2d87.1-0 ; 
       add_Decalfin-t2d88.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 28 110 ; 
       7 29 110 ; 
       8 11 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 6 110 ; 
       12 8 110 ; 
       13 23 110 ; 
       15 21 110 ; 
       16 6 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 44 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 47 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 15 110 ; 
       28 29 110 ; 
       30 36 110 ; 
       31 9 110 ; 
       32 34 110 ; 
       33 35 110 ; 
       34 44 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       37 47 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 32 110 ; 
       41 14 110 ; 
       42 31 110 ; 
       43 8 110 ; 
       44 6 110 ; 
       45 29 110 ; 
       46 8 110 ; 
       47 6 110 ; 
       14 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 9 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       6 3 300 ; 
       8 19 300 ; 
       8 21 300 ; 
       8 36 300 ; 
       10 10 300 ; 
       15 42 300 ; 
       17 29 300 ; 
       17 30 300 ; 
       17 31 300 ; 
       18 1 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       19 2 300 ; 
       19 34 300 ; 
       19 35 300 ; 
       21 46 300 ; 
       22 11 300 ; 
       22 18 300 ; 
       22 20 300 ; 
       23 22 300 ; 
       23 23 300 ; 
       23 24 300 ; 
       23 37 300 ; 
       24 28 300 ; 
       31 44 300 ; 
       32 43 300 ; 
       34 47 300 ; 
       35 39 300 ; 
       35 40 300 ; 
       35 41 300 ; 
       36 25 300 ; 
       36 26 300 ; 
       36 27 300 ; 
       36 38 300 ; 
       37 0 300 ; 
       41 15 300 ; 
       42 16 300 ; 
       43 17 300 ; 
       14 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 35 401 ; 
       2 36 401 ; 
       4 20 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       44 42 401 ; 
       10 5 401 ; 
       11 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 7 401 ; 
       18 6 401 ; 
       19 9 401 ; 
       20 8 401 ; 
       21 17 401 ; 
       22 30 401 ; 
       23 29 401 ; 
       24 28 401 ; 
       25 27 401 ; 
       26 26 401 ; 
       27 25 401 ; 
       28 32 401 ; 
       29 33 401 ; 
       30 11 401 ; 
       31 12 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 21 401 ; 
       37 22 401 ; 
       38 24 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       46 23 401 ; 
       47 34 401 ; 
       45 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 148.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 132.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 130 -12 0 MPRFLG 0 ; 
       4 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 147.5 -12 0 MPRFLG 0 ; 
       6 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 162.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       8 SCHEM 116.25 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 40 -8 0 MPRFLG 0 ; 
       11 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 120 -10 0 MPRFLG 0 ; 
       13 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 MPRFLG 0 ; 
       18 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 MPRFLG 0 ; 
       20 SCHEM 10 -12 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       23 SCHEM 80 -10 0 MPRFLG 0 ; 
       24 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       25 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 85 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       32 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       33 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 30 -8 0 MPRFLG 0 ; 
       35 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       36 SCHEM 95 -10 0 MPRFLG 0 ; 
       37 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       38 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       43 SCHEM 105 -10 0 MPRFLG 0 ; 
       44 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       45 SCHEM 165 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 88.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 48.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 140 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 150 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 166.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
