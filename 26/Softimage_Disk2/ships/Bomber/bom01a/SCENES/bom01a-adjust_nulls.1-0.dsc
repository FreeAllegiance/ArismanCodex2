SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-null3_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.21-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       check_nulls-default1.2-0 ; 
       check_nulls-default3.1-0 ; 
       check_nulls-default4.1-0 ; 
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat25.1-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat45.2-0 ; 
       check_nulls-mat46.2-0 ; 
       check_nulls-mat47.2-0 ; 
       check_nulls-mat48.2-0 ; 
       check_nulls-mat49.2-0 ; 
       check_nulls-mat50.2-0 ; 
       check_nulls-mat51.2-0 ; 
       check_nulls-mat52.1-0 ; 
       check_nulls-mat53.1-0 ; 
       check_nulls-mat54.1-0 ; 
       check_nulls-mat55.1-0 ; 
       check_nulls-mat56.1-0 ; 
       check_nulls-mat57.1-0 ; 
       check_nulls-mat58.1-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat63.2-0 ; 
       check_nulls-mat64.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat69.2-0 ; 
       check_nulls-mat70.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat9.2-0 ; 
       fix_blinker-mat23.2-0 ; 
       fix_blinker-mat24.2-0 ; 
       fix_blinker-mat71.3-0 ; 
       fix_blinker-mat72.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1.8-0 ; 
       check_nulls-cockpt_1.3-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLl.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-null2.1-0 ; 
       check_nulls-null3_1.2-0 ROOT ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-adjust_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d53.1-0 ; 
       check_nulls-t2d54.1-0 ; 
       check_nulls-t2d55.1-0 ; 
       check_nulls-t2d56.1-0 ; 
       check_nulls-t2d58.1-0 ; 
       check_nulls-t2d59.1-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d69.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d70.2-0 ; 
       check_nulls-t2d71.2-0 ; 
       check_nulls-t2d72.2-0 ; 
       check_nulls-t2d73.2-0 ; 
       check_nulls-t2d74.2-0 ; 
       check_nulls-t2d75.2-0 ; 
       check_nulls-t2d76.2-0 ; 
       check_nulls-t2d77.2-0 ; 
       check_nulls-t2d78.2-0 ; 
       check_nulls-t2d79.1-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d80.1-0 ; 
       check_nulls-t2d81.1-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       check_nulls-t2d85.2-0 ; 
       check_nulls-t2d86.2-0 ; 
       fix_blinker-t2d87.3-0 ; 
       fix_blinker-t2d88.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 9 110 ; 
       2 5 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 12 110 ; 
       6 2 110 ; 
       7 29 110 ; 
       8 30 110 ; 
       9 12 110 ; 
       10 7 110 ; 
       11 10 110 ; 
       12 7 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 21 110 ; 
       16 7 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 44 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 47 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 15 110 ; 
       28 23 110 ; 
       29 30 110 ; 
       31 10 110 ; 
       32 34 110 ; 
       33 35 110 ; 
       34 44 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       37 47 110 ; 
       38 9 110 ; 
       39 9 110 ; 
       40 32 110 ; 
       41 14 110 ; 
       42 31 110 ; 
       43 9 110 ; 
       44 7 110 ; 
       45 30 110 ; 
       46 9 110 ; 
       47 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 43 300 ; 
       2 9 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       7 3 300 ; 
       9 17 300 ; 
       9 19 300 ; 
       9 34 300 ; 
       11 10 300 ; 
       14 48 300 ; 
       15 40 300 ; 
       17 27 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       18 1 300 ; 
       18 30 300 ; 
       18 31 300 ; 
       19 2 300 ; 
       19 32 300 ; 
       19 33 300 ; 
       21 42 300 ; 
       22 11 300 ; 
       22 16 300 ; 
       22 18 300 ; 
       23 20 300 ; 
       23 21 300 ; 
       23 22 300 ; 
       23 35 300 ; 
       24 26 300 ; 
       31 47 300 ; 
       32 41 300 ; 
       34 44 300 ; 
       35 37 300 ; 
       35 38 300 ; 
       35 39 300 ; 
       36 23 300 ; 
       36 24 300 ; 
       36 25 300 ; 
       36 36 300 ; 
       37 0 300 ; 
       41 45 300 ; 
       42 46 300 ; 
       43 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       30 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 35 401 ; 
       2 36 401 ; 
       4 20 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 7 401 ; 
       16 6 401 ; 
       17 9 401 ; 
       18 8 401 ; 
       19 17 401 ; 
       20 30 401 ; 
       21 29 401 ; 
       22 28 401 ; 
       23 27 401 ; 
       24 26 401 ; 
       25 25 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       28 11 401 ; 
       29 12 401 ; 
       30 13 401 ; 
       31 14 401 ; 
       32 15 401 ; 
       33 16 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       41 41 401 ; 
       42 23 401 ; 
       44 34 401 ; 
       47 42 401 ; 
       48 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 65 -12 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 65 -8 0 MPRFLG 0 ; 
       6 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       7 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 70 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 MPRFLG 0 ; 
       12 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 50 -10 0 MPRFLG 0 ; 
       14 SCHEM 15.3673 -13.23221 0 USR MPRFLG 0 ; 
       15 SCHEM 5 -10 0 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 25 -8 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 30 -10 0 MPRFLG 0 ; 
       24 SCHEM 30 -8 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.02023 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       30 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 20.3673 -13.23221 0 USR MPRFLG 0 ; 
       32 SCHEM 10 -10 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       35 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 15.3673 -15.23221 0 MPRFLG 0 ; 
       42 SCHEM 20.3673 -15.23221 0 MPRFLG 0 ; 
       43 SCHEM 35 -10 0 MPRFLG 0 ; 
       44 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       45 SCHEM 72.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 31.25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14.3673 -17.23221 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19.3673 -17.23221 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.8673 -15.23221 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.8673 -15.23221 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 69 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 69 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 69 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.8673 -17.23221 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.8673 -17.23221 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 79 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
