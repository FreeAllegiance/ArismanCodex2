SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       animate-null3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.7-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       animate-default1.1-0 ; 
       animate-default3.1-0 ; 
       animate-default4.1-0 ; 
       animate-mat1.1-0 ; 
       animate-mat10.1-0 ; 
       animate-mat12.1-0 ; 
       animate-mat13.1-0 ; 
       animate-mat14.1-0 ; 
       animate-mat15.1-0 ; 
       animate-mat16.1-0 ; 
       animate-mat17.1-0 ; 
       animate-mat18.1-0 ; 
       animate-mat19.1-0 ; 
       animate-mat2.1-0 ; 
       animate-mat20.1-0 ; 
       animate-mat21.1-0 ; 
       animate-mat22.1-0 ; 
       animate-mat23.1-0 ; 
       animate-mat24.1-0 ; 
       animate-mat25.1-0 ; 
       animate-mat3.1-0 ; 
       animate-mat34.1-0 ; 
       animate-mat4.1-0 ; 
       animate-mat44.1-0 ; 
       animate-mat45.1-0 ; 
       animate-mat46.1-0 ; 
       animate-mat47.1-0 ; 
       animate-mat48.1-0 ; 
       animate-mat49.1-0 ; 
       animate-mat50.1-0 ; 
       animate-mat51.1-0 ; 
       animate-mat52.1-0 ; 
       animate-mat53.1-0 ; 
       animate-mat54.1-0 ; 
       animate-mat55.1-0 ; 
       animate-mat56.1-0 ; 
       animate-mat57.1-0 ; 
       animate-mat58.1-0 ; 
       animate-mat59.1-0 ; 
       animate-mat63.1-0 ; 
       animate-mat64.1-0 ; 
       animate-mat65.1-0 ; 
       animate-mat66.1-0 ; 
       animate-mat67.1-0 ; 
       animate-mat69.1-0 ; 
       animate-mat70.1-0 ; 
       animate-mat8.1-0 ; 
       animate-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       animate-abfuselg.1-0 ; 
       animate-afuselg.1-0 ; 
       animate-alengine.1-0 ; 
       animate-alengine1.1-0 ; 
       animate-atfuselg.1-0 ; 
       animate-awepmnt.1-0 ; 
       animate-bom01a_1.8-0 ; 
       animate-ffuselg.11-0 ; 
       animate-finzzz0.1-0 ; 
       animate-finzzz1.1-0 ; 
       animate-fuselg0.1-0 ; 
       animate-fwepmnt.1-0 ; 
       animate-l-gun.1-0 ; 
       animate-l-gun1.1-0 ; 
       animate-lbwepemt.1-0 ; 
       animate-lfinzzz.1-0 ; 
       animate-LL0.1-0 ; 
       animate-LLf.1-0 ; 
       animate-LLl.1-0 ; 
       animate-LLr.1-0 ; 
       animate-lthrust.1-0 ; 
       animate-lthrust1.1-0 ; 
       animate-lturbine.1-0 ; 
       animate-lwepbar.1-0 ; 
       animate-lwepbas.1-0 ; 
       animate-lwepemt1.1-0 ; 
       animate-lwepemt2.1-0 ; 
       animate-null2.1-0 ; 
       animate-null3.3-0 ROOT ; 
       animate-rbwepemt.1-0 ; 
       animate-rfinzzz.1-0 ; 
       animate-rotation_limit.1-0 ROOT ; 
       animate-rthrust.1-0 ; 
       animate-rthrust12.1-0 ; 
       animate-rturbine.1-0 ; 
       animate-rwepbar.1-0 ; 
       animate-rwepbas.1-0 ; 
       animate-rwepemt1.1-0 ; 
       animate-rwepemt2.1-0 ; 
       animate-SSal.1-0 ; 
       animate-SSar.1-0 ; 
       animate-SSf.1-0 ; 
       animate-thrust0.1-0 ; 
       animate-tturatt.1-0 ; 
       animate-wepbas0.1-0 ; 
       bom07-cockpt.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-animate.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       animate-t2d11.1-0 ; 
       animate-t2d12.1-0 ; 
       animate-t2d13.1-0 ; 
       animate-t2d14.1-0 ; 
       animate-t2d15.1-0 ; 
       animate-t2d16.1-0 ; 
       animate-t2d17.1-0 ; 
       animate-t2d18.1-0 ; 
       animate-t2d2.1-0 ; 
       animate-t2d21.1-0 ; 
       animate-t2d3.1-0 ; 
       animate-t2d30.1-0 ; 
       animate-t2d31.1-0 ; 
       animate-t2d53.1-0 ; 
       animate-t2d54.1-0 ; 
       animate-t2d55.1-0 ; 
       animate-t2d56.1-0 ; 
       animate-t2d58.1-0 ; 
       animate-t2d59.1-0 ; 
       animate-t2d61.1-0 ; 
       animate-t2d62.1-0 ; 
       animate-t2d63.1-0 ; 
       animate-t2d64.1-0 ; 
       animate-t2d65.1-0 ; 
       animate-t2d69.1-0 ; 
       animate-t2d7.1-0 ; 
       animate-t2d70.1-0 ; 
       animate-t2d71.1-0 ; 
       animate-t2d72.1-0 ; 
       animate-t2d73.1-0 ; 
       animate-t2d74.1-0 ; 
       animate-t2d75.1-0 ; 
       animate-t2d76.1-0 ; 
       animate-t2d77.1-0 ; 
       animate-t2d78.1-0 ; 
       animate-t2d79.1-0 ; 
       animate-t2d8.1-0 ; 
       animate-t2d80.1-0 ; 
       animate-t2d81.1-0 ; 
       animate-t2d82.1-0 ; 
       animate-t2d83.1-0 ; 
       animate-t2d84.1-0 ; 
       animate-t2d85.1-0 ; 
       animate-t2d86.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 10 110 ; 
       5 1 110 ; 
       6 27 110 ; 
       7 10 110 ; 
       8 6 110 ; 
       9 8 110 ; 
       10 6 110 ; 
       11 7 110 ; 
       12 21 110 ; 
       14 23 110 ; 
       15 8 110 ; 
       16 6 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 42 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 44 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 28 110 ; 
       29 35 110 ; 
       30 8 110 ; 
       32 34 110 ; 
       33 42 110 ; 
       34 33 110 ; 
       35 36 110 ; 
       36 44 110 ; 
       37 7 110 ; 
       38 7 110 ; 
       39 15 110 ; 
       40 30 110 ; 
       41 7 110 ; 
       42 6 110 ; 
       43 7 110 ; 
       44 6 110 ; 
       13 33 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 9 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       6 3 300 ; 
       7 21 300 ; 
       7 23 300 ; 
       7 38 300 ; 
       9 12 300 ; 
       12 44 300 ; 
       15 11 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       18 1 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       19 2 300 ; 
       19 36 300 ; 
       19 37 300 ; 
       21 46 300 ; 
       22 13 300 ; 
       22 20 300 ; 
       22 22 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 26 300 ; 
       23 39 300 ; 
       24 30 300 ; 
       30 10 300 ; 
       33 47 300 ; 
       34 41 300 ; 
       34 42 300 ; 
       34 43 300 ; 
       35 27 300 ; 
       35 28 300 ; 
       35 29 300 ; 
       35 40 300 ; 
       36 0 300 ; 
       39 17 300 ; 
       40 18 300 ; 
       41 19 300 ; 
       13 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       28 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       46 25 401 ; 
       47 36 401 ; 
       45 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       45 SCHEM 76.11894 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       0 SCHEM 56.92675 -9.51318 0 USR MPRFLG 0 ; 
       1 SCHEM 59.42677 -9.51318 0 USR MPRFLG 0 ; 
       2 SCHEM 58.17675 -11.51318 0 MPRFLG 0 ; 
       3 SCHEM 55.67675 -11.51318 0 MPRFLG 0 ; 
       4 SCHEM 58.17675 -7.513179 0 USR MPRFLG 0 ; 
       5 SCHEM 59.42677 -11.51318 0 USR MPRFLG 0 ; 
       6 SCHEM 34.4268 -3.513181 0 USR MPRFLG 0 ; 
       7 SCHEM 46.92677 -7.513179 0 USR MPRFLG 0 ; 
       8 SCHEM 21.92679 -5.513179 0 USR MPRFLG 0 ; 
       9 SCHEM 19.42679 -7.513179 0 USR MPRFLG 0 ; 
       10 SCHEM 49.42677 -5.513179 0 USR MPRFLG 0 ; 
       11 SCHEM 54.42677 -9.51318 0 USR MPRFLG 0 ; 
       12 SCHEM 9.426802 -9.51318 0 MPRFLG 0 ; 
       14 SCHEM 34.4268 -11.51318 0 USR MPRFLG 0 ; 
       15 SCHEM 21.92679 -7.513179 0 USR MPRFLG 0 ; 
       16 SCHEM 29.42679 -5.513179 0 USR MPRFLG 0 ; 
       17 SCHEM 26.92678 -7.513179 0 USR MPRFLG 0 ; 
       18 SCHEM 29.42679 -7.513179 0 USR MPRFLG 0 ; 
       19 SCHEM 31.92679 -7.513179 0 USR MPRFLG 0 ; 
       20 SCHEM 11.9268 -11.51318 0 MPRFLG 0 ; 
       21 SCHEM 10.6768 -7.513179 0 MPRFLG 0 ; 
       22 SCHEM 11.9268 -9.51318 0 MPRFLG 0 ; 
       23 SCHEM 34.4268 -9.51318 0 USR MPRFLG 0 ; 
       24 SCHEM 34.4268 -7.513179 0 USR MPRFLG 0 ; 
       25 SCHEM 44.42677 -9.51318 0 USR MPRFLG 0 ; 
       26 SCHEM 41.9268 -9.51318 0 USR MPRFLG 0 ; 
       27 SCHEM 34.4268 -1.513181 0 USR MPRFLG 0 ; 
       28 SCHEM 34.4268 0.4868187 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 36.9268 -11.51318 0 USR MPRFLG 0 ; 
       30 SCHEM 24.42679 -7.513179 0 USR MPRFLG 0 ; 
       31 SCHEM 80.67677 0.4868187 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       32 SCHEM 16.9268 -11.51318 0 MPRFLG 0 ; 
       33 SCHEM 15.6768 -7.513179 0 MPRFLG 0 ; 
       34 SCHEM 16.9268 -9.51318 0 MPRFLG 0 ; 
       35 SCHEM 36.9268 -9.51318 0 USR MPRFLG 0 ; 
       36 SCHEM 36.9268 -7.513179 0 USR MPRFLG 0 ; 
       37 SCHEM 49.42677 -9.51318 0 USR MPRFLG 0 ; 
       38 SCHEM 46.92677 -9.51318 0 USR MPRFLG 0 ; 
       39 SCHEM 21.92679 -9.51318 0 USR MPRFLG 0 ; 
       40 SCHEM 24.42679 -9.51318 0 USR MPRFLG 0 ; 
       41 SCHEM 39.4268 -9.51318 0 USR MPRFLG 0 ; 
       42 SCHEM 13.1768 -5.513179 0 USR MPRFLG 0 ; 
       43 SCHEM 51.92677 -9.51318 0 USR MPRFLG 0 ; 
       44 SCHEM 35.6768 -5.513179 0 USR MPRFLG 0 ; 
       13 SCHEM 14.4268 -9.51318 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 38.4268 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.17677 -5.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60.92677 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 23.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 18.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20.92679 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 23.42679 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35.9268 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 8.426802 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 12.1768 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 17.29679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.92677 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 23.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 18.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 12.1768 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 38.4268 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 35.9268 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 17.29679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 8.426802 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 62.1768 -1.513181 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 14 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
