SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       final-null3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.9-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       final-default1.1-0 ; 
       final-default3.1-0 ; 
       final-default4.1-0 ; 
       final-mat1.1-0 ; 
       final-mat10.1-0 ; 
       final-mat12.1-0 ; 
       final-mat13.1-0 ; 
       final-mat14.1-0 ; 
       final-mat15.1-0 ; 
       final-mat16.1-0 ; 
       final-mat17.1-0 ; 
       final-mat18.1-0 ; 
       final-mat19.1-0 ; 
       final-mat2.1-0 ; 
       final-mat20.1-0 ; 
       final-mat21.1-0 ; 
       final-mat22.1-0 ; 
       final-mat23.1-0 ; 
       final-mat24.1-0 ; 
       final-mat25.1-0 ; 
       final-mat3.1-0 ; 
       final-mat34.1-0 ; 
       final-mat4.1-0 ; 
       final-mat44.1-0 ; 
       final-mat45.1-0 ; 
       final-mat46.1-0 ; 
       final-mat47.1-0 ; 
       final-mat48.1-0 ; 
       final-mat49.1-0 ; 
       final-mat50.1-0 ; 
       final-mat51.1-0 ; 
       final-mat52.1-0 ; 
       final-mat53.1-0 ; 
       final-mat54.1-0 ; 
       final-mat55.1-0 ; 
       final-mat56.1-0 ; 
       final-mat57.1-0 ; 
       final-mat58.1-0 ; 
       final-mat59.1-0 ; 
       final-mat63.1-0 ; 
       final-mat64.1-0 ; 
       final-mat65.1-0 ; 
       final-mat66.1-0 ; 
       final-mat67.1-0 ; 
       final-mat69.1-0 ; 
       final-mat70.1-0 ; 
       final-mat8.1-0 ; 
       final-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       bom07-cockpt_1.1-0 ROOT ; 
       final-abfuselg.1-0 ; 
       final-afuselg.1-0 ; 
       final-alengine.1-0 ; 
       final-alengine1.1-0 ; 
       final-atfuselg.1-0 ; 
       final-awepmnt.1-0 ; 
       final-bom01a_1.8-0 ; 
       final-ffuselg.11-0 ; 
       final-finzzz0.1-0 ; 
       final-finzzz1.1-0 ; 
       final-fuselg0.1-0 ; 
       final-fwepmnt.1-0 ; 
       final-l-gun.1-0 ; 
       final-l-gun1.1-0 ; 
       final-lbwepemt.1-0 ; 
       final-lfinzzz.1-0 ; 
       final-LL0.1-0 ; 
       final-LLf.1-0 ; 
       final-LLl.1-0 ; 
       final-LLr.1-0 ; 
       final-lthrust.1-0 ; 
       final-lthrust1.1-0 ; 
       final-lturbine.1-0 ; 
       final-lwepbar.1-0 ; 
       final-lwepbas.1-0 ; 
       final-lwepemt1.1-0 ; 
       final-lwepemt2.1-0 ; 
       final-null2.1-0 ; 
       final-null3.2-0 ROOT ; 
       final-rbwepemt.1-0 ; 
       final-rfinzzz.1-0 ; 
       final-rotation_limit.1-0 ROOT ; 
       final-rthrust.1-0 ; 
       final-rthrust12.1-0 ; 
       final-rturbine.1-0 ; 
       final-rwepbar.1-0 ; 
       final-rwepbas.1-0 ; 
       final-rwepemt1.1-0 ; 
       final-rwepemt2.1-0 ; 
       final-SSal.1-0 ; 
       final-SSar.1-0 ; 
       final-SSf.1-0 ; 
       final-thrust0.1-0 ; 
       final-tturatt.1-0 ; 
       final-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-final.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       final-t2d11.1-0 ; 
       final-t2d12.1-0 ; 
       final-t2d13.1-0 ; 
       final-t2d14.1-0 ; 
       final-t2d15.1-0 ; 
       final-t2d16.1-0 ; 
       final-t2d17.1-0 ; 
       final-t2d18.1-0 ; 
       final-t2d2.1-0 ; 
       final-t2d21.1-0 ; 
       final-t2d3.1-0 ; 
       final-t2d30.1-0 ; 
       final-t2d31.1-0 ; 
       final-t2d53.1-0 ; 
       final-t2d54.1-0 ; 
       final-t2d55.1-0 ; 
       final-t2d56.1-0 ; 
       final-t2d58.1-0 ; 
       final-t2d59.1-0 ; 
       final-t2d61.1-0 ; 
       final-t2d62.1-0 ; 
       final-t2d63.1-0 ; 
       final-t2d64.1-0 ; 
       final-t2d65.1-0 ; 
       final-t2d69.1-0 ; 
       final-t2d7.1-0 ; 
       final-t2d70.1-0 ; 
       final-t2d71.1-0 ; 
       final-t2d72.1-0 ; 
       final-t2d73.1-0 ; 
       final-t2d74.1-0 ; 
       final-t2d75.1-0 ; 
       final-t2d76.1-0 ; 
       final-t2d77.1-0 ; 
       final-t2d78.1-0 ; 
       final-t2d79.1-0 ; 
       final-t2d8.1-0 ; 
       final-t2d80.1-0 ; 
       final-t2d81.1-0 ; 
       final-t2d82.1-0 ; 
       final-t2d83.1-0 ; 
       final-t2d84.1-0 ; 
       final-t2d85.1-0 ; 
       final-t2d86.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 5 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 11 110 ; 
       6 2 110 ; 
       7 28 110 ; 
       8 11 110 ; 
       9 7 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 22 110 ; 
       14 34 110 ; 
       15 24 110 ; 
       16 9 110 ; 
       17 7 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 23 110 ; 
       22 43 110 ; 
       23 22 110 ; 
       24 25 110 ; 
       25 45 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 29 110 ; 
       30 36 110 ; 
       31 9 110 ; 
       33 35 110 ; 
       34 43 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       37 45 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 16 110 ; 
       41 31 110 ; 
       42 8 110 ; 
       43 7 110 ; 
       44 8 110 ; 
       45 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       7 3 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       8 38 300 ; 
       10 12 300 ; 
       13 44 300 ; 
       14 45 300 ; 
       16 11 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       19 1 300 ; 
       19 34 300 ; 
       19 35 300 ; 
       20 2 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       22 46 300 ; 
       23 13 300 ; 
       23 20 300 ; 
       23 22 300 ; 
       24 24 300 ; 
       24 25 300 ; 
       24 26 300 ; 
       24 39 300 ; 
       25 30 300 ; 
       31 10 300 ; 
       34 47 300 ; 
       35 41 300 ; 
       35 42 300 ; 
       35 43 300 ; 
       36 27 300 ; 
       36 28 300 ; 
       36 29 300 ; 
       36 40 300 ; 
       37 0 300 ; 
       40 17 300 ; 
       41 18 300 ; 
       42 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 25 401 ; 
       47 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 56.92675 -9.51318 0 USR MPRFLG 0 ; 
       2 SCHEM 59.42677 -9.51318 0 USR MPRFLG 0 ; 
       3 SCHEM 58.17675 -11.51318 0 MPRFLG 0 ; 
       4 SCHEM 55.67675 -11.51318 0 MPRFLG 0 ; 
       5 SCHEM 58.17675 -7.513179 0 USR MPRFLG 0 ; 
       6 SCHEM 59.42677 -11.51318 0 USR MPRFLG 0 ; 
       7 SCHEM 34.4268 -3.513181 0 USR MPRFLG 0 ; 
       8 SCHEM 46.92677 -7.513179 0 USR MPRFLG 0 ; 
       9 SCHEM 21.92679 -5.513179 0 USR MPRFLG 0 ; 
       10 SCHEM 19.42679 -7.513179 0 USR MPRFLG 0 ; 
       11 SCHEM 49.42677 -5.513179 0 USR MPRFLG 0 ; 
       12 SCHEM 54.42677 -9.51318 0 USR MPRFLG 0 ; 
       13 SCHEM 9.426802 -9.51318 0 MPRFLG 0 ; 
       14 SCHEM 14.4268 -9.51318 0 MPRFLG 0 ; 
       15 SCHEM 34.4268 -11.51318 0 USR MPRFLG 0 ; 
       16 SCHEM 21.92679 -7.513179 0 USR MPRFLG 0 ; 
       17 SCHEM 29.42679 -5.513179 0 USR MPRFLG 0 ; 
       18 SCHEM 26.92678 -7.513179 0 USR MPRFLG 0 ; 
       19 SCHEM 29.42679 -7.513179 0 USR MPRFLG 0 ; 
       20 SCHEM 31.92679 -7.513179 0 USR MPRFLG 0 ; 
       21 SCHEM 11.9268 -11.51318 0 MPRFLG 0 ; 
       22 SCHEM 10.6768 -7.513179 0 MPRFLG 0 ; 
       23 SCHEM 11.9268 -9.51318 0 MPRFLG 0 ; 
       24 SCHEM 34.4268 -9.51318 0 USR MPRFLG 0 ; 
       25 SCHEM 34.4268 -7.513179 0 USR MPRFLG 0 ; 
       26 SCHEM 44.42677 -9.51318 0 USR MPRFLG 0 ; 
       27 SCHEM 41.9268 -9.51318 0 USR MPRFLG 0 ; 
       28 SCHEM 34.4268 -1.513181 0 USR MPRFLG 0 ; 
       29 SCHEM 34.4268 0.4868187 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 36.9268 -11.51318 0 USR MPRFLG 0 ; 
       31 SCHEM 24.42679 -7.513179 0 USR MPRFLG 0 ; 
       32 SCHEM 80.67677 0.4868187 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       33 SCHEM 16.9268 -11.51318 0 MPRFLG 0 ; 
       34 SCHEM 15.6768 -7.513179 0 MPRFLG 0 ; 
       35 SCHEM 16.9268 -9.51318 0 MPRFLG 0 ; 
       36 SCHEM 36.9268 -9.51318 0 USR MPRFLG 0 ; 
       37 SCHEM 36.9268 -7.513179 0 USR MPRFLG 0 ; 
       38 SCHEM 49.42677 -9.51318 0 USR MPRFLG 0 ; 
       39 SCHEM 46.92677 -9.51318 0 USR MPRFLG 0 ; 
       40 SCHEM 21.92679 -9.51318 0 USR MPRFLG 0 ; 
       41 SCHEM 24.42679 -9.51318 0 USR MPRFLG 0 ; 
       42 SCHEM 39.4268 -9.51318 0 USR MPRFLG 0 ; 
       43 SCHEM 13.1768 -5.513179 0 USR MPRFLG 0 ; 
       44 SCHEM 51.92677 -9.51318 0 USR MPRFLG 0 ; 
       45 SCHEM 35.6768 -5.513179 0 USR MPRFLG 0 ; 
       0 SCHEM 76.11894 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 38.4268 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.17677 -5.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.67675 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60.92677 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 23.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 18.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20.92679 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 23.42679 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35.9268 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 25.92678 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 28.42679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 30.92679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 55.92677 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35.9268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 38.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.0468 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 8.426802 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14.4268 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 12.1768 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 17.29679 -9.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.92677 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 23.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 18.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 13.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59.67675 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 55.92677 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 12.1768 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 38.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 35.9268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 38.4268 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 35.9268 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 25.92678 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 17.29679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 28.42679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 30.92679 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.0468 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 8.426802 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14.4268 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 62.1768 -1.513181 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 54 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
