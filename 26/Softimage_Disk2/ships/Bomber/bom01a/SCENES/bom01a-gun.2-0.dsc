SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       gun-null3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       gun-default1.1-0 ; 
       gun-default3.1-0 ; 
       gun-default4.1-0 ; 
       gun-mat1.1-0 ; 
       gun-mat10.1-0 ; 
       gun-mat12.1-0 ; 
       gun-mat13.1-0 ; 
       gun-mat14.1-0 ; 
       gun-mat15.1-0 ; 
       gun-mat16.1-0 ; 
       gun-mat17.1-0 ; 
       gun-mat18.1-0 ; 
       gun-mat19.1-0 ; 
       gun-mat2.1-0 ; 
       gun-mat20.1-0 ; 
       gun-mat21.1-0 ; 
       gun-mat22.1-0 ; 
       gun-mat23.1-0 ; 
       gun-mat24.1-0 ; 
       gun-mat25.1-0 ; 
       gun-mat3.1-0 ; 
       gun-mat34.1-0 ; 
       gun-mat4.1-0 ; 
       gun-mat44.1-0 ; 
       gun-mat45.1-0 ; 
       gun-mat46.1-0 ; 
       gun-mat47.1-0 ; 
       gun-mat48.1-0 ; 
       gun-mat49.1-0 ; 
       gun-mat50.1-0 ; 
       gun-mat51.1-0 ; 
       gun-mat52.1-0 ; 
       gun-mat53.1-0 ; 
       gun-mat54.1-0 ; 
       gun-mat55.1-0 ; 
       gun-mat56.1-0 ; 
       gun-mat57.1-0 ; 
       gun-mat58.1-0 ; 
       gun-mat59.1-0 ; 
       gun-mat63.1-0 ; 
       gun-mat64.1-0 ; 
       gun-mat65.1-0 ; 
       gun-mat66.1-0 ; 
       gun-mat67.1-0 ; 
       gun-mat69.1-0 ; 
       gun-mat8.1-0 ; 
       gun-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       bom07-cockpt.4-0 ROOT ; 
       gun-abfuselg.1-0 ; 
       gun-afuselg.1-0 ; 
       gun-alengine.1-0 ; 
       gun-alengine1.1-0 ; 
       gun-atfuselg.1-0 ; 
       gun-awepmnt.1-0 ; 
       gun-bom01a_1.8-0 ; 
       gun-ffuselg.11-0 ; 
       gun-finzzz0.1-0 ; 
       gun-finzzz1.1-0 ; 
       gun-fuselg0.1-0 ; 
       gun-fwepmnt.1-0 ; 
       gun-l-gun.1-0 ; 
       gun-lbwepemt.1-0 ; 
       gun-lfinzzz.1-0 ; 
       gun-LL0.1-0 ; 
       gun-LLf.1-0 ; 
       gun-LLl.1-0 ; 
       gun-LLr.1-0 ; 
       gun-lthrust.1-0 ; 
       gun-lthrust1.1-0 ; 
       gun-lturbine.1-0 ; 
       gun-lwepbar.1-0 ; 
       gun-lwepbas.1-0 ; 
       gun-lwepemt1.1-0 ; 
       gun-lwepemt2.1-0 ; 
       gun-null2.1-0 ; 
       gun-null3.2-0 ROOT ; 
       gun-rbwepemt.1-0 ; 
       gun-rfinzzz.1-0 ; 
       gun-rotation_limit.2-0 ROOT ; 
       gun-rthrust.1-0 ; 
       gun-rthrust12.1-0 ; 
       gun-rturbine.1-0 ; 
       gun-rwepbar.1-0 ; 
       gun-rwepbas.1-0 ; 
       gun-rwepemt1.1-0 ; 
       gun-rwepemt2.1-0 ; 
       gun-SSal.1-0 ; 
       gun-SSar.1-0 ; 
       gun-SSf.1-0 ; 
       gun-thrust0.1-0 ; 
       gun-tturatt.1-0 ; 
       gun-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-gun.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       gun-t2d11.1-0 ; 
       gun-t2d12.1-0 ; 
       gun-t2d13.1-0 ; 
       gun-t2d14.1-0 ; 
       gun-t2d15.1-0 ; 
       gun-t2d16.1-0 ; 
       gun-t2d17.1-0 ; 
       gun-t2d18.1-0 ; 
       gun-t2d2.1-0 ; 
       gun-t2d21.1-0 ; 
       gun-t2d3.1-0 ; 
       gun-t2d30.1-0 ; 
       gun-t2d31.1-0 ; 
       gun-t2d53.1-0 ; 
       gun-t2d54.1-0 ; 
       gun-t2d55.1-0 ; 
       gun-t2d56.1-0 ; 
       gun-t2d58.1-0 ; 
       gun-t2d59.1-0 ; 
       gun-t2d61.1-0 ; 
       gun-t2d62.1-0 ; 
       gun-t2d63.1-0 ; 
       gun-t2d64.1-0 ; 
       gun-t2d65.1-0 ; 
       gun-t2d69.1-0 ; 
       gun-t2d7.1-0 ; 
       gun-t2d70.1-0 ; 
       gun-t2d71.1-0 ; 
       gun-t2d72.1-0 ; 
       gun-t2d73.1-0 ; 
       gun-t2d74.1-0 ; 
       gun-t2d75.1-0 ; 
       gun-t2d76.1-0 ; 
       gun-t2d77.1-0 ; 
       gun-t2d78.1-0 ; 
       gun-t2d79.1-0 ; 
       gun-t2d8.1-0 ; 
       gun-t2d80.1-0 ; 
       gun-t2d81.1-0 ; 
       gun-t2d82.1-0 ; 
       gun-t2d83.1-0 ; 
       gun-t2d84.1-0 ; 
       gun-t2d85.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 5 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 11 110 ; 
       6 2 110 ; 
       7 27 110 ; 
       8 11 110 ; 
       9 7 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 22 110 ; 
       14 23 110 ; 
       15 9 110 ; 
       16 7 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 42 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 44 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 28 110 ; 
       29 35 110 ; 
       30 9 110 ; 
       32 34 110 ; 
       33 42 110 ; 
       34 33 110 ; 
       35 36 110 ; 
       36 44 110 ; 
       37 8 110 ; 
       38 8 110 ; 
       39 15 110 ; 
       40 30 110 ; 
       41 8 110 ; 
       42 7 110 ; 
       43 8 110 ; 
       44 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       7 3 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       8 38 300 ; 
       10 12 300 ; 
       13 44 300 ; 
       15 11 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       18 1 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       19 2 300 ; 
       19 36 300 ; 
       19 37 300 ; 
       21 45 300 ; 
       22 13 300 ; 
       22 20 300 ; 
       22 22 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 26 300 ; 
       23 39 300 ; 
       24 30 300 ; 
       30 10 300 ; 
       33 46 300 ; 
       34 41 300 ; 
       34 42 300 ; 
       34 43 300 ; 
       35 27 300 ; 
       35 28 300 ; 
       35 29 300 ; 
       35 40 300 ; 
       36 0 300 ; 
       39 17 300 ; 
       40 18 300 ; 
       41 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       28 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 25 401 ; 
       46 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 79.99998 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       1 SCHEM 60.80779 -9.513181 0 USR MPRFLG 0 ; 
       2 SCHEM 63.3078 -9.513181 0 USR MPRFLG 0 ; 
       3 SCHEM 55.80779 -11.51318 0 MPRFLG 0 ; 
       4 SCHEM 53.30779 -11.51318 0 MPRFLG 0 ; 
       5 SCHEM 62.05779 -7.51318 0 USR MPRFLG 0 ; 
       6 SCHEM 63.3078 -11.51318 0 USR MPRFLG 0 ; 
       7 SCHEM 38.30784 -3.513181 0 USR MPRFLG 0 ; 
       8 SCHEM 50.80781 -7.51318 0 USR MPRFLG 0 ; 
       9 SCHEM 25.80783 -5.51318 0 USR MPRFLG 0 ; 
       10 SCHEM 23.30783 -7.51318 0 USR MPRFLG 0 ; 
       11 SCHEM 53.3078 -5.51318 0 USR MPRFLG 0 ; 
       12 SCHEM 58.3078 -9.513181 0 USR MPRFLG 0 ; 
       13 SCHEM 9.557838 -11.51318 0 MPRFLG 0 ; 
       14 SCHEM 38.30784 -11.51318 0 USR MPRFLG 0 ; 
       15 SCHEM 25.80783 -7.51318 0 USR MPRFLG 0 ; 
       16 SCHEM 33.30783 -5.51318 0 USR MPRFLG 0 ; 
       17 SCHEM 30.80782 -7.51318 0 USR MPRFLG 0 ; 
       18 SCHEM 33.30783 -7.51318 0 USR MPRFLG 0 ; 
       19 SCHEM 35.80783 -7.51318 0 USR MPRFLG 0 ; 
       20 SCHEM 15.80784 -11.51318 0 USR MPRFLG 0 ; 
       21 SCHEM 14.55784 -7.51318 0 USR MPRFLG 0 ; 
       22 SCHEM 14.55784 -9.513181 0 USR MPRFLG 0 ; 
       23 SCHEM 38.30784 -9.513181 0 USR MPRFLG 0 ; 
       24 SCHEM 38.30784 -7.51318 0 USR MPRFLG 0 ; 
       25 SCHEM 48.30781 -9.513181 0 USR MPRFLG 0 ; 
       26 SCHEM 45.80783 -9.513181 0 USR MPRFLG 0 ; 
       27 SCHEM 38.30784 -1.513181 0 USR MPRFLG 0 ; 
       28 SCHEM 38.30784 0.4868187 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 40.80783 -11.51318 0 USR MPRFLG 0 ; 
       30 SCHEM 28.30783 -7.51318 0 USR MPRFLG 0 ; 
       31 SCHEM 84.5578 0.4868187 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       32 SCHEM 18.42784 -11.63318 0 USR MPRFLG 0 ; 
       33 SCHEM 19.55784 -7.51318 0 USR MPRFLG 0 ; 
       34 SCHEM 19.67783 -9.633181 0 USR MPRFLG 0 ; 
       35 SCHEM 40.80783 -9.513181 0 USR MPRFLG 0 ; 
       36 SCHEM 40.80783 -7.51318 0 USR MPRFLG 0 ; 
       37 SCHEM 53.3078 -9.513181 0 USR MPRFLG 0 ; 
       38 SCHEM 50.80781 -9.513181 0 USR MPRFLG 0 ; 
       39 SCHEM 25.80783 -9.513181 0 USR MPRFLG 0 ; 
       40 SCHEM 28.30783 -9.513181 0 USR MPRFLG 0 ; 
       41 SCHEM 43.30783 -9.513181 0 USR MPRFLG 0 ; 
       42 SCHEM 17.05784 -5.51318 0 USR MPRFLG 0 ; 
       43 SCHEM 55.8078 -9.513181 0 USR MPRFLG 0 ; 
       44 SCHEM 39.55784 -5.51318 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 48.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35.80783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 38.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 82.0578 -5.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 68.30779 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 58.30779 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60.80779 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 63.30779 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65.80779 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65.8078 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30.80783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 28.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 23.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 23.30784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 72.0578 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.0578 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 69.5578 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 25.80783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 28.30783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 43.30783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 18.30784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65.8078 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20.80784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60.8078 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 48.30784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 40.80784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 43.30784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 50.80783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 43.30783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 45.80783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45.80784 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 33.30782 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 28.30782 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 30.80782 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 30.80783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 33.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 33.30783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 35.80783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 63.3078 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 45.80784 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 48.30783 -11.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 25.92784 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 20.92784 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 23.42784 -11.63318 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9.557838 -13.51318 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 22.05784 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 25.92783 -9.513181 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 58.30779 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60.80779 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 63.30779 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 65.80779 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65.8078 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30.80783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 28.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 23.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 18.30784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 69.5578 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20.80784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 65.8078 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 23.30784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 28.30782 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30.80782 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 30.80783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 33.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 33.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 35.80783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 60.8078 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 72.0578 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 67.0578 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 68.30779 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 63.3078 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 45.80784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 22.05784 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 48.30783 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 45.80783 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 43.30783 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 50.80783 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 43.30784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 40.80784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 48.30784 -13.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 48.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 45.80784 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 33.30782 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 25.92783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 35.80783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 38.30783 -11.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 25.92784 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 20.92784 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 23.42784 -13.63318 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 9.557838 -15.51318 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 113.5578 -1.513181 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
