SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       null_edit-null3_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.13-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       null_edit-default1.1-0 ; 
       null_edit-default3.1-0 ; 
       null_edit-default4.1-0 ; 
       null_edit-mat1.1-0 ; 
       null_edit-mat10.1-0 ; 
       null_edit-mat12.1-0 ; 
       null_edit-mat13.1-0 ; 
       null_edit-mat14.1-0 ; 
       null_edit-mat15.1-0 ; 
       null_edit-mat16.1-0 ; 
       null_edit-mat17.1-0 ; 
       null_edit-mat18.1-0 ; 
       null_edit-mat19.1-0 ; 
       null_edit-mat2.1-0 ; 
       null_edit-mat20.1-0 ; 
       null_edit-mat21.1-0 ; 
       null_edit-mat22.1-0 ; 
       null_edit-mat23.1-0 ; 
       null_edit-mat24.1-0 ; 
       null_edit-mat25.1-0 ; 
       null_edit-mat3.1-0 ; 
       null_edit-mat34.1-0 ; 
       null_edit-mat4.1-0 ; 
       null_edit-mat44.1-0 ; 
       null_edit-mat45.1-0 ; 
       null_edit-mat46.1-0 ; 
       null_edit-mat47.1-0 ; 
       null_edit-mat48.1-0 ; 
       null_edit-mat49.1-0 ; 
       null_edit-mat50.1-0 ; 
       null_edit-mat51.1-0 ; 
       null_edit-mat52.1-0 ; 
       null_edit-mat53.1-0 ; 
       null_edit-mat54.1-0 ; 
       null_edit-mat55.1-0 ; 
       null_edit-mat56.1-0 ; 
       null_edit-mat57.1-0 ; 
       null_edit-mat58.1-0 ; 
       null_edit-mat59.1-0 ; 
       null_edit-mat63.1-0 ; 
       null_edit-mat64.1-0 ; 
       null_edit-mat65.1-0 ; 
       null_edit-mat66.1-0 ; 
       null_edit-mat67.1-0 ; 
       null_edit-mat69.1-0 ; 
       null_edit-mat70.1-0 ; 
       null_edit-mat8.1-0 ; 
       null_edit-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       null_edit-abfuselg.1-0 ; 
       null_edit-afuselg.1-0 ; 
       null_edit-alengine.1-0 ; 
       null_edit-alengine1.1-0 ; 
       null_edit-atfuselg.1-0 ; 
       null_edit-awepmnt.1-0 ; 
       null_edit-bom01a_1.8-0 ; 
       null_edit-cockpt.3-0 ; 
       null_edit-ffuselg.11-0 ; 
       null_edit-finzzz0.1-0 ; 
       null_edit-finzzz1.1-0 ; 
       null_edit-fuselg0.1-0 ; 
       null_edit-fwepmnt.1-0 ; 
       null_edit-lbwepemt.1-0 ; 
       null_edit-lfinzzz.1-0 ; 
       null_edit-lgun.1-0 ; 
       null_edit-LL0.1-0 ; 
       null_edit-LLf.1-0 ; 
       null_edit-LLl.1-0 ; 
       null_edit-LLr.1-0 ; 
       null_edit-lthrust.1-0 ; 
       null_edit-lthrust1.1-0 ; 
       null_edit-lturbine.1-0 ; 
       null_edit-lwepbar.1-0 ; 
       null_edit-lwepbas.1-0 ; 
       null_edit-lwepemt1.1-0 ; 
       null_edit-lwepemt2.1-0 ; 
       null_edit-lwepemt3.1-0 ; 
       null_edit-null2.1-0 ; 
       null_edit-null3_1.4-0 ROOT ; 
       null_edit-rbwepemt.1-0 ; 
       null_edit-rfinzzz.1-0 ; 
       null_edit-rgun.1-0 ; 
       null_edit-rthrust.1-0 ; 
       null_edit-rthrust12.1-0 ; 
       null_edit-rturbine.1-0 ; 
       null_edit-rwepbar.1-0 ; 
       null_edit-rwepbas.1-0 ; 
       null_edit-rwepemt1.1-0 ; 
       null_edit-rwepemt2.1-0 ; 
       null_edit-rwepemt3.1-0 ; 
       null_edit-SSal.1-0 ; 
       null_edit-SSar.1-0 ; 
       null_edit-SSf.1-0 ; 
       null_edit-thrust0.1-0 ; 
       null_edit-trail.1-0 ; 
       null_edit-tturatt.1-0 ; 
       null_edit-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-null_edit.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       null_edit-t2d11.1-0 ; 
       null_edit-t2d12.1-0 ; 
       null_edit-t2d13.1-0 ; 
       null_edit-t2d14.1-0 ; 
       null_edit-t2d15.1-0 ; 
       null_edit-t2d16.1-0 ; 
       null_edit-t2d17.1-0 ; 
       null_edit-t2d18.1-0 ; 
       null_edit-t2d2.1-0 ; 
       null_edit-t2d21.1-0 ; 
       null_edit-t2d3.1-0 ; 
       null_edit-t2d30.1-0 ; 
       null_edit-t2d31.1-0 ; 
       null_edit-t2d53.1-0 ; 
       null_edit-t2d54.1-0 ; 
       null_edit-t2d55.1-0 ; 
       null_edit-t2d56.1-0 ; 
       null_edit-t2d58.1-0 ; 
       null_edit-t2d59.1-0 ; 
       null_edit-t2d61.1-0 ; 
       null_edit-t2d62.1-0 ; 
       null_edit-t2d63.1-0 ; 
       null_edit-t2d64.1-0 ; 
       null_edit-t2d65.1-0 ; 
       null_edit-t2d69.1-0 ; 
       null_edit-t2d7.1-0 ; 
       null_edit-t2d70.1-0 ; 
       null_edit-t2d71.1-0 ; 
       null_edit-t2d72.1-0 ; 
       null_edit-t2d73.1-0 ; 
       null_edit-t2d74.1-0 ; 
       null_edit-t2d75.1-0 ; 
       null_edit-t2d76.1-0 ; 
       null_edit-t2d77.1-0 ; 
       null_edit-t2d78.1-0 ; 
       null_edit-t2d79.1-0 ; 
       null_edit-t2d8.1-0 ; 
       null_edit-t2d80.1-0 ; 
       null_edit-t2d81.1-0 ; 
       null_edit-t2d82.1-0 ; 
       null_edit-t2d83.1-0 ; 
       null_edit-t2d84.1-0 ; 
       null_edit-t2d85.1-0 ; 
       null_edit-t2d86.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 28 110 ; 
       7 29 110 ; 
       8 11 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 6 110 ; 
       12 8 110 ; 
       13 23 110 ; 
       14 9 110 ; 
       15 21 110 ; 
       16 6 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 22 110 ; 
       21 44 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 47 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 15 110 ; 
       28 29 110 ; 
       30 36 110 ; 
       31 9 110 ; 
       32 34 110 ; 
       33 35 110 ; 
       34 44 110 ; 
       35 34 110 ; 
       36 37 110 ; 
       37 47 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 32 110 ; 
       41 14 110 ; 
       42 31 110 ; 
       43 8 110 ; 
       44 6 110 ; 
       45 29 110 ; 
       46 8 110 ; 
       47 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 9 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       6 3 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       8 38 300 ; 
       10 12 300 ; 
       14 11 300 ; 
       15 44 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       18 1 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       19 2 300 ; 
       19 36 300 ; 
       19 37 300 ; 
       21 46 300 ; 
       22 13 300 ; 
       22 20 300 ; 
       22 22 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 26 300 ; 
       23 39 300 ; 
       24 30 300 ; 
       31 10 300 ; 
       32 45 300 ; 
       34 47 300 ; 
       35 41 300 ; 
       35 42 300 ; 
       35 43 300 ; 
       36 27 300 ; 
       36 28 300 ; 
       36 29 300 ; 
       36 40 300 ; 
       37 0 300 ; 
       41 17 300 ; 
       42 18 300 ; 
       43 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 25 401 ; 
       47 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 136.25 -12 0 MPRFLG 0 ; 
       1 SCHEM 140 -12 0 MPRFLG 0 ; 
       2 SCHEM 137.5 -14 0 MPRFLG 0 ; 
       3 SCHEM 135 -14 0 MPRFLG 0 ; 
       4 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 140 -14 0 MPRFLG 0 ; 
       6 SCHEM 108.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 110.2049 -24.81841 0 USR WIRECOL 2 7 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 120 -10 0 MPRFLG 0 ; 
       9 SCHEM 90 -8 0 MPRFLG 0 ; 
       10 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 122.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 116.746 -24.27744 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 90 -10 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 95 -10 0 MPRFLG 0 ; 
       18 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 100 -10 0 MPRFLG 0 ; 
       20 SCHEM 80 -14 0 MPRFLG 0 ; 
       21 SCHEM 78.75 -10 0 MPRFLG 0 ; 
       22 SCHEM 80 -12 0 MPRFLG 0 ; 
       23 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       24 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 113.3366 -22.5559 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 114.416 -23.09186 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 115.4746 -23.66516 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 108.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 108.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 121.6204 -24.28857 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 92.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       33 SCHEM 85 -14 0 MPRFLG 0 ; 
       34 SCHEM 83.75 -10 0 MPRFLG 0 ; 
       35 SCHEM 85 -12 0 MPRFLG 0 ; 
       36 SCHEM 105 -12 0 MPRFLG 0 ; 
       37 SCHEM 105 -10 0 MPRFLG 0 ; 
       38 SCHEM 118.2689 -22.54647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 119.2941 -23.07716 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 120.3476 -23.63049 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 90 -12 0 MPRFLG 0 ; 
       42 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       45 SCHEM 111.6978 -25.54067 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 120 -12 0 MPRFLG 0 ; 
       47 SCHEM 103.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 106.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 141.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 141.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 89 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 104 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 79 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 84 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 141.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 91.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 81.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 106.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 104 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 86.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 79 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 84 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 145 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
