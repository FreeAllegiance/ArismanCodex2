SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.26-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       check_nulls-default1.2-0 ; 
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat25.1-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat45.2-0 ; 
       check_nulls-mat46.2-0 ; 
       check_nulls-mat47.2-0 ; 
       check_nulls-mat48.2-0 ; 
       check_nulls-mat49.2-0 ; 
       check_nulls-mat50.2-0 ; 
       check_nulls-mat51.2-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat63.2-0 ; 
       check_nulls-mat64.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat69.2-0 ; 
       check_nulls-mat70.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat9.2-0 ; 
       fix_blinker-mat23.2-0 ; 
       fix_blinker-mat24.2-0 ; 
       fix_blinker-mat71.3-0 ; 
       fix_blinker-mat72.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1.4-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d69.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d70.2-0 ; 
       check_nulls-t2d71.2-0 ; 
       check_nulls-t2d72.2-0 ; 
       check_nulls-t2d73.2-0 ; 
       check_nulls-t2d74.2-0 ; 
       check_nulls-t2d75.2-0 ; 
       check_nulls-t2d76.2-0 ; 
       check_nulls-t2d77.2-0 ; 
       check_nulls-t2d78.2-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       check_nulls-t2d85.2-0 ; 
       check_nulls-t2d86.2-0 ; 
       fix_blinker-t2d87.3-0 ; 
       fix_blinker-t2d88.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 9 110 ; 
       2 5 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 12 110 ; 
       6 2 110 ; 
       8 7 110 ; 
       9 12 110 ; 
       10 7 110 ; 
       11 10 110 ; 
       12 7 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 17 110 ; 
       16 18 110 ; 
       17 38 110 ; 
       18 17 110 ; 
       19 20 110 ; 
       20 41 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 15 110 ; 
       24 19 110 ; 
       25 10 110 ; 
       26 28 110 ; 
       27 29 110 ; 
       28 38 110 ; 
       29 28 110 ; 
       30 31 110 ; 
       31 41 110 ; 
       32 9 110 ; 
       33 9 110 ; 
       34 26 110 ; 
       35 14 110 ; 
       36 25 110 ; 
       37 9 110 ; 
       38 7 110 ; 
       39 7 110 ; 
       40 9 110 ; 
       41 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       1 34 300 ; 
       2 7 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       7 1 300 ; 
       9 15 300 ; 
       9 17 300 ; 
       9 25 300 ; 
       11 8 300 ; 
       14 39 300 ; 
       15 31 300 ; 
       17 33 300 ; 
       18 9 300 ; 
       18 14 300 ; 
       18 16 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       19 26 300 ; 
       20 24 300 ; 
       25 38 300 ; 
       26 32 300 ; 
       28 35 300 ; 
       29 28 300 ; 
       29 29 300 ; 
       29 30 300 ; 
       30 21 300 ; 
       30 22 300 ; 
       30 23 300 ; 
       30 27 300 ; 
       31 0 300 ; 
       35 36 300 ; 
       36 37 300 ; 
       37 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 25 401 ; 
       2 14 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 10 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 7 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 8 401 ; 
       17 11 401 ; 
       18 24 401 ; 
       19 23 401 ; 
       20 22 401 ; 
       21 21 401 ; 
       22 20 401 ; 
       23 19 401 ; 
       24 26 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 18 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 17 401 ; 
       35 27 401 ; 
       38 33 401 ; 
       39 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 MPRFLG 0 ; 
       3 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 55 -8 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 -8 0 MPRFLG 0 ; 
       7 SCHEM 35 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 50 -6 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 30 -6 0 MPRFLG 0 ; 
       20 SCHEM 30 -4 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 20 -4 0 MPRFLG 0 ; 
       26 SCHEM 10 -6 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       29 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 20 -6 0 MPRFLG 0 ; 
       37 SCHEM 35 -6 0 MPRFLG 0 ; 
       38 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       39 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 31.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
