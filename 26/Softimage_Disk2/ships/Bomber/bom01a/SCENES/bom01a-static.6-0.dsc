SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.30-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat9.2-0 ; 
       fix_blinker-mat71.3-0 ; 
       fix_blinker-mat72.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-bom01a_1.7-0 ROOT ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       fix_blinker-t2d87.3-0 ; 
       fix_blinker-t2d88.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 7 110 ; 
       4 7 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 5 110 ; 
       9 14 110 ; 
       10 9 110 ; 
       11 5 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       1 6 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       3 0 300 ; 
       4 13 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       6 7 300 ; 
       8 23 300 ; 
       9 20 300 ; 
       10 8 300 ; 
       10 12 300 ; 
       10 14 300 ; 
       11 22 300 ; 
       12 21 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 14 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 10 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 7 401 ; 
       12 6 401 ; 
       13 9 401 ; 
       14 8 401 ; 
       15 11 401 ; 
       16 15 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 20 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 21 401 ; 
       23 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
