SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       il_bomber_F-null3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       il_bomber_F-light1_1.1-0 ROOT ; 
       il_bomber_F-light2_1.1-0 ROOT ; 
       il_bomber_F-light3_1.1-0 ROOT ; 
       il_bomber_F-light4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       il_bomber_F-default1.1-0 ; 
       il_bomber_F-default3.1-0 ; 
       il_bomber_F-default4.1-0 ; 
       il_bomber_F-mat1.1-0 ; 
       il_bomber_F-mat10.1-0 ; 
       il_bomber_F-mat12.1-0 ; 
       il_bomber_F-mat13.1-0 ; 
       il_bomber_F-mat14.1-0 ; 
       il_bomber_F-mat15.1-0 ; 
       il_bomber_F-mat16.1-0 ; 
       il_bomber_F-mat17.1-0 ; 
       il_bomber_F-mat18.1-0 ; 
       il_bomber_F-mat19.1-0 ; 
       il_bomber_F-mat2.1-0 ; 
       il_bomber_F-mat20.1-0 ; 
       il_bomber_F-mat21.1-0 ; 
       il_bomber_F-mat22.1-0 ; 
       il_bomber_F-mat23.1-0 ; 
       il_bomber_F-mat24.1-0 ; 
       il_bomber_F-mat25.1-0 ; 
       il_bomber_F-mat3.1-0 ; 
       il_bomber_F-mat34.1-0 ; 
       il_bomber_F-mat4.1-0 ; 
       il_bomber_F-mat44.1-0 ; 
       il_bomber_F-mat45.1-0 ; 
       il_bomber_F-mat46.1-0 ; 
       il_bomber_F-mat47.1-0 ; 
       il_bomber_F-mat48.1-0 ; 
       il_bomber_F-mat49.1-0 ; 
       il_bomber_F-mat50.1-0 ; 
       il_bomber_F-mat51.1-0 ; 
       il_bomber_F-mat52.1-0 ; 
       il_bomber_F-mat53.1-0 ; 
       il_bomber_F-mat54.1-0 ; 
       il_bomber_F-mat55.1-0 ; 
       il_bomber_F-mat56.1-0 ; 
       il_bomber_F-mat57.1-0 ; 
       il_bomber_F-mat58.1-0 ; 
       il_bomber_F-mat59.1-0 ; 
       il_bomber_F-mat63.1-0 ; 
       il_bomber_F-mat64.1-0 ; 
       il_bomber_F-mat65.1-0 ; 
       il_bomber_F-mat66.1-0 ; 
       il_bomber_F-mat67.1-0 ; 
       il_bomber_F-mat8.1-0 ; 
       il_bomber_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       bom07-cockpt.1-0 ROOT ; 
       il_bomber_F-abfuselg.1-0 ; 
       il_bomber_F-afuselg.1-0 ; 
       il_bomber_F-alengine.1-0 ; 
       il_bomber_F-alengine1.1-0 ; 
       il_bomber_F-atfuselg.1-0 ; 
       il_bomber_F-awepmnt.1-0 ; 
       il_bomber_F-bom01a_1.8-0 ; 
       il_bomber_F-ffuselg.11-0 ; 
       il_bomber_F-finzzz0.1-0 ; 
       il_bomber_F-finzzz1.1-0 ; 
       il_bomber_F-fuselg0.1-0 ; 
       il_bomber_F-fwepmnt.1-0 ; 
       il_bomber_F-lbwepemt.1-0 ; 
       il_bomber_F-lfinzzz.1-0 ; 
       il_bomber_F-LL0.1-0 ; 
       il_bomber_F-LLf.1-0 ; 
       il_bomber_F-LLl.1-0 ; 
       il_bomber_F-LLr.1-0 ; 
       il_bomber_F-lthrust.1-0 ; 
       il_bomber_F-lthrust1.1-0 ; 
       il_bomber_F-lturbine.1-0 ; 
       il_bomber_F-lwepatt.1-0 ; 
       il_bomber_F-lwepbar.1-0 ; 
       il_bomber_F-lwepbas.1-0 ; 
       il_bomber_F-lwepemt1.1-0 ; 
       il_bomber_F-lwepemt2.1-0 ; 
       il_bomber_F-null2.1-0 ; 
       il_bomber_F-null3.1-0 ROOT ; 
       il_bomber_F-rbwepemt.1-0 ; 
       il_bomber_F-rfinzzz.1-0 ; 
       il_bomber_F-rthrust.1-0 ; 
       il_bomber_F-rthrust12.1-0 ; 
       il_bomber_F-rturbine.1-0 ; 
       il_bomber_F-rwepatt.1-0 ; 
       il_bomber_F-rwepbar.1-0 ; 
       il_bomber_F-rwepbas.1-0 ; 
       il_bomber_F-rwepemt1.1-0 ; 
       il_bomber_F-rwepemt2.1-0 ; 
       il_bomber_F-SSal.1-0 ; 
       il_bomber_F-SSar.1-0 ; 
       il_bomber_F-SSf.1-0 ; 
       il_bomber_F-thrust0.1-0 ; 
       il_bomber_F-tturatt.1-0 ; 
       il_bomber_F-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Art fixes/soft/bugfixes/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a_final-il_bomber_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 42     
       il_bomber_F-t2d11.1-0 ; 
       il_bomber_F-t2d12.1-0 ; 
       il_bomber_F-t2d13.1-0 ; 
       il_bomber_F-t2d14.1-0 ; 
       il_bomber_F-t2d15.1-0 ; 
       il_bomber_F-t2d16.1-0 ; 
       il_bomber_F-t2d17.1-0 ; 
       il_bomber_F-t2d18.1-0 ; 
       il_bomber_F-t2d2.1-0 ; 
       il_bomber_F-t2d21.1-0 ; 
       il_bomber_F-t2d3.1-0 ; 
       il_bomber_F-t2d30.1-0 ; 
       il_bomber_F-t2d31.1-0 ; 
       il_bomber_F-t2d53.1-0 ; 
       il_bomber_F-t2d54.1-0 ; 
       il_bomber_F-t2d55.1-0 ; 
       il_bomber_F-t2d56.1-0 ; 
       il_bomber_F-t2d58.1-0 ; 
       il_bomber_F-t2d59.1-0 ; 
       il_bomber_F-t2d61.1-0 ; 
       il_bomber_F-t2d62.1-0 ; 
       il_bomber_F-t2d63.1-0 ; 
       il_bomber_F-t2d64.1-0 ; 
       il_bomber_F-t2d65.1-0 ; 
       il_bomber_F-t2d69.1-0 ; 
       il_bomber_F-t2d7.1-0 ; 
       il_bomber_F-t2d70.1-0 ; 
       il_bomber_F-t2d71.1-0 ; 
       il_bomber_F-t2d72.1-0 ; 
       il_bomber_F-t2d73.1-0 ; 
       il_bomber_F-t2d74.1-0 ; 
       il_bomber_F-t2d75.1-0 ; 
       il_bomber_F-t2d76.1-0 ; 
       il_bomber_F-t2d77.1-0 ; 
       il_bomber_F-t2d78.1-0 ; 
       il_bomber_F-t2d79.1-0 ; 
       il_bomber_F-t2d8.1-0 ; 
       il_bomber_F-t2d80.1-0 ; 
       il_bomber_F-t2d81.1-0 ; 
       il_bomber_F-t2d82.1-0 ; 
       il_bomber_F-t2d83.1-0 ; 
       il_bomber_F-t2d84.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 5 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 11 110 ; 
       6 2 110 ; 
       7 27 110 ; 
       8 11 110 ; 
       9 7 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 23 110 ; 
       14 9 110 ; 
       15 7 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 21 110 ; 
       20 42 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 44 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 28 110 ; 
       29 35 110 ; 
       30 9 110 ; 
       31 33 110 ; 
       32 42 110 ; 
       33 32 110 ; 
       34 33 110 ; 
       35 36 110 ; 
       36 44 110 ; 
       37 8 110 ; 
       38 8 110 ; 
       39 14 110 ; 
       40 30 110 ; 
       41 8 110 ; 
       42 7 110 ; 
       43 8 110 ; 
       44 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       7 3 300 ; 
       8 21 300 ; 
       8 23 300 ; 
       8 38 300 ; 
       10 12 300 ; 
       14 11 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       16 33 300 ; 
       17 1 300 ; 
       17 34 300 ; 
       17 35 300 ; 
       18 2 300 ; 
       18 36 300 ; 
       18 37 300 ; 
       20 44 300 ; 
       21 13 300 ; 
       21 20 300 ; 
       21 22 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 26 300 ; 
       23 39 300 ; 
       24 30 300 ; 
       30 10 300 ; 
       32 45 300 ; 
       33 41 300 ; 
       33 42 300 ; 
       33 43 300 ; 
       35 27 300 ; 
       35 28 300 ; 
       35 29 300 ; 
       35 40 300 ; 
       36 0 300 ; 
       39 17 300 ; 
       40 18 300 ; 
       41 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       28 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 25 401 ; 
       45 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       1 SCHEM 56.36233 -10 0 USR MPRFLG 0 ; 
       2 SCHEM 58.86234 -10 0 USR MPRFLG 0 ; 
       3 SCHEM 57.61233 -12 0 MPRFLG 0 ; 
       4 SCHEM 55.11233 -12 0 MPRFLG 0 ; 
       5 SCHEM 57.61233 -8 0 USR MPRFLG 0 ; 
       6 SCHEM 58.86234 -12 0 USR MPRFLG 0 ; 
       7 SCHEM 33.86238 -4 0 USR MPRFLG 0 ; 
       8 SCHEM 46.36235 -8 0 USR MPRFLG 0 ; 
       9 SCHEM 21.36237 -6 0 USR MPRFLG 0 ; 
       10 SCHEM 18.86238 -8 0 USR MPRFLG 0 ; 
       11 SCHEM 48.86234 -6 0 USR MPRFLG 0 ; 
       12 SCHEM 53.86234 -10 0 USR MPRFLG 0 ; 
       13 SCHEM 33.86238 -12 0 USR MPRFLG 0 ; 
       14 SCHEM 21.36237 -8 0 USR MPRFLG 0 ; 
       15 SCHEM 28.86238 -6 0 USR MPRFLG 0 ; 
       16 SCHEM 26.36236 -8 0 USR MPRFLG 0 ; 
       17 SCHEM 28.86238 -8 0 USR MPRFLG 0 ; 
       18 SCHEM 31.36238 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 11.36238 -12 0 USR MPRFLG 0 ; 
       20 SCHEM 10.11238 -8 0 USR MPRFLG 0 ; 
       21 SCHEM 10.11238 -10 0 USR MPRFLG 0 ; 
       22 SCHEM 8.862381 -12 0 USR MPRFLG 0 ; 
       23 SCHEM 33.86238 -10 0 USR MPRFLG 0 ; 
       24 SCHEM 33.86238 -8 0 USR MPRFLG 0 ; 
       25 SCHEM 43.86235 -10 0 USR MPRFLG 0 ; 
       26 SCHEM 41.36237 -10 0 USR MPRFLG 0 ; 
       27 SCHEM 33.86238 -2 0 USR MPRFLG 0 ; 
       28 SCHEM 33.86238 0 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 36.36237 -12 0 USR MPRFLG 0 ; 
       30 SCHEM 23.86237 -8 0 USR MPRFLG 0 ; 
       31 SCHEM 13.98238 -12.12 0 USR MPRFLG 0 ; 
       32 SCHEM 15.11238 -8 0 USR MPRFLG 0 ; 
       33 SCHEM 15.23237 -10.12 0 USR MPRFLG 0 ; 
       34 SCHEM 16.48237 -12.12 0 USR MPRFLG 0 ; 
       35 SCHEM 36.36237 -10 0 USR MPRFLG 0 ; 
       36 SCHEM 36.36237 -8 0 USR MPRFLG 0 ; 
       37 SCHEM 48.86234 -10 0 USR MPRFLG 0 ; 
       38 SCHEM 46.36235 -10 0 USR MPRFLG 0 ; 
       39 SCHEM 21.36237 -10 0 USR MPRFLG 0 ; 
       40 SCHEM 23.86237 -10 0 USR MPRFLG 0 ; 
       41 SCHEM 38.86237 -10 0 USR MPRFLG 0 ; 
       42 SCHEM 12.61238 -6 0 USR MPRFLG 0 ; 
       43 SCHEM 51.36234 -10 0 USR MPRFLG 0 ; 
       44 SCHEM 35.11238 -6 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.86237 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.86238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30.36238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.61234 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59.11233 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59.11233 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.11233 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.11233 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.11233 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60.36234 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25.36237 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.86237 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.86238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.86238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20.36237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 37.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.86238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.86238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35.36238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 35.36238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35.36238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 37.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35.36238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25.36236 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25.36236 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 25.36236 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 27.86238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 27.86238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30.36238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 30.36238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 55.36234 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35.36238 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 37.86237 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 17.98237 -12.12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 17.98237 -12.12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 17.98237 -12.12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 12.86238 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 17.98237 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59.11233 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59.11233 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59.11233 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59.11233 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.36234 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25.36237 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.86237 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.86238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.86238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.86238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.86238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25.36236 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25.36236 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.86238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 27.86238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 30.36238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 30.36238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 55.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59.11233 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 55.36234 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 35.36238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 12.86238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 37.86237 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 37.86237 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 37.86237 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 37.86237 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35.36238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 35.36238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 35.36238 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 37.86237 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 35.36238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 25.36236 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 17.98237 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 27.86238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 30.36238 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 17.98237 -14.12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 17.98237 -14.12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 17.98237 -14.12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.61238 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
