SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-null3_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.19-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       check_nulls-default1.1-0 ; 
       check_nulls-default3.1-0 ; 
       check_nulls-default4.1-0 ; 
       check_nulls-mat1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat13.1-0 ; 
       check_nulls-mat14.1-0 ; 
       check_nulls-mat15.1-0 ; 
       check_nulls-mat16.1-0 ; 
       check_nulls-mat19.1-0 ; 
       check_nulls-mat2.1-0 ; 
       check_nulls-mat20.1-0 ; 
       check_nulls-mat21.1-0 ; 
       check_nulls-mat22.1-0 ; 
       check_nulls-mat25.1-0 ; 
       check_nulls-mat3.1-0 ; 
       check_nulls-mat34.1-0 ; 
       check_nulls-mat4.1-0 ; 
       check_nulls-mat44.1-0 ; 
       check_nulls-mat45.1-0 ; 
       check_nulls-mat46.1-0 ; 
       check_nulls-mat47.1-0 ; 
       check_nulls-mat48.1-0 ; 
       check_nulls-mat49.1-0 ; 
       check_nulls-mat50.1-0 ; 
       check_nulls-mat51.1-0 ; 
       check_nulls-mat52.1-0 ; 
       check_nulls-mat53.1-0 ; 
       check_nulls-mat54.1-0 ; 
       check_nulls-mat55.1-0 ; 
       check_nulls-mat56.1-0 ; 
       check_nulls-mat57.1-0 ; 
       check_nulls-mat58.1-0 ; 
       check_nulls-mat59.1-0 ; 
       check_nulls-mat63.1-0 ; 
       check_nulls-mat64.1-0 ; 
       check_nulls-mat65.1-0 ; 
       check_nulls-mat66.1-0 ; 
       check_nulls-mat67.1-0 ; 
       check_nulls-mat69.1-0 ; 
       check_nulls-mat70.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat82.1-0 ; 
       check_nulls-mat84.1-0 ; 
       check_nulls-mat85.1-0 ; 
       check_nulls-mat86.1-0 ; 
       check_nulls-mat87.1-0 ; 
       check_nulls-mat88.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-mat90.1-0 ; 
       check_nulls-mat91.1-0 ; 
       check_nulls-mat92.1-0 ; 
       check_nulls-mat93.1-0 ; 
       check_nulls-mat94.1-0 ; 
       fix_blinker-mat23.1-0 ; 
       fix_blinker-mat24.1-0 ; 
       fix_blinker-mat71.1-0 ; 
       fix_blinker-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-antenn2.1-0 ; 
       check_nulls-antenn3.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1.8-0 ; 
       check_nulls-cockpt_1.3-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-lbwepemt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLl.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepbas_3.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-null2.1-0 ; 
       check_nulls-null3_1.3-0 ROOT ; 
       check_nulls-rbwepemt.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepbas_3.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-twepemt.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-add_decals.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d2.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d3.1-0 ; 
       check_nulls-t2d30.1-0 ; 
       check_nulls-t2d31.1-0 ; 
       check_nulls-t2d53.1-0 ; 
       check_nulls-t2d54.1-0 ; 
       check_nulls-t2d55.1-0 ; 
       check_nulls-t2d56.1-0 ; 
       check_nulls-t2d58.1-0 ; 
       check_nulls-t2d59.1-0 ; 
       check_nulls-t2d61.1-0 ; 
       check_nulls-t2d62.1-0 ; 
       check_nulls-t2d63.1-0 ; 
       check_nulls-t2d64.1-0 ; 
       check_nulls-t2d65.1-0 ; 
       check_nulls-t2d69.1-0 ; 
       check_nulls-t2d7.1-0 ; 
       check_nulls-t2d70.1-0 ; 
       check_nulls-t2d71.1-0 ; 
       check_nulls-t2d72.1-0 ; 
       check_nulls-t2d73.1-0 ; 
       check_nulls-t2d74.1-0 ; 
       check_nulls-t2d75.1-0 ; 
       check_nulls-t2d76.1-0 ; 
       check_nulls-t2d77.1-0 ; 
       check_nulls-t2d78.1-0 ; 
       check_nulls-t2d79.1-0 ; 
       check_nulls-t2d8.1-0 ; 
       check_nulls-t2d80.1-0 ; 
       check_nulls-t2d81.1-0 ; 
       check_nulls-t2d82.1-0 ; 
       check_nulls-t2d83.1-0 ; 
       check_nulls-t2d84.1-0 ; 
       check_nulls-t2d85.1-0 ; 
       check_nulls-t2d86.1-0 ; 
       check_nulls-t2d87.1-0 ; 
       check_nulls-t2d88.1-0 ; 
       check_nulls-t2d89.1-0 ; 
       check_nulls-t2d90.1-0 ; 
       check_nulls-t2d91.1-0 ; 
       check_nulls-t2d92.1-0 ; 
       check_nulls-t2d93.1-0 ; 
       check_nulls-t2d94.1-0 ; 
       fix_blinker-t2d87.1-0 ; 
       fix_blinker-t2d88.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       37 13 110 ; 
       49 18 110 ; 
       50 37 110 ; 
       18 13 110 ; 
       0 8 110 ; 
       1 12 110 ; 
       2 8 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 33 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       10 34 110 ; 
       11 35 110 ; 
       12 15 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 27 110 ; 
       19 25 110 ; 
       20 10 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 26 110 ; 
       25 52 110 ; 
       26 25 110 ; 
       27 28 110 ; 
       28 57 110 ; 
       29 55 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 19 110 ; 
       33 55 110 ; 
       34 35 110 ; 
       36 42 110 ; 
       38 40 110 ; 
       39 41 110 ; 
       40 52 110 ; 
       41 40 110 ; 
       42 43 110 ; 
       43 57 110 ; 
       44 55 110 ; 
       45 12 110 ; 
       46 12 110 ; 
       47 38 110 ; 
       48 55 110 ; 
       51 12 110 ; 
       52 10 110 ; 
       53 35 110 ; 
       54 12 110 ; 
       55 1 110 ; 
       56 44 110 ; 
       57 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       37 58 300 ; 
       49 56 300 ; 
       50 57 300 ; 
       18 59 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 43 300 ; 
       2 9 300 ; 
       5 45 300 ; 
       6 46 300 ; 
       7 47 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       10 3 300 ; 
       12 17 300 ; 
       12 19 300 ; 
       12 34 300 ; 
       14 10 300 ; 
       19 40 300 ; 
       21 27 300 ; 
       21 28 300 ; 
       21 29 300 ; 
       22 1 300 ; 
       22 30 300 ; 
       22 31 300 ; 
       23 2 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       25 42 300 ; 
       26 11 300 ; 
       26 16 300 ; 
       26 18 300 ; 
       27 20 300 ; 
       27 21 300 ; 
       27 22 300 ; 
       27 35 300 ; 
       28 26 300 ; 
       29 49 300 ; 
       33 55 300 ; 
       38 41 300 ; 
       40 50 300 ; 
       41 37 300 ; 
       41 38 300 ; 
       41 39 300 ; 
       42 23 300 ; 
       42 24 300 ; 
       42 25 300 ; 
       42 36 300 ; 
       43 0 300 ; 
       44 51 300 ; 
       48 48 300 ; 
       51 15 300 ; 
       55 44 300 ; 
       55 53 300 ; 
       55 54 300 ; 
       56 52 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       35 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 35 401 ; 
       2 36 401 ; 
       4 20 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       58 50 401 ; 
       10 5 401 ; 
       11 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 7 401 ; 
       16 6 401 ; 
       17 9 401 ; 
       18 8 401 ; 
       19 17 401 ; 
       20 30 401 ; 
       21 29 401 ; 
       22 28 401 ; 
       23 27 401 ; 
       24 26 401 ; 
       25 25 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       28 11 401 ; 
       29 12 401 ; 
       30 13 401 ; 
       31 14 401 ; 
       32 15 401 ; 
       33 16 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       59 51 401 ; 
       41 41 401 ; 
       42 23 401 ; 
       44 42 401 ; 
       45 47 401 ; 
       46 48 401 ; 
       47 49 401 ; 
       48 43 401 ; 
       50 34 401 ; 
       53 44 401 ; 
       54 45 401 ; 
       55 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       37 SCHEM 47.8673 -13.23221 0 USR MPRFLG 0 ; 
       49 SCHEM 41.6173 -15.23221 0 MPRFLG 0 ; 
       50 SCHEM 46.6173 -15.23221 0 MPRFLG 0 ; 
       18 SCHEM 42.8673 -13.23221 0 USR MPRFLG 0 ; 
       0 SCHEM 167.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 136.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 178.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 162.5 -12 0 MPRFLG 0 ; 
       4 SCHEM 160 -12 0 MPRFLG 0 ; 
       5 SCHEM 127.5 -16 0 MPRFLG 0 ; 
       6 SCHEM 126.25 -18 0 MPRFLG 0 ; 
       7 SCHEM 125 -20 0 MPRFLG 0 ; 
       8 SCHEM 173.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 177.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 192.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 131.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 45 -6 0 MPRFLG 0 ; 
       14 SCHEM 40 -8 0 MPRFLG 0 ; 
       15 SCHEM 146.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 120 -10 0 MPRFLG 0 ; 
       17 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 55 -8 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 70 -8 0 MPRFLG 0 ; 
       24 SCHEM 10 -12 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       27 SCHEM 80 -10 0 MPRFLG 0 ; 
       28 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       29 SCHEM 140 -14 0 MPRFLG 0 ; 
       30 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 128.75 -14 0 MPRFLG 0 ; 
       34 SCHEM 97.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       40 SCHEM 30 -8 0 MPRFLG 0 ; 
       41 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       42 SCHEM 95 -10 0 MPRFLG 0 ; 
       43 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       44 SCHEM 136.25 -14 0 MPRFLG 0 ; 
       45 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 122.5 -14 0 MPRFLG 0 ; 
       51 SCHEM 105 -10 0 MPRFLG 0 ; 
       52 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       53 SCHEM 195 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       55 SCHEM 135 -12 0 MPRFLG 0 ; 
       56 SCHEM 135 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 88.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 190 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 175 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 165 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 170 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 180 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 205 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 187.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 182.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 197.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 185 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 200 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 147.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 130 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 127.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 125 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 122.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 140 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 137.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 135 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 142.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 145 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 132.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 165 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 167.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 170 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 172.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 180 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 205 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 185 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 187.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 182.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 175 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 200 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 147.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 142.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 145 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 132.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 130 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 127.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 125 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 196.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
