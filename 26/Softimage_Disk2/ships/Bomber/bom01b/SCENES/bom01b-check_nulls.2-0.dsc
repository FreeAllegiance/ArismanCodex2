SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-null3_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.18-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       check_nulls-default1.1-0 ; 
       check_nulls-default3.1-0 ; 
       check_nulls-default4.1-0 ; 
       check_nulls-mat1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat13.1-0 ; 
       check_nulls-mat14.1-0 ; 
       check_nulls-mat15.1-0 ; 
       check_nulls-mat16.1-0 ; 
       check_nulls-mat17.1-0 ; 
       check_nulls-mat18.1-0 ; 
       check_nulls-mat19.1-0 ; 
       check_nulls-mat2.1-0 ; 
       check_nulls-mat20.1-0 ; 
       check_nulls-mat21.1-0 ; 
       check_nulls-mat22.1-0 ; 
       check_nulls-mat23.1-0 ; 
       check_nulls-mat24.1-0 ; 
       check_nulls-mat25.1-0 ; 
       check_nulls-mat3.1-0 ; 
       check_nulls-mat34.1-0 ; 
       check_nulls-mat4.1-0 ; 
       check_nulls-mat44.1-0 ; 
       check_nulls-mat45.1-0 ; 
       check_nulls-mat46.1-0 ; 
       check_nulls-mat47.1-0 ; 
       check_nulls-mat48.1-0 ; 
       check_nulls-mat49.1-0 ; 
       check_nulls-mat50.1-0 ; 
       check_nulls-mat51.1-0 ; 
       check_nulls-mat52.1-0 ; 
       check_nulls-mat53.1-0 ; 
       check_nulls-mat54.1-0 ; 
       check_nulls-mat55.1-0 ; 
       check_nulls-mat56.1-0 ; 
       check_nulls-mat57.1-0 ; 
       check_nulls-mat58.1-0 ; 
       check_nulls-mat59.1-0 ; 
       check_nulls-mat63.1-0 ; 
       check_nulls-mat64.1-0 ; 
       check_nulls-mat65.1-0 ; 
       check_nulls-mat66.1-0 ; 
       check_nulls-mat67.1-0 ; 
       check_nulls-mat69.1-0 ; 
       check_nulls-mat70.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat82.1-0 ; 
       check_nulls-mat84.1-0 ; 
       check_nulls-mat85.1-0 ; 
       check_nulls-mat86.1-0 ; 
       check_nulls-mat87.1-0 ; 
       check_nulls-mat88.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-mat90.1-0 ; 
       check_nulls-mat91.1-0 ; 
       check_nulls-mat92.1-0 ; 
       check_nulls-mat93.1-0 ; 
       check_nulls-mat94.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-antenn2.1-0 ; 
       check_nulls-antenn3.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1.8-0 ; 
       check_nulls-cockpt_1.3-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-lbwepemt.1-0 ; 
       check_nulls-lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLl.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepbas_3.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-null2.1-0 ; 
       check_nulls-null3_1.2-0 ROOT ; 
       check_nulls-rbwepemt.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepbas_3.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-twepemt.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-check_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d16.1-0 ; 
       check_nulls-t2d17.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d2.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d3.1-0 ; 
       check_nulls-t2d30.1-0 ; 
       check_nulls-t2d31.1-0 ; 
       check_nulls-t2d53.1-0 ; 
       check_nulls-t2d54.1-0 ; 
       check_nulls-t2d55.1-0 ; 
       check_nulls-t2d56.1-0 ; 
       check_nulls-t2d58.1-0 ; 
       check_nulls-t2d59.1-0 ; 
       check_nulls-t2d61.1-0 ; 
       check_nulls-t2d62.1-0 ; 
       check_nulls-t2d63.1-0 ; 
       check_nulls-t2d64.1-0 ; 
       check_nulls-t2d65.1-0 ; 
       check_nulls-t2d69.1-0 ; 
       check_nulls-t2d7.1-0 ; 
       check_nulls-t2d70.1-0 ; 
       check_nulls-t2d71.1-0 ; 
       check_nulls-t2d72.1-0 ; 
       check_nulls-t2d73.1-0 ; 
       check_nulls-t2d74.1-0 ; 
       check_nulls-t2d75.1-0 ; 
       check_nulls-t2d76.1-0 ; 
       check_nulls-t2d77.1-0 ; 
       check_nulls-t2d78.1-0 ; 
       check_nulls-t2d79.1-0 ; 
       check_nulls-t2d8.1-0 ; 
       check_nulls-t2d80.1-0 ; 
       check_nulls-t2d81.1-0 ; 
       check_nulls-t2d82.1-0 ; 
       check_nulls-t2d83.1-0 ; 
       check_nulls-t2d84.1-0 ; 
       check_nulls-t2d85.1-0 ; 
       check_nulls-t2d86.1-0 ; 
       check_nulls-t2d87.1-0 ; 
       check_nulls-t2d88.1-0 ; 
       check_nulls-t2d89.1-0 ; 
       check_nulls-t2d90.1-0 ; 
       check_nulls-t2d91.1-0 ; 
       check_nulls-t2d92.1-0 ; 
       check_nulls-t2d93.1-0 ; 
       check_nulls-t2d94.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 12 110 ; 
       2 8 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 33 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       10 34 110 ; 
       11 35 110 ; 
       12 15 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 27 110 ; 
       18 13 110 ; 
       19 25 110 ; 
       20 10 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 26 110 ; 
       25 52 110 ; 
       26 25 110 ; 
       27 28 110 ; 
       28 57 110 ; 
       29 55 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 19 110 ; 
       33 55 110 ; 
       34 35 110 ; 
       36 42 110 ; 
       37 13 110 ; 
       38 40 110 ; 
       39 41 110 ; 
       40 52 110 ; 
       41 40 110 ; 
       42 43 110 ; 
       43 57 110 ; 
       44 55 110 ; 
       45 12 110 ; 
       46 12 110 ; 
       47 38 110 ; 
       48 55 110 ; 
       49 18 110 ; 
       50 37 110 ; 
       51 12 110 ; 
       52 10 110 ; 
       53 35 110 ; 
       54 12 110 ; 
       55 1 110 ; 
       56 44 110 ; 
       57 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 47 300 ; 
       2 9 300 ; 
       5 49 300 ; 
       6 50 300 ; 
       7 51 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       10 3 300 ; 
       12 21 300 ; 
       12 23 300 ; 
       12 38 300 ; 
       14 12 300 ; 
       18 11 300 ; 
       19 44 300 ; 
       21 31 300 ; 
       21 32 300 ; 
       21 33 300 ; 
       22 1 300 ; 
       22 34 300 ; 
       22 35 300 ; 
       23 2 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       25 46 300 ; 
       26 13 300 ; 
       26 20 300 ; 
       26 22 300 ; 
       27 24 300 ; 
       27 25 300 ; 
       27 26 300 ; 
       27 39 300 ; 
       28 30 300 ; 
       29 53 300 ; 
       33 59 300 ; 
       37 10 300 ; 
       38 45 300 ; 
       40 54 300 ; 
       41 41 300 ; 
       41 42 300 ; 
       41 43 300 ; 
       42 27 300 ; 
       42 28 300 ; 
       42 29 300 ; 
       42 40 300 ; 
       43 0 300 ; 
       44 55 300 ; 
       48 52 300 ; 
       49 17 300 ; 
       50 18 300 ; 
       51 19 300 ; 
       55 48 300 ; 
       55 57 300 ; 
       55 58 300 ; 
       56 56 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       35 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 25 401 ; 
       48 44 401 ; 
       49 49 401 ; 
       50 50 401 ; 
       51 51 401 ; 
       52 45 401 ; 
       54 36 401 ; 
       57 46 401 ; 
       58 47 401 ; 
       59 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 136.25 -12 0 MPRFLG 0 ; 
       1 SCHEM 128.75 -12 0 MPRFLG 0 ; 
       2 SCHEM 140 -12 0 MPRFLG 0 ; 
       3 SCHEM 137.5 -14 0 MPRFLG 0 ; 
       4 SCHEM 135 -14 0 MPRFLG 0 ; 
       5 SCHEM 127.5 -18 0 MPRFLG 0 ; 
       6 SCHEM 127.5 -20 0 MPRFLG 0 ; 
       7 SCHEM 127.5 -22 0 MPRFLG 0 ; 
       8 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 140 -14 0 MPRFLG 0 ; 
       10 SCHEM 108.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 110.2049 -24.81841 0 USR WIRECOL 2 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 120 -10 0 MPRFLG 0 ; 
       13 SCHEM 90 -8 0 MPRFLG 0 ; 
       14 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 122.5 -12 0 MPRFLG 0 ; 
       17 SCHEM 116.746 -24.27744 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 90 -10 0 MPRFLG 0 ; 
       19 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       20 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 95 -10 0 MPRFLG 0 ; 
       22 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 100 -10 0 MPRFLG 0 ; 
       24 SCHEM 80 -14 0 MPRFLG 0 ; 
       25 SCHEM 78.75 -10 0 MPRFLG 0 ; 
       26 SCHEM 80 -12 0 MPRFLG 0 ; 
       27 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 132.5 -16 0 MPRFLG 0 ; 
       30 SCHEM 113.3366 -22.5559 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 114.416 -23.09186 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 115.4746 -23.66516 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 127.5 -16 0 MPRFLG 0 ; 
       34 SCHEM 108.75 -4 0 MPRFLG 0 ; 
       35 SCHEM 108.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 121.6204 -24.28857 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 85 -14 0 MPRFLG 0 ; 
       40 SCHEM 83.75 -10 0 MPRFLG 0 ; 
       41 SCHEM 85 -12 0 MPRFLG 0 ; 
       42 SCHEM 105 -12 0 MPRFLG 0 ; 
       43 SCHEM 105 -10 0 MPRFLG 0 ; 
       44 SCHEM 130 -16 0 MPRFLG 0 ; 
       45 SCHEM 118.2689 -22.54647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 119.2941 -23.07716 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 120.3476 -23.63049 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 125 -16 0 MPRFLG 0 ; 
       49 SCHEM 90 -12 0 MPRFLG 0 ; 
       50 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       53 SCHEM 111.6978 -25.54067 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 120 -12 0 MPRFLG 0 ; 
       55 SCHEM 128.75 -14 0 MPRFLG 0 ; 
       56 SCHEM 124.2818 -24.1344 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 103.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 106.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 141.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 139 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 141.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 141.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 89 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 104 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 134 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 79 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 84 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 134 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 134 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 129 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 129 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 126.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 124 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 131.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 131.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 129 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 134 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 134 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 129 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 141.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 91.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 81.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 141.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 139 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 134 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 81.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 106.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 104 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 106.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 104 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 94 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 86.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 96.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 99 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 86.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 79 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 84 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 134 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 124 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 134 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 134 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 129 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 129 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 129 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 126.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 145 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
