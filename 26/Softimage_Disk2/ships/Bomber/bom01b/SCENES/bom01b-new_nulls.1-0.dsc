SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom01a_1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.26-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 59     
       check_nulls-default1.1-0 ; 
       check_nulls-default3.1-0 ; 
       check_nulls-default4.1-0 ; 
       check_nulls-mat1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat13.1-0 ; 
       check_nulls-mat14.1-0 ; 
       check_nulls-mat15.1-0 ; 
       check_nulls-mat16.1-0 ; 
       check_nulls-mat19.1-0 ; 
       check_nulls-mat2.1-0 ; 
       check_nulls-mat20.1-0 ; 
       check_nulls-mat21.1-0 ; 
       check_nulls-mat22.1-0 ; 
       check_nulls-mat25.1-0 ; 
       check_nulls-mat3.1-0 ; 
       check_nulls-mat34.1-0 ; 
       check_nulls-mat4.1-0 ; 
       check_nulls-mat44.1-0 ; 
       check_nulls-mat45.1-0 ; 
       check_nulls-mat46.1-0 ; 
       check_nulls-mat47.1-0 ; 
       check_nulls-mat48.1-0 ; 
       check_nulls-mat49.1-0 ; 
       check_nulls-mat50.1-0 ; 
       check_nulls-mat51.1-0 ; 
       check_nulls-mat52.1-0 ; 
       check_nulls-mat53.1-0 ; 
       check_nulls-mat54.1-0 ; 
       check_nulls-mat55.1-0 ; 
       check_nulls-mat56.1-0 ; 
       check_nulls-mat57.1-0 ; 
       check_nulls-mat58.1-0 ; 
       check_nulls-mat59.1-0 ; 
       check_nulls-mat63.1-0 ; 
       check_nulls-mat64.1-0 ; 
       check_nulls-mat65.1-0 ; 
       check_nulls-mat66.1-0 ; 
       check_nulls-mat67.1-0 ; 
       check_nulls-mat69.1-0 ; 
       check_nulls-mat70.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat82.1-0 ; 
       check_nulls-mat84.1-0 ; 
       check_nulls-mat85.1-0 ; 
       check_nulls-mat86.1-0 ; 
       check_nulls-mat87.1-0 ; 
       check_nulls-mat88.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-mat90.1-0 ; 
       check_nulls-mat92.1-0 ; 
       check_nulls-mat93.1-0 ; 
       check_nulls-mat94.1-0 ; 
       fix_blinker-mat23.1-0 ; 
       fix_blinker-mat24.1-0 ; 
       fix_blinker-mat71.1-0 ; 
       fix_blinker-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-antenn2.1-0 ; 
       check_nulls-antenn3.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1_1.1-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLl.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepbas_3.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-missemt1.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepbas_3.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-new_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d2.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d3.1-0 ; 
       check_nulls-t2d30.1-0 ; 
       check_nulls-t2d31.1-0 ; 
       check_nulls-t2d53.1-0 ; 
       check_nulls-t2d54.1-0 ; 
       check_nulls-t2d55.1-0 ; 
       check_nulls-t2d56.1-0 ; 
       check_nulls-t2d58.1-0 ; 
       check_nulls-t2d59.1-0 ; 
       check_nulls-t2d61.1-0 ; 
       check_nulls-t2d62.1-0 ; 
       check_nulls-t2d63.1-0 ; 
       check_nulls-t2d64.1-0 ; 
       check_nulls-t2d65.1-0 ; 
       check_nulls-t2d69.1-0 ; 
       check_nulls-t2d7.1-0 ; 
       check_nulls-t2d70.1-0 ; 
       check_nulls-t2d71.1-0 ; 
       check_nulls-t2d72.1-0 ; 
       check_nulls-t2d73.1-0 ; 
       check_nulls-t2d74.1-0 ; 
       check_nulls-t2d75.1-0 ; 
       check_nulls-t2d76.1-0 ; 
       check_nulls-t2d77.1-0 ; 
       check_nulls-t2d78.1-0 ; 
       check_nulls-t2d79.1-0 ; 
       check_nulls-t2d8.1-0 ; 
       check_nulls-t2d80.1-0 ; 
       check_nulls-t2d81.1-0 ; 
       check_nulls-t2d82.1-0 ; 
       check_nulls-t2d83.1-0 ; 
       check_nulls-t2d84.1-0 ; 
       check_nulls-t2d85.1-0 ; 
       check_nulls-t2d86.1-0 ; 
       check_nulls-t2d87.1-0 ; 
       check_nulls-t2d88.1-0 ; 
       check_nulls-t2d89.1-0 ; 
       check_nulls-t2d90.1-0 ; 
       check_nulls-t2d91.1-0 ; 
       check_nulls-t2d92.1-0 ; 
       check_nulls-t2d93.1-0 ; 
       check_nulls-t2d94.1-0 ; 
       fix_blinker-t2d87.1-0 ; 
       fix_blinker-t2d88.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 12 110 ; 
       2 8 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 32 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       34 42 110 ; 
       12 15 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 13 110 ; 
       18 24 110 ; 
       19 10 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       24 50 110 ; 
       25 24 110 ; 
       26 27 110 ; 
       27 54 110 ; 
       28 53 110 ; 
       11 10 110 ; 
       32 53 110 ; 
       35 13 110 ; 
       36 38 110 ; 
       38 50 110 ; 
       39 38 110 ; 
       40 41 110 ; 
       41 54 110 ; 
       42 53 110 ; 
       46 53 110 ; 
       47 17 110 ; 
       48 35 110 ; 
       23 25 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 18 110 ; 
       33 26 110 ; 
       37 39 110 ; 
       43 12 110 ; 
       44 12 110 ; 
       45 36 110 ; 
       51 10 110 ; 
       49 12 110 ; 
       50 10 110 ; 
       52 12 110 ; 
       53 1 110 ; 
       54 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 43 300 ; 
       2 9 300 ; 
       5 45 300 ; 
       6 46 300 ; 
       7 47 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       10 3 300 ; 
       12 17 300 ; 
       12 19 300 ; 
       12 34 300 ; 
       14 10 300 ; 
       17 58 300 ; 
       18 40 300 ; 
       20 27 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       21 1 300 ; 
       21 30 300 ; 
       21 31 300 ; 
       22 2 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       24 42 300 ; 
       25 11 300 ; 
       25 16 300 ; 
       25 18 300 ; 
       26 20 300 ; 
       26 21 300 ; 
       26 22 300 ; 
       26 35 300 ; 
       27 26 300 ; 
       28 49 300 ; 
       32 54 300 ; 
       35 57 300 ; 
       36 41 300 ; 
       38 50 300 ; 
       39 37 300 ; 
       39 38 300 ; 
       39 39 300 ; 
       40 23 300 ; 
       40 24 300 ; 
       40 25 300 ; 
       40 36 300 ; 
       41 0 300 ; 
       42 51 300 ; 
       46 48 300 ; 
       47 55 300 ; 
       48 56 300 ; 
       49 15 300 ; 
       53 44 300 ; 
       53 52 300 ; 
       53 53 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 35 401 ; 
       2 36 401 ; 
       4 20 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 7 401 ; 
       16 6 401 ; 
       17 9 401 ; 
       18 8 401 ; 
       19 17 401 ; 
       20 30 401 ; 
       21 29 401 ; 
       22 28 401 ; 
       23 27 401 ; 
       24 26 401 ; 
       25 25 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       28 11 401 ; 
       29 12 401 ; 
       30 13 401 ; 
       31 14 401 ; 
       32 15 401 ; 
       33 16 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       41 41 401 ; 
       42 23 401 ; 
       44 42 401 ; 
       45 47 401 ; 
       46 48 401 ; 
       47 49 401 ; 
       48 43 401 ; 
       50 34 401 ; 
       52 44 401 ; 
       53 45 401 ; 
       54 46 401 ; 
       57 50 401 ; 
       58 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 58.75 -8 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 60 -10 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 50 -14 0 MPRFLG 0 ; 
       6 SCHEM 50 -16 0 MPRFLG 0 ; 
       7 SCHEM 50 -18 0 MPRFLG 0 ; 
       8 SCHEM 60 -6 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 52.5 -14 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 10 -6 0 MPRFLG 0 ; 
       15 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 45 -8 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 0 -8 0 MPRFLG 0 ; 
       19 SCHEM 20 -4 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 20 -6 0 MPRFLG 0 ; 
       22 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 25 -8 0 MPRFLG 0 ; 
       27 SCHEM 25 -6 0 MPRFLG 0 ; 
       28 SCHEM 55 -12 0 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50 -12 0 MPRFLG 0 ; 
       35 SCHEM 15 -6 0 MPRFLG 0 ; 
       36 SCHEM 5 -8 0 MPRFLG 0 ; 
       38 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       46 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       47 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       48 SCHEM 15 -8 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 30 -8 0 MPRFLG 0 ; 
       50 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       52 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       53 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       54 SCHEM 26.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 54.55698 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 51.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 46.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 49 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
