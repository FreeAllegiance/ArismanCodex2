SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom01a_1_1_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       check_nulls-default1.2-0 ; 
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat25.2-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat45.2-0 ; 
       check_nulls-mat46.2-0 ; 
       check_nulls-mat47.2-0 ; 
       check_nulls-mat48.2-0 ; 
       check_nulls-mat49.2-0 ; 
       check_nulls-mat50.2-0 ; 
       check_nulls-mat51.2-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat63.2-0 ; 
       check_nulls-mat64.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat69.2-0 ; 
       check_nulls-mat70.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat81.2-0 ; 
       check_nulls-mat82.2-0 ; 
       check_nulls-mat84.2-0 ; 
       check_nulls-mat85.2-0 ; 
       check_nulls-mat86.2-0 ; 
       check_nulls-mat87.2-0 ; 
       check_nulls-mat88.2-0 ; 
       check_nulls-mat9.2-0 ; 
       check_nulls-mat90.2-0 ; 
       check_nulls-mat92.2-0 ; 
       check_nulls-mat93.2-0 ; 
       check_nulls-mat94.2-0 ; 
       fix_blinker-mat23.2-0 ; 
       fix_blinker-mat24.2-0 ; 
       fix_blinker-mat71.2-0 ; 
       fix_blinker-mat72.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-antenn2.1-0 ; 
       check_nulls-antenn3.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1_1_1.2-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-lsmoke.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepbas_3.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rsmoke.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepbas_3.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-turwepemt1.1-0 ; 
       check_nulls-turwepemt2.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-removed_landing.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d69.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d70.2-0 ; 
       check_nulls-t2d71.2-0 ; 
       check_nulls-t2d72.2-0 ; 
       check_nulls-t2d73.2-0 ; 
       check_nulls-t2d74.2-0 ; 
       check_nulls-t2d75.2-0 ; 
       check_nulls-t2d76.2-0 ; 
       check_nulls-t2d77.2-0 ; 
       check_nulls-t2d78.2-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       check_nulls-t2d85.2-0 ; 
       check_nulls-t2d86.2-0 ; 
       check_nulls-t2d87.2-0 ; 
       check_nulls-t2d88.2-0 ; 
       check_nulls-t2d89.2-0 ; 
       check_nulls-t2d90.2-0 ; 
       check_nulls-t2d91.2-0 ; 
       check_nulls-t2d92.2-0 ; 
       check_nulls-t2d93.2-0 ; 
       check_nulls-t2d94.2-0 ; 
       fix_blinker-t2d87.2-0 ; 
       fix_blinker-t2d88.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 12 110 ; 
       2 8 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 29 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       11 10 110 ; 
       12 15 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       16 12 110 ; 
       17 13 110 ; 
       18 21 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 47 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 53 110 ; 
       25 50 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 18 110 ; 
       29 50 110 ; 
       30 23 110 ; 
       31 13 110 ; 
       32 35 110 ; 
       33 36 110 ; 
       34 36 110 ; 
       35 47 110 ; 
       36 35 110 ; 
       37 38 110 ; 
       38 53 110 ; 
       39 50 110 ; 
       40 12 110 ; 
       41 12 110 ; 
       42 32 110 ; 
       43 50 110 ; 
       44 17 110 ; 
       45 31 110 ; 
       46 12 110 ; 
       47 10 110 ; 
       48 10 110 ; 
       49 12 110 ; 
       50 1 110 ; 
       51 39 110 ; 
       52 10 110 ; 
       53 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       1 34 300 ; 
       2 7 300 ; 
       5 36 300 ; 
       6 37 300 ; 
       7 38 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       10 1 300 ; 
       12 15 300 ; 
       12 17 300 ; 
       12 25 300 ; 
       14 8 300 ; 
       17 49 300 ; 
       18 31 300 ; 
       21 33 300 ; 
       22 9 300 ; 
       22 14 300 ; 
       22 16 300 ; 
       23 18 300 ; 
       23 19 300 ; 
       23 20 300 ; 
       23 26 300 ; 
       24 24 300 ; 
       25 40 300 ; 
       29 45 300 ; 
       31 48 300 ; 
       32 32 300 ; 
       35 41 300 ; 
       36 28 300 ; 
       36 29 300 ; 
       36 30 300 ; 
       37 21 300 ; 
       37 22 300 ; 
       37 23 300 ; 
       37 27 300 ; 
       38 0 300 ; 
       39 42 300 ; 
       43 39 300 ; 
       44 46 300 ; 
       45 47 300 ; 
       46 13 300 ; 
       50 35 300 ; 
       50 43 300 ; 
       50 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 25 401 ; 
       2 14 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 10 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 7 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 8 401 ; 
       17 11 401 ; 
       18 24 401 ; 
       19 23 401 ; 
       20 22 401 ; 
       21 21 401 ; 
       22 20 401 ; 
       23 19 401 ; 
       24 26 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 18 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 17 401 ; 
       35 33 401 ; 
       36 38 401 ; 
       37 39 401 ; 
       38 40 401 ; 
       39 34 401 ; 
       41 27 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       45 37 401 ; 
       48 41 401 ; 
       49 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 68.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 61.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 70 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 67.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 60 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 60 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 60 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 72.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 41.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 75 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 56.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 55 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 8.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 42.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 45 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 60 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 17.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 16.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 62.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 47.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 50 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 12.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 57.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 52.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 61.25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 62.5 -12 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 47.13975 -7.714386 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 36.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 79 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 61.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 61.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 59 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
