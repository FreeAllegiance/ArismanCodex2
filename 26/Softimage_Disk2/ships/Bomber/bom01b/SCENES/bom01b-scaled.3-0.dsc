SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom01a_1_1.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 59     
       check_nulls-default1.2-0 ; 
       check_nulls-default3.2-0 ; 
       check_nulls-default4.2-0 ; 
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat25.2-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat45.2-0 ; 
       check_nulls-mat46.2-0 ; 
       check_nulls-mat47.2-0 ; 
       check_nulls-mat48.2-0 ; 
       check_nulls-mat49.2-0 ; 
       check_nulls-mat50.2-0 ; 
       check_nulls-mat51.2-0 ; 
       check_nulls-mat52.2-0 ; 
       check_nulls-mat53.2-0 ; 
       check_nulls-mat54.2-0 ; 
       check_nulls-mat55.2-0 ; 
       check_nulls-mat56.2-0 ; 
       check_nulls-mat57.2-0 ; 
       check_nulls-mat58.2-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat63.2-0 ; 
       check_nulls-mat64.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat69.2-0 ; 
       check_nulls-mat70.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat81.2-0 ; 
       check_nulls-mat82.2-0 ; 
       check_nulls-mat84.2-0 ; 
       check_nulls-mat85.2-0 ; 
       check_nulls-mat86.2-0 ; 
       check_nulls-mat87.2-0 ; 
       check_nulls-mat88.2-0 ; 
       check_nulls-mat9.2-0 ; 
       check_nulls-mat90.2-0 ; 
       check_nulls-mat92.2-0 ; 
       check_nulls-mat93.2-0 ; 
       check_nulls-mat94.2-0 ; 
       fix_blinker-mat23.2-0 ; 
       fix_blinker-mat24.2-0 ; 
       fix_blinker-mat71.2-0 ; 
       fix_blinker-mat72.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       check_nulls-175deg.2-0 ROOT ; 
       check_nulls-175deg_1.3-0 ROOT ; 
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-alengine.1-0 ; 
       check_nulls-alengine1.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-antenn2.1-0 ; 
       check_nulls-antenn3.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-awepmnt.1-0 ; 
       check_nulls-bom01a_1_1.14-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLl.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-lsmoke.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-lwepbas_3.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-lwepemt2.1-0 ; 
       check_nulls-lwepemt3.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rsmoke.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwepbas_3.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-turwepemt1.1-0 ; 
       check_nulls-turwepemt2.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
       scaled-cube2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-scaled.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d53.2-0 ; 
       check_nulls-t2d54.2-0 ; 
       check_nulls-t2d55.2-0 ; 
       check_nulls-t2d56.2-0 ; 
       check_nulls-t2d58.2-0 ; 
       check_nulls-t2d59.2-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d69.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d70.2-0 ; 
       check_nulls-t2d71.2-0 ; 
       check_nulls-t2d72.2-0 ; 
       check_nulls-t2d73.2-0 ; 
       check_nulls-t2d74.2-0 ; 
       check_nulls-t2d75.2-0 ; 
       check_nulls-t2d76.2-0 ; 
       check_nulls-t2d77.2-0 ; 
       check_nulls-t2d78.2-0 ; 
       check_nulls-t2d79.2-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d80.2-0 ; 
       check_nulls-t2d81.2-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       check_nulls-t2d85.2-0 ; 
       check_nulls-t2d86.2-0 ; 
       check_nulls-t2d87.2-0 ; 
       check_nulls-t2d88.2-0 ; 
       check_nulls-t2d89.2-0 ; 
       check_nulls-t2d90.2-0 ; 
       check_nulls-t2d91.2-0 ; 
       check_nulls-t2d92.2-0 ; 
       check_nulls-t2d93.2-0 ; 
       check_nulls-t2d94.2-0 ; 
       fix_blinker-t2d87.2-0 ; 
       fix_blinker-t2d88.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 10 110 ; 
       3 14 110 ; 
       4 10 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 35 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 17 110 ; 
       11 4 110 ; 
       13 12 110 ; 
       14 17 110 ; 
       15 12 110 ; 
       16 15 110 ; 
       17 12 110 ; 
       18 14 110 ; 
       19 15 110 ; 
       20 27 110 ; 
       21 12 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 28 110 ; 
       26 28 110 ; 
       27 53 110 ; 
       28 27 110 ; 
       29 30 110 ; 
       30 59 110 ; 
       31 56 110 ; 
       32 14 110 ; 
       33 14 110 ; 
       34 20 110 ; 
       35 56 110 ; 
       36 29 110 ; 
       37 15 110 ; 
       38 41 110 ; 
       39 42 110 ; 
       40 42 110 ; 
       41 53 110 ; 
       42 41 110 ; 
       43 44 110 ; 
       44 59 110 ; 
       45 56 110 ; 
       46 14 110 ; 
       47 14 110 ; 
       48 38 110 ; 
       49 56 110 ; 
       50 19 110 ; 
       51 37 110 ; 
       52 14 110 ; 
       53 12 110 ; 
       54 12 110 ; 
       55 14 110 ; 
       56 3 110 ; 
       57 45 110 ; 
       58 12 110 ; 
       59 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 4 300 ; 
       2 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       3 43 300 ; 
       4 9 300 ; 
       7 45 300 ; 
       8 46 300 ; 
       9 47 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       12 3 300 ; 
       14 17 300 ; 
       14 19 300 ; 
       14 34 300 ; 
       16 10 300 ; 
       19 58 300 ; 
       20 40 300 ; 
       22 27 300 ; 
       22 28 300 ; 
       22 29 300 ; 
       23 1 300 ; 
       23 30 300 ; 
       23 31 300 ; 
       24 2 300 ; 
       24 32 300 ; 
       24 33 300 ; 
       27 42 300 ; 
       28 11 300 ; 
       28 16 300 ; 
       28 18 300 ; 
       29 20 300 ; 
       29 21 300 ; 
       29 22 300 ; 
       29 35 300 ; 
       30 26 300 ; 
       31 49 300 ; 
       35 54 300 ; 
       37 57 300 ; 
       38 41 300 ; 
       41 50 300 ; 
       42 37 300 ; 
       42 38 300 ; 
       42 39 300 ; 
       43 23 300 ; 
       43 24 300 ; 
       43 25 300 ; 
       43 36 300 ; 
       44 0 300 ; 
       45 51 300 ; 
       49 48 300 ; 
       50 55 300 ; 
       51 56 300 ; 
       52 15 300 ; 
       56 44 300 ; 
       56 52 300 ; 
       56 53 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       12 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 35 401 ; 
       2 36 401 ; 
       4 20 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 7 401 ; 
       16 6 401 ; 
       17 9 401 ; 
       18 8 401 ; 
       19 17 401 ; 
       20 30 401 ; 
       21 29 401 ; 
       22 28 401 ; 
       23 27 401 ; 
       24 26 401 ; 
       25 25 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       28 11 401 ; 
       29 12 401 ; 
       30 13 401 ; 
       31 14 401 ; 
       32 15 401 ; 
       33 16 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 40 401 ; 
       41 41 401 ; 
       42 23 401 ; 
       44 42 401 ; 
       45 47 401 ; 
       46 48 401 ; 
       47 49 401 ; 
       48 43 401 ; 
       50 34 401 ; 
       52 44 401 ; 
       53 45 401 ; 
       54 46 401 ; 
       57 50 401 ; 
       58 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.47096 3.919877 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 0.9979199 0.9979199 0.9979199 0.37 0 0 0 1.502891 0.5853071 MPRFLG 0 ; 
       1 SCHEM 31.15345 3.973749 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 0.9979199 0.9979199 0.9979199 5.312391 0 0 0 0.3595352 -3.060951 MPRFLG 0 ; 
       2 SCHEM 68.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 61.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 72.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 70 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 67.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 60 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 60 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 60 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 72.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 41.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0.8294539 MPRFLG 0 ; 
       13 SCHEM 75 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 56.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 55 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 8.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 65 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 42.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 45 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 60 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 35 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 15 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 17.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 16.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 62.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 47.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 50 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 12.5 -8 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 57.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 52.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 61.25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 62.5 -12 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 47.13975 -7.714386 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 36.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 40.51402 10.1325 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 79 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 61.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 59 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 61.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 61.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 59 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
