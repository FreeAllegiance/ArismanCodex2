SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.37-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       check_nulls-mat1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat13.1-0 ; 
       check_nulls-mat14.1-0 ; 
       check_nulls-mat15.1-0 ; 
       check_nulls-mat16.1-0 ; 
       check_nulls-mat19.1-0 ; 
       check_nulls-mat2.1-0 ; 
       check_nulls-mat20.1-0 ; 
       check_nulls-mat21.1-0 ; 
       check_nulls-mat22.1-0 ; 
       check_nulls-mat3.1-0 ; 
       check_nulls-mat34.1-0 ; 
       check_nulls-mat4.1-0 ; 
       check_nulls-mat44.1-0 ; 
       check_nulls-mat59.1-0 ; 
       check_nulls-mat65.1-0 ; 
       check_nulls-mat66.1-0 ; 
       check_nulls-mat67.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat81.1-0 ; 
       check_nulls-mat82.1-0 ; 
       check_nulls-mat87.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-mat92.1-0 ; 
       check_nulls-mat93.1-0 ; 
       check_nulls-mat94.1-0 ; 
       fix_blinker-mat71.1-0 ; 
       fix_blinker-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-bom01a_1_1.10-0 ROOT ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-static.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d2.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d3.1-0 ; 
       check_nulls-t2d30.1-0 ; 
       check_nulls-t2d31.1-0 ; 
       check_nulls-t2d61.1-0 ; 
       check_nulls-t2d62.1-0 ; 
       check_nulls-t2d63.1-0 ; 
       check_nulls-t2d64.1-0 ; 
       check_nulls-t2d65.1-0 ; 
       check_nulls-t2d7.1-0 ; 
       check_nulls-t2d8.1-0 ; 
       check_nulls-t2d82.1-0 ; 
       check_nulls-t2d83.1-0 ; 
       check_nulls-t2d84.1-0 ; 
       check_nulls-t2d87.1-0 ; 
       check_nulls-t2d88.1-0 ; 
       check_nulls-t2d89.1-0 ; 
       check_nulls-t2d90.1-0 ; 
       check_nulls-t2d91.1-0 ; 
       fix_blinker-t2d87.1-0 ; 
       fix_blinker-t2d88.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 5 110 ; 
       2 3 110 ; 
       3 8 110 ; 
       5 8 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 4 110 ; 
       9 6 110 ; 
       10 17 110 ; 
       11 10 110 ; 
       12 18 110 ; 
       13 6 110 ; 
       14 17 110 ; 
       15 14 110 ; 
       16 18 110 ; 
       17 4 110 ; 
       18 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       1 21 300 ; 
       2 6 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       4 0 300 ; 
       5 13 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       7 7 300 ; 
       9 29 300 ; 
       10 20 300 ; 
       11 8 300 ; 
       11 12 300 ; 
       11 14 300 ; 
       12 27 300 ; 
       13 28 300 ; 
       14 24 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       16 23 300 ; 
       18 22 300 ; 
       18 25 300 ; 
       18 26 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 14 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 10 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 7 401 ; 
       12 6 401 ; 
       13 9 401 ; 
       14 8 401 ; 
       15 11 401 ; 
       16 15 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 20 401 ; 
       20 16 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 17 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 18.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 21.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 17.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 18.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
