SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom01a_1_1_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.7-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       check_nulls-default1.2-0 ; 
       check_nulls-mat1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat14.2-0 ; 
       check_nulls-mat15.2-0 ; 
       check_nulls-mat16.2-0 ; 
       check_nulls-mat19.2-0 ; 
       check_nulls-mat2.2-0 ; 
       check_nulls-mat20.2-0 ; 
       check_nulls-mat21.2-0 ; 
       check_nulls-mat22.2-0 ; 
       check_nulls-mat25.2-0 ; 
       check_nulls-mat3.2-0 ; 
       check_nulls-mat34.2-0 ; 
       check_nulls-mat4.2-0 ; 
       check_nulls-mat44.2-0 ; 
       check_nulls-mat45.2-0 ; 
       check_nulls-mat46.2-0 ; 
       check_nulls-mat47.2-0 ; 
       check_nulls-mat48.2-0 ; 
       check_nulls-mat49.2-0 ; 
       check_nulls-mat50.2-0 ; 
       check_nulls-mat51.2-0 ; 
       check_nulls-mat59.2-0 ; 
       check_nulls-mat63.2-0 ; 
       check_nulls-mat64.2-0 ; 
       check_nulls-mat65.2-0 ; 
       check_nulls-mat66.2-0 ; 
       check_nulls-mat67.2-0 ; 
       check_nulls-mat69.2-0 ; 
       check_nulls-mat70.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat81.2-0 ; 
       check_nulls-mat82.2-0 ; 
       check_nulls-mat84.2-0 ; 
       check_nulls-mat87.2-0 ; 
       check_nulls-mat9.2-0 ; 
       check_nulls-mat92.2-0 ; 
       check_nulls-mat93.2-0 ; 
       check_nulls-mat94.2-0 ; 
       fix_blinker-mat23.2-0 ; 
       fix_blinker-mat24.2-0 ; 
       fix_blinker-mat71.2-0 ; 
       fix_blinker-mat72.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       check_nulls-abfuselg.1-0 ; 
       check_nulls-acs63_1.4-0 ; 
       check_nulls-afuselg.1-0 ; 
       check_nulls-antenn1.1-0 ; 
       check_nulls-atfuselg.1-0 ; 
       check_nulls-bom01a_1_1_1.3-0 ROOT ; 
       check_nulls-ffuselg.11-0 ; 
       check_nulls-finzzz0.1-0 ; 
       check_nulls-finzzz1.1-0 ; 
       check_nulls-fuselg0.1-0 ; 
       check_nulls-Lfinzzz.1-0 ; 
       check_nulls-lgun.1-0 ; 
       check_nulls-lthrust1.1-0 ; 
       check_nulls-lturbine.1-0 ; 
       check_nulls-lwepbar.1-0 ; 
       check_nulls-lwepbas.1-0 ; 
       check_nulls-Lwingzz.1-0 ; 
       check_nulls-rfinzzz.1-0 ; 
       check_nulls-rgun.1-0 ; 
       check_nulls-rthrust12.1-0 ; 
       check_nulls-rturbine.1-0 ; 
       check_nulls-rwepbar.1-0 ; 
       check_nulls-rwepbas.1-0 ; 
       check_nulls-rwingzz.1-0 ; 
       check_nulls-SSal_1.1-0 ; 
       check_nulls-SSar_1.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-thrust0.1-0 ; 
       check_nulls-turret1.1-0 ; 
       check_nulls-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-static.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       check_nulls-t2d11.2-0 ; 
       check_nulls-t2d12.2-0 ; 
       check_nulls-t2d13.2-0 ; 
       check_nulls-t2d14.2-0 ; 
       check_nulls-t2d15.2-0 ; 
       check_nulls-t2d18.2-0 ; 
       check_nulls-t2d2.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d3.2-0 ; 
       check_nulls-t2d30.2-0 ; 
       check_nulls-t2d31.2-0 ; 
       check_nulls-t2d61.2-0 ; 
       check_nulls-t2d62.2-0 ; 
       check_nulls-t2d63.2-0 ; 
       check_nulls-t2d64.2-0 ; 
       check_nulls-t2d65.2-0 ; 
       check_nulls-t2d69.2-0 ; 
       check_nulls-t2d7.2-0 ; 
       check_nulls-t2d70.2-0 ; 
       check_nulls-t2d71.2-0 ; 
       check_nulls-t2d72.2-0 ; 
       check_nulls-t2d73.2-0 ; 
       check_nulls-t2d74.2-0 ; 
       check_nulls-t2d75.2-0 ; 
       check_nulls-t2d76.2-0 ; 
       check_nulls-t2d77.2-0 ; 
       check_nulls-t2d78.2-0 ; 
       check_nulls-t2d8.2-0 ; 
       check_nulls-t2d82.2-0 ; 
       check_nulls-t2d83.2-0 ; 
       check_nulls-t2d84.2-0 ; 
       check_nulls-t2d85.2-0 ; 
       check_nulls-t2d86.2-0 ; 
       check_nulls-t2d87.2-0 ; 
       check_nulls-t2d88.2-0 ; 
       check_nulls-t2d89.2-0 ; 
       check_nulls-t2d90.2-0 ; 
       check_nulls-t2d91.2-0 ; 
       check_nulls-t2d92.2-0 ; 
       fix_blinker-t2d87.2-0 ; 
       fix_blinker-t2d88.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 6 110 ; 
       2 4 110 ; 
       3 16 110 ; 
       4 9 110 ; 
       6 9 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 5 110 ; 
       10 7 110 ; 
       11 12 110 ; 
       12 27 110 ; 
       13 12 110 ; 
       14 15 110 ; 
       15 29 110 ; 
       16 28 110 ; 
       17 7 110 ; 
       18 19 110 ; 
       19 27 110 ; 
       20 19 110 ; 
       21 22 110 ; 
       22 29 110 ; 
       23 28 110 ; 
       24 10 110 ; 
       25 17 110 ; 
       26 6 110 ; 
       27 5 110 ; 
       28 1 110 ; 
       29 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       1 34 300 ; 
       2 7 300 ; 
       3 36 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       5 1 300 ; 
       6 15 300 ; 
       6 17 300 ; 
       6 25 300 ; 
       8 8 300 ; 
       10 45 300 ; 
       11 31 300 ; 
       12 33 300 ; 
       13 9 300 ; 
       13 14 300 ; 
       13 16 300 ; 
       14 18 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       14 26 300 ; 
       15 24 300 ; 
       16 41 300 ; 
       17 44 300 ; 
       18 32 300 ; 
       19 38 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       21 23 300 ; 
       21 27 300 ; 
       22 0 300 ; 
       23 37 300 ; 
       24 42 300 ; 
       25 43 300 ; 
       26 13 300 ; 
       28 35 300 ; 
       28 39 300 ; 
       28 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 25 401 ; 
       2 14 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 10 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 7 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 8 401 ; 
       17 11 401 ; 
       18 24 401 ; 
       19 23 401 ; 
       20 22 401 ; 
       21 21 401 ; 
       22 20 401 ; 
       23 19 401 ; 
       24 26 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 18 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 17 401 ; 
       35 33 401 ; 
       36 38 401 ; 
       37 34 401 ; 
       38 27 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       44 39 401 ; 
       45 40 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 68.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 61.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 60 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 41.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 56.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 8.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 60 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 16.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 57.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 61.25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 36.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 79 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 61.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
