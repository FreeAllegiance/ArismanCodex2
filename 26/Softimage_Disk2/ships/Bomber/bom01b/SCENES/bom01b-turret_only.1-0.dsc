SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       mdfedtur_sPTLN-mat53_1.2-0 ; 
       mdfedtur_sPTLN-mat54.2-0 ; 
       mdfedtur_sPTLN-mat55.2-0 ; 
       mdfedtur_sPTLN-mat56.2-0 ; 
       mdfedtur_sPTLN-mat57.2-0 ; 
       mdfedtur_sPTLN-mat58.2-0 ; 
       mdfedtur_sPTLN-mat59.2-0 ; 
       mdfedtur_sPTLN-mat60.2-0 ; 
       mdfedtur_sPTLN-mat61.2-0 ; 
       mdfedtur_sPTLN-mat62.2-0 ; 
       mdfedtur_sPTLN-mat63.2-0 ; 
       mdfedtur_sPTLN-mat64.2-0 ; 
       mdfedtur_sPTLN-mat65.2-0 ; 
       mdfedtur_sPTLN-mat66.2-0 ; 
       mdfedtur_sPTLN-mat67.2-0 ; 
       mdfedtur_sPTLN-mat68.2-0 ; 
       mdfedtur_sPTLN-mat69.2-0 ; 
       mdfedtur_sPTLN-mat70.2-0 ; 
       mdfedtur_sPTLN-mat71.2-0 ; 
       mdfedtur_sPTLN-mat72.2-0 ; 
       mdfedtur_sPTLN-mat73.2-0 ; 
       mdfedtur_sPTLN-mat74.2-0 ; 
       mdfedtur_sPTLN-mat75.2-0 ; 
       mdfedtur_sPTLN-mat76.2-0 ; 
       mdfedtur_sPTLN-mat77.2-0 ; 
       mdfedtur_sPTLN-mat78.2-0 ; 
       mdfedtur_sPTLN-mat79.2-0 ; 
       mdfedtur_sPTLN-mat80.2-0 ; 
       mdfedtur_sPTLN-mat81.2-0 ; 
       mdfedtur_sPTLN-mat82.2-0 ; 
       mdfedtur_sPTLN-mat83.2-0 ; 
       mdfedtur_sPTLN-mat84.2-0 ; 
       mdfedtur_sPTLN-mat85.2-0 ; 
       mdfedtur_sPTLN-mat86.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       acs63-acs63.2-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwepmnt.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwepmnt.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01b/PICTURES/acs63 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-turret_only.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       mdfedtur_sPTLN-t2d1.2-0 ; 
       mdfedtur_sPTLN-t2d10.2-0 ; 
       mdfedtur_sPTLN-t2d11.2-0 ; 
       mdfedtur_sPTLN-t2d12.2-0 ; 
       mdfedtur_sPTLN-t2d13.2-0 ; 
       mdfedtur_sPTLN-t2d14.2-0 ; 
       mdfedtur_sPTLN-t2d15.2-0 ; 
       mdfedtur_sPTLN-t2d16.2-0 ; 
       mdfedtur_sPTLN-t2d17.2-0 ; 
       mdfedtur_sPTLN-t2d18.2-0 ; 
       mdfedtur_sPTLN-t2d19.2-0 ; 
       mdfedtur_sPTLN-t2d2.2-0 ; 
       mdfedtur_sPTLN-t2d20.2-0 ; 
       mdfedtur_sPTLN-t2d21.2-0 ; 
       mdfedtur_sPTLN-t2d22.2-0 ; 
       mdfedtur_sPTLN-t2d23.2-0 ; 
       mdfedtur_sPTLN-t2d24.2-0 ; 
       mdfedtur_sPTLN-t2d25.2-0 ; 
       mdfedtur_sPTLN-t2d26.2-0 ; 
       mdfedtur_sPTLN-t2d27.2-0 ; 
       mdfedtur_sPTLN-t2d28.2-0 ; 
       mdfedtur_sPTLN-t2d29.2-0 ; 
       mdfedtur_sPTLN-t2d3.2-0 ; 
       mdfedtur_sPTLN-t2d30.2-0 ; 
       mdfedtur_sPTLN-t2d4.2-0 ; 
       mdfedtur_sPTLN-t2d5.2-0 ; 
       mdfedtur_sPTLN-t2d6.2-0 ; 
       mdfedtur_sPTLN-t2d7.2-0 ; 
       mdfedtur_sPTLN-t2d8.2-0 ; 
       mdfedtur_sPTLN-t2d9.2-0 ; 
       mdfedtur_sPTLN-z.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 10 110 ; 
       5 4 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 32 300 ; 
       2 0 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       3 0 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 33 300 ; 
       4 0 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       6 0 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 0 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 10 300 ; 
       9 0 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       10 0 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       10 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       2 0 401 ; 
       3 11 401 ; 
       4 22 401 ; 
       5 24 401 ; 
       6 25 401 ; 
       7 26 401 ; 
       8 27 401 ; 
       9 28 401 ; 
       10 29 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 21 401 ; 
       32 23 401 ; 
       33 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 1.015213 1.015213 1.015213 0.1 0 0 0 1.092601 -0.3735156 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
