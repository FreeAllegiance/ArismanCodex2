SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       acs63-acs63.4-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwepmnt.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwepmnt.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-turret_only_no_mat.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 10 110 ; 
       5 4 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 1.015213 1.015213 1.015213 0.1 0 0 0 1.092601 -0.3735156 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 2 2 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
