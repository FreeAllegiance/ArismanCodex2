SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       final-null3_1.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.11-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       final-default1.3-0 ; 
       final-default3.3-0 ; 
       final-default4.3-0 ; 
       final-mat1.3-0 ; 
       final-mat10.3-0 ; 
       final-mat12.3-0 ; 
       final-mat13.3-0 ; 
       final-mat14.3-0 ; 
       final-mat15.3-0 ; 
       final-mat16.3-0 ; 
       final-mat17.3-0 ; 
       final-mat18.3-0 ; 
       final-mat19.3-0 ; 
       final-mat2.3-0 ; 
       final-mat20.3-0 ; 
       final-mat21.3-0 ; 
       final-mat22.3-0 ; 
       final-mat23.3-0 ; 
       final-mat24.3-0 ; 
       final-mat25.3-0 ; 
       final-mat3.3-0 ; 
       final-mat34.3-0 ; 
       final-mat4.3-0 ; 
       final-mat44.3-0 ; 
       final-mat45.3-0 ; 
       final-mat46.3-0 ; 
       final-mat47.3-0 ; 
       final-mat48.3-0 ; 
       final-mat49.3-0 ; 
       final-mat50.3-0 ; 
       final-mat51.3-0 ; 
       final-mat52.3-0 ; 
       final-mat53.3-0 ; 
       final-mat54.3-0 ; 
       final-mat55.3-0 ; 
       final-mat56.3-0 ; 
       final-mat57.3-0 ; 
       final-mat58.3-0 ; 
       final-mat59.3-0 ; 
       final-mat63.3-0 ; 
       final-mat64.3-0 ; 
       final-mat65.3-0 ; 
       final-mat66.3-0 ; 
       final-mat67.3-0 ; 
       final-mat69.3-0 ; 
       final-mat70.3-0 ; 
       final-mat8.3-0 ; 
       final-mat9.3-0 ; 
       with_turret-mat81.2-0 ; 
       with_turret-mat82.3-0 ; 
       with_turret-mat84.2-0 ; 
       with_turret-mat85.2-0 ; 
       with_turret-mat86.2-0 ; 
       with_turret-mat87.2-0 ; 
       with_turret-mat88.2-0 ; 
       with_turret-mat90.2-0 ; 
       with_turret-mat91.2-0 ; 
       with_turret-mat92.2-0 ; 
       with_turret-mat93.1-0 ; 
       with_turret-mat94.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       bom07-cockpt_1_2.2-0 ROOT ; 
       final-abfuselg.1-0 ; 
       final-acs63_1.4-0 ; 
       final-afuselg.1-0 ; 
       final-alengine.1-0 ; 
       final-alengine1.1-0 ; 
       final-antenn1.1-0 ; 
       final-antenn2.1-0 ; 
       final-antenn3.1-0 ; 
       final-atfuselg.1-0 ; 
       final-awepmnt.1-0 ; 
       final-bom01a_1.8-0 ; 
       final-ffuselg.11-0 ; 
       final-finzzz0.1-0 ; 
       final-finzzz1.1-0 ; 
       final-fuselg0.1-0 ; 
       final-fwepmnt.1-0 ; 
       final-l-gun.1-0 ; 
       final-l-gun1.1-0 ; 
       final-lbwepemt.1-0 ; 
       final-lfinzzz.1-0 ; 
       final-LL0.1-0 ; 
       final-LLf.1-0 ; 
       final-LLl.1-0 ; 
       final-LLr.1-0 ; 
       final-lthrust.1-0 ; 
       final-lthrust1.1-0 ; 
       final-lturbine.1-0 ; 
       final-lwepbar.1-0 ; 
       final-lwepbas.1-0 ; 
       final-lwepbas_3.1-0 ; 
       final-lwepemt1.1-0 ; 
       final-lwepemt2.1-0 ; 
       final-Lwingzz.1-0 ; 
       final-null2.1-0 ; 
       final-null3_1.7-0 ROOT ; 
       final-rbwepemt.1-0 ; 
       final-rfinzzz.1-0 ; 
       final-rotation_limit_2.2-0 ROOT ; 
       final-rthrust.1-0 ; 
       final-rthrust12.1-0 ; 
       final-rturbine.1-0 ; 
       final-rwepbar.1-0 ; 
       final-rwepbas.1-0 ; 
       final-rwepbas_3.1-0 ; 
       final-rwepemt1.1-0 ; 
       final-rwepemt2.1-0 ; 
       final-rwingzz.1-0 ; 
       final-SSal.1-0 ; 
       final-SSar.1-0 ; 
       final-SSf.1-0 ; 
       final-thrust0.1-0 ; 
       final-tturatt.1-0 ; 
       final-turret1.1-0 ; 
       final-wepbas0.1-0 ; 
       final-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom01b/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01b-with_turret.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       final-t2d11.3-0 ; 
       final-t2d12.3-0 ; 
       final-t2d13.3-0 ; 
       final-t2d14.3-0 ; 
       final-t2d15.3-0 ; 
       final-t2d16.3-0 ; 
       final-t2d17.3-0 ; 
       final-t2d18.3-0 ; 
       final-t2d2.3-0 ; 
       final-t2d21.3-0 ; 
       final-t2d3.3-0 ; 
       final-t2d30.3-0 ; 
       final-t2d31.3-0 ; 
       final-t2d53.3-0 ; 
       final-t2d54.3-0 ; 
       final-t2d55.3-0 ; 
       final-t2d56.3-0 ; 
       final-t2d58.3-0 ; 
       final-t2d59.3-0 ; 
       final-t2d61.3-0 ; 
       final-t2d62.3-0 ; 
       final-t2d63.3-0 ; 
       final-t2d64.3-0 ; 
       final-t2d65.3-0 ; 
       final-t2d69.3-0 ; 
       final-t2d7.3-0 ; 
       final-t2d70.3-0 ; 
       final-t2d71.3-0 ; 
       final-t2d72.3-0 ; 
       final-t2d73.3-0 ; 
       final-t2d74.3-0 ; 
       final-t2d75.3-0 ; 
       final-t2d76.3-0 ; 
       final-t2d77.3-0 ; 
       final-t2d78.3-0 ; 
       final-t2d79.3-0 ; 
       final-t2d8.3-0 ; 
       final-t2d80.3-0 ; 
       final-t2d81.3-0 ; 
       final-t2d82.3-0 ; 
       final-t2d83.3-0 ; 
       final-t2d84.3-0 ; 
       final-t2d85.3-0 ; 
       final-t2d86.3-0 ; 
       with_turret-t2d87.4-0 ; 
       with_turret-t2d88.3-0 ; 
       with_turret-t2d89.2-0 ; 
       with_turret-t2d90.1-0 ; 
       with_turret-t2d91.1-0 ; 
       with_turret-t2d92.1-0 ; 
       with_turret-t2d93.1-0 ; 
       with_turret-t2d94.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       33 53 110 ; 
       1 9 110 ; 
       2 12 110 ; 
       3 9 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 33 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 15 110 ; 
       10 3 110 ; 
       11 34 110 ; 
       12 15 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 12 110 ; 
       17 26 110 ; 
       18 40 110 ; 
       19 28 110 ; 
       20 13 110 ; 
       21 11 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 27 110 ; 
       26 51 110 ; 
       27 26 110 ; 
       28 29 110 ; 
       29 54 110 ; 
       30 53 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       34 35 110 ; 
       36 42 110 ; 
       37 13 110 ; 
       39 41 110 ; 
       40 51 110 ; 
       41 40 110 ; 
       42 43 110 ; 
       43 54 110 ; 
       44 53 110 ; 
       45 12 110 ; 
       46 12 110 ; 
       55 44 110 ; 
       47 53 110 ; 
       48 20 110 ; 
       49 37 110 ; 
       50 12 110 ; 
       51 11 110 ; 
       52 12 110 ; 
       53 2 110 ; 
       54 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       33 59 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 48 300 ; 
       3 9 300 ; 
       6 50 300 ; 
       7 51 300 ; 
       8 52 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       11 3 300 ; 
       12 21 300 ; 
       12 23 300 ; 
       12 38 300 ; 
       14 12 300 ; 
       17 44 300 ; 
       18 45 300 ; 
       20 11 300 ; 
       22 31 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       23 1 300 ; 
       23 34 300 ; 
       23 35 300 ; 
       24 2 300 ; 
       24 36 300 ; 
       24 37 300 ; 
       26 46 300 ; 
       27 13 300 ; 
       27 20 300 ; 
       27 22 300 ; 
       28 24 300 ; 
       28 25 300 ; 
       28 26 300 ; 
       28 39 300 ; 
       29 30 300 ; 
       30 54 300 ; 
       37 10 300 ; 
       40 47 300 ; 
       41 41 300 ; 
       41 42 300 ; 
       41 43 300 ; 
       42 27 300 ; 
       42 28 300 ; 
       42 29 300 ; 
       42 40 300 ; 
       43 0 300 ; 
       44 55 300 ; 
       55 56 300 ; 
       47 53 300 ; 
       48 17 300 ; 
       49 18 300 ; 
       50 19 300 ; 
       53 49 300 ; 
       53 57 300 ; 
       53 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       35 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 33 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       4 22 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 12 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 9 401 ; 
       20 8 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 19 401 ; 
       24 32 401 ; 
       25 31 401 ; 
       26 30 401 ; 
       27 29 401 ; 
       28 28 401 ; 
       29 27 401 ; 
       30 34 401 ; 
       31 35 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       35 16 401 ; 
       36 17 401 ; 
       37 18 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 25 401 ; 
       47 36 401 ; 
       49 44 401 ; 
       59 48 401 ; 
       50 49 401 ; 
       51 50 401 ; 
       52 51 401 ; 
       53 45 401 ; 
       57 46 401 ; 
       58 47 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       33 SCHEM 50 -16 0 MPRFLG 0 ; 
       0 SCHEM 0 -26 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       1 SCHEM 58.75 -12 0 MPRFLG 0 ; 
       2 SCHEM 51.25 -12 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       4 SCHEM 60 -14 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -14 0 MPRFLG 0 ; 
       6 SCHEM 50 -18 0 MPRFLG 0 ; 
       7 SCHEM 50 -20 0 MPRFLG 0 ; 
       8 SCHEM 50 -22 0 MPRFLG 0 ; 
       9 SCHEM 60 -10 0 MPRFLG 0 ; 
       10 SCHEM 62.5 -14 0 MPRFLG 0 ; 
       11 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 10 -10 0 MPRFLG 0 ; 
       15 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 45 -12 0 MPRFLG 0 ; 
       17 SCHEM 0 -12 0 MPRFLG 0 ; 
       18 SCHEM 5 -12 0 MPRFLG 0 ; 
       19 SCHEM 25 -14 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 20 -10 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -14 0 MPRFLG 0 ; 
       26 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       27 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 25 -12 0 MPRFLG 0 ; 
       29 SCHEM 25 -10 0 MPRFLG 0 ; 
       30 SCHEM 55 -16 0 MPRFLG 0 ; 
       31 SCHEM 35 -12 0 MPRFLG 0 ; 
       32 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       35 SCHEM 31.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 27.5 -14 0 MPRFLG 0 ; 
       37 SCHEM 15 -10 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -26 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       39 SCHEM 7.5 -14 0 MPRFLG 0 ; 
       40 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       42 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       44 SCHEM 52.5 -16 0 MPRFLG 0 ; 
       45 SCHEM 40 -12 0 MPRFLG 0 ; 
       46 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       55 SCHEM 52.5 -18 0 MPRFLG 0 ; 
       47 SCHEM 47.5 -16 0 MPRFLG 0 ; 
       48 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       49 SCHEM 15 -12 0 MPRFLG 0 ; 
       50 SCHEM 30 -12 0 MPRFLG 0 ; 
       51 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       52 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       53 SCHEM 51.25 -14 0 MPRFLG 0 ; 
       54 SCHEM 26.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 57.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 52.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 52.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 52.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 50 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 47.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 57.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 55 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 52.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 57.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 57.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 0 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 57.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 47.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 57.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 57.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 52.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 52.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 52.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 50 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
