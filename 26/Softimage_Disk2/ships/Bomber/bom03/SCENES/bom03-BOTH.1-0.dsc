SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom03_1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.86-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.86-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       BOTH-mat14.1-0 ; 
       BOTH-mat15.1-0 ; 
       BOTH-mat16.1-0 ; 
       BOTH-mat17.1-0 ; 
       BOTH-mat18.1-0 ; 
       check_nulls-bottom.2-0 ; 
       check_nulls-bottom_1.1-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_side_1.1-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-cockpit_top_1.1-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-engine1_1.1-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-exterior_intake_duct1_1.1-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-glows_1.1-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-inside_landing_bays_1.1-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom_1.1-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-intake1_1.1-0 ; 
       check_nulls-mat10.3-0 ; 
       check_nulls-mat11.3-0 ; 
       check_nulls-mat12.3-0 ; 
       check_nulls-mat13.3-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat6_1.1-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-mat7_1.1-0 ; 
       check_nulls-mat8.3-0 ; 
       check_nulls-mat9.3-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-Rear_1.1-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-side_glow1_1.1-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-top_1.1-0 ; 
       check_nulls-wing.2-0 ; 
       check_nulls-wing_1.1-0 ; 
       tweak-mat3.3-0 ; 
       tweak-mat3_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       check_nulls-body.1-0 ; 
       check_nulls-body_1.1-0 ; 
       check_nulls-bom03_1.9-0 ROOT ; 
       check_nulls-bom03_1_1.1-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-fwepmnt_1.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-lsmoke.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rsmoke.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz_1_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-rwingzz4_1.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-tturatt_1.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
       check_nulls-wingzz0_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d10_1.1-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d11_1.1-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d12_1.1-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d13_1.1-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d14_1.1-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d15_1.1-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d16_1.1-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d17_1.1-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d18_1.1-0 ; 
       check_nulls-t2d19.3-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d2_1_1.1-0 ; 
       check_nulls-t2d20.3-0 ; 
       check_nulls-t2d21.3-0 ; 
       check_nulls-t2d22.3-0 ; 
       check_nulls-t2d23.3-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d24_1.1-0 ; 
       check_nulls-t2d25.3-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d4_1.1-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d5_1.1-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d6_1.1-0 ; 
       check_nulls-t2d7.3-0 ; 
       check_nulls-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       4 3 110 ; 
       5 10 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       9 5 110 ; 
       10 0 110 ; 
       11 5 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 23 110 ; 
       17 0 110 ; 
       18 3 110 ; 
       19 3 110 ; 
       20 21 110 ; 
       21 35 110 ; 
       23 35 110 ; 
       25 5 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 3 110 ; 
       32 0 110 ; 
       34 6 110 ; 
       35 0 110 ; 
       1 2 110 ; 
       8 1 110 ; 
       22 36 110 ; 
       24 36 110 ; 
       33 1 110 ; 
       36 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 34 300 ; 
       0 16 300 ; 
       0 10 300 ; 
       0 40 300 ; 
       0 38 300 ; 
       0 6 300 ; 
       0 18 300 ; 
       0 8 300 ; 
       0 36 300 ; 
       0 20 300 ; 
       0 22 300 ; 
       0 14 300 ; 
       0 12 300 ; 
       3 42 300 ; 
       6 26 300 ; 
       9 32 300 ; 
       11 31 300 ; 
       12 24 300 ; 
       13 25 300 ; 
       21 28 300 ; 
       23 30 300 ; 
       25 23 300 ; 
       26 3 300 ; 
       27 2 300 ; 
       28 4 300 ; 
       29 1 300 ; 
       30 0 300 ; 
       1 33 300 ; 
       1 15 300 ; 
       1 9 300 ; 
       1 39 300 ; 
       1 37 300 ; 
       1 5 300 ; 
       1 17 300 ; 
       1 7 300 ; 
       1 35 300 ; 
       1 19 300 ; 
       1 21 300 ; 
       1 13 300 ; 
       1 11 300 ; 
       2 41 300 ; 
       22 27 300 ; 
       24 29 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 1 401 ; 
       8 5 401 ; 
       10 31 401 ; 
       12 17 401 ; 
       14 26 401 ; 
       16 29 401 ; 
       18 3 401 ; 
       20 9 401 ; 
       22 11 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 27 401 ; 
       28 20 401 ; 
       30 13 401 ; 
       31 18 401 ; 
       32 21 401 ; 
       34 15 401 ; 
       36 7 401 ; 
       38 35 401 ; 
       40 33 401 ; 
       5 0 401 ; 
       7 4 401 ; 
       9 30 401 ; 
       11 16 401 ; 
       13 25 401 ; 
       15 28 401 ; 
       17 2 401 ; 
       19 8 401 ; 
       21 10 401 ; 
       27 19 401 ; 
       29 12 401 ; 
       33 14 401 ; 
       35 6 401 ; 
       37 34 401 ; 
       39 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 48.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 53.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 28.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 8.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       26 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       27 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       30 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       31 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       34 SCHEM 11.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 64.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 64.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 60.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 68.25 -6 0 MPRFLG 0 ; 
       24 SCHEM 65.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 63.25 -4 0 MPRFLG 0 ; 
       36 SCHEM 67 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 67.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 64.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 69.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 69.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 64.75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 67.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 69.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
