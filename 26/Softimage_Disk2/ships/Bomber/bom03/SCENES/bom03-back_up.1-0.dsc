SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       back_up-rwingzz2.1-0 ; 
       bom03-bom03.65-0 ; 
       bom03-SSl.29-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tweak-cam_int1.49-0 ROOT ; 
       tweak-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       back_up-bottom.1-0 ; 
       back_up-cockpit_side.1-0 ; 
       back_up-cockpit_top.1-0 ; 
       back_up-glows.1-0 ; 
       back_up-inside_landing_bays.1-0 ; 
       back_up-intake_ducts1.1-0 ; 
       back_up-intake1.1-0 ; 
       back_up-mat4.1-0 ; 
       back_up-mat6.1-0 ; 
       back_up-mat7.1-0 ; 
       back_up-side_glow1.1-0 ; 
       back_up-top.1-0 ; 
       back_up-wing.1-0 ; 
       tweak-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       back_up-rwingzz2.1-0 ROOT ; 
       bom03-body.1-0 ; 
       bom03-bom03.58-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.4-0 ROOT ; 
       bom03-lwepemt2.4-0 ROOT ; 
       bom03-lwepemt3.4-0 ROOT ; 
       bom03-lwepemt4.4-0 ROOT ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.13-0 ROOT ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-back_up.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       back_up-t2d10.1-0 ; 
       back_up-t2d11.1-0 ; 
       back_up-t2d12.1-0 ; 
       back_up-t2d13.1-0 ; 
       back_up-t2d14.1-0 ; 
       back_up-t2d15.1-0 ; 
       back_up-t2d2.1-0 ; 
       back_up-t2d3.1-0 ; 
       back_up-t2d4.1-0 ; 
       back_up-t2d5.1-0 ; 
       back_up-t2d6.1-0 ; 
       back_up-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 1 110 ; 
       4 9 110 ; 
       5 1 110 ; 
       6 4 110 ; 
       7 1 110 ; 
       8 7 110 ; 
       9 1 110 ; 
       10 4 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       17 1 110 ; 
       18 17 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 23 110 ; 
       23 31 110 ; 
       24 4 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       29 23 110 ; 
       30 1 110 ; 
       31 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       1 3 300 ; 
       1 2 300 ; 
       1 12 300 ; 
       1 11 300 ; 
       1 0 300 ; 
       1 4 300 ; 
       1 1 300 ; 
       1 10 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       2 13 300 ; 
       23 8 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 1 15000 ; 
       28 2 15000 ; 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 9 401 ; 
       3 8 401 ; 
       4 1 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 3 401 ; 
       11 11 401 ; 
       12 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 38.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 47.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 37.59199 -5.9974 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 40.09199 -7.9974 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 40 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.59199 -7.9974 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 42.59199 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 45.09199 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 92.5 0 0 DISPLAY 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.469012 0.1655194 0.03320503 MPRFLG 0 ; 
       14 SCHEM 90 0 0 DISPLAY 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.728187 0.1655194 0.04909849 MPRFLG 0 ; 
       15 SCHEM 87.5 0 0 DISPLAY 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.972691 0.1655194 0.1187822 MPRFLG 0 ; 
       16 SCHEM 85 0 0 DISPLAY 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 2.220863 0.1655199 0.1872434 MPRFLG 0 ; 
       17 SCHEM 30 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 13.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 35.09199 -7.9974 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 82.5 0 0 DISPLAY 0 0 SRT 0.263884 0.263884 0.263884 -1.570796 3.141593 1.293485e-007 3.725748 0.1616147 -2.547819 MPRFLG 0 ; 
       29 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 13.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 78.75 0 0 DISPLAY 0 0 SRT 0.9999998 0.9999999 1 0 0 0 0 0.1630181 -0.5624125 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 62.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 55 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 95 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 102.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 80 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 75 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 67.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 100 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 107.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 80 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 60 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 72.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 82.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 77.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 55 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
