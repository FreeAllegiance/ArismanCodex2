SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-null1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.64-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.64-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       check_nulls-bottom.1-0 ; 
       check_nulls-cockpit_side.1-0 ; 
       check_nulls-cockpit_top.1-0 ; 
       check_nulls-engine1.1-0 ; 
       check_nulls-exterior_intake_duct1.1-0 ; 
       check_nulls-glows.1-0 ; 
       check_nulls-inside_landing_bays.1-0 ; 
       check_nulls-intake_ducts_top-n-bottom.1-0 ; 
       check_nulls-intake1.1-0 ; 
       check_nulls-mat10.1-0 ; 
       check_nulls-mat11.1-0 ; 
       check_nulls-mat12.1-0 ; 
       check_nulls-mat13.1-0 ; 
       check_nulls-mat6.1-0 ; 
       check_nulls-mat7.1-0 ; 
       check_nulls-mat8.1-0 ; 
       check_nulls-mat9.1-0 ; 
       check_nulls-Rear.1-0 ; 
       check_nulls-side_glow1.1-0 ; 
       check_nulls-top.1-0 ; 
       check_nulls-wing.1-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.27-0 ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-lengine.5-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-lwepemt1.8-0 ; 
       check_nulls-lwepemt2.8-0 ; 
       check_nulls-lwepemt3.8-0 ; 
       check_nulls-lwepemt4.8-0 ; 
       check_nulls-null1.2-0 ROOT ; 
       check_nulls-rengine.5-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwepemt4.1-0 ; 
       check_nulls-rwingzz.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-check_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       check_nulls-t2d10.1-0 ; 
       check_nulls-t2d11.1-0 ; 
       check_nulls-t2d12.1-0 ; 
       check_nulls-t2d13.1-0 ; 
       check_nulls-t2d14.1-0 ; 
       check_nulls-t2d15.1-0 ; 
       check_nulls-t2d16.1-0 ; 
       check_nulls-t2d17.1-0 ; 
       check_nulls-t2d18.1-0 ; 
       check_nulls-t2d19.1-0 ; 
       check_nulls-t2d2_1.1-0 ; 
       check_nulls-t2d20.1-0 ; 
       check_nulls-t2d21.1-0 ; 
       check_nulls-t2d22.1-0 ; 
       check_nulls-t2d23.1-0 ; 
       check_nulls-t2d24.1-0 ; 
       check_nulls-t2d25.1-0 ; 
       check_nulls-t2d4.1-0 ; 
       check_nulls-t2d5.1-0 ; 
       check_nulls-t2d6.1-0 ; 
       check_nulls-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 16 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       17 0 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 3 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 23 110 ; 
       29 22 110 ; 
       31 0 110 ; 
       32 4 110 ; 
       33 0 110 ; 
       30 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 18 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 21 300 ; 
       4 12 300 ; 
       6 16 300 ; 
       9 15 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       22 13 300 ; 
       23 14 300 ; 
       24 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 18 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 17 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 16 401 ; 
       13 10 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 7 401 ; 
       18 3 401 ; 
       19 20 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.508028 -4 0 MPRFLG 0 ; 
       1 SCHEM 8.508028 -2 0 MPRFLG 0 ; 
       2 SCHEM 36.00803 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM -3.991972 -8 0 MPRFLG 0 ; 
       4 SCHEM -16.49197 -8 0 MPRFLG 0 ; 
       5 SCHEM -16.49197 -6 0 MPRFLG 0 ; 
       6 SCHEM -1.491972 -10 0 MPRFLG 0 ; 
       7 SCHEM -18.99197 -6 0 MPRFLG 0 ; 
       8 SCHEM -6.491972 -6 0 MPRFLG 0 ; 
       9 SCHEM -6.491972 -10 0 MPRFLG 0 ; 
       10 SCHEM -8.991972 -8 0 MPRFLG 0 ; 
       11 SCHEM -11.49197 -8 0 MPRFLG 0 ; 
       12 SCHEM 18.50803 -10 0 MPRFLG 0 ; 
       13 SCHEM 11.00803 -10 0 MPRFLG 0 ; 
       14 SCHEM 13.50803 -10 0 MPRFLG 0 ; 
       15 SCHEM 16.00803 -10 0 MPRFLG 0 ; 
       16 SCHEM 7.258028 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM -13.99197 -6 0 MPRFLG 0 ; 
       18 SCHEM 33.50803 -10 0 MPRFLG 0 ; 
       19 SCHEM 23.50803 -10 0 MPRFLG 0 ; 
       20 SCHEM 26.00803 -10 0 MPRFLG 0 ; 
       21 SCHEM 28.50803 -10 0 MPRFLG 0 ; 
       22 SCHEM 28.50803 -8 0 MPRFLG 0 ; 
       23 SCHEM 16.00803 -8 0 MPRFLG 0 ; 
       24 SCHEM -3.991972 -10 0 MPRFLG 0 ; 
       25 SCHEM 1.008028 -6 0 MPRFLG 0 ; 
       26 SCHEM 3.508028 -6 0 MPRFLG 0 ; 
       27 SCHEM 6.008028 -6 0 MPRFLG 0 ; 
       28 SCHEM 21.00803 -10 0 MPRFLG 0 ; 
       29 SCHEM 31.00803 -10 0 MPRFLG 0 ; 
       31 SCHEM 8.508028 -6 0 MPRFLG 0 ; 
       32 SCHEM -16.49197 -10 0 MPRFLG 0 ; 
       33 SCHEM 22.25803 -6 0 MPRFLG 0 ; 
       30 SCHEM -21.49197 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20.94294 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15.94294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13.44294 -12.45996 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.51879 -9.837687 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 18.44294 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 23.44294 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 18.44294 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 23.44294 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20.94294 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15.94294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 13.44294 -14.45996 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.51879 -11.83769 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
