SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-null1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.68-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.68-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       check_nulls-bottom.2-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat11.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat9.2-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-wing.2-0 ; 
       lights-mat14.3-0 ; 
       lights-mat15.3-0 ; 
       lights-mat16.3-0 ; 
       lights-mat17.3-0 ; 
       lights-mat18.3-0 ; 
       tweak-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.27-0 ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-lengine.5-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-lwepemt1_1.8-0 ; 
       check_nulls-lwepemt2_1.8-0 ; 
       check_nulls-lwepemt3_1.8-0 ; 
       check_nulls-lwepemt4.8-0 ; 
       check_nulls-null1.6-0 ROOT ; 
       check_nulls-rengine.5-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwepemt4.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-lights.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d19.2-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d20.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d22.2-0 ; 
       check_nulls-t2d23.2-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d25.2-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 16 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       17 0 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 3 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 16 110 ; 
       31 0 110 ; 
       32 4 110 ; 
       33 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 18 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 26 300 ; 
       4 12 300 ; 
       6 16 300 ; 
       9 15 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       22 13 300 ; 
       23 14 300 ; 
       24 9 300 ; 
       25 24 300 ; 
       26 23 300 ; 
       27 25 300 ; 
       28 22 300 ; 
       29 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 18 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 17 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 16 401 ; 
       13 10 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 7 401 ; 
       18 3 401 ; 
       19 20 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 -6 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 66.7712 -8 0 MPRFLG 0 ; 
       3 SCHEM 20 -10 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 15 -10 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 35 -12 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 30 -12 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 50 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 40 -12 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 45 -12 0 MPRFLG 0 ; 
       22 SCHEM 45 -10 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 20 -12 0 MPRFLG 0 ; 
       25 SCHEM 56.04722 -41.50734 0 USR MPRFLG 0 ; 
       26 SCHEM 61.25033 -41.44765 0 USR MPRFLG 0 ; 
       27 SCHEM 67.16129 -41.60423 0 USR MPRFLG 0 ; 
       28 SCHEM 58.74413 -41.52789 0 USR MPRFLG 0 ; 
       29 SCHEM 64.2712 -41.55927 0 USR MPRFLG 0 ; 
       30 SCHEM 0 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 25 -8 0 MPRFLG 0 ; 
       32 SCHEM 5 -12 0 MPRFLG 0 ; 
       33 SCHEM 38.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 82.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 74.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 99.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 72.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 69.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 92.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 94.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 84.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 89.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 79.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 77.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 64.2712 -43.55927 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 58.74413 -43.52789 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 61.25033 -43.44765 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 56.04722 -43.50734 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67.16129 -43.60423 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 82.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 69.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 87.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 89.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 94.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 84.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 99.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 97.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 74.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 77.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 79.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 101.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
