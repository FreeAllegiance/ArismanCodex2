SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom03_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.78-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.78-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       check_nulls-bottom.2-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-mat10.2-0 ; 
       check_nulls-mat11.2-0 ; 
       check_nulls-mat12.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-mat8.2-0 ; 
       check_nulls-mat9.2-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-wing.2-0 ; 
       new_nulls-mat14.1-0 ; 
       new_nulls-mat15.1-0 ; 
       new_nulls-mat16.1-0 ; 
       new_nulls-mat17.1-0 ; 
       new_nulls-mat18.1-0 ; 
       tweak-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.2-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-new_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d19.2-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d20.2-0 ; 
       check_nulls-t2d21.2-0 ; 
       check_nulls-t2d22.2-0 ; 
       check_nulls-t2d23.2-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d25.2-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 0 110 ; 
       2 1 110 ; 
       12 17 110 ; 
       15 16 110 ; 
       26 4 110 ; 
       11 0 110 ; 
       14 0 110 ; 
       0 1 110 ; 
       3 7 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       16 27 110 ; 
       17 27 110 ; 
       18 3 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       25 0 110 ; 
       27 0 110 ; 
       24 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 18 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 26 300 ; 
       4 12 300 ; 
       6 16 300 ; 
       8 15 300 ; 
       9 10 300 ; 
       10 11 300 ; 
       16 13 300 ; 
       17 14 300 ; 
       18 9 300 ; 
       19 24 300 ; 
       20 23 300 ; 
       21 25 300 ; 
       22 22 300 ; 
       23 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 18 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 17 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 16 401 ; 
       13 10 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 7 401 ; 
       18 3 401 ; 
       19 20 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -8 0 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 35 -4 0 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 MPRFLG 0 ; 
       21 SCHEM 45 -4 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 30 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
