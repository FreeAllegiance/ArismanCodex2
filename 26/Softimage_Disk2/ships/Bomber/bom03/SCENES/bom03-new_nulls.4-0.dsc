SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_nulls-bom03_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.85-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.85-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       check_nulls-bottom.3-0 ; 
       check_nulls-cockpit_side.3-0 ; 
       check_nulls-cockpit_top.3-0 ; 
       check_nulls-engine1.3-0 ; 
       check_nulls-exterior_intake_duct1.3-0 ; 
       check_nulls-glows.3-0 ; 
       check_nulls-inside_landing_bays.3-0 ; 
       check_nulls-intake_ducts_top-n-bottom.3-0 ; 
       check_nulls-intake1.3-0 ; 
       check_nulls-mat10.3-0 ; 
       check_nulls-mat11.3-0 ; 
       check_nulls-mat12.3-0 ; 
       check_nulls-mat13.3-0 ; 
       check_nulls-mat6.3-0 ; 
       check_nulls-mat7.3-0 ; 
       check_nulls-mat8.3-0 ; 
       check_nulls-mat9.3-0 ; 
       check_nulls-Rear.3-0 ; 
       check_nulls-side_glow1.3-0 ; 
       check_nulls-top.3-0 ; 
       check_nulls-wing.3-0 ; 
       new_nulls-mat14.2-0 ; 
       new_nulls-mat15.2-0 ; 
       new_nulls-mat16.2-0 ; 
       new_nulls-mat17.2-0 ; 
       new_nulls-mat18.2-0 ; 
       tweak-mat3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.8-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gear_root.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-large_strut.1-0 ; 
       check_nulls-LL0.1-0 ; 
       check_nulls-LLf.1-0 ; 
       check_nulls-LLr.1-0 ; 
       check_nulls-LLr1.1-0 ; 
       check_nulls-lsmoke.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rsmoke.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-small_strut.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-new_nulls.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       check_nulls-t2d10.4-0 ; 
       check_nulls-t2d11.4-0 ; 
       check_nulls-t2d12.4-0 ; 
       check_nulls-t2d13.4-0 ; 
       check_nulls-t2d14.4-0 ; 
       check_nulls-t2d15.4-0 ; 
       check_nulls-t2d16.3-0 ; 
       check_nulls-t2d17.4-0 ; 
       check_nulls-t2d18.4-0 ; 
       check_nulls-t2d19.3-0 ; 
       check_nulls-t2d2_1.3-0 ; 
       check_nulls-t2d20.3-0 ; 
       check_nulls-t2d21.3-0 ; 
       check_nulls-t2d22.3-0 ; 
       check_nulls-t2d23.3-0 ; 
       check_nulls-t2d24.4-0 ; 
       check_nulls-t2d25.3-0 ; 
       check_nulls-t2d4.4-0 ; 
       check_nulls-t2d5.4-0 ; 
       check_nulls-t2d6.4-0 ; 
       check_nulls-t2d7.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 1 110 ; 
       0 1 110 ; 
       2 1 110 ; 
       3 7 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       12 1 110 ; 
       13 19 110 ; 
       14 0 110 ; 
       16 1 110 ; 
       17 18 110 ; 
       18 29 110 ; 
       19 29 110 ; 
       20 3 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 1 110 ; 
       27 0 110 ; 
       28 4 110 ; 
       29 0 110 ; 
       15 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 18 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 26 300 ; 
       4 12 300 ; 
       6 16 300 ; 
       8 15 300 ; 
       9 10 300 ; 
       10 11 300 ; 
       18 13 300 ; 
       19 14 300 ; 
       20 9 300 ; 
       21 24 300 ; 
       22 23 300 ; 
       23 25 300 ; 
       24 22 300 ; 
       25 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 18 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 17 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 16 401 ; 
       13 10 401 ; 
       14 6 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 7 401 ; 
       18 3 401 ; 
       19 20 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 53.75 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 48.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 28.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 8.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       21 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       23 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       26 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 11.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30 -4 0 MPRFLG 0 ; 
       15 SCHEM 51.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 30 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
