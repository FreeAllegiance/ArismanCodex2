SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03_1.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.55-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.55-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       oct_5_Final-bottom.1-0 ; 
       oct_5_Final-cockpit_side.1-0 ; 
       oct_5_Final-cockpit_top.1-0 ; 
       oct_5_Final-engine1.1-0 ; 
       oct_5_Final-exterior_intake_duct1.1-0 ; 
       oct_5_Final-glows.1-0 ; 
       oct_5_Final-inside_landing_bays.1-0 ; 
       oct_5_Final-intake_ducts_top-n-bottom.1-0 ; 
       oct_5_Final-intake1.1-0 ; 
       oct_5_Final-mat10.1-0 ; 
       oct_5_Final-mat11.1-0 ; 
       oct_5_Final-mat12.1-0 ; 
       oct_5_Final-mat6.1-0 ; 
       oct_5_Final-mat7.1-0 ; 
       oct_5_Final-mat8.1-0 ; 
       oct_5_Final-mat9.1-0 ; 
       oct_5_Final-Rear.1-0 ; 
       oct_5_Final-side_glow1.1-0 ; 
       oct_5_Final-top.1-0 ; 
       oct_5_Final-wing.1-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       bom03-body.1-0 ; 
       bom03-bom03_1.24-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-front_laser.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lengine.5-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-LLr1.1-0 ; 
       bom03-lwepemt1.8-0 ; 
       bom03-lwepemt2.8-0 ; 
       bom03-lwepemt3.8-0 ; 
       bom03-lwepemt4.8-0 ; 
       bom03-rengine.5-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz4.1-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.19-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-oct_5_Final.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       oct_5_Final-t2d10.1-0 ; 
       oct_5_Final-t2d11.1-0 ; 
       oct_5_Final-t2d12.1-0 ; 
       oct_5_Final-t2d13.1-0 ; 
       oct_5_Final-t2d14.1-0 ; 
       oct_5_Final-t2d15.1-0 ; 
       oct_5_Final-t2d16.1-0 ; 
       oct_5_Final-t2d17.1-0 ; 
       oct_5_Final-t2d18.1-0 ; 
       oct_5_Final-t2d19.1-0 ; 
       oct_5_Final-t2d2_1.1-0 ; 
       oct_5_Final-t2d20.1-0 ; 
       oct_5_Final-t2d21.1-0 ; 
       oct_5_Final-t2d22.1-0 ; 
       oct_5_Final-t2d23.1-0 ; 
       oct_5_Final-t2d24.1-0 ; 
       oct_5_Final-t2d4.1-0 ; 
       oct_5_Final-t2d5.1-0 ; 
       oct_5_Final-t2d6.1-0 ; 
       oct_5_Final-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 22 110 ; 
       15 22 110 ; 
       16 0 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 3 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 22 110 ; 
       28 21 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 19 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 17 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 20 300 ; 
       6 15 300 ; 
       9 14 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       21 12 300 ; 
       22 13 300 ; 
       23 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 17 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 16 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 10 401 ; 
       13 6 401 ; 
       14 9 401 ; 
       15 11 401 ; 
       16 7 401 ; 
       17 3 401 ; 
       18 19 401 ; 
       19 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 65 -4 0 MPRFLG 0 ; 
       3 SCHEM 19.44294 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 21.94294 -8 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 16.94294 -8 0 MPRFLG 0 ; 
       10 SCHEM 14.44294 -6 0 MPRFLG 0 ; 
       11 SCHEM 11.94294 -8.45996 0 USR MPRFLG 0 ; 
       12 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 35 -8 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 MPRFLG 0 ; 
       16 SCHEM 10 -4 0 MPRFLG 0 ; 
       17 SCHEM 60 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 MPRFLG 0 ; 
       19 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 55 -8 0 MPRFLG 0 ; 
       21 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 19.44294 -8 0 MPRFLG 0 ; 
       24 SCHEM 25 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 30 -4 0 MPRFLG 0 ; 
       27 SCHEM 45 -8 0 MPRFLG 0 ; 
       28 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.191502 -5.284634 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19.44294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14.44294 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.94294 -10.45996 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.94294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.94294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.94294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.94294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19.44294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14.44294 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.94294 -12.45996 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 99 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
