SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       oct_5_Final-null1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.59-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.59-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       oct_5_Final-bottom.1-0 ; 
       oct_5_Final-cockpit_side.1-0 ; 
       oct_5_Final-cockpit_top.1-0 ; 
       oct_5_Final-engine1.1-0 ; 
       oct_5_Final-exterior_intake_duct1.1-0 ; 
       oct_5_Final-glows.1-0 ; 
       oct_5_Final-inside_landing_bays.1-0 ; 
       oct_5_Final-intake_ducts_top-n-bottom.1-0 ; 
       oct_5_Final-intake1.1-0 ; 
       oct_5_Final-mat10.1-0 ; 
       oct_5_Final-mat11.1-0 ; 
       oct_5_Final-mat12.1-0 ; 
       oct_5_Final-mat6.1-0 ; 
       oct_5_Final-mat7.1-0 ; 
       oct_5_Final-mat8.1-0 ; 
       oct_5_Final-mat9.1-0 ; 
       oct_5_Final-Rear.1-0 ; 
       oct_5_Final-side_glow1.1-0 ; 
       oct_5_Final-top.1-0 ; 
       oct_5_Final-wing.1-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       oct_5_Final-body.1-0 ; 
       oct_5_Final-bom03_1.27-0 ; 
       oct_5_Final-cockpt.1-0 ; 
       oct_5_Final-front_gear_root.1-0 ; 
       oct_5_Final-front_gun.1-0 ; 
       oct_5_Final-fwepmnt.1-0 ; 
       oct_5_Final-large_strut.1-0 ; 
       oct_5_Final-lengine.5-0 ; 
       oct_5_Final-LL0.1-0 ; 
       oct_5_Final-LLf.1-0 ; 
       oct_5_Final-LLr.1-0 ; 
       oct_5_Final-LLr1.1-0 ; 
       oct_5_Final-lwepemt1.8-0 ; 
       oct_5_Final-lwepemt2.8-0 ; 
       oct_5_Final-lwepemt3.8-0 ; 
       oct_5_Final-lwepemt4.8-0 ; 
       oct_5_Final-null1.1-0 ROOT ; 
       oct_5_Final-rengine.5-0 ; 
       oct_5_Final-rwepemt1.1-0 ; 
       oct_5_Final-rwepemt2.1-0 ; 
       oct_5_Final-rwepemt3.1-0 ; 
       oct_5_Final-rwepemt4.1-0 ; 
       oct_5_Final-rwingzz.2-0 ; 
       oct_5_Final-rwingzz4.1-0 ; 
       oct_5_Final-small_strut.1-0 ; 
       oct_5_Final-SSal.1-0 ; 
       oct_5_Final-SSar.1-0 ; 
       oct_5_Final-SSf.1-0 ; 
       oct_5_Final-SSl.19-0 ; 
       oct_5_Final-SSr.1-0 ; 
       oct_5_Final-tturatt.1-0 ; 
       oct_5_Final-wepemt.1-0 ; 
       oct_5_Final-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-oct_5_Final.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       oct_5_Final-t2d10.1-0 ; 
       oct_5_Final-t2d11.1-0 ; 
       oct_5_Final-t2d12.1-0 ; 
       oct_5_Final-t2d13.1-0 ; 
       oct_5_Final-t2d14.1-0 ; 
       oct_5_Final-t2d15.1-0 ; 
       oct_5_Final-t2d16.1-0 ; 
       oct_5_Final-t2d17.1-0 ; 
       oct_5_Final-t2d18.1-0 ; 
       oct_5_Final-t2d19.1-0 ; 
       oct_5_Final-t2d2_1.1-0 ; 
       oct_5_Final-t2d20.1-0 ; 
       oct_5_Final-t2d21.1-0 ; 
       oct_5_Final-t2d22.1-0 ; 
       oct_5_Final-t2d23.1-0 ; 
       oct_5_Final-t2d24.1-0 ; 
       oct_5_Final-t2d4.1-0 ; 
       oct_5_Final-t2d5.1-0 ; 
       oct_5_Final-t2d6.1-0 ; 
       oct_5_Final-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 16 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       5 0 110 ; 
       6 3 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       17 0 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 32 110 ; 
       23 32 110 ; 
       24 3 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 23 110 ; 
       29 22 110 ; 
       30 0 110 ; 
       31 4 110 ; 
       32 0 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 19 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 17 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 20 300 ; 
       6 15 300 ; 
       9 14 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       22 12 300 ; 
       23 13 300 ; 
       24 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 17 401 ; 
       3 8 401 ; 
       4 15 401 ; 
       5 16 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 10 401 ; 
       13 6 401 ; 
       14 9 401 ; 
       15 11 401 ; 
       16 7 401 ; 
       17 3 401 ; 
       18 19 401 ; 
       19 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 MPRFLG 0 ; 
       3 SCHEM 14.44294 -10 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 16.94294 -12 0 MPRFLG 0 ; 
       7 SCHEM 0 -8 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 11.94294 -12 0 MPRFLG 0 ; 
       10 SCHEM 9.44294 -10 0 MPRFLG 0 ; 
       11 SCHEM 6.94294 -12.45996 0 USR MPRFLG 0 ; 
       12 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 30 -12 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 35 -12 0 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 MPRFLG 0 ; 
       18 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       20 SCHEM 45 -12 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       22 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 35 -10 0 MPRFLG 0 ; 
       24 SCHEM 14.44294 -12 0 MPRFLG 0 ; 
       25 SCHEM 20 -8 0 MPRFLG 0 ; 
       26 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 25 -8 0 MPRFLG 0 ; 
       28 SCHEM 40 -12 0 MPRFLG 0 ; 
       29 SCHEM 50 -12 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 2.518787 -10.83239 0 USR MPRFLG 0 ; 
       32 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 4.553707 -9.837687 0 USR DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 13.44294 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 8.44294 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5.94294 -14.45996 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10.94294 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15.94294 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10.94294 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15.94294 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 13.44294 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 8.44294 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 5.94294 -16.45996 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
