SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03.100-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.25-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       remodel-bottom.6-0 ; 
       remodel-cockpit_side.6-0 ; 
       remodel-cockpit_top.6-0 ; 
       remodel-engine1.1-0 ; 
       remodel-exterior_intake_duct1.6-0 ; 
       remodel-glows.6-0 ; 
       remodel-inside_landing_bays.6-0 ; 
       remodel-intake_ducts_top-n-bottom.4-0 ; 
       remodel-intake1.6-0 ; 
       remodel-mat10.1-0 ; 
       remodel-mat11.1-0 ; 
       remodel-mat6.2-0 ; 
       remodel-mat7.2-0 ; 
       remodel-mat8.1-0 ; 
       remodel-mat9.1-0 ; 
       remodel-Rear.4-0 ; 
       remodel-side_glow1.6-0 ; 
       remodel-top.6-0 ; 
       remodel-wing.6-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       bom03-body.1-0 ; 
       bom03-bom03.87-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lengine.1-0 ROOT ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.8-0 ; 
       bom03-lwepemt2.8-0 ; 
       bom03-lwepemt3.8-0 ; 
       bom03-lwepemt4.8-0 ; 
       bom03-rengine.1-0 ROOT ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz4.1-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.19-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-remodel.28-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       remodel-t2d10.8-0 ; 
       remodel-t2d11.8-0 ; 
       remodel-t2d12.8-0 ; 
       remodel-t2d13.8-0 ; 
       remodel-t2d14.8-0 ; 
       remodel-t2d15.8-0 ; 
       remodel-t2d16.2-0 ; 
       remodel-t2d17.3-0 ; 
       remodel-t2d18.2-0 ; 
       remodel-t2d19.1-0 ; 
       remodel-t2d2_1.1-0 ; 
       remodel-t2d20.1-0 ; 
       remodel-t2d21.1-0 ; 
       remodel-t2d22.3-0 ; 
       remodel-t2d4.8-0 ; 
       remodel-t2d5.8-0 ; 
       remodel-t2d6.8-0 ; 
       remodel-t2d7.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 0 110 ; 
       3 7 110 ; 
       4 0 110 ; 
       5 3 110 ; 
       7 0 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       16 20 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 29 110 ; 
       22 3 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 21 110 ; 
       27 20 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       21 29 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 15 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 18 300 ; 
       0 17 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 16 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 19 300 ; 
       5 14 300 ; 
       8 13 300 ; 
       10 10 300 ; 
       20 11 300 ; 
       22 9 300 ; 
       21 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 8 401 ; 
       0 0 401 ; 
       1 2 401 ; 
       2 15 401 ; 
       5 14 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       15 7 401 ; 
       13 9 401 ; 
       11 10 401 ; 
       12 6 401 ; 
       16 3 401 ; 
       17 17 401 ; 
       18 16 401 ; 
       14 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 45 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 MPRFLG 0 ; 
       4 SCHEM 0 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 0 -14 0 SRT 0.9999998 0.9999999 0.9999999 3.141593 0 -2.594441e-017 0.7125049 0.05340566 -2.038682 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 25 -10 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 30 -10 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -14 0 SRT 0.9999998 0.9999999 0.9999999 3.141593 -6.93892e-018 2.594441e-017 -0.712505 0.05340566 -2.038683 MPRFLG 0 ; 
       16 SCHEM 50 -10 0 MPRFLG 0 ; 
       17 SCHEM 40 -10 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 MPRFLG 0 ; 
       20 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       22 SCHEM 10 -10 0 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 20 -6 0 MPRFLG 0 ; 
       26 SCHEM 35 -10 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       21 SCHEM 31.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 92.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 100 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 117.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 107.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 115 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 120 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 89 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 18 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
