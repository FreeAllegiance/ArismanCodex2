SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       bom03-bom03.77-0 ; 
       bom03-SSl.41-0 ; 
       bom04-rwingzz0.2-0 ; 
       remodel-rwingzz4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       remodel-bottom.2-0 ; 
       remodel-cockpit_side.2-0 ; 
       remodel-cockpit_top.2-0 ; 
       remodel-exterior_intake_duct1.2-0 ; 
       remodel-glows.2-0 ; 
       remodel-inside_landing_bays.2-0 ; 
       remodel-intake_ducts1.2-0 ; 
       remodel-intake1.2-0 ; 
       remodel-mat4.2-0 ; 
       remodel-mat6.2-0 ; 
       remodel-mat7.2-0 ; 
       remodel-side_glow1.2-0 ; 
       remodel-top.2-0 ; 
       remodel-wing.2-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       bom03-body.1-0 ; 
       bom03-bom03.66-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.8-0 ROOT ; 
       bom03-lwepemt2.8-0 ROOT ; 
       bom03-lwepemt3.8-0 ROOT ; 
       bom03-lwepemt4.8-0 ROOT ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.19-0 ROOT ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
       bom04-rwingzz0.1-0 ROOT ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-SSr1.1-0 ; 
       remodel-rwingzz4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-remodel.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       remodel-t2d10.2-0 ; 
       remodel-t2d11.2-0 ; 
       remodel-t2d12.2-0 ; 
       remodel-t2d13.2-0 ; 
       remodel-t2d14.2-0 ; 
       remodel-t2d15.2-0 ; 
       remodel-t2d16.2-0 ; 
       remodel-t2d2.2-0 ; 
       remodel-t2d4.2-0 ; 
       remodel-t2d5.2-0 ; 
       remodel-t2d6.2-0 ; 
       remodel-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       4 0 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 30 110 ; 
       23 3 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       28 22 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       32 31 110 ; 
       33 32 110 ; 
       34 33 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 4 300 ; 
       0 2 300 ; 
       0 13 300 ; 
       0 12 300 ; 
       0 0 300 ; 
       0 5 300 ; 
       0 1 300 ; 
       0 11 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 3 300 ; 
       1 14 300 ; 
       22 9 300 ; 
       35 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       27 1 15000 ; 
       35 3 15000 ; 
       31 2 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 9 401 ; 
       4 8 401 ; 
       5 1 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       9 7 401 ; 
       10 6 401 ; 
       11 3 401 ; 
       12 11 401 ; 
       13 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 38.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       3 SCHEM 35.09199 -7.9974 0 USR MPRFLG 0 ; 
       4 SCHEM 30 -6 0 MPRFLG 0 ; 
       5 SCHEM 37.59199 -9.9974 0 MPRFLG 0 ; 
       6 SCHEM 0 -6 0 MPRFLG 0 ; 
       7 SCHEM 0 -8 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 35.09199 -9.9974 0 MPRFLG 0 ; 
       10 SCHEM 40.09199 -8 0 MPRFLG 0 ; 
       11 SCHEM 42.59199 -8 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -14 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.469012 0.1655194 0.03320503 MPRFLG 0 ; 
       13 SCHEM 30 -14 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.728187 0.1655194 0.04909849 MPRFLG 0 ; 
       14 SCHEM 27.5 -14 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.972691 0.1655194 0.1187822 MPRFLG 0 ; 
       15 SCHEM 35 -14 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 2.220863 0.1655199 0.1872434 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 5 -10 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 10 -10 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 32.59199 -9.9974 0 MPRFLG 0 ; 
       24 SCHEM 20 -6 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -14 0 SRT 0.1319899 0.1319899 0.1319899 -1.570796 3.141593 1.293485e-007 3.250528 0.1616147 -2.029068 MPRFLG 0 ; 
       28 SCHEM 15 -10 0 MPRFLG 0 ; 
       29 SCHEM 25 -6 0 MPRFLG 0 ; 
       30 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       35 SCHEM 80 0 0 DISPLAY 1 2 SRT 0.9999998 0.9999999 1 0 0 0 0 0.1630181 -0.5624125 MPRFLG 0 ; 
       31 SCHEM 12.5 -14 0 DISPLAY 0 0 SRT 0.6531264 0.6531264 0.8650263 -0.13 4.246055e-009 1.570796 -0.978892 0.16609 -0.5528765 MPRFLG 0 ; 
       32 SCHEM 12.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 3.75 -18 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 0 -20 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 76.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 80 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
