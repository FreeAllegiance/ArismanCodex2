SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03.78-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       remodel-bottom.2-0 ; 
       remodel-cockpit_side.2-0 ; 
       remodel-cockpit_top.2-0 ; 
       remodel-exterior_intake_duct1.2-0 ; 
       remodel-glows.2-0 ; 
       remodel-inside_landing_bays.2-0 ; 
       remodel-intake_ducts1.2-0 ; 
       remodel-intake1.2-0 ; 
       remodel-mat1.1-0 ; 
       remodel-mat2.1-0 ; 
       remodel-mat3.1-0 ; 
       remodel-mat4.2-0 ; 
       remodel-mat57.1-0 ; 
       remodel-mat58.1-0 ; 
       remodel-mat59.1-0 ; 
       remodel-mat6.2-0 ; 
       remodel-mat60.1-0 ; 
       remodel-mat61.1-0 ; 
       remodel-mat62.1-0 ; 
       remodel-mat63.1-0 ; 
       remodel-mat7.2-0 ; 
       remodel-side_glow1.2-0 ; 
       remodel-top.2-0 ; 
       remodel-wing.2-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       bom03-afinzzz.1-0 ; 
       bom03-afinzzz1.1-0 ; 
       bom03-body.1-0 ; 
       bom03-bom03.67-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.8-0 ; 
       bom03-lwepemt2.8-0 ; 
       bom03-lwepemt3.8-0 ; 
       bom03-lwepemt4.8-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz4.1-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.19-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-remodel.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       remodel-t2d1.1-0 ; 
       remodel-t2d10.2-0 ; 
       remodel-t2d11.2-0 ; 
       remodel-t2d12.2-0 ; 
       remodel-t2d13.2-0 ; 
       remodel-t2d14.2-0 ; 
       remodel-t2d15.2-0 ; 
       remodel-t2d16.2-0 ; 
       remodel-t2d2.3-0 ; 
       remodel-t2d2_1.1-0 ; 
       remodel-t2d4.2-0 ; 
       remodel-t2d5.2-0 ; 
       remodel-t2d57.1-0 ; 
       remodel-t2d58.1-0 ; 
       remodel-t2d59.1-0 ; 
       remodel-t2d6.2-0 ; 
       remodel-t2d60.1-0 ; 
       remodel-t2d61.1-0 ; 
       remodel-t2d62.1-0 ; 
       remodel-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 3 110 ; 
       4 2 110 ; 
       5 9 110 ; 
       6 2 110 ; 
       7 5 110 ; 
       1 2 110 ; 
       8 1 110 ; 
       9 2 110 ; 
       10 5 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 23 110 ; 
       17 0 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 31 110 ; 
       24 5 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 23 110 ; 
       29 22 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       23 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       2 11 300 ; 
       2 4 300 ; 
       2 2 300 ; 
       2 23 300 ; 
       2 22 300 ; 
       2 0 300 ; 
       2 5 300 ; 
       2 1 300 ; 
       2 21 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 3 300 ; 
       3 24 300 ; 
       1 14 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       22 15 300 ; 
       23 20 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 11 401 ; 
       4 10 401 ; 
       5 2 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       9 0 401 ; 
       15 9 401 ; 
       20 7 401 ; 
       21 4 401 ; 
       22 19 401 ; 
       23 15 401 ; 
       16 14 401 ; 
       17 16 401 ; 
       10 8 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       12 12 401 ; 
       13 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 32.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 40 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 35 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 22.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 30 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 24 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 61.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 61.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 36.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 24 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 61.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
