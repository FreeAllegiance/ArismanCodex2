SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03_1.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.37-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       retexture-light1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       retexture-bottom.1-0 ; 
       retexture-cockpit_side.1-0 ; 
       retexture-cockpit_top.1-0 ; 
       retexture-engine1.1-0 ; 
       retexture-exterior_intake_duct1.1-0 ; 
       retexture-glows.1-0 ; 
       retexture-inside_landing_bays.1-0 ; 
       retexture-intake_ducts_top-n-bottom.1-0 ; 
       retexture-intake1.1-0 ; 
       retexture-mat10.1-0 ; 
       retexture-mat11.1-0 ; 
       retexture-mat12.1-0 ; 
       retexture-mat6.1-0 ; 
       retexture-mat7.1-0 ; 
       retexture-mat8.1-0 ; 
       retexture-mat9.1-0 ; 
       retexture-Rear.1-0 ; 
       retexture-side_glow1.1-0 ; 
       retexture-top.1-0 ; 
       retexture-wing.1-0 ; 
       tweak-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       bom03-body.1-0 ; 
       bom03-bom03_1.8-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lengine.2-0 ROOT ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-LLr1.1-0 ; 
       bom03-lwepemt1.8-0 ; 
       bom03-lwepemt2.8-0 ; 
       bom03-lwepemt3.8-0 ; 
       bom03-lwepemt4.8-0 ; 
       bom03-rengine.2-0 ROOT ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz4.1-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.19-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-retexture.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       retexture-t2d10.4-0 ; 
       retexture-t2d11.4-0 ; 
       retexture-t2d12.4-0 ; 
       retexture-t2d13.4-0 ; 
       retexture-t2d14.4-0 ; 
       retexture-t2d15.4-0 ; 
       retexture-t2d16.1-0 ; 
       retexture-t2d17.4-0 ; 
       retexture-t2d18.4-0 ; 
       retexture-t2d19.1-0 ; 
       retexture-t2d2_1.1-0 ; 
       retexture-t2d20.1-0 ; 
       retexture-t2d21.1-0 ; 
       retexture-t2d22.1-0 ; 
       retexture-t2d23.1-0 ; 
       retexture-t2d4.4-0 ; 
       retexture-t2d5.4-0 ; 
       retexture-t2d6.4-0 ; 
       retexture-t2d7.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 7 110 ; 
       0 1 110 ; 
       2 0 110 ; 
       3 7 110 ; 
       4 0 110 ; 
       5 3 110 ; 
       7 0 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       16 20 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 29 110 ; 
       21 29 110 ; 
       22 3 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 21 110 ; 
       27 20 110 ; 
       28 0 110 ; 
       29 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 11 300 ; 
       0 16 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 19 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 17 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 20 300 ; 
       5 15 300 ; 
       8 14 300 ; 
       9 10 300 ; 
       20 12 300 ; 
       21 13 300 ; 
       22 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 16 401 ; 
       3 8 401 ; 
       5 15 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       12 10 401 ; 
       13 6 401 ; 
       14 9 401 ; 
       15 11 401 ; 
       16 7 401 ; 
       17 3 401 ; 
       18 18 401 ; 
       19 17 401 ; 
       11 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 6.94294 -8.459957 0 USR MPRFLG 0 ; 
       0 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 50 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 MPRFLG 0 ; 
       3 SCHEM 14.44294 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 16.94294 -8 0 MPRFLG 0 ; 
       6 SCHEM 97.5 0 0 SRT 0.9999998 0.9999999 0.9999999 3.141593 0 -2.594441e-017 0.7125049 0.05340566 -2.038682 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 11.94294 -8 0 MPRFLG 0 ; 
       9 SCHEM 9.44294 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 30 -8 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 MPRFLG 0 ; 
       15 SCHEM 100 0 0 SRT 0.9999998 0.9999999 0.9999999 3.141593 -6.93892e-018 2.594441e-017 -0.712505 0.05340566 -2.038683 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 MPRFLG 0 ; 
       20 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 14.44294 -8 0 MPRFLG 0 ; 
       23 SCHEM 20 -4 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 25 -4 0 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 43.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14.44294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9.44294 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.94294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.94294 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.94294 -10.45996 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.94294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.94294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14.44294 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9.44294 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.94294 -12.45996 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 94 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 37 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
