SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.74-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.74-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       check_nulls-bottom.2-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-wing.2-0 ; 
       tweak-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.27-0 ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-lengine.5-0 ; 
       check_nulls-lwepemt1_1.8-0 ; 
       check_nulls-lwepemt2_1.8-0 ; 
       check_nulls-lwepemt3_1.8-0 ; 
       check_nulls-lwepemt4.8-0 ; 
       check_nulls-null1_4.2-0 ROOT ; 
       check_nulls-rengine.5-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwepemt2.1-0 ; 
       check_nulls-rwepemt3.1-0 ; 
       check_nulls-rwepemt4.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d25.2-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 10 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       11 0 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 10 110 ; 
       19 0 110 ; 
       20 3 110 ; 
       21 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 15 300 ; 
       0 14 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 13 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 16 300 ; 
       3 9 300 ; 
       16 10 300 ; 
       17 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 13 401 ; 
       3 8 401 ; 
       4 10 401 ; 
       5 12 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 11 401 ; 
       10 9 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 3 401 ; 
       14 15 401 ; 
       15 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 -6 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 66.7712 -8 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 30 -12 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 50 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 10 -8 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 40 -12 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 45 -12 0 MPRFLG 0 ; 
       16 SCHEM 45 -10 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 0 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 25 -8 0 MPRFLG 0 ; 
       20 SCHEM 5 -12 0 MPRFLG 0 ; 
       21 SCHEM 38.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 82.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 74.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 99.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 72.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 69.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 92.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 94.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 84.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 89.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 79.66129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 77.16129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 82.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 69.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 87.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 89.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 94.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 84.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 99.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 97.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 72.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 74.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 77.16129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79.66129 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
