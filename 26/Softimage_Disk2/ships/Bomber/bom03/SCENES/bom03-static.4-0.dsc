SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.79-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.79-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       check_nulls-bottom.2-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-mat13.2-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-wing.2-0 ; 
       static-mat14.2-0 ; 
       static-mat15.2-0 ; 
       static-mat16.2-0 ; 
       static-mat17.2-0 ; 
       static-mat18.2-0 ; 
       tweak-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.3-0 ROOT ; 
       check_nulls-cockpt.1-0 ; 
       check_nulls-front_gun.1-0 ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-lthrust.1-0 ; 
       check_nulls-lwepemt1.1-0 ; 
       check_nulls-missemt.1-0 ; 
       check_nulls-rthrust.1-0 ; 
       check_nulls-rwepemt1.1-0 ; 
       check_nulls-rwingzz_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-SSal.1-0 ; 
       check_nulls-SSar.1-0 ; 
       check_nulls-SSf.1-0 ; 
       check_nulls-SSl.19-0 ; 
       check_nulls-SSr.1-0 ; 
       check_nulls-trail.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wepemt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d25.2-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 0 110 ; 
       2 1 110 ; 
       6 11 110 ; 
       9 10 110 ; 
       19 3 110 ; 
       5 0 110 ; 
       8 0 110 ; 
       0 1 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       18 0 110 ; 
       20 0 110 ; 
       17 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 15 300 ; 
       0 14 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 13 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 21 300 ; 
       3 9 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       12 19 300 ; 
       13 18 300 ; 
       14 20 300 ; 
       15 17 300 ; 
       16 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 13 401 ; 
       3 8 401 ; 
       4 10 401 ; 
       5 12 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 11 401 ; 
       10 9 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 3 401 ; 
       14 15 401 ; 
       15 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 47.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 32.5 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 27.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 35 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 40 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 45 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 37.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       18 SCHEM 27.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 31.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       17 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       16 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
