SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.81-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.81-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       check_nulls-bottom.2-0 ; 
       check_nulls-cockpit_side.2-0 ; 
       check_nulls-cockpit_top.2-0 ; 
       check_nulls-engine1.2-0 ; 
       check_nulls-exterior_intake_duct1.2-0 ; 
       check_nulls-glows.2-0 ; 
       check_nulls-inside_landing_bays.2-0 ; 
       check_nulls-intake_ducts_top-n-bottom.2-0 ; 
       check_nulls-intake1.2-0 ; 
       check_nulls-mat6.2-0 ; 
       check_nulls-mat7.2-0 ; 
       check_nulls-Rear.2-0 ; 
       check_nulls-side_glow1.2-0 ; 
       check_nulls-top.2-0 ; 
       check_nulls-wing.2-0 ; 
       tweak-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       check_nulls-body.1-0 ; 
       check_nulls-bom03_1.4-0 ROOT ; 
       check_nulls-fwepmnt.1-0 ; 
       check_nulls-rwingzz_1_1.2-0 ; 
       check_nulls-rwingzz4.1-0 ; 
       check_nulls-tturatt.1-0 ; 
       check_nulls-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       check_nulls-t2d10.3-0 ; 
       check_nulls-t2d11.3-0 ; 
       check_nulls-t2d12.3-0 ; 
       check_nulls-t2d13.3-0 ; 
       check_nulls-t2d14.3-0 ; 
       check_nulls-t2d15.3-0 ; 
       check_nulls-t2d16.2-0 ; 
       check_nulls-t2d17.3-0 ; 
       check_nulls-t2d18.3-0 ; 
       check_nulls-t2d2_1.2-0 ; 
       check_nulls-t2d24.3-0 ; 
       check_nulls-t2d4.3-0 ; 
       check_nulls-t2d5.3-0 ; 
       check_nulls-t2d6.3-0 ; 
       check_nulls-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 0 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       0 5 300 ; 
       0 2 300 ; 
       0 14 300 ; 
       0 13 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 1 300 ; 
       0 12 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       1 15 300 ; 
       3 9 300 ; 
       4 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 12 401 ; 
       3 8 401 ; 
       4 10 401 ; 
       5 11 401 ; 
       6 1 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 9 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 3 401 ; 
       13 14 401 ; 
       14 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
