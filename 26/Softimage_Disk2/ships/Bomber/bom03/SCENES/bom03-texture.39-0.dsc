SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       bom03-bom03.72-0 ; 
       bom03-SSl.36-0 ; 
       texture-rwingzz3.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tweak-cam_int1.56-0 ROOT ; 
       tweak-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       texture-bottom.8-0 ; 
       texture-cockpit_side.8-0 ; 
       texture-cockpit_top.8-0 ; 
       texture-exterior_intake_duct1.1-0 ; 
       texture-glows.8-0 ; 
       texture-inside_landing_bays.8-0 ; 
       texture-intake_ducts1.6-0 ; 
       texture-intake1.5-0 ; 
       texture-mat4.19-0 ; 
       texture-mat6.2-0 ; 
       texture-mat7.3-0 ; 
       texture-side_glow1.7-0 ; 
       texture-top.8-0 ; 
       texture-wing.8-0 ; 
       tweak-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       bom03-body.1-0 ; 
       bom03-bom03.63-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.6-0 ROOT ; 
       bom03-lwepemt2.6-0 ROOT ; 
       bom03-lwepemt3.6-0 ROOT ; 
       bom03-lwepemt4.6-0 ROOT ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.17-0 ROOT ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
       texture-rwingzz3.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom03/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-texture.39-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       texture-t2d10.11-0 ; 
       texture-t2d11.10-0 ; 
       texture-t2d12.9-0 ; 
       texture-t2d13.7-0 ; 
       texture-t2d14.6-0 ; 
       texture-t2d15.5-0 ; 
       texture-t2d16.2-0 ; 
       texture-t2d2.4-0 ; 
       texture-t2d4.18-0 ; 
       texture-t2d5.16-0 ; 
       texture-t2d6.14-0 ; 
       texture-t2d7.13-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       4 0 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 30 110 ; 
       23 3 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       28 22 110 ; 
       29 0 110 ; 
       30 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 4 300 ; 
       0 2 300 ; 
       0 13 300 ; 
       0 12 300 ; 
       0 0 300 ; 
       0 5 300 ; 
       0 1 300 ; 
       0 11 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 3 300 ; 
       1 14 300 ; 
       22 9 300 ; 
       31 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       27 1 15000 ; 
       31 2 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 9 401 ; 
       4 8 401 ; 
       5 1 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       9 7 401 ; 
       10 6 401 ; 
       11 3 401 ; 
       12 11 401 ; 
       13 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 37.59199 -5.9974 0 USR MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 40.09199 -7.9974 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -4 0 MPRFLG 0 ; 
       9 SCHEM 37.59199 -7.9974 0 MPRFLG 0 ; 
       10 SCHEM 42.59199 -6 0 MPRFLG 0 ; 
       11 SCHEM 45.09199 -6 0 MPRFLG 0 ; 
       12 SCHEM 97.5 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.469012 0.1655194 0.03320503 MPRFLG 0 ; 
       13 SCHEM 95 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.728187 0.1655194 0.04909849 MPRFLG 0 ; 
       14 SCHEM 92.5 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 1.972691 0.1655194 0.1187822 MPRFLG 0 ; 
       15 SCHEM 90 0 0 SRT 0.9999999 1 1 -4.768372e-007 8.204153e-028 2.400892e-021 2.220863 0.1655199 0.1872434 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 15 -8 0 MPRFLG 0 ; 
       22 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 35.09199 -7.9974 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 25 -4 0 MPRFLG 0 ; 
       26 SCHEM 5 -4 0 MPRFLG 0 ; 
       27 SCHEM 87.5 0 0 SRT 0.263884 0.263884 0.263884 -1.570796 3.141593 1.293485e-007 3.725748 0.1616147 -2.547819 MPRFLG 0 ; 
       28 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 83.75 0 0 SRT 0.9999998 0.9999999 1 0 0 0 0 0.1630181 -0.5624125 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 100 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 77.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 55 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
