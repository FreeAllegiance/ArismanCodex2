SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       bom03-bom03.21-0 ; 
       bom03-lwingzz.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tweak-cam_int1.5-0 ROOT ; 
       tweak-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       tweak-mat3.1-0 ; 
       tweak-mat4.1-0 ; 
       tweak_anim-mat63.1-0 ; 
       tweak_anim-mat64.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       bom03-bmerge1.1-0 ; 
       bom03-bom03.20-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-fuselg.1-0 ROOT ; 
       bom03-fwepmnt.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.1-0 ; 
       bom03-lwepemt2.1-0 ; 
       bom03-lwepemt3.1-0 ; 
       bom03-lwepemt4.1-0 ; 
       bom03-lwingzz.10-0 ROOT ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz2.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.1-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
       bom04-ffuselg.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-tweak_anim.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 30 110 ; 
       0 1 110 ; 
       23 30 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 15 110 ; 
       28 22 110 ; 
       29 0 110 ; 
       30 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       3 1 300 ; 
       31 2 300 ; 
       31 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       15 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 26.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 MPRFLG 0 ; 
       3 SCHEM 20 -14 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 MPRFLG 0 ; 
       5 SCHEM 0 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 35 -10 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 0 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 6.25 -14 0 DISPLAY 0 0 SRT 1 1 1 -1.570796 0 0 0 0.163018 -0.5624124 MPRFLG 0 ; 
       16 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 10 -12 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 15 -12 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       0 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 10 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 25 -8 0 MPRFLG 0 ; 
       30 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       31 SCHEM 16.25 -14 0 DISPLAY 0 0 SRT 1 1 1 6.283185 -1.748456e-007 -3.088981e-020 -1.303873e-007 -4.861334e-022 3.287629 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 50 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
