SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tweak-cam_int1.14-0 ROOT ; 
       tweak-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       tweak-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       bom03-body.1-0 ; 
       bom03-bom03.28-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-front_gear_root.1-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-large_strut.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.1-0 ; 
       bom03-lwepemt2.1-0 ; 
       bom03-lwepemt3.1-0 ; 
       bom03-lwepemt4.1-0 ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz2.1-0 ; 
       bom03-small_strut.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.1-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03-tweak_landing_gear.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       4 0 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 0 110 ; 
       9 3 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 31 110 ; 
       0 1 110 ; 
       23 31 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 23 110 ; 
       29 22 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       24 3 110 ; 
       5 3 110 ; 
       3 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 30 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -8 0 MPRFLG 0 ; 
       7 SCHEM 0 -10 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 45.09199 -11.9974 0 MPRFLG 0 ; 
       10 SCHEM 50.09199 -10 0 MPRFLG 0 ; 
       11 SCHEM 52.59199 -10 0 MPRFLG 0 ; 
       12 SCHEM 10 -12 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 5 -12 0 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 20 -12 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 25 -12 0 MPRFLG 0 ; 
       22 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       0 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 MPRFLG 0 ; 
       25 SCHEM 30 -8 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 15 -12 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 MPRFLG 0 ; 
       31 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       24 SCHEM 42.59199 -11.9974 0 MPRFLG 0 ; 
       5 SCHEM 47.59199 -11.9974 0 MPRFLG 0 ; 
       3 SCHEM 45.09199 -9.9974 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 57.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 55 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
