SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       bom03-bom03.13-0 ; 
       bom03-lwingzz.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       body-cam_int1.7-0 ROOT ; 
       body-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       body-mat3.1-0 ; 
       body-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       bom03-bom03.13-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-fuselg.10-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.1-0 ; 
       bom03-lwepemt2.1-0 ; 
       bom03-lwepemt3.1-0 ; 
       bom03-lwepemt4.1-0 ; 
       bom03-lwingzz.4-0 ROOT ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-rwingzz1.1-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.1-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03_final-body.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       22 29 110 ; 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       15 2 110 ; 
       16 15 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       21 29 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 14 110 ; 
       27 21 110 ; 
       28 2 110 ; 
       29 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       2 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
       14 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       22 SCHEM 5 -6 0 MPRFLG 0 ; 
       0 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 35.63409 -6 0 USR MPRFLG 0 ; 
       8 SCHEM 37.75103 -6 0 USR MPRFLG 0 ; 
       9 SCHEM 40.25103 -6 0 MPRFLG 0 ; 
       10 SCHEM 11.09409 -15.13838 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 13.59409 -15.13838 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 8.594092 -15.13838 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 6.094092 -15.13838 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 11.09409 -13.13838 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.163018 -0.5624124 MPRFLG 0 ; 
       15 SCHEM 20 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -6 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 10 -8 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 15 -8 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 30 -4 0 MPRFLG 0 ; 
       25 SCHEM 25 -4 0 MPRFLG 0 ; 
       26 SCHEM 16.09409 -15.13838 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 11.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 55 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
