SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom03-bom03.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       onyx_figbom_F-cam_int1.1-0 ROOT ; 
       onyx_figbom_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       onyx_figbom_F-inf_light1_1.1-0 ROOT ; 
       onyx_figbom_F-inf_light2_1.1-0 ROOT ; 
       onyx_figbom_F-light3_1.1-0 ROOT ; 
       onyx_figbom_F-spot1.1-0 ; 
       onyx_figbom_F-spot1_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       onyx_figbom_F-default1.1-0 ; 
       onyx_figbom_F-default3.1-0 ; 
       onyx_figbom_F-mat1.1-0 ; 
       onyx_figbom_F-mat10.1-0 ; 
       onyx_figbom_F-mat11.1-0 ; 
       onyx_figbom_F-mat12.1-0 ; 
       onyx_figbom_F-mat13.1-0 ; 
       onyx_figbom_F-mat14.1-0 ; 
       onyx_figbom_F-mat15.1-0 ; 
       onyx_figbom_F-mat16.1-0 ; 
       onyx_figbom_F-mat17.1-0 ; 
       onyx_figbom_F-mat18.1-0 ; 
       onyx_figbom_F-mat19.1-0 ; 
       onyx_figbom_F-mat2.1-0 ; 
       onyx_figbom_F-mat20.1-0 ; 
       onyx_figbom_F-mat21.1-0 ; 
       onyx_figbom_F-mat22.1-0 ; 
       onyx_figbom_F-mat23.1-0 ; 
       onyx_figbom_F-mat24.1-0 ; 
       onyx_figbom_F-mat25.1-0 ; 
       onyx_figbom_F-mat26.1-0 ; 
       onyx_figbom_F-mat27.1-0 ; 
       onyx_figbom_F-mat28.1-0 ; 
       onyx_figbom_F-mat29.1-0 ; 
       onyx_figbom_F-mat3.1-0 ; 
       onyx_figbom_F-mat30.1-0 ; 
       onyx_figbom_F-mat31.1-0 ; 
       onyx_figbom_F-mat32.1-0 ; 
       onyx_figbom_F-mat33.1-0 ; 
       onyx_figbom_F-mat34.1-0 ; 
       onyx_figbom_F-mat35.1-0 ; 
       onyx_figbom_F-mat36.1-0 ; 
       onyx_figbom_F-mat37.1-0 ; 
       onyx_figbom_F-mat38.1-0 ; 
       onyx_figbom_F-mat39.1-0 ; 
       onyx_figbom_F-mat4.1-0 ; 
       onyx_figbom_F-mat40.1-0 ; 
       onyx_figbom_F-mat41.1-0 ; 
       onyx_figbom_F-mat42.1-0 ; 
       onyx_figbom_F-mat43.1-0 ; 
       onyx_figbom_F-mat44.1-0 ; 
       onyx_figbom_F-mat45.1-0 ; 
       onyx_figbom_F-mat46.1-0 ; 
       onyx_figbom_F-mat47.1-0 ; 
       onyx_figbom_F-mat48.1-0 ; 
       onyx_figbom_F-mat5.1-0 ; 
       onyx_figbom_F-mat51.1-0 ; 
       onyx_figbom_F-mat52.1-0 ; 
       onyx_figbom_F-mat6.1-0 ; 
       onyx_figbom_F-mat7.1-0 ; 
       onyx_figbom_F-mat8.1-0 ; 
       onyx_figbom_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       bom03-bom03.1-0 ROOT ; 
       bom03-cockpt.1-0 ; 
       bom03-fuselg.10-0 ; 
       bom03-fwepmnt.1-0 ; 
       bom03-lbooster.1-0 ; 
       bom03-lengine.1-0 ; 
       bom03-LL0.1-0 ; 
       bom03-LLf.1-0 ; 
       bom03-LLl.1-0 ; 
       bom03-LLr.1-0 ; 
       bom03-lwepemt1.1-0 ; 
       bom03-lwepemt2.1-0 ; 
       bom03-lwepemt3.1-0 ; 
       bom03-lwepemt4.1-0 ; 
       bom03-lwingzz.1-0 ; 
       bom03-rbooster.1-0 ; 
       bom03-rengine.1-0 ; 
       bom03-rwepemt1.1-0 ; 
       bom03-rwepemt2.1-0 ; 
       bom03-rwepemt3.1-0 ; 
       bom03-rwepemt4.1-0 ; 
       bom03-rwingzz.2-0 ; 
       bom03-SSal.1-0 ; 
       bom03-SSar.1-0 ; 
       bom03-SSf.1-0 ; 
       bom03-SSl.1-0 ; 
       bom03-SSr.1-0 ; 
       bom03-tturatt.1-0 ; 
       bom03-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Art fixes/soft/bugfixes/PICTURES/bom03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom03_final-onyx_figbom_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       onyx_figbom_F-gt2d1.1-0 ; 
       onyx_figbom_F-gt2d2.1-0 ; 
       onyx_figbom_F-t2d1.1-0 ; 
       onyx_figbom_F-t2d10.1-0 ; 
       onyx_figbom_F-t2d11.1-0 ; 
       onyx_figbom_F-t2d13.1-0 ; 
       onyx_figbom_F-t2d15.1-0 ; 
       onyx_figbom_F-t2d16.1-0 ; 
       onyx_figbom_F-t2d17.1-0 ; 
       onyx_figbom_F-t2d19.1-0 ; 
       onyx_figbom_F-t2d2.1-0 ; 
       onyx_figbom_F-t2d20.1-0 ; 
       onyx_figbom_F-t2d21.1-0 ; 
       onyx_figbom_F-t2d22.1-0 ; 
       onyx_figbom_F-t2d23.1-0 ; 
       onyx_figbom_F-t2d24.1-0 ; 
       onyx_figbom_F-t2d25.1-0 ; 
       onyx_figbom_F-t2d26.1-0 ; 
       onyx_figbom_F-t2d27.1-0 ; 
       onyx_figbom_F-t2d28.1-0 ; 
       onyx_figbom_F-t2d29.1-0 ; 
       onyx_figbom_F-t2d3.1-0 ; 
       onyx_figbom_F-t2d30.1-0 ; 
       onyx_figbom_F-t2d31.1-0 ; 
       onyx_figbom_F-t2d32.1-0 ; 
       onyx_figbom_F-t2d33.1-0 ; 
       onyx_figbom_F-t2d34.1-0 ; 
       onyx_figbom_F-t2d36.1-0 ; 
       onyx_figbom_F-t2d37.1-0 ; 
       onyx_figbom_F-t2d4.1-0 ; 
       onyx_figbom_F-t2d41.1-0 ; 
       onyx_figbom_F-t2d42.1-0 ; 
       onyx_figbom_F-t2d43.1-0 ; 
       onyx_figbom_F-t2d5.1-0 ; 
       onyx_figbom_F-t2d6.1-0 ; 
       onyx_figbom_F-t2d7.1-0 ; 
       onyx_figbom_F-t2d8.1-0 ; 
       onyx_figbom_F-t2d9.1-0 ; 
       onyx_figbom_F-zzzzzzzzt2d35.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       14 28 110 ; 
       15 2 110 ; 
       16 15 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       21 28 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 14 110 ; 
       26 21 110 ; 
       27 2 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       2 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 14 300 ; 
       2 16 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       4 48 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       8 0 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       9 1 300 ; 
       9 46 300 ; 
       9 47 300 ; 
       14 13 300 ; 
       14 24 300 ; 
       14 35 300 ; 
       14 45 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 15 300 ; 
       14 17 300 ; 
       15 51 300 ; 
       15 3 300 ; 
       15 4 300 ; 
       21 20 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       21 23 300 ; 
       21 25 300 ; 
       21 26 300 ; 
       21 27 300 ; 
       21 28 300 ; 
       22 38 300 ; 
       23 39 300 ; 
       24 34 300 ; 
       25 37 300 ; 
       26 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       3 4 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 24 401 ; 
       1 30 401 ; 
       3 33 401 ; 
       4 34 401 ; 
       5 35 401 ; 
       6 36 401 ; 
       7 37 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 19 401 ; 
       11 5 401 ; 
       12 0 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       17 8 401 ; 
       18 11 401 ; 
       19 9 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 2 401 ; 
       25 14 401 ; 
       26 1 401 ; 
       27 15 401 ; 
       28 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 20 401 ; 
       32 22 401 ; 
       33 23 401 ; 
       35 10 401 ; 
       40 25 401 ; 
       41 26 401 ; 
       42 38 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       46 31 401 ; 
       47 32 401 ; 
       49 21 401 ; 
       50 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 50 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 30 -8 0 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 MPRFLG 0 ; 
       7 SCHEM 43.13409 -10 0 USR MPRFLG 0 ; 
       8 SCHEM 45.25103 -10 0 USR MPRFLG 0 ; 
       9 SCHEM 47.75103 -10 0 MPRFLG 0 ; 
       10 SCHEM 5 -12 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 10 -12 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 15 -12 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 20 -12 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 20 -10 0 MPRFLG 0 ; 
       22 SCHEM 35 -8 0 MPRFLG 0 ; 
       23 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       26 SCHEM 25 -12 0 MPRFLG 0 ; 
       27 SCHEM 40 -8 0 MPRFLG 0 ; 
       28 SCHEM 13.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44.25103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.75103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44.25103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44.25103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 42.13409 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 42.13409 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 42.13409 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 46.75103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.75103 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 44.25103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44.25103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44.25103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 42.13409 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 42.13409 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.75103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.75103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.75103 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 42.13409 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 55 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
