SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-bom04_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.23-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       add_smoke-mat1.1-0 ; 
       add_smoke-mat10.1-0 ; 
       add_smoke-mat11.1-0 ; 
       add_smoke-mat12.1-0 ; 
       add_smoke-mat13.1-0 ; 
       add_smoke-mat14.1-0 ; 
       add_smoke-mat15.1-0 ; 
       add_smoke-mat16.1-0 ; 
       add_smoke-mat17.1-0 ; 
       add_smoke-mat18.1-0 ; 
       add_smoke-mat2.1-0 ; 
       add_smoke-mat21.1-0 ; 
       add_smoke-mat22.1-0 ; 
       add_smoke-mat23.1-0 ; 
       add_smoke-mat24.1-0 ; 
       add_smoke-mat25.1-0 ; 
       add_smoke-mat26.1-0 ; 
       add_smoke-mat27.1-0 ; 
       add_smoke-mat28.1-0 ; 
       add_smoke-mat29.1-0 ; 
       add_smoke-mat3.1-0 ; 
       add_smoke-mat36.1-0 ; 
       add_smoke-mat37.1-0 ; 
       add_smoke-mat4.1-0 ; 
       add_smoke-mat44.1-0 ; 
       add_smoke-mat46.1-0 ; 
       add_smoke-mat48.1-0 ; 
       add_smoke-mat49.1-0 ; 
       add_smoke-mat5.1-0 ; 
       add_smoke-mat50.1-0 ; 
       add_smoke-mat51.1-0 ; 
       add_smoke-mat52.1-0 ; 
       add_smoke-mat53.1-0 ; 
       add_smoke-mat54.1-0 ; 
       add_smoke-mat55.1-0 ; 
       add_smoke-mat56.1-0 ; 
       add_smoke-mat57.1-0 ; 
       add_smoke-mat58.1-0 ; 
       add_smoke-mat59.1-0 ; 
       add_smoke-mat6.1-0 ; 
       add_smoke-mat60.1-0 ; 
       add_smoke-mat61.1-0 ; 
       add_smoke-mat62.1-0 ; 
       add_smoke-mat63.1-0 ; 
       add_smoke-mat64.1-0 ; 
       add_smoke-mat65.1-0 ; 
       add_smoke-mat66.1-0 ; 
       add_smoke-mat7.1-0 ; 
       add_smoke-mat8.1-0 ; 
       add_smoke-mat83.1-0 ; 
       add_smoke-mat84.1-0 ; 
       add_smoke-mat85.1-0 ; 
       add_smoke-mat86.1-0 ; 
       add_smoke-mat87.1-0 ; 
       add_smoke-mat9.1-0 ; 
       rix_fig_F-mat75.2-0 ; 
       rix_fig_F-mat79.2-0 ; 
       rix_fig_F-mat80.2-0 ; 
       rix_fig_F-mat81.2-0 ; 
       rix_fig_F-mat82.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04_3.2-0 ROOT ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-landgr0.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-LLa1.1-0 ; 
       bom04-LLa2.1-0 ; 
       bom04-LLl1.1-0 ; 
       bom04-LLl2.1-0 ; 
       bom04-LLl3.1-0 ; 
       bom04-LLr1.1-0 ; 
       bom04-LLr2.1-0 ; 
       bom04-LLr3.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-missemt.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt1.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-smoke.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
       bom04-thrust.1-0 ; 
       bom04-trail.1-0 ; 
       bom04-turwepemt1.1-0 ; 
       bom04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-add_smoke.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       add_smoke-t2d1.1-0 ; 
       add_smoke-t2d10.1-0 ; 
       add_smoke-t2d11.1-0 ; 
       add_smoke-t2d12.1-0 ; 
       add_smoke-t2d13.1-0 ; 
       add_smoke-t2d14.1-0 ; 
       add_smoke-t2d15.1-0 ; 
       add_smoke-t2d16.1-0 ; 
       add_smoke-t2d17.1-0 ; 
       add_smoke-t2d2.1-0 ; 
       add_smoke-t2d20.1-0 ; 
       add_smoke-t2d22.1-0 ; 
       add_smoke-t2d23.1-0 ; 
       add_smoke-t2d24.1-0 ; 
       add_smoke-t2d25.1-0 ; 
       add_smoke-t2d26.1-0 ; 
       add_smoke-t2d27.1-0 ; 
       add_smoke-t2d28.1-0 ; 
       add_smoke-t2d35.1-0 ; 
       add_smoke-t2d36.1-0 ; 
       add_smoke-t2d4.1-0 ; 
       add_smoke-t2d43.1-0 ; 
       add_smoke-t2d45.1-0 ; 
       add_smoke-t2d48.1-0 ; 
       add_smoke-t2d49.1-0 ; 
       add_smoke-t2d5.1-0 ; 
       add_smoke-t2d50.1-0 ; 
       add_smoke-t2d51.1-0 ; 
       add_smoke-t2d52.1-0 ; 
       add_smoke-t2d53.1-0 ; 
       add_smoke-t2d54.1-0 ; 
       add_smoke-t2d55.1-0 ; 
       add_smoke-t2d56.1-0 ; 
       add_smoke-t2d57.1-0 ; 
       add_smoke-t2d58.1-0 ; 
       add_smoke-t2d59.1-0 ; 
       add_smoke-t2d6.1-0 ; 
       add_smoke-t2d60.1-0 ; 
       add_smoke-t2d61.1-0 ; 
       add_smoke-t2d62.1-0 ; 
       add_smoke-t2d63.1-0 ; 
       add_smoke-t2d64.1-0 ; 
       add_smoke-t2d65.1-0 ; 
       add_smoke-t2d66.1-0 ; 
       add_smoke-t2d67.1-0 ; 
       add_smoke-t2d68.1-0 ; 
       add_smoke-t2d69.1-0 ; 
       add_smoke-t2d7.1-0 ; 
       add_smoke-t2d8.1-0 ; 
       add_smoke-t2d9.1-0 ; 
       add_smoke-zt2d21.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 26 110 ; 
       8 25 110 ; 
       9 37 110 ; 
       10 36 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 11 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 8 110 ; 
       24 7 110 ; 
       25 6 110 ; 
       26 6 110 ; 
       27 5 110 ; 
       28 27 110 ; 
       29 28 110 ; 
       30 6 110 ; 
       31 6 110 ; 
       32 6 110 ; 
       33 6 110 ; 
       34 10 110 ; 
       35 9 110 ; 
       36 6 110 ; 
       37 6 110 ; 
       38 5 110 ; 
       39 38 110 ; 
       40 39 110 ; 
       42 6 110 ; 
       43 6 110 ; 
       44 6 110 ; 
       45 6 110 ; 
       46 29 110 ; 
       47 40 110 ; 
       48 6 110 ; 
       49 5 110 ; 
       50 5 110 ; 
       51 6 110 ; 
       52 6 110 ; 
       41 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 10 300 ; 
       0 20 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       2 45 300 ; 
       4 43 300 ; 
       4 44 300 ; 
       6 23 300 ; 
       6 28 300 ; 
       6 39 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 54 300 ; 
       6 1 300 ; 
       6 25 300 ; 
       6 46 300 ; 
       7 50 300 ; 
       8 51 300 ; 
       9 52 300 ; 
       10 53 300 ; 
       13 14 300 ; 
       13 42 300 ; 
       14 13 300 ; 
       14 41 300 ; 
       15 24 300 ; 
       16 19 300 ; 
       17 18 300 ; 
       18 15 300 ; 
       19 17 300 ; 
       20 16 300 ; 
       21 4 300 ; 
       21 35 300 ; 
       28 8 300 ; 
       28 9 300 ; 
       28 12 300 ; 
       28 21 300 ; 
       28 31 300 ; 
       28 32 300 ; 
       29 2 300 ; 
       29 33 300 ; 
       29 38 300 ; 
       32 5 300 ; 
       32 30 300 ; 
       32 34 300 ; 
       39 6 300 ; 
       39 7 300 ; 
       39 11 300 ; 
       39 22 300 ; 
       39 26 300 ; 
       39 27 300 ; 
       40 3 300 ; 
       40 29 300 ; 
       40 40 300 ; 
       42 56 300 ; 
       43 57 300 ; 
       44 58 300 ; 
       45 59 300 ; 
       46 55 300 ; 
       47 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       39 23 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 49 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 50 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 9 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 24 401 ; 
       27 26 401 ; 
       28 20 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 25 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       44 41 401 ; 
       46 42 401 ; 
       47 36 401 ; 
       48 47 401 ; 
       50 43 401 ; 
       51 44 401 ; 
       52 45 401 ; 
       53 46 401 ; 
       54 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -6 0 MPRFLG 0 ; 
       1 SCHEM 45 -6 0 MPRFLG 0 ; 
       2 SCHEM 43.75 0 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       3 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 65 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 MPRFLG 0 ; 
       15 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 10 -8 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 25 -6 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -6 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 MPRFLG 0 ; 
       28 SCHEM 5 -6 0 MPRFLG 0 ; 
       29 SCHEM 5 -8 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 35 -6 0 MPRFLG 0 ; 
       34 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 50 -6 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 55 -6 0 MPRFLG 0 ; 
       44 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       45 SCHEM 60 -6 0 MPRFLG 0 ; 
       46 SCHEM 5 -10 0 MPRFLG 0 ; 
       47 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       48 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       49 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 84 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
