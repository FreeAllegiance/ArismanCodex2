SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-bom04.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       adding_gun-mat1.1-0 ; 
       adding_gun-mat10.1-0 ; 
       adding_gun-mat11.1-0 ; 
       adding_gun-mat12.1-0 ; 
       adding_gun-mat13.1-0 ; 
       adding_gun-mat14.1-0 ; 
       adding_gun-mat15.1-0 ; 
       adding_gun-mat16.1-0 ; 
       adding_gun-mat17.1-0 ; 
       adding_gun-mat18.1-0 ; 
       adding_gun-mat2.1-0 ; 
       adding_gun-mat21.1-0 ; 
       adding_gun-mat22.1-0 ; 
       adding_gun-mat23.1-0 ; 
       adding_gun-mat24.1-0 ; 
       adding_gun-mat25.1-0 ; 
       adding_gun-mat26.1-0 ; 
       adding_gun-mat27.1-0 ; 
       adding_gun-mat28.1-0 ; 
       adding_gun-mat29.1-0 ; 
       adding_gun-mat3.1-0 ; 
       adding_gun-mat36.1-0 ; 
       adding_gun-mat37.1-0 ; 
       adding_gun-mat4.1-0 ; 
       adding_gun-mat44.1-0 ; 
       adding_gun-mat46.1-0 ; 
       adding_gun-mat48.1-0 ; 
       adding_gun-mat49.1-0 ; 
       adding_gun-mat5.1-0 ; 
       adding_gun-mat50.1-0 ; 
       adding_gun-mat51.1-0 ; 
       adding_gun-mat52.1-0 ; 
       adding_gun-mat53.1-0 ; 
       adding_gun-mat54.1-0 ; 
       adding_gun-mat55.1-0 ; 
       adding_gun-mat56.1-0 ; 
       adding_gun-mat57.1-0 ; 
       adding_gun-mat58.1-0 ; 
       adding_gun-mat59.1-0 ; 
       adding_gun-mat6.1-0 ; 
       adding_gun-mat60.1-0 ; 
       adding_gun-mat61.1-0 ; 
       adding_gun-mat62.1-0 ; 
       adding_gun-mat63.1-0 ; 
       adding_gun-mat64.1-0 ; 
       adding_gun-mat65.1-0 ; 
       adding_gun-mat66.1-0 ; 
       adding_gun-mat7.1-0 ; 
       adding_gun-mat8.1-0 ; 
       adding_gun-mat83.1-0 ; 
       adding_gun-mat84.1-0 ; 
       adding_gun-mat9.1-0 ; 
       rix_fig_F-mat75.1-0 ; 
       rix_fig_F-mat79.1-0 ; 
       rix_fig_F-mat80.1-0 ; 
       rix_fig_F-mat81.1-0 ; 
       rix_fig_F-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       adding_gun-cyl2.2-0 ROOT ; 
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04.2-0 ROOT ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-landgr0.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-LLa1.1-0 ; 
       bom04-LLa2.1-0 ; 
       bom04-LLl1.1-0 ; 
       bom04-LLl2.1-0 ; 
       bom04-LLl3.1-0 ; 
       bom04-LLr1.1-0 ; 
       bom04-LLr2.1-0 ; 
       bom04-LLr3.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepemt3.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-adding_gun.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 48     
       adding_gun-t2d1.1-0 ; 
       adding_gun-t2d10.1-0 ; 
       adding_gun-t2d11.1-0 ; 
       adding_gun-t2d12.1-0 ; 
       adding_gun-t2d13.1-0 ; 
       adding_gun-t2d14.1-0 ; 
       adding_gun-t2d15.1-0 ; 
       adding_gun-t2d16.1-0 ; 
       adding_gun-t2d17.1-0 ; 
       adding_gun-t2d2.1-0 ; 
       adding_gun-t2d20.1-0 ; 
       adding_gun-t2d22.1-0 ; 
       adding_gun-t2d23.1-0 ; 
       adding_gun-t2d24.1-0 ; 
       adding_gun-t2d25.1-0 ; 
       adding_gun-t2d26.1-0 ; 
       adding_gun-t2d27.1-0 ; 
       adding_gun-t2d28.1-0 ; 
       adding_gun-t2d35.1-0 ; 
       adding_gun-t2d36.1-0 ; 
       adding_gun-t2d4.1-0 ; 
       adding_gun-t2d43.1-0 ; 
       adding_gun-t2d45.1-0 ; 
       adding_gun-t2d48.1-0 ; 
       adding_gun-t2d49.1-0 ; 
       adding_gun-t2d5.1-0 ; 
       adding_gun-t2d50.1-0 ; 
       adding_gun-t2d51.1-0 ; 
       adding_gun-t2d52.1-0 ; 
       adding_gun-t2d53.1-0 ; 
       adding_gun-t2d54.1-0 ; 
       adding_gun-t2d55.1-0 ; 
       adding_gun-t2d56.1-0 ; 
       adding_gun-t2d57.1-0 ; 
       adding_gun-t2d58.1-0 ; 
       adding_gun-t2d59.1-0 ; 
       adding_gun-t2d6.1-0 ; 
       adding_gun-t2d60.1-0 ; 
       adding_gun-t2d61.1-0 ; 
       adding_gun-t2d62.1-0 ; 
       adding_gun-t2d63.1-0 ; 
       adding_gun-t2d64.1-0 ; 
       adding_gun-t2d65.1-0 ; 
       adding_gun-t2d66.1-0 ; 
       adding_gun-t2d7.1-0 ; 
       adding_gun-t2d8.1-0 ; 
       adding_gun-t2d9.1-0 ; 
       adding_gun-zt2d21.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 6 110 ; 
       2 6 110 ; 
       4 5 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 6 110 ; 
       9 7 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 7 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 21 110 ; 
       20 22 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 5 110 ; 
       24 23 110 ; 
       25 24 110 ; 
       26 6 110 ; 
       27 6 110 ; 
       28 6 110 ; 
       29 32 110 ; 
       30 31 110 ; 
       31 6 110 ; 
       32 6 110 ; 
       33 5 110 ; 
       34 33 110 ; 
       35 34 110 ; 
       36 6 110 ; 
       37 6 110 ; 
       38 6 110 ; 
       39 6 110 ; 
       40 25 110 ; 
       41 35 110 ; 
       42 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 50 300 ; 
       1 0 300 ; 
       1 10 300 ; 
       1 20 300 ; 
       1 36 300 ; 
       1 37 300 ; 
       3 45 300 ; 
       4 43 300 ; 
       4 44 300 ; 
       6 23 300 ; 
       6 28 300 ; 
       6 39 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 51 300 ; 
       6 1 300 ; 
       6 25 300 ; 
       6 46 300 ; 
       9 14 300 ; 
       9 42 300 ; 
       10 13 300 ; 
       10 41 300 ; 
       11 24 300 ; 
       12 19 300 ; 
       13 18 300 ; 
       14 15 300 ; 
       15 17 300 ; 
       16 16 300 ; 
       17 4 300 ; 
       17 35 300 ; 
       24 8 300 ; 
       24 9 300 ; 
       24 12 300 ; 
       24 21 300 ; 
       24 31 300 ; 
       24 32 300 ; 
       25 2 300 ; 
       25 33 300 ; 
       25 38 300 ; 
       27 5 300 ; 
       27 30 300 ; 
       27 34 300 ; 
       34 6 300 ; 
       34 7 300 ; 
       34 11 300 ; 
       34 22 300 ; 
       34 26 300 ; 
       34 27 300 ; 
       35 3 300 ; 
       35 29 300 ; 
       35 40 300 ; 
       36 53 300 ; 
       37 54 300 ; 
       38 55 300 ; 
       39 56 300 ; 
       40 52 300 ; 
       41 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       34 23 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 46 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 47 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 9 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 24 401 ; 
       27 26 401 ; 
       28 20 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 25 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       44 41 401 ; 
       46 42 401 ; 
       47 36 401 ; 
       48 44 401 ; 
       51 45 401 ; 
       50 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 48.54255 -9.622824 0 USR DISPLAY 1 2 SRT 1 1 1 0 0 0 0 -0.7517414 1.918751 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       3 SCHEM 35 0 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 10 -8 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 25 -6 0 MPRFLG 0 ; 
       18 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 MPRFLG 0 ; 
       24 SCHEM 5 -6 0 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 MPRFLG 0 ; 
       26 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 35 -6 0 MPRFLG 0 ; 
       29 SCHEM 47.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 50 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 50 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 55 -6 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 60 -6 0 MPRFLG 0 ; 
       40 SCHEM 5 -10 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 37.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 66.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 48.54255 -11.62282 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 48.54255 -13.62282 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
