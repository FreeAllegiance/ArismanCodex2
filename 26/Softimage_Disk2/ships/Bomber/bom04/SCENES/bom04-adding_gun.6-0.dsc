SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-bom04.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.7-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       adding_gun-mat1.1-0 ; 
       adding_gun-mat10.1-0 ; 
       adding_gun-mat11.1-0 ; 
       adding_gun-mat12.1-0 ; 
       adding_gun-mat13.1-0 ; 
       adding_gun-mat14.1-0 ; 
       adding_gun-mat15.1-0 ; 
       adding_gun-mat16.1-0 ; 
       adding_gun-mat17.1-0 ; 
       adding_gun-mat18.1-0 ; 
       adding_gun-mat2.1-0 ; 
       adding_gun-mat21.1-0 ; 
       adding_gun-mat22.1-0 ; 
       adding_gun-mat23.1-0 ; 
       adding_gun-mat24.1-0 ; 
       adding_gun-mat25.1-0 ; 
       adding_gun-mat26.1-0 ; 
       adding_gun-mat27.1-0 ; 
       adding_gun-mat28.1-0 ; 
       adding_gun-mat29.1-0 ; 
       adding_gun-mat3.1-0 ; 
       adding_gun-mat36.1-0 ; 
       adding_gun-mat37.1-0 ; 
       adding_gun-mat4.1-0 ; 
       adding_gun-mat44.1-0 ; 
       adding_gun-mat46.1-0 ; 
       adding_gun-mat48.1-0 ; 
       adding_gun-mat49.1-0 ; 
       adding_gun-mat5.1-0 ; 
       adding_gun-mat50.1-0 ; 
       adding_gun-mat51.1-0 ; 
       adding_gun-mat52.1-0 ; 
       adding_gun-mat53.1-0 ; 
       adding_gun-mat54.1-0 ; 
       adding_gun-mat55.1-0 ; 
       adding_gun-mat56.1-0 ; 
       adding_gun-mat57.1-0 ; 
       adding_gun-mat58.1-0 ; 
       adding_gun-mat59.1-0 ; 
       adding_gun-mat6.1-0 ; 
       adding_gun-mat60.1-0 ; 
       adding_gun-mat61.1-0 ; 
       adding_gun-mat62.1-0 ; 
       adding_gun-mat63.1-0 ; 
       adding_gun-mat64.1-0 ; 
       adding_gun-mat65.1-0 ; 
       adding_gun-mat66.1-0 ; 
       adding_gun-mat7.1-0 ; 
       adding_gun-mat8.1-0 ; 
       adding_gun-mat83.1-0 ; 
       adding_gun-mat84.1-0 ; 
       adding_gun-mat85.2-0 ; 
       adding_gun-mat86.1-0 ; 
       adding_gun-mat87.1-0 ; 
       adding_gun-mat9.1-0 ; 
       rix_fig_F-mat75.1-0 ; 
       rix_fig_F-mat79.1-0 ; 
       rix_fig_F-mat80.1-0 ; 
       rix_fig_F-mat81.1-0 ; 
       rix_fig_F-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04.6-0 ROOT ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-landgr0.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-LLa1.1-0 ; 
       bom04-LLa2.1-0 ; 
       bom04-LLl1.1-0 ; 
       bom04-LLl2.1-0 ; 
       bom04-LLl3.1-0 ; 
       bom04-LLr1.1-0 ; 
       bom04-LLr2.1-0 ; 
       bom04-LLr3.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepemt3.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-adding_gun.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       adding_gun-t2d1.1-0 ; 
       adding_gun-t2d10.1-0 ; 
       adding_gun-t2d11.1-0 ; 
       adding_gun-t2d12.1-0 ; 
       adding_gun-t2d13.1-0 ; 
       adding_gun-t2d14.1-0 ; 
       adding_gun-t2d15.1-0 ; 
       adding_gun-t2d16.1-0 ; 
       adding_gun-t2d17.1-0 ; 
       adding_gun-t2d2.1-0 ; 
       adding_gun-t2d20.1-0 ; 
       adding_gun-t2d22.1-0 ; 
       adding_gun-t2d23.1-0 ; 
       adding_gun-t2d24.1-0 ; 
       adding_gun-t2d25.1-0 ; 
       adding_gun-t2d26.1-0 ; 
       adding_gun-t2d27.1-0 ; 
       adding_gun-t2d28.1-0 ; 
       adding_gun-t2d35.1-0 ; 
       adding_gun-t2d36.1-0 ; 
       adding_gun-t2d4.1-0 ; 
       adding_gun-t2d43.1-0 ; 
       adding_gun-t2d45.1-0 ; 
       adding_gun-t2d48.1-0 ; 
       adding_gun-t2d49.1-0 ; 
       adding_gun-t2d5.1-0 ; 
       adding_gun-t2d50.1-0 ; 
       adding_gun-t2d51.1-0 ; 
       adding_gun-t2d52.1-0 ; 
       adding_gun-t2d53.1-0 ; 
       adding_gun-t2d54.1-0 ; 
       adding_gun-t2d55.1-0 ; 
       adding_gun-t2d56.1-0 ; 
       adding_gun-t2d57.1-0 ; 
       adding_gun-t2d58.1-0 ; 
       adding_gun-t2d59.1-0 ; 
       adding_gun-t2d6.1-0 ; 
       adding_gun-t2d60.1-0 ; 
       adding_gun-t2d61.1-0 ; 
       adding_gun-t2d62.1-0 ; 
       adding_gun-t2d63.1-0 ; 
       adding_gun-t2d64.1-0 ; 
       adding_gun-t2d65.1-0 ; 
       adding_gun-t2d66.2-0 ; 
       adding_gun-t2d67.2-0 ; 
       adding_gun-t2d68.1-0 ; 
       adding_gun-t2d69.1-0 ; 
       adding_gun-t2d7.1-0 ; 
       adding_gun-t2d8.1-0 ; 
       adding_gun-t2d9.1-0 ; 
       adding_gun-zt2d21.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 24 110 ; 
       8 35 110 ; 
       9 34 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       6 25 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       10 4 110 ; 
       11 5 110 ; 
       12 10 110 ; 
       13 12 110 ; 
       14 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 10 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 5 110 ; 
       21 5 110 ; 
       22 7 110 ; 
       23 6 110 ; 
       24 5 110 ; 
       25 5 110 ; 
       26 4 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 5 110 ; 
       30 5 110 ; 
       31 5 110 ; 
       32 8 110 ; 
       33 9 110 ; 
       34 5 110 ; 
       35 5 110 ; 
       36 4 110 ; 
       37 36 110 ; 
       38 37 110 ; 
       39 5 110 ; 
       40 5 110 ; 
       41 5 110 ; 
       42 5 110 ; 
       43 28 110 ; 
       44 38 110 ; 
       45 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 51 300 ; 
       8 52 300 ; 
       9 53 300 ; 
       0 0 300 ; 
       0 10 300 ; 
       0 20 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       2 45 300 ; 
       6 50 300 ; 
       3 43 300 ; 
       3 44 300 ; 
       5 23 300 ; 
       5 28 300 ; 
       5 39 300 ; 
       5 47 300 ; 
       5 48 300 ; 
       5 54 300 ; 
       5 1 300 ; 
       5 25 300 ; 
       5 46 300 ; 
       12 14 300 ; 
       12 42 300 ; 
       13 13 300 ; 
       13 41 300 ; 
       14 24 300 ; 
       15 19 300 ; 
       16 18 300 ; 
       17 15 300 ; 
       18 17 300 ; 
       19 16 300 ; 
       20 4 300 ; 
       20 35 300 ; 
       27 8 300 ; 
       27 9 300 ; 
       27 12 300 ; 
       27 21 300 ; 
       27 31 300 ; 
       27 32 300 ; 
       28 2 300 ; 
       28 33 300 ; 
       28 38 300 ; 
       30 5 300 ; 
       30 30 300 ; 
       30 34 300 ; 
       37 6 300 ; 
       37 7 300 ; 
       37 11 300 ; 
       37 22 300 ; 
       37 26 300 ; 
       37 27 300 ; 
       38 3 300 ; 
       38 29 300 ; 
       38 40 300 ; 
       39 56 300 ; 
       40 57 300 ; 
       41 58 300 ; 
       42 59 300 ; 
       43 55 300 ; 
       44 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       37 23 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 49 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 50 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 9 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 24 401 ; 
       27 26 401 ; 
       28 20 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 25 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       44 41 401 ; 
       46 42 401 ; 
       47 36 401 ; 
       48 47 401 ; 
       50 43 401 ; 
       51 44 401 ; 
       54 48 401 ; 
       52 45 401 ; 
       53 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 145.0792 -9.481613 0 USR MPRFLG 0 ; 
       8 SCHEM 122.7966 -9.363791 0 USR MPRFLG 0 ; 
       9 SCHEM 128.5687 -9.337972 0 USR MPRFLG 0 ; 
       0 SCHEM 100 -8 0 MPRFLG 0 ; 
       1 SCHEM 120 -8 0 MPRFLG 0 ; 
       2 SCHEM 86.25 -2 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       6 SCHEM 142.0664 -9.395215 0 USR MPRFLG 0 ; 
       3 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 125 -6 0 MPRFLG 0 ; 
       10 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 115 -8 0 MPRFLG 0 ; 
       12 SCHEM 71.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       14 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 60 -10 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 MPRFLG 0 ; 
       18 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 55 -10 0 MPRFLG 0 ; 
       20 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       21 SCHEM 107.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 145 -11.46877 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 142.1128 -11.1619 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 145 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 142.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       27 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       28 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       29 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 90 -8 0 MPRFLG 0 ; 
       31 SCHEM 110 -8 0 MPRFLG 0 ; 
       32 SCHEM 122.5 -12 0 USR WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       33 SCHEM 127.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 128.75 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 123.75 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       39 SCHEM 132.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 135 -8 0 MPRFLG 0 ; 
       41 SCHEM 137.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 140 -8 0 MPRFLG 0 ; 
       43 SCHEM 0 -12 0 MPRFLG 0 ; 
       44 SCHEM 25 -12 0 MPRFLG 0 ; 
       45 SCHEM 112.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 132.3436 -13.19653 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 176.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 186.0936 -3.196527 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 167.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 132.3436 -15.19653 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 162.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 176.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 186.0936 -5.196527 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 170 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 57 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
