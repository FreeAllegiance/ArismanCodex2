SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-bom04.13-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.15-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       adjust_nulls-mat1.2-0 ; 
       adjust_nulls-mat10.2-0 ; 
       adjust_nulls-mat11.2-0 ; 
       adjust_nulls-mat12.2-0 ; 
       adjust_nulls-mat13.2-0 ; 
       adjust_nulls-mat14.2-0 ; 
       adjust_nulls-mat15.2-0 ; 
       adjust_nulls-mat16.2-0 ; 
       adjust_nulls-mat17.2-0 ; 
       adjust_nulls-mat18.2-0 ; 
       adjust_nulls-mat2.2-0 ; 
       adjust_nulls-mat21.2-0 ; 
       adjust_nulls-mat22.2-0 ; 
       adjust_nulls-mat23.2-0 ; 
       adjust_nulls-mat24.2-0 ; 
       adjust_nulls-mat25.2-0 ; 
       adjust_nulls-mat26.2-0 ; 
       adjust_nulls-mat27.2-0 ; 
       adjust_nulls-mat28.2-0 ; 
       adjust_nulls-mat29.2-0 ; 
       adjust_nulls-mat3.2-0 ; 
       adjust_nulls-mat36.2-0 ; 
       adjust_nulls-mat37.2-0 ; 
       adjust_nulls-mat4.2-0 ; 
       adjust_nulls-mat44.2-0 ; 
       adjust_nulls-mat46.2-0 ; 
       adjust_nulls-mat48.2-0 ; 
       adjust_nulls-mat49.2-0 ; 
       adjust_nulls-mat5.2-0 ; 
       adjust_nulls-mat50.2-0 ; 
       adjust_nulls-mat51.2-0 ; 
       adjust_nulls-mat52.2-0 ; 
       adjust_nulls-mat53.2-0 ; 
       adjust_nulls-mat54.2-0 ; 
       adjust_nulls-mat55.2-0 ; 
       adjust_nulls-mat56.2-0 ; 
       adjust_nulls-mat57.2-0 ; 
       adjust_nulls-mat58.2-0 ; 
       adjust_nulls-mat59.2-0 ; 
       adjust_nulls-mat6.2-0 ; 
       adjust_nulls-mat60.2-0 ; 
       adjust_nulls-mat61.2-0 ; 
       adjust_nulls-mat62.2-0 ; 
       adjust_nulls-mat63.2-0 ; 
       adjust_nulls-mat64.2-0 ; 
       adjust_nulls-mat65.2-0 ; 
       adjust_nulls-mat66.2-0 ; 
       adjust_nulls-mat7.2-0 ; 
       adjust_nulls-mat8.2-0 ; 
       adjust_nulls-mat83.2-0 ; 
       adjust_nulls-mat84.2-0 ; 
       adjust_nulls-mat85.2-0 ; 
       adjust_nulls-mat86.2-0 ; 
       adjust_nulls-mat87.2-0 ; 
       adjust_nulls-mat9.2-0 ; 
       rix_fig_F-mat75.2-0 ; 
       rix_fig_F-mat79.2-0 ; 
       rix_fig_F-mat80.2-0 ; 
       rix_fig_F-mat81.2-0 ; 
       rix_fig_F-mat82.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04.12-0 ROOT ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-landgr0.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-LLa1.1-0 ; 
       bom04-LLa2.1-0 ; 
       bom04-LLl1.1-0 ; 
       bom04-LLl2.1-0 ; 
       bom04-LLl3.1-0 ; 
       bom04-LLr1.1-0 ; 
       bom04-LLr2.1-0 ; 
       bom04-LLr3.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-missemt.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt1.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
       bom04-trail.1-0 ; 
       bom04-turwepemt1.1-0 ; 
       bom04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-adjust_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       adjust_nulls-t2d1.2-0 ; 
       adjust_nulls-t2d10.2-0 ; 
       adjust_nulls-t2d11.2-0 ; 
       adjust_nulls-t2d12.2-0 ; 
       adjust_nulls-t2d13.2-0 ; 
       adjust_nulls-t2d14.2-0 ; 
       adjust_nulls-t2d15.2-0 ; 
       adjust_nulls-t2d16.2-0 ; 
       adjust_nulls-t2d17.2-0 ; 
       adjust_nulls-t2d2.2-0 ; 
       adjust_nulls-t2d20.2-0 ; 
       adjust_nulls-t2d22.2-0 ; 
       adjust_nulls-t2d23.2-0 ; 
       adjust_nulls-t2d24.2-0 ; 
       adjust_nulls-t2d25.2-0 ; 
       adjust_nulls-t2d26.2-0 ; 
       adjust_nulls-t2d27.2-0 ; 
       adjust_nulls-t2d28.2-0 ; 
       adjust_nulls-t2d35.2-0 ; 
       adjust_nulls-t2d36.2-0 ; 
       adjust_nulls-t2d4.2-0 ; 
       adjust_nulls-t2d43.2-0 ; 
       adjust_nulls-t2d45.2-0 ; 
       adjust_nulls-t2d48.2-0 ; 
       adjust_nulls-t2d49.2-0 ; 
       adjust_nulls-t2d5.2-0 ; 
       adjust_nulls-t2d50.2-0 ; 
       adjust_nulls-t2d51.2-0 ; 
       adjust_nulls-t2d52.2-0 ; 
       adjust_nulls-t2d53.2-0 ; 
       adjust_nulls-t2d54.2-0 ; 
       adjust_nulls-t2d55.2-0 ; 
       adjust_nulls-t2d56.2-0 ; 
       adjust_nulls-t2d57.2-0 ; 
       adjust_nulls-t2d58.2-0 ; 
       adjust_nulls-t2d59.2-0 ; 
       adjust_nulls-t2d6.2-0 ; 
       adjust_nulls-t2d60.2-0 ; 
       adjust_nulls-t2d61.2-0 ; 
       adjust_nulls-t2d62.2-0 ; 
       adjust_nulls-t2d63.2-0 ; 
       adjust_nulls-t2d64.2-0 ; 
       adjust_nulls-t2d65.2-0 ; 
       adjust_nulls-t2d66.2-0 ; 
       adjust_nulls-t2d67.2-0 ; 
       adjust_nulls-t2d68.2-0 ; 
       adjust_nulls-t2d69.2-0 ; 
       adjust_nulls-t2d7.2-0 ; 
       adjust_nulls-t2d8.2-0 ; 
       adjust_nulls-t2d9.2-0 ; 
       adjust_nulls-zt2d21.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       3 2 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 26 110 ; 
       8 25 110 ; 
       9 37 110 ; 
       10 36 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 11 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 8 110 ; 
       24 7 110 ; 
       25 6 110 ; 
       26 6 110 ; 
       27 5 110 ; 
       28 27 110 ; 
       29 28 110 ; 
       30 13 110 ; 
       31 6 110 ; 
       32 6 110 ; 
       33 6 110 ; 
       34 10 110 ; 
       35 9 110 ; 
       36 6 110 ; 
       37 6 110 ; 
       38 5 110 ; 
       39 38 110 ; 
       40 39 110 ; 
       41 6 110 ; 
       42 6 110 ; 
       43 6 110 ; 
       44 6 110 ; 
       45 29 110 ; 
       46 40 110 ; 
       47 6 110 ; 
       48 2 110 ; 
       49 6 110 ; 
       50 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 10 300 ; 
       0 20 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       2 45 300 ; 
       4 43 300 ; 
       4 44 300 ; 
       6 23 300 ; 
       6 28 300 ; 
       6 39 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       6 54 300 ; 
       6 1 300 ; 
       6 25 300 ; 
       6 46 300 ; 
       7 50 300 ; 
       8 51 300 ; 
       9 52 300 ; 
       10 53 300 ; 
       13 14 300 ; 
       13 42 300 ; 
       14 13 300 ; 
       14 41 300 ; 
       15 24 300 ; 
       16 19 300 ; 
       17 18 300 ; 
       18 15 300 ; 
       19 17 300 ; 
       20 16 300 ; 
       21 4 300 ; 
       21 35 300 ; 
       28 8 300 ; 
       28 9 300 ; 
       28 12 300 ; 
       28 21 300 ; 
       28 31 300 ; 
       28 32 300 ; 
       29 2 300 ; 
       29 33 300 ; 
       29 38 300 ; 
       32 5 300 ; 
       32 30 300 ; 
       32 34 300 ; 
       39 6 300 ; 
       39 7 300 ; 
       39 11 300 ; 
       39 22 300 ; 
       39 26 300 ; 
       39 27 300 ; 
       40 3 300 ; 
       40 29 300 ; 
       40 40 300 ; 
       41 56 300 ; 
       42 57 300 ; 
       43 58 300 ; 
       44 59 300 ; 
       45 55 300 ; 
       46 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       39 23 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 49 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 50 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 9 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 24 401 ; 
       27 26 401 ; 
       28 20 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 25 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       44 41 401 ; 
       46 42 401 ; 
       47 36 401 ; 
       48 47 401 ; 
       50 43 401 ; 
       51 44 401 ; 
       52 45 401 ; 
       53 46 401 ; 
       54 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32 -6 0 MPRFLG 0 ; 
       1 SCHEM 47 -6 0 MPRFLG 0 ; 
       2 SCHEM 39.5 0 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       3 SCHEM 58.13683 -10.11655 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 37 -2 0 MPRFLG 0 ; 
       6 SCHEM 47 -4 0 MPRFLG 0 ; 
       7 SCHEM 64.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 67 -8 0 MPRFLG 0 ; 
       9 SCHEM 49.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 52 -8 0 MPRFLG 0 ; 
       11 SCHEM 17 -4 0 MPRFLG 0 ; 
       12 SCHEM 42 -6 0 MPRFLG 0 ; 
       13 SCHEM 22 -6 0 MPRFLG 0 ; 
       14 SCHEM 22 -8 0 MPRFLG 0 ; 
       15 SCHEM 18.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 17 -8 0 MPRFLG 0 ; 
       17 SCHEM 19.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 13.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 12 -8 0 MPRFLG 0 ; 
       20 SCHEM 14.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 27 -6 0 MPRFLG 0 ; 
       22 SCHEM 34.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 67 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 64.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67 -6 0 MPRFLG 0 ; 
       26 SCHEM 64.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 7 -4 0 MPRFLG 0 ; 
       28 SCHEM 7 -6 0 MPRFLG 0 ; 
       29 SCHEM 7 -8 0 MPRFLG 0 ; 
       30 SCHEM 74.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 44.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 29.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 37 -6 0 MPRFLG 0 ; 
       34 SCHEM 52 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 52 -6 0 MPRFLG 0 ; 
       37 SCHEM 49.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 9.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 9.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 9.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 54.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 57 -6 0 MPRFLG 0 ; 
       43 SCHEM 59.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 62 -6 0 MPRFLG 0 ; 
       45 SCHEM 7 -10 0 MPRFLG 0 ; 
       46 SCHEM 9.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 39.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 60.63683 -10.11655 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 69.5 0 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       50 SCHEM 72 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 28.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 23.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 18.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 18.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 23.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 13.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 28.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 23.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 26 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 71 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 68.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 71 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 53.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 56 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 71 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 8.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 56 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 58.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 61 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 63.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 28.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 23.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 18.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 18.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 23.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 13.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 13.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 28.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 23.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 68.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 71 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 53.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 56 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 11 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 12 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
