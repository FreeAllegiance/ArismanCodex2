SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-bom04_3.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       rix_fig_F-mat75.3-0 ; 
       rix_fig_F-mat79.3-0 ; 
       rix_fig_F-mat80.3-0 ; 
       rix_fig_F-mat81.3-0 ; 
       rix_fig_F-mat82.3-0 ; 
       scaled-mat1.1-0 ; 
       scaled-mat10.1-0 ; 
       scaled-mat11.1-0 ; 
       scaled-mat12.1-0 ; 
       scaled-mat13.1-0 ; 
       scaled-mat14.1-0 ; 
       scaled-mat15.1-0 ; 
       scaled-mat16.1-0 ; 
       scaled-mat17.1-0 ; 
       scaled-mat18.1-0 ; 
       scaled-mat2.1-0 ; 
       scaled-mat21.1-0 ; 
       scaled-mat22.1-0 ; 
       scaled-mat23.1-0 ; 
       scaled-mat24.1-0 ; 
       scaled-mat25.1-0 ; 
       scaled-mat26.1-0 ; 
       scaled-mat27.1-0 ; 
       scaled-mat28.1-0 ; 
       scaled-mat29.1-0 ; 
       scaled-mat3.1-0 ; 
       scaled-mat36.1-0 ; 
       scaled-mat37.1-0 ; 
       scaled-mat4.1-0 ; 
       scaled-mat44.1-0 ; 
       scaled-mat46.1-0 ; 
       scaled-mat48.1-0 ; 
       scaled-mat49.1-0 ; 
       scaled-mat5.1-0 ; 
       scaled-mat50.1-0 ; 
       scaled-mat51.1-0 ; 
       scaled-mat52.1-0 ; 
       scaled-mat53.1-0 ; 
       scaled-mat54.1-0 ; 
       scaled-mat55.1-0 ; 
       scaled-mat56.1-0 ; 
       scaled-mat57.1-0 ; 
       scaled-mat58.1-0 ; 
       scaled-mat59.1-0 ; 
       scaled-mat6.1-0 ; 
       scaled-mat60.1-0 ; 
       scaled-mat61.1-0 ; 
       scaled-mat62.1-0 ; 
       scaled-mat63.1-0 ; 
       scaled-mat64.1-0 ; 
       scaled-mat65.1-0 ; 
       scaled-mat66.1-0 ; 
       scaled-mat7.1-0 ; 
       scaled-mat8.1-0 ; 
       scaled-mat83.1-0 ; 
       scaled-mat84.1-0 ; 
       scaled-mat85.1-0 ; 
       scaled-mat86.1-0 ; 
       scaled-mat87.1-0 ; 
       scaled-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       bom04-175deg.2-0 ROOT ; 
       bom04-175deg1.2-0 ROOT ; 
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04_3.10-0 ROOT ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-landgr0.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-LLa1.1-0 ; 
       bom04-LLa2.1-0 ; 
       bom04-LLl1.1-0 ; 
       bom04-LLl2.1-0 ; 
       bom04-LLl3.1-0 ; 
       bom04-LLr1.1-0 ; 
       bom04-LLr2.1-0 ; 
       bom04-LLr3.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-missemt.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt1.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-smoke.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
       bom04-thrust.1-0 ; 
       bom04-trail.1-0 ; 
       bom04-turwepemt1.1-0 ; 
       bom04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       scaled-t2d1.1-0 ; 
       scaled-t2d10.1-0 ; 
       scaled-t2d11.1-0 ; 
       scaled-t2d12.1-0 ; 
       scaled-t2d13.1-0 ; 
       scaled-t2d14.1-0 ; 
       scaled-t2d15.1-0 ; 
       scaled-t2d16.1-0 ; 
       scaled-t2d17.1-0 ; 
       scaled-t2d2.1-0 ; 
       scaled-t2d20.1-0 ; 
       scaled-t2d22.1-0 ; 
       scaled-t2d23.1-0 ; 
       scaled-t2d24.1-0 ; 
       scaled-t2d25.1-0 ; 
       scaled-t2d26.1-0 ; 
       scaled-t2d27.1-0 ; 
       scaled-t2d28.1-0 ; 
       scaled-t2d35.1-0 ; 
       scaled-t2d36.1-0 ; 
       scaled-t2d4.1-0 ; 
       scaled-t2d43.1-0 ; 
       scaled-t2d45.1-0 ; 
       scaled-t2d48.1-0 ; 
       scaled-t2d49.1-0 ; 
       scaled-t2d5.1-0 ; 
       scaled-t2d50.1-0 ; 
       scaled-t2d51.1-0 ; 
       scaled-t2d52.1-0 ; 
       scaled-t2d53.1-0 ; 
       scaled-t2d54.1-0 ; 
       scaled-t2d55.1-0 ; 
       scaled-t2d56.1-0 ; 
       scaled-t2d57.1-0 ; 
       scaled-t2d58.1-0 ; 
       scaled-t2d59.1-0 ; 
       scaled-t2d6.1-0 ; 
       scaled-t2d60.1-0 ; 
       scaled-t2d61.1-0 ; 
       scaled-t2d62.1-0 ; 
       scaled-t2d63.1-0 ; 
       scaled-t2d64.1-0 ; 
       scaled-t2d65.1-0 ; 
       scaled-t2d66.1-0 ; 
       scaled-t2d67.1-0 ; 
       scaled-t2d68.1-0 ; 
       scaled-t2d69.1-0 ; 
       scaled-t2d7.1-0 ; 
       scaled-t2d8.1-0 ; 
       scaled-t2d9.1-0 ; 
       scaled-zt2d21.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 8 110 ; 
       3 8 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 4 110 ; 
       8 7 110 ; 
       9 28 110 ; 
       10 27 110 ; 
       11 39 110 ; 
       12 38 110 ; 
       13 7 110 ; 
       14 8 110 ; 
       15 13 110 ; 
       16 15 110 ; 
       17 13 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 13 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 10 110 ; 
       26 9 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 7 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 8 110 ; 
       35 8 110 ; 
       36 12 110 ; 
       37 11 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 7 110 ; 
       41 40 110 ; 
       42 41 110 ; 
       43 4 110 ; 
       44 8 110 ; 
       45 8 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 31 110 ; 
       49 42 110 ; 
       50 8 110 ; 
       51 7 110 ; 
       52 7 110 ; 
       53 8 110 ; 
       54 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 5 300 ; 
       2 15 300 ; 
       2 25 300 ; 
       2 41 300 ; 
       2 42 300 ; 
       4 50 300 ; 
       6 48 300 ; 
       6 49 300 ; 
       8 28 300 ; 
       8 33 300 ; 
       8 44 300 ; 
       8 52 300 ; 
       8 53 300 ; 
       8 59 300 ; 
       8 6 300 ; 
       8 30 300 ; 
       8 51 300 ; 
       9 55 300 ; 
       10 56 300 ; 
       11 57 300 ; 
       12 58 300 ; 
       15 19 300 ; 
       15 47 300 ; 
       16 18 300 ; 
       16 46 300 ; 
       17 29 300 ; 
       18 24 300 ; 
       19 23 300 ; 
       20 20 300 ; 
       21 22 300 ; 
       22 21 300 ; 
       23 9 300 ; 
       23 40 300 ; 
       30 13 300 ; 
       30 14 300 ; 
       30 17 300 ; 
       30 26 300 ; 
       30 36 300 ; 
       30 37 300 ; 
       31 7 300 ; 
       31 38 300 ; 
       31 43 300 ; 
       34 10 300 ; 
       34 35 300 ; 
       34 39 300 ; 
       41 11 300 ; 
       41 12 300 ; 
       41 16 300 ; 
       41 27 300 ; 
       41 31 300 ; 
       41 32 300 ; 
       42 8 300 ; 
       42 34 300 ; 
       42 45 300 ; 
       44 1 300 ; 
       45 2 300 ; 
       46 3 300 ; 
       47 4 300 ; 
       48 0 300 ; 
       49 54 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       41 23 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 49 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 0 401 ; 
       16 10 401 ; 
       17 50 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 9 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 24 401 ; 
       32 26 401 ; 
       33 20 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 33 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 25 401 ; 
       45 37 401 ; 
       46 38 401 ; 
       47 39 401 ; 
       48 40 401 ; 
       49 41 401 ; 
       51 42 401 ; 
       52 36 401 ; 
       53 47 401 ; 
       55 43 401 ; 
       56 44 401 ; 
       57 45 401 ; 
       58 46 401 ; 
       59 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 46.87844 12.58692 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 0.8340005 0.8340005 0.8340005 0 0 0 1.606354e-014 0.8082325 1.285704 MPRFLG 0 ; 
       1 SCHEM 50.42503 12.97766 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 0.8340005 0.8340005 0.8340005 3.141593 0 0 -1.606354e-014 -0.8082325 1.285704 MPRFLG 0 ; 
       2 SCHEM 30 -6 0 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 MPRFLG 0 ; 
       4 SCHEM 43.75 0 0 SRT 1 1 1 0 0 1.570796 0 0 2.126869 MPRFLG 0 ; 
       5 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 65 -8 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 50 -8 0 MPRFLG 0 ; 
       13 SCHEM 15 -4 0 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 MPRFLG 0 ; 
       15 SCHEM 20 -6 0 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 MPRFLG 0 ; 
       17 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 10 -8 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 25 -6 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 65 -6 0 MPRFLG 0 ; 
       28 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 5 -4 0 MPRFLG 0 ; 
       30 SCHEM 5 -6 0 MPRFLG 0 ; 
       31 SCHEM 5 -8 0 MPRFLG 0 ; 
       32 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 35 -6 0 MPRFLG 0 ; 
       36 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 50 -6 0 MPRFLG 0 ; 
       39 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       45 SCHEM 55 -6 0 MPRFLG 0 ; 
       46 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       47 SCHEM 60 -6 0 MPRFLG 0 ; 
       48 SCHEM 5 -10 0 MPRFLG 0 ; 
       49 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       50 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 84 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
