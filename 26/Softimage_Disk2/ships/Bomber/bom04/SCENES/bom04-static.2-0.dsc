SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.13-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       static-mat1.1-0 ; 
       static-mat10.1-0 ; 
       static-mat11.1-0 ; 
       static-mat12.1-0 ; 
       static-mat13.1-0 ; 
       static-mat14.1-0 ; 
       static-mat15.1-0 ; 
       static-mat16.1-0 ; 
       static-mat17.1-0 ; 
       static-mat18.1-0 ; 
       static-mat2.1-0 ; 
       static-mat21.1-0 ; 
       static-mat22.1-0 ; 
       static-mat3.1-0 ; 
       static-mat36.1-0 ; 
       static-mat37.1-0 ; 
       static-mat4.1-0 ; 
       static-mat46.1-0 ; 
       static-mat48.1-0 ; 
       static-mat49.1-0 ; 
       static-mat5.1-0 ; 
       static-mat50.1-0 ; 
       static-mat51.1-0 ; 
       static-mat52.1-0 ; 
       static-mat53.1-0 ; 
       static-mat54.1-0 ; 
       static-mat55.1-0 ; 
       static-mat56.1-0 ; 
       static-mat57.1-0 ; 
       static-mat58.1-0 ; 
       static-mat59.1-0 ; 
       static-mat6.1-0 ; 
       static-mat60.1-0 ; 
       static-mat63.1-0 ; 
       static-mat64.1-0 ; 
       static-mat65.1-0 ; 
       static-mat66.1-0 ; 
       static-mat7.1-0 ; 
       static-mat8.1-0 ; 
       static-mat84.1-0 ; 
       static-mat85.1-0 ; 
       static-mat86.1-0 ; 
       static-mat87.1-0 ; 
       static-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04.10-0 ROOT ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt1.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-taturatt.1-0 ; 
       bom04-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       static-t2d1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d36.1-0 ; 
       static-t2d4.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d48.1-0 ; 
       static-t2d49.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d50.1-0 ; 
       static-t2d51.1-0 ; 
       static-t2d52.1-0 ; 
       static-t2d53.1-0 ; 
       static-t2d54.1-0 ; 
       static-t2d55.1-0 ; 
       static-t2d56.1-0 ; 
       static-t2d57.1-0 ; 
       static-t2d58.1-0 ; 
       static-t2d59.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d60.1-0 ; 
       static-t2d63.1-0 ; 
       static-t2d64.1-0 ; 
       static-t2d65.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
       static-t2d68.1-0 ; 
       static-t2d69.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
       static-zt2d21.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       3 2 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 17 110 ; 
       8 16 110 ; 
       9 27 110 ; 
       10 26 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 8 110 ; 
       15 7 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 5 110 ; 
       19 18 110 ; 
       20 19 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 6 110 ; 
       24 10 110 ; 
       25 9 110 ; 
       26 6 110 ; 
       27 6 110 ; 
       28 5 110 ; 
       29 28 110 ; 
       30 29 110 ; 
       31 6 110 ; 
       32 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 10 300 ; 
       0 13 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       2 35 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       6 16 300 ; 
       6 20 300 ; 
       6 31 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       6 43 300 ; 
       6 1 300 ; 
       6 17 300 ; 
       6 36 300 ; 
       7 39 300 ; 
       8 40 300 ; 
       9 41 300 ; 
       10 42 300 ; 
       12 4 300 ; 
       12 27 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       19 12 300 ; 
       19 14 300 ; 
       19 23 300 ; 
       19 24 300 ; 
       20 2 300 ; 
       20 25 300 ; 
       20 30 300 ; 
       22 5 300 ; 
       22 22 300 ; 
       22 26 300 ; 
       29 6 300 ; 
       29 7 300 ; 
       29 11 300 ; 
       29 15 300 ; 
       29 18 300 ; 
       29 19 300 ; 
       30 3 300 ; 
       30 21 300 ; 
       30 32 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       29 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 39 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 40 401 ; 
       13 9 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       17 14 401 ; 
       18 16 401 ; 
       19 18 401 ; 
       20 13 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 17 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       36 32 401 ; 
       37 28 401 ; 
       38 37 401 ; 
       39 33 401 ; 
       40 34 401 ; 
       41 35 401 ; 
       42 36 401 ; 
       43 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 35 0 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       3 SCHEM 53.63683 -10.11655 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 60 -8 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 45 -8 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 MPRFLG 0 ; 
       14 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 60 -6 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 MPRFLG 0 ; 
       22 SCHEM 25 -6 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 45 -6 0 MPRFLG 0 ; 
       28 SCHEM 5 -4 0 MPRFLG 0 ; 
       29 SCHEM 5 -6 0 MPRFLG 0 ; 
       30 SCHEM 5 -8 0 MPRFLG 0 ; 
       31 SCHEM 35 -6 0 MPRFLG 0 ; 
       32 SCHEM 56.13683 -10.11655 0 USR WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 72 72 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
