SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.21-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       rix_fig_F-mat75.2-0 ; 
       rix_fig_F-mat79.2-0 ; 
       rix_fig_F-mat80.2-0 ; 
       rix_fig_F-mat81.2-0 ; 
       rix_fig_F-mat82.2-0 ; 
       static-mat1.3-0 ; 
       static-mat10.3-0 ; 
       static-mat11.3-0 ; 
       static-mat12.3-0 ; 
       static-mat13.3-0 ; 
       static-mat14.3-0 ; 
       static-mat15.3-0 ; 
       static-mat16.3-0 ; 
       static-mat17.3-0 ; 
       static-mat18.3-0 ; 
       static-mat2.3-0 ; 
       static-mat21.3-0 ; 
       static-mat22.3-0 ; 
       static-mat3.3-0 ; 
       static-mat36.3-0 ; 
       static-mat37.3-0 ; 
       static-mat4.3-0 ; 
       static-mat46.3-0 ; 
       static-mat48.3-0 ; 
       static-mat49.3-0 ; 
       static-mat5.3-0 ; 
       static-mat50.3-0 ; 
       static-mat51.3-0 ; 
       static-mat52.3-0 ; 
       static-mat53.3-0 ; 
       static-mat54.3-0 ; 
       static-mat55.3-0 ; 
       static-mat56.3-0 ; 
       static-mat57.3-0 ; 
       static-mat58.3-0 ; 
       static-mat59.3-0 ; 
       static-mat6.3-0 ; 
       static-mat60.3-0 ; 
       static-mat63.3-0 ; 
       static-mat64.3-0 ; 
       static-mat65.3-0 ; 
       static-mat66.3-0 ; 
       static-mat7.3-0 ; 
       static-mat8.3-0 ; 
       static-mat83.2-0 ; 
       static-mat84.3-0 ; 
       static-mat85.3-0 ; 
       static-mat86.3-0 ; 
       static-mat87.3-0 ; 
       static-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       bom04-afinzzz.1-0 ; 
       bom04-baturatt.1-0 ; 
       bom04-bom04.18-0 ROOT ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-gun.2-0 ; 
       bom04-gun1.1-0 ; 
       bom04-gun2.1-0 ; 
       bom04-gun3.1-0 ; 
       bom04-lbturatt.1-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-ltturatt.1-0 ; 
       bom04-lwepemt1.1-0 ; 
       bom04-lwepemt2.1-0 ; 
       bom04-lwepmnt1.1-0 ; 
       bom04-lwepmnt2.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-missemt.1-0 ; 
       bom04-rbturatt.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rtturatt.1-0 ; 
       bom04-rwepemt1.1-0 ; 
       bom04-rwepemt2.1-0 ; 
       bom04-rwepmnt1.1-0 ; 
       bom04-rwepmnt2.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-SSr.1-0 ; 
       bom04-SSr1.1-0 ; 
       bom04-taturatt.1-0 ; 
       bom04-thrust.1-0 ; 
       bom04-trail.1-0 ; 
       bom04-turwepemt1.1-0 ; 
       bom04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       static-t2d1.3-0 ; 
       static-t2d10.3-0 ; 
       static-t2d11.3-0 ; 
       static-t2d12.3-0 ; 
       static-t2d13.3-0 ; 
       static-t2d14.3-0 ; 
       static-t2d15.3-0 ; 
       static-t2d16.3-0 ; 
       static-t2d17.3-0 ; 
       static-t2d2.3-0 ; 
       static-t2d20.3-0 ; 
       static-t2d35.3-0 ; 
       static-t2d36.3-0 ; 
       static-t2d4.3-0 ; 
       static-t2d45.3-0 ; 
       static-t2d48.3-0 ; 
       static-t2d49.3-0 ; 
       static-t2d5.3-0 ; 
       static-t2d50.3-0 ; 
       static-t2d51.3-0 ; 
       static-t2d52.3-0 ; 
       static-t2d53.3-0 ; 
       static-t2d54.3-0 ; 
       static-t2d55.3-0 ; 
       static-t2d56.3-0 ; 
       static-t2d57.3-0 ; 
       static-t2d58.3-0 ; 
       static-t2d59.3-0 ; 
       static-t2d6.3-0 ; 
       static-t2d60.3-0 ; 
       static-t2d63.3-0 ; 
       static-t2d64.3-0 ; 
       static-t2d65.3-0 ; 
       static-t2d66.3-0 ; 
       static-t2d67.3-0 ; 
       static-t2d68.3-0 ; 
       static-t2d69.3-0 ; 
       static-t2d7.3-0 ; 
       static-t2d8.3-0 ; 
       static-t2d9.3-0 ; 
       static-zt2d21.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       40 5 110 ; 
       41 6 110 ; 
       42 6 110 ; 
       21 6 110 ; 
       26 9 110 ; 
       25 10 110 ; 
       15 7 110 ; 
       0 6 110 ; 
       1 6 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 17 110 ; 
       8 16 110 ; 
       9 28 110 ; 
       10 27 110 ; 
       11 6 110 ; 
       14 8 110 ; 
       39 5 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 5 110 ; 
       19 18 110 ; 
       20 19 110 ; 
       22 6 110 ; 
       23 6 110 ; 
       24 6 110 ; 
       27 6 110 ; 
       28 6 110 ; 
       29 5 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 6 110 ; 
       33 6 110 ; 
       34 6 110 ; 
       35 6 110 ; 
       36 20 110 ; 
       37 31 110 ; 
       38 6 110 ; 
       3 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 15 300 ; 
       0 18 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       2 40 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       6 21 300 ; 
       6 25 300 ; 
       6 36 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       6 49 300 ; 
       6 6 300 ; 
       6 22 300 ; 
       6 41 300 ; 
       7 45 300 ; 
       8 46 300 ; 
       9 47 300 ; 
       10 48 300 ; 
       12 9 300 ; 
       12 32 300 ; 
       19 13 300 ; 
       19 14 300 ; 
       19 17 300 ; 
       19 19 300 ; 
       19 28 300 ; 
       19 29 300 ; 
       20 7 300 ; 
       20 30 300 ; 
       20 35 300 ; 
       23 10 300 ; 
       23 27 300 ; 
       23 31 300 ; 
       30 11 300 ; 
       30 12 300 ; 
       30 16 300 ; 
       30 20 300 ; 
       30 23 300 ; 
       30 24 300 ; 
       31 8 300 ; 
       31 26 300 ; 
       31 37 300 ; 
       32 1 300 ; 
       33 2 300 ; 
       34 3 300 ; 
       35 4 300 ; 
       36 0 300 ; 
       37 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       30 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 39 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 0 401 ; 
       16 10 401 ; 
       17 40 401 ; 
       18 9 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       25 13 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 17 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       41 32 401 ; 
       42 28 401 ; 
       43 37 401 ; 
       45 33 401 ; 
       46 34 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 36.25 0 0 SRT 1 1 1 0 3.141593 3.141592 0 0 0.1933552 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 50 -8 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 35 -8 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 50 -6 0 MPRFLG 0 ; 
       18 SCHEM 5 -4 0 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -8 0 MPRFLG 0 ; 
       22 SCHEM 30 -6 0 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 35 -6 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 40 -6 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 45 -6 0 MPRFLG 0 ; 
       35 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 5 -10 0 MPRFLG 0 ; 
       37 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 69 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
