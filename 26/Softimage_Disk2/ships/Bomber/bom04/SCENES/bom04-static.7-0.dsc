SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.28-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.28-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       static-mat1.4-0 ; 
       static-mat10.4-0 ; 
       static-mat11.4-0 ; 
       static-mat12.4-0 ; 
       static-mat13.4-0 ; 
       static-mat14.4-0 ; 
       static-mat15.4-0 ; 
       static-mat16.4-0 ; 
       static-mat17.4-0 ; 
       static-mat18.4-0 ; 
       static-mat2.4-0 ; 
       static-mat21.4-0 ; 
       static-mat22.4-0 ; 
       static-mat3.4-0 ; 
       static-mat36.4-0 ; 
       static-mat37.4-0 ; 
       static-mat4.4-0 ; 
       static-mat46.4-0 ; 
       static-mat48.4-0 ; 
       static-mat49.4-0 ; 
       static-mat5.4-0 ; 
       static-mat50.4-0 ; 
       static-mat51.4-0 ; 
       static-mat52.4-0 ; 
       static-mat53.4-0 ; 
       static-mat54.4-0 ; 
       static-mat55.4-0 ; 
       static-mat56.4-0 ; 
       static-mat57.4-0 ; 
       static-mat58.4-0 ; 
       static-mat59.4-0 ; 
       static-mat6.4-0 ; 
       static-mat60.4-0 ; 
       static-mat63.4-0 ; 
       static-mat64.4-0 ; 
       static-mat65.4-0 ; 
       static-mat66.4-0 ; 
       static-mat7.4-0 ; 
       static-mat8.4-0 ; 
       static-mat9.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       bom04-afinzzz.1-0 ; 
       bom04-bom04_3.7-0 ROOT ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-static.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       static-t2d1.4-0 ; 
       static-t2d10.4-0 ; 
       static-t2d11.4-0 ; 
       static-t2d12.4-0 ; 
       static-t2d13.4-0 ; 
       static-t2d14.4-0 ; 
       static-t2d15.4-0 ; 
       static-t2d16.4-0 ; 
       static-t2d17.4-0 ; 
       static-t2d2.4-0 ; 
       static-t2d20.4-0 ; 
       static-t2d35.4-0 ; 
       static-t2d36.4-0 ; 
       static-t2d4.4-0 ; 
       static-t2d45.4-0 ; 
       static-t2d48.4-0 ; 
       static-t2d49.4-0 ; 
       static-t2d5.4-0 ; 
       static-t2d50.4-0 ; 
       static-t2d51.4-0 ; 
       static-t2d52.4-0 ; 
       static-t2d53.4-0 ; 
       static-t2d54.4-0 ; 
       static-t2d55.4-0 ; 
       static-t2d56.4-0 ; 
       static-t2d57.4-0 ; 
       static-t2d58.4-0 ; 
       static-t2d59.4-0 ; 
       static-t2d6.4-0 ; 
       static-t2d60.4-0 ; 
       static-t2d63.4-0 ; 
       static-t2d64.4-0 ; 
       static-t2d65.4-0 ; 
       static-t2d7.4-0 ; 
       static-t2d8.4-0 ; 
       static-t2d9.4-0 ; 
       static-zt2d21.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 4 110 ; 
       10 3 110 ; 
       11 10 110 ; 
       12 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 10 300 ; 
       0 13 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       1 35 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       4 16 300 ; 
       4 20 300 ; 
       4 31 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 1 300 ; 
       4 17 300 ; 
       4 36 300 ; 
       5 4 300 ; 
       5 27 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 12 300 ; 
       7 14 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       8 2 300 ; 
       8 25 300 ; 
       8 30 300 ; 
       9 5 300 ; 
       9 22 300 ; 
       9 26 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 11 300 ; 
       11 15 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       12 3 300 ; 
       12 21 300 ; 
       12 32 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 35 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 36 401 ; 
       13 9 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       17 14 401 ; 
       18 16 401 ; 
       19 18 401 ; 
       20 13 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 17 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       36 32 401 ; 
       37 28 401 ; 
       38 33 401 ; 
       39 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 11.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 1.570796 0 0 0.1933552 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 64 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
