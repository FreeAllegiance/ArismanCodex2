SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       static-mat1.5-0 ; 
       static-mat10.5-0 ; 
       static-mat11.5-0 ; 
       static-mat12.5-0 ; 
       static-mat13.5-0 ; 
       static-mat14.5-0 ; 
       static-mat15.5-0 ; 
       static-mat16.5-0 ; 
       static-mat17.5-0 ; 
       static-mat18.5-0 ; 
       static-mat2.5-0 ; 
       static-mat21.5-0 ; 
       static-mat22.5-0 ; 
       static-mat3.5-0 ; 
       static-mat36.5-0 ; 
       static-mat37.5-0 ; 
       static-mat4.5-0 ; 
       static-mat46.5-0 ; 
       static-mat48.5-0 ; 
       static-mat49.5-0 ; 
       static-mat5.5-0 ; 
       static-mat50.5-0 ; 
       static-mat51.5-0 ; 
       static-mat52.5-0 ; 
       static-mat53.5-0 ; 
       static-mat54.5-0 ; 
       static-mat55.5-0 ; 
       static-mat56.5-0 ; 
       static-mat57.5-0 ; 
       static-mat58.5-0 ; 
       static-mat59.5-0 ; 
       static-mat6.5-0 ; 
       static-mat60.5-0 ; 
       static-mat63.5-0 ; 
       static-mat64.5-0 ; 
       static-mat65.5-0 ; 
       static-mat66.5-0 ; 
       static-mat7.5-0 ; 
       static-mat8.5-0 ; 
       static-mat9.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       bom04-afinzzz.1-0 ; 
       bom04-bom04_3.9-0 ROOT ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.1-0 ; 
       bom04-fuselg1.2-0 ; 
       bom04-lstrake.1-0 ; 
       bom04-lwingzz0.1-0 ; 
       bom04-lwingzz1.1-0 ; 
       bom04-lwingzz2.1-0 ; 
       bom04-rstrake.1-0 ; 
       bom04-rwingzz0.1-0 ; 
       bom04-rwingzz1.1-0 ; 
       bom04-rwingzz2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Bomber/bom04/PICTURES/bom04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom04-static.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       static-t2d1.5-0 ; 
       static-t2d10.5-0 ; 
       static-t2d11.5-0 ; 
       static-t2d12.5-0 ; 
       static-t2d13.5-0 ; 
       static-t2d14.5-0 ; 
       static-t2d15.5-0 ; 
       static-t2d16.5-0 ; 
       static-t2d17.5-0 ; 
       static-t2d2.5-0 ; 
       static-t2d20.5-0 ; 
       static-t2d35.5-0 ; 
       static-t2d36.5-0 ; 
       static-t2d4.5-0 ; 
       static-t2d45.5-0 ; 
       static-t2d48.5-0 ; 
       static-t2d49.5-0 ; 
       static-t2d5.5-0 ; 
       static-t2d50.5-0 ; 
       static-t2d51.5-0 ; 
       static-t2d52.5-0 ; 
       static-t2d53.5-0 ; 
       static-t2d54.5-0 ; 
       static-t2d55.5-0 ; 
       static-t2d56.5-0 ; 
       static-t2d57.5-0 ; 
       static-t2d58.5-0 ; 
       static-t2d59.5-0 ; 
       static-t2d6.5-0 ; 
       static-t2d60.5-0 ; 
       static-t2d63.5-0 ; 
       static-t2d64.5-0 ; 
       static-t2d65.5-0 ; 
       static-t2d7.5-0 ; 
       static-t2d8.5-0 ; 
       static-t2d9.5-0 ; 
       static-zt2d21.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 4 110 ; 
       10 3 110 ; 
       11 10 110 ; 
       12 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 10 300 ; 
       0 13 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       1 35 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       4 16 300 ; 
       4 20 300 ; 
       4 31 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 1 300 ; 
       4 17 300 ; 
       4 36 300 ; 
       5 4 300 ; 
       5 27 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 12 300 ; 
       7 14 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       8 2 300 ; 
       8 25 300 ; 
       8 30 300 ; 
       9 5 300 ; 
       9 22 300 ; 
       9 26 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 11 300 ; 
       11 15 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       12 3 300 ; 
       12 21 300 ; 
       12 32 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 15 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 35 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       12 36 401 ; 
       13 9 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       17 14 401 ; 
       18 16 401 ; 
       19 18 401 ; 
       20 13 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 17 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       36 32 401 ; 
       37 28 401 ; 
       38 33 401 ; 
       39 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 43.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 1.570796 0 0 2.126869 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 48.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
