SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.55-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       add_gun-default7.8-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.6-0 ; 
       add_gun-mat10.6-0 ; 
       add_gun-mat11.6-0 ; 
       add_gun-mat12.6-0 ; 
       add_gun-mat13.6-0 ; 
       add_gun-mat18.6-0 ; 
       add_gun-mat19.6-0 ; 
       add_gun-mat20.6-0 ; 
       add_gun-mat21.6-0 ; 
       add_gun-mat22.6-0 ; 
       add_gun-mat23.6-0 ; 
       add_gun-mat24.6-0 ; 
       add_gun-mat25.6-0 ; 
       add_gun-mat26.6-0 ; 
       add_gun-mat27.6-0 ; 
       add_gun-mat28.6-0 ; 
       add_gun-mat34.6-0 ; 
       add_gun-mat35.6-0 ; 
       add_gun-mat36.6-0 ; 
       add_gun-mat37.6-0 ; 
       add_gun-mat38.6-0 ; 
       add_gun-mat39.6-0 ; 
       add_gun-mat40.6-0 ; 
       add_gun-mat41.6-0 ; 
       add_gun-mat45.7-0 ; 
       add_gun-mat47.8-0 ; 
       add_gun-mat50.8-0 ; 
       add_gun-mat62.6-0 ; 
       add_gun-mat63.6-0 ; 
       add_gun-mat69.6-0 ; 
       add_gun-mat71.7-0 ; 
       add_gun-starbord_green-right.1-0.6-0 ; 
       nulls-0.3-0 ; 
       nulls-1.3-0 ; 
       nulls-mat109.3-0 ; 
       nulls-mat110.3-0 ; 
       nulls-mat111.3-0 ; 
       nulls-mat112.3-0 ; 
       nulls-mat113.3-0 ; 
       nulls-mat114.3-0 ; 
       nulls-mat115.3-0 ; 
       nulls-mat128.3-0 ; 
       nulls-mat95.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.3-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_gun-t2d10.6-0 ; 
       add_gun-t2d11.6-0 ; 
       add_gun-t2d15.6-0 ; 
       add_gun-t2d16.6-0 ; 
       add_gun-t2d17.6-0 ; 
       add_gun-t2d18.6-0 ; 
       add_gun-t2d19.6-0 ; 
       add_gun-t2d20.6-0 ; 
       add_gun-t2d21.6-0 ; 
       add_gun-t2d25.6-0 ; 
       add_gun-t2d26.6-0 ; 
       add_gun-t2d27.6-0 ; 
       add_gun-t2d28.6-0 ; 
       add_gun-t2d29.6-0 ; 
       add_gun-t2d30.6-0 ; 
       add_gun-t2d35.10-0 ; 
       add_gun-t2d38.10-0 ; 
       add_gun-t2d49.7-0 ; 
       add_gun-t2d50.8-0 ; 
       add_gun-t2d9.6-0 ; 
       nulls-t2d105.3-0 ; 
       nulls-t2d106.3-0 ; 
       nulls-t2d107.3-0 ; 
       nulls-t2d108.3-0 ; 
       nulls-t2d109.3-0 ; 
       nulls-t2d110.3-0 ; 
       nulls-t2d111.3-0 ; 
       nulls-t2d112.3-0 ; 
       nulls-t2d113.3-0 ; 
       nulls-t2d122.3-0 ; 
       nulls-t2d95.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       31 3 110 ; 
       29 3 110 ; 
       1 3 110 ; 
       5 3 110 ; 
       7 13 110 ; 
       6 0 110 ; 
       15 9 110 ; 
       0 22 110 ; 
       3 13 110 ; 
       4 22 110 ; 
       8 14 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 11 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 11 110 ; 
       24 19 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 13 110 ; 
       28 13 110 ; 
       30 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 31 300 ; 
       0 39 300 ; 
       4 43 300 ; 
       4 33 300 ; 
       8 35 300 ; 
       8 38 300 ; 
       9 36 300 ; 
       10 37 300 ; 
       12 40 300 ; 
       13 0 300 ; 
       13 26 300 ; 
       13 27 300 ; 
       13 41 300 ; 
       13 34 300 ; 
       16 25 300 ; 
       16 42 300 ; 
       17 21 300 ; 
       17 22 300 ; 
       17 23 300 ; 
       17 24 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 17 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       20 10 300 ; 
       20 11 300 ; 
       20 12 300 ; 
       20 13 300 ; 
       22 30 300 ; 
       23 2 300 ; 
       23 3 300 ; 
       23 4 300 ; 
       23 5 300 ; 
       24 14 300 ; 
       24 15 300 ; 
       24 16 300 ; 
       25 29 300 ; 
       26 28 300 ; 
       27 1 300 ; 
       28 32 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 19 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       19 10 401 ; 
       20 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       33 25 401 ; 
       34 28 401 ; 
       35 20 401 ; 
       36 21 401 ; 
       37 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 29 401 ; 
       43 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       31 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 0 -12 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 15 -12 0 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 30 -10 0 MPRFLG 0 ; 
       19 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       20 SCHEM 10 -12 0 MPRFLG 0 ; 
       21 SCHEM 5 -10 0 MPRFLG 0 ; 
       22 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 25 -8 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
