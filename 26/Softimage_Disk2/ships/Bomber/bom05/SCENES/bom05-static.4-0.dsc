SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.57-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       add_gun-default7.8-0 ; 
       add_gun-mat10.6-0 ; 
       add_gun-mat11.6-0 ; 
       add_gun-mat12.6-0 ; 
       add_gun-mat13.6-0 ; 
       add_gun-mat18.6-0 ; 
       add_gun-mat19.6-0 ; 
       add_gun-mat20.6-0 ; 
       add_gun-mat21.6-0 ; 
       add_gun-mat22.6-0 ; 
       add_gun-mat23.6-0 ; 
       add_gun-mat24.6-0 ; 
       add_gun-mat25.6-0 ; 
       add_gun-mat26.6-0 ; 
       add_gun-mat27.6-0 ; 
       add_gun-mat28.6-0 ; 
       add_gun-mat34.6-0 ; 
       add_gun-mat35.6-0 ; 
       add_gun-mat36.6-0 ; 
       add_gun-mat37.6-0 ; 
       add_gun-mat38.6-0 ; 
       add_gun-mat39.6-0 ; 
       add_gun-mat40.6-0 ; 
       add_gun-mat41.6-0 ; 
       add_gun-mat45.7-0 ; 
       add_gun-mat47.8-0 ; 
       add_gun-mat50.8-0 ; 
       nulls-1.3-0 ; 
       nulls-mat114.3-0 ; 
       nulls-mat115.3-0 ; 
       nulls-mat128.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       nulls-bom05.4-0 ROOT ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       add_gun-t2d10.6-0 ; 
       add_gun-t2d11.6-0 ; 
       add_gun-t2d15.6-0 ; 
       add_gun-t2d16.6-0 ; 
       add_gun-t2d17.6-0 ; 
       add_gun-t2d18.6-0 ; 
       add_gun-t2d19.6-0 ; 
       add_gun-t2d20.6-0 ; 
       add_gun-t2d21.6-0 ; 
       add_gun-t2d25.6-0 ; 
       add_gun-t2d26.6-0 ; 
       add_gun-t2d27.6-0 ; 
       add_gun-t2d28.6-0 ; 
       add_gun-t2d29.6-0 ; 
       add_gun-t2d30.6-0 ; 
       add_gun-t2d35.10-0 ; 
       add_gun-t2d38.10-0 ; 
       add_gun-t2d9.6-0 ; 
       nulls-t2d111.3-0 ; 
       nulls-t2d112.3-0 ; 
       nulls-t2d113.3-0 ; 
       nulls-t2d122.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 1 110 ; 
       10 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 28 300 ; 
       3 0 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 29 300 ; 
       3 27 300 ; 
       4 24 300 ; 
       4 30 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 17 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 20 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
