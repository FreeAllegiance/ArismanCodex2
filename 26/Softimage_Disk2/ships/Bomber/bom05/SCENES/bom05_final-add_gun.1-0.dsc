SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat45.1-0 ; 
       add_gun-mat46.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat53.1-0 ; 
       add_gun-mat54.1-0 ; 
       add_gun-mat55.1-0 ; 
       add_gun-mat56.1-0 ; 
       add_gun-mat57.1-0 ; 
       add_gun-mat58.1-0 ; 
       add_gun-mat60.1-0 ; 
       add_gun-mat61.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       add_gun-cyl1.1-0 ROOT ; 
       bom05-blthrust.1-0 ; 
       bom05-bom05.3-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ROOT ; 
       bom05-cockpt.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
       dur_bomber_F1-cyl1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-add_gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d33.1-0 ; 
       add_gun-t2d35.1-0 ; 
       add_gun-t2d38.1-0 ; 
       add_gun-t2d40.1-0 ; 
       add_gun-t2d41.1-0 ; 
       add_gun-t2d42.1-0 ; 
       add_gun-t2d43.1-0 ; 
       add_gun-t2d44.1-0 ; 
       add_gun-t2d45.1-0 ; 
       add_gun-t2d46.1-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       20 11 110 ; 
       1 3 110 ; 
       3 9 110 ; 
       4 3 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 7 110 ; 
       14 12 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 9 110 ; 
       21 7 110 ; 
       22 14 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 3 110 ; 
       29 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 37 300 ; 
       8 38 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       8 42 300 ; 
       9 0 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       11 31 300 ; 
       11 32 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 26 300 ; 
       12 27 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       15 30 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       18 45 300 ; 
       18 46 300 ; 
       18 47 300 ; 
       21 2 300 ; 
       21 3 300 ; 
       21 4 300 ; 
       21 5 300 ; 
       22 14 300 ; 
       22 15 300 ; 
       22 16 300 ; 
       24 44 300 ; 
       25 43 300 ; 
       26 1 300 ; 
       27 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       17 10 400 ; 
       18 29 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 31 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       20 SCHEM 0 -6.363465 0 MPRFLG 0 ; 
       30 SCHEM 55 0 0 SRT 0.6219999 0.6219999 0.848408 0 0 -0.28 0.04443415 -0.3130249 1.881639 MPRFLG 0 ; 
       1 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 23.38376 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 50 -10 0 MPRFLG 0 ; 
       5 SCHEM 0 -10.36347 0 SRT 1 1 1 0 1.192093e-007 0 -2.910383e-011 -0.3122452 2.421417 MPRFLG 0 ; 
       6 SCHEM 40 -8 0 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 23.38376 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 0 -4.363465 0 USR MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 5 -12 0 MPRFLG 0 ; 
       17 SCHEM 10 -12 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 25 -8 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 30 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 45 -10 0 MPRFLG 0 ; 
       29 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       0 SCHEM 52.5 0 0 SRT 0.6219999 0.6219999 0.848408 0 0 -0.28 -0.0402675 -0.3130249 1.881639 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -6.363465 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -6.363465 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8.363465 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49.88376 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 85 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
