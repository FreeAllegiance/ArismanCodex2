SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat45.1-0 ; 
       add_gun-mat46.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat53.1-0 ; 
       add_gun-mat54.1-0 ; 
       add_gun-mat55.1-0 ; 
       add_gun-mat56.1-0 ; 
       add_gun-mat57.1-0 ; 
       add_gun-mat58.1-0 ; 
       add_gun-mat60.1-0 ; 
       add_gun-mat61.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-mat72.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       bom05-blthrust.1-0 ; 
       bom05-bom05.5-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl1.1-0 ; 
       bom05-cyl2.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-add_gun.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d33.1-0 ; 
       add_gun-t2d35.1-0 ; 
       add_gun-t2d38.1-0 ; 
       add_gun-t2d40.1-0 ; 
       add_gun-t2d41.1-0 ; 
       add_gun-t2d42.1-0 ; 
       add_gun-t2d43.1-0 ; 
       add_gun-t2d44.1-0 ; 
       add_gun-t2d45.1-0 ; 
       add_gun-t2d46.1-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.2-0 ; 
       add_gun-t2d50.1-0 ; 
       add_gun-t2d51.1-0 ; 
       add_gun-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 12 110 ; 
       0 2 110 ; 
       2 10 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 10 110 ; 
       8 10 110 ; 
       9 8 110 ; 
       10 1 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 8 110 ; 
       15 13 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 10 110 ; 
       22 8 110 ; 
       23 15 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       6 21 110 ; 
       7 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 48 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 39 300 ; 
       9 40 300 ; 
       9 41 300 ; 
       9 42 300 ; 
       10 0 300 ; 
       10 33 300 ; 
       10 34 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       13 26 300 ; 
       13 27 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       15 22 300 ; 
       15 23 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       17 12 300 ; 
       17 13 300 ; 
       18 17 300 ; 
       18 18 300 ; 
       18 19 300 ; 
       19 45 300 ; 
       19 46 300 ; 
       19 47 300 ; 
       22 2 300 ; 
       22 3 300 ; 
       22 4 300 ; 
       22 5 300 ; 
       23 14 300 ; 
       23 15 300 ; 
       23 16 300 ; 
       25 44 300 ; 
       26 43 300 ; 
       27 1 300 ; 
       28 51 300 ; 
       6 49 300 ; 
       7 50 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       18 10 400 ; 
       19 29 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 34 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 30 401 ; 
       48 31 401 ; 
       50 33 401 ; 
       49 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 2.068224 -6 0 MPRFLG 0 ; 
       0 SCHEM 50.81822 -6 0 MPRFLG 0 ; 
       1 SCHEM 27.06822 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 49.56822 -4 0 MPRFLG 0 ; 
       3 SCHEM 53.31822 -6 0 MPRFLG 0 ; 
       4 SCHEM 3.318224 -10 0 MPRFLG 0 ; 
       5 SCHEM 43.31822 -4 0 MPRFLG 0 ; 
       8 SCHEM 38.31822 -4 0 MPRFLG 0 ; 
       9 SCHEM 35.81822 -6 0 MPRFLG 0 ; 
       10 SCHEM 27.06822 -2 0 MPRFLG 0 ; 
       11 SCHEM 23.31822 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.068224 -4 0 MPRFLG 0 ; 
       13 SCHEM 10.81822 -4 0 MPRFLG 0 ; 
       14 SCHEM 38.31822 -6 0 MPRFLG 0 ; 
       15 SCHEM 10.81822 -6 0 MPRFLG 0 ; 
       16 SCHEM 15.81822 -8 0 MPRFLG 0 ; 
       17 SCHEM 8.318224 -8 0 MPRFLG 0 ; 
       18 SCHEM 13.31822 -8 0 MPRFLG 0 ; 
       19 SCHEM 10.81822 -8 0 MPRFLG 0 ; 
       20 SCHEM 18.31822 -4 0 MPRFLG 0 ; 
       22 SCHEM 40.81822 -6 0 MPRFLG 0 ; 
       23 SCHEM 5.818224 -8 0 MPRFLG 0 ; 
       24 SCHEM 20.81822 -4 0 MPRFLG 0 ; 
       25 SCHEM 25.81822 -4 0 MPRFLG 0 ; 
       26 SCHEM 28.31822 -4 0 MPRFLG 0 ; 
       27 SCHEM 30.81822 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 33.31822 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 48.31822 -6 0 MPRFLG 0 ; 
       30 SCHEM 45.81822 -6 0 MPRFLG 0 ; 
       6 SCHEM 3.318224 -8 0 MPRFLG 0 ; 
       7 SCHEM 0.818224 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54.81822 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29.81822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 37.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.318224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.318224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.318224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 7.318224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 12.31822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.31822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.31822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 17.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 17.31822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 17.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 17.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14.81822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14.81822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14.81822 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4.818224 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4.818224 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54.81822 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54.81822 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 54.81822 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 54.81822 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34.81822 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 27.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24.81822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 32.31822 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4.818224 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM -0.181776 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 4.818224 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.318224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.318224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4.818224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4.818224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.31822 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.31822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.31822 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.31822 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.31822 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14.81822 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14.81822 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4.818224 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54.81822 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54.81822 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54.81822 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 54.81822 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 34.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9.818224 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9.818224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 39.81822 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4.818224 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4.818224 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM -0.181776 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54.81822 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 85 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
