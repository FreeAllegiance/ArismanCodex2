SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.22-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat45.1-0 ; 
       add_gun-mat46.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat53.1-0 ; 
       add_gun-mat54.1-0 ; 
       add_gun-mat55.1-0 ; 
       add_gun-mat56.1-0 ; 
       add_gun-mat57.1-0 ; 
       add_gun-mat58.1-0 ; 
       add_gun-mat60.1-0 ; 
       add_gun-mat61.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
       add_turret-mat81.1-0 ; 
       add_turret-mat82.1-0 ; 
       add_turret-mat84.1-0 ; 
       add_turret-mat85.1-0 ; 
       add_turret-mat86.1-0 ; 
       add_turret-mat88.1-0 ; 
       add_turret-mat90.1-0 ; 
       add_turret-mat91.1-0 ; 
       add_turret-mat92.1-0 ; 
       add_turret-mat93.1-0 ; 
       add_turret-mat94.1-0 ; 
       add_turret-mat95.1-0 ; 
       add_turret-mat96.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       bom05-acs63_1.1-0 ; 
       bom05-antenn1.1-0 ; 
       bom05-antenn2.1-0 ; 
       bom05-antenn3.1-0 ; 
       bom05-blthrust.1-0 ; 
       bom05-bom05.20-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl1.1-0 ; 
       bom05-cyl3.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-lwepbas_3.1-0 ; 
       bom05-Lwingzz.1-0 ; 
       bom05-Lwingzz1.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-rwepbas_3.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
       bom05-turret1.1-0 ; 
       bom05-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-add_turret.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d33.1-0 ; 
       add_gun-t2d35.1-0 ; 
       add_gun-t2d38.1-0 ; 
       add_gun-t2d40.1-0 ; 
       add_gun-t2d41.1-0 ; 
       add_gun-t2d42.1-0 ; 
       add_gun-t2d43.1-0 ; 
       add_gun-t2d44.1-0 ; 
       add_gun-t2d45.1-0 ; 
       add_gun-t2d46.1-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.2-0 ; 
       add_gun-t2d50.2-0 ; 
       add_gun-t2d9.1-0 ; 
       add_turret-t2d87.9-0 ; 
       add_turret-t2d89.9-0 ; 
       add_turret-t2d90.9-0 ; 
       add_turret-t2d91.4-0 ; 
       add_turret-t2d92.4-0 ; 
       add_turret-t2d93.4-0 ; 
       add_turret-t2d94.4-0 ; 
       add_turret-t2d95.1-0 ; 
       add_turret-t2d96.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 26 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       6 14 110 ; 
       7 6 110 ; 
       8 10 110 ; 
       9 14 110 ; 
       10 28 110 ; 
       11 28 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 5 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 12 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 14 110 ; 
       25 39 110 ; 
       26 39 110 ; 
       28 16 110 ; 
       29 12 110 ; 
       30 19 110 ; 
       31 14 110 ; 
       32 39 110 ; 
       33 14 110 ; 
       34 14 110 ; 
       35 14 110 ; 
       36 14 110 ; 
       37 6 110 ; 
       38 6 110 ; 
       39 0 110 ; 
       40 32 110 ; 
       27 39 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 51 300 ; 
       1 53 300 ; 
       2 54 300 ; 
       3 55 300 ; 
       10 49 300 ; 
       11 62 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       13 39 300 ; 
       13 40 300 ; 
       13 41 300 ; 
       13 42 300 ; 
       14 0 300 ; 
       14 33 300 ; 
       14 34 300 ; 
       14 35 300 ; 
       14 36 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 45 300 ; 
       23 46 300 ; 
       23 47 300 ; 
       25 56 300 ; 
       26 61 300 ; 
       28 48 300 ; 
       29 2 300 ; 
       29 3 300 ; 
       29 4 300 ; 
       29 5 300 ; 
       30 14 300 ; 
       30 15 300 ; 
       30 16 300 ; 
       32 57 300 ; 
       33 44 300 ; 
       34 43 300 ; 
       35 1 300 ; 
       36 50 300 ; 
       39 52 300 ; 
       39 59 300 ; 
       39 60 300 ; 
       40 58 300 ; 
       27 63 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 29 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 33 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 30 401 ; 
       48 31 401 ; 
       49 32 401 ; 
       52 34 401 ; 
       53 38 401 ; 
       54 39 401 ; 
       55 40 401 ; 
       59 35 401 ; 
       60 36 401 ; 
       61 37 401 ; 
       62 41 401 ; 
       63 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 93.75 -8 0 DISPLAY 3 2 MPRFLG 0 ; 
       1 SCHEM 82.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 81.25 -16 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 80 -18 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 170 -10 0 MPRFLG 0 ; 
       5 SCHEM 92.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 168.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 172.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 0 -14 0 MPRFLG 0 ; 
       9 SCHEM 162.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 1.25 -12 0 MPRFLG 0 ; 
       11 SCHEM 5 -12 0 MPRFLG 0 ; 
       12 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 133.75 -10 0 MPRFLG 0 ; 
       14 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 115 -8 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       17 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 146.25 -10 0 MPRFLG 0 ; 
       19 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 55 -12 0 MPRFLG 0 ; 
       21 SCHEM 26.25 -12 0 MPRFLG 0 ; 
       22 SCHEM 46.25 -12 0 MPRFLG 0 ; 
       23 SCHEM 36.25 -12 0 MPRFLG 0 ; 
       24 SCHEM 110 -8 0 MPRFLG 0 ; 
       25 SCHEM 97.5 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 83.75 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       29 SCHEM 156.25 -10 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       31 SCHEM 112.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 93.75 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 120 -8 0 MPRFLG 0 ; 
       35 SCHEM 122.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 125 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 167.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 165 -10 0 MPRFLG 0 ; 
       39 SCHEM 92.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 92.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 90 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 160 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 152.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 155 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 32.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 2.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 85 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 82.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 80 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 92.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 155 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 157.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 147.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 55 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 135 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 137.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 152.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 87.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 85 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 82.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 80 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 90 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 186.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
