SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.51-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.49-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 53     
       add_gun-default7.5-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat45.4-0 ; 
       add_gun-mat47.5-0 ; 
       add_gun-mat50.5-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-mat66.3-0 ; 
       add_gun-mat67.3-0 ; 
       add_gun-mat68.3-0 ; 
       add_gun-mat69.3-0 ; 
       add_gun-mat71.4-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       check_nulls-0.1-0 ; 
       check_nulls-1.1-0 ; 
       check_nulls-mat109.1-0 ; 
       check_nulls-mat110.1-0 ; 
       check_nulls-mat111.1-0 ; 
       check_nulls-mat112.1-0 ; 
       check_nulls-mat113.1-0 ; 
       check_nulls-mat114.1-0 ; 
       check_nulls-mat115.1-0 ; 
       check_nulls-mat128.1-0 ; 
       check_nulls-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       bom05-blgun.1-0 ; 
       bom05-blthrust.1-0 ; 
       bom05-blwepemt.1-0 ; 
       bom05-bom05.46-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brgun.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-brwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl4.1-0 ; 
       bom05-face2.1-0 ; 
       bom05-face3.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-fwepemt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-thrust.1-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-check_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       add_gun-t2d10.3-0 ; 
       add_gun-t2d11.3-0 ; 
       add_gun-t2d15.3-0 ; 
       add_gun-t2d16.3-0 ; 
       add_gun-t2d17.3-0 ; 
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d28.3-0 ; 
       add_gun-t2d29.3-0 ; 
       add_gun-t2d30.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       add_gun-t2d35.7-0 ; 
       add_gun-t2d38.7-0 ; 
       add_gun-t2d47.3-0 ; 
       add_gun-t2d48.3-0 ; 
       add_gun-t2d49.4-0 ; 
       add_gun-t2d50.5-0 ; 
       add_gun-t2d9.3-0 ; 
       check_nulls-t2d105.1-0 ; 
       check_nulls-t2d106.1-0 ; 
       check_nulls-t2d107.1-0 ; 
       check_nulls-t2d108.1-0 ; 
       check_nulls-t2d109.1-0 ; 
       check_nulls-t2d110.1-0 ; 
       check_nulls-t2d111.1-0 ; 
       check_nulls-t2d112.1-0 ; 
       check_nulls-t2d113.1-0 ; 
       check_nulls-t2d122.1-0 ; 
       check_nulls-t2d95.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 25 110 ; 
       1 4 110 ; 
       2 0 110 ; 
       4 14 110 ; 
       5 25 110 ; 
       6 4 110 ; 
       7 5 110 ; 
       8 14 110 ; 
       9 15 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 3 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 12 110 ; 
       20 18 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 17 110 ; 
       26 12 110 ; 
       27 20 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 14 110 ; 
       32 14 110 ; 
       33 4 110 ; 
       34 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 40 300 ; 
       0 48 300 ; 
       5 52 300 ; 
       5 42 300 ; 
       9 44 300 ; 
       9 47 300 ; 
       10 45 300 ; 
       11 46 300 ; 
       13 49 300 ; 
       14 0 300 ; 
       14 32 300 ; 
       14 33 300 ; 
       14 50 300 ; 
       14 43 300 ; 
       17 31 300 ; 
       17 51 300 ; 
       18 24 300 ; 
       18 25 300 ; 
       18 26 300 ; 
       18 27 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       20 20 300 ; 
       20 21 300 ; 
       20 22 300 ; 
       20 23 300 ; 
       21 28 300 ; 
       21 29 300 ; 
       21 30 300 ; 
       22 10 300 ; 
       22 11 300 ; 
       22 12 300 ; 
       22 13 300 ; 
       23 17 300 ; 
       23 18 300 ; 
       23 19 300 ; 
       24 36 300 ; 
       24 37 300 ; 
       24 38 300 ; 
       25 39 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       27 16 300 ; 
       28 35 300 ; 
       29 34 300 ; 
       30 1 300 ; 
       31 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       23 10 400 ; 
       24 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       51 35 401 ; 
       52 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 70 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 68.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       9 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 40 -8 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 55 -6 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 10 -8 0 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 MPRFLG 0 ; 
       24 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 60 -6 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 MPRFLG 0 ; 
       29 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 50 -4 0 MPRFLG 0 ; 
       31 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 76.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 101 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
