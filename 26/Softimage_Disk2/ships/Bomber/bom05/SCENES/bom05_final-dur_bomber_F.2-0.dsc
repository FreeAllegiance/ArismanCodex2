SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       dur_bomber_F-aWindow.1-0 ; 
       dur_bomber_F-cam_int1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       dur_bomber_F-default7.1-0 ; 
       dur_bomber_F-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       dur_bomber_F-mat10.1-0 ; 
       dur_bomber_F-mat11.1-0 ; 
       dur_bomber_F-mat12.1-0 ; 
       dur_bomber_F-mat13.1-0 ; 
       dur_bomber_F-mat18.1-0 ; 
       dur_bomber_F-mat19.1-0 ; 
       dur_bomber_F-mat20.1-0 ; 
       dur_bomber_F-mat21.1-0 ; 
       dur_bomber_F-mat22.1-0 ; 
       dur_bomber_F-mat23.1-0 ; 
       dur_bomber_F-mat24.1-0 ; 
       dur_bomber_F-mat25.1-0 ; 
       dur_bomber_F-mat26.1-0 ; 
       dur_bomber_F-mat27.1-0 ; 
       dur_bomber_F-mat28.1-0 ; 
       dur_bomber_F-mat29.1-0 ; 
       dur_bomber_F-mat30.1-0 ; 
       dur_bomber_F-mat31.1-0 ; 
       dur_bomber_F-mat34.1-0 ; 
       dur_bomber_F-mat35.1-0 ; 
       dur_bomber_F-mat36.1-0 ; 
       dur_bomber_F-mat37.1-0 ; 
       dur_bomber_F-mat38.1-0 ; 
       dur_bomber_F-mat39.1-0 ; 
       dur_bomber_F-mat40.1-0 ; 
       dur_bomber_F-mat41.1-0 ; 
       dur_bomber_F-mat42.1-0 ; 
       dur_bomber_F-mat43.1-0 ; 
       dur_bomber_F-mat44.1-0 ; 
       dur_bomber_F-mat45.1-0 ; 
       dur_bomber_F-mat46.1-0 ; 
       dur_bomber_F-mat47.1-0 ; 
       dur_bomber_F-mat50.1-0 ; 
       dur_bomber_F-mat53.1-0 ; 
       dur_bomber_F-mat54.1-0 ; 
       dur_bomber_F-mat55.1-0 ; 
       dur_bomber_F-mat56.1-0 ; 
       dur_bomber_F-mat57.1-0 ; 
       dur_bomber_F-mat58.1-0 ; 
       dur_bomber_F-mat60.1-0 ; 
       dur_bomber_F-mat61.1-0 ; 
       dur_bomber_F-mat62.1-0 ; 
       dur_bomber_F-mat63.1-0 ; 
       dur_bomber_F-mat66.1-0 ; 
       dur_bomber_F-mat67.1-0 ; 
       dur_bomber_F-mat68.1-0 ; 
       dur_bomber_F-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       bom05-blthrust.1-0 ; 
       bom05-bom05.2-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ROOT ; 
       bom05-cockpt.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
       dur_bomber_F-cyl1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-dur_bomber_F.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       dur_bomber_F-t2d10.1-0 ; 
       dur_bomber_F-t2d11.1-0 ; 
       dur_bomber_F-t2d15.1-0 ; 
       dur_bomber_F-t2d16.1-0 ; 
       dur_bomber_F-t2d17.1-0 ; 
       dur_bomber_F-t2d18.1-0 ; 
       dur_bomber_F-t2d19.1-0 ; 
       dur_bomber_F-t2d20.1-0 ; 
       dur_bomber_F-t2d21.1-0 ; 
       dur_bomber_F-t2d22.1-0 ; 
       dur_bomber_F-t2d23.1-0 ; 
       dur_bomber_F-t2d25.1-0 ; 
       dur_bomber_F-t2d26.1-0 ; 
       dur_bomber_F-t2d27.1-0 ; 
       dur_bomber_F-t2d28.1-0 ; 
       dur_bomber_F-t2d29.1-0 ; 
       dur_bomber_F-t2d30.1-0 ; 
       dur_bomber_F-t2d31.1-0 ; 
       dur_bomber_F-t2d32.1-0 ; 
       dur_bomber_F-t2d33.1-0 ; 
       dur_bomber_F-t2d35.1-0 ; 
       dur_bomber_F-t2d38.1-0 ; 
       dur_bomber_F-t2d40.1-0 ; 
       dur_bomber_F-t2d41.1-0 ; 
       dur_bomber_F-t2d42.1-0 ; 
       dur_bomber_F-t2d43.1-0 ; 
       dur_bomber_F-t2d44.1-0 ; 
       dur_bomber_F-t2d45.1-0 ; 
       dur_bomber_F-t2d46.1-0 ; 
       dur_bomber_F-t2d47.1-0 ; 
       dur_bomber_F-t2d48.1-0 ; 
       dur_bomber_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       19 10 110 ; 
       0 2 110 ; 
       2 8 110 ; 
       3 2 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 6 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 8 110 ; 
       20 6 110 ; 
       21 13 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 2 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 37 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 42 300 ; 
       8 0 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       8 35 300 ; 
       8 36 300 ; 
       10 31 300 ; 
       10 32 300 ; 
       11 24 300 ; 
       11 25 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       14 28 300 ; 
       14 29 300 ; 
       14 30 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       15 13 300 ; 
       16 17 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       17 45 300 ; 
       17 46 300 ; 
       17 47 300 ; 
       20 2 300 ; 
       20 3 300 ; 
       20 4 300 ; 
       20 5 300 ; 
       21 14 300 ; 
       21 15 300 ; 
       21 16 300 ; 
       23 44 300 ; 
       24 43 300 ; 
       25 1 300 ; 
       26 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       16 10 400 ; 
       17 29 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 31 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       19 SCHEM 0 -6.363465 0 MPRFLG 0 ; 
       0 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 23.38376 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 50 -10 0 MPRFLG 0 ; 
       4 SCHEM 0 -10.36347 0 SRT 1 1 1 0 1.192093e-007 0 -2.910383e-011 -0.3122452 2.421417 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 MPRFLG 0 ; 
       6 SCHEM 35 -8 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 23.38376 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -8 0 MPRFLG 0 ; 
       10 SCHEM 0 -4.363465 0 USR MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 35 -10 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       15 SCHEM 5 -12 0 MPRFLG 0 ; 
       16 SCHEM 10 -12 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 MPRFLG 0 ; 
       20 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 30 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 45 -10 0 MPRFLG 0 ; 
       28 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 52.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 -0.3130249 1.895418 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -6.363465 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -6.363465 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8.363465 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49.88376 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 85 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
