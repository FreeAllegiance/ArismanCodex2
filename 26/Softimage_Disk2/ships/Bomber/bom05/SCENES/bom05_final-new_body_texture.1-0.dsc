SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.47-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 53     
       add_gun-default7.4-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.2-0 ; 
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat45.2-0 ; 
       add_gun-mat46.2-0 ; 
       add_gun-mat47.4-0 ; 
       add_gun-mat50.4-0 ; 
       add_gun-mat62.2-0 ; 
       add_gun-mat63.2-0 ; 
       add_gun-mat66.2-0 ; 
       add_gun-mat67.2-0 ; 
       add_gun-mat68.2-0 ; 
       add_gun-mat69.2-0 ; 
       add_gun-mat71.3-0 ; 
       add_gun-starbord_green-right.1-0.2-0 ; 
       new_body_texture-0.1-0 ; 
       new_body_texture-1.1-0 ; 
       new_body_texture-mat109.1-0 ; 
       new_body_texture-mat110.1-0 ; 
       new_body_texture-mat111.1-0 ; 
       new_body_texture-mat112.1-0 ; 
       new_body_texture-mat113.1-0 ; 
       new_body_texture-mat114.1-0 ; 
       new_body_texture-mat115.2-0 ; 
       new_body_texture-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       bom05-blthrust.1-0 ; 
       bom05-bom05.42-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl1.1-0 ; 
       bom05-cyl3.1-0 ; 
       bom05-cyl4.1-0 ; 
       bom05-face2.1-0 ; 
       bom05-face3.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-fwepemt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-new_body_texture.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d33.2-0 ; 
       add_gun-t2d35.6-0 ; 
       add_gun-t2d38.6-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_gun-t2d49.3-0 ; 
       add_gun-t2d50.4-0 ; 
       add_gun-t2d9.2-0 ; 
       new_body_texture-t2d105.1-0 ; 
       new_body_texture-t2d106.1-0 ; 
       new_body_texture-t2d107.1-0 ; 
       new_body_texture-t2d108.1-0 ; 
       new_body_texture-t2d109.1-0 ; 
       new_body_texture-t2d110.1-0 ; 
       new_body_texture-t2d111.1-0 ; 
       new_body_texture-t2d112.3-0 ; 
       new_body_texture-t2d113.2-0 ; 
       new_body_texture-t2d95.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 13 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 13 110 ; 
       6 25 110 ; 
       7 25 110 ; 
       8 14 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 1 110 ; 
       14 13 110 ; 
       15 9 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 11 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 13 110 ; 
       25 16 110 ; 
       26 11 110 ; 
       27 19 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 2 110 ; 
       34 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 41 300 ; 
       6 49 300 ; 
       7 52 300 ; 
       7 43 300 ; 
       8 45 300 ; 
       8 48 300 ; 
       9 46 300 ; 
       10 47 300 ; 
       12 50 300 ; 
       13 0 300 ; 
       13 33 300 ; 
       13 34 300 ; 
       13 51 300 ; 
       13 44 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 37 300 ; 
       23 38 300 ; 
       23 39 300 ; 
       25 40 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       27 16 300 ; 
       29 36 300 ; 
       30 35 300 ; 
       31 1 300 ; 
       32 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 26 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       38 23 401 ; 
       40 24 401 ; 
       41 25 401 ; 
       43 32 401 ; 
       44 35 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 31 401 ; 
       50 33 401 ; 
       51 34 401 ; 
       52 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 202.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 110 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 201.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 205 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 195 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 155 -6 0 MPRFLG 0 ; 
       9 SCHEM 151.25 -8 0 MPRFLG 0 ; 
       10 SCHEM 155 -8 0 MPRFLG 0 ; 
       11 SCHEM 182.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 172.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 110 -2 0 MPRFLG 0 ; 
       14 SCHEM 155 -4 0 MPRFLG 0 ; 
       15 SCHEM 150 -10 0 MPRFLG 0 ; 
       16 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 178.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       23 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 188.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 25 -8 0 MPRFLG 0 ; 
       28 SCHEM 118.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 162.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 165 -4 0 MPRFLG 0 ; 
       31 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 170 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 200 -6 0 MPRFLG 0 ; 
       34 SCHEM 197.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 217.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 210 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 217.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 219 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 101 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
