SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.52-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 53     
       add_gun-default7.7-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.5-0 ; 
       add_gun-mat10.5-0 ; 
       add_gun-mat11.5-0 ; 
       add_gun-mat12.5-0 ; 
       add_gun-mat13.5-0 ; 
       add_gun-mat18.5-0 ; 
       add_gun-mat19.5-0 ; 
       add_gun-mat20.5-0 ; 
       add_gun-mat21.5-0 ; 
       add_gun-mat22.5-0 ; 
       add_gun-mat23.5-0 ; 
       add_gun-mat24.5-0 ; 
       add_gun-mat25.5-0 ; 
       add_gun-mat26.5-0 ; 
       add_gun-mat27.5-0 ; 
       add_gun-mat28.5-0 ; 
       add_gun-mat29.5-0 ; 
       add_gun-mat30.5-0 ; 
       add_gun-mat31.5-0 ; 
       add_gun-mat34.5-0 ; 
       add_gun-mat35.5-0 ; 
       add_gun-mat36.5-0 ; 
       add_gun-mat37.5-0 ; 
       add_gun-mat38.5-0 ; 
       add_gun-mat39.5-0 ; 
       add_gun-mat40.5-0 ; 
       add_gun-mat41.5-0 ; 
       add_gun-mat42.5-0 ; 
       add_gun-mat43.5-0 ; 
       add_gun-mat44.5-0 ; 
       add_gun-mat45.6-0 ; 
       add_gun-mat47.7-0 ; 
       add_gun-mat50.7-0 ; 
       add_gun-mat62.5-0 ; 
       add_gun-mat63.5-0 ; 
       add_gun-mat66.5-0 ; 
       add_gun-mat67.5-0 ; 
       add_gun-mat68.5-0 ; 
       add_gun-mat69.5-0 ; 
       add_gun-mat71.6-0 ; 
       add_gun-starbord_green-right.1-0.5-0 ; 
       nulls-0.2-0 ; 
       nulls-1.2-0 ; 
       nulls-mat109.2-0 ; 
       nulls-mat110.2-0 ; 
       nulls-mat111.2-0 ; 
       nulls-mat112.2-0 ; 
       nulls-mat113.2-0 ; 
       nulls-mat114.2-0 ; 
       nulls-mat115.2-0 ; 
       nulls-mat128.2-0 ; 
       nulls-mat95.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05_1.1-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-thrust.1-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05_final-nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       add_gun-t2d10.5-0 ; 
       add_gun-t2d11.5-0 ; 
       add_gun-t2d15.5-0 ; 
       add_gun-t2d16.5-0 ; 
       add_gun-t2d17.5-0 ; 
       add_gun-t2d18.5-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d20.5-0 ; 
       add_gun-t2d21.5-0 ; 
       add_gun-t2d22.5-0 ; 
       add_gun-t2d23.5-0 ; 
       add_gun-t2d25.5-0 ; 
       add_gun-t2d26.5-0 ; 
       add_gun-t2d27.5-0 ; 
       add_gun-t2d28.5-0 ; 
       add_gun-t2d29.5-0 ; 
       add_gun-t2d30.5-0 ; 
       add_gun-t2d31.5-0 ; 
       add_gun-t2d32.5-0 ; 
       add_gun-t2d35.9-0 ; 
       add_gun-t2d38.9-0 ; 
       add_gun-t2d47.5-0 ; 
       add_gun-t2d48.5-0 ; 
       add_gun-t2d49.6-0 ; 
       add_gun-t2d50.7-0 ; 
       add_gun-t2d9.5-0 ; 
       nulls-t2d105.2-0 ; 
       nulls-t2d106.2-0 ; 
       nulls-t2d107.2-0 ; 
       nulls-t2d108.2-0 ; 
       nulls-t2d109.2-0 ; 
       nulls-t2d110.2-0 ; 
       nulls-t2d111.2-0 ; 
       nulls-t2d112.2-0 ; 
       nulls-t2d113.2-0 ; 
       nulls-t2d122.2-0 ; 
       nulls-t2d95.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 25 110 ; 
       1 3 110 ; 
       3 13 110 ; 
       4 25 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 13 110 ; 
       8 14 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       15 9 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 11 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 11 110 ; 
       27 19 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 3 110 ; 
       34 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 40 300 ; 
       0 48 300 ; 
       4 52 300 ; 
       4 42 300 ; 
       8 44 300 ; 
       8 47 300 ; 
       9 45 300 ; 
       10 46 300 ; 
       12 49 300 ; 
       13 0 300 ; 
       13 32 300 ; 
       13 33 300 ; 
       13 50 300 ; 
       13 43 300 ; 
       16 31 300 ; 
       16 51 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       23 38 300 ; 
       25 39 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       27 16 300 ; 
       28 35 300 ; 
       29 34 300 ; 
       30 1 300 ; 
       31 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       51 35 401 ; 
       52 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       2 SCHEM 40 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 71.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 MPRFLG 0 ; 
       5 SCHEM 75 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 65 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       8 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 MPRFLG 0 ; 
       14 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5 -4 0 MPRFLG 0 ; 
       17 SCHEM 15 -4 0 MPRFLG 0 ; 
       18 SCHEM 60 -6 0 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 10 -8 0 MPRFLG 0 ; 
       28 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 50 -4 0 MPRFLG 0 ; 
       30 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 55 -4 0 MPRFLG 0 ; 
       32 SCHEM 77.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 70 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 59 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
