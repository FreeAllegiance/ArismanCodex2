SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.39-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 82     
       add_gun-default7.2-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.2-0 ; 
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat45.2-0 ; 
       add_gun-mat46.2-0 ; 
       add_gun-mat47.2-0 ; 
       add_gun-mat50.2-0 ; 
       add_gun-mat53.2-0 ; 
       add_gun-mat54.2-0 ; 
       add_gun-mat55.2-0 ; 
       add_gun-mat56.2-0 ; 
       add_gun-mat57.2-0 ; 
       add_gun-mat58.2-0 ; 
       add_gun-mat60.2-0 ; 
       add_gun-mat61.2-0 ; 
       add_gun-mat62.2-0 ; 
       add_gun-mat63.2-0 ; 
       add_gun-mat66.2-0 ; 
       add_gun-mat67.2-0 ; 
       add_gun-mat68.2-0 ; 
       add_gun-mat69.2-0 ; 
       add_gun-mat71.3-0 ; 
       add_gun-starbord_green-right.1-0.2-0 ; 
       b_version_oct_6-0.1-0 ; 
       b_version_oct_6-mat100.1-0 ; 
       b_version_oct_6-mat101.1-0 ; 
       b_version_oct_6-mat102.1-0 ; 
       b_version_oct_6-mat103.1-0 ; 
       b_version_oct_6-mat104.1-0 ; 
       b_version_oct_6-mat105.1-0 ; 
       b_version_oct_6-mat106.1-0 ; 
       b_version_oct_6-mat107.1-0 ; 
       b_version_oct_6-mat108.1-0 ; 
       b_version_oct_6-mat109.2-0 ; 
       b_version_oct_6-mat110.1-0 ; 
       b_version_oct_6-mat111.1-0 ; 
       b_version_oct_6-mat112.1-0 ; 
       b_version_oct_6-mat113.1-0 ; 
       b_version_oct_6-mat81.1-0 ; 
       b_version_oct_6-mat82.1-0 ; 
       b_version_oct_6-mat84.1-0 ; 
       b_version_oct_6-mat85.1-0 ; 
       b_version_oct_6-mat86.1-0 ; 
       b_version_oct_6-mat88.1-0 ; 
       b_version_oct_6-mat90.1-0 ; 
       b_version_oct_6-mat91.1-0 ; 
       b_version_oct_6-mat92.1-0 ; 
       b_version_oct_6-mat93.1-0 ; 
       b_version_oct_6-mat94.1-0 ; 
       b_version_oct_6-mat95.2-0 ; 
       b_version_oct_6-mat96.1-0 ; 
       b_version_oct_6-mat97.1-0 ; 
       b_version_oct_6-mat98.1-0 ; 
       b_version_oct_6-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       bom05-acs63_1.1-0 ; 
       bom05-acs63_2.1-0 ; 
       bom05-antenn1.1-0 ; 
       bom05-antenn2.1-0 ; 
       bom05-antenn3.1-0 ; 
       bom05-antenn4.1-0 ; 
       bom05-antenn5.1-0 ; 
       bom05-antenn6.1-0 ; 
       bom05-blthrust.1-0 ; 
       bom05-bom05.35-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl1.1-0 ; 
       bom05-cyl3.1-0 ; 
       bom05-cyl4.1-0 ; 
       bom05-face2.1-0 ; 
       bom05-face3.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-fwepemt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-lwepbas_3.1-0 ; 
       bom05-lwepbas_4.1-0 ; 
       bom05-Lwingzz.1-0 ; 
       bom05-Lwingzz1.1-0 ; 
       bom05-Lwingzz2.1-0 ; 
       bom05-Lwingzz3.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-rwepbas_3.1-0 ; 
       bom05-rwepbas_4.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
       bom05-turret1.1-0 ; 
       bom05-turret2.1-0 ; 
       bom05-wepemt.1-0 ; 
       bom05-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b_final-b_version_oct_6.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 57     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d33.2-0 ; 
       add_gun-t2d35.3-0 ; 
       add_gun-t2d38.3-0 ; 
       add_gun-t2d40.3-0 ; 
       add_gun-t2d41.3-0 ; 
       add_gun-t2d42.2-0 ; 
       add_gun-t2d43.2-0 ; 
       add_gun-t2d44.2-0 ; 
       add_gun-t2d45.2-0 ; 
       add_gun-t2d46.2-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_gun-t2d49.3-0 ; 
       add_gun-t2d50.4-0 ; 
       add_gun-t2d9.2-0 ; 
       b_version_oct_6-t2d100.1-0 ; 
       b_version_oct_6-t2d101.1-0 ; 
       b_version_oct_6-t2d102.1-0 ; 
       b_version_oct_6-t2d103.1-0 ; 
       b_version_oct_6-t2d104.1-0 ; 
       b_version_oct_6-t2d105.2-0 ; 
       b_version_oct_6-t2d106.1-0 ; 
       b_version_oct_6-t2d107.1-0 ; 
       b_version_oct_6-t2d108.1-0 ; 
       b_version_oct_6-t2d109.1-0 ; 
       b_version_oct_6-t2d110.1-0 ; 
       b_version_oct_6-t2d87.3-0 ; 
       b_version_oct_6-t2d89.3-0 ; 
       b_version_oct_6-t2d90.3-0 ; 
       b_version_oct_6-t2d91.1-0 ; 
       b_version_oct_6-t2d92.1-0 ; 
       b_version_oct_6-t2d93.1-0 ; 
       b_version_oct_6-t2d94.1-0 ; 
       b_version_oct_6-t2d95.2-0 ; 
       b_version_oct_6-t2d96.1-0 ; 
       b_version_oct_6-t2d97.1-0 ; 
       b_version_oct_6-t2d98.1-0 ; 
       b_version_oct_6-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 42 110 ; 
       1 32 110 ; 
       2 35 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 37 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 10 110 ; 
       10 21 110 ; 
       11 10 110 ; 
       12 14 110 ; 
       13 21 110 ; 
       14 39 110 ; 
       15 39 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       21 9 110 ; 
       22 21 110 ; 
       23 17 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 19 110 ; 
       27 25 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 21 110 ; 
       33 51 110 ; 
       34 52 110 ; 
       35 51 110 ; 
       36 51 110 ; 
       37 52 110 ; 
       38 52 110 ; 
       39 24 110 ; 
       40 19 110 ; 
       41 27 110 ; 
       42 21 110 ; 
       43 51 110 ; 
       44 52 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 10 110 ; 
       50 10 110 ; 
       51 0 110 ; 
       52 1 110 ; 
       53 43 110 ; 
       54 44 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 66 300 ; 
       1 79 300 ; 
       2 68 300 ; 
       3 69 300 ; 
       4 70 300 ; 
       5 57 300 ; 
       6 58 300 ; 
       7 59 300 ; 
       14 49 300 ; 
       14 65 300 ; 
       15 77 300 ; 
       15 51 300 ; 
       16 61 300 ; 
       16 64 300 ; 
       17 62 300 ; 
       18 63 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       20 39 300 ; 
       20 40 300 ; 
       20 41 300 ; 
       20 42 300 ; 
       21 0 300 ; 
       21 33 300 ; 
       21 34 300 ; 
       21 35 300 ; 
       21 36 300 ; 
       24 31 300 ; 
       24 32 300 ; 
       25 24 300 ; 
       25 25 300 ; 
       25 26 300 ; 
       25 27 300 ; 
       26 6 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 20 300 ; 
       27 21 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 30 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       30 17 300 ; 
       30 18 300 ; 
       30 19 300 ; 
       31 45 300 ; 
       31 46 300 ; 
       31 47 300 ; 
       33 71 300 ; 
       34 53 300 ; 
       35 76 300 ; 
       36 78 300 ; 
       37 55 300 ; 
       38 56 300 ; 
       39 48 300 ; 
       40 2 300 ; 
       40 3 300 ; 
       40 4 300 ; 
       40 5 300 ; 
       41 14 300 ; 
       41 15 300 ; 
       41 16 300 ; 
       43 72 300 ; 
       44 54 300 ; 
       45 44 300 ; 
       46 43 300 ; 
       47 1 300 ; 
       48 50 300 ; 
       51 67 300 ; 
       51 74 300 ; 
       51 75 300 ; 
       52 80 300 ; 
       52 81 300 ; 
       52 52 300 ; 
       53 73 300 ; 
       54 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       30 10 400 ; 
       31 29 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 33 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 30 401 ; 
       48 31 401 ; 
       49 32 401 ; 
       52 56 401 ; 
       55 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       61 39 401 ; 
       62 40 401 ; 
       63 41 401 ; 
       67 45 401 ; 
       68 49 401 ; 
       69 50 401 ; 
       70 51 401 ; 
       74 46 401 ; 
       75 47 401 ; 
       76 48 401 ; 
       77 52 401 ; 
       78 53 401 ; 
       80 54 401 ; 
       81 55 401 ; 
       64 42 401 ; 
       65 43 401 ; 
       51 44 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 120 -12 0 MPRFLG 0 ; 
       3 SCHEM 118.75 -14 0 MPRFLG 0 ; 
       4 SCHEM 117.5 -16 0 MPRFLG 0 ; 
       5 SCHEM 90 -12 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -14 0 MPRFLG 0 ; 
       7 SCHEM 87.5 -16 0 MPRFLG 0 ; 
       8 SCHEM 212.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 115 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 211.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 215 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 205 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -8 0 MPRFLG 0 ; 
       15 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 152.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 148.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 152.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 186.25 -4 0 MPRFLG 0 ; 
       20 SCHEM 176.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 115 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 152.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 147.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 188.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       28 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       30 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       32 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 135 -10 0 MPRFLG 0 ; 
       34 SCHEM 100 -10 0 MPRFLG 0 ; 
       35 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       36 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       38 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       39 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       40 SCHEM 198.75 -6 0 MPRFLG 0 ; 
       41 SCHEM 25 -8 0 MPRFLG 0 ; 
       42 SCHEM 131.25 -4 0 MPRFLG 0 ; 
       43 SCHEM 131.25 -10 0 MPRFLG 0 ; 
       44 SCHEM 103.75 -10 0 MPRFLG 0 ; 
       45 SCHEM 160 -4 0 MPRFLG 0 ; 
       46 SCHEM 162.5 -4 0 MPRFLG 0 ; 
       47 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 167.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 210 -6 0 MPRFLG 0 ; 
       50 SCHEM 207.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 130 -8 0 MPRFLG 0 ; 
       52 SCHEM 100 -8 0 MPRFLG 0 ; 
       53 SCHEM 130 -12 0 MPRFLG 0 ; 
       54 SCHEM 102.5 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 227.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 202.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 217.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 220 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 222.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 225 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 90 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 87.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 122.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 120 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 117.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 197.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 200 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 217.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 220 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 222.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 225 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 195 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 92.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 90 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 87.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 150 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 122.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 120 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 117.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 229 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 101 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
