SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom05-bom05.46-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.44-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       add_gun-default7.4-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.2-0 ; 
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat45.2-0 ; 
       add_gun-mat46.2-0 ; 
       add_gun-mat47.4-0 ; 
       add_gun-mat50.4-0 ; 
       add_gun-mat62.2-0 ; 
       add_gun-mat63.2-0 ; 
       add_gun-mat66.2-0 ; 
       add_gun-mat67.2-0 ; 
       add_gun-mat68.2-0 ; 
       add_gun-mat69.2-0 ; 
       add_gun-mat71.3-0 ; 
       add_gun-starbord_green-right.1-0.2-0 ; 
       new_body_texture-0.1-0 ; 
       new_body_texture-1.1-0 ; 
       new_body_texture-mat109.1-0 ; 
       new_body_texture-mat110.1-0 ; 
       new_body_texture-mat111.1-0 ; 
       new_body_texture-mat112.1-0 ; 
       new_body_texture-mat113.1-0 ; 
       new_body_texture-mat114.1-0 ; 
       new_body_texture-mat115.2-0 ; 
       new_body_texture-mat116.1-0 ; 
       new_body_texture-mat117.1-0 ; 
       new_body_texture-mat118.1-0 ; 
       new_body_texture-mat119.1-0 ; 
       new_body_texture-mat120.1-0 ; 
       new_body_texture-mat121.1-0 ; 
       new_body_texture-mat122.1-0 ; 
       new_body_texture-mat123.1-0 ; 
       new_body_texture-mat124.1-0 ; 
       new_body_texture-mat125.1-0 ; 
       new_body_texture-mat126.1-0 ; 
       new_body_texture-mat127.1-0 ; 
       new_body_texture-mat81.1-0 ; 
       new_body_texture-mat82.1-0 ; 
       new_body_texture-mat84.1-0 ; 
       new_body_texture-mat85.1-0 ; 
       new_body_texture-mat86.1-0 ; 
       new_body_texture-mat88.1-0 ; 
       new_body_texture-mat90.1-0 ; 
       new_body_texture-mat91.1-0 ; 
       new_body_texture-mat92.1-0 ; 
       new_body_texture-mat93.1-0 ; 
       new_body_texture-mat94.1-0 ; 
       new_body_texture-mat95.1-0 ; 
       new_body_texture-mat96.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       bom05-acs63_1.1-0 ; 
       bom05-acs63_3.1-0 ; 
       bom05-antenn1.1-0 ; 
       bom05-antenn2.1-0 ; 
       bom05-antenn3.1-0 ; 
       bom05-antenn7.1-0 ; 
       bom05-antenn8.1-0 ; 
       bom05-antenn9.1-0 ; 
       bom05-blthrust.1-0 ; 
       bom05-bom05.41-0 ROOT ; 
       bom05-boosters.1-0 ; 
       bom05-brthrust.1-0 ; 
       bom05-bwepemt.1-0 ; 
       bom05-cockpt.1-0 ; 
       bom05-cyl1.1-0 ; 
       bom05-cyl3.1-0 ; 
       bom05-cyl4.1-0 ; 
       bom05-face2.1-0 ; 
       bom05-face3.1-0 ; 
       bom05-finzzz0.1-0 ; 
       bom05-finzzz1.2-0 ; 
       bom05-fuselg.2-0 ; 
       bom05-fwepatt.1-0 ; 
       bom05-fwepemt.1-0 ; 
       bom05-hatchz.1-0 ; 
       bom05-landgr1.1-0 ; 
       bom05-lfinzzz.1-0 ; 
       bom05-LL1.2-0 ; 
       bom05-LLa.1-0 ; 
       bom05-llandgr.1-0 ; 
       bom05-LLl.1-0 ; 
       bom05-LLr.1-0 ; 
       bom05-lturatt.1-0 ; 
       bom05-lwepbas_3.1-0 ; 
       bom05-lwepbas_5.1-0 ; 
       bom05-lwepemt.1-0 ; 
       bom05-Lwingzz.1-0 ; 
       bom05-Lwingzz1.1-0 ; 
       bom05-Lwingzz4.1-0 ; 
       bom05-Lwingzz5.1-0 ; 
       bom05-pod.1-0 ; 
       bom05-rfinzzz.1-0 ; 
       bom05-rlandgr.1-0 ; 
       bom05-rturatt.1-0 ; 
       bom05-rwepbas_3.1-0 ; 
       bom05-rwepbas_5.1-0 ; 
       bom05-rwepemt.1-0 ; 
       bom05-SSa.2-0 ; 
       bom05-SSf.2-0 ; 
       bom05-SSl.2-0 ; 
       bom05-SSr.2-0 ; 
       bom05-tlthrust.1-0 ; 
       bom05-trthrust.1-0 ; 
       bom05-turret1.1-0 ; 
       bom05-turret3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b_final-new_body_texture.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d33.2-0 ; 
       add_gun-t2d35.6-0 ; 
       add_gun-t2d38.6-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_gun-t2d49.3-0 ; 
       add_gun-t2d50.4-0 ; 
       add_gun-t2d9.2-0 ; 
       new_body_texture-t2d105.1-0 ; 
       new_body_texture-t2d106.1-0 ; 
       new_body_texture-t2d107.1-0 ; 
       new_body_texture-t2d108.1-0 ; 
       new_body_texture-t2d109.1-0 ; 
       new_body_texture-t2d110.1-0 ; 
       new_body_texture-t2d111.1-0 ; 
       new_body_texture-t2d112.3-0 ; 
       new_body_texture-t2d113.2-0 ; 
       new_body_texture-t2d114.1-0 ; 
       new_body_texture-t2d115.1-0 ; 
       new_body_texture-t2d116.1-0 ; 
       new_body_texture-t2d117.1-0 ; 
       new_body_texture-t2d118.1-0 ; 
       new_body_texture-t2d119.1-0 ; 
       new_body_texture-t2d120.1-0 ; 
       new_body_texture-t2d121.1-0 ; 
       new_body_texture-t2d87.1-0 ; 
       new_body_texture-t2d89.1-0 ; 
       new_body_texture-t2d90.1-0 ; 
       new_body_texture-t2d91.1-0 ; 
       new_body_texture-t2d92.1-0 ; 
       new_body_texture-t2d93.1-0 ; 
       new_body_texture-t2d94.1-0 ; 
       new_body_texture-t2d95.1-0 ; 
       new_body_texture-t2d96.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 43 110 ; 
       1 32 110 ; 
       2 36 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 38 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 10 110 ; 
       10 21 110 ; 
       11 10 110 ; 
       12 14 110 ; 
       13 21 110 ; 
       14 40 110 ; 
       15 40 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       21 9 110 ; 
       22 21 110 ; 
       23 17 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 19 110 ; 
       27 25 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 21 110 ; 
       33 53 110 ; 
       34 54 110 ; 
       35 45 110 ; 
       36 53 110 ; 
       37 53 110 ; 
       38 54 110 ; 
       39 54 110 ; 
       40 24 110 ; 
       41 19 110 ; 
       42 27 110 ; 
       43 21 110 ; 
       44 53 110 ; 
       45 54 110 ; 
       46 44 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 21 110 ; 
       50 21 110 ; 
       51 10 110 ; 
       52 10 110 ; 
       53 0 110 ; 
       54 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 64 300 ; 
       1 52 300 ; 
       2 66 300 ; 
       3 67 300 ; 
       4 68 300 ; 
       5 60 300 ; 
       6 61 300 ; 
       7 62 300 ; 
       14 41 300 ; 
       14 49 300 ; 
       15 75 300 ; 
       15 43 300 ; 
       16 45 300 ; 
       16 48 300 ; 
       17 46 300 ; 
       18 47 300 ; 
       20 50 300 ; 
       21 0 300 ; 
       21 33 300 ; 
       21 34 300 ; 
       21 51 300 ; 
       21 44 300 ; 
       24 31 300 ; 
       24 32 300 ; 
       25 24 300 ; 
       25 25 300 ; 
       25 26 300 ; 
       25 27 300 ; 
       26 6 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 20 300 ; 
       27 21 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 30 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       30 17 300 ; 
       30 18 300 ; 
       30 19 300 ; 
       31 37 300 ; 
       31 38 300 ; 
       31 39 300 ; 
       33 69 300 ; 
       34 56 300 ; 
       35 63 300 ; 
       36 74 300 ; 
       37 76 300 ; 
       38 58 300 ; 
       39 59 300 ; 
       40 40 300 ; 
       41 2 300 ; 
       41 3 300 ; 
       41 4 300 ; 
       41 5 300 ; 
       42 14 300 ; 
       42 15 300 ; 
       42 16 300 ; 
       44 70 300 ; 
       45 57 300 ; 
       46 71 300 ; 
       47 36 300 ; 
       48 35 300 ; 
       49 1 300 ; 
       50 42 300 ; 
       53 65 300 ; 
       53 72 300 ; 
       53 73 300 ; 
       54 53 300 ; 
       54 54 300 ; 
       54 55 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       30 10 400 ; 
       31 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 26 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       38 23 401 ; 
       40 24 401 ; 
       41 25 401 ; 
       43 32 401 ; 
       44 35 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 31 401 ; 
       50 33 401 ; 
       51 34 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       55 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       65 44 401 ; 
       66 48 401 ; 
       67 49 401 ; 
       68 50 401 ; 
       72 45 401 ; 
       73 46 401 ; 
       74 47 401 ; 
       75 51 401 ; 
       76 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 120 -12 0 MPRFLG 0 ; 
       3 SCHEM 118.75 -14 0 MPRFLG 0 ; 
       4 SCHEM 117.5 -16 0 MPRFLG 0 ; 
       5 SCHEM 90 -12 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -14 0 MPRFLG 0 ; 
       7 SCHEM 87.5 -16 0 MPRFLG 0 ; 
       8 SCHEM 200 -6 0 MPRFLG 0 ; 
       9 SCHEM 108.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 198.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 202.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 192.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -8 0 MPRFLG 0 ; 
       15 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 152.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 148.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 152.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 180 -4 0 MPRFLG 0 ; 
       20 SCHEM 170 -6 0 MPRFLG 0 ; 
       21 SCHEM 108.75 -2 0 MPRFLG 0 ; 
       22 SCHEM 152.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 147.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 176.25 -6 0 MPRFLG 0 ; 
       27 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       28 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       30 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       32 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 135 -10 0 MPRFLG 0 ; 
       34 SCHEM 100 -10 0 MPRFLG 0 ; 
       35 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       37 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       39 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       40 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       41 SCHEM 186.25 -6 0 MPRFLG 0 ; 
       42 SCHEM 25 -8 0 MPRFLG 0 ; 
       43 SCHEM 131.25 -4 0 MPRFLG 0 ; 
       44 SCHEM 131.25 -10 0 MPRFLG 0 ; 
       45 SCHEM 103.75 -10 0 MPRFLG 0 ; 
       46 SCHEM 130 -12 0 MPRFLG 0 ; 
       47 SCHEM 160 -4 0 MPRFLG 0 ; 
       48 SCHEM 162.5 -4 0 MPRFLG 0 ; 
       49 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 167.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 197.5 -6 0 MPRFLG 0 ; 
       52 SCHEM 195 -6 0 MPRFLG 0 ; 
       53 SCHEM 130 -8 0 MPRFLG 0 ; 
       54 SCHEM 100 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 90 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 87.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 122.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 120 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 117.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 150 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 212.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 90 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 87.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 125 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 122.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 120 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 117.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 216.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 101 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
