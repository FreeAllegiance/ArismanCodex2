SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 75     
       add_gun-default7.2-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.2-0 ; 
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat45.2-0 ; 
       add_gun-mat47.2-0 ; 
       add_gun-mat50.2-0 ; 
       add_gun-mat62.2-0 ; 
       add_gun-mat63.2-0 ; 
       add_gun-mat66.2-0 ; 
       add_gun-mat67.2-0 ; 
       add_gun-mat68.2-0 ; 
       add_gun-mat69.2-0 ; 
       add_gun-mat71.2-0 ; 
       add_gun-starbord_green-right.1-0.2-0 ; 
       nulls-0.2-0 ; 
       nulls-1.2-0 ; 
       nulls-mat109.2-0 ; 
       nulls-mat110.2-0 ; 
       nulls-mat111.2-0 ; 
       nulls-mat112.2-0 ; 
       nulls-mat113.2-0 ; 
       nulls-mat114.2-0 ; 
       nulls-mat115.2-0 ; 
       nulls-mat116.2-0 ; 
       nulls-mat117.2-0 ; 
       nulls-mat118.2-0 ; 
       nulls-mat119.2-0 ; 
       nulls-mat120.2-0 ; 
       nulls-mat121.2-0 ; 
       nulls-mat122.2-0 ; 
       nulls-mat123.2-0 ; 
       nulls-mat124.2-0 ; 
       nulls-mat125.2-0 ; 
       nulls-mat126.2-0 ; 
       nulls-mat128.2-0 ; 
       nulls-mat81.2-0 ; 
       nulls-mat82.2-0 ; 
       nulls-mat84.2-0 ; 
       nulls-mat85.2-0 ; 
       nulls-mat86.2-0 ; 
       nulls-mat88.2-0 ; 
       nulls-mat90.2-0 ; 
       nulls-mat92.2-0 ; 
       nulls-mat93.2-0 ; 
       nulls-mat94.2-0 ; 
       nulls-mat95.2-0 ; 
       nulls-mat96.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 57     
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-antenn1.1-0 ; 
       nulls-antenn2.1-0 ; 
       nulls-antenn3.1-0 ; 
       nulls-antenn7.1-0 ; 
       nulls-antenn8.1-0 ; 
       nulls-antenn9.1-0 ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.6-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-lwepbas_3.1-0 ; 
       nulls-lwepbas_5.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-rwepbas_3.1-0 ; 
       nulls-rwepbas_5.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
       nulls-turwepemt1.1-0 ; 
       nulls-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-new_nulls.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d35.2-0 ; 
       add_gun-t2d38.2-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_gun-t2d49.2-0 ; 
       add_gun-t2d50.3-0 ; 
       add_gun-t2d9.2-0 ; 
       nulls-t2d105.2-0 ; 
       nulls-t2d106.2-0 ; 
       nulls-t2d107.2-0 ; 
       nulls-t2d108.2-0 ; 
       nulls-t2d109.2-0 ; 
       nulls-t2d110.2-0 ; 
       nulls-t2d111.2-0 ; 
       nulls-t2d112.2-0 ; 
       nulls-t2d113.2-0 ; 
       nulls-t2d114.2-0 ; 
       nulls-t2d115.2-0 ; 
       nulls-t2d116.2-0 ; 
       nulls-t2d117.2-0 ; 
       nulls-t2d118.2-0 ; 
       nulls-t2d119.2-0 ; 
       nulls-t2d120.2-0 ; 
       nulls-t2d121.2-0 ; 
       nulls-t2d122.2-0 ; 
       nulls-t2d87.2-0 ; 
       nulls-t2d89.2-0 ; 
       nulls-t2d90.2-0 ; 
       nulls-t2d91.2-0 ; 
       nulls-t2d92.2-0 ; 
       nulls-t2d93.2-0 ; 
       nulls-t2d94.2-0 ; 
       nulls-t2d95.2-0 ; 
       nulls-t2d96.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       52 11 110 ; 
       50 11 110 ; 
       9 11 110 ; 
       13 11 110 ; 
       15 21 110 ; 
       14 8 110 ; 
       56 45 110 ; 
       55 44 110 ; 
       23 17 110 ; 
       0 43 110 ; 
       1 32 110 ; 
       2 35 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 37 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 40 110 ; 
       11 21 110 ; 
       12 40 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       21 10 110 ; 
       22 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 19 110 ; 
       27 25 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 21 110 ; 
       33 53 110 ; 
       34 54 110 ; 
       35 53 110 ; 
       36 53 110 ; 
       37 54 110 ; 
       38 54 110 ; 
       39 24 110 ; 
       40 24 110 ; 
       41 19 110 ; 
       42 27 110 ; 
       43 21 110 ; 
       44 53 110 ; 
       45 54 110 ; 
       46 21 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 21 110 ; 
       53 0 110 ; 
       54 1 110 ; 
       51 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 63 300 ; 
       1 51 300 ; 
       2 65 300 ; 
       3 66 300 ; 
       4 67 300 ; 
       5 59 300 ; 
       6 60 300 ; 
       7 61 300 ; 
       8 40 300 ; 
       8 48 300 ; 
       12 73 300 ; 
       12 42 300 ; 
       16 44 300 ; 
       16 47 300 ; 
       17 45 300 ; 
       18 46 300 ; 
       20 49 300 ; 
       21 0 300 ; 
       21 32 300 ; 
       21 33 300 ; 
       21 50 300 ; 
       21 43 300 ; 
       24 31 300 ; 
       24 62 300 ; 
       25 24 300 ; 
       25 25 300 ; 
       25 26 300 ; 
       25 27 300 ; 
       26 6 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 20 300 ; 
       27 21 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 30 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       30 17 300 ; 
       30 18 300 ; 
       30 19 300 ; 
       31 36 300 ; 
       31 37 300 ; 
       31 38 300 ; 
       33 68 300 ; 
       34 55 300 ; 
       35 72 300 ; 
       36 74 300 ; 
       37 57 300 ; 
       38 58 300 ; 
       40 39 300 ; 
       41 2 300 ; 
       41 3 300 ; 
       41 4 300 ; 
       41 5 300 ; 
       42 14 300 ; 
       42 15 300 ; 
       42 16 300 ; 
       44 69 300 ; 
       45 56 300 ; 
       46 35 300 ; 
       47 34 300 ; 
       48 1 300 ; 
       49 41 300 ; 
       53 64 300 ; 
       53 70 300 ; 
       53 71 300 ; 
       54 52 300 ; 
       54 53 300 ; 
       54 54 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       30 10 400 ; 
       31 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       57 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       64 44 401 ; 
       65 48 401 ; 
       66 49 401 ; 
       67 50 401 ; 
       70 45 401 ; 
       71 46 401 ; 
       72 47 401 ; 
       73 51 401 ; 
       74 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       52 SCHEM 67.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 70 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 30 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 37.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -10 0 USR WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 36.25 -6 0 USR MPRFLG 0 ; 
       1 SCHEM 26.25 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 32.5 -12 0 USR MPRFLG 0 ; 
       3 SCHEM 32.5 -14 0 USR MPRFLG 0 ; 
       4 SCHEM 32.5 -16 0 USR MPRFLG 0 ; 
       5 SCHEM 22.5 -12 0 USR MPRFLG 0 ; 
       6 SCHEM 22.5 -14 0 USR MPRFLG 0 ; 
       7 SCHEM 22.5 -16 0 USR MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 USR MPRFLG 0 ; 
       10 SCHEM 40 0 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 71.25 -4 0 USR MPRFLG 0 ; 
       12 SCHEM 5 -8 0 USR MPRFLG 0 ; 
       16 SCHEM 43.75 -6 0 USR MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 USR MPRFLG 0 ; 
       18 SCHEM 45 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 60 -4 0 USR MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       21 SCHEM 40 -2 0 USR MPRFLG 0 ; 
       22 SCHEM 43.75 -4 0 USR MPRFLG 0 ; 
       24 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       25 SCHEM 15 -4 0 USR MPRFLG 0 ; 
       26 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       27 SCHEM 15 -6 0 USR MPRFLG 0 ; 
       28 SCHEM 20 -8 0 USR MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       30 SCHEM 17.5 -8 0 USR MPRFLG 0 ; 
       31 SCHEM 15 -8 0 USR MPRFLG 0 ; 
       32 SCHEM 26.25 -4 0 USR MPRFLG 0 ; 
       33 SCHEM 40 -10 0 USR MPRFLG 0 ; 
       34 SCHEM 27.5 -10 0 USR MPRFLG 0 ; 
       35 SCHEM 32.5 -10 0 USR MPRFLG 0 ; 
       36 SCHEM 35 -10 0 USR MPRFLG 0 ; 
       37 SCHEM 22.5 -10 0 USR MPRFLG 0 ; 
       38 SCHEM 25 -10 0 USR MPRFLG 0 ; 
       39 SCHEM 7.5 -6 0 USR MPRFLG 0 ; 
       40 SCHEM 3.75 -6 0 USR MPRFLG 0 ; 
       41 SCHEM 62.5 -6 0 USR MPRFLG 0 ; 
       42 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       43 SCHEM 36.25 -4 0 USR MPRFLG 0 ; 
       44 SCHEM 37.5 -10 0 USR MPRFLG 0 ; 
       45 SCHEM 30 -10 0 USR MPRFLG 0 ; 
       46 SCHEM 47.5 -4 0 USR MPRFLG 0 ; 
       47 SCHEM 50 -4 0 USR MPRFLG 0 ; 
       48 SCHEM 52.5 -4 0 USR MPRFLG 0 ; 
       49 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       53 SCHEM 36.25 -8 0 USR MPRFLG 0 ; 
       54 SCHEM 26.25 -8 0 USR MPRFLG 0 ; 
       51 SCHEM 77.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 34 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 98 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
