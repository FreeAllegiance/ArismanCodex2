SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 75     
       add_gun-default7.5-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.5-0 ; 
       add_gun-mat10.5-0 ; 
       add_gun-mat11.5-0 ; 
       add_gun-mat12.5-0 ; 
       add_gun-mat13.5-0 ; 
       add_gun-mat18.5-0 ; 
       add_gun-mat19.5-0 ; 
       add_gun-mat20.5-0 ; 
       add_gun-mat21.5-0 ; 
       add_gun-mat22.5-0 ; 
       add_gun-mat23.5-0 ; 
       add_gun-mat24.5-0 ; 
       add_gun-mat25.5-0 ; 
       add_gun-mat26.5-0 ; 
       add_gun-mat27.5-0 ; 
       add_gun-mat28.5-0 ; 
       add_gun-mat29.4-0 ; 
       add_gun-mat30.4-0 ; 
       add_gun-mat31.4-0 ; 
       add_gun-mat34.5-0 ; 
       add_gun-mat35.5-0 ; 
       add_gun-mat36.5-0 ; 
       add_gun-mat37.5-0 ; 
       add_gun-mat38.5-0 ; 
       add_gun-mat39.5-0 ; 
       add_gun-mat40.5-0 ; 
       add_gun-mat41.5-0 ; 
       add_gun-mat42.4-0 ; 
       add_gun-mat43.4-0 ; 
       add_gun-mat44.4-0 ; 
       add_gun-mat45.5-0 ; 
       add_gun-mat47.5-0 ; 
       add_gun-mat50.5-0 ; 
       add_gun-mat62.5-0 ; 
       add_gun-mat63.5-0 ; 
       add_gun-mat66.4-0 ; 
       add_gun-mat67.4-0 ; 
       add_gun-mat68.4-0 ; 
       add_gun-mat69.5-0 ; 
       add_gun-mat71.5-0 ; 
       add_gun-starbord_green-right.1-0.5-0 ; 
       nulls-0.5-0 ; 
       nulls-1.5-0 ; 
       nulls-mat109.5-0 ; 
       nulls-mat110.5-0 ; 
       nulls-mat111.5-0 ; 
       nulls-mat112.5-0 ; 
       nulls-mat113.5-0 ; 
       nulls-mat114.5-0 ; 
       nulls-mat115.5-0 ; 
       nulls-mat116.5-0 ; 
       nulls-mat117.5-0 ; 
       nulls-mat118.5-0 ; 
       nulls-mat119.5-0 ; 
       nulls-mat120.5-0 ; 
       nulls-mat121.5-0 ; 
       nulls-mat122.5-0 ; 
       nulls-mat123.5-0 ; 
       nulls-mat124.5-0 ; 
       nulls-mat125.5-0 ; 
       nulls-mat126.5-0 ; 
       nulls-mat128.5-0 ; 
       nulls-mat81.5-0 ; 
       nulls-mat82.5-0 ; 
       nulls-mat84.5-0 ; 
       nulls-mat85.5-0 ; 
       nulls-mat86.5-0 ; 
       nulls-mat88.5-0 ; 
       nulls-mat90.5-0 ; 
       nulls-mat92.5-0 ; 
       nulls-mat93.5-0 ; 
       nulls-mat94.5-0 ; 
       nulls-mat95.5-0 ; 
       nulls-mat96.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       nulls-170deg.1-0 ROOT ; 
       nulls-170deg1.1-0 ROOT ; 
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-antenn1.1-0 ; 
       nulls-antenn2.1-0 ; 
       nulls-antenn3.1-0 ; 
       nulls-antenn7.1-0 ; 
       nulls-antenn8.1-0 ; 
       nulls-antenn9.1-0 ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.8-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-lwepbas_3.1-0 ; 
       nulls-lwepbas_5.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-rwepbas_3.1-0 ; 
       nulls-rwepbas_5.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
       nulls-turwepemt1.1-0 ; 
       nulls-turwepemt2.1-0 ; 
       scaled-cube1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_gun-t2d10.5-0 ; 
       add_gun-t2d11.5-0 ; 
       add_gun-t2d15.5-0 ; 
       add_gun-t2d16.5-0 ; 
       add_gun-t2d17.5-0 ; 
       add_gun-t2d18.5-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d20.5-0 ; 
       add_gun-t2d21.5-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.5-0 ; 
       add_gun-t2d26.5-0 ; 
       add_gun-t2d27.5-0 ; 
       add_gun-t2d28.5-0 ; 
       add_gun-t2d29.5-0 ; 
       add_gun-t2d30.5-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       add_gun-t2d35.5-0 ; 
       add_gun-t2d38.5-0 ; 
       add_gun-t2d47.4-0 ; 
       add_gun-t2d48.4-0 ; 
       add_gun-t2d49.5-0 ; 
       add_gun-t2d50.6-0 ; 
       add_gun-t2d9.5-0 ; 
       nulls-t2d105.5-0 ; 
       nulls-t2d106.5-0 ; 
       nulls-t2d107.5-0 ; 
       nulls-t2d108.5-0 ; 
       nulls-t2d109.5-0 ; 
       nulls-t2d110.5-0 ; 
       nulls-t2d111.5-0 ; 
       nulls-t2d112.5-0 ; 
       nulls-t2d113.5-0 ; 
       nulls-t2d114.5-0 ; 
       nulls-t2d115.5-0 ; 
       nulls-t2d116.5-0 ; 
       nulls-t2d117.5-0 ; 
       nulls-t2d118.5-0 ; 
       nulls-t2d119.5-0 ; 
       nulls-t2d120.5-0 ; 
       nulls-t2d121.5-0 ; 
       nulls-t2d122.5-0 ; 
       nulls-t2d87.5-0 ; 
       nulls-t2d89.5-0 ; 
       nulls-t2d90.5-0 ; 
       nulls-t2d91.5-0 ; 
       nulls-t2d92.5-0 ; 
       nulls-t2d93.5-0 ; 
       nulls-t2d94.5-0 ; 
       nulls-t2d95.5-0 ; 
       nulls-t2d96.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 45 110 ; 
       3 34 110 ; 
       4 37 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 39 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 42 110 ; 
       11 13 110 ; 
       13 23 110 ; 
       14 42 110 ; 
       15 13 110 ; 
       16 10 110 ; 
       17 23 110 ; 
       18 24 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 23 110 ; 
       22 21 110 ; 
       23 12 110 ; 
       24 23 110 ; 
       25 19 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 21 110 ; 
       29 27 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 23 110 ; 
       35 55 110 ; 
       36 56 110 ; 
       37 55 110 ; 
       38 55 110 ; 
       39 56 110 ; 
       40 56 110 ; 
       41 26 110 ; 
       42 26 110 ; 
       43 21 110 ; 
       44 29 110 ; 
       45 23 110 ; 
       46 55 110 ; 
       47 56 110 ; 
       48 23 110 ; 
       49 23 110 ; 
       50 23 110 ; 
       51 23 110 ; 
       52 13 110 ; 
       53 23 110 ; 
       54 13 110 ; 
       55 2 110 ; 
       56 3 110 ; 
       57 46 110 ; 
       58 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 63 300 ; 
       3 51 300 ; 
       4 65 300 ; 
       5 66 300 ; 
       6 67 300 ; 
       7 59 300 ; 
       8 60 300 ; 
       9 61 300 ; 
       10 40 300 ; 
       10 48 300 ; 
       14 73 300 ; 
       14 42 300 ; 
       18 44 300 ; 
       18 47 300 ; 
       19 45 300 ; 
       20 46 300 ; 
       22 49 300 ; 
       23 0 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       23 50 300 ; 
       23 43 300 ; 
       26 31 300 ; 
       26 62 300 ; 
       27 24 300 ; 
       27 25 300 ; 
       27 26 300 ; 
       27 27 300 ; 
       28 6 300 ; 
       28 7 300 ; 
       28 8 300 ; 
       28 9 300 ; 
       29 20 300 ; 
       29 21 300 ; 
       29 22 300 ; 
       29 23 300 ; 
       30 28 300 ; 
       30 29 300 ; 
       30 30 300 ; 
       31 10 300 ; 
       31 11 300 ; 
       31 12 300 ; 
       31 13 300 ; 
       32 17 300 ; 
       32 18 300 ; 
       32 19 300 ; 
       33 36 300 ; 
       33 37 300 ; 
       33 38 300 ; 
       35 68 300 ; 
       36 55 300 ; 
       37 72 300 ; 
       38 74 300 ; 
       39 57 300 ; 
       40 58 300 ; 
       42 39 300 ; 
       43 2 300 ; 
       43 3 300 ; 
       43 4 300 ; 
       43 5 300 ; 
       44 14 300 ; 
       44 15 300 ; 
       44 16 300 ; 
       46 69 300 ; 
       47 56 300 ; 
       48 35 300 ; 
       49 34 300 ; 
       50 1 300 ; 
       51 41 300 ; 
       55 64 300 ; 
       55 70 300 ; 
       55 71 300 ; 
       56 52 300 ; 
       56 53 300 ; 
       56 54 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       32 10 400 ; 
       33 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       12 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       57 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       64 44 401 ; 
       65 48 401 ; 
       66 49 401 ; 
       67 50 401 ; 
       70 45 401 ; 
       71 46 401 ; 
       72 47 401 ; 
       73 51 401 ; 
       74 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 42.20412 5.746938 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.003865 2.003865 2.003865 3.051593 1.659119e-007 -1.570797 -1.11721 0.5573955 -0.1949797 MPRFLG 0 ; 
       59 SCHEM 41.38255 7.289917 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 36.25 -6 0 USR MPRFLG 0 ; 
       3 SCHEM 26.25 -6 0 USR MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 USR MPRFLG 0 ; 
       5 SCHEM 32.5 -14 0 USR MPRFLG 0 ; 
       6 SCHEM 32.5 -16 0 USR MPRFLG 0 ; 
       7 SCHEM 22.5 -12 0 USR MPRFLG 0 ; 
       8 SCHEM 22.5 -14 0 USR MPRFLG 0 ; 
       9 SCHEM 22.5 -16 0 USR MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 USR MPRFLG 0 ; 
       11 SCHEM 72.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.7132013 MPRFLG 0 ; 
       13 SCHEM 71.25 -4 0 USR MPRFLG 0 ; 
       14 SCHEM 5 -8 0 USR MPRFLG 0 ; 
       15 SCHEM 75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 43.75 -6 0 USR MPRFLG 0 ; 
       19 SCHEM 42.5 -8 0 USR MPRFLG 0 ; 
       20 SCHEM 45 -8 0 USR MPRFLG 0 ; 
       21 SCHEM 60 -4 0 USR MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       23 SCHEM 40 -2 0 USR MPRFLG 0 ; 
       24 SCHEM 43.75 -4 0 USR MPRFLG 0 ; 
       25 SCHEM 42.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       27 SCHEM 15 -4 0 USR MPRFLG 0 ; 
       28 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       29 SCHEM 15 -6 0 USR MPRFLG 0 ; 
       30 SCHEM 20 -8 0 USR MPRFLG 0 ; 
       31 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       32 SCHEM 17.5 -8 0 USR MPRFLG 0 ; 
       33 SCHEM 15 -8 0 USR MPRFLG 0 ; 
       34 SCHEM 26.25 -4 0 USR MPRFLG 0 ; 
       35 SCHEM 40 -10 0 USR MPRFLG 0 ; 
       36 SCHEM 27.5 -10 0 USR MPRFLG 0 ; 
       37 SCHEM 32.5 -10 0 USR MPRFLG 0 ; 
       38 SCHEM 35 -10 0 USR MPRFLG 0 ; 
       39 SCHEM 22.5 -10 0 USR MPRFLG 0 ; 
       40 SCHEM 25 -10 0 USR MPRFLG 0 ; 
       41 SCHEM 7.5 -6 0 USR MPRFLG 0 ; 
       42 SCHEM 3.75 -6 0 USR MPRFLG 0 ; 
       43 SCHEM 62.5 -6 0 USR MPRFLG 0 ; 
       44 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       45 SCHEM 36.25 -4 0 USR MPRFLG 0 ; 
       46 SCHEM 37.5 -10 0 USR MPRFLG 0 ; 
       47 SCHEM 30 -10 0 USR MPRFLG 0 ; 
       48 SCHEM 47.5 -4 0 USR MPRFLG 0 ; 
       49 SCHEM 50 -4 0 USR MPRFLG 0 ; 
       50 SCHEM 52.5 -4 0 USR MPRFLG 0 ; 
       51 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       52 SCHEM 70 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 77.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 67.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.25 -8 0 USR MPRFLG 0 ; 
       56 SCHEM 26.25 -8 0 USR MPRFLG 0 ; 
       57 SCHEM 37.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 30 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 39.70412 5.746938 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.003865 2.003865 2.003865 0.09000004 1.659119e-007 -1.570796 1.11721 0.5573955 -0.1949797 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 34 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
