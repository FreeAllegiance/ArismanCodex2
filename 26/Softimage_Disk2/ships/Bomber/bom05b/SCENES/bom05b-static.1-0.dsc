SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       add_gun-default7.3-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat45.3-0 ; 
       add_gun-mat47.3-0 ; 
       add_gun-mat50.3-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-mat69.3-0 ; 
       add_gun-mat71.3-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       nulls-0.3-0 ; 
       nulls-1.3-0 ; 
       nulls-mat109.3-0 ; 
       nulls-mat110.3-0 ; 
       nulls-mat111.3-0 ; 
       nulls-mat112.3-0 ; 
       nulls-mat113.3-0 ; 
       nulls-mat114.3-0 ; 
       nulls-mat115.3-0 ; 
       nulls-mat116.3-0 ; 
       nulls-mat117.3-0 ; 
       nulls-mat118.3-0 ; 
       nulls-mat119.3-0 ; 
       nulls-mat120.3-0 ; 
       nulls-mat121.3-0 ; 
       nulls-mat122.3-0 ; 
       nulls-mat123.3-0 ; 
       nulls-mat124.3-0 ; 
       nulls-mat125.3-0 ; 
       nulls-mat126.3-0 ; 
       nulls-mat128.3-0 ; 
       nulls-mat81.3-0 ; 
       nulls-mat82.3-0 ; 
       nulls-mat84.3-0 ; 
       nulls-mat85.3-0 ; 
       nulls-mat86.3-0 ; 
       nulls-mat88.3-0 ; 
       nulls-mat90.3-0 ; 
       nulls-mat92.3-0 ; 
       nulls-mat93.3-0 ; 
       nulls-mat94.3-0 ; 
       nulls-mat95.3-0 ; 
       nulls-mat96.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-antenn1.1-0 ; 
       nulls-antenn2.1-0 ; 
       nulls-antenn3.1-0 ; 
       nulls-antenn7.1-0 ; 
       nulls-antenn8.1-0 ; 
       nulls-antenn9.1-0 ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05_1.1-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-lwepbas_3.1-0 ; 
       nulls-lwepbas_5.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-rwepbas_3.1-0 ; 
       nulls-rwepbas_5.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
       nulls-turwepemt1.1-0 ; 
       nulls-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       add_gun-t2d10.3-0 ; 
       add_gun-t2d11.3-0 ; 
       add_gun-t2d15.3-0 ; 
       add_gun-t2d16.3-0 ; 
       add_gun-t2d17.3-0 ; 
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d28.3-0 ; 
       add_gun-t2d29.3-0 ; 
       add_gun-t2d30.3-0 ; 
       add_gun-t2d35.3-0 ; 
       add_gun-t2d38.3-0 ; 
       add_gun-t2d49.3-0 ; 
       add_gun-t2d50.4-0 ; 
       add_gun-t2d9.3-0 ; 
       nulls-t2d105.3-0 ; 
       nulls-t2d106.3-0 ; 
       nulls-t2d107.3-0 ; 
       nulls-t2d108.3-0 ; 
       nulls-t2d109.3-0 ; 
       nulls-t2d110.3-0 ; 
       nulls-t2d111.3-0 ; 
       nulls-t2d112.3-0 ; 
       nulls-t2d113.3-0 ; 
       nulls-t2d114.3-0 ; 
       nulls-t2d115.3-0 ; 
       nulls-t2d116.3-0 ; 
       nulls-t2d117.3-0 ; 
       nulls-t2d118.3-0 ; 
       nulls-t2d119.3-0 ; 
       nulls-t2d120.3-0 ; 
       nulls-t2d121.3-0 ; 
       nulls-t2d122.3-0 ; 
       nulls-t2d87.3-0 ; 
       nulls-t2d89.3-0 ; 
       nulls-t2d90.3-0 ; 
       nulls-t2d91.3-0 ; 
       nulls-t2d92.3-0 ; 
       nulls-t2d93.3-0 ; 
       nulls-t2d94.3-0 ; 
       nulls-t2d95.3-0 ; 
       nulls-t2d96.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 40 110 ; 
       1 29 110 ; 
       2 32 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 34 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 37 110 ; 
       9 11 110 ; 
       11 21 110 ; 
       12 37 110 ; 
       13 11 110 ; 
       14 8 110 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       21 10 110 ; 
       22 21 110 ; 
       23 17 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 19 110 ; 
       27 25 110 ; 
       28 27 110 ; 
       29 21 110 ; 
       30 50 110 ; 
       31 51 110 ; 
       32 50 110 ; 
       33 50 110 ; 
       34 51 110 ; 
       35 51 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 19 110 ; 
       39 27 110 ; 
       40 21 110 ; 
       41 50 110 ; 
       42 51 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 11 110 ; 
       48 21 110 ; 
       49 11 110 ; 
       50 0 110 ; 
       51 1 110 ; 
       52 41 110 ; 
       53 42 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 54 300 ; 
       1 42 300 ; 
       2 56 300 ; 
       3 57 300 ; 
       4 58 300 ; 
       5 50 300 ; 
       6 51 300 ; 
       7 52 300 ; 
       8 31 300 ; 
       8 39 300 ; 
       12 64 300 ; 
       12 33 300 ; 
       16 35 300 ; 
       16 38 300 ; 
       17 36 300 ; 
       18 37 300 ; 
       20 40 300 ; 
       21 0 300 ; 
       21 26 300 ; 
       21 27 300 ; 
       21 41 300 ; 
       21 34 300 ; 
       24 25 300 ; 
       24 53 300 ; 
       25 21 300 ; 
       25 22 300 ; 
       25 23 300 ; 
       25 24 300 ; 
       26 6 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 17 300 ; 
       27 18 300 ; 
       27 19 300 ; 
       27 20 300 ; 
       28 10 300 ; 
       28 11 300 ; 
       28 12 300 ; 
       28 13 300 ; 
       30 59 300 ; 
       31 46 300 ; 
       32 63 300 ; 
       33 65 300 ; 
       34 48 300 ; 
       35 49 300 ; 
       37 30 300 ; 
       38 2 300 ; 
       38 3 300 ; 
       38 4 300 ; 
       38 5 300 ; 
       39 14 300 ; 
       39 15 300 ; 
       39 16 300 ; 
       41 60 300 ; 
       42 47 300 ; 
       43 29 300 ; 
       44 28 300 ; 
       45 1 300 ; 
       46 32 300 ; 
       50 55 300 ; 
       50 61 300 ; 
       50 62 300 ; 
       51 43 300 ; 
       51 44 300 ; 
       51 45 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 19 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       19 10 401 ; 
       20 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       33 25 401 ; 
       34 28 401 ; 
       35 20 401 ; 
       36 21 401 ; 
       37 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       43 29 401 ; 
       44 30 401 ; 
       45 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 36 401 ; 
       53 37 401 ; 
       55 38 401 ; 
       56 42 401 ; 
       57 43 401 ; 
       58 44 401 ; 
       61 39 401 ; 
       62 40 401 ; 
       63 41 401 ; 
       64 45 401 ; 
       65 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 -6 0 USR MPRFLG 0 ; 
       1 SCHEM 26.25 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 32.5 -12 0 USR MPRFLG 0 ; 
       3 SCHEM 32.5 -14 0 USR MPRFLG 0 ; 
       4 SCHEM 32.5 -16 0 USR MPRFLG 0 ; 
       5 SCHEM 22.5 -12 0 USR MPRFLG 0 ; 
       6 SCHEM 22.5 -14 0 USR MPRFLG 0 ; 
       7 SCHEM 22.5 -16 0 USR MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 USR MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 0 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 71.25 -4 0 USR MPRFLG 0 ; 
       12 SCHEM 5 -8 0 USR MPRFLG 0 ; 
       13 SCHEM 75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 43.75 -6 0 USR MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 USR MPRFLG 0 ; 
       18 SCHEM 45 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 60 -4 0 USR MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       21 SCHEM 40 -2 0 USR MPRFLG 0 ; 
       22 SCHEM 43.75 -4 0 USR MPRFLG 0 ; 
       23 SCHEM 42.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       25 SCHEM 15 -4 0 USR MPRFLG 0 ; 
       26 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       27 SCHEM 15 -6 0 USR MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       29 SCHEM 26.25 -4 0 USR MPRFLG 0 ; 
       30 SCHEM 40 -10 0 USR MPRFLG 0 ; 
       31 SCHEM 27.5 -10 0 USR MPRFLG 0 ; 
       32 SCHEM 32.5 -10 0 USR MPRFLG 0 ; 
       33 SCHEM 35 -10 0 USR MPRFLG 0 ; 
       34 SCHEM 22.5 -10 0 USR MPRFLG 0 ; 
       35 SCHEM 25 -10 0 USR MPRFLG 0 ; 
       36 SCHEM 7.5 -6 0 USR MPRFLG 0 ; 
       37 SCHEM 3.75 -6 0 USR MPRFLG 0 ; 
       38 SCHEM 62.5 -6 0 USR MPRFLG 0 ; 
       39 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       40 SCHEM 36.25 -4 0 USR MPRFLG 0 ; 
       41 SCHEM 37.5 -10 0 USR MPRFLG 0 ; 
       42 SCHEM 30 -10 0 USR MPRFLG 0 ; 
       43 SCHEM 47.5 -4 0 USR MPRFLG 0 ; 
       44 SCHEM 50 -4 0 USR MPRFLG 0 ; 
       45 SCHEM 52.5 -4 0 USR MPRFLG 0 ; 
       46 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       47 SCHEM 70 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 67.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 36.25 -8 0 USR MPRFLG 0 ; 
       51 SCHEM 26.25 -8 0 USR MPRFLG 0 ; 
       52 SCHEM 37.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 30 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 21.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 31.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
