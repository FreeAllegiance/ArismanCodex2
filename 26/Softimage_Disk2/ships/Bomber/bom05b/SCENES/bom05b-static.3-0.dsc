SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       add_gun-default7.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat45.3-0 ; 
       add_gun-mat47.3-0 ; 
       add_gun-mat50.3-0 ; 
       nulls-1.3-0 ; 
       nulls-mat114.3-0 ; 
       nulls-mat115.3-0 ; 
       nulls-mat116.3-0 ; 
       nulls-mat117.3-0 ; 
       nulls-mat118.3-0 ; 
       nulls-mat119.3-0 ; 
       nulls-mat122.3-0 ; 
       nulls-mat123.3-0 ; 
       nulls-mat128.3-0 ; 
       nulls-mat81.3-0 ; 
       nulls-mat82.3-0 ; 
       nulls-mat92.3-0 ; 
       nulls-mat93.3-0 ; 
       nulls-mat94.3-0 ; 
       nulls-mat96.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-bom05_1.2-0 ROOT ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       add_gun-t2d10.3-0 ; 
       add_gun-t2d11.3-0 ; 
       add_gun-t2d15.3-0 ; 
       add_gun-t2d16.3-0 ; 
       add_gun-t2d17.3-0 ; 
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d28.3-0 ; 
       add_gun-t2d29.3-0 ; 
       add_gun-t2d30.3-0 ; 
       add_gun-t2d35.3-0 ; 
       add_gun-t2d38.3-0 ; 
       add_gun-t2d9.3-0 ; 
       nulls-t2d111.3-0 ; 
       nulls-t2d112.3-0 ; 
       nulls-t2d113.3-0 ; 
       nulls-t2d114.3-0 ; 
       nulls-t2d115.3-0 ; 
       nulls-t2d116.3-0 ; 
       nulls-t2d117.3-0 ; 
       nulls-t2d118.3-0 ; 
       nulls-t2d122.3-0 ; 
       nulls-t2d87.3-0 ; 
       nulls-t2d89.3-0 ; 
       nulls-t2d90.3-0 ; 
       nulls-t2d91.3-0 ; 
       nulls-t2d96.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 11 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 9 110 ; 
       11 5 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 20 110 ; 
       15 20 110 ; 
       16 3 110 ; 
       17 9 110 ; 
       18 5 110 ; 
       19 0 110 ; 
       20 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 37 300 ; 
       1 30 300 ; 
       4 28 300 ; 
       5 0 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 29 300 ; 
       5 27 300 ; 
       6 24 300 ; 
       6 36 300 ; 
       7 20 300 ; 
       7 21 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       12 41 300 ; 
       13 42 300 ; 
       14 34 300 ; 
       15 35 300 ; 
       16 1 300 ; 
       16 2 300 ; 
       16 3 300 ; 
       16 4 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       19 38 300 ; 
       19 39 300 ; 
       19 40 300 ; 
       20 31 300 ; 
       20 32 300 ; 
       20 33 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 17 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 20 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       31 21 401 ; 
       32 22 401 ; 
       33 23 401 ; 
       34 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 20 -8 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 0 -8 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 MPRFLG 0 ; 
       9 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 5 -12 0 MPRFLG 0 ; 
       11 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -14 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 15 -14 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -14 0 MPRFLG 0 ; 
       15 SCHEM 10 -14 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 13.75 -12 0 MPRFLG 0 ; 
       20 SCHEM 8.75 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
