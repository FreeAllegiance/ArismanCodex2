SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       add_gun-default7.5-0 ; 
       add_gun-mat10.5-0 ; 
       add_gun-mat11.5-0 ; 
       add_gun-mat12.5-0 ; 
       add_gun-mat13.5-0 ; 
       add_gun-mat18.5-0 ; 
       add_gun-mat19.5-0 ; 
       add_gun-mat20.5-0 ; 
       add_gun-mat21.5-0 ; 
       add_gun-mat22.5-0 ; 
       add_gun-mat23.5-0 ; 
       add_gun-mat24.5-0 ; 
       add_gun-mat25.5-0 ; 
       add_gun-mat26.5-0 ; 
       add_gun-mat27.5-0 ; 
       add_gun-mat28.5-0 ; 
       add_gun-mat34.5-0 ; 
       add_gun-mat35.5-0 ; 
       add_gun-mat36.5-0 ; 
       add_gun-mat37.5-0 ; 
       add_gun-mat38.5-0 ; 
       add_gun-mat39.5-0 ; 
       add_gun-mat40.5-0 ; 
       add_gun-mat41.5-0 ; 
       add_gun-mat45.5-0 ; 
       add_gun-mat47.5-0 ; 
       add_gun-mat50.5-0 ; 
       add_gun-mat69.5-0 ; 
       nulls-1.5-0 ; 
       nulls-mat109.5-0 ; 
       nulls-mat112.5-0 ; 
       nulls-mat114.5-0 ; 
       nulls-mat115.5-0 ; 
       nulls-mat116.5-0 ; 
       nulls-mat117.5-0 ; 
       nulls-mat118.5-0 ; 
       nulls-mat119.5-0 ; 
       nulls-mat122.5-0 ; 
       nulls-mat128.5-0 ; 
       nulls-mat81.5-0 ; 
       nulls-mat82.5-0 ; 
       nulls-mat92.5-0 ; 
       nulls-mat93.5-0 ; 
       nulls-mat94.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-bom05.10-0 ROOT ; 
       nulls-cyl4.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_gun-t2d10.5-0 ; 
       add_gun-t2d11.5-0 ; 
       add_gun-t2d15.5-0 ; 
       add_gun-t2d16.5-0 ; 
       add_gun-t2d17.5-0 ; 
       add_gun-t2d18.5-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d20.5-0 ; 
       add_gun-t2d21.5-0 ; 
       add_gun-t2d25.5-0 ; 
       add_gun-t2d26.5-0 ; 
       add_gun-t2d27.5-0 ; 
       add_gun-t2d28.5-0 ; 
       add_gun-t2d29.5-0 ; 
       add_gun-t2d30.5-0 ; 
       add_gun-t2d35.5-0 ; 
       add_gun-t2d38.5-0 ; 
       add_gun-t2d49.5-0 ; 
       add_gun-t2d9.5-0 ; 
       nulls-t2d105.5-0 ; 
       nulls-t2d108.5-0 ; 
       nulls-t2d111.5-0 ; 
       nulls-t2d112.5-0 ; 
       nulls-t2d113.5-0 ; 
       nulls-t2d114.5-0 ; 
       nulls-t2d115.5-0 ; 
       nulls-t2d116.5-0 ; 
       nulls-t2d117.5-0 ; 
       nulls-t2d122.5-0 ; 
       nulls-t2d87.5-0 ; 
       nulls-t2d89.5-0 ; 
       nulls-t2d90.5-0 ; 
       nulls-t2d91.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 13 110 ; 
       3 7 110 ; 
       4 6 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 4 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 6 110 ; 
       14 20 110 ; 
       15 21 110 ; 
       16 8 110 ; 
       17 4 110 ; 
       18 11 110 ; 
       19 6 110 ; 
       20 0 110 ; 
       21 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       1 33 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       5 31 300 ; 
       6 0 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 32 300 ; 
       6 28 300 ; 
       8 24 300 ; 
       8 38 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       14 43 300 ; 
       15 37 300 ; 
       16 27 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       18 13 300 ; 
       18 14 300 ; 
       18 15 300 ; 
       20 40 300 ; 
       20 41 300 ; 
       20 42 300 ; 
       21 34 300 ; 
       21 35 300 ; 
       21 36 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 18 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 23 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 22 401 ; 
       34 24 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 -6 0 USR MPRFLG 0 ; 
       1 SCHEM 26.25 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 40 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.7132013 MPRFLG 0 ; 
       3 SCHEM 43.75 -6 0 USR MPRFLG 0 ; 
       4 SCHEM 60 -4 0 USR MPRFLG 0 ; 
       5 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       6 SCHEM 40 -2 0 USR MPRFLG 0 ; 
       7 SCHEM 43.75 -4 0 USR MPRFLG 0 ; 
       8 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       9 SCHEM 15 -4 0 USR MPRFLG 0 ; 
       10 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       11 SCHEM 15 -6 0 USR MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       13 SCHEM 26.25 -4 0 USR MPRFLG 0 ; 
       14 SCHEM 32.5 -10 0 USR MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 USR MPRFLG 0 ; 
       16 SCHEM 3.75 -6 0 USR MPRFLG 0 ; 
       17 SCHEM 62.5 -6 0 USR MPRFLG 0 ; 
       18 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 36.25 -4 0 USR MPRFLG 0 ; 
       20 SCHEM 36.25 -8 0 USR MPRFLG 0 ; 
       21 SCHEM 26.25 -8 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
