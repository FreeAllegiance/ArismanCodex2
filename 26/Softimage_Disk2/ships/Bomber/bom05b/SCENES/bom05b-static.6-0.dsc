SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.9-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       static-1.2-0 ; 
       static-2.2-0 ; 
       static-default1.2-0 ; 
       static-durethff.1-0 ; 
       static-mat1.2-0 ; 
       static-mat10.2-0 ; 
       static-mat11.2-0 ; 
       static-mat12.2-0 ; 
       static-mat13.2-0 ; 
       static-mat14.2-0 ; 
       static-mat15.2-0 ; 
       static-mat16.2-0 ; 
       static-mat17.2-0 ; 
       static-mat18.2-0 ; 
       static-mat19.2-0 ; 
       static-mat2.2-0 ; 
       static-mat20.2-0 ; 
       static-mat21.2-0 ; 
       static-mat22.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat26.2-0 ; 
       static-mat27.2-0 ; 
       static-mat28.2-0 ; 
       static-mat29.2-0 ; 
       static-mat3.2-0 ; 
       static-mat30.2-0 ; 
       static-mat31.2-0 ; 
       static-mat32.2-0 ; 
       static-mat33.2-0 ; 
       static-mat34.2-0 ; 
       static-mat35.2-0 ; 
       static-mat36.2-0 ; 
       static-mat37.2-0 ; 
       static-mat38.2-0 ; 
       static-mat39.2-0 ; 
       static-mat4.2-0 ; 
       static-mat40.2-0 ; 
       static-mat41.1-0 ; 
       static-mat42.1-0 ; 
       static-mat43.1-0 ; 
       static-mat44.1-0 ; 
       static-mat45.1-0 ; 
       static-mat46.1-0 ; 
       static-mat47.1-0 ; 
       static-mat48.1-0 ; 
       static-mat49.1-0 ; 
       static-mat5.2-0 ; 
       static-mat50.1-0 ; 
       static-mat6.2-0 ; 
       static-mat7.2-0 ; 
       static-mat8.2-0 ; 
       static-mat9.2-0 ; 
       static-starboardgrn.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       nulls-blgun_1.1-0 ; 
       nulls-bom05_1.4-0 ROOT ; 
       nulls-brgun_1.1-0 ; 
       nulls-cyl4_1.1-0 ; 
       nulls-face2_1.1-0 ; 
       nulls-face3_1.1-0 ; 
       nulls-finzzz0_1.1-0 ; 
       nulls-finzzz1_1.2-0 ; 
       nulls-fuselg_1.2-0 ; 
       nulls-fwepatt_1.1-0 ; 
       nulls-hatchz_1.1-0 ; 
       nulls-landgr1_1.1-0 ; 
       nulls-lfinzzz_1.1-0 ; 
       nulls-LL1_1.2-0 ; 
       nulls-llandgr_1.1-0 ; 
       nulls-LLl_1.1-0 ; 
       nulls-LLr_1.1-0 ; 
       nulls-lturatt_1.1-0 ; 
       nulls-pod_1.1-0 ; 
       nulls-rfinzzz_1.1-0 ; 
       nulls-rlandgr_1.1-0 ; 
       nulls-rturatt_1.1-0 ; 
       nulls-SSa_1.2-0 ; 
       nulls-SSl_1.2-0 ; 
       nulls-SSr_1.2-0 ; 
       nulls-turret1_1.1-0 ; 
       nulls-turret3_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       static-t2d1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d19.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d21.1-0 ; 
       static-t2d22.1-0 ; 
       static-t2d23.1-0 ; 
       static-t2d24.1-0 ; 
       static-t2d25.1-0 ; 
       static-t2d26.1-0 ; 
       static-t2d27.1-0 ; 
       static-t2d28.1-0 ; 
       static-t2d29.1-0 ; 
       static-t2d3.1-0 ; 
       static-t2d30.1-0 ; 
       static-t2d31.1-0 ; 
       static-t2d32.1-0 ; 
       static-t2d33.1-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d36.1-0 ; 
       static-t2d37.1-0 ; 
       static-t2d38.1-0 ; 
       static-t2d39.1-0 ; 
       static-t2d4.1-0 ; 
       static-t2d40.1-0 ; 
       static-t2d41.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       2 18 110 ; 
       3 9 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 6 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 8 110 ; 
       18 10 110 ; 
       19 6 110 ; 
       20 13 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 21 110 ; 
       26 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 51 300 ; 
       0 52 300 ; 
       2 53 300 ; 
       2 1 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       4 47 300 ; 
       5 49 300 ; 
       7 13 300 ; 
       8 2 300 ; 
       8 4 300 ; 
       8 15 300 ; 
       8 26 300 ; 
       8 0 300 ; 
       10 37 300 ; 
       10 48 300 ; 
       11 14 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       14 30 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       14 33 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       16 34 300 ; 
       16 35 300 ; 
       16 36 300 ; 
       18 50 300 ; 
       19 5 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       20 27 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       22 38 300 ; 
       23 3 300 ; 
       24 54 300 ; 
       25 42 300 ; 
       25 43 300 ; 
       25 44 300 ; 
       26 39 300 ; 
       26 40 300 ; 
       26 41 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 17 400 ; 
       16 24 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       15 11 401 ; 
       26 22 401 ; 
       0 33 401 ; 
       48 36 401 ; 
       50 37 401 ; 
       51 38 401 ; 
       52 39 401 ; 
       53 40 401 ; 
       1 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       24 16 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       32 20 401 ; 
       33 21 401 ; 
       35 23 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       43 29 401 ; 
       44 30 401 ; 
       45 31 401 ; 
       46 32 401 ; 
       47 34 401 ; 
       49 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 4.964405 -2.155313 0 MPRFLG 0 ; 
       1 SCHEM 23.7144 5.844687 0 USR SRT 1 1 1 0 0 0 0 0 -0.7132013 MPRFLG 0 ; 
       2 SCHEM 7.464405 -2.155313 0 MPRFLG 0 ; 
       3 SCHEM 26.2144 -0.1553124 0 MPRFLG 0 ; 
       4 SCHEM 24.9644 -2.155313 0 MPRFLG 0 ; 
       5 SCHEM 27.4644 -2.155313 0 MPRFLG 0 ; 
       6 SCHEM 39.9644 1.844687 0 MPRFLG 0 ; 
       7 SCHEM 37.4644 -0.1553124 0 MPRFLG 0 ; 
       8 SCHEM 23.7144 3.844687 0 MPRFLG 0 ; 
       9 SCHEM 26.2144 1.844687 0 MPRFLG 0 ; 
       10 SCHEM 6.214405 1.844687 0 MPRFLG 0 ; 
       11 SCHEM 13.7144 1.844687 0 MPRFLG 0 ; 
       12 SCHEM 39.9644 -0.1553124 0 MPRFLG 0 ; 
       13 SCHEM 13.7144 -0.1553124 0 MPRFLG 0 ; 
       14 SCHEM 12.4644 -2.155313 0 MPRFLG 0 ; 
       15 SCHEM 17.4644 -2.155313 0 MPRFLG 0 ; 
       16 SCHEM 14.9644 -2.155313 0 MPRFLG 0 ; 
       17 SCHEM 19.9644 1.844687 0 MPRFLG 0 ; 
       18 SCHEM 6.214405 -0.1553124 0 MPRFLG 0 ; 
       19 SCHEM 42.4644 -0.1553124 0 MPRFLG 0 ; 
       20 SCHEM 9.964401 -2.155313 0 MPRFLG 0 ; 
       21 SCHEM 22.4644 1.844687 0 MPRFLG 0 ; 
       22 SCHEM 29.9644 1.844687 0 MPRFLG 0 ; 
       23 SCHEM 32.4644 1.844687 0 MPRFLG 0 ; 
       24 SCHEM 34.9644 1.844687 0 MPRFLG 0 ; 
       25 SCHEM 22.4644 -0.1553124 0 MPRFLG 0 ; 
       26 SCHEM 19.9644 -0.1553124 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 247.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 247.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 247.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 242.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 242.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 245 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 245 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 280 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 280 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 280 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 280 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 277.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 277.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 277.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 277.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 275 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 255 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 255 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 255 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 247.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 247.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 247.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 250 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 250 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 250 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 250 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 252.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 252.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 252.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 267.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 272.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 270 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 260 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 260 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 260 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 267.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 267.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 262.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 265 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 282.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 282.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 282.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 282.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 247.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 247.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 242.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 242.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 245 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 245 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 280 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 280 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 280 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 277.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 277.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 277.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 275 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 257.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 257.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 257.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 255 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 255 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 247.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 247.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 250 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 250 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 252.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 252.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 257.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 260 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 260 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 260 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 267.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 267.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 262.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 265 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
