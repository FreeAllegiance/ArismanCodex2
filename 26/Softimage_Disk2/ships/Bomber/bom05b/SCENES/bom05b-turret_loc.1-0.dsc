SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       add_gun-default7.4-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.4-0 ; 
       add_gun-mat10.4-0 ; 
       add_gun-mat11.4-0 ; 
       add_gun-mat12.4-0 ; 
       add_gun-mat13.4-0 ; 
       add_gun-mat18.4-0 ; 
       add_gun-mat19.4-0 ; 
       add_gun-mat20.4-0 ; 
       add_gun-mat21.4-0 ; 
       add_gun-mat22.4-0 ; 
       add_gun-mat23.4-0 ; 
       add_gun-mat24.4-0 ; 
       add_gun-mat25.4-0 ; 
       add_gun-mat26.4-0 ; 
       add_gun-mat27.4-0 ; 
       add_gun-mat28.4-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.4-0 ; 
       add_gun-mat35.4-0 ; 
       add_gun-mat36.4-0 ; 
       add_gun-mat37.4-0 ; 
       add_gun-mat38.4-0 ; 
       add_gun-mat39.4-0 ; 
       add_gun-mat40.4-0 ; 
       add_gun-mat41.4-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat45.4-0 ; 
       add_gun-mat47.4-0 ; 
       add_gun-mat50.4-0 ; 
       add_gun-mat62.4-0 ; 
       add_gun-mat63.4-0 ; 
       add_gun-mat66.3-0 ; 
       add_gun-mat67.3-0 ; 
       add_gun-mat68.3-0 ; 
       add_gun-mat69.4-0 ; 
       add_gun-mat71.4-0 ; 
       add_gun-starbord_green-right.1-0.4-0 ; 
       nulls-0.4-0 ; 
       nulls-1.4-0 ; 
       nulls-mat109.4-0 ; 
       nulls-mat110.4-0 ; 
       nulls-mat111.4-0 ; 
       nulls-mat112.4-0 ; 
       nulls-mat113.4-0 ; 
       nulls-mat114.4-0 ; 
       nulls-mat115.4-0 ; 
       nulls-mat116.4-0 ; 
       nulls-mat117.4-0 ; 
       nulls-mat118.4-0 ; 
       nulls-mat119.4-0 ; 
       nulls-mat120.4-0 ; 
       nulls-mat121.4-0 ; 
       nulls-mat122.4-0 ; 
       nulls-mat123.4-0 ; 
       nulls-mat124.4-0 ; 
       nulls-mat125.4-0 ; 
       nulls-mat126.4-0 ; 
       nulls-mat128.4-0 ; 
       nulls-mat81.4-0 ; 
       nulls-mat82.4-0 ; 
       nulls-mat84.4-0 ; 
       nulls-mat85.4-0 ; 
       nulls-mat86.4-0 ; 
       nulls-mat88.4-0 ; 
       nulls-mat90.4-0 ; 
       nulls-mat92.4-0 ; 
       nulls-mat93.4-0 ; 
       nulls-mat94.4-0 ; 
       nulls-mat95.4-0 ; 
       nulls-mat96.4-0 ; 
       turret_loc-mat129.1-0 ; 
       turret_loc-mat130.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 59     
       arc-cone.1-0 ROOT ; 
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-antenn1.1-0 ; 
       nulls-antenn2.1-0 ; 
       nulls-antenn3.1-0 ; 
       nulls-antenn7.1-0 ; 
       nulls-antenn8.1-0 ; 
       nulls-antenn9.1-0 ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.7-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-lwepbas_3.1-0 ; 
       nulls-lwepbas_5.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-rwepbas_3.1-0 ; 
       nulls-rwepbas_5.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
       nulls-turwepemt1.1-0 ; 
       nulls-turwepemt2.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.4-0 ; 
       add_gun-t2d26.4-0 ; 
       add_gun-t2d27.4-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       add_gun-t2d35.4-0 ; 
       add_gun-t2d38.4-0 ; 
       add_gun-t2d47.3-0 ; 
       add_gun-t2d48.3-0 ; 
       add_gun-t2d49.4-0 ; 
       add_gun-t2d50.5-0 ; 
       add_gun-t2d9.4-0 ; 
       nulls-t2d105.4-0 ; 
       nulls-t2d106.4-0 ; 
       nulls-t2d107.4-0 ; 
       nulls-t2d108.4-0 ; 
       nulls-t2d109.4-0 ; 
       nulls-t2d110.4-0 ; 
       nulls-t2d111.4-0 ; 
       nulls-t2d112.4-0 ; 
       nulls-t2d113.4-0 ; 
       nulls-t2d114.4-0 ; 
       nulls-t2d115.4-0 ; 
       nulls-t2d116.4-0 ; 
       nulls-t2d117.4-0 ; 
       nulls-t2d118.4-0 ; 
       nulls-t2d119.4-0 ; 
       nulls-t2d120.4-0 ; 
       nulls-t2d121.4-0 ; 
       nulls-t2d122.4-0 ; 
       nulls-t2d87.4-0 ; 
       nulls-t2d89.4-0 ; 
       nulls-t2d90.4-0 ; 
       nulls-t2d91.4-0 ; 
       nulls-t2d92.4-0 ; 
       nulls-t2d93.4-0 ; 
       nulls-t2d94.4-0 ; 
       nulls-t2d95.4-0 ; 
       nulls-t2d96.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 44 110 ; 
       2 33 110 ; 
       3 36 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 38 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 41 110 ; 
       10 12 110 ; 
       12 22 110 ; 
       13 41 110 ; 
       14 12 110 ; 
       15 9 110 ; 
       16 22 110 ; 
       17 23 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 22 110 ; 
       21 20 110 ; 
       22 11 110 ; 
       23 22 110 ; 
       24 18 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 20 110 ; 
       28 26 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 28 110 ; 
       33 22 110 ; 
       34 54 110 ; 
       35 55 110 ; 
       36 54 110 ; 
       37 54 110 ; 
       38 55 110 ; 
       39 55 110 ; 
       40 25 110 ; 
       41 25 110 ; 
       42 20 110 ; 
       43 28 110 ; 
       44 22 110 ; 
       45 54 110 ; 
       46 55 110 ; 
       47 22 110 ; 
       48 22 110 ; 
       49 22 110 ; 
       50 22 110 ; 
       51 12 110 ; 
       52 22 110 ; 
       53 12 110 ; 
       54 1 110 ; 
       55 2 110 ; 
       56 45 110 ; 
       57 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 63 300 ; 
       2 51 300 ; 
       3 65 300 ; 
       4 66 300 ; 
       5 67 300 ; 
       6 59 300 ; 
       7 60 300 ; 
       8 61 300 ; 
       9 40 300 ; 
       9 48 300 ; 
       13 73 300 ; 
       13 42 300 ; 
       17 44 300 ; 
       17 47 300 ; 
       18 45 300 ; 
       19 46 300 ; 
       21 49 300 ; 
       22 0 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       22 50 300 ; 
       22 43 300 ; 
       25 31 300 ; 
       25 62 300 ; 
       26 24 300 ; 
       26 25 300 ; 
       26 26 300 ; 
       26 27 300 ; 
       27 6 300 ; 
       27 7 300 ; 
       27 8 300 ; 
       27 9 300 ; 
       28 20 300 ; 
       28 21 300 ; 
       28 22 300 ; 
       28 23 300 ; 
       29 28 300 ; 
       29 29 300 ; 
       29 30 300 ; 
       30 10 300 ; 
       30 11 300 ; 
       30 12 300 ; 
       30 13 300 ; 
       31 17 300 ; 
       31 18 300 ; 
       31 19 300 ; 
       32 36 300 ; 
       32 37 300 ; 
       32 38 300 ; 
       34 68 300 ; 
       35 55 300 ; 
       36 72 300 ; 
       37 74 300 ; 
       38 57 300 ; 
       39 58 300 ; 
       41 39 300 ; 
       42 2 300 ; 
       42 3 300 ; 
       42 4 300 ; 
       42 5 300 ; 
       43 14 300 ; 
       43 15 300 ; 
       43 16 300 ; 
       45 69 300 ; 
       46 56 300 ; 
       47 35 300 ; 
       48 34 300 ; 
       49 1 300 ; 
       50 41 300 ; 
       58 76 300 ; 
       54 64 300 ; 
       54 70 300 ; 
       54 71 300 ; 
       55 52 300 ; 
       55 53 300 ; 
       55 54 300 ; 
       0 75 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       31 10 400 ; 
       32 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       11 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       57 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       64 44 401 ; 
       65 48 401 ; 
       66 49 401 ; 
       67 50 401 ; 
       70 45 401 ; 
       71 46 401 ; 
       72 47 401 ; 
       73 51 401 ; 
       74 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 36.25 -6 0 USR MPRFLG 0 ; 
       2 SCHEM 26.25 -6 0 USR MPRFLG 0 ; 
       3 SCHEM 32.5 -12 0 USR MPRFLG 0 ; 
       4 SCHEM 32.5 -14 0 USR MPRFLG 0 ; 
       5 SCHEM 32.5 -16 0 USR MPRFLG 0 ; 
       6 SCHEM 22.5 -12 0 USR MPRFLG 0 ; 
       7 SCHEM 22.5 -14 0 USR MPRFLG 0 ; 
       8 SCHEM 22.5 -16 0 USR MPRFLG 0 ; 
       9 SCHEM 2.5 -8 0 USR MPRFLG 0 ; 
       10 SCHEM 72.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 40 0 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 71.25 -4 0 USR MPRFLG 0 ; 
       13 SCHEM 5 -8 0 USR MPRFLG 0 ; 
       14 SCHEM 75 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 43.75 -6 0 USR MPRFLG 0 ; 
       18 SCHEM 42.5 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 45 -8 0 USR MPRFLG 0 ; 
       20 SCHEM 60 -4 0 USR MPRFLG 0 ; 
       21 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       22 SCHEM 40 -2 0 USR MPRFLG 0 ; 
       23 SCHEM 43.75 -4 0 USR MPRFLG 0 ; 
       24 SCHEM 42.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       26 SCHEM 15 -4 0 USR MPRFLG 0 ; 
       27 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       28 SCHEM 15 -6 0 USR MPRFLG 0 ; 
       29 SCHEM 20 -8 0 USR MPRFLG 0 ; 
       30 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       31 SCHEM 17.5 -8 0 USR MPRFLG 0 ; 
       32 SCHEM 15 -8 0 USR MPRFLG 0 ; 
       33 SCHEM 26.25 -4 0 USR MPRFLG 0 ; 
       34 SCHEM 40 -10 0 USR MPRFLG 0 ; 
       35 SCHEM 27.5 -10 0 USR MPRFLG 0 ; 
       36 SCHEM 32.5 -10 0 USR MPRFLG 0 ; 
       37 SCHEM 35 -10 0 USR MPRFLG 0 ; 
       38 SCHEM 22.5 -10 0 USR MPRFLG 0 ; 
       39 SCHEM 25 -10 0 USR MPRFLG 0 ; 
       40 SCHEM 7.5 -6 0 USR MPRFLG 0 ; 
       41 SCHEM 3.75 -6 0 USR MPRFLG 0 ; 
       42 SCHEM 62.5 -6 0 USR MPRFLG 0 ; 
       43 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       44 SCHEM 36.25 -4 0 USR MPRFLG 0 ; 
       45 SCHEM 37.5 -10 0 USR MPRFLG 0 ; 
       46 SCHEM 30 -10 0 USR MPRFLG 0 ; 
       47 SCHEM 47.5 -4 0 USR MPRFLG 0 ; 
       48 SCHEM 50 -4 0 USR MPRFLG 0 ; 
       49 SCHEM 52.5 -4 0 USR MPRFLG 0 ; 
       50 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       58 SCHEM 85 0 0 SRT 1.78995 1.789948 1.789949 -0.54 3.141593 1.570796 -0.6210856 0.3098704 0.2880926 MPRFLG 0 ; 
       51 SCHEM 70 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 77.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 67.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 36.25 -8 0 USR MPRFLG 0 ; 
       55 SCHEM 26.25 -8 0 USR MPRFLG 0 ; 
       56 SCHEM 37.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 30 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 82.5 0 0 SRT 1.78995 1.789948 1.789949 0.55 2.334495e-008 -1.570796 0.6210856 0.3098704 0.2880926 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 34 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 98 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
