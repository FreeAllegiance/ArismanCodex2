SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-null1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat45.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
       nulls-0.1-0 ; 
       nulls-1.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat112.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat116.1-0 ; 
       nulls-mat117.1-0 ; 
       nulls-mat118.1-0 ; 
       nulls-mat119.1-0 ; 
       nulls-mat120.1-0 ; 
       nulls-mat121.1-0 ; 
       nulls-mat122.1-0 ; 
       nulls-mat123.1-0 ; 
       nulls-mat124.1-0 ; 
       nulls-mat125.1-0 ; 
       nulls-mat126.1-0 ; 
       nulls-mat127.1-0 ; 
       nulls-mat128.1-0 ; 
       nulls-mat81.1-0 ; 
       nulls-mat82.1-0 ; 
       nulls-mat84.1-0 ; 
       nulls-mat85.1-0 ; 
       nulls-mat86.1-0 ; 
       nulls-mat88.1-0 ; 
       nulls-mat90.1-0 ; 
       nulls-mat91.1-0 ; 
       nulls-mat92.1-0 ; 
       nulls-mat93.1-0 ; 
       nulls-mat94.1-0 ; 
       nulls-mat95.1-0 ; 
       nulls-mat96.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 57     
       nulls-acs63_1.1-0 ; 
       nulls-acs63_3.1-0 ; 
       nulls-antenn1.1-0 ; 
       nulls-antenn2.1-0 ; 
       nulls-antenn3.1-0 ; 
       nulls-antenn7.1-0 ; 
       nulls-antenn8.1-0 ; 
       nulls-antenn9.1-0 ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.7-0 ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-hatchz.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-lturatt.1-0 ; 
       nulls-lwepbas_3.1-0 ; 
       nulls-lwepbas_5.1-0 ; 
       nulls-lwepemt.1-0 ; 
       nulls-Lwingzz.1-0 ; 
       nulls-Lwingzz1.1-0 ; 
       nulls-Lwingzz4.1-0 ; 
       nulls-Lwingzz5.1-0 ; 
       nulls-null1.1-0 ROOT ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-rturatt.1-0 ; 
       nulls-rwepbas_3.1-0 ; 
       nulls-rwepbas_5.1-0 ; 
       nulls-rwepemt.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-thrust.1-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turret1.1-0 ; 
       nulls-turret3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom05b/PICTURES/bom05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom05b_final-nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d35.1-0 ; 
       add_gun-t2d38.1-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.1-0 ; 
       add_gun-t2d50.2-0 ; 
       add_gun-t2d9.1-0 ; 
       nulls-t2d105.1-0 ; 
       nulls-t2d106.1-0 ; 
       nulls-t2d107.1-0 ; 
       nulls-t2d108.1-0 ; 
       nulls-t2d109.1-0 ; 
       nulls-t2d110.1-0 ; 
       nulls-t2d111.1-0 ; 
       nulls-t2d112.1-0 ; 
       nulls-t2d113.1-0 ; 
       nulls-t2d114.1-0 ; 
       nulls-t2d115.1-0 ; 
       nulls-t2d116.1-0 ; 
       nulls-t2d117.1-0 ; 
       nulls-t2d118.1-0 ; 
       nulls-t2d119.1-0 ; 
       nulls-t2d120.1-0 ; 
       nulls-t2d121.1-0 ; 
       nulls-t2d122.1-0 ; 
       nulls-t2d87.1-0 ; 
       nulls-t2d89.1-0 ; 
       nulls-t2d90.1-0 ; 
       nulls-t2d91.1-0 ; 
       nulls-t2d92.1-0 ; 
       nulls-t2d93.1-0 ; 
       nulls-t2d94.1-0 ; 
       nulls-t2d95.1-0 ; 
       nulls-t2d96.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 44 110 ; 
       1 32 110 ; 
       2 36 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 38 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 41 110 ; 
       9 11 110 ; 
       14 8 110 ; 
       10 40 110 ; 
       11 21 110 ; 
       12 41 110 ; 
       13 11 110 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 21 110 ; 
       20 19 110 ; 
       21 10 110 ; 
       22 21 110 ; 
       23 17 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 19 110 ; 
       27 25 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 21 110 ; 
       33 55 110 ; 
       34 56 110 ; 
       35 46 110 ; 
       36 55 110 ; 
       37 55 110 ; 
       38 56 110 ; 
       39 56 110 ; 
       41 24 110 ; 
       42 19 110 ; 
       43 27 110 ; 
       44 21 110 ; 
       45 55 110 ; 
       46 56 110 ; 
       47 45 110 ; 
       48 21 110 ; 
       49 21 110 ; 
       50 21 110 ; 
       51 21 110 ; 
       52 21 110 ; 
       53 11 110 ; 
       54 11 110 ; 
       55 0 110 ; 
       56 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 64 300 ; 
       1 51 300 ; 
       2 66 300 ; 
       3 67 300 ; 
       4 68 300 ; 
       5 59 300 ; 
       6 60 300 ; 
       7 61 300 ; 
       8 40 300 ; 
       8 48 300 ; 
       12 75 300 ; 
       12 42 300 ; 
       16 44 300 ; 
       16 47 300 ; 
       17 45 300 ; 
       18 46 300 ; 
       20 49 300 ; 
       21 0 300 ; 
       21 32 300 ; 
       21 33 300 ; 
       21 50 300 ; 
       21 43 300 ; 
       24 31 300 ; 
       24 63 300 ; 
       25 24 300 ; 
       25 25 300 ; 
       25 26 300 ; 
       25 27 300 ; 
       26 6 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 20 300 ; 
       27 21 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 30 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       30 17 300 ; 
       30 18 300 ; 
       30 19 300 ; 
       31 36 300 ; 
       31 37 300 ; 
       31 38 300 ; 
       33 69 300 ; 
       34 55 300 ; 
       35 62 300 ; 
       36 74 300 ; 
       37 76 300 ; 
       38 57 300 ; 
       39 58 300 ; 
       41 39 300 ; 
       42 2 300 ; 
       42 3 300 ; 
       42 4 300 ; 
       42 5 300 ; 
       43 14 300 ; 
       43 15 300 ; 
       43 16 300 ; 
       45 70 300 ; 
       46 56 300 ; 
       47 71 300 ; 
       48 35 300 ; 
       49 34 300 ; 
       50 1 300 ; 
       51 41 300 ; 
       55 65 300 ; 
       55 72 300 ; 
       55 73 300 ; 
       56 52 300 ; 
       56 53 300 ; 
       56 54 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       30 10 400 ; 
       31 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       40 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       37 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       42 31 401 ; 
       43 34 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 32 401 ; 
       50 33 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       57 38 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       63 43 401 ; 
       65 44 401 ; 
       66 48 401 ; 
       67 49 401 ; 
       68 50 401 ; 
       72 45 401 ; 
       73 46 401 ; 
       74 47 401 ; 
       75 51 401 ; 
       76 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 77.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 MPRFLG 0 ; 
       3 SCHEM 30 -14 0 MPRFLG 0 ; 
       4 SCHEM 30 -16 0 MPRFLG 0 ; 
       5 SCHEM 20 -12 0 MPRFLG 0 ; 
       6 SCHEM 20 -14 0 MPRFLG 0 ; 
       7 SCHEM 20 -16 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 70 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 38.75 0 0 MPRFLG 0 ; 
       11 SCHEM 68.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 40 -8 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 MPRFLG 0 ; 
       21 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       22 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 10 -8 0 MPRFLG 0 ; 
       30 SCHEM 15 -8 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 25 -10 0 MPRFLG 0 ; 
       35 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30 -10 0 MPRFLG 0 ; 
       37 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 20 -10 0 MPRFLG 0 ; 
       39 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       41 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       42 SCHEM 60 -6 0 MPRFLG 0 ; 
       43 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       44 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       45 SCHEM 35 -10 0 MPRFLG 0 ; 
       46 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 45 -4 0 MPRFLG 0 ; 
       49 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       50 SCHEM 50 -4 0 MPRFLG 0 ; 
       51 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       52 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       56 SCHEM 23.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 19 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 19 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 31.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 29 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 80 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 101 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
