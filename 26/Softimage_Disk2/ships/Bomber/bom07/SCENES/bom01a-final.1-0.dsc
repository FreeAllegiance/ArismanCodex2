SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.22-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.34-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.34-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       final-mat280.1-0 ; 
       final-mat69.1-0 ; 
       il_light_bomber_sT-default1.2-0 ; 
       il_light_bomber_sT-mat99.3-0 ; 
       retext-back_shelf.1-0 ; 
       retext-bottom.1-0 ; 
       retext-default10.1-0 ; 
       retext-default11.1-0 ; 
       retext-default12.3-0 ; 
       retext-default13.1-0 ; 
       retext-global.1-0 ; 
       retext-lower_back_panel.1-0 ; 
       retext-mat162.1-0 ; 
       retext-mat164.1-0 ; 
       retext-mat167.1-0 ; 
       retext-mat168.1-0 ; 
       retext-mat169.1-0 ; 
       retext-mat170.1-0 ; 
       retext-mat171.1-0 ; 
       retext-mat173.1-0 ; 
       retext-mat174.1-0 ; 
       retext-mat175.1-0 ; 
       retext-mat180.1-0 ; 
       retext-mat181.1-0 ; 
       retext-mat182.1-0 ; 
       retext-mat185.1-0 ; 
       retext-mat186.1-0 ; 
       retext-mat192.1-0 ; 
       retext-mat193.1-0 ; 
       retext-mat194.1-0 ; 
       retext-mat195.1-0 ; 
       retext-mat196.1-0 ; 
       retext-mat198.1-0 ; 
       retext-mat199.1-0 ; 
       retext-mat200.1-0 ; 
       retext-mat201.1-0 ; 
       retext-mat202.1-0 ; 
       retext-mat203.1-0 ; 
       retext-mat209.1-0 ; 
       retext-mat210.1-0 ; 
       retext-mat211.1-0 ; 
       retext-mat212.1-0 ; 
       retext-mat213.1-0 ; 
       retext-mat214.1-0 ; 
       retext-mat215.1-0 ; 
       retext-mat216.1-0 ; 
       retext-mat217.1-0 ; 
       retext-mat218.1-0 ; 
       retext-mat219.1-0 ; 
       retext-mat220.1-0 ; 
       retext-mat221.1-0 ; 
       retext-mat222.1-0 ; 
       retext-mat232.1-0 ; 
       retext-mat236.1-0 ; 
       retext-mat237.1-0 ; 
       retext-mat239.1-0 ; 
       retext-mat240.1-0 ; 
       retext-mat242.1-0 ; 
       retext-mat243.1-0 ; 
       retext-mat244.1-0 ; 
       retext-mat247.1-0 ; 
       retext-mat255.1-0 ; 
       retext-mat257.1-0 ; 
       retext-mat258.1-0 ; 
       retext-mat263.1-0 ; 
       retext-mat264.1-0 ; 
       retext-mat266.1-0 ; 
       retext-mat267.1-0 ; 
       retext-mat268.1-0 ; 
       retext-mat269.1-0 ; 
       retext-mat270.1-0 ; 
       retext-mat271.1-0 ; 
       retext-mat272.1-0 ; 
       retext-mat273.1-0 ; 
       retext-mat274.2-0 ; 
       retext-mat275.1-0 ; 
       retext-mat276.1-0 ; 
       retext-mat277.1-0 ; 
       retext-mat278.1-0 ; 
       retext-mat279.1-0 ; 
       retext-mat46.1-0 ; 
       retext-side1.1-0 ; 
       retext-top.1-0 ; 
       retext-under_nose.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.21-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.7-0 ROOT ; 
       bom07-cockpt_1_2.1-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-l-gun.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-r-gun.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
       final-rotation_limit.7-0 ROOT ; 
       final-rotation_limit_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom01a-final.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 63     
       final-t2d188.1-0 ; 
       final-t2d85.1-0 ; 
       retext-t2d101.1-0 ; 
       retext-t2d102.1-0 ; 
       retext-t2d105.1-0 ; 
       retext-t2d106.1-0 ; 
       retext-t2d110.1-0 ; 
       retext-t2d111.1-0 ; 
       retext-t2d112.1-0 ; 
       retext-t2d113.1-0 ; 
       retext-t2d114.1-0 ; 
       retext-t2d115.4-0 ; 
       retext-t2d116.1-0 ; 
       retext-t2d117.1-0 ; 
       retext-t2d118.1-0 ; 
       retext-t2d119.1-0 ; 
       retext-t2d120.1-0 ; 
       retext-t2d121.1-0 ; 
       retext-t2d125.1-0 ; 
       retext-t2d127.1-0 ; 
       retext-t2d128.1-0 ; 
       retext-t2d129.1-0 ; 
       retext-t2d130.1-0 ; 
       retext-t2d131.1-0 ; 
       retext-t2d132.1-0 ; 
       retext-t2d133.1-0 ; 
       retext-t2d134.1-0 ; 
       retext-t2d135.1-0 ; 
       retext-t2d149.4-0 ; 
       retext-t2d150.1-0 ; 
       retext-t2d151.1-0 ; 
       retext-t2d153.1-0 ; 
       retext-t2d154.1-0 ; 
       retext-t2d155.1-0 ; 
       retext-t2d156.1-0 ; 
       retext-t2d160.1-0 ; 
       retext-t2d168.1-0 ; 
       retext-t2d169.1-0 ; 
       retext-t2d170.1-0 ; 
       retext-t2d175.1-0 ; 
       retext-t2d176.1-0 ; 
       retext-t2d177.4-0 ; 
       retext-t2d178.1-0 ; 
       retext-t2d179.4-0 ; 
       retext-t2d180.3-0 ; 
       retext-t2d181.3-0 ; 
       retext-t2d182.3-0 ; 
       retext-t2d183.2-0 ; 
       retext-t2d184.2-0 ; 
       retext-t2d185.2-0 ; 
       retext-t2d186.2-0 ; 
       retext-t2d187.2-0 ; 
       retext-t2d66.4-0 ; 
       retext-t2d83.4-0 ; 
       retext-t2d86.1-0 ; 
       retext-t2d88.1-0 ; 
       retext-t2d90.1-0 ; 
       retext-t2d91.1-0 ; 
       retext-t2d92.1-0 ; 
       retext-t2d93.1-0 ; 
       retext-t2d95.1-0 ; 
       retext-t2d96.1-0 ; 
       retext-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 29 110 ; 
       11 19 110 ; 
       0 2 110 ; 
       1 41 110 ; 
       3 39 110 ; 
       4 7 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 41 110 ; 
       12 2 110 ; 
       13 3 110 ; 
       14 7 110 ; 
       15 8 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 14 110 ; 
       20 22 110 ; 
       21 42 110 ; 
       22 21 110 ; 
       23 20 110 ; 
       24 0 110 ; 
       26 3 110 ; 
       27 7 110 ; 
       28 8 110 ; 
       29 27 110 ; 
       30 32 110 ; 
       31 42 110 ; 
       32 31 110 ; 
       33 30 110 ; 
       34 15 110 ; 
       35 28 110 ; 
       36 7 110 ; 
       37 19 110 ; 
       38 29 110 ; 
       39 0 110 ; 
       40 39 110 ; 
       41 2 110 ; 
       42 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 0 300 ; 
       11 1 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 62 300 ; 
       3 66 300 ; 
       7 10 300 ; 
       7 82 300 ; 
       7 5 300 ; 
       7 4 300 ; 
       7 11 300 ; 
       7 83 300 ; 
       7 81 300 ; 
       9 7 300 ; 
       9 55 300 ; 
       14 80 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       14 56 300 ; 
       15 59 300 ; 
       15 60 300 ; 
       16 2 300 ; 
       17 2 300 ; 
       17 12 300 ; 
       17 16 300 ; 
       17 17 300 ; 
       18 2 300 ; 
       18 13 300 ; 
       18 14 300 ; 
       18 15 300 ; 
       19 9 300 ; 
       19 77 300 ; 
       19 78 300 ; 
       19 79 300 ; 
       20 6 300 ; 
       20 32 300 ; 
       20 33 300 ; 
       20 34 300 ; 
       20 35 300 ; 
       20 53 300 ; 
       21 45 300 ; 
       21 46 300 ; 
       21 47 300 ; 
       21 48 300 ; 
       22 49 300 ; 
       22 50 300 ; 
       22 51 300 ; 
       24 61 300 ; 
       24 63 300 ; 
       24 64 300 ; 
       24 65 300 ; 
       27 24 300 ; 
       27 25 300 ; 
       27 26 300 ; 
       27 57 300 ; 
       28 52 300 ; 
       28 58 300 ; 
       29 8 300 ; 
       29 74 300 ; 
       29 75 300 ; 
       29 76 300 ; 
       30 27 300 ; 
       30 28 300 ; 
       30 29 300 ; 
       30 30 300 ; 
       30 31 300 ; 
       30 54 300 ; 
       31 40 300 ; 
       31 41 300 ; 
       31 42 300 ; 
       31 43 300 ; 
       32 38 300 ; 
       32 39 300 ; 
       32 44 300 ; 
       34 68 300 ; 
       35 69 300 ; 
       36 67 300 ; 
       37 71 300 ; 
       38 70 300 ; 
       39 3 300 ; 
       39 72 300 ; 
       39 73 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       16 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 11 401 ; 
       5 53 401 ; 
       11 28 401 ; 
       12 54 401 ; 
       13 55 401 ; 
       14 56 401 ; 
       15 57 401 ; 
       16 58 401 ; 
       17 59 401 ; 
       19 60 401 ; 
       20 61 401 ; 
       21 62 401 ; 
       22 2 401 ; 
       23 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       28 7 401 ; 
       29 8 401 ; 
       30 9 401 ; 
       31 10 401 ; 
       32 12 401 ; 
       33 13 401 ; 
       34 14 401 ; 
       35 15 401 ; 
       36 16 401 ; 
       37 17 401 ; 
       39 18 401 ; 
       41 19 401 ; 
       42 20 401 ; 
       43 21 401 ; 
       44 22 401 ; 
       1 1 401 ; 
       46 23 401 ; 
       47 24 401 ; 
       48 25 401 ; 
       50 26 401 ; 
       51 27 401 ; 
       53 29 401 ; 
       54 30 401 ; 
       55 31 401 ; 
       56 32 401 ; 
       57 33 401 ; 
       58 34 401 ; 
       60 35 401 ; 
       61 36 401 ; 
       62 37 401 ; 
       63 38 401 ; 
       64 39 401 ; 
       65 40 401 ; 
       66 42 401 ; 
       72 44 401 ; 
       73 45 401 ; 
       74 46 401 ; 
       75 47 401 ; 
       76 48 401 ; 
       77 49 401 ; 
       78 50 401 ; 
       79 51 401 ; 
       81 43 401 ; 
       82 52 401 ; 
       83 41 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       25 SCHEM 35.55366 -7.107223 0 USR MPRFLG 0 ; 
       11 SCHEM 40.27937 -6.839772 0 USR MPRFLG 0 ; 
       43 SCHEM 62.5 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       0 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       7 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 50 -8 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -8 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       22 SCHEM 5 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 MPRFLG 0 ; 
       24 SCHEM 45 -4 0 MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 MPRFLG 0 ; 
       35 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 30 -4 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 32.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 50 -4 0 MPRFLG 0 ; 
       40 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       44 SCHEM 55 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39.27937 -8.839772 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 34.55366 -9.107224 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39.27937 -10.83977 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 34.55366 -11.10722 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
