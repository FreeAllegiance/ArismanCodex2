SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.69-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.69-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 69     
       animation-back_shelf.4-0 ; 
       animation-bottom.4-0 ; 
       animation-default10.4-0 ; 
       animation-default11.4-0 ; 
       animation-default12.4-0 ; 
       animation-default13.4-0 ; 
       animation-global.4-0 ; 
       animation-lower_back_panel.4-0 ; 
       animation-mat171.4-0 ; 
       animation-mat173.4-0 ; 
       animation-mat174.4-0 ; 
       animation-mat175.4-0 ; 
       animation-mat180.4-0 ; 
       animation-mat181.4-0 ; 
       animation-mat182.4-0 ; 
       animation-mat185.4-0 ; 
       animation-mat186.4-0 ; 
       animation-mat192.4-0 ; 
       animation-mat193.4-0 ; 
       animation-mat194.4-0 ; 
       animation-mat195.4-0 ; 
       animation-mat196.4-0 ; 
       animation-mat198.4-0 ; 
       animation-mat199.4-0 ; 
       animation-mat200.4-0 ; 
       animation-mat201.4-0 ; 
       animation-mat202.4-0 ; 
       animation-mat203.4-0 ; 
       animation-mat209.4-0 ; 
       animation-mat210.4-0 ; 
       animation-mat211.4-0 ; 
       animation-mat212.4-0 ; 
       animation-mat213.4-0 ; 
       animation-mat214.4-0 ; 
       animation-mat215.4-0 ; 
       animation-mat216.4-0 ; 
       animation-mat217.4-0 ; 
       animation-mat218.4-0 ; 
       animation-mat219.4-0 ; 
       animation-mat220.4-0 ; 
       animation-mat221.4-0 ; 
       animation-mat222.4-0 ; 
       animation-mat236.4-0 ; 
       animation-mat237.4-0 ; 
       animation-mat239.4-0 ; 
       animation-mat240.4-0 ; 
       animation-mat242.4-0 ; 
       animation-mat255.4-0 ; 
       animation-mat257.4-0 ; 
       animation-mat258.4-0 ; 
       animation-mat263.4-0 ; 
       animation-mat264.4-0 ; 
       animation-mat266.4-0 ; 
       animation-mat272.4-0 ; 
       animation-mat273.4-0 ; 
       animation-mat274.4-0 ; 
       animation-mat275.4-0 ; 
       animation-mat276.4-0 ; 
       animation-mat277.4-0 ; 
       animation-mat278.4-0 ; 
       animation-mat279.4-0 ; 
       animation-mat281.4-0 ; 
       animation-mat46.4-0 ; 
       animation-side1.4-0 ; 
       animation-top.4-0 ; 
       animation-under_nose.4-0 ; 
       il_light_bomber_sT-mat99.7-0 ; 
       static-mat285.2-0 ; 
       static-mat286.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       bom07-afuselg0.1-0 ; 
       bom07-bom07_3_2.51-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-lthruster.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthruster.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 55     
       animation-t2d101.4-0 ; 
       animation-t2d102.4-0 ; 
       animation-t2d105.4-0 ; 
       animation-t2d106.4-0 ; 
       animation-t2d111.4-0 ; 
       animation-t2d112.4-0 ; 
       animation-t2d113.4-0 ; 
       animation-t2d114.4-0 ; 
       animation-t2d115.4-0 ; 
       animation-t2d116.4-0 ; 
       animation-t2d117.4-0 ; 
       animation-t2d118.4-0 ; 
       animation-t2d119.4-0 ; 
       animation-t2d120.4-0 ; 
       animation-t2d121.4-0 ; 
       animation-t2d125.4-0 ; 
       animation-t2d127.4-0 ; 
       animation-t2d128.4-0 ; 
       animation-t2d129.4-0 ; 
       animation-t2d130.4-0 ; 
       animation-t2d131.4-0 ; 
       animation-t2d132.4-0 ; 
       animation-t2d133.4-0 ; 
       animation-t2d134.4-0 ; 
       animation-t2d135.4-0 ; 
       animation-t2d149.4-0 ; 
       animation-t2d150.4-0 ; 
       animation-t2d151.4-0 ; 
       animation-t2d153.4-0 ; 
       animation-t2d154.4-0 ; 
       animation-t2d155.4-0 ; 
       animation-t2d168.4-0 ; 
       animation-t2d169.4-0 ; 
       animation-t2d170.4-0 ; 
       animation-t2d175.4-0 ; 
       animation-t2d176.4-0 ; 
       animation-t2d177.4-0 ; 
       animation-t2d178.4-0 ; 
       animation-t2d179.4-0 ; 
       animation-t2d180.4-0 ; 
       animation-t2d181.4-0 ; 
       animation-t2d182.4-0 ; 
       animation-t2d183.4-0 ; 
       animation-t2d184.4-0 ; 
       animation-t2d185.4-0 ; 
       animation-t2d186.4-0 ; 
       animation-t2d187.4-0 ; 
       animation-t2d189.4-0 ; 
       animation-t2d66.4-0 ; 
       animation-t2d83.4-0 ; 
       animation-t2d95.4-0 ; 
       animation-t2d96.4-0 ; 
       animation-t2d97.4-0 ; 
       static-t2d193.2-0 ; 
       static-t2d194.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 19 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 4 110 ; 
       8 6 110 ; 
       9 11 110 ; 
       10 20 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       13 3 110 ; 
       14 4 110 ; 
       15 13 110 ; 
       16 18 110 ; 
       17 20 110 ; 
       18 17 110 ; 
       19 0 110 ; 
       20 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 48 300 ; 
       2 52 300 ; 
       3 6 300 ; 
       3 64 300 ; 
       3 1 300 ; 
       3 0 300 ; 
       3 7 300 ; 
       3 65 300 ; 
       3 63 300 ; 
       5 3 300 ; 
       5 44 300 ; 
       6 62 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 45 300 ; 
       7 67 300 ; 
       8 5 300 ; 
       8 58 300 ; 
       8 59 300 ; 
       8 60 300 ; 
       9 2 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       9 42 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       10 37 300 ; 
       10 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       12 47 300 ; 
       12 49 300 ; 
       12 50 300 ; 
       12 51 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 46 300 ; 
       14 68 300 ; 
       15 4 300 ; 
       15 55 300 ; 
       15 56 300 ; 
       15 57 300 ; 
       16 17 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       16 43 300 ; 
       17 30 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 34 300 ; 
       19 66 300 ; 
       19 53 300 ; 
       19 54 300 ; 
       19 61 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 49 401 ; 
       7 25 401 ; 
       9 50 401 ; 
       10 51 401 ; 
       11 52 401 ; 
       12 0 401 ; 
       13 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 12 401 ; 
       26 13 401 ; 
       27 14 401 ; 
       29 15 401 ; 
       31 16 401 ; 
       32 17 401 ; 
       33 18 401 ; 
       34 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 29 401 ; 
       46 30 401 ; 
       47 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 37 401 ; 
       53 39 401 ; 
       54 40 401 ; 
       55 41 401 ; 
       56 42 401 ; 
       57 43 401 ; 
       58 44 401 ; 
       59 45 401 ; 
       60 46 401 ; 
       61 47 401 ; 
       63 38 401 ; 
       64 48 401 ; 
       65 36 401 ; 
       67 53 401 ; 
       68 54 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 0 0 SRT 1 1 1 0.08377581 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 25 -4 0 MPRFLG 0 ; 
       20 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
