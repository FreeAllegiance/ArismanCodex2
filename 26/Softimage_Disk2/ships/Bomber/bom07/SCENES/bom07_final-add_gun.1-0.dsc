SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       add_gun-default10.1-0 ; 
       add_gun-default11.1-0 ; 
       add_gun-default12.1-0 ; 
       add_gun-mat140.1-0 ; 
       add_gun-mat141.1-0 ; 
       add_gun-mat145.1-0 ; 
       add_gun-mat146.1-0 ; 
       add_gun-mat150.1-0 ; 
       add_gun-mat154.1-0 ; 
       add_gun-mat155.1-0 ; 
       add_gun-mat159.1-0 ; 
       add_gun-mat160.1-0 ; 
       add_gun-mat162.1-0 ; 
       add_gun-mat164.1-0 ; 
       add_gun-mat167.1-0 ; 
       add_gun-mat168.1-0 ; 
       add_gun-mat169.1-0 ; 
       add_gun-mat170.1-0 ; 
       add_gun-mat171.1-0 ; 
       add_gun-mat173.1-0 ; 
       add_gun-mat174.1-0 ; 
       add_gun-mat175.1-0 ; 
       add_gun-mat180.1-0 ; 
       add_gun-mat181.1-0 ; 
       add_gun-mat182.1-0 ; 
       add_gun-mat185.1-0 ; 
       add_gun-mat186.1-0 ; 
       add_gun-mat192.1-0 ; 
       add_gun-mat193.1-0 ; 
       add_gun-mat194.1-0 ; 
       add_gun-mat195.1-0 ; 
       add_gun-mat196.1-0 ; 
       add_gun-mat197.1-0 ; 
       add_gun-mat198.1-0 ; 
       add_gun-mat199.1-0 ; 
       add_gun-mat200.1-0 ; 
       add_gun-mat201.1-0 ; 
       add_gun-mat202.1-0 ; 
       add_gun-mat203.1-0 ; 
       add_gun-mat204.1-0 ; 
       add_gun-mat209.1-0 ; 
       add_gun-mat210.1-0 ; 
       add_gun-mat211.1-0 ; 
       add_gun-mat212.1-0 ; 
       add_gun-mat213.1-0 ; 
       add_gun-mat214.1-0 ; 
       add_gun-mat215.1-0 ; 
       add_gun-mat216.1-0 ; 
       add_gun-mat217.1-0 ; 
       add_gun-mat218.1-0 ; 
       add_gun-mat219.1-0 ; 
       add_gun-mat220.1-0 ; 
       add_gun-mat221.1-0 ; 
       add_gun-mat222.1-0 ; 
       add_gun-mat223.1-0 ; 
       add_gun-mat224.1-0 ; 
       add_gun-mat225.1-0 ; 
       add_gun-mat232.1-0 ; 
       add_gun-mat236.1-0 ; 
       add_gun-mat237.1-0 ; 
       add_gun-mat239.1-0 ; 
       add_gun-mat240.1-0 ; 
       add_gun-mat242.1-0 ; 
       add_gun-mat243.1-0 ; 
       add_gun-mat244.1-0 ; 
       add_gun-mat247.1-0 ; 
       add_gun-mat251.1-0 ; 
       add_gun-mat255.1-0 ; 
       add_gun-mat257.1-0 ; 
       add_gun-mat258.1-0 ; 
       add_gun-mat263.1-0 ; 
       add_gun-mat264.1-0 ; 
       add_gun-mat265.1-0 ; 
       add_gun-mat266.1-0 ; 
       add_gun-mat267.1-0 ; 
       add_gun-mat268.1-0 ; 
       add_gun-mat269.1-0 ; 
       add_gun-mat270.1-0 ; 
       add_gun-mat271.1-0 ; 
       add_gun-mat46.1-0 ; 
       add_gun-mat52.1-0 ; 
       add_gun-mat66.1-0 ; 
       il_light_bomber_sT-default1.1-0 ; 
       il_light_bomber_sT-mat99.1-0 ; 
       skycraneF-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       add_gun-cyl1.1-0 ROOT ; 
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3.2-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-add_gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       add_gun-t2d101.1-0 ; 
       add_gun-t2d102.1-0 ; 
       add_gun-t2d105.1-0 ; 
       add_gun-t2d106.1-0 ; 
       add_gun-t2d110.1-0 ; 
       add_gun-t2d111.1-0 ; 
       add_gun-t2d112.1-0 ; 
       add_gun-t2d113.1-0 ; 
       add_gun-t2d114.1-0 ; 
       add_gun-t2d115.1-0 ; 
       add_gun-t2d116.1-0 ; 
       add_gun-t2d117.1-0 ; 
       add_gun-t2d118.1-0 ; 
       add_gun-t2d119.1-0 ; 
       add_gun-t2d120.1-0 ; 
       add_gun-t2d121.1-0 ; 
       add_gun-t2d125.1-0 ; 
       add_gun-t2d127.1-0 ; 
       add_gun-t2d128.1-0 ; 
       add_gun-t2d129.1-0 ; 
       add_gun-t2d130.1-0 ; 
       add_gun-t2d131.1-0 ; 
       add_gun-t2d132.1-0 ; 
       add_gun-t2d133.1-0 ; 
       add_gun-t2d134.1-0 ; 
       add_gun-t2d135.1-0 ; 
       add_gun-t2d136.1-0 ; 
       add_gun-t2d137.1-0 ; 
       add_gun-t2d138.1-0 ; 
       add_gun-t2d149.1-0 ; 
       add_gun-t2d150.1-0 ; 
       add_gun-t2d151.1-0 ; 
       add_gun-t2d153.1-0 ; 
       add_gun-t2d154.1-0 ; 
       add_gun-t2d155.1-0 ; 
       add_gun-t2d156.1-0 ; 
       add_gun-t2d160.1-0 ; 
       add_gun-t2d164.1-0 ; 
       add_gun-t2d168.1-0 ; 
       add_gun-t2d169.1-0 ; 
       add_gun-t2d170.1-0 ; 
       add_gun-t2d175.1-0 ; 
       add_gun-t2d176.1-0 ; 
       add_gun-t2d177.1-0 ; 
       add_gun-t2d178.1-0 ; 
       add_gun-t2d65.1-0 ; 
       add_gun-t2d66.1-0 ; 
       add_gun-t2d69.1-0 ; 
       add_gun-t2d71.1-0 ; 
       add_gun-t2d72.1-0 ; 
       add_gun-t2d76.1-0 ; 
       add_gun-t2d78.1-0 ; 
       add_gun-t2d79.1-0 ; 
       add_gun-t2d83.1-0 ; 
       add_gun-t2d84.1-0 ; 
       add_gun-t2d86.1-0 ; 
       add_gun-t2d88.1-0 ; 
       add_gun-t2d90.1-0 ; 
       add_gun-t2d91.1-0 ; 
       add_gun-t2d92.1-0 ; 
       add_gun-t2d93.1-0 ; 
       add_gun-t2d95.1-0 ; 
       add_gun-t2d96.1-0 ; 
       add_gun-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 38 110 ; 
       4 36 110 ; 
       5 6 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 7 110 ; 
       9 38 110 ; 
       10 3 110 ; 
       11 4 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 12 110 ; 
       18 20 110 ; 
       19 39 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 1 110 ; 
       23 4 110 ; 
       24 6 110 ; 
       25 7 110 ; 
       26 24 110 ; 
       27 29 110 ; 
       28 39 110 ; 
       29 28 110 ; 
       30 27 110 ; 
       31 13 110 ; 
       32 25 110 ; 
       33 6 110 ; 
       34 17 110 ; 
       35 26 110 ; 
       36 1 110 ; 
       37 36 110 ; 
       38 3 110 ; 
       39 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       4 68 300 ; 
       4 73 300 ; 
       6 80 300 ; 
       6 81 300 ; 
       6 3 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 32 300 ; 
       6 39 300 ; 
       6 72 300 ; 
       8 1 300 ; 
       8 60 300 ; 
       12 79 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 61 300 ; 
       13 64 300 ; 
       13 65 300 ; 
       14 82 300 ; 
       15 82 300 ; 
       15 12 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       16 82 300 ; 
       16 13 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       17 84 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       18 0 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       18 36 300 ; 
       18 58 300 ; 
       19 47 300 ; 
       19 48 300 ; 
       19 49 300 ; 
       19 50 300 ; 
       20 51 300 ; 
       20 52 300 ; 
       20 53 300 ; 
       22 67 300 ; 
       22 69 300 ; 
       22 70 300 ; 
       22 71 300 ; 
       24 24 300 ; 
       24 25 300 ; 
       24 26 300 ; 
       24 62 300 ; 
       25 57 300 ; 
       25 63 300 ; 
       26 2 300 ; 
       26 7 300 ; 
       26 8 300 ; 
       26 9 300 ; 
       27 27 300 ; 
       27 28 300 ; 
       27 29 300 ; 
       27 30 300 ; 
       27 31 300 ; 
       27 59 300 ; 
       28 42 300 ; 
       28 43 300 ; 
       28 44 300 ; 
       28 45 300 ; 
       29 40 300 ; 
       29 41 300 ; 
       29 46 300 ; 
       31 75 300 ; 
       32 76 300 ; 
       33 74 300 ; 
       34 78 300 ; 
       35 77 300 ; 
       36 83 300 ; 
       36 54 300 ; 
       36 55 300 ; 
       36 56 300 ; 
       36 66 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 45 401 ; 
       4 47 401 ; 
       5 48 401 ; 
       6 49 401 ; 
       7 50 401 ; 
       8 51 401 ; 
       9 52 401 ; 
       10 53 401 ; 
       11 54 401 ; 
       12 55 401 ; 
       13 56 401 ; 
       14 57 401 ; 
       15 58 401 ; 
       16 59 401 ; 
       17 60 401 ; 
       19 61 401 ; 
       20 62 401 ; 
       21 63 401 ; 
       22 0 401 ; 
       23 1 401 ; 
       25 2 401 ; 
       26 3 401 ; 
       28 5 401 ; 
       29 6 401 ; 
       30 7 401 ; 
       31 8 401 ; 
       32 9 401 ; 
       33 10 401 ; 
       34 11 401 ; 
       35 12 401 ; 
       36 13 401 ; 
       37 14 401 ; 
       38 15 401 ; 
       39 29 401 ; 
       41 16 401 ; 
       43 17 401 ; 
       44 18 401 ; 
       45 19 401 ; 
       46 20 401 ; 
       48 21 401 ; 
       49 22 401 ; 
       50 23 401 ; 
       52 24 401 ; 
       53 25 401 ; 
       54 26 401 ; 
       55 27 401 ; 
       56 28 401 ; 
       58 30 401 ; 
       59 31 401 ; 
       60 32 401 ; 
       61 33 401 ; 
       62 34 401 ; 
       63 35 401 ; 
       65 36 401 ; 
       66 37 401 ; 
       67 38 401 ; 
       68 39 401 ; 
       69 40 401 ; 
       70 41 401 ; 
       71 42 401 ; 
       72 43 401 ; 
       73 44 401 ; 
       81 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 50 0 0 DISPLAY 1 2 SRT 0.06599989 0.06599989 0.06599989 1.570796 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       3 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 45 -8 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 25 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 5 -8 0 MPRFLG 0 ; 
       19 SCHEM 5 -4 0 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -10 0 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 35 -6 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 35 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 45 -4 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       39 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 105 105 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
