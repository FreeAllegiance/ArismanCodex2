SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.33-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.45-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       animation-back_shelf.1-0 ; 
       animation-bottom.1-0 ; 
       animation-default10.1-0 ; 
       animation-default11.1-0 ; 
       animation-default12.1-0 ; 
       animation-default13.1-0 ; 
       animation-global.1-0 ; 
       animation-lower_back_panel.1-0 ; 
       animation-mat162.1-0 ; 
       animation-mat164.1-0 ; 
       animation-mat167.1-0 ; 
       animation-mat168.1-0 ; 
       animation-mat169.1-0 ; 
       animation-mat170.1-0 ; 
       animation-mat171.1-0 ; 
       animation-mat173.1-0 ; 
       animation-mat174.1-0 ; 
       animation-mat175.1-0 ; 
       animation-mat180.1-0 ; 
       animation-mat181.1-0 ; 
       animation-mat182.1-0 ; 
       animation-mat185.1-0 ; 
       animation-mat186.1-0 ; 
       animation-mat192.1-0 ; 
       animation-mat193.1-0 ; 
       animation-mat194.1-0 ; 
       animation-mat195.1-0 ; 
       animation-mat196.1-0 ; 
       animation-mat198.1-0 ; 
       animation-mat199.1-0 ; 
       animation-mat200.1-0 ; 
       animation-mat201.1-0 ; 
       animation-mat202.1-0 ; 
       animation-mat203.1-0 ; 
       animation-mat209.1-0 ; 
       animation-mat210.1-0 ; 
       animation-mat211.1-0 ; 
       animation-mat212.1-0 ; 
       animation-mat213.1-0 ; 
       animation-mat214.1-0 ; 
       animation-mat215.1-0 ; 
       animation-mat216.1-0 ; 
       animation-mat217.1-0 ; 
       animation-mat218.1-0 ; 
       animation-mat219.1-0 ; 
       animation-mat220.1-0 ; 
       animation-mat221.1-0 ; 
       animation-mat222.1-0 ; 
       animation-mat232.1-0 ; 
       animation-mat236.1-0 ; 
       animation-mat237.1-0 ; 
       animation-mat239.1-0 ; 
       animation-mat240.1-0 ; 
       animation-mat242.1-0 ; 
       animation-mat243.1-0 ; 
       animation-mat244.1-0 ; 
       animation-mat247.1-0 ; 
       animation-mat255.1-0 ; 
       animation-mat257.1-0 ; 
       animation-mat258.1-0 ; 
       animation-mat263.1-0 ; 
       animation-mat264.1-0 ; 
       animation-mat266.1-0 ; 
       animation-mat267.1-0 ; 
       animation-mat268.1-0 ; 
       animation-mat269.1-0 ; 
       animation-mat270.1-0 ; 
       animation-mat271.1-0 ; 
       animation-mat272.1-0 ; 
       animation-mat273.1-0 ; 
       animation-mat274.1-0 ; 
       animation-mat275.1-0 ; 
       animation-mat276.1-0 ; 
       animation-mat277.1-0 ; 
       animation-mat278.1-0 ; 
       animation-mat279.1-0 ; 
       animation-mat280.1-0 ; 
       animation-mat281.1-0 ; 
       animation-mat46.1-0 ; 
       animation-mat69.1-0 ; 
       animation-side1.1-0 ; 
       animation-top.1-0 ; 
       animation-under_nose.1-0 ; 
       il_light_bomber_sT-default1.3-0 ; 
       il_light_bomber_sT-mat99.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       animation-rotation_limit.3-0 ROOT ; 
       animation-rotation_limit_2.3-0 ROOT ; 
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.32-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.11-0 ROOT ; 
       bom07-cockpt_1_2.5-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-l-gun.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-r-gun.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-animation.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       animation-t2d101.1-0 ; 
       animation-t2d102.1-0 ; 
       animation-t2d105.1-0 ; 
       animation-t2d106.1-0 ; 
       animation-t2d110.1-0 ; 
       animation-t2d111.1-0 ; 
       animation-t2d112.1-0 ; 
       animation-t2d113.1-0 ; 
       animation-t2d114.1-0 ; 
       animation-t2d115.1-0 ; 
       animation-t2d116.1-0 ; 
       animation-t2d117.1-0 ; 
       animation-t2d118.1-0 ; 
       animation-t2d119.1-0 ; 
       animation-t2d120.1-0 ; 
       animation-t2d121.1-0 ; 
       animation-t2d125.1-0 ; 
       animation-t2d127.1-0 ; 
       animation-t2d128.1-0 ; 
       animation-t2d129.1-0 ; 
       animation-t2d130.1-0 ; 
       animation-t2d131.1-0 ; 
       animation-t2d132.1-0 ; 
       animation-t2d133.1-0 ; 
       animation-t2d134.1-0 ; 
       animation-t2d135.1-0 ; 
       animation-t2d149.1-0 ; 
       animation-t2d150.1-0 ; 
       animation-t2d151.1-0 ; 
       animation-t2d153.1-0 ; 
       animation-t2d154.1-0 ; 
       animation-t2d155.1-0 ; 
       animation-t2d156.1-0 ; 
       animation-t2d160.1-0 ; 
       animation-t2d168.1-0 ; 
       animation-t2d169.1-0 ; 
       animation-t2d170.1-0 ; 
       animation-t2d175.1-0 ; 
       animation-t2d176.1-0 ; 
       animation-t2d177.1-0 ; 
       animation-t2d178.1-0 ; 
       animation-t2d179.1-0 ; 
       animation-t2d180.1-0 ; 
       animation-t2d181.1-0 ; 
       animation-t2d182.1-0 ; 
       animation-t2d183.1-0 ; 
       animation-t2d184.1-0 ; 
       animation-t2d185.1-0 ; 
       animation-t2d186.1-0 ; 
       animation-t2d187.1-0 ; 
       animation-t2d188.1-0 ; 
       animation-t2d189.1-0 ; 
       animation-t2d66.1-0 ; 
       animation-t2d83.1-0 ; 
       animation-t2d85.1-0 ; 
       animation-t2d86.1-0 ; 
       animation-t2d88.1-0 ; 
       animation-t2d90.1-0 ; 
       animation-t2d91.1-0 ; 
       animation-t2d92.1-0 ; 
       animation-t2d93.1-0 ; 
       animation-t2d95.1-0 ; 
       animation-t2d96.1-0 ; 
       animation-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 4 110 ; 
       3 43 110 ; 
       5 41 110 ; 
       6 9 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       12 43 110 ; 
       13 16 110 ; 
       14 4 110 ; 
       15 5 110 ; 
       16 9 110 ; 
       17 10 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 16 110 ; 
       22 24 110 ; 
       23 44 110 ; 
       24 23 110 ; 
       25 22 110 ; 
       26 2 110 ; 
       27 29 110 ; 
       28 5 110 ; 
       29 9 110 ; 
       30 10 110 ; 
       31 29 110 ; 
       32 34 110 ; 
       33 44 110 ; 
       34 33 110 ; 
       35 32 110 ; 
       36 17 110 ; 
       37 30 110 ; 
       38 9 110 ; 
       39 21 110 ; 
       40 31 110 ; 
       41 2 110 ; 
       42 41 110 ; 
       43 4 110 ; 
       44 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 32 300 ; 
       5 33 300 ; 
       5 58 300 ; 
       5 62 300 ; 
       9 6 300 ; 
       9 81 300 ; 
       9 1 300 ; 
       9 0 300 ; 
       9 7 300 ; 
       9 82 300 ; 
       9 80 300 ; 
       11 3 300 ; 
       11 51 300 ; 
       13 79 300 ; 
       16 78 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       16 52 300 ; 
       17 55 300 ; 
       17 56 300 ; 
       18 83 300 ; 
       19 83 300 ; 
       19 8 300 ; 
       19 12 300 ; 
       19 13 300 ; 
       20 83 300 ; 
       20 9 300 ; 
       20 10 300 ; 
       20 11 300 ; 
       21 5 300 ; 
       21 73 300 ; 
       21 74 300 ; 
       21 75 300 ; 
       22 2 300 ; 
       22 28 300 ; 
       22 29 300 ; 
       22 30 300 ; 
       22 31 300 ; 
       22 49 300 ; 
       23 41 300 ; 
       23 42 300 ; 
       23 43 300 ; 
       23 44 300 ; 
       24 45 300 ; 
       24 46 300 ; 
       24 47 300 ; 
       26 57 300 ; 
       26 59 300 ; 
       26 60 300 ; 
       26 61 300 ; 
       27 76 300 ; 
       29 20 300 ; 
       29 21 300 ; 
       29 22 300 ; 
       29 53 300 ; 
       30 48 300 ; 
       30 54 300 ; 
       31 4 300 ; 
       31 70 300 ; 
       31 71 300 ; 
       31 72 300 ; 
       32 23 300 ; 
       32 24 300 ; 
       32 25 300 ; 
       32 26 300 ; 
       32 27 300 ; 
       32 50 300 ; 
       33 36 300 ; 
       33 37 300 ; 
       33 38 300 ; 
       33 39 300 ; 
       34 34 300 ; 
       34 35 300 ; 
       34 40 300 ; 
       36 64 300 ; 
       37 65 300 ; 
       38 63 300 ; 
       39 67 300 ; 
       40 66 300 ; 
       41 84 300 ; 
       41 68 300 ; 
       41 69 300 ; 
       41 77 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       18 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 53 401 ; 
       7 26 401 ; 
       8 55 401 ; 
       9 56 401 ; 
       10 57 401 ; 
       11 58 401 ; 
       12 59 401 ; 
       13 60 401 ; 
       15 61 401 ; 
       16 62 401 ; 
       17 63 401 ; 
       18 0 401 ; 
       19 1 401 ; 
       21 2 401 ; 
       22 3 401 ; 
       24 5 401 ; 
       25 6 401 ; 
       26 7 401 ; 
       27 8 401 ; 
       28 10 401 ; 
       29 11 401 ; 
       30 12 401 ; 
       31 13 401 ; 
       32 14 401 ; 
       33 15 401 ; 
       35 16 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 19 401 ; 
       40 20 401 ; 
       42 21 401 ; 
       43 22 401 ; 
       44 23 401 ; 
       46 24 401 ; 
       47 25 401 ; 
       49 27 401 ; 
       50 28 401 ; 
       51 29 401 ; 
       52 30 401 ; 
       53 31 401 ; 
       54 32 401 ; 
       56 33 401 ; 
       57 34 401 ; 
       58 35 401 ; 
       59 36 401 ; 
       60 37 401 ; 
       61 38 401 ; 
       62 40 401 ; 
       68 42 401 ; 
       69 43 401 ; 
       70 44 401 ; 
       71 45 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       74 48 401 ; 
       75 49 401 ; 
       76 50 401 ; 
       77 51 401 ; 
       79 54 401 ; 
       80 41 401 ; 
       81 52 401 ; 
       82 39 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       1 SCHEM 55 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       2 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 28.75 0 0 SRT 1 1 1 0.08377581 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       8 SCHEM 60 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       9 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 40.27937 -6.839772 0 USR MPRFLG 0 ; 
       14 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 50 -8 0 MPRFLG 0 ; 
       16 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 MPRFLG 0 ; 
       18 SCHEM 15 -4 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 20 -4 0 MPRFLG 0 ; 
       21 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       22 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 DISPLAY 3 2 MPRFLG 0 ; 
       24 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 45 -4 0 MPRFLG 0 ; 
       27 SCHEM 35.55366 -7.107223 0 USR MPRFLG 0 ; 
       28 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 25 -6 0 MPRFLG 0 ; 
       37 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 30 -4 0 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 32.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 50 -4 0 MPRFLG 0 ; 
       42 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       44 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 34.55366 -9.107224 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 65 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 39.27937 -8.839772 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 34.55366 -11.10722 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 67.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 39.27937 -10.83977 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 74 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
