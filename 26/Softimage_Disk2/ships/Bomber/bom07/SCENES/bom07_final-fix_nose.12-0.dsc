SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.15-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.15-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       fix_nose-back_shelf.2-0 ; 
       fix_nose-bottom.2-0 ; 
       fix_nose-default10.2-0 ; 
       fix_nose-default11.2-0 ; 
       fix_nose-default12.2-0 ; 
       fix_nose-global.2-0 ; 
       fix_nose-lower_back_panel.2-0 ; 
       fix_nose-mat141.2-0 ; 
       fix_nose-mat145.2-0 ; 
       fix_nose-mat146.2-0 ; 
       fix_nose-mat150.2-0 ; 
       fix_nose-mat154.2-0 ; 
       fix_nose-mat155.2-0 ; 
       fix_nose-mat162.2-0 ; 
       fix_nose-mat164.2-0 ; 
       fix_nose-mat167.2-0 ; 
       fix_nose-mat168.2-0 ; 
       fix_nose-mat169.2-0 ; 
       fix_nose-mat170.2-0 ; 
       fix_nose-mat171.2-0 ; 
       fix_nose-mat173.2-0 ; 
       fix_nose-mat174.2-0 ; 
       fix_nose-mat175.2-0 ; 
       fix_nose-mat180.2-0 ; 
       fix_nose-mat181.2-0 ; 
       fix_nose-mat182.2-0 ; 
       fix_nose-mat185.2-0 ; 
       fix_nose-mat186.2-0 ; 
       fix_nose-mat192.2-0 ; 
       fix_nose-mat193.2-0 ; 
       fix_nose-mat194.2-0 ; 
       fix_nose-mat195.2-0 ; 
       fix_nose-mat196.2-0 ; 
       fix_nose-mat198.2-0 ; 
       fix_nose-mat199.2-0 ; 
       fix_nose-mat200.2-0 ; 
       fix_nose-mat201.2-0 ; 
       fix_nose-mat202.2-0 ; 
       fix_nose-mat203.2-0 ; 
       fix_nose-mat209.2-0 ; 
       fix_nose-mat210.2-0 ; 
       fix_nose-mat211.2-0 ; 
       fix_nose-mat212.2-0 ; 
       fix_nose-mat213.2-0 ; 
       fix_nose-mat214.2-0 ; 
       fix_nose-mat215.2-0 ; 
       fix_nose-mat216.2-0 ; 
       fix_nose-mat217.2-0 ; 
       fix_nose-mat218.2-0 ; 
       fix_nose-mat219.2-0 ; 
       fix_nose-mat220.2-0 ; 
       fix_nose-mat221.2-0 ; 
       fix_nose-mat222.2-0 ; 
       fix_nose-mat223.2-0 ; 
       fix_nose-mat224.2-0 ; 
       fix_nose-mat225.2-0 ; 
       fix_nose-mat232.2-0 ; 
       fix_nose-mat236.2-0 ; 
       fix_nose-mat237.2-0 ; 
       fix_nose-mat239.2-0 ; 
       fix_nose-mat240.2-0 ; 
       fix_nose-mat242.2-0 ; 
       fix_nose-mat243.2-0 ; 
       fix_nose-mat244.2-0 ; 
       fix_nose-mat247.2-0 ; 
       fix_nose-mat251.2-0 ; 
       fix_nose-mat255.2-0 ; 
       fix_nose-mat257.2-0 ; 
       fix_nose-mat258.2-0 ; 
       fix_nose-mat263.2-0 ; 
       fix_nose-mat264.2-0 ; 
       fix_nose-mat266.2-0 ; 
       fix_nose-mat267.2-0 ; 
       fix_nose-mat268.2-0 ; 
       fix_nose-mat269.2-0 ; 
       fix_nose-mat270.2-0 ; 
       fix_nose-mat271.2-0 ; 
       fix_nose-mat46.2-0 ; 
       fix_nose-side1.1-0 ; 
       fix_nose-top.2-0 ; 
       fix_nose-under_nose.2-0 ; 
       il_light_bomber_sT-default1.2-0 ; 
       il_light_bomber_sT-mat99.2-0 ; 
       skycraneF-default9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.3-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.3-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
       final-rotation_limit.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-fix_nose.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 63     
       fix_nose-t2d101.2-0 ; 
       fix_nose-t2d102.2-0 ; 
       fix_nose-t2d105.2-0 ; 
       fix_nose-t2d106.2-0 ; 
       fix_nose-t2d110.2-0 ; 
       fix_nose-t2d111.2-0 ; 
       fix_nose-t2d112.2-0 ; 
       fix_nose-t2d113.2-0 ; 
       fix_nose-t2d114.2-0 ; 
       fix_nose-t2d115.5-0 ; 
       fix_nose-t2d116.2-0 ; 
       fix_nose-t2d117.2-0 ; 
       fix_nose-t2d118.2-0 ; 
       fix_nose-t2d119.2-0 ; 
       fix_nose-t2d120.2-0 ; 
       fix_nose-t2d121.2-0 ; 
       fix_nose-t2d125.2-0 ; 
       fix_nose-t2d127.2-0 ; 
       fix_nose-t2d128.2-0 ; 
       fix_nose-t2d129.2-0 ; 
       fix_nose-t2d130.2-0 ; 
       fix_nose-t2d131.2-0 ; 
       fix_nose-t2d132.2-0 ; 
       fix_nose-t2d133.2-0 ; 
       fix_nose-t2d134.2-0 ; 
       fix_nose-t2d135.2-0 ; 
       fix_nose-t2d136.2-0 ; 
       fix_nose-t2d137.2-0 ; 
       fix_nose-t2d138.2-0 ; 
       fix_nose-t2d149.5-0 ; 
       fix_nose-t2d150.2-0 ; 
       fix_nose-t2d151.2-0 ; 
       fix_nose-t2d153.2-0 ; 
       fix_nose-t2d154.2-0 ; 
       fix_nose-t2d155.2-0 ; 
       fix_nose-t2d156.2-0 ; 
       fix_nose-t2d160.2-0 ; 
       fix_nose-t2d164.2-0 ; 
       fix_nose-t2d168.2-0 ; 
       fix_nose-t2d169.2-0 ; 
       fix_nose-t2d170.2-0 ; 
       fix_nose-t2d175.2-0 ; 
       fix_nose-t2d176.2-0 ; 
       fix_nose-t2d177.5-0 ; 
       fix_nose-t2d178.2-0 ; 
       fix_nose-t2d179.2-0 ; 
       fix_nose-t2d66.5-0 ; 
       fix_nose-t2d69.2-0 ; 
       fix_nose-t2d71.2-0 ; 
       fix_nose-t2d72.2-0 ; 
       fix_nose-t2d76.2-0 ; 
       fix_nose-t2d78.2-0 ; 
       fix_nose-t2d79.2-0 ; 
       fix_nose-t2d83.5-0 ; 
       fix_nose-t2d86.2-0 ; 
       fix_nose-t2d88.2-0 ; 
       fix_nose-t2d90.2-0 ; 
       fix_nose-t2d91.2-0 ; 
       fix_nose-t2d92.2-0 ; 
       fix_nose-t2d93.2-0 ; 
       fix_nose-t2d95.2-0 ; 
       fix_nose-t2d96.2-0 ; 
       fix_nose-t2d97.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 38 110 ; 
       3 36 110 ; 
       4 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 38 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 12 110 ; 
       18 20 110 ; 
       19 39 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 0 110 ; 
       23 3 110 ; 
       24 6 110 ; 
       25 7 110 ; 
       26 24 110 ; 
       27 29 110 ; 
       28 39 110 ; 
       29 28 110 ; 
       30 27 110 ; 
       31 13 110 ; 
       32 25 110 ; 
       33 6 110 ; 
       34 17 110 ; 
       35 26 110 ; 
       36 0 110 ; 
       37 36 110 ; 
       38 2 110 ; 
       39 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 67 300 ; 
       3 71 300 ; 
       6 5 300 ; 
       6 79 300 ; 
       6 1 300 ; 
       6 0 300 ; 
       6 6 300 ; 
       6 80 300 ; 
       6 78 300 ; 
       8 3 300 ; 
       8 59 300 ; 
       12 77 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 60 300 ; 
       13 63 300 ; 
       13 64 300 ; 
       14 81 300 ; 
       15 81 300 ; 
       15 13 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       16 81 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       17 83 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       17 9 300 ; 
       18 2 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       18 36 300 ; 
       18 57 300 ; 
       19 46 300 ; 
       19 47 300 ; 
       19 48 300 ; 
       19 49 300 ; 
       20 50 300 ; 
       20 51 300 ; 
       20 52 300 ; 
       22 66 300 ; 
       22 68 300 ; 
       22 69 300 ; 
       22 70 300 ; 
       24 25 300 ; 
       24 26 300 ; 
       24 27 300 ; 
       24 61 300 ; 
       25 56 300 ; 
       25 62 300 ; 
       26 4 300 ; 
       26 10 300 ; 
       26 11 300 ; 
       26 12 300 ; 
       27 28 300 ; 
       27 29 300 ; 
       27 30 300 ; 
       27 31 300 ; 
       27 32 300 ; 
       27 58 300 ; 
       28 41 300 ; 
       28 42 300 ; 
       28 43 300 ; 
       28 44 300 ; 
       29 39 300 ; 
       29 40 300 ; 
       29 45 300 ; 
       31 73 300 ; 
       32 74 300 ; 
       33 72 300 ; 
       34 76 300 ; 
       35 75 300 ; 
       36 82 300 ; 
       36 53 300 ; 
       36 54 300 ; 
       36 55 300 ; 
       36 65 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 53 401 ; 
       6 29 401 ; 
       7 47 401 ; 
       8 48 401 ; 
       9 49 401 ; 
       10 50 401 ; 
       11 51 401 ; 
       12 52 401 ; 
       13 54 401 ; 
       14 55 401 ; 
       15 56 401 ; 
       16 57 401 ; 
       17 58 401 ; 
       18 59 401 ; 
       20 60 401 ; 
       21 61 401 ; 
       22 62 401 ; 
       23 0 401 ; 
       24 1 401 ; 
       26 2 401 ; 
       27 3 401 ; 
       29 5 401 ; 
       30 6 401 ; 
       31 7 401 ; 
       32 8 401 ; 
       33 10 401 ; 
       34 11 401 ; 
       35 12 401 ; 
       36 13 401 ; 
       37 14 401 ; 
       38 15 401 ; 
       40 16 401 ; 
       42 17 401 ; 
       43 18 401 ; 
       44 19 401 ; 
       45 20 401 ; 
       47 21 401 ; 
       48 22 401 ; 
       49 23 401 ; 
       51 24 401 ; 
       52 25 401 ; 
       53 26 401 ; 
       54 27 401 ; 
       55 28 401 ; 
       57 30 401 ; 
       58 31 401 ; 
       59 32 401 ; 
       60 33 401 ; 
       61 34 401 ; 
       62 35 401 ; 
       64 36 401 ; 
       65 37 401 ; 
       66 38 401 ; 
       67 39 401 ; 
       68 40 401 ; 
       69 41 401 ; 
       70 42 401 ; 
       71 44 401 ; 
       78 45 401 ; 
       79 46 401 ; 
       80 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 211.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 120 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 208.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 167.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 237.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       6 SCHEM 152.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 108.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 101.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 88.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 200 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 132.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 107.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 81.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 87.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 95 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 127.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 21.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 16.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 191.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 197.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 155 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 115 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 150 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 56.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 51.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 40 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 105 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 112.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 120 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 122.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 145 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 216.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 222.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 76.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 38.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 240 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 220 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 202.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 205 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 207.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 210 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 212.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 195 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 190 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 192.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 217.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 235 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 210 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 212.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 225 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 227.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 230 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 232.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 195 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 187.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 190 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 192.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 217.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 185 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 202.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 205 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 207.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 236.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
