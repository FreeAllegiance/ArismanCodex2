SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       destroy-inf_light4_1_3_1_2_1_1_1.1-0 ROOT ; 
       il_bomber_F-light1_1_3.1-0 ROOT ; 
       il_heavyfighterx_sAT-spot1.1-0 ; 
       il_heavyfighterx_sAT-spot1_int_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       il_bomber_F-default10.1-0 ; 
       il_bomber_F-default11.1-0 ; 
       il_bomber_F-default12.1-0 ; 
       il_bomber_F-mat140.1-0 ; 
       il_bomber_F-mat141.1-0 ; 
       il_bomber_F-mat145.1-0 ; 
       il_bomber_F-mat146.1-0 ; 
       il_bomber_F-mat150.1-0 ; 
       il_bomber_F-mat154.1-0 ; 
       il_bomber_F-mat155.1-0 ; 
       il_bomber_F-mat159.1-0 ; 
       il_bomber_F-mat160.1-0 ; 
       il_bomber_F-mat162.1-0 ; 
       il_bomber_F-mat164.1-0 ; 
       il_bomber_F-mat167.1-0 ; 
       il_bomber_F-mat168.1-0 ; 
       il_bomber_F-mat169.1-0 ; 
       il_bomber_F-mat170.1-0 ; 
       il_bomber_F-mat171.1-0 ; 
       il_bomber_F-mat173.1-0 ; 
       il_bomber_F-mat174.1-0 ; 
       il_bomber_F-mat175.1-0 ; 
       il_bomber_F-mat180.1-0 ; 
       il_bomber_F-mat181.1-0 ; 
       il_bomber_F-mat182.1-0 ; 
       il_bomber_F-mat185.1-0 ; 
       il_bomber_F-mat186.1-0 ; 
       il_bomber_F-mat192.1-0 ; 
       il_bomber_F-mat193.1-0 ; 
       il_bomber_F-mat194.1-0 ; 
       il_bomber_F-mat195.1-0 ; 
       il_bomber_F-mat196.1-0 ; 
       il_bomber_F-mat197.1-0 ; 
       il_bomber_F-mat198.1-0 ; 
       il_bomber_F-mat199.1-0 ; 
       il_bomber_F-mat200.1-0 ; 
       il_bomber_F-mat201.1-0 ; 
       il_bomber_F-mat202.1-0 ; 
       il_bomber_F-mat203.1-0 ; 
       il_bomber_F-mat204.1-0 ; 
       il_bomber_F-mat209.1-0 ; 
       il_bomber_F-mat210.1-0 ; 
       il_bomber_F-mat211.1-0 ; 
       il_bomber_F-mat212.1-0 ; 
       il_bomber_F-mat213.1-0 ; 
       il_bomber_F-mat214.1-0 ; 
       il_bomber_F-mat215.1-0 ; 
       il_bomber_F-mat216.1-0 ; 
       il_bomber_F-mat217.1-0 ; 
       il_bomber_F-mat218.1-0 ; 
       il_bomber_F-mat219.1-0 ; 
       il_bomber_F-mat220.1-0 ; 
       il_bomber_F-mat221.1-0 ; 
       il_bomber_F-mat222.1-0 ; 
       il_bomber_F-mat223.1-0 ; 
       il_bomber_F-mat224.1-0 ; 
       il_bomber_F-mat225.1-0 ; 
       il_bomber_F-mat232.1-0 ; 
       il_bomber_F-mat236.1-0 ; 
       il_bomber_F-mat237.1-0 ; 
       il_bomber_F-mat239.1-0 ; 
       il_bomber_F-mat240.1-0 ; 
       il_bomber_F-mat242.1-0 ; 
       il_bomber_F-mat243.1-0 ; 
       il_bomber_F-mat244.1-0 ; 
       il_bomber_F-mat247.1-0 ; 
       il_bomber_F-mat251.1-0 ; 
       il_bomber_F-mat255.1-0 ; 
       il_bomber_F-mat257.1-0 ; 
       il_bomber_F-mat258.1-0 ; 
       il_bomber_F-mat263.1-0 ; 
       il_bomber_F-mat264.1-0 ; 
       il_bomber_F-mat265.1-0 ; 
       il_bomber_F-mat266.1-0 ; 
       il_bomber_F-mat267.1-0 ; 
       il_bomber_F-mat268.1-0 ; 
       il_bomber_F-mat269.1-0 ; 
       il_bomber_F-mat270.1-0 ; 
       il_bomber_F-mat271.1-0 ; 
       il_bomber_F-mat46.1-0 ; 
       il_bomber_F-mat52.1-0 ; 
       il_bomber_F-mat66.1-0 ; 
       il_light_bomber_sT-default1.1-0 ; 
       il_light_bomber_sT-mat99.1-0 ; 
       skycraneF-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3.1-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Art fixes/soft/bugfixes/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-il_bomber_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       il_bomber_F-t2d101.1-0 ; 
       il_bomber_F-t2d102.1-0 ; 
       il_bomber_F-t2d105.1-0 ; 
       il_bomber_F-t2d106.1-0 ; 
       il_bomber_F-t2d110.1-0 ; 
       il_bomber_F-t2d111.1-0 ; 
       il_bomber_F-t2d112.1-0 ; 
       il_bomber_F-t2d113.1-0 ; 
       il_bomber_F-t2d114.1-0 ; 
       il_bomber_F-t2d115.1-0 ; 
       il_bomber_F-t2d116.1-0 ; 
       il_bomber_F-t2d117.1-0 ; 
       il_bomber_F-t2d118.1-0 ; 
       il_bomber_F-t2d119.1-0 ; 
       il_bomber_F-t2d120.1-0 ; 
       il_bomber_F-t2d121.1-0 ; 
       il_bomber_F-t2d125.1-0 ; 
       il_bomber_F-t2d127.1-0 ; 
       il_bomber_F-t2d128.1-0 ; 
       il_bomber_F-t2d129.1-0 ; 
       il_bomber_F-t2d130.1-0 ; 
       il_bomber_F-t2d131.1-0 ; 
       il_bomber_F-t2d132.1-0 ; 
       il_bomber_F-t2d133.1-0 ; 
       il_bomber_F-t2d134.1-0 ; 
       il_bomber_F-t2d135.1-0 ; 
       il_bomber_F-t2d136.1-0 ; 
       il_bomber_F-t2d137.1-0 ; 
       il_bomber_F-t2d138.1-0 ; 
       il_bomber_F-t2d149.1-0 ; 
       il_bomber_F-t2d150.1-0 ; 
       il_bomber_F-t2d151.1-0 ; 
       il_bomber_F-t2d153.1-0 ; 
       il_bomber_F-t2d154.1-0 ; 
       il_bomber_F-t2d155.1-0 ; 
       il_bomber_F-t2d156.1-0 ; 
       il_bomber_F-t2d160.1-0 ; 
       il_bomber_F-t2d164.1-0 ; 
       il_bomber_F-t2d168.1-0 ; 
       il_bomber_F-t2d169.1-0 ; 
       il_bomber_F-t2d170.1-0 ; 
       il_bomber_F-t2d175.1-0 ; 
       il_bomber_F-t2d176.1-0 ; 
       il_bomber_F-t2d177.1-0 ; 
       il_bomber_F-t2d178.1-0 ; 
       il_bomber_F-t2d65.1-0 ; 
       il_bomber_F-t2d66.1-0 ; 
       il_bomber_F-t2d69.1-0 ; 
       il_bomber_F-t2d71.1-0 ; 
       il_bomber_F-t2d72.1-0 ; 
       il_bomber_F-t2d76.1-0 ; 
       il_bomber_F-t2d78.1-0 ; 
       il_bomber_F-t2d79.1-0 ; 
       il_bomber_F-t2d83.1-0 ; 
       il_bomber_F-t2d84.1-0 ; 
       il_bomber_F-t2d86.1-0 ; 
       il_bomber_F-t2d88.1-0 ; 
       il_bomber_F-t2d90.1-0 ; 
       il_bomber_F-t2d91.1-0 ; 
       il_bomber_F-t2d92.1-0 ; 
       il_bomber_F-t2d93.1-0 ; 
       il_bomber_F-t2d95.1-0 ; 
       il_bomber_F-t2d96.1-0 ; 
       il_bomber_F-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 37 110 ; 
       3 35 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 37 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 11 110 ; 
       17 19 110 ; 
       18 38 110 ; 
       19 18 110 ; 
       20 17 110 ; 
       21 0 110 ; 
       22 3 110 ; 
       23 5 110 ; 
       24 6 110 ; 
       25 23 110 ; 
       26 28 110 ; 
       27 38 110 ; 
       28 27 110 ; 
       29 26 110 ; 
       30 12 110 ; 
       31 24 110 ; 
       32 5 110 ; 
       33 16 110 ; 
       34 25 110 ; 
       35 0 110 ; 
       36 35 110 ; 
       37 2 110 ; 
       38 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 68 300 ; 
       3 73 300 ; 
       5 80 300 ; 
       5 81 300 ; 
       5 3 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       5 32 300 ; 
       5 39 300 ; 
       5 72 300 ; 
       7 1 300 ; 
       7 60 300 ; 
       11 79 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 61 300 ; 
       12 64 300 ; 
       12 65 300 ; 
       13 82 300 ; 
       14 82 300 ; 
       14 12 300 ; 
       14 16 300 ; 
       14 17 300 ; 
       15 82 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       16 84 300 ; 
       16 4 300 ; 
       16 5 300 ; 
       16 6 300 ; 
       17 0 300 ; 
       17 33 300 ; 
       17 34 300 ; 
       17 35 300 ; 
       17 36 300 ; 
       17 58 300 ; 
       18 47 300 ; 
       18 48 300 ; 
       18 49 300 ; 
       18 50 300 ; 
       19 51 300 ; 
       19 52 300 ; 
       19 53 300 ; 
       21 67 300 ; 
       21 69 300 ; 
       21 70 300 ; 
       21 71 300 ; 
       23 24 300 ; 
       23 25 300 ; 
       23 26 300 ; 
       23 62 300 ; 
       24 57 300 ; 
       24 63 300 ; 
       25 2 300 ; 
       25 7 300 ; 
       25 8 300 ; 
       25 9 300 ; 
       26 27 300 ; 
       26 28 300 ; 
       26 29 300 ; 
       26 30 300 ; 
       26 31 300 ; 
       26 59 300 ; 
       27 42 300 ; 
       27 43 300 ; 
       27 44 300 ; 
       27 45 300 ; 
       28 40 300 ; 
       28 41 300 ; 
       28 46 300 ; 
       30 75 300 ; 
       31 76 300 ; 
       32 74 300 ; 
       33 78 300 ; 
       34 77 300 ; 
       35 83 300 ; 
       35 54 300 ; 
       35 55 300 ; 
       35 56 300 ; 
       35 66 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       13 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 45 401 ; 
       4 47 401 ; 
       5 48 401 ; 
       6 49 401 ; 
       7 50 401 ; 
       8 51 401 ; 
       9 52 401 ; 
       10 53 401 ; 
       11 54 401 ; 
       12 55 401 ; 
       13 56 401 ; 
       14 57 401 ; 
       15 58 401 ; 
       16 59 401 ; 
       17 60 401 ; 
       19 61 401 ; 
       20 62 401 ; 
       21 63 401 ; 
       22 0 401 ; 
       23 1 401 ; 
       25 2 401 ; 
       26 3 401 ; 
       28 5 401 ; 
       29 6 401 ; 
       30 7 401 ; 
       31 8 401 ; 
       32 9 401 ; 
       33 10 401 ; 
       34 11 401 ; 
       35 12 401 ; 
       36 13 401 ; 
       37 14 401 ; 
       38 15 401 ; 
       39 29 401 ; 
       41 16 401 ; 
       43 17 401 ; 
       44 18 401 ; 
       45 19 401 ; 
       46 20 401 ; 
       48 21 401 ; 
       49 22 401 ; 
       50 23 401 ; 
       52 24 401 ; 
       53 25 401 ; 
       54 26 401 ; 
       55 27 401 ; 
       56 28 401 ; 
       58 30 401 ; 
       59 31 401 ; 
       60 32 401 ; 
       61 33 401 ; 
       62 34 401 ; 
       63 35 401 ; 
       65 36 401 ; 
       66 37 401 ; 
       67 38 401 ; 
       68 39 401 ; 
       69 40 401 ; 
       70 41 401 ; 
       71 42 401 ; 
       72 43 401 ; 
       73 44 401 ; 
       81 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 SRT 1 1 1 -0.02148097 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 28.75 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 40 -10 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 0 -10 0 MPRFLG 0 ; 
       18 SCHEM 0 -6 0 MPRFLG 0 ; 
       19 SCHEM 0 -8 0 MPRFLG 0 ; 
       20 SCHEM 0 -12 0 MPRFLG 0 ; 
       21 SCHEM 35 -6 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 30 -6 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 30 -8 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 20 -8 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 25 -6 0 MPRFLG 0 ; 
       33 SCHEM 27.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 30 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 40 -6 0 MPRFLG 0 ; 
       36 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       38 SCHEM 1.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 105 26 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
