SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.20-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.32-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.32-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 82     
       il_light_bomber_sT-default1.2-0 ; 
       il_light_bomber_sT-mat99.3-0 ; 
       retext-back_shelf.1-0 ; 
       retext-bottom.1-0 ; 
       retext-default10.1-0 ; 
       retext-default11.1-0 ; 
       retext-default12.3-0 ; 
       retext-default13.1-0 ; 
       retext-global.1-0 ; 
       retext-lower_back_panel.1-0 ; 
       retext-mat162.1-0 ; 
       retext-mat164.1-0 ; 
       retext-mat167.1-0 ; 
       retext-mat168.1-0 ; 
       retext-mat169.1-0 ; 
       retext-mat170.1-0 ; 
       retext-mat171.1-0 ; 
       retext-mat173.1-0 ; 
       retext-mat174.1-0 ; 
       retext-mat175.1-0 ; 
       retext-mat180.1-0 ; 
       retext-mat181.1-0 ; 
       retext-mat182.1-0 ; 
       retext-mat185.1-0 ; 
       retext-mat186.1-0 ; 
       retext-mat192.1-0 ; 
       retext-mat193.1-0 ; 
       retext-mat194.1-0 ; 
       retext-mat195.1-0 ; 
       retext-mat196.1-0 ; 
       retext-mat198.1-0 ; 
       retext-mat199.1-0 ; 
       retext-mat200.1-0 ; 
       retext-mat201.1-0 ; 
       retext-mat202.1-0 ; 
       retext-mat203.1-0 ; 
       retext-mat209.1-0 ; 
       retext-mat210.1-0 ; 
       retext-mat211.1-0 ; 
       retext-mat212.1-0 ; 
       retext-mat213.1-0 ; 
       retext-mat214.1-0 ; 
       retext-mat215.1-0 ; 
       retext-mat216.1-0 ; 
       retext-mat217.1-0 ; 
       retext-mat218.1-0 ; 
       retext-mat219.1-0 ; 
       retext-mat220.1-0 ; 
       retext-mat221.1-0 ; 
       retext-mat222.1-0 ; 
       retext-mat232.1-0 ; 
       retext-mat236.1-0 ; 
       retext-mat237.1-0 ; 
       retext-mat239.1-0 ; 
       retext-mat240.1-0 ; 
       retext-mat242.1-0 ; 
       retext-mat243.1-0 ; 
       retext-mat244.1-0 ; 
       retext-mat247.1-0 ; 
       retext-mat255.1-0 ; 
       retext-mat257.1-0 ; 
       retext-mat258.1-0 ; 
       retext-mat263.1-0 ; 
       retext-mat264.1-0 ; 
       retext-mat266.1-0 ; 
       retext-mat267.1-0 ; 
       retext-mat268.1-0 ; 
       retext-mat269.1-0 ; 
       retext-mat270.1-0 ; 
       retext-mat271.1-0 ; 
       retext-mat272.1-0 ; 
       retext-mat273.1-0 ; 
       retext-mat274.2-0 ; 
       retext-mat275.1-0 ; 
       retext-mat276.1-0 ; 
       retext-mat277.1-0 ; 
       retext-mat278.1-0 ; 
       retext-mat279.1-0 ; 
       retext-mat46.1-0 ; 
       retext-side1.1-0 ; 
       retext-top.1-0 ; 
       retext-under_nose.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.19-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.6-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
       final-rotation_limit.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-retext.14-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 61     
       retext-t2d101.1-0 ; 
       retext-t2d102.1-0 ; 
       retext-t2d105.1-0 ; 
       retext-t2d106.1-0 ; 
       retext-t2d110.1-0 ; 
       retext-t2d111.1-0 ; 
       retext-t2d112.1-0 ; 
       retext-t2d113.1-0 ; 
       retext-t2d114.1-0 ; 
       retext-t2d115.4-0 ; 
       retext-t2d116.1-0 ; 
       retext-t2d117.1-0 ; 
       retext-t2d118.1-0 ; 
       retext-t2d119.1-0 ; 
       retext-t2d120.1-0 ; 
       retext-t2d121.1-0 ; 
       retext-t2d125.1-0 ; 
       retext-t2d127.1-0 ; 
       retext-t2d128.1-0 ; 
       retext-t2d129.1-0 ; 
       retext-t2d130.1-0 ; 
       retext-t2d131.1-0 ; 
       retext-t2d132.1-0 ; 
       retext-t2d133.1-0 ; 
       retext-t2d134.1-0 ; 
       retext-t2d135.1-0 ; 
       retext-t2d149.4-0 ; 
       retext-t2d150.1-0 ; 
       retext-t2d151.1-0 ; 
       retext-t2d153.1-0 ; 
       retext-t2d154.1-0 ; 
       retext-t2d155.1-0 ; 
       retext-t2d156.1-0 ; 
       retext-t2d160.1-0 ; 
       retext-t2d168.1-0 ; 
       retext-t2d169.1-0 ; 
       retext-t2d170.1-0 ; 
       retext-t2d175.1-0 ; 
       retext-t2d176.1-0 ; 
       retext-t2d177.4-0 ; 
       retext-t2d178.1-0 ; 
       retext-t2d179.4-0 ; 
       retext-t2d180.3-0 ; 
       retext-t2d181.3-0 ; 
       retext-t2d182.2-0 ; 
       retext-t2d183.1-0 ; 
       retext-t2d184.1-0 ; 
       retext-t2d185.1-0 ; 
       retext-t2d186.1-0 ; 
       retext-t2d187.1-0 ; 
       retext-t2d66.4-0 ; 
       retext-t2d83.4-0 ; 
       retext-t2d86.1-0 ; 
       retext-t2d88.1-0 ; 
       retext-t2d90.1-0 ; 
       retext-t2d91.1-0 ; 
       retext-t2d92.1-0 ; 
       retext-t2d93.1-0 ; 
       retext-t2d95.1-0 ; 
       retext-t2d96.1-0 ; 
       retext-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 38 110 ; 
       3 36 110 ; 
       4 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 38 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       18 20 110 ; 
       19 39 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 0 110 ; 
       23 3 110 ; 
       24 6 110 ; 
       25 7 110 ; 
       26 24 110 ; 
       27 29 110 ; 
       28 39 110 ; 
       29 28 110 ; 
       30 27 110 ; 
       31 13 110 ; 
       32 25 110 ; 
       33 6 110 ; 
       34 17 110 ; 
       35 26 110 ; 
       36 0 110 ; 
       37 36 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       17 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 60 300 ; 
       3 64 300 ; 
       6 8 300 ; 
       6 80 300 ; 
       6 3 300 ; 
       6 2 300 ; 
       6 9 300 ; 
       6 81 300 ; 
       6 79 300 ; 
       8 5 300 ; 
       8 53 300 ; 
       12 78 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 54 300 ; 
       13 57 300 ; 
       13 58 300 ; 
       14 0 300 ; 
       15 0 300 ; 
       15 10 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       16 0 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       18 4 300 ; 
       18 30 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       18 51 300 ; 
       19 43 300 ; 
       19 44 300 ; 
       19 45 300 ; 
       19 46 300 ; 
       20 47 300 ; 
       20 48 300 ; 
       20 49 300 ; 
       22 59 300 ; 
       22 61 300 ; 
       22 62 300 ; 
       22 63 300 ; 
       24 22 300 ; 
       24 23 300 ; 
       24 24 300 ; 
       24 55 300 ; 
       25 50 300 ; 
       25 56 300 ; 
       26 6 300 ; 
       26 72 300 ; 
       26 73 300 ; 
       26 74 300 ; 
       27 25 300 ; 
       27 26 300 ; 
       27 27 300 ; 
       27 28 300 ; 
       27 29 300 ; 
       27 52 300 ; 
       28 38 300 ; 
       28 39 300 ; 
       28 40 300 ; 
       28 41 300 ; 
       29 36 300 ; 
       29 37 300 ; 
       29 42 300 ; 
       31 66 300 ; 
       32 67 300 ; 
       33 65 300 ; 
       34 69 300 ; 
       35 68 300 ; 
       36 1 300 ; 
       36 70 300 ; 
       36 71 300 ; 
       17 7 300 ; 
       17 75 300 ; 
       17 76 300 ; 
       17 77 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 9 401 ; 
       3 51 401 ; 
       9 26 401 ; 
       10 52 401 ; 
       11 53 401 ; 
       12 54 401 ; 
       13 55 401 ; 
       14 56 401 ; 
       15 57 401 ; 
       17 58 401 ; 
       18 59 401 ; 
       19 60 401 ; 
       20 0 401 ; 
       21 1 401 ; 
       23 2 401 ; 
       24 3 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       28 7 401 ; 
       29 8 401 ; 
       30 10 401 ; 
       31 11 401 ; 
       32 12 401 ; 
       33 13 401 ; 
       34 14 401 ; 
       35 15 401 ; 
       37 16 401 ; 
       39 17 401 ; 
       40 18 401 ; 
       41 19 401 ; 
       42 20 401 ; 
       44 21 401 ; 
       45 22 401 ; 
       46 23 401 ; 
       48 24 401 ; 
       49 25 401 ; 
       51 27 401 ; 
       52 28 401 ; 
       53 29 401 ; 
       54 30 401 ; 
       55 31 401 ; 
       56 32 401 ; 
       58 33 401 ; 
       59 34 401 ; 
       60 35 401 ; 
       61 36 401 ; 
       62 37 401 ; 
       63 38 401 ; 
       64 40 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 44 401 ; 
       73 45 401 ; 
       74 46 401 ; 
       79 41 401 ; 
       80 50 401 ; 
       81 39 401 ; 
       75 47 401 ; 
       76 48 401 ; 
       77 49 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 44.94934 -4 0 MPRFLG 0 ; 
       5 SCHEM 50 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       6 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 45 -8 0 MPRFLG 0 ; 
       12 SCHEM 42.44934 -8.099906 0 USR MPRFLG 0 ; 
       13 SCHEM 25 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
       18 SCHEM 5 -8 0 MPRFLG 0 ; 
       19 SCHEM 5 -4 0 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -10 0 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 MPRFLG 0 ; 
       34 SCHEM 42.44934 -12.09991 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 45 -4 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       39 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 42.44934 -10.09991 0 MPRFLG 0 ; 
       40 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 43.94934 -10.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 43.94934 -10.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 43.94934 -10.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 41.44934 -14.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 43.94934 -10.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 46.44934 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 43.94934 -12.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 43.94934 -12.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 43.94934 -12.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 43.94934 -12.09991 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 43.94934 -12.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 43.94934 -12.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 43.94934 -12.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 46.44934 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 43.94934 -14.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 43.94934 -14.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 43.94934 -14.09991 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 68 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
