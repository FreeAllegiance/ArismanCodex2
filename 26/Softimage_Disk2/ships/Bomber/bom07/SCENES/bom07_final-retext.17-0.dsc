SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3_2.24-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.36-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.36-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       il_light_bomber_sT-default1.3-0 ; 
       il_light_bomber_sT-mat99.4-0 ; 
       retext-back_shelf.2-0 ; 
       retext-bottom.2-0 ; 
       retext-default10.2-0 ; 
       retext-default11.2-0 ; 
       retext-default12.4-0 ; 
       retext-default13.2-0 ; 
       retext-global.2-0 ; 
       retext-lower_back_panel.2-0 ; 
       retext-mat162.2-0 ; 
       retext-mat164.2-0 ; 
       retext-mat167.2-0 ; 
       retext-mat168.2-0 ; 
       retext-mat169.2-0 ; 
       retext-mat170.2-0 ; 
       retext-mat171.2-0 ; 
       retext-mat173.2-0 ; 
       retext-mat174.2-0 ; 
       retext-mat175.2-0 ; 
       retext-mat180.2-0 ; 
       retext-mat181.2-0 ; 
       retext-mat182.2-0 ; 
       retext-mat185.2-0 ; 
       retext-mat186.2-0 ; 
       retext-mat192.2-0 ; 
       retext-mat193.2-0 ; 
       retext-mat194.2-0 ; 
       retext-mat195.2-0 ; 
       retext-mat196.2-0 ; 
       retext-mat198.2-0 ; 
       retext-mat199.2-0 ; 
       retext-mat200.2-0 ; 
       retext-mat201.2-0 ; 
       retext-mat202.2-0 ; 
       retext-mat203.2-0 ; 
       retext-mat209.2-0 ; 
       retext-mat210.2-0 ; 
       retext-mat211.2-0 ; 
       retext-mat212.2-0 ; 
       retext-mat213.2-0 ; 
       retext-mat214.2-0 ; 
       retext-mat215.2-0 ; 
       retext-mat216.2-0 ; 
       retext-mat217.2-0 ; 
       retext-mat218.2-0 ; 
       retext-mat219.2-0 ; 
       retext-mat220.2-0 ; 
       retext-mat221.2-0 ; 
       retext-mat222.2-0 ; 
       retext-mat232.2-0 ; 
       retext-mat236.2-0 ; 
       retext-mat237.2-0 ; 
       retext-mat239.2-0 ; 
       retext-mat240.2-0 ; 
       retext-mat242.2-0 ; 
       retext-mat243.2-0 ; 
       retext-mat244.2-0 ; 
       retext-mat247.2-0 ; 
       retext-mat255.2-0 ; 
       retext-mat257.2-0 ; 
       retext-mat258.2-0 ; 
       retext-mat263.2-0 ; 
       retext-mat264.2-0 ; 
       retext-mat266.2-0 ; 
       retext-mat267.2-0 ; 
       retext-mat268.2-0 ; 
       retext-mat269.2-0 ; 
       retext-mat270.2-0 ; 
       retext-mat271.2-0 ; 
       retext-mat272.2-0 ; 
       retext-mat273.2-0 ; 
       retext-mat274.3-0 ; 
       retext-mat275.2-0 ; 
       retext-mat276.2-0 ; 
       retext-mat277.2-0 ; 
       retext-mat278.2-0 ; 
       retext-mat279.2-0 ; 
       retext-mat280.2-0 ; 
       retext-mat281.1-0 ; 
       retext-mat46.2-0 ; 
       retext-mat69.2-0 ; 
       retext-side1.2-0 ; 
       retext-top.2-0 ; 
       retext-under_nose.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.23-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.9-0 ROOT ; 
       bom07-cockpt_1_2.3-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-l-gun.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-r-gun.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
       retext-rotation_limit.2-0 ROOT ; 
       retext-rotation_limit_2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-retext.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       retext-t2d101.2-0 ; 
       retext-t2d102.2-0 ; 
       retext-t2d105.2-0 ; 
       retext-t2d106.2-0 ; 
       retext-t2d110.2-0 ; 
       retext-t2d111.2-0 ; 
       retext-t2d112.2-0 ; 
       retext-t2d113.2-0 ; 
       retext-t2d114.2-0 ; 
       retext-t2d115.5-0 ; 
       retext-t2d116.2-0 ; 
       retext-t2d117.2-0 ; 
       retext-t2d118.2-0 ; 
       retext-t2d119.2-0 ; 
       retext-t2d120.2-0 ; 
       retext-t2d121.2-0 ; 
       retext-t2d125.2-0 ; 
       retext-t2d127.2-0 ; 
       retext-t2d128.2-0 ; 
       retext-t2d129.2-0 ; 
       retext-t2d130.2-0 ; 
       retext-t2d131.2-0 ; 
       retext-t2d132.2-0 ; 
       retext-t2d133.2-0 ; 
       retext-t2d134.2-0 ; 
       retext-t2d135.2-0 ; 
       retext-t2d149.5-0 ; 
       retext-t2d150.2-0 ; 
       retext-t2d151.2-0 ; 
       retext-t2d153.2-0 ; 
       retext-t2d154.2-0 ; 
       retext-t2d155.2-0 ; 
       retext-t2d156.2-0 ; 
       retext-t2d160.2-0 ; 
       retext-t2d168.2-0 ; 
       retext-t2d169.2-0 ; 
       retext-t2d170.2-0 ; 
       retext-t2d175.2-0 ; 
       retext-t2d176.2-0 ; 
       retext-t2d177.5-0 ; 
       retext-t2d178.2-0 ; 
       retext-t2d179.5-0 ; 
       retext-t2d180.4-0 ; 
       retext-t2d181.4-0 ; 
       retext-t2d182.4-0 ; 
       retext-t2d183.3-0 ; 
       retext-t2d184.3-0 ; 
       retext-t2d185.3-0 ; 
       retext-t2d186.3-0 ; 
       retext-t2d187.3-0 ; 
       retext-t2d188.2-0 ; 
       retext-t2d189.1-0 ; 
       retext-t2d66.5-0 ; 
       retext-t2d83.5-0 ; 
       retext-t2d85.2-0 ; 
       retext-t2d86.2-0 ; 
       retext-t2d88.2-0 ; 
       retext-t2d90.2-0 ; 
       retext-t2d91.2-0 ; 
       retext-t2d92.2-0 ; 
       retext-t2d93.2-0 ; 
       retext-t2d95.2-0 ; 
       retext-t2d96.2-0 ; 
       retext-t2d97.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 41 110 ; 
       3 39 110 ; 
       4 7 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 41 110 ; 
       11 19 110 ; 
       12 2 110 ; 
       13 3 110 ; 
       14 7 110 ; 
       15 8 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 14 110 ; 
       20 22 110 ; 
       21 42 110 ; 
       22 21 110 ; 
       23 20 110 ; 
       24 0 110 ; 
       25 29 110 ; 
       26 3 110 ; 
       27 7 110 ; 
       28 8 110 ; 
       29 27 110 ; 
       30 32 110 ; 
       31 42 110 ; 
       32 31 110 ; 
       33 30 110 ; 
       34 15 110 ; 
       35 28 110 ; 
       36 7 110 ; 
       37 19 110 ; 
       38 29 110 ; 
       39 0 110 ; 
       40 39 110 ; 
       41 2 110 ; 
       42 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 60 300 ; 
       3 64 300 ; 
       7 8 300 ; 
       7 83 300 ; 
       7 3 300 ; 
       7 2 300 ; 
       7 9 300 ; 
       7 84 300 ; 
       7 82 300 ; 
       9 5 300 ; 
       9 53 300 ; 
       11 81 300 ; 
       14 80 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 54 300 ; 
       15 57 300 ; 
       15 58 300 ; 
       16 0 300 ; 
       17 0 300 ; 
       17 10 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       18 0 300 ; 
       18 11 300 ; 
       18 12 300 ; 
       18 13 300 ; 
       19 7 300 ; 
       19 75 300 ; 
       19 76 300 ; 
       19 77 300 ; 
       20 4 300 ; 
       20 30 300 ; 
       20 31 300 ; 
       20 32 300 ; 
       20 33 300 ; 
       20 51 300 ; 
       21 43 300 ; 
       21 44 300 ; 
       21 45 300 ; 
       21 46 300 ; 
       22 47 300 ; 
       22 48 300 ; 
       22 49 300 ; 
       24 59 300 ; 
       24 61 300 ; 
       24 62 300 ; 
       24 63 300 ; 
       25 78 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       27 24 300 ; 
       27 55 300 ; 
       28 50 300 ; 
       28 56 300 ; 
       29 6 300 ; 
       29 72 300 ; 
       29 73 300 ; 
       29 74 300 ; 
       30 25 300 ; 
       30 26 300 ; 
       30 27 300 ; 
       30 28 300 ; 
       30 29 300 ; 
       30 52 300 ; 
       31 38 300 ; 
       31 39 300 ; 
       31 40 300 ; 
       31 41 300 ; 
       32 36 300 ; 
       32 37 300 ; 
       32 42 300 ; 
       34 66 300 ; 
       35 67 300 ; 
       36 65 300 ; 
       37 69 300 ; 
       38 68 300 ; 
       39 1 300 ; 
       39 70 300 ; 
       39 71 300 ; 
       39 79 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       16 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 9 401 ; 
       3 53 401 ; 
       9 26 401 ; 
       10 55 401 ; 
       11 56 401 ; 
       12 57 401 ; 
       13 58 401 ; 
       14 59 401 ; 
       15 60 401 ; 
       17 61 401 ; 
       18 62 401 ; 
       19 63 401 ; 
       20 0 401 ; 
       21 1 401 ; 
       23 2 401 ; 
       24 3 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       28 7 401 ; 
       29 8 401 ; 
       30 10 401 ; 
       31 11 401 ; 
       32 12 401 ; 
       33 13 401 ; 
       34 14 401 ; 
       35 15 401 ; 
       37 16 401 ; 
       39 17 401 ; 
       40 18 401 ; 
       41 19 401 ; 
       42 20 401 ; 
       44 21 401 ; 
       45 22 401 ; 
       46 23 401 ; 
       48 24 401 ; 
       49 25 401 ; 
       51 27 401 ; 
       52 28 401 ; 
       53 29 401 ; 
       54 30 401 ; 
       55 31 401 ; 
       56 32 401 ; 
       58 33 401 ; 
       59 34 401 ; 
       60 35 401 ; 
       61 36 401 ; 
       62 37 401 ; 
       63 38 401 ; 
       64 40 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 44 401 ; 
       73 45 401 ; 
       74 46 401 ; 
       75 47 401 ; 
       76 48 401 ; 
       77 49 401 ; 
       78 50 401 ; 
       81 54 401 ; 
       82 41 401 ; 
       83 52 401 ; 
       84 39 401 ; 
       79 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       6 SCHEM 60 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       7 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 40.27937 -6.839772 0 USR MPRFLG 0 ; 
       12 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 50 -8 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -8 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       22 SCHEM 5 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 MPRFLG 0 ; 
       24 SCHEM 45 -4 0 MPRFLG 0 ; 
       25 SCHEM 35.55366 -7.107223 0 USR MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 MPRFLG 0 ; 
       35 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 30 -4 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 32.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 50 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       40 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       43 SCHEM 62.5 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
       44 SCHEM 55 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 37.05366 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 41.77937 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 34.55366 -9.107224 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 39.27937 -8.839772 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 65 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 37.05366 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 41.77937 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 34.55366 -11.10722 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 39.27937 -10.83977 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 67.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
