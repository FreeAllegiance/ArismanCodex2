SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom07-bom07_3.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.12-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       il_light_bomber_sT-default1.2-0 ; 
       il_light_bomber_sT-mat99.2-0 ; 
       skycraneF-default9.2-0 ; 
       tex-back_shelf.1-0 ; 
       tex-bottom.1-0 ; 
       tex-default10.1-0 ; 
       tex-default11.1-0 ; 
       tex-default12.1-0 ; 
       tex-global.1-0 ; 
       tex-lower_back_panel.1-0 ; 
       tex-mat141.1-0 ; 
       tex-mat145.1-0 ; 
       tex-mat146.1-0 ; 
       tex-mat150.1-0 ; 
       tex-mat154.1-0 ; 
       tex-mat155.1-0 ; 
       tex-mat162.1-0 ; 
       tex-mat164.1-0 ; 
       tex-mat167.1-0 ; 
       tex-mat168.1-0 ; 
       tex-mat169.1-0 ; 
       tex-mat170.1-0 ; 
       tex-mat171.1-0 ; 
       tex-mat173.1-0 ; 
       tex-mat174.1-0 ; 
       tex-mat175.1-0 ; 
       tex-mat180.1-0 ; 
       tex-mat181.1-0 ; 
       tex-mat182.1-0 ; 
       tex-mat185.1-0 ; 
       tex-mat186.1-0 ; 
       tex-mat192.1-0 ; 
       tex-mat193.1-0 ; 
       tex-mat194.1-0 ; 
       tex-mat195.1-0 ; 
       tex-mat196.1-0 ; 
       tex-mat198.1-0 ; 
       tex-mat199.1-0 ; 
       tex-mat200.1-0 ; 
       tex-mat201.1-0 ; 
       tex-mat202.1-0 ; 
       tex-mat203.1-0 ; 
       tex-mat209.1-0 ; 
       tex-mat210.1-0 ; 
       tex-mat211.1-0 ; 
       tex-mat212.1-0 ; 
       tex-mat213.1-0 ; 
       tex-mat214.1-0 ; 
       tex-mat215.1-0 ; 
       tex-mat216.1-0 ; 
       tex-mat217.1-0 ; 
       tex-mat218.1-0 ; 
       tex-mat219.1-0 ; 
       tex-mat220.1-0 ; 
       tex-mat221.1-0 ; 
       tex-mat222.1-0 ; 
       tex-mat223.1-0 ; 
       tex-mat224.1-0 ; 
       tex-mat225.1-0 ; 
       tex-mat232.1-0 ; 
       tex-mat236.1-0 ; 
       tex-mat237.1-0 ; 
       tex-mat239.1-0 ; 
       tex-mat240.1-0 ; 
       tex-mat242.1-0 ; 
       tex-mat243.1-0 ; 
       tex-mat244.1-0 ; 
       tex-mat247.1-0 ; 
       tex-mat251.1-0 ; 
       tex-mat255.1-0 ; 
       tex-mat257.1-0 ; 
       tex-mat258.1-0 ; 
       tex-mat263.1-0 ; 
       tex-mat264.1-0 ; 
       tex-mat266.1-0 ; 
       tex-mat267.1-0 ; 
       tex-mat268.1-0 ; 
       tex-mat269.1-0 ; 
       tex-mat270.1-0 ; 
       tex-mat271.1-0 ; 
       tex-mat46.1-0 ; 
       tex-sides.1-0 ; 
       tex-top.1-0 ; 
       tex-under_nose.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3.10-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-cockpt_1.2-0 ROOT ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-landgr0.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-LLa.1-0 ; 
       bom07-LLl.1-0 ; 
       bom07-LLr.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-wepbas0.1-0 ; 
       final-rotation_limit.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07_final-tex.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 63     
       tex-t2d101.1-0 ; 
       tex-t2d102.1-0 ; 
       tex-t2d105.1-0 ; 
       tex-t2d106.1-0 ; 
       tex-t2d110.1-0 ; 
       tex-t2d111.1-0 ; 
       tex-t2d112.1-0 ; 
       tex-t2d113.1-0 ; 
       tex-t2d114.1-0 ; 
       tex-t2d115.1-0 ; 
       tex-t2d116.1-0 ; 
       tex-t2d117.1-0 ; 
       tex-t2d118.1-0 ; 
       tex-t2d119.1-0 ; 
       tex-t2d120.1-0 ; 
       tex-t2d121.1-0 ; 
       tex-t2d125.1-0 ; 
       tex-t2d127.1-0 ; 
       tex-t2d128.1-0 ; 
       tex-t2d129.1-0 ; 
       tex-t2d130.1-0 ; 
       tex-t2d131.1-0 ; 
       tex-t2d132.1-0 ; 
       tex-t2d133.1-0 ; 
       tex-t2d134.1-0 ; 
       tex-t2d135.1-0 ; 
       tex-t2d136.1-0 ; 
       tex-t2d137.1-0 ; 
       tex-t2d138.1-0 ; 
       tex-t2d149.1-0 ; 
       tex-t2d150.1-0 ; 
       tex-t2d151.1-0 ; 
       tex-t2d153.1-0 ; 
       tex-t2d154.1-0 ; 
       tex-t2d155.1-0 ; 
       tex-t2d156.1-0 ; 
       tex-t2d160.1-0 ; 
       tex-t2d164.1-0 ; 
       tex-t2d168.1-0 ; 
       tex-t2d169.1-0 ; 
       tex-t2d170.1-0 ; 
       tex-t2d175.1-0 ; 
       tex-t2d176.1-0 ; 
       tex-t2d177.1-0 ; 
       tex-t2d178.1-0 ; 
       tex-t2d65.1-0 ; 
       tex-t2d66.1-0 ; 
       tex-t2d69.1-0 ; 
       tex-t2d71.1-0 ; 
       tex-t2d72.1-0 ; 
       tex-t2d76.1-0 ; 
       tex-t2d78.1-0 ; 
       tex-t2d79.1-0 ; 
       tex-t2d83.1-0 ; 
       tex-t2d86.1-0 ; 
       tex-t2d88.1-0 ; 
       tex-t2d90.1-0 ; 
       tex-t2d91.1-0 ; 
       tex-t2d92.1-0 ; 
       tex-t2d93.1-0 ; 
       tex-t2d95.1-0 ; 
       tex-t2d96.1-0 ; 
       tex-t2d97.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 38 110 ; 
       3 36 110 ; 
       4 6 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 38 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 6 110 ; 
       13 7 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 12 110 ; 
       18 20 110 ; 
       19 39 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 0 110 ; 
       23 3 110 ; 
       24 6 110 ; 
       25 7 110 ; 
       26 24 110 ; 
       27 29 110 ; 
       28 39 110 ; 
       29 28 110 ; 
       30 27 110 ; 
       31 13 110 ; 
       32 25 110 ; 
       33 6 110 ; 
       34 17 110 ; 
       35 26 110 ; 
       36 0 110 ; 
       37 36 110 ; 
       38 2 110 ; 
       39 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 40 300 ; 
       3 41 300 ; 
       3 70 300 ; 
       3 74 300 ; 
       6 8 300 ; 
       6 82 300 ; 
       6 81 300 ; 
       6 4 300 ; 
       6 3 300 ; 
       6 9 300 ; 
       6 83 300 ; 
       8 6 300 ; 
       8 62 300 ; 
       12 80 300 ; 
       12 26 300 ; 
       12 27 300 ; 
       12 63 300 ; 
       13 66 300 ; 
       13 67 300 ; 
       14 0 300 ; 
       15 0 300 ; 
       15 16 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       16 0 300 ; 
       16 17 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       17 2 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       17 12 300 ; 
       18 5 300 ; 
       18 36 300 ; 
       18 37 300 ; 
       18 38 300 ; 
       18 39 300 ; 
       18 60 300 ; 
       19 49 300 ; 
       19 50 300 ; 
       19 51 300 ; 
       19 52 300 ; 
       20 53 300 ; 
       20 54 300 ; 
       20 55 300 ; 
       22 69 300 ; 
       22 71 300 ; 
       22 72 300 ; 
       22 73 300 ; 
       24 28 300 ; 
       24 29 300 ; 
       24 30 300 ; 
       24 64 300 ; 
       25 59 300 ; 
       25 65 300 ; 
       26 7 300 ; 
       26 13 300 ; 
       26 14 300 ; 
       26 15 300 ; 
       27 31 300 ; 
       27 32 300 ; 
       27 33 300 ; 
       27 34 300 ; 
       27 35 300 ; 
       27 61 300 ; 
       28 44 300 ; 
       28 45 300 ; 
       28 46 300 ; 
       28 47 300 ; 
       29 42 300 ; 
       29 43 300 ; 
       29 48 300 ; 
       31 76 300 ; 
       32 77 300 ; 
       33 75 300 ; 
       34 79 300 ; 
       35 78 300 ; 
       36 1 300 ; 
       36 56 300 ; 
       36 57 300 ; 
       36 58 300 ; 
       36 68 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       81 45 401 ; 
       10 47 401 ; 
       11 48 401 ; 
       12 49 401 ; 
       13 50 401 ; 
       14 51 401 ; 
       15 52 401 ; 
       4 53 401 ; 
       16 54 401 ; 
       17 55 401 ; 
       18 56 401 ; 
       19 57 401 ; 
       20 58 401 ; 
       21 59 401 ; 
       23 60 401 ; 
       24 61 401 ; 
       25 62 401 ; 
       26 0 401 ; 
       27 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
       35 8 401 ; 
       3 9 401 ; 
       36 10 401 ; 
       37 11 401 ; 
       38 12 401 ; 
       39 13 401 ; 
       40 14 401 ; 
       41 15 401 ; 
       9 29 401 ; 
       43 16 401 ; 
       45 17 401 ; 
       46 18 401 ; 
       47 19 401 ; 
       48 20 401 ; 
       50 21 401 ; 
       51 22 401 ; 
       52 23 401 ; 
       54 24 401 ; 
       55 25 401 ; 
       56 26 401 ; 
       57 27 401 ; 
       58 28 401 ; 
       60 30 401 ; 
       61 31 401 ; 
       62 32 401 ; 
       63 33 401 ; 
       64 34 401 ; 
       65 35 401 ; 
       67 36 401 ; 
       68 37 401 ; 
       69 38 401 ; 
       70 39 401 ; 
       71 40 401 ; 
       72 41 401 ; 
       73 42 401 ; 
       83 43 401 ; 
       74 44 401 ; 
       82 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 211.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 120 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 208.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 167.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 237.5 0 0 SRT 1 1 1 0 0 0 0 0.09794417 1.540394 MPRFLG 0 ; 
       6 SCHEM 152.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 108.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 101.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 88.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 200 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 132.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 107.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 81.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 87.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 95 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 127.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 21.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 16.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 191.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 197.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 155 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 115 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 150 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 56.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 51.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 40 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 105 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 112.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 120 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 122.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 145 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 216.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 222.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 76.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 38.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 240 0 0 SRT 1 1 1 0 0 0 1.788218 -0.2179163 4.64888 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 220 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 202.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 205 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 207.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 210 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 212.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 195 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 190 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 192.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 217.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 235 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 210 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 212.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 225 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 227.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 230 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 232.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 195 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 187.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 190 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 192.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 182.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 217.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 202.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 205 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 207.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 236.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
