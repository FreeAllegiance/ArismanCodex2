SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.17-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 86     
       animation-back_shelf.1-0 ; 
       animation-bottom.1-0 ; 
       animation-default10.1-0 ; 
       animation-default11.1-0 ; 
       animation-default12.1-0 ; 
       animation-default13.1-0 ; 
       animation-global.1-0 ; 
       animation-lower_back_panel.1-0 ; 
       animation-mat171.1-0 ; 
       animation-mat173.1-0 ; 
       animation-mat174.1-0 ; 
       animation-mat175.1-0 ; 
       animation-mat180.1-0 ; 
       animation-mat181.1-0 ; 
       animation-mat182.1-0 ; 
       animation-mat185.1-0 ; 
       animation-mat186.1-0 ; 
       animation-mat192.1-0 ; 
       animation-mat193.1-0 ; 
       animation-mat194.1-0 ; 
       animation-mat195.1-0 ; 
       animation-mat196.1-0 ; 
       animation-mat198.1-0 ; 
       animation-mat199.1-0 ; 
       animation-mat200.1-0 ; 
       animation-mat201.1-0 ; 
       animation-mat202.1-0 ; 
       animation-mat203.1-0 ; 
       animation-mat209.1-0 ; 
       animation-mat210.1-0 ; 
       animation-mat211.1-0 ; 
       animation-mat212.1-0 ; 
       animation-mat213.1-0 ; 
       animation-mat214.1-0 ; 
       animation-mat215.1-0 ; 
       animation-mat216.1-0 ; 
       animation-mat217.1-0 ; 
       animation-mat218.1-0 ; 
       animation-mat219.1-0 ; 
       animation-mat220.1-0 ; 
       animation-mat221.1-0 ; 
       animation-mat222.1-0 ; 
       animation-mat236.1-0 ; 
       animation-mat237.1-0 ; 
       animation-mat239.1-0 ; 
       animation-mat240.1-0 ; 
       animation-mat242.1-0 ; 
       animation-mat255.1-0 ; 
       animation-mat257.1-0 ; 
       animation-mat258.1-0 ; 
       animation-mat263.1-0 ; 
       animation-mat264.1-0 ; 
       animation-mat266.1-0 ; 
       animation-mat267.1-0 ; 
       animation-mat268.1-0 ; 
       animation-mat269.1-0 ; 
       animation-mat270.1-0 ; 
       animation-mat271.1-0 ; 
       animation-mat272.1-0 ; 
       animation-mat273.1-0 ; 
       animation-mat274.1-0 ; 
       animation-mat275.1-0 ; 
       animation-mat276.1-0 ; 
       animation-mat277.1-0 ; 
       animation-mat278.1-0 ; 
       animation-mat279.1-0 ; 
       animation-mat280.1-0 ; 
       animation-mat281.1-0 ; 
       animation-mat46.1-0 ; 
       animation-mat69.1-0 ; 
       animation-side1.1-0 ; 
       animation-top.1-0 ; 
       animation-under_nose.1-0 ; 
       il_light_bomber_sT-mat99.1-0 ; 
       mess_with_landing_gear-mat282.1-0 ; 
       mess_with_landing_gear-mat82.1-0 ; 
       mess_with_landing_gear-mat84.1-0 ; 
       mess_with_landing_gear-mat85.1-0 ; 
       mess_with_landing_gear-mat86.1-0 ; 
       mess_with_landing_gear-mat87.1-0 ; 
       mess_with_landing_gear-mat88.1-0 ; 
       mess_with_landing_gear-mat90.1-0 ; 
       mess_with_landing_gear-mat92.1-0 ; 
       mess_with_landing_gear-mat93.1-0 ; 
       static-mat285.1-0 ; 
       static-mat286.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       bom07-afuselg0.1-0 ; 
       bom07-antenn1.1-0 ; 
       bom07-antenn2.1-0 ; 
       bom07-antenn3.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.13-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-cockpt.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-fwepatt.1-0 ; 
       bom07-l-gun.1-0 ; 
       bom07-lengine.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-lthrust.1-0 ; 
       bom07-lthruster.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas_3.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwepemt.1-0 ; 
       bom07-lwingzz.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-missemt.1-0 ; 
       bom07-r-gun.1-0 ; 
       bom07-rengine.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthrust.1-0 ; 
       bom07-rthruster.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas_3.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwepemt.1-0 ; 
       bom07-rwingzz.1-0 ; 
       bom07-SSal.1-0 ; 
       bom07-SSal1.1-0 ; 
       bom07-SSf.1-0 ; 
       bom07-SSml.1-0 ; 
       bom07-SSmr.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-trail.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-turret1.1-0 ; 
       bom07-turwepemt1.1-0 ; 
       bom07-turwepemt2.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/bom07b/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07b-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 65     
       animation-t2d101.1-0 ; 
       animation-t2d102.1-0 ; 
       animation-t2d105.1-0 ; 
       animation-t2d106.1-0 ; 
       animation-t2d111.1-0 ; 
       animation-t2d112.1-0 ; 
       animation-t2d113.1-0 ; 
       animation-t2d114.1-0 ; 
       animation-t2d115.1-0 ; 
       animation-t2d116.1-0 ; 
       animation-t2d117.1-0 ; 
       animation-t2d118.1-0 ; 
       animation-t2d119.1-0 ; 
       animation-t2d120.1-0 ; 
       animation-t2d121.1-0 ; 
       animation-t2d125.1-0 ; 
       animation-t2d127.1-0 ; 
       animation-t2d128.1-0 ; 
       animation-t2d129.1-0 ; 
       animation-t2d130.1-0 ; 
       animation-t2d131.1-0 ; 
       animation-t2d132.1-0 ; 
       animation-t2d133.1-0 ; 
       animation-t2d134.1-0 ; 
       animation-t2d135.1-0 ; 
       animation-t2d149.1-0 ; 
       animation-t2d150.1-0 ; 
       animation-t2d151.1-0 ; 
       animation-t2d153.1-0 ; 
       animation-t2d154.1-0 ; 
       animation-t2d155.1-0 ; 
       animation-t2d168.1-0 ; 
       animation-t2d169.1-0 ; 
       animation-t2d170.1-0 ; 
       animation-t2d175.1-0 ; 
       animation-t2d176.1-0 ; 
       animation-t2d177.1-0 ; 
       animation-t2d178.1-0 ; 
       animation-t2d179.1-0 ; 
       animation-t2d180.2-0 ; 
       animation-t2d181.2-0 ; 
       animation-t2d182.1-0 ; 
       animation-t2d183.1-0 ; 
       animation-t2d184.1-0 ; 
       animation-t2d185.1-0 ; 
       animation-t2d186.1-0 ; 
       animation-t2d187.1-0 ; 
       animation-t2d188.1-0 ; 
       animation-t2d189.2-0 ; 
       animation-t2d66.1-0 ; 
       animation-t2d83.1-0 ; 
       animation-t2d85.1-0 ; 
       animation-t2d95.1-0 ; 
       animation-t2d96.1-0 ; 
       animation-t2d97.1-0 ; 
       mess_with_landing_gear-t2d190.1-0 ; 
       mess_with_landing_gear-t2d87.1-0 ; 
       mess_with_landing_gear-t2d88.1-0 ; 
       mess_with_landing_gear-t2d89.1-0 ; 
       mess_with_landing_gear-t2d90.1-0 ; 
       mess_with_landing_gear-t2d93.1-0 ; 
       mess_with_landing_gear-t2d94.1-0 ; 
       mess_with_landing_gear-z.1-0 ; 
       static-t2d193.1-0 ; 
       static-t2d194.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 23 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 46 110 ; 
       6 43 110 ; 
       7 8 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 9 110 ; 
       11 46 110 ; 
       12 14 110 ; 
       13 6 110 ; 
       14 8 110 ; 
       15 9 110 ; 
       16 17 110 ; 
       17 14 110 ; 
       18 21 110 ; 
       19 47 110 ; 
       20 50 110 ; 
       21 20 110 ; 
       22 12 110 ; 
       23 47 110 ; 
       24 0 110 ; 
       25 32 110 ; 
       26 28 110 ; 
       27 6 110 ; 
       28 8 110 ; 
       29 9 110 ; 
       30 31 110 ; 
       31 28 110 ; 
       32 35 110 ; 
       33 47 110 ; 
       34 50 110 ; 
       35 34 110 ; 
       36 26 110 ; 
       37 47 110 ; 
       38 15 110 ; 
       39 29 110 ; 
       40 8 110 ; 
       41 17 110 ; 
       42 31 110 ; 
       43 0 110 ; 
       44 5 110 ; 
       45 43 110 ; 
       46 5 110 ; 
       47 4 110 ; 
       48 33 110 ; 
       49 24 110 ; 
       50 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 76 300 ; 
       2 77 300 ; 
       3 78 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 48 300 ; 
       6 52 300 ; 
       8 6 300 ; 
       8 71 300 ; 
       8 1 300 ; 
       8 0 300 ; 
       8 7 300 ; 
       8 72 300 ; 
       8 70 300 ; 
       10 3 300 ; 
       10 44 300 ; 
       12 69 300 ; 
       14 68 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       14 45 300 ; 
       15 84 300 ; 
       17 5 300 ; 
       17 63 300 ; 
       17 64 300 ; 
       17 65 300 ; 
       18 2 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 25 300 ; 
       18 42 300 ; 
       19 80 300 ; 
       20 35 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       21 39 300 ; 
       21 40 300 ; 
       21 41 300 ; 
       23 74 300 ; 
       24 47 300 ; 
       24 49 300 ; 
       24 50 300 ; 
       24 51 300 ; 
       26 66 300 ; 
       28 14 300 ; 
       28 15 300 ; 
       28 16 300 ; 
       28 46 300 ; 
       29 85 300 ; 
       31 4 300 ; 
       31 60 300 ; 
       31 61 300 ; 
       31 62 300 ; 
       32 17 300 ; 
       32 18 300 ; 
       32 19 300 ; 
       32 20 300 ; 
       32 21 300 ; 
       32 43 300 ; 
       33 81 300 ; 
       34 30 300 ; 
       34 31 300 ; 
       34 32 300 ; 
       34 33 300 ; 
       35 28 300 ; 
       35 29 300 ; 
       35 34 300 ; 
       37 79 300 ; 
       38 54 300 ; 
       39 55 300 ; 
       40 53 300 ; 
       41 57 300 ; 
       42 56 300 ; 
       43 73 300 ; 
       43 58 300 ; 
       43 59 300 ; 
       43 67 300 ; 
       47 75 300 ; 
       47 82 300 ; 
       47 83 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 50 401 ; 
       7 25 401 ; 
       9 52 401 ; 
       10 53 401 ; 
       11 54 401 ; 
       12 0 401 ; 
       13 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 12 401 ; 
       26 13 401 ; 
       27 14 401 ; 
       29 15 401 ; 
       31 16 401 ; 
       32 17 401 ; 
       33 18 401 ; 
       34 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 29 401 ; 
       46 30 401 ; 
       47 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 37 401 ; 
       58 39 401 ; 
       59 40 401 ; 
       60 41 401 ; 
       61 42 401 ; 
       62 43 401 ; 
       63 44 401 ; 
       64 45 401 ; 
       65 46 401 ; 
       66 47 401 ; 
       67 48 401 ; 
       69 51 401 ; 
       70 38 401 ; 
       71 49 401 ; 
       72 36 401 ; 
       74 55 401 ; 
       75 56 401 ; 
       76 62 401 ; 
       77 60 401 ; 
       78 61 401 ; 
       79 57 401 ; 
       82 58 401 ; 
       83 59 401 ; 
       84 63 401 ; 
       85 64 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -14 0 MPRFLG 0 ; 
       4 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 0 0 SRT 1 1 1 0.08377581 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 MPRFLG 0 ; 
       13 SCHEM 55 -8 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 5 -8 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 MPRFLG 0 ; 
       21 SCHEM 5 -6 0 MPRFLG 0 ; 
       22 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 50 -4 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 35 -4 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 MPRFLG 0 ; 
       34 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -8 0 MPRFLG 0 ; 
       38 SCHEM 25 -6 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 30 -4 0 MPRFLG 0 ; 
       41 SCHEM 40 -8 0 MPRFLG 0 ; 
       42 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 55 -4 0 MPRFLG 0 ; 
       44 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 15 -2 0 MPRFLG 0 ; 
       47 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       48 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
