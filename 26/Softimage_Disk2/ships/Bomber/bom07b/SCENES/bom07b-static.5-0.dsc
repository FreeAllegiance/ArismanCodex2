SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 76     
       animation-back_shelf.4-0 ; 
       animation-bottom.4-0 ; 
       animation-default10.4-0 ; 
       animation-default11.4-0 ; 
       animation-default12.4-0 ; 
       animation-default13.4-0 ; 
       animation-global.4-0 ; 
       animation-lower_back_panel.4-0 ; 
       animation-mat171.4-0 ; 
       animation-mat173.4-0 ; 
       animation-mat174.4-0 ; 
       animation-mat175.4-0 ; 
       animation-mat180.4-0 ; 
       animation-mat181.4-0 ; 
       animation-mat182.4-0 ; 
       animation-mat185.4-0 ; 
       animation-mat186.4-0 ; 
       animation-mat192.4-0 ; 
       animation-mat193.4-0 ; 
       animation-mat194.4-0 ; 
       animation-mat195.4-0 ; 
       animation-mat196.4-0 ; 
       animation-mat198.4-0 ; 
       animation-mat199.4-0 ; 
       animation-mat200.4-0 ; 
       animation-mat201.4-0 ; 
       animation-mat202.4-0 ; 
       animation-mat203.4-0 ; 
       animation-mat209.4-0 ; 
       animation-mat210.4-0 ; 
       animation-mat211.4-0 ; 
       animation-mat212.4-0 ; 
       animation-mat213.4-0 ; 
       animation-mat214.4-0 ; 
       animation-mat215.4-0 ; 
       animation-mat216.4-0 ; 
       animation-mat217.4-0 ; 
       animation-mat218.4-0 ; 
       animation-mat219.4-0 ; 
       animation-mat220.4-0 ; 
       animation-mat221.4-0 ; 
       animation-mat222.4-0 ; 
       animation-mat236.4-0 ; 
       animation-mat237.4-0 ; 
       animation-mat239.4-0 ; 
       animation-mat240.4-0 ; 
       animation-mat242.4-0 ; 
       animation-mat255.4-0 ; 
       animation-mat257.4-0 ; 
       animation-mat258.4-0 ; 
       animation-mat263.4-0 ; 
       animation-mat264.4-0 ; 
       animation-mat266.4-0 ; 
       animation-mat272.4-0 ; 
       animation-mat273.4-0 ; 
       animation-mat274.4-0 ; 
       animation-mat275.4-0 ; 
       animation-mat276.4-0 ; 
       animation-mat277.4-0 ; 
       animation-mat278.4-0 ; 
       animation-mat279.4-0 ; 
       animation-mat281.4-0 ; 
       animation-mat46.4-0 ; 
       animation-side1.4-0 ; 
       animation-top.4-0 ; 
       animation-under_nose.4-0 ; 
       il_light_bomber_sT-mat99.4-0 ; 
       mess_with_landing_gear-mat282.4-0 ; 
       mess_with_landing_gear-mat283.3-0 ; 
       mess_with_landing_gear-mat284.3-0 ; 
       mess_with_landing_gear-mat82.4-0 ; 
       mess_with_landing_gear-mat87.4-0 ; 
       mess_with_landing_gear-mat92.4-0 ; 
       mess_with_landing_gear-mat93.4-0 ; 
       static-mat285.4-0 ; 
       static-mat286.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       bom07-afuselg0.1-0 ; 
       bom07-aturatt.1-0 ; 
       bom07-bom07_3_2.20-0 ROOT ; 
       bom07-booster.1-0 ; 
       bom07-ffuselg.4-0 ; 
       bom07-finzz0.1-0 ; 
       bom07-finzzz1.1-0 ; 
       bom07-lbaydoor.1-0 ; 
       bom07-lffuselg.1-0 ; 
       bom07-lfinzzz.1-0 ; 
       bom07-lthruster.1-0 ; 
       bom07-lwepbar.1-0 ; 
       bom07-lwepbas1.2-0 ; 
       bom07-lwepbas2.1-0 ; 
       bom07-lwingzz.1-0 ; 
       bom07-mafuselg.1-0 ; 
       bom07-rbaydoor.1-0 ; 
       bom07-rffuselg.1-0 ; 
       bom07-rfinzzz.1-0 ; 
       bom07-rthruster.1-0 ; 
       bom07-rwepbar.1-0 ; 
       bom07-rwepbas1.1-0 ; 
       bom07-rwepbas2.1-0 ; 
       bom07-rwingzz.1-0 ; 
       bom07-tafuselg.1-0 ; 
       bom07-tturatt.1-0 ; 
       bom07-turret0.1-0 ; 
       bom07-turret1.1-0 ; 
       bom07-wepbas0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Bomber/bom07b/PICTURES/bom07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bom07b-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 62     
       animation-t2d101.4-0 ; 
       animation-t2d102.4-0 ; 
       animation-t2d105.4-0 ; 
       animation-t2d106.4-0 ; 
       animation-t2d111.4-0 ; 
       animation-t2d112.4-0 ; 
       animation-t2d113.4-0 ; 
       animation-t2d114.4-0 ; 
       animation-t2d115.4-0 ; 
       animation-t2d116.4-0 ; 
       animation-t2d117.4-0 ; 
       animation-t2d118.4-0 ; 
       animation-t2d119.4-0 ; 
       animation-t2d120.4-0 ; 
       animation-t2d121.4-0 ; 
       animation-t2d125.4-0 ; 
       animation-t2d127.4-0 ; 
       animation-t2d128.4-0 ; 
       animation-t2d129.4-0 ; 
       animation-t2d130.4-0 ; 
       animation-t2d131.4-0 ; 
       animation-t2d132.4-0 ; 
       animation-t2d133.4-0 ; 
       animation-t2d134.4-0 ; 
       animation-t2d135.4-0 ; 
       animation-t2d149.4-0 ; 
       animation-t2d150.4-0 ; 
       animation-t2d151.4-0 ; 
       animation-t2d153.4-0 ; 
       animation-t2d154.4-0 ; 
       animation-t2d155.4-0 ; 
       animation-t2d168.4-0 ; 
       animation-t2d169.4-0 ; 
       animation-t2d170.4-0 ; 
       animation-t2d175.4-0 ; 
       animation-t2d176.4-0 ; 
       animation-t2d177.4-0 ; 
       animation-t2d178.4-0 ; 
       animation-t2d179.4-0 ; 
       animation-t2d180.5-0 ; 
       animation-t2d181.5-0 ; 
       animation-t2d182.4-0 ; 
       animation-t2d183.4-0 ; 
       animation-t2d184.4-0 ; 
       animation-t2d185.4-0 ; 
       animation-t2d186.4-0 ; 
       animation-t2d187.4-0 ; 
       animation-t2d189.5-0 ; 
       animation-t2d66.4-0 ; 
       animation-t2d83.4-0 ; 
       animation-t2d95.4-0 ; 
       animation-t2d96.4-0 ; 
       animation-t2d97.4-0 ; 
       mess_with_landing_gear-t2d190.4-0 ; 
       mess_with_landing_gear-t2d191.3-0 ; 
       mess_with_landing_gear-t2d192.3-0 ; 
       mess_with_landing_gear-t2d87.4-0 ; 
       mess_with_landing_gear-t2d88.4-0 ; 
       mess_with_landing_gear-t2d89.4-0 ; 
       mess_with_landing_gear-t2d90.4-0 ; 
       static-t2d193.4-0 ; 
       static-t2d194.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 26 110 ; 
       3 24 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 15 110 ; 
       8 4 110 ; 
       9 5 110 ; 
       10 8 110 ; 
       11 13 110 ; 
       12 28 110 ; 
       13 12 110 ; 
       14 27 110 ; 
       15 0 110 ; 
       16 15 110 ; 
       17 4 110 ; 
       18 5 110 ; 
       19 17 110 ; 
       20 22 110 ; 
       21 28 110 ; 
       22 21 110 ; 
       23 27 110 ; 
       24 0 110 ; 
       25 24 110 ; 
       26 2 110 ; 
       27 1 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 48 300 ; 
       3 52 300 ; 
       4 6 300 ; 
       4 64 300 ; 
       4 1 300 ; 
       4 0 300 ; 
       4 7 300 ; 
       4 65 300 ; 
       4 63 300 ; 
       6 3 300 ; 
       6 44 300 ; 
       7 68 300 ; 
       8 62 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 45 300 ; 
       9 74 300 ; 
       10 5 300 ; 
       10 58 300 ; 
       10 59 300 ; 
       10 60 300 ; 
       11 2 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 24 300 ; 
       11 25 300 ; 
       11 42 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 37 300 ; 
       12 38 300 ; 
       13 39 300 ; 
       13 40 300 ; 
       13 41 300 ; 
       14 67 300 ; 
       15 47 300 ; 
       15 49 300 ; 
       15 50 300 ; 
       15 51 300 ; 
       16 69 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       17 46 300 ; 
       18 75 300 ; 
       19 4 300 ; 
       19 55 300 ; 
       19 56 300 ; 
       19 57 300 ; 
       20 17 300 ; 
       20 18 300 ; 
       20 19 300 ; 
       20 20 300 ; 
       20 21 300 ; 
       20 43 300 ; 
       21 30 300 ; 
       21 31 300 ; 
       21 32 300 ; 
       21 33 300 ; 
       22 28 300 ; 
       22 29 300 ; 
       22 34 300 ; 
       23 71 300 ; 
       24 66 300 ; 
       24 53 300 ; 
       24 54 300 ; 
       24 61 300 ; 
       27 70 300 ; 
       27 72 300 ; 
       27 73 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 49 401 ; 
       7 25 401 ; 
       9 50 401 ; 
       10 51 401 ; 
       11 52 401 ; 
       12 0 401 ; 
       13 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 12 401 ; 
       26 13 401 ; 
       27 14 401 ; 
       29 15 401 ; 
       31 16 401 ; 
       32 17 401 ; 
       33 18 401 ; 
       34 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 29 401 ; 
       46 30 401 ; 
       47 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 37 401 ; 
       53 39 401 ; 
       54 40 401 ; 
       55 41 401 ; 
       56 42 401 ; 
       57 43 401 ; 
       58 44 401 ; 
       59 45 401 ; 
       60 46 401 ; 
       61 47 401 ; 
       63 38 401 ; 
       64 48 401 ; 
       65 36 401 ; 
       67 53 401 ; 
       68 55 401 ; 
       69 54 401 ; 
       70 56 401 ; 
       71 57 401 ; 
       72 58 401 ; 
       73 59 401 ; 
       74 60 401 ; 
       75 61 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 38.75 0 0 SRT 1 1 1 0.08377581 0 0 0 0 1.056846 MPRFLG 0 ; 
       3 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 MPRFLG 0 ; 
       7 SCHEM 60 -6 0 MPRFLG 0 ; 
       8 SCHEM 50 -4 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 60 -4 0 MPRFLG 0 ; 
       16 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 35 -4 0 MPRFLG 0 ; 
       19 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 MPRFLG 0 ; 
       24 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 MPRFLG 0 ; 
       26 SCHEM 15 -2 0 MPRFLG 0 ; 
       27 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       28 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 32 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
