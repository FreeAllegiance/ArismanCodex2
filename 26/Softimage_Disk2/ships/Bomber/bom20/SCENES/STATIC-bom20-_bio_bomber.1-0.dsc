SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.19-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       bom20__bio_bomber-main.3-0 ; 
       bom20__bio_bomber-mat100.1-0 ; 
       bom20__bio_bomber-mat83.1-0 ; 
       bom20__bio_bomber-mat84.1-0 ; 
       bom20__bio_bomber-mat85.1-0 ; 
       bom20__bio_bomber-mat86.1-0 ; 
       bom20__bio_bomber-mat87.1-0 ; 
       bom20__bio_bomber-mat88.1-0 ; 
       bom20__bio_bomber-mat89.2-0 ; 
       bom20__bio_bomber-mat90.1-0 ; 
       bom20__bio_bomber-mat91.1-0 ; 
       bom20__bio_bomber-mat92.1-0 ; 
       bom20__bio_bomber-mat93.1-0 ; 
       bom20__bio_bomber-mat94.1-0 ; 
       bom20__bio_bomber-mat95.1-0 ; 
       bom20__bio_bomber-mat96.1-0 ; 
       bom20__bio_bomber-mat97.1-0 ; 
       bom20__bio_bomber-mat98.1-0 ; 
       bom20__bio_bomber-mat99.1-0 ; 
       bom20__bio_bomber-Panels1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       bomber-bool1.15-0 ROOT ; 
       bomber-cyl25.1-0 ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl36.1-0 ; 
       bomber-cyl43.1-0 ; 
       bomber-cyl44.1-0 ; 
       bomber-cyl45.1-0 ; 
       bomber-cyl46.1-0 ; 
       bomber-cyl47.1-0 ; 
       bomber-cyl48.1-0 ; 
       bomber-cyl49.1-0 ; 
       bomber-cyl50.1-0 ; 
       bomber-cyl51.1-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-sphere6.1-0 ; 
       bomber-sphere7.1-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_20.1-0 ; 
       bomber-zz_base_1_21.1-0 ; 
       bomber-zz_base_1_22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom20/PICTURES/bom20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bom20-_bio_bomber.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       bom20__bio_bomber-t2d10.2-0 ; 
       bom20__bio_bomber-t2d11.2-0 ; 
       bom20__bio_bomber-t2d12.2-0 ; 
       bom20__bio_bomber-t2d13.4-0 ; 
       bom20__bio_bomber-t2d14.2-0 ; 
       bom20__bio_bomber-t2d3.2-0 ; 
       bom20__bio_bomber-t2d4.2-0 ; 
       bom20__bio_bomber-t2d5.2-0 ; 
       bom20__bio_bomber-t2d6.2-0 ; 
       bom20__bio_bomber-t2d7.2-0 ; 
       bom20__bio_bomber-t2d8.2-0 ; 
       bom20__bio_bomber-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 17 110 ; 
       2 13 110 ; 
       3 17 110 ; 
       4 18 110 ; 
       5 18 110 ; 
       6 19 110 ; 
       7 19 110 ; 
       8 20 110 ; 
       9 20 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       12 13 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 18 110 ; 
       16 20 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 19 300 ; 
       1 12 300 ; 
       2 8 300 ; 
       3 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 1 300 ; 
       10 9 300 ; 
       11 10 300 ; 
       12 11 300 ; 
       15 5 300 ; 
       16 7 300 ; 
       17 2 300 ; 
       18 3 300 ; 
       19 4 300 ; 
       20 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 0 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       19 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 0 0 SRT 1.122 1.306007 1.122 2.249483e-009 5.801462e-016 1.570796 0 4.97031e-008 -1.503088 MPRFLG 0 ; 
       1 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 60 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 65 -4 0 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 70 -4 0 MPRFLG 0 ; 
       13 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 55 -6 0 MPRFLG 0 ; 
       17 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 MPRFLG 0 ; 
       19 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
