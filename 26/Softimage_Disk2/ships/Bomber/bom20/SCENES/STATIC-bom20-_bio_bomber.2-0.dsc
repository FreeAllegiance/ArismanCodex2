SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.22-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       bom20__bio_bomber-main.3-0 ; 
       bom20__bio_bomber-mat102.1-0 ; 
       bom20__bio_bomber-mat104.1-0 ; 
       bom20__bio_bomber-mat107.1-0 ; 
       bom20__bio_bomber-mat83.1-0 ; 
       bom20__bio_bomber-mat87.1-0 ; 
       bom20__bio_bomber-mat88.1-0 ; 
       bom20__bio_bomber-mat89.2-0 ; 
       bom20__bio_bomber-mat90.1-0 ; 
       bom20__bio_bomber-mat91.1-0 ; 
       bom20__bio_bomber-mat92.1-0 ; 
       bom20__bio_bomber-Panels1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       bomber-bool1.17-0 ROOT ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl49.1-0 ; 
       bomber-cyl50.1-0 ; 
       bomber-cyl51.1-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-sphere7.1-0 ; 
       bomber-sphere8.1-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_22.1-0 ; 
       bomber-zz_base_1_23.1-0 ; 
       bomber-zz_base_1_24.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom20/PICTURES/bom20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bom20-_bio_bomber.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       bom20__bio_bomber-t2d10.2-0 ; 
       bom20__bio_bomber-t2d11.2-0 ; 
       bom20__bio_bomber-t2d12.2-0 ; 
       bom20__bio_bomber-t2d13.4-0 ; 
       bom20__bio_bomber-t2d14.2-0 ; 
       bom20__bio_bomber-t2d17.1-0 ; 
       bom20__bio_bomber-t2d19.1-0 ; 
       bom20__bio_bomber-t2d23.1-0 ; 
       bom20__bio_bomber-t2d3.2-0 ; 
       bom20__bio_bomber-t2d7.2-0 ; 
       bom20__bio_bomber-t2d8.2-0 ; 
       bom20__bio_bomber-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       12 6 110 ; 
       11 6 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 10 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       8 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 11 300 ; 
       1 7 300 ; 
       12 3 300 ; 
       11 1 300 ; 
       2 8 300 ; 
       3 9 300 ; 
       4 10 300 ; 
       7 6 300 ; 
       9 4 300 ; 
       10 5 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       4 8 401 ; 
       1 5 401 ; 
       5 9 401 ; 
       6 10 401 ; 
       7 11 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       3 7 401 ; 
       2 6 401 ; 
       11 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 0 0 SRT 1.122 1.306007 1.122 2.249483e-009 5.801462e-016 1.570796 0 4.97031e-008 -1.503088 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
