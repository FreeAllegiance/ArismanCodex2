SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.27-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       bom20__bio_bomber-main.3-0 ; 
       bom20__bio_bomber-mat100.2-0 ; 
       bom20__bio_bomber-mat101.1-0 ; 
       bom20__bio_bomber-mat102.1-0 ; 
       bom20__bio_bomber-mat103.1-0 ; 
       bom20__bio_bomber-mat104.1-0 ; 
       bom20__bio_bomber-mat105.1-0 ; 
       bom20__bio_bomber-mat106.1-0 ; 
       bom20__bio_bomber-mat107.1-0 ; 
       bom20__bio_bomber-mat108.1-0 ; 
       bom20__bio_bomber-mat109.1-0 ; 
       bom20__bio_bomber-mat83.1-0 ; 
       bom20__bio_bomber-mat87.1-0 ; 
       bom20__bio_bomber-mat88.1-0 ; 
       bom20__bio_bomber-mat89.2-0 ; 
       bom20__bio_bomber-mat90.1-0 ; 
       bom20__bio_bomber-mat91.1-0 ; 
       bom20__bio_bomber-mat92.1-0 ; 
       bom20__bio_bomber-mat93.2-0 ; 
       bom20__bio_bomber-Panels1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       bomber-bool1.20-0 ROOT ; 
       bomber-cyl25.1-0 ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl48.1-0 ; 
       bomber-cyl49.1-0 ; 
       bomber-cyl50.1-0 ; 
       bomber-cyl51.1-0 ; 
       bomber-cyl52.1-0 ; 
       bomber-cyl53.1-0 ; 
       bomber-cyl54.1-0 ; 
       bomber-cyl55.1-0 ; 
       bomber-cyl56.1-0 ; 
       bomber-cyl57.1-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-sphere7.1-0 ; 
       bomber-sphere8.1-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_22.1-0 ; 
       bomber-zz_base_1_23.1-0 ; 
       bomber-zz_base_1_24.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom20/PICTURES/bom20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bom20-_bio_bomber.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       bom20__bio_bomber-t2d10.4-0 ; 
       bom20__bio_bomber-t2d11.4-0 ; 
       bom20__bio_bomber-t2d12.4-0 ; 
       bom20__bio_bomber-t2d13.6-0 ; 
       bom20__bio_bomber-t2d14.4-0 ; 
       bom20__bio_bomber-t2d15.3-0 ; 
       bom20__bio_bomber-t2d16.3-0 ; 
       bom20__bio_bomber-t2d17.3-0 ; 
       bom20__bio_bomber-t2d18.3-0 ; 
       bom20__bio_bomber-t2d19.3-0 ; 
       bom20__bio_bomber-t2d20.3-0 ; 
       bom20__bio_bomber-t2d21.3-0 ; 
       bom20__bio_bomber-t2d22.3-0 ; 
       bom20__bio_bomber-t2d23.3-0 ; 
       bom20__bio_bomber-t2d24.3-0 ; 
       bom20__bio_bomber-t2d25.3-0 ; 
       bom20__bio_bomber-t2d3.4-0 ; 
       bom20__bio_bomber-t2d7.4-0 ; 
       bom20__bio_bomber-t2d8.4-0 ; 
       bom20__bio_bomber-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 17 110 ; 
       2 13 110 ; 
       3 18 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 18 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       10 17 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 18 110 ; 
       16 19 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 19 300 ; 
       1 18 300 ; 
       2 14 300 ; 
       3 1 300 ; 
       4 15 300 ; 
       5 16 300 ; 
       6 17 300 ; 
       7 2 300 ; 
       8 4 300 ; 
       9 6 300 ; 
       10 7 300 ; 
       11 9 300 ; 
       12 10 300 ; 
       15 13 300 ; 
       16 5 300 ; 
       17 11 300 ; 
       18 12 300 ; 
       19 3 300 ; 
       20 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 8 401 ; 
       5 9 401 ; 
       6 10 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       11 16 401 ; 
       12 17 401 ; 
       13 18 401 ; 
       14 19 401 ; 
       15 0 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 11 401 ; 
       19 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 4.97031e-008 -1.503088 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       13 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 10 -4 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 23.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
