SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.18-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       bom20__bio_bomber-main.3-0 ; 
       bom20__bio_bomber-mat100.1-0 ; 
       bom20__bio_bomber-mat83.1-0 ; 
       bom20__bio_bomber-mat84.1-0 ; 
       bom20__bio_bomber-mat85.1-0 ; 
       bom20__bio_bomber-mat86.1-0 ; 
       bom20__bio_bomber-mat87.1-0 ; 
       bom20__bio_bomber-mat88.1-0 ; 
       bom20__bio_bomber-mat89.2-0 ; 
       bom20__bio_bomber-mat90.1-0 ; 
       bom20__bio_bomber-mat91.1-0 ; 
       bom20__bio_bomber-mat92.1-0 ; 
       bom20__bio_bomber-mat93.1-0 ; 
       bom20__bio_bomber-mat94.1-0 ; 
       bom20__bio_bomber-mat95.1-0 ; 
       bom20__bio_bomber-mat96.1-0 ; 
       bom20__bio_bomber-mat97.1-0 ; 
       bom20__bio_bomber-mat98.1-0 ; 
       bom20__bio_bomber-mat99.1-0 ; 
       bom20__bio_bomber-Panels1.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.2-0 ; 
       utl101-mat77.2-0 ; 
       utl101-mat78.2-0 ; 
       utl101-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       bomber-bool1.14-0 ROOT ; 
       bomber-cockpt.1-0 ; 
       bomber-cyl25.1-0 ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl36.1-0 ; 
       bomber-cyl43.1-0 ; 
       bomber-cyl44.1-0 ; 
       bomber-cyl45.1-0 ; 
       bomber-cyl46.1-0 ; 
       bomber-cyl47.1-0 ; 
       bomber-cyl48.1-0 ; 
       bomber-cyl49.1-0 ; 
       bomber-cyl50.1-0 ; 
       bomber-cyl51.1-0 ; 
       bomber-lbthrust.2-0 ; 
       bomber-lsmoke.2-0 ; 
       bomber-ltthrust.2-0 ; 
       bomber-lwepemt.3-0 ; 
       bomber-missemt.1-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-rbthrust.2-0 ; 
       bomber-rsmoke.2-0 ; 
       bomber-rtthrust.2-0 ; 
       bomber-rwepemt.2-0 ; 
       bomber-sphere6.1-0 ; 
       bomber-sphere7.1-0 ; 
       bomber-SS01.2-0 ; 
       bomber-SS02.2-0 ; 
       bomber-SS03.2-0 ; 
       bomber-SS04.2-0 ; 
       bomber-SS05.2-0 ; 
       bomber-SS06.2-0 ; 
       bomber-trail.2-0 ; 
       bomber-turwepemt1.1-0 ; 
       bomber-turwepemt2.3-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_20.1-0 ; 
       bomber-zz_base_1_21.1-0 ; 
       bomber-zz_base_1_22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom20/PICTURES/bom20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-bom20-_bio_bomber.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       bom20__bio_bomber-t2d10.2-0 ; 
       bom20__bio_bomber-t2d11.2-0 ; 
       bom20__bio_bomber-t2d12.2-0 ; 
       bom20__bio_bomber-t2d13.4-0 ; 
       bom20__bio_bomber-t2d14.2-0 ; 
       bom20__bio_bomber-t2d3.2-0 ; 
       bom20__bio_bomber-t2d4.2-0 ; 
       bom20__bio_bomber-t2d5.2-0 ; 
       bom20__bio_bomber-t2d6.2-0 ; 
       bom20__bio_bomber-t2d7.2-0 ; 
       bom20__bio_bomber-t2d8.2-0 ; 
       bom20__bio_bomber-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 36 110 ; 
       3 19 110 ; 
       4 36 110 ; 
       5 37 110 ; 
       6 37 110 ; 
       7 38 110 ; 
       8 38 110 ; 
       9 39 110 ; 
       10 39 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 37 110 ; 
       26 39 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       33 0 110 ; 
       34 0 110 ; 
       35 0 110 ; 
       36 20 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 19 300 ; 
       2 12 300 ; 
       3 8 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       6 15 300 ; 
       7 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       10 1 300 ; 
       11 9 300 ; 
       12 10 300 ; 
       13 11 300 ; 
       25 5 300 ; 
       26 7 300 ; 
       27 20 300 ; 
       28 21 300 ; 
       29 23 300 ; 
       30 22 300 ; 
       31 25 300 ; 
       32 24 300 ; 
       36 2 300 ; 
       37 3 300 ; 
       38 4 300 ; 
       39 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 0 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       19 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 0 0 SRT 1.122 1.306007 1.122 2.249483e-009 5.801462e-016 1.570796 0 4.97031e-008 -1.503088 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 40 -6 0 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 65 -6 0 MPRFLG 0 ; 
       11 SCHEM 75 -4 0 MPRFLG 0 ; 
       12 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 80 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -6 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 85 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 90 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 92.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 40 -4 0 MPRFLG 0 ; 
       37 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 66.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
