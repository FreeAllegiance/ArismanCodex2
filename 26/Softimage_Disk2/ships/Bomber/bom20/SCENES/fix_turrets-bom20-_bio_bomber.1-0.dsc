SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.25-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       bom20__bio_bomber-main.3-0 ; 
       bom20__bio_bomber-mat100.2-0 ; 
       bom20__bio_bomber-mat101.1-0 ; 
       bom20__bio_bomber-mat102.1-0 ; 
       bom20__bio_bomber-mat103.1-0 ; 
       bom20__bio_bomber-mat104.1-0 ; 
       bom20__bio_bomber-mat105.1-0 ; 
       bom20__bio_bomber-mat106.1-0 ; 
       bom20__bio_bomber-mat107.1-0 ; 
       bom20__bio_bomber-mat108.1-0 ; 
       bom20__bio_bomber-mat109.1-0 ; 
       bom20__bio_bomber-mat83.1-0 ; 
       bom20__bio_bomber-mat87.1-0 ; 
       bom20__bio_bomber-mat88.1-0 ; 
       bom20__bio_bomber-mat89.2-0 ; 
       bom20__bio_bomber-mat90.1-0 ; 
       bom20__bio_bomber-mat91.1-0 ; 
       bom20__bio_bomber-mat92.1-0 ; 
       bom20__bio_bomber-mat93.2-0 ; 
       bom20__bio_bomber-Panels1.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.2-0 ; 
       utl101-mat77.2-0 ; 
       utl101-mat78.2-0 ; 
       utl101-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       bomber-bool1.19-0 ROOT ; 
       bomber-cockpt.1-0 ; 
       bomber-cyl25.1-0 ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl48.1-0 ; 
       bomber-cyl49.1-0 ; 
       bomber-cyl50.1-0 ; 
       bomber-cyl51.1-0 ; 
       bomber-cyl52.1-0 ; 
       bomber-cyl53.1-0 ; 
       bomber-cyl54.1-0 ; 
       bomber-cyl55.1-0 ; 
       bomber-cyl56.1-0 ; 
       bomber-cyl57.1-0 ; 
       bomber-lbthrust.2-0 ; 
       bomber-lsmoke.2-0 ; 
       bomber-ltthrust.2-0 ; 
       bomber-lwepemt.3-0 ; 
       bomber-missemt.1-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-rbthrust.2-0 ; 
       bomber-rsmoke.2-0 ; 
       bomber-rtthrust.2-0 ; 
       bomber-rwepemt.2-0 ; 
       bomber-sphere7.1-0 ; 
       bomber-sphere8.1-0 ; 
       bomber-SS01.2-0 ; 
       bomber-SS02.2-0 ; 
       bomber-SS03.2-0 ; 
       bomber-SS04.2-0 ; 
       bomber-SS05.2-0 ; 
       bomber-SS06.2-0 ; 
       bomber-trail.2-0 ; 
       bomber-turwepemt1.1-0 ; 
       bomber-turwepemt2.3-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_22.1-0 ; 
       bomber-zz_base_1_23.1-0 ; 
       bomber-zz_base_1_24.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Bomber/bom20/PICTURES/bom20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fix_turrets-bom20-_bio_bomber.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       bom20__bio_bomber-t2d10.4-0 ; 
       bom20__bio_bomber-t2d11.4-0 ; 
       bom20__bio_bomber-t2d12.4-0 ; 
       bom20__bio_bomber-t2d13.6-0 ; 
       bom20__bio_bomber-t2d14.4-0 ; 
       bom20__bio_bomber-t2d15.3-0 ; 
       bom20__bio_bomber-t2d16.3-0 ; 
       bom20__bio_bomber-t2d17.3-0 ; 
       bom20__bio_bomber-t2d18.3-0 ; 
       bom20__bio_bomber-t2d19.3-0 ; 
       bom20__bio_bomber-t2d20.3-0 ; 
       bom20__bio_bomber-t2d21.3-0 ; 
       bom20__bio_bomber-t2d22.3-0 ; 
       bom20__bio_bomber-t2d23.3-0 ; 
       bom20__bio_bomber-t2d24.3-0 ; 
       bom20__bio_bomber-t2d25.3-0 ; 
       bom20__bio_bomber-t2d3.4-0 ; 
       bom20__bio_bomber-t2d7.4-0 ; 
       bom20__bio_bomber-t2d8.4-0 ; 
       bom20__bio_bomber-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 36 110 ; 
       3 19 110 ; 
       4 37 110 ; 
       5 19 110 ; 
       6 19 110 ; 
       7 19 110 ; 
       8 37 110 ; 
       9 38 110 ; 
       10 38 110 ; 
       11 36 110 ; 
       12 39 110 ; 
       13 39 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 37 110 ; 
       26 38 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       33 0 110 ; 
       34 0 110 ; 
       35 0 110 ; 
       36 20 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 19 300 ; 
       2 18 300 ; 
       3 14 300 ; 
       4 1 300 ; 
       5 15 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 2 300 ; 
       9 4 300 ; 
       10 6 300 ; 
       11 7 300 ; 
       12 9 300 ; 
       13 10 300 ; 
       25 13 300 ; 
       26 5 300 ; 
       27 20 300 ; 
       28 21 300 ; 
       29 23 300 ; 
       30 22 300 ; 
       31 25 300 ; 
       32 24 300 ; 
       36 11 300 ; 
       37 12 300 ; 
       38 3 300 ; 
       39 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 8 401 ; 
       5 9 401 ; 
       6 10 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       11 16 401 ; 
       12 17 401 ; 
       13 18 401 ; 
       14 19 401 ; 
       15 0 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 11 401 ; 
       19 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 4.97031e-008 -1.503088 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 67.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 52.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 55 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 60 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 66.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 48.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 42.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 77.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 75 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 72.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 80 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 82.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 38.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 45 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 58.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 81.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
