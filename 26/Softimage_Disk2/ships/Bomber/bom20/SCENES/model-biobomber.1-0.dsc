SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       utl101-mat71.1-0 ; 
       utl101-mat75.1-0 ; 
       utl101-mat77.1-0 ; 
       utl101-mat78.1-0 ; 
       utl101-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       biobomber-175deg2.1-0 ROOT ; 
       bomber-bool1.1-0 ROOT ; 
       bomber-cockpt.1-0 ; 
       bomber-cyl21.1-0 ; 
       bomber-cyl22.1-0 ; 
       bomber-cyl25.1-0 ; 
       bomber-cyl30.1-0 ; 
       bomber-cyl31.1-0 ; 
       bomber-cyl32.1-0 ; 
       bomber-cyl33.1-0 ; 
       bomber-cyl34.1-0 ; 
       bomber-cyl35.1-0 ; 
       bomber-cyl36.1-0 ; 
       bomber-cyl37.1-0 ; 
       bomber-cyl38.1-0 ; 
       bomber-lbthrust.2-0 ; 
       bomber-lsmoke.2-0 ; 
       bomber-ltthrust.2-0 ; 
       bomber-lwepemt.3-0 ; 
       bomber-null3.1-0 ; 
       bomber-null6.1-0 ; 
       bomber-rbthrust.2-0 ; 
       bomber-rsmoke.2-0 ; 
       bomber-rtthrust.2-0 ; 
       bomber-rwepemt.2-0 ; 
       bomber-sphere3.1-0 ; 
       bomber-sphere4.1-0 ; 
       bomber-SS01.2-0 ; 
       bomber-SS02.2-0 ; 
       bomber-SS03.2-0 ; 
       bomber-SS04.2-0 ; 
       bomber-SS05.2-0 ; 
       bomber-SS06.2-0 ; 
       bomber-trail.2-0 ; 
       bomber-turwepemt1.1-0 ; 
       bomber-turwepemt2.3-0 ; 
       bomber-zz_base_1_11.1-0 ; 
       bomber-zz_base_1_16.1-0 ; 
       bomber-zz_base_1_17.1-0 ; 
       bomber-zz_base_1_2.1-0 ; 
       turcone-175deg.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-biobomber.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       18 1 110 ; 
       18 24 114 ; 
       2 1 110 ; 
       3 39 110 ; 
       4 39 110 ; 
       5 36 110 ; 
       6 19 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 36 110 ; 
       13 38 110 ; 
       14 38 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       25 39 110 ; 
       26 37 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       35 1 110 ; 
       24 1 110 ; 
       36 20 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       34 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       27 0 300 ; 
       28 1 300 ; 
       29 3 300 ; 
       30 2 300 ; 
       31 5 300 ; 
       32 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.20373 18.67338 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 3.011593 0 0 0 -1.590189 -0.2591608 MPRFLG 0 ; 
       1 SCHEM 25.04403 14.69099 0 USR DISPLAY 3 2 SRT 1.122 1.306007 1.122 2.249483e-009 5.801462e-016 1.570796 0.04835916 4.97031e-008 -1.503088 MPRFLG 0 ; 
       18 SCHEM 19.93843 1.723902 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 16.00604 2.171375 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 11.29403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13.79403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 23.79403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 33.79403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 36.29403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 38.79403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 41.29403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 21.29403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 18.79403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 26.29403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 28.79403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 31.29402 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 23.10097 0.3078396 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 27.51779 0.6772472 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 23.12218 2.370365 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 37.54403 12.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 20.04403 12.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 23.09618 0.9850867 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 0 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 23.13517 1.723902 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 8.794026 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 16.29403 8.690989 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 32.42061 0.8824221 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 32.5 0 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 34.84122 0.8824223 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 34.78485 -0.03393932 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 37.29577 0.8484828 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 37.34122 -0.03393932 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 15.91702 1.262143 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 28.5015 2.333704 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 19.99351 0.9543029 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 25.04403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 18.79403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 30.04402 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 11.29403 10.69099 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 22.33774 19.67584 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 0.13 0 0 0 1.590189 -0.2591608 MPRFLG 0 ; 
       34 SCHEM 28.62109 2.966997 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
