SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.46-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       bom30_belter_bomber-bottom1.3-0 ; 
       bom30_belter_bomber-mat105.1-0 ; 
       bom30_belter_bomber-mat81.2-0 ; 
       bom30_belter_bomber-mat82.2-0 ; 
       bom30_belter_bomber-mat83.2-0 ; 
       bom30_belter_bomber-mat84.3-0 ; 
       bom30_belter_bomber-mat85.1-0 ; 
       bom30_belter_bomber-mat86.1-0 ; 
       bom30_belter_bomber-mat87.1-0 ; 
       bom30_belter_bomber-mat88.1-0 ; 
       bom30_belter_bomber-mat89.2-0 ; 
       bom30_belter_bomber-mat90.1-0 ; 
       bom30_belter_bomber-mat91.1-0 ; 
       bom30_belter_bomber-mat92.1-0 ; 
       bom30_belter_bomber-mat93.2-0 ; 
       bom30_belter_bomber-mat94.1-0 ; 
       bom30_belter_bomber-mat95.1-0 ; 
       bom30_belter_bomber-mat96.1-0 ; 
       bom30_belter_bomber-mat99.1-0 ; 
       bom30_belter_bomber-top1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       bel_fighter-cube10.1-0 ; 
       bel_fighter-cube2_1.1-0 ; 
       bel_fighter-cube2_1_1.3-0 ; 
       bel_fighter-cube22.1-0 ; 
       bel_fighter-cube8.1-0 ; 
       bel_fighter-cyl3_1.1-0 ; 
       bel_fighter-cyl4_1.4-0 ; 
       bel_fighter-cyl9.1-0 ; 
       bel_fighter-null2.38-0 ROOT ; 
       bel_fighter-sphere2.1-0 ; 
       bel_fighter-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom30/PICTURES/bom30 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bom30-belter_bomber.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       bom30_belter_bomber-t2d1.4-0 ; 
       bom30_belter_bomber-t2d10.1-0 ; 
       bom30_belter_bomber-t2d11.3-0 ; 
       bom30_belter_bomber-t2d12.3-0 ; 
       bom30_belter_bomber-t2d13.1-0 ; 
       bom30_belter_bomber-t2d14.1-0 ; 
       bom30_belter_bomber-t2d15.4-0 ; 
       bom30_belter_bomber-t2d16.3-0 ; 
       bom30_belter_bomber-t2d17.2-0 ; 
       bom30_belter_bomber-t2d18.2-0 ; 
       bom30_belter_bomber-t2d19.1-0 ; 
       bom30_belter_bomber-t2d2.4-0 ; 
       bom30_belter_bomber-t2d26.1-0 ; 
       bom30_belter_bomber-t2d3.4-0 ; 
       bom30_belter_bomber-t2d4.10-0 ; 
       bom30_belter_bomber-t2d5.9-0 ; 
       bom30_belter_bomber-t2d6.9-0 ; 
       bom30_belter_bomber-t2d7.3-0 ; 
       bom30_belter_bomber-t2d8.3-0 ; 
       bom30_belter_bomber-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 6 110 ; 
       2 8 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 6 110 ; 
       6 2 110 ; 
       7 0 110 ; 
       9 2 110 ; 
       10 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 1 300 ; 
       3 12 300 ; 
       4 18 300 ; 
       5 13 300 ; 
       6 5 300 ; 
       6 19 300 ; 
       6 0 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       9 16 300 ; 
       10 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 16 401 ; 
       1 12 401 ; 
       2 0 401 ; 
       3 11 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 17 401 ; 
       7 18 401 ; 
       8 19 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 40 -4 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 -8.599935e-009 -6.029957e-009 -4.524091e-008 MPRFLG 0 ; 
       9 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
