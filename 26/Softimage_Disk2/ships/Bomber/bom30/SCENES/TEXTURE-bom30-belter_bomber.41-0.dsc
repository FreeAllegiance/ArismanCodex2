SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.45-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       bom30_belter_bomber-bottom1.3-0 ; 
       bom30_belter_bomber-mat100.1-0 ; 
       bom30_belter_bomber-mat101.1-0 ; 
       bom30_belter_bomber-mat102.1-0 ; 
       bom30_belter_bomber-mat103.1-0 ; 
       bom30_belter_bomber-mat104.1-0 ; 
       bom30_belter_bomber-mat105.1-0 ; 
       bom30_belter_bomber-mat81.2-0 ; 
       bom30_belter_bomber-mat82.2-0 ; 
       bom30_belter_bomber-mat83.2-0 ; 
       bom30_belter_bomber-mat84.3-0 ; 
       bom30_belter_bomber-mat85.1-0 ; 
       bom30_belter_bomber-mat86.1-0 ; 
       bom30_belter_bomber-mat87.1-0 ; 
       bom30_belter_bomber-mat88.1-0 ; 
       bom30_belter_bomber-mat89.2-0 ; 
       bom30_belter_bomber-mat90.1-0 ; 
       bom30_belter_bomber-mat91.1-0 ; 
       bom30_belter_bomber-mat92.1-0 ; 
       bom30_belter_bomber-mat93.2-0 ; 
       bom30_belter_bomber-mat94.1-0 ; 
       bom30_belter_bomber-mat95.1-0 ; 
       bom30_belter_bomber-mat96.1-0 ; 
       bom30_belter_bomber-mat97.1-0 ; 
       bom30_belter_bomber-mat99.1-0 ; 
       bom30_belter_bomber-top1.3-0 ; 
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       bel_fighter-bsmoke.2-0 ; 
       bel_fighter-bthrust.1-0 ; 
       bel_fighter-cockpt.2-0 ; 
       bel_fighter-cube10.1-0 ; 
       bel_fighter-cube2_1.1-0 ; 
       bel_fighter-cube2_1_1.3-0 ; 
       bel_fighter-cube22.1-0 ; 
       bel_fighter-cube8.1-0 ; 
       bel_fighter-cyl10.1-0 ; 
       bel_fighter-cyl11.1-0 ; 
       bel_fighter-cyl12.1-0 ; 
       bel_fighter-cyl3.1-0 ; 
       bel_fighter-cyl3_1.1-0 ; 
       bel_fighter-cyl4.1-0 ; 
       bel_fighter-cyl4_1.4-0 ; 
       bel_fighter-cyl5.1-0 ; 
       bel_fighter-cyl9.1-0 ; 
       bel_fighter-lwepemt.2-0 ; 
       bel_fighter-missemt.2-0 ; 
       bel_fighter-null1.1-0 ; 
       bel_fighter-null2.37-0 ROOT ; 
       bel_fighter-rthrust.2-0 ; 
       bel_fighter-rwepemt.2-0 ; 
       bel_fighter-sphere2.1-0 ; 
       bel_fighter-sphere3.1-0 ; 
       bel_fighter-SS01.2-0 ; 
       bel_fighter-SS02.2-0 ; 
       bel_fighter-SS03.2-0 ; 
       bel_fighter-SS04.2-0 ; 
       bel_fighter-SS05.2-0 ; 
       bel_fighter-SS06.2-0 ; 
       bel_fighter-trail.2-0 ; 
       bel_fighter-tsmoke.1-0 ; 
       bel_fighter-tthrust.1-0 ; 
       bel_fighter-turwepemt1.1-0 ; 
       bel_fighter-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Bomber/bom30/PICTURES/bom30 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-bom30-belter_bomber.40-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       bom30_belter_bomber-t2d1.4-0 ; 
       bom30_belter_bomber-t2d10.1-0 ; 
       bom30_belter_bomber-t2d11.3-0 ; 
       bom30_belter_bomber-t2d12.3-0 ; 
       bom30_belter_bomber-t2d13.1-0 ; 
       bom30_belter_bomber-t2d14.1-0 ; 
       bom30_belter_bomber-t2d15.4-0 ; 
       bom30_belter_bomber-t2d16.3-0 ; 
       bom30_belter_bomber-t2d17.2-0 ; 
       bom30_belter_bomber-t2d18.2-0 ; 
       bom30_belter_bomber-t2d19.1-0 ; 
       bom30_belter_bomber-t2d2.4-0 ; 
       bom30_belter_bomber-t2d20.1-0 ; 
       bom30_belter_bomber-t2d21.1-0 ; 
       bom30_belter_bomber-t2d22.1-0 ; 
       bom30_belter_bomber-t2d23.1-0 ; 
       bom30_belter_bomber-t2d24.1-0 ; 
       bom30_belter_bomber-t2d25.1-0 ; 
       bom30_belter_bomber-t2d26.1-0 ; 
       bom30_belter_bomber-t2d3.4-0 ; 
       bom30_belter_bomber-t2d4.10-0 ; 
       bom30_belter_bomber-t2d5.9-0 ; 
       bom30_belter_bomber-t2d6.9-0 ; 
       bom30_belter_bomber-t2d7.3-0 ; 
       bom30_belter_bomber-t2d8.3-0 ; 
       bom30_belter_bomber-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 20 110 ; 
       2 20 110 ; 
       3 5 110 ; 
       4 14 110 ; 
       5 20 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       11 19 110 ; 
       12 14 110 ; 
       13 11 110 ; 
       14 5 110 ; 
       15 11 110 ; 
       8 19 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       16 3 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 7 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 5 110 ; 
       24 14 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 14 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 6 300 ; 
       6 17 300 ; 
       7 24 300 ; 
       11 1 300 ; 
       12 18 300 ; 
       13 23 300 ; 
       14 10 300 ; 
       14 25 300 ; 
       14 0 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       15 2 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       10 5 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       23 21 300 ; 
       24 22 300 ; 
       25 26 300 ; 
       26 27 300 ; 
       27 29 300 ; 
       28 28 300 ; 
       29 31 300 ; 
       30 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 22 401 ; 
       7 0 401 ; 
       8 11 401 ; 
       9 19 401 ; 
       10 20 401 ; 
       11 23 401 ; 
       12 24 401 ; 
       13 25 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       20 7 401 ; 
       25 21 401 ; 
       21 8 401 ; 
       22 9 401 ; 
       23 14 401 ; 
       1 12 401 ; 
       24 10 401 ; 
       2 13 401 ; 
       3 15 401 ; 
       4 16 401 ; 
       5 17 401 ; 
       6 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 78.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 100 -4 0 MPRFLG 0 ; 
       7 SCHEM 80 -4 0 MPRFLG 0 ; 
       11 SCHEM 75 -8 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 75 -10 0 MPRFLG 0 ; 
       14 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 80 -10 0 MPRFLG 0 ; 
       10 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 56.25 0 0 SRT 1 1 1 0 0 0 -8.599935e-009 -6.029957e-009 -4.524091e-008 MPRFLG 0 ; 
       21 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
