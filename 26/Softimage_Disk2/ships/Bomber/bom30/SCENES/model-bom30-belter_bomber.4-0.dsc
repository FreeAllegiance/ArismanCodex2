SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       bel_fighter-bsmoke.2-0 ; 
       bel_fighter-bthrust.2-0 ; 
       bel_fighter-cockpt.2-0 ; 
       bel_fighter-cube2_1.1-0 ; 
       bel_fighter-cube2_1_1.3-0 ; 
       bel_fighter-cube5.4-0 ; 
       bel_fighter-cube8.1-0 ; 
       bel_fighter-cyl1.1-0 ; 
       bel_fighter-cyl2.1-0 ; 
       bel_fighter-cyl3.1-0 ; 
       bel_fighter-cyl3_1.1-0 ; 
       bel_fighter-cyl4.1-0 ; 
       bel_fighter-cyl4_1.4-0 ; 
       bel_fighter-cyl5.1-0 ; 
       bel_fighter-cyl6.1-0 ; 
       bel_fighter-cyl7.1-0 ; 
       bel_fighter-cyl8.1-0 ; 
       bel_fighter-lwepemt.2-0 ; 
       bel_fighter-missemt.2-0 ; 
       bel_fighter-msmoke.1-0 ; 
       bel_fighter-mthrust.1-0 ; 
       bel_fighter-null1.1-0 ; 
       bel_fighter-null2.3-0 ROOT ; 
       bel_fighter-rwepemt.2-0 ; 
       bel_fighter-sphere2.1-0 ; 
       bel_fighter-sphere3.1-0 ; 
       bel_fighter-SS01.2-0 ; 
       bel_fighter-SS02.2-0 ; 
       bel_fighter-SS03.2-0 ; 
       bel_fighter-SS04.2-0 ; 
       bel_fighter-SS05.2-0 ; 
       bel_fighter-SS06.2-0 ; 
       bel_fighter-SS07.2-0 ; 
       bel_fighter-SS08.2-0 ; 
       bel_fighter-trail.2-0 ; 
       bel_fighter-tsmoke.2-0 ; 
       bel_fighter-tthrust.2-0 ; 
       bel_fighter-turwepemt1.1-0 ; 
       bel_fighter-turwepemt2.1-0 ; 
       bom30_belter_bomber-175deg1.1-0 ROOT ; 
       turcone-180deg.1-0 ROOT ; 
       turcone1-175deg.1-0 ROOT ; 
       turcone2-175deg.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bom30-belter_bomber.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 21 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       6 4 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       25 12 110 ; 
       24 4 110 ; 
       37 22 110 ; 
       9 21 110 ; 
       11 9 110 ; 
       13 9 110 ; 
       38 22 110 ; 
       21 6 110 ; 
       3 12 110 ; 
       4 22 110 ; 
       5 4 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       10 12 110 ; 
       12 4 110 ; 
       0 22 110 ; 
       1 22 110 ; 
       2 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       23 22 110 ; 
       26 22 110 ; 
       27 22 110 ; 
       28 22 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 22 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 22 110 ; 
       35 22 110 ; 
       36 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       26 0 300 ; 
       27 1 300 ; 
       28 3 300 ; 
       29 2 300 ; 
       30 5 300 ; 
       31 4 300 ; 
       32 6 300 ; 
       33 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 211.2609 14.9956 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 211.2609 12.9956 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       14 SCHEM 185.6009 4.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 184.3509 2.047998 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 186.8509 2.047998 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 183.1009 8.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 155.2892 -11.31637 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 155.2962 -12.22702 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 177.0385 6.0038 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 189.3509 8.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 150.8575 7.049841 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 180.6009 4.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 181.8509 2.047998 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 179.3509 2.047998 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 150.8094 6.275111 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.135644 -1.534796 0 0 0 0.6516752 1.704974 MPRFLG 0 ; 
       41 SCHEM 153.3041 6.283732 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.135644 -1.722796 1.643016e-008 3.141593 0 -0.8519825 -2.413624 MPRFLG 0 ; 
       42 SCHEM 211.6187 11.57858 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.135644 -1.534796 0 1.570796 -2.013724 0.01155111 1.704974 MPRFLG 0 ; 
       39 SCHEM 214.1187 11.57858 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.135644 -1.606796 0 1.570796 1.24024 0.01155111 1.704974 MPRFLG 0 ; 
       38 SCHEM 153.1379 7.0483 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 183.1009 6.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 174.3509 6.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 180.6009 10.048 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 189.7457 6.460707 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 188.4957 4.460707 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 190.9957 4.460707 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 171.8509 6.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 174.3509 8.047997 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 163.8787 11.49611 0 USR DISPLAY 3 2 SRT 0.9999999 0.9999999 0.9999999 -5.679913e-008 -1.173386e-016 -4.996004e-016 -8.599935e-009 -6.029957e-009 -4.524091e-008 MPRFLG 0 ; 
       0 SCHEM 153.1017 -11.29878 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 153.0613 -12.15997 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 161.4217 -10.93072 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 163.9753 -12.04738 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 161.4753 -12.04738 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 158.9753 -12.04738 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 168.282 -9.451464 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 168.3447 -10.21763 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 170.7193 -9.398626 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 170.6763 -10.24405 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 173.14 -9.504305 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 173.183 -10.21763 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 175.7529 -9.455687 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 175.4465 -10.2377 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 152.9665 -9.796137 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 150.8113 -11.32584 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 150.8049 -12.22322 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 168.282 -11.45146 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 168.3447 -12.21763 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 170.6763 -12.24405 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 170.7193 -11.39863 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 173.183 -12.21763 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 173.14 -11.5043 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 175.7529 -11.45569 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 175.4465 -12.2377 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
