SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       bel_fighter-bsmoke.2-0 ; 
       bel_fighter-bthrust.1-0 ; 
       bel_fighter-cockpt.2-0 ; 
       bel_fighter-cube10.1-0 ; 
       bel_fighter-cube2_1.1-0 ; 
       bel_fighter-cube2_1_1.3-0 ; 
       bel_fighter-cube22.1-0 ; 
       bel_fighter-cube8.1-0 ; 
       bel_fighter-cyl3.1-0 ; 
       bel_fighter-cyl3_1.1-0 ; 
       bel_fighter-cyl4.1-0 ; 
       bel_fighter-cyl4_1.4-0 ; 
       bel_fighter-cyl5.1-0 ; 
       bel_fighter-cyl6.1-0 ; 
       bel_fighter-cyl7.1-0 ; 
       bel_fighter-cyl8.1-0 ; 
       bel_fighter-cyl9.1-0 ; 
       bel_fighter-lwepemt.2-0 ; 
       bel_fighter-missemt.2-0 ; 
       bel_fighter-null1.1-0 ; 
       bel_fighter-null2.6-0 ROOT ; 
       bel_fighter-rthrust.2-0 ; 
       bel_fighter-rwepemt.2-0 ; 
       bel_fighter-sphere2.1-0 ; 
       bel_fighter-sphere3.1-0 ; 
       bel_fighter-SS01.2-0 ; 
       bel_fighter-SS02.2-0 ; 
       bel_fighter-SS03.2-0 ; 
       bel_fighter-SS04.2-0 ; 
       bel_fighter-SS05.2-0 ; 
       bel_fighter-SS06.2-0 ; 
       bel_fighter-trail.2-0 ; 
       bel_fighter-tsmoke.1-0 ; 
       bel_fighter-tthrust.1-0 ; 
       bel_fighter-turwepemt1.1-0 ; 
       bel_fighter-turwepemt2.1-0 ; 
       turcone-180deg.4-0 ROOT ; 
       turcone1-175deg.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bom30-belter_bomber.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 20 110 ; 
       6 5 110 ; 
       3 5 110 ; 
       4 11 110 ; 
       5 20 110 ; 
       7 5 110 ; 
       8 19 110 ; 
       9 11 110 ; 
       10 8 110 ; 
       11 5 110 ; 
       12 8 110 ; 
       13 19 110 ; 
       1 20 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 3 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 7 110 ; 
       0 20 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 5 110 ; 
       24 11 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 0 300 ; 
       26 1 300 ; 
       27 3 300 ; 
       28 2 300 ; 
       29 5 300 ; 
       30 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 211.2609 14.9956 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 211.2609 12.9956 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 227.0229 -9.989369 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 263.6702 3.491648 0 USR MPRFLG 0 ; 
       3 SCHEM 257.9565 1.975718 0 USR MPRFLG 0 ; 
       4 SCHEM 243.5998 1.811921 0 MPRFLG 0 ; 
       5 SCHEM 249.8499 5.811925 0 USR MPRFLG 0 ; 
       7 SCHEM 252.3499 3.811921 0 MPRFLG 0 ; 
       8 SCHEM 249.8499 -0.1880789 0 MPRFLG 0 ; 
       9 SCHEM 241.0998 1.811921 0 MPRFLG 0 ; 
       10 SCHEM 251.0999 -2.188078 0 MPRFLG 0 ; 
       11 SCHEM 243.5998 3.811921 0 MPRFLG 0 ; 
       12 SCHEM 248.5998 -2.188078 0 MPRFLG 0 ; 
       13 SCHEM 254.8499 -0.1880789 0 MPRFLG 0 ; 
       1 SCHEM 222.3654 -11.27506 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 253.5999 -2.188078 0 MPRFLG 0 ; 
       15 SCHEM 256.0999 -2.188078 0 MPRFLG 0 ; 
       16 SCHEM 257.9565 -0.02428299 0 MPRFLG 0 ; 
       32 SCHEM 220.228 -10.37502 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 220.235 -11.28567 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 229.5765 -11.10603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 227.0765 -11.10603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 252.3499 1.811921 0 MPRFLG 0 ; 
       20 SCHEM 229.4799 12.43746 0 USR SRT 0.9999999 0.9999999 0.9999999 -5.679913e-008 -1.173386e-016 -4.996004e-016 -8.599935e-009 -6.029957e-009 -4.524091e-008 MPRFLG 0 ; 
       0 SCHEM 222.3533 -10.34398 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 216.4061 -11.28187 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 224.5765 -11.10603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 258.5999 3.811921 0 MPRFLG 0 ; 
       24 SCHEM 246.2874 1.767724 0 USR MPRFLG 0 ; 
       25 SCHEM 233.8832 -8.510113 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 233.9459 -9.276279 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 236.3204 -8.457275 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 236.2775 -9.302699 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 238.7411 -8.562954 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 238.7842 -9.276279 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 218.5677 -8.854786 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 216.4587 7.991191 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 218.7391 7.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 226.2948 17.45365 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.146848 -1.534796 0 0 0 0.7706879 1.649074 MPRFLG 0 ; 
       37 SCHEM 228.7894 17.46227 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1.135644 1.135644 1.018497 -1.784796 3.661347e-008 3.141593 0 -0.9312903 -2.413624 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 259.535 -11.45146 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 259.5977 -12.21763 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 261.9293 -12.24405 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 261.9723 -11.39863 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 264.436 -12.21763 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 264.3929 -11.5043 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
