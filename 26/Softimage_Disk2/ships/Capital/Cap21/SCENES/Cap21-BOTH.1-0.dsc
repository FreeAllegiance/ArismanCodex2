SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.8-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       BOTH-crystal1.1-0 ; 
       BOTH-mat1.1-0 ; 
       BOTH-mat10.1-0 ; 
       BOTH-mat11.1-0 ; 
       BOTH-mat12.1-0 ; 
       BOTH-mat13.1-0 ; 
       BOTH-mat14.1-0 ; 
       BOTH-mat15.1-0 ; 
       BOTH-mat16.1-0 ; 
       BOTH-mat17.1-0 ; 
       BOTH-mat18.1-0 ; 
       BOTH-mat19.1-0 ; 
       BOTH-mat2.1-0 ; 
       BOTH-mat3.1-0 ; 
       BOTH-mat4.1-0 ; 
       BOTH-mat5.1-0 ; 
       BOTH-mat6.1-0 ; 
       BOTH-mat7.1-0 ; 
       BOTH-mat8.1-0 ; 
       BOTH-mat9.1-0 ; 
       STATIC-crystal1.3-0 ; 
       STATIC-mat1.3-0 ; 
       STATIC-mat12.3-0 ; 
       STATIC-mat13.3-0 ; 
       STATIC-mat14.3-0 ; 
       STATIC-mat15.3-0 ; 
       STATIC-mat16.3-0 ; 
       STATIC-mat17.3-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.3-0 ; 
       STATIC-mat3.3-0 ; 
       STATIC-mat4.3-0 ; 
       STATIC-mat5.3-0 ; 
       STATIC-mat6.3-0 ; 
       STATIC-mat7.3-0 ; 
       STATIC-mat8.3-0 ; 
       STATIC-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       BOTH-cargo_unit1.1-0 ; 
       BOTH-cargo_unit2.2-0 ; 
       BOTH-cargo_unit2_1.2-0 ; 
       BOTH-cargo_unit3.2-0 ; 
       BOTH-cargo_unit3_1.2-0 ; 
       BOTH-cargo_unit4.2-0 ; 
       BOTH-cargo_unit4_1.2-0 ; 
       BOTH-cone1.1-0 ; 
       BOTH-cone1_1.1-0 ; 
       BOTH-cone2.1-0 ; 
       BOTH-cone2_1.1-0 ; 
       BOTH-lwing.3-0 ; 
       BOTH-lwing_1.3-0 ; 
       BOTH-lwing1.1-0 ; 
       BOTH-lwing1_1.1-0 ; 
       BOTH-missemt1.1-0 ; 
       BOTH-root.1-0 ROOT ; 
       BOTH-root_1.1-0 ROOT ; 
       BOTH-thrust.1-0 ; 
       BOTH-top_window.2-0 ; 
       BOTH-top_window_1.2-0 ; 
       BOTH-top_window1.1-0 ; 
       BOTH-top_window1_1.1-0 ; 
       BOTH-trail.1-0 ; 
       BOTH-turwepemt1.1-0 ; 
       BOTH-turwepemt2.1-0 ; 
       BOTH-turwepemt3.1-0 ; 
       BOTH-turwepemt4.1-0 ; 
       BOTH-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       BOTH-t2d1.1-0 ; 
       BOTH-t2d10.1-0 ; 
       BOTH-t2d11.1-0 ; 
       BOTH-t2d12.1-0 ; 
       BOTH-t2d13.1-0 ; 
       BOTH-t2d14.1-0 ; 
       BOTH-t2d15.1-0 ; 
       BOTH-t2d16.1-0 ; 
       BOTH-t2d17.1-0 ; 
       BOTH-t2d18.1-0 ; 
       BOTH-t2d19.1-0 ; 
       BOTH-t2d2.1-0 ; 
       BOTH-t2d20.1-0 ; 
       BOTH-t2d3.1-0 ; 
       BOTH-t2d4.1-0 ; 
       BOTH-t2d5.1-0 ; 
       BOTH-t2d6.1-0 ; 
       BOTH-t2d7.1-0 ; 
       BOTH-t2d8.1-0 ; 
       BOTH-t2d9.1-0 ; 
       STATIC-t2d1.3-0 ; 
       STATIC-t2d12.3-0 ; 
       STATIC-t2d13.3-0 ; 
       STATIC-t2d14.3-0 ; 
       STATIC-t2d15.3-0 ; 
       STATIC-t2d16.3-0 ; 
       STATIC-t2d17.3-0 ; 
       STATIC-t2d18.4-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.3-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d3.3-0 ; 
       STATIC-t2d4.3-0 ; 
       STATIC-t2d5.3-0 ; 
       STATIC-t2d6.3-0 ; 
       STATIC-t2d7.4-0 ; 
       STATIC-t2d8.4-0 ; 
       STATIC-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       3 16 110 ; 
       5 16 110 ; 
       7 16 110 ; 
       9 16 110 ; 
       11 16 110 ; 
       13 16 110 ; 
       15 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       21 16 110 ; 
       23 16 110 ; 
       24 7 110 ; 
       25 9 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       2 17 110 ; 
       4 17 110 ; 
       6 17 110 ; 
       8 17 110 ; 
       10 17 110 ; 
       12 17 110 ; 
       14 17 110 ; 
       20 17 110 ; 
       22 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       7 10 300 ; 
       9 11 300 ; 
       11 1 300 ; 
       11 12 300 ; 
       13 13 300 ; 
       13 14 300 ; 
       16 17 300 ; 
       16 0 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       19 15 300 ; 
       21 16 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       8 28 300 ; 
       10 29 300 ; 
       12 21 300 ; 
       12 30 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       17 35 300 ; 
       17 20 300 ; 
       17 36 300 ; 
       17 37 300 ; 
       20 33 300 ; 
       22 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       3 5 400 ; 
       5 7 400 ; 
       7 10 400 ; 
       9 12 400 ; 
       2 21 400 ; 
       4 23 400 ; 
       6 25 400 ; 
       8 28 400 ; 
       10 30 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 17 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       12 11 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 9 401 ; 
       20 35 401 ; 
       21 20 401 ; 
       23 22 401 ; 
       25 24 401 ; 
       27 26 401 ; 
       30 29 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 36 401 ; 
       36 37 401 ; 
       37 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 46.28824 -2 0 MPRFLG 0 ; 
       1 SCHEM 43.11097 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 35.61097 -4.67391 0 USR MPRFLG 0 ; 
       9 SCHEM 38.78824 -4.923895 0 USR MPRFLG 0 ; 
       11 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 45.24396 -2 0 MPRFLG 0 ; 
       15 SCHEM 54.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 43.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 49.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 46.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35.7017 -7.68536 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 38.89563 -7.935347 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.49396 -7.675736 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44.36525 -7.709874 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 51.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 97.65349 -2 0 MPRFLG 0 ; 
       4 SCHEM 83.86525 -2 0 MPRFLG 0 ; 
       6 SCHEM 73.86525 -2 0 MPRFLG 0 ; 
       8 SCHEM 88.22622 -4.67391 0 USR MPRFLG 0 ; 
       10 SCHEM 91.40349 -4.923895 0 USR MPRFLG 0 ; 
       12 SCHEM 90.11525 -2 0 MPRFLG 0 ; 
       14 SCHEM 103.9035 -2 0 MPRFLG 0 ; 
       17 SCHEM 96.36525 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 68.86525 -2 0 MPRFLG 0 ; 
       22 SCHEM 78.86525 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 48.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 43.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45.61097 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40.61097 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 38.2017 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.39563 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.49396 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 43.99396 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 64.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 61.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 107.6535 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 91.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 100.1535 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95.15349 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 86.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 81.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 76.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 71.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 86.97622 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 90.15349 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 88.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 105.1535 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 102.6535 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 68.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 78.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 115.1535 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 110.1535 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 112.6535 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.28824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 43.78824 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 43.11097 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40.61097 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 40.7017 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 43.89563 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.49396 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 43.99396 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 91.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 97.65349 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 95.15349 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 83.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 81.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 73.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 71.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 112.6535 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 89.47622 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 88.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 92.65349 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 105.1535 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 102.6535 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 68.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 78.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 107.6535 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 115.1535 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 110.1535 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
