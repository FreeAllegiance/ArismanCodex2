SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 6     
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.9-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       BOTH-crystal1.1-0 ; 
       BOTH-mat1.1-0 ; 
       BOTH-mat10.1-0 ; 
       BOTH-mat107.1-0 ; 
       BOTH-mat108.1-0 ; 
       BOTH-mat109.1-0 ; 
       BOTH-mat11.1-0 ; 
       BOTH-mat110.1-0 ; 
       BOTH-mat112.1-0 ; 
       BOTH-mat113.1-0 ; 
       BOTH-mat114.1-0 ; 
       BOTH-mat115.1-0 ; 
       BOTH-mat116.1-0 ; 
       BOTH-mat117.1-0 ; 
       BOTH-mat118.1-0 ; 
       BOTH-mat119.1-0 ; 
       BOTH-mat12.1-0 ; 
       BOTH-mat13.1-0 ; 
       BOTH-mat14.1-0 ; 
       BOTH-mat15.1-0 ; 
       BOTH-mat16.1-0 ; 
       BOTH-mat17.1-0 ; 
       BOTH-mat18.1-0 ; 
       BOTH-mat19.1-0 ; 
       BOTH-mat2.1-0 ; 
       BOTH-mat3.1-0 ; 
       BOTH-mat4.1-0 ; 
       BOTH-mat5.1-0 ; 
       BOTH-mat6.1-0 ; 
       BOTH-mat7.1-0 ; 
       BOTH-mat8.1-0 ; 
       BOTH-mat84.1-0 ; 
       BOTH-mat85.1-0 ; 
       BOTH-mat86.1-0 ; 
       BOTH-mat87.1-0 ; 
       BOTH-mat9.1-0 ; 
       STATIC-crystal1.3-0 ; 
       STATIC-mat1.3-0 ; 
       STATIC-mat12.3-0 ; 
       STATIC-mat13.3-0 ; 
       STATIC-mat14.3-0 ; 
       STATIC-mat15.3-0 ; 
       STATIC-mat16.3-0 ; 
       STATIC-mat17.3-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.3-0 ; 
       STATIC-mat3.3-0 ; 
       STATIC-mat4.3-0 ; 
       STATIC-mat5.3-0 ; 
       STATIC-mat6.3-0 ; 
       STATIC-mat7.3-0 ; 
       STATIC-mat8.3-0 ; 
       STATIC-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       BOTH-cargo_unit1.1-0 ; 
       BOTH-cargo_unit2.2-0 ; 
       BOTH-cargo_unit2_1.2-0 ; 
       BOTH-cargo_unit3.2-0 ; 
       BOTH-cargo_unit3_1.2-0 ; 
       BOTH-cargo_unit4.2-0 ; 
       BOTH-cargo_unit4_1.2-0 ; 
       BOTH-cone1.1-0 ; 
       BOTH-cone1_1.1-0 ; 
       BOTH-cone2.1-0 ; 
       BOTH-cone2_1.1-0 ; 
       BOTH-lwing.3-0 ; 
       BOTH-lwing_1.3-0 ; 
       BOTH-lwing1.1-0 ; 
       BOTH-lwing1_1.1-0 ; 
       BOTH-missemt1.1-0 ; 
       BOTH-root.2-0 ROOT ; 
       BOTH-root_1.2-0 ROOT ; 
       BOTH-SS0a.2-0 ; 
       BOTH-SSb4.1-0 ; 
       BOTH-SSbl1.1-0 ; 
       BOTH-SSbl2.1-0 ; 
       BOTH-SSbl3.1-0 ; 
       BOTH-SSbl4.1-0 ; 
       BOTH-SSbr1.1-0 ; 
       BOTH-SSbr2.1-0 ; 
       BOTH-SSbr3.1-0 ; 
       BOTH-SStl1.1-0 ; 
       BOTH-SStl2.1-0 ; 
       BOTH-SStl3.1-0 ; 
       BOTH-SStl4.1-0 ; 
       BOTH-SStr1.1-0 ; 
       BOTH-SStr2.1-0 ; 
       BOTH-SStr3.1-0 ; 
       BOTH-SStr4.1-0 ; 
       BOTH-thrust.1-0 ; 
       BOTH-top_window.2-0 ; 
       BOTH-top_window_1.2-0 ; 
       BOTH-top_window1.1-0 ; 
       BOTH-top_window1_1.1-0 ; 
       BOTH-trail.1-0 ; 
       BOTH-turwepemt1.1-0 ; 
       BOTH-turwepemt2.1-0 ; 
       BOTH-turwepemt3.1-0 ; 
       BOTH-turwepemt4.1-0 ; 
       BOTH-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-BOTH.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       BOTH-t2d1.1-0 ; 
       BOTH-t2d10.1-0 ; 
       BOTH-t2d11.1-0 ; 
       BOTH-t2d12.1-0 ; 
       BOTH-t2d13.1-0 ; 
       BOTH-t2d14.1-0 ; 
       BOTH-t2d15.1-0 ; 
       BOTH-t2d16.1-0 ; 
       BOTH-t2d17.1-0 ; 
       BOTH-t2d18.1-0 ; 
       BOTH-t2d19.1-0 ; 
       BOTH-t2d2.1-0 ; 
       BOTH-t2d20.1-0 ; 
       BOTH-t2d3.1-0 ; 
       BOTH-t2d4.1-0 ; 
       BOTH-t2d5.1-0 ; 
       BOTH-t2d6.1-0 ; 
       BOTH-t2d7.1-0 ; 
       BOTH-t2d8.1-0 ; 
       BOTH-t2d9.1-0 ; 
       STATIC-t2d1.3-0 ; 
       STATIC-t2d12.3-0 ; 
       STATIC-t2d13.3-0 ; 
       STATIC-t2d14.3-0 ; 
       STATIC-t2d15.3-0 ; 
       STATIC-t2d16.3-0 ; 
       STATIC-t2d17.3-0 ; 
       STATIC-t2d18.4-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.3-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d3.3-0 ; 
       STATIC-t2d4.3-0 ; 
       STATIC-t2d5.3-0 ; 
       STATIC-t2d6.3-0 ; 
       STATIC-t2d7.4-0 ; 
       STATIC-t2d8.4-0 ; 
       STATIC-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       2 17 110 ; 
       3 16 110 ; 
       4 17 110 ; 
       5 16 110 ; 
       6 17 110 ; 
       7 16 110 ; 
       8 17 110 ; 
       9 16 110 ; 
       10 17 110 ; 
       11 16 110 ; 
       12 17 110 ; 
       13 16 110 ; 
       14 17 110 ; 
       15 16 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 17 110 ; 
       38 16 110 ; 
       39 17 110 ; 
       40 16 110 ; 
       41 7 110 ; 
       42 9 110 ; 
       43 16 110 ; 
       44 16 110 ; 
       45 16 110 ; 
       18 16 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
       29 18 110 ; 
       30 18 110 ; 
       31 18 110 ; 
       32 18 110 ; 
       33 18 110 ; 
       34 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 6 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       4 40 300 ; 
       4 41 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       7 22 300 ; 
       8 44 300 ; 
       9 23 300 ; 
       10 45 300 ; 
       11 1 300 ; 
       11 24 300 ; 
       12 37 300 ; 
       12 46 300 ; 
       13 25 300 ; 
       13 26 300 ; 
       14 47 300 ; 
       14 48 300 ; 
       16 29 300 ; 
       16 0 300 ; 
       16 30 300 ; 
       16 35 300 ; 
       17 51 300 ; 
       17 36 300 ; 
       17 52 300 ; 
       17 53 300 ; 
       36 27 300 ; 
       37 49 300 ; 
       38 28 300 ; 
       39 50 300 ; 
       19 7 300 ; 
       20 15 300 ; 
       21 14 300 ; 
       22 13 300 ; 
       23 12 300 ; 
       24 3 300 ; 
       25 4 300 ; 
       26 5 300 ; 
       27 8 300 ; 
       28 9 300 ; 
       29 10 300 ; 
       30 11 300 ; 
       31 31 300 ; 
       32 32 300 ; 
       33 33 300 ; 
       34 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 21 400 ; 
       3 5 400 ; 
       4 23 400 ; 
       5 7 400 ; 
       6 25 400 ; 
       7 10 400 ; 
       8 28 400 ; 
       9 12 400 ; 
       10 30 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 17 401 ; 
       1 0 401 ; 
       6 2 401 ; 
       17 4 401 ; 
       19 6 401 ; 
       21 8 401 ; 
       24 11 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       28 16 401 ; 
       29 18 401 ; 
       30 19 401 ; 
       35 9 401 ; 
       36 35 401 ; 
       37 20 401 ; 
       39 22 401 ; 
       41 24 401 ; 
       43 26 401 ; 
       46 29 401 ; 
       47 31 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 36 401 ; 
       52 37 401 ; 
       53 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.99459 -0.9608381 0 MPRFLG 0 ; 
       1 SCHEM 27.99459 -0.9608381 0 MPRFLG 0 ; 
       2 SCHEM 60.72867 -2.296903 0 MPRFLG 0 ; 
       3 SCHEM 13.62933 -0.9608381 0 MPRFLG 0 ; 
       4 SCHEM 58.1904 -2.296903 0 MPRFLG 0 ; 
       5 SCHEM 8.629333 -0.9608381 0 MPRFLG 0 ; 
       6 SCHEM 53.1904 -2.296903 0 MPRFLG 0 ; 
       7 SCHEM 16.7403 -3.634748 0 USR MPRFLG 0 ; 
       8 SCHEM 52.5514 -4.970813 0 USR MPRFLG 0 ; 
       9 SCHEM 19.91757 -3.884732 0 USR MPRFLG 0 ; 
       10 SCHEM 55.72867 -5.220798 0 USR MPRFLG 0 ; 
       11 SCHEM 16.12934 -0.9608381 0 MPRFLG 0 ; 
       12 SCHEM 58.22867 -2.296903 0 MPRFLG 0 ; 
       13 SCHEM 30.49459 -0.9608381 0 MPRFLG 0 ; 
       14 SCHEM 63.22867 -2.296903 0 MPRFLG 0 ; 
       15 SCHEM 42.99459 -0.9608381 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24.87934 1.039162 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 60.6904 -0.2969036 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 37.99459 -0.9608381 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.129335 -0.9608381 0 MPRFLG 0 ; 
       37 SCHEM 50.6904 -2.296903 0 MPRFLG 0 ; 
       38 SCHEM 11.12933 -0.9608381 0 MPRFLG 0 ; 
       39 SCHEM 55.6904 -2.296903 0 MPRFLG 0 ; 
       40 SCHEM 35.49459 -0.9608381 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.83103 -6.646198 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 20.02497 -6.896184 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 22.62329 -6.636573 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 25.49459 -6.670712 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 40.49459 -0.9608381 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 28.82104 -7.719488 0 USR MPRFLG 0 ; 
       19 SCHEM 30.07103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 12.57104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 15.07104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 17.57105 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 20.07104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 22.57104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 25.07104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 27.57104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 10.07104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 32.57103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 35.07103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 37.57104 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 40.07103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 42.57103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 45.07103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 47.57103 -9.719488 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 80.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 68.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 68.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 63.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 63.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 54.70169 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.89563 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 80.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 80.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 80.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 95.90352 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 88.40352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 90.90352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 90.90352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 88.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 88.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 83.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 83.36525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 82.72624 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 85.90352 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 88.40352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 93.40352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 93.40352 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 80.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 85.86525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 95.90352 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 95.90352 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 95.90352 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 68.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 68.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 63.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 63.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54.70169 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 57.89563 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 65.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 65.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 80.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 80.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 88.40352 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 90.90352 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 90.90352 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 88.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 88.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 83.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 83.36525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 95.90352 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 82.72624 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 88.40352 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85.90352 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 93.40352 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 93.40352 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 80.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 85.86525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 95.90352 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 95.90352 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 95.90352 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 394.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 397 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 399.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
