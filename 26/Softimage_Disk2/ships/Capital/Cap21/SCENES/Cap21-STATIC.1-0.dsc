SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       static-crystal1.1-0 ; 
       static-mat1.1-0 ; 
       static-mat10.1-0 ; 
       static-mat11.1-0 ; 
       static-mat12.1-0 ; 
       static-mat13.1-0 ; 
       static-mat14.1-0 ; 
       static-mat15.1-0 ; 
       static-mat16.1-0 ; 
       static-mat17.1-0 ; 
       static-mat2.1-0 ; 
       static-mat3.1-0 ; 
       static-mat4.1-0 ; 
       static-mat5.1-0 ; 
       static-mat6.1-0 ; 
       static-mat7.1-0 ; 
       static-mat8.1-0 ; 
       static-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-root.25-0 ROOT ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       static-t2d1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d3.1-0 ; 
       static-t2d4.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       7 6 110 ; 
       8 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       4 1 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       6 15 300 ; 
       6 0 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 13 300 ; 
       8 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 5 400 ; 
       3 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -62.90636 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -62.90636 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -45.40636 -2 0 MPRFLG 0 ; 
       1 SCHEM -47.90636 -2 0 MPRFLG 0 ; 
       2 SCHEM -52.90636 -2 0 MPRFLG 0 ; 
       3 SCHEM -57.90636 -2 0 MPRFLG 0 ; 
       4 SCHEM -50.40636 -2 0 MPRFLG 0 ; 
       5 SCHEM -42.90636 -2 0 MPRFLG 0 ; 
       6 SCHEM -41.65636 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 13.54891 MPRFLG 0 ; 
       7 SCHEM -60.40636 -2 0 MPRFLG 0 ; 
       8 SCHEM -55.40636 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -61.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -56.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -46.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -48.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -53.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -58.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -61.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -56.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
