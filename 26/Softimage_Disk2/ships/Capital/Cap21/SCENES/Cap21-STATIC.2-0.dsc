SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.38-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       STATIC-crystal1.1-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-mat8.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-root.18-0 ROOT ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d6.1-0 ; 
       STATIC-t2d7.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       4 1 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       6 15 300 ; 
       6 0 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 13 300 ; 
       8 14 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 5 400 ; 
       3 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
