SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.7-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       STATIC-crystal1.3-0 ; 
       STATIC-mat1.3-0 ; 
       STATIC-mat12.3-0 ; 
       STATIC-mat13.3-0 ; 
       STATIC-mat14.3-0 ; 
       STATIC-mat15.3-0 ; 
       STATIC-mat16.3-0 ; 
       STATIC-mat17.3-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat2.3-0 ; 
       STATIC-mat3.3-0 ; 
       STATIC-mat4.3-0 ; 
       STATIC-mat5.3-0 ; 
       STATIC-mat6.3-0 ; 
       STATIC-mat7.3-0 ; 
       STATIC-mat8.3-0 ; 
       STATIC-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-cone1.1-0 ; 
       cap21-cone2.1-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-root.28-0 ROOT ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-STATIC.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       STATIC-t2d1.3-0 ; 
       STATIC-t2d12.3-0 ; 
       STATIC-t2d13.3-0 ; 
       STATIC-t2d14.3-0 ; 
       STATIC-t2d15.3-0 ; 
       STATIC-t2d16.3-0 ; 
       STATIC-t2d17.3-0 ; 
       STATIC-t2d18.3-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.3-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d3.3-0 ; 
       STATIC-t2d4.3-0 ; 
       STATIC-t2d5.3-0 ; 
       STATIC-t2d6.3-0 ; 
       STATIC-t2d7.3-0 ; 
       STATIC-t2d8.3-0 ; 
       STATIC-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       4 9 300 ; 
       5 1 300 ; 
       5 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       7 15 300 ; 
       7 0 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       8 13 300 ; 
       9 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 5 400 ; 
       3 8 400 ; 
       4 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 31.28824 -2 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 21.86097 -4.67391 0 USR MPRFLG 0 ; 
       4 SCHEM 25.03824 -4.923895 0 USR MPRFLG 0 ; 
       5 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 37.53824 -2 0 MPRFLG 0 ; 
       7 SCHEM 30 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 1.513667 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.28824 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 33.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 28.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20.61097 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 23.78824 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 38.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.28824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 48.78824 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 43.78824 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.28824 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.28824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 28.78824 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.28824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 23.11097 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.28824 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 38.78824 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.28824 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.28824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 48.78824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 43.78824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
