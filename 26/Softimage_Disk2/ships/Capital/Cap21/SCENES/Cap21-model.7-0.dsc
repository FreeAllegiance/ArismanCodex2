SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.13-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       model-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       cap21-bottom_window.1-0 ; 
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit1_1.1-0 ; 
       cap21-cargo_unit1_2.1-0 ; 
       cap21-lwing.3-0 ; 
       cap21-root.3-0 ROOT ; 
       cap21-rwing.1-0 ; 
       cap21-top_window.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-model.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       model-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 0 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
