SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       scaled-crystal1.1-0 ; 
       scaled-mat1.1-0 ; 
       scaled-mat10.1-0 ; 
       scaled-mat11.1-0 ; 
       scaled-mat12.1-0 ; 
       scaled-mat13.1-0 ; 
       scaled-mat14.1-0 ; 
       scaled-mat15.1-0 ; 
       scaled-mat16.1-0 ; 
       scaled-mat17.1-0 ; 
       scaled-mat18.1-0 ; 
       scaled-mat19.1-0 ; 
       scaled-mat2.1-0 ; 
       scaled-mat20.1-0 ; 
       scaled-mat21.1-0 ; 
       scaled-mat3.1-0 ; 
       scaled-mat4.1-0 ; 
       scaled-mat5.1-0 ; 
       scaled-mat6.1-0 ; 
       scaled-mat7.1-0 ; 
       scaled-mat8.1-0 ; 
       scaled-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       cap21-170deg.2-0 ROOT ; 
       cap21-170deg1.2-0 ROOT ; 
       cap21-170deg1_1.2-0 ROOT ; 
       cap21-170deg2.2-0 ROOT ; 
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-cone1.1-0 ; 
       cap21-cone2.1-0 ; 
       cap21-cone3.2-0 ROOT ; 
       cap21-cone4.2-0 ROOT ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-missemt1.1-0 ; 
       cap21-root.26-0 ROOT ; 
       cap21-thrust.1-0 ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
       cap21-trail.1-0 ; 
       cap21-turwepemt1.1-0 ; 
       cap21-turwepemt2.1-0 ; 
       cap21-turwepemt3.1-0 ; 
       cap21-turwepemt4.1-0 ; 
       cap21-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       scaled-t2d1.1-0 ; 
       scaled-t2d10.1-0 ; 
       scaled-t2d11.1-0 ; 
       scaled-t2d12.1-0 ; 
       scaled-t2d13.1-0 ; 
       scaled-t2d14.1-0 ; 
       scaled-t2d15.1-0 ; 
       scaled-t2d16.1-0 ; 
       scaled-t2d17.1-0 ; 
       scaled-t2d18.1-0 ; 
       scaled-t2d19.1-0 ; 
       scaled-t2d2.1-0 ; 
       scaled-t2d20.1-0 ; 
       scaled-t2d21.1-0 ; 
       scaled-t2d22.1-0 ; 
       scaled-t2d3.1-0 ; 
       scaled-t2d4.1-0 ; 
       scaled-t2d5.1-0 ; 
       scaled-t2d6.1-0 ; 
       scaled-t2d7.1-0 ; 
       scaled-t2d8.1-0 ; 
       scaled-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 15 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 15 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 8 110 ; 
       21 9 110 ; 
       22 15 110 ; 
       23 15 110 ; 
       24 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 2 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       8 10 300 ; 
       9 11 300 ; 
       10 13 300 ; 
       11 14 300 ; 
       12 1 300 ; 
       12 12 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       15 19 300 ; 
       15 0 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       17 17 300 ; 
       18 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 1 400 ; 
       5 3 400 ; 
       6 5 400 ; 
       7 7 400 ; 
       8 10 400 ; 
       9 12 400 ; 
       10 13 400 ; 
       11 14 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       12 11 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 20 401 ; 
       20 21 401 ; 
       21 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -62.90636 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -62.90636 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -43.91819 -9.60108 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 4.91968 4.91968 4.91968 -0.24 0 0 0.003982622 1.037763 -20.78308 MPRFLG 0 ; 
       1 SCHEM -49.67905 -9.355854 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 4.91968 4.91968 4.91968 0.3219042 -0.009111422 1.590762 -9.119192 0.02517572 4.967047 MPRFLG 0 ; 
       2 SCHEM -41.35885 -9.602335 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 4.91968 4.91968 4.91968 -2.901593 0 0 0.003982622 -2.418284 -20.77283 MPRFLG 0 ; 
       3 SCHEM -46.69446 -9.427277 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 4.91968 4.91968 4.91968 2.819689 -0.009111422 1.550831 9.127158 0.02517572 4.967047 MPRFLG 0 ; 
       4 SCHEM -45.40636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM -47.90636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM -52.90636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM -57.90636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM -49.79539 -4.67391 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM -46.61812 -4.923895 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM -33.08923 -11.46937 0 USR DISPLAY 0 0 SRT 1.319177 1.319177 1.319177 0 0 0 0.003982622 1.167882 -20.75981 MPRFLG 0 ; 
       11 SCHEM -31.1289 -11.43344 0 USR DISPLAY 0 0 SRT 1.108552 1.108552 1.108552 3.141593 0 0 0.003982622 -2.417084 -20.75016 MPRFLG 0 ; 
       12 SCHEM -50.40636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM -42.90636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM -32.90636 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM -41.65636 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 13.54891 MPRFLG 0 ; 
       16 SCHEM -37.90636 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM -60.40636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM -55.40636 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM -40.40636 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM -49.70466 -7.68536 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM -46.51073 -7.935347 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM -43.9124 -7.675736 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM -41.04111 -7.709874 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM -35.40636 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -16.40636 5.351718 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20.62954 18.74884 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 91.52406 45.79306 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 231.5525 81.58659 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -61.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -56.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -46.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -48.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -53.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -58.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -11.40636 5.351718 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25.62954 18.74884 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96.52406 45.79306 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 236.5525 81.58659 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -61.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM -56.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
