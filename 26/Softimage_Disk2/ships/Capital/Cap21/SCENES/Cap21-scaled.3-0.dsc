SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       scaled-crystal1.1-0 ; 
       scaled-mat1.1-0 ; 
       scaled-mat10.1-0 ; 
       scaled-mat11.1-0 ; 
       scaled-mat12.1-0 ; 
       scaled-mat13.1-0 ; 
       scaled-mat14.1-0 ; 
       scaled-mat15.1-0 ; 
       scaled-mat16.1-0 ; 
       scaled-mat17.1-0 ; 
       scaled-mat18.1-0 ; 
       scaled-mat19.1-0 ; 
       scaled-mat2.1-0 ; 
       scaled-mat3.1-0 ; 
       scaled-mat4.1-0 ; 
       scaled-mat5.1-0 ; 
       scaled-mat6.1-0 ; 
       scaled-mat7.1-0 ; 
       scaled-mat8.1-0 ; 
       scaled-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-cone1.1-0 ; 
       cap21-cone2.1-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-missemt1.1-0 ; 
       cap21-root.27-0 ROOT ; 
       cap21-thrust.1-0 ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
       cap21-trail.1-0 ; 
       cap21-turwepemt1.1-0 ; 
       cap21-turwepemt2.1-0 ; 
       cap21-turwepemt3.1-0 ; 
       cap21-turwepemt4.1-0 ; 
       cap21-wepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-scaled.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       scaled-t2d1.2-0 ; 
       scaled-t2d10.2-0 ; 
       scaled-t2d11.2-0 ; 
       scaled-t2d12.2-0 ; 
       scaled-t2d13.2-0 ; 
       scaled-t2d14.2-0 ; 
       scaled-t2d15.2-0 ; 
       scaled-t2d16.2-0 ; 
       scaled-t2d17.2-0 ; 
       scaled-t2d18.2-0 ; 
       scaled-t2d19.2-0 ; 
       scaled-t2d2.2-0 ; 
       scaled-t2d20.2-0 ; 
       scaled-t2d3.2-0 ; 
       scaled-t2d4.2-0 ; 
       scaled-t2d5.2-0 ; 
       scaled-t2d6.2-0 ; 
       scaled-t2d7.2-0 ; 
       scaled-t2d8.2-0 ; 
       scaled-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 4 110 ; 
       15 5 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       6 1 300 ; 
       6 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       9 17 300 ; 
       9 0 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       11 15 300 ; 
       12 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 5 400 ; 
       3 7 400 ; 
       4 10 400 ; 
       5 12 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 17 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       12 11 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 46.28824 -2 0 MPRFLG 0 ; 
       1 SCHEM 43.11097 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 35.61097 -4.67391 0 USR MPRFLG 0 ; 
       5 SCHEM 38.78824 -4.923895 0 USR MPRFLG 0 ; 
       6 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 45.24396 -2 0 MPRFLG 0 ; 
       8 SCHEM 54.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 43.75 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 1.513667 MPRFLG 0 ; 
       10 SCHEM 49.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 46.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35.7017 -7.68536 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 38.89563 -7.935347 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.49396 -7.675736 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44.36525 -7.709874 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 48.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 43.78824 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45.61097 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40.61097 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 38.2017 -6.67391 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.39563 -6.923895 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.49396 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 43.99396 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 64.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59.36525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 61.86525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.28824 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 43.78824 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 43.11097 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40.61097 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 40.7017 -6.67391 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 43.89563 -6.923895 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.49396 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 43.99396 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56.86525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59.36525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
