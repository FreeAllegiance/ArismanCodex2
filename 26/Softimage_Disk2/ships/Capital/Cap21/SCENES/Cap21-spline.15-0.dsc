SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.5-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       spline-bmerge1.5-0 ROOT ; 
       spline-bmerge2.1-0 ROOT ; 
       spline-circle5.2-0 ROOT ; 
       spline-extru2.3-0 ROOT ; 
       spline-extru3.2-0 ROOT ; 
       spline-sphere1.2-0 ROOT ; 
       spline1-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-spline.15-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 0 -8 0 SRT 0.792 0.792 0.792 0 0 0 0 0.7748184 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 SRT 0.4973761 0.4973761 0.4973761 0 0 0 0 -1.157577 -2.506519 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -6 0 SRT 1 1 1 0 0 0 5.960464e-008 -0.4174906 -10.18623 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
