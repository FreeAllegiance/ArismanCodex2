SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.6-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       spline-bmerge1.6-0 ROOT ; 
       spline-bmerge2.2-0 ROOT ; 
       spline-circle5.2-0 ROOT ; 
       spline-extru2.3-0 ; 
       spline-extru4.1-0 ; 
       spline-sphere1.1-0 ; 
       spline-sphere1_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-spline.16-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 0 110 ; 
       5 0 110 ; 
       4 0 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 0 -4 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 5.960464e-008 -0.4174906 -10.18623 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
