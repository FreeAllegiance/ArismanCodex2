SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.34-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       text-crystal1.3-0 ; 
       text-mat1.1-0 ; 
       text-mat10.1-0 ; 
       text-mat11.1-0 ; 
       text-mat12.1-0 ; 
       text-mat13.1-0 ; 
       text-mat14.1-0 ; 
       text-mat15.1-0 ; 
       text-mat16.1-0 ; 
       text-mat17.1-0 ; 
       text-mat2.1-0 ; 
       text-mat3.1-0 ; 
       text-mat4.1-0 ; 
       text-mat5.1-0 ; 
       text-mat6.1-0 ; 
       text-mat7.3-0 ; 
       text-mat8.2-0 ; 
       text-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       cap21-cargo_unit1.1-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-root.16-0 ROOT ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
       text-cargo_unit2.2-0 ROOT ; 
       text-cargo_unit3.2-0 ROOT ; 
       text-cargo_unit4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-text.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       text-t2d1.1-0 ; 
       text-t2d10.1-0 ; 
       text-t2d11.1-0 ; 
       text-t2d12.1-0 ; 
       text-t2d13.1-0 ; 
       text-t2d14.1-0 ; 
       text-t2d15.1-0 ; 
       text-t2d16.1-0 ; 
       text-t2d17.1-0 ; 
       text-t2d18.2-0 ; 
       text-t2d2.1-0 ; 
       text-t2d3.1-0 ; 
       text-t2d4.1-0 ; 
       text-t2d5.1-0 ; 
       text-t2d6.1-0 ; 
       text-t2d7.5-0 ; 
       text-t2d8.4-0 ; 
       text-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 1 300 ; 
       1 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       3 15 300 ; 
       3 0 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       6 3 400 ; 
       7 5 400 ; 
       8 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 9 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       4 SCHEM 0 -6 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -12 0 SRT 1 1 1 0 0 1.570796 5.960464e-008 -0.4174906 -11.20978 MPRFLG 0 ; 
       7 SCHEM 2.5 -12 0 SRT 1 1 1 0 0 1.570796 5.960464e-008 -0.4174906 -13.84452 MPRFLG 0 ; 
       8 SCHEM 2.5 -18 0 SRT 1 1 1 0 0 1.570796 5.960464e-008 -0.4174906 -16.5275 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 0 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
