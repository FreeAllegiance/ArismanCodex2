SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.42-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       turret_loc-crystal1.1-0 ; 
       turret_loc-mat1.1-0 ; 
       turret_loc-mat10.1-0 ; 
       turret_loc-mat11.1-0 ; 
       turret_loc-mat12.1-0 ; 
       turret_loc-mat13.1-0 ; 
       turret_loc-mat14.1-0 ; 
       turret_loc-mat15.1-0 ; 
       turret_loc-mat16.1-0 ; 
       turret_loc-mat17.1-0 ; 
       turret_loc-mat18.1-0 ; 
       turret_loc-mat19.1-0 ; 
       turret_loc-mat2.1-0 ; 
       turret_loc-mat20.1-0 ; 
       turret_loc-mat21.1-0 ; 
       turret_loc-mat3.1-0 ; 
       turret_loc-mat4.1-0 ; 
       turret_loc-mat5.1-0 ; 
       turret_loc-mat6.1-0 ; 
       turret_loc-mat7.1-0 ; 
       turret_loc-mat8.1-0 ; 
       turret_loc-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       arc-cone.1-0 ROOT ; 
       arc1-cone.1-0 ROOT ; 
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-missemt1.1-0 ; 
       cap21-root.21-0 ROOT ; 
       cap21-thrust.1-0 ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
       cap21-trail.1-0 ; 
       cap21-turwepemt1.1-0 ; 
       cap21-turwepemt2.1-0 ; 
       cap21-turwepemt3.1-0 ; 
       cap21-turwepemt4.1-0 ; 
       cap21-wepemt1.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
       turret_loc-cone2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       turret_loc-t2d1.1-0 ; 
       turret_loc-t2d10.1-0 ; 
       turret_loc-t2d11.1-0 ; 
       turret_loc-t2d12.1-0 ; 
       turret_loc-t2d13.1-0 ; 
       turret_loc-t2d14.1-0 ; 
       turret_loc-t2d15.1-0 ; 
       turret_loc-t2d16.1-0 ; 
       turret_loc-t2d17.1-0 ; 
       turret_loc-t2d18.1-0 ; 
       turret_loc-t2d2.1-0 ; 
       turret_loc-t2d3.1-0 ; 
       turret_loc-t2d4.1-0 ; 
       turret_loc-t2d5.1-0 ; 
       turret_loc-t2d6.1-0 ; 
       turret_loc-t2d7.1-0 ; 
       turret_loc-t2d8.1-0 ; 
       turret_loc-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       6 1 300 ; 
       6 12 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       9 19 300 ; 
       9 0 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       0 10 300 ; 
       19 11 300 ; 
       1 13 300 ; 
       20 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 1 400 ; 
       3 3 400 ; 
       4 5 400 ; 
       5 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       12 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 0 0 DISPLAY 1 2 SRT 3.632844 3.632844 3.632844 0 0 0 0 0.6404828 -11.29587 MPRFLG 0 ; 
       19 SCHEM 47.5 0 0 DISPLAY 1 2 SRT 3.632844 3.632844 3.632844 3.141593 0 0 0 -1.357878 -11.29587 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 SRT 3.632844 3.632844 3.632844 0 0 1.570796 -0.7089925 -0.384717 -11.28922 MPRFLG 0 ; 
       20 SCHEM 57.5 0 0 SRT 3.632844 3.632844 3.632844 0 3.141593 -1.570796 0.7089925 -0.384717 -11.28922 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
