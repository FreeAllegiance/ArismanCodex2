SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       turret_loc-crystal1.2-0 ; 
       turret_loc-mat1.2-0 ; 
       turret_loc-mat10.2-0 ; 
       turret_loc-mat11.2-0 ; 
       turret_loc-mat12.2-0 ; 
       turret_loc-mat13.2-0 ; 
       turret_loc-mat14.2-0 ; 
       turret_loc-mat15.2-0 ; 
       turret_loc-mat16.2-0 ; 
       turret_loc-mat17.2-0 ; 
       turret_loc-mat2.2-0 ; 
       turret_loc-mat3.2-0 ; 
       turret_loc-mat4.2-0 ; 
       turret_loc-mat5.2-0 ; 
       turret_loc-mat6.2-0 ; 
       turret_loc-mat7.2-0 ; 
       turret_loc-mat8.2-0 ; 
       turret_loc-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       cap21-cargo_unit1.1-0 ; 
       cap21-cargo_unit2.2-0 ; 
       cap21-cargo_unit3.2-0 ; 
       cap21-cargo_unit4.2-0 ; 
       cap21-lwing.3-0 ; 
       cap21-lwing1.1-0 ; 
       cap21-missemt1.1-0 ; 
       cap21-root.22-0 ROOT ; 
       cap21-thrust.1-0 ; 
       cap21-top_window.2-0 ; 
       cap21-top_window1.1-0 ; 
       cap21-trail.1-0 ; 
       cap21-turwepemt1.1-0 ; 
       cap21-turwepemt2.1-0 ; 
       cap21-turwepemt3.1-0 ; 
       cap21-turwepemt4.1-0 ; 
       cap21-wepemt1.1-0 ; 
       turcone-140deg.1-0 ROOT ; 
       turcone-170deg.1-0 ROOT ; 
       turret_loc-140deg1.1-0 ROOT ; 
       turret_loc-170deg1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/Cap21/PICTURES/cap21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Cap21-turret_loc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       turret_loc-t2d1.2-0 ; 
       turret_loc-t2d10.2-0 ; 
       turret_loc-t2d11.2-0 ; 
       turret_loc-t2d12.2-0 ; 
       turret_loc-t2d13.2-0 ; 
       turret_loc-t2d14.2-0 ; 
       turret_loc-t2d15.2-0 ; 
       turret_loc-t2d16.2-0 ; 
       turret_loc-t2d17.2-0 ; 
       turret_loc-t2d18.2-0 ; 
       turret_loc-t2d2.2-0 ; 
       turret_loc-t2d3.2-0 ; 
       turret_loc-t2d4.2-0 ; 
       turret_loc-t2d5.2-0 ; 
       turret_loc-t2d6.2-0 ; 
       turret_loc-t2d7.2-0 ; 
       turret_loc-t2d8.2-0 ; 
       turret_loc-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       4 1 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       7 15 300 ; 
       7 0 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       9 13 300 ; 
       10 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       1 3 400 ; 
       2 5 400 ; 
       3 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 0 401 ; 
       3 2 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -62.90636 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -62.90636 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       20 SCHEM -44.45084 9.532828 0 USR WIRECOL 7 7 SRT 3.251232 3.251232 3.251232 -2.901593 0 0 0 -0.6989971 -16.58603 MPRFLG 0 ; 
       19 SCHEM -42.49669 9.532828 0 USR WIRECOL 7 7 SRT 3.251232 3.251232 3.251232 -3.023591 0.0002598725 1.548851 0.7089925 -0.384717 -13.85578 MPRFLG 0 ; 
       0 SCHEM -45.40636 -2 0 MPRFLG 0 ; 
       1 SCHEM -47.90636 -2 0 MPRFLG 0 ; 
       2 SCHEM -52.90636 -2 0 MPRFLG 0 ; 
       3 SCHEM -57.90636 -2 0 MPRFLG 0 ; 
       4 SCHEM -50.40636 -2 0 MPRFLG 0 ; 
       5 SCHEM -42.90636 -2 0 MPRFLG 0 ; 
       6 SCHEM -32.90636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -41.65636 0 0 SRT 1 1 1 0 0 0 -0.007760763 -1.233816e-005 6.14192 MPRFLG 0 ; 
       8 SCHEM -37.90636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -60.40636 -2 0 MPRFLG 0 ; 
       10 SCHEM -55.40636 -2 0 MPRFLG 0 ; 
       11 SCHEM -40.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -30.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -27.90636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -25.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -22.90636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -35.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -50.8116 9.721235 0 USR WIRECOL 7 7 SRT 3.251232 3.251232 3.251232 -0.1180019 0.0002598725 1.592741 -0.7089925 -0.384717 -13.85578 MPRFLG 0 ; 
       18 SCHEM -47.75703 9.758139 0 USR WIRECOL 7 7 SRT 3.251232 3.251232 3.251232 -0.24 0 0 0 0.6989971 -16.58603 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -46.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -48.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -53.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -58.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -51.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -43.90636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -61.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -56.40636 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -21.40636 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -46.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -46.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -48.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -48.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -53.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -53.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -58.90636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -58.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -51.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -43.90636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -61.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -56.40636 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -21.40636 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
