SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01_2.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.51-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       add_turr-mat100.1-0 ; 
       add_turr-mat101.1-0 ; 
       add_turr-mat96.1-0 ; 
       add_turr-mat97.1-0 ; 
       add_turr-mat98.1-0 ; 
       add_turr-mat99.1-0 ; 
       kez_frigate_F-mat1.4-0 ; 
       kez_frigate_F-mat10.4-0 ; 
       kez_frigate_F-mat11.4-0 ; 
       kez_frigate_F-mat12.4-0 ; 
       kez_frigate_F-mat13.4-0 ; 
       kez_frigate_F-mat14.7-0 ; 
       kez_frigate_F-mat15.7-0 ; 
       kez_frigate_F-mat17.7-0 ; 
       kez_frigate_F-mat18.7-0 ; 
       kez_frigate_F-mat19.4-0 ; 
       kez_frigate_F-mat2.4-0 ; 
       kez_frigate_F-mat20.4-0 ; 
       kez_frigate_F-mat21.4-0 ; 
       kez_frigate_F-mat22.4-0 ; 
       kez_frigate_F-mat23.4-0 ; 
       kez_frigate_F-mat24.4-0 ; 
       kez_frigate_F-mat25.4-0 ; 
       kez_frigate_F-mat26.4-0 ; 
       kez_frigate_F-mat28.4-0 ; 
       kez_frigate_F-mat3.4-0 ; 
       kez_frigate_F-mat30.4-0 ; 
       kez_frigate_F-mat32.7-0 ; 
       kez_frigate_F-mat33.7-0 ; 
       kez_frigate_F-mat34.7-0 ; 
       kez_frigate_F-mat35.7-0 ; 
       kez_frigate_F-mat36.7-0 ; 
       kez_frigate_F-mat37.7-0 ; 
       kez_frigate_F-mat38.7-0 ; 
       kez_frigate_F-mat39.7-0 ; 
       kez_frigate_F-mat4.4-0 ; 
       kez_frigate_F-mat40.4-0 ; 
       kez_frigate_F-mat41.4-0 ; 
       kez_frigate_F-mat43.4-0 ; 
       kez_frigate_F-mat44.4-0 ; 
       kez_frigate_F-mat45.4-0 ; 
       kez_frigate_F-mat46.4-0 ; 
       kez_frigate_F-mat48.4-0 ; 
       kez_frigate_F-mat49.4-0 ; 
       kez_frigate_F-mat5.4-0 ; 
       kez_frigate_F-mat50.4-0 ; 
       kez_frigate_F-mat51.4-0 ; 
       kez_frigate_F-mat52.4-0 ; 
       kez_frigate_F-mat53.4-0 ; 
       kez_frigate_F-mat54.4-0 ; 
       kez_frigate_F-mat55.9-0 ; 
       kez_frigate_F-mat56.6-0 ; 
       kez_frigate_F-mat57.6-0 ; 
       kez_frigate_F-mat58.6-0 ; 
       kez_frigate_F-mat59.4-0 ; 
       kez_frigate_F-mat6.4-0 ; 
       kez_frigate_F-mat60.6-0 ; 
       kez_frigate_F-mat66.6-0 ; 
       kez_frigate_F-mat69.6-0 ; 
       kez_frigate_F-mat7.4-0 ; 
       kez_frigate_F-mat70.6-0 ; 
       kez_frigate_F-mat71.6-0 ; 
       kez_frigate_F-mat72.6-0 ; 
       kez_frigate_F-mat74.6-0 ; 
       kez_frigate_F-mat8.4-0 ; 
       kez_frigate_F-mat80.4-0 ; 
       kez_frigate_F-mat81.4-0 ; 
       kez_frigate_F-mat82.4-0 ; 
       kez_frigate_F-mat83.4-0 ; 
       kez_frigate_F-mat84.4-0 ; 
       kez_frigate_F-mat85.4-0 ; 
       kez_frigate_F-mat86.4-0 ; 
       kez_frigate_F-mat87.4-0 ; 
       kez_frigate_F-mat88.4-0 ; 
       kez_frigate_F-mat89.4-0 ; 
       kez_frigate_F-mat9.4-0 ; 
       kez_frigate_F-mat90.4-0 ; 
       kez_frigate_F-mat91.4-0 ; 
       kez_frigate_F-mat92.4-0 ; 
       kez_frigate_F-mat93.4-0 ; 
       kez_frigate_F-mat94.4-0 ; 
       kez_frigate_F-mat95.4-0 ; 
       kez_frigate_F-port_red-left.1-0.4-0 ; 
       kez_frigate_F-starbord_green-right.1-0.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 62     
       add_turr-circle1.1-0 ROOT ; 
       add_turr-circle2.1-0 ROOT ; 
       add_turr1-circle1.1-0 ROOT ; 
       add_turr2-circle1.1-0 ROOT ; 
       add_turr3-circle1.1-0 ROOT ; 
       add_turr4-circle1.1-0 ROOT ; 
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01_2.5-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-ffuselg.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-lthrust.1-0 ; 
       cap01-lwepemt1.1-0 ; 
       cap01-lwepemt2.1-0 ; 
       cap01-lwepemt3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-missemt.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-rthrust.1-0 ; 
       cap01-rwepemt1.1-0 ; 
       cap01-rwepemt2.1-0 ; 
       cap01-rwepemt3.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-slrsal0.2-0 ; 
       cap01-smoke.1-0 ; 
       cap01-SSal.1-0 ; 
       cap01-SSal1.1-0 ; 
       cap01-SSal2.1-0 ; 
       cap01-SSal3.1-0 ; 
       cap01-SSal4.1-0 ; 
       cap01-SSal5.1-0 ; 
       cap01-SSal6.1-0 ; 
       cap01-SSar.1-0 ; 
       cap01-SSar1.1-0 ; 
       cap01-SSar2.1-0 ; 
       cap01-SSar3.1-0 ; 
       cap01-SSar4.1-0 ; 
       cap01-SSar5.1-0 ; 
       cap01-SSar6.1-0 ; 
       cap01-SSat.1-0 ; 
       cap01-SSfb.1-0 ; 
       cap01-SSml1.1-0 ; 
       cap01-SSmr1.1-0 ; 
       cap01-thrust.1-0 ; 
       cap01-trail.1-0 ; 
       cap01-turwepemt1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-add_turr.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       add_turr-t2d77.1-0 ; 
       add_turr-t2d78.1-0 ; 
       add_turr-t2d79.1-0 ; 
       add_turr-t2d80.1-0 ; 
       add_turr-t2d81.1-0 ; 
       add_turr-t2d82.1-0 ; 
       kez_frigate_F-t2d12.4-0 ; 
       kez_frigate_F-t2d14.7-0 ; 
       kez_frigate_F-t2d15.7-0 ; 
       kez_frigate_F-t2d16.7-0 ; 
       kez_frigate_F-t2d17.4-0 ; 
       kez_frigate_F-t2d18.4-0 ; 
       kez_frigate_F-t2d19.4-0 ; 
       kez_frigate_F-t2d2.4-0 ; 
       kez_frigate_F-t2d20.4-0 ; 
       kez_frigate_F-t2d21.4-0 ; 
       kez_frigate_F-t2d22.4-0 ; 
       kez_frigate_F-t2d24.4-0 ; 
       kez_frigate_F-t2d26.4-0 ; 
       kez_frigate_F-t2d27.7-0 ; 
       kez_frigate_F-t2d28.7-0 ; 
       kez_frigate_F-t2d29.7-0 ; 
       kez_frigate_F-t2d30.7-0 ; 
       kez_frigate_F-t2d31.7-0 ; 
       kez_frigate_F-t2d32.7-0 ; 
       kez_frigate_F-t2d33.7-0 ; 
       kez_frigate_F-t2d34.4-0 ; 
       kez_frigate_F-t2d35.4-0 ; 
       kez_frigate_F-t2d36.4-0 ; 
       kez_frigate_F-t2d37.4-0 ; 
       kez_frigate_F-t2d38.4-0 ; 
       kez_frigate_F-t2d4.4-0 ; 
       kez_frigate_F-t2d40.4-0 ; 
       kez_frigate_F-t2d41.4-0 ; 
       kez_frigate_F-t2d42.4-0 ; 
       kez_frigate_F-t2d43.4-0 ; 
       kez_frigate_F-t2d44.4-0 ; 
       kez_frigate_F-t2d45.4-0 ; 
       kez_frigate_F-t2d46.4-0 ; 
       kez_frigate_F-t2d48.6-0 ; 
       kez_frigate_F-t2d49.6-0 ; 
       kez_frigate_F-t2d50.6-0 ; 
       kez_frigate_F-t2d51.4-0 ; 
       kez_frigate_F-t2d55.6-0 ; 
       kez_frigate_F-t2d58.6-0 ; 
       kez_frigate_F-t2d59.6-0 ; 
       kez_frigate_F-t2d60.6-0 ; 
       kez_frigate_F-t2d61.6-0 ; 
       kez_frigate_F-t2d62.6-0 ; 
       kez_frigate_F-t2d7.4-0 ; 
       kez_frigate_F-t2d70.4-0 ; 
       kez_frigate_F-t2d71.4-0 ; 
       kez_frigate_F-t2d72.4-0 ; 
       kez_frigate_F-t2d73.4-0 ; 
       kez_frigate_F-t2d74.4-0 ; 
       kez_frigate_F-t2d75.4-0 ; 
       kez_frigate_F-t2d76.4-0 ; 
       kez_frigate_F-t2d8.4-0 ; 
       kez_frigate_F-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       60 9 110 ; 
       36 9 110 ; 
       35 9 110 ; 
       34 9 110 ; 
       23 9 110 ; 
       22 9 110 ; 
       21 9 110 ; 
       33 9 110 ; 
       20 9 110 ; 
       59 9 110 ; 
       40 9 110 ; 
       25 9 110 ; 
       61 9 110 ; 
       6 24 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 24 110 ; 
       14 39 110 ; 
       15 39 110 ; 
       16 39 110 ; 
       17 39 110 ; 
       18 39 110 ; 
       19 39 110 ; 
       24 9 110 ; 
       26 39 110 ; 
       27 39 110 ; 
       28 39 110 ; 
       29 39 110 ; 
       30 39 110 ; 
       31 39 110 ; 
       32 39 110 ; 
       37 24 110 ; 
       38 24 110 ; 
       39 7 110 ; 
       41 6 110 ; 
       42 14 110 ; 
       43 15 110 ; 
       44 16 110 ; 
       45 17 110 ; 
       46 18 110 ; 
       47 19 110 ; 
       48 6 110 ; 
       49 27 110 ; 
       50 28 110 ; 
       51 29 110 ; 
       52 30 110 ; 
       53 31 110 ; 
       54 32 110 ; 
       55 8 110 ; 
       56 38 110 ; 
       57 26 110 ; 
       58 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 36 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       6 39 300 ; 
       6 40 300 ; 
       6 41 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 5 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       8 4 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 26 300 ; 
       11 15 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       13 50 300 ; 
       13 51 300 ; 
       13 52 300 ; 
       13 53 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 55 300 ; 
       15 44 300 ; 
       16 35 300 ; 
       17 25 300 ; 
       18 16 300 ; 
       19 6 300 ; 
       24 56 300 ; 
       24 57 300 ; 
       24 58 300 ; 
       24 60 300 ; 
       24 61 300 ; 
       24 62 300 ; 
       24 63 300 ; 
       24 0 300 ; 
       24 1 300 ; 
       26 10 300 ; 
       27 59 300 ; 
       28 64 300 ; 
       29 75 300 ; 
       30 7 300 ; 
       31 8 300 ; 
       32 9 300 ; 
       37 49 300 ; 
       38 54 300 ; 
       41 82 300 ; 
       42 73 300 ; 
       43 74 300 ; 
       44 76 300 ; 
       45 77 300 ; 
       46 78 300 ; 
       47 79 300 ; 
       48 83 300 ; 
       49 67 300 ; 
       50 68 300 ; 
       51 69 300 ; 
       52 70 300 ; 
       53 71 300 ; 
       54 72 300 ; 
       55 81 300 ; 
       56 80 300 ; 
       57 66 300 ; 
       58 65 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 10 400 ; 
       16 31 400 ; 
       18 13 400 ; 
       26 49 400 ; 
       27 57 400 ; 
       28 58 400 ; 
       31 6 400 ; 
       38 42 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 56 401 ; 
       7 51 401 ; 
       9 50 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 55 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 54 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       48 37 401 ; 
       49 38 401 ; 
       51 40 401 ; 
       52 39 401 ; 
       53 41 401 ; 
       55 53 401 ; 
       57 43 401 ; 
       58 44 401 ; 
       60 45 401 ; 
       61 46 401 ; 
       62 47 401 ; 
       63 48 401 ; 
       75 52 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       0 4 401 ; 
       1 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       60 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 62.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 70 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 75 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 80 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 85 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 65 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 67.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 82.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 87.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 90 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 105 0 0 DISPLAY 0 0 SRT 0.2709176 0.254144 0.220597 0 0 0 0.3541463 -3.359071 25.03026 MPRFLG 0 ; 
       2 SCHEM 102.5 0 0 DISPLAY 0 0 SRT 0.2042718 0.1916246 0.1663301 0 0 0 0.2433263 2.949066 13.70066 MPRFLG 0 ; 
       3 SCHEM 92.5 0 0 DISPLAY 0 0 SRT 0.1305823 0.1224974 0.1063277 0.12 0 0 -0.2393938 4.205039 -21.75597 MPRFLG 0 ; 
       4 SCHEM 95 0 0 DISPLAY 0 0 SRT 0.2042718 0.1916246 0.1663301 0.7199999 0 0 -0.2728266 -4.72145 -20.81565 MPRFLG 0 ; 
       5 SCHEM 97.5 0 0 DISPLAY 0 0 SRT 0.1654602 0.1552159 0.1347274 0 0 0.35 1.728034 1.23375 0.0641499 MPRFLG 0 ; 
       1 SCHEM 100 0 0 DISPLAY 0 0 SRT 0.1654602 0.1552159 0.1347274 0 -3.141593 -0.35 -1.728034 1.23375 0.0641499 MPRFLG 0 ; 
       6 SCHEM 33.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 31.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 46.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 30 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 35 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 40 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 30 -2 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 26.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 10 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 15 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 20 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 22.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 26.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 30 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 32.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 35 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 37.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 40 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 42.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 55 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 10 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 12.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 15 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 17.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 20 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 22.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 52.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 27.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 44 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 29 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 41.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 46.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 46.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 44 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 39 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 54 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 54 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 54 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 54 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 54 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 36.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 34 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 59 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 6.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 6.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 6.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 6.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 31.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 11.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 59 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 14 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 26.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 24 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 9 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 11.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 14 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 16.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 19 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 21.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 29 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 31.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 16.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 34 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 36.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 39 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 41.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 1.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 51.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 56.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 107.5 0 0 MPRFLG 0 ; 
       1 SCHEM 112.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 46.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 46.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 46.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 41.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 44 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 49 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 49 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 54 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 54 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 54 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 54 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 59 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 4 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 59 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 29 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 24 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 19 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 16.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 31.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 34 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 39 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 44 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 54 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 110 0 0 MPRFLG 0 ; 
       5 SCHEM 115 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 91.5 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
