SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01_2.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.54-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       all_frames-mat100.1-0 ; 
       all_frames-mat101.1-0 ; 
       all_frames-mat96.1-0 ; 
       all_frames-mat97.1-0 ; 
       all_frames-mat98.1-0 ; 
       all_frames-mat99.1-0 ; 
       kez_frigate_F-mat1.4-0 ; 
       kez_frigate_F-mat10.4-0 ; 
       kez_frigate_F-mat11.4-0 ; 
       kez_frigate_F-mat12.4-0 ; 
       kez_frigate_F-mat13.4-0 ; 
       kez_frigate_F-mat14.7-0 ; 
       kez_frigate_F-mat15.7-0 ; 
       kez_frigate_F-mat17.7-0 ; 
       kez_frigate_F-mat18.7-0 ; 
       kez_frigate_F-mat19.4-0 ; 
       kez_frigate_F-mat2.4-0 ; 
       kez_frigate_F-mat20.4-0 ; 
       kez_frigate_F-mat21.4-0 ; 
       kez_frigate_F-mat22.4-0 ; 
       kez_frigate_F-mat23.4-0 ; 
       kez_frigate_F-mat24.4-0 ; 
       kez_frigate_F-mat25.4-0 ; 
       kez_frigate_F-mat26.4-0 ; 
       kez_frigate_F-mat28.4-0 ; 
       kez_frigate_F-mat3.4-0 ; 
       kez_frigate_F-mat30.4-0 ; 
       kez_frigate_F-mat32.7-0 ; 
       kez_frigate_F-mat33.7-0 ; 
       kez_frigate_F-mat34.7-0 ; 
       kez_frigate_F-mat35.7-0 ; 
       kez_frigate_F-mat36.7-0 ; 
       kez_frigate_F-mat37.7-0 ; 
       kez_frigate_F-mat38.7-0 ; 
       kez_frigate_F-mat39.7-0 ; 
       kez_frigate_F-mat4.4-0 ; 
       kez_frigate_F-mat40.4-0 ; 
       kez_frigate_F-mat41.4-0 ; 
       kez_frigate_F-mat43.4-0 ; 
       kez_frigate_F-mat44.4-0 ; 
       kez_frigate_F-mat45.4-0 ; 
       kez_frigate_F-mat46.4-0 ; 
       kez_frigate_F-mat48.4-0 ; 
       kez_frigate_F-mat49.4-0 ; 
       kez_frigate_F-mat5.4-0 ; 
       kez_frigate_F-mat50.4-0 ; 
       kez_frigate_F-mat51.4-0 ; 
       kez_frigate_F-mat52.4-0 ; 
       kez_frigate_F-mat53.4-0 ; 
       kez_frigate_F-mat54.4-0 ; 
       kez_frigate_F-mat55.9-0 ; 
       kez_frigate_F-mat56.6-0 ; 
       kez_frigate_F-mat57.6-0 ; 
       kez_frigate_F-mat58.6-0 ; 
       kez_frigate_F-mat59.4-0 ; 
       kez_frigate_F-mat6.4-0 ; 
       kez_frigate_F-mat60.6-0 ; 
       kez_frigate_F-mat66.6-0 ; 
       kez_frigate_F-mat69.6-0 ; 
       kez_frigate_F-mat7.4-0 ; 
       kez_frigate_F-mat70.6-0 ; 
       kez_frigate_F-mat71.6-0 ; 
       kez_frigate_F-mat72.6-0 ; 
       kez_frigate_F-mat74.6-0 ; 
       kez_frigate_F-mat8.4-0 ; 
       kez_frigate_F-mat80.4-0 ; 
       kez_frigate_F-mat81.4-0 ; 
       kez_frigate_F-mat82.4-0 ; 
       kez_frigate_F-mat83.4-0 ; 
       kez_frigate_F-mat84.4-0 ; 
       kez_frigate_F-mat85.4-0 ; 
       kez_frigate_F-mat86.4-0 ; 
       kez_frigate_F-mat87.4-0 ; 
       kez_frigate_F-mat88.4-0 ; 
       kez_frigate_F-mat89.4-0 ; 
       kez_frigate_F-mat9.4-0 ; 
       kez_frigate_F-mat90.4-0 ; 
       kez_frigate_F-mat91.4-0 ; 
       kez_frigate_F-mat92.4-0 ; 
       kez_frigate_F-mat93.4-0 ; 
       kez_frigate_F-mat94.4-0 ; 
       kez_frigate_F-mat95.4-0 ; 
       kez_frigate_F-port_red-left.1-0.4-0 ; 
       kez_frigate_F-starbord_green-right.1-0.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01_2.8-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-ffuselg.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-lthrust.1-0 ; 
       cap01-lwepemt1.1-0 ; 
       cap01-lwepemt2.1-0 ; 
       cap01-lwepemt3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-missemt.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-rthrust.1-0 ; 
       cap01-rwepemt1.1-0 ; 
       cap01-rwepemt2.1-0 ; 
       cap01-rwepemt3.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-slrsal0.2-0 ; 
       cap01-smoke.1-0 ; 
       cap01-SSal.1-0 ; 
       cap01-SSal1.1-0 ; 
       cap01-SSal2.1-0 ; 
       cap01-SSal3.1-0 ; 
       cap01-SSal4.1-0 ; 
       cap01-SSal5.1-0 ; 
       cap01-SSal6.1-0 ; 
       cap01-SSar.1-0 ; 
       cap01-SSar1.1-0 ; 
       cap01-SSar2.1-0 ; 
       cap01-SSar3.1-0 ; 
       cap01-SSar4.1-0 ; 
       cap01-SSar5.1-0 ; 
       cap01-SSar6.1-0 ; 
       cap01-SSat.1-0 ; 
       cap01-SSfb.1-0 ; 
       cap01-SSml1.1-0 ; 
       cap01-SSmr1.1-0 ; 
       cap01-thrust.1-0 ; 
       cap01-trail.1-0 ; 
       cap01-turwepemt1.1-0 ; 
       cap01-turwepemt2.1-0 ; 
       cap01-turwepemt3.1-0 ; 
       cap01-turwepemt4.1-0 ; 
       cap01-turwepemt5.1-0 ; 
       cap01-turwepemt6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-all_frames.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       all_frames-t2d77.1-0 ; 
       all_frames-t2d78.1-0 ; 
       all_frames-t2d79.1-0 ; 
       all_frames-t2d80.1-0 ; 
       all_frames-t2d81.1-0 ; 
       all_frames-t2d82.1-0 ; 
       kez_frigate_F-t2d12.4-0 ; 
       kez_frigate_F-t2d14.8-0 ; 
       kez_frigate_F-t2d15.8-0 ; 
       kez_frigate_F-t2d16.8-0 ; 
       kez_frigate_F-t2d17.4-0 ; 
       kez_frigate_F-t2d18.4-0 ; 
       kez_frigate_F-t2d19.4-0 ; 
       kez_frigate_F-t2d2.4-0 ; 
       kez_frigate_F-t2d20.4-0 ; 
       kez_frigate_F-t2d21.4-0 ; 
       kez_frigate_F-t2d22.4-0 ; 
       kez_frigate_F-t2d24.4-0 ; 
       kez_frigate_F-t2d26.4-0 ; 
       kez_frigate_F-t2d27.8-0 ; 
       kez_frigate_F-t2d28.8-0 ; 
       kez_frigate_F-t2d29.8-0 ; 
       kez_frigate_F-t2d30.7-0 ; 
       kez_frigate_F-t2d31.7-0 ; 
       kez_frigate_F-t2d32.7-0 ; 
       kez_frigate_F-t2d33.7-0 ; 
       kez_frigate_F-t2d34.4-0 ; 
       kez_frigate_F-t2d35.4-0 ; 
       kez_frigate_F-t2d36.4-0 ; 
       kez_frigate_F-t2d37.4-0 ; 
       kez_frigate_F-t2d38.4-0 ; 
       kez_frigate_F-t2d4.4-0 ; 
       kez_frigate_F-t2d40.4-0 ; 
       kez_frigate_F-t2d41.4-0 ; 
       kez_frigate_F-t2d42.4-0 ; 
       kez_frigate_F-t2d43.4-0 ; 
       kez_frigate_F-t2d44.4-0 ; 
       kez_frigate_F-t2d45.4-0 ; 
       kez_frigate_F-t2d46.4-0 ; 
       kez_frigate_F-t2d48.6-0 ; 
       kez_frigate_F-t2d49.6-0 ; 
       kez_frigate_F-t2d50.6-0 ; 
       kez_frigate_F-t2d51.4-0 ; 
       kez_frigate_F-t2d55.7-0 ; 
       kez_frigate_F-t2d58.7-0 ; 
       kez_frigate_F-t2d59.7-0 ; 
       kez_frigate_F-t2d60.7-0 ; 
       kez_frigate_F-t2d61.7-0 ; 
       kez_frigate_F-t2d62.7-0 ; 
       kez_frigate_F-t2d7.4-0 ; 
       kez_frigate_F-t2d70.4-0 ; 
       kez_frigate_F-t2d71.4-0 ; 
       kez_frigate_F-t2d72.4-0 ; 
       kez_frigate_F-t2d73.4-0 ; 
       kez_frigate_F-t2d74.4-0 ; 
       kez_frigate_F-t2d75.4-0 ; 
       kez_frigate_F-t2d76.4-0 ; 
       kez_frigate_F-t2d8.4-0 ; 
       kez_frigate_F-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       54 3 110 ; 
       30 3 110 ; 
       29 3 110 ; 
       28 3 110 ; 
       17 3 110 ; 
       16 3 110 ; 
       15 3 110 ; 
       27 3 110 ; 
       14 3 110 ; 
       53 3 110 ; 
       34 3 110 ; 
       19 3 110 ; 
       55 3 110 ; 
       56 3 110 ; 
       57 3 110 ; 
       58 3 110 ; 
       59 3 110 ; 
       60 3 110 ; 
       0 18 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 18 110 ; 
       8 33 110 ; 
       9 33 110 ; 
       10 33 110 ; 
       11 33 110 ; 
       12 33 110 ; 
       13 33 110 ; 
       18 3 110 ; 
       20 33 110 ; 
       21 33 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 33 110 ; 
       25 33 110 ; 
       26 33 110 ; 
       31 18 110 ; 
       32 18 110 ; 
       33 1 110 ; 
       35 0 110 ; 
       36 8 110 ; 
       37 9 110 ; 
       38 10 110 ; 
       39 11 110 ; 
       40 12 110 ; 
       41 13 110 ; 
       42 0 110 ; 
       43 21 110 ; 
       44 22 110 ; 
       45 23 110 ; 
       46 24 110 ; 
       47 25 110 ; 
       48 26 110 ; 
       49 2 110 ; 
       50 32 110 ; 
       51 20 110 ; 
       52 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 36 300 ; 
       0 37 300 ; 
       0 38 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       0 43 300 ; 
       0 45 300 ; 
       0 46 300 ; 
       0 47 300 ; 
       0 48 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 5 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 4 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 26 300 ; 
       5 15 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       7 53 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       8 55 300 ; 
       9 44 300 ; 
       10 35 300 ; 
       11 25 300 ; 
       12 16 300 ; 
       13 6 300 ; 
       18 56 300 ; 
       18 57 300 ; 
       18 58 300 ; 
       18 60 300 ; 
       18 61 300 ; 
       18 62 300 ; 
       18 63 300 ; 
       18 0 300 ; 
       18 1 300 ; 
       20 10 300 ; 
       21 59 300 ; 
       22 64 300 ; 
       23 75 300 ; 
       24 7 300 ; 
       25 8 300 ; 
       26 9 300 ; 
       31 49 300 ; 
       32 54 300 ; 
       35 82 300 ; 
       36 73 300 ; 
       37 74 300 ; 
       38 76 300 ; 
       39 77 300 ; 
       40 78 300 ; 
       41 79 300 ; 
       42 83 300 ; 
       43 67 300 ; 
       44 68 300 ; 
       45 69 300 ; 
       46 70 300 ; 
       47 71 300 ; 
       48 72 300 ; 
       49 81 300 ; 
       50 80 300 ; 
       51 66 300 ; 
       52 65 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 10 400 ; 
       10 31 400 ; 
       12 13 400 ; 
       20 49 400 ; 
       21 57 400 ; 
       22 58 400 ; 
       25 6 400 ; 
       32 42 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 56 401 ; 
       7 51 401 ; 
       9 50 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 55 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 54 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       48 37 401 ; 
       49 38 401 ; 
       51 40 401 ; 
       52 39 401 ; 
       53 41 401 ; 
       55 53 401 ; 
       57 43 401 ; 
       58 44 401 ; 
       60 45 401 ; 
       61 46 401 ; 
       62 47 401 ; 
       63 48 401 ; 
       75 52 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       0 4 401 ; 
       1 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       54 SCHEM 71.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 53.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 56.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 63.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 68.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 73.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 78.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 58.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 61.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 76.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 81.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 83.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 86.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 88.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 91.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 93.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 96.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 MPRFLG 0 ; 
       2 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 46.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 26.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       11 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       13 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       18 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 MPRFLG 0 ; 
       21 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       22 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       23 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       24 SCHEM 11.25 -10 0 MPRFLG 0 ; 
       25 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       26 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       31 SCHEM -1.25 -4 0 MPRFLG 0 ; 
       32 SCHEM -3.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 20 -8 0 MPRFLG 0 ; 
       35 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       36 SCHEM 23.75 -12 0 MPRFLG 0 ; 
       37 SCHEM 26.25 -12 0 MPRFLG 0 ; 
       38 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       39 SCHEM 31.25 -12 0 MPRFLG 0 ; 
       40 SCHEM 33.75 -12 0 MPRFLG 0 ; 
       41 SCHEM 36.25 -12 0 MPRFLG 0 ; 
       42 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       43 SCHEM 3.75 -12 0 MPRFLG 0 ; 
       44 SCHEM 6.25 -12 0 MPRFLG 0 ; 
       45 SCHEM 8.75 -12 0 MPRFLG 0 ; 
       46 SCHEM 11.25 -12 0 MPRFLG 0 ; 
       47 SCHEM 13.75 -12 0 MPRFLG 0 ; 
       48 SCHEM 16.25 -12 0 MPRFLG 0 ; 
       49 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       50 SCHEM -3.75 -6 0 MPRFLG 0 ; 
       51 SCHEM 18.75 -12 0 MPRFLG 0 ; 
       52 SCHEM 21.25 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 MPRFLG 0 ; 
       0 SCHEM 59 -4 0 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 24 -14 0 MPRFLG 0 ; 
       51 SCHEM 19 -14 0 MPRFLG 0 ; 
       52 SCHEM 16.5 -14 0 MPRFLG 0 ; 
       53 SCHEM 31.5 -14 0 MPRFLG 0 ; 
       54 SCHEM 34 -14 0 MPRFLG 0 ; 
       55 SCHEM 39 -14 0 MPRFLG 0 ; 
       56 SCHEM 44 -14 0 MPRFLG 0 ; 
       57 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 54 -12 0 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 91.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
