SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.38-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       bounding_model-mat1.1-0 ; 
       bounding_model-mat10.1-0 ; 
       bounding_model-mat11.1-0 ; 
       bounding_model-mat12.2-0 ; 
       bounding_model-mat13.1-0 ; 
       bounding_model-mat14.1-0 ; 
       bounding_model-mat15.1-0 ; 
       bounding_model-mat16.2-0 ; 
       bounding_model-mat17.2-0 ; 
       bounding_model-mat18.1-0 ; 
       bounding_model-mat19.1-0 ; 
       bounding_model-mat2.1-0 ; 
       bounding_model-mat20.1-0 ; 
       bounding_model-mat3.1-0 ; 
       bounding_model-mat4.1-0 ; 
       bounding_model-mat5.1-0 ; 
       bounding_model-mat6.1-0 ; 
       bounding_model-mat7.1-0 ; 
       bounding_model-mat8.1-0 ; 
       bounding_model-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       bounding_model-abound1.1-0 ; 
       bounding_model-abound2.1-0 ; 
       bounding_model-bbound.1-0 ; 
       bounding_model-bounding_model.11-0 ROOT ; 
       bounding_model-fbound1.1-0 ; 
       bounding_model-fbound2.1-0 ; 
       bounding_model-midbound1.1-0 ; 
       bounding_model-midbound2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-bounding_model.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       bounding_model-t2d1.1-0 ; 
       bounding_model-t2d10.1-0 ; 
       bounding_model-t2d11.1-0 ; 
       bounding_model-t2d12.1-0 ; 
       bounding_model-t2d13.1-0 ; 
       bounding_model-t2d14.1-0 ; 
       bounding_model-t2d15.2-0 ; 
       bounding_model-t2d16.1-0 ; 
       bounding_model-t2d17.1-0 ; 
       bounding_model-t2d18.1-0 ; 
       bounding_model-t2d2.1-0 ; 
       bounding_model-t2d3.1-0 ; 
       bounding_model-t2d4.1-0 ; 
       bounding_model-t2d5.1-0 ; 
       bounding_model-t2d6.1-0 ; 
       bounding_model-t2d7.1-0 ; 
       bounding_model-t2d8.1-0 ; 
       bounding_model-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       2 10 300 ; 
       2 12 300 ; 
       4 0 300 ; 
       4 11 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       6 18 300 ; 
       7 19 300 ; 
       7 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 16 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 17 401 ; 
       2 1 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 8 401 ; 
       8 6 401 ; 
       11 0 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 15 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       19 2 401 ; 
       9 7 401 ; 
       12 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 45 -2 0 MPRFLG 0 ; 
       2 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 35 -4 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 40 -4 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 10 -4 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 50 -4 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 35 -6 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 40 -6 0 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
