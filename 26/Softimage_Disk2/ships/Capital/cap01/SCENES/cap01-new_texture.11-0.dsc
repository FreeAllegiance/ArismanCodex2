SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.23-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 76     
       kez_frigate_F-mat1.5-0 ; 
       kez_frigate_F-mat10.5-0 ; 
       kez_frigate_F-mat11.5-0 ; 
       kez_frigate_F-mat12.5-0 ; 
       kez_frigate_F-mat13.5-0 ; 
       kez_frigate_F-mat14.5-0 ; 
       kez_frigate_F-mat15.5-0 ; 
       kez_frigate_F-mat17.5-0 ; 
       kez_frigate_F-mat18.5-0 ; 
       kez_frigate_F-mat19.5-0 ; 
       kez_frigate_F-mat2.5-0 ; 
       kez_frigate_F-mat20.5-0 ; 
       kez_frigate_F-mat21.5-0 ; 
       kez_frigate_F-mat22.5-0 ; 
       kez_frigate_F-mat23.5-0 ; 
       kez_frigate_F-mat24.5-0 ; 
       kez_frigate_F-mat25.5-0 ; 
       kez_frigate_F-mat26.5-0 ; 
       kez_frigate_F-mat28.5-0 ; 
       kez_frigate_F-mat3.5-0 ; 
       kez_frigate_F-mat30.5-0 ; 
       kez_frigate_F-mat32.5-0 ; 
       kez_frigate_F-mat33.5-0 ; 
       kez_frigate_F-mat34.5-0 ; 
       kez_frigate_F-mat35.5-0 ; 
       kez_frigate_F-mat36.5-0 ; 
       kez_frigate_F-mat37.5-0 ; 
       kez_frigate_F-mat38.5-0 ; 
       kez_frigate_F-mat39.5-0 ; 
       kez_frigate_F-mat4.5-0 ; 
       kez_frigate_F-mat40.5-0 ; 
       kez_frigate_F-mat41.5-0 ; 
       kez_frigate_F-mat43.5-0 ; 
       kez_frigate_F-mat44.5-0 ; 
       kez_frigate_F-mat45.5-0 ; 
       kez_frigate_F-mat46.5-0 ; 
       kez_frigate_F-mat48.5-0 ; 
       kez_frigate_F-mat49.5-0 ; 
       kez_frigate_F-mat5.5-0 ; 
       kez_frigate_F-mat50.5-0 ; 
       kez_frigate_F-mat51.5-0 ; 
       kez_frigate_F-mat52.5-0 ; 
       kez_frigate_F-mat53.5-0 ; 
       kez_frigate_F-mat55.7-0 ; 
       kez_frigate_F-mat59.5-0 ; 
       kez_frigate_F-mat6.5-0 ; 
       kez_frigate_F-mat7.5-0 ; 
       kez_frigate_F-mat8.5-0 ; 
       kez_frigate_F-mat80.5-0 ; 
       kez_frigate_F-mat81.5-0 ; 
       kez_frigate_F-mat82.5-0 ; 
       kez_frigate_F-mat83.5-0 ; 
       kez_frigate_F-mat84.5-0 ; 
       kez_frigate_F-mat85.5-0 ; 
       kez_frigate_F-mat86.5-0 ; 
       kez_frigate_F-mat87.5-0 ; 
       kez_frigate_F-mat88.5-0 ; 
       kez_frigate_F-mat89.5-0 ; 
       kez_frigate_F-mat9.5-0 ; 
       kez_frigate_F-mat90.5-0 ; 
       kez_frigate_F-mat91.5-0 ; 
       kez_frigate_F-mat92.5-0 ; 
       kez_frigate_F-mat93.5-0 ; 
       kez_frigate_F-mat94.5-0 ; 
       kez_frigate_F-mat95.5-0 ; 
       kez_frigate_F-port_red-left.1-0.5-0 ; 
       kez_frigate_F-starbord_green-right.1-0.5-0 ; 
       new_texture-mat100.4-0 ; 
       new_texture-mat102.3-0 ; 
       new_texture-mat103.3-0 ; 
       new_texture-mat104.2-0 ; 
       new_texture-mat105.1-0 ; 
       new_texture-mat96.2-0 ; 
       new_texture-mat97.3-0 ; 
       new_texture-mat98.2-0 ; 
       new_texture-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01.22-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-ffuselg.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-lthrust.1-0 ; 
       cap01-lwepemt1.1-0 ; 
       cap01-lwepemt2.1-0 ; 
       cap01-lwepemt3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rlndpad1.1-0 ; 
       cap01-rlndpad2.1-0 ; 
       cap01-rlndpad3.1-0 ; 
       cap01-rlndpad4.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-rthrust.1-0 ; 
       cap01-rwep1.1-0 ; 
       cap01-rwepemt1.1-0 ; 
       cap01-rwepemt2.1-0 ; 
       cap01-rwepemt3.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-slrsal0.2-0 ; 
       cap01-SSal.1-0 ; 
       cap01-SSal1.1-0 ; 
       cap01-SSal2.1-0 ; 
       cap01-SSal3.1-0 ; 
       cap01-SSal4.1-0 ; 
       cap01-SSal5.1-0 ; 
       cap01-SSal6.1-0 ; 
       cap01-SSar.1-0 ; 
       cap01-SSar1.1-0 ; 
       cap01-SSar2.1-0 ; 
       cap01-SSar3.1-0 ; 
       cap01-SSar4.1-0 ; 
       cap01-SSar5.1-0 ; 
       cap01-SSar6.1-0 ; 
       cap01-SSat.1-0 ; 
       cap01-SSfb.1-0 ; 
       cap01-SSml1.1-0 ; 
       cap01-SSmr1.1-0 ; 
       cap01-trail.1-0 ; 
       cap01-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-new_texture.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       kez_frigate_F-t2d12.5-0 ; 
       kez_frigate_F-t2d14.5-0 ; 
       kez_frigate_F-t2d15.5-0 ; 
       kez_frigate_F-t2d16.5-0 ; 
       kez_frigate_F-t2d17.5-0 ; 
       kez_frigate_F-t2d18.5-0 ; 
       kez_frigate_F-t2d19.5-0 ; 
       kez_frigate_F-t2d2.5-0 ; 
       kez_frigate_F-t2d20.5-0 ; 
       kez_frigate_F-t2d21.5-0 ; 
       kez_frigate_F-t2d22.5-0 ; 
       kez_frigate_F-t2d24.5-0 ; 
       kez_frigate_F-t2d26.5-0 ; 
       kez_frigate_F-t2d27.5-0 ; 
       kez_frigate_F-t2d28.5-0 ; 
       kez_frigate_F-t2d29.5-0 ; 
       kez_frigate_F-t2d30.5-0 ; 
       kez_frigate_F-t2d31.5-0 ; 
       kez_frigate_F-t2d32.5-0 ; 
       kez_frigate_F-t2d33.5-0 ; 
       kez_frigate_F-t2d34.5-0 ; 
       kez_frigate_F-t2d35.5-0 ; 
       kez_frigate_F-t2d36.5-0 ; 
       kez_frigate_F-t2d37.5-0 ; 
       kez_frigate_F-t2d38.5-0 ; 
       kez_frigate_F-t2d4.5-0 ; 
       kez_frigate_F-t2d40.5-0 ; 
       kez_frigate_F-t2d41.5-0 ; 
       kez_frigate_F-t2d42.5-0 ; 
       kez_frigate_F-t2d43.5-0 ; 
       kez_frigate_F-t2d44.5-0 ; 
       kez_frigate_F-t2d45.5-0 ; 
       kez_frigate_F-t2d7.5-0 ; 
       kez_frigate_F-t2d70.5-0 ; 
       kez_frigate_F-t2d71.5-0 ; 
       kez_frigate_F-t2d72.5-0 ; 
       kez_frigate_F-t2d73.5-0 ; 
       kez_frigate_F-t2d74.5-0 ; 
       kez_frigate_F-t2d75.5-0 ; 
       kez_frigate_F-t2d76.5-0 ; 
       kez_frigate_F-t2d8.5-0 ; 
       kez_frigate_F-t2d9.5-0 ; 
       new_texture-t2d77.2-0 ; 
       new_texture-t2d78.3-0 ; 
       new_texture-t2d79.2-0 ; 
       new_texture-t2d80.2-0 ; 
       new_texture-t2d81.2-0 ; 
       new_texture-t2d84.4-0 ; 
       new_texture-t2d85.2-0 ; 
       new_texture-t2d86.1-0 ; 
       new_texture-t2d87.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 18 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 4 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 3 110 ; 
       19 37 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 6 110 ; 
       31 7 110 ; 
       32 31 110 ; 
       33 7 110 ; 
       34 7 110 ; 
       35 18 110 ; 
       36 18 110 ; 
       37 1 110 ; 
       38 0 110 ; 
       39 8 110 ; 
       40 9 110 ; 
       41 10 110 ; 
       42 11 110 ; 
       43 12 110 ; 
       44 13 110 ; 
       45 0 110 ; 
       46 24 110 ; 
       47 25 110 ; 
       48 26 110 ; 
       49 27 110 ; 
       50 28 110 ; 
       51 29 110 ; 
       52 2 110 ; 
       53 36 110 ; 
       54 19 110 ; 
       55 19 110 ; 
       56 3 110 ; 
       57 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 20 300 ; 
       5 9 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 43 300 ; 
       7 73 300 ; 
       7 74 300 ; 
       7 75 300 ; 
       8 45 300 ; 
       9 38 300 ; 
       10 29 300 ; 
       11 19 300 ; 
       12 10 300 ; 
       13 0 300 ; 
       18 67 300 ; 
       18 68 300 ; 
       18 69 300 ; 
       18 70 300 ; 
       19 4 300 ; 
       24 46 300 ; 
       25 47 300 ; 
       26 58 300 ; 
       27 1 300 ; 
       28 2 300 ; 
       29 3 300 ; 
       31 72 300 ; 
       35 71 300 ; 
       36 44 300 ; 
       38 65 300 ; 
       39 56 300 ; 
       40 57 300 ; 
       41 59 300 ; 
       42 60 300 ; 
       43 61 300 ; 
       44 62 300 ; 
       45 66 300 ; 
       46 50 300 ; 
       47 51 300 ; 
       48 52 300 ; 
       49 53 300 ; 
       50 54 300 ; 
       51 55 300 ; 
       52 64 300 ; 
       53 63 300 ; 
       54 49 300 ; 
       55 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 4 400 ; 
       10 25 400 ; 
       12 7 400 ; 
       19 32 400 ; 
       24 40 400 ; 
       25 41 400 ; 
       28 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 39 401 ; 
       1 34 401 ; 
       3 33 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       18 11 401 ; 
       19 38 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 37 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       71 50 401 ; 
       44 46 401 ; 
       45 36 401 ; 
       58 35 401 ; 
       68 49 401 ; 
       69 47 401 ; 
       70 48 401 ; 
       72 42 401 ; 
       73 43 401 ; 
       74 44 401 ; 
       75 45 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 135 -4 0 MPRFLG 0 ; 
       1 SCHEM 122.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 171.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 122.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 158.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 148.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 137.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 98.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 103.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 110 -10 0 MPRFLG 0 ; 
       11 SCHEM 116.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 122.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 128.75 -10 0 MPRFLG 0 ; 
       14 SCHEM 155 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 121.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 45 -6 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 40 -6 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 60 -10 0 MPRFLG 0 ; 
       26 SCHEM 66.25 -10 0 MPRFLG 0 ; 
       27 SCHEM 71.25 -10 0 MPRFLG 0 ; 
       28 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 83.75 -10 0 MPRFLG 0 ; 
       30 SCHEM 132.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       37 SCHEM 90 -8 0 MPRFLG 0 ; 
       38 SCHEM 200 -6 0 MPRFLG 0 ; 
       39 SCHEM 97.5 -12 0 MPRFLG 0 ; 
       40 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       41 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       42 SCHEM 115 -12 0 MPRFLG 0 ; 
       43 SCHEM 120 -12 0 MPRFLG 0 ; 
       44 SCHEM 127.5 -12 0 MPRFLG 0 ; 
       45 SCHEM 197.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 50 -12 0 MPRFLG 0 ; 
       47 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 65 -12 0 MPRFLG 0 ; 
       49 SCHEM 70 -12 0 MPRFLG 0 ; 
       50 SCHEM 75 -12 0 MPRFLG 0 ; 
       51 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 165 -10 0 MPRFLG 0 ; 
       53 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       55 SCHEM 90 -12 0 MPRFLG 0 ; 
       56 SCHEM 242.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 10 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 125 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 167.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 170 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 172.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 175 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 205 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 207.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 210 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 202.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 222.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 107.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 115 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 120 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 127.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 165 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 232.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 235 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 237.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 240 -4 0 MPRFLG 0 ; 
       72 SCHEM 20 -8 0 MPRFLG 0 ; 
       73 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       74 SCHEM 35 -6 0 MPRFLG 0 ; 
       75 SCHEM 37.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 147.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 167.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 170 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 172.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 175 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 205 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 207.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 210 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 212.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 202.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 220 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 222.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 225 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 227.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 230 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 245 0 0 MPRFLG 0 ; 
       32 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 85 -14 0 MPRFLG 0 ; 
       34 SCHEM 72.5 -14 0 MPRFLG 0 ; 
       35 SCHEM 67.5 -14 0 MPRFLG 0 ; 
       36 SCHEM 100 -14 0 MPRFLG 0 ; 
       37 SCHEM 105 -14 0 MPRFLG 0 ; 
       38 SCHEM 117.5 -14 0 MPRFLG 0 ; 
       39 SCHEM 130 -14 0 MPRFLG 0 ; 
       40 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 20 -10 0 MPRFLG 0 ; 
       43 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       44 SCHEM 35 -8 0 MPRFLG 0 ; 
       45 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       46 SCHEM 5 -8 0 MPRFLG 0 ; 
       47 SCHEM 237.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 240 -6 0 MPRFLG 0 ; 
       49 SCHEM 235 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 244 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
