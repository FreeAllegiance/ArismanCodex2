SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01_2.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.56-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       kez_frigate_F-mat1.4-0 ; 
       kez_frigate_F-mat10.4-0 ; 
       kez_frigate_F-mat11.4-0 ; 
       kez_frigate_F-mat12.4-0 ; 
       kez_frigate_F-mat13.4-0 ; 
       kez_frigate_F-mat14.7-0 ; 
       kez_frigate_F-mat15.7-0 ; 
       kez_frigate_F-mat17.7-0 ; 
       kez_frigate_F-mat18.7-0 ; 
       kez_frigate_F-mat19.4-0 ; 
       kez_frigate_F-mat2.4-0 ; 
       kez_frigate_F-mat20.4-0 ; 
       kez_frigate_F-mat21.4-0 ; 
       kez_frigate_F-mat22.4-0 ; 
       kez_frigate_F-mat23.4-0 ; 
       kez_frigate_F-mat24.4-0 ; 
       kez_frigate_F-mat25.4-0 ; 
       kez_frigate_F-mat26.4-0 ; 
       kez_frigate_F-mat28.4-0 ; 
       kez_frigate_F-mat3.4-0 ; 
       kez_frigate_F-mat30.4-0 ; 
       kez_frigate_F-mat32.7-0 ; 
       kez_frigate_F-mat33.7-0 ; 
       kez_frigate_F-mat34.7-0 ; 
       kez_frigate_F-mat35.7-0 ; 
       kez_frigate_F-mat36.7-0 ; 
       kez_frigate_F-mat37.7-0 ; 
       kez_frigate_F-mat38.7-0 ; 
       kez_frigate_F-mat39.7-0 ; 
       kez_frigate_F-mat4.4-0 ; 
       kez_frigate_F-mat40.4-0 ; 
       kez_frigate_F-mat41.4-0 ; 
       kez_frigate_F-mat43.4-0 ; 
       kez_frigate_F-mat44.4-0 ; 
       kez_frigate_F-mat45.4-0 ; 
       kez_frigate_F-mat46.4-0 ; 
       kez_frigate_F-mat48.4-0 ; 
       kez_frigate_F-mat49.4-0 ; 
       kez_frigate_F-mat5.4-0 ; 
       kez_frigate_F-mat50.4-0 ; 
       kez_frigate_F-mat51.4-0 ; 
       kez_frigate_F-mat52.4-0 ; 
       kez_frigate_F-mat53.4-0 ; 
       kez_frigate_F-mat54.4-0 ; 
       kez_frigate_F-mat55.9-0 ; 
       kez_frigate_F-mat56.6-0 ; 
       kez_frigate_F-mat57.6-0 ; 
       kez_frigate_F-mat58.6-0 ; 
       kez_frigate_F-mat59.4-0 ; 
       kez_frigate_F-mat6.4-0 ; 
       kez_frigate_F-mat60.6-0 ; 
       kez_frigate_F-mat66.6-0 ; 
       kez_frigate_F-mat69.6-0 ; 
       kez_frigate_F-mat7.4-0 ; 
       kez_frigate_F-mat70.6-0 ; 
       kez_frigate_F-mat71.6-0 ; 
       kez_frigate_F-mat72.6-0 ; 
       kez_frigate_F-mat74.6-0 ; 
       kez_frigate_F-mat8.4-0 ; 
       kez_frigate_F-mat9.4-0 ; 
       static-mat100.1-0 ; 
       static-mat101.1-0 ; 
       static-mat96.2-0 ; 
       static-mat97.1-0 ; 
       static-mat98.1-0 ; 
       static-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01_2.10-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-ffuselg.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-slrsal0.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       kez_frigate_F-t2d12.4-0 ; 
       kez_frigate_F-t2d14.8-0 ; 
       kez_frigate_F-t2d15.8-0 ; 
       kez_frigate_F-t2d16.8-0 ; 
       kez_frigate_F-t2d17.4-0 ; 
       kez_frigate_F-t2d18.4-0 ; 
       kez_frigate_F-t2d19.4-0 ; 
       kez_frigate_F-t2d2.4-0 ; 
       kez_frigate_F-t2d20.4-0 ; 
       kez_frigate_F-t2d21.4-0 ; 
       kez_frigate_F-t2d22.4-0 ; 
       kez_frigate_F-t2d24.4-0 ; 
       kez_frigate_F-t2d26.4-0 ; 
       kez_frigate_F-t2d27.8-0 ; 
       kez_frigate_F-t2d28.8-0 ; 
       kez_frigate_F-t2d29.8-0 ; 
       kez_frigate_F-t2d30.7-0 ; 
       kez_frigate_F-t2d31.7-0 ; 
       kez_frigate_F-t2d32.7-0 ; 
       kez_frigate_F-t2d33.7-0 ; 
       kez_frigate_F-t2d34.4-0 ; 
       kez_frigate_F-t2d35.4-0 ; 
       kez_frigate_F-t2d36.4-0 ; 
       kez_frigate_F-t2d37.4-0 ; 
       kez_frigate_F-t2d38.4-0 ; 
       kez_frigate_F-t2d4.4-0 ; 
       kez_frigate_F-t2d40.4-0 ; 
       kez_frigate_F-t2d41.4-0 ; 
       kez_frigate_F-t2d42.4-0 ; 
       kez_frigate_F-t2d43.4-0 ; 
       kez_frigate_F-t2d44.4-0 ; 
       kez_frigate_F-t2d45.4-0 ; 
       kez_frigate_F-t2d46.4-0 ; 
       kez_frigate_F-t2d48.6-0 ; 
       kez_frigate_F-t2d49.6-0 ; 
       kez_frigate_F-t2d50.6-0 ; 
       kez_frigate_F-t2d51.4-0 ; 
       kez_frigate_F-t2d55.7-0 ; 
       kez_frigate_F-t2d58.7-0 ; 
       kez_frigate_F-t2d59.7-0 ; 
       kez_frigate_F-t2d60.7-0 ; 
       kez_frigate_F-t2d61.7-0 ; 
       kez_frigate_F-t2d62.7-0 ; 
       kez_frigate_F-t2d7.4-0 ; 
       kez_frigate_F-t2d70.4-0 ; 
       kez_frigate_F-t2d71.4-0 ; 
       kez_frigate_F-t2d72.4-0 ; 
       kez_frigate_F-t2d73.4-0 ; 
       kez_frigate_F-t2d74.4-0 ; 
       kez_frigate_F-t2d75.4-0 ; 
       kez_frigate_F-t2d76.4-0 ; 
       kez_frigate_F-t2d8.4-0 ; 
       kez_frigate_F-t2d9.4-0 ; 
       static-t2d77.2-0 ; 
       static-t2d78.1-0 ; 
       static-t2d79.1-0 ; 
       static-t2d80.1-0 ; 
       static-t2d81.1-0 ; 
       static-t2d82.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 14 110 ; 
       8 24 110 ; 
       9 24 110 ; 
       10 24 110 ; 
       11 24 110 ; 
       12 24 110 ; 
       13 24 110 ; 
       14 3 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       21 24 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 65 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 64 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 20 300 ; 
       5 9 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 44 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       7 47 300 ; 
       7 62 300 ; 
       7 63 300 ; 
       8 49 300 ; 
       9 38 300 ; 
       10 29 300 ; 
       11 19 300 ; 
       12 10 300 ; 
       13 0 300 ; 
       14 50 300 ; 
       14 51 300 ; 
       14 52 300 ; 
       14 54 300 ; 
       14 55 300 ; 
       14 56 300 ; 
       14 57 300 ; 
       14 60 300 ; 
       14 61 300 ; 
       15 4 300 ; 
       16 53 300 ; 
       17 58 300 ; 
       18 59 300 ; 
       19 1 300 ; 
       20 2 300 ; 
       21 3 300 ; 
       22 43 300 ; 
       23 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 4 400 ; 
       10 25 400 ; 
       12 7 400 ; 
       15 43 400 ; 
       16 51 400 ; 
       17 52 400 ; 
       20 0 400 ; 
       23 36 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 50 401 ; 
       1 45 401 ; 
       3 44 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       18 11 401 ; 
       19 49 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 48 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       45 34 401 ; 
       46 33 401 ; 
       47 35 401 ; 
       49 47 401 ; 
       51 37 401 ; 
       52 38 401 ; 
       54 39 401 ; 
       55 40 401 ; 
       56 41 401 ; 
       57 42 401 ; 
       59 46 401 ; 
       62 53 401 ; 
       63 54 401 ; 
       64 55 401 ; 
       65 56 401 ; 
       60 57 401 ; 
       61 58 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 MPRFLG 0 ; 
       2 SCHEM 50 -8 0 MPRFLG 0 ; 
       3 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 45 -8 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 30 -10 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 35 -10 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 40 -10 0 MPRFLG 0 ; 
       14 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 25 -10 0 MPRFLG 0 ; 
       16 SCHEM 10 -10 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 15 -10 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 5 -4 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       63 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       64 SCHEM 49 -10 0 MPRFLG 0 ; 
       65 SCHEM 51.5 -8 0 MPRFLG 0 ; 
       60 SCHEM 51.5 -4 0 MPRFLG 0 ; 
       61 SCHEM 51.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -14 0 MPRFLG 0 ; 
       45 SCHEM 16.5 -14 0 MPRFLG 0 ; 
       46 SCHEM 14 -14 0 MPRFLG 0 ; 
       47 SCHEM 26.5 -14 0 MPRFLG 0 ; 
       48 SCHEM 29 -14 0 MPRFLG 0 ; 
       49 SCHEM 34 -14 0 MPRFLG 0 ; 
       50 SCHEM 39 -14 0 MPRFLG 0 ; 
       51 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -8 0 MPRFLG 0 ; 
       54 SCHEM 6.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 49 -12 0 MPRFLG 0 ; 
       56 SCHEM 51.5 -10 0 MPRFLG 0 ; 
       57 SCHEM 51.5 -6 0 MPRFLG 0 ; 
       58 SCHEM 51.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
