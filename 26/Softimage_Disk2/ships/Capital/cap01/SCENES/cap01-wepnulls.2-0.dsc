SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.12-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 79     
       kez_frigate_F-mat1.4-0 ; 
       kez_frigate_F-mat10.4-0 ; 
       kez_frigate_F-mat11.4-0 ; 
       kez_frigate_F-mat12.4-0 ; 
       kez_frigate_F-mat13.4-0 ; 
       kez_frigate_F-mat14.4-0 ; 
       kez_frigate_F-mat15.4-0 ; 
       kez_frigate_F-mat17.4-0 ; 
       kez_frigate_F-mat18.4-0 ; 
       kez_frigate_F-mat19.4-0 ; 
       kez_frigate_F-mat2.4-0 ; 
       kez_frigate_F-mat20.4-0 ; 
       kez_frigate_F-mat21.4-0 ; 
       kez_frigate_F-mat22.4-0 ; 
       kez_frigate_F-mat23.4-0 ; 
       kez_frigate_F-mat24.4-0 ; 
       kez_frigate_F-mat25.4-0 ; 
       kez_frigate_F-mat26.4-0 ; 
       kez_frigate_F-mat28.4-0 ; 
       kez_frigate_F-mat3.4-0 ; 
       kez_frigate_F-mat30.4-0 ; 
       kez_frigate_F-mat32.4-0 ; 
       kez_frigate_F-mat33.4-0 ; 
       kez_frigate_F-mat34.4-0 ; 
       kez_frigate_F-mat35.4-0 ; 
       kez_frigate_F-mat36.4-0 ; 
       kez_frigate_F-mat37.4-0 ; 
       kez_frigate_F-mat38.4-0 ; 
       kez_frigate_F-mat39.4-0 ; 
       kez_frigate_F-mat4.4-0 ; 
       kez_frigate_F-mat40.4-0 ; 
       kez_frigate_F-mat41.4-0 ; 
       kez_frigate_F-mat43.4-0 ; 
       kez_frigate_F-mat44.4-0 ; 
       kez_frigate_F-mat45.4-0 ; 
       kez_frigate_F-mat46.4-0 ; 
       kez_frigate_F-mat48.4-0 ; 
       kez_frigate_F-mat49.4-0 ; 
       kez_frigate_F-mat5.4-0 ; 
       kez_frigate_F-mat50.4-0 ; 
       kez_frigate_F-mat51.4-0 ; 
       kez_frigate_F-mat52.4-0 ; 
       kez_frigate_F-mat53.4-0 ; 
       kez_frigate_F-mat54.4-0 ; 
       kez_frigate_F-mat55.4-0 ; 
       kez_frigate_F-mat56.4-0 ; 
       kez_frigate_F-mat57.4-0 ; 
       kez_frigate_F-mat58.4-0 ; 
       kez_frigate_F-mat59.4-0 ; 
       kez_frigate_F-mat6.4-0 ; 
       kez_frigate_F-mat60.4-0 ; 
       kez_frigate_F-mat66.4-0 ; 
       kez_frigate_F-mat69.4-0 ; 
       kez_frigate_F-mat7.4-0 ; 
       kez_frigate_F-mat70.4-0 ; 
       kez_frigate_F-mat71.4-0 ; 
       kez_frigate_F-mat72.4-0 ; 
       kez_frigate_F-mat74.4-0 ; 
       kez_frigate_F-mat8.4-0 ; 
       kez_frigate_F-mat80.4-0 ; 
       kez_frigate_F-mat81.4-0 ; 
       kez_frigate_F-mat82.4-0 ; 
       kez_frigate_F-mat83.4-0 ; 
       kez_frigate_F-mat84.4-0 ; 
       kez_frigate_F-mat85.4-0 ; 
       kez_frigate_F-mat86.4-0 ; 
       kez_frigate_F-mat87.4-0 ; 
       kez_frigate_F-mat88.4-0 ; 
       kez_frigate_F-mat89.4-0 ; 
       kez_frigate_F-mat9.4-0 ; 
       kez_frigate_F-mat90.4-0 ; 
       kez_frigate_F-mat91.4-0 ; 
       kez_frigate_F-mat92.4-0 ; 
       kez_frigate_F-mat93.4-0 ; 
       kez_frigate_F-mat94.4-0 ; 
       kez_frigate_F-mat95.4-0 ; 
       kez_frigate_F-port_red-left.1-0.4-0 ; 
       kez_frigate_F-starbord_green-right.1-0.4-0 ; 
       wepnulls-mat96.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       cap01-afuselg1.1-0 ; 
       cap01-afuselg2.1-0 ; 
       cap01-atfuselg.1-0 ; 
       cap01-cap01.12-0 ROOT ; 
       cap01-engine1.3-0 ; 
       cap01-engine2.1-0 ; 
       cap01-engine3.1-0 ; 
       cap01-ffuselg.1-0 ; 
       cap01-lslrsal1.1-0 ; 
       cap01-lslrsal2.1-0 ; 
       cap01-lslrsal3.1-0 ; 
       cap01-lslrsal4.1-0 ; 
       cap01-lslrsal5.1-0 ; 
       cap01-lslrsal6.1-0 ; 
       cap01-lthrust.1-0 ; 
       cap01-lwepemt1.1-0 ; 
       cap01-lwepemt2.1-0 ; 
       cap01-lwepemt3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-mslrsal.1-0 ; 
       cap01-rlndpad1.1-0 ; 
       cap01-rlndpad2.1-0 ; 
       cap01-rlndpad3.1-0 ; 
       cap01-rlndpad4.1-0 ; 
       cap01-rslrsal1.1-0 ; 
       cap01-rslrsal2.1-0 ; 
       cap01-rslrsal3.1-0 ; 
       cap01-rslrsal4.1-0 ; 
       cap01-rslrsal5.1-0 ; 
       cap01-rslrsal6.1-0 ; 
       cap01-rthrust.1-0 ; 
       cap01-rwep1.1-0 ; 
       cap01-rwepemt1.1-0 ; 
       cap01-rwepemt2.1-0 ; 
       cap01-rwepemt3.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-slrsal0.2-0 ; 
       cap01-SSal.1-0 ; 
       cap01-SSal1.1-0 ; 
       cap01-SSal2.1-0 ; 
       cap01-SSal3.1-0 ; 
       cap01-SSal4.1-0 ; 
       cap01-SSal5.1-0 ; 
       cap01-SSal6.1-0 ; 
       cap01-SSar.1-0 ; 
       cap01-SSar1.1-0 ; 
       cap01-SSar2.1-0 ; 
       cap01-SSar3.1-0 ; 
       cap01-SSar4.1-0 ; 
       cap01-SSar5.1-0 ; 
       cap01-SSar6.1-0 ; 
       cap01-SSat.1-0 ; 
       cap01-SSfb.1-0 ; 
       cap01-SSml1.1-0 ; 
       cap01-SSmr1.1-0 ; 
       cap01-trail.1-0 ; 
       cap01-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-wepnulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 54     
       kez_frigate_F-t2d12.4-0 ; 
       kez_frigate_F-t2d14.4-0 ; 
       kez_frigate_F-t2d15.4-0 ; 
       kez_frigate_F-t2d16.4-0 ; 
       kez_frigate_F-t2d17.4-0 ; 
       kez_frigate_F-t2d18.4-0 ; 
       kez_frigate_F-t2d19.4-0 ; 
       kez_frigate_F-t2d2.4-0 ; 
       kez_frigate_F-t2d20.4-0 ; 
       kez_frigate_F-t2d21.4-0 ; 
       kez_frigate_F-t2d22.4-0 ; 
       kez_frigate_F-t2d24.4-0 ; 
       kez_frigate_F-t2d26.4-0 ; 
       kez_frigate_F-t2d27.4-0 ; 
       kez_frigate_F-t2d28.4-0 ; 
       kez_frigate_F-t2d29.4-0 ; 
       kez_frigate_F-t2d30.4-0 ; 
       kez_frigate_F-t2d31.4-0 ; 
       kez_frigate_F-t2d32.4-0 ; 
       kez_frigate_F-t2d33.4-0 ; 
       kez_frigate_F-t2d34.4-0 ; 
       kez_frigate_F-t2d35.4-0 ; 
       kez_frigate_F-t2d36.4-0 ; 
       kez_frigate_F-t2d37.4-0 ; 
       kez_frigate_F-t2d38.4-0 ; 
       kez_frigate_F-t2d4.4-0 ; 
       kez_frigate_F-t2d40.4-0 ; 
       kez_frigate_F-t2d41.4-0 ; 
       kez_frigate_F-t2d42.4-0 ; 
       kez_frigate_F-t2d43.4-0 ; 
       kez_frigate_F-t2d44.4-0 ; 
       kez_frigate_F-t2d45.4-0 ; 
       kez_frigate_F-t2d46.4-0 ; 
       kez_frigate_F-t2d48.4-0 ; 
       kez_frigate_F-t2d49.4-0 ; 
       kez_frigate_F-t2d50.4-0 ; 
       kez_frigate_F-t2d51.4-0 ; 
       kez_frigate_F-t2d55.4-0 ; 
       kez_frigate_F-t2d58.4-0 ; 
       kez_frigate_F-t2d59.4-0 ; 
       kez_frigate_F-t2d60.4-0 ; 
       kez_frigate_F-t2d61.4-0 ; 
       kez_frigate_F-t2d62.4-0 ; 
       kez_frigate_F-t2d7.4-0 ; 
       kez_frigate_F-t2d70.4-0 ; 
       kez_frigate_F-t2d71.4-0 ; 
       kez_frigate_F-t2d72.4-0 ; 
       kez_frigate_F-t2d73.4-0 ; 
       kez_frigate_F-t2d74.4-0 ; 
       kez_frigate_F-t2d75.4-0 ; 
       kez_frigate_F-t2d76.4-0 ; 
       kez_frigate_F-t2d8.4-0 ; 
       kez_frigate_F-t2d9.4-0 ; 
       wepnulls-t2d77.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 18 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       56 3 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 3 110 ; 
       19 37 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       32 31 110 ; 
       33 7 110 ; 
       34 7 110 ; 
       35 18 110 ; 
       36 18 110 ; 
       37 1 110 ; 
       38 0 110 ; 
       39 8 110 ; 
       40 9 110 ; 
       41 10 110 ; 
       30 6 110 ; 
       14 4 110 ; 
       31 7 110 ; 
       42 11 110 ; 
       43 12 110 ; 
       44 13 110 ; 
       45 0 110 ; 
       46 24 110 ; 
       47 25 110 ; 
       48 26 110 ; 
       49 27 110 ; 
       50 28 110 ; 
       51 29 110 ; 
       52 2 110 ; 
       53 36 110 ; 
       54 19 110 ; 
       55 19 110 ; 
       57 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 20 300 ; 
       5 9 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 44 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       7 47 300 ; 
       8 49 300 ; 
       9 38 300 ; 
       10 29 300 ; 
       11 19 300 ; 
       12 10 300 ; 
       13 0 300 ; 
       18 50 300 ; 
       18 51 300 ; 
       18 52 300 ; 
       18 54 300 ; 
       18 55 300 ; 
       18 56 300 ; 
       18 57 300 ; 
       19 4 300 ; 
       24 53 300 ; 
       25 58 300 ; 
       26 69 300 ; 
       27 1 300 ; 
       28 2 300 ; 
       29 3 300 ; 
       35 43 300 ; 
       36 48 300 ; 
       38 76 300 ; 
       39 67 300 ; 
       40 68 300 ; 
       41 70 300 ; 
       31 78 300 ; 
       42 71 300 ; 
       43 72 300 ; 
       44 73 300 ; 
       45 77 300 ; 
       46 61 300 ; 
       47 62 300 ; 
       48 63 300 ; 
       49 64 300 ; 
       50 65 300 ; 
       51 66 300 ; 
       52 75 300 ; 
       53 74 300 ; 
       54 60 300 ; 
       55 59 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 4 400 ; 
       10 25 400 ; 
       12 7 400 ; 
       19 43 400 ; 
       24 51 400 ; 
       25 52 400 ; 
       28 0 400 ; 
       36 36 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 50 401 ; 
       1 45 401 ; 
       3 44 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       18 11 401 ; 
       19 49 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 48 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       45 34 401 ; 
       46 33 401 ; 
       47 35 401 ; 
       49 47 401 ; 
       51 37 401 ; 
       52 38 401 ; 
       54 39 401 ; 
       55 40 401 ; 
       56 41 401 ; 
       57 42 401 ; 
       69 46 401 ; 
       78 53 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 137.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 125 -6 0 MPRFLG 0 ; 
       2 SCHEM 173.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 127.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 161.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 151.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 140 -8 0 MPRFLG 0 ; 
       7 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 101.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 106.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 112.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 118.75 -10 0 MPRFLG 0 ; 
       56 SCHEM 252.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 125 -10 0 MPRFLG 0 ; 
       13 SCHEM 131.25 -10 0 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 93.75 -10 0 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 45 -6 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 55 -10 0 MPRFLG 0 ; 
       25 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       27 SCHEM 73.75 -10 0 MPRFLG 0 ; 
       28 SCHEM 80 -10 0 MPRFLG 0 ; 
       29 SCHEM 86.25 -10 0 MPRFLG 0 ; 
       32 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10 -4 0 MPRFLG 0 ; 
       36 SCHEM 5 -4 0 MPRFLG 0 ; 
       37 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 202.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 100 -12 0 MPRFLG 0 ; 
       40 SCHEM 105 -12 0 MPRFLG 0 ; 
       41 SCHEM 110 -12 0 MPRFLG 0 ; 
       30 SCHEM 135 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 157.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 21.25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       42 SCHEM 117.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 122.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 130 -12 0 MPRFLG 0 ; 
       45 SCHEM 200 -6 0 MPRFLG 0 ; 
       46 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       47 SCHEM 60 -12 0 MPRFLG 0 ; 
       48 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       49 SCHEM 72.5 -12 0 MPRFLG 0 ; 
       50 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 85 -12 0 MPRFLG 0 ; 
       52 SCHEM 167.5 -10 0 MPRFLG 0 ; 
       53 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 90 -12 0 MPRFLG 0 ; 
       55 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       57 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 165 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 180 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 170 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 172.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 175 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 207.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 210 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 205 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 222.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 250 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 235 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 237.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 240 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 242.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 245 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 247.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 100 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 105 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 110 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 117.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 122.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 202.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 22.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 147.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 150 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 142.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 162.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 195 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 170 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 172.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 175 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 177.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 207.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 210 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 212.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 217.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 205 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 222.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 225 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 227.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 230 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 232.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 235 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 237.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 240 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 242.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 245 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 247.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -14 0 MPRFLG 0 ; 
       45 SCHEM 75 -14 0 MPRFLG 0 ; 
       46 SCHEM 70 -14 0 MPRFLG 0 ; 
       47 SCHEM 102.5 -14 0 MPRFLG 0 ; 
       48 SCHEM 107.5 -14 0 MPRFLG 0 ; 
       49 SCHEM 120 -14 0 MPRFLG 0 ; 
       50 SCHEM 132.5 -14 0 MPRFLG 0 ; 
       51 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 22.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 254 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
