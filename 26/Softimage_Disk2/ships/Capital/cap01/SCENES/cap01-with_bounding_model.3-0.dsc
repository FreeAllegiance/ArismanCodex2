SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       with_bounding_model-cap01_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.27-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 78     
       kez_frigate_F-mat1.6-0 ; 
       kez_frigate_F-mat10.6-0 ; 
       kez_frigate_F-mat11.6-0 ; 
       kez_frigate_F-mat12.6-0 ; 
       kez_frigate_F-mat13.6-0 ; 
       kez_frigate_F-mat14.6-0 ; 
       kez_frigate_F-mat15.6-0 ; 
       kez_frigate_F-mat17.6-0 ; 
       kez_frigate_F-mat18.6-0 ; 
       kez_frigate_F-mat19.6-0 ; 
       kez_frigate_F-mat2.6-0 ; 
       kez_frigate_F-mat20.6-0 ; 
       kez_frigate_F-mat21.6-0 ; 
       kez_frigate_F-mat22.6-0 ; 
       kez_frigate_F-mat23.6-0 ; 
       kez_frigate_F-mat24.6-0 ; 
       kez_frigate_F-mat25.6-0 ; 
       kez_frigate_F-mat26.6-0 ; 
       kez_frigate_F-mat28.6-0 ; 
       kez_frigate_F-mat3.6-0 ; 
       kez_frigate_F-mat30.6-0 ; 
       kez_frigate_F-mat32.6-0 ; 
       kez_frigate_F-mat33.6-0 ; 
       kez_frigate_F-mat34.6-0 ; 
       kez_frigate_F-mat35.6-0 ; 
       kez_frigate_F-mat36.6-0 ; 
       kez_frigate_F-mat37.6-0 ; 
       kez_frigate_F-mat38.6-0 ; 
       kez_frigate_F-mat39.6-0 ; 
       kez_frigate_F-mat4.6-0 ; 
       kez_frigate_F-mat40.6-0 ; 
       kez_frigate_F-mat41.6-0 ; 
       kez_frigate_F-mat43.6-0 ; 
       kez_frigate_F-mat44.6-0 ; 
       kez_frigate_F-mat45.6-0 ; 
       kez_frigate_F-mat46.6-0 ; 
       kez_frigate_F-mat48.6-0 ; 
       kez_frigate_F-mat49.6-0 ; 
       kez_frigate_F-mat5.6-0 ; 
       kez_frigate_F-mat50.6-0 ; 
       kez_frigate_F-mat51.6-0 ; 
       kez_frigate_F-mat52.6-0 ; 
       kez_frigate_F-mat53.6-0 ; 
       kez_frigate_F-mat54.6-0 ; 
       kez_frigate_F-mat55.8-0 ; 
       kez_frigate_F-mat56.5-0 ; 
       kez_frigate_F-mat57.5-0 ; 
       kez_frigate_F-mat58.5-0 ; 
       kez_frigate_F-mat59.6-0 ; 
       kez_frigate_F-mat6.6-0 ; 
       kez_frigate_F-mat60.5-0 ; 
       kez_frigate_F-mat66.5-0 ; 
       kez_frigate_F-mat69.5-0 ; 
       kez_frigate_F-mat7.6-0 ; 
       kez_frigate_F-mat70.5-0 ; 
       kez_frigate_F-mat71.5-0 ; 
       kez_frigate_F-mat72.5-0 ; 
       kez_frigate_F-mat74.5-0 ; 
       kez_frigate_F-mat8.6-0 ; 
       kez_frigate_F-mat80.6-0 ; 
       kez_frigate_F-mat81.6-0 ; 
       kez_frigate_F-mat82.6-0 ; 
       kez_frigate_F-mat83.6-0 ; 
       kez_frigate_F-mat84.6-0 ; 
       kez_frigate_F-mat85.6-0 ; 
       kez_frigate_F-mat86.6-0 ; 
       kez_frigate_F-mat87.6-0 ; 
       kez_frigate_F-mat88.6-0 ; 
       kez_frigate_F-mat89.6-0 ; 
       kez_frigate_F-mat9.6-0 ; 
       kez_frigate_F-mat90.6-0 ; 
       kez_frigate_F-mat91.6-0 ; 
       kez_frigate_F-mat92.6-0 ; 
       kez_frigate_F-mat93.6-0 ; 
       kez_frigate_F-mat94.6-0 ; 
       kez_frigate_F-mat95.6-0 ; 
       kez_frigate_F-port_red-left.1-0.6-0 ; 
       kez_frigate_F-starbord_green-right.1-0.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 62     
       with_bounding_model-abound1.1-0 ; 
       with_bounding_model-abound2.1-0 ; 
       with_bounding_model-afuselg1.1-0 ; 
       with_bounding_model-afuselg2.1-0 ; 
       with_bounding_model-atfuselg.1-0 ; 
       with_bounding_model-bbound.1-0 ; 
       with_bounding_model-bounding_model.1-0 ROOT ; 
       with_bounding_model-cap01_1.1-0 ROOT ; 
       with_bounding_model-engine1.3-0 ; 
       with_bounding_model-engine2.1-0 ; 
       with_bounding_model-engine3.1-0 ; 
       with_bounding_model-fbound1.1-0 ; 
       with_bounding_model-fbound2.1-0 ; 
       with_bounding_model-ffuselg.1-0 ; 
       with_bounding_model-lslrsal1.1-0 ; 
       with_bounding_model-lslrsal2.1-0 ; 
       with_bounding_model-lslrsal3.1-0 ; 
       with_bounding_model-lslrsal4.1-0 ; 
       with_bounding_model-lslrsal5.1-0 ; 
       with_bounding_model-lslrsal6.1-0 ; 
       with_bounding_model-lwepemt1.1-0 ; 
       with_bounding_model-lwepemt2.1-0 ; 
       with_bounding_model-lwepemt3.1-0 ; 
       with_bounding_model-mfuselg.1-0 ; 
       with_bounding_model-midbound1.1-0 ; 
       with_bounding_model-midbound2.1-0 ; 
       with_bounding_model-mslrsal.1-0 ; 
       with_bounding_model-rlndpad1.1-0 ; 
       with_bounding_model-rlndpad2.1-0 ; 
       with_bounding_model-rlndpad3.1-0 ; 
       with_bounding_model-rlndpad4.1-0 ; 
       with_bounding_model-rslrsal1.1-0 ; 
       with_bounding_model-rslrsal2.1-0 ; 
       with_bounding_model-rslrsal3.1-0 ; 
       with_bounding_model-rslrsal4.1-0 ; 
       with_bounding_model-rslrsal5.1-0 ; 
       with_bounding_model-rslrsal6.1-0 ; 
       with_bounding_model-rwepemt1.1-0 ; 
       with_bounding_model-rwepemt2.1-0 ; 
       with_bounding_model-rwepemt3.1-0 ; 
       with_bounding_model-shield.1-0 ; 
       with_bounding_model-slicer.1-0 ; 
       with_bounding_model-slrsal0.2-0 ; 
       with_bounding_model-SSal.1-0 ; 
       with_bounding_model-SSal1.1-0 ; 
       with_bounding_model-SSal2.1-0 ; 
       with_bounding_model-SSal3.1-0 ; 
       with_bounding_model-SSal4.1-0 ; 
       with_bounding_model-SSal5.1-0 ; 
       with_bounding_model-SSal6.1-0 ; 
       with_bounding_model-SSar.1-0 ; 
       with_bounding_model-SSar1.1-0 ; 
       with_bounding_model-SSar2.1-0 ; 
       with_bounding_model-SSar3.1-0 ; 
       with_bounding_model-SSar4.1-0 ; 
       with_bounding_model-SSar5.1-0 ; 
       with_bounding_model-SSar6.1-0 ; 
       with_bounding_model-SSat.1-0 ; 
       with_bounding_model-SSfb.1-0 ; 
       with_bounding_model-SSml1.1-0 ; 
       with_bounding_model-SSmr1.1-0 ; 
       with_bounding_model-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap01/PICTURES/cap01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap01-with_bounding_model.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       kez_frigate_F-t2d12.6-0 ; 
       kez_frigate_F-t2d14.6-0 ; 
       kez_frigate_F-t2d15.6-0 ; 
       kez_frigate_F-t2d16.6-0 ; 
       kez_frigate_F-t2d17.6-0 ; 
       kez_frigate_F-t2d18.6-0 ; 
       kez_frigate_F-t2d19.6-0 ; 
       kez_frigate_F-t2d2.6-0 ; 
       kez_frigate_F-t2d20.6-0 ; 
       kez_frigate_F-t2d21.6-0 ; 
       kez_frigate_F-t2d22.6-0 ; 
       kez_frigate_F-t2d24.6-0 ; 
       kez_frigate_F-t2d26.6-0 ; 
       kez_frigate_F-t2d27.6-0 ; 
       kez_frigate_F-t2d28.6-0 ; 
       kez_frigate_F-t2d29.6-0 ; 
       kez_frigate_F-t2d30.6-0 ; 
       kez_frigate_F-t2d31.6-0 ; 
       kez_frigate_F-t2d32.6-0 ; 
       kez_frigate_F-t2d33.6-0 ; 
       kez_frigate_F-t2d34.6-0 ; 
       kez_frigate_F-t2d35.6-0 ; 
       kez_frigate_F-t2d36.6-0 ; 
       kez_frigate_F-t2d37.6-0 ; 
       kez_frigate_F-t2d38.6-0 ; 
       kez_frigate_F-t2d4.6-0 ; 
       kez_frigate_F-t2d40.6-0 ; 
       kez_frigate_F-t2d41.6-0 ; 
       kez_frigate_F-t2d42.6-0 ; 
       kez_frigate_F-t2d43.6-0 ; 
       kez_frigate_F-t2d44.6-0 ; 
       kez_frigate_F-t2d45.6-0 ; 
       kez_frigate_F-t2d46.6-0 ; 
       kez_frigate_F-t2d48.5-0 ; 
       kez_frigate_F-t2d49.5-0 ; 
       kez_frigate_F-t2d50.5-0 ; 
       kez_frigate_F-t2d51.5-0 ; 
       kez_frigate_F-t2d55.5-0 ; 
       kez_frigate_F-t2d58.5-0 ; 
       kez_frigate_F-t2d59.5-0 ; 
       kez_frigate_F-t2d60.5-0 ; 
       kez_frigate_F-t2d61.5-0 ; 
       kez_frigate_F-t2d62.5-0 ; 
       kez_frigate_F-t2d7.6-0 ; 
       kez_frigate_F-t2d70.6-0 ; 
       kez_frigate_F-t2d71.6-0 ; 
       kez_frigate_F-t2d72.6-0 ; 
       kez_frigate_F-t2d73.6-0 ; 
       kez_frigate_F-t2d74.6-0 ; 
       kez_frigate_F-t2d75.6-0 ; 
       kez_frigate_F-t2d76.6-0 ; 
       kez_frigate_F-t2d8.6-0 ; 
       kez_frigate_F-t2d9.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 23 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 6 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 23 110 ; 
       14 42 110 ; 
       15 42 110 ; 
       16 42 110 ; 
       17 42 110 ; 
       18 42 110 ; 
       19 42 110 ; 
       20 13 110 ; 
       21 13 110 ; 
       22 13 110 ; 
       23 7 110 ; 
       24 6 110 ; 
       25 6 110 ; 
       26 42 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 42 110 ; 
       32 42 110 ; 
       33 42 110 ; 
       34 42 110 ; 
       35 42 110 ; 
       36 42 110 ; 
       37 13 110 ; 
       38 13 110 ; 
       39 13 110 ; 
       40 23 110 ; 
       41 23 110 ; 
       42 3 110 ; 
       43 2 110 ; 
       44 14 110 ; 
       45 15 110 ; 
       46 16 110 ; 
       47 17 110 ; 
       48 18 110 ; 
       49 19 110 ; 
       50 2 110 ; 
       51 31 110 ; 
       52 32 110 ; 
       53 33 110 ; 
       54 34 110 ; 
       55 35 110 ; 
       56 36 110 ; 
       57 4 110 ; 
       58 41 110 ; 
       59 26 110 ; 
       60 26 110 ; 
       61 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 36 300 ; 
       2 37 300 ; 
       2 39 300 ; 
       2 40 300 ; 
       2 41 300 ; 
       2 42 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 20 300 ; 
       9 9 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       13 44 300 ; 
       13 45 300 ; 
       13 46 300 ; 
       13 47 300 ; 
       14 49 300 ; 
       15 38 300 ; 
       16 29 300 ; 
       17 19 300 ; 
       18 10 300 ; 
       19 0 300 ; 
       23 50 300 ; 
       23 51 300 ; 
       23 52 300 ; 
       23 54 300 ; 
       23 55 300 ; 
       23 56 300 ; 
       23 57 300 ; 
       26 4 300 ; 
       31 53 300 ; 
       32 58 300 ; 
       33 69 300 ; 
       34 1 300 ; 
       35 2 300 ; 
       36 3 300 ; 
       40 43 300 ; 
       41 48 300 ; 
       43 76 300 ; 
       44 67 300 ; 
       45 68 300 ; 
       46 70 300 ; 
       47 71 300 ; 
       48 72 300 ; 
       49 73 300 ; 
       50 77 300 ; 
       51 61 300 ; 
       52 62 300 ; 
       53 63 300 ; 
       54 64 300 ; 
       55 65 300 ; 
       56 66 300 ; 
       57 75 300 ; 
       58 74 300 ; 
       59 60 300 ; 
       60 59 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 4 400 ; 
       16 25 400 ; 
       18 7 400 ; 
       26 43 400 ; 
       31 51 400 ; 
       32 52 400 ; 
       35 0 400 ; 
       41 36 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 50 401 ; 
       1 45 401 ; 
       3 44 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       18 11 401 ; 
       19 49 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 48 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       45 34 401 ; 
       46 33 401 ; 
       47 35 401 ; 
       49 47 401 ; 
       51 37 401 ; 
       52 38 401 ; 
       54 39 401 ; 
       55 40 401 ; 
       56 41 401 ; 
       57 42 401 ; 
       69 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 95 -6 0 MPRFLG 0 ; 
       1 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 53.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 56.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 100 -6 0 MPRFLG 0 ; 
       6 SCHEM 92.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -2 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 72.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 70 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 85 -6 0 MPRFLG 0 ; 
       12 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 55 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 60 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 62.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 65 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 67.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 90 -6 0 MPRFLG 0 ; 
       25 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 51.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 30 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 35 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 40 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 45 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 47.5 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 10 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 51.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 82.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 55 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 57.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 60 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 62.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 65 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 67.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 80 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 35 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 37.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 40 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 42.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 45 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 47.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 77.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 50 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 52.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 7.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 64 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 51.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 49 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 36.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 39 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 41.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 44 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 46.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 54 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 56.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 59 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 61.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 64 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 66.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 76.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 71.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 74 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 74 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 79 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 79 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 79 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 79 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 49 -16 0 MPRFLG 0 ; 
       45 SCHEM 44 -16 0 MPRFLG 0 ; 
       46 SCHEM 41.5 -16 0 MPRFLG 0 ; 
       47 SCHEM 56.5 -16 0 MPRFLG 0 ; 
       48 SCHEM 59 -16 0 MPRFLG 0 ; 
       49 SCHEM 64 -16 0 MPRFLG 0 ; 
       50 SCHEM 69 -16 0 MPRFLG 0 ; 
       51 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 39 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 102.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
