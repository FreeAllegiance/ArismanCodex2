SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 10     
       cap02-cap02_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.4-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       il_frig1_F-mat100.2-0 ; 
       il_frig1_F-mat101.2-0 ; 
       il_frigate_sT-mat1.2-0 ; 
       il_frigate_sT-mat10.2-0 ; 
       il_frigate_sT-mat11.2-0 ; 
       il_frigate_sT-mat12.2-0 ; 
       il_frigate_sT-mat13.2-0 ; 
       il_frigate_sT-mat14.2-0 ; 
       il_frigate_sT-mat15.2-0 ; 
       il_frigate_sT-mat16.2-0 ; 
       il_frigate_sT-mat17.2-0 ; 
       il_frigate_sT-mat18.2-0 ; 
       il_frigate_sT-mat19.2-0 ; 
       il_frigate_sT-mat2.2-0 ; 
       il_frigate_sT-mat20.2-0 ; 
       il_frigate_sT-mat21.2-0 ; 
       il_frigate_sT-mat22.2-0 ; 
       il_frigate_sT-mat23.2-0 ; 
       il_frigate_sT-mat24.2-0 ; 
       il_frigate_sT-mat25.2-0 ; 
       il_frigate_sT-mat26.2-0 ; 
       il_frigate_sT-mat27.2-0 ; 
       il_frigate_sT-mat28.2-0 ; 
       il_frigate_sT-mat29.2-0 ; 
       il_frigate_sT-mat3.2-0 ; 
       il_frigate_sT-mat30.2-0 ; 
       il_frigate_sT-mat31.2-0 ; 
       il_frigate_sT-mat32.2-0 ; 
       il_frigate_sT-mat33.2-0 ; 
       il_frigate_sT-mat34.2-0 ; 
       il_frigate_sT-mat35.2-0 ; 
       il_frigate_sT-mat36.2-0 ; 
       il_frigate_sT-mat37.2-0 ; 
       il_frigate_sT-mat38.2-0 ; 
       il_frigate_sT-mat39.2-0 ; 
       il_frigate_sT-mat4.2-0 ; 
       il_frigate_sT-mat40.2-0 ; 
       il_frigate_sT-mat41.2-0 ; 
       il_frigate_sT-mat42.2-0 ; 
       il_frigate_sT-mat43.2-0 ; 
       il_frigate_sT-mat44.2-0 ; 
       il_frigate_sT-mat45.2-0 ; 
       il_frigate_sT-mat46.2-0 ; 
       il_frigate_sT-mat47.2-0 ; 
       il_frigate_sT-mat48.2-0 ; 
       il_frigate_sT-mat5.2-0 ; 
       il_frigate_sT-mat56.2-0 ; 
       il_frigate_sT-mat57.2-0 ; 
       il_frigate_sT-mat58.2-0 ; 
       il_frigate_sT-mat59.2-0 ; 
       il_frigate_sT-mat6.2-0 ; 
       il_frigate_sT-mat60.2-0 ; 
       il_frigate_sT-mat61.2-0 ; 
       il_frigate_sT-mat62.2-0 ; 
       il_frigate_sT-mat63.2-0 ; 
       il_frigate_sT-mat64.2-0 ; 
       il_frigate_sT-mat65.2-0 ; 
       il_frigate_sT-mat66.2-0 ; 
       il_frigate_sT-mat67.2-0 ; 
       il_frigate_sT-mat68.2-0 ; 
       il_frigate_sT-mat69.2-0 ; 
       il_frigate_sT-mat7.2-0 ; 
       il_frigate_sT-mat70.2-0 ; 
       il_frigate_sT-mat71.2-0 ; 
       il_frigate_sT-mat72.2-0 ; 
       il_frigate_sT-mat73.2-0 ; 
       il_frigate_sT-mat74.2-0 ; 
       il_frigate_sT-mat75.2-0 ; 
       il_frigate_sT-mat76.2-0 ; 
       il_frigate_sT-mat77.2-0 ; 
       il_frigate_sT-mat78.2-0 ; 
       il_frigate_sT-mat79.2-0 ; 
       il_frigate_sT-mat8.2-0 ; 
       il_frigate_sT-mat80.2-0 ; 
       il_frigate_sT-mat81.2-0 ; 
       il_frigate_sT-mat82.2-0 ; 
       il_frigate_sT-mat83.2-0 ; 
       il_frigate_sT-mat84.2-0 ; 
       il_frigate_sT-mat85.2-0 ; 
       il_frigate_sT-mat86.2-0 ; 
       il_frigate_sT-mat87.2-0 ; 
       il_frigate_sT-mat88.2-0 ; 
       il_frigate_sT-mat9.2-0 ; 
       il_frigate_stubbyL-mat89.2-0 ; 
       il_frigate_stubbyL-mat91.2-0 ; 
       il_frigate_stubbyL-mat92.2-0 ; 
       il_frigate_stubbyL-mat93.2-0 ; 
       il_frigate_stubbyL-mat95.2-0 ; 
       il_frigate_stubbyL-mat97.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       cap02-antenn.1-0 ; 
       cap02-bturatt.1-0 ; 
       cap02-cap02_2.1-0 ROOT ; 
       cap02-engine0.1-0 ; 
       cap02-finzzz.1-0 ; 
       cap02-fuselg1.1-0 ; 
       cap02-fuselg2.1-0 ; 
       cap02-fuselg3.1-0 ; 
       cap02-fuselg4.1-0 ; 
       cap02-fwepemt1.1-0 ; 
       cap02-fwepemt2.1-0 ; 
       cap02-lbengine.1-0 ; 
       cap02-ltengine.1-0 ; 
       cap02-lthrust.1-0 ; 
       cap02-rbengine.1-0 ; 
       cap02-rtengine.1-0 ; 
       cap02-rthrust.1-0 ; 
       cap02-slicer.1-0 ; 
       cap02-SSab.1-0 ; 
       cap02-SSab2.1-0 ; 
       cap02-SSat.1-0 ; 
       cap02-SSfstrobe.1-0 ; 
       cap02-SSl1.1-0 ; 
       cap02-SSl2.1-0 ; 
       cap02-SSr1.1-0 ; 
       cap02-SSr2.1-0 ; 
       cap02-thrust0.1-0 ; 
       cap02-turatt.1-0 ; 
       cap02-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02-retexture.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 54     
       il_frigate_sT-t2d10.2-0 ; 
       il_frigate_sT-t2d11.2-0 ; 
       il_frigate_sT-t2d12.2-0 ; 
       il_frigate_sT-t2d13.2-0 ; 
       il_frigate_sT-t2d14.2-0 ; 
       il_frigate_sT-t2d15.2-0 ; 
       il_frigate_sT-t2d16.2-0 ; 
       il_frigate_sT-t2d17.2-0 ; 
       il_frigate_sT-t2d18.2-0 ; 
       il_frigate_sT-t2d19.2-0 ; 
       il_frigate_sT-t2d2.3-0 ; 
       il_frigate_sT-t2d20.2-0 ; 
       il_frigate_sT-t2d21.2-0 ; 
       il_frigate_sT-t2d22.2-0 ; 
       il_frigate_sT-t2d23.2-0 ; 
       il_frigate_sT-t2d24.2-0 ; 
       il_frigate_sT-t2d25.2-0 ; 
       il_frigate_sT-t2d26.2-0 ; 
       il_frigate_sT-t2d28.2-0 ; 
       il_frigate_sT-t2d29.2-0 ; 
       il_frigate_sT-t2d30.2-0 ; 
       il_frigate_sT-t2d31.2-0 ; 
       il_frigate_sT-t2d36.2-0 ; 
       il_frigate_sT-t2d37.2-0 ; 
       il_frigate_sT-t2d38.2-0 ; 
       il_frigate_sT-t2d39.2-0 ; 
       il_frigate_sT-t2d40.2-0 ; 
       il_frigate_sT-t2d41.2-0 ; 
       il_frigate_sT-t2d42.2-0 ; 
       il_frigate_sT-t2d43.2-0 ; 
       il_frigate_sT-t2d44.2-0 ; 
       il_frigate_sT-t2d45.2-0 ; 
       il_frigate_sT-t2d46.2-0 ; 
       il_frigate_sT-t2d47.2-0 ; 
       il_frigate_sT-t2d48.2-0 ; 
       il_frigate_sT-t2d49.2-0 ; 
       il_frigate_sT-t2d5.3-0 ; 
       il_frigate_sT-t2d50.2-0 ; 
       il_frigate_sT-t2d51.2-0 ; 
       il_frigate_sT-t2d52.2-0 ; 
       il_frigate_sT-t2d53.2-0 ; 
       il_frigate_sT-t2d54.2-0 ; 
       il_frigate_sT-t2d55.2-0 ; 
       il_frigate_sT-t2d56.2-0 ; 
       il_frigate_sT-t2d59.3-0 ; 
       il_frigate_sT-t2d6.3-0 ; 
       il_frigate_sT-t2d60.3-0 ; 
       il_frigate_sT-t2d61.3-0 ; 
       il_frigate_sT-t2d62.3-0 ; 
       il_frigate_sT-t2d63.3-0 ; 
       il_frigate_sT-t2d64.3-0 ; 
       il_frigate_sT-t2d7.3-0 ; 
       il_frigate_sT-t2d8.2-0 ; 
       il_frigate_sT-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 5 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 26 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 26 110 ; 
       17 5 110 ; 
       18 4 110 ; 
       19 0 110 ; 
       20 7 110 ; 
       21 5 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 2 110 ; 
       27 6 110 ; 
       28 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 57 300 ; 
       0 58 300 ; 
       0 59 300 ; 
       0 60 300 ; 
       0 62 300 ; 
       0 63 300 ; 
       0 64 300 ; 
       0 65 300 ; 
       0 66 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       5 2 300 ; 
       5 13 300 ; 
       5 24 300 ; 
       5 35 300 ; 
       5 45 300 ; 
       5 50 300 ; 
       5 61 300 ; 
       5 72 300 ; 
       5 82 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 77 300 ; 
       5 78 300 ; 
       5 79 300 ; 
       5 80 300 ; 
       5 81 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       6 70 300 ; 
       6 71 300 ; 
       7 56 300 ; 
       7 67 300 ; 
       7 68 300 ; 
       7 69 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 75 300 ; 
       8 76 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 38 300 ; 
       13 39 300 ; 
       13 40 300 ; 
       13 41 300 ; 
       13 42 300 ; 
       13 43 300 ; 
       13 44 300 ; 
       13 46 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       14 25 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       16 47 300 ; 
       16 48 300 ; 
       16 49 300 ; 
       16 51 300 ; 
       16 52 300 ; 
       16 53 300 ; 
       16 54 300 ; 
       16 55 300 ; 
       17 73 300 ; 
       17 74 300 ; 
       18 0 300 ; 
       19 1 300 ; 
       20 85 300 ; 
       21 84 300 ; 
       22 86 300 ; 
       23 88 300 ; 
       24 83 300 ; 
       25 87 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 45 401 ; 
       5 51 401 ; 
       7 52 401 ; 
       8 53 401 ; 
       9 0 401 ; 
       10 1 401 ; 
       12 2 401 ; 
       14 3 401 ; 
       15 4 401 ; 
       16 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       22 9 401 ; 
       23 11 401 ; 
       25 12 401 ; 
       27 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       31 16 401 ; 
       35 46 401 ; 
       36 17 401 ; 
       37 43 401 ; 
       41 18 401 ; 
       42 19 401 ; 
       43 20 401 ; 
       44 21 401 ; 
       45 10 401 ; 
       46 22 401 ; 
       51 23 401 ; 
       52 24 401 ; 
       53 25 401 ; 
       54 26 401 ; 
       55 27 401 ; 
       58 28 401 ; 
       59 29 401 ; 
       60 30 401 ; 
       61 50 401 ; 
       63 31 401 ; 
       64 32 401 ; 
       65 33 401 ; 
       66 34 401 ; 
       67 35 401 ; 
       68 37 401 ; 
       69 38 401 ; 
       71 39 401 ; 
       74 40 401 ; 
       75 41 401 ; 
       76 42 401 ; 
       77 44 401 ; 
       78 47 401 ; 
       79 48 401 ; 
       81 49 401 ; 
       82 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 221.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 70 -8 0 MPRFLG 0 ; 
       2 SCHEM 116.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 186.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 85 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 65 -8 0 MPRFLG 0 ; 
       10 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 203.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 170 -8 0 MPRFLG 0 ; 
       13 SCHEM 126.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 193.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 182.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 151.25 -8 0 MPRFLG 0 ; 
       17 SCHEM 61.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 210 -8 0 MPRFLG 0 ; 
       20 SCHEM 0 -10 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 115 -10 0 MPRFLG 0 ; 
       23 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 140 -10 0 MPRFLG 0 ; 
       25 SCHEM 142.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 138.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 35 -8 0 MPRFLG 0 ; 
       28 SCHEM 37.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 210 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 187.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 180 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 182.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 185 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 175 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 165 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 167.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 170 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 172.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 207.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 200 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 202.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 205 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 197.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 190 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 192.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 195 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 222.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 217.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 220 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 212.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 225 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 227.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 230 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 232.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 182.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 185 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 165 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 167.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 170 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 172.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 200 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 202.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 205 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 190 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 192.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 195 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 132.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 150 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 152.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 217.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 220 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 225 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 227.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 230 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 232.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 177.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 180 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 234 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 29 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
