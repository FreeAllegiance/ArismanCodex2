SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.12-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       add_turrets-light1.5-0 ROOT ; 
       add_turrets-light1_1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 91     
       add_turrets-mat101.1-0 ; 
       add_turrets-mat102.1-0 ; 
       add_turrets-mat109.2-0 ; 
       add_turrets-mat110.1-0 ; 
       add_turrets-mat112.1-0 ; 
       add_turrets-mat115.1-0 ; 
       add_turrets-mat116.1-0 ; 
       add_turrets-mat117.1-0 ; 
       add_turrets-mat89.1-0 ; 
       add_turrets-mat90.1-0 ; 
       add_turrets-mat91.1-0 ; 
       add_turrets-mat92.1-0 ; 
       add_turrets-mat95.1-0 ; 
       add_turrets-mat99.1-0 ; 
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat66.1-0 ; 
       il_frigate_sPtL-mat67.1-0 ; 
       il_frigate_sPtL-mat68.1-0 ; 
       il_frigate_sPtL-mat69.1-0 ; 
       il_frigate_sPtL-mat70.1-0 ; 
       il_frigate_sPtL-mat71.1-0 ; 
       il_frigate_sPtL-mat72.1-0 ; 
       il_frigate_sPtL-mat73.1-0 ; 
       il_frigate_sPtL-mat74.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.5-0 ROOT ; 
       cap02a-cube1.1-0 ; 
       cap02a-cube1_1.3-0 ; 
       cap02a-cube1_2.1-0 ; 
       cap02a-cube1_3.1-0 ; 
       cap02a-cube9.1-0 ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-add_turrets.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       add_turrets-t2d65.1-0 ; 
       add_turrets-t2d66.3-0 ; 
       add_turrets-t2d67.1-0 ; 
       add_turrets-t2d69.1-0 ; 
       add_turrets-t2d72.1-0 ; 
       add_turrets-t2d73.1-0 ; 
       add_turrets-t2d74.2-0 ; 
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d42.1-0 ; 
       il_frigate_sPtL-t2d43.1-0 ; 
       il_frigate_sPtL-t2d44.1-0 ; 
       il_frigate_sPtL-t2d45.1-0 ; 
       il_frigate_sPtL-t2d46.1-0 ; 
       il_frigate_sPtL-t2d47.1-0 ; 
       il_frigate_sPtL-t2d48.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 10 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 11 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 30 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 30 110 ; 
       21 10 110 ; 
       22 0 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 2 110 ; 
       31 10 110 ; 
       32 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 58 300 ; 
       0 59 300 ; 
       0 60 300 ; 
       0 61 300 ; 
       0 62 300 ; 
       0 63 300 ; 
       0 64 300 ; 
       0 65 300 ; 
       0 66 300 ; 
       3 5 300 ; 
       4 2 300 ; 
       4 7 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 6 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       10 72 300 ; 
       10 76 300 ; 
       10 77 300 ; 
       10 78 300 ; 
       10 79 300 ; 
       10 80 300 ; 
       10 81 300 ; 
       10 82 300 ; 
       10 90 300 ; 
       10 73 300 ; 
       10 74 300 ; 
       10 75 300 ; 
       10 85 300 ; 
       10 86 300 ; 
       10 87 300 ; 
       10 88 300 ; 
       10 89 300 ; 
       11 36 300 ; 
       11 37 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 70 300 ; 
       11 71 300 ; 
       12 57 300 ; 
       12 67 300 ; 
       12 68 300 ; 
       12 69 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       16 22 300 ; 
       16 23 300 ; 
       17 41 300 ; 
       17 42 300 ; 
       17 43 300 ; 
       17 44 300 ; 
       17 45 300 ; 
       17 46 300 ; 
       17 47 300 ; 
       17 48 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       18 31 300 ; 
       19 14 300 ; 
       19 15 300 ; 
       19 16 300 ; 
       19 17 300 ; 
       19 18 300 ; 
       20 49 300 ; 
       20 50 300 ; 
       20 51 300 ; 
       20 52 300 ; 
       20 53 300 ; 
       20 54 300 ; 
       20 55 300 ; 
       20 56 300 ; 
       21 83 300 ; 
       21 84 300 ; 
       22 1 300 ; 
       23 0 300 ; 
       24 11 300 ; 
       25 10 300 ; 
       26 9 300 ; 
       27 13 300 ; 
       28 8 300 ; 
       29 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       15 45 401 ; 
       16 46 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 11 401 ; 
       23 12 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       29 16 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       32 0 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       39 22 401 ; 
       40 44 401 ; 
       44 23 401 ; 
       45 24 401 ; 
       46 25 401 ; 
       47 26 401 ; 
       48 27 401 ; 
       52 28 401 ; 
       53 29 401 ; 
       54 30 401 ; 
       55 31 401 ; 
       56 32 401 ; 
       59 33 401 ; 
       60 34 401 ; 
       61 35 401 ; 
       63 36 401 ; 
       64 37 401 ; 
       65 38 401 ; 
       66 39 401 ; 
       67 40 401 ; 
       68 41 401 ; 
       69 42 401 ; 
       71 43 401 ; 
       73 51 401 ; 
       75 57 401 ; 
       78 52 401 ; 
       79 47 401 ; 
       81 56 401 ; 
       84 49 401 ; 
       85 50 401 ; 
       86 53 401 ; 
       87 54 401 ; 
       89 55 401 ; 
       90 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 50 -2 0 MPRFLG 0 ; 
       5 SCHEM 52.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 15 -4 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 40 -4 0 MPRFLG 0 ; 
       20 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       21 SCHEM 25 -4 0 MPRFLG 0 ; 
       22 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 5 -6 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 30 -6 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 35 -6 0 MPRFLG 0 ; 
       30 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
