SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.13-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       add_turrets-light1.6-0 ROOT ; 
       add_turrets-light1_1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 95     
       add_turrets-mat101.1-0 ; 
       add_turrets-mat102.1-0 ; 
       add_turrets-mat109.2-0 ; 
       add_turrets-mat117.1-0 ; 
       add_turrets-mat118.1-0 ; 
       add_turrets-mat119.1-0 ; 
       add_turrets-mat120.1-0 ; 
       add_turrets-mat121.1-0 ; 
       add_turrets-mat122.1-0 ; 
       add_turrets-mat123.1-0 ; 
       add_turrets-mat124.1-0 ; 
       add_turrets-mat125.1-0 ; 
       add_turrets-mat89.1-0 ; 
       add_turrets-mat90.1-0 ; 
       add_turrets-mat91.1-0 ; 
       add_turrets-mat92.1-0 ; 
       add_turrets-mat95.1-0 ; 
       add_turrets-mat99.1-0 ; 
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat66.1-0 ; 
       il_frigate_sPtL-mat67.1-0 ; 
       il_frigate_sPtL-mat68.1-0 ; 
       il_frigate_sPtL-mat69.1-0 ; 
       il_frigate_sPtL-mat70.1-0 ; 
       il_frigate_sPtL-mat71.1-0 ; 
       il_frigate_sPtL-mat72.1-0 ; 
       il_frigate_sPtL-mat73.1-0 ; 
       il_frigate_sPtL-mat74.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.6-0 ROOT ; 
       cap02a-cube1_1.3-0 ; 
       cap02a-cube1_4.1-0 ; 
       cap02a-cube1_5.1-0 ; 
       cap02a-cube1_6.1-0 ; 
       cap02a-cube1_7.1-0 ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-add_turrets.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 62     
       add_turrets-t2d65.1-0 ; 
       add_turrets-t2d66.3-0 ; 
       add_turrets-t2d74.2-0 ; 
       add_turrets-t2d75.1-0 ; 
       add_turrets-t2d76.1-0 ; 
       add_turrets-t2d77.1-0 ; 
       add_turrets-t2d78.1-0 ; 
       add_turrets-t2d79.1-0 ; 
       add_turrets-t2d80.1-0 ; 
       add_turrets-t2d81.1-0 ; 
       add_turrets-t2d82.1-0 ; 
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d42.1-0 ; 
       il_frigate_sPtL-t2d43.1-0 ; 
       il_frigate_sPtL-t2d44.1-0 ; 
       il_frigate_sPtL-t2d45.1-0 ; 
       il_frigate_sPtL-t2d46.1-0 ; 
       il_frigate_sPtL-t2d47.1-0 ; 
       il_frigate_sPtL-t2d48.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 2 110 ; 
       0 2 110 ; 
       5 2 110 ; 
       1 10 110 ; 
       3 2 110 ; 
       8 2 110 ; 
       9 11 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 30 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 30 110 ; 
       21 10 110 ; 
       22 0 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 2 110 ; 
       31 10 110 ; 
       32 11 110 ; 
       6 2 110 ; 
       7 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 4 300 ; 
       4 5 300 ; 
       0 62 300 ; 
       0 63 300 ; 
       0 64 300 ; 
       0 65 300 ; 
       0 66 300 ; 
       0 67 300 ; 
       0 68 300 ; 
       0 69 300 ; 
       0 70 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 39 300 ; 
       10 76 300 ; 
       10 80 300 ; 
       10 81 300 ; 
       10 82 300 ; 
       10 83 300 ; 
       10 84 300 ; 
       10 85 300 ; 
       10 86 300 ; 
       10 94 300 ; 
       10 77 300 ; 
       10 78 300 ; 
       10 79 300 ; 
       10 89 300 ; 
       10 90 300 ; 
       10 91 300 ; 
       10 92 300 ; 
       10 93 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       11 42 300 ; 
       11 43 300 ; 
       11 44 300 ; 
       11 74 300 ; 
       11 75 300 ; 
       12 61 300 ; 
       12 71 300 ; 
       12 72 300 ; 
       12 73 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       15 30 300 ; 
       15 31 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       16 25 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       17 45 300 ; 
       17 46 300 ; 
       17 47 300 ; 
       17 48 300 ; 
       17 49 300 ; 
       17 50 300 ; 
       17 51 300 ; 
       17 52 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       18 35 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       20 53 300 ; 
       20 54 300 ; 
       20 55 300 ; 
       20 56 300 ; 
       20 57 300 ; 
       20 58 300 ; 
       20 59 300 ; 
       20 60 300 ; 
       21 87 300 ; 
       21 88 300 ; 
       22 1 300 ; 
       23 0 300 ; 
       24 15 300 ; 
       25 14 300 ; 
       26 13 300 ; 
       27 17 300 ; 
       28 12 300 ; 
       29 16 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       19 49 401 ; 
       20 50 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       24 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 0 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       43 26 401 ; 
       44 48 401 ; 
       48 27 401 ; 
       49 28 401 ; 
       50 29 401 ; 
       51 30 401 ; 
       52 31 401 ; 
       56 32 401 ; 
       57 33 401 ; 
       58 34 401 ; 
       59 35 401 ; 
       60 36 401 ; 
       63 37 401 ; 
       64 38 401 ; 
       65 39 401 ; 
       67 40 401 ; 
       68 41 401 ; 
       69 42 401 ; 
       70 43 401 ; 
       71 44 401 ; 
       72 45 401 ; 
       73 46 401 ; 
       75 47 401 ; 
       77 55 401 ; 
       79 61 401 ; 
       82 56 401 ; 
       83 51 401 ; 
       85 60 401 ; 
       88 53 401 ; 
       89 54 401 ; 
       90 57 401 ; 
       91 58 401 ; 
       93 59 401 ; 
       94 52 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 40 -6 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 28.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 35 -6 0 MPRFLG 0 ; 
       8 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 0 -8 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 20 -8 0 MPRFLG 0 ; 
       26 SCHEM 25 -10 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 30 -10 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       30 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       31 SCHEM 10 -8 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 0 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 0 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 0 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 0 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
