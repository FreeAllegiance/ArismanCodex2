SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.24-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 71     
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
       static-mat101.1-0 ; 
       static-mat91.1-0 ; 
       static-mat92.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.14-0 ROOT ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
       static-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 1 110 ; 
       3 5 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 17 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 17 110 ; 
       13 4 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 4 110 ; 
       17 1 110 ; 
       18 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       4 49 300 ; 
       4 53 300 ; 
       4 54 300 ; 
       4 55 300 ; 
       4 56 300 ; 
       4 57 300 ; 
       4 58 300 ; 
       4 59 300 ; 
       4 67 300 ; 
       4 50 300 ; 
       4 51 300 ; 
       4 52 300 ; 
       4 62 300 ; 
       4 63 300 ; 
       4 64 300 ; 
       4 65 300 ; 
       4 66 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 47 300 ; 
       5 48 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       9 29 300 ; 
       9 30 300 ; 
       9 31 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 37 300 ; 
       12 38 300 ; 
       12 39 300 ; 
       12 40 300 ; 
       12 41 300 ; 
       12 42 300 ; 
       13 60 300 ; 
       13 61 300 ; 
       14 68 300 ; 
       15 70 300 ; 
       16 69 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 31 401 ; 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 44 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 30 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       48 29 401 ; 
       50 37 401 ; 
       52 43 401 ; 
       55 38 401 ; 
       56 33 401 ; 
       58 42 401 ; 
       61 35 401 ; 
       62 36 401 ; 
       63 39 401 ; 
       64 40 401 ; 
       66 41 401 ; 
       67 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -8 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 0 -8 0 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 15 -8 0 MPRFLG 0 ; 
       14 SCHEM 5 -10 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       68 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       44 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 36.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 0 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 0 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 0 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 0 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
