SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.25-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       cap02a-cap02a.15-0 ROOT ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
       static-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 4 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 13 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 13 110 ; 
       12 3 110 ; 
       13 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       3 49 300 ; 
       3 53 300 ; 
       3 54 300 ; 
       3 55 300 ; 
       3 56 300 ; 
       3 57 300 ; 
       3 58 300 ; 
       3 59 300 ; 
       3 67 300 ; 
       3 50 300 ; 
       3 51 300 ; 
       3 52 300 ; 
       3 62 300 ; 
       3 63 300 ; 
       3 64 300 ; 
       3 65 300 ; 
       3 66 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 47 300 ; 
       4 48 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       5 46 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       8 27 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       10 0 300 ; 
       10 1 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       10 4 300 ; 
       11 35 300 ; 
       11 36 300 ; 
       11 37 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       11 42 300 ; 
       12 60 300 ; 
       12 61 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 31 401 ; 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 44 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 30 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       48 29 401 ; 
       50 37 401 ; 
       52 43 401 ; 
       55 38 401 ; 
       56 33 401 ; 
       58 42 401 ; 
       61 35 401 ; 
       62 36 401 ; 
       63 39 401 ; 
       64 40 401 ; 
       66 41 401 ; 
       67 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 0 -8 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 MPRFLG 0 ; 
       9 SCHEM 20 -8 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 11.25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       44 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 0 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -18 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 0 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -20 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 0 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -22 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
