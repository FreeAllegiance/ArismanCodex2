SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur-cam_int1.9-0 ROOT ; 
       tur-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS NBELEM 1     
       tur-Flares1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       tur-light1.3-0 ROOT ; 
       tur-spot1.1-0 ; 
       tur-spot1_int.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       tur-mat1.3-0 ; 
       tur-mat2.1-0 ; 
       tur-mat3.1-0 ; 
       tur-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       tur-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       tur-bool6.4-0 ROOT ; 
       tur-sphere1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       tur-DGlow1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       tur-tur.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       tur-t2d1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       tur-March_Fractal1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 2 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       3 0 550 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       -1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER CAMERA_SHADERS 
       -1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER MODELS 
       0 1 551 1 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 1 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
