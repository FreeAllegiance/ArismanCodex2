SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       scaled-light1.3-0 ROOT ; 
       scaled-light2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       il_frigate_sPtL-mat13.3-0 ; 
       il_frigate_sPtL-mat14.3-0 ; 
       il_frigate_sPtL-mat15.3-0 ; 
       il_frigate_sPtL-mat16.3-0 ; 
       il_frigate_sPtL-mat17.3-0 ; 
       il_frigate_sPtL-mat18.3-0 ; 
       il_frigate_sPtL-mat19.3-0 ; 
       il_frigate_sPtL-mat20.3-0 ; 
       il_frigate_sPtL-mat21.3-0 ; 
       il_frigate_sPtL-mat22.3-0 ; 
       il_frigate_sPtL-mat23.3-0 ; 
       il_frigate_sPtL-mat24.3-0 ; 
       il_frigate_sPtL-mat25.3-0 ; 
       il_frigate_sPtL-mat26.3-0 ; 
       il_frigate_sPtL-mat27.3-0 ; 
       il_frigate_sPtL-mat28.3-0 ; 
       il_frigate_sPtL-mat29.3-0 ; 
       il_frigate_sPtL-mat30.3-0 ; 
       il_frigate_sPtL-mat31.3-0 ; 
       il_frigate_sPtL-mat32.3-0 ; 
       il_frigate_sPtL-mat33.3-0 ; 
       il_frigate_sPtL-mat34.3-0 ; 
       il_frigate_sPtL-mat37.3-0 ; 
       il_frigate_sPtL-mat38.3-0 ; 
       il_frigate_sPtL-mat39.3-0 ; 
       il_frigate_sPtL-mat40.3-0 ; 
       il_frigate_sPtL-mat41.3-0 ; 
       il_frigate_sPtL-mat42.3-0 ; 
       il_frigate_sPtL-mat43.3-0 ; 
       il_frigate_sPtL-mat44.3-0 ; 
       il_frigate_sPtL-mat45.3-0 ; 
       il_frigate_sPtL-mat46.3-0 ; 
       il_frigate_sPtL-mat47.3-0 ; 
       il_frigate_sPtL-mat48.3-0 ; 
       il_frigate_sPtL-mat56.3-0 ; 
       il_frigate_sPtL-mat57.3-0 ; 
       il_frigate_sPtL-mat58.3-0 ; 
       il_frigate_sPtL-mat59.3-0 ; 
       il_frigate_sPtL-mat60.3-0 ; 
       il_frigate_sPtL-mat61.3-0 ; 
       il_frigate_sPtL-mat62.3-0 ; 
       il_frigate_sPtL-mat63.3-0 ; 
       il_frigate_sPtL-mat64.3-0 ; 
       il_frigate_sPtL-mat65.3-0 ; 
       il_frigate_sPtL-mat66.3-0 ; 
       il_frigate_sPtL-mat67.3-0 ; 
       il_frigate_sPtL-mat68.3-0 ; 
       il_frigate_sPtL-mat69.3-0 ; 
       il_frigate_sPtL-mat70.3-0 ; 
       il_frigate_sPtL-mat71.3-0 ; 
       il_frigate_sPtL-mat72.3-0 ; 
       il_frigate_sPtL-mat73.3-0 ; 
       il_frigate_sPtL-mat74.3-0 ; 
       il_frigate_sPtL-mat75.3-0 ; 
       il_frigate_sPtL-mat76.3-0 ; 
       il_frigate_sPtL-mat77.3-0 ; 
       il_frigate_sPtL-mat78.3-0 ; 
       il_frigate_sPtL-mat79.3-0 ; 
       il_frigate_sT-mat1_1.3-0 ; 
       il_frigate_sT-mat10_1.3-0 ; 
       il_frigate_sT-mat11_1.3-0 ; 
       il_frigate_sT-mat12_1.3-0 ; 
       il_frigate_sT-mat2_1.3-0 ; 
       il_frigate_sT-mat3_1.3-0 ; 
       il_frigate_sT-mat4_1.3-0 ; 
       il_frigate_sT-mat5_1.3-0 ; 
       il_frigate_sT-mat6_1.3-0 ; 
       il_frigate_sT-mat7_1.3-0 ; 
       il_frigate_sT-mat8_1.3-0 ; 
       il_frigate_sT-mat80_1.3-0 ; 
       il_frigate_sT-mat81_1.3-0 ; 
       il_frigate_sT-mat84_1.3-0 ; 
       il_frigate_sT-mat85_1.3-0 ; 
       il_frigate_sT-mat86_1.3-0 ; 
       il_frigate_sT-mat87_1.3-0 ; 
       il_frigate_sT-mat88_1.3-0 ; 
       il_frigate_sT-mat9_1.3-0 ; 
       scaled-mat1_2.1-0 ; 
       scaled-mat1_3.1-0 ; 
       scaled-mat1_4.1-0 ; 
       scaled-mat1_5.1-0 ; 
       scaled-mat101.1-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat89.1-0 ; 
       scaled-mat90.1-0 ; 
       scaled-mat91.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-mat95.1-0 ; 
       scaled-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 52     
       cap02a-140deg.3-0 ROOT ; 
       cap02a-170deg.3-0 ROOT ; 
       cap02a-170deg1.3-0 ROOT ; 
       cap02a-175deg.3-0 ROOT ; 
       frames-antenn.1-0 ; 
       frames-bturatt.1-0 ; 
       frames-cap02a.1-0 ROOT ; 
       frames-cockpt.1-0 ; 
       frames-cube2.1-0 ; 
       frames-cube3.1-0 ; 
       frames-cube4.1-0 ; 
       frames-cube5.1-0 ; 
       frames-engine0.1-0 ; 
       frames-finzzz.1-0 ; 
       frames-fuselg1.1-0 ; 
       frames-fuselg2.1-0 ; 
       frames-fuselg3.1-0 ; 
       frames-fwepemt.1-0 ; 
       frames-fwepemt2.1-0 ; 
       frames-lbengine.1-0 ; 
       frames-ltengine.1-0 ; 
       frames-misswepemt.1-0 ; 
       frames-qq.1-0 ; 
       frames-rbengine.1-0 ; 
       frames-rtengine.1-0 ; 
       frames-slicer.1-0 ; 
       frames-smoke1.1-0 ; 
       frames-smoke2.1-0 ; 
       frames-smoke3.1-0 ; 
       frames-smoke4.1-0 ; 
       frames-SSa.1-0 ; 
       frames-SSab.1-0 ; 
       frames-SSat.1-0 ; 
       frames-SSf.1-0 ; 
       frames-SSl1.1-0 ; 
       frames-SSl2.1-0 ; 
       frames-SSr1.1-0 ; 
       frames-SSr2.1-0 ; 
       frames-thrust1.1-0 ; 
       frames-thrust2.1-0 ; 
       frames-thrust3.1-0 ; 
       frames-thrust4.1-0 ; 
       frames-trail.1-0 ; 
       frames-tturatt.1-0 ; 
       frames-turwepemt1.1-0 ; 
       frames-turwepemt2.1-0 ; 
       frames-turwepemt3.1-0 ; 
       frames-turwepemt4.1-0 ; 
       frames-twepemt.1-0 ; 
       frames-w.1-0 ; 
       frames-w1.1-0 ; 
       frames-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-frames.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       il_frigate_sPtL-t2d10.5-0 ; 
       il_frigate_sPtL-t2d11.5-0 ; 
       il_frigate_sPtL-t2d12.5-0 ; 
       il_frigate_sPtL-t2d13.5-0 ; 
       il_frigate_sPtL-t2d14.5-0 ; 
       il_frigate_sPtL-t2d15.5-0 ; 
       il_frigate_sPtL-t2d16.5-0 ; 
       il_frigate_sPtL-t2d17.5-0 ; 
       il_frigate_sPtL-t2d18.5-0 ; 
       il_frigate_sPtL-t2d19.5-0 ; 
       il_frigate_sPtL-t2d20.5-0 ; 
       il_frigate_sPtL-t2d21.5-0 ; 
       il_frigate_sPtL-t2d22.5-0 ; 
       il_frigate_sPtL-t2d23.5-0 ; 
       il_frigate_sPtL-t2d24.5-0 ; 
       il_frigate_sPtL-t2d26.5-0 ; 
       il_frigate_sPtL-t2d28.5-0 ; 
       il_frigate_sPtL-t2d29.5-0 ; 
       il_frigate_sPtL-t2d30.5-0 ; 
       il_frigate_sPtL-t2d31.5-0 ; 
       il_frigate_sPtL-t2d36.5-0 ; 
       il_frigate_sPtL-t2d37.5-0 ; 
       il_frigate_sPtL-t2d38.5-0 ; 
       il_frigate_sPtL-t2d39.5-0 ; 
       il_frigate_sPtL-t2d40.5-0 ; 
       il_frigate_sPtL-t2d41.5-0 ; 
       il_frigate_sPtL-t2d42.5-0 ; 
       il_frigate_sPtL-t2d43.5-0 ; 
       il_frigate_sPtL-t2d44.5-0 ; 
       il_frigate_sPtL-t2d45.5-0 ; 
       il_frigate_sPtL-t2d46.5-0 ; 
       il_frigate_sPtL-t2d47.5-0 ; 
       il_frigate_sPtL-t2d48.5-0 ; 
       il_frigate_sPtL-t2d49.5-0 ; 
       il_frigate_sPtL-t2d50.5-0 ; 
       il_frigate_sPtL-t2d51.5-0 ; 
       il_frigate_sPtL-t2d52.5-0 ; 
       il_frigate_sPtL-t2d56.5-0 ; 
       il_frigate_sPtL-t2d8.5-0 ; 
       il_frigate_sPtL-t2d9.5-0 ; 
       il_frigate_sT-t2d2_1.5-0 ; 
       il_frigate_sT-t2d5_1.5-0 ; 
       il_frigate_sT-t2d53_1.5-0 ; 
       il_frigate_sT-t2d59_1.5-0 ; 
       il_frigate_sT-t2d6_1.5-0 ; 
       il_frigate_sT-t2d60_1.5-0 ; 
       il_frigate_sT-t2d61_1.5-0 ; 
       il_frigate_sT-t2d62_1.5-0 ; 
       il_frigate_sT-t2d63_1.5-0 ; 
       il_frigate_sT-t2d64_1.5-0 ; 
       il_frigate_sT-t2d7_1.5-0 ; 
       scaled-t2d65.2-0 ; 
       scaled-t2d66.2-0 ; 
       scaled-t2d67.2-0 ; 
       scaled-t2d68.2-0 ; 
       scaled-t2d69.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 6 110 ; 
       5 14 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 15 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 15 110 ; 
       7 6 110 ; 
       21 6 110 ; 
       51 6 110 ; 
       42 6 110 ; 
       38 6 110 ; 
       39 6 110 ; 
       40 6 110 ; 
       41 6 110 ; 
       48 6 110 ; 
       26 6 110 ; 
       27 6 110 ; 
       28 6 110 ; 
       29 6 110 ; 
       17 6 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       49 22 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       50 22 110 ; 
       25 14 110 ; 
       30 4 110 ; 
       31 13 110 ; 
       32 13 110 ; 
       33 14 110 ; 
       34 49 110 ; 
       35 49 110 ; 
       36 50 110 ; 
       37 50 110 ; 
       22 6 110 ; 
       43 14 110 ; 
       44 8 110 ; 
       45 9 110 ; 
       46 10 110 ; 
       47 11 110 ; 
       18 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 44 300 ; 
       4 45 300 ; 
       4 46 300 ; 
       4 47 300 ; 
       4 48 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       4 51 300 ; 
       4 52 300 ; 
       8 77 300 ; 
       9 78 300 ; 
       10 79 300 ; 
       11 80 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       14 58 300 ; 
       14 62 300 ; 
       14 63 300 ; 
       14 64 300 ; 
       14 65 300 ; 
       14 66 300 ; 
       14 67 300 ; 
       14 68 300 ; 
       14 76 300 ; 
       14 59 300 ; 
       14 60 300 ; 
       14 61 300 ; 
       14 71 300 ; 
       14 72 300 ; 
       14 73 300 ; 
       14 74 300 ; 
       14 75 300 ; 
       15 22 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 56 300 ; 
       15 57 300 ; 
       16 43 300 ; 
       16 53 300 ; 
       16 54 300 ; 
       16 55 300 ; 
       19 10 300 ; 
       19 11 300 ; 
       19 12 300 ; 
       19 13 300 ; 
       20 5 300 ; 
       20 6 300 ; 
       20 7 300 ; 
       20 8 300 ; 
       20 9 300 ; 
       49 27 300 ; 
       49 28 300 ; 
       49 29 300 ; 
       49 30 300 ; 
       49 31 300 ; 
       49 32 300 ; 
       49 33 300 ; 
       49 34 300 ; 
       23 14 300 ; 
       23 15 300 ; 
       23 16 300 ; 
       23 17 300 ; 
       24 0 300 ; 
       24 1 300 ; 
       24 2 300 ; 
       24 3 300 ; 
       24 4 300 ; 
       50 35 300 ; 
       50 36 300 ; 
       50 37 300 ; 
       50 38 300 ; 
       50 39 300 ; 
       50 40 300 ; 
       50 41 300 ; 
       50 42 300 ; 
       25 69 300 ; 
       25 70 300 ; 
       30 82 300 ; 
       31 81 300 ; 
       32 86 300 ; 
       33 85 300 ; 
       34 84 300 ; 
       35 88 300 ; 
       36 83 300 ; 
       37 87 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 52 400 ; 
       9 53 400 ; 
       10 54 400 ; 
       11 55 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 39 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 51 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 37 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       59 44 401 ; 
       61 50 401 ; 
       64 45 401 ; 
       65 40 401 ; 
       67 49 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       75 48 401 ; 
       76 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 92.5 0 0 WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 -3.061593 0 0 0 -3.220569 8.205599 MPRFLG 0 ; 
       1 SCHEM 97.5 0 0 WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 0.185873 0.01233854 1.55021 -4.386421 -0.2763987 22.67802 MPRFLG 0 ; 
       2 SCHEM 95 0 0 WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 2.95572 0.01233854 1.591383 4.386421 -0.2763987 22.67802 MPRFLG 0 ; 
       3 SCHEM 90 0 0 WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 0.1099999 0 0 0 5.941219 -4.858912 MPRFLG 0 ; 
       4 SCHEM 50 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 45 0 0 SRT 1 1 1 0 0 0 0 0 -0.9010603 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 35 -2 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 60 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 65 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 70 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 75 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 77.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 85 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 MPRFLG 0 ; 
       49 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 45 -4 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       50 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 15 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       43 SCHEM 10 -4 0 MPRFLG 0 ; 
       44 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 39 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 267.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 265.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 262.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 297.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 275.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 282.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 257.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 287.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 285.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 295.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 270.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 277.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 260.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 290.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 292.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 272.6812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 280.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 255.1812 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
