SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.1-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       il_frig2_F-light1.1-0 ROOT ; 
       il_frig2_F-light1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       il_frig2_F-mat101.1-0 ; 
       il_frig2_F-mat102.1-0 ; 
       il_frig2_F-mat89.1-0 ; 
       il_frig2_F-mat90.1-0 ; 
       il_frig2_F-mat91.1-0 ; 
       il_frig2_F-mat92.1-0 ; 
       il_frig2_F-mat95.1-0 ; 
       il_frig2_F-mat99.1-0 ; 
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat66.1-0 ; 
       il_frigate_sPtL-mat67.1-0 ; 
       il_frigate_sPtL-mat68.1-0 ; 
       il_frigate_sPtL-mat69.1-0 ; 
       il_frigate_sPtL-mat70.1-0 ; 
       il_frigate_sPtL-mat71.1-0 ; 
       il_frigate_sPtL-mat72.1-0 ; 
       il_frigate_sPtL-mat73.1-0 ; 
       il_frigate_sPtL-mat74.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.1-0 ROOT ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-il_frig2_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       il_frig2_F-t2d65.1-0 ; 
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d42.1-0 ; 
       il_frigate_sPtL-t2d43.1-0 ; 
       il_frigate_sPtL-t2d44.1-0 ; 
       il_frigate_sPtL-t2d45.1-0 ; 
       il_frigate_sPtL-t2d46.1-0 ; 
       il_frigate_sPtL-t2d47.1-0 ; 
       il_frigate_sPtL-t2d48.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       17 0 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 5 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       0 2 110 ; 
       1 5 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 25 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 25 110 ; 
       16 5 110 ; 
       25 2 110 ; 
       26 5 110 ; 
       27 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 1 300 ; 
       18 0 300 ; 
       19 5 300 ; 
       20 4 300 ; 
       21 3 300 ; 
       22 7 300 ; 
       23 2 300 ; 
       24 6 300 ; 
       0 52 300 ; 
       0 53 300 ; 
       0 54 300 ; 
       0 55 300 ; 
       0 56 300 ; 
       0 57 300 ; 
       0 58 300 ; 
       0 59 300 ; 
       0 60 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       5 66 300 ; 
       5 70 300 ; 
       5 71 300 ; 
       5 72 300 ; 
       5 73 300 ; 
       5 74 300 ; 
       5 75 300 ; 
       5 76 300 ; 
       5 84 300 ; 
       5 67 300 ; 
       5 68 300 ; 
       5 69 300 ; 
       5 79 300 ; 
       5 80 300 ; 
       5 81 300 ; 
       5 82 300 ; 
       5 83 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 64 300 ; 
       6 65 300 ; 
       7 51 300 ; 
       7 61 300 ; 
       7 62 300 ; 
       7 63 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 37 300 ; 
       12 38 300 ; 
       12 39 300 ; 
       12 40 300 ; 
       12 41 300 ; 
       12 42 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       15 43 300 ; 
       15 44 300 ; 
       15 45 300 ; 
       15 46 300 ; 
       15 47 300 ; 
       15 48 300 ; 
       15 49 300 ; 
       15 50 300 ; 
       16 77 300 ; 
       16 78 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 39 401 ; 
       10 40 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       14 3 401 ; 
       15 4 401 ; 
       16 5 401 ; 
       17 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 12 401 ; 
       26 0 401 ; 
       27 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       33 16 401 ; 
       34 38 401 ; 
       38 17 401 ; 
       39 18 401 ; 
       40 19 401 ; 
       41 20 401 ; 
       42 21 401 ; 
       46 22 401 ; 
       47 23 401 ; 
       48 24 401 ; 
       49 25 401 ; 
       50 26 401 ; 
       53 27 401 ; 
       54 28 401 ; 
       55 29 401 ; 
       57 30 401 ; 
       58 31 401 ; 
       59 32 401 ; 
       60 33 401 ; 
       61 34 401 ; 
       62 35 401 ; 
       63 36 401 ; 
       65 37 401 ; 
       67 45 401 ; 
       69 51 401 ; 
       72 46 401 ; 
       73 41 401 ; 
       75 50 401 ; 
       78 43 401 ; 
       79 44 401 ; 
       80 47 401 ; 
       81 48 401 ; 
       83 49 401 ; 
       84 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 30 -6 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 35 -6 0 MPRFLG 0 ; 
       0 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 45 -4 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 40 -4 0 MPRFLG 0 ; 
       15 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 MPRFLG 0 ; 
       25 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
