SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.3-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 85     
       il_frigate_sPtL-mat13.1-0 ; 
       il_frigate_sPtL-mat14.1-0 ; 
       il_frigate_sPtL-mat15.1-0 ; 
       il_frigate_sPtL-mat16.1-0 ; 
       il_frigate_sPtL-mat17.1-0 ; 
       il_frigate_sPtL-mat18.1-0 ; 
       il_frigate_sPtL-mat19.1-0 ; 
       il_frigate_sPtL-mat20.1-0 ; 
       il_frigate_sPtL-mat21.1-0 ; 
       il_frigate_sPtL-mat22.1-0 ; 
       il_frigate_sPtL-mat23.1-0 ; 
       il_frigate_sPtL-mat24.1-0 ; 
       il_frigate_sPtL-mat25.1-0 ; 
       il_frigate_sPtL-mat26.1-0 ; 
       il_frigate_sPtL-mat27.1-0 ; 
       il_frigate_sPtL-mat28.1-0 ; 
       il_frigate_sPtL-mat29.1-0 ; 
       il_frigate_sPtL-mat30.1-0 ; 
       il_frigate_sPtL-mat31.1-0 ; 
       il_frigate_sPtL-mat32.1-0 ; 
       il_frigate_sPtL-mat33.1-0 ; 
       il_frigate_sPtL-mat34.1-0 ; 
       il_frigate_sPtL-mat37.1-0 ; 
       il_frigate_sPtL-mat38.1-0 ; 
       il_frigate_sPtL-mat39.1-0 ; 
       il_frigate_sPtL-mat40.1-0 ; 
       il_frigate_sPtL-mat41.1-0 ; 
       il_frigate_sPtL-mat42.1-0 ; 
       il_frigate_sPtL-mat43.1-0 ; 
       il_frigate_sPtL-mat44.1-0 ; 
       il_frigate_sPtL-mat45.1-0 ; 
       il_frigate_sPtL-mat46.1-0 ; 
       il_frigate_sPtL-mat47.1-0 ; 
       il_frigate_sPtL-mat48.1-0 ; 
       il_frigate_sPtL-mat56.1-0 ; 
       il_frigate_sPtL-mat57.1-0 ; 
       il_frigate_sPtL-mat58.1-0 ; 
       il_frigate_sPtL-mat59.1-0 ; 
       il_frigate_sPtL-mat60.1-0 ; 
       il_frigate_sPtL-mat61.1-0 ; 
       il_frigate_sPtL-mat62.1-0 ; 
       il_frigate_sPtL-mat63.1-0 ; 
       il_frigate_sPtL-mat64.1-0 ; 
       il_frigate_sPtL-mat65.1-0 ; 
       il_frigate_sPtL-mat66.1-0 ; 
       il_frigate_sPtL-mat67.1-0 ; 
       il_frigate_sPtL-mat68.1-0 ; 
       il_frigate_sPtL-mat69.1-0 ; 
       il_frigate_sPtL-mat70.1-0 ; 
       il_frigate_sPtL-mat71.1-0 ; 
       il_frigate_sPtL-mat72.1-0 ; 
       il_frigate_sPtL-mat73.1-0 ; 
       il_frigate_sPtL-mat74.1-0 ; 
       il_frigate_sPtL-mat75.1-0 ; 
       il_frigate_sPtL-mat76.1-0 ; 
       il_frigate_sPtL-mat77.1-0 ; 
       il_frigate_sPtL-mat78.1-0 ; 
       il_frigate_sPtL-mat79.1-0 ; 
       il_frigate_sT-mat1_1.1-0 ; 
       il_frigate_sT-mat10_1.1-0 ; 
       il_frigate_sT-mat11_1.1-0 ; 
       il_frigate_sT-mat12_1.1-0 ; 
       il_frigate_sT-mat2_1.1-0 ; 
       il_frigate_sT-mat3_1.1-0 ; 
       il_frigate_sT-mat4_1.1-0 ; 
       il_frigate_sT-mat5_1.1-0 ; 
       il_frigate_sT-mat6_1.1-0 ; 
       il_frigate_sT-mat7_1.1-0 ; 
       il_frigate_sT-mat8_1.1-0 ; 
       il_frigate_sT-mat80_1.1-0 ; 
       il_frigate_sT-mat81_1.1-0 ; 
       il_frigate_sT-mat84_1.1-0 ; 
       il_frigate_sT-mat85_1.1-0 ; 
       il_frigate_sT-mat86_1.1-0 ; 
       il_frigate_sT-mat87_1.1-0 ; 
       il_frigate_sT-mat88_1.1-0 ; 
       il_frigate_sT-mat9_1.1-0 ; 
       reduce_poly_to_542-mat101.1-0 ; 
       reduce_poly_to_542-mat102.1-0 ; 
       reduce_poly_to_542-mat89.1-0 ; 
       reduce_poly_to_542-mat90.1-0 ; 
       reduce_poly_to_542-mat91.1-0 ; 
       reduce_poly_to_542-mat92.1-0 ; 
       reduce_poly_to_542-mat95.1-0 ; 
       reduce_poly_to_542-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.3-0 ROOT ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-reduce_poly_to_542.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       il_frigate_sPtL-t2d10.1-0 ; 
       il_frigate_sPtL-t2d11.1-0 ; 
       il_frigate_sPtL-t2d12.1-0 ; 
       il_frigate_sPtL-t2d13.1-0 ; 
       il_frigate_sPtL-t2d14.1-0 ; 
       il_frigate_sPtL-t2d15.1-0 ; 
       il_frigate_sPtL-t2d16.1-0 ; 
       il_frigate_sPtL-t2d17.1-0 ; 
       il_frigate_sPtL-t2d18.1-0 ; 
       il_frigate_sPtL-t2d19.1-0 ; 
       il_frigate_sPtL-t2d20.1-0 ; 
       il_frigate_sPtL-t2d21.1-0 ; 
       il_frigate_sPtL-t2d22.1-0 ; 
       il_frigate_sPtL-t2d23.1-0 ; 
       il_frigate_sPtL-t2d24.1-0 ; 
       il_frigate_sPtL-t2d26.1-0 ; 
       il_frigate_sPtL-t2d28.1-0 ; 
       il_frigate_sPtL-t2d29.1-0 ; 
       il_frigate_sPtL-t2d30.1-0 ; 
       il_frigate_sPtL-t2d31.1-0 ; 
       il_frigate_sPtL-t2d36.1-0 ; 
       il_frigate_sPtL-t2d37.1-0 ; 
       il_frigate_sPtL-t2d38.1-0 ; 
       il_frigate_sPtL-t2d39.1-0 ; 
       il_frigate_sPtL-t2d40.1-0 ; 
       il_frigate_sPtL-t2d41.1-0 ; 
       il_frigate_sPtL-t2d42.1-0 ; 
       il_frigate_sPtL-t2d43.1-0 ; 
       il_frigate_sPtL-t2d44.1-0 ; 
       il_frigate_sPtL-t2d45.1-0 ; 
       il_frigate_sPtL-t2d46.1-0 ; 
       il_frigate_sPtL-t2d47.1-0 ; 
       il_frigate_sPtL-t2d48.1-0 ; 
       il_frigate_sPtL-t2d49.1-0 ; 
       il_frigate_sPtL-t2d50.1-0 ; 
       il_frigate_sPtL-t2d51.1-0 ; 
       il_frigate_sPtL-t2d52.1-0 ; 
       il_frigate_sPtL-t2d56.1-0 ; 
       il_frigate_sPtL-t2d8.1-0 ; 
       il_frigate_sPtL-t2d9.1-0 ; 
       il_frigate_sT-t2d2_1.1-0 ; 
       il_frigate_sT-t2d5_1.1-0 ; 
       il_frigate_sT-t2d53_1.1-0 ; 
       il_frigate_sT-t2d59_1.1-0 ; 
       il_frigate_sT-t2d6_1.1-0 ; 
       il_frigate_sT-t2d60_1.1-0 ; 
       il_frigate_sT-t2d61_1.1-0 ; 
       il_frigate_sT-t2d62_1.1-0 ; 
       il_frigate_sT-t2d63_1.1-0 ; 
       il_frigate_sT-t2d64_1.1-0 ; 
       il_frigate_sT-t2d7_1.1-0 ; 
       reduce_poly_to_542-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 5 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 25 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 25 110 ; 
       16 5 110 ; 
       17 0 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 5 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       25 2 110 ; 
       26 5 110 ; 
       27 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 44 300 ; 
       0 45 300 ; 
       0 46 300 ; 
       0 47 300 ; 
       0 48 300 ; 
       0 49 300 ; 
       0 50 300 ; 
       0 51 300 ; 
       0 52 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       5 58 300 ; 
       5 62 300 ; 
       5 63 300 ; 
       5 64 300 ; 
       5 65 300 ; 
       5 66 300 ; 
       5 67 300 ; 
       5 68 300 ; 
       5 76 300 ; 
       5 59 300 ; 
       5 60 300 ; 
       5 61 300 ; 
       5 71 300 ; 
       5 72 300 ; 
       5 73 300 ; 
       5 74 300 ; 
       5 75 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 56 300 ; 
       6 57 300 ; 
       7 43 300 ; 
       7 53 300 ; 
       7 54 300 ; 
       7 55 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       14 0 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       15 35 300 ; 
       15 36 300 ; 
       15 37 300 ; 
       15 38 300 ; 
       15 39 300 ; 
       15 40 300 ; 
       15 41 300 ; 
       15 42 300 ; 
       16 69 300 ; 
       16 70 300 ; 
       17 78 300 ; 
       18 77 300 ; 
       19 82 300 ; 
       20 81 300 ; 
       21 80 300 ; 
       22 84 300 ; 
       23 79 300 ; 
       24 83 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 39 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 51 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 37 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       59 44 401 ; 
       61 50 401 ; 
       64 45 401 ; 
       65 40 401 ; 
       67 49 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       75 48 401 ; 
       76 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 45 -4 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 40 -4 0 MPRFLG 0 ; 
       15 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 30 -6 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 35 -6 0 MPRFLG 0 ; 
       25 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       77 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       51 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
