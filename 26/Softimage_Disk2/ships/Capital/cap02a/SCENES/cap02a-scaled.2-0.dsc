SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       scaled-light1.2-0 ROOT ; 
       scaled-light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       il_frigate_sPtL-mat13.3-0 ; 
       il_frigate_sPtL-mat14.3-0 ; 
       il_frigate_sPtL-mat15.3-0 ; 
       il_frigate_sPtL-mat16.3-0 ; 
       il_frigate_sPtL-mat17.3-0 ; 
       il_frigate_sPtL-mat18.3-0 ; 
       il_frigate_sPtL-mat19.3-0 ; 
       il_frigate_sPtL-mat20.3-0 ; 
       il_frigate_sPtL-mat21.3-0 ; 
       il_frigate_sPtL-mat22.3-0 ; 
       il_frigate_sPtL-mat23.3-0 ; 
       il_frigate_sPtL-mat24.3-0 ; 
       il_frigate_sPtL-mat25.3-0 ; 
       il_frigate_sPtL-mat26.3-0 ; 
       il_frigate_sPtL-mat27.3-0 ; 
       il_frigate_sPtL-mat28.3-0 ; 
       il_frigate_sPtL-mat29.3-0 ; 
       il_frigate_sPtL-mat30.3-0 ; 
       il_frigate_sPtL-mat31.3-0 ; 
       il_frigate_sPtL-mat32.3-0 ; 
       il_frigate_sPtL-mat33.3-0 ; 
       il_frigate_sPtL-mat34.3-0 ; 
       il_frigate_sPtL-mat37.3-0 ; 
       il_frigate_sPtL-mat38.3-0 ; 
       il_frigate_sPtL-mat39.3-0 ; 
       il_frigate_sPtL-mat40.3-0 ; 
       il_frigate_sPtL-mat41.3-0 ; 
       il_frigate_sPtL-mat42.3-0 ; 
       il_frigate_sPtL-mat43.3-0 ; 
       il_frigate_sPtL-mat44.3-0 ; 
       il_frigate_sPtL-mat45.3-0 ; 
       il_frigate_sPtL-mat46.3-0 ; 
       il_frigate_sPtL-mat47.3-0 ; 
       il_frigate_sPtL-mat48.3-0 ; 
       il_frigate_sPtL-mat56.3-0 ; 
       il_frigate_sPtL-mat57.3-0 ; 
       il_frigate_sPtL-mat58.3-0 ; 
       il_frigate_sPtL-mat59.3-0 ; 
       il_frigate_sPtL-mat60.3-0 ; 
       il_frigate_sPtL-mat61.3-0 ; 
       il_frigate_sPtL-mat62.3-0 ; 
       il_frigate_sPtL-mat63.3-0 ; 
       il_frigate_sPtL-mat64.3-0 ; 
       il_frigate_sPtL-mat65.3-0 ; 
       il_frigate_sPtL-mat66.3-0 ; 
       il_frigate_sPtL-mat67.3-0 ; 
       il_frigate_sPtL-mat68.3-0 ; 
       il_frigate_sPtL-mat69.3-0 ; 
       il_frigate_sPtL-mat70.3-0 ; 
       il_frigate_sPtL-mat71.3-0 ; 
       il_frigate_sPtL-mat72.3-0 ; 
       il_frigate_sPtL-mat73.3-0 ; 
       il_frigate_sPtL-mat74.3-0 ; 
       il_frigate_sPtL-mat75.3-0 ; 
       il_frigate_sPtL-mat76.3-0 ; 
       il_frigate_sPtL-mat77.3-0 ; 
       il_frigate_sPtL-mat78.3-0 ; 
       il_frigate_sPtL-mat79.3-0 ; 
       il_frigate_sT-mat1_1.3-0 ; 
       il_frigate_sT-mat10_1.3-0 ; 
       il_frigate_sT-mat11_1.3-0 ; 
       il_frigate_sT-mat12_1.3-0 ; 
       il_frigate_sT-mat2_1.3-0 ; 
       il_frigate_sT-mat3_1.3-0 ; 
       il_frigate_sT-mat4_1.3-0 ; 
       il_frigate_sT-mat5_1.3-0 ; 
       il_frigate_sT-mat6_1.3-0 ; 
       il_frigate_sT-mat7_1.3-0 ; 
       il_frigate_sT-mat8_1.3-0 ; 
       il_frigate_sT-mat80_1.3-0 ; 
       il_frigate_sT-mat81_1.3-0 ; 
       il_frigate_sT-mat84_1.3-0 ; 
       il_frigate_sT-mat85_1.3-0 ; 
       il_frigate_sT-mat86_1.3-0 ; 
       il_frigate_sT-mat87_1.3-0 ; 
       il_frigate_sT-mat88_1.3-0 ; 
       il_frigate_sT-mat9_1.3-0 ; 
       scaled-mat1_2.1-0 ; 
       scaled-mat1_3.1-0 ; 
       scaled-mat1_4.1-0 ; 
       scaled-mat1_5.1-0 ; 
       scaled-mat101.1-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat89.1-0 ; 
       scaled-mat90.1-0 ; 
       scaled-mat91.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-mat95.1-0 ; 
       scaled-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cap02a-140deg.2-0 ROOT ; 
       cap02a-170deg.2-0 ROOT ; 
       cap02a-170deg1.2-0 ROOT ; 
       cap02a-175deg.2-0 ROOT ; 
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.8-0 ROOT ; 
       cap02a-cube2.1-0 ; 
       cap02a-cube3.1-0 ; 
       cap02a-cube4.1-0 ; 
       cap02a-cube5.1-0 ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-turwepemt1.1-0 ; 
       cap02a-turwepemt2.1-0 ; 
       cap02a-turwepemt3.1-0 ; 
       cap02a-turwepemt4.1-0 ; 
       cap02a-twepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       il_frigate_sPtL-t2d10.4-0 ; 
       il_frigate_sPtL-t2d11.4-0 ; 
       il_frigate_sPtL-t2d12.4-0 ; 
       il_frigate_sPtL-t2d13.4-0 ; 
       il_frigate_sPtL-t2d14.4-0 ; 
       il_frigate_sPtL-t2d15.4-0 ; 
       il_frigate_sPtL-t2d16.4-0 ; 
       il_frigate_sPtL-t2d17.4-0 ; 
       il_frigate_sPtL-t2d18.4-0 ; 
       il_frigate_sPtL-t2d19.4-0 ; 
       il_frigate_sPtL-t2d20.4-0 ; 
       il_frigate_sPtL-t2d21.4-0 ; 
       il_frigate_sPtL-t2d22.4-0 ; 
       il_frigate_sPtL-t2d23.4-0 ; 
       il_frigate_sPtL-t2d24.4-0 ; 
       il_frigate_sPtL-t2d26.4-0 ; 
       il_frigate_sPtL-t2d28.4-0 ; 
       il_frigate_sPtL-t2d29.4-0 ; 
       il_frigate_sPtL-t2d30.4-0 ; 
       il_frigate_sPtL-t2d31.4-0 ; 
       il_frigate_sPtL-t2d36.4-0 ; 
       il_frigate_sPtL-t2d37.4-0 ; 
       il_frigate_sPtL-t2d38.4-0 ; 
       il_frigate_sPtL-t2d39.4-0 ; 
       il_frigate_sPtL-t2d40.4-0 ; 
       il_frigate_sPtL-t2d41.4-0 ; 
       il_frigate_sPtL-t2d42.4-0 ; 
       il_frigate_sPtL-t2d43.4-0 ; 
       il_frigate_sPtL-t2d44.4-0 ; 
       il_frigate_sPtL-t2d45.4-0 ; 
       il_frigate_sPtL-t2d46.4-0 ; 
       il_frigate_sPtL-t2d47.4-0 ; 
       il_frigate_sPtL-t2d48.4-0 ; 
       il_frigate_sPtL-t2d49.4-0 ; 
       il_frigate_sPtL-t2d50.4-0 ; 
       il_frigate_sPtL-t2d51.4-0 ; 
       il_frigate_sPtL-t2d52.4-0 ; 
       il_frigate_sPtL-t2d56.4-0 ; 
       il_frigate_sPtL-t2d8.4-0 ; 
       il_frigate_sPtL-t2d9.4-0 ; 
       il_frigate_sT-t2d2_1.4-0 ; 
       il_frigate_sT-t2d5_1.4-0 ; 
       il_frigate_sT-t2d53_1.4-0 ; 
       il_frigate_sT-t2d59_1.4-0 ; 
       il_frigate_sT-t2d6_1.4-0 ; 
       il_frigate_sT-t2d60_1.4-0 ; 
       il_frigate_sT-t2d61_1.4-0 ; 
       il_frigate_sT-t2d62_1.4-0 ; 
       il_frigate_sT-t2d63_1.4-0 ; 
       il_frigate_sT-t2d64_1.4-0 ; 
       il_frigate_sT-t2d7_1.4-0 ; 
       scaled-t2d65.1-0 ; 
       scaled-t2d66.1-0 ; 
       scaled-t2d67.1-0 ; 
       scaled-t2d68.1-0 ; 
       scaled-t2d69.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       37 9 110 ; 
       38 10 110 ; 
       4 6 110 ; 
       5 13 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 14 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 33 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 33 110 ; 
       24 13 110 ; 
       25 4 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 23 110 ; 
       32 23 110 ; 
       33 6 110 ; 
       34 13 110 ; 
       39 14 110 ; 
       35 7 110 ; 
       36 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 44 300 ; 
       4 45 300 ; 
       4 46 300 ; 
       4 47 300 ; 
       4 48 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       4 51 300 ; 
       4 52 300 ; 
       7 77 300 ; 
       8 78 300 ; 
       9 79 300 ; 
       10 80 300 ; 
       12 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 58 300 ; 
       13 62 300 ; 
       13 63 300 ; 
       13 64 300 ; 
       13 65 300 ; 
       13 66 300 ; 
       13 67 300 ; 
       13 68 300 ; 
       13 76 300 ; 
       13 59 300 ; 
       13 60 300 ; 
       13 61 300 ; 
       13 71 300 ; 
       13 72 300 ; 
       13 73 300 ; 
       13 74 300 ; 
       13 75 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       14 24 300 ; 
       14 25 300 ; 
       14 26 300 ; 
       14 56 300 ; 
       14 57 300 ; 
       15 43 300 ; 
       15 53 300 ; 
       15 54 300 ; 
       15 55 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       18 12 300 ; 
       18 13 300 ; 
       19 5 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       20 27 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       20 31 300 ; 
       20 32 300 ; 
       20 33 300 ; 
       20 34 300 ; 
       21 14 300 ; 
       21 15 300 ; 
       21 16 300 ; 
       21 17 300 ; 
       22 0 300 ; 
       22 1 300 ; 
       22 2 300 ; 
       22 3 300 ; 
       22 4 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       23 38 300 ; 
       23 39 300 ; 
       23 40 300 ; 
       23 41 300 ; 
       23 42 300 ; 
       24 69 300 ; 
       24 70 300 ; 
       25 82 300 ; 
       26 81 300 ; 
       27 86 300 ; 
       28 85 300 ; 
       29 84 300 ; 
       30 88 300 ; 
       31 83 300 ; 
       32 87 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 52 400 ; 
       8 53 400 ; 
       9 54 400 ; 
       10 55 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 39 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 51 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 37 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       59 44 401 ; 
       61 50 401 ; 
       64 45 401 ; 
       65 40 401 ; 
       67 49 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       75 48 401 ; 
       76 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 43.07884 14.89098 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40.50974 14.81342 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 64.56044 -13.72309 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 -3.061593 0 0 0 -3.220569 8.205599 MPRFLG 0 ; 
       37 SCHEM 69.21223 -11.93225 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 73.50697 -13.52622 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 0.185873 0.01233854 1.55021 -4.386421 -0.2763987 22.67802 MPRFLG 0 ; 
       38 SCHEM 73.49467 -11.90833 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 69.13269 -13.61559 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 2.95572 0.01233854 1.591383 4.386421 -0.2763987 22.67802 MPRFLG 0 ; 
       3 SCHEM 59.97648 -13.50252 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.771938 9.771938 9.771938 0.1099999 0 0 0 5.941219 -4.858912 MPRFLG 0 ; 
       4 SCHEM 59.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 32 -4 0 MPRFLG 0 ; 
       6 SCHEM 37 0 0 SRT 1 1 1 0 0 0 0 0 -0.9010603 MPRFLG 0 ; 
       7 SCHEM 59.87091 -9.658305 0 USR MPRFLG 0 ; 
       8 SCHEM 64.51459 -9.725003 0 USR MPRFLG 0 ; 
       9 SCHEM 69.14724 -9.401638 0 USR MPRFLG 0 ; 
       10 SCHEM 73.43122 -9.561992 0 USR MPRFLG 0 ; 
       11 SCHEM 53.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 18.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 30.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 18.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 14.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 27 -4 0 MPRFLG 0 ; 
       17 SCHEM 29.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 57 -4 0 MPRFLG 0 ; 
       19 SCHEM 49.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 40.75 -4 0 MPRFLG 0 ; 
       21 SCHEM 54.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 52 -4 0 MPRFLG 0 ; 
       23 SCHEM 45.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 37 -4 0 MPRFLG 0 ; 
       25 SCHEM 59.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 19.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 17 -6 0 MPRFLG 0 ; 
       28 SCHEM 34.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 39.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 42 -6 0 MPRFLG 0 ; 
       31 SCHEM 44.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 47 -6 0 MPRFLG 0 ; 
       33 SCHEM 43.25 -2 0 MPRFLG 0 ; 
       34 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 22 -4 0 MPRFLG 0 ; 
       35 SCHEM 59.84858 -11.86049 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 64.58343 -11.93225 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 53.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 53.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 53.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 53.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 43.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 13.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 13.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 13.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 13.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 36 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 36 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 63.5 7.569401 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 74.62909 24.79711 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 94.74359 59.31922 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 130.34 128.0401 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 18.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 58.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 33.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 16 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 46 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 41 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 53.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 53.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 53.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 23.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 48.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 13.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 13.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 13.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 23.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 23.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 51 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 51 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 36 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 21 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 68.5 7.569401 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 79.62909 24.79711 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 99.74359 59.31922 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 135.34 128.0401 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 137.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 137.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 137.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 210.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 152.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 177.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 119 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 195.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 195.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 210.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 152.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 177.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 119 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 195.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 152.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 177.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 119 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
