SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.10-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 81     
       il_frigate_sPtL-mat13.3-0 ; 
       il_frigate_sPtL-mat14.3-0 ; 
       il_frigate_sPtL-mat15.3-0 ; 
       il_frigate_sPtL-mat16.3-0 ; 
       il_frigate_sPtL-mat17.3-0 ; 
       il_frigate_sPtL-mat18.3-0 ; 
       il_frigate_sPtL-mat19.3-0 ; 
       il_frigate_sPtL-mat20.3-0 ; 
       il_frigate_sPtL-mat21.3-0 ; 
       il_frigate_sPtL-mat22.3-0 ; 
       il_frigate_sPtL-mat23.3-0 ; 
       il_frigate_sPtL-mat24.3-0 ; 
       il_frigate_sPtL-mat25.3-0 ; 
       il_frigate_sPtL-mat26.3-0 ; 
       il_frigate_sPtL-mat27.3-0 ; 
       il_frigate_sPtL-mat28.3-0 ; 
       il_frigate_sPtL-mat29.3-0 ; 
       il_frigate_sPtL-mat30.3-0 ; 
       il_frigate_sPtL-mat31.3-0 ; 
       il_frigate_sPtL-mat32.3-0 ; 
       il_frigate_sPtL-mat33.3-0 ; 
       il_frigate_sPtL-mat34.3-0 ; 
       il_frigate_sPtL-mat37.3-0 ; 
       il_frigate_sPtL-mat38.3-0 ; 
       il_frigate_sPtL-mat39.3-0 ; 
       il_frigate_sPtL-mat40.3-0 ; 
       il_frigate_sPtL-mat41.3-0 ; 
       il_frigate_sPtL-mat42.3-0 ; 
       il_frigate_sPtL-mat43.3-0 ; 
       il_frigate_sPtL-mat44.3-0 ; 
       il_frigate_sPtL-mat45.3-0 ; 
       il_frigate_sPtL-mat46.3-0 ; 
       il_frigate_sPtL-mat47.3-0 ; 
       il_frigate_sPtL-mat48.3-0 ; 
       il_frigate_sPtL-mat56.3-0 ; 
       il_frigate_sPtL-mat57.3-0 ; 
       il_frigate_sPtL-mat58.3-0 ; 
       il_frigate_sPtL-mat59.3-0 ; 
       il_frigate_sPtL-mat60.3-0 ; 
       il_frigate_sPtL-mat61.3-0 ; 
       il_frigate_sPtL-mat62.3-0 ; 
       il_frigate_sPtL-mat63.3-0 ; 
       il_frigate_sPtL-mat64.3-0 ; 
       il_frigate_sPtL-mat65.3-0 ; 
       il_frigate_sPtL-mat66.3-0 ; 
       il_frigate_sPtL-mat67.3-0 ; 
       il_frigate_sPtL-mat68.3-0 ; 
       il_frigate_sPtL-mat69.3-0 ; 
       il_frigate_sPtL-mat70.3-0 ; 
       il_frigate_sPtL-mat71.3-0 ; 
       il_frigate_sPtL-mat72.3-0 ; 
       il_frigate_sPtL-mat73.3-0 ; 
       il_frigate_sPtL-mat74.3-0 ; 
       il_frigate_sPtL-mat75.3-0 ; 
       il_frigate_sPtL-mat76.3-0 ; 
       il_frigate_sPtL-mat77.3-0 ; 
       il_frigate_sPtL-mat78.3-0 ; 
       il_frigate_sPtL-mat79.3-0 ; 
       il_frigate_sT-mat1_1.3-0 ; 
       il_frigate_sT-mat10_1.3-0 ; 
       il_frigate_sT-mat11_1.3-0 ; 
       il_frigate_sT-mat12_1.3-0 ; 
       il_frigate_sT-mat2_1.3-0 ; 
       il_frigate_sT-mat3_1.3-0 ; 
       il_frigate_sT-mat4_1.3-0 ; 
       il_frigate_sT-mat5_1.3-0 ; 
       il_frigate_sT-mat6_1.3-0 ; 
       il_frigate_sT-mat7_1.3-0 ; 
       il_frigate_sT-mat8_1.3-0 ; 
       il_frigate_sT-mat80_1.3-0 ; 
       il_frigate_sT-mat81_1.3-0 ; 
       il_frigate_sT-mat84_1.3-0 ; 
       il_frigate_sT-mat85_1.3-0 ; 
       il_frigate_sT-mat86_1.3-0 ; 
       il_frigate_sT-mat87_1.3-0 ; 
       il_frigate_sT-mat88_1.3-0 ; 
       il_frigate_sT-mat9_1.3-0 ; 
       scaled-mat1_2.1-0 ; 
       scaled-mat1_3.1-0 ; 
       scaled-mat1_4.1-0 ; 
       scaled-mat1_5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       STATIC-antenn.1-0 ; 
       STATIC-bturatt.1-0 ; 
       STATIC-cap02a.1-0 ROOT ; 
       STATIC-cube2.1-0 ; 
       STATIC-cube3.1-0 ; 
       STATIC-cube4.1-0 ; 
       STATIC-cube5.1-0 ; 
       STATIC-engine0.1-0 ; 
       STATIC-finzzz.1-0 ; 
       STATIC-fuselg1.1-0 ; 
       STATIC-fuselg2.1-0 ; 
       STATIC-fuselg3.1-0 ; 
       STATIC-lbengine.1-0 ; 
       STATIC-ltengine.1-0 ; 
       STATIC-qq.1-0 ; 
       STATIC-rbengine.1-0 ; 
       STATIC-rtengine.1-0 ; 
       STATIC-slicer.1-0 ; 
       STATIC-tturatt.1-0 ; 
       STATIC-w.1-0 ; 
       STATIC-w1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       il_frigate_sPtL-t2d10.6-0 ; 
       il_frigate_sPtL-t2d11.6-0 ; 
       il_frigate_sPtL-t2d12.6-0 ; 
       il_frigate_sPtL-t2d13.6-0 ; 
       il_frigate_sPtL-t2d14.6-0 ; 
       il_frigate_sPtL-t2d15.6-0 ; 
       il_frigate_sPtL-t2d16.6-0 ; 
       il_frigate_sPtL-t2d17.6-0 ; 
       il_frigate_sPtL-t2d18.6-0 ; 
       il_frigate_sPtL-t2d19.6-0 ; 
       il_frigate_sPtL-t2d20.6-0 ; 
       il_frigate_sPtL-t2d21.6-0 ; 
       il_frigate_sPtL-t2d22.6-0 ; 
       il_frigate_sPtL-t2d23.6-0 ; 
       il_frigate_sPtL-t2d24.6-0 ; 
       il_frigate_sPtL-t2d26.6-0 ; 
       il_frigate_sPtL-t2d28.6-0 ; 
       il_frigate_sPtL-t2d29.6-0 ; 
       il_frigate_sPtL-t2d30.6-0 ; 
       il_frigate_sPtL-t2d31.6-0 ; 
       il_frigate_sPtL-t2d36.6-0 ; 
       il_frigate_sPtL-t2d37.6-0 ; 
       il_frigate_sPtL-t2d38.6-0 ; 
       il_frigate_sPtL-t2d39.6-0 ; 
       il_frigate_sPtL-t2d40.6-0 ; 
       il_frigate_sPtL-t2d41.6-0 ; 
       il_frigate_sPtL-t2d42.6-0 ; 
       il_frigate_sPtL-t2d43.6-0 ; 
       il_frigate_sPtL-t2d44.6-0 ; 
       il_frigate_sPtL-t2d45.6-0 ; 
       il_frigate_sPtL-t2d46.6-0 ; 
       il_frigate_sPtL-t2d47.6-0 ; 
       il_frigate_sPtL-t2d48.6-0 ; 
       il_frigate_sPtL-t2d49.6-0 ; 
       il_frigate_sPtL-t2d50.6-0 ; 
       il_frigate_sPtL-t2d51.6-0 ; 
       il_frigate_sPtL-t2d52.6-0 ; 
       il_frigate_sPtL-t2d56.6-0 ; 
       il_frigate_sPtL-t2d8.6-0 ; 
       il_frigate_sPtL-t2d9.6-0 ; 
       il_frigate_sT-t2d2_1.7-0 ; 
       il_frigate_sT-t2d5_1.7-0 ; 
       il_frigate_sT-t2d53_1.6-0 ; 
       il_frigate_sT-t2d59_1.7-0 ; 
       il_frigate_sT-t2d6_1.7-0 ; 
       il_frigate_sT-t2d60_1.7-0 ; 
       il_frigate_sT-t2d61_1.7-0 ; 
       il_frigate_sT-t2d62_1.7-0 ; 
       il_frigate_sT-t2d63_1.7-0 ; 
       il_frigate_sT-t2d64_1.7-0 ; 
       il_frigate_sT-t2d7_1.7-0 ; 
       scaled-t2d65.3-0 ; 
       scaled-t2d66.3-0 ; 
       scaled-t2d67.3-0 ; 
       scaled-t2d68.3-0 ; 
       scaled-t2d69.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 9 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 10 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 10 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 2 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       19 14 110 ; 
       20 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 44 300 ; 
       0 45 300 ; 
       0 46 300 ; 
       0 47 300 ; 
       0 48 300 ; 
       0 49 300 ; 
       0 50 300 ; 
       0 51 300 ; 
       0 52 300 ; 
       3 77 300 ; 
       4 78 300 ; 
       5 79 300 ; 
       6 80 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       9 58 300 ; 
       9 62 300 ; 
       9 63 300 ; 
       9 64 300 ; 
       9 65 300 ; 
       9 66 300 ; 
       9 67 300 ; 
       9 68 300 ; 
       9 76 300 ; 
       9 59 300 ; 
       9 60 300 ; 
       9 61 300 ; 
       9 71 300 ; 
       9 72 300 ; 
       9 73 300 ; 
       9 74 300 ; 
       9 75 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       10 56 300 ; 
       10 57 300 ; 
       11 43 300 ; 
       11 53 300 ; 
       11 54 300 ; 
       11 55 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       12 13 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       16 0 300 ; 
       16 1 300 ; 
       16 2 300 ; 
       16 3 300 ; 
       16 4 300 ; 
       17 69 300 ; 
       17 70 300 ; 
       19 27 300 ; 
       19 28 300 ; 
       19 29 300 ; 
       19 30 300 ; 
       19 31 300 ; 
       19 32 300 ; 
       19 33 300 ; 
       19 34 300 ; 
       20 35 300 ; 
       20 36 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       20 39 300 ; 
       20 40 300 ; 
       20 41 300 ; 
       20 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 52 400 ; 
       4 53 400 ; 
       5 54 400 ; 
       6 55 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 39 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 51 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 37 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       59 44 401 ; 
       61 50 401 ; 
       64 45 401 ; 
       65 40 401 ; 
       67 49 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       75 48 401 ; 
       76 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 -0.01166344 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 MPRFLG 0 ; 
       10 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 MPRFLG 0 ; 
       14 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 35 -4 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 36.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 34 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 16.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 39 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 6.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 19 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 21.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 24 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 26.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 31.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 29 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 29 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 36.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 34 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 39 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 6.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 31.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 31.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 11.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 14 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 19 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 21.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 24 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 26.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 267.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 265.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 262.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 297.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 275.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 282.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 257.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 287.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 285.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 295.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 270.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 277.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 260.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 290.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 292.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 272.6812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 280.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 255.1812 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
