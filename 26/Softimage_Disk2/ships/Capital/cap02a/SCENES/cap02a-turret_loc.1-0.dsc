SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 19     
       cap02a-cap02a.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8_1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.5-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       il_frigate_sPtL-mat13.2-0 ; 
       il_frigate_sPtL-mat14.2-0 ; 
       il_frigate_sPtL-mat15.2-0 ; 
       il_frigate_sPtL-mat16.2-0 ; 
       il_frigate_sPtL-mat17.2-0 ; 
       il_frigate_sPtL-mat18.2-0 ; 
       il_frigate_sPtL-mat19.2-0 ; 
       il_frigate_sPtL-mat20.2-0 ; 
       il_frigate_sPtL-mat21.2-0 ; 
       il_frigate_sPtL-mat22.2-0 ; 
       il_frigate_sPtL-mat23.2-0 ; 
       il_frigate_sPtL-mat24.2-0 ; 
       il_frigate_sPtL-mat25.2-0 ; 
       il_frigate_sPtL-mat26.2-0 ; 
       il_frigate_sPtL-mat27.2-0 ; 
       il_frigate_sPtL-mat28.2-0 ; 
       il_frigate_sPtL-mat29.2-0 ; 
       il_frigate_sPtL-mat30.2-0 ; 
       il_frigate_sPtL-mat31.2-0 ; 
       il_frigate_sPtL-mat32.2-0 ; 
       il_frigate_sPtL-mat33.2-0 ; 
       il_frigate_sPtL-mat34.2-0 ; 
       il_frigate_sPtL-mat37.2-0 ; 
       il_frigate_sPtL-mat38.2-0 ; 
       il_frigate_sPtL-mat39.2-0 ; 
       il_frigate_sPtL-mat40.2-0 ; 
       il_frigate_sPtL-mat41.2-0 ; 
       il_frigate_sPtL-mat42.2-0 ; 
       il_frigate_sPtL-mat43.2-0 ; 
       il_frigate_sPtL-mat44.2-0 ; 
       il_frigate_sPtL-mat45.2-0 ; 
       il_frigate_sPtL-mat46.2-0 ; 
       il_frigate_sPtL-mat47.2-0 ; 
       il_frigate_sPtL-mat48.2-0 ; 
       il_frigate_sPtL-mat56.2-0 ; 
       il_frigate_sPtL-mat57.2-0 ; 
       il_frigate_sPtL-mat58.2-0 ; 
       il_frigate_sPtL-mat59.2-0 ; 
       il_frigate_sPtL-mat60.2-0 ; 
       il_frigate_sPtL-mat61.2-0 ; 
       il_frigate_sPtL-mat62.2-0 ; 
       il_frigate_sPtL-mat63.2-0 ; 
       il_frigate_sPtL-mat64.2-0 ; 
       il_frigate_sPtL-mat65.2-0 ; 
       il_frigate_sPtL-mat66.2-0 ; 
       il_frigate_sPtL-mat67.2-0 ; 
       il_frigate_sPtL-mat68.2-0 ; 
       il_frigate_sPtL-mat69.2-0 ; 
       il_frigate_sPtL-mat70.2-0 ; 
       il_frigate_sPtL-mat71.2-0 ; 
       il_frigate_sPtL-mat72.2-0 ; 
       il_frigate_sPtL-mat73.2-0 ; 
       il_frigate_sPtL-mat74.2-0 ; 
       il_frigate_sPtL-mat75.2-0 ; 
       il_frigate_sPtL-mat76.2-0 ; 
       il_frigate_sPtL-mat77.2-0 ; 
       il_frigate_sPtL-mat78.2-0 ; 
       il_frigate_sPtL-mat79.2-0 ; 
       il_frigate_sT-mat1_1.2-0 ; 
       il_frigate_sT-mat10_1.2-0 ; 
       il_frigate_sT-mat11_1.2-0 ; 
       il_frigate_sT-mat12_1.2-0 ; 
       il_frigate_sT-mat2_1.2-0 ; 
       il_frigate_sT-mat3_1.2-0 ; 
       il_frigate_sT-mat4_1.2-0 ; 
       il_frigate_sT-mat5_1.2-0 ; 
       il_frigate_sT-mat6_1.2-0 ; 
       il_frigate_sT-mat7_1.2-0 ; 
       il_frigate_sT-mat8_1.2-0 ; 
       il_frigate_sT-mat80_1.2-0 ; 
       il_frigate_sT-mat81_1.2-0 ; 
       il_frigate_sT-mat84_1.2-0 ; 
       il_frigate_sT-mat85_1.2-0 ; 
       il_frigate_sT-mat86_1.2-0 ; 
       il_frigate_sT-mat87_1.2-0 ; 
       il_frigate_sT-mat88_1.2-0 ; 
       il_frigate_sT-mat9_1.2-0 ; 
       turret_loc-mat101.1-0 ; 
       turret_loc-mat102.1-0 ; 
       turret_loc-mat103.1-0 ; 
       turret_loc-mat104.1-0 ; 
       turret_loc-mat106.1-0 ; 
       turret_loc-mat107.1-0 ; 
       turret_loc-mat89.1-0 ; 
       turret_loc-mat90.1-0 ; 
       turret_loc-mat91.1-0 ; 
       turret_loc-mat92.1-0 ; 
       turret_loc-mat95.1-0 ; 
       turret_loc-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       arc-cone.1-0 ROOT ; 
       cap02a-antenn.1-0 ; 
       cap02a-bturatt.1-0 ; 
       cap02a-cap02a.4-0 ROOT ; 
       cap02a-engine0.1-0 ; 
       cap02a-finzzz.1-0 ; 
       cap02a-fuselg1.1-0 ; 
       cap02a-fuselg2.1-0 ; 
       cap02a-fuselg3.1-0 ; 
       cap02a-fwepemt.1-0 ; 
       cap02a-fwepemt2.1-0 ; 
       cap02a-lbengine.1-0 ; 
       cap02a-ltengine.1-0 ; 
       cap02a-lthrust.1-0 ; 
       cap02a-rbengine.1-0 ; 
       cap02a-rtengine.1-0 ; 
       cap02a-rthrust.1-0 ; 
       cap02a-slicer.1-0 ; 
       cap02a-SSa.1-0 ; 
       cap02a-SSab.1-0 ; 
       cap02a-SSat.1-0 ; 
       cap02a-SSf.1-0 ; 
       cap02a-SSl1.1-0 ; 
       cap02a-SSl2.1-0 ; 
       cap02a-SSr1.1-0 ; 
       cap02a-SSr2.1-0 ; 
       cap02a-thrust0.1-0 ; 
       cap02a-tturatt.1-0 ; 
       cap02a-twepemt.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
       turret_loc-cone2.1-0 ROOT ; 
       turret_loc-cone3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap02a/PICTURES/cap02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap02a-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       il_frigate_sPtL-t2d10.2-0 ; 
       il_frigate_sPtL-t2d11.2-0 ; 
       il_frigate_sPtL-t2d12.2-0 ; 
       il_frigate_sPtL-t2d13.2-0 ; 
       il_frigate_sPtL-t2d14.2-0 ; 
       il_frigate_sPtL-t2d15.2-0 ; 
       il_frigate_sPtL-t2d16.2-0 ; 
       il_frigate_sPtL-t2d17.2-0 ; 
       il_frigate_sPtL-t2d18.2-0 ; 
       il_frigate_sPtL-t2d19.2-0 ; 
       il_frigate_sPtL-t2d20.2-0 ; 
       il_frigate_sPtL-t2d21.2-0 ; 
       il_frigate_sPtL-t2d22.2-0 ; 
       il_frigate_sPtL-t2d23.2-0 ; 
       il_frigate_sPtL-t2d24.2-0 ; 
       il_frigate_sPtL-t2d26.2-0 ; 
       il_frigate_sPtL-t2d28.2-0 ; 
       il_frigate_sPtL-t2d29.2-0 ; 
       il_frigate_sPtL-t2d30.2-0 ; 
       il_frigate_sPtL-t2d31.2-0 ; 
       il_frigate_sPtL-t2d36.2-0 ; 
       il_frigate_sPtL-t2d37.2-0 ; 
       il_frigate_sPtL-t2d38.2-0 ; 
       il_frigate_sPtL-t2d39.2-0 ; 
       il_frigate_sPtL-t2d40.2-0 ; 
       il_frigate_sPtL-t2d41.2-0 ; 
       il_frigate_sPtL-t2d42.2-0 ; 
       il_frigate_sPtL-t2d43.2-0 ; 
       il_frigate_sPtL-t2d44.2-0 ; 
       il_frigate_sPtL-t2d45.2-0 ; 
       il_frigate_sPtL-t2d46.2-0 ; 
       il_frigate_sPtL-t2d47.2-0 ; 
       il_frigate_sPtL-t2d48.2-0 ; 
       il_frigate_sPtL-t2d49.2-0 ; 
       il_frigate_sPtL-t2d50.2-0 ; 
       il_frigate_sPtL-t2d51.2-0 ; 
       il_frigate_sPtL-t2d52.2-0 ; 
       il_frigate_sPtL-t2d56.2-0 ; 
       il_frigate_sPtL-t2d8.2-0 ; 
       il_frigate_sPtL-t2d9.2-0 ; 
       il_frigate_sT-t2d2_1.2-0 ; 
       il_frigate_sT-t2d5_1.2-0 ; 
       il_frigate_sT-t2d53_1.2-0 ; 
       il_frigate_sT-t2d59_1.2-0 ; 
       il_frigate_sT-t2d6_1.2-0 ; 
       il_frigate_sT-t2d60_1.2-0 ; 
       il_frigate_sT-t2d61_1.2-0 ; 
       il_frigate_sT-t2d62_1.2-0 ; 
       il_frigate_sT-t2d63_1.2-0 ; 
       il_frigate_sT-t2d64_1.2-0 ; 
       il_frigate_sT-t2d7_1.2-0 ; 
       turret_loc-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 6 110 ; 
       4 3 110 ; 
       5 7 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 26 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 26 110 ; 
       17 6 110 ; 
       18 1 110 ; 
       19 5 110 ; 
       20 5 110 ; 
       21 6 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 3 110 ; 
       27 6 110 ; 
       28 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 79 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 48 300 ; 
       1 49 300 ; 
       1 50 300 ; 
       1 51 300 ; 
       1 52 300 ; 
       29 80 300 ; 
       30 81 300 ; 
       31 82 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       6 58 300 ; 
       6 62 300 ; 
       6 63 300 ; 
       6 64 300 ; 
       6 65 300 ; 
       6 66 300 ; 
       6 67 300 ; 
       6 68 300 ; 
       6 76 300 ; 
       6 59 300 ; 
       6 60 300 ; 
       6 61 300 ; 
       6 71 300 ; 
       6 72 300 ; 
       6 73 300 ; 
       6 74 300 ; 
       6 75 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 56 300 ; 
       7 57 300 ; 
       8 43 300 ; 
       8 53 300 ; 
       8 54 300 ; 
       8 55 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 5 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       13 27 300 ; 
       13 28 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       13 33 300 ; 
       13 34 300 ; 
       14 14 300 ; 
       14 15 300 ; 
       14 16 300 ; 
       14 17 300 ; 
       15 0 300 ; 
       15 1 300 ; 
       15 2 300 ; 
       15 3 300 ; 
       15 4 300 ; 
       16 35 300 ; 
       16 36 300 ; 
       16 37 300 ; 
       16 38 300 ; 
       16 39 300 ; 
       16 40 300 ; 
       16 41 300 ; 
       16 42 300 ; 
       17 69 300 ; 
       17 70 300 ; 
       18 78 300 ; 
       19 77 300 ; 
       20 86 300 ; 
       21 85 300 ; 
       22 84 300 ; 
       23 88 300 ; 
       24 83 300 ; 
       25 87 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 39 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 51 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       25 15 401 ; 
       26 37 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       38 21 401 ; 
       39 22 401 ; 
       40 23 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       45 26 401 ; 
       46 27 401 ; 
       47 28 401 ; 
       49 29 401 ; 
       50 30 401 ; 
       51 31 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       59 44 401 ; 
       61 50 401 ; 
       64 45 401 ; 
       65 40 401 ; 
       67 49 401 ; 
       70 42 401 ; 
       71 43 401 ; 
       72 46 401 ; 
       73 47 401 ; 
       75 48 401 ; 
       76 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 52.5 0 0 DISPLAY 1 2 SRT 10.36435 10.36435 10.36435 -0.4100001 0 0 0 3.77816 18.42768 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 55 0 0 SRT 10.36435 10.36435 10.36435 -2.801593 0 0 0 -2.393981 2.371239 MPRFLG 0 ; 
       30 SCHEM 65 0 0 SRT 10.36435 10.36435 10.36435 -0.54 3.141593 -1.570796 4.686642 -0.2693945 19.81185 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 MPRFLG 0 ; 
       31 SCHEM 67.5 0 0 SRT 10.36435 10.36435 10.36435 0.54 1.509958e-007 1.570796 -4.686642 -0.2693945 19.81185 MPRFLG 0 ; 
       3 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 45 -4 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 MPRFLG 0 ; 
       16 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 30 -6 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 35 -6 0 MPRFLG 0 ; 
       26 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 193.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 208.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 150.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 175.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 117 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
