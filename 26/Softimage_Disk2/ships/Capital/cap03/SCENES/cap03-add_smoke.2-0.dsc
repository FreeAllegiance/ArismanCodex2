SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap03-cap03.92-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_carrier_sPTd-cam_int1.94-0 ROOT ; 
       utann_carrier_sPTd-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_smoke-light1.2-0 ROOT ; 
       add_smoke-light2.2-0 ROOT ; 
       add_smoke-light3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_smoke-back1.1-0 ; 
       add_smoke-mat54.1-0 ; 
       add_smoke-mat57.1-0 ; 
       add_smoke-mat58.1-0 ; 
       add_smoke-nose_white-center.1-11.1-0 ; 
       add_smoke-nose_white-center.1-12.1-0 ; 
       add_smoke-nose_white-center.1-15.1-0 ; 
       add_smoke-nose_white-center.1-31.1-0 ; 
       add_smoke-nose_white-center.1-32.1-0 ; 
       add_smoke-nose_white-center.1-35.1-0 ; 
       add_smoke-nose_white-center.1-38.1-0 ; 
       add_smoke-nose_white-center.1-41.1-0 ; 
       add_smoke-nose_white-center.1-7.1-0 ; 
       add_smoke-port_red-left.1-10.1-0 ; 
       add_smoke-port_red-left.1-18.1-0 ; 
       add_smoke-port_red-left.1-21.1-0 ; 
       add_smoke-port_red-left.1-4.1-0 ; 
       add_smoke-port_red-left.1-5.1-0 ; 
       add_smoke-port_red-left.1-8.1-0 ; 
       add_smoke-sides1.1-0 ; 
       add_smoke-starbord_green-right.1-1.1-0 ; 
       add_smoke-starbord_green-right.1-10.1-0 ; 
       add_smoke-starbord_green-right.1-11.1-0 ; 
       add_smoke-starbord_green-right.1-12.1-0 ; 
       add_smoke-starbord_green-right.1-3.1-0 ; 
       add_smoke-starbord_green-right.1-6.1-0 ; 
       add_smoke-starbord_green-right.1-7.1-0 ; 
       add_smoke-starbord_green-right.1-8.1-0 ; 
       add_smoke-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.3-0 ; 
       utann_carrier_sPTd-bottom.1-0 ; 
       utann_carrier_sPTd-default.2-0 ; 
       utann_carrier_sPTd-default1.1-0 ; 
       utann_carrier_sPTd-mat12.1-0 ; 
       utann_carrier_sPTd-mat22.4-0 ; 
       utann_carrier_sPTd-mat31.1-0 ; 
       utann_carrier_sPTd-mat32.1-0 ; 
       utann_carrier_sPTd-mat39.1-0 ; 
       utann_carrier_sPTd-mat40.1-0 ; 
       utann_carrier_sPTd-mat5.1-0 ; 
       utann_carrier_sPTd-mat52.1-0 ; 
       utann_carrier_sPTd-mat53.1-0 ; 
       utann_carrier_sPTd-mat8.1-0 ; 
       utann_carrier_sPTd-mat9.1-0 ; 
       utann_carrier_sPTd-side.3-0 ; 
       utann_carrier_sPTd-top.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 71     
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-blthrust.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-brthrust.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.67-0 ROOT ; 
       cap03-cockpt.1-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret1.1-0 ; 
       cap03-lturret2.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-missemt.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret1.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-smoke.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trail.1-0 ; 
       cap03-trlndpad.1-0 ; 
       cap03-tthrust.1-0 ; 
       cap03-turwepemt1.1-0 ; 
       cap03-turwepemt2.1-0 ; 
       cap03-turwepemt3.1-0 ; 
       cap03-turwepemt4.1-0 ; 
       cap03-turwepemt5.1-0 ; 
       cap03-turwepemt6.1-0 ; 
       cap03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-add_smoke.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_smoke-t2d55.1-0 ; 
       add_smoke-t2d57.1-0 ; 
       add_smoke-t2d58.1-0 ; 
       add_smoke-t2d59.1-0 ; 
       add_smoke-t2d60.1-0 ; 
       add_smoke-t2d61.1-0 ; 
       utann_carrier_sPTd-t2d10.1-0 ; 
       utann_carrier_sPTd-t2d11.1-0 ; 
       utann_carrier_sPTd-t2d17.11-0 ; 
       utann_carrier_sPTd-t2d19.11-0 ; 
       utann_carrier_sPTd-t2d20.11-0 ; 
       utann_carrier_sPTd-t2d25.1-0 ; 
       utann_carrier_sPTd-t2d26.1-0 ; 
       utann_carrier_sPTd-t2d29.1-0 ; 
       utann_carrier_sPTd-t2d31.1-0 ; 
       utann_carrier_sPTd-t2d32.1-0 ; 
       utann_carrier_sPTd-t2d33.1-0 ; 
       utann_carrier_sPTd-t2d34.1-0 ; 
       utann_carrier_sPTd-t2d39.11-0 ; 
       utann_carrier_sPTd-t2d4.1-0 ; 
       utann_carrier_sPTd-t2d41.1-0 ; 
       utann_carrier_sPTd-t2d42.1-0 ; 
       utann_carrier_sPTd-t2d43.1-0 ; 
       utann_carrier_sPTd-t2d44.1-0 ; 
       utann_carrier_sPTd-t2d45.1-0 ; 
       utann_carrier_sPTd-t2d46.1-0 ; 
       utann_carrier_sPTd-t2d49.11-0 ; 
       utann_carrier_sPTd-t2d51.1-0 ; 
       utann_carrier_sPTd-t2d52.1-0 ; 
       utann_carrier_sPTd-t2d7.1-0 ; 
       utann_carrier_sPTd-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 16 110 ; 
       2 1 110 ; 
       3 16 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 24 110 ; 
       7 14 110 ; 
       8 2 110 ; 
       9 10 110 ; 
       10 16 110 ; 
       11 24 110 ; 
       12 14 110 ; 
       13 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 55 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 19 110 ; 
       21 10 110 ; 
       22 19 110 ; 
       23 14 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 26 110 ; 
       28 16 110 ; 
       29 26 110 ; 
       31 25 110 ; 
       32 31 110 ; 
       33 25 110 ; 
       34 33 110 ; 
       35 10 110 ; 
       36 10 110 ; 
       37 18 110 ; 
       38 18 110 ; 
       39 38 110 ; 
       40 43 110 ; 
       41 38 110 ; 
       42 18 110 ; 
       43 18 110 ; 
       44 25 110 ; 
       45 44 110 ; 
       46 25 110 ; 
       47 25 110 ; 
       48 60 110 ; 
       49 60 110 ; 
       50 56 110 ; 
       51 57 110 ; 
       52 57 110 ; 
       53 59 110 ; 
       54 59 110 ; 
       55 3 110 ; 
       56 16 110 ; 
       57 16 110 ; 
       58 24 110 ; 
       59 16 110 ; 
       60 16 110 ; 
       61 14 110 ; 
       62 24 110 ; 
       63 14 110 ; 
       64 28 110 ; 
       65 21 110 ; 
       66 22 110 ; 
       67 20 110 ; 
       68 27 110 ; 
       69 29 110 ; 
       70 17 110 ; 
       30 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 33 300 ; 
       1 33 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       3 33 300 ; 
       3 37 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       10 32 300 ; 
       10 39 300 ; 
       10 42 300 ; 
       10 40 300 ; 
       10 41 300 ; 
       14 32 300 ; 
       16 31 300 ; 
       16 34 300 ; 
       16 45 300 ; 
       16 30 300 ; 
       16 29 300 ; 
       16 44 300 ; 
       17 3 300 ; 
       17 19 300 ; 
       17 0 300 ; 
       18 33 300 ; 
       20 32 300 ; 
       21 32 300 ; 
       22 32 300 ; 
       24 43 300 ; 
       25 33 300 ; 
       27 33 300 ; 
       28 32 300 ; 
       29 32 300 ; 
       31 25 300 ; 
       32 16 300 ; 
       33 21 300 ; 
       34 14 300 ; 
       35 8 300 ; 
       36 7 300 ; 
       37 27 300 ; 
       38 26 300 ; 
       39 18 300 ; 
       40 15 300 ; 
       41 13 300 ; 
       42 23 300 ; 
       43 22 300 ; 
       44 24 300 ; 
       45 17 300 ; 
       46 28 300 ; 
       47 20 300 ; 
       48 9 300 ; 
       49 10 300 ; 
       50 11 300 ; 
       51 4 300 ; 
       52 12 300 ; 
       53 6 300 ; 
       54 5 300 ; 
       55 33 300 ; 
       55 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 13 400 ; 
       3 14 400 ; 
       18 7 400 ; 
       20 23 400 ; 
       21 24 400 ; 
       22 25 400 ; 
       25 6 400 ; 
       27 20 400 ; 
       28 21 400 ; 
       29 22 400 ; 
       55 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       14 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 1 401 ; 
       2 0 401 ; 
       3 4 401 ; 
       19 3 401 ; 
       29 18 401 ; 
       30 10 401 ; 
       31 5 401 ; 
       34 8 401 ; 
       35 11 401 ; 
       36 12 401 ; 
       37 16 401 ; 
       38 17 401 ; 
       39 19 401 ; 
       40 27 401 ; 
       41 28 401 ; 
       42 29 401 ; 
       43 30 401 ; 
       44 26 401 ; 
       45 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 200 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 202.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 205 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 126.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 115 -6 0 MPRFLG 0 ; 
       7 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 MPRFLG 0 ; 
       10 SCHEM 60 -4 0 MPRFLG 0 ; 
       11 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 192.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       20 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       21 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       22 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 195 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 138.75 -4 0 MPRFLG 0 ; 
       27 SCHEM 141.25 -6 0 MPRFLG 0 ; 
       28 SCHEM 163.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 136.25 -6 0 MPRFLG 0 ; 
       31 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       32 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       33 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 82.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 90 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 80 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 87.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       43 SCHEM 91.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       44 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       45 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       46 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       47 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 157.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       49 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 145 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       51 SCHEM 150 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       52 SCHEM 147.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       53 SCHEM 155 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 152.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       55 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       56 SCHEM 145 -4 0 MPRFLG 0 ; 
       57 SCHEM 148.75 -4 0 MPRFLG 0 ; 
       58 SCHEM 110 -6 0 MPRFLG 0 ; 
       59 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       60 SCHEM 158.75 -4 0 MPRFLG 0 ; 
       61 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       63 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 197.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 167.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 167.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 199 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
