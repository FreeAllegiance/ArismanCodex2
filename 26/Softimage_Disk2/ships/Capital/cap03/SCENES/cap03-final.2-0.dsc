SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap03-cap03.73-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_carrier_sPTd-cam_int1.73-0 ROOT ; 
       utann_carrier_sPTd-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       final-back1.1-0 ; 
       final-mat54.1-0 ; 
       final-mat57.1-0 ; 
       final-mat58.1-0 ; 
       final-nose_white-center.1-11.1-0 ; 
       final-nose_white-center.1-12.1-0 ; 
       final-nose_white-center.1-15.1-0 ; 
       final-nose_white-center.1-31.1-0 ; 
       final-nose_white-center.1-32.1-0 ; 
       final-nose_white-center.1-35.1-0 ; 
       final-nose_white-center.1-38.1-0 ; 
       final-nose_white-center.1-41.1-0 ; 
       final-nose_white-center.1-7.1-0 ; 
       final-port_red-left.1-10.1-0 ; 
       final-port_red-left.1-18.1-0 ; 
       final-port_red-left.1-21.1-0 ; 
       final-port_red-left.1-4.1-0 ; 
       final-port_red-left.1-5.1-0 ; 
       final-port_red-left.1-8.1-0 ; 
       final-sides1.1-0 ; 
       final-starbord_green-right.1-1.1-0 ; 
       final-starbord_green-right.1-10.1-0 ; 
       final-starbord_green-right.1-11.1-0 ; 
       final-starbord_green-right.1-12.1-0 ; 
       final-starbord_green-right.1-3.1-0 ; 
       final-starbord_green-right.1-6.1-0 ; 
       final-starbord_green-right.1-7.1-0 ; 
       final-starbord_green-right.1-8.1-0 ; 
       final-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.1-0 ; 
       utann_carrier_sPTd-default1.1-0 ; 
       utann_carrier_sPTd-front_bottom.1-0 ; 
       utann_carrier_sPTd-mat10.2-0 ; 
       utann_carrier_sPTd-mat12.1-0 ; 
       utann_carrier_sPTd-mat22.2-0 ; 
       utann_carrier_sPTd-mat31.1-0 ; 
       utann_carrier_sPTd-mat32.1-0 ; 
       utann_carrier_sPTd-mat39.1-0 ; 
       utann_carrier_sPTd-mat40.1-0 ; 
       utann_carrier_sPTd-mat46.2-0 ; 
       utann_carrier_sPTd-mat5.1-0 ; 
       utann_carrier_sPTd-mat51.2-0 ; 
       utann_carrier_sPTd-mat52.1-0 ; 
       utann_carrier_sPTd-mat53.1-0 ; 
       utann_carrier_sPTd-mat8.1-0 ; 
       utann_carrier_sPTd-mat9.1-0 ; 
       utann_carrier_sPTd-rear_bottom.1-0 ; 
       utann_carrier_sPTd-side.1-0 ; 
       utann_carrier_sPTd-top.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 69     
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-blthrust.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-brthrust.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.54-0 ROOT ; 
       cap03-cockpt.1-0 ; 
       cap03-cyl1.4-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret1.1-0 ; 
       cap03-lturret2.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-missemt.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret1.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trail.1-0 ; 
       cap03-trlndpad.1-0 ; 
       cap03-tthrust.1-0 ; 
       cap03-turwepemt1.1-0 ; 
       cap03-turwepemt2.1-0 ; 
       cap03-turwepemt3.1-0 ; 
       cap03-turwepemt4.1-0 ; 
       cap03-turwepemt5.1-0 ; 
       cap03-turwepemt6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-final.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       final-t2d55.1-0 ; 
       final-t2d57.1-0 ; 
       final-t2d58.2-0 ; 
       final-t2d59.2-0 ; 
       final-t2d60.2-0 ; 
       utann_carrier_sPTd-t2d10.1-0 ; 
       utann_carrier_sPTd-t2d11.1-0 ; 
       utann_carrier_sPTd-t2d17.6-0 ; 
       utann_carrier_sPTd-t2d19.6-0 ; 
       utann_carrier_sPTd-t2d20.6-0 ; 
       utann_carrier_sPTd-t2d21.6-0 ; 
       utann_carrier_sPTd-t2d25.1-0 ; 
       utann_carrier_sPTd-t2d26.1-0 ; 
       utann_carrier_sPTd-t2d29.1-0 ; 
       utann_carrier_sPTd-t2d31.1-0 ; 
       utann_carrier_sPTd-t2d32.1-0 ; 
       utann_carrier_sPTd-t2d33.1-0 ; 
       utann_carrier_sPTd-t2d34.1-0 ; 
       utann_carrier_sPTd-t2d39.6-0 ; 
       utann_carrier_sPTd-t2d4.1-0 ; 
       utann_carrier_sPTd-t2d41.1-0 ; 
       utann_carrier_sPTd-t2d42.1-0 ; 
       utann_carrier_sPTd-t2d43.1-0 ; 
       utann_carrier_sPTd-t2d44.1-0 ; 
       utann_carrier_sPTd-t2d45.1-0 ; 
       utann_carrier_sPTd-t2d46.1-0 ; 
       utann_carrier_sPTd-t2d49.6-0 ; 
       utann_carrier_sPTd-t2d51.1-0 ; 
       utann_carrier_sPTd-t2d52.1-0 ; 
       utann_carrier_sPTd-t2d7.1-0 ; 
       utann_carrier_sPTd-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 17 110 ; 
       2 1 110 ; 
       3 17 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 24 110 ; 
       7 14 110 ; 
       8 2 110 ; 
       9 10 110 ; 
       10 17 110 ; 
       11 24 110 ; 
       12 14 110 ; 
       13 10 110 ; 
       15 14 110 ; 
       16 54 110 ; 
       17 14 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 10 110 ; 
       22 19 110 ; 
       24 17 110 ; 
       25 17 110 ; 
       26 17 110 ; 
       27 26 110 ; 
       28 17 110 ; 
       29 26 110 ; 
       30 25 110 ; 
       31 30 110 ; 
       32 25 110 ; 
       33 32 110 ; 
       34 10 110 ; 
       35 10 110 ; 
       36 18 110 ; 
       37 18 110 ; 
       38 37 110 ; 
       39 42 110 ; 
       40 37 110 ; 
       41 18 110 ; 
       42 18 110 ; 
       43 25 110 ; 
       44 43 110 ; 
       45 25 110 ; 
       46 25 110 ; 
       47 59 110 ; 
       48 59 110 ; 
       49 55 110 ; 
       50 56 110 ; 
       51 56 110 ; 
       52 58 110 ; 
       53 58 110 ; 
       54 3 110 ; 
       55 17 110 ; 
       56 17 110 ; 
       57 24 110 ; 
       58 17 110 ; 
       59 17 110 ; 
       60 14 110 ; 
       61 24 110 ; 
       62 14 110 ; 
       63 28 110 ; 
       64 21 110 ; 
       65 22 110 ; 
       66 20 110 ; 
       67 27 110 ; 
       68 29 110 ; 
       23 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 33 300 ; 
       1 33 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       3 33 300 ; 
       3 37 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       10 30 300 ; 
       10 40 300 ; 
       10 44 300 ; 
       10 42 300 ; 
       10 43 300 ; 
       14 30 300 ; 
       16 3 300 ; 
       16 19 300 ; 
       16 0 300 ; 
       17 32 300 ; 
       17 34 300 ; 
       17 48 300 ; 
       17 31 300 ; 
       17 46 300 ; 
       17 29 300 ; 
       17 39 300 ; 
       17 47 300 ; 
       17 41 300 ; 
       18 33 300 ; 
       20 30 300 ; 
       21 30 300 ; 
       22 30 300 ; 
       24 45 300 ; 
       25 33 300 ; 
       27 33 300 ; 
       28 30 300 ; 
       29 30 300 ; 
       30 25 300 ; 
       31 16 300 ; 
       32 21 300 ; 
       33 14 300 ; 
       34 8 300 ; 
       35 7 300 ; 
       36 27 300 ; 
       37 26 300 ; 
       38 18 300 ; 
       39 15 300 ; 
       40 13 300 ; 
       41 23 300 ; 
       42 22 300 ; 
       43 24 300 ; 
       44 17 300 ; 
       45 28 300 ; 
       46 20 300 ; 
       47 9 300 ; 
       48 10 300 ; 
       49 11 300 ; 
       50 4 300 ; 
       51 12 300 ; 
       52 6 300 ; 
       53 5 300 ; 
       54 33 300 ; 
       54 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 13 400 ; 
       3 14 400 ; 
       18 6 400 ; 
       20 23 400 ; 
       21 24 400 ; 
       22 25 400 ; 
       25 5 400 ; 
       27 20 400 ; 
       28 21 400 ; 
       29 22 400 ; 
       54 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       14 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 1 401 ; 
       2 0 401 ; 
       3 4 401 ; 
       19 3 401 ; 
       29 18 401 ; 
       31 9 401 ; 
       34 7 401 ; 
       35 11 401 ; 
       36 12 401 ; 
       37 16 401 ; 
       38 17 401 ; 
       40 19 401 ; 
       42 27 401 ; 
       43 28 401 ; 
       44 29 401 ; 
       45 30 401 ; 
       46 10 401 ; 
       47 26 401 ; 
       48 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 60 -6 0 MPRFLG 0 ; 
       7 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 MPRFLG 0 ; 
       14 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 45 -2 0 MPRFLG 0 ; 
       18 SCHEM 40 -4 0 MPRFLG 0 ; 
       19 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 MPRFLG 0 ; 
       21 SCHEM 30 -6 0 MPRFLG 0 ; 
       22 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       27 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 65 -6 0 MPRFLG 0 ; 
       30 SCHEM 10 -6 0 MPRFLG 0 ; 
       31 SCHEM 10 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 20 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 35 -6 0 MPRFLG 0 ; 
       37 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       38 SCHEM 40 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 45 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 37.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       42 SCHEM 45 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       43 SCHEM 5 -6 0 MPRFLG 0 ; 
       44 SCHEM 5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       45 SCHEM 7.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       46 SCHEM 12.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 82.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       48 SCHEM 85 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 70 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       50 SCHEM 75 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       51 SCHEM 72.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       52 SCHEM 80 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 77.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       54 SCHEM 15 -6 0 MPRFLG 0 ; 
       55 SCHEM 70 -4 0 MPRFLG 0 ; 
       56 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       57 SCHEM 55 -6 0 MPRFLG 0 ; 
       58 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       59 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       60 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       62 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 89 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 64 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 89 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 104 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
