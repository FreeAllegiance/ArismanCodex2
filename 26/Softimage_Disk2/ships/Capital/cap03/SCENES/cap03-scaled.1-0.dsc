SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       scaled-light1.1-0 ROOT ; 
       scaled-light2.1-0 ROOT ; 
       scaled-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       scaled-back1.1-0 ; 
       scaled-default2.1-0 ; 
       scaled-mat54.1-0 ; 
       scaled-mat57.1-0 ; 
       scaled-mat58.1-0 ; 
       scaled-nose_white-center.1-11.1-0 ; 
       scaled-nose_white-center.1-12.1-0 ; 
       scaled-nose_white-center.1-15.1-0 ; 
       scaled-nose_white-center.1-31.1-0 ; 
       scaled-nose_white-center.1-32.1-0 ; 
       scaled-nose_white-center.1-35.1-0 ; 
       scaled-nose_white-center.1-38.1-0 ; 
       scaled-nose_white-center.1-41.1-0 ; 
       scaled-nose_white-center.1-7.1-0 ; 
       scaled-port_red-left.1-10.1-0 ; 
       scaled-port_red-left.1-18.1-0 ; 
       scaled-port_red-left.1-21.1-0 ; 
       scaled-port_red-left.1-4.1-0 ; 
       scaled-port_red-left.1-5.1-0 ; 
       scaled-port_red-left.1-8.1-0 ; 
       scaled-sides1.1-0 ; 
       scaled-starbord_green-right.1-1.1-0 ; 
       scaled-starbord_green-right.1-10.1-0 ; 
       scaled-starbord_green-right.1-11.1-0 ; 
       scaled-starbord_green-right.1-12.1-0 ; 
       scaled-starbord_green-right.1-3.1-0 ; 
       scaled-starbord_green-right.1-6.1-0 ; 
       scaled-starbord_green-right.1-7.1-0 ; 
       scaled-starbord_green-right.1-8.1-0 ; 
       scaled-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.6-0 ; 
       utann_carrier_sPTd-bottom.4-0 ; 
       utann_carrier_sPTd-default.5-0 ; 
       utann_carrier_sPTd-default1.4-0 ; 
       utann_carrier_sPTd-mat12.4-0 ; 
       utann_carrier_sPTd-mat22.7-0 ; 
       utann_carrier_sPTd-mat31.4-0 ; 
       utann_carrier_sPTd-mat32.4-0 ; 
       utann_carrier_sPTd-mat39.4-0 ; 
       utann_carrier_sPTd-mat40.4-0 ; 
       utann_carrier_sPTd-mat5.4-0 ; 
       utann_carrier_sPTd-mat52.4-0 ; 
       utann_carrier_sPTd-mat53.4-0 ; 
       utann_carrier_sPTd-mat8.4-0 ; 
       utann_carrier_sPTd-mat9.4-0 ; 
       utann_carrier_sPTd-side.6-0 ; 
       utann_carrier_sPTd-top.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 72     
       cap03-170deg.1-0 ROOT ; 
       cap03-175deg.1-0 ROOT ; 
       cap03-175deg_1.1-0 ROOT ; 
       cap03-175deg1.1-0 ROOT ; 
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-blthrust.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-brthrust.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.76-0 ROOT ; 
       cap03-cockpt.1-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-missemt.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-rturret4.1-0 ; 
       cap03-smoke.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trail.1-0 ; 
       cap03-trlndpad.1-0 ; 
       cap03-tthrust.1-0 ; 
       cap03-turwepemt1.1-0 ; 
       cap03-turwepemt2.1-0 ; 
       cap03-turwepemt3.1-0 ; 
       cap03-turwepemt4.1-0 ; 
       cap03-wepemt.1-0 ; 
       scaled-cube1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       scaled-t2d55.1-0 ; 
       scaled-t2d57.1-0 ; 
       scaled-t2d58.1-0 ; 
       scaled-t2d59.1-0 ; 
       scaled-t2d60.1-0 ; 
       scaled-t2d61.1-0 ; 
       scaled-t2d62.1-0 ; 
       utann_carrier_sPTd-t2d10.5-0 ; 
       utann_carrier_sPTd-t2d11.5-0 ; 
       utann_carrier_sPTd-t2d17.15-0 ; 
       utann_carrier_sPTd-t2d19.15-0 ; 
       utann_carrier_sPTd-t2d20.15-0 ; 
       utann_carrier_sPTd-t2d25.5-0 ; 
       utann_carrier_sPTd-t2d26.5-0 ; 
       utann_carrier_sPTd-t2d29.5-0 ; 
       utann_carrier_sPTd-t2d31.5-0 ; 
       utann_carrier_sPTd-t2d32.5-0 ; 
       utann_carrier_sPTd-t2d33.5-0 ; 
       utann_carrier_sPTd-t2d34.5-0 ; 
       utann_carrier_sPTd-t2d39.15-0 ; 
       utann_carrier_sPTd-t2d4.5-0 ; 
       utann_carrier_sPTd-t2d42.5-0 ; 
       utann_carrier_sPTd-t2d43.5-0 ; 
       utann_carrier_sPTd-t2d46.5-0 ; 
       utann_carrier_sPTd-t2d49.15-0 ; 
       utann_carrier_sPTd-t2d51.5-0 ; 
       utann_carrier_sPTd-t2d52.5-0 ; 
       utann_carrier_sPTd-t2d7.5-0 ; 
       utann_carrier_sPTd-t2d8.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 12 110 ; 
       5 20 110 ; 
       6 5 110 ; 
       7 20 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 26 110 ; 
       11 18 110 ; 
       12 6 110 ; 
       13 14 110 ; 
       14 20 110 ; 
       15 26 110 ; 
       16 18 110 ; 
       17 14 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 57 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 23 110 ; 
       25 18 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 28 110 ; 
       32 18 110 ; 
       33 27 110 ; 
       34 33 110 ; 
       35 27 110 ; 
       36 35 110 ; 
       37 14 110 ; 
       38 14 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 40 110 ; 
       42 45 110 ; 
       43 40 110 ; 
       44 22 110 ; 
       45 22 110 ; 
       46 27 110 ; 
       47 46 110 ; 
       31 28 110 ; 
       48 27 110 ; 
       49 27 110 ; 
       50 62 110 ; 
       51 62 110 ; 
       52 58 110 ; 
       53 59 110 ; 
       54 59 110 ; 
       55 61 110 ; 
       56 61 110 ; 
       57 7 110 ; 
       58 20 110 ; 
       59 20 110 ; 
       60 26 110 ; 
       61 20 110 ; 
       62 20 110 ; 
       63 18 110 ; 
       64 26 110 ; 
       65 18 110 ; 
       66 29 110 ; 
       66 2 114 ; 
       67 24 110 ; 
       68 30 110 ; 
       70 21 110 ; 
       69 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 34 300 ; 
       5 34 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       7 34 300 ; 
       7 38 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       14 33 300 ; 
       14 40 300 ; 
       14 43 300 ; 
       14 41 300 ; 
       14 42 300 ; 
       18 33 300 ; 
       20 32 300 ; 
       20 35 300 ; 
       20 46 300 ; 
       20 31 300 ; 
       20 30 300 ; 
       20 45 300 ; 
       21 4 300 ; 
       21 20 300 ; 
       21 0 300 ; 
       22 34 300 ; 
       24 33 300 ; 
       26 44 300 ; 
       27 34 300 ; 
       29 33 300 ; 
       30 33 300 ; 
       33 26 300 ; 
       34 17 300 ; 
       35 22 300 ; 
       36 15 300 ; 
       37 9 300 ; 
       38 8 300 ; 
       39 28 300 ; 
       40 27 300 ; 
       41 19 300 ; 
       42 16 300 ; 
       43 14 300 ; 
       44 24 300 ; 
       45 23 300 ; 
       46 25 300 ; 
       47 18 300 ; 
       31 1 300 ; 
       48 29 300 ; 
       49 21 300 ; 
       50 10 300 ; 
       51 11 300 ; 
       52 12 300 ; 
       53 5 300 ; 
       54 13 300 ; 
       55 7 300 ; 
       56 6 300 ; 
       57 34 300 ; 
       57 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 14 400 ; 
       7 15 400 ; 
       22 8 400 ; 
       24 23 400 ; 
       27 7 400 ; 
       29 21 400 ; 
       30 22 400 ; 
       31 6 400 ; 
       57 16 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 4 401 ; 
       20 3 401 ; 
       30 19 401 ; 
       31 11 401 ; 
       32 5 401 ; 
       35 9 401 ; 
       36 12 401 ; 
       37 13 401 ; 
       38 17 401 ; 
       39 18 401 ; 
       40 20 401 ; 
       41 25 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 24 401 ; 
       46 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 200 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 202.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 205 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       71 SCHEM 105.9713 5.672479 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 126.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 115 -6 0 MPRFLG 0 ; 
       11 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 50 -6 0 MPRFLG 0 ; 
       14 SCHEM 60 -4 0 MPRFLG 0 ; 
       15 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 192.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 -0.9983624 MPRFLG 0 ; 
       19 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       21 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 99.70628 -23.85052 0 USR MPRFLG 0 ; 
       25 SCHEM 195 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 138.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 95.8543 -24.40059 0 USR MPRFLG 0 ; 
       30 SCHEM 137.2063 -23.85052 0 USR MPRFLG 0 ; 
       32 SCHEM 197.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       34 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 90 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 80 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       45 SCHEM 91.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       46 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       47 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       31 SCHEM 158.7702 -21.87208 0 USR MPRFLG 0 ; 
       48 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       49 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 157.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       51 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 145 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       53 SCHEM 150 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       54 SCHEM 147.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       55 SCHEM 155 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 152.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       57 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       58 SCHEM 145 -4 0 MPRFLG 0 ; 
       59 SCHEM 148.75 -4 0 MPRFLG 0 ; 
       60 SCHEM 110 -6 0 MPRFLG 0 ; 
       61 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       62 SCHEM 158.75 -4 0 MPRFLG 0 ; 
       63 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       65 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 95.9224 -27.10693 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 99.61472 -27.19574 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 137.2744 -26.39713 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 159.5191 -28.0645 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 11.16625 11.16625 11.16625 -2.879999 0 0 0 -10.56425 -31.44325 MPRFLG 0 ; 
       2 SCHEM 96.64736 -30.61032 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 11.96518 11.96518 11.96518 0.108 0 0 0 8.383189 8.178627 MPRFLG 0 ; 
       1 SCHEM 99.95956 -30.96959 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 11.96518 11.96518 11.96518 2.962482 -0.06373696 1.232522 5.97391 -4.282989 32.84564 MPRFLG 0 ; 
       3 SCHEM 137.7099 -33.03534 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 11.96518 11.96518 11.96518 0.1791109 -0.06373696 1.90907 -5.97391 -4.282989 32.84564 MPRFLG 0 ; 
       69 SCHEM 158.7591 -23.48647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 167.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 186.6457 40.7403 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 167.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 276.6457 40.7403 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
