SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       scaled-back1.1-0 ; 
       scaled-default2.1-0 ; 
       scaled-mat54.1-0 ; 
       scaled-mat57.1-0 ; 
       scaled-mat58.1-0 ; 
       scaled-nose_white-center.1-11.1-0 ; 
       scaled-nose_white-center.1-12.1-0 ; 
       scaled-nose_white-center.1-15.1-0 ; 
       scaled-nose_white-center.1-31.1-0 ; 
       scaled-nose_white-center.1-32.1-0 ; 
       scaled-nose_white-center.1-35.1-0 ; 
       scaled-nose_white-center.1-38.1-0 ; 
       scaled-nose_white-center.1-41.1-0 ; 
       scaled-nose_white-center.1-7.1-0 ; 
       scaled-port_red-left.1-10.1-0 ; 
       scaled-port_red-left.1-18.1-0 ; 
       scaled-port_red-left.1-21.1-0 ; 
       scaled-port_red-left.1-4.1-0 ; 
       scaled-port_red-left.1-5.1-0 ; 
       scaled-port_red-left.1-8.1-0 ; 
       scaled-sides1.1-0 ; 
       scaled-starbord_green-right.1-1.1-0 ; 
       scaled-starbord_green-right.1-10.1-0 ; 
       scaled-starbord_green-right.1-11.1-0 ; 
       scaled-starbord_green-right.1-12.1-0 ; 
       scaled-starbord_green-right.1-3.1-0 ; 
       scaled-starbord_green-right.1-6.1-0 ; 
       scaled-starbord_green-right.1-7.1-0 ; 
       scaled-starbord_green-right.1-8.1-0 ; 
       scaled-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.6-0 ; 
       utann_carrier_sPTd-bottom.4-0 ; 
       utann_carrier_sPTd-default.5-0 ; 
       utann_carrier_sPTd-default1.4-0 ; 
       utann_carrier_sPTd-mat12.4-0 ; 
       utann_carrier_sPTd-mat22.7-0 ; 
       utann_carrier_sPTd-mat31.4-0 ; 
       utann_carrier_sPTd-mat32.4-0 ; 
       utann_carrier_sPTd-mat39.4-0 ; 
       utann_carrier_sPTd-mat40.4-0 ; 
       utann_carrier_sPTd-mat5.4-0 ; 
       utann_carrier_sPTd-mat52.4-0 ; 
       utann_carrier_sPTd-mat53.4-0 ; 
       utann_carrier_sPTd-mat8.4-0 ; 
       utann_carrier_sPTd-mat9.4-0 ; 
       utann_carrier_sPTd-side.6-0 ; 
       utann_carrier_sPTd-top.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 67     
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-blthrust.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-brthrust.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.79-0 ROOT ; 
       cap03-cockpt.1-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-missemt.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-rturret4.1-0 ; 
       cap03-smoke.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trail.1-0 ; 
       cap03-trlndpad.1-0 ; 
       cap03-tthrust.1-0 ; 
       cap03-turwepemt1.1-0 ; 
       cap03-turwepemt2.1-0 ; 
       cap03-turwepemt3.1-0 ; 
       cap03-turwepemt4.1-0 ; 
       cap03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-scaled.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       scaled-t2d55.2-0 ; 
       scaled-t2d57.2-0 ; 
       scaled-t2d58.2-0 ; 
       scaled-t2d59.2-0 ; 
       scaled-t2d60.2-0 ; 
       scaled-t2d61.2-0 ; 
       scaled-t2d62.2-0 ; 
       utann_carrier_sPTd-t2d10.6-0 ; 
       utann_carrier_sPTd-t2d11.6-0 ; 
       utann_carrier_sPTd-t2d17.16-0 ; 
       utann_carrier_sPTd-t2d19.16-0 ; 
       utann_carrier_sPTd-t2d20.16-0 ; 
       utann_carrier_sPTd-t2d25.6-0 ; 
       utann_carrier_sPTd-t2d26.6-0 ; 
       utann_carrier_sPTd-t2d29.6-0 ; 
       utann_carrier_sPTd-t2d31.6-0 ; 
       utann_carrier_sPTd-t2d32.6-0 ; 
       utann_carrier_sPTd-t2d33.6-0 ; 
       utann_carrier_sPTd-t2d34.6-0 ; 
       utann_carrier_sPTd-t2d39.16-0 ; 
       utann_carrier_sPTd-t2d4.6-0 ; 
       utann_carrier_sPTd-t2d42.6-0 ; 
       utann_carrier_sPTd-t2d43.6-0 ; 
       utann_carrier_sPTd-t2d46.6-0 ; 
       utann_carrier_sPTd-t2d49.16-0 ; 
       utann_carrier_sPTd-t2d51.6-0 ; 
       utann_carrier_sPTd-t2d52.6-0 ; 
       utann_carrier_sPTd-t2d7.6-0 ; 
       utann_carrier_sPTd-t2d8.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 16 110 ; 
       2 1 110 ; 
       3 16 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 22 110 ; 
       7 14 110 ; 
       8 2 110 ; 
       9 10 110 ; 
       10 16 110 ; 
       11 22 110 ; 
       12 14 110 ; 
       13 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 53 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 19 110 ; 
       21 14 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 14 110 ; 
       29 23 110 ; 
       30 29 110 ; 
       31 23 110 ; 
       32 31 110 ; 
       33 10 110 ; 
       34 10 110 ; 
       35 18 110 ; 
       36 18 110 ; 
       37 36 110 ; 
       38 41 110 ; 
       39 36 110 ; 
       40 18 110 ; 
       41 18 110 ; 
       42 23 110 ; 
       43 42 110 ; 
       44 23 110 ; 
       45 23 110 ; 
       46 58 110 ; 
       47 58 110 ; 
       48 54 110 ; 
       49 55 110 ; 
       50 55 110 ; 
       51 57 110 ; 
       52 57 110 ; 
       53 3 110 ; 
       54 16 110 ; 
       55 16 110 ; 
       56 22 110 ; 
       57 16 110 ; 
       58 16 110 ; 
       59 14 110 ; 
       60 22 110 ; 
       61 14 110 ; 
       62 25 110 ; 
       63 20 110 ; 
       64 26 110 ; 
       65 27 110 ; 
       66 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 34 300 ; 
       1 34 300 ; 
       1 36 300 ; 
       1 37 300 ; 
       3 34 300 ; 
       3 38 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       10 33 300 ; 
       10 40 300 ; 
       10 43 300 ; 
       10 41 300 ; 
       10 42 300 ; 
       14 33 300 ; 
       16 32 300 ; 
       16 35 300 ; 
       16 46 300 ; 
       16 31 300 ; 
       16 30 300 ; 
       16 45 300 ; 
       17 4 300 ; 
       17 20 300 ; 
       17 0 300 ; 
       18 34 300 ; 
       20 33 300 ; 
       22 44 300 ; 
       23 34 300 ; 
       25 33 300 ; 
       26 33 300 ; 
       27 1 300 ; 
       29 26 300 ; 
       30 17 300 ; 
       31 22 300 ; 
       32 15 300 ; 
       33 9 300 ; 
       34 8 300 ; 
       35 28 300 ; 
       36 27 300 ; 
       37 19 300 ; 
       38 16 300 ; 
       39 14 300 ; 
       40 24 300 ; 
       41 23 300 ; 
       42 25 300 ; 
       43 18 300 ; 
       44 29 300 ; 
       45 21 300 ; 
       46 10 300 ; 
       47 11 300 ; 
       48 12 300 ; 
       49 5 300 ; 
       50 13 300 ; 
       51 7 300 ; 
       52 6 300 ; 
       53 34 300 ; 
       53 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 14 400 ; 
       3 15 400 ; 
       18 8 400 ; 
       20 23 400 ; 
       23 7 400 ; 
       25 21 400 ; 
       26 22 400 ; 
       27 6 400 ; 
       53 16 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 4 401 ; 
       20 3 401 ; 
       30 19 401 ; 
       31 11 401 ; 
       32 5 401 ; 
       35 9 401 ; 
       36 12 401 ; 
       37 13 401 ; 
       38 17 401 ; 
       39 18 401 ; 
       40 20 401 ; 
       41 25 401 ; 
       42 26 401 ; 
       43 27 401 ; 
       44 28 401 ; 
       45 24 401 ; 
       46 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 116.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 121.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 118.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 110 -6 0 MPRFLG 0 ; 
       7 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 96.25 0 0 SRT 1 1 1 0 0 0 0 0 -0.08925438 MPRFLG 0 ; 
       15 SCHEM 177.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 81.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       21 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 135 -4 0 MPRFLG 0 ; 
       25 SCHEM 93.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       27 SCHEM 137.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       30 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       31 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       33 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 77.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 85 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 75 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       41 SCHEM 86.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       42 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       43 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       45 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 155 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       47 SCHEM 157.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 142.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       49 SCHEM 147.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       50 SCHEM 145 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       51 SCHEM 152.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 150 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       53 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       54 SCHEM 142.5 -4 0 MPRFLG 0 ; 
       55 SCHEM 146.25 -4 0 MPRFLG 0 ; 
       56 SCHEM 105 -6 0 MPRFLG 0 ; 
       57 SCHEM 151.25 -4 0 MPRFLG 0 ; 
       58 SCHEM 156.25 -4 0 MPRFLG 0 ; 
       59 SCHEM 175 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       61 SCHEM 180 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 160 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 160 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
