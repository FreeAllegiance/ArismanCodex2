SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.7-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       STATIC-back1.1-0 ; 
       STATIC-default2.1-0 ; 
       STATIC-mat54.1-0 ; 
       STATIC-mat57.1-0 ; 
       STATIC-mat58.1-0 ; 
       STATIC-sides1.1-0 ; 
       STATIC-starbord_green-right.1-10.1-0 ; 
       STATIC-starbord_green-right.1-11.1-0 ; 
       STATIC-starbord_green-right.1-12.1-0 ; 
       STATIC-starbord_green-right.1-3.1-0 ; 
       STATIC-starbord_green-right.1-6.1-0 ; 
       STATIC-starbord_green-right.1-7.1-0 ; 
       STATIC-starbord_green-right.1-8.1-0 ; 
       STATIC-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.6-0 ; 
       utann_carrier_sPTd-bottom.4-0 ; 
       utann_carrier_sPTd-default.5-0 ; 
       utann_carrier_sPTd-default1.4-0 ; 
       utann_carrier_sPTd-mat12.4-0 ; 
       utann_carrier_sPTd-mat22.7-0 ; 
       utann_carrier_sPTd-mat31.4-0 ; 
       utann_carrier_sPTd-mat32.4-0 ; 
       utann_carrier_sPTd-mat39.4-0 ; 
       utann_carrier_sPTd-mat40.4-0 ; 
       utann_carrier_sPTd-mat5.4-0 ; 
       utann_carrier_sPTd-mat52.4-0 ; 
       utann_carrier_sPTd-mat53.4-0 ; 
       utann_carrier_sPTd-mat8.4-0 ; 
       utann_carrier_sPTd-mat9.4-0 ; 
       utann_carrier_sPTd-side.6-0 ; 
       utann_carrier_sPTd-top.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.80-0 ROOT ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-rturret4.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trlndpad.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       STATIC-t2d55.1-0 ; 
       STATIC-t2d57.1-0 ; 
       STATIC-t2d58.1-0 ; 
       STATIC-t2d59.1-0 ; 
       STATIC-t2d60.1-0 ; 
       STATIC-t2d61.1-0 ; 
       STATIC-t2d62.1-0 ; 
       utann_carrier_sPTd-t2d10.6-0 ; 
       utann_carrier_sPTd-t2d11.6-0 ; 
       utann_carrier_sPTd-t2d17.16-0 ; 
       utann_carrier_sPTd-t2d19.16-0 ; 
       utann_carrier_sPTd-t2d20.16-0 ; 
       utann_carrier_sPTd-t2d25.6-0 ; 
       utann_carrier_sPTd-t2d26.6-0 ; 
       utann_carrier_sPTd-t2d29.6-0 ; 
       utann_carrier_sPTd-t2d31.6-0 ; 
       utann_carrier_sPTd-t2d32.6-0 ; 
       utann_carrier_sPTd-t2d33.6-0 ; 
       utann_carrier_sPTd-t2d34.6-0 ; 
       utann_carrier_sPTd-t2d39.16-0 ; 
       utann_carrier_sPTd-t2d4.6-0 ; 
       utann_carrier_sPTd-t2d42.6-0 ; 
       utann_carrier_sPTd-t2d43.6-0 ; 
       utann_carrier_sPTd-t2d46.6-0 ; 
       utann_carrier_sPTd-t2d49.16-0 ; 
       utann_carrier_sPTd-t2d51.6-0 ; 
       utann_carrier_sPTd-t2d52.6-0 ; 
       utann_carrier_sPTd-t2d7.6-0 ; 
       utann_carrier_sPTd-t2d8.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 13 110 ; 
       2 1 110 ; 
       3 13 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 18 110 ; 
       7 2 110 ; 
       8 9 110 ; 
       9 13 110 ; 
       10 18 110 ; 
       11 9 110 ; 
       13 12 110 ; 
       14 32 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 16 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 13 110 ; 
       21 13 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 15 110 ; 
       27 15 110 ; 
       28 15 110 ; 
       29 15 110 ; 
       30 19 110 ; 
       31 19 110 ; 
       32 3 110 ; 
       33 13 110 ; 
       34 13 110 ; 
       35 18 110 ; 
       36 13 110 ; 
       37 13 110 ; 
       38 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       1 18 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       3 18 300 ; 
       3 22 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       9 17 300 ; 
       9 24 300 ; 
       9 27 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       12 17 300 ; 
       13 16 300 ; 
       13 19 300 ; 
       13 30 300 ; 
       13 15 300 ; 
       13 14 300 ; 
       13 29 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       14 0 300 ; 
       15 18 300 ; 
       17 17 300 ; 
       18 28 300 ; 
       19 18 300 ; 
       21 17 300 ; 
       22 17 300 ; 
       23 1 300 ; 
       24 10 300 ; 
       25 6 300 ; 
       26 12 300 ; 
       27 11 300 ; 
       28 8 300 ; 
       29 7 300 ; 
       30 9 300 ; 
       31 13 300 ; 
       32 18 300 ; 
       32 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 14 400 ; 
       3 15 400 ; 
       15 8 400 ; 
       17 23 400 ; 
       19 7 400 ; 
       21 21 400 ; 
       22 22 400 ; 
       23 6 400 ; 
       32 16 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 4 401 ; 
       5 3 401 ; 
       14 19 401 ; 
       15 11 401 ; 
       16 5 401 ; 
       19 9 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 17 401 ; 
       23 18 401 ; 
       24 20 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 24 401 ; 
       30 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 116.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 121.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 118.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 110 -6 0 MPRFLG 0 ; 
       7 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 50 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 96.25 0 0 SRT 1 1 1 0 0 0 0 0 -0.08925438 MPRFLG 0 ; 
       13 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 81.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 135 -4 0 MPRFLG 0 ; 
       21 SCHEM 93.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 137.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 82.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       29 SCHEM 86.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       30 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       32 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       33 SCHEM 142.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 146.25 -4 0 MPRFLG 0 ; 
       35 SCHEM 105 -6 0 MPRFLG 0 ; 
       36 SCHEM 151.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 156.25 -4 0 MPRFLG 0 ; 
       38 SCHEM 107.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 160 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 160 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
