SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap03-cap03.95-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_carrier_sPTd-cam_int1.97-0 ROOT ; 
       utann_carrier_sPTd-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       static-back1.1-0 ; 
       static-mat54.1-0 ; 
       static-mat57.1-0 ; 
       static-mat58.1-0 ; 
       static-port_red-left.1-18.1-0 ; 
       static-port_red-left.1-4.1-0 ; 
       static-port_red-left.1-5.1-0 ; 
       static-sides1.1-0 ; 
       static-starbord_green-right.1-1.1-0 ; 
       static-starbord_green-right.1-10.1-0 ; 
       static-starbord_green-right.1-3.1-0 ; 
       static-starbord_green-right.1-6.1-0 ; 
       static-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.3-0 ; 
       utann_carrier_sPTd-bottom.1-0 ; 
       utann_carrier_sPTd-default.2-0 ; 
       utann_carrier_sPTd-default1.1-0 ; 
       utann_carrier_sPTd-mat12.1-0 ; 
       utann_carrier_sPTd-mat22.4-0 ; 
       utann_carrier_sPTd-mat31.1-0 ; 
       utann_carrier_sPTd-mat32.1-0 ; 
       utann_carrier_sPTd-mat39.1-0 ; 
       utann_carrier_sPTd-mat40.1-0 ; 
       utann_carrier_sPTd-mat5.1-0 ; 
       utann_carrier_sPTd-mat52.1-0 ; 
       utann_carrier_sPTd-mat53.1-0 ; 
       utann_carrier_sPTd-mat8.1-0 ; 
       utann_carrier_sPTd-mat9.1-0 ; 
       utann_carrier_sPTd-side.3-0 ; 
       utann_carrier_sPTd-top.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-cap03.70-0 ROOT ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       static-t2d55.1-0 ; 
       static-t2d57.1-0 ; 
       static-t2d58.1-0 ; 
       static-t2d59.1-0 ; 
       static-t2d60.1-0 ; 
       static-t2d61.1-0 ; 
       utann_carrier_sPTd-t2d10.1-0 ; 
       utann_carrier_sPTd-t2d11.1-0 ; 
       utann_carrier_sPTd-t2d17.11-0 ; 
       utann_carrier_sPTd-t2d19.11-0 ; 
       utann_carrier_sPTd-t2d20.11-0 ; 
       utann_carrier_sPTd-t2d25.1-0 ; 
       utann_carrier_sPTd-t2d26.1-0 ; 
       utann_carrier_sPTd-t2d31.1-0 ; 
       utann_carrier_sPTd-t2d32.1-0 ; 
       utann_carrier_sPTd-t2d33.1-0 ; 
       utann_carrier_sPTd-t2d34.1-0 ; 
       utann_carrier_sPTd-t2d39.11-0 ; 
       utann_carrier_sPTd-t2d4.1-0 ; 
       utann_carrier_sPTd-t2d49.11-0 ; 
       utann_carrier_sPTd-t2d51.1-0 ; 
       utann_carrier_sPTd-t2d52.1-0 ; 
       utann_carrier_sPTd-t2d7.1-0 ; 
       utann_carrier_sPTd-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 1 110 ; 
       4 6 110 ; 
       6 5 110 ; 
       7 19 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       16 15 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 2 110 ; 
       20 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 19 300 ; 
       0 20 300 ; 
       2 17 300 ; 
       2 21 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 16 300 ; 
       4 23 300 ; 
       4 26 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       5 16 300 ; 
       6 15 300 ; 
       6 18 300 ; 
       6 29 300 ; 
       6 14 300 ; 
       6 13 300 ; 
       6 28 300 ; 
       7 3 300 ; 
       7 7 300 ; 
       7 0 300 ; 
       8 17 300 ; 
       9 27 300 ; 
       10 17 300 ; 
       11 11 300 ; 
       12 5 300 ; 
       13 9 300 ; 
       14 4 300 ; 
       15 10 300 ; 
       16 6 300 ; 
       17 12 300 ; 
       18 8 300 ; 
       19 17 300 ; 
       19 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 13 400 ; 
       8 7 400 ; 
       10 6 400 ; 
       19 14 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 1 401 ; 
       2 0 401 ; 
       3 4 401 ; 
       7 3 401 ; 
       13 17 401 ; 
       14 10 401 ; 
       15 5 401 ; 
       18 8 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 18 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 19 401 ; 
       29 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 -4 0 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 50 -8 0 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 MPRFLG 0 ; 
       5 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 40 -4 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 MPRFLG 0 ; 
       16 SCHEM 5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 MPRFLG 0 ; 
       20 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 51.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
