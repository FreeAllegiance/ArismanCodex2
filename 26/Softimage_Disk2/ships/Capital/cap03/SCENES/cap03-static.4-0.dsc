SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       static-back1.2-0 ; 
       static-mat54.2-0 ; 
       static-mat57.2-0 ; 
       static-mat58.2-0 ; 
       static-sides1.2-0 ; 
       utann_carrier_sPTd-back.6-0 ; 
       utann_carrier_sPTd-bottom.4-0 ; 
       utann_carrier_sPTd-default.5-0 ; 
       utann_carrier_sPTd-default1.4-0 ; 
       utann_carrier_sPTd-mat12.4-0 ; 
       utann_carrier_sPTd-mat22.7-0 ; 
       utann_carrier_sPTd-mat31.4-0 ; 
       utann_carrier_sPTd-mat32.4-0 ; 
       utann_carrier_sPTd-mat39.4-0 ; 
       utann_carrier_sPTd-mat40.4-0 ; 
       utann_carrier_sPTd-mat5.4-0 ; 
       utann_carrier_sPTd-mat52.4-0 ; 
       utann_carrier_sPTd-mat53.4-0 ; 
       utann_carrier_sPTd-mat8.4-0 ; 
       utann_carrier_sPTd-mat9.4-0 ; 
       utann_carrier_sPTd-side.6-0 ; 
       utann_carrier_sPTd-top.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-cap03.77-0 ROOT ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-supergun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       static-t2d55.2-0 ; 
       static-t2d57.2-0 ; 
       static-t2d58.2-0 ; 
       static-t2d59.2-0 ; 
       static-t2d60.2-0 ; 
       static-t2d61.2-0 ; 
       utann_carrier_sPTd-t2d10.5-0 ; 
       utann_carrier_sPTd-t2d11.5-0 ; 
       utann_carrier_sPTd-t2d17.15-0 ; 
       utann_carrier_sPTd-t2d19.15-0 ; 
       utann_carrier_sPTd-t2d20.15-0 ; 
       utann_carrier_sPTd-t2d25.5-0 ; 
       utann_carrier_sPTd-t2d26.5-0 ; 
       utann_carrier_sPTd-t2d31.5-0 ; 
       utann_carrier_sPTd-t2d32.5-0 ; 
       utann_carrier_sPTd-t2d33.5-0 ; 
       utann_carrier_sPTd-t2d34.5-0 ; 
       utann_carrier_sPTd-t2d39.15-0 ; 
       utann_carrier_sPTd-t2d4.5-0 ; 
       utann_carrier_sPTd-t2d49.15-0 ; 
       utann_carrier_sPTd-t2d51.5-0 ; 
       utann_carrier_sPTd-t2d52.5-0 ; 
       utann_carrier_sPTd-t2d7.5-0 ; 
       utann_carrier_sPTd-t2d8.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 1 110 ; 
       4 6 110 ; 
       6 5 110 ; 
       7 11 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       2 9 300 ; 
       2 13 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 8 300 ; 
       4 15 300 ; 
       4 18 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       5 8 300 ; 
       6 7 300 ; 
       6 10 300 ; 
       6 21 300 ; 
       6 6 300 ; 
       6 5 300 ; 
       6 20 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       7 0 300 ; 
       8 9 300 ; 
       9 19 300 ; 
       10 9 300 ; 
       11 9 300 ; 
       11 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 13 400 ; 
       8 7 400 ; 
       10 6 400 ; 
       11 14 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 1 401 ; 
       2 0 401 ; 
       3 4 401 ; 
       4 3 401 ; 
       5 17 401 ; 
       6 10 401 ; 
       7 5 401 ; 
       10 8 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 18 401 ; 
       16 20 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 19 401 ; 
       21 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 126.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 MPRFLG 0 ; 
       5 SCHEM 91.19147 -0.3748309 0 USR SRT 1 1 1 0 0 0 0 0 -0.9983624 MPRFLG 0 ; 
       6 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 31.25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 167.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 167.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
