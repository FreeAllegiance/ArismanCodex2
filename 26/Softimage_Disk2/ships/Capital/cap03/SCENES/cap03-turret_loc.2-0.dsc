SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap03-cap03.97-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_carrier_sPTd-cam_int1.100-0 ROOT ; 
       utann_carrier_sPTd-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       turret_loc-light1.2-0 ROOT ; 
       turret_loc-light2.2-0 ROOT ; 
       turret_loc-light3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       turret_loc-back1.1-0 ; 
       turret_loc-mat54.1-0 ; 
       turret_loc-mat57.1-0 ; 
       turret_loc-mat58.1-0 ; 
       turret_loc-mat59.1-0 ; 
       turret_loc-mat61.1-0 ; 
       turret_loc-mat62.1-0 ; 
       turret_loc-mat63.2-0 ; 
       turret_loc-nose_white-center.1-11.1-0 ; 
       turret_loc-nose_white-center.1-12.1-0 ; 
       turret_loc-nose_white-center.1-15.1-0 ; 
       turret_loc-nose_white-center.1-31.1-0 ; 
       turret_loc-nose_white-center.1-32.1-0 ; 
       turret_loc-nose_white-center.1-35.1-0 ; 
       turret_loc-nose_white-center.1-38.1-0 ; 
       turret_loc-nose_white-center.1-41.1-0 ; 
       turret_loc-nose_white-center.1-7.1-0 ; 
       turret_loc-port_red-left.1-10.1-0 ; 
       turret_loc-port_red-left.1-18.1-0 ; 
       turret_loc-port_red-left.1-21.1-0 ; 
       turret_loc-port_red-left.1-4.1-0 ; 
       turret_loc-port_red-left.1-5.1-0 ; 
       turret_loc-port_red-left.1-8.1-0 ; 
       turret_loc-sides1.1-0 ; 
       turret_loc-starbord_green-right.1-1.1-0 ; 
       turret_loc-starbord_green-right.1-10.1-0 ; 
       turret_loc-starbord_green-right.1-11.1-0 ; 
       turret_loc-starbord_green-right.1-12.1-0 ; 
       turret_loc-starbord_green-right.1-3.1-0 ; 
       turret_loc-starbord_green-right.1-6.1-0 ; 
       turret_loc-starbord_green-right.1-7.1-0 ; 
       turret_loc-starbord_green-right.1-8.1-0 ; 
       turret_loc-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.4-0 ; 
       utann_carrier_sPTd-bottom.2-0 ; 
       utann_carrier_sPTd-default.3-0 ; 
       utann_carrier_sPTd-default1.2-0 ; 
       utann_carrier_sPTd-mat12.2-0 ; 
       utann_carrier_sPTd-mat22.5-0 ; 
       utann_carrier_sPTd-mat31.2-0 ; 
       utann_carrier_sPTd-mat32.2-0 ; 
       utann_carrier_sPTd-mat39.2-0 ; 
       utann_carrier_sPTd-mat40.2-0 ; 
       utann_carrier_sPTd-mat5.2-0 ; 
       utann_carrier_sPTd-mat52.2-0 ; 
       utann_carrier_sPTd-mat53.2-0 ; 
       utann_carrier_sPTd-mat8.2-0 ; 
       utann_carrier_sPTd-mat9.2-0 ; 
       utann_carrier_sPTd-side.4-0 ; 
       utann_carrier_sPTd-top.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 75     
       arc-cone.2-0 ROOT ; 
       arc2-cone.2-0 ROOT ; 
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-blthrust.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-brthrust.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.73-0 ROOT ; 
       cap03-cockpt.1-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-gun_tip.4-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret1.1-0 ; 
       cap03-lturret2.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-missemt.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret1.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-smoke.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trail.1-0 ; 
       cap03-trlndpad.1-0 ; 
       cap03-tthrust.1-0 ; 
       cap03-turwepemt1.1-0 ; 
       cap03-turwepemt2.1-0 ; 
       cap03-turwepemt3.1-0 ; 
       cap03-turwepemt4.1-0 ; 
       cap03-turwepemt5.1-0 ; 
       cap03-turwepemt6.1-0 ; 
       cap03-wepemt.1-0 ; 
       turret_loc-cone1.2-0 ROOT ; 
       turret_loc-cone2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap03-turret_loc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       turret_loc-t2d55.1-0 ; 
       turret_loc-t2d57.1-0 ; 
       turret_loc-t2d58.1-0 ; 
       turret_loc-t2d59.1-0 ; 
       turret_loc-t2d60.1-0 ; 
       turret_loc-t2d61.1-0 ; 
       utann_carrier_sPTd-t2d10.2-0 ; 
       utann_carrier_sPTd-t2d11.2-0 ; 
       utann_carrier_sPTd-t2d17.12-0 ; 
       utann_carrier_sPTd-t2d19.12-0 ; 
       utann_carrier_sPTd-t2d20.12-0 ; 
       utann_carrier_sPTd-t2d25.2-0 ; 
       utann_carrier_sPTd-t2d26.2-0 ; 
       utann_carrier_sPTd-t2d29.2-0 ; 
       utann_carrier_sPTd-t2d31.2-0 ; 
       utann_carrier_sPTd-t2d32.2-0 ; 
       utann_carrier_sPTd-t2d33.2-0 ; 
       utann_carrier_sPTd-t2d34.2-0 ; 
       utann_carrier_sPTd-t2d39.12-0 ; 
       utann_carrier_sPTd-t2d4.2-0 ; 
       utann_carrier_sPTd-t2d41.2-0 ; 
       utann_carrier_sPTd-t2d42.2-0 ; 
       utann_carrier_sPTd-t2d43.2-0 ; 
       utann_carrier_sPTd-t2d44.2-0 ; 
       utann_carrier_sPTd-t2d45.2-0 ; 
       utann_carrier_sPTd-t2d46.2-0 ; 
       utann_carrier_sPTd-t2d49.12-0 ; 
       utann_carrier_sPTd-t2d51.2-0 ; 
       utann_carrier_sPTd-t2d52.2-0 ; 
       utann_carrier_sPTd-t2d7.2-0 ; 
       utann_carrier_sPTd-t2d8.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 10 110 ; 
       3 18 110 ; 
       4 3 110 ; 
       5 18 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 26 110 ; 
       9 16 110 ; 
       10 4 110 ; 
       11 12 110 ; 
       12 18 110 ; 
       13 26 110 ; 
       14 16 110 ; 
       15 12 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 57 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 21 110 ; 
       23 12 110 ; 
       24 21 110 ; 
       25 16 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
       29 28 110 ; 
       30 18 110 ; 
       31 28 110 ; 
       32 16 110 ; 
       33 27 110 ; 
       34 33 110 ; 
       35 27 110 ; 
       36 35 110 ; 
       37 12 110 ; 
       38 12 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 40 110 ; 
       42 45 110 ; 
       43 40 110 ; 
       44 20 110 ; 
       45 20 110 ; 
       46 27 110 ; 
       47 46 110 ; 
       48 27 110 ; 
       49 27 110 ; 
       50 62 110 ; 
       51 62 110 ; 
       52 58 110 ; 
       53 59 110 ; 
       54 59 110 ; 
       55 61 110 ; 
       56 61 110 ; 
       57 5 110 ; 
       58 18 110 ; 
       59 18 110 ; 
       60 26 110 ; 
       61 18 110 ; 
       62 18 110 ; 
       63 16 110 ; 
       64 26 110 ; 
       65 16 110 ; 
       66 30 110 ; 
       67 23 110 ; 
       68 24 110 ; 
       69 22 110 ; 
       70 29 110 ; 
       71 31 110 ; 
       72 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 6 300 ; 
       74 7 300 ; 
       2 37 300 ; 
       3 37 300 ; 
       3 39 300 ; 
       3 40 300 ; 
       5 37 300 ; 
       5 41 300 ; 
       10 1 300 ; 
       10 2 300 ; 
       12 36 300 ; 
       12 43 300 ; 
       12 46 300 ; 
       12 44 300 ; 
       12 45 300 ; 
       16 36 300 ; 
       18 35 300 ; 
       18 38 300 ; 
       18 49 300 ; 
       18 34 300 ; 
       18 33 300 ; 
       18 48 300 ; 
       19 3 300 ; 
       19 23 300 ; 
       19 0 300 ; 
       20 37 300 ; 
       22 36 300 ; 
       23 36 300 ; 
       24 36 300 ; 
       26 47 300 ; 
       27 37 300 ; 
       29 37 300 ; 
       30 36 300 ; 
       31 36 300 ; 
       33 29 300 ; 
       34 20 300 ; 
       35 25 300 ; 
       36 18 300 ; 
       37 12 300 ; 
       38 11 300 ; 
       39 31 300 ; 
       40 30 300 ; 
       41 22 300 ; 
       42 19 300 ; 
       43 17 300 ; 
       44 27 300 ; 
       45 26 300 ; 
       46 28 300 ; 
       47 21 300 ; 
       48 32 300 ; 
       49 24 300 ; 
       50 13 300 ; 
       51 14 300 ; 
       52 15 300 ; 
       53 8 300 ; 
       54 16 300 ; 
       55 10 300 ; 
       56 9 300 ; 
       57 37 300 ; 
       57 42 300 ; 
       73 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 13 400 ; 
       5 14 400 ; 
       20 7 400 ; 
       22 23 400 ; 
       23 24 400 ; 
       24 25 400 ; 
       27 6 400 ; 
       29 20 400 ; 
       30 21 400 ; 
       31 22 400 ; 
       57 15 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 1 401 ; 
       2 0 401 ; 
       3 4 401 ; 
       23 3 401 ; 
       33 18 401 ; 
       34 10 401 ; 
       35 5 401 ; 
       38 8 401 ; 
       39 11 401 ; 
       40 12 401 ; 
       41 16 401 ; 
       42 17 401 ; 
       43 19 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       48 26 401 ; 
       49 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 200 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 202.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 205 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 210 0 0 DISPLAY 1 2 SRT 12.45715 12.45715 12.45715 0.31 0 0 0 8.205476 5.995504 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 SRT 15.62713 2.647234 15.62713 0.18 -7.854293e-009 1.570796 -6.380746 -4.574669 36.14885 MPRFLG 0 ; 
       74 SCHEM 225 0 0 SRT 15.62713 2.647234 15.62713 2.961593 -7.854293e-009 1.570797 6.380746 -4.574669 36.14885 MPRFLG 0 ; 
       2 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       3 SCHEM 126.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 55 -6 0 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 MPRFLG 0 ; 
       8 SCHEM 115 -6 0 MPRFLG 0 ; 
       9 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 MPRFLG 0 ; 
       12 SCHEM 60 -4 0 MPRFLG 0 ; 
       13 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 192.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 100 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       22 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       23 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 195 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 138.75 -4 0 MPRFLG 0 ; 
       29 SCHEM 141.25 -6 0 MPRFLG 0 ; 
       30 SCHEM 163.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 136.25 -6 0 MPRFLG 0 ; 
       32 SCHEM 197.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       34 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 90 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 80 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       45 SCHEM 91.25 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       46 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       47 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       48 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       49 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 157.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       51 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 145 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       53 SCHEM 150 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       54 SCHEM 147.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       55 SCHEM 155 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 152.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       57 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       58 SCHEM 145 -4 0 MPRFLG 0 ; 
       59 SCHEM 148.75 -4 0 MPRFLG 0 ; 
       60 SCHEM 110 -6 0 MPRFLG 0 ; 
       61 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       62 SCHEM 158.75 -4 0 MPRFLG 0 ; 
       63 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       65 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 163.8181 -13.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 58.65338 -15.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 98.81817 -15.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 103.8182 -15.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 141.3181 -15.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 136.3181 -15.41461 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 217.5 0 0 SRT 12.45715 1.822829 12.45715 3.141593 0 0 -1.084599 -18.11868 -6.014482 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 210 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 217.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 222.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 225 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 167.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 167.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 199 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
