SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap03-cap03.59-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_carrier_sPTd-cam_int1.59-0 ROOT ; 
       utann_carrier_sPTd-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       strobes-back1.1-0 ; 
       strobes-mat54.1-0 ; 
       strobes-mat57.1-0 ; 
       strobes-mat58.1-0 ; 
       strobes-nose_white-center.1-11.2-0 ; 
       strobes-nose_white-center.1-12.2-0 ; 
       strobes-nose_white-center.1-15.2-0 ; 
       strobes-nose_white-center.1-31.2-0 ; 
       strobes-nose_white-center.1-32.2-0 ; 
       strobes-nose_white-center.1-35.2-0 ; 
       strobes-nose_white-center.1-38.2-0 ; 
       strobes-nose_white-center.1-41.2-0 ; 
       strobes-nose_white-center.1-7.2-0 ; 
       strobes-port_red-left.1-10.2-0 ; 
       strobes-port_red-left.1-18.2-0 ; 
       strobes-port_red-left.1-21.2-0 ; 
       strobes-port_red-left.1-4.2-0 ; 
       strobes-port_red-left.1-5.2-0 ; 
       strobes-port_red-left.1-8.2-0 ; 
       strobes-sides1.1-0 ; 
       strobes-starbord_green-right.1-1.2-0 ; 
       strobes-starbord_green-right.1-10.1-0 ; 
       strobes-starbord_green-right.1-11.1-0 ; 
       strobes-starbord_green-right.1-12.1-0 ; 
       strobes-starbord_green-right.1-3.1-0 ; 
       strobes-starbord_green-right.1-6.1-0 ; 
       strobes-starbord_green-right.1-7.1-0 ; 
       strobes-starbord_green-right.1-8.1-0 ; 
       strobes-starbord_green-right.1-9.1-0 ; 
       utann_carrier_sPTd-back.1-0 ; 
       utann_carrier_sPTd-default1.1-0 ; 
       utann_carrier_sPTd-front_bottom.1-0 ; 
       utann_carrier_sPTd-mat10.2-0 ; 
       utann_carrier_sPTd-mat12.1-0 ; 
       utann_carrier_sPTd-mat22.2-0 ; 
       utann_carrier_sPTd-mat31.1-0 ; 
       utann_carrier_sPTd-mat32.1-0 ; 
       utann_carrier_sPTd-mat39.1-0 ; 
       utann_carrier_sPTd-mat40.1-0 ; 
       utann_carrier_sPTd-mat46.2-0 ; 
       utann_carrier_sPTd-mat5.1-0 ; 
       utann_carrier_sPTd-mat51.2-0 ; 
       utann_carrier_sPTd-mat52.1-0 ; 
       utann_carrier_sPTd-mat53.1-0 ; 
       utann_carrier_sPTd-mat8.1-0 ; 
       utann_carrier_sPTd-mat9.1-0 ; 
       utann_carrier_sPTd-rear_bottom.1-0 ; 
       utann_carrier_sPTd-side.1-0 ; 
       utann_carrier_sPTd-top.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       cap03-amantenn2.1-0 ; 
       cap03-amcontwr1.1-0 ; 
       cap03-amcontwr2.1-0 ; 
       cap03-bacontwr1.1-0 ; 
       cap03-balndpad.1-0 ; 
       cap03-bflndpad.1-0 ; 
       cap03-bllndpad.1-0 ; 
       cap03-bmerge1.1-0 ; 
       cap03-bmlndpad.1-0 ; 
       cap03-bplatfm.1-0 ; 
       cap03-brlndpad.1-0 ; 
       cap03-bturatt.1-0 ; 
       cap03-cap03.44-0 ROOT ; 
       cap03-cyl1.4-0 ; 
       cap03-fuselg.2-0 ; 
       cap03-larmour.1-0 ; 
       cap03-lturret0.1-0 ; 
       cap03-lturret1.1-0 ; 
       cap03-lturret2.1-0 ; 
       cap03-lturret3.1-0 ; 
       cap03-lwepatt1.4-0 ; 
       cap03-lwepatt2.1-0 ; 
       cap03-lwepatt3.1-0 ; 
       cap03-platfm2.1-0 ; 
       cap03-rarmour.1-0 ; 
       cap03-rturret0.1-0 ; 
       cap03-rturret1.1-0 ; 
       cap03-rturret2.1-0 ; 
       cap03-rturret3.1-0 ; 
       cap03-rwepatt1.1-0 ; 
       cap03-rwepatt2.1-0 ; 
       cap03-rwepatt3.1-0 ; 
       cap03-SSar0.1-0 ; 
       cap03-SSar2.1-0 ; 
       cap03-SSar4.1-0 ; 
       cap03-SSar6.1-0 ; 
       cap03-SSbl2.1-0 ; 
       cap03-SSbr2.1-0 ; 
       cap03-SSla0.1-0 ; 
       cap03-SSmal0.1-0 ; 
       cap03-SSmal1.1-0 ; 
       cap03-SSmal10.1-0 ; 
       cap03-SSmal3.1-0 ; 
       cap03-SSmal4.1-0 ; 
       cap03-SSmal8.1-0 ; 
       cap03-SSmr0.1-0 ; 
       cap03-SSmr3.1-0 ; 
       cap03-SSmr4.1-0 ; 
       cap03-SSr2.1-0 ; 
       cap03-SSta3.1-0 ; 
       cap03-SSta4.1-0 ; 
       cap03-SSta7.1-0 ; 
       cap03-SStf1.1-0 ; 
       cap03-SStf6.1-0 ; 
       cap03-SStm1.1-0 ; 
       cap03-SStm4.1-0 ; 
       cap03-supergun.1-0 ; 
       cap03-talndpad4.1-0 ; 
       cap03-tflndpad.1-0 ; 
       cap03-tllndpad.1-0 ; 
       cap03-tmlndpad.3-0 ; 
       cap03-tmlndpad3.1-0 ; 
       cap03-trlndpad.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap03/PICTURES/cap03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       reduce-strobes.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       strobes-t2d55.1-0 ; 
       strobes-t2d57.1-0 ; 
       strobes-t2d58.1-0 ; 
       strobes-t2d59.1-0 ; 
       strobes-t2d60.1-0 ; 
       utann_carrier_sPTd-t2d10.1-0 ; 
       utann_carrier_sPTd-t2d11.1-0 ; 
       utann_carrier_sPTd-t2d17.6-0 ; 
       utann_carrier_sPTd-t2d19.6-0 ; 
       utann_carrier_sPTd-t2d20.6-0 ; 
       utann_carrier_sPTd-t2d21.6-0 ; 
       utann_carrier_sPTd-t2d25.1-0 ; 
       utann_carrier_sPTd-t2d26.1-0 ; 
       utann_carrier_sPTd-t2d29.1-0 ; 
       utann_carrier_sPTd-t2d31.1-0 ; 
       utann_carrier_sPTd-t2d32.1-0 ; 
       utann_carrier_sPTd-t2d33.1-0 ; 
       utann_carrier_sPTd-t2d34.1-0 ; 
       utann_carrier_sPTd-t2d39.6-0 ; 
       utann_carrier_sPTd-t2d4.1-0 ; 
       utann_carrier_sPTd-t2d41.1-0 ; 
       utann_carrier_sPTd-t2d42.1-0 ; 
       utann_carrier_sPTd-t2d43.1-0 ; 
       utann_carrier_sPTd-t2d44.1-0 ; 
       utann_carrier_sPTd-t2d45.1-0 ; 
       utann_carrier_sPTd-t2d46.1-0 ; 
       utann_carrier_sPTd-t2d49.6-0 ; 
       utann_carrier_sPTd-t2d51.1-0 ; 
       utann_carrier_sPTd-t2d52.1-0 ; 
       utann_carrier_sPTd-t2d7.1-0 ; 
       utann_carrier_sPTd-t2d8.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 56 110 ; 
       0 7 110 ; 
       1 14 110 ; 
       2 1 110 ; 
       3 14 110 ; 
       56 3 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 23 110 ; 
       7 2 110 ; 
       8 9 110 ; 
       9 14 110 ; 
       10 23 110 ; 
       11 9 110 ; 
       14 12 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 16 110 ; 
       18 9 110 ; 
       19 16 110 ; 
       20 17 110 ; 
       21 18 110 ; 
       22 19 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 25 110 ; 
       27 14 110 ; 
       28 25 110 ; 
       29 26 110 ; 
       30 27 110 ; 
       31 28 110 ; 
       32 24 110 ; 
       33 32 110 ; 
       34 24 110 ; 
       35 34 110 ; 
       36 9 110 ; 
       37 9 110 ; 
       38 15 110 ; 
       39 15 110 ; 
       40 39 110 ; 
       41 44 110 ; 
       42 39 110 ; 
       43 15 110 ; 
       44 15 110 ; 
       45 24 110 ; 
       46 45 110 ; 
       47 24 110 ; 
       48 24 110 ; 
       49 61 110 ; 
       50 61 110 ; 
       51 57 110 ; 
       52 58 110 ; 
       53 58 110 ; 
       54 60 110 ; 
       55 60 110 ; 
       57 14 110 ; 
       58 14 110 ; 
       59 23 110 ; 
       60 14 110 ; 
       61 14 110 ; 
       62 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 3 300 ; 
       13 19 300 ; 
       13 0 300 ; 
       0 33 300 ; 
       1 33 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       3 33 300 ; 
       3 37 300 ; 
       56 33 300 ; 
       56 38 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       9 30 300 ; 
       9 40 300 ; 
       9 44 300 ; 
       9 42 300 ; 
       9 43 300 ; 
       12 30 300 ; 
       14 32 300 ; 
       14 34 300 ; 
       14 48 300 ; 
       14 31 300 ; 
       14 46 300 ; 
       14 29 300 ; 
       14 39 300 ; 
       14 47 300 ; 
       14 41 300 ; 
       15 33 300 ; 
       17 30 300 ; 
       18 30 300 ; 
       19 30 300 ; 
       23 45 300 ; 
       24 33 300 ; 
       26 33 300 ; 
       27 30 300 ; 
       28 30 300 ; 
       32 25 300 ; 
       33 16 300 ; 
       34 21 300 ; 
       35 14 300 ; 
       36 8 300 ; 
       37 7 300 ; 
       38 27 300 ; 
       39 26 300 ; 
       40 18 300 ; 
       41 15 300 ; 
       42 13 300 ; 
       43 23 300 ; 
       44 22 300 ; 
       45 24 300 ; 
       46 17 300 ; 
       47 28 300 ; 
       48 20 300 ; 
       49 9 300 ; 
       50 10 300 ; 
       51 11 300 ; 
       52 4 300 ; 
       53 12 300 ; 
       54 6 300 ; 
       55 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 13 400 ; 
       3 14 400 ; 
       56 15 400 ; 
       15 6 400 ; 
       17 23 400 ; 
       18 24 400 ; 
       19 25 400 ; 
       24 5 400 ; 
       26 20 400 ; 
       27 21 400 ; 
       28 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       12 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       2 0 401 ; 
       29 18 401 ; 
       31 9 401 ; 
       34 7 401 ; 
       35 11 401 ; 
       36 12 401 ; 
       37 16 401 ; 
       38 17 401 ; 
       40 19 401 ; 
       42 27 401 ; 
       43 28 401 ; 
       44 29 401 ; 
       45 30 401 ; 
       46 10 401 ; 
       47 26 401 ; 
       48 8 401 ; 
       3 4 401 ; 
       19 3 401 ; 
       0 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 118.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 123.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 121.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       56 SCHEM 30 -6 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 121.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 105 -6 0 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 MPRFLG 0 ; 
       12 SCHEM 93.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 93.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 96.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 100 -8 0 MPRFLG 0 ; 
       21 SCHEM 55 -8 0 MPRFLG 0 ; 
       22 SCHEM 95 -8 0 MPRFLG 0 ; 
       23 SCHEM 110 -4 0 MPRFLG 0 ; 
       24 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 136.25 -4 0 MPRFLG 0 ; 
       26 SCHEM 138.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 161.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 133.75 -6 0 MPRFLG 0 ; 
       29 SCHEM 137.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 160 -6 0 MPRFLG 0 ; 
       31 SCHEM 132.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 3.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 45 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 60 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 75 -6 0 MPRFLG 0 ; 
       39 SCHEM 80 -6 0 MPRFLG 0 ; 
       40 SCHEM 80 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 77.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 85 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       44 SCHEM 88.75 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       45 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       46 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       47 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       48 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 155 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       50 SCHEM 157.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 142.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       52 SCHEM 147.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       53 SCHEM 145 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       54 SCHEM 152.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 150 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       57 SCHEM 142.5 -4 0 MPRFLG 0 ; 
       58 SCHEM 146.25 -4 0 MPRFLG 0 ; 
       59 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       60 SCHEM 151.25 -4 0 MPRFLG 0 ; 
       61 SCHEM 156.25 -4 0 MPRFLG 0 ; 
       62 SCHEM 110 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 167.5 -4 1 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 167.5 -6 1 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 117.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 182.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 186.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 75 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
