SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.10-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       add_top_tur-mat100.1-0 ; 
       add_top_tur-mat101.1-0 ; 
       add_top_tur-mat102.1-0 ; 
       add_top_tur-mat131.1-0 ; 
       add_top_tur-mat94.1-0 ; 
       add_top_tur-mat95.1-0 ; 
       add_top_tur-mat96.1-0 ; 
       add_top_tur-mat97.1-0 ; 
       add_top_tur-mat98.1-0 ; 
       add_top_tur-mat99.1-0 ; 
       il_shdgnrator_sPaTL-mat10.3-0 ; 
       il_shdgnrator_sPaTL-mat11.3-0 ; 
       il_shdgnrator_sPaTL-mat12.3-0 ; 
       il_shdgnrator_sPaTL-mat13.3-0 ; 
       il_shdgnrator_sPaTL-mat14.3-0 ; 
       il_shdgnrator_sPaTL-mat15.3-0 ; 
       il_shdgnrator_sPaTL-mat16.3-0 ; 
       il_shdgnrator_sPaTL-mat17.3-0 ; 
       il_shdgnrator_sPaTL-mat18.3-0 ; 
       il_shdgnrator_sPaTL-mat19.3-0 ; 
       il_shdgnrator_sPaTL-mat20.3-0 ; 
       il_shdgnrator_sPaTL-mat21.3-0 ; 
       il_shdgnrator_sPaTL-mat22.3-0 ; 
       il_shdgnrator_sPaTL-mat23.3-0 ; 
       il_shdgnrator_sPaTL-mat24.3-0 ; 
       il_shdgnrator_sPaTL-mat25.3-0 ; 
       il_shdgnrator_sPaTL-mat26.3-0 ; 
       il_shdgnrator_sPaTL-mat27.3-0 ; 
       il_shdgnrator_sPaTL-mat28.3-0 ; 
       il_shdgnrator_sPaTL-mat3.3-0 ; 
       il_shdgnrator_sPaTL-mat30.3-0 ; 
       il_shdgnrator_sPaTL-mat31.3-0 ; 
       il_shdgnrator_sPaTL-mat32.3-0 ; 
       il_shdgnrator_sPaTL-mat33.3-0 ; 
       il_shdgnrator_sPaTL-mat34.3-0 ; 
       il_shdgnrator_sPaTL-mat35.3-0 ; 
       il_shdgnrator_sPaTL-mat38.3-0 ; 
       il_shdgnrator_sPaTL-mat39.3-0 ; 
       il_shdgnrator_sPaTL-mat40.3-0 ; 
       il_shdgnrator_sPaTL-mat41.3-0 ; 
       il_shdgnrator_sPaTL-mat42.3-0 ; 
       il_shdgnrator_sPaTL-mat43.3-0 ; 
       il_shdgnrator_sPaTL-mat44.3-0 ; 
       il_shdgnrator_sPaTL-mat45.3-0 ; 
       il_shdgnrator_sPaTL-mat46.3-0 ; 
       il_shdgnrator_sPaTL-mat47.3-0 ; 
       il_shdgnrator_sPaTL-mat48.3-0 ; 
       il_shdgnrator_sPaTL-mat49.3-0 ; 
       il_shdgnrator_sPaTL-mat50.3-0 ; 
       il_shdgnrator_sPaTL-mat51.3-0 ; 
       il_shdgnrator_sPaTL-mat6.3-0 ; 
       il_shdgnrator_sPaTL-mat7.3-0 ; 
       il_shdgnrator_sPaTL-mat8.3-0 ; 
       il_shdgnrator_sPaTL-mat9.3-0 ; 
       il_shieldgen_sPATL-mat88.3-0 ; 
       il_shieldgen_sPATL-mat91.3-0 ; 
       il_shieldgen_sPATL-mat92.3-0 ; 
       rix_fighter_sPAt-mat75.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       add_top_tur-turret1.1-0 ROOT ; 
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-cap04.9-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-lwingzz3.1-0 ; 
       cap04-lwingzz4.1-0 ; 
       cap04-lwingzz5.1-0 ; 
       cap04-missemt.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-rwingzz3.1-0 ; 
       cap04-rwingzz4.1-0 ; 
       cap04-rwingzz5.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turret.1-0 ; 
       cap04-turwepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-add_top-tur.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       add_top_tur-t2d46.1-0 ; 
       add_top_tur-t2d47.1-0 ; 
       add_top_tur-t2d48.1-0 ; 
       il_shdgnrator_sPaTL-t2d1.3-0 ; 
       il_shdgnrator_sPaTL-t2d10.3-0 ; 
       il_shdgnrator_sPaTL-t2d11.3-0 ; 
       il_shdgnrator_sPaTL-t2d12.3-0 ; 
       il_shdgnrator_sPaTL-t2d13.3-0 ; 
       il_shdgnrator_sPaTL-t2d14.3-0 ; 
       il_shdgnrator_sPaTL-t2d15.3-0 ; 
       il_shdgnrator_sPaTL-t2d16.3-0 ; 
       il_shdgnrator_sPaTL-t2d17.3-0 ; 
       il_shdgnrator_sPaTL-t2d18.3-0 ; 
       il_shdgnrator_sPaTL-t2d19.3-0 ; 
       il_shdgnrator_sPaTL-t2d2.3-0 ; 
       il_shdgnrator_sPaTL-t2d20.3-0 ; 
       il_shdgnrator_sPaTL-t2d21.3-0 ; 
       il_shdgnrator_sPaTL-t2d22.3-0 ; 
       il_shdgnrator_sPaTL-t2d23.3-0 ; 
       il_shdgnrator_sPaTL-t2d25.3-0 ; 
       il_shdgnrator_sPaTL-t2d26.3-0 ; 
       il_shdgnrator_sPaTL-t2d27.3-0 ; 
       il_shdgnrator_sPaTL-t2d28.3-0 ; 
       il_shdgnrator_sPaTL-t2d29.3-0 ; 
       il_shdgnrator_sPaTL-t2d3.3-0 ; 
       il_shdgnrator_sPaTL-t2d30.3-0 ; 
       il_shdgnrator_sPaTL-t2d33.3-0 ; 
       il_shdgnrator_sPaTL-t2d34.3-0 ; 
       il_shdgnrator_sPaTL-t2d35.3-0 ; 
       il_shdgnrator_sPaTL-t2d36.3-0 ; 
       il_shdgnrator_sPaTL-t2d37.3-0 ; 
       il_shdgnrator_sPaTL-t2d38.3-0 ; 
       il_shdgnrator_sPaTL-t2d39.3-0 ; 
       il_shdgnrator_sPaTL-t2d4.3-0 ; 
       il_shdgnrator_sPaTL-t2d40.3-0 ; 
       il_shdgnrator_sPaTL-t2d41.3-0 ; 
       il_shdgnrator_sPaTL-t2d42.3-0 ; 
       il_shdgnrator_sPaTL-t2d43.3-0 ; 
       il_shdgnrator_sPaTL-t2d44.3-0 ; 
       il_shdgnrator_sPaTL-t2d45.3-0 ; 
       il_shdgnrator_sPaTL-t2d5.3-0 ; 
       il_shdgnrator_sPaTL-t2d6.3-0 ; 
       il_shdgnrator_sPaTL-t2d7.3-0 ; 
       il_shdgnrator_sPaTL-t2d8.3-0 ; 
       il_shdgnrator_sPaTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 4 110 ; 
       2 4 110 ; 
       3 7 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 13 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       16 21 110 ; 
       17 16 110 ; 
       18 4 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 7 110 ; 
       24 11 110 ; 
       25 14 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 6 110 ; 
       29 19 110 ; 
       30 22 110 ; 
       31 16 110 ; 
       32 16 110 ; 
       33 6 110 ; 
       34 4 110 ; 
       35 4 110 ; 
       36 4 110 ; 
       37 7 110 ; 
       38 7 110 ; 
       39 38 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 27 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       7 53 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 45 300 ; 
       7 9 300 ; 
       8 47 300 ; 
       8 48 300 ; 
       8 49 300 ; 
       10 22 300 ; 
       10 30 300 ; 
       10 46 300 ; 
       11 23 300 ; 
       11 38 300 ; 
       12 24 300 ; 
       12 31 300 ; 
       13 26 300 ; 
       13 32 300 ; 
       14 25 300 ; 
       14 39 300 ; 
       16 29 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       18 15 300 ; 
       18 35 300 ; 
       19 16 300 ; 
       19 36 300 ; 
       20 17 300 ; 
       20 34 300 ; 
       21 19 300 ; 
       21 33 300 ; 
       22 18 300 ; 
       22 37 300 ; 
       23 56 300 ; 
       24 4 300 ; 
       25 8 300 ; 
       26 7 300 ; 
       27 1 300 ; 
       28 55 300 ; 
       29 5 300 ; 
       30 57 300 ; 
       31 6 300 ; 
       32 0 300 ; 
       33 54 300 ; 
       37 28 300 ; 
       38 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 2 401 ; 
       2 1 401 ; 
       9 0 401 ; 
       10 33 401 ; 
       11 40 401 ; 
       12 41 401 ; 
       13 42 401 ; 
       14 43 401 ; 
       15 44 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 9 401 ; 
       22 10 401 ; 
       23 11 401 ; 
       24 12 401 ; 
       25 13 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       28 18 401 ; 
       30 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 28 401 ; 
       39 29 401 ; 
       40 30 401 ; 
       41 31 401 ; 
       42 32 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       45 36 401 ; 
       46 37 401 ; 
       48 38 401 ; 
       49 39 401 ; 
       50 17 401 ; 
       51 3 401 ; 
       52 14 401 ; 
       53 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 MPRFLG 0 ; 
       4 SCHEM 31.25 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 MPRFLG 0 ; 
       20 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 25 -8 0 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 5 -10 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 40 -4 0 MPRFLG 0 ; 
       29 SCHEM 15 -6 0 MPRFLG 0 ; 
       30 SCHEM 25 -10 0 MPRFLG 0 ; 
       31 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 20 -10 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 37.5 -3.587081 0 USR MPRFLG 0 ; 
       39 SCHEM 37.5 -5.587081 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 62.5 0 0 USR SRT 0.2241515 0.2241515 0.2241515 3.141593 3.141593 0 -1.24546e-008 5.293238 0.1440821 MPRFLG 0 ; 
       15 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -5.587081 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -7.587081 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
