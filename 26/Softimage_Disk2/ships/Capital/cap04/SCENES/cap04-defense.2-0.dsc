SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.15-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.15-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       add_top_tur-mat100.1-0 ; 
       add_top_tur-mat101.1-0 ; 
       add_top_tur-mat102.1-0 ; 
       add_top_tur-mat131.1-0 ; 
       add_top_tur-mat94.1-0 ; 
       add_top_tur-mat95.1-0 ; 
       add_top_tur-mat96.1-0 ; 
       add_top_tur-mat97.1-0 ; 
       add_top_tur-mat98.1-0 ; 
       add_top_tur-mat99.1-0 ; 
       defense-mat132.1-0 ; 
       defense-mat133.1-0 ; 
       defense-mat134.1-0 ; 
       defense-mat135.1-0 ; 
       il_shdgnrator_sPaTL-mat10.3-0 ; 
       il_shdgnrator_sPaTL-mat11.3-0 ; 
       il_shdgnrator_sPaTL-mat12.3-0 ; 
       il_shdgnrator_sPaTL-mat13.3-0 ; 
       il_shdgnrator_sPaTL-mat14.3-0 ; 
       il_shdgnrator_sPaTL-mat15.3-0 ; 
       il_shdgnrator_sPaTL-mat16.3-0 ; 
       il_shdgnrator_sPaTL-mat20.3-0 ; 
       il_shdgnrator_sPaTL-mat21.3-0 ; 
       il_shdgnrator_sPaTL-mat22.3-0 ; 
       il_shdgnrator_sPaTL-mat23.3-0 ; 
       il_shdgnrator_sPaTL-mat27.3-0 ; 
       il_shdgnrator_sPaTL-mat28.3-0 ; 
       il_shdgnrator_sPaTL-mat3.3-0 ; 
       il_shdgnrator_sPaTL-mat30.3-0 ; 
       il_shdgnrator_sPaTL-mat35.3-0 ; 
       il_shdgnrator_sPaTL-mat38.3-0 ; 
       il_shdgnrator_sPaTL-mat40.3-0 ; 
       il_shdgnrator_sPaTL-mat42.3-0 ; 
       il_shdgnrator_sPaTL-mat43.3-0 ; 
       il_shdgnrator_sPaTL-mat44.3-0 ; 
       il_shdgnrator_sPaTL-mat45.3-0 ; 
       il_shdgnrator_sPaTL-mat46.3-0 ; 
       il_shdgnrator_sPaTL-mat47.3-0 ; 
       il_shdgnrator_sPaTL-mat48.3-0 ; 
       il_shdgnrator_sPaTL-mat49.3-0 ; 
       il_shdgnrator_sPaTL-mat50.3-0 ; 
       il_shdgnrator_sPaTL-mat51.3-0 ; 
       il_shdgnrator_sPaTL-mat6.3-0 ; 
       il_shdgnrator_sPaTL-mat7.3-0 ; 
       il_shdgnrator_sPaTL-mat8.3-0 ; 
       il_shdgnrator_sPaTL-mat9.3-0 ; 
       il_shieldgen_sPATL-mat88.3-0 ; 
       il_shieldgen_sPATL-mat91.3-0 ; 
       il_shieldgen_sPATL-mat92.3-0 ; 
       rix_fighter_sPAt-mat75.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-bturret.1-0 ; 
       cap04-cap04.13-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-missemt.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-tturret1.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turwepemt1.1-0 ; 
       cap04-turwepemt2.1-0 ; 
       defense-cone1.1-0 ROOT ; 
       defense-cone2.1-0 ROOT ; 
       defense-cone3.1-0 ROOT ; 
       defense-cone4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-defense.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_top_tur-t2d46.1-0 ; 
       add_top_tur-t2d47.1-0 ; 
       add_top_tur-t2d48.1-0 ; 
       il_shdgnrator_sPaTL-t2d1.3-0 ; 
       il_shdgnrator_sPaTL-t2d10.3-0 ; 
       il_shdgnrator_sPaTL-t2d14.3-0 ; 
       il_shdgnrator_sPaTL-t2d15.3-0 ; 
       il_shdgnrator_sPaTL-t2d16.3-0 ; 
       il_shdgnrator_sPaTL-t2d17.3-0 ; 
       il_shdgnrator_sPaTL-t2d2.3-0 ; 
       il_shdgnrator_sPaTL-t2d21.3-0 ; 
       il_shdgnrator_sPaTL-t2d22.3-0 ; 
       il_shdgnrator_sPaTL-t2d23.3-0 ; 
       il_shdgnrator_sPaTL-t2d25.3-0 ; 
       il_shdgnrator_sPaTL-t2d3.3-0 ; 
       il_shdgnrator_sPaTL-t2d30.3-0 ; 
       il_shdgnrator_sPaTL-t2d33.3-0 ; 
       il_shdgnrator_sPaTL-t2d35.3-0 ; 
       il_shdgnrator_sPaTL-t2d37.3-0 ; 
       il_shdgnrator_sPaTL-t2d38.3-0 ; 
       il_shdgnrator_sPaTL-t2d39.3-0 ; 
       il_shdgnrator_sPaTL-t2d4.3-0 ; 
       il_shdgnrator_sPaTL-t2d40.3-0 ; 
       il_shdgnrator_sPaTL-t2d41.3-0 ; 
       il_shdgnrator_sPaTL-t2d42.3-0 ; 
       il_shdgnrator_sPaTL-t2d43.3-0 ; 
       il_shdgnrator_sPaTL-t2d44.3-0 ; 
       il_shdgnrator_sPaTL-t2d45.3-0 ; 
       il_shdgnrator_sPaTL-t2d5.3-0 ; 
       il_shdgnrator_sPaTL-t2d6.3-0 ; 
       il_shdgnrator_sPaTL-t2d7.3-0 ; 
       il_shdgnrator_sPaTL-t2d8.3-0 ; 
       il_shdgnrator_sPaTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 7 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 10 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       34 31 110 ; 
       13 15 110 ; 
       14 13 110 ; 
       15 4 110 ; 
       16 15 110 ; 
       17 7 110 ; 
       18 11 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 6 110 ; 
       23 16 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 6 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       32 7 110 ; 
       3 7 110 ; 
       33 3 110 ; 
       31 7 110 ; 
       12 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 25 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 45 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 9 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       10 23 300 ; 
       10 28 300 ; 
       10 38 300 ; 
       11 24 300 ; 
       11 31 300 ; 
       35 13 300 ; 
       36 12 300 ; 
       13 27 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       15 19 300 ; 
       15 29 300 ; 
       16 20 300 ; 
       16 30 300 ; 
       37 11 300 ; 
       38 10 300 ; 
       17 48 300 ; 
       18 4 300 ; 
       19 8 300 ; 
       20 7 300 ; 
       21 1 300 ; 
       22 47 300 ; 
       23 5 300 ; 
       24 49 300 ; 
       25 6 300 ; 
       26 0 300 ; 
       27 46 300 ; 
       32 26 300 ; 
       3 2 300 ; 
       31 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 2 401 ; 
       2 1 401 ; 
       9 0 401 ; 
       14 21 401 ; 
       15 28 401 ; 
       16 29 401 ; 
       17 30 401 ; 
       18 31 401 ; 
       19 32 401 ; 
       20 4 401 ; 
       21 5 401 ; 
       22 6 401 ; 
       23 7 401 ; 
       24 8 401 ; 
       25 10 401 ; 
       26 12 401 ; 
       28 13 401 ; 
       29 15 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       37 24 401 ; 
       38 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 11 401 ; 
       43 3 401 ; 
       44 9 401 ; 
       45 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 135 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 132.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 75 -4 0 MPRFLG 0 ; 
       4 SCHEM 73.75 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 142.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 125 -2 0 MPRFLG 0 ; 
       7 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       34 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 155 0 0 SRT 3.437936 3.437936 3.437936 0 0 0 0 -13.71223 6.981259 MPRFLG 0 ; 
       36 SCHEM 147.5 0 0 SRT 3.437936 3.437936 3.437936 3.141593 0 0 0 17.55383 0.0993275 MPRFLG 0 ; 
       13 SCHEM 50 -4 0 MPRFLG 0 ; 
       14 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 150 0 0 SRT 3.437936 3.437936 3.437936 -1.570796 0 0 15.29318 -2.157384 14.79777 MPRFLG 0 ; 
       38 SCHEM 152.5 0 0 SRT 3.437936 3.437936 3.437936 -1.570796 0 0 -14.95333 -2.157384 14.79777 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 10 -6 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 35 -6 0 MPRFLG 0 ; 
       24 SCHEM 50 -6 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 45 -6 0 MPRFLG 0 ; 
       27 SCHEM 125 -4 0 MPRFLG 0 ; 
       28 SCHEM 140 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 70 -4 0 MPRFLG 0 ; 
       3 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 152.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 147.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 155 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 146.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 29 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
