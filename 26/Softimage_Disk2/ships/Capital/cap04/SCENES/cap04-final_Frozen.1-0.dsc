SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.8-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 59     
       final_Frozen-default1.1-0 ; 
       final_Frozen-default2.1-0 ; 
       final_Frozen-mat100.1-0 ; 
       final_Frozen-mat101.1-0 ; 
       final_Frozen-mat102.1-0 ; 
       final_Frozen-mat94.1-0 ; 
       final_Frozen-mat95.1-0 ; 
       final_Frozen-mat96.1-0 ; 
       final_Frozen-mat97.1-0 ; 
       final_Frozen-mat98.1-0 ; 
       final_Frozen-mat99.1-0 ; 
       il_shdgnrator_sPaTL-mat10.2-0 ; 
       il_shdgnrator_sPaTL-mat11.2-0 ; 
       il_shdgnrator_sPaTL-mat12.2-0 ; 
       il_shdgnrator_sPaTL-mat13.2-0 ; 
       il_shdgnrator_sPaTL-mat14.2-0 ; 
       il_shdgnrator_sPaTL-mat15.2-0 ; 
       il_shdgnrator_sPaTL-mat16.2-0 ; 
       il_shdgnrator_sPaTL-mat17.2-0 ; 
       il_shdgnrator_sPaTL-mat18.2-0 ; 
       il_shdgnrator_sPaTL-mat19.2-0 ; 
       il_shdgnrator_sPaTL-mat20.2-0 ; 
       il_shdgnrator_sPaTL-mat21.2-0 ; 
       il_shdgnrator_sPaTL-mat22.2-0 ; 
       il_shdgnrator_sPaTL-mat23.2-0 ; 
       il_shdgnrator_sPaTL-mat24.2-0 ; 
       il_shdgnrator_sPaTL-mat25.2-0 ; 
       il_shdgnrator_sPaTL-mat26.2-0 ; 
       il_shdgnrator_sPaTL-mat27.2-0 ; 
       il_shdgnrator_sPaTL-mat28.2-0 ; 
       il_shdgnrator_sPaTL-mat3.2-0 ; 
       il_shdgnrator_sPaTL-mat30.2-0 ; 
       il_shdgnrator_sPaTL-mat31.2-0 ; 
       il_shdgnrator_sPaTL-mat32.2-0 ; 
       il_shdgnrator_sPaTL-mat33.2-0 ; 
       il_shdgnrator_sPaTL-mat34.2-0 ; 
       il_shdgnrator_sPaTL-mat35.2-0 ; 
       il_shdgnrator_sPaTL-mat38.2-0 ; 
       il_shdgnrator_sPaTL-mat39.2-0 ; 
       il_shdgnrator_sPaTL-mat40.2-0 ; 
       il_shdgnrator_sPaTL-mat41.2-0 ; 
       il_shdgnrator_sPaTL-mat42.2-0 ; 
       il_shdgnrator_sPaTL-mat43.2-0 ; 
       il_shdgnrator_sPaTL-mat44.2-0 ; 
       il_shdgnrator_sPaTL-mat45.2-0 ; 
       il_shdgnrator_sPaTL-mat46.2-0 ; 
       il_shdgnrator_sPaTL-mat47.2-0 ; 
       il_shdgnrator_sPaTL-mat48.2-0 ; 
       il_shdgnrator_sPaTL-mat49.2-0 ; 
       il_shdgnrator_sPaTL-mat50.2-0 ; 
       il_shdgnrator_sPaTL-mat51.2-0 ; 
       il_shdgnrator_sPaTL-mat6.2-0 ; 
       il_shdgnrator_sPaTL-mat7.2-0 ; 
       il_shdgnrator_sPaTL-mat8.2-0 ; 
       il_shdgnrator_sPaTL-mat9.2-0 ; 
       il_shieldgen_sPATL-mat88.2-0 ; 
       il_shieldgen_sPATL-mat91.2-0 ; 
       il_shieldgen_sPATL-mat92.2-0 ; 
       rix_fighter_sPAt-mat75.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-cap04.7-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-lwingzz3.1-0 ; 
       cap04-lwingzz4.1-0 ; 
       cap04-lwingzz5.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-rwingzz3.1-0 ; 
       cap04-rwingzz4.1-0 ; 
       cap04-rwingzz5.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turret.1-0 ; 
       cap04-turwepemt.1-0 ; 
       cap04-XX.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-final_Frozen.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       final_Frozen-t2d46.1-0 ; 
       final_Frozen-t2d47.1-0 ; 
       il_shdgnrator_sPaTL-t2d1.2-0 ; 
       il_shdgnrator_sPaTL-t2d10.2-0 ; 
       il_shdgnrator_sPaTL-t2d11.2-0 ; 
       il_shdgnrator_sPaTL-t2d12.2-0 ; 
       il_shdgnrator_sPaTL-t2d13.2-0 ; 
       il_shdgnrator_sPaTL-t2d14.2-0 ; 
       il_shdgnrator_sPaTL-t2d15.2-0 ; 
       il_shdgnrator_sPaTL-t2d16.2-0 ; 
       il_shdgnrator_sPaTL-t2d17.2-0 ; 
       il_shdgnrator_sPaTL-t2d18.2-0 ; 
       il_shdgnrator_sPaTL-t2d19.2-0 ; 
       il_shdgnrator_sPaTL-t2d2.2-0 ; 
       il_shdgnrator_sPaTL-t2d20.2-0 ; 
       il_shdgnrator_sPaTL-t2d21.2-0 ; 
       il_shdgnrator_sPaTL-t2d22.2-0 ; 
       il_shdgnrator_sPaTL-t2d23.2-0 ; 
       il_shdgnrator_sPaTL-t2d25.2-0 ; 
       il_shdgnrator_sPaTL-t2d26.2-0 ; 
       il_shdgnrator_sPaTL-t2d27.2-0 ; 
       il_shdgnrator_sPaTL-t2d28.2-0 ; 
       il_shdgnrator_sPaTL-t2d29.2-0 ; 
       il_shdgnrator_sPaTL-t2d3.2-0 ; 
       il_shdgnrator_sPaTL-t2d30.2-0 ; 
       il_shdgnrator_sPaTL-t2d33.2-0 ; 
       il_shdgnrator_sPaTL-t2d34.2-0 ; 
       il_shdgnrator_sPaTL-t2d35.2-0 ; 
       il_shdgnrator_sPaTL-t2d36.2-0 ; 
       il_shdgnrator_sPaTL-t2d37.2-0 ; 
       il_shdgnrator_sPaTL-t2d38.2-0 ; 
       il_shdgnrator_sPaTL-t2d39.2-0 ; 
       il_shdgnrator_sPaTL-t2d4.2-0 ; 
       il_shdgnrator_sPaTL-t2d40.2-0 ; 
       il_shdgnrator_sPaTL-t2d41.2-0 ; 
       il_shdgnrator_sPaTL-t2d42.2-0 ; 
       il_shdgnrator_sPaTL-t2d43.2-0 ; 
       il_shdgnrator_sPaTL-t2d44.2-0 ; 
       il_shdgnrator_sPaTL-t2d45.2-0 ; 
       il_shdgnrator_sPaTL-t2d5.2-0 ; 
       il_shdgnrator_sPaTL-t2d6.2-0 ; 
       il_shdgnrator_sPaTL-t2d7.2-0 ; 
       il_shdgnrator_sPaTL-t2d8.2-0 ; 
       il_shdgnrator_sPaTL-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 12 110 ; 
       9 3 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 19 110 ; 
       16 3 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 18 110 ; 
       20 19 110 ; 
       21 6 110 ; 
       22 10 110 ; 
       23 13 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       26 5 110 ; 
       27 17 110 ; 
       28 20 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 5 110 ; 
       35 6 110 ; 
       38 6 110 ; 
       36 6 110 ; 
       33 3 110 ; 
       4 3 110 ; 
       8 7 110 ; 
       15 14 110 ; 
       1 3 110 ; 
       0 3 110 ; 
       34 3 110 ; 
       32 3 110 ; 
       37 36 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 28 300 ; 
       6 51 300 ; 
       6 52 300 ; 
       6 53 300 ; 
       6 54 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 41 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 10 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       9 23 300 ; 
       9 31 300 ; 
       9 47 300 ; 
       10 24 300 ; 
       10 39 300 ; 
       11 25 300 ; 
       11 32 300 ; 
       12 27 300 ; 
       12 33 300 ; 
       13 26 300 ; 
       13 40 300 ; 
       14 30 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       16 16 300 ; 
       16 36 300 ; 
       17 17 300 ; 
       17 37 300 ; 
       18 18 300 ; 
       18 35 300 ; 
       19 20 300 ; 
       19 34 300 ; 
       20 19 300 ; 
       20 38 300 ; 
       21 57 300 ; 
       22 5 300 ; 
       23 9 300 ; 
       24 8 300 ; 
       25 3 300 ; 
       26 56 300 ; 
       27 6 300 ; 
       28 58 300 ; 
       29 7 300 ; 
       30 2 300 ; 
       31 55 300 ; 
       35 29 300 ; 
       38 0 300 ; 
       38 1 300 ; 
       36 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 32 401 ; 
       12 39 401 ; 
       13 40 401 ; 
       14 41 401 ; 
       15 42 401 ; 
       16 43 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       26 12 401 ; 
       27 14 401 ; 
       28 15 401 ; 
       29 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 25 401 ; 
       38 26 401 ; 
       39 27 401 ; 
       40 28 401 ; 
       41 29 401 ; 
       42 30 401 ; 
       43 31 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       51 16 401 ; 
       52 2 401 ; 
       53 13 401 ; 
       54 23 401 ; 
       10 0 401 ; 
       4 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -12 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       10 SCHEM 0 -8 0 MPRFLG 0 ; 
       11 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       13 SCHEM 10 -12 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 18.75 -10 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 0 -10 0 MPRFLG 0 ; 
       23 SCHEM 10 -14 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -14 0 MPRFLG 0 ; 
       25 SCHEM 5 -14 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 22.5 -14 0 MPRFLG 0 ; 
       29 SCHEM 15 -14 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       31 SCHEM 40 -8 0 MPRFLG 0 ; 
       35 SCHEM 25 -8 0 MPRFLG 0 ; 
       38 SCHEM 30 -8 0 MPRFLG 0 ; 
       36 SCHEM 35 -7.587081 0 USR MPRFLG 0 ; 
       33 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -14 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 35 -9.587081 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -9.587081 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -11.58708 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
