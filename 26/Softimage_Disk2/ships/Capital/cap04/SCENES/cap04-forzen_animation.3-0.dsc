SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.5-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       forzen_animation-light1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 59     
       forzen_animation-default1.1-0 ; 
       forzen_animation-default2.1-0 ; 
       forzen_animation-mat100.1-0 ; 
       forzen_animation-mat101.1-0 ; 
       forzen_animation-mat102.1-0 ; 
       forzen_animation-mat94.1-0 ; 
       forzen_animation-mat95.1-0 ; 
       forzen_animation-mat96.1-0 ; 
       forzen_animation-mat97.1-0 ; 
       forzen_animation-mat98.1-0 ; 
       forzen_animation-mat99.1-0 ; 
       il_shdgnrator_sPaTL-mat10.2-0 ; 
       il_shdgnrator_sPaTL-mat11.2-0 ; 
       il_shdgnrator_sPaTL-mat12.2-0 ; 
       il_shdgnrator_sPaTL-mat13.2-0 ; 
       il_shdgnrator_sPaTL-mat14.2-0 ; 
       il_shdgnrator_sPaTL-mat15.2-0 ; 
       il_shdgnrator_sPaTL-mat16.2-0 ; 
       il_shdgnrator_sPaTL-mat17.2-0 ; 
       il_shdgnrator_sPaTL-mat18.2-0 ; 
       il_shdgnrator_sPaTL-mat19.2-0 ; 
       il_shdgnrator_sPaTL-mat20.2-0 ; 
       il_shdgnrator_sPaTL-mat21.2-0 ; 
       il_shdgnrator_sPaTL-mat22.2-0 ; 
       il_shdgnrator_sPaTL-mat23.2-0 ; 
       il_shdgnrator_sPaTL-mat24.2-0 ; 
       il_shdgnrator_sPaTL-mat25.2-0 ; 
       il_shdgnrator_sPaTL-mat26.2-0 ; 
       il_shdgnrator_sPaTL-mat27.2-0 ; 
       il_shdgnrator_sPaTL-mat28.2-0 ; 
       il_shdgnrator_sPaTL-mat3.2-0 ; 
       il_shdgnrator_sPaTL-mat30.2-0 ; 
       il_shdgnrator_sPaTL-mat31.2-0 ; 
       il_shdgnrator_sPaTL-mat32.2-0 ; 
       il_shdgnrator_sPaTL-mat33.2-0 ; 
       il_shdgnrator_sPaTL-mat34.2-0 ; 
       il_shdgnrator_sPaTL-mat35.2-0 ; 
       il_shdgnrator_sPaTL-mat38.2-0 ; 
       il_shdgnrator_sPaTL-mat39.2-0 ; 
       il_shdgnrator_sPaTL-mat40.2-0 ; 
       il_shdgnrator_sPaTL-mat41.2-0 ; 
       il_shdgnrator_sPaTL-mat42.2-0 ; 
       il_shdgnrator_sPaTL-mat43.2-0 ; 
       il_shdgnrator_sPaTL-mat44.2-0 ; 
       il_shdgnrator_sPaTL-mat45.2-0 ; 
       il_shdgnrator_sPaTL-mat46.2-0 ; 
       il_shdgnrator_sPaTL-mat47.2-0 ; 
       il_shdgnrator_sPaTL-mat48.2-0 ; 
       il_shdgnrator_sPaTL-mat49.2-0 ; 
       il_shdgnrator_sPaTL-mat50.2-0 ; 
       il_shdgnrator_sPaTL-mat51.2-0 ; 
       il_shdgnrator_sPaTL-mat6.2-0 ; 
       il_shdgnrator_sPaTL-mat7.2-0 ; 
       il_shdgnrator_sPaTL-mat8.2-0 ; 
       il_shdgnrator_sPaTL-mat9.2-0 ; 
       il_shieldgen_sPATL-mat88.2-0 ; 
       il_shieldgen_sPATL-mat91.2-0 ; 
       il_shieldgen_sPATL-mat92.2-0 ; 
       rix_fighter_sPAt-mat75.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       cap04-bturatt.1-0 ; 
       cap04-cap04.5-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-lwingzz3.1-0 ; 
       cap04-lwingzz4.1-0 ; 
       cap04-lwingzz5.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-rwingzz3.1-0 ; 
       cap04-rwingzz4.1-0 ; 
       cap04-rwingzz5.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turret.1-0 ; 
       cap04-XX.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-forzen_animation.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       forzen_animation-t2d46.1-0 ; 
       forzen_animation-t2d47.2-0 ; 
       il_shdgnrator_sPaTL-t2d1.2-0 ; 
       il_shdgnrator_sPaTL-t2d10.2-0 ; 
       il_shdgnrator_sPaTL-t2d11.2-0 ; 
       il_shdgnrator_sPaTL-t2d12.2-0 ; 
       il_shdgnrator_sPaTL-t2d13.2-0 ; 
       il_shdgnrator_sPaTL-t2d14.2-0 ; 
       il_shdgnrator_sPaTL-t2d15.2-0 ; 
       il_shdgnrator_sPaTL-t2d16.2-0 ; 
       il_shdgnrator_sPaTL-t2d17.2-0 ; 
       il_shdgnrator_sPaTL-t2d18.2-0 ; 
       il_shdgnrator_sPaTL-t2d19.2-0 ; 
       il_shdgnrator_sPaTL-t2d2.2-0 ; 
       il_shdgnrator_sPaTL-t2d20.2-0 ; 
       il_shdgnrator_sPaTL-t2d21.2-0 ; 
       il_shdgnrator_sPaTL-t2d22.2-0 ; 
       il_shdgnrator_sPaTL-t2d23.2-0 ; 
       il_shdgnrator_sPaTL-t2d25.2-0 ; 
       il_shdgnrator_sPaTL-t2d26.2-0 ; 
       il_shdgnrator_sPaTL-t2d27.2-0 ; 
       il_shdgnrator_sPaTL-t2d28.2-0 ; 
       il_shdgnrator_sPaTL-t2d29.2-0 ; 
       il_shdgnrator_sPaTL-t2d3.2-0 ; 
       il_shdgnrator_sPaTL-t2d30.2-0 ; 
       il_shdgnrator_sPaTL-t2d33.2-0 ; 
       il_shdgnrator_sPaTL-t2d34.2-0 ; 
       il_shdgnrator_sPaTL-t2d35.2-0 ; 
       il_shdgnrator_sPaTL-t2d36.2-0 ; 
       il_shdgnrator_sPaTL-t2d37.2-0 ; 
       il_shdgnrator_sPaTL-t2d38.2-0 ; 
       il_shdgnrator_sPaTL-t2d39.2-0 ; 
       il_shdgnrator_sPaTL-t2d4.2-0 ; 
       il_shdgnrator_sPaTL-t2d40.2-0 ; 
       il_shdgnrator_sPaTL-t2d41.2-0 ; 
       il_shdgnrator_sPaTL-t2d42.2-0 ; 
       il_shdgnrator_sPaTL-t2d43.2-0 ; 
       il_shdgnrator_sPaTL-t2d44.2-0 ; 
       il_shdgnrator_sPaTL-t2d45.2-0 ; 
       il_shdgnrator_sPaTL-t2d5.2-0 ; 
       il_shdgnrator_sPaTL-t2d6.2-0 ; 
       il_shdgnrator_sPaTL-t2d7.2-0 ; 
       il_shdgnrator_sPaTL-t2d8.2-0 ; 
       il_shdgnrator_sPaTL-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 8 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 14 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 3 110 ; 
       17 6 110 ; 
       18 9 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 2 110 ; 
       22 12 110 ; 
       23 15 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 2 110 ; 
       27 3 110 ; 
       29 3 110 ; 
       28 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 28 300 ; 
       3 51 300 ; 
       3 52 300 ; 
       3 53 300 ; 
       3 54 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 41 300 ; 
       3 42 300 ; 
       3 43 300 ; 
       3 44 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 10 300 ; 
       4 48 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       5 23 300 ; 
       5 31 300 ; 
       5 47 300 ; 
       6 24 300 ; 
       6 39 300 ; 
       7 25 300 ; 
       7 32 300 ; 
       8 27 300 ; 
       8 33 300 ; 
       9 26 300 ; 
       9 40 300 ; 
       10 30 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       11 16 300 ; 
       11 36 300 ; 
       12 17 300 ; 
       12 37 300 ; 
       13 18 300 ; 
       13 35 300 ; 
       14 20 300 ; 
       14 34 300 ; 
       15 19 300 ; 
       15 38 300 ; 
       16 57 300 ; 
       17 5 300 ; 
       18 9 300 ; 
       19 8 300 ; 
       20 3 300 ; 
       21 56 300 ; 
       22 6 300 ; 
       23 58 300 ; 
       24 7 300 ; 
       25 2 300 ; 
       26 55 300 ; 
       27 29 300 ; 
       29 0 300 ; 
       29 1 300 ; 
       28 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 32 401 ; 
       12 39 401 ; 
       13 40 401 ; 
       14 41 401 ; 
       15 42 401 ; 
       16 43 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       26 12 401 ; 
       27 14 401 ; 
       28 15 401 ; 
       29 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       36 24 401 ; 
       37 25 401 ; 
       38 26 401 ; 
       39 27 401 ; 
       40 28 401 ; 
       41 29 401 ; 
       42 30 401 ; 
       43 31 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       51 16 401 ; 
       52 2 401 ; 
       53 13 401 ; 
       54 23 401 ; 
       10 0 401 ; 
       4 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 20 -8 0 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 MPRFLG 0 ; 
       19 SCHEM 5 -10 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 35 -4 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 20 -10 0 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -3.587081 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 39 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
