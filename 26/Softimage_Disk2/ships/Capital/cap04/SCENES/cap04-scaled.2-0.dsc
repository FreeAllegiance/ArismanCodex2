SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_top_tur-mat100.2-0 ; 
       add_top_tur-mat101.2-0 ; 
       add_top_tur-mat102.2-0 ; 
       add_top_tur-mat131.2-0 ; 
       add_top_tur-mat94.2-0 ; 
       add_top_tur-mat95.2-0 ; 
       add_top_tur-mat96.2-0 ; 
       add_top_tur-mat97.2-0 ; 
       add_top_tur-mat98.2-0 ; 
       add_top_tur-mat99.2-0 ; 
       il_shdgnrator_sPaTL-mat10.4-0 ; 
       il_shdgnrator_sPaTL-mat11.4-0 ; 
       il_shdgnrator_sPaTL-mat12.4-0 ; 
       il_shdgnrator_sPaTL-mat13.4-0 ; 
       il_shdgnrator_sPaTL-mat14.4-0 ; 
       il_shdgnrator_sPaTL-mat15.4-0 ; 
       il_shdgnrator_sPaTL-mat16.4-0 ; 
       il_shdgnrator_sPaTL-mat20.4-0 ; 
       il_shdgnrator_sPaTL-mat21.4-0 ; 
       il_shdgnrator_sPaTL-mat22.4-0 ; 
       il_shdgnrator_sPaTL-mat23.4-0 ; 
       il_shdgnrator_sPaTL-mat27.4-0 ; 
       il_shdgnrator_sPaTL-mat28.4-0 ; 
       il_shdgnrator_sPaTL-mat3.4-0 ; 
       il_shdgnrator_sPaTL-mat30.4-0 ; 
       il_shdgnrator_sPaTL-mat35.4-0 ; 
       il_shdgnrator_sPaTL-mat38.4-0 ; 
       il_shdgnrator_sPaTL-mat40.4-0 ; 
       il_shdgnrator_sPaTL-mat42.4-0 ; 
       il_shdgnrator_sPaTL-mat43.4-0 ; 
       il_shdgnrator_sPaTL-mat44.4-0 ; 
       il_shdgnrator_sPaTL-mat45.4-0 ; 
       il_shdgnrator_sPaTL-mat46.4-0 ; 
       il_shdgnrator_sPaTL-mat47.4-0 ; 
       il_shdgnrator_sPaTL-mat48.4-0 ; 
       il_shdgnrator_sPaTL-mat49.4-0 ; 
       il_shdgnrator_sPaTL-mat50.4-0 ; 
       il_shdgnrator_sPaTL-mat51.4-0 ; 
       il_shdgnrator_sPaTL-mat6.4-0 ; 
       il_shdgnrator_sPaTL-mat7.4-0 ; 
       il_shdgnrator_sPaTL-mat8.4-0 ; 
       il_shdgnrator_sPaTL-mat9.4-0 ; 
       il_shieldgen_sPATL-mat88.4-0 ; 
       il_shieldgen_sPATL-mat91.4-0 ; 
       il_shieldgen_sPATL-mat92.4-0 ; 
       rix_fighter_sPAt-mat75.4-0 ; 
       scaled-mat132.1-0 ; 
       scaled-mat133.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cap04-175deg.2-0 ROOT ; 
       cap04-175deg_1.2-0 ROOT ; 
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-bturret.1-0 ; 
       cap04-cap04.20-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-cone3.2-0 ROOT ; 
       cap04-cone4.2-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-missemt.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-tturret1.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turwepemt1.1-0 ; 
       cap04-turwepemt2.1-0 ; 
       scaled-scale_cube.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_top_tur-t2d46.3-0 ; 
       add_top_tur-t2d47.3-0 ; 
       add_top_tur-t2d48.3-0 ; 
       il_shdgnrator_sPaTL-t2d1.5-0 ; 
       il_shdgnrator_sPaTL-t2d10.5-0 ; 
       il_shdgnrator_sPaTL-t2d14.5-0 ; 
       il_shdgnrator_sPaTL-t2d15.5-0 ; 
       il_shdgnrator_sPaTL-t2d16.5-0 ; 
       il_shdgnrator_sPaTL-t2d17.5-0 ; 
       il_shdgnrator_sPaTL-t2d2.5-0 ; 
       il_shdgnrator_sPaTL-t2d21.5-0 ; 
       il_shdgnrator_sPaTL-t2d22.5-0 ; 
       il_shdgnrator_sPaTL-t2d23.5-0 ; 
       il_shdgnrator_sPaTL-t2d25.5-0 ; 
       il_shdgnrator_sPaTL-t2d3.5-0 ; 
       il_shdgnrator_sPaTL-t2d30.5-0 ; 
       il_shdgnrator_sPaTL-t2d33.5-0 ; 
       il_shdgnrator_sPaTL-t2d35.5-0 ; 
       il_shdgnrator_sPaTL-t2d37.5-0 ; 
       il_shdgnrator_sPaTL-t2d38.5-0 ; 
       il_shdgnrator_sPaTL-t2d39.5-0 ; 
       il_shdgnrator_sPaTL-t2d4.5-0 ; 
       il_shdgnrator_sPaTL-t2d40.5-0 ; 
       il_shdgnrator_sPaTL-t2d41.5-0 ; 
       il_shdgnrator_sPaTL-t2d42.5-0 ; 
       il_shdgnrator_sPaTL-t2d43.5-0 ; 
       il_shdgnrator_sPaTL-t2d44.5-0 ; 
       il_shdgnrator_sPaTL-t2d45.5-0 ; 
       il_shdgnrator_sPaTL-t2d5.5-0 ; 
       il_shdgnrator_sPaTL-t2d6.5-0 ; 
       il_shdgnrator_sPaTL-t2d7.5-0 ; 
       il_shdgnrator_sPaTL-t2d8.5-0 ; 
       il_shdgnrator_sPaTL-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       3 6 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       7 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 6 110 ; 
       17 19 110 ; 
       18 17 110 ; 
       19 6 110 ; 
       20 19 110 ; 
       21 11 110 ; 
       22 15 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 10 110 ; 
       27 20 110 ; 
       28 17 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 10 110 ; 
       32 6 110 ; 
       33 6 110 ; 
       34 6 110 ; 
       35 11 110 ; 
       36 11 110 ; 
       37 5 110 ; 
       38 35 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 2 300 ; 
       8 47 300 ; 
       9 46 300 ; 
       10 21 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 28 300 ; 
       11 29 300 ; 
       11 30 300 ; 
       11 31 300 ; 
       11 32 300 ; 
       11 33 300 ; 
       11 9 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 37 300 ; 
       14 19 300 ; 
       14 24 300 ; 
       14 34 300 ; 
       15 20 300 ; 
       15 27 300 ; 
       17 23 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       19 15 300 ; 
       19 25 300 ; 
       20 16 300 ; 
       20 26 300 ; 
       21 44 300 ; 
       22 4 300 ; 
       23 8 300 ; 
       24 7 300 ; 
       25 1 300 ; 
       26 43 300 ; 
       27 5 300 ; 
       28 45 300 ; 
       29 6 300 ; 
       30 0 300 ; 
       31 42 300 ; 
       35 3 300 ; 
       36 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       9 0 401 ; 
       10 21 401 ; 
       11 28 401 ; 
       12 29 401 ; 
       13 30 401 ; 
       14 31 401 ; 
       15 32 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       24 13 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 11 401 ; 
       39 3 401 ; 
       40 9 401 ; 
       41 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 78.76324 -8.201202 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.908009 9.908009 9.908009 3.149999 3.576279e-007 0 1.83181e-013 -2.829492 7.82634 MPRFLG 0 ; 
       1 SCHEM 65.9541 -8.098251 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 9.908009 9.908009 9.908009 0.31 3.576279e-007 0 2.790121e-015 7.564643 -0.5852953 MPRFLG 0 ; 
       2 SCHEM 136.25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 133.75 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 76.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 80 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 75 0 0 DISPLAY 3 2 SRT 1 1 1 0 -1.570796 0 0 0 -0.7995199 MPRFLG 0 ; 
       7 SCHEM 143.75 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 62.06639 12.17136 0 USR SRT 4.247417 4.247417 4.247417 -1.570796 3.576279e-007 0 18.89404 -2.665352 17.48247 MPRFLG 0 ; 
       9 SCHEM 64.5664 12.17136 0 USR SRT 4.247417 4.247417 4.247417 -1.570796 3.576279e-007 0 -18.47418 -2.665352 17.48247 MPRFLG 0 ; 
       10 SCHEM 126.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 93.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 18.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 16.25 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 18.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 6.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 146.25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 51.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 48.75 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 50 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 38.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 73.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 3.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 18.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 11.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 13.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 123.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 36.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 51.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 43.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 46.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 126.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 141.25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 131.25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 138.75 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 67.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 71.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 78.75 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 66.25 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 68.06867 14.03725 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 1.19974 1.37113 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 147.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
