SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.11-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_top_tur-mat100.2-0 ; 
       add_top_tur-mat101.2-0 ; 
       add_top_tur-mat102.2-0 ; 
       add_top_tur-mat131.2-0 ; 
       add_top_tur-mat94.2-0 ; 
       add_top_tur-mat95.2-0 ; 
       add_top_tur-mat96.2-0 ; 
       add_top_tur-mat97.2-0 ; 
       add_top_tur-mat98.2-0 ; 
       add_top_tur-mat99.2-0 ; 
       il_shdgnrator_sPaTL-mat10.4-0 ; 
       il_shdgnrator_sPaTL-mat11.4-0 ; 
       il_shdgnrator_sPaTL-mat12.4-0 ; 
       il_shdgnrator_sPaTL-mat13.4-0 ; 
       il_shdgnrator_sPaTL-mat14.4-0 ; 
       il_shdgnrator_sPaTL-mat15.4-0 ; 
       il_shdgnrator_sPaTL-mat16.4-0 ; 
       il_shdgnrator_sPaTL-mat20.4-0 ; 
       il_shdgnrator_sPaTL-mat21.4-0 ; 
       il_shdgnrator_sPaTL-mat22.4-0 ; 
       il_shdgnrator_sPaTL-mat23.4-0 ; 
       il_shdgnrator_sPaTL-mat27.4-0 ; 
       il_shdgnrator_sPaTL-mat28.4-0 ; 
       il_shdgnrator_sPaTL-mat3.4-0 ; 
       il_shdgnrator_sPaTL-mat30.4-0 ; 
       il_shdgnrator_sPaTL-mat35.4-0 ; 
       il_shdgnrator_sPaTL-mat38.4-0 ; 
       il_shdgnrator_sPaTL-mat40.4-0 ; 
       il_shdgnrator_sPaTL-mat42.4-0 ; 
       il_shdgnrator_sPaTL-mat43.4-0 ; 
       il_shdgnrator_sPaTL-mat44.4-0 ; 
       il_shdgnrator_sPaTL-mat45.4-0 ; 
       il_shdgnrator_sPaTL-mat46.4-0 ; 
       il_shdgnrator_sPaTL-mat47.4-0 ; 
       il_shdgnrator_sPaTL-mat48.4-0 ; 
       il_shdgnrator_sPaTL-mat49.4-0 ; 
       il_shdgnrator_sPaTL-mat50.4-0 ; 
       il_shdgnrator_sPaTL-mat51.4-0 ; 
       il_shdgnrator_sPaTL-mat6.4-0 ; 
       il_shdgnrator_sPaTL-mat7.4-0 ; 
       il_shdgnrator_sPaTL-mat8.4-0 ; 
       il_shdgnrator_sPaTL-mat9.4-0 ; 
       il_shieldgen_sPATL-mat88.4-0 ; 
       il_shieldgen_sPATL-mat91.4-0 ; 
       il_shieldgen_sPATL-mat92.4-0 ; 
       rix_fighter_sPAt-mat75.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-bturret.1-0 ; 
       cap04-cap04.26-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-missemt.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-tturret1.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turwepemt1.1-0 ; 
       cap04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-scaled.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_top_tur-t2d46.5-0 ; 
       add_top_tur-t2d47.5-0 ; 
       add_top_tur-t2d48.5-0 ; 
       il_shdgnrator_sPaTL-t2d1.7-0 ; 
       il_shdgnrator_sPaTL-t2d10.7-0 ; 
       il_shdgnrator_sPaTL-t2d14.7-0 ; 
       il_shdgnrator_sPaTL-t2d15.7-0 ; 
       il_shdgnrator_sPaTL-t2d16.7-0 ; 
       il_shdgnrator_sPaTL-t2d17.7-0 ; 
       il_shdgnrator_sPaTL-t2d2.7-0 ; 
       il_shdgnrator_sPaTL-t2d21.7-0 ; 
       il_shdgnrator_sPaTL-t2d22.7-0 ; 
       il_shdgnrator_sPaTL-t2d23.7-0 ; 
       il_shdgnrator_sPaTL-t2d25.7-0 ; 
       il_shdgnrator_sPaTL-t2d3.7-0 ; 
       il_shdgnrator_sPaTL-t2d30.7-0 ; 
       il_shdgnrator_sPaTL-t2d33.7-0 ; 
       il_shdgnrator_sPaTL-t2d35.7-0 ; 
       il_shdgnrator_sPaTL-t2d37.7-0 ; 
       il_shdgnrator_sPaTL-t2d38.7-0 ; 
       il_shdgnrator_sPaTL-t2d39.7-0 ; 
       il_shdgnrator_sPaTL-t2d4.7-0 ; 
       il_shdgnrator_sPaTL-t2d40.7-0 ; 
       il_shdgnrator_sPaTL-t2d41.7-0 ; 
       il_shdgnrator_sPaTL-t2d42.7-0 ; 
       il_shdgnrator_sPaTL-t2d43.7-0 ; 
       il_shdgnrator_sPaTL-t2d44.7-0 ; 
       il_shdgnrator_sPaTL-t2d45.7-0 ; 
       il_shdgnrator_sPaTL-t2d5.7-0 ; 
       il_shdgnrator_sPaTL-t2d6.7-0 ; 
       il_shdgnrator_sPaTL-t2d7.7-0 ; 
       il_shdgnrator_sPaTL-t2d8.7-0 ; 
       il_shdgnrator_sPaTL-t2d9.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 10 110 ; 
       9 8 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       12 4 110 ; 
       13 15 110 ; 
       14 13 110 ; 
       15 4 110 ; 
       16 15 110 ; 
       17 7 110 ; 
       18 11 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 6 110 ; 
       23 16 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 6 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 7 110 ; 
       32 7 110 ; 
       33 3 110 ; 
       34 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       6 21 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 9 300 ; 
       8 35 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       10 19 300 ; 
       10 24 300 ; 
       10 34 300 ; 
       11 20 300 ; 
       11 27 300 ; 
       13 23 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       15 15 300 ; 
       15 25 300 ; 
       16 16 300 ; 
       16 26 300 ; 
       17 44 300 ; 
       18 4 300 ; 
       19 8 300 ; 
       20 7 300 ; 
       21 1 300 ; 
       22 43 300 ; 
       23 5 300 ; 
       24 45 300 ; 
       25 6 300 ; 
       26 0 300 ; 
       27 42 300 ; 
       31 3 300 ; 
       32 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       9 0 401 ; 
       10 21 401 ; 
       11 28 401 ; 
       12 29 401 ; 
       13 30 401 ; 
       14 31 401 ; 
       15 32 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       24 13 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 11 401 ; 
       39 3 401 ; 
       40 9 401 ; 
       41 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 31.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 41.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 21.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 42.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 55 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 52.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 37.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 27.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
