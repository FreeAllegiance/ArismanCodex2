SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.18-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       add_top_tur-mat99.1-0 ; 
       il_shdgnrator_sPaTL-mat10.3-0 ; 
       il_shdgnrator_sPaTL-mat11.3-0 ; 
       il_shdgnrator_sPaTL-mat12.3-0 ; 
       il_shdgnrator_sPaTL-mat13.3-0 ; 
       il_shdgnrator_sPaTL-mat14.3-0 ; 
       il_shdgnrator_sPaTL-mat15.3-0 ; 
       il_shdgnrator_sPaTL-mat16.3-0 ; 
       il_shdgnrator_sPaTL-mat22.3-0 ; 
       il_shdgnrator_sPaTL-mat23.3-0 ; 
       il_shdgnrator_sPaTL-mat27.3-0 ; 
       il_shdgnrator_sPaTL-mat30.3-0 ; 
       il_shdgnrator_sPaTL-mat35.3-0 ; 
       il_shdgnrator_sPaTL-mat38.3-0 ; 
       il_shdgnrator_sPaTL-mat40.3-0 ; 
       il_shdgnrator_sPaTL-mat42.3-0 ; 
       il_shdgnrator_sPaTL-mat43.3-0 ; 
       il_shdgnrator_sPaTL-mat44.3-0 ; 
       il_shdgnrator_sPaTL-mat45.3-0 ; 
       il_shdgnrator_sPaTL-mat46.3-0 ; 
       il_shdgnrator_sPaTL-mat47.3-0 ; 
       il_shdgnrator_sPaTL-mat48.3-0 ; 
       il_shdgnrator_sPaTL-mat6.3-0 ; 
       il_shdgnrator_sPaTL-mat7.3-0 ; 
       il_shdgnrator_sPaTL-mat8.3-0 ; 
       il_shdgnrator_sPaTL-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       cap04-cap04.15-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_top_tur-t2d46.1-0 ; 
       il_shdgnrator_sPaTL-t2d1.3-0 ; 
       il_shdgnrator_sPaTL-t2d10.3-0 ; 
       il_shdgnrator_sPaTL-t2d16.3-0 ; 
       il_shdgnrator_sPaTL-t2d17.3-0 ; 
       il_shdgnrator_sPaTL-t2d2.3-0 ; 
       il_shdgnrator_sPaTL-t2d21.3-0 ; 
       il_shdgnrator_sPaTL-t2d22.3-0 ; 
       il_shdgnrator_sPaTL-t2d25.3-0 ; 
       il_shdgnrator_sPaTL-t2d3.3-0 ; 
       il_shdgnrator_sPaTL-t2d30.3-0 ; 
       il_shdgnrator_sPaTL-t2d33.3-0 ; 
       il_shdgnrator_sPaTL-t2d35.3-0 ; 
       il_shdgnrator_sPaTL-t2d37.3-0 ; 
       il_shdgnrator_sPaTL-t2d38.3-0 ; 
       il_shdgnrator_sPaTL-t2d39.3-0 ; 
       il_shdgnrator_sPaTL-t2d4.3-0 ; 
       il_shdgnrator_sPaTL-t2d40.3-0 ; 
       il_shdgnrator_sPaTL-t2d41.3-0 ; 
       il_shdgnrator_sPaTL-t2d42.3-0 ; 
       il_shdgnrator_sPaTL-t2d43.3-0 ; 
       il_shdgnrator_sPaTL-t2d5.3-0 ; 
       il_shdgnrator_sPaTL-t2d6.3-0 ; 
       il_shdgnrator_sPaTL-t2d7.3-0 ; 
       il_shdgnrator_sPaTL-t2d8.3-0 ; 
       il_shdgnrator_sPaTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 10 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 0 300 ; 
       3 8 300 ; 
       3 11 300 ; 
       3 21 300 ; 
       4 9 300 ; 
       4 14 300 ; 
       5 6 300 ; 
       5 12 300 ; 
       6 7 300 ; 
       6 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 16 401 ; 
       2 21 401 ; 
       3 22 401 ; 
       4 23 401 ; 
       5 24 401 ; 
       6 25 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 6 401 ; 
       11 8 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 7 401 ; 
       23 1 401 ; 
       24 5 401 ; 
       25 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 29 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
