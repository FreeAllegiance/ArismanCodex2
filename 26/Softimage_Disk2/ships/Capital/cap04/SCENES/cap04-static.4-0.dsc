SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       add_top_tur-mat99.2-0 ; 
       il_shdgnrator_sPaTL-mat10.4-0 ; 
       il_shdgnrator_sPaTL-mat11.4-0 ; 
       il_shdgnrator_sPaTL-mat12.4-0 ; 
       il_shdgnrator_sPaTL-mat13.4-0 ; 
       il_shdgnrator_sPaTL-mat14.4-0 ; 
       il_shdgnrator_sPaTL-mat15.4-0 ; 
       il_shdgnrator_sPaTL-mat16.4-0 ; 
       il_shdgnrator_sPaTL-mat20.4-0 ; 
       il_shdgnrator_sPaTL-mat21.4-0 ; 
       il_shdgnrator_sPaTL-mat22.4-0 ; 
       il_shdgnrator_sPaTL-mat23.4-0 ; 
       il_shdgnrator_sPaTL-mat27.4-0 ; 
       il_shdgnrator_sPaTL-mat3.4-0 ; 
       il_shdgnrator_sPaTL-mat30.4-0 ; 
       il_shdgnrator_sPaTL-mat35.4-0 ; 
       il_shdgnrator_sPaTL-mat38.4-0 ; 
       il_shdgnrator_sPaTL-mat40.4-0 ; 
       il_shdgnrator_sPaTL-mat42.4-0 ; 
       il_shdgnrator_sPaTL-mat43.4-0 ; 
       il_shdgnrator_sPaTL-mat44.4-0 ; 
       il_shdgnrator_sPaTL-mat45.4-0 ; 
       il_shdgnrator_sPaTL-mat46.4-0 ; 
       il_shdgnrator_sPaTL-mat47.4-0 ; 
       il_shdgnrator_sPaTL-mat48.4-0 ; 
       il_shdgnrator_sPaTL-mat49.4-0 ; 
       il_shdgnrator_sPaTL-mat50.4-0 ; 
       il_shdgnrator_sPaTL-mat51.4-0 ; 
       il_shdgnrator_sPaTL-mat6.4-0 ; 
       il_shdgnrator_sPaTL-mat7.4-0 ; 
       il_shdgnrator_sPaTL-mat8.4-0 ; 
       il_shdgnrator_sPaTL-mat9.4-0 ; 
       il_shieldgen_sPATL-mat88.4-0 ; 
       il_shieldgen_sPATL-mat91.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       cap04-cap04.19-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSra.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_top_tur-t2d46.3-0 ; 
       il_shdgnrator_sPaTL-t2d1.5-0 ; 
       il_shdgnrator_sPaTL-t2d10.5-0 ; 
       il_shdgnrator_sPaTL-t2d14.5-0 ; 
       il_shdgnrator_sPaTL-t2d15.5-0 ; 
       il_shdgnrator_sPaTL-t2d16.5-0 ; 
       il_shdgnrator_sPaTL-t2d17.5-0 ; 
       il_shdgnrator_sPaTL-t2d2.5-0 ; 
       il_shdgnrator_sPaTL-t2d21.5-0 ; 
       il_shdgnrator_sPaTL-t2d22.5-0 ; 
       il_shdgnrator_sPaTL-t2d25.5-0 ; 
       il_shdgnrator_sPaTL-t2d3.5-0 ; 
       il_shdgnrator_sPaTL-t2d30.5-0 ; 
       il_shdgnrator_sPaTL-t2d33.5-0 ; 
       il_shdgnrator_sPaTL-t2d35.5-0 ; 
       il_shdgnrator_sPaTL-t2d37.5-0 ; 
       il_shdgnrator_sPaTL-t2d38.5-0 ; 
       il_shdgnrator_sPaTL-t2d39.5-0 ; 
       il_shdgnrator_sPaTL-t2d4.5-0 ; 
       il_shdgnrator_sPaTL-t2d40.5-0 ; 
       il_shdgnrator_sPaTL-t2d41.5-0 ; 
       il_shdgnrator_sPaTL-t2d42.5-0 ; 
       il_shdgnrator_sPaTL-t2d43.5-0 ; 
       il_shdgnrator_sPaTL-t2d44.5-0 ; 
       il_shdgnrator_sPaTL-t2d45.5-0 ; 
       il_shdgnrator_sPaTL-t2d5.5-0 ; 
       il_shdgnrator_sPaTL-t2d6.5-0 ; 
       il_shdgnrator_sPaTL-t2d7.5-0 ; 
       il_shdgnrator_sPaTL-t2d8.5-0 ; 
       il_shdgnrator_sPaTL-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 1 110 ; 
       10 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 12 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 0 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       4 10 300 ; 
       4 14 300 ; 
       4 24 300 ; 
       5 11 300 ; 
       5 17 300 ; 
       6 13 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       7 6 300 ; 
       7 15 300 ; 
       8 7 300 ; 
       8 16 300 ; 
       9 33 300 ; 
       10 32 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 18 401 ; 
       2 25 401 ; 
       3 26 401 ; 
       4 27 401 ; 
       5 28 401 ; 
       6 29 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 8 401 ; 
       14 10 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 9 401 ; 
       29 1 401 ; 
       30 7 401 ; 
       31 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 0 0 SRT 1 1 1 0 -1.570796 0 0 0 -0.7995199 MPRFLG 0 ; 
       1 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 80 -4 0 MPRFLG 0 ; 
       10 SCHEM 82.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
