SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.8-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_top_tur-mat100.2-0 ; 
       add_top_tur-mat101.2-0 ; 
       add_top_tur-mat102.2-0 ; 
       add_top_tur-mat131.2-0 ; 
       add_top_tur-mat94.2-0 ; 
       add_top_tur-mat95.2-0 ; 
       add_top_tur-mat96.2-0 ; 
       add_top_tur-mat97.2-0 ; 
       add_top_tur-mat98.2-0 ; 
       add_top_tur-mat99.2-0 ; 
       il_shdgnrator_sPaTL-mat10.4-0 ; 
       il_shdgnrator_sPaTL-mat11.4-0 ; 
       il_shdgnrator_sPaTL-mat12.4-0 ; 
       il_shdgnrator_sPaTL-mat13.4-0 ; 
       il_shdgnrator_sPaTL-mat14.4-0 ; 
       il_shdgnrator_sPaTL-mat15.4-0 ; 
       il_shdgnrator_sPaTL-mat16.4-0 ; 
       il_shdgnrator_sPaTL-mat20.4-0 ; 
       il_shdgnrator_sPaTL-mat21.4-0 ; 
       il_shdgnrator_sPaTL-mat22.4-0 ; 
       il_shdgnrator_sPaTL-mat23.4-0 ; 
       il_shdgnrator_sPaTL-mat27.4-0 ; 
       il_shdgnrator_sPaTL-mat28.4-0 ; 
       il_shdgnrator_sPaTL-mat3.4-0 ; 
       il_shdgnrator_sPaTL-mat30.4-0 ; 
       il_shdgnrator_sPaTL-mat35.4-0 ; 
       il_shdgnrator_sPaTL-mat38.4-0 ; 
       il_shdgnrator_sPaTL-mat40.4-0 ; 
       il_shdgnrator_sPaTL-mat42.4-0 ; 
       il_shdgnrator_sPaTL-mat43.4-0 ; 
       il_shdgnrator_sPaTL-mat44.4-0 ; 
       il_shdgnrator_sPaTL-mat45.4-0 ; 
       il_shdgnrator_sPaTL-mat46.4-0 ; 
       il_shdgnrator_sPaTL-mat47.4-0 ; 
       il_shdgnrator_sPaTL-mat48.4-0 ; 
       il_shdgnrator_sPaTL-mat49.4-0 ; 
       il_shdgnrator_sPaTL-mat50.4-0 ; 
       il_shdgnrator_sPaTL-mat51.4-0 ; 
       il_shdgnrator_sPaTL-mat6.4-0 ; 
       il_shdgnrator_sPaTL-mat7.4-0 ; 
       il_shdgnrator_sPaTL-mat8.4-0 ; 
       il_shdgnrator_sPaTL-mat9.4-0 ; 
       il_shieldgen_sPATL-mat88.4-0 ; 
       il_shieldgen_sPATL-mat91.4-0 ; 
       il_shieldgen_sPATL-mat92.4-0 ; 
       rix_fighter_sPAt-mat75.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       cap04-bturatt.1-0 ; 
       cap04-bturret.1-0 ; 
       cap04-cap04.24-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tturret1.1-0 ; 
       cap04-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_top_tur-t2d46.6-0 ; 
       add_top_tur-t2d47.6-0 ; 
       add_top_tur-t2d48.6-0 ; 
       il_shdgnrator_sPaTL-t2d1.8-0 ; 
       il_shdgnrator_sPaTL-t2d10.8-0 ; 
       il_shdgnrator_sPaTL-t2d14.8-0 ; 
       il_shdgnrator_sPaTL-t2d15.8-0 ; 
       il_shdgnrator_sPaTL-t2d16.8-0 ; 
       il_shdgnrator_sPaTL-t2d17.8-0 ; 
       il_shdgnrator_sPaTL-t2d2.8-0 ; 
       il_shdgnrator_sPaTL-t2d21.8-0 ; 
       il_shdgnrator_sPaTL-t2d22.8-0 ; 
       il_shdgnrator_sPaTL-t2d23.8-0 ; 
       il_shdgnrator_sPaTL-t2d25.8-0 ; 
       il_shdgnrator_sPaTL-t2d3.8-0 ; 
       il_shdgnrator_sPaTL-t2d30.8-0 ; 
       il_shdgnrator_sPaTL-t2d33.8-0 ; 
       il_shdgnrator_sPaTL-t2d35.8-0 ; 
       il_shdgnrator_sPaTL-t2d37.8-0 ; 
       il_shdgnrator_sPaTL-t2d38.8-0 ; 
       il_shdgnrator_sPaTL-t2d39.8-0 ; 
       il_shdgnrator_sPaTL-t2d4.8-0 ; 
       il_shdgnrator_sPaTL-t2d40.8-0 ; 
       il_shdgnrator_sPaTL-t2d41.8-0 ; 
       il_shdgnrator_sPaTL-t2d42.8-0 ; 
       il_shdgnrator_sPaTL-t2d43.8-0 ; 
       il_shdgnrator_sPaTL-t2d44.8-0 ; 
       il_shdgnrator_sPaTL-t2d45.8-0 ; 
       il_shdgnrator_sPaTL-t2d5.8-0 ; 
       il_shdgnrator_sPaTL-t2d6.8-0 ; 
       il_shdgnrator_sPaTL-t2d7.8-0 ; 
       il_shdgnrator_sPaTL-t2d8.8-0 ; 
       il_shdgnrator_sPaTL-t2d9.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 6 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       11 4 110 ; 
       12 7 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 3 110 ; 
       17 10 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 3 110 ; 
       22 4 110 ; 
       23 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       3 21 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       4 41 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 9 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       6 19 300 ; 
       6 24 300 ; 
       6 34 300 ; 
       7 20 300 ; 
       7 27 300 ; 
       8 23 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       9 15 300 ; 
       9 25 300 ; 
       10 16 300 ; 
       10 26 300 ; 
       11 44 300 ; 
       12 4 300 ; 
       13 8 300 ; 
       14 7 300 ; 
       15 1 300 ; 
       16 43 300 ; 
       17 5 300 ; 
       18 45 300 ; 
       19 6 300 ; 
       20 0 300 ; 
       21 42 300 ; 
       22 3 300 ; 
       23 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       9 0 401 ; 
       10 21 401 ; 
       11 28 401 ; 
       12 29 401 ; 
       13 30 401 ; 
       14 31 401 ; 
       15 32 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       24 13 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 11 401 ; 
       39 3 401 ; 
       40 9 401 ; 
       41 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 31.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 21.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 40 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 42.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
