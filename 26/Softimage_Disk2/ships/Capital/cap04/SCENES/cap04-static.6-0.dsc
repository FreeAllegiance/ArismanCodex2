SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.10-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       add_top_tur-mat102_1.1-0 ; 
       add_top_tur-mat131_1.1-0 ; 
       add_top_tur-mat99_1.1-0 ; 
       il_shdgnrator_sPaTL-mat10_1.1-0 ; 
       il_shdgnrator_sPaTL-mat11_1.1-0 ; 
       il_shdgnrator_sPaTL-mat12_1.1-0 ; 
       il_shdgnrator_sPaTL-mat13_1.1-0 ; 
       il_shdgnrator_sPaTL-mat14_1.1-0 ; 
       il_shdgnrator_sPaTL-mat15_1.1-0 ; 
       il_shdgnrator_sPaTL-mat16_1.1-0 ; 
       il_shdgnrator_sPaTL-mat20_1.1-0 ; 
       il_shdgnrator_sPaTL-mat21_1.1-0 ; 
       il_shdgnrator_sPaTL-mat22_1.1-0 ; 
       il_shdgnrator_sPaTL-mat23_1.1-0 ; 
       il_shdgnrator_sPaTL-mat27_1.1-0 ; 
       il_shdgnrator_sPaTL-mat3_1.1-0 ; 
       il_shdgnrator_sPaTL-mat30_1.1-0 ; 
       il_shdgnrator_sPaTL-mat35_1.1-0 ; 
       il_shdgnrator_sPaTL-mat38_1.1-0 ; 
       il_shdgnrator_sPaTL-mat40_1.1-0 ; 
       il_shdgnrator_sPaTL-mat42_1.1-0 ; 
       il_shdgnrator_sPaTL-mat43_1.1-0 ; 
       il_shdgnrator_sPaTL-mat44_1.1-0 ; 
       il_shdgnrator_sPaTL-mat45_1.1-0 ; 
       il_shdgnrator_sPaTL-mat46_1.1-0 ; 
       il_shdgnrator_sPaTL-mat47_1.1-0 ; 
       il_shdgnrator_sPaTL-mat48_1.1-0 ; 
       il_shdgnrator_sPaTL-mat49_1.1-0 ; 
       il_shdgnrator_sPaTL-mat50_1.1-0 ; 
       il_shdgnrator_sPaTL-mat51_1.1-0 ; 
       il_shdgnrator_sPaTL-mat6_1.1-0 ; 
       il_shdgnrator_sPaTL-mat7_1.1-0 ; 
       il_shdgnrator_sPaTL-mat8_1.1-0 ; 
       il_shdgnrator_sPaTL-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       cap04-bturret_1.1-0 ; 
       cap04-cap04_1.2-0 ROOT ; 
       cap04-finzzz_1.1-0 ; 
       cap04-fuselage_1.1-0 ; 
       cap04-lantenn_1.1-0 ; 
       cap04-lwingzz1_1.1-0 ; 
       cap04-lwingzz2_1.1-0 ; 
       cap04-rantenn_1.1-0 ; 
       cap04-rwingzz1_1.1-0 ; 
       cap04-rwingzz2_1.1-0 ; 
       cap04-tturret1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       add_top_tur-t2d46_1.1-0 ; 
       add_top_tur-t2d47_1.1-0 ; 
       add_top_tur-t2d48_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d1_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d10_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d14_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d15_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d16_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d17_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d2_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d21_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d22_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d25_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d3_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d30_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d33_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d35_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d37_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d38_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d39_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d4_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d40_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d41_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d42_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d43_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d44_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d45_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d5_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d6_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d7_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d8_1.1-0 ; 
       il_shdgnrator_sPaTL-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 5 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 8 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       2 14 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 2 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       5 12 300 ; 
       5 16 300 ; 
       5 26 300 ; 
       6 13 300 ; 
       6 19 300 ; 
       7 15 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 8 300 ; 
       8 17 300 ; 
       9 9 300 ; 
       9 18 300 ; 
       10 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 0 401 ; 
       3 20 401 ; 
       4 27 401 ; 
       5 28 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 10 401 ; 
       16 12 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 11 401 ; 
       31 3 401 ; 
       32 9 401 ; 
       33 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 99.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 93.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 103.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 94.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 70.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 69.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 64.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 83.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 82 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 77 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 89.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 88.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 78.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 66 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 106 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 88.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 78.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 76 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 76 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 101 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 78.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 88.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 88.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 66 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 106 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 88.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 78.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 66 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 76 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 76 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 101 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 88.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
