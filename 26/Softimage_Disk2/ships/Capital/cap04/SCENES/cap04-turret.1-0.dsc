SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.2-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       il_shdgnrator_sPaTL-mat10.2-0 ; 
       il_shdgnrator_sPaTL-mat11.2-0 ; 
       il_shdgnrator_sPaTL-mat12.2-0 ; 
       il_shdgnrator_sPaTL-mat13.2-0 ; 
       il_shdgnrator_sPaTL-mat14.2-0 ; 
       il_shdgnrator_sPaTL-mat15.2-0 ; 
       il_shdgnrator_sPaTL-mat16.2-0 ; 
       il_shdgnrator_sPaTL-mat17.2-0 ; 
       il_shdgnrator_sPaTL-mat18.2-0 ; 
       il_shdgnrator_sPaTL-mat19.2-0 ; 
       il_shdgnrator_sPaTL-mat20.2-0 ; 
       il_shdgnrator_sPaTL-mat21.2-0 ; 
       il_shdgnrator_sPaTL-mat22.2-0 ; 
       il_shdgnrator_sPaTL-mat23.2-0 ; 
       il_shdgnrator_sPaTL-mat24.2-0 ; 
       il_shdgnrator_sPaTL-mat25.2-0 ; 
       il_shdgnrator_sPaTL-mat26.2-0 ; 
       il_shdgnrator_sPaTL-mat27.2-0 ; 
       il_shdgnrator_sPaTL-mat28.2-0 ; 
       il_shdgnrator_sPaTL-mat3.2-0 ; 
       il_shdgnrator_sPaTL-mat30.2-0 ; 
       il_shdgnrator_sPaTL-mat31.2-0 ; 
       il_shdgnrator_sPaTL-mat32.2-0 ; 
       il_shdgnrator_sPaTL-mat33.2-0 ; 
       il_shdgnrator_sPaTL-mat34.2-0 ; 
       il_shdgnrator_sPaTL-mat35.2-0 ; 
       il_shdgnrator_sPaTL-mat38.2-0 ; 
       il_shdgnrator_sPaTL-mat39.2-0 ; 
       il_shdgnrator_sPaTL-mat40.2-0 ; 
       il_shdgnrator_sPaTL-mat41.2-0 ; 
       il_shdgnrator_sPaTL-mat42.2-0 ; 
       il_shdgnrator_sPaTL-mat43.2-0 ; 
       il_shdgnrator_sPaTL-mat44.2-0 ; 
       il_shdgnrator_sPaTL-mat45.2-0 ; 
       il_shdgnrator_sPaTL-mat46.2-0 ; 
       il_shdgnrator_sPaTL-mat47.2-0 ; 
       il_shdgnrator_sPaTL-mat48.2-0 ; 
       il_shdgnrator_sPaTL-mat49.2-0 ; 
       il_shdgnrator_sPaTL-mat50.2-0 ; 
       il_shdgnrator_sPaTL-mat51.2-0 ; 
       il_shdgnrator_sPaTL-mat6.2-0 ; 
       il_shdgnrator_sPaTL-mat7.2-0 ; 
       il_shdgnrator_sPaTL-mat8.2-0 ; 
       il_shdgnrator_sPaTL-mat9.2-0 ; 
       il_shieldgen_sPATL-mat88.2-0 ; 
       il_shieldgen_sPATL-mat91.2-0 ; 
       il_shieldgen_sPATL-mat92.2-0 ; 
       rix_fighter_sPAt-mat75.2-0 ; 
       turret-default1.1-0 ; 
       turret-default2.1-0 ; 
       turret-mat100.1-0 ; 
       turret-mat101.1-0 ; 
       turret-mat94.1-0 ; 
       turret-mat95.1-0 ; 
       turret-mat96.1-0 ; 
       turret-mat97.1-0 ; 
       turret-mat98.1-0 ; 
       turret-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       cap04-bturatt.1-0 ; 
       cap04-cap04.2-0 ROOT ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-lwingzz3.1-0 ; 
       cap04-lwingzz4.1-0 ; 
       cap04-lwingzz5.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-rwingzz3.1-0 ; 
       cap04-rwingzz4.1-0 ; 
       cap04-rwingzz5.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turret.1-0 ; 
       cap04-XX.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       il_shdgnrator_sPaTL-t2d1.2-0 ; 
       il_shdgnrator_sPaTL-t2d10.2-0 ; 
       il_shdgnrator_sPaTL-t2d11.2-0 ; 
       il_shdgnrator_sPaTL-t2d12.2-0 ; 
       il_shdgnrator_sPaTL-t2d13.2-0 ; 
       il_shdgnrator_sPaTL-t2d14.2-0 ; 
       il_shdgnrator_sPaTL-t2d15.2-0 ; 
       il_shdgnrator_sPaTL-t2d16.2-0 ; 
       il_shdgnrator_sPaTL-t2d17.2-0 ; 
       il_shdgnrator_sPaTL-t2d18.2-0 ; 
       il_shdgnrator_sPaTL-t2d19.2-0 ; 
       il_shdgnrator_sPaTL-t2d2.2-0 ; 
       il_shdgnrator_sPaTL-t2d20.2-0 ; 
       il_shdgnrator_sPaTL-t2d21.2-0 ; 
       il_shdgnrator_sPaTL-t2d22.2-0 ; 
       il_shdgnrator_sPaTL-t2d23.2-0 ; 
       il_shdgnrator_sPaTL-t2d25.2-0 ; 
       il_shdgnrator_sPaTL-t2d26.2-0 ; 
       il_shdgnrator_sPaTL-t2d27.2-0 ; 
       il_shdgnrator_sPaTL-t2d28.2-0 ; 
       il_shdgnrator_sPaTL-t2d29.2-0 ; 
       il_shdgnrator_sPaTL-t2d3.2-0 ; 
       il_shdgnrator_sPaTL-t2d30.2-0 ; 
       il_shdgnrator_sPaTL-t2d33.2-0 ; 
       il_shdgnrator_sPaTL-t2d34.2-0 ; 
       il_shdgnrator_sPaTL-t2d35.2-0 ; 
       il_shdgnrator_sPaTL-t2d36.2-0 ; 
       il_shdgnrator_sPaTL-t2d37.2-0 ; 
       il_shdgnrator_sPaTL-t2d38.2-0 ; 
       il_shdgnrator_sPaTL-t2d39.2-0 ; 
       il_shdgnrator_sPaTL-t2d4.2-0 ; 
       il_shdgnrator_sPaTL-t2d40.2-0 ; 
       il_shdgnrator_sPaTL-t2d41.2-0 ; 
       il_shdgnrator_sPaTL-t2d42.2-0 ; 
       il_shdgnrator_sPaTL-t2d43.2-0 ; 
       il_shdgnrator_sPaTL-t2d44.2-0 ; 
       il_shdgnrator_sPaTL-t2d45.2-0 ; 
       il_shdgnrator_sPaTL-t2d5.2-0 ; 
       il_shdgnrator_sPaTL-t2d6.2-0 ; 
       il_shdgnrator_sPaTL-t2d7.2-0 ; 
       il_shdgnrator_sPaTL-t2d8.2-0 ; 
       il_shdgnrator_sPaTL-t2d9.2-0 ; 
       turret-t2d46.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 8 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 14 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 3 110 ; 
       17 6 110 ; 
       18 9 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 2 110 ; 
       22 12 110 ; 
       23 15 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 2 110 ; 
       27 3 110 ; 
       29 3 110 ; 
       28 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 17 300 ; 
       3 40 300 ; 
       3 41 300 ; 
       3 42 300 ; 
       3 43 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 57 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       5 12 300 ; 
       5 20 300 ; 
       5 36 300 ; 
       6 13 300 ; 
       6 28 300 ; 
       7 14 300 ; 
       7 21 300 ; 
       8 16 300 ; 
       8 22 300 ; 
       9 15 300 ; 
       9 29 300 ; 
       10 19 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       11 5 300 ; 
       11 25 300 ; 
       12 6 300 ; 
       12 26 300 ; 
       13 7 300 ; 
       13 24 300 ; 
       14 9 300 ; 
       14 23 300 ; 
       15 8 300 ; 
       15 27 300 ; 
       16 46 300 ; 
       17 52 300 ; 
       18 56 300 ; 
       19 55 300 ; 
       20 51 300 ; 
       21 45 300 ; 
       22 53 300 ; 
       23 47 300 ; 
       24 54 300 ; 
       25 50 300 ; 
       26 44 300 ; 
       27 18 300 ; 
       29 48 300 ; 
       29 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 30 401 ; 
       1 37 401 ; 
       2 38 401 ; 
       3 39 401 ; 
       4 40 401 ; 
       5 41 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       38 35 401 ; 
       39 36 401 ; 
       40 14 401 ; 
       41 0 401 ; 
       42 11 401 ; 
       43 21 401 ; 
       57 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 20 -8 0 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 MPRFLG 0 ; 
       19 SCHEM 5 -10 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 35 -4 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 20 -10 0 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 39 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
