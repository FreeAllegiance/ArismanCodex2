SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap04-cap04.17-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.19-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       add_top_tur-mat100.1-0 ; 
       add_top_tur-mat101.1-0 ; 
       add_top_tur-mat102.1-0 ; 
       add_top_tur-mat131.1-0 ; 
       add_top_tur-mat94.1-0 ; 
       add_top_tur-mat95.1-0 ; 
       add_top_tur-mat96.1-0 ; 
       add_top_tur-mat97.1-0 ; 
       add_top_tur-mat98.1-0 ; 
       add_top_tur-mat99.1-0 ; 
       il_shdgnrator_sPaTL-mat10.3-0 ; 
       il_shdgnrator_sPaTL-mat11.3-0 ; 
       il_shdgnrator_sPaTL-mat12.3-0 ; 
       il_shdgnrator_sPaTL-mat13.3-0 ; 
       il_shdgnrator_sPaTL-mat14.3-0 ; 
       il_shdgnrator_sPaTL-mat15.3-0 ; 
       il_shdgnrator_sPaTL-mat16.3-0 ; 
       il_shdgnrator_sPaTL-mat20.3-0 ; 
       il_shdgnrator_sPaTL-mat21.3-0 ; 
       il_shdgnrator_sPaTL-mat22.3-0 ; 
       il_shdgnrator_sPaTL-mat23.3-0 ; 
       il_shdgnrator_sPaTL-mat27.3-0 ; 
       il_shdgnrator_sPaTL-mat28.3-0 ; 
       il_shdgnrator_sPaTL-mat3.3-0 ; 
       il_shdgnrator_sPaTL-mat30.3-0 ; 
       il_shdgnrator_sPaTL-mat35.3-0 ; 
       il_shdgnrator_sPaTL-mat38.3-0 ; 
       il_shdgnrator_sPaTL-mat40.3-0 ; 
       il_shdgnrator_sPaTL-mat42.3-0 ; 
       il_shdgnrator_sPaTL-mat43.3-0 ; 
       il_shdgnrator_sPaTL-mat44.3-0 ; 
       il_shdgnrator_sPaTL-mat45.3-0 ; 
       il_shdgnrator_sPaTL-mat46.3-0 ; 
       il_shdgnrator_sPaTL-mat47.3-0 ; 
       il_shdgnrator_sPaTL-mat48.3-0 ; 
       il_shdgnrator_sPaTL-mat49.3-0 ; 
       il_shdgnrator_sPaTL-mat50.3-0 ; 
       il_shdgnrator_sPaTL-mat51.3-0 ; 
       il_shdgnrator_sPaTL-mat6.3-0 ; 
       il_shdgnrator_sPaTL-mat7.3-0 ; 
       il_shdgnrator_sPaTL-mat8.3-0 ; 
       il_shdgnrator_sPaTL-mat9.3-0 ; 
       il_shieldgen_sPATL-mat88.3-0 ; 
       il_shieldgen_sPATL-mat91.3-0 ; 
       il_shieldgen_sPATL-mat92.3-0 ; 
       rix_fighter_sPAt-mat75.3-0 ; 
       turret_loc-mat132.1-0 ; 
       turret_loc-mat133.1-0 ; 
       turret_loc-mat134.1-0 ; 
       turret_loc-mat135.1-0 ; 
       turret_loc-mat136.1-0 ; 
       turret_loc-mat137.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       arc-cone.1-0 ROOT ; 
       cap04-blthrust.1-0 ; 
       cap04-brthrust.1-0 ; 
       cap04-bturatt.1-0 ; 
       cap04-bturret.1-0 ; 
       cap04-cap04.16-0 ROOT ; 
       cap04-cockpt.1-0 ; 
       cap04-finzzz.1-0 ; 
       cap04-fuselage.1-0 ; 
       cap04-lantenn.1-0 ; 
       cap04-lwepemt.1-0 ; 
       cap04-lwingzz1.1-0 ; 
       cap04-lwingzz2.1-0 ; 
       cap04-missemt.1-0 ; 
       cap04-rantenn.1-0 ; 
       cap04-rwepemt.1-0 ; 
       cap04-rwingzz1.1-0 ; 
       cap04-rwingzz2.1-0 ; 
       cap04-SSf.1-0 ; 
       cap04-SSl1.1-0 ; 
       cap04-SSl2.1-0 ; 
       cap04-SSl3.1-0 ; 
       cap04-SSl4.1-0 ; 
       cap04-SSla.1-0 ; 
       cap04-SSr1.1-0 ; 
       cap04-SSr2.1-0 ; 
       cap04-SSr3.1-0 ; 
       cap04-SSr4.1-0 ; 
       cap04-SSra.1-0 ; 
       cap04-tlthrust.1-0 ; 
       cap04-trail.1-0 ; 
       cap04-trthrust.1-0 ; 
       cap04-tturret1.1-0 ; 
       cap04-turatt.1-0 ; 
       cap04-turwepemt1.1-0 ; 
       cap04-turwepemt2.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
       turret_loc-cone2.1-0 ROOT ; 
       turret_loc-cone3.1-0 ROOT ; 
       turret_loc-cone4.1-0 ROOT ; 
       turret_loc-cone5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap04/PICTURES/cap04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap04-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       add_top_tur-t2d46.1-0 ; 
       add_top_tur-t2d47.1-0 ; 
       add_top_tur-t2d48.1-0 ; 
       il_shdgnrator_sPaTL-t2d1.3-0 ; 
       il_shdgnrator_sPaTL-t2d10.3-0 ; 
       il_shdgnrator_sPaTL-t2d14.3-0 ; 
       il_shdgnrator_sPaTL-t2d15.3-0 ; 
       il_shdgnrator_sPaTL-t2d16.3-0 ; 
       il_shdgnrator_sPaTL-t2d17.3-0 ; 
       il_shdgnrator_sPaTL-t2d2.3-0 ; 
       il_shdgnrator_sPaTL-t2d21.3-0 ; 
       il_shdgnrator_sPaTL-t2d22.3-0 ; 
       il_shdgnrator_sPaTL-t2d23.3-0 ; 
       il_shdgnrator_sPaTL-t2d25.3-0 ; 
       il_shdgnrator_sPaTL-t2d3.3-0 ; 
       il_shdgnrator_sPaTL-t2d30.3-0 ; 
       il_shdgnrator_sPaTL-t2d33.3-0 ; 
       il_shdgnrator_sPaTL-t2d35.3-0 ; 
       il_shdgnrator_sPaTL-t2d37.3-0 ; 
       il_shdgnrator_sPaTL-t2d38.3-0 ; 
       il_shdgnrator_sPaTL-t2d39.3-0 ; 
       il_shdgnrator_sPaTL-t2d4.3-0 ; 
       il_shdgnrator_sPaTL-t2d40.3-0 ; 
       il_shdgnrator_sPaTL-t2d41.3-0 ; 
       il_shdgnrator_sPaTL-t2d42.3-0 ; 
       il_shdgnrator_sPaTL-t2d43.3-0 ; 
       il_shdgnrator_sPaTL-t2d44.3-0 ; 
       il_shdgnrator_sPaTL-t2d45.3-0 ; 
       il_shdgnrator_sPaTL-t2d5.3-0 ; 
       il_shdgnrator_sPaTL-t2d6.3-0 ; 
       il_shdgnrator_sPaTL-t2d7.3-0 ; 
       il_shdgnrator_sPaTL-t2d8.3-0 ; 
       il_shdgnrator_sPaTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 5 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 11 110 ; 
       10 9 110 ; 
       11 5 110 ; 
       12 11 110 ; 
       13 5 110 ; 
       14 16 110 ; 
       15 14 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 8 110 ; 
       19 12 110 ; 
       20 9 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 7 110 ; 
       24 17 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 7 110 ; 
       29 5 110 ; 
       30 5 110 ; 
       31 5 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 4 110 ; 
       35 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 50 300 ; 
       40 51 300 ; 
       4 2 300 ; 
       7 21 300 ; 
       8 38 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 9 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       11 19 300 ; 
       11 24 300 ; 
       11 34 300 ; 
       12 20 300 ; 
       12 27 300 ; 
       14 23 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       16 15 300 ; 
       16 25 300 ; 
       17 16 300 ; 
       17 26 300 ; 
       18 44 300 ; 
       19 4 300 ; 
       20 8 300 ; 
       21 7 300 ; 
       22 1 300 ; 
       23 43 300 ; 
       24 5 300 ; 
       25 45 300 ; 
       26 6 300 ; 
       27 0 300 ; 
       28 42 300 ; 
       32 3 300 ; 
       33 22 300 ; 
       36 49 300 ; 
       37 48 300 ; 
       38 47 300 ; 
       39 46 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       9 0 401 ; 
       10 21 401 ; 
       11 28 401 ; 
       12 29 401 ; 
       13 30 401 ; 
       14 31 401 ; 
       15 32 401 ; 
       16 4 401 ; 
       17 5 401 ; 
       18 6 401 ; 
       19 7 401 ; 
       20 8 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       24 13 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 11 401 ; 
       39 3 401 ; 
       40 9 401 ; 
       41 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 0 0 DISPLAY 1 2 SRT 8.695093 8.695093 8.695093 0 0 0 0 5.863967 0.213834 MPRFLG 0 ; 
       40 SCHEM 5 0 0 SRT 8.695093 8.695093 8.695093 3.141593 0 0 0 -2.120762 6.669127 MPRFLG 0 ; 
       1 SCHEM 142 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 139.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 82 -4 0 MPRFLG 0 ; 
       4 SCHEM 85.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 80.75 0 0 SRT 1 1 1 0 -1.570796 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 149.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 132 -2 0 MPRFLG 0 ; 
       8 SCHEM 99.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 24.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 22 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 12 -4 0 MPRFLG 0 ; 
       13 SCHEM 152 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57 -4 0 MPRFLG 0 ; 
       15 SCHEM 54.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 44.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 79.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 9.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 24.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 17 -6 0 MPRFLG 0 ; 
       22 SCHEM 19.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 129.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 42 -6 0 MPRFLG 0 ; 
       25 SCHEM 57 -6 0 MPRFLG 0 ; 
       26 SCHEM 49.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 52 -6 0 MPRFLG 0 ; 
       28 SCHEM 132 -4 0 MPRFLG 0 ; 
       29 SCHEM 147 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 137 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 144.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 73.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 77 -4 0 MPRFLG 0 ; 
       34 SCHEM 84.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 72 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 162 0 0 DISPLAY 0 0 SRT 3.437936 3.437936 3.437936 0 0 0 0 -13.71223 6.981259 MPRFLG 0 ; 
       37 SCHEM 154.5 0 0 DISPLAY 0 0 SRT 3.437936 3.437936 3.437936 3.141593 0 0 0 17.55383 0.0993275 MPRFLG 0 ; 
       38 SCHEM 157 0 0 SRT 3.437936 3.437936 3.437936 -1.570796 0 0 15.29318 -2.157384 14.79777 MPRFLG 0 ; 
       39 SCHEM 159.5 0 0 SRT 3.437936 3.437936 3.437936 -1.570796 0 0 -14.95333 -2.157384 14.79777 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 87 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 74.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 159.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 157 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 154.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 162 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 99.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 102 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 104.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 107 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 89.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 134.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 77 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 64.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 12 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 112 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 114.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 117 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 119.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 122 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 124.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 37 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 32 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 27 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 109.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 92 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 94.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 97 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 132 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 129.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 79.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 57 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 127 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 87 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 74.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 92 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 62 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 94.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 134.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 109.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 77 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 97 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 67 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 12 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 112 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 114.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 117 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 99.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 119.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 122 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 124.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 37 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 27 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 102 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 104.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 107 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 89.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 69.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 153.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 83 29 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
