SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       Add_garages-cap05.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.30-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.30-0 ROOT ; 
       rix_carrier_s-inf_light2_1.30-0 ROOT ; 
       rix_carrier_s-inf_light3_1.30-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.30-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.30-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       add_garages-bay1.1-0 ; 
       add_garages-mat144.1-0 ; 
       add_garages-mat145.1-0 ; 
       add_garages-mat146.1-0 ; 
       add_garages-mat147.1-0 ; 
       add_garages-mat148.1-0 ; 
       add_garages-mat149.1-0 ; 
       add_garages-mat150.1-0 ; 
       add_garages-mat151.1-0 ; 
       add_garages-mat152.1-0 ; 
       add_garages-mat153.1-0 ; 
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.2-0 ; 
       rix_post_sPTL-mat15.2-0 ; 
       rix_post_sPTL-mat16.2-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.2-0 ; 
       rix_post_sPTL-mat22.2-0 ; 
       rix_post_sPTL-mat23.2-0 ; 
       rix_post_sPTL-mat24.2-0 ; 
       rix_post_sPTL-mat25.2-0 ; 
       rix_post_sPTL-mat26.2-0 ; 
       rix_post_sPTL-mat27.2-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       Add_garages-adoccon.1-0 ; 
       Add_garages-BAY1.1-0 ; 
       Add_garages-BAY2.1-0 ; 
       Add_garages-cap05.1-0 ROOT ; 
       Add_garages-contwr.1-0 ; 
       Add_garages-cube1.1-0 ; 
       Add_garages-cube2.1-0 ; 
       Add_garages-fuselg0.1-0 ; 
       Add_garages-fuselg1.1-0 ; 
       Add_garages-fuselg2.1-0 ; 
       Add_garages-fuselg3.1-0 ; 
       Add_garages-fuselg4.1-0 ; 
       Add_garages-fuselg5.1-0 ; 
       Add_garages-fuselg6.1-0 ; 
       Add_garages-garage1A.1-0 ; 
       Add_garages-garage1A1.1-0 ; 
       Add_garages-garage1B.1-0 ; 
       Add_garages-garage1B1.1-0 ; 
       Add_garages-garage1C.1-0 ; 
       Add_garages-garage1C1.1-0 ; 
       Add_garages-garage1D.1-0 ; 
       Add_garages-garage1D1.1-0 ; 
       Add_garages-garage1E.1-0 ; 
       Add_garages-garage1E1.1-0 ; 
       Add_garages-ldoccon.1-0 ; 
       Add_garages-lndpad1.1-0 ; 
       Add_garages-lndpad2.1-0 ; 
       Add_garages-lndpad3.1-0 ; 
       Add_garages-lndpad4.1-0 ; 
       Add_garages-lturatt.1-0 ; 
       Add_garages-platfm0.1-0 ; 
       Add_garages-platfm1.1-0 ; 
       Add_garages-platfm2.1-0 ; 
       Add_garages-platfm3.1-0 ; 
       Add_garages-platfm4.1-0 ; 
       Add_garages-rdoccon.1-0 ; 
       Add_garages-SS1.1-0 ; 
       Add_garages-SS2.1-0 ; 
       Add_garages-SS3.1-0 ; 
       Add_garages-SS4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/cap05 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-Add_garages.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_garages-t2d29.1-0 ; 
       add_garages-t2d30.4-0 ; 
       add_garages-t2d31.4-0 ; 
       add_garages-t2d32.4-0 ; 
       add_garages-t2d33.1-0 ; 
       add_garages-t2d34.1-0 ; 
       add_garages-t2d35.1-0 ; 
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.5-0 ; 
       rix_post_sPTL-t2d14.5-0 ; 
       rix_post_sPTL-t2d19.5-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.5-0 ; 
       rix_post_sPTL-t2d21.5-0 ; 
       rix_post_sPTL-t2d22.5-0 ; 
       rix_post_sPTL-t2d23.5-0 ; 
       rix_post_sPTL-t2d24.5-0 ; 
       rix_post_sPTL-t2d25.5-0 ; 
       rix_post_sPTL-t2d26.5-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       16 1 110 ; 
       18 1 110 ; 
       20 1 110 ; 
       22 1 110 ; 
       2 6 110 ; 
       15 2 110 ; 
       17 2 110 ; 
       19 2 110 ; 
       21 2 110 ; 
       23 2 110 ; 
       0 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 3 110 ; 
       24 8 110 ; 
       25 31 110 ; 
       26 32 110 ; 
       26 37 111 ; 
       26 37 114 ; 
       27 33 110 ; 
       28 34 110 ; 
       29 33 110 ; 
       30 3 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 8 110 ; 
       36 31 110 ; 
       37 32 110 ; 
       38 33 110 ; 
       39 34 110 ; 
       14 1 110 ; 
       1 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 11 300 ; 
       4 32 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       8 0 300 ; 
       9 31 300 ; 
       10 19 300 ; 
       11 30 300 ; 
       12 29 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 27 300 ; 
       13 28 300 ; 
       31 33 300 ; 
       32 12 300 ; 
       33 13 300 ; 
       34 34 300 ; 
       36 2 300 ; 
       37 1 300 ; 
       38 4 300 ; 
       39 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 1 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       12 28 401 ; 
       13 29 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 15 401 ; 
       18 11 401 ; 
       19 7 401 ; 
       20 12 401 ; 
       21 14 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 13 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 25 -8 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 30 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 40 -6 0 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 40 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 45 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       3 SCHEM 36.25 0 0 DISPLAY 1 2 SRT 0.9999999 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 40 -4 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 70 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 15 -4 0 MPRFLG 0 ; 
       25 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 60 -6 0 MPRFLG 0 ; 
       27 SCHEM 55 -6 0 MPRFLG 0 ; 
       28 SCHEM 50 -6 0 MPRFLG 0 ; 
       29 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 66.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 61.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 55 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 48.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 65 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 52.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 47.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
