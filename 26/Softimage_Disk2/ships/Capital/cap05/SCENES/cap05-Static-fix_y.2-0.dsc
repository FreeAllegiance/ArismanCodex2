SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.45-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       rix_carrier_s-inf_light1_1.43-0 ROOT ; 
       rix_carrier_s-inf_light1_1_1.8-0 ROOT ; 
       rix_carrier_s-inf_light2_1.43-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.8-0 ROOT ; 
       rix_carrier_s-inf_light3_1.43-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.8-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.43-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.8-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.43-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.8-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.43-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       add_garages-bay1.1-0 ; 
       add_garages-mat148.1-0 ; 
       add_garages-mat149.1-0 ; 
       add_garages-mat150.1-0 ; 
       add_garages-mat151.1-0 ; 
       add_garages-mat152.1-0 ; 
       add_garages-mat153.1-0 ; 
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.2-0 ; 
       rix_post_sPTL-mat15.2-0 ; 
       rix_post_sPTL-mat16.2-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.2-0 ; 
       rix_post_sPTL-mat22.2-0 ; 
       rix_post_sPTL-mat23.2-0 ; 
       rix_post_sPTL-mat24.2-0 ; 
       rix_post_sPTL-mat25.2-0 ; 
       rix_post_sPTL-mat26.2-0 ; 
       rix_post_sPTL-mat27.2-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       STATIC-adoccon.1-0 ; 
       STATIC-cap05.3-0 ROOT ; 
       STATIC-contwr.1-0 ; 
       STATIC-cube1.1-0 ; 
       STATIC-cube2.1-0 ; 
       STATIC-fuselg0.1-0 ; 
       STATIC-fuselg1.1-0 ; 
       STATIC-fuselg2.1-0 ; 
       STATIC-fuselg3.1-0 ; 
       STATIC-fuselg4.1-0 ; 
       STATIC-fuselg5.1-0 ; 
       STATIC-fuselg6.1-0 ; 
       STATIC-ldoccon.1-0 ; 
       STATIC-lndpad1.1-0 ; 
       STATIC-lndpad2.1-0 ; 
       STATIC-lndpad3.1-0 ; 
       STATIC-lndpad4.1-0 ; 
       STATIC-lturatt.1-0 ; 
       STATIC-platfm0.1-0 ; 
       STATIC-platfm1.1-0 ; 
       STATIC-platfm2.1-0 ; 
       STATIC-platfm3.1-0 ; 
       STATIC-platfm4.1-0 ; 
       STATIC-rdoccon.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/cap05 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-Static-fix_y.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_garages-t2d29.1-0 ; 
       add_garages-t2d30.4-0 ; 
       add_garages-t2d31.4-0 ; 
       add_garages-t2d32.4-0 ; 
       add_garages-t2d33.1-0 ; 
       add_garages-t2d34.1-0 ; 
       add_garages-t2d35.1-0 ; 
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.5-0 ; 
       rix_post_sPTL-t2d14.5-0 ; 
       rix_post_sPTL-t2d19.5-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.5-0 ; 
       rix_post_sPTL-t2d21.5-0 ; 
       rix_post_sPTL-t2d22.5-0 ; 
       rix_post_sPTL-t2d23.5-0 ; 
       rix_post_sPTL-t2d24.5-0 ; 
       rix_post_sPTL-t2d25.5-0 ; 
       rix_post_sPTL-t2d26.5-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 1 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 1 110 ; 
       12 6 110 ; 
       13 19 110 ; 
       14 20 110 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 21 110 ; 
       18 1 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       2 28 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       6 0 300 ; 
       7 27 300 ; 
       8 15 300 ; 
       9 26 300 ; 
       10 25 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 23 300 ; 
       11 24 300 ; 
       19 29 300 ; 
       20 8 300 ; 
       21 9 300 ; 
       22 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 1 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       8 28 401 ; 
       9 29 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 15 401 ; 
       14 11 401 ; 
       15 7 401 ; 
       16 12 401 ; 
       17 14 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 13 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 21.25 0 0 DISPLAY 3 2 SRT 0.9999999 1 1 -1.570796 3.141593 -0.7853982 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 13.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 32.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 31.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
