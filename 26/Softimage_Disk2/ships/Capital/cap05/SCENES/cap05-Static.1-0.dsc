SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.64-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       rix_carrier_s-inf_light1_1.60-0 ROOT ; 
       rix_carrier_s-inf_light1_1_1.25-0 ROOT ; 
       rix_carrier_s-inf_light2_1.60-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.25-0 ROOT ; 
       rix_carrier_s-inf_light3_1.60-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.25-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.60-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.25-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.60-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.25-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.60-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       add_garages-bay1.1-0 ; 
       add_garages-mat148.1-0 ; 
       add_garages-mat149.1-0 ; 
       add_garages-mat150.1-0 ; 
       add_garages-mat151.1-0 ; 
       add_garages-mat152.1-0 ; 
       add_garages-mat153.1-0 ; 
       rix_post_sPTL-mat1.3-0 ; 
       rix_post_sPTL-mat10.3-0 ; 
       rix_post_sPTL-mat11.3-0 ; 
       rix_post_sPTL-mat12.3-0 ; 
       rix_post_sPTL-mat13.3-0 ; 
       rix_post_sPTL-mat14.4-0 ; 
       rix_post_sPTL-mat15.4-0 ; 
       rix_post_sPTL-mat16.4-0 ; 
       rix_post_sPTL-mat2.3-0 ; 
       rix_post_sPTL-mat21.4-0 ; 
       rix_post_sPTL-mat22.4-0 ; 
       rix_post_sPTL-mat23.4-0 ; 
       rix_post_sPTL-mat24.4-0 ; 
       rix_post_sPTL-mat25.4-0 ; 
       rix_post_sPTL-mat26.4-0 ; 
       rix_post_sPTL-mat27.4-0 ; 
       rix_post_sPTL-mat28.3-0 ; 
       rix_post_sPTL-mat29.3-0 ; 
       rix_post_sPTL-mat3.3-0 ; 
       rix_post_sPTL-mat4.3-0 ; 
       rix_post_sPTL-mat5.3-0 ; 
       rix_post_sPTL-mat7.3-0 ; 
       rix_post_sPTL-mat8.3-0 ; 
       rix_post_sPTL-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       ROTATE-adoccon.1-0 ; 
       ROTATE-BAY1.1-0 ; 
       ROTATE-BAY2.1-0 ; 
       ROTATE-cap05.17-0 ROOT ; 
       ROTATE-contwr.1-0 ; 
       ROTATE-cube1.1-0 ; 
       ROTATE-cube2.1-0 ; 
       ROTATE-fuselg0.1-0 ; 
       ROTATE-fuselg1.1-0 ; 
       ROTATE-fuselg2.1-0 ; 
       ROTATE-fuselg3.1-0 ; 
       ROTATE-fuselg4.1-0 ; 
       ROTATE-fuselg5.1-0 ; 
       ROTATE-fuselg6.1-0 ; 
       ROTATE-ldoccon.1-0 ; 
       ROTATE-platfm0.1-0 ; 
       ROTATE-platfm1.1-0 ; 
       ROTATE-platfm2.1-0 ; 
       ROTATE-platfm3.1-0 ; 
       ROTATE-platfm4.1-0 ; 
       ROTATE-rdoccon.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap05/PICTURES/cap05 ; 
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap05/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_garages-t2d29.1-0 ; 
       add_garages-t2d30.6-0 ; 
       add_garages-t2d31.6-0 ; 
       add_garages-t2d32.6-0 ; 
       add_garages-t2d33.3-0 ; 
       add_garages-t2d34.3-0 ; 
       add_garages-t2d35.3-0 ; 
       rix_post_sPTL-t2d1.3-0 ; 
       rix_post_sPTL-t2d10.3-0 ; 
       rix_post_sPTL-t2d11.3-0 ; 
       rix_post_sPTL-t2d12.7-0 ; 
       rix_post_sPTL-t2d14.7-0 ; 
       rix_post_sPTL-t2d19.7-0 ; 
       rix_post_sPTL-t2d2.3-0 ; 
       rix_post_sPTL-t2d20.7-0 ; 
       rix_post_sPTL-t2d21.7-0 ; 
       rix_post_sPTL-t2d22.7-0 ; 
       rix_post_sPTL-t2d23.7-0 ; 
       rix_post_sPTL-t2d24.7-0 ; 
       rix_post_sPTL-t2d25.7-0 ; 
       rix_post_sPTL-t2d26.7-0 ; 
       rix_post_sPTL-t2d27.3-0 ; 
       rix_post_sPTL-t2d28.3-0 ; 
       rix_post_sPTL-t2d3.3-0 ; 
       rix_post_sPTL-t2d4.3-0 ; 
       rix_post_sPTL-t2d5.3-0 ; 
       rix_post_sPTL-t2d6.3-0 ; 
       rix_post_sPTL-t2d7.3-0 ; 
       rix_post_sPTL-t2d8.3-0 ; 
       rix_post_sPTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 3 110 ; 
       14 8 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 7 300 ; 
       4 28 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 0 300 ; 
       9 27 300 ; 
       10 15 300 ; 
       11 26 300 ; 
       12 25 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       16 29 300 ; 
       17 8 300 ; 
       18 9 300 ; 
       19 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 1 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       8 28 401 ; 
       9 29 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 15 401 ; 
       14 11 401 ; 
       15 7 401 ; 
       16 12 401 ; 
       17 14 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 13 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 15 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 20 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 20 -10 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 18.75 -4 0 SRT 0.9999999 1 1 -1.570796 3.141593 -0.7853982 0 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 MPRFLG 0 ; 
       5 SCHEM 20 -8 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 10 -10 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 30 -8 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 25 -8 0 MPRFLG 0 ; 
       20 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 15 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
