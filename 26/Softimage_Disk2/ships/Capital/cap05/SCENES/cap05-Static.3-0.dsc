SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap05-cap05.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.19-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.19-0 ROOT ; 
       rix_carrier_s-inf_light2_1.19-0 ROOT ; 
       rix_carrier_s-inf_light3_1.19-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.19-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.19-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.2-0 ; 
       rix_post_sPTL-mat15.2-0 ; 
       rix_post_sPTL-mat16.2-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.2-0 ; 
       rix_post_sPTL-mat22.2-0 ; 
       rix_post_sPTL-mat23.2-0 ; 
       rix_post_sPTL-mat24.2-0 ; 
       rix_post_sPTL-mat25.2-0 ; 
       rix_post_sPTL-mat26.2-0 ; 
       rix_post_sPTL-mat27.2-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
       Static-bay1.1-0 ; 
       Static-mat144.1-0 ; 
       Static-mat145.1-0 ; 
       Static-mat146.1-0 ; 
       Static-mat147.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       cap05-adoccon.1-0 ; 
       cap05-cap05.18-0 ROOT ; 
       cap05-contwr.1-0 ; 
       cap05-fuselg0.1-0 ; 
       cap05-fuselg1.1-0 ; 
       cap05-fuselg2.1-0 ; 
       cap05-fuselg3.1-0 ; 
       cap05-fuselg4.1-0 ; 
       cap05-fuselg5.1-0 ; 
       cap05-fuselg6.1-0 ; 
       cap05-ldoccon.1-0 ; 
       cap05-lndpad1.1-0 ; 
       cap05-lndpad2.1-0 ; 
       cap05-lndpad3.1-0 ; 
       cap05-lndpad4.1-0 ; 
       cap05-lturatt.1-0 ; 
       cap05-platfm0.1-0 ; 
       cap05-platfm1.1-0 ; 
       cap05-platfm2.1-0 ; 
       cap05-platfm3.1-0 ; 
       cap05-platfm4.1-0 ; 
       cap05-rdoccon.1-0 ; 
       cap05-SS1.1-0 ; 
       cap05-SS2.1-0 ; 
       cap05-SS3.1-0 ; 
       cap05-SS4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/cap05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-Static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.5-0 ; 
       rix_post_sPTL-t2d14.5-0 ; 
       rix_post_sPTL-t2d19.5-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.5-0 ; 
       rix_post_sPTL-t2d21.5-0 ; 
       rix_post_sPTL-t2d22.5-0 ; 
       rix_post_sPTL-t2d23.5-0 ; 
       rix_post_sPTL-t2d24.5-0 ; 
       rix_post_sPTL-t2d25.5-0 ; 
       rix_post_sPTL-t2d26.5-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
       Static-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 1 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 1 110 ; 
       10 4 110 ; 
       11 17 110 ; 
       12 18 110 ; 
       12 23 111 ; 
       12 23 114 ; 
       13 19 110 ; 
       14 20 110 ; 
       15 19 110 ; 
       16 1 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 4 110 ; 
       22 17 110 ; 
       23 18 110 ; 
       24 19 110 ; 
       25 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       2 21 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 24 300 ; 
       5 20 300 ; 
       6 8 300 ; 
       7 19 300 ; 
       8 18 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       17 22 300 ; 
       18 1 300 ; 
       19 2 300 ; 
       20 23 300 ; 
       22 26 300 ; 
       23 25 300 ; 
       24 28 300 ; 
       25 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 21 401 ; 
       2 22 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 8 401 ; 
       7 4 401 ; 
       8 0 401 ; 
       9 5 401 ; 
       10 7 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 6 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 23 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 23.75 0 0 DISPLAY 3 2 SRT 0.9999999 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 42.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 41.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 36.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 37.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 27.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 22.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
