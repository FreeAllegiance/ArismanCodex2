SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap05-cap05.21-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.21-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.21-0 ROOT ; 
       rix_carrier_s-inf_light2_1.21-0 ROOT ; 
       rix_carrier_s-inf_light3_1.21-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.21-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.21-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       add_garages-bay1.1-0 ; 
       add_garages-mat144.1-0 ; 
       add_garages-mat145.1-0 ; 
       add_garages-mat146.1-0 ; 
       add_garages-mat147.1-0 ; 
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.2-0 ; 
       rix_post_sPTL-mat15.2-0 ; 
       rix_post_sPTL-mat16.2-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.2-0 ; 
       rix_post_sPTL-mat22.2-0 ; 
       rix_post_sPTL-mat23.2-0 ; 
       rix_post_sPTL-mat24.2-0 ; 
       rix_post_sPTL-mat25.2-0 ; 
       rix_post_sPTL-mat26.2-0 ; 
       rix_post_sPTL-mat27.2-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap05-adoccon.1-0 ; 
       cap05-cap05.20-0 ROOT ; 
       cap05-contwr.1-0 ; 
       cap05-cube1.1-0 ; 
       cap05-cube1_1.1-0 ; 
       cap05-fuselg0.1-0 ; 
       cap05-fuselg1.1-0 ; 
       cap05-fuselg2.1-0 ; 
       cap05-fuselg3.1-0 ; 
       cap05-fuselg4.1-0 ; 
       cap05-fuselg5.1-0 ; 
       cap05-fuselg6.1-0 ; 
       cap05-ldoccon.1-0 ; 
       cap05-lndpad1.1-0 ; 
       cap05-lndpad2.1-0 ; 
       cap05-lndpad3.1-0 ; 
       cap05-lndpad4.1-0 ; 
       cap05-lturatt.1-0 ; 
       cap05-platfm0.1-0 ; 
       cap05-platfm1.1-0 ; 
       cap05-platfm2.1-0 ; 
       cap05-platfm3.1-0 ; 
       cap05-platfm4.1-0 ; 
       cap05-rdoccon.1-0 ; 
       cap05-SS1.1-0 ; 
       cap05-SS2.1-0 ; 
       cap05-SS3.1-0 ; 
       cap05-SS4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/cap05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-add_garages.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       add_garages-t2d29.1-0 ; 
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.5-0 ; 
       rix_post_sPTL-t2d14.5-0 ; 
       rix_post_sPTL-t2d19.5-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.5-0 ; 
       rix_post_sPTL-t2d21.5-0 ; 
       rix_post_sPTL-t2d22.5-0 ; 
       rix_post_sPTL-t2d23.5-0 ; 
       rix_post_sPTL-t2d24.5-0 ; 
       rix_post_sPTL-t2d25.5-0 ; 
       rix_post_sPTL-t2d26.5-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 6 110 ; 
       0 6 110 ; 
       2 6 110 ; 
       5 6 110 ; 
       6 1 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 1 110 ; 
       12 6 110 ; 
       13 19 110 ; 
       14 20 110 ; 
       14 25 111 ; 
       14 25 114 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 21 110 ; 
       18 1 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 6 110 ; 
       24 19 110 ; 
       25 20 110 ; 
       26 21 110 ; 
       27 22 110 ; 
       3 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       2 26 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 0 300 ; 
       7 25 300 ; 
       8 13 300 ; 
       9 24 300 ; 
       10 23 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       19 27 300 ; 
       20 6 300 ; 
       21 7 300 ; 
       22 28 300 ; 
       24 2 300 ; 
       25 1 300 ; 
       26 4 300 ; 
       27 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 22 401 ; 
       7 23 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 9 401 ; 
       12 5 401 ; 
       13 1 401 ; 
       14 6 401 ; 
       15 8 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 7 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 51.5 0 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       1 SCHEM 23.75 0 0 SRT 0.9999999 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 45 -2 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 35 -6 0 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       20 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 MPRFLG 0 ; 
       22 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 40 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 49 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 46.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
