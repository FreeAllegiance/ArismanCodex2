SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap05-cap05.22-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.22-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rix_carrier_s-inf_light1_1.22-0 ROOT ; 
       rix_carrier_s-inf_light2_1.22-0 ROOT ; 
       rix_carrier_s-inf_light3_1.22-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.22-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.22-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_garages-bay1.1-0 ; 
       add_garages-mat144.1-0 ; 
       add_garages-mat145.1-0 ; 
       add_garages-mat146.1-0 ; 
       add_garages-mat147.1-0 ; 
       add_garages-mat148.1-0 ; 
       add_garages-mat149.1-0 ; 
       add_garages-mat150.1-0 ; 
       rix_post_sPTL-mat1.1-0 ; 
       rix_post_sPTL-mat10.1-0 ; 
       rix_post_sPTL-mat11.1-0 ; 
       rix_post_sPTL-mat12.1-0 ; 
       rix_post_sPTL-mat13.1-0 ; 
       rix_post_sPTL-mat14.2-0 ; 
       rix_post_sPTL-mat15.2-0 ; 
       rix_post_sPTL-mat16.2-0 ; 
       rix_post_sPTL-mat2.1-0 ; 
       rix_post_sPTL-mat21.2-0 ; 
       rix_post_sPTL-mat22.2-0 ; 
       rix_post_sPTL-mat23.2-0 ; 
       rix_post_sPTL-mat24.2-0 ; 
       rix_post_sPTL-mat25.2-0 ; 
       rix_post_sPTL-mat26.2-0 ; 
       rix_post_sPTL-mat27.2-0 ; 
       rix_post_sPTL-mat28.1-0 ; 
       rix_post_sPTL-mat29.1-0 ; 
       rix_post_sPTL-mat3.1-0 ; 
       rix_post_sPTL-mat4.1-0 ; 
       rix_post_sPTL-mat5.1-0 ; 
       rix_post_sPTL-mat7.1-0 ; 
       rix_post_sPTL-mat8.1-0 ; 
       rix_post_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap05-adoccon.1-0 ; 
       cap05-cap05.21-0 ROOT ; 
       cap05-contwr.1-0 ; 
       cap05-cube1.1-0 ; 
       cap05-cube1_1.1-0 ; 
       cap05-fuselg0.1-0 ; 
       cap05-fuselg1.1-0 ; 
       cap05-fuselg2.1-0 ; 
       cap05-fuselg3.1-0 ; 
       cap05-fuselg4.1-0 ; 
       cap05-fuselg5.1-0 ; 
       cap05-fuselg6.1-0 ; 
       cap05-ldoccon.1-0 ; 
       cap05-lndpad1.1-0 ; 
       cap05-lndpad2.1-0 ; 
       cap05-lndpad3.1-0 ; 
       cap05-lndpad4.1-0 ; 
       cap05-lturatt.1-0 ; 
       cap05-platfm0.1-0 ; 
       cap05-platfm1.1-0 ; 
       cap05-platfm2.1-0 ; 
       cap05-platfm3.1-0 ; 
       cap05-platfm4.1-0 ; 
       cap05-rdoccon.1-0 ; 
       cap05-SS1.1-0 ; 
       cap05-SS2.1-0 ; 
       cap05-SS3.1-0 ; 
       cap05-SS4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/cap05 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap05/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-add_garages.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_garages-t2d29.1-0 ; 
       add_garages-t2d30.1-0 ; 
       add_garages-t2d31.1-0 ; 
       add_garages-t2d32.1-0 ; 
       rix_post_sPTL-t2d1.1-0 ; 
       rix_post_sPTL-t2d10.1-0 ; 
       rix_post_sPTL-t2d11.1-0 ; 
       rix_post_sPTL-t2d12.5-0 ; 
       rix_post_sPTL-t2d14.5-0 ; 
       rix_post_sPTL-t2d19.5-0 ; 
       rix_post_sPTL-t2d2.1-0 ; 
       rix_post_sPTL-t2d20.5-0 ; 
       rix_post_sPTL-t2d21.5-0 ; 
       rix_post_sPTL-t2d22.5-0 ; 
       rix_post_sPTL-t2d23.5-0 ; 
       rix_post_sPTL-t2d24.5-0 ; 
       rix_post_sPTL-t2d25.5-0 ; 
       rix_post_sPTL-t2d26.5-0 ; 
       rix_post_sPTL-t2d27.1-0 ; 
       rix_post_sPTL-t2d28.1-0 ; 
       rix_post_sPTL-t2d3.1-0 ; 
       rix_post_sPTL-t2d4.1-0 ; 
       rix_post_sPTL-t2d5.1-0 ; 
       rix_post_sPTL-t2d6.1-0 ; 
       rix_post_sPTL-t2d7.1-0 ; 
       rix_post_sPTL-t2d8.1-0 ; 
       rix_post_sPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 6 110 ; 
       0 6 110 ; 
       2 6 110 ; 
       5 6 110 ; 
       6 1 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 1 110 ; 
       12 6 110 ; 
       13 19 110 ; 
       14 20 110 ; 
       14 25 111 ; 
       14 25 114 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 21 110 ; 
       18 1 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 6 110 ; 
       24 19 110 ; 
       25 20 110 ; 
       26 21 110 ; 
       27 22 110 ; 
       3 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 8 300 ; 
       2 29 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       6 0 300 ; 
       7 28 300 ; 
       8 16 300 ; 
       9 27 300 ; 
       10 26 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 24 300 ; 
       11 25 300 ; 
       19 30 300 ; 
       20 9 300 ; 
       21 10 300 ; 
       22 31 300 ; 
       24 2 300 ; 
       25 1 300 ; 
       26 4 300 ; 
       27 3 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 25 401 ; 
       10 26 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 12 401 ; 
       15 8 401 ; 
       16 4 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 10 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       0 0 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 50 -4 0 SRT 0.9999999 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -8 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 5 -10 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 10 -10 0 MPRFLG 0 ; 
       11 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 85 -10 0 MPRFLG 0 ; 
       14 SCHEM 75 -10 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 60 -10 0 MPRFLG 0 ; 
       17 SCHEM 70 -10 0 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 85 -8 0 MPRFLG 0 ; 
       20 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 68.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 MPRFLG 0 ; 
       24 SCHEM 82.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 65 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 99 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 6 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
