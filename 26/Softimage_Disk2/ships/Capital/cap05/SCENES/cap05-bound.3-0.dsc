SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.40-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       rix_carrier_s-inf_light1_1.40-0 ROOT ; 
       rix_carrier_s-inf_light1_1_1_1.3-0 ROOT ; 
       rix_carrier_s-inf_light2_1.40-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1_1.3-0 ROOT ; 
       rix_carrier_s-inf_light3_1.40-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.40-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.40-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1_1.3-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.40-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       bound-cube3.1-0 ; 
       bound-cube4.1-0 ; 
       bound-cube6.1-0 ; 
       bound-cube7.1-0 ; 
       bound-cube8.1-0 ; 
       bound-null1.1-0 ROOT ; 
       bound-tetra1.1-0 ; 
       bound-tetra2.1-0 ; 
       bound-tetra3.1-0 ; 
       bound-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-bound.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       4 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       0 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
