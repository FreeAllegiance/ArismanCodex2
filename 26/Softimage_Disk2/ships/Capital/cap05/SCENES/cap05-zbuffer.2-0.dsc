SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ROTATE-cap05.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.65-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       rix_carrier_s-inf_light1_1.61-0 ROOT ; 
       rix_carrier_s-inf_light1_1_1.26-0 ROOT ; 
       rix_carrier_s-inf_light2_1.61-0 ROOT ; 
       rix_carrier_s-inf_light2_1_1.26-0 ROOT ; 
       rix_carrier_s-inf_light3_1.61-0 ROOT ; 
       rix_carrier_s-inf_light3_1_1.26-0 ROOT ; 
       rix_post_sPTL-inf_light1_1.61-0 ROOT ; 
       rix_post_sPTL-inf_light1_1_1.26-0 ROOT ; 
       rix_post_sPTL-inf_light2_1.61-0 ROOT ; 
       rix_post_sPTL-inf_light2_1_1.26-0 ROOT ; 
       rix_post_sPTL-inf_light3_1.61-0 ROOT ; 
       rix_post_sPTL-inf_light3_1_1.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 93     
       add_garages-bay1.1-0 ; 
       add_garages-mat144.1-0 ; 
       add_garages-mat145.1-0 ; 
       add_garages-mat146.1-0 ; 
       add_garages-mat147.1-0 ; 
       add_garages-mat148.1-0 ; 
       add_garages-mat149.1-0 ; 
       add_garages-mat150.1-0 ; 
       add_garages-mat151.1-0 ; 
       add_garages-mat152.1-0 ; 
       add_garages-mat153.1-0 ; 
       rix_post_sPTL-mat1.3-0 ; 
       rix_post_sPTL-mat10.3-0 ; 
       rix_post_sPTL-mat11.3-0 ; 
       rix_post_sPTL-mat12.3-0 ; 
       rix_post_sPTL-mat13.3-0 ; 
       rix_post_sPTL-mat14.4-0 ; 
       rix_post_sPTL-mat15.4-0 ; 
       rix_post_sPTL-mat16.4-0 ; 
       rix_post_sPTL-mat2.3-0 ; 
       rix_post_sPTL-mat21.4-0 ; 
       rix_post_sPTL-mat22.4-0 ; 
       rix_post_sPTL-mat23.4-0 ; 
       rix_post_sPTL-mat24.4-0 ; 
       rix_post_sPTL-mat25.4-0 ; 
       rix_post_sPTL-mat26.4-0 ; 
       rix_post_sPTL-mat27.4-0 ; 
       rix_post_sPTL-mat28.3-0 ; 
       rix_post_sPTL-mat29.3-0 ; 
       rix_post_sPTL-mat3.3-0 ; 
       rix_post_sPTL-mat4.3-0 ; 
       rix_post_sPTL-mat5.3-0 ; 
       rix_post_sPTL-mat7.3-0 ; 
       rix_post_sPTL-mat8.3-0 ; 
       rix_post_sPTL-mat9.3-0 ; 
       ROTATE-mat167.2-0 ; 
       ROTATE-mat168.2-0 ; 
       ROTATE-mat169.2-0 ; 
       ROTATE-mat170.2-0 ; 
       ROTATE-mat171.2-0 ; 
       Rotate_fix_y-mat111.2-0 ; 
       Rotate_fix_y-mat116.2-0 ; 
       Rotate_fix_y-mat117.2-0 ; 
       Rotate_fix_y-mat118.2-0 ; 
       Rotate_fix_y-mat119.2-0 ; 
       Rotate_fix_y-mat120.2-0 ; 
       Rotate_fix_y-mat121.2-0 ; 
       Rotate_fix_y-mat122.2-0 ; 
       Rotate_fix_y-mat123.2-0 ; 
       Rotate_fix_y-mat124.2-0 ; 
       Rotate_fix_y-mat125.2-0 ; 
       Rotate_fix_y-mat126.2-0 ; 
       Rotate_fix_y-mat127.2-0 ; 
       Rotate_fix_y-mat128.2-0 ; 
       Rotate_fix_y-mat129.2-0 ; 
       Rotate_fix_y-mat130.2-0 ; 
       Rotate_fix_y-mat131.2-0 ; 
       Rotate_fix_y-mat132.2-0 ; 
       Rotate_fix_y-mat133.2-0 ; 
       Rotate_fix_y-mat134.2-0 ; 
       Rotate_fix_y-mat135.2-0 ; 
       Rotate_fix_y-mat136.2-0 ; 
       Rotate_fix_y-mat137.2-0 ; 
       Rotate_fix_y-mat138.2-0 ; 
       Rotate_fix_y-mat139.2-0 ; 
       Rotate_fix_y-mat140.2-0 ; 
       Rotate_fix_y-mat141.2-0 ; 
       Rotate_fix_y-mat142.2-0 ; 
       Rotate_fix_y-mat148.2-0 ; 
       Rotate_fix_y-mat149.2-0 ; 
       Rotate_fix_y-mat150.2-0 ; 
       Rotate_fix_y-mat151.2-0 ; 
       Rotate_fix_y-mat152.2-0 ; 
       Rotate_fix_y-mat153.2-0 ; 
       Rotate_fix_y-mat154.2-0 ; 
       Rotate_fix_y-mat155.2-0 ; 
       Rotate_fix_y-mat156.2-0 ; 
       Rotate_fix_y-mat157.2-0 ; 
       Rotate_fix_y-mat158.2-0 ; 
       Rotate_fix_y-mat159.2-0 ; 
       Rotate_fix_y-mat160.2-0 ; 
       Rotate_fix_y-mat161.2-0 ; 
       Rotate_fix_y-mat162.2-0 ; 
       Rotate_fix_y-mat84.2-0 ; 
       Rotate_fix_y-mat85.2-0 ; 
       Rotate_fix_y-mat86.2-0 ; 
       Rotate_fix_y-mat87.2-0 ; 
       Rotate_fix_y-mat88.2-0 ; 
       Rotate_fix_y-mat89.2-0 ; 
       Rotate_fix_y-mat90.2-0 ; 
       Rotate_fix_y-mat91.2-0 ; 
       Rotate_fix_y-mat92.2-0 ; 
       zbuffer-mat329.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 121     
       ROTATE-adoccon.1-0 ; 
       ROTATE-BAY1.1-0 ; 
       ROTATE-BAY2.1-0 ; 
       ROTATE-cap05.18-0 ROOT ; 
       ROTATE-contwr.1-0 ; 
       ROTATE-cube1.1-0 ; 
       ROTATE-cube2.1-0 ; 
       ROTATE-fuselg0.1-0 ; 
       ROTATE-fuselg1.1-0 ; 
       ROTATE-fuselg2.1-0 ; 
       ROTATE-fuselg3.1-0 ; 
       ROTATE-fuselg4.1-0 ; 
       ROTATE-fuselg5.1-0 ; 
       ROTATE-fuselg6.1-0 ; 
       ROTATE-garage1A.1-0 ; 
       ROTATE-garage1B.1-0 ; 
       ROTATE-garage1C.1-0 ; 
       ROTATE-garage1D.1-0 ; 
       ROTATE-garage1E.1-0 ; 
       ROTATE-garage2A.1-0 ; 
       ROTATE-garage2B.1-0 ; 
       ROTATE-garage2C.1-0 ; 
       ROTATE-garage2D.1-0 ; 
       ROTATE-garage2E.1-0 ; 
       ROTATE-launch1.1-0 ; 
       ROTATE-launch2.1-0 ; 
       ROTATE-ldoccon.1-0 ; 
       ROTATE-lndpad1.1-0 ; 
       ROTATE-lndpad2.1-0 ; 
       ROTATE-lndpad3.1-0 ; 
       ROTATE-lndpad4.1-0 ; 
       ROTATE-lturatt.1-0 ; 
       ROTATE-platfm0.1-0 ; 
       ROTATE-platfm1.1-0 ; 
       ROTATE-platfm2.1-0 ; 
       ROTATE-platfm3.1-0 ; 
       ROTATE-platfm4.1-0 ; 
       ROTATE-rdoccon.1-0 ; 
       ROTATE-Sab6.1-0 ; 
       ROTATE-SS1.1-0 ; 
       ROTATE-SS2.1-0 ; 
       ROTATE-SS3.1-0 ; 
       ROTATE-SS4.1-0 ; 
       ROTATE-SSa0.1-0 ; 
       ROTATE-SSa1.1-0 ; 
       ROTATE-SSa2.1-0 ; 
       ROTATE-SSa3.1-0 ; 
       ROTATE-SSa4.1-0 ; 
       ROTATE-SSab0.1-0 ; 
       ROTATE-SSab1.1-0 ; 
       ROTATE-SSab2.1-0 ; 
       ROTATE-SSab3.1-0 ; 
       ROTATE-SSab4.1-0 ; 
       ROTATE-SSab5.1-0 ; 
       ROTATE-SSab7.1-0 ; 
       ROTATE-SSab8.1-0 ; 
       ROTATE-SSab9.1-0 ; 
       ROTATE-SSf0.1-0 ; 
       ROTATE-SSf1.1-0 ; 
       ROTATE-SSf2.1-0 ; 
       ROTATE-SSf3.1-0 ; 
       ROTATE-SSf4.1-0 ; 
       ROTATE-SSfb0.2-0 ; 
       ROTATE-SSfb1.1-0 ; 
       ROTATE-SSfb2.1-0 ; 
       ROTATE-SSfb3.1-0 ; 
       ROTATE-SSfb4.1-0 ; 
       ROTATE-SSfb5.1-0 ; 
       ROTATE-SSfb6.1-0 ; 
       ROTATE-SSfb7.1-0 ; 
       ROTATE-SSfb8.1-0 ; 
       ROTATE-SSfb9.1-0 ; 
       ROTATE-SSl0.1-0 ; 
       ROTATE-SSl1.1-0 ; 
       ROTATE-SSl2.1-0 ; 
       ROTATE-SSl3.1-0 ; 
       ROTATE-SSl4.1-0 ; 
       ROTATE-SSlb0.1-0 ; 
       ROTATE-SSlb1.1-0 ; 
       ROTATE-SSlb2.1-0 ; 
       ROTATE-SSlb3.1-0 ; 
       ROTATE-SSlb4.1-0 ; 
       ROTATE-SSlb5.1-0 ; 
       ROTATE-SSlb6.1-0 ; 
       ROTATE-SSlb7.1-0 ; 
       ROTATE-SSlb8.1-0 ; 
       ROTATE-SSlb9.1-0 ; 
       ROTATE-SSr0.1-0 ; 
       ROTATE-SSr1.1-0 ; 
       ROTATE-SSr10.1-0 ; 
       ROTATE-SSr11.1-0 ; 
       ROTATE-SSr12.1-0 ; 
       ROTATE-SSr13.1-0 ; 
       ROTATE-SSr14.1-0 ; 
       ROTATE-SSr2.1-0 ; 
       ROTATE-SSr3.1-0 ; 
       ROTATE-SSr4.1-0 ; 
       ROTATE-SSr5.1-0 ; 
       ROTATE-SSr6.1-0 ; 
       ROTATE-SSr7.1-0 ; 
       ROTATE-SSr8.1-0 ; 
       ROTATE-SSr9.1-0 ; 
       ROTATE-SSrb0.1-0 ; 
       ROTATE-SSrb1.1-0 ; 
       ROTATE-SSrb2.1-0 ; 
       ROTATE-SSrb3.1-0 ; 
       ROTATE-SSrb4.1-0 ; 
       ROTATE-SSrb5.1-0 ; 
       ROTATE-SSrb6.1-0 ; 
       ROTATE-SSrb7.1-0 ; 
       ROTATE-SSrb8.1-0 ; 
       ROTATE-SSrb9.1-0 ; 
       ROTATE-SSt.1-0 ; 
       ROTATE-SSt1.1-0 ; 
       ROTATE-SSt2.1-0 ; 
       ROTATE-SSt3.1-0 ; 
       ROTATE-SSt4.1-0 ; 
       ROTATE-turwepemt1.1-0 ; 
       ROTATE-turwepemt2.1-0 ; 
       ROTATE-turwepemt3.1-0 ; 
       ROTATE-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap05/PICTURES/cap05 ; 
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap05/PICTURES/rixbay ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap05-zbuffer.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       add_garages-t2d29.1-0 ; 
       add_garages-t2d30.6-0 ; 
       add_garages-t2d31.6-0 ; 
       add_garages-t2d32.6-0 ; 
       add_garages-t2d33.3-0 ; 
       add_garages-t2d34.3-0 ; 
       add_garages-t2d35.3-0 ; 
       rix_post_sPTL-t2d1.3-0 ; 
       rix_post_sPTL-t2d10.3-0 ; 
       rix_post_sPTL-t2d11.3-0 ; 
       rix_post_sPTL-t2d12.7-0 ; 
       rix_post_sPTL-t2d14.7-0 ; 
       rix_post_sPTL-t2d19.7-0 ; 
       rix_post_sPTL-t2d2.3-0 ; 
       rix_post_sPTL-t2d20.7-0 ; 
       rix_post_sPTL-t2d21.7-0 ; 
       rix_post_sPTL-t2d22.7-0 ; 
       rix_post_sPTL-t2d23.7-0 ; 
       rix_post_sPTL-t2d24.7-0 ; 
       rix_post_sPTL-t2d25.7-0 ; 
       rix_post_sPTL-t2d26.7-0 ; 
       rix_post_sPTL-t2d27.3-0 ; 
       rix_post_sPTL-t2d28.3-0 ; 
       rix_post_sPTL-t2d3.3-0 ; 
       rix_post_sPTL-t2d4.3-0 ; 
       rix_post_sPTL-t2d5.3-0 ; 
       rix_post_sPTL-t2d6.3-0 ; 
       rix_post_sPTL-t2d7.3-0 ; 
       rix_post_sPTL-t2d8.3-0 ; 
       rix_post_sPTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 5 110 ; 
       2 6 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 3 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 3 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 8 110 ; 
       27 33 110 ; 
       28 34 110 ; 
       28 40 111 ; 
       28 40 114 ; 
       29 35 110 ; 
       30 36 110 ; 
       31 35 110 ; 
       32 3 110 ; 
       33 32 110 ; 
       34 32 110 ; 
       35 32 110 ; 
       36 32 110 ; 
       37 8 110 ; 
       38 48 110 ; 
       39 33 110 ; 
       40 34 110 ; 
       41 35 110 ; 
       42 36 110 ; 
       43 3 110 ; 
       44 43 110 ; 
       45 43 110 ; 
       46 43 110 ; 
       47 43 110 ; 
       48 3 110 ; 
       49 48 110 ; 
       50 48 110 ; 
       51 48 110 ; 
       52 48 110 ; 
       53 48 110 ; 
       54 48 110 ; 
       55 48 110 ; 
       56 48 110 ; 
       57 3 110 ; 
       58 57 110 ; 
       59 57 110 ; 
       60 57 110 ; 
       61 57 110 ; 
       62 3 110 ; 
       63 62 110 ; 
       64 62 110 ; 
       65 62 110 ; 
       66 62 110 ; 
       67 62 110 ; 
       68 62 110 ; 
       69 62 110 ; 
       70 62 110 ; 
       71 62 110 ; 
       72 3 110 ; 
       73 72 110 ; 
       74 72 110 ; 
       75 72 110 ; 
       76 72 110 ; 
       77 3 110 ; 
       78 77 110 ; 
       79 77 110 ; 
       80 77 110 ; 
       81 77 110 ; 
       82 77 110 ; 
       83 77 110 ; 
       84 77 110 ; 
       85 77 110 ; 
       86 77 110 ; 
       87 3 110 ; 
       88 87 110 ; 
       89 3 110 ; 
       90 89 110 ; 
       91 89 110 ; 
       92 89 110 ; 
       93 89 110 ; 
       94 87 110 ; 
       95 87 110 ; 
       96 87 110 ; 
       97 3 110 ; 
       98 97 110 ; 
       99 97 110 ; 
       100 97 110 ; 
       101 97 110 ; 
       102 3 110 ; 
       103 102 110 ; 
       104 102 110 ; 
       105 102 110 ; 
       106 102 110 ; 
       107 102 110 ; 
       108 102 110 ; 
       109 102 110 ; 
       110 102 110 ; 
       111 102 110 ; 
       112 3 110 ; 
       113 3 110 ; 
       114 3 110 ; 
       115 3 110 ; 
       116 3 110 ; 
       117 3 110 ; 
       118 3 110 ; 
       119 3 110 ; 
       120 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 11 300 ; 
       4 32 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       8 0 300 ; 
       9 31 300 ; 
       10 19 300 ; 
       11 30 300 ; 
       12 29 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 27 300 ; 
       13 28 300 ; 
       33 33 300 ; 
       34 12 300 ; 
       35 13 300 ; 
       36 34 300 ; 
       38 55 300 ; 
       39 2 300 ; 
       40 1 300 ; 
       41 4 300 ; 
       42 3 300 ; 
       44 75 300 ; 
       45 76 300 ; 
       46 77 300 ; 
       47 78 300 ; 
       49 50 300 ; 
       50 51 300 ; 
       51 52 300 ; 
       52 53 300 ; 
       53 54 300 ; 
       54 56 300 ; 
       55 57 300 ; 
       56 58 300 ; 
       58 40 300 ; 
       59 68 300 ; 
       60 69 300 ; 
       61 70 300 ; 
       63 83 300 ; 
       64 84 300 ; 
       65 85 300 ; 
       66 86 300 ; 
       67 87 300 ; 
       68 88 300 ; 
       69 89 300 ; 
       70 90 300 ; 
       71 91 300 ; 
       73 71 300 ; 
       74 72 300 ; 
       75 73 300 ; 
       76 74 300 ; 
       78 41 300 ; 
       79 42 300 ; 
       80 43 300 ; 
       81 44 300 ; 
       82 45 300 ; 
       83 46 300 ; 
       84 47 300 ; 
       85 48 300 ; 
       86 49 300 ; 
       88 79 300 ; 
       90 92 300 ; 
       91 92 300 ; 
       92 92 300 ; 
       93 92 300 ; 
       94 80 300 ; 
       95 81 300 ; 
       96 82 300 ; 
       98 92 300 ; 
       99 92 300 ; 
       100 92 300 ; 
       101 92 300 ; 
       103 59 300 ; 
       104 60 300 ; 
       105 61 300 ; 
       106 62 300 ; 
       107 63 300 ; 
       108 64 300 ; 
       109 65 300 ; 
       110 66 300 ; 
       111 67 300 ; 
       112 35 300 ; 
       113 36 300 ; 
       114 37 300 ; 
       115 38 300 ; 
       116 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 1 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       12 28 401 ; 
       13 29 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 15 401 ; 
       18 11 401 ; 
       19 7 401 ; 
       20 12 401 ; 
       21 14 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 13 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 312.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 315 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 317.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 320 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 322.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 325 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 327.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 330 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 332.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 335 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 337.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 340 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 156.25 0 0 SRT 0.9999999 1 1 -1.570796 3.141593 -0.7853982 0 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 45 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 126.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 25 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 30 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 45 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 50 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 132.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 135 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 15 -4 0 MPRFLG 0 ; 
       27 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 100 -6 0 MPRFLG 0 ; 
       30 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 105 -2 0 MPRFLG 0 ; 
       33 SCHEM 117.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 110 -4 0 MPRFLG 0 ; 
       35 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       36 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 195 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 110 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 97.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 90 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 251.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 247.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 250 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 252.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 255 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 192.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 182.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 185 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 187.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 190 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 192.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 197.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 200 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 202.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 231.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 227.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 230 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 232.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 235 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 147.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 137.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 140 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 142.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 145 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 147.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 150 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       69 SCHEM 152.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 155 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 157.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 241.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 237.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 240 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 242.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 245 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       77 SCHEM 215 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       78 SCHEM 205 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       79 SCHEM 207.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       80 SCHEM 210 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       81 SCHEM 212.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       82 SCHEM 215 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       83 SCHEM 217.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       84 SCHEM 220 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       85 SCHEM 222.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       86 SCHEM 225 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       87 SCHEM 261.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       88 SCHEM 257.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       89 SCHEM 303.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       90 SCHEM 300 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       91 SCHEM 302.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       92 SCHEM 305 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       93 SCHEM 307.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       94 SCHEM 260 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       95 SCHEM 262.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       96 SCHEM 265 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       97 SCHEM 293.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       98 SCHEM 290 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       99 SCHEM 292.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       100 SCHEM 295 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       101 SCHEM 297.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       102 SCHEM 170 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       103 SCHEM 160 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       104 SCHEM 162.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       105 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       106 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       107 SCHEM 170 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       108 SCHEM 172.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       109 SCHEM 175 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       110 SCHEM 177.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       111 SCHEM 180 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       112 SCHEM 277.5 -2 0 MPRFLG 0 ; 
       113 SCHEM 280 -2 0 MPRFLG 0 ; 
       114 SCHEM 282.5 -2 0 MPRFLG 0 ; 
       115 SCHEM 285 -2 0 MPRFLG 0 ; 
       116 SCHEM 287.5 -2 0 MPRFLG 0 ; 
       117 SCHEM 267.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       118 SCHEM 270 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       119 SCHEM 272.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       120 SCHEM 275 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 310 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 277.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 280 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 282.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 285 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 287.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 205 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 207.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 210 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 222.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 182.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 185 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 190 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 192.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 195 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 197.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 200 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 202.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 172.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 175 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 177.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 180 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 235 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 237.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 240 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 242.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 245 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 247.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 250 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 252.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 255 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 260 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 262.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 265 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 290 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 309 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 15 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
