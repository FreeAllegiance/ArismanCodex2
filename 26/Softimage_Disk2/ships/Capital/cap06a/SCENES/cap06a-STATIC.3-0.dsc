SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       rix_frigate_sPtL-1.3-0 ; 
       rix_frigate_sPtL-mat1.4-0 ; 
       rix_frigate_sPtL-mat10.3-0 ; 
       rix_frigate_sPtL-mat11.3-0 ; 
       rix_frigate_sPtL-mat12.3-0 ; 
       rix_frigate_sPtL-mat13.3-0 ; 
       rix_frigate_sPtL-mat14.3-0 ; 
       rix_frigate_sPtL-mat15.3-0 ; 
       rix_frigate_sPtL-mat16.3-0 ; 
       rix_frigate_sPtL-mat17.3-0 ; 
       rix_frigate_sPtL-mat18.3-0 ; 
       rix_frigate_sPtL-mat19.3-0 ; 
       rix_frigate_sPtL-mat2.3-0 ; 
       rix_frigate_sPtL-mat20.3-0 ; 
       rix_frigate_sPtL-mat21.3-0 ; 
       rix_frigate_sPtL-mat22.3-0 ; 
       rix_frigate_sPtL-mat23.3-0 ; 
       rix_frigate_sPtL-mat24.3-0 ; 
       rix_frigate_sPtL-mat25.3-0 ; 
       rix_frigate_sPtL-mat26.3-0 ; 
       rix_frigate_sPtL-mat27.3-0 ; 
       rix_frigate_sPtL-mat28.3-0 ; 
       rix_frigate_sPtL-mat29.3-0 ; 
       rix_frigate_sPtL-mat3.3-0 ; 
       rix_frigate_sPtL-mat30.3-0 ; 
       rix_frigate_sPtL-mat31.3-0 ; 
       rix_frigate_sPtL-mat32.3-0 ; 
       rix_frigate_sPtL-mat33.3-0 ; 
       rix_frigate_sPtL-mat34.3-0 ; 
       rix_frigate_sPtL-mat35.3-0 ; 
       rix_frigate_sPtL-mat36.3-0 ; 
       rix_frigate_sPtL-mat37.3-0 ; 
       rix_frigate_sPtL-mat38.3-0 ; 
       rix_frigate_sPtL-mat39.3-0 ; 
       rix_frigate_sPtL-mat4.3-0 ; 
       rix_frigate_sPtL-mat40.3-0 ; 
       rix_frigate_sPtL-mat41.3-0 ; 
       rix_frigate_sPtL-mat42.3-0 ; 
       rix_frigate_sPtL-mat43.3-0 ; 
       rix_frigate_sPtL-mat44.3-0 ; 
       rix_frigate_sPtL-mat45.3-0 ; 
       rix_frigate_sPtL-mat46.3-0 ; 
       rix_frigate_sPtL-mat47.3-0 ; 
       rix_frigate_sPtL-mat48.3-0 ; 
       rix_frigate_sPtL-mat49.4-0 ; 
       rix_frigate_sPtL-mat5.3-0 ; 
       rix_frigate_sPtL-mat56.3-0 ; 
       rix_frigate_sPtL-mat57.3-0 ; 
       rix_frigate_sPtL-mat58.3-0 ; 
       rix_frigate_sPtL-mat59.3-0 ; 
       rix_frigate_sPtL-mat6.3-0 ; 
       rix_frigate_sPtL-mat7.3-0 ; 
       rix_frigate_sPtL-mat8.3-0 ; 
       rix_frigate_sPtL-mat9.3-0 ; 
       STATIC-2.1-0 ; 
       STATIC-5.1-0 ; 
       STATIC-6.1-0 ; 
       STATIC-7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap06a-bfuselg.2-0 ; 
       cap06a-bfuselg1.1-0 ; 
       cap06a-cap06a.12-0 ROOT ; 
       cap06a-cone1.1-0 ; 
       cap06a-cone4.1-0 ; 
       cap06a-cone5.1-0 ; 
       cap06a-cone6.1-0 ; 
       cap06a-contwr1.1-0 ; 
       cap06a-contwr2.1-0 ; 
       cap06a-fbslicer.1-0 ; 
       cap06a-fbslicer0.1-0 ; 
       cap06a-flsicer.1-0 ; 
       cap06a-flslicer0.1-0 ; 
       cap06a-frslicer.1-0 ; 
       cap06a-frslicer0.1-0 ; 
       cap06a-ftslicer.1-0 ; 
       cap06a-ftslicer0.1-0 ; 
       cap06a-mfuselg.1-0 ; 
       cap06a-slicer.1-0 ; 
       cap06a-slicer1.1-0 ; 
       cap06a-SSf.2-0 ; 
       cap06a-SSt0.2-0 ; 
       cap06a-tfuselg.2-0 ; 
       cap06a-tfuselg0.1-0 ; 
       cap06a-tfuselg1.1-0 ; 
       cap06a-tfuselg2.1-0 ; 
       cap06a-tlfuselg.1-0 ; 
       cap06a-trfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap06a/PICTURES/cap06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap06a-STATIC.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 57     
       rix_frigate_sPtL-t2d1.6-0 ; 
       rix_frigate_sPtL-t2d10.6-0 ; 
       rix_frigate_sPtL-t2d11.6-0 ; 
       rix_frigate_sPtL-t2d12.6-0 ; 
       rix_frigate_sPtL-t2d13.6-0 ; 
       rix_frigate_sPtL-t2d14.6-0 ; 
       rix_frigate_sPtL-t2d15.6-0 ; 
       rix_frigate_sPtL-t2d16.6-0 ; 
       rix_frigate_sPtL-t2d17.6-0 ; 
       rix_frigate_sPtL-t2d18.5-0 ; 
       rix_frigate_sPtL-t2d19.5-0 ; 
       rix_frigate_sPtL-t2d2.6-0 ; 
       rix_frigate_sPtL-t2d20.5-0 ; 
       rix_frigate_sPtL-t2d21.5-0 ; 
       rix_frigate_sPtL-t2d22.6-0 ; 
       rix_frigate_sPtL-t2d23.6-0 ; 
       rix_frigate_sPtL-t2d24.6-0 ; 
       rix_frigate_sPtL-t2d25.6-0 ; 
       rix_frigate_sPtL-t2d26.6-0 ; 
       rix_frigate_sPtL-t2d27.6-0 ; 
       rix_frigate_sPtL-t2d28.6-0 ; 
       rix_frigate_sPtL-t2d29.6-0 ; 
       rix_frigate_sPtL-t2d3.6-0 ; 
       rix_frigate_sPtL-t2d30.6-0 ; 
       rix_frigate_sPtL-t2d31.6-0 ; 
       rix_frigate_sPtL-t2d32.6-0 ; 
       rix_frigate_sPtL-t2d33.6-0 ; 
       rix_frigate_sPtL-t2d34.6-0 ; 
       rix_frigate_sPtL-t2d35.6-0 ; 
       rix_frigate_sPtL-t2d36.6-0 ; 
       rix_frigate_sPtL-t2d37.6-0 ; 
       rix_frigate_sPtL-t2d38.6-0 ; 
       rix_frigate_sPtL-t2d39.6-0 ; 
       rix_frigate_sPtL-t2d4.6-0 ; 
       rix_frigate_sPtL-t2d40.6-0 ; 
       rix_frigate_sPtL-t2d41.6-0 ; 
       rix_frigate_sPtL-t2d42.6-0 ; 
       rix_frigate_sPtL-t2d43.6-0 ; 
       rix_frigate_sPtL-t2d44.6-0 ; 
       rix_frigate_sPtL-t2d45.5-0 ; 
       rix_frigate_sPtL-t2d46.5-0 ; 
       rix_frigate_sPtL-t2d47.5-0 ; 
       rix_frigate_sPtL-t2d48.7-0 ; 
       rix_frigate_sPtL-t2d5.6-0 ; 
       rix_frigate_sPtL-t2d55.6-0 ; 
       rix_frigate_sPtL-t2d56.5-0 ; 
       rix_frigate_sPtL-t2d57.6-0 ; 
       rix_frigate_sPtL-t2d58.6-0 ; 
       rix_frigate_sPtL-t2d59.6-0 ; 
       rix_frigate_sPtL-t2d6.6-0 ; 
       rix_frigate_sPtL-t2d7.6-0 ; 
       rix_frigate_sPtL-t2d8.6-0 ; 
       rix_frigate_sPtL-t2d9.6-0 ; 
       STATIC-t2d60.1-0 ; 
       STATIC-t2d63.1-0 ; 
       STATIC-t2d64.1-0 ; 
       STATIC-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 0 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 22 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 19 110 ; 
       11 12 110 ; 
       12 19 110 ; 
       13 14 110 ; 
       14 19 110 ; 
       15 16 110 ; 
       16 19 110 ; 
       17 22 110 ; 
       18 1 110 ; 
       19 22 110 ; 
       20 0 110 ; 
       21 8 110 ; 
       22 2 110 ; 
       23 22 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 11 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 41 300 ; 
       1 1 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 47 300 ; 
       2 0 300 ; 
       3 54 300 ; 
       4 55 300 ; 
       5 56 300 ; 
       6 57 300 ; 
       7 0 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 49 300 ; 
       8 0 300 ; 
       8 50 300 ; 
       8 51 300 ; 
       8 52 300 ; 
       8 53 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       9 0 300 ; 
       9 22 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       11 0 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 26 300 ; 
       13 0 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 46 300 ; 
       15 0 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       15 27 300 ; 
       17 0 300 ; 
       17 8 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       18 1 300 ; 
       18 44 300 ; 
       19 0 300 ; 
       19 28 300 ; 
       19 29 300 ; 
       19 30 300 ; 
       19 31 300 ; 
       19 32 300 ; 
       19 33 300 ; 
       22 0 300 ; 
       22 12 300 ; 
       22 23 300 ; 
       22 34 300 ; 
       22 45 300 ; 
       22 7 300 ; 
       24 0 300 ; 
       24 37 300 ; 
       24 38 300 ; 
       24 48 300 ; 
       25 0 300 ; 
       25 35 300 ; 
       25 36 300 ; 
       26 0 300 ; 
       26 39 300 ; 
       27 0 300 ; 
       27 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 53 400 ; 
       4 54 400 ; 
       5 55 400 ; 
       6 56 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 48 401 ; 
       2 2 401 ; 
       3 50 401 ; 
       4 51 401 ; 
       5 4 401 ; 
       6 3 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 0 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 11 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 27 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 22 401 ; 
       35 34 401 ; 
       36 32 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 33 401 ; 
       46 44 401 ; 
       47 45 401 ; 
       48 46 401 ; 
       49 47 401 ; 
       50 43 401 ; 
       51 49 401 ; 
       52 52 401 ; 
       53 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 -0.7384528 MPRFLG 0 ; 
       3 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 MPRFLG 0 ; 
       6 SCHEM 40 -2 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 25 -8 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 30 -4 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 MPRFLG 0 ; 
       19 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 10 -6 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 39 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
