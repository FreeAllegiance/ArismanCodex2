SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap06a-cap06a.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 5     
       rix_frigate_sT-cam_int1.5-0 ROOT ; 
       rix_frigate_sT-cam_int1_1.5-0 ROOT ; 
       rix_frigate_sT-Camera1.5-0 ROOT ; 
       rix_frigate_sT-Camera1_1.1-0 ; 
       rix_frigate_sT-Camera1_2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_frigate_sPtL-light1.5-0 ROOT ; 
       rix_frigate_sPtL-light2.5-0 ROOT ; 
       rix_frigate_sPtL-light3.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 83     
       add_frames-mat100.1-0 ; 
       add_frames-mat102.1-0 ; 
       add_frames-mat103.1-0 ; 
       add_frames-mat104.1-0 ; 
       add_frames-mat108.1-0 ; 
       add_frames-mat110.1-0 ; 
       add_frames-mat111.1-0 ; 
       add_frames-mat112.1-0 ; 
       add_frames-mat75.1-0 ; 
       add_frames-mat79.1-0 ; 
       add_frames-mat80.1-0 ; 
       add_frames-mat81.1-0 ; 
       add_frames-mat82.1-0 ; 
       add_frames-mat84.1-0 ; 
       add_frames-mat85.1-0 ; 
       add_frames-mat86.1-0 ; 
       add_frames-mat87.1-0 ; 
       add_frames-mat88.1-0 ; 
       add_frames-mat89.1-0 ; 
       add_frames-mat90.1-0 ; 
       add_frames-mat91.1-0 ; 
       add_frames-mat92.1-0 ; 
       add_frames-mat93.1-0 ; 
       add_frames-mat94.1-0 ; 
       add_frames-mat95.1-0 ; 
       add_frames-mat96.1-0 ; 
       add_frames-mat97.1-0 ; 
       add_frames-mat98.1-0 ; 
       add_frames-mat99.1-0 ; 
       rix_frigate_sPtL-1.1-0 ; 
       rix_frigate_sPtL-mat1.2-0 ; 
       rix_frigate_sPtL-mat10.1-0 ; 
       rix_frigate_sPtL-mat11.1-0 ; 
       rix_frigate_sPtL-mat12.1-0 ; 
       rix_frigate_sPtL-mat13.1-0 ; 
       rix_frigate_sPtL-mat14.1-0 ; 
       rix_frigate_sPtL-mat15.1-0 ; 
       rix_frigate_sPtL-mat16.1-0 ; 
       rix_frigate_sPtL-mat17.1-0 ; 
       rix_frigate_sPtL-mat18.1-0 ; 
       rix_frigate_sPtL-mat19.1-0 ; 
       rix_frigate_sPtL-mat2.1-0 ; 
       rix_frigate_sPtL-mat20.1-0 ; 
       rix_frigate_sPtL-mat21.1-0 ; 
       rix_frigate_sPtL-mat22.1-0 ; 
       rix_frigate_sPtL-mat23.1-0 ; 
       rix_frigate_sPtL-mat24.1-0 ; 
       rix_frigate_sPtL-mat25.1-0 ; 
       rix_frigate_sPtL-mat26.1-0 ; 
       rix_frigate_sPtL-mat27.1-0 ; 
       rix_frigate_sPtL-mat28.1-0 ; 
       rix_frigate_sPtL-mat29.1-0 ; 
       rix_frigate_sPtL-mat3.1-0 ; 
       rix_frigate_sPtL-mat30.1-0 ; 
       rix_frigate_sPtL-mat31.1-0 ; 
       rix_frigate_sPtL-mat32.1-0 ; 
       rix_frigate_sPtL-mat33.1-0 ; 
       rix_frigate_sPtL-mat34.1-0 ; 
       rix_frigate_sPtL-mat35.1-0 ; 
       rix_frigate_sPtL-mat36.1-0 ; 
       rix_frigate_sPtL-mat37.1-0 ; 
       rix_frigate_sPtL-mat38.1-0 ; 
       rix_frigate_sPtL-mat39.1-0 ; 
       rix_frigate_sPtL-mat4.1-0 ; 
       rix_frigate_sPtL-mat40.1-0 ; 
       rix_frigate_sPtL-mat41.1-0 ; 
       rix_frigate_sPtL-mat42.1-0 ; 
       rix_frigate_sPtL-mat43.1-0 ; 
       rix_frigate_sPtL-mat44.1-0 ; 
       rix_frigate_sPtL-mat45.1-0 ; 
       rix_frigate_sPtL-mat46.1-0 ; 
       rix_frigate_sPtL-mat47.1-0 ; 
       rix_frigate_sPtL-mat48.1-0 ; 
       rix_frigate_sPtL-mat49.2-0 ; 
       rix_frigate_sPtL-mat5.1-0 ; 
       rix_frigate_sPtL-mat56.1-0 ; 
       rix_frigate_sPtL-mat57.1-0 ; 
       rix_frigate_sPtL-mat58.1-0 ; 
       rix_frigate_sPtL-mat59.1-0 ; 
       rix_frigate_sPtL-mat6.1-0 ; 
       rix_frigate_sPtL-mat7.1-0 ; 
       rix_frigate_sPtL-mat8.1-0 ; 
       rix_frigate_sPtL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       cap06a-bfuselg.2-0 ; 
       cap06a-bfuselg1.1-0 ; 
       cap06a-cap06a.4-0 ROOT ; 
       cap06a-contwr1.1-0 ; 
       cap06a-contwr2.1-0 ; 
       cap06a-fbslicer.1-0 ; 
       cap06a-fbslicer0.1-0 ; 
       cap06a-flsicer.1-0 ; 
       cap06a-flslicer0.1-0 ; 
       cap06a-frslicer.1-0 ; 
       cap06a-frslicer0.1-0 ; 
       cap06a-ftslicer.1-0 ; 
       cap06a-ftslicer0.1-0 ; 
       cap06a-mfuselg.1-0 ; 
       cap06a-missemt.1-0 ; 
       cap06a-slicer.1-0 ; 
       cap06a-slicer1.1-0 ; 
       cap06a-smoke1.1-0 ; 
       cap06a-smoke2.1-0 ; 
       cap06a-SSb1.2-0 ; 
       cap06a-SSb2.2-0 ; 
       cap06a-SSb3.2-0 ; 
       cap06a-SSb4.2-0 ; 
       cap06a-SSf.2-0 ; 
       cap06a-SSfb.1-0 ; 
       cap06a-SSfl.1-0 ; 
       cap06a-SSfr.1-0 ; 
       cap06a-SSft.1-0 ; 
       cap06a-SSl1.1-0 ; 
       cap06a-SSl2.1-0 ; 
       cap06a-SSl3.1-0 ; 
       cap06a-SSl4.1-0 ; 
       cap06a-SSl5.1-0 ; 
       cap06a-SSl6.1-0 ; 
       cap06a-SSl7.1-0 ; 
       cap06a-SSl8.1-0 ; 
       cap06a-SSl9.1-0 ; 
       cap06a-SSlastrobe.1-0 ; 
       cap06a-SSlb.1-0 ; 
       cap06a-SSr1.1-0 ; 
       cap06a-SSr2.1-0 ; 
       cap06a-SSr3.1-0 ; 
       cap06a-SSr4.1-0 ; 
       cap06a-SSr5.1-0 ; 
       cap06a-SSr6.1-0 ; 
       cap06a-SSr7.1-0 ; 
       cap06a-SSr8.1-0 ; 
       cap06a-SSrastrobe.1-0 ; 
       cap06a-SSrb.1-0 ; 
       cap06a-SSt0.2-0 ; 
       cap06a-tfuselg.2-0 ; 
       cap06a-tfuselg0.1-0 ; 
       cap06a-tfuselg1.1-0 ; 
       cap06a-tfuselg2.1-0 ; 
       cap06a-thrust1.1-0 ; 
       cap06a-thrust2.1-0 ; 
       cap06a-tlfuselg.1-0 ; 
       cap06a-trail.1-0 ; 
       cap06a-trfuselg.1-0 ; 
       cap06a-turwepemt1.1-0 ; 
       cap06a-turwepemt2.1-0 ; 
       cap06a-wepemt1.1-0 ; 
       cap06a-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap06a/PICTURES/cap06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap06a-add_frames.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       rix_frigate_sPtL-t2d1.2-0 ; 
       rix_frigate_sPtL-t2d10.2-0 ; 
       rix_frigate_sPtL-t2d11.2-0 ; 
       rix_frigate_sPtL-t2d12.2-0 ; 
       rix_frigate_sPtL-t2d13.2-0 ; 
       rix_frigate_sPtL-t2d14.2-0 ; 
       rix_frigate_sPtL-t2d15.2-0 ; 
       rix_frigate_sPtL-t2d16.2-0 ; 
       rix_frigate_sPtL-t2d17.2-0 ; 
       rix_frigate_sPtL-t2d18.1-0 ; 
       rix_frigate_sPtL-t2d19.1-0 ; 
       rix_frigate_sPtL-t2d2.2-0 ; 
       rix_frigate_sPtL-t2d20.1-0 ; 
       rix_frigate_sPtL-t2d21.1-0 ; 
       rix_frigate_sPtL-t2d22.2-0 ; 
       rix_frigate_sPtL-t2d23.2-0 ; 
       rix_frigate_sPtL-t2d24.2-0 ; 
       rix_frigate_sPtL-t2d25.2-0 ; 
       rix_frigate_sPtL-t2d26.2-0 ; 
       rix_frigate_sPtL-t2d27.2-0 ; 
       rix_frigate_sPtL-t2d28.2-0 ; 
       rix_frigate_sPtL-t2d29.2-0 ; 
       rix_frigate_sPtL-t2d3.2-0 ; 
       rix_frigate_sPtL-t2d30.2-0 ; 
       rix_frigate_sPtL-t2d31.2-0 ; 
       rix_frigate_sPtL-t2d32.2-0 ; 
       rix_frigate_sPtL-t2d33.2-0 ; 
       rix_frigate_sPtL-t2d34.2-0 ; 
       rix_frigate_sPtL-t2d35.2-0 ; 
       rix_frigate_sPtL-t2d36.2-0 ; 
       rix_frigate_sPtL-t2d37.2-0 ; 
       rix_frigate_sPtL-t2d38.2-0 ; 
       rix_frigate_sPtL-t2d39.2-0 ; 
       rix_frigate_sPtL-t2d4.2-0 ; 
       rix_frigate_sPtL-t2d40.2-0 ; 
       rix_frigate_sPtL-t2d41.2-0 ; 
       rix_frigate_sPtL-t2d42.2-0 ; 
       rix_frigate_sPtL-t2d43.2-0 ; 
       rix_frigate_sPtL-t2d44.2-0 ; 
       rix_frigate_sPtL-t2d45.1-0 ; 
       rix_frigate_sPtL-t2d46.1-0 ; 
       rix_frigate_sPtL-t2d47.1-0 ; 
       rix_frigate_sPtL-t2d48.3-0 ; 
       rix_frigate_sPtL-t2d5.2-0 ; 
       rix_frigate_sPtL-t2d55.2-0 ; 
       rix_frigate_sPtL-t2d56.1-0 ; 
       rix_frigate_sPtL-t2d57.2-0 ; 
       rix_frigate_sPtL-t2d58.2-0 ; 
       rix_frigate_sPtL-t2d59.2-0 ; 
       rix_frigate_sPtL-t2d6.2-0 ; 
       rix_frigate_sPtL-t2d7.2-0 ; 
       rix_frigate_sPtL-t2d8.2-0 ; 
       rix_frigate_sPtL-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 50 110 ; 
       57 2 110 ; 
       1 0 110 ; 
       54 2 110 ; 
       55 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       14 2 110 ; 
       61 2 110 ; 
       3 50 110 ; 
       4 3 110 ; 
       5 6 110 ; 
       6 16 110 ; 
       7 8 110 ; 
       8 16 110 ; 
       9 10 110 ; 
       10 16 110 ; 
       11 12 110 ; 
       12 16 110 ; 
       13 50 110 ; 
       15 1 110 ; 
       16 50 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 23 110 ; 
       23 0 110 ; 
       24 5 110 ; 
       25 7 110 ; 
       26 9 110 ; 
       27 11 110 ; 
       28 49 110 ; 
       29 49 110 ; 
       30 49 110 ; 
       31 49 110 ; 
       32 49 110 ; 
       33 49 110 ; 
       34 49 110 ; 
       35 49 110 ; 
       36 49 110 ; 
       37 50 110 ; 
       38 0 110 ; 
       39 49 110 ; 
       40 49 110 ; 
       41 49 110 ; 
       42 49 110 ; 
       43 49 110 ; 
       44 49 110 ; 
       45 49 110 ; 
       46 49 110 ; 
       47 50 110 ; 
       48 0 110 ; 
       49 4 110 ; 
       50 2 110 ; 
       51 50 110 ; 
       52 51 110 ; 
       53 51 110 ; 
       56 51 110 ; 
       58 51 110 ; 
       62 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 30 300 ; 
       0 40 300 ; 
       0 42 300 ; 
       0 43 300 ; 
       0 44 300 ; 
       0 70 300 ; 
       1 30 300 ; 
       1 71 300 ; 
       1 72 300 ; 
       1 76 300 ; 
       2 29 300 ; 
       3 29 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 78 300 ; 
       4 29 300 ; 
       4 79 300 ; 
       4 80 300 ; 
       4 81 300 ; 
       4 82 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       5 29 300 ; 
       5 51 300 ; 
       5 53 300 ; 
       5 54 300 ; 
       7 29 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       7 55 300 ; 
       9 29 300 ; 
       9 49 300 ; 
       9 50 300 ; 
       9 75 300 ; 
       11 29 300 ; 
       11 47 300 ; 
       11 48 300 ; 
       11 56 300 ; 
       13 29 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       13 39 300 ; 
       15 30 300 ; 
       15 73 300 ; 
       16 29 300 ; 
       16 57 300 ; 
       16 58 300 ; 
       16 59 300 ; 
       16 60 300 ; 
       16 61 300 ; 
       16 62 300 ; 
       19 9 300 ; 
       20 10 300 ; 
       21 11 300 ; 
       22 12 300 ; 
       24 6 300 ; 
       25 4 300 ; 
       26 5 300 ; 
       27 7 300 ; 
       28 13 300 ; 
       29 14 300 ; 
       30 15 300 ; 
       31 16 300 ; 
       32 17 300 ; 
       33 18 300 ; 
       34 19 300 ; 
       35 20 300 ; 
       36 21 300 ; 
       37 8 300 ; 
       38 2 300 ; 
       39 22 300 ; 
       40 23 300 ; 
       41 24 300 ; 
       42 25 300 ; 
       43 26 300 ; 
       44 27 300 ; 
       45 28 300 ; 
       46 0 300 ; 
       47 1 300 ; 
       48 3 300 ; 
       50 29 300 ; 
       50 41 300 ; 
       50 52 300 ; 
       50 63 300 ; 
       50 74 300 ; 
       50 36 300 ; 
       52 29 300 ; 
       52 66 300 ; 
       52 67 300 ; 
       52 77 300 ; 
       53 29 300 ; 
       53 64 300 ; 
       53 65 300 ; 
       56 29 300 ; 
       56 68 300 ; 
       58 29 300 ; 
       58 69 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       3 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       29 48 401 ; 
       31 2 401 ; 
       32 50 401 ; 
       33 51 401 ; 
       34 4 401 ; 
       35 3 401 ; 
       36 5 401 ; 
       37 6 401 ; 
       38 7 401 ; 
       39 8 401 ; 
       40 9 401 ; 
       41 0 401 ; 
       42 10 401 ; 
       43 12 401 ; 
       44 13 401 ; 
       45 14 401 ; 
       46 15 401 ; 
       47 16 401 ; 
       48 17 401 ; 
       49 18 401 ; 
       50 19 401 ; 
       51 20 401 ; 
       52 11 401 ; 
       53 21 401 ; 
       54 23 401 ; 
       55 24 401 ; 
       56 25 401 ; 
       57 27 401 ; 
       58 26 401 ; 
       59 28 401 ; 
       60 29 401 ; 
       61 30 401 ; 
       62 31 401 ; 
       63 22 401 ; 
       64 34 401 ; 
       65 32 401 ; 
       66 35 401 ; 
       67 36 401 ; 
       68 37 401 ; 
       69 38 401 ; 
       70 39 401 ; 
       71 40 401 ; 
       72 41 401 ; 
       73 42 401 ; 
       74 33 401 ; 
       75 44 401 ; 
       76 45 401 ; 
       77 46 401 ; 
       78 47 401 ; 
       79 43 401 ; 
       80 49 401 ; 
       81 52 401 ; 
       82 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       57 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -6 0 MPRFLG 0 ; 
       54 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 110 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 65 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 85 -8 0 MPRFLG 0 ; 
       8 SCHEM 85 -6 0 MPRFLG 0 ; 
       9 SCHEM 90 -8 0 MPRFLG 0 ; 
       10 SCHEM 90 -6 0 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 95 -4 0 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 MPRFLG 0 ; 
       16 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 35 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 87.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 85 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 90 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 65 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 70 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 72.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 80 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 75 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 12.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 45 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 47.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 50 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 60 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 57.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 55 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 52.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 10 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       50 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       51 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       52 SCHEM 15 -6 0 MPRFLG 0 ; 
       53 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       56 SCHEM 20 -6 0 MPRFLG 0 ; 
       58 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       62 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       29 SCHEM 121.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 89 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 121.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 121.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 63 63 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
