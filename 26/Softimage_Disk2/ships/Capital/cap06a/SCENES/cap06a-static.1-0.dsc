SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.11-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       rix_frigate_sPtL-1.3-0 ; 
       rix_frigate_sPtL-mat1.4-0 ; 
       rix_frigate_sPtL-mat10.3-0 ; 
       rix_frigate_sPtL-mat11.3-0 ; 
       rix_frigate_sPtL-mat12.3-0 ; 
       rix_frigate_sPtL-mat13.3-0 ; 
       rix_frigate_sPtL-mat14.3-0 ; 
       rix_frigate_sPtL-mat15.3-0 ; 
       rix_frigate_sPtL-mat16.3-0 ; 
       rix_frigate_sPtL-mat17.3-0 ; 
       rix_frigate_sPtL-mat18.3-0 ; 
       rix_frigate_sPtL-mat19.3-0 ; 
       rix_frigate_sPtL-mat2.3-0 ; 
       rix_frigate_sPtL-mat20.3-0 ; 
       rix_frigate_sPtL-mat21.3-0 ; 
       rix_frigate_sPtL-mat22.3-0 ; 
       rix_frigate_sPtL-mat23.3-0 ; 
       rix_frigate_sPtL-mat24.3-0 ; 
       rix_frigate_sPtL-mat25.3-0 ; 
       rix_frigate_sPtL-mat26.3-0 ; 
       rix_frigate_sPtL-mat27.3-0 ; 
       rix_frigate_sPtL-mat28.3-0 ; 
       rix_frigate_sPtL-mat29.3-0 ; 
       rix_frigate_sPtL-mat3.3-0 ; 
       rix_frigate_sPtL-mat30.3-0 ; 
       rix_frigate_sPtL-mat31.3-0 ; 
       rix_frigate_sPtL-mat32.3-0 ; 
       rix_frigate_sPtL-mat33.3-0 ; 
       rix_frigate_sPtL-mat34.3-0 ; 
       rix_frigate_sPtL-mat35.3-0 ; 
       rix_frigate_sPtL-mat36.3-0 ; 
       rix_frigate_sPtL-mat37.3-0 ; 
       rix_frigate_sPtL-mat38.3-0 ; 
       rix_frigate_sPtL-mat39.3-0 ; 
       rix_frigate_sPtL-mat4.3-0 ; 
       rix_frigate_sPtL-mat40.3-0 ; 
       rix_frigate_sPtL-mat41.3-0 ; 
       rix_frigate_sPtL-mat42.3-0 ; 
       rix_frigate_sPtL-mat43.3-0 ; 
       rix_frigate_sPtL-mat44.3-0 ; 
       rix_frigate_sPtL-mat45.3-0 ; 
       rix_frigate_sPtL-mat46.3-0 ; 
       rix_frigate_sPtL-mat5.3-0 ; 
       rix_frigate_sPtL-mat56.3-0 ; 
       rix_frigate_sPtL-mat58.3-0 ; 
       rix_frigate_sPtL-mat59.3-0 ; 
       rix_frigate_sPtL-mat6.3-0 ; 
       rix_frigate_sPtL-mat7.3-0 ; 
       rix_frigate_sPtL-mat8.3-0 ; 
       rix_frigate_sPtL-mat9.3-0 ; 
       scaled-2.1-0 ; 
       scaled-5.1-0 ; 
       scaled-6.1-0 ; 
       scaled-7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       cap06a-bfuselg.2-0 ; 
       cap06a-cap06a.15-0 ROOT ; 
       cap06a-cone1.1-0 ; 
       cap06a-cone4.1-0 ; 
       cap06a-cone5.1-0 ; 
       cap06a-cone6.1-0 ; 
       cap06a-contwr1.1-0 ; 
       cap06a-contwr2.1-0 ; 
       cap06a-fbslicer.1-0 ; 
       cap06a-fbslicer0.1-0 ; 
       cap06a-flsicer.1-0 ; 
       cap06a-flslicer0.1-0 ; 
       cap06a-frslicer.1-0 ; 
       cap06a-frslicer0.1-0 ; 
       cap06a-ftslicer.1-0 ; 
       cap06a-ftslicer0.1-0 ; 
       cap06a-mfuselg.1-0 ; 
       cap06a-slicer1.1-0 ; 
       cap06a-SSf.2-0 ; 
       cap06a-SSt0.2-0 ; 
       cap06a-tfuselg.2-0 ; 
       cap06a-tfuselg0.1-0 ; 
       cap06a-tfuselg1.1-0 ; 
       cap06a-tfuselg2.1-0 ; 
       cap06a-tlfuselg.1-0 ; 
       cap06a-trfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage_Archive/Softimage_Disk2/ships/Capital/cap06a/PICTURES/cap06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap06a-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       rix_frigate_sPtL-t2d1.6-0 ; 
       rix_frigate_sPtL-t2d10.6-0 ; 
       rix_frigate_sPtL-t2d11.6-0 ; 
       rix_frigate_sPtL-t2d12.6-0 ; 
       rix_frigate_sPtL-t2d13.6-0 ; 
       rix_frigate_sPtL-t2d14.6-0 ; 
       rix_frigate_sPtL-t2d15.6-0 ; 
       rix_frigate_sPtL-t2d16.6-0 ; 
       rix_frigate_sPtL-t2d17.6-0 ; 
       rix_frigate_sPtL-t2d18.5-0 ; 
       rix_frigate_sPtL-t2d19.5-0 ; 
       rix_frigate_sPtL-t2d2.6-0 ; 
       rix_frigate_sPtL-t2d20.5-0 ; 
       rix_frigate_sPtL-t2d21.5-0 ; 
       rix_frigate_sPtL-t2d22.6-0 ; 
       rix_frigate_sPtL-t2d23.6-0 ; 
       rix_frigate_sPtL-t2d24.6-0 ; 
       rix_frigate_sPtL-t2d25.6-0 ; 
       rix_frigate_sPtL-t2d26.6-0 ; 
       rix_frigate_sPtL-t2d27.6-0 ; 
       rix_frigate_sPtL-t2d28.6-0 ; 
       rix_frigate_sPtL-t2d29.6-0 ; 
       rix_frigate_sPtL-t2d3.6-0 ; 
       rix_frigate_sPtL-t2d30.6-0 ; 
       rix_frigate_sPtL-t2d31.6-0 ; 
       rix_frigate_sPtL-t2d32.6-0 ; 
       rix_frigate_sPtL-t2d33.6-0 ; 
       rix_frigate_sPtL-t2d34.6-0 ; 
       rix_frigate_sPtL-t2d35.6-0 ; 
       rix_frigate_sPtL-t2d36.6-0 ; 
       rix_frigate_sPtL-t2d37.6-0 ; 
       rix_frigate_sPtL-t2d38.6-0 ; 
       rix_frigate_sPtL-t2d39.6-0 ; 
       rix_frigate_sPtL-t2d4.6-0 ; 
       rix_frigate_sPtL-t2d40.6-0 ; 
       rix_frigate_sPtL-t2d41.6-0 ; 
       rix_frigate_sPtL-t2d42.6-0 ; 
       rix_frigate_sPtL-t2d43.6-0 ; 
       rix_frigate_sPtL-t2d44.6-0 ; 
       rix_frigate_sPtL-t2d45.5-0 ; 
       rix_frigate_sPtL-t2d5.6-0 ; 
       rix_frigate_sPtL-t2d55.6-0 ; 
       rix_frigate_sPtL-t2d57.6-0 ; 
       rix_frigate_sPtL-t2d58.6-0 ; 
       rix_frigate_sPtL-t2d59.6-0 ; 
       rix_frigate_sPtL-t2d6.6-0 ; 
       rix_frigate_sPtL-t2d7.6-0 ; 
       rix_frigate_sPtL-t2d8.6-0 ; 
       rix_frigate_sPtL-t2d9.6-0 ; 
       scaled-t2d60.2-0 ; 
       scaled-t2d63.2-0 ; 
       scaled-t2d64.2-0 ; 
       scaled-t2d65.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 20 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 17 110 ; 
       10 11 110 ; 
       11 17 110 ; 
       12 13 110 ; 
       13 17 110 ; 
       14 15 110 ; 
       15 17 110 ; 
       16 20 110 ; 
       17 20 110 ; 
       18 0 110 ; 
       19 7 110 ; 
       20 1 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 11 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 41 300 ; 
       1 0 300 ; 
       2 50 300 ; 
       3 51 300 ; 
       4 52 300 ; 
       5 53 300 ; 
       6 0 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 45 300 ; 
       7 0 300 ; 
       7 46 300 ; 
       7 47 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       8 0 300 ; 
       8 22 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       10 0 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 26 300 ; 
       12 0 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 43 300 ; 
       14 0 300 ; 
       14 18 300 ; 
       14 19 300 ; 
       14 27 300 ; 
       16 0 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       17 0 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       17 30 300 ; 
       17 31 300 ; 
       17 32 300 ; 
       17 33 300 ; 
       20 0 300 ; 
       20 12 300 ; 
       20 23 300 ; 
       20 34 300 ; 
       20 42 300 ; 
       20 7 300 ; 
       22 0 300 ; 
       22 37 300 ; 
       22 38 300 ; 
       22 44 300 ; 
       23 0 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       24 0 300 ; 
       24 39 300 ; 
       25 0 300 ; 
       25 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 49 400 ; 
       3 50 400 ; 
       4 51 400 ; 
       5 52 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 44 401 ; 
       2 2 401 ; 
       3 46 401 ; 
       4 47 401 ; 
       5 4 401 ; 
       6 3 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 0 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 11 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 27 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 22 401 ; 
       35 34 401 ; 
       36 32 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 33 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 40 401 ; 
       47 45 401 ; 
       48 48 401 ; 
       49 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 -0.7384528 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 25 -8 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 MPRFLG 0 ; 
       20 SCHEM 15 -2 0 MPRFLG 0 ; 
       21 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -6 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 39 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
