SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap06a-cap06a.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       turret_loc-cam_int1.1-0 ROOT ; 
       turret_loc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_frigate_sPtL-light1.8-0 ROOT ; 
       rix_frigate_sPtL-light2.8-0 ROOT ; 
       rix_frigate_sPtL-light3.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 86     
       rix_frigate_sPtL-1.2-0 ; 
       rix_frigate_sPtL-mat1.3-0 ; 
       rix_frigate_sPtL-mat10.2-0 ; 
       rix_frigate_sPtL-mat11.2-0 ; 
       rix_frigate_sPtL-mat12.2-0 ; 
       rix_frigate_sPtL-mat13.2-0 ; 
       rix_frigate_sPtL-mat14.2-0 ; 
       rix_frigate_sPtL-mat15.2-0 ; 
       rix_frigate_sPtL-mat16.2-0 ; 
       rix_frigate_sPtL-mat17.2-0 ; 
       rix_frigate_sPtL-mat18.2-0 ; 
       rix_frigate_sPtL-mat19.2-0 ; 
       rix_frigate_sPtL-mat2.2-0 ; 
       rix_frigate_sPtL-mat20.2-0 ; 
       rix_frigate_sPtL-mat21.2-0 ; 
       rix_frigate_sPtL-mat22.2-0 ; 
       rix_frigate_sPtL-mat23.2-0 ; 
       rix_frigate_sPtL-mat24.2-0 ; 
       rix_frigate_sPtL-mat25.2-0 ; 
       rix_frigate_sPtL-mat26.2-0 ; 
       rix_frigate_sPtL-mat27.2-0 ; 
       rix_frigate_sPtL-mat28.2-0 ; 
       rix_frigate_sPtL-mat29.2-0 ; 
       rix_frigate_sPtL-mat3.2-0 ; 
       rix_frigate_sPtL-mat30.2-0 ; 
       rix_frigate_sPtL-mat31.2-0 ; 
       rix_frigate_sPtL-mat32.2-0 ; 
       rix_frigate_sPtL-mat33.2-0 ; 
       rix_frigate_sPtL-mat34.2-0 ; 
       rix_frigate_sPtL-mat35.2-0 ; 
       rix_frigate_sPtL-mat36.2-0 ; 
       rix_frigate_sPtL-mat37.2-0 ; 
       rix_frigate_sPtL-mat38.2-0 ; 
       rix_frigate_sPtL-mat39.2-0 ; 
       rix_frigate_sPtL-mat4.2-0 ; 
       rix_frigate_sPtL-mat40.2-0 ; 
       rix_frigate_sPtL-mat41.2-0 ; 
       rix_frigate_sPtL-mat42.2-0 ; 
       rix_frigate_sPtL-mat43.2-0 ; 
       rix_frigate_sPtL-mat44.2-0 ; 
       rix_frigate_sPtL-mat45.2-0 ; 
       rix_frigate_sPtL-mat46.2-0 ; 
       rix_frigate_sPtL-mat47.2-0 ; 
       rix_frigate_sPtL-mat48.2-0 ; 
       rix_frigate_sPtL-mat49.3-0 ; 
       rix_frigate_sPtL-mat5.2-0 ; 
       rix_frigate_sPtL-mat56.2-0 ; 
       rix_frigate_sPtL-mat57.2-0 ; 
       rix_frigate_sPtL-mat58.2-0 ; 
       rix_frigate_sPtL-mat59.2-0 ; 
       rix_frigate_sPtL-mat6.2-0 ; 
       rix_frigate_sPtL-mat7.2-0 ; 
       rix_frigate_sPtL-mat8.2-0 ; 
       rix_frigate_sPtL-mat9.2-0 ; 
       turret_loc-mat100.1-0 ; 
       turret_loc-mat102.1-0 ; 
       turret_loc-mat103.1-0 ; 
       turret_loc-mat104.1-0 ; 
       turret_loc-mat108.1-0 ; 
       turret_loc-mat110.1-0 ; 
       turret_loc-mat111.1-0 ; 
       turret_loc-mat112.1-0 ; 
       turret_loc-mat113.1-0 ; 
       turret_loc-mat114.1-0 ; 
       turret_loc-mat115.1-0 ; 
       turret_loc-mat75.1-0 ; 
       turret_loc-mat79.1-0 ; 
       turret_loc-mat80.1-0 ; 
       turret_loc-mat81.1-0 ; 
       turret_loc-mat82.1-0 ; 
       turret_loc-mat84.1-0 ; 
       turret_loc-mat85.1-0 ; 
       turret_loc-mat86.1-0 ; 
       turret_loc-mat87.1-0 ; 
       turret_loc-mat88.1-0 ; 
       turret_loc-mat89.1-0 ; 
       turret_loc-mat90.1-0 ; 
       turret_loc-mat91.1-0 ; 
       turret_loc-mat92.1-0 ; 
       turret_loc-mat93.1-0 ; 
       turret_loc-mat94.1-0 ; 
       turret_loc-mat95.1-0 ; 
       turret_loc-mat96.1-0 ; 
       turret_loc-mat97.1-0 ; 
       turret_loc-mat98.1-0 ; 
       turret_loc-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 66     
       arc-cone.1-0 ROOT ; 
       arc1-cone.1-0 ROOT ; 
       cap06a-bfuselg.2-0 ; 
       cap06a-bfuselg1.1-0 ; 
       cap06a-cap06a.6-0 ROOT ; 
       cap06a-contwr1.1-0 ; 
       cap06a-contwr2.1-0 ; 
       cap06a-fbslicer.1-0 ; 
       cap06a-fbslicer0.1-0 ; 
       cap06a-flsicer.1-0 ; 
       cap06a-flslicer0.1-0 ; 
       cap06a-frslicer.1-0 ; 
       cap06a-frslicer0.1-0 ; 
       cap06a-ftslicer.1-0 ; 
       cap06a-ftslicer0.1-0 ; 
       cap06a-mfuselg.1-0 ; 
       cap06a-missemt.1-0 ; 
       cap06a-slicer.1-0 ; 
       cap06a-slicer1.1-0 ; 
       cap06a-smoke1.1-0 ; 
       cap06a-smoke2.1-0 ; 
       cap06a-SSb1.2-0 ; 
       cap06a-SSb2.2-0 ; 
       cap06a-SSb3.2-0 ; 
       cap06a-SSb4.2-0 ; 
       cap06a-SSf.2-0 ; 
       cap06a-SSfb.1-0 ; 
       cap06a-SSfl.1-0 ; 
       cap06a-SSfr.1-0 ; 
       cap06a-SSft.1-0 ; 
       cap06a-SSl1.1-0 ; 
       cap06a-SSl2.1-0 ; 
       cap06a-SSl3.1-0 ; 
       cap06a-SSl4.1-0 ; 
       cap06a-SSl5.1-0 ; 
       cap06a-SSl6.1-0 ; 
       cap06a-SSl7.1-0 ; 
       cap06a-SSl8.1-0 ; 
       cap06a-SSl9.1-0 ; 
       cap06a-SSlastrobe.1-0 ; 
       cap06a-SSlb.1-0 ; 
       cap06a-SSr1.1-0 ; 
       cap06a-SSr2.1-0 ; 
       cap06a-SSr3.1-0 ; 
       cap06a-SSr4.1-0 ; 
       cap06a-SSr5.1-0 ; 
       cap06a-SSr6.1-0 ; 
       cap06a-SSr7.1-0 ; 
       cap06a-SSr8.1-0 ; 
       cap06a-SSrastrobe.1-0 ; 
       cap06a-SSrb.1-0 ; 
       cap06a-SSt0.2-0 ; 
       cap06a-tfuselg.2-0 ; 
       cap06a-tfuselg0.1-0 ; 
       cap06a-tfuselg1.1-0 ; 
       cap06a-tfuselg2.1-0 ; 
       cap06a-thrust1.1-0 ; 
       cap06a-thrust2.1-0 ; 
       cap06a-tlfuselg.1-0 ; 
       cap06a-trail.1-0 ; 
       cap06a-trfuselg.1-0 ; 
       cap06a-turwepemt1.1-0 ; 
       cap06a-turwepemt2.1-0 ; 
       cap06a-wepemt1.1-0 ; 
       cap06a-wepemt2.1-0 ; 
       turret_loc-cone2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap06a/PICTURES/cap06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap06a-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       rix_frigate_sPtL-t2d1.3-0 ; 
       rix_frigate_sPtL-t2d10.3-0 ; 
       rix_frigate_sPtL-t2d11.3-0 ; 
       rix_frigate_sPtL-t2d12.3-0 ; 
       rix_frigate_sPtL-t2d13.3-0 ; 
       rix_frigate_sPtL-t2d14.3-0 ; 
       rix_frigate_sPtL-t2d15.3-0 ; 
       rix_frigate_sPtL-t2d16.3-0 ; 
       rix_frigate_sPtL-t2d17.3-0 ; 
       rix_frigate_sPtL-t2d18.2-0 ; 
       rix_frigate_sPtL-t2d19.2-0 ; 
       rix_frigate_sPtL-t2d2.3-0 ; 
       rix_frigate_sPtL-t2d20.2-0 ; 
       rix_frigate_sPtL-t2d21.2-0 ; 
       rix_frigate_sPtL-t2d22.3-0 ; 
       rix_frigate_sPtL-t2d23.3-0 ; 
       rix_frigate_sPtL-t2d24.3-0 ; 
       rix_frigate_sPtL-t2d25.3-0 ; 
       rix_frigate_sPtL-t2d26.3-0 ; 
       rix_frigate_sPtL-t2d27.3-0 ; 
       rix_frigate_sPtL-t2d28.3-0 ; 
       rix_frigate_sPtL-t2d29.3-0 ; 
       rix_frigate_sPtL-t2d3.3-0 ; 
       rix_frigate_sPtL-t2d30.3-0 ; 
       rix_frigate_sPtL-t2d31.3-0 ; 
       rix_frigate_sPtL-t2d32.3-0 ; 
       rix_frigate_sPtL-t2d33.3-0 ; 
       rix_frigate_sPtL-t2d34.3-0 ; 
       rix_frigate_sPtL-t2d35.3-0 ; 
       rix_frigate_sPtL-t2d36.3-0 ; 
       rix_frigate_sPtL-t2d37.3-0 ; 
       rix_frigate_sPtL-t2d38.3-0 ; 
       rix_frigate_sPtL-t2d39.3-0 ; 
       rix_frigate_sPtL-t2d4.3-0 ; 
       rix_frigate_sPtL-t2d40.3-0 ; 
       rix_frigate_sPtL-t2d41.3-0 ; 
       rix_frigate_sPtL-t2d42.3-0 ; 
       rix_frigate_sPtL-t2d43.3-0 ; 
       rix_frigate_sPtL-t2d44.3-0 ; 
       rix_frigate_sPtL-t2d45.2-0 ; 
       rix_frigate_sPtL-t2d46.2-0 ; 
       rix_frigate_sPtL-t2d47.2-0 ; 
       rix_frigate_sPtL-t2d48.4-0 ; 
       rix_frigate_sPtL-t2d5.3-0 ; 
       rix_frigate_sPtL-t2d55.3-0 ; 
       rix_frigate_sPtL-t2d56.2-0 ; 
       rix_frigate_sPtL-t2d57.3-0 ; 
       rix_frigate_sPtL-t2d58.3-0 ; 
       rix_frigate_sPtL-t2d59.3-0 ; 
       rix_frigate_sPtL-t2d6.3-0 ; 
       rix_frigate_sPtL-t2d7.3-0 ; 
       rix_frigate_sPtL-t2d8.3-0 ; 
       rix_frigate_sPtL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 52 110 ; 
       3 2 110 ; 
       5 52 110 ; 
       6 5 110 ; 
       7 8 110 ; 
       8 18 110 ; 
       9 10 110 ; 
       10 18 110 ; 
       11 12 110 ; 
       12 18 110 ; 
       13 14 110 ; 
       14 18 110 ; 
       15 52 110 ; 
       16 4 110 ; 
       17 3 110 ; 
       18 52 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 25 110 ; 
       22 25 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       25 2 110 ; 
       26 7 110 ; 
       27 9 110 ; 
       28 11 110 ; 
       29 13 110 ; 
       30 51 110 ; 
       31 51 110 ; 
       32 51 110 ; 
       33 51 110 ; 
       34 51 110 ; 
       35 51 110 ; 
       36 51 110 ; 
       37 51 110 ; 
       38 51 110 ; 
       39 52 110 ; 
       40 2 110 ; 
       41 51 110 ; 
       42 51 110 ; 
       43 51 110 ; 
       44 51 110 ; 
       45 51 110 ; 
       46 51 110 ; 
       47 51 110 ; 
       48 51 110 ; 
       49 52 110 ; 
       50 2 110 ; 
       51 6 110 ; 
       52 4 110 ; 
       53 52 110 ; 
       54 53 110 ; 
       55 53 110 ; 
       56 4 110 ; 
       57 4 110 ; 
       58 53 110 ; 
       59 4 110 ; 
       60 53 110 ; 
       61 4 110 ; 
       62 4 110 ; 
       63 4 110 ; 
       64 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       65 63 300 ; 
       0 62 300 ; 
       2 1 300 ; 
       2 11 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 41 300 ; 
       3 1 300 ; 
       3 42 300 ; 
       3 43 300 ; 
       3 47 300 ; 
       4 0 300 ; 
       5 0 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 49 300 ; 
       6 0 300 ; 
       6 50 300 ; 
       6 51 300 ; 
       6 52 300 ; 
       6 53 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       7 0 300 ; 
       7 22 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       9 0 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 26 300 ; 
       11 0 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 46 300 ; 
       13 0 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 27 300 ; 
       15 0 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       17 1 300 ; 
       17 44 300 ; 
       18 0 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       18 33 300 ; 
       21 66 300 ; 
       22 67 300 ; 
       23 68 300 ; 
       24 69 300 ; 
       26 60 300 ; 
       27 58 300 ; 
       28 59 300 ; 
       29 61 300 ; 
       30 70 300 ; 
       31 71 300 ; 
       32 72 300 ; 
       33 73 300 ; 
       34 74 300 ; 
       35 75 300 ; 
       36 76 300 ; 
       37 77 300 ; 
       38 78 300 ; 
       39 65 300 ; 
       40 56 300 ; 
       41 79 300 ; 
       42 80 300 ; 
       43 81 300 ; 
       44 82 300 ; 
       45 83 300 ; 
       46 84 300 ; 
       47 85 300 ; 
       48 54 300 ; 
       49 55 300 ; 
       50 57 300 ; 
       52 0 300 ; 
       52 12 300 ; 
       52 23 300 ; 
       52 34 300 ; 
       52 45 300 ; 
       52 7 300 ; 
       54 0 300 ; 
       54 37 300 ; 
       54 38 300 ; 
       54 48 300 ; 
       55 0 300 ; 
       55 35 300 ; 
       55 36 300 ; 
       58 0 300 ; 
       58 39 300 ; 
       60 0 300 ; 
       60 40 300 ; 
       1 64 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 48 401 ; 
       2 2 401 ; 
       3 50 401 ; 
       4 51 401 ; 
       5 4 401 ; 
       6 3 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 0 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 11 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 27 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 22 401 ; 
       35 34 401 ; 
       36 32 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 33 401 ; 
       46 44 401 ; 
       47 45 401 ; 
       48 46 401 ; 
       49 47 401 ; 
       50 43 401 ; 
       51 49 401 ; 
       52 52 401 ; 
       53 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       65 SCHEM 135 0 0 DISPLAY 1 2 SRT 8.901213 8.901213 8.901213 3.571593 0 0 0 -4.140597 -5.840216 MPRFLG 0 ; 
       0 SCHEM 132.5 0 0 DISPLAY 1 2 SRT 8.894146 8.894146 8.894146 -0.67 0 0 0 6.571783 -8.877365 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 MPRFLG 0 ; 
       4 SCHEM 65 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 85 -8 0 MPRFLG 0 ; 
       10 SCHEM 85 -6 0 MPRFLG 0 ; 
       11 SCHEM 90 -8 0 MPRFLG 0 ; 
       12 SCHEM 90 -6 0 MPRFLG 0 ; 
       13 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 95 -4 0 MPRFLG 0 ; 
       16 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 40 -8 0 MPRFLG 0 ; 
       18 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       19 SCHEM 110 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 87.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 85 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 90 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 92.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 65 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 70 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 82.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 80 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 77.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 75 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 62.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 12.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 45 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 47.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 50 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 60 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 57.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 55 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 52.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 10 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       52 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       53 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       54 SCHEM 15 -6 0 MPRFLG 0 ; 
       55 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       56 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 20 -6 0 MPRFLG 0 ; 
       59 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       61 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 0 0 SRT 10.78789 10.78789 10.78789 0.8100001 0 0 0 4.061095 6.871964 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       54 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 84 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 89 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 132.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 135 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 121.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 121.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 121.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 63 63 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
