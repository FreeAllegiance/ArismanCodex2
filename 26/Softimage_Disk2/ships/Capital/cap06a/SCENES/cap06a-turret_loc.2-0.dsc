SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap06a-cap06a.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_frigate_sPtL-light1.9-0 ROOT ; 
       rix_frigate_sPtL-light2.9-0 ROOT ; 
       rix_frigate_sPtL-light3.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 83     
       rix_frigate_sPtL-1.3-0 ; 
       rix_frigate_sPtL-mat1.4-0 ; 
       rix_frigate_sPtL-mat10.3-0 ; 
       rix_frigate_sPtL-mat11.3-0 ; 
       rix_frigate_sPtL-mat12.3-0 ; 
       rix_frigate_sPtL-mat13.3-0 ; 
       rix_frigate_sPtL-mat14.3-0 ; 
       rix_frigate_sPtL-mat15.3-0 ; 
       rix_frigate_sPtL-mat16.3-0 ; 
       rix_frigate_sPtL-mat17.3-0 ; 
       rix_frigate_sPtL-mat18.3-0 ; 
       rix_frigate_sPtL-mat19.3-0 ; 
       rix_frigate_sPtL-mat2.3-0 ; 
       rix_frigate_sPtL-mat20.3-0 ; 
       rix_frigate_sPtL-mat21.3-0 ; 
       rix_frigate_sPtL-mat22.3-0 ; 
       rix_frigate_sPtL-mat23.3-0 ; 
       rix_frigate_sPtL-mat24.3-0 ; 
       rix_frigate_sPtL-mat25.3-0 ; 
       rix_frigate_sPtL-mat26.3-0 ; 
       rix_frigate_sPtL-mat27.3-0 ; 
       rix_frigate_sPtL-mat28.3-0 ; 
       rix_frigate_sPtL-mat29.3-0 ; 
       rix_frigate_sPtL-mat3.3-0 ; 
       rix_frigate_sPtL-mat30.3-0 ; 
       rix_frigate_sPtL-mat31.3-0 ; 
       rix_frigate_sPtL-mat32.3-0 ; 
       rix_frigate_sPtL-mat33.3-0 ; 
       rix_frigate_sPtL-mat34.3-0 ; 
       rix_frigate_sPtL-mat35.3-0 ; 
       rix_frigate_sPtL-mat36.3-0 ; 
       rix_frigate_sPtL-mat37.3-0 ; 
       rix_frigate_sPtL-mat38.3-0 ; 
       rix_frigate_sPtL-mat39.3-0 ; 
       rix_frigate_sPtL-mat4.3-0 ; 
       rix_frigate_sPtL-mat40.3-0 ; 
       rix_frigate_sPtL-mat41.3-0 ; 
       rix_frigate_sPtL-mat42.3-0 ; 
       rix_frigate_sPtL-mat43.3-0 ; 
       rix_frigate_sPtL-mat44.3-0 ; 
       rix_frigate_sPtL-mat45.3-0 ; 
       rix_frigate_sPtL-mat46.3-0 ; 
       rix_frigate_sPtL-mat47.3-0 ; 
       rix_frigate_sPtL-mat48.3-0 ; 
       rix_frigate_sPtL-mat49.4-0 ; 
       rix_frigate_sPtL-mat5.3-0 ; 
       rix_frigate_sPtL-mat56.3-0 ; 
       rix_frigate_sPtL-mat57.3-0 ; 
       rix_frigate_sPtL-mat58.3-0 ; 
       rix_frigate_sPtL-mat59.3-0 ; 
       rix_frigate_sPtL-mat6.3-0 ; 
       rix_frigate_sPtL-mat7.3-0 ; 
       rix_frigate_sPtL-mat8.3-0 ; 
       rix_frigate_sPtL-mat9.3-0 ; 
       turret_loc-mat100.2-0 ; 
       turret_loc-mat102.2-0 ; 
       turret_loc-mat103.2-0 ; 
       turret_loc-mat104.2-0 ; 
       turret_loc-mat108.2-0 ; 
       turret_loc-mat110.2-0 ; 
       turret_loc-mat111.2-0 ; 
       turret_loc-mat112.2-0 ; 
       turret_loc-mat75.2-0 ; 
       turret_loc-mat79.2-0 ; 
       turret_loc-mat80.2-0 ; 
       turret_loc-mat81.2-0 ; 
       turret_loc-mat82.2-0 ; 
       turret_loc-mat84.2-0 ; 
       turret_loc-mat85.2-0 ; 
       turret_loc-mat86.2-0 ; 
       turret_loc-mat87.2-0 ; 
       turret_loc-mat88.2-0 ; 
       turret_loc-mat89.2-0 ; 
       turret_loc-mat90.2-0 ; 
       turret_loc-mat91.2-0 ; 
       turret_loc-mat92.2-0 ; 
       turret_loc-mat93.2-0 ; 
       turret_loc-mat94.2-0 ; 
       turret_loc-mat95.2-0 ; 
       turret_loc-mat96.2-0 ; 
       turret_loc-mat97.2-0 ; 
       turret_loc-mat98.2-0 ; 
       turret_loc-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 68     
       cap06a-bfuselg.2-0 ; 
       cap06a-bfuselg1.1-0 ; 
       cap06a-cap06a.7-0 ROOT ; 
       cap06a-contwr1.1-0 ; 
       cap06a-contwr2.1-0 ; 
       cap06a-fbslicer.1-0 ; 
       cap06a-fbslicer0.1-0 ; 
       cap06a-flsicer.1-0 ; 
       cap06a-flslicer0.1-0 ; 
       cap06a-frslicer.1-0 ; 
       cap06a-frslicer0.1-0 ; 
       cap06a-ftslicer.1-0 ; 
       cap06a-ftslicer0.1-0 ; 
       cap06a-mfuselg.1-0 ; 
       cap06a-missemt.1-0 ; 
       cap06a-slicer.1-0 ; 
       cap06a-slicer1.1-0 ; 
       cap06a-smoke1.1-0 ; 
       cap06a-smoke2.1-0 ; 
       cap06a-SSb1.2-0 ; 
       cap06a-SSb2.2-0 ; 
       cap06a-SSb3.2-0 ; 
       cap06a-SSb4.2-0 ; 
       cap06a-SSf.2-0 ; 
       cap06a-SSfb.1-0 ; 
       cap06a-SSfl.1-0 ; 
       cap06a-SSfr.1-0 ; 
       cap06a-SSft.1-0 ; 
       cap06a-SSl1.1-0 ; 
       cap06a-SSl2.1-0 ; 
       cap06a-SSl3.1-0 ; 
       cap06a-SSl4.1-0 ; 
       cap06a-SSl5.1-0 ; 
       cap06a-SSl6.1-0 ; 
       cap06a-SSl7.1-0 ; 
       cap06a-SSl8.1-0 ; 
       cap06a-SSl9.1-0 ; 
       cap06a-SSlastrobe.1-0 ; 
       cap06a-SSlb.1-0 ; 
       cap06a-SSr1.1-0 ; 
       cap06a-SSr2.1-0 ; 
       cap06a-SSr3.1-0 ; 
       cap06a-SSr4.1-0 ; 
       cap06a-SSr5.1-0 ; 
       cap06a-SSr6.1-0 ; 
       cap06a-SSr7.1-0 ; 
       cap06a-SSr8.1-0 ; 
       cap06a-SSrastrobe.1-0 ; 
       cap06a-SSrb.1-0 ; 
       cap06a-SSt0.2-0 ; 
       cap06a-tfuselg.2-0 ; 
       cap06a-tfuselg0.1-0 ; 
       cap06a-tfuselg1.1-0 ; 
       cap06a-tfuselg2.1-0 ; 
       cap06a-thrust1.1-0 ; 
       cap06a-thrust2.1-0 ; 
       cap06a-tlfuselg.1-0 ; 
       cap06a-trail.1-0 ; 
       cap06a-trfuselg.1-0 ; 
       cap06a-turwepemt1.1-0 ; 
       cap06a-turwepemt2.1-0 ; 
       cap06a-wepemt1.1-0 ; 
       cap06a-wepemt2.1-0 ; 
       turcone-160deg.1-0 ROOT ; 
       turcone-175deg.1-0 ROOT ; 
       turcone5-175deg.1-0 ROOT ; 
       turret_loc-175deg1.1-0 ROOT ; 
       turret_loc-scale_cube.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap06a/PICTURES/cap06a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap06a-turret_loc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       rix_frigate_sPtL-t2d1.4-0 ; 
       rix_frigate_sPtL-t2d10.4-0 ; 
       rix_frigate_sPtL-t2d11.4-0 ; 
       rix_frigate_sPtL-t2d12.4-0 ; 
       rix_frigate_sPtL-t2d13.4-0 ; 
       rix_frigate_sPtL-t2d14.4-0 ; 
       rix_frigate_sPtL-t2d15.4-0 ; 
       rix_frigate_sPtL-t2d16.4-0 ; 
       rix_frigate_sPtL-t2d17.4-0 ; 
       rix_frigate_sPtL-t2d18.3-0 ; 
       rix_frigate_sPtL-t2d19.3-0 ; 
       rix_frigate_sPtL-t2d2.4-0 ; 
       rix_frigate_sPtL-t2d20.3-0 ; 
       rix_frigate_sPtL-t2d21.3-0 ; 
       rix_frigate_sPtL-t2d22.4-0 ; 
       rix_frigate_sPtL-t2d23.4-0 ; 
       rix_frigate_sPtL-t2d24.4-0 ; 
       rix_frigate_sPtL-t2d25.4-0 ; 
       rix_frigate_sPtL-t2d26.4-0 ; 
       rix_frigate_sPtL-t2d27.4-0 ; 
       rix_frigate_sPtL-t2d28.4-0 ; 
       rix_frigate_sPtL-t2d29.4-0 ; 
       rix_frigate_sPtL-t2d3.4-0 ; 
       rix_frigate_sPtL-t2d30.4-0 ; 
       rix_frigate_sPtL-t2d31.4-0 ; 
       rix_frigate_sPtL-t2d32.4-0 ; 
       rix_frigate_sPtL-t2d33.4-0 ; 
       rix_frigate_sPtL-t2d34.4-0 ; 
       rix_frigate_sPtL-t2d35.4-0 ; 
       rix_frigate_sPtL-t2d36.4-0 ; 
       rix_frigate_sPtL-t2d37.4-0 ; 
       rix_frigate_sPtL-t2d38.4-0 ; 
       rix_frigate_sPtL-t2d39.4-0 ; 
       rix_frigate_sPtL-t2d4.4-0 ; 
       rix_frigate_sPtL-t2d40.4-0 ; 
       rix_frigate_sPtL-t2d41.4-0 ; 
       rix_frigate_sPtL-t2d42.4-0 ; 
       rix_frigate_sPtL-t2d43.4-0 ; 
       rix_frigate_sPtL-t2d44.4-0 ; 
       rix_frigate_sPtL-t2d45.3-0 ; 
       rix_frigate_sPtL-t2d46.3-0 ; 
       rix_frigate_sPtL-t2d47.3-0 ; 
       rix_frigate_sPtL-t2d48.5-0 ; 
       rix_frigate_sPtL-t2d5.4-0 ; 
       rix_frigate_sPtL-t2d55.4-0 ; 
       rix_frigate_sPtL-t2d56.3-0 ; 
       rix_frigate_sPtL-t2d57.4-0 ; 
       rix_frigate_sPtL-t2d58.4-0 ; 
       rix_frigate_sPtL-t2d59.4-0 ; 
       rix_frigate_sPtL-t2d6.4-0 ; 
       rix_frigate_sPtL-t2d7.4-0 ; 
       rix_frigate_sPtL-t2d8.4-0 ; 
       rix_frigate_sPtL-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 50 110 ; 
       1 0 110 ; 
       3 50 110 ; 
       4 3 110 ; 
       5 6 110 ; 
       6 16 110 ; 
       7 8 110 ; 
       8 16 110 ; 
       9 10 110 ; 
       10 16 110 ; 
       11 12 110 ; 
       12 16 110 ; 
       13 50 110 ; 
       14 2 110 ; 
       15 1 110 ; 
       16 50 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 23 110 ; 
       23 0 110 ; 
       24 5 110 ; 
       25 7 110 ; 
       26 9 110 ; 
       27 11 110 ; 
       28 49 110 ; 
       29 49 110 ; 
       30 49 110 ; 
       31 49 110 ; 
       32 49 110 ; 
       33 49 110 ; 
       34 49 110 ; 
       35 49 110 ; 
       36 49 110 ; 
       37 50 110 ; 
       38 0 110 ; 
       39 49 110 ; 
       40 49 110 ; 
       41 49 110 ; 
       42 49 110 ; 
       43 49 110 ; 
       44 49 110 ; 
       45 49 110 ; 
       46 49 110 ; 
       47 50 110 ; 
       48 0 110 ; 
       49 4 110 ; 
       50 2 110 ; 
       51 50 110 ; 
       52 51 110 ; 
       53 51 110 ; 
       54 2 110 ; 
       55 2 110 ; 
       56 51 110 ; 
       57 2 110 ; 
       58 51 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       61 2 110 ; 
       62 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 11 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 41 300 ; 
       1 1 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 47 300 ; 
       2 0 300 ; 
       3 0 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 49 300 ; 
       4 0 300 ; 
       4 50 300 ; 
       4 51 300 ; 
       4 52 300 ; 
       4 53 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       5 0 300 ; 
       5 22 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       7 0 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 26 300 ; 
       9 0 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 46 300 ; 
       11 0 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 27 300 ; 
       13 0 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       15 1 300 ; 
       15 44 300 ; 
       16 0 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       16 33 300 ; 
       19 63 300 ; 
       20 64 300 ; 
       21 65 300 ; 
       22 66 300 ; 
       24 60 300 ; 
       25 58 300 ; 
       26 59 300 ; 
       27 61 300 ; 
       28 67 300 ; 
       29 68 300 ; 
       30 69 300 ; 
       31 70 300 ; 
       32 71 300 ; 
       33 72 300 ; 
       34 73 300 ; 
       35 74 300 ; 
       36 75 300 ; 
       37 62 300 ; 
       38 56 300 ; 
       39 76 300 ; 
       40 77 300 ; 
       41 78 300 ; 
       42 79 300 ; 
       43 80 300 ; 
       44 81 300 ; 
       45 82 300 ; 
       46 54 300 ; 
       47 55 300 ; 
       48 57 300 ; 
       50 0 300 ; 
       50 12 300 ; 
       50 23 300 ; 
       50 34 300 ; 
       50 45 300 ; 
       50 7 300 ; 
       52 0 300 ; 
       52 37 300 ; 
       52 38 300 ; 
       52 48 300 ; 
       53 0 300 ; 
       53 35 300 ; 
       53 36 300 ; 
       56 0 300 ; 
       56 39 300 ; 
       58 0 300 ; 
       58 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 48 401 ; 
       2 2 401 ; 
       3 50 401 ; 
       4 51 401 ; 
       5 4 401 ; 
       6 3 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 0 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 11 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 27 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 22 401 ; 
       35 34 401 ; 
       36 32 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 33 401 ; 
       46 44 401 ; 
       47 45 401 ; 
       48 46 401 ; 
       49 47 401 ; 
       50 43 401 ; 
       51 49 401 ; 
       52 52 401 ; 
       53 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       67 SCHEM 60.80232 13.81018 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       65 SCHEM 72.27885 9.821846 0 USR WIRECOL 7 7 SRT 6.471934 6.471934 6.471934 -0.02681842 0.004903844 -1.734874 4.752245 1.870624 -9.513206 MPRFLG 0 ; 
       0 SCHEM 32.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 40 -6 0 MPRFLG 0 ; 
       2 SCHEM 65 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 85 -8 0 MPRFLG 0 ; 
       8 SCHEM 85 -6 0 MPRFLG 0 ; 
       9 SCHEM 90 -8 0 MPRFLG 0 ; 
       10 SCHEM 90 -6 0 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 95 -4 0 MPRFLG 0 ; 
       14 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 MPRFLG 0 ; 
       16 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 110 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 35 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 87.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 85 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 90 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 65 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 70 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 72.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 80 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 75 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 12.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 45 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 47.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 50 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 60 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 57.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 55 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 52.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 10 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       50 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       51 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       52 SCHEM 15 -6 0 MPRFLG 0 ; 
       53 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 20 -6 0 MPRFLG 0 ; 
       57 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       59 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 74.77885 9.821846 0 USR WIRECOL 7 7 SRT 6.471934 6.471934 6.471934 -3.114774 0.004903844 -1.406719 -4.752245 1.870624 -9.513206 MPRFLG 0 ; 
       63 SCHEM 57.44197 9.767974 0 USR WIRECOL 7 7 SRT 6.498698 6.498698 6.498698 0.16 0 0 0 4.222975 7.150941 MPRFLG 0 ; 
       64 SCHEM 59.77884 9.767974 0 USR WIRECOL 7 7 SRT 6.498698 6.498698 6.498698 -0.21 0 0 0 5.891156 -13.79968 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 121.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 84 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 89 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 91.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 71.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 94 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 121.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 121.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
