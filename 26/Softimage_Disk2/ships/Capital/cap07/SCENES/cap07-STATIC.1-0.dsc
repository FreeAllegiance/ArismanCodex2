SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.8-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       rix_destroyB_sPTL-mat1.4-0 ; 
       rix_destroyB_sPTL-mat10.4-0 ; 
       rix_destroyB_sPTL-mat11.4-0 ; 
       rix_destroyB_sPTL-mat13.4-0 ; 
       rix_destroyB_sPTL-mat15.4-0 ; 
       rix_destroyB_sPTL-mat16.4-0 ; 
       rix_destroyB_sPTL-mat17.4-0 ; 
       rix_destroyB_sPTL-mat20.4-0 ; 
       rix_destroyB_sPTL-mat21.4-0 ; 
       rix_destroyB_sPTL-mat22.4-0 ; 
       rix_destroyB_sPTL-mat23.4-0 ; 
       rix_destroyB_sPTL-mat24.4-0 ; 
       rix_destroyB_sPTL-mat29.4-0 ; 
       rix_destroyB_sPTL-mat3.4-0 ; 
       rix_destroyB_sPTL-mat30.4-0 ; 
       rix_destroyB_sPTL-mat31.4-0 ; 
       rix_destroyB_sPTL-mat32.4-0 ; 
       rix_destroyB_sPTL-mat33.4-0 ; 
       rix_destroyB_sPTL-mat38.4-0 ; 
       rix_destroyB_sPTL-mat40.4-0 ; 
       rix_destroyB_sPTL-mat41.4-0 ; 
       rix_destroyB_sPTL-mat42.4-0 ; 
       rix_destroyB_sPTL-mat43.4-0 ; 
       rix_destroyB_sPTL-mat46.4-0 ; 
       rix_destroyB_sPTL-mat47.4-0 ; 
       rix_destroyB_sPTL-mat49.4-0 ; 
       rix_destroyB_sPTL-mat5.4-0 ; 
       rix_destroyB_sPTL-mat50.4-0 ; 
       rix_destroyB_sPTL-mat51.4-0 ; 
       rix_destroyB_sPTL-mat52.4-0 ; 
       rix_destroyB_sPTL-mat53.4-0 ; 
       rix_destroyB_sPTL-mat54.4-0 ; 
       rix_destroyB_sPTL-mat55.4-0 ; 
       rix_destroyB_sPTL-mat56.4-0 ; 
       rix_destroyB_sPTL-mat57.4-0 ; 
       rix_destroyB_sPTL-mat58.4-0 ; 
       rix_destroyB_sPTL-mat59.4-0 ; 
       rix_destroyB_sPTL-mat6.4-0 ; 
       rix_destroyB_sPTL-mat7.4-0 ; 
       rix_destroyB_sPTL-mat8.4-0 ; 
       rix_destroyB_sPTL-mat9.4-0 ; 
       scaled-mat61.2-0 ; 
       scaled-mat62.2-0 ; 
       scaled-mat63.2-0 ; 
       scaled-mat64.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.14-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-cube3.1-0 ; 
       cap07-cube4.1-0 ; 
       cap07-cube6.1-0 ; 
       cap07-cube7.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-twepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap07/PICTURES/cap03 ; 
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       rix_destroyB_sPTL-t2d10.6-0 ; 
       rix_destroyB_sPTL-t2d12.6-0 ; 
       rix_destroyB_sPTL-t2d13.6-0 ; 
       rix_destroyB_sPTL-t2d16.6-0 ; 
       rix_destroyB_sPTL-t2d17.6-0 ; 
       rix_destroyB_sPTL-t2d18.6-0 ; 
       rix_destroyB_sPTL-t2d19.6-0 ; 
       rix_destroyB_sPTL-t2d2.6-0 ; 
       rix_destroyB_sPTL-t2d22.6-0 ; 
       rix_destroyB_sPTL-t2d23.6-0 ; 
       rix_destroyB_sPTL-t2d24.6-0 ; 
       rix_destroyB_sPTL-t2d25.6-0 ; 
       rix_destroyB_sPTL-t2d29.6-0 ; 
       rix_destroyB_sPTL-t2d30.6-0 ; 
       rix_destroyB_sPTL-t2d31.6-0 ; 
       rix_destroyB_sPTL-t2d32.6-0 ; 
       rix_destroyB_sPTL-t2d33.6-0 ; 
       rix_destroyB_sPTL-t2d36.6-0 ; 
       rix_destroyB_sPTL-t2d37.6-0 ; 
       rix_destroyB_sPTL-t2d38.6-0 ; 
       rix_destroyB_sPTL-t2d39.6-0 ; 
       rix_destroyB_sPTL-t2d4.7-0 ; 
       rix_destroyB_sPTL-t2d40.6-0 ; 
       rix_destroyB_sPTL-t2d41.6-0 ; 
       rix_destroyB_sPTL-t2d42.6-0 ; 
       rix_destroyB_sPTL-t2d43.6-0 ; 
       rix_destroyB_sPTL-t2d44.7-0 ; 
       rix_destroyB_sPTL-t2d45.7-0 ; 
       rix_destroyB_sPTL-t2d46.7-0 ; 
       rix_destroyB_sPTL-t2d5.6-0 ; 
       rix_destroyB_sPTL-t2d6.6-0 ; 
       rix_destroyB_sPTL-t2d7.6-0 ; 
       rix_destroyB_sPTL-t2d8.6-0 ; 
       scaled-t2d48.3-0 ; 
       scaled-t2d49.3-0 ; 
       scaled-t2d50.3-0 ; 
       scaled-t2d51.3-0 ; 
       utann_carrier_sPTd-t2d14.5-0 ; 
       utann_carrier_sPTd-t2d5.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       3 8 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 8 110 ; 
       14 10 110 ; 
       15 8 110 ; 
       16 10 110 ; 
       17 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       3 6 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 41 300 ; 
       5 42 300 ; 
       6 43 300 ; 
       7 44 300 ; 
       8 0 300 ; 
       8 13 300 ; 
       8 38 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       10 26 300 ; 
       10 37 300 ; 
       10 34 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       11 25 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       11 33 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       17 12 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       17 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 33 400 ; 
       5 34 400 ; 
       6 35 400 ; 
       7 36 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 21 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 67.5 0 0 SRT 1 1 1 0 0 0 0 0 1.315417 MPRFLG 0 ; 
       3 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 85.93193 -4.799345 0 USR MPRFLG 0 ; 
       5 SCHEM 88.21497 -4.815994 0 USR MPRFLG 0 ; 
       6 SCHEM 90.52235 -4.782678 0 USR MPRFLG 0 ; 
       7 SCHEM 83.79383 -4.813927 0 USR MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 92.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 61.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 5 -4 0 MPRFLG 0 ; 
       16 SCHEM 85 -6 0 MPRFLG 0 ; 
       17 SCHEM 40 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 84.68193 -6.799345 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 86.96497 -6.815994 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 89.27235 -6.782678 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 82.54383 -6.813927 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 87.18193 -6.799345 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 89.46497 -6.815994 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 91.77235 -6.782678 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 85.04383 -6.813927 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
