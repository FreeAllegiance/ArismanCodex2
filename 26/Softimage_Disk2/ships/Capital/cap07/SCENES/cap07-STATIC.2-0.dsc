SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.8-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_destroyB_sPTL-light1_1.8-0 ROOT ; 
       rix_destroyB_sPTL-light2_1.8-0 ROOT ; 
       rix_destroyB_sPTL-light3.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       rix_destroyB_sPTL-mat1.1-0 ; 
       rix_destroyB_sPTL-mat10.1-0 ; 
       rix_destroyB_sPTL-mat11.1-0 ; 
       rix_destroyB_sPTL-mat13.1-0 ; 
       rix_destroyB_sPTL-mat15.1-0 ; 
       rix_destroyB_sPTL-mat16.1-0 ; 
       rix_destroyB_sPTL-mat17.1-0 ; 
       rix_destroyB_sPTL-mat20.1-0 ; 
       rix_destroyB_sPTL-mat21.1-0 ; 
       rix_destroyB_sPTL-mat22.1-0 ; 
       rix_destroyB_sPTL-mat23.1-0 ; 
       rix_destroyB_sPTL-mat24.1-0 ; 
       rix_destroyB_sPTL-mat29.1-0 ; 
       rix_destroyB_sPTL-mat3.1-0 ; 
       rix_destroyB_sPTL-mat30.1-0 ; 
       rix_destroyB_sPTL-mat31.1-0 ; 
       rix_destroyB_sPTL-mat32.1-0 ; 
       rix_destroyB_sPTL-mat33.1-0 ; 
       rix_destroyB_sPTL-mat38.1-0 ; 
       rix_destroyB_sPTL-mat40.1-0 ; 
       rix_destroyB_sPTL-mat41.1-0 ; 
       rix_destroyB_sPTL-mat42.1-0 ; 
       rix_destroyB_sPTL-mat43.1-0 ; 
       rix_destroyB_sPTL-mat46.1-0 ; 
       rix_destroyB_sPTL-mat47.1-0 ; 
       rix_destroyB_sPTL-mat49.1-0 ; 
       rix_destroyB_sPTL-mat5.1-0 ; 
       rix_destroyB_sPTL-mat50.1-0 ; 
       rix_destroyB_sPTL-mat51.1-0 ; 
       rix_destroyB_sPTL-mat52.1-0 ; 
       rix_destroyB_sPTL-mat53.1-0 ; 
       rix_destroyB_sPTL-mat54.1-0 ; 
       rix_destroyB_sPTL-mat55.1-0 ; 
       rix_destroyB_sPTL-mat56.1-0 ; 
       rix_destroyB_sPTL-mat57.1-0 ; 
       rix_destroyB_sPTL-mat58.1-0 ; 
       rix_destroyB_sPTL-mat59.1-0 ; 
       rix_destroyB_sPTL-mat6.1-0 ; 
       rix_destroyB_sPTL-mat7.1-0 ; 
       rix_destroyB_sPTL-mat8.1-0 ; 
       rix_destroyB_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.5-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-twepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap03 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       rix_destroyB_sPTL-t2d10.1-0 ; 
       rix_destroyB_sPTL-t2d12.1-0 ; 
       rix_destroyB_sPTL-t2d13.1-0 ; 
       rix_destroyB_sPTL-t2d16.1-0 ; 
       rix_destroyB_sPTL-t2d17.1-0 ; 
       rix_destroyB_sPTL-t2d18.1-0 ; 
       rix_destroyB_sPTL-t2d19.1-0 ; 
       rix_destroyB_sPTL-t2d2.1-0 ; 
       rix_destroyB_sPTL-t2d22.1-0 ; 
       rix_destroyB_sPTL-t2d23.1-0 ; 
       rix_destroyB_sPTL-t2d24.1-0 ; 
       rix_destroyB_sPTL-t2d25.1-0 ; 
       rix_destroyB_sPTL-t2d29.1-0 ; 
       rix_destroyB_sPTL-t2d30.1-0 ; 
       rix_destroyB_sPTL-t2d31.1-0 ; 
       rix_destroyB_sPTL-t2d32.1-0 ; 
       rix_destroyB_sPTL-t2d33.1-0 ; 
       rix_destroyB_sPTL-t2d36.1-0 ; 
       rix_destroyB_sPTL-t2d37.1-0 ; 
       rix_destroyB_sPTL-t2d38.1-0 ; 
       rix_destroyB_sPTL-t2d39.1-0 ; 
       rix_destroyB_sPTL-t2d4.1-0 ; 
       rix_destroyB_sPTL-t2d40.1-0 ; 
       rix_destroyB_sPTL-t2d41.1-0 ; 
       rix_destroyB_sPTL-t2d42.1-0 ; 
       rix_destroyB_sPTL-t2d43.1-0 ; 
       rix_destroyB_sPTL-t2d44.1-0 ; 
       rix_destroyB_sPTL-t2d45.1-0 ; 
       rix_destroyB_sPTL-t2d46.1-0 ; 
       rix_destroyB_sPTL-t2d5.1-0 ; 
       rix_destroyB_sPTL-t2d6.1-0 ; 
       rix_destroyB_sPTL-t2d7.1-0 ; 
       rix_destroyB_sPTL-t2d8.1-0 ; 
       utann_carrier_sPTd-t2d14.1-0 ; 
       utann_carrier_sPTd-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 4 110 ; 
       10 6 110 ; 
       11 4 110 ; 
       12 6 110 ; 
       13 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       3 6 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 0 300 ; 
       4 13 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       6 26 300 ; 
       6 37 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       7 25 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 33 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       13 12 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 21 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 21.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
