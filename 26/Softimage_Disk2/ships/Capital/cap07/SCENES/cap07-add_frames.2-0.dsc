SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap07-cap07.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.6-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_destroyB_sPTL-light1_1.6-0 ROOT ; 
       rix_destroyB_sPTL-light2_1.6-0 ROOT ; 
       rix_destroyB_sPTL-light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_frames-nose_white-center.1-0_10.1-0 ; 
       add_frames-nose_white-center.1-0_11.1-0 ; 
       add_frames-nose_white-center.1-0_12.1-0 ; 
       add_frames-nose_white-center.1-0_13.1-0 ; 
       add_frames-nose_white-center.1-0_14.1-0 ; 
       add_frames-nose_white-center.1-0_15.1-0 ; 
       add_frames-nose_white-center.1-0_16.1-0 ; 
       add_frames-nose_white-center.1-0_5.1-0 ; 
       add_frames-nose_white-center.1-0_6.1-0 ; 
       add_frames-nose_white-center.1-0_7.1-0 ; 
       add_frames-nose_white-center.1-0_8.1-0 ; 
       add_frames-nose_white-center.1-0_9.1-0 ; 
       add_frames-nose_white-center.1-10.1-0 ; 
       add_frames-nose_white-center.1-11.1-0 ; 
       add_frames-nose_white-center.1-12.1-0 ; 
       add_frames-nose_white-center.1-3.1-0 ; 
       rix_destroyB_sPTL-mat1.1-0 ; 
       rix_destroyB_sPTL-mat10.1-0 ; 
       rix_destroyB_sPTL-mat11.1-0 ; 
       rix_destroyB_sPTL-mat13.1-0 ; 
       rix_destroyB_sPTL-mat15.1-0 ; 
       rix_destroyB_sPTL-mat16.1-0 ; 
       rix_destroyB_sPTL-mat17.1-0 ; 
       rix_destroyB_sPTL-mat20.1-0 ; 
       rix_destroyB_sPTL-mat21.1-0 ; 
       rix_destroyB_sPTL-mat22.1-0 ; 
       rix_destroyB_sPTL-mat23.1-0 ; 
       rix_destroyB_sPTL-mat24.1-0 ; 
       rix_destroyB_sPTL-mat29.1-0 ; 
       rix_destroyB_sPTL-mat3.1-0 ; 
       rix_destroyB_sPTL-mat30.1-0 ; 
       rix_destroyB_sPTL-mat31.1-0 ; 
       rix_destroyB_sPTL-mat32.1-0 ; 
       rix_destroyB_sPTL-mat33.1-0 ; 
       rix_destroyB_sPTL-mat38.1-0 ; 
       rix_destroyB_sPTL-mat40.1-0 ; 
       rix_destroyB_sPTL-mat41.1-0 ; 
       rix_destroyB_sPTL-mat42.1-0 ; 
       rix_destroyB_sPTL-mat43.1-0 ; 
       rix_destroyB_sPTL-mat46.1-0 ; 
       rix_destroyB_sPTL-mat47.1-0 ; 
       rix_destroyB_sPTL-mat49.1-0 ; 
       rix_destroyB_sPTL-mat5.1-0 ; 
       rix_destroyB_sPTL-mat50.1-0 ; 
       rix_destroyB_sPTL-mat51.1-0 ; 
       rix_destroyB_sPTL-mat52.1-0 ; 
       rix_destroyB_sPTL-mat53.1-0 ; 
       rix_destroyB_sPTL-mat54.1-0 ; 
       rix_destroyB_sPTL-mat55.1-0 ; 
       rix_destroyB_sPTL-mat56.1-0 ; 
       rix_destroyB_sPTL-mat57.1-0 ; 
       rix_destroyB_sPTL-mat58.1-0 ; 
       rix_destroyB_sPTL-mat59.1-0 ; 
       rix_destroyB_sPTL-mat6.1-0 ; 
       rix_destroyB_sPTL-mat7.1-0 ; 
       rix_destroyB_sPTL-mat8.1-0 ; 
       rix_destroyB_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.4-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-smoke.1-0 ; 
       cap07-SSb4.1-0 ; 
       cap07-SSb7.1-0 ; 
       cap07-SSb7_1.1-0 ; 
       cap07-SSb8.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-SSm10.1-0 ; 
       cap07-SSm11.1-0 ; 
       cap07-SSm12.1-0 ; 
       cap07-SSm13.1-0 ; 
       cap07-SSm14.1-0 ; 
       cap07-SSm15.1-0 ; 
       cap07-SSm5.1-0 ; 
       cap07-SSm5_1.1-0 ; 
       cap07-SSm6.1-0 ; 
       cap07-SSm7.1-0 ; 
       cap07-SSm8.1-0 ; 
       cap07-SSm9.1-0 ; 
       cap07-thrust.1-0 ; 
       cap07-trail.1-0 ; 
       cap07-turwepemt1.1-0 ; 
       cap07-turwepemt2.1-0 ; 
       cap07-turwepemt3.1-0 ; 
       cap07-turwepemt4.1-0 ; 
       cap07-turwepemt5.1-0 ; 
       cap07-turwepemt6.1-0 ; 
       cap07-twepatt.1-0 ; 
       cap07-wepemt1.1-0 ; 
       cap07-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap03 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-add_frames.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       rix_destroyB_sPTL-t2d10.1-0 ; 
       rix_destroyB_sPTL-t2d12.1-0 ; 
       rix_destroyB_sPTL-t2d13.1-0 ; 
       rix_destroyB_sPTL-t2d16.1-0 ; 
       rix_destroyB_sPTL-t2d17.1-0 ; 
       rix_destroyB_sPTL-t2d18.1-0 ; 
       rix_destroyB_sPTL-t2d19.1-0 ; 
       rix_destroyB_sPTL-t2d2.1-0 ; 
       rix_destroyB_sPTL-t2d22.1-0 ; 
       rix_destroyB_sPTL-t2d23.1-0 ; 
       rix_destroyB_sPTL-t2d24.1-0 ; 
       rix_destroyB_sPTL-t2d25.1-0 ; 
       rix_destroyB_sPTL-t2d29.1-0 ; 
       rix_destroyB_sPTL-t2d30.1-0 ; 
       rix_destroyB_sPTL-t2d31.1-0 ; 
       rix_destroyB_sPTL-t2d32.1-0 ; 
       rix_destroyB_sPTL-t2d33.1-0 ; 
       rix_destroyB_sPTL-t2d36.1-0 ; 
       rix_destroyB_sPTL-t2d37.1-0 ; 
       rix_destroyB_sPTL-t2d38.1-0 ; 
       rix_destroyB_sPTL-t2d39.1-0 ; 
       rix_destroyB_sPTL-t2d4.1-0 ; 
       rix_destroyB_sPTL-t2d40.1-0 ; 
       rix_destroyB_sPTL-t2d41.1-0 ; 
       rix_destroyB_sPTL-t2d42.1-0 ; 
       rix_destroyB_sPTL-t2d43.1-0 ; 
       rix_destroyB_sPTL-t2d44.1-0 ; 
       rix_destroyB_sPTL-t2d45.1-0 ; 
       rix_destroyB_sPTL-t2d46.1-0 ; 
       rix_destroyB_sPTL-t2d5.1-0 ; 
       rix_destroyB_sPTL-t2d6.1-0 ; 
       rix_destroyB_sPTL-t2d7.1-0 ; 
       rix_destroyB_sPTL-t2d8.1-0 ; 
       utann_carrier_sPTd-t2d14.1-0 ; 
       utann_carrier_sPTd-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 0 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       9 2 110 ; 
       8 0 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 6 110 ; 
       16 4 110 ; 
       17 6 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       23 15 110 ; 
       24 14 110 ; 
       25 16 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 16 110 ; 
       29 16 110 ; 
       38 5 110 ; 
       32 2 110 ; 
       31 2 110 ; 
       30 2 110 ; 
       39 2 110 ; 
       40 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 23 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       3 22 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       4 16 300 ; 
       4 29 300 ; 
       4 54 300 ; 
       4 55 300 ; 
       4 56 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 42 300 ; 
       6 53 300 ; 
       6 50 300 ; 
       6 51 300 ; 
       6 52 300 ; 
       7 41 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 49 300 ; 
       8 45 300 ; 
       8 46 300 ; 
       8 47 300 ; 
       8 48 300 ; 
       10 15 300 ; 
       11 12 300 ; 
       12 13 300 ; 
       13 14 300 ; 
       18 1 300 ; 
       19 2 300 ; 
       20 3 300 ; 
       21 4 300 ; 
       22 5 300 ; 
       23 6 300 ; 
       24 7 300 ; 
       25 10 300 ; 
       26 8 300 ; 
       27 9 300 ; 
       28 11 300 ; 
       29 0 300 ; 
       38 28 300 ; 
       38 30 300 ; 
       38 31 300 ; 
       38 32 300 ; 
       38 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       18 32 401 ; 
       19 0 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       24 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       29 7 401 ; 
       30 8 401 ; 
       31 9 401 ; 
       32 10 401 ; 
       33 11 401 ; 
       34 12 401 ; 
       35 13 401 ; 
       36 14 401 ; 
       37 15 401 ; 
       38 16 401 ; 
       39 17 401 ; 
       40 18 401 ; 
       43 19 401 ; 
       44 20 401 ; 
       46 22 401 ; 
       47 23 401 ; 
       48 24 401 ; 
       49 25 401 ; 
       50 26 401 ; 
       51 27 401 ; 
       52 28 401 ; 
       53 21 401 ; 
       54 29 401 ; 
       55 30 401 ; 
       56 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 MPRFLG 0 ; 
       5 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -12 0 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 MPRFLG 0 ; 
       17 SCHEM 40 -10 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 40 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 50 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 45 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 22.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 0 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 30 -10 0 MPRFLG 0 ; 
       32 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
