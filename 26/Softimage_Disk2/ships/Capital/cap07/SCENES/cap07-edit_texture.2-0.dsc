SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap07-cap07.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.3-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_destroyB_sPTL-light1_1.3-0 ROOT ; 
       rix_destroyB_sPTL-light2_1.3-0 ROOT ; 
       rix_destroyB_sPTL-light3.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       edit_texture-nose_white-center.1-0_10.1-0 ; 
       edit_texture-nose_white-center.1-0_11.1-0 ; 
       edit_texture-nose_white-center.1-0_12.1-0 ; 
       edit_texture-nose_white-center.1-0_13.1-0 ; 
       edit_texture-nose_white-center.1-0_14.1-0 ; 
       edit_texture-nose_white-center.1-0_15.1-0 ; 
       edit_texture-nose_white-center.1-0_16.1-0 ; 
       edit_texture-nose_white-center.1-0_5.1-0 ; 
       edit_texture-nose_white-center.1-0_6.1-0 ; 
       edit_texture-nose_white-center.1-0_7.1-0 ; 
       edit_texture-nose_white-center.1-0_8.1-0 ; 
       edit_texture-nose_white-center.1-0_9.1-0 ; 
       edit_texture-nose_white-center.1-10.1-0 ; 
       edit_texture-nose_white-center.1-11.1-0 ; 
       edit_texture-nose_white-center.1-12.1-0 ; 
       edit_texture-nose_white-center.1-3.1-0 ; 
       rix_destroyB_sPTL-mat1.1-0 ; 
       rix_destroyB_sPTL-mat10.1-0 ; 
       rix_destroyB_sPTL-mat11.1-0 ; 
       rix_destroyB_sPTL-mat13.1-0 ; 
       rix_destroyB_sPTL-mat15.1-0 ; 
       rix_destroyB_sPTL-mat16.1-0 ; 
       rix_destroyB_sPTL-mat17.1-0 ; 
       rix_destroyB_sPTL-mat20.1-0 ; 
       rix_destroyB_sPTL-mat21.1-0 ; 
       rix_destroyB_sPTL-mat22.1-0 ; 
       rix_destroyB_sPTL-mat23.1-0 ; 
       rix_destroyB_sPTL-mat24.1-0 ; 
       rix_destroyB_sPTL-mat29.1-0 ; 
       rix_destroyB_sPTL-mat3.1-0 ; 
       rix_destroyB_sPTL-mat30.1-0 ; 
       rix_destroyB_sPTL-mat31.1-0 ; 
       rix_destroyB_sPTL-mat32.1-0 ; 
       rix_destroyB_sPTL-mat33.1-0 ; 
       rix_destroyB_sPTL-mat38.1-0 ; 
       rix_destroyB_sPTL-mat40.1-0 ; 
       rix_destroyB_sPTL-mat41.1-0 ; 
       rix_destroyB_sPTL-mat42.1-0 ; 
       rix_destroyB_sPTL-mat43.1-0 ; 
       rix_destroyB_sPTL-mat46.1-0 ; 
       rix_destroyB_sPTL-mat47.1-0 ; 
       rix_destroyB_sPTL-mat49.1-0 ; 
       rix_destroyB_sPTL-mat5.1-0 ; 
       rix_destroyB_sPTL-mat50.1-0 ; 
       rix_destroyB_sPTL-mat51.1-0 ; 
       rix_destroyB_sPTL-mat52.1-0 ; 
       rix_destroyB_sPTL-mat53.1-0 ; 
       rix_destroyB_sPTL-mat54.1-0 ; 
       rix_destroyB_sPTL-mat55.1-0 ; 
       rix_destroyB_sPTL-mat56.1-0 ; 
       rix_destroyB_sPTL-mat57.1-0 ; 
       rix_destroyB_sPTL-mat58.1-0 ; 
       rix_destroyB_sPTL-mat59.1-0 ; 
       rix_destroyB_sPTL-mat6.1-0 ; 
       rix_destroyB_sPTL-mat7.1-0 ; 
       rix_destroyB_sPTL-mat8.1-0 ; 
       rix_destroyB_sPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.2-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-lndpad1.1-0 ; 
       cap07-lndpad2.1-0 ; 
       cap07-lndpad3.1-0 ; 
       cap07-lndpad4.1-0 ; 
       cap07-lndpad5.1-0 ; 
       cap07-lndpad6.1-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-SSb4.1-0 ; 
       cap07-SSb7.1-0 ; 
       cap07-SSb7_1.1-0 ; 
       cap07-SSb8.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-SSm10.1-0 ; 
       cap07-SSm11.1-0 ; 
       cap07-SSm12.1-0 ; 
       cap07-SSm13.1-0 ; 
       cap07-SSm14.1-0 ; 
       cap07-SSm15.1-0 ; 
       cap07-SSm5.1-0 ; 
       cap07-SSm5_1.1-0 ; 
       cap07-SSm6.1-0 ; 
       cap07-SSm7.1-0 ; 
       cap07-SSm8.1-0 ; 
       cap07-SSm9.1-0 ; 
       cap07-twepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap03 ; 
       D:/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-edit_texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       rix_destroyB_sPTL-t2d10.1-0 ; 
       rix_destroyB_sPTL-t2d12.1-0 ; 
       rix_destroyB_sPTL-t2d13.1-0 ; 
       rix_destroyB_sPTL-t2d16.1-0 ; 
       rix_destroyB_sPTL-t2d17.1-0 ; 
       rix_destroyB_sPTL-t2d18.1-0 ; 
       rix_destroyB_sPTL-t2d19.1-0 ; 
       rix_destroyB_sPTL-t2d2.1-0 ; 
       rix_destroyB_sPTL-t2d22.1-0 ; 
       rix_destroyB_sPTL-t2d23.1-0 ; 
       rix_destroyB_sPTL-t2d24.1-0 ; 
       rix_destroyB_sPTL-t2d25.1-0 ; 
       rix_destroyB_sPTL-t2d29.1-0 ; 
       rix_destroyB_sPTL-t2d30.1-0 ; 
       rix_destroyB_sPTL-t2d31.1-0 ; 
       rix_destroyB_sPTL-t2d32.1-0 ; 
       rix_destroyB_sPTL-t2d33.1-0 ; 
       rix_destroyB_sPTL-t2d36.1-0 ; 
       rix_destroyB_sPTL-t2d37.1-0 ; 
       rix_destroyB_sPTL-t2d38.1-0 ; 
       rix_destroyB_sPTL-t2d39.1-0 ; 
       rix_destroyB_sPTL-t2d4.1-0 ; 
       rix_destroyB_sPTL-t2d40.1-0 ; 
       rix_destroyB_sPTL-t2d41.1-0 ; 
       rix_destroyB_sPTL-t2d42.1-0 ; 
       rix_destroyB_sPTL-t2d43.1-0 ; 
       rix_destroyB_sPTL-t2d44.1-0 ; 
       rix_destroyB_sPTL-t2d45.1-0 ; 
       rix_destroyB_sPTL-t2d46.1-0 ; 
       rix_destroyB_sPTL-t2d5.1-0 ; 
       rix_destroyB_sPTL-t2d6.1-0 ; 
       rix_destroyB_sPTL-t2d7.1-0 ; 
       rix_destroyB_sPTL-t2d8.1-0 ; 
       utann_carrier_sPTd-t2d14.1-0 ; 
       utann_carrier_sPTd-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 0 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 0 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 6 110 ; 
       21 4 110 ; 
       22 6 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 19 110 ; 
       30 21 110 ; 
       31 19 110 ; 
       32 19 110 ; 
       33 21 110 ; 
       34 21 110 ; 
       35 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 23 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       3 22 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       4 16 300 ; 
       4 29 300 ; 
       4 54 300 ; 
       4 55 300 ; 
       4 56 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 42 300 ; 
       6 53 300 ; 
       6 50 300 ; 
       6 51 300 ; 
       6 52 300 ; 
       7 41 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 49 300 ; 
       14 45 300 ; 
       14 46 300 ; 
       14 47 300 ; 
       14 48 300 ; 
       15 15 300 ; 
       16 12 300 ; 
       17 13 300 ; 
       18 14 300 ; 
       23 1 300 ; 
       24 2 300 ; 
       25 3 300 ; 
       26 4 300 ; 
       27 5 300 ; 
       28 6 300 ; 
       29 7 300 ; 
       30 10 300 ; 
       31 8 300 ; 
       32 9 300 ; 
       33 11 300 ; 
       34 0 300 ; 
       35 28 300 ; 
       35 30 300 ; 
       35 31 300 ; 
       35 32 300 ; 
       35 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       18 32 401 ; 
       19 0 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       24 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       29 7 401 ; 
       30 8 401 ; 
       31 9 401 ; 
       32 10 401 ; 
       33 11 401 ; 
       34 12 401 ; 
       35 13 401 ; 
       36 14 401 ; 
       37 15 401 ; 
       38 16 401 ; 
       39 17 401 ; 
       40 18 401 ; 
       43 19 401 ; 
       44 20 401 ; 
       46 22 401 ; 
       47 23 401 ; 
       48 24 401 ; 
       49 25 401 ; 
       50 26 401 ; 
       51 27 401 ; 
       52 28 401 ; 
       53 21 401 ; 
       54 29 401 ; 
       55 30 401 ; 
       56 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 73.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 45 -10 0 MPRFLG 0 ; 
       2 SCHEM 77.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 70 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 78.75 -12 0 MPRFLG 0 ; 
       8 SCHEM 110 -10 0 MPRFLG 0 ; 
       9 SCHEM 112.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 115 -10 0 MPRFLG 0 ; 
       11 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 120 -10 0 MPRFLG 0 ; 
       13 SCHEM 122.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 68.75 -12 0 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 MPRFLG 0 ; 
       22 SCHEM 105 -10 0 MPRFLG 0 ; 
       23 SCHEM 107.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 102.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 105 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 130 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 125 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 127.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       31 SCHEM 0 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       33 SCHEM 17.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 20 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 57.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 107.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 105 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 125 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 127.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 145 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 132.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 80 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 156.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
