SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_destroyB_sPTL-light1_1.12-0 ROOT ; 
       rix_destroyB_sPTL-light2_1.12-0 ROOT ; 
       rix_destroyB_sPTL-light3.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       rix_destroyB_sPTL-mat1.3-0 ; 
       rix_destroyB_sPTL-mat10.3-0 ; 
       rix_destroyB_sPTL-mat11.3-0 ; 
       rix_destroyB_sPTL-mat13.3-0 ; 
       rix_destroyB_sPTL-mat15.3-0 ; 
       rix_destroyB_sPTL-mat16.3-0 ; 
       rix_destroyB_sPTL-mat17.3-0 ; 
       rix_destroyB_sPTL-mat20.3-0 ; 
       rix_destroyB_sPTL-mat21.3-0 ; 
       rix_destroyB_sPTL-mat22.3-0 ; 
       rix_destroyB_sPTL-mat23.3-0 ; 
       rix_destroyB_sPTL-mat24.3-0 ; 
       rix_destroyB_sPTL-mat29.3-0 ; 
       rix_destroyB_sPTL-mat3.3-0 ; 
       rix_destroyB_sPTL-mat30.3-0 ; 
       rix_destroyB_sPTL-mat31.3-0 ; 
       rix_destroyB_sPTL-mat32.3-0 ; 
       rix_destroyB_sPTL-mat33.3-0 ; 
       rix_destroyB_sPTL-mat38.3-0 ; 
       rix_destroyB_sPTL-mat40.3-0 ; 
       rix_destroyB_sPTL-mat41.3-0 ; 
       rix_destroyB_sPTL-mat42.3-0 ; 
       rix_destroyB_sPTL-mat43.3-0 ; 
       rix_destroyB_sPTL-mat46.3-0 ; 
       rix_destroyB_sPTL-mat47.3-0 ; 
       rix_destroyB_sPTL-mat49.3-0 ; 
       rix_destroyB_sPTL-mat5.3-0 ; 
       rix_destroyB_sPTL-mat50.3-0 ; 
       rix_destroyB_sPTL-mat51.3-0 ; 
       rix_destroyB_sPTL-mat52.3-0 ; 
       rix_destroyB_sPTL-mat53.3-0 ; 
       rix_destroyB_sPTL-mat54.3-0 ; 
       rix_destroyB_sPTL-mat55.3-0 ; 
       rix_destroyB_sPTL-mat56.3-0 ; 
       rix_destroyB_sPTL-mat57.3-0 ; 
       rix_destroyB_sPTL-mat58.3-0 ; 
       rix_destroyB_sPTL-mat59.3-0 ; 
       rix_destroyB_sPTL-mat6.3-0 ; 
       rix_destroyB_sPTL-mat7.3-0 ; 
       rix_destroyB_sPTL-mat8.3-0 ; 
       rix_destroyB_sPTL-mat9.3-0 ; 
       scaled-mat61.1-0 ; 
       scaled-mat62.1-0 ; 
       scaled-mat63.1-0 ; 
       scaled-mat64.1-0 ; 
       scaled-nose_white-center.1-0_10.1-0 ; 
       scaled-nose_white-center.1-0_11.1-0 ; 
       scaled-nose_white-center.1-0_12.1-0 ; 
       scaled-nose_white-center.1-0_13.1-0 ; 
       scaled-nose_white-center.1-0_14.1-0 ; 
       scaled-nose_white-center.1-0_15.1-0 ; 
       scaled-nose_white-center.1-0_16.1-0 ; 
       scaled-nose_white-center.1-0_5.1-0 ; 
       scaled-nose_white-center.1-0_6.1-0 ; 
       scaled-nose_white-center.1-0_7.1-0 ; 
       scaled-nose_white-center.1-0_8.1-0 ; 
       scaled-nose_white-center.1-0_9.1-0 ; 
       scaled-nose_white-center.1-10.1-0 ; 
       scaled-nose_white-center.1-11.1-0 ; 
       scaled-nose_white-center.1-12.1-0 ; 
       scaled-nose_white-center.1-3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       cap07-160deg.2-0 ROOT ; 
       cap07-160deg1.2-0 ROOT ; 
       cap07-180deg.2-0 ROOT ; 
       cap07-180deg1.2-0 ROOT ; 
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.10-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-cube3.1-0 ; 
       cap07-cube4.1-0 ; 
       cap07-cube6.1-0 ; 
       cap07-cube7.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-smoke.1-0 ; 
       cap07-SSb4.1-0 ; 
       cap07-SSb7.1-0 ; 
       cap07-SSb7_1.1-0 ; 
       cap07-SSb8.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-SSm10.1-0 ; 
       cap07-SSm11.1-0 ; 
       cap07-SSm12.1-0 ; 
       cap07-SSm13.1-0 ; 
       cap07-SSm14.1-0 ; 
       cap07-SSm15.1-0 ; 
       cap07-SSm5.1-0 ; 
       cap07-SSm5_1.1-0 ; 
       cap07-SSm6.1-0 ; 
       cap07-SSm7.1-0 ; 
       cap07-SSm8.1-0 ; 
       cap07-SSm9.1-0 ; 
       cap07-thrust.1-0 ; 
       cap07-trail.1-0 ; 
       cap07-turwepemt1.1-0 ; 
       cap07-turwepemt2.1-0 ; 
       cap07-turwepemt3.1-0 ; 
       cap07-turwepemt4.1-0 ; 
       cap07-twepatt.1-0 ; 
       cap07-wepemt1.1-0 ; 
       cap07-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap03 ; 
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       rix_destroyB_sPTL-t2d10.4-0 ; 
       rix_destroyB_sPTL-t2d12.4-0 ; 
       rix_destroyB_sPTL-t2d13.4-0 ; 
       rix_destroyB_sPTL-t2d16.4-0 ; 
       rix_destroyB_sPTL-t2d17.4-0 ; 
       rix_destroyB_sPTL-t2d18.4-0 ; 
       rix_destroyB_sPTL-t2d19.4-0 ; 
       rix_destroyB_sPTL-t2d2.4-0 ; 
       rix_destroyB_sPTL-t2d22.4-0 ; 
       rix_destroyB_sPTL-t2d23.4-0 ; 
       rix_destroyB_sPTL-t2d24.4-0 ; 
       rix_destroyB_sPTL-t2d25.4-0 ; 
       rix_destroyB_sPTL-t2d29.4-0 ; 
       rix_destroyB_sPTL-t2d30.4-0 ; 
       rix_destroyB_sPTL-t2d31.4-0 ; 
       rix_destroyB_sPTL-t2d32.4-0 ; 
       rix_destroyB_sPTL-t2d33.4-0 ; 
       rix_destroyB_sPTL-t2d36.4-0 ; 
       rix_destroyB_sPTL-t2d37.4-0 ; 
       rix_destroyB_sPTL-t2d38.4-0 ; 
       rix_destroyB_sPTL-t2d39.4-0 ; 
       rix_destroyB_sPTL-t2d4.5-0 ; 
       rix_destroyB_sPTL-t2d40.4-0 ; 
       rix_destroyB_sPTL-t2d41.4-0 ; 
       rix_destroyB_sPTL-t2d42.4-0 ; 
       rix_destroyB_sPTL-t2d43.4-0 ; 
       rix_destroyB_sPTL-t2d44.5-0 ; 
       rix_destroyB_sPTL-t2d45.5-0 ; 
       rix_destroyB_sPTL-t2d46.5-0 ; 
       rix_destroyB_sPTL-t2d5.4-0 ; 
       rix_destroyB_sPTL-t2d6.4-0 ; 
       rix_destroyB_sPTL-t2d7.4-0 ; 
       rix_destroyB_sPTL-t2d8.4-0 ; 
       scaled-t2d48.1-0 ; 
       scaled-t2d49.1-0 ; 
       scaled-t2d50.1-0 ; 
       scaled-t2d51.1-0 ; 
       utann_carrier_sPTd-t2d14.3-0 ; 
       utann_carrier_sPTd-t2d5.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 13 110 ; 
       5 13 110 ; 
       7 12 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 6 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 14 110 ; 
       24 12 110 ; 
       25 14 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       29 23 110 ; 
       30 23 110 ; 
       31 23 110 ; 
       32 22 110 ; 
       33 24 110 ; 
       34 22 110 ; 
       35 22 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 6 110 ; 
       39 6 110 ; 
       41 8 110 ; 
       42 9 110 ; 
       43 10 110 ; 
       44 13 110 ; 
       45 6 110 ; 
       46 6 110 ; 
       11 6 110 ; 
       40 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       7 6 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       7 20 300 ; 
       7 21 300 ; 
       7 22 300 ; 
       8 41 300 ; 
       9 42 300 ; 
       10 43 300 ; 
       12 0 300 ; 
       12 13 300 ; 
       12 38 300 ; 
       12 39 300 ; 
       12 40 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       14 26 300 ; 
       14 37 300 ; 
       14 34 300 ; 
       14 35 300 ; 
       14 36 300 ; 
       15 25 300 ; 
       15 27 300 ; 
       15 28 300 ; 
       15 33 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       18 60 300 ; 
       19 57 300 ; 
       20 58 300 ; 
       21 59 300 ; 
       26 46 300 ; 
       27 47 300 ; 
       28 48 300 ; 
       29 49 300 ; 
       30 50 300 ; 
       31 51 300 ; 
       32 52 300 ; 
       33 55 300 ; 
       34 53 300 ; 
       35 54 300 ; 
       36 56 300 ; 
       37 45 300 ; 
       44 12 300 ; 
       44 14 300 ; 
       44 15 300 ; 
       44 16 300 ; 
       44 17 300 ; 
       11 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 33 400 ; 
       9 34 400 ; 
       10 35 400 ; 
       11 36 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 21 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 59.46476 -12.69853 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 13.3512 13.3512 13.3512 1.593258 1.636781 3.174727 -0.9928954 0.1694246 -25.02672 MPRFLG 0 ; 
       1 SCHEM 61.87538 -12.71487 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 13.3512 13.3512 13.3512 1.548335 1.636782 -0.03313401 1.035283 0.1694246 -25.01134 MPRFLG 0 ; 
       2 SCHEM 57.36611 -12.76766 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 13.3512 13.3512 13.3512 0.9295948 -7.650281e-008 9.222256e-007 -0.003131567 20.7161 7.237432 MPRFLG 0 ; 
       3 SCHEM 55.09441 -12.64424 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 13.3512 13.3512 13.3512 2.211998 -7.650281e-008 -9.222256e-007 0.01036527 -20.71019 7.25055 MPRFLG 0 ; 
       4 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 25 -8 0 MPRFLG 0 ; 
       8 SCHEM 57.18193 -8.799345 0 USR MPRFLG 0 ; 
       9 SCHEM 59.46497 -8.815994 0 USR MPRFLG 0 ; 
       10 SCHEM 61.77235 -8.782678 0 USR MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       13 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 43.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 35 -12 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 MPRFLG 0 ; 
       25 SCHEM 40 -10 0 MPRFLG 0 ; 
       26 SCHEM 42.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 40 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 50 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 45 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 47.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 22.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 0 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 20 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 57.18193 -10.79934 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59.46497 -10.81599 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.77235 -10.78268 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 30 -10 0 MPRFLG 0 ; 
       45 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55.04383 -8.813927 0 USR MPRFLG 0 ; 
       40 SCHEM 55.06921 -10.77225 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 114.9308 17.70855 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 182.6796 44.21645 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 315.8943 97.24889 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 383.6431 123.7568 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 122.4308 17.70855 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 190.1796 44.21645 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 323.3943 97.24889 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 391.1431 123.7568 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
