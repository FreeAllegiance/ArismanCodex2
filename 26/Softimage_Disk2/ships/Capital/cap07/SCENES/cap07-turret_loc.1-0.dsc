SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap07-cap07.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.9-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_destroyB_sPTL-light1_1.9-0 ROOT ; 
       rix_destroyB_sPTL-light2_1.9-0 ROOT ; 
       rix_destroyB_sPTL-light3.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       rix_destroyB_sPTL-mat1.2-0 ; 
       rix_destroyB_sPTL-mat10.2-0 ; 
       rix_destroyB_sPTL-mat11.2-0 ; 
       rix_destroyB_sPTL-mat13.2-0 ; 
       rix_destroyB_sPTL-mat15.2-0 ; 
       rix_destroyB_sPTL-mat16.2-0 ; 
       rix_destroyB_sPTL-mat17.2-0 ; 
       rix_destroyB_sPTL-mat20.2-0 ; 
       rix_destroyB_sPTL-mat21.2-0 ; 
       rix_destroyB_sPTL-mat22.2-0 ; 
       rix_destroyB_sPTL-mat23.2-0 ; 
       rix_destroyB_sPTL-mat24.2-0 ; 
       rix_destroyB_sPTL-mat29.2-0 ; 
       rix_destroyB_sPTL-mat3.2-0 ; 
       rix_destroyB_sPTL-mat30.2-0 ; 
       rix_destroyB_sPTL-mat31.2-0 ; 
       rix_destroyB_sPTL-mat32.2-0 ; 
       rix_destroyB_sPTL-mat33.2-0 ; 
       rix_destroyB_sPTL-mat38.2-0 ; 
       rix_destroyB_sPTL-mat40.2-0 ; 
       rix_destroyB_sPTL-mat41.2-0 ; 
       rix_destroyB_sPTL-mat42.2-0 ; 
       rix_destroyB_sPTL-mat43.2-0 ; 
       rix_destroyB_sPTL-mat46.2-0 ; 
       rix_destroyB_sPTL-mat47.2-0 ; 
       rix_destroyB_sPTL-mat49.2-0 ; 
       rix_destroyB_sPTL-mat5.2-0 ; 
       rix_destroyB_sPTL-mat50.2-0 ; 
       rix_destroyB_sPTL-mat51.2-0 ; 
       rix_destroyB_sPTL-mat52.2-0 ; 
       rix_destroyB_sPTL-mat53.2-0 ; 
       rix_destroyB_sPTL-mat54.2-0 ; 
       rix_destroyB_sPTL-mat55.2-0 ; 
       rix_destroyB_sPTL-mat56.2-0 ; 
       rix_destroyB_sPTL-mat57.2-0 ; 
       rix_destroyB_sPTL-mat58.2-0 ; 
       rix_destroyB_sPTL-mat59.2-0 ; 
       rix_destroyB_sPTL-mat6.2-0 ; 
       rix_destroyB_sPTL-mat7.2-0 ; 
       rix_destroyB_sPTL-mat8.2-0 ; 
       rix_destroyB_sPTL-mat9.2-0 ; 
       turret_loc-nose_white-center.1-0_10.1-0 ; 
       turret_loc-nose_white-center.1-0_11.1-0 ; 
       turret_loc-nose_white-center.1-0_12.1-0 ; 
       turret_loc-nose_white-center.1-0_13.1-0 ; 
       turret_loc-nose_white-center.1-0_14.1-0 ; 
       turret_loc-nose_white-center.1-0_15.1-0 ; 
       turret_loc-nose_white-center.1-0_16.1-0 ; 
       turret_loc-nose_white-center.1-0_5.1-0 ; 
       turret_loc-nose_white-center.1-0_6.1-0 ; 
       turret_loc-nose_white-center.1-0_7.1-0 ; 
       turret_loc-nose_white-center.1-0_8.1-0 ; 
       turret_loc-nose_white-center.1-0_9.1-0 ; 
       turret_loc-nose_white-center.1-10.1-0 ; 
       turret_loc-nose_white-center.1-11.1-0 ; 
       turret_loc-nose_white-center.1-12.1-0 ; 
       turret_loc-nose_white-center.1-3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       cap07-armour0.1-0 ; 
       cap07-bwepatt.1-0 ; 
       cap07-cap07.6-0 ROOT ; 
       cap07-contwr.1-0 ; 
       cap07-fuselg1.3-0 ; 
       cap07-fuselg2.1-0 ; 
       cap07-fuselg3.1-0 ; 
       cap07-larmour.2-0 ; 
       cap07-rarmour.1-0 ; 
       cap07-smoke.1-0 ; 
       cap07-SSb4.1-0 ; 
       cap07-SSb7.1-0 ; 
       cap07-SSb7_1.1-0 ; 
       cap07-SSb8.1-0 ; 
       cap07-SSm0b1.1-0 ; 
       cap07-SSm0b2.1-0 ; 
       cap07-SSm0t1.1-0 ; 
       cap07-SSm0t2.1-0 ; 
       cap07-SSm10.1-0 ; 
       cap07-SSm11.1-0 ; 
       cap07-SSm12.1-0 ; 
       cap07-SSm13.1-0 ; 
       cap07-SSm14.1-0 ; 
       cap07-SSm15.1-0 ; 
       cap07-SSm5.1-0 ; 
       cap07-SSm5_1.1-0 ; 
       cap07-SSm6.1-0 ; 
       cap07-SSm7.1-0 ; 
       cap07-SSm8.1-0 ; 
       cap07-SSm9.1-0 ; 
       cap07-thrust.1-0 ; 
       cap07-trail.1-0 ; 
       cap07-turwepemt1.1-0 ; 
       cap07-turwepemt2.1-0 ; 
       cap07-turwepemt3.1-0 ; 
       cap07-turwepemt4.1-0 ; 
       cap07-turwepemt5.1-0 ; 
       cap07-turwepemt6.1-0 ; 
       cap07-twepatt.1-0 ; 
       cap07-wepemt1.1-0 ; 
       cap07-wepemt2.1-0 ; 
       turret_loc-chn1.1-0 ROOT ; 
       turret_loc-chn2.1-0 ROOT ; 
       turret_loc-chn3.1-0 ROOT ; 
       turret_loc-chn4.1-0 ROOT ; 
       turret_loc-cone.1-0 ; 
       turret_loc-cone1.1-0 ; 
       turret_loc-cone2.1-0 ; 
       turret_loc-cone3.1-0 ; 
       turret_loc-cone4.1-0 ROOT ; 
       turret_loc-eff1.1-0 ; 
       turret_loc-eff2.1-0 ; 
       turret_loc-eff3.1-0 ; 
       turret_loc-eff4.1-0 ; 
       turret_loc-jnt1_1.1-0 ; 
       turret_loc-jnt2_1.1-0 ; 
       turret_loc-jnt3_1.1-0 ; 
       turret_loc-jnt4_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap03 ; 
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap07/PICTURES/cap07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap07-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       rix_destroyB_sPTL-t2d10.2-0 ; 
       rix_destroyB_sPTL-t2d12.2-0 ; 
       rix_destroyB_sPTL-t2d13.2-0 ; 
       rix_destroyB_sPTL-t2d16.2-0 ; 
       rix_destroyB_sPTL-t2d17.2-0 ; 
       rix_destroyB_sPTL-t2d18.2-0 ; 
       rix_destroyB_sPTL-t2d19.2-0 ; 
       rix_destroyB_sPTL-t2d2.2-0 ; 
       rix_destroyB_sPTL-t2d22.2-0 ; 
       rix_destroyB_sPTL-t2d23.2-0 ; 
       rix_destroyB_sPTL-t2d24.2-0 ; 
       rix_destroyB_sPTL-t2d25.2-0 ; 
       rix_destroyB_sPTL-t2d29.2-0 ; 
       rix_destroyB_sPTL-t2d30.2-0 ; 
       rix_destroyB_sPTL-t2d31.2-0 ; 
       rix_destroyB_sPTL-t2d32.2-0 ; 
       rix_destroyB_sPTL-t2d33.2-0 ; 
       rix_destroyB_sPTL-t2d36.2-0 ; 
       rix_destroyB_sPTL-t2d37.2-0 ; 
       rix_destroyB_sPTL-t2d38.2-0 ; 
       rix_destroyB_sPTL-t2d39.2-0 ; 
       rix_destroyB_sPTL-t2d4.2-0 ; 
       rix_destroyB_sPTL-t2d40.2-0 ; 
       rix_destroyB_sPTL-t2d41.2-0 ; 
       rix_destroyB_sPTL-t2d42.2-0 ; 
       rix_destroyB_sPTL-t2d43.2-0 ; 
       rix_destroyB_sPTL-t2d44.2-0 ; 
       rix_destroyB_sPTL-t2d45.2-0 ; 
       rix_destroyB_sPTL-t2d46.2-0 ; 
       rix_destroyB_sPTL-t2d5.2-0 ; 
       rix_destroyB_sPTL-t2d6.2-0 ; 
       rix_destroyB_sPTL-t2d7.2-0 ; 
       rix_destroyB_sPTL-t2d8.2-0 ; 
       utann_carrier_sPTd-t2d14.2-0 ; 
       utann_carrier_sPTd-t2d5.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 2 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 6 110 ; 
       16 4 110 ; 
       17 6 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       23 15 110 ; 
       24 14 110 ; 
       25 16 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 16 110 ; 
       29 16 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 5 110 ; 
       39 2 110 ; 
       40 2 110 ; 
       57 44 110 ; 
       45 54 110 ; 
       54 41 110 ; 
       50 54 110 ; 
       55 42 110 ; 
       51 55 110 ; 
       46 55 110 ; 
       56 43 110 ; 
       52 56 110 ; 
       47 56 110 ; 
       53 57 110 ; 
       48 57 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       3 6 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 0 300 ; 
       4 13 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       6 26 300 ; 
       6 37 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       7 25 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 33 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       10 56 300 ; 
       11 53 300 ; 
       12 54 300 ; 
       13 55 300 ; 
       18 42 300 ; 
       19 43 300 ; 
       20 44 300 ; 
       21 45 300 ; 
       22 46 300 ; 
       23 47 300 ; 
       24 48 300 ; 
       25 51 300 ; 
       26 49 300 ; 
       27 50 300 ; 
       28 52 300 ; 
       29 41 300 ; 
       38 12 300 ; 
       38 14 300 ; 
       38 15 300 ; 
       38 16 300 ; 
       38 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 32 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 21 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 MPRFLG 0 ; 
       5 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -12 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 MPRFLG 0 ; 
       17 SCHEM 40 -10 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 40 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 50 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 45 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 22.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 0 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 30 -10 0 MPRFLG 0 ; 
       39 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 104.6538 4.985445 0 USR DISPLAY 0 0 SRT 1.49 1.49 1.49 0 1.570796 0 -4.35873 -0.9277529 -20.25846 MPRFLG 0 ; 
       57 SCHEM 104.6538 2.985445 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 93.75 1.161381 0 DISPLAY 3 2 SRT 1.481722 1.481722 1.481722 1.570796 1.3808 1.570797 0 17.78151 6.212048 MPRFLG 0 ; 
       45 SCHEM 83.75 -2.838619 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 85 1.161381 0 DISPLAY 3 2 SRT 1.481722 1.481722 1.481722 -1.570796 1.500794 -1.570796 0 -17.64493 6.814795 MPRFLG 0 ; 
       54 SCHEM 85 -0.8386188 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 86.25 -2.838619 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 93.75 -0.8386188 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 95 -2.838619 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 92.5 -2.838619 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 99.65377 4.985445 0 USR SRT 1.49 1.49 1.49 0 1.570796 0 1.968709 -0.2617067 -20.25846 MPRFLG 0 ; 
       56 SCHEM 99.65377 2.985445 0 MPRFLG 0 ; 
       52 SCHEM 100.9038 0.985445 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 98.40377 0.985445 0 MPRFLG 0 ; 
       53 SCHEM 105.9038 0.985445 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 103.4038 0.985445 0 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 108.4038 4.985445 0 SRT 9.959606 9.959606 9.959605 -0.1112857 2.038332 1.451247 -1.817091 0.2646223 -20.01461 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       41 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 10 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 28 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
