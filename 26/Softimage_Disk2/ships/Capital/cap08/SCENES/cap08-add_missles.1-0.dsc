SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.14-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_missles-mat102.1-0 ; 
       add_missles-mat105.1-0 ; 
       add_missles-mat106.1-0 ; 
       add_missles-mat107.1-0 ; 
       add_missles-mat108.1-0 ; 
       add_missles-mat109.1-0 ; 
       add_missles-mat110.1-0 ; 
       add_missles-mat112.1-0 ; 
       add_missles-mat113.1-0 ; 
       add_missles-mat114.1-0 ; 
       add_missles-mat115.1-0 ; 
       add_missles-mat116.1-0 ; 
       add_missles-mat117.1-0 ; 
       add_missles-mat118.1-0 ; 
       add_missles-mat119.1-0 ; 
       add_missles-mat120.1-0 ; 
       add_missles-mat121.1-0 ; 
       add_missles-mat122.1-0 ; 
       add_missles-mat123.1-0 ; 
       add_missles-mat124.1-0 ; 
       add_missles-mat125.1-0 ; 
       add_missles-mat126.1-0 ; 
       add_missles-mat127.1-0 ; 
       add_missles-mat128.1-0 ; 
       add_missles-mat129.1-0 ; 
       add_missles-mat130.1-0 ; 
       add_missles-mat131.1-0 ; 
       add_missles-mat132.1-0 ; 
       add_missles-mat133.1-0 ; 
       add_missles-mat75.1-0 ; 
       add_missles-mat84.1-0 ; 
       add_missles-mat85.1-0 ; 
       add_missles-mat86.1-0 ; 
       add_missles-mat87.1-0 ; 
       add_missles-spine_turrets1.1-0 ; 
       rix_carrier_sTL-mat10.3-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.3-0 ; 
       rix_carrier_sTL-mat18.3-0 ; 
       rix_carrier_sTL-mat21.2-0 ; 
       rix_carrier_sTL-mat22.2-0 ; 
       rix_carrier_sTL-mat23.2-0 ; 
       rix_carrier_sTL-mat24.2-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.3-0 ; 
       rix_carrier_sTL-mat33.3-0 ; 
       rix_carrier_sTL-mat34.2-0 ; 
       rix_carrier_sTL-mat35.2-0 ; 
       rix_carrier_sTL-mat36.2-0 ; 
       rix_carrier_sTL-mat37.3-0 ; 
       rix_carrier_sTL-mat7.3-0 ; 
       rix_carrier_sTL-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 73     
       cap08-cap08.11-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-thrust.1-0 ; 
       cap08-trail.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt1_1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-add_missles.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       add_missles-t2d29.1-0 ; 
       add_missles-t2d30.1-0 ; 
       rix_carrier_sTL-ot2d1.3-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.3-0 ; 
       rix_carrier_sTL-t2d18.3-0 ; 
       rix_carrier_sTL-t2d19.3-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.3-0 ; 
       rix_carrier_sTL-t2d24.3-0 ; 
       rix_carrier_sTL-t2d25.3-0 ; 
       rix_carrier_sTL-t2d26.3-0 ; 
       rix_carrier_sTL-t2d27.3-0 ; 
       rix_carrier_sTL-t2d8.3-0 ; 
       rix_carrier_sTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 72 110 ; 
       6 71 110 ; 
       66 0 110 ; 
       57 0 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 64 110 ; 
       20 0 110 ; 
       21 1 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 2 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       41 21 110 ; 
       42 21 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 72 110 ; 
       46 72 110 ; 
       47 72 110 ; 
       48 71 110 ; 
       49 71 110 ; 
       50 71 110 ; 
       51 72 110 ; 
       52 72 110 ; 
       53 72 110 ; 
       54 71 110 ; 
       55 71 110 ; 
       56 71 110 ; 
       59 3 110 ; 
       60 59 110 ; 
       61 59 110 ; 
       62 59 110 ; 
       63 59 110 ; 
       64 2 110 ; 
       65 64 110 ; 
       67 64 110 ; 
       68 64 110 ; 
       69 64 110 ; 
       70 0 110 ; 
       71 70 110 ; 
       72 70 110 ; 
       58 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 40 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 51 300 ; 
       1 52 300 ; 
       1 53 300 ; 
       1 34 300 ; 
       2 55 300 ; 
       2 56 300 ; 
       2 35 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 49 300 ; 
       2 50 300 ; 
       2 54 300 ; 
       3 44 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       3 48 300 ; 
       22 6 300 ; 
       23 14 300 ; 
       24 13 300 ; 
       25 12 300 ; 
       26 11 300 ; 
       27 3 300 ; 
       28 4 300 ; 
       29 5 300 ; 
       31 16 300 ; 
       32 2 300 ; 
       33 0 300 ; 
       34 15 300 ; 
       35 1 300 ; 
       36 29 300 ; 
       37 7 300 ; 
       38 8 300 ; 
       39 9 300 ; 
       40 10 300 ; 
       41 30 300 ; 
       42 31 300 ; 
       43 32 300 ; 
       44 33 300 ; 
       45 17 300 ; 
       46 20 300 ; 
       47 21 300 ; 
       48 24 300 ; 
       49 25 300 ; 
       50 28 300 ; 
       51 18 300 ; 
       52 19 300 ; 
       53 22 300 ; 
       54 23 300 ; 
       55 26 300 ; 
       56 27 300 ; 
       71 36 300 ; 
       72 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 1 401 ; 
       35 15 401 ; 
       36 3 401 ; 
       37 4 401 ; 
       38 2 401 ; 
       39 5 401 ; 
       42 6 401 ; 
       43 7 401 ; 
       48 8 401 ; 
       49 9 401 ; 
       50 10 401 ; 
       51 11 401 ; 
       53 12 401 ; 
       54 13 401 ; 
       55 0 401 ; 
       56 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 103.75 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 166.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 MPRFLG 0 ; 
       4 SCHEM 60 -2 0 MPRFLG 0 ; 
       5 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       66 SCHEM 205 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 202.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 85 -8 0 MPRFLG 0 ; 
       9 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 90 -8 0 MPRFLG 0 ; 
       11 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 95 -8 0 MPRFLG 0 ; 
       13 SCHEM 130 -8 0 MPRFLG 0 ; 
       14 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 120 -8 0 MPRFLG 0 ; 
       16 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 125 -8 0 MPRFLG 0 ; 
       18 SCHEM 127.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 197.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 156.25 -4 0 MPRFLG 0 ; 
       22 SCHEM 157.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 140 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 142.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 145 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 147.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 150 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 152.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 155 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 137.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 160 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 162.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 165 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 167.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 170 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 172.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 175 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 100 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 72.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 75 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 132.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 105 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 107.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 77.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 80 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 82.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 110 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 112.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       60 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       61 SCHEM 50 -8 0 MPRFLG 0 ; 
       62 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       63 SCHEM 55 -8 0 MPRFLG 0 ; 
       64 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       65 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       67 SCHEM 10 -6 0 MPRFLG 0 ; 
       68 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       69 SCHEM 15 -6 0 MPRFLG 0 ; 
       70 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       71 SCHEM 120 -4 0 MPRFLG 0 ; 
       72 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       58 SCHEM 200 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 192.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 195 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 187.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 190 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 192.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 182.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 185 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 190 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 206.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
