SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.12-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_turrets-mat102.1-0 ; 
       add_turrets-mat105.1-0 ; 
       add_turrets-mat106.1-0 ; 
       add_turrets-mat107.1-0 ; 
       add_turrets-mat108.1-0 ; 
       add_turrets-mat109.1-0 ; 
       add_turrets-mat110.1-0 ; 
       add_turrets-mat112.1-0 ; 
       add_turrets-mat113.1-0 ; 
       add_turrets-mat114.1-0 ; 
       add_turrets-mat115.1-0 ; 
       add_turrets-mat116.1-0 ; 
       add_turrets-mat117.1-0 ; 
       add_turrets-mat118.1-0 ; 
       add_turrets-mat119.1-0 ; 
       add_turrets-mat120.1-0 ; 
       add_turrets-mat121.1-0 ; 
       add_turrets-mat122.1-0 ; 
       add_turrets-mat123.1-0 ; 
       add_turrets-mat124.1-0 ; 
       add_turrets-mat125.1-0 ; 
       add_turrets-mat126.1-0 ; 
       add_turrets-mat127.1-0 ; 
       add_turrets-mat128.1-0 ; 
       add_turrets-mat129.1-0 ; 
       add_turrets-mat130.1-0 ; 
       add_turrets-mat131.1-0 ; 
       add_turrets-mat132.1-0 ; 
       add_turrets-mat133.1-0 ; 
       add_turrets-mat75.1-0 ; 
       add_turrets-mat84.1-0 ; 
       add_turrets-mat85.1-0 ; 
       add_turrets-mat86.1-0 ; 
       add_turrets-mat87.1-0 ; 
       add_turrets-spine_turrets1.1-0 ; 
       rix_carrier_sTL-mat10.3-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.3-0 ; 
       rix_carrier_sTL-mat18.3-0 ; 
       rix_carrier_sTL-mat21.2-0 ; 
       rix_carrier_sTL-mat22.2-0 ; 
       rix_carrier_sTL-mat23.2-0 ; 
       rix_carrier_sTL-mat24.2-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.3-0 ; 
       rix_carrier_sTL-mat33.3-0 ; 
       rix_carrier_sTL-mat34.2-0 ; 
       rix_carrier_sTL-mat35.2-0 ; 
       rix_carrier_sTL-mat36.2-0 ; 
       rix_carrier_sTL-mat37.3-0 ; 
       rix_carrier_sTL-mat7.3-0 ; 
       rix_carrier_sTL-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 80     
       cap08-cap08.10-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-add_turrets.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       add_turrets-t2d29.1-0 ; 
       add_turrets-t2d30.1-0 ; 
       rix_carrier_sTL-ot2d1.3-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.3-0 ; 
       rix_carrier_sTL-t2d18.2-0 ; 
       rix_carrier_sTL-t2d19.2-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.3-0 ; 
       rix_carrier_sTL-t2d24.3-0 ; 
       rix_carrier_sTL-t2d25.2-0 ; 
       rix_carrier_sTL-t2d26.2-0 ; 
       rix_carrier_sTL-t2d27.3-0 ; 
       rix_carrier_sTL-t2d8.3-0 ; 
       rix_carrier_sTL-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 79 110 ; 
       6 78 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 72 110 ; 
       30 0 110 ; 
       31 1 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       37 31 110 ; 
       38 31 110 ; 
       39 31 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 2 110 ; 
       45 40 110 ; 
       46 40 110 ; 
       47 31 110 ; 
       48 31 110 ; 
       49 31 110 ; 
       50 31 110 ; 
       51 31 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 79 110 ; 
       56 79 110 ; 
       57 79 110 ; 
       58 78 110 ; 
       59 78 110 ; 
       60 78 110 ; 
       61 79 110 ; 
       62 79 110 ; 
       63 79 110 ; 
       64 78 110 ; 
       65 78 110 ; 
       66 78 110 ; 
       67 3 110 ; 
       68 67 110 ; 
       69 67 110 ; 
       70 67 110 ; 
       71 67 110 ; 
       72 2 110 ; 
       73 72 110 ; 
       74 72 110 ; 
       75 72 110 ; 
       76 72 110 ; 
       77 0 110 ; 
       78 77 110 ; 
       79 77 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 40 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 51 300 ; 
       1 52 300 ; 
       1 53 300 ; 
       1 34 300 ; 
       2 55 300 ; 
       2 56 300 ; 
       2 35 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 49 300 ; 
       2 50 300 ; 
       2 54 300 ; 
       3 44 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       3 48 300 ; 
       32 6 300 ; 
       33 14 300 ; 
       34 13 300 ; 
       35 12 300 ; 
       36 11 300 ; 
       37 3 300 ; 
       38 4 300 ; 
       39 5 300 ; 
       41 16 300 ; 
       42 2 300 ; 
       43 0 300 ; 
       44 15 300 ; 
       45 1 300 ; 
       46 29 300 ; 
       47 7 300 ; 
       48 8 300 ; 
       49 9 300 ; 
       50 10 300 ; 
       51 30 300 ; 
       52 31 300 ; 
       53 32 300 ; 
       54 33 300 ; 
       55 17 300 ; 
       56 20 300 ; 
       57 21 300 ; 
       58 24 300 ; 
       59 25 300 ; 
       60 28 300 ; 
       61 18 300 ; 
       62 19 300 ; 
       63 22 300 ; 
       64 23 300 ; 
       65 26 300 ; 
       66 27 300 ; 
       78 36 300 ; 
       79 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 1 401 ; 
       35 15 401 ; 
       36 3 401 ; 
       37 4 401 ; 
       38 2 401 ; 
       39 5 401 ; 
       42 6 401 ; 
       43 7 401 ; 
       48 8 401 ; 
       49 9 401 ; 
       50 10 401 ; 
       51 11 401 ; 
       53 12 401 ; 
       54 13 401 ; 
       55 0 401 ; 
       56 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 111.25 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 177.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 126.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 143.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 100 -8 0 MPRFLG 0 ; 
       10 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 90 -8 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 95 -8 0 MPRFLG 0 ; 
       14 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 132.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 120 -8 0 MPRFLG 0 ; 
       17 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 125 -8 0 MPRFLG 0 ; 
       19 SCHEM 127.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 130 -8 0 MPRFLG 0 ; 
       21 SCHEM 147.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 140 -6 0 MPRFLG 0 ; 
       23 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 145 -6 0 MPRFLG 0 ; 
       25 SCHEM 157.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 150 -6 0 MPRFLG 0 ; 
       27 SCHEM 152.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 155 -6 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 217.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 178.75 -4 0 MPRFLG 0 ; 
       32 SCHEM 180 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 162.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 165 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 167.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 170 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 172.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 175 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 177.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 160 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 182.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 185 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 187.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 190 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 192.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 195 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 197.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       68 SCHEM 60 -8 0 MPRFLG 0 ; 
       69 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       70 SCHEM 55 -8 0 MPRFLG 0 ; 
       71 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       72 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       73 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       74 SCHEM 10 -6 0 MPRFLG 0 ; 
       75 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       76 SCHEM 15 -6 0 MPRFLG 0 ; 
       77 SCHEM 106.25 -2 0 MPRFLG 0 ; 
       78 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       79 SCHEM 90 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 232.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 235 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 202.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 212.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 220 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
