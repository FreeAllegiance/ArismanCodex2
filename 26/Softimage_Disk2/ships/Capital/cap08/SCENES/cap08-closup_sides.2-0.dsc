SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.6-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       closup_sides-mat102.1-0 ; 
       closup_sides-mat105.1-0 ; 
       closup_sides-mat106.1-0 ; 
       closup_sides-mat107.1-0 ; 
       closup_sides-mat108.1-0 ; 
       closup_sides-mat109.1-0 ; 
       closup_sides-mat110.1-0 ; 
       closup_sides-mat111.1-0 ; 
       closup_sides-mat112.1-0 ; 
       closup_sides-mat113.1-0 ; 
       closup_sides-mat114.1-0 ; 
       closup_sides-mat115.1-0 ; 
       closup_sides-mat116.1-0 ; 
       closup_sides-mat117.1-0 ; 
       closup_sides-mat118.1-0 ; 
       closup_sides-mat119.1-0 ; 
       closup_sides-mat120.1-0 ; 
       closup_sides-mat121.1-0 ; 
       closup_sides-mat122.1-0 ; 
       closup_sides-mat123.1-0 ; 
       closup_sides-mat124.1-0 ; 
       closup_sides-mat125.1-0 ; 
       closup_sides-mat126.1-0 ; 
       closup_sides-mat127.1-0 ; 
       closup_sides-mat128.1-0 ; 
       closup_sides-mat129.1-0 ; 
       closup_sides-mat130.1-0 ; 
       closup_sides-mat131.1-0 ; 
       closup_sides-mat132.1-0 ; 
       closup_sides-mat133.1-0 ; 
       closup_sides-mat75.1-0 ; 
       closup_sides-mat84.1-0 ; 
       closup_sides-mat85.1-0 ; 
       closup_sides-mat86.1-0 ; 
       closup_sides-mat87.1-0 ; 
       rix_carrier_sTL-mat10.1-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.1-0 ; 
       rix_carrier_sTL-mat18.1-0 ; 
       rix_carrier_sTL-mat21.1-0 ; 
       rix_carrier_sTL-mat22.1-0 ; 
       rix_carrier_sTL-mat23.1-0 ; 
       rix_carrier_sTL-mat24.1-0 ; 
       rix_carrier_sTL-mat25.1-0 ; 
       rix_carrier_sTL-mat26.1-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.1-0 ; 
       rix_carrier_sTL-mat33.1-0 ; 
       rix_carrier_sTL-mat34.1-0 ; 
       rix_carrier_sTL-mat35.1-0 ; 
       rix_carrier_sTL-mat36.1-0 ; 
       rix_carrier_sTL-mat37.1-0 ; 
       rix_carrier_sTL-mat7.1-0 ; 
       rix_carrier_sTL-mat8.1-0 ; 
       rix_carrier_sTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 80     
       cap08-cap08.5-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-closup_sides.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       closup_sides-t2d28.1-0 ; 
       rix_carrier_sTL-ot2d1.1-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.1-0 ; 
       rix_carrier_sTL-t2d18.1-0 ; 
       rix_carrier_sTL-t2d19.1-0 ; 
       rix_carrier_sTL-t2d20.1-0 ; 
       rix_carrier_sTL-t2d21.1-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.1-0 ; 
       rix_carrier_sTL-t2d24.1-0 ; 
       rix_carrier_sTL-t2d25.1-0 ; 
       rix_carrier_sTL-t2d26.1-0 ; 
       rix_carrier_sTL-t2d27.1-0 ; 
       rix_carrier_sTL-t2d7.1-0 ; 
       rix_carrier_sTL-t2d8.1-0 ; 
       rix_carrier_sTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 79 110 ; 
       6 78 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 72 110 ; 
       30 0 110 ; 
       31 1 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       37 31 110 ; 
       38 31 110 ; 
       39 31 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 2 110 ; 
       45 40 110 ; 
       46 40 110 ; 
       47 31 110 ; 
       48 31 110 ; 
       49 31 110 ; 
       50 31 110 ; 
       51 31 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 79 110 ; 
       56 79 110 ; 
       57 79 110 ; 
       58 78 110 ; 
       59 78 110 ; 
       60 78 110 ; 
       61 79 110 ; 
       62 79 110 ; 
       63 79 110 ; 
       64 78 110 ; 
       65 78 110 ; 
       66 78 110 ; 
       67 3 110 ; 
       68 67 110 ; 
       69 67 110 ; 
       70 67 110 ; 
       71 67 110 ; 
       72 2 110 ; 
       73 72 110 ; 
       74 72 110 ; 
       75 72 110 ; 
       76 72 110 ; 
       77 0 110 ; 
       78 77 110 ; 
       79 77 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 40 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 53 300 ; 
       1 54 300 ; 
       1 55 300 ; 
       2 57 300 ; 
       2 58 300 ; 
       2 59 300 ; 
       2 35 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 44 300 ; 
       2 45 300 ; 
       2 51 300 ; 
       2 52 300 ; 
       2 56 300 ; 
       2 7 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       3 48 300 ; 
       3 49 300 ; 
       3 50 300 ; 
       32 6 300 ; 
       33 15 300 ; 
       34 14 300 ; 
       35 13 300 ; 
       36 12 300 ; 
       37 3 300 ; 
       38 4 300 ; 
       39 5 300 ; 
       41 17 300 ; 
       42 2 300 ; 
       43 0 300 ; 
       44 16 300 ; 
       45 1 300 ; 
       46 30 300 ; 
       47 8 300 ; 
       48 9 300 ; 
       49 10 300 ; 
       50 11 300 ; 
       51 31 300 ; 
       52 32 300 ; 
       53 33 300 ; 
       54 34 300 ; 
       55 18 300 ; 
       56 21 300 ; 
       57 22 300 ; 
       58 25 300 ; 
       59 26 300 ; 
       60 29 300 ; 
       61 19 300 ; 
       62 20 300 ; 
       63 23 300 ; 
       64 24 300 ; 
       65 27 300 ; 
       66 28 300 ; 
       78 36 300 ; 
       79 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       35 17 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 1 401 ; 
       39 4 401 ; 
       42 5 401 ; 
       43 6 401 ; 
       44 7 401 ; 
       45 8 401 ; 
       50 9 401 ; 
       51 10 401 ; 
       52 11 401 ; 
       53 12 401 ; 
       55 13 401 ; 
       56 14 401 ; 
       58 15 401 ; 
       59 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 81.25 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 128.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 113.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 65 -8 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 55 -8 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 60 -8 0 MPRFLG 0 ; 
       14 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 95 -8 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 85 -8 0 MPRFLG 0 ; 
       18 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 90 -8 0 MPRFLG 0 ; 
       20 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 100 -6 0 MPRFLG 0 ; 
       23 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 105 -6 0 MPRFLG 0 ; 
       25 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 110 -6 0 MPRFLG 0 ; 
       27 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 115 -6 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 160 -2 0 MPRFLG 0 ; 
       31 SCHEM 138.75 -4 0 MPRFLG 0 ; 
       32 SCHEM 140 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 122.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 125 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 127.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 130 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 132.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 135 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 137.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 120 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 142.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 145 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 147.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 150 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 155 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 157.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       68 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       69 SCHEM 30 -8 0 MPRFLG 0 ; 
       70 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       71 SCHEM 35 -8 0 MPRFLG 0 ; 
       72 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       73 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       74 SCHEM 10 -6 0 MPRFLG 0 ; 
       75 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       76 SCHEM 15 -6 0 MPRFLG 0 ; 
       77 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       78 SCHEM 83.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 53.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 131.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 134 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 139 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 119 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 141.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 144 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 146.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 126.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 124 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 96.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 151.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 154 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 156.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 99 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 99 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 159 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 159 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 159 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 159 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 161.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
