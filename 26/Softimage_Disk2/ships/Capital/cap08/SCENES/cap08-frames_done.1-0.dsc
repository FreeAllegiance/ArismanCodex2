SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.13-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.15-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.15-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.15-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.15-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       frames_done-mat102.1-0 ; 
       frames_done-mat105.1-0 ; 
       frames_done-mat106.1-0 ; 
       frames_done-mat107.1-0 ; 
       frames_done-mat108.1-0 ; 
       frames_done-mat109.1-0 ; 
       frames_done-mat110.1-0 ; 
       frames_done-mat112.1-0 ; 
       frames_done-mat113.1-0 ; 
       frames_done-mat114.1-0 ; 
       frames_done-mat115.1-0 ; 
       frames_done-mat116.1-0 ; 
       frames_done-mat117.1-0 ; 
       frames_done-mat118.1-0 ; 
       frames_done-mat119.1-0 ; 
       frames_done-mat120.1-0 ; 
       frames_done-mat121.1-0 ; 
       frames_done-mat122.1-0 ; 
       frames_done-mat123.1-0 ; 
       frames_done-mat124.1-0 ; 
       frames_done-mat125.1-0 ; 
       frames_done-mat126.1-0 ; 
       frames_done-mat127.1-0 ; 
       frames_done-mat128.1-0 ; 
       frames_done-mat129.1-0 ; 
       frames_done-mat130.1-0 ; 
       frames_done-mat131.1-0 ; 
       frames_done-mat132.1-0 ; 
       frames_done-mat133.1-0 ; 
       frames_done-mat134.1-0 ; 
       frames_done-mat75.1-0 ; 
       frames_done-mat84.1-0 ; 
       frames_done-mat85.1-0 ; 
       frames_done-mat86.1-0 ; 
       frames_done-mat87.1-0 ; 
       frames_done-spine_turrets1.1-0 ; 
       rix_carrier_sTL-mat10.4-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.4-0 ; 
       rix_carrier_sTL-mat18.4-0 ; 
       rix_carrier_sTL-mat21.2-0 ; 
       rix_carrier_sTL-mat22.2-0 ; 
       rix_carrier_sTL-mat23.2-0 ; 
       rix_carrier_sTL-mat24.2-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.4-0 ; 
       rix_carrier_sTL-mat33.4-0 ; 
       rix_carrier_sTL-mat34.2-0 ; 
       rix_carrier_sTL-mat35.2-0 ; 
       rix_carrier_sTL-mat36.2-0 ; 
       rix_carrier_sTL-mat37.4-0 ; 
       rix_carrier_sTL-mat7.4-0 ; 
       rix_carrier_sTL-mat9.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 84     
       cap08-cap08.12-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-missemt1.1-0 ; 
       cap08-missemt2.1-0 ; 
       cap08-missemt3.1-0 ; 
       cap08-missemt4.1-0 ; 
       cap08-missemt5.1-0 ; 
       cap08-missemt6.1-0 ; 
       cap08-missemt7.1-0 ; 
       cap08-missemt8.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-thrust.1-0 ; 
       cap08-trail.1-0 ; 
       cap08-turwepemt1.1-0 ; 
       cap08-turwepemt2.1-0 ; 
       cap08-turwepemt3.1-0 ; 
       cap08-turwepemt4.1-0 ; 
       cap08-turwepemt5.1-0 ; 
       cap08-turwepemt6.1-0 ; 
       cap08-turwepemt7.1-0 ; 
       cap08-turwepemt8.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt1_1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-frames_done.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       frames_done-t2d29.1-0 ; 
       frames_done-t2d30.1-0 ; 
       frames_done-t2d31.1-0 ; 
       rix_carrier_sTL-ot2d1.4-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.4-0 ; 
       rix_carrier_sTL-t2d18.3-0 ; 
       rix_carrier_sTL-t2d19.3-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.4-0 ; 
       rix_carrier_sTL-t2d24.4-0 ; 
       rix_carrier_sTL-t2d25.3-0 ; 
       rix_carrier_sTL-t2d26.3-0 ; 
       rix_carrier_sTL-t2d27.4-0 ; 
       rix_carrier_sTL-t2d8.4-0 ; 
       rix_carrier_sTL-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 83 110 ; 
       6 82 110 ; 
       77 0 110 ; 
       65 0 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       67 0 110 ; 
       27 75 110 ; 
       28 0 110 ; 
       29 1 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 29 110 ; 
       35 29 110 ; 
       36 29 110 ; 
       37 29 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       40 38 110 ; 
       41 38 110 ; 
       42 2 110 ; 
       43 38 110 ; 
       44 38 110 ; 
       45 29 110 ; 
       46 29 110 ; 
       47 29 110 ; 
       48 29 110 ; 
       49 29 110 ; 
       50 29 110 ; 
       51 29 110 ; 
       52 29 110 ; 
       53 83 110 ; 
       54 83 110 ; 
       55 83 110 ; 
       56 82 110 ; 
       57 82 110 ; 
       58 82 110 ; 
       59 83 110 ; 
       60 83 110 ; 
       61 83 110 ; 
       62 82 110 ; 
       63 82 110 ; 
       64 82 110 ; 
       68 0 110 ; 
       69 0 110 ; 
       70 0 110 ; 
       71 0 110 ; 
       72 0 110 ; 
       75 2 110 ; 
       76 75 110 ; 
       78 75 110 ; 
       79 75 110 ; 
       80 75 110 ; 
       81 0 110 ; 
       82 81 110 ; 
       83 81 110 ; 
       66 0 110 ; 
       26 0 110 ; 
       73 0 110 ; 
       74 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 44 300 ; 
       1 52 300 ; 
       1 53 300 ; 
       1 54 300 ; 
       1 35 300 ; 
       2 56 300 ; 
       2 57 300 ; 
       2 36 300 ; 
       2 39 300 ; 
       2 40 300 ; 
       2 50 300 ; 
       2 51 300 ; 
       2 55 300 ; 
       2 29 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       3 48 300 ; 
       3 49 300 ; 
       30 6 300 ; 
       31 14 300 ; 
       32 13 300 ; 
       33 12 300 ; 
       34 11 300 ; 
       35 3 300 ; 
       36 4 300 ; 
       37 5 300 ; 
       39 16 300 ; 
       40 2 300 ; 
       41 0 300 ; 
       42 15 300 ; 
       43 1 300 ; 
       44 30 300 ; 
       45 7 300 ; 
       46 8 300 ; 
       47 9 300 ; 
       48 10 300 ; 
       49 31 300 ; 
       50 32 300 ; 
       51 33 300 ; 
       52 34 300 ; 
       53 17 300 ; 
       54 20 300 ; 
       55 21 300 ; 
       56 24 300 ; 
       57 25 300 ; 
       58 28 300 ; 
       59 18 300 ; 
       60 19 300 ; 
       61 22 300 ; 
       62 23 300 ; 
       63 26 300 ; 
       64 27 300 ; 
       82 37 300 ; 
       83 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       35 1 401 ; 
       36 16 401 ; 
       37 4 401 ; 
       38 5 401 ; 
       39 3 401 ; 
       40 6 401 ; 
       43 7 401 ; 
       44 8 401 ; 
       49 9 401 ; 
       50 10 401 ; 
       51 11 401 ; 
       52 12 401 ; 
       54 13 401 ; 
       55 14 401 ; 
       56 0 401 ; 
       57 15 401 ; 
       29 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 91.25 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 111.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       5 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 81.25 -6 0 MPRFLG 0 ; 
       77 SCHEM 140 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 55 -8 0 MPRFLG 0 ; 
       13 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 75 -8 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 80 -8 0 MPRFLG 0 ; 
       17 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 85 -8 0 MPRFLG 0 ; 
       19 SCHEM 142.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 147.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 152.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 155 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 157.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 162.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 132.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       30 SCHEM 112.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 95 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 97.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 100 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 102.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 105 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 107.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 110 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 20 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 22.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 115 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 117.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 120 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 122.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 125 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 127.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 130 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 60 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 32.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 35 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 90 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       57 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM 65 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM 37.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       60 SCHEM 40 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 42.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 67.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 70 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 72.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       68 SCHEM 165 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 167.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 170 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 172.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 175 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       76 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       78 SCHEM 10 -6 0 MPRFLG 0 ; 
       79 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       80 SCHEM 15 -6 0 MPRFLG 0 ; 
       81 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       82 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       83 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       66 SCHEM 135 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 160 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 177.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 180 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 104 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 106.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 91.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 116.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 119 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 101.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 99 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 96.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 94 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 124 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 126.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 129 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 131.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 131.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 91.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 131.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 131.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 131.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 131.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 194.8812 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
