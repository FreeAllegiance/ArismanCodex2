SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.1-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rix_carrier_sTL-inf_light1_1.1-0 ROOT ; 
       rix_carrier_sTL-inf_light2_1.1-0 ROOT ; 
       rix_carrier_sTL-inf_light3_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       rix_carrier_F-mat102.1-0 ; 
       rix_carrier_F-mat105.1-0 ; 
       rix_carrier_F-mat106.1-0 ; 
       rix_carrier_F-mat107.1-0 ; 
       rix_carrier_F-mat108.1-0 ; 
       rix_carrier_F-mat109.1-0 ; 
       rix_carrier_F-mat110.1-0 ; 
       rix_carrier_F-mat111.1-0 ; 
       rix_carrier_F-mat112.1-0 ; 
       rix_carrier_F-mat113.1-0 ; 
       rix_carrier_F-mat114.1-0 ; 
       rix_carrier_F-mat115.1-0 ; 
       rix_carrier_F-mat116.1-0 ; 
       rix_carrier_F-mat117.1-0 ; 
       rix_carrier_F-mat118.1-0 ; 
       rix_carrier_F-mat119.1-0 ; 
       rix_carrier_F-mat120.1-0 ; 
       rix_carrier_F-mat121.1-0 ; 
       rix_carrier_F-mat122.1-0 ; 
       rix_carrier_F-mat123.1-0 ; 
       rix_carrier_F-mat124.1-0 ; 
       rix_carrier_F-mat125.1-0 ; 
       rix_carrier_F-mat126.1-0 ; 
       rix_carrier_F-mat127.1-0 ; 
       rix_carrier_F-mat128.1-0 ; 
       rix_carrier_F-mat129.1-0 ; 
       rix_carrier_F-mat130.1-0 ; 
       rix_carrier_F-mat131.1-0 ; 
       rix_carrier_F-mat132.1-0 ; 
       rix_carrier_F-mat133.1-0 ; 
       rix_carrier_F-mat75.1-0 ; 
       rix_carrier_F-mat84.1-0 ; 
       rix_carrier_F-mat85.1-0 ; 
       rix_carrier_F-mat86.1-0 ; 
       rix_carrier_F-mat87.1-0 ; 
       rix_carrier_sTL-mat10.1-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.1-0 ; 
       rix_carrier_sTL-mat18.1-0 ; 
       rix_carrier_sTL-mat21.1-0 ; 
       rix_carrier_sTL-mat22.1-0 ; 
       rix_carrier_sTL-mat23.1-0 ; 
       rix_carrier_sTL-mat24.1-0 ; 
       rix_carrier_sTL-mat25.1-0 ; 
       rix_carrier_sTL-mat26.1-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.1-0 ; 
       rix_carrier_sTL-mat33.1-0 ; 
       rix_carrier_sTL-mat34.1-0 ; 
       rix_carrier_sTL-mat35.1-0 ; 
       rix_carrier_sTL-mat36.1-0 ; 
       rix_carrier_sTL-mat37.1-0 ; 
       rix_carrier_sTL-mat7.1-0 ; 
       rix_carrier_sTL-mat8.1-0 ; 
       rix_carrier_sTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       cap08-cap08.1-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-rix_carrier_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       rix_carrier_F-t2d28.1-0 ; 
       rix_carrier_sTL-ot2d1.1-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.1-0 ; 
       rix_carrier_sTL-t2d18.1-0 ; 
       rix_carrier_sTL-t2d19.1-0 ; 
       rix_carrier_sTL-t2d20.1-0 ; 
       rix_carrier_sTL-t2d21.1-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.1-0 ; 
       rix_carrier_sTL-t2d24.1-0 ; 
       rix_carrier_sTL-t2d25.1-0 ; 
       rix_carrier_sTL-t2d26.1-0 ; 
       rix_carrier_sTL-t2d27.1-0 ; 
       rix_carrier_sTL-t2d7.1-0 ; 
       rix_carrier_sTL-t2d8.1-0 ; 
       rix_carrier_sTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       30 1 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 30 110 ; 
       37 30 110 ; 
       38 30 110 ; 
       39 2 110 ; 
       40 2 110 ; 
       41 39 110 ; 
       42 39 110 ; 
       43 2 110 ; 
       44 39 110 ; 
       45 39 110 ; 
       46 30 110 ; 
       47 30 110 ; 
       48 30 110 ; 
       49 30 110 ; 
       50 30 110 ; 
       51 30 110 ; 
       52 30 110 ; 
       53 30 110 ; 
       54 78 110 ; 
       55 78 110 ; 
       56 78 110 ; 
       57 77 110 ; 
       58 77 110 ; 
       59 77 110 ; 
       60 78 110 ; 
       61 78 110 ; 
       62 78 110 ; 
       63 77 110 ; 
       64 77 110 ; 
       65 77 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 78 110 ; 
       6 77 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 71 110 ; 
       66 3 110 ; 
       67 66 110 ; 
       68 66 110 ; 
       69 66 110 ; 
       70 66 110 ; 
       71 2 110 ; 
       72 71 110 ; 
       73 71 110 ; 
       74 71 110 ; 
       75 71 110 ; 
       76 0 110 ; 
       77 76 110 ; 
       78 76 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       31 6 300 ; 
       32 15 300 ; 
       33 14 300 ; 
       34 13 300 ; 
       35 12 300 ; 
       36 3 300 ; 
       37 4 300 ; 
       38 5 300 ; 
       40 17 300 ; 
       41 2 300 ; 
       42 0 300 ; 
       43 16 300 ; 
       44 1 300 ; 
       45 30 300 ; 
       46 8 300 ; 
       47 9 300 ; 
       48 10 300 ; 
       49 11 300 ; 
       50 31 300 ; 
       51 32 300 ; 
       52 33 300 ; 
       53 34 300 ; 
       54 18 300 ; 
       55 21 300 ; 
       56 22 300 ; 
       57 25 300 ; 
       58 26 300 ; 
       59 29 300 ; 
       60 19 300 ; 
       61 20 300 ; 
       62 23 300 ; 
       63 24 300 ; 
       64 27 300 ; 
       65 28 300 ; 
       1 40 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       1 53 300 ; 
       1 54 300 ; 
       1 55 300 ; 
       2 57 300 ; 
       2 58 300 ; 
       2 59 300 ; 
       2 35 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       2 44 300 ; 
       2 45 300 ; 
       2 51 300 ; 
       2 52 300 ; 
       2 56 300 ; 
       2 7 300 ; 
       3 46 300 ; 
       3 47 300 ; 
       3 48 300 ; 
       3 49 300 ; 
       3 50 300 ; 
       77 36 300 ; 
       78 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       35 17 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 1 401 ; 
       39 4 401 ; 
       42 5 401 ; 
       43 6 401 ; 
       44 7 401 ; 
       45 8 401 ; 
       50 9 401 ; 
       51 10 401 ; 
       52 11 401 ; 
       53 12 401 ; 
       55 13 401 ; 
       56 14 401 ; 
       58 15 401 ; 
       59 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -4.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       30 SCHEM 13 -37.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 16.5 -44.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 16.5 -46.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 16.5 -48.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 16.5 -50.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 16.5 -52.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 16.5 -38.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 16.5 -40.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 16.5 -42.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 13 -3.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 13 -10.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 16.5 -6.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 16.5 -2.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 13 -8.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 16.5 -4.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 16.5 -0.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 16.5 -30.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 16.5 -32.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 16.5 -34.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 16.5 -36.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 16.5 -28.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 16.5 -26.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 16.5 -24.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 16.5 -22.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 16.5 -78.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 16.5 -84.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 16.5 -86.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 16.5 -116.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 16.5 -120.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 16.5 -122.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 16.5 -80.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 16.5 -82.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 16.5 -88.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 16.5 -114.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 16.5 -118.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 16.5 -124.5 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 6 -62.5 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 9.5 -45.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 9.5 -10.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 13 -73.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 9.5 -73.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 16.5 -95.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 16.5 -107.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 13 -57.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 13 -65.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 20 -98.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 20 -96.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 20 -94.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 20 -92.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20 -90.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 20 -100.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 20 -104.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20 -102.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 20 -112.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 20 -110.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 20 -108.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 20 -106.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 16.5 -58.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 16.5 -56.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 16.5 -54.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 16.5 -60.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 16.5 -66.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 16.5 -64.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 16.5 -62.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 16.5 -68.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 16.5 -16.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 16.5 -73.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 20 -76.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 20 -70.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 20 -74.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 20 -72.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 13 -16.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 16.5 -14.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 16.5 -12.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 16.5 -18.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 16.5 -20.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 9.5 -101.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 13 -113.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 13 -89.5 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -6.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -38.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -40.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -42.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -44.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -32.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -34.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -52.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -50.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -48.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -46.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -90.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -92.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -94.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 20 -96.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -98.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -100.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -114.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 20 -116.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -120.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20 -118.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20 -124.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20 -122.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 20 -24.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 20 -22.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -88.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -88.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -68.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -68.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -68.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -68.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -68.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 13 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -88.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -88.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -68.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9.5 1.25 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
