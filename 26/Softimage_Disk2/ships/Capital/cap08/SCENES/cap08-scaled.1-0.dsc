SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       rix_carrier_sTL-mat10.6-0 ; 
       rix_carrier_sTL-mat11.3-0 ; 
       rix_carrier_sTL-mat12.3-0 ; 
       rix_carrier_sTL-mat17.6-0 ; 
       rix_carrier_sTL-mat18.6-0 ; 
       rix_carrier_sTL-mat21.4-0 ; 
       rix_carrier_sTL-mat22.4-0 ; 
       rix_carrier_sTL-mat23.4-0 ; 
       rix_carrier_sTL-mat24.4-0 ; 
       rix_carrier_sTL-mat27.3-0 ; 
       rix_carrier_sTL-mat28.3-0 ; 
       rix_carrier_sTL-mat29.3-0 ; 
       rix_carrier_sTL-mat30.3-0 ; 
       rix_carrier_sTL-mat31.3-0 ; 
       rix_carrier_sTL-mat32.6-0 ; 
       rix_carrier_sTL-mat33.6-0 ; 
       rix_carrier_sTL-mat34.4-0 ; 
       rix_carrier_sTL-mat35.4-0 ; 
       rix_carrier_sTL-mat36.4-0 ; 
       rix_carrier_sTL-mat37.6-0 ; 
       rix_carrier_sTL-mat7.6-0 ; 
       rix_carrier_sTL-mat9.6-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat106.1-0 ; 
       scaled-mat107.1-0 ; 
       scaled-mat108.1-0 ; 
       scaled-mat109.1-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat112.1-0 ; 
       scaled-mat113.1-0 ; 
       scaled-mat114.1-0 ; 
       scaled-mat115.1-0 ; 
       scaled-mat116.1-0 ; 
       scaled-mat117.1-0 ; 
       scaled-mat118.1-0 ; 
       scaled-mat119.1-0 ; 
       scaled-mat120.1-0 ; 
       scaled-mat121.1-0 ; 
       scaled-mat122.1-0 ; 
       scaled-mat123.1-0 ; 
       scaled-mat124.1-0 ; 
       scaled-mat125.1-0 ; 
       scaled-mat126.1-0 ; 
       scaled-mat127.1-0 ; 
       scaled-mat128.1-0 ; 
       scaled-mat129.1-0 ; 
       scaled-mat130.1-0 ; 
       scaled-mat131.1-0 ; 
       scaled-mat132.1-0 ; 
       scaled-mat133.1-0 ; 
       scaled-mat134.1-0 ; 
       scaled-mat135.1-0 ; 
       scaled-mat136.1-0 ; 
       scaled-mat137.1-0 ; 
       scaled-mat75.1-0 ; 
       scaled-mat84.1-0 ; 
       scaled-mat85.1-0 ; 
       scaled-mat86.1-0 ; 
       scaled-mat87.1-0 ; 
       scaled-spine_turrets1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 96     
       arc-cone.2-0 ROOT ; 
       cap08-150deg1.1-0 ROOT ; 
       cap08-150deg2.1-0 ROOT ; 
       cap08-175deg.1-0 ROOT ; 
       cap08-175deg2.1-0 ROOT ; 
       cap08-cap08.15-0 ROOT ; 
       cap08-cone4.1-0 ; 
       cap08-cone5.1-0 ; 
       cap08-cone6.1-0 ; 
       cap08-cone7.1-0 ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-turwepemt1.1-0 ; 
       cap08-turwepemt2.1-0 ; 
       cap08-turwepemt3.1-0 ; 
       cap08-turwepemt4.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
       scaled-cone1.1-0 ROOT ; 
       scaled-cone2.1-0 ROOT ; 
       scaled-cone3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER NSELECTIONS NBELEM 1     
       scaled-strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rix_carrier_sTL-ot2d1.6-0 ; 
       rix_carrier_sTL-t2d10.3-0 ; 
       rix_carrier_sTL-t2d11.3-0 ; 
       rix_carrier_sTL-t2d17.6-0 ; 
       rix_carrier_sTL-t2d18.5-0 ; 
       rix_carrier_sTL-t2d19.5-0 ; 
       rix_carrier_sTL-t2d22.3-0 ; 
       rix_carrier_sTL-t2d23.6-0 ; 
       rix_carrier_sTL-t2d24.6-0 ; 
       rix_carrier_sTL-t2d25.5-0 ; 
       rix_carrier_sTL-t2d26.5-0 ; 
       rix_carrier_sTL-t2d27.6-0 ; 
       rix_carrier_sTL-t2d8.6-0 ; 
       rix_carrier_sTL-t2d9.6-0 ; 
       scaled-t2d29.1-0 ; 
       scaled-t2d30.1-0 ; 
       scaled-t2d31.1-0 ; 
       scaled-t2d32.1-0 ; 
       scaled-t2d33.1-0 ; 
       scaled-t2d34.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       81 6 110 ; 
       6 5 110 ; 
       10 5 110 ; 
       7 5 110 ; 
       82 7 110 ; 
       8 5 110 ; 
       83 8 110 ; 
       9 5 110 ; 
       84 9 110 ; 
       11 5 110 ; 
       12 13 110 ; 
       13 5 110 ; 
       14 92 110 ; 
       15 91 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 15 110 ; 
       25 15 110 ; 
       26 15 110 ; 
       27 15 110 ; 
       28 15 110 ; 
       29 15 110 ; 
       30 16 110 ; 
       31 16 110 ; 
       32 16 110 ; 
       33 16 110 ; 
       34 17 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 17 110 ; 
       38 85 110 ; 
       39 5 110 ; 
       40 10 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 40 110 ; 
       46 40 110 ; 
       47 40 110 ; 
       48 40 110 ; 
       49 11 110 ; 
       50 11 110 ; 
       51 49 110 ; 
       52 49 110 ; 
       53 11 110 ; 
       54 49 110 ; 
       55 49 110 ; 
       56 40 110 ; 
       57 40 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 40 110 ; 
       61 40 110 ; 
       62 40 110 ; 
       63 40 110 ; 
       64 92 110 ; 
       65 92 110 ; 
       66 92 110 ; 
       67 91 110 ; 
       68 91 110 ; 
       69 91 110 ; 
       70 92 110 ; 
       71 92 110 ; 
       72 92 110 ; 
       73 91 110 ; 
       74 91 110 ; 
       75 91 110 ; 
       76 12 110 ; 
       77 76 110 ; 
       78 76 110 ; 
       79 76 110 ; 
       80 76 110 ; 
       85 11 110 ; 
       86 85 110 ; 
       87 85 110 ; 
       88 85 110 ; 
       89 85 110 ; 
       90 5 110 ; 
       91 90 110 ; 
       92 90 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 51 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 60 300 ; 
       7 52 300 ; 
       8 53 300 ; 
       9 54 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 0 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 19 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       12 13 300 ; 
       41 28 300 ; 
       42 36 300 ; 
       43 35 300 ; 
       44 34 300 ; 
       45 33 300 ; 
       46 25 300 ; 
       47 26 300 ; 
       48 27 300 ; 
       50 38 300 ; 
       51 24 300 ; 
       52 22 300 ; 
       53 37 300 ; 
       54 23 300 ; 
       55 55 300 ; 
       56 29 300 ; 
       57 30 300 ; 
       58 31 300 ; 
       59 32 300 ; 
       60 56 300 ; 
       61 57 300 ; 
       62 58 300 ; 
       63 59 300 ; 
       64 39 300 ; 
       65 42 300 ; 
       66 43 300 ; 
       67 46 300 ; 
       68 47 300 ; 
       69 50 300 ; 
       70 40 300 ; 
       71 41 300 ; 
       72 44 300 ; 
       73 45 300 ; 
       74 48 300 ; 
       75 49 300 ; 
       91 1 300 ; 
       92 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 16 400 ; 
       7 17 400 ; 
       8 18 400 ; 
       9 19 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
       60 15 401 ; 
    EndOfCHAPTER 

    CHAPTER NSELECTIONS CHAPTER MODELS 
       0 55 10000 ; 
       0 52 10000 ; 
       0 54 10000 ; 
       0 51 10000 ; 
       0 53 10000 ; 
       0 50 10000 ; 
       0 70 10000 ; 
       0 64 10000 ; 
       0 71 10000 ; 
       0 65 10000 ; 
       0 66 10000 ; 
       0 72 10000 ; 
       0 73 10000 ; 
       0 67 10000 ; 
       0 68 10000 ; 
       0 75 10000 ; 
       0 69 10000 ; 
       0 74 10000 ; 
       0 60 10000 ; 
       0 61 10000 ; 
       0 62 10000 ; 
       0 63 10000 ; 
       0 41 10000 ; 
       0 48 10000 ; 
       0 47 10000 ; 
       0 46 10000 ; 
       0 56 10000 ; 
       0 57 10000 ; 
       0 58 10000 ; 
       0 59 10000 ; 
       0 45 10000 ; 
       0 44 10000 ; 
       0 43 10000 ; 
       0 42 10000 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 120.0362 10.88351 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 10.49343 10.49343 10.49343 -2.959593 0 -1.570796 -3.538872 0.6933587 -12.09596 MPRFLG 0 ; 
       0 SCHEM 88.91772 17.19644 0 USR DISPLAY 0 0 SRT 10.49966 10.49966 10.49966 0 0 -0.8600004 7.551412 3.840198 0 MPRFLG 0 ; 
       4 SCHEM 114.1995 10.96464 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 7.379344 7.379344 7.379344 1.951593 0 0 0 -30.14811 38.47372 MPRFLG 0 ; 
       81 SCHEM 116.7573 12.84544 0 USR MPRFLG 0 ; 
       6 SCHEM 116.7635 14.77804 0 USR MPRFLG 0 ; 
       5 SCHEM 111.25 0 0 SRT 1 1 1 0 0 0 0 0 19.27457 MPRFLG 0 ; 
       10 SCHEM 166.7928 2.217994 0 USR MPRFLG 0 ; 
       7 SCHEM 114.0675 14.7725 0 USR MPRFLG 0 ; 
       82 SCHEM 114.0613 12.8399 0 USR MPRFLG 0 ; 
       8 SCHEM 119.9635 14.77804 0 USR MPRFLG 0 ; 
       83 SCHEM 119.9574 12.84545 0 USR MPRFLG 0 ; 
       9 SCHEM 122.9122 14.72829 0 USR MPRFLG 0 ; 
       84 SCHEM 122.906 12.7957 0 USR MPRFLG 0 ; 
       2 SCHEM 122.9572 10.9517 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 10.49343 10.49343 10.49343 -0.1819998 0 -1.570796 3.538872 0.6933587 -12.09596 MPRFLG 0 ; 
       11 SCHEM 88.54655 10.65398 0 USR MPRFLG 0 ; 
       12 SCHEM 72.55828 -0.7553896 0 MPRFLG 0 ; 
       13 SCHEM 72.55828 1.244611 0 USR MPRFLG 0 ; 
       14 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 126.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 133.0428 0.2179939 0 MPRFLG 0 ; 
       17 SCHEM 143.0428 0.2179939 0 MPRFLG 0 ; 
       18 SCHEM 100 -8 0 MPRFLG 0 ; 
       19 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 90 -8 0 MPRFLG 0 ; 
       21 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 95 -8 0 MPRFLG 0 ; 
       23 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 132.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 120 -8 0 MPRFLG 0 ; 
       26 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 125 -8 0 MPRFLG 0 ; 
       28 SCHEM 127.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 130 -8 0 MPRFLG 0 ; 
       30 SCHEM 136.7928 -1.782006 0 MPRFLG 0 ; 
       31 SCHEM 129.2928 -1.782006 0 MPRFLG 0 ; 
       32 SCHEM 131.7928 -1.782006 0 MPRFLG 0 ; 
       33 SCHEM 134.2928 -1.782006 0 MPRFLG 0 ; 
       34 SCHEM 146.7928 -1.782006 0 MPRFLG 0 ; 
       35 SCHEM 139.2928 -1.782006 0 MPRFLG 0 ; 
       36 SCHEM 141.7928 -1.782006 0 MPRFLG 0 ; 
       37 SCHEM 144.2928 -1.782006 0 MPRFLG 0 ; 
       38 SCHEM 79.79655 6.653982 0 MPRFLG 0 ; 
       39 SCHEM 133.4646 5.787066 0 USR MPRFLG 0 ; 
       40 SCHEM 168.0428 0.2179939 0 MPRFLG 0 ; 
       41 SCHEM 169.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 151.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 154.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 156.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 159.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 161.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 164.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 166.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 86.04655 8.653982 0 MPRFLG 0 ; 
       50 SCHEM 64.79654 8.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 89.79655 6.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 82.29655 6.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 67.29655 8.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 84.79655 6.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 87.29655 6.653982 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 149.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 171.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 174.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 176.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 179.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 181.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 184.2928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       63 SCHEM 186.7928 -1.782006 0 WIRECOL 2 7 MPRFLG 0 ; 
       64 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 66.30828 -2.75539 0 MPRFLG 0 ; 
       77 SCHEM 70.05828 -4.755389 0 MPRFLG 0 ; 
       78 SCHEM 62.55828 -4.755389 0 MPRFLG 0 ; 
       79 SCHEM 65.05828 -4.755389 0 MPRFLG 0 ; 
       80 SCHEM 67.55828 -4.755389 0 MPRFLG 0 ; 
       85 SCHEM 74.79655 8.653982 0 MPRFLG 0 ; 
       86 SCHEM 69.79655 6.653982 0 MPRFLG 0 ; 
       87 SCHEM 72.29655 6.653982 0 MPRFLG 0 ; 
       88 SCHEM 74.79655 6.653982 0 MPRFLG 0 ; 
       89 SCHEM 77.29655 6.653982 0 MPRFLG 0 ; 
       90 SCHEM 106.25 -2 0 MPRFLG 0 ; 
       91 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       92 SCHEM 90 -4 0 MPRFLG 0 ; 
       93 SCHEM 91.41772 17.19644 0 USR DISPLAY 0 0 SRT 10.49966 10.49966 10.49966 0 3.141593 0.8600004 -7.551412 3.840198 0 MPRFLG 0 ; 
       94 SCHEM 93.91772 17.19644 0 USR DISPLAY 0 0 SRT 10.49966 10.49966 10.49966 0 1.509958e-007 2.281592 -7.551412 -3.840198 0 MPRFLG 0 ; 
       95 SCHEM 96.41772 17.19644 0 USR DISPLAY 0 0 SRT 10.49966 10.49966 10.49966 3.141593 1.509958e-007 0.8600005 7.551412 -3.840198 0 MPRFLG 0 ; 
       3 SCHEM 116.9307 10.88923 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 7.379344 7.379344 7.379344 1.19 0 0 0 30.14811 38.47372 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 232.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 248.2811 1.657706 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 255.7811 1.657706 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 263.2811 12.77804 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 273.2811 12.77804 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 202.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 212.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 235 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 250.7811 1.657706 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 258.2811 1.657706 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 265.7811 12.77804 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 416.5987 12.77804 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER NSELECTIONS 
       0 SCHEM 105.3824 19.23518 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
