SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.24-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.24-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       rix_carrier_sTL-mat10.6-0 ; 
       rix_carrier_sTL-mat11.3-0 ; 
       rix_carrier_sTL-mat12.3-0 ; 
       rix_carrier_sTL-mat17.6-0 ; 
       rix_carrier_sTL-mat18.6-0 ; 
       rix_carrier_sTL-mat21.4-0 ; 
       rix_carrier_sTL-mat22.4-0 ; 
       rix_carrier_sTL-mat23.4-0 ; 
       rix_carrier_sTL-mat24.4-0 ; 
       rix_carrier_sTL-mat27.3-0 ; 
       rix_carrier_sTL-mat28.3-0 ; 
       rix_carrier_sTL-mat29.3-0 ; 
       rix_carrier_sTL-mat30.3-0 ; 
       rix_carrier_sTL-mat31.3-0 ; 
       rix_carrier_sTL-mat32.6-0 ; 
       rix_carrier_sTL-mat33.6-0 ; 
       rix_carrier_sTL-mat34.4-0 ; 
       rix_carrier_sTL-mat35.4-0 ; 
       rix_carrier_sTL-mat36.4-0 ; 
       rix_carrier_sTL-mat37.6-0 ; 
       rix_carrier_sTL-mat7.6-0 ; 
       rix_carrier_sTL-mat9.6-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat106.1-0 ; 
       scaled-mat107.1-0 ; 
       scaled-mat108.1-0 ; 
       scaled-mat109.1-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat112.1-0 ; 
       scaled-mat113.1-0 ; 
       scaled-mat114.1-0 ; 
       scaled-mat115.1-0 ; 
       scaled-mat116.1-0 ; 
       scaled-mat117.1-0 ; 
       scaled-mat118.1-0 ; 
       scaled-mat119.1-0 ; 
       scaled-mat120.1-0 ; 
       scaled-mat121.1-0 ; 
       scaled-mat122.1-0 ; 
       scaled-mat123.1-0 ; 
       scaled-mat124.1-0 ; 
       scaled-mat125.1-0 ; 
       scaled-mat126.1-0 ; 
       scaled-mat127.1-0 ; 
       scaled-mat128.1-0 ; 
       scaled-mat129.1-0 ; 
       scaled-mat130.1-0 ; 
       scaled-mat131.1-0 ; 
       scaled-mat132.1-0 ; 
       scaled-mat133.1-0 ; 
       scaled-mat134.1-0 ; 
       scaled-mat135.1-0 ; 
       scaled-mat136.1-0 ; 
       scaled-mat137.1-0 ; 
       scaled-mat75.1-0 ; 
       scaled-mat84.1-0 ; 
       scaled-mat85.1-0 ; 
       scaled-mat86.1-0 ; 
       scaled-mat87.1-0 ; 
       scaled-spine_turrets1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 89     
       cap08-cap08.20-0 ROOT ; 
       cap08-cone4.1-0 ; 
       cap08-cone5.1-0 ; 
       cap08-cone6.1-0 ; 
       cap08-cone7.1-0 ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-missemt.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-turwepemt1.1-0 ; 
       cap08-turwepemt2.1-0 ; 
       cap08-turwepemt3.1-0 ; 
       cap08-turwepemt4.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-scaled.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rix_carrier_sTL-ot2d1.6-0 ; 
       rix_carrier_sTL-t2d10.3-0 ; 
       rix_carrier_sTL-t2d11.3-0 ; 
       rix_carrier_sTL-t2d17.6-0 ; 
       rix_carrier_sTL-t2d18.5-0 ; 
       rix_carrier_sTL-t2d19.5-0 ; 
       rix_carrier_sTL-t2d22.3-0 ; 
       rix_carrier_sTL-t2d23.6-0 ; 
       rix_carrier_sTL-t2d24.6-0 ; 
       rix_carrier_sTL-t2d25.5-0 ; 
       rix_carrier_sTL-t2d26.5-0 ; 
       rix_carrier_sTL-t2d27.6-0 ; 
       rix_carrier_sTL-t2d8.6-0 ; 
       rix_carrier_sTL-t2d9.6-0 ; 
       scaled-t2d29.1-0 ; 
       scaled-t2d30.1-0 ; 
       scaled-t2d31.1-0 ; 
       scaled-t2d32.1-0 ; 
       scaled-t2d33.1-0 ; 
       scaled-t2d34.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 88 110 ; 
       10 87 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 10 110 ; 
       23 10 110 ; 
       24 10 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       34 81 110 ; 
       35 0 110 ; 
       36 5 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 36 110 ; 
       40 36 110 ; 
       41 36 110 ; 
       42 36 110 ; 
       43 36 110 ; 
       44 36 110 ; 
       45 6 110 ; 
       46 6 110 ; 
       47 45 110 ; 
       48 45 110 ; 
       49 6 110 ; 
       50 45 110 ; 
       51 45 110 ; 
       52 36 110 ; 
       53 36 110 ; 
       54 36 110 ; 
       55 36 110 ; 
       56 36 110 ; 
       57 36 110 ; 
       58 36 110 ; 
       59 36 110 ; 
       60 88 110 ; 
       61 88 110 ; 
       62 88 110 ; 
       63 87 110 ; 
       64 87 110 ; 
       65 87 110 ; 
       66 88 110 ; 
       67 88 110 ; 
       68 88 110 ; 
       69 87 110 ; 
       70 87 110 ; 
       71 87 110 ; 
       72 7 110 ; 
       73 72 110 ; 
       74 72 110 ; 
       75 72 110 ; 
       76 72 110 ; 
       77 1 110 ; 
       78 2 110 ; 
       79 3 110 ; 
       80 4 110 ; 
       81 6 110 ; 
       82 81 110 ; 
       83 81 110 ; 
       84 81 110 ; 
       85 81 110 ; 
       86 0 110 ; 
       87 86 110 ; 
       88 86 110 ; 
       33 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 51 300 ; 
       2 52 300 ; 
       3 53 300 ; 
       4 54 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 60 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 0 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 19 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       37 28 300 ; 
       38 36 300 ; 
       39 35 300 ; 
       40 34 300 ; 
       41 33 300 ; 
       42 25 300 ; 
       43 26 300 ; 
       44 27 300 ; 
       46 38 300 ; 
       47 24 300 ; 
       48 22 300 ; 
       49 37 300 ; 
       50 23 300 ; 
       51 55 300 ; 
       52 29 300 ; 
       53 30 300 ; 
       54 31 300 ; 
       55 32 300 ; 
       56 56 300 ; 
       57 57 300 ; 
       58 58 300 ; 
       59 59 300 ; 
       60 39 300 ; 
       61 42 300 ; 
       62 43 300 ; 
       63 46 300 ; 
       64 47 300 ; 
       65 50 300 ; 
       66 40 300 ; 
       67 41 300 ; 
       68 44 300 ; 
       69 45 300 ; 
       70 48 300 ; 
       71 49 300 ; 
       87 1 300 ; 
       88 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 16 400 ; 
       2 17 400 ; 
       3 18 400 ; 
       4 19 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
       60 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 86.25 0 0 DISPLAY 3 2 SRT 0.08467198 0.08467198 0.08467198 0 0 0 0 0 1.102146 MPRFLG 0 ; 
       1 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 47.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 141.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 6.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 71.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 101.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 116.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 126.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 77.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 67.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 70 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 72.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 107.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 95 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 97.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 100 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 102.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 105 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 120 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 112.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 115 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 117.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 130 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 122.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 125 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 127.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 50 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 151.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 152.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 135 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 137.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 140 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 142.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 145 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 147.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 150 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 33.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 12.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 37.5 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 30 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 15 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 32.5 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 35 -6 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 132.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 155 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 157.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 160 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 162.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 165 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 167.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 170 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 80 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 52.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 55 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 110 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 82.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 85 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 57.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 60 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 62.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 87.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 90 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 92.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 6.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 10 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 2.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 42.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 40 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 45 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       80 SCHEM 47.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       84 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       85 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       86 SCHEM 81.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       87 SCHEM 96.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       88 SCHEM 66.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 85.43719 -11.28592 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 144 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 146.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 151.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 131.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 154 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 156.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 159 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 141.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 139 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 136.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 134 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 81.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 91.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 84 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 161.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 164 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 166.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 169 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 171.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 111.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 171.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 171.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 171.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 171.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 171.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
