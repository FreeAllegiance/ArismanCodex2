SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       rix_carrier_sTL-mat10.6-0 ; 
       rix_carrier_sTL-mat11.3-0 ; 
       rix_carrier_sTL-mat12.3-0 ; 
       rix_carrier_sTL-mat17.6-0 ; 
       rix_carrier_sTL-mat18.6-0 ; 
       rix_carrier_sTL-mat21.4-0 ; 
       rix_carrier_sTL-mat22.4-0 ; 
       rix_carrier_sTL-mat23.4-0 ; 
       rix_carrier_sTL-mat24.4-0 ; 
       rix_carrier_sTL-mat27.3-0 ; 
       rix_carrier_sTL-mat28.3-0 ; 
       rix_carrier_sTL-mat29.3-0 ; 
       rix_carrier_sTL-mat30.3-0 ; 
       rix_carrier_sTL-mat31.3-0 ; 
       rix_carrier_sTL-mat32.6-0 ; 
       rix_carrier_sTL-mat33.6-0 ; 
       rix_carrier_sTL-mat34.4-0 ; 
       rix_carrier_sTL-mat35.4-0 ; 
       rix_carrier_sTL-mat36.4-0 ; 
       rix_carrier_sTL-mat37.6-0 ; 
       rix_carrier_sTL-mat7.6-0 ; 
       rix_carrier_sTL-mat9.6-0 ; 
       STATIC-mat134.2-0 ; 
       STATIC-mat135.1-0 ; 
       STATIC-mat136.1-0 ; 
       STATIC-mat137.1-0 ; 
       STATIC-spine_turrets1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 50     
       cap08-cap08.19-0 ROOT ; 
       cap08-cone4.1-0 ; 
       cap08-cone5.1-0 ; 
       cap08-cone6.1-0 ; 
       cap08-cone7.1-0 ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSf.2-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       rix_carrier_sTL-ot2d1.6-0 ; 
       rix_carrier_sTL-t2d10.3-0 ; 
       rix_carrier_sTL-t2d11.3-0 ; 
       rix_carrier_sTL-t2d17.6-0 ; 
       rix_carrier_sTL-t2d18.5-0 ; 
       rix_carrier_sTL-t2d19.5-0 ; 
       rix_carrier_sTL-t2d22.3-0 ; 
       rix_carrier_sTL-t2d23.6-0 ; 
       rix_carrier_sTL-t2d24.6-0 ; 
       rix_carrier_sTL-t2d25.5-0 ; 
       rix_carrier_sTL-t2d26.5-0 ; 
       rix_carrier_sTL-t2d27.6-0 ; 
       rix_carrier_sTL-t2d8.6-0 ; 
       rix_carrier_sTL-t2d9.6-0 ; 
       STATIC-t2d29.2-0 ; 
       STATIC-t2d30.2-0 ; 
       STATIC-t2d31.2-0 ; 
       STATIC-t2d32.1-0 ; 
       STATIC-t2d33.1-0 ; 
       STATIC-t2d34.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 49 110 ; 
       10 48 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 10 110 ; 
       23 10 110 ; 
       24 10 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 42 110 ; 
       34 0 110 ; 
       35 5 110 ; 
       36 6 110 ; 
       37 7 110 ; 
       38 37 110 ; 
       39 37 110 ; 
       40 37 110 ; 
       41 37 110 ; 
       42 6 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 42 110 ; 
       46 42 110 ; 
       47 0 110 ; 
       48 47 110 ; 
       49 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 22 300 ; 
       2 23 300 ; 
       3 24 300 ; 
       4 25 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 26 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 0 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 19 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       48 1 300 ; 
       49 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 16 400 ; 
       2 17 400 ; 
       3 18 400 ; 
       4 19 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
       26 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 46.25 0 0 SRT 0.08467198 0.08467198 0.08467198 0 0 0 0 0 1.102146 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 35 -2 0 MPRFLG 0 ; 
       5 SCHEM 80 -2 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 61.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 40 -8 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 45 -8 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 MPRFLG 0 ; 
       19 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 55 -8 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 MPRFLG 0 ; 
       23 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 65 -8 0 MPRFLG 0 ; 
       25 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 70 -6 0 MPRFLG 0 ; 
       27 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 75 -6 0 MPRFLG 0 ; 
       29 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 80 -6 0 MPRFLG 0 ; 
       31 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 85 -6 0 MPRFLG 0 ; 
       33 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 90 -4 0 MPRFLG 0 ; 
       36 SCHEM 25 -4 0 MPRFLG 0 ; 
       37 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       38 SCHEM 10 -8 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 5 -8 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 15 -6 0 MPRFLG 0 ; 
       45 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 20 -6 0 MPRFLG 0 ; 
       47 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       48 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       49 SCHEM 46.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
