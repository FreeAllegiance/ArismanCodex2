SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.4-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       rix_carrier_sTL-mat10.1-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.1-0 ; 
       rix_carrier_sTL-mat18.1-0 ; 
       rix_carrier_sTL-mat21.1-0 ; 
       rix_carrier_sTL-mat22.1-0 ; 
       rix_carrier_sTL-mat23.1-0 ; 
       rix_carrier_sTL-mat24.1-0 ; 
       rix_carrier_sTL-mat25.1-0 ; 
       rix_carrier_sTL-mat26.1-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.1-0 ; 
       rix_carrier_sTL-mat33.1-0 ; 
       rix_carrier_sTL-mat34.1-0 ; 
       rix_carrier_sTL-mat35.1-0 ; 
       rix_carrier_sTL-mat36.1-0 ; 
       rix_carrier_sTL-mat37.1-0 ; 
       rix_carrier_sTL-mat7.1-0 ; 
       rix_carrier_sTL-mat8.1-0 ; 
       rix_carrier_sTL-mat9.1-0 ; 
       static-mat111.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       cap08-cap08.3-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       rix_carrier_sTL-ot2d1.1-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.1-0 ; 
       rix_carrier_sTL-t2d18.1-0 ; 
       rix_carrier_sTL-t2d19.1-0 ; 
       rix_carrier_sTL-t2d20.1-0 ; 
       rix_carrier_sTL-t2d21.1-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.1-0 ; 
       rix_carrier_sTL-t2d24.1-0 ; 
       rix_carrier_sTL-t2d25.1-0 ; 
       rix_carrier_sTL-t2d26.1-0 ; 
       rix_carrier_sTL-t2d27.1-0 ; 
       rix_carrier_sTL-t2d7.1-0 ; 
       rix_carrier_sTL-t2d8.1-0 ; 
       rix_carrier_sTL-t2d9.1-0 ; 
       static-t2d28.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 21 300 ; 
       2 25 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       6 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       25 17 401 ; 
       0 16 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       23 14 401 ; 
       24 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       25 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       17 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 111.1796 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
