SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.20-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       rix_carrier_sTL-mat10.6-0 ; 
       rix_carrier_sTL-mat11.3-0 ; 
       rix_carrier_sTL-mat12.3-0 ; 
       rix_carrier_sTL-mat17.6-0 ; 
       rix_carrier_sTL-mat18.6-0 ; 
       rix_carrier_sTL-mat21.4-0 ; 
       rix_carrier_sTL-mat22.4-0 ; 
       rix_carrier_sTL-mat23.4-0 ; 
       rix_carrier_sTL-mat24.4-0 ; 
       rix_carrier_sTL-mat27.3-0 ; 
       rix_carrier_sTL-mat28.3-0 ; 
       rix_carrier_sTL-mat29.3-0 ; 
       rix_carrier_sTL-mat30.3-0 ; 
       rix_carrier_sTL-mat31.3-0 ; 
       rix_carrier_sTL-mat32.6-0 ; 
       rix_carrier_sTL-mat33.6-0 ; 
       rix_carrier_sTL-mat34.4-0 ; 
       rix_carrier_sTL-mat35.4-0 ; 
       rix_carrier_sTL-mat36.4-0 ; 
       rix_carrier_sTL-mat37.6-0 ; 
       rix_carrier_sTL-mat7.6-0 ; 
       rix_carrier_sTL-mat9.6-0 ; 
       static-spine_turrets1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       cap08-cap08.16-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       rix_carrier_sTL-ot2d1.6-0 ; 
       rix_carrier_sTL-t2d10.3-0 ; 
       rix_carrier_sTL-t2d11.3-0 ; 
       rix_carrier_sTL-t2d17.6-0 ; 
       rix_carrier_sTL-t2d18.5-0 ; 
       rix_carrier_sTL-t2d19.5-0 ; 
       rix_carrier_sTL-t2d22.3-0 ; 
       rix_carrier_sTL-t2d23.6-0 ; 
       rix_carrier_sTL-t2d24.6-0 ; 
       rix_carrier_sTL-t2d25.5-0 ; 
       rix_carrier_sTL-t2d26.5-0 ; 
       rix_carrier_sTL-t2d27.6-0 ; 
       rix_carrier_sTL-t2d8.6-0 ; 
       rix_carrier_sTL-t2d9.6-0 ; 
       static-t2d29.1-0 ; 
       static-t2d30.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 22 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 19 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       7 1 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
       22 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 111.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 19.27457 MPRFLG 0 ; 
       1 SCHEM 166.7928 2.217994 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 88.54655 10.65398 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 72.55828 -0.7553896 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 89.9328 1.771111 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 168.0428 0.2179939 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 106.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 122.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 90 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 232.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 202.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 212.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 235 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
