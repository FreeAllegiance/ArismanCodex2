SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 12     
       utann_heavy_fighter_land-utann_hvy_fighter_4.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_11.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.8-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       rix_carrier_sTL-mat10_1.1-0 ; 
       rix_carrier_sTL-mat11_1.1-0 ; 
       rix_carrier_sTL-mat12_1.1-0 ; 
       rix_carrier_sTL-mat17_1.1-0 ; 
       rix_carrier_sTL-mat18_1.1-0 ; 
       rix_carrier_sTL-mat21_1.1-0 ; 
       rix_carrier_sTL-mat22_1.1-0 ; 
       rix_carrier_sTL-mat23_1.1-0 ; 
       rix_carrier_sTL-mat24_1.1-0 ; 
       rix_carrier_sTL-mat27_1.1-0 ; 
       rix_carrier_sTL-mat28_1.1-0 ; 
       rix_carrier_sTL-mat29_1.1-0 ; 
       rix_carrier_sTL-mat30_1.1-0 ; 
       rix_carrier_sTL-mat31_1.1-0 ; 
       rix_carrier_sTL-mat32_1.1-0 ; 
       rix_carrier_sTL-mat33_1.1-0 ; 
       rix_carrier_sTL-mat34_1.1-0 ; 
       rix_carrier_sTL-mat35_1.1-0 ; 
       rix_carrier_sTL-mat36_1.1-0 ; 
       rix_carrier_sTL-mat37_1.1-0 ; 
       rix_carrier_sTL-mat7_1.1-0 ; 
       rix_carrier_sTL-mat9_1.1-0 ; 
       STATIC-spine_turrets1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       cap08-cap08.22-0 ROOT ; 
       cap08-fuselga_1.1-0 ; 
       cap08-fuselgf_1.1-0 ; 
       cap08-fuselgm_1.1-0 ; 
       cap08-fuselgm0_1.1-0 ; 
       cap08-wingzz0_1.1-0 ; 
       cap08-wingzzb_1.1-0 ; 
       cap08-wingzzt_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       rix_carrier_sTL-ot2d1_1.1-0 ; 
       rix_carrier_sTL-t2d10_1.1-0 ; 
       rix_carrier_sTL-t2d11_1.1-0 ; 
       rix_carrier_sTL-t2d17_1.1-0 ; 
       rix_carrier_sTL-t2d18_1.1-0 ; 
       rix_carrier_sTL-t2d19_1.1-0 ; 
       rix_carrier_sTL-t2d22_1.1-0 ; 
       rix_carrier_sTL-t2d23_1.1-0 ; 
       rix_carrier_sTL-t2d24_1.1-0 ; 
       rix_carrier_sTL-t2d25_1.1-0 ; 
       rix_carrier_sTL-t2d26_1.1-0 ; 
       rix_carrier_sTL-t2d27_1.1-0 ; 
       rix_carrier_sTL-t2d8_1.1-0 ; 
       rix_carrier_sTL-t2d9_1.1-0 ; 
       STATIC-t2d29.2-0 ; 
       STATIC-t2d30.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 22 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 19 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       6 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
       22 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 394.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 397 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 399.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 224.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 396.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 399 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 401.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 229.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 227 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
