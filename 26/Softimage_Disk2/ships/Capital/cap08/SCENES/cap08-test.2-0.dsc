SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.7-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.9-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.9-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       rix_carrier_sTL-mat10.2-0 ; 
       rix_carrier_sTL-mat11.1-0 ; 
       rix_carrier_sTL-mat12.1-0 ; 
       rix_carrier_sTL-mat17.2-0 ; 
       rix_carrier_sTL-mat18.2-0 ; 
       rix_carrier_sTL-mat21.1-0 ; 
       rix_carrier_sTL-mat22.1-0 ; 
       rix_carrier_sTL-mat23.1-0 ; 
       rix_carrier_sTL-mat24.1-0 ; 
       rix_carrier_sTL-mat27.1-0 ; 
       rix_carrier_sTL-mat28.1-0 ; 
       rix_carrier_sTL-mat29.1-0 ; 
       rix_carrier_sTL-mat30.1-0 ; 
       rix_carrier_sTL-mat31.1-0 ; 
       rix_carrier_sTL-mat32.2-0 ; 
       rix_carrier_sTL-mat33.2-0 ; 
       rix_carrier_sTL-mat34.1-0 ; 
       rix_carrier_sTL-mat35.1-0 ; 
       rix_carrier_sTL-mat36.1-0 ; 
       rix_carrier_sTL-mat37.2-0 ; 
       rix_carrier_sTL-mat7.2-0 ; 
       rix_carrier_sTL-mat9.2-0 ; 
       test-mat102.1-0 ; 
       test-mat105.1-0 ; 
       test-mat106.1-0 ; 
       test-mat107.1-0 ; 
       test-mat108.1-0 ; 
       test-mat109.1-0 ; 
       test-mat110.1-0 ; 
       test-mat111.1-0 ; 
       test-mat112.1-0 ; 
       test-mat113.1-0 ; 
       test-mat114.1-0 ; 
       test-mat115.1-0 ; 
       test-mat116.1-0 ; 
       test-mat117.1-0 ; 
       test-mat118.1-0 ; 
       test-mat119.1-0 ; 
       test-mat120.1-0 ; 
       test-mat121.1-0 ; 
       test-mat122.1-0 ; 
       test-mat123.1-0 ; 
       test-mat124.1-0 ; 
       test-mat125.1-0 ; 
       test-mat126.1-0 ; 
       test-mat127.1-0 ; 
       test-mat128.1-0 ; 
       test-mat129.1-0 ; 
       test-mat130.1-0 ; 
       test-mat131.1-0 ; 
       test-mat132.1-0 ; 
       test-mat133.1-0 ; 
       test-mat75.1-0 ; 
       test-mat84.1-0 ; 
       test-mat85.1-0 ; 
       test-mat86.1-0 ; 
       test-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 80     
       cap08-cap08.8-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-test.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       rix_carrier_sTL-ot2d1.2-0 ; 
       rix_carrier_sTL-t2d10.1-0 ; 
       rix_carrier_sTL-t2d11.1-0 ; 
       rix_carrier_sTL-t2d17.2-0 ; 
       rix_carrier_sTL-t2d18.1-0 ; 
       rix_carrier_sTL-t2d19.1-0 ; 
       rix_carrier_sTL-t2d22.1-0 ; 
       rix_carrier_sTL-t2d23.2-0 ; 
       rix_carrier_sTL-t2d24.2-0 ; 
       rix_carrier_sTL-t2d25.1-0 ; 
       rix_carrier_sTL-t2d26.1-0 ; 
       rix_carrier_sTL-t2d27.2-0 ; 
       rix_carrier_sTL-t2d8.2-0 ; 
       rix_carrier_sTL-t2d9.2-0 ; 
       test-t2d28.2-0 ; 
       test-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 79 110 ; 
       6 78 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 72 110 ; 
       30 0 110 ; 
       31 1 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       37 31 110 ; 
       38 31 110 ; 
       39 31 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       43 40 110 ; 
       42 40 110 ; 
       44 2 110 ; 
       45 40 110 ; 
       46 40 110 ; 
       47 31 110 ; 
       48 31 110 ; 
       49 31 110 ; 
       50 31 110 ; 
       51 31 110 ; 
       52 31 110 ; 
       53 31 110 ; 
       54 31 110 ; 
       55 79 110 ; 
       56 79 110 ; 
       57 79 110 ; 
       58 78 110 ; 
       59 78 110 ; 
       60 78 110 ; 
       61 79 110 ; 
       62 79 110 ; 
       63 79 110 ; 
       64 78 110 ; 
       65 78 110 ; 
       66 78 110 ; 
       67 3 110 ; 
       68 67 110 ; 
       69 67 110 ; 
       70 67 110 ; 
       71 67 110 ; 
       72 2 110 ; 
       73 72 110 ; 
       74 72 110 ; 
       75 72 110 ; 
       76 72 110 ; 
       77 0 110 ; 
       78 77 110 ; 
       79 77 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 19 300 ; 
       2 29 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       32 28 300 ; 
       33 37 300 ; 
       34 36 300 ; 
       35 35 300 ; 
       36 34 300 ; 
       37 25 300 ; 
       38 26 300 ; 
       39 27 300 ; 
       41 39 300 ; 
       43 24 300 ; 
       42 22 300 ; 
       44 38 300 ; 
       45 23 300 ; 
       46 52 300 ; 
       47 30 300 ; 
       48 31 300 ; 
       49 32 300 ; 
       50 33 300 ; 
       51 53 300 ; 
       52 54 300 ; 
       53 55 300 ; 
       54 56 300 ; 
       55 40 300 ; 
       56 43 300 ; 
       57 44 300 ; 
       58 47 300 ; 
       59 48 300 ; 
       60 51 300 ; 
       61 41 300 ; 
       62 42 300 ; 
       63 45 300 ; 
       64 46 300 ; 
       65 49 300 ; 
       66 50 300 ; 
       78 1 300 ; 
       79 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       29 14 401 ; 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 15 401 ; 
       21 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 111.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       1 SCHEM 177.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 93.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 126.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 143.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 153.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 100 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 87.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 90 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 95 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 97.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 132.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 120 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 122.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 125 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 127.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 130 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 147.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 140 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 142.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 145 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 157.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 150 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 152.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 155 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 217.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 178.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 180 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 162.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 165 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 167.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 170 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 172.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 175 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 177.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 27.5 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 20 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 22.5 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 25 -6 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 160 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 182.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 185 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 187.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 190 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 192.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 195 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 197.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 102.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 75 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 77.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 135 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 107.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 110 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 80 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 82.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 85 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 112.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 115 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 117.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 56.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 60 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 52.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 55 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 57.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 15 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 106.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 122.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 90 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       22 SCHEM 20 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 172.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 175 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 177.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 180 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 50 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 160 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 182.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 185 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 187.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 170 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 167.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 165 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 162.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 102.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 80 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 82.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 75 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 77.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 85 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 112.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 135 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 107.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 115 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 117.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 110 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 25 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 190 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 192.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 195 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 197.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 137.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 105 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 215 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 200 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 202.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 205 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 62.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 65 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 207.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 210 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 212.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 50 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 35 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 105 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 202.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 205 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 230 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 207.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 212.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 220 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 225 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 227.5 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
