SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       cap08-cap08.15-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rix_fighter_sPa-cam_int1.18-0 ROOT ; 
       rix_fighter_sPa-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       rix_carrier_sTL-mat10.5-0 ; 
       rix_carrier_sTL-mat11.2-0 ; 
       rix_carrier_sTL-mat12.2-0 ; 
       rix_carrier_sTL-mat17.5-0 ; 
       rix_carrier_sTL-mat18.5-0 ; 
       rix_carrier_sTL-mat21.3-0 ; 
       rix_carrier_sTL-mat22.3-0 ; 
       rix_carrier_sTL-mat23.3-0 ; 
       rix_carrier_sTL-mat24.3-0 ; 
       rix_carrier_sTL-mat27.2-0 ; 
       rix_carrier_sTL-mat28.2-0 ; 
       rix_carrier_sTL-mat29.2-0 ; 
       rix_carrier_sTL-mat30.2-0 ; 
       rix_carrier_sTL-mat31.2-0 ; 
       rix_carrier_sTL-mat32.5-0 ; 
       rix_carrier_sTL-mat33.5-0 ; 
       rix_carrier_sTL-mat34.3-0 ; 
       rix_carrier_sTL-mat35.3-0 ; 
       rix_carrier_sTL-mat36.3-0 ; 
       rix_carrier_sTL-mat37.5-0 ; 
       rix_carrier_sTL-mat7.5-0 ; 
       rix_carrier_sTL-mat9.5-0 ; 
       turret_loc-mat102.1-0 ; 
       turret_loc-mat105.1-0 ; 
       turret_loc-mat106.1-0 ; 
       turret_loc-mat107.1-0 ; 
       turret_loc-mat108.1-0 ; 
       turret_loc-mat109.1-0 ; 
       turret_loc-mat110.1-0 ; 
       turret_loc-mat112.1-0 ; 
       turret_loc-mat113.1-0 ; 
       turret_loc-mat114.1-0 ; 
       turret_loc-mat115.1-0 ; 
       turret_loc-mat116.1-0 ; 
       turret_loc-mat117.1-0 ; 
       turret_loc-mat118.1-0 ; 
       turret_loc-mat119.1-0 ; 
       turret_loc-mat120.1-0 ; 
       turret_loc-mat121.1-0 ; 
       turret_loc-mat122.1-0 ; 
       turret_loc-mat123.1-0 ; 
       turret_loc-mat124.1-0 ; 
       turret_loc-mat125.1-0 ; 
       turret_loc-mat126.1-0 ; 
       turret_loc-mat127.1-0 ; 
       turret_loc-mat128.1-0 ; 
       turret_loc-mat129.1-0 ; 
       turret_loc-mat130.1-0 ; 
       turret_loc-mat131.1-0 ; 
       turret_loc-mat132.1-0 ; 
       turret_loc-mat133.1-0 ; 
       turret_loc-mat75.1-0 ; 
       turret_loc-mat84.1-0 ; 
       turret_loc-mat85.1-0 ; 
       turret_loc-mat86.1-0 ; 
       turret_loc-mat87.1-0 ; 
       turret_loc-spine_turrets1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 84     
       arc-cone.1-0 ROOT ; 
       cap08-cap08.14-0 ROOT ; 
       cap08-fuselga.1-0 ; 
       cap08-fuselgf.1-0 ; 
       cap08-fuselgm.1-0 ; 
       cap08-fuselgm0.1-0 ; 
       cap08-lndpad0a.1-0 ; 
       cap08-lndpad0b.1-0 ; 
       cap08-lndpad0c.1-0 ; 
       cap08-lndpad0d.1-0 ; 
       cap08-lndpada1.1-0 ; 
       cap08-lndpada2.1-0 ; 
       cap08-lndpada3.1-0 ; 
       cap08-lndpada4.1-0 ; 
       cap08-lndpada5.1-0 ; 
       cap08-lndpada6.1-0 ; 
       cap08-lndpadb10.1-0 ; 
       cap08-lndpadb11.1-0 ; 
       cap08-lndpadb12.1-0 ; 
       cap08-lndpadb7.1-0 ; 
       cap08-lndpadb8.1-0 ; 
       cap08-lndpadb9.1-0 ; 
       cap08-lndpadc1.1-0 ; 
       cap08-lndpadc2.1-0 ; 
       cap08-lndpadc3.1-0 ; 
       cap08-lndpadc4.1-0 ; 
       cap08-lndpadd1.1-0 ; 
       cap08-lndpadd2.1-0 ; 
       cap08-lndpadd3.1-0 ; 
       cap08-lndpadd4.1-0 ; 
       cap08-mwepemt.1-0 ; 
       cap08-smoke.1-0 ; 
       cap08-SS0a.2-0 ; 
       cap08-SSb4.1-0 ; 
       cap08-SSbl1.1-0 ; 
       cap08-SSbl2.1-0 ; 
       cap08-SSbl3.1-0 ; 
       cap08-SSbl4.1-0 ; 
       cap08-SSbr1.1-0 ; 
       cap08-SSbr2.1-0 ; 
       cap08-SSbr3.1-0 ; 
       cap08-SSf.2-0 ; 
       cap08-SSl.1-0 ; 
       cap08-SSlb.1-0 ; 
       cap08-SSlt.1-0 ; 
       cap08-SSr.1-0 ; 
       cap08-SSrb.1-0 ; 
       cap08-SSrt.1-0 ; 
       cap08-SStl1.1-0 ; 
       cap08-SStl2.1-0 ; 
       cap08-SStl3.1-0 ; 
       cap08-SStl4.1-0 ; 
       cap08-SStr1.1-0 ; 
       cap08-SStr2.1-0 ; 
       cap08-SStr3.1-0 ; 
       cap08-SStr4.1-0 ; 
       cap08-SSwl1.1-0 ; 
       cap08-SSwl2.1-0 ; 
       cap08-SSwl3.4-0 ; 
       cap08-SSwl4.1-0 ; 
       cap08-SSwl5.1-0 ; 
       cap08-SSwl6.1-0 ; 
       cap08-SSwr1.1-0 ; 
       cap08-SSwr2.1-0 ; 
       cap08-SSwr3.4-0 ; 
       cap08-SSwr4.1-0 ; 
       cap08-SSwr5.1-0 ; 
       cap08-SSwr6.1-0 ; 
       cap08-turatt0.1-0 ; 
       cap08-turatt1.1-0 ; 
       cap08-turatt14.1-0 ; 
       cap08-turatt2.1-0 ; 
       cap08-turatt3.1-0 ; 
       cap08-wepemt0.1-0 ; 
       cap08-wepemt1.1-0 ; 
       cap08-wepemt2.1-0 ; 
       cap08-wepemt3.1-0 ; 
       cap08-wepemt4.1-0 ; 
       cap08-wingzz0.1-0 ; 
       cap08-wingzzb.1-0 ; 
       cap08-wingzzt.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
       turret_loc-cone2.1-0 ROOT ; 
       turret_loc-cone3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap08/PICTURES/cap08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap08-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       rix_carrier_sTL-ot2d1.5-0 ; 
       rix_carrier_sTL-t2d10.2-0 ; 
       rix_carrier_sTL-t2d11.2-0 ; 
       rix_carrier_sTL-t2d17.5-0 ; 
       rix_carrier_sTL-t2d18.4-0 ; 
       rix_carrier_sTL-t2d19.4-0 ; 
       rix_carrier_sTL-t2d22.2-0 ; 
       rix_carrier_sTL-t2d23.5-0 ; 
       rix_carrier_sTL-t2d24.5-0 ; 
       rix_carrier_sTL-t2d25.4-0 ; 
       rix_carrier_sTL-t2d26.4-0 ; 
       rix_carrier_sTL-t2d27.5-0 ; 
       rix_carrier_sTL-t2d8.5-0 ; 
       rix_carrier_sTL-t2d9.5-0 ; 
       turret_loc-t2d29.1-0 ; 
       turret_loc-t2d30.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 5 110 ; 
       5 1 110 ; 
       6 80 110 ; 
       7 79 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 7 110 ; 
       21 7 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 9 110 ; 
       29 9 110 ; 
       30 73 110 ; 
       31 1 110 ; 
       32 2 110 ; 
       33 32 110 ; 
       34 32 110 ; 
       35 32 110 ; 
       36 32 110 ; 
       37 32 110 ; 
       38 32 110 ; 
       39 32 110 ; 
       40 32 110 ; 
       41 3 110 ; 
       42 3 110 ; 
       43 41 110 ; 
       44 41 110 ; 
       45 3 110 ; 
       46 41 110 ; 
       47 41 110 ; 
       48 32 110 ; 
       49 32 110 ; 
       50 32 110 ; 
       51 32 110 ; 
       52 32 110 ; 
       53 32 110 ; 
       54 32 110 ; 
       55 32 110 ; 
       56 80 110 ; 
       57 80 110 ; 
       58 80 110 ; 
       59 79 110 ; 
       60 79 110 ; 
       61 79 110 ; 
       62 80 110 ; 
       63 80 110 ; 
       64 80 110 ; 
       65 79 110 ; 
       66 79 110 ; 
       67 79 110 ; 
       68 4 110 ; 
       69 68 110 ; 
       70 68 110 ; 
       71 68 110 ; 
       72 68 110 ; 
       73 3 110 ; 
       74 73 110 ; 
       75 73 110 ; 
       76 73 110 ; 
       77 73 110 ; 
       78 1 110 ; 
       79 78 110 ; 
       80 78 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 5 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 56 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 0 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 19 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       33 28 300 ; 
       34 36 300 ; 
       35 35 300 ; 
       36 34 300 ; 
       37 33 300 ; 
       38 25 300 ; 
       39 26 300 ; 
       40 27 300 ; 
       42 38 300 ; 
       43 24 300 ; 
       44 22 300 ; 
       45 37 300 ; 
       46 23 300 ; 
       47 51 300 ; 
       48 29 300 ; 
       49 30 300 ; 
       50 31 300 ; 
       51 32 300 ; 
       52 52 300 ; 
       53 53 300 ; 
       54 54 300 ; 
       55 55 300 ; 
       56 39 300 ; 
       57 42 300 ; 
       58 43 300 ; 
       59 46 300 ; 
       60 47 300 ; 
       61 50 300 ; 
       62 40 300 ; 
       63 41 300 ; 
       64 44 300 ; 
       65 45 300 ; 
       66 48 300 ; 
       67 49 300 ; 
       79 1 300 ; 
       80 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       56 15 401 ; 
       0 13 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 14 401 ; 
       21 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       81 SCHEM 222.5 0 0 SRT 10.49966 10.49966 10.49966 0 3.141593 0.8600004 -7.551412 3.840198 0 MPRFLG 0 ; 
       82 SCHEM 225 0 0 SRT 10.49966 10.49966 10.49966 0 1.509958e-007 2.281592 -7.551412 -3.840198 0 MPRFLG 0 ; 
       83 SCHEM 227.5 0 0 SRT 10.49966 10.49966 10.49966 3.141593 1.509958e-007 0.8600005 7.551412 -3.840198 0 MPRFLG 0 ; 
       1 SCHEM 111.25 0 0 SRT 1 1 1 0 0 0 0 0 -7.419998 MPRFLG 0 ; 
       2 SCHEM 177.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 126.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 143.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 100 -8 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 90 -8 0 MPRFLG 0 ; 
       13 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 95 -8 0 MPRFLG 0 ; 
       15 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 132.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 120 -8 0 MPRFLG 0 ; 
       18 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 125 -8 0 MPRFLG 0 ; 
       20 SCHEM 127.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 130 -8 0 MPRFLG 0 ; 
       22 SCHEM 147.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 140 -6 0 MPRFLG 0 ; 
       24 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 145 -6 0 MPRFLG 0 ; 
       26 SCHEM 157.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 150 -6 0 MPRFLG 0 ; 
       28 SCHEM 152.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 155 -6 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 217.5 -2 0 MPRFLG 0 ; 
       32 SCHEM 178.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 180 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 162.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 165 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 167.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 170 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 172.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 175 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 177.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       42 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 25 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 160 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 182.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 185 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 187.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM 190 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM 192.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 195 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 197.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       69 SCHEM 60 -8 0 MPRFLG 0 ; 
       70 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       71 SCHEM 55 -8 0 MPRFLG 0 ; 
       72 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       73 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       74 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       75 SCHEM 10 -6 0 MPRFLG 0 ; 
       76 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       77 SCHEM 15 -6 0 MPRFLG 0 ; 
       78 SCHEM 106.25 -2 0 MPRFLG 0 ; 
       79 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       80 SCHEM 90 -4 0 MPRFLG 0 ; 
       0 SCHEM 220 0 0 SRT 10.49966 10.49966 10.49966 0 0 -0.8600004 7.551412 3.840198 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       22 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 232.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 200 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 202.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 205 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 210 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 235 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 202.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 205 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 207.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 212.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 220 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 222.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 225 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 227.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
