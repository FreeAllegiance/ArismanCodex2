SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap09-cap09.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.7-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       add_gun-default1.1-0 ; 
       add_gun-mat100.1-0 ; 
       add_gun-mat101.1-0 ; 
       add_gun-mat102.1-0 ; 
       add_gun-mat104.1-0 ; 
       add_gun-mat105.1-0 ; 
       add_gun-mat108.1-0 ; 
       add_gun-mat109.1-0 ; 
       add_gun-mat110.1-0 ; 
       add_gun-mat111.1-0 ; 
       add_gun-mat113.1-0 ; 
       add_gun-mat114.1-0 ; 
       add_gun-mat115.1-0 ; 
       add_gun-mat116.1-0 ; 
       add_gun-mat117.1-0 ; 
       add_gun-mat118.1-0 ; 
       add_gun-mat119.1-0 ; 
       add_gun-mat120.1-0 ; 
       add_gun-mat124.1-0 ; 
       add_gun-mat125.1-0 ; 
       add_gun-mat126.1-0 ; 
       add_gun-mat127.1-0 ; 
       add_gun-mat128.1-0 ; 
       add_gun-mat131.1-0 ; 
       add_gun-mat131_1.1-0 ; 
       add_gun-mat132.1-0 ; 
       add_gun-mat133.1-0 ; 
       add_gun-mat134.1-0 ; 
       add_gun-mat136.1-0 ; 
       add_gun-mat137.1-0 ; 
       add_gun-mat138.1-0 ; 
       add_gun-mat139.1-0 ; 
       add_gun-mat140.1-0 ; 
       add_gun-mat92.1-0 ; 
       add_gun-mat99.1-0 ; 
       add_gun-nose_white-center.1-1.1-0 ; 
       add_gun-port_red-left.1-1.1-0 ; 
       add_gun-port_red-left.1-2.1-0 ; 
       add_gun-starbord_green-right.1-1.1-0 ; 
       add_gun-starbord_green-right.1-2.1-0 ; 
       add_gun-starbord_green-right.1-3.1-0 ; 
       add_gun-starbord_green-right.1-4.1-0 ; 
       add_gun-starbord_green-right.1-5.1-0 ; 
       add_gun-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 32     
       add_gun-circle1.1-0 ROOT ; 
       add_gun-circle2.1-0 ROOT ; 
       add_gun-circle3.1-0 ROOT ; 
       add_gun-circle4.1-0 ROOT ; 
       add_gun-circle5.1-0 ROOT ; 
       add_gun-circle6.1-0 ROOT ; 
       add_gun-gun.6-0 ROOT ; 
       cap09-antenn.1-0 ; 
       cap09-cap09.4-0 ROOT ; 
       cap09-contwr.1-0 ; 
       cap09-engine0.1-0 ; 
       cap09-fuselg.1-0 ; 
       cap09-lbengine.1-0 ; 
       cap09-ltengine.1-0 ; 
       cap09-rbengine.1-0 ; 
       cap09-rtengine.1-0 ; 
       cap09-SS1.1-0 ; 
       cap09-SSb1.1-0 ; 
       cap09-SSla.1-0 ; 
       cap09-SSlm.1-0 ; 
       cap09-SSra.1-0 ; 
       cap09-SSrm.1-0 ; 
       cap09-SSt0.1-0 ; 
       cap09-SSt1.1-0 ; 
       cap09-SSt2.1-0 ; 
       cap09-SSt3.1-0 ; 
       cap09-tlndpad1.1-0 ; 
       cap09-tlndpad2.1-0 ; 
       cap09-tlndpad3.1-0 ; 
       cap09-turatt.1-0 ; 
       cap09-wepemt1.1-0 ; 
       cap09-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-add_gun.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_gun-t2d10_1.1-0 ; 
       add_gun-t2d12_1.1-0 ; 
       add_gun-t2d13_1.1-0 ; 
       add_gun-t2d16_1.1-0 ; 
       add_gun-t2d17_1.1-0 ; 
       add_gun-t2d19_1.1-0 ; 
       add_gun-t2d21_1.1-0 ; 
       add_gun-t2d22_1.1-0 ; 
       add_gun-t2d23_1.1-0 ; 
       add_gun-t2d24_1.1-0 ; 
       add_gun-t2d25_1.1-0 ; 
       add_gun-t2d26_1.1-0 ; 
       add_gun-t2d27_1.1-0 ; 
       add_gun-t2d28_1.1-0 ; 
       add_gun-t2d32_1.1-0 ; 
       add_gun-t2d33_1.1-0 ; 
       add_gun-t2d34_1.1-0 ; 
       add_gun-t2d35_1.1-0 ; 
       add_gun-t2d36_1.1-0 ; 
       add_gun-t2d39.1-0 ; 
       add_gun-t2d40.1-0 ; 
       add_gun-t2d41.1-0 ; 
       add_gun-t2d42.1-0 ; 
       add_gun-t2d42_1.1-0 ; 
       add_gun-t2d43.1-0 ; 
       add_gun-t2d44.1-0 ; 
       add_gun-t2d45.1-0 ; 
       add_gun-t2d46.1-0 ; 
       add_gun-t2d7_1.1-0 ; 
       add_gun-t2d8_1.1-0 ; 
       add_gun-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 9 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 8 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 7 110 ; 
       17 11 110 ; 
       18 13 110 ; 
       19 11 110 ; 
       20 15 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 11 110 ; 
       30 11 110 ; 
       31 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 32 300 ; 
       9 0 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       11 24 300 ; 
       11 28 300 ; 
       11 29 300 ; 
       11 30 300 ; 
       11 31 300 ; 
       12 33 300 ; 
       12 2 300 ; 
       12 5 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       12 17 300 ; 
       13 33 300 ; 
       13 34 300 ; 
       13 3 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       14 33 300 ; 
       14 1 300 ; 
       14 4 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       15 33 300 ; 
       15 23 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       16 35 300 ; 
       17 43 300 ; 
       18 36 300 ; 
       19 37 300 ; 
       20 38 300 ; 
       21 39 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 29 401 ; 
       2 30 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 27 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       34 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 60 0 0 SRT 1 0.498 1 0 0 0 -7.482044 0.0143492 32.50368 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 30 -4 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 20 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 25 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -4 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 10 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 12.5 -6 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 62.60229 -3.887051 0 USR DISPLAY 0 0 SRT 0.2027999 0.2027999 0.2027999 0 0 0 -7.5 0.1522872 34.49353 MPRFLG 0 ; 
       26 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 35 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 40 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65.10229 -3.887051 0 USR DISPLAY 0 0 SRT 0.2027999 0.2027999 0.2027999 0 0 0 -7.5 0.1522872 32.30751 MPRFLG 0 ; 
       2 SCHEM 67.60229 -3.887051 0 USR DISPLAY 0 0 SRT 0.06367916 0.06367916 0.06367916 0 0 0 -7.5 0.1522872 28.51291 MPRFLG 0 ; 
       3 SCHEM 70.10229 -3.887051 0 USR DISPLAY 0 0 SRT 0.1188407 0.1188407 0.1188407 0 0 0 -7.5 0.1522872 27.35803 MPRFLG 0 ; 
       4 SCHEM 72.60229 -3.887051 0 USR DISPLAY 0 0 SRT 0.2263247 0.2263247 0.2263247 0 0 0 -7.5 0.1522872 24.01713 MPRFLG 0 ; 
       5 SCHEM 75.10229 -3.887051 0 USR DISPLAY 0 0 SRT 1e-005 1e-005 1e-005 0 0 0 -7.5 0.1522872 36.48575 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 44 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 44 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 44 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 44 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 44 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 62.5 0 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 6.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 11.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 29 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 44 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 44 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 44 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 21.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
