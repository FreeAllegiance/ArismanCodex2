SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap09-cap09.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.9-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       add_gun-default1.1-0 ; 
       add_gun-mat100.1-0 ; 
       add_gun-mat101.1-0 ; 
       add_gun-mat102.1-0 ; 
       add_gun-mat104.1-0 ; 
       add_gun-mat105.1-0 ; 
       add_gun-mat108.1-0 ; 
       add_gun-mat109.1-0 ; 
       add_gun-mat110.1-0 ; 
       add_gun-mat111.1-0 ; 
       add_gun-mat113.1-0 ; 
       add_gun-mat114.1-0 ; 
       add_gun-mat115.1-0 ; 
       add_gun-mat116.1-0 ; 
       add_gun-mat117.1-0 ; 
       add_gun-mat118.1-0 ; 
       add_gun-mat119.1-0 ; 
       add_gun-mat120.1-0 ; 
       add_gun-mat124.1-0 ; 
       add_gun-mat125.1-0 ; 
       add_gun-mat126.1-0 ; 
       add_gun-mat127.1-0 ; 
       add_gun-mat128.1-0 ; 
       add_gun-mat131.1-0 ; 
       add_gun-mat131_1.1-0 ; 
       add_gun-mat132.1-0 ; 
       add_gun-mat133.1-0 ; 
       add_gun-mat134.1-0 ; 
       add_gun-mat136.1-0 ; 
       add_gun-mat137.1-0 ; 
       add_gun-mat138.1-0 ; 
       add_gun-mat139.1-0 ; 
       add_gun-mat140.1-0 ; 
       add_gun-mat92.1-0 ; 
       add_gun-mat99.1-0 ; 
       add_gun-nose_white-center.1-1.1-0 ; 
       add_gun-port_red-left.1-1.1-0 ; 
       add_gun-port_red-left.1-2.1-0 ; 
       add_gun-starbord_green-right.1-1.1-0 ; 
       add_gun-starbord_green-right.1-2.1-0 ; 
       add_gun-starbord_green-right.1-3.1-0 ; 
       add_gun-starbord_green-right.1-4.1-0 ; 
       add_gun-starbord_green-right.1-5.1-0 ; 
       add_gun-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       cap09-antenn.1-0 ; 
       cap09-cap09.5-0 ROOT ; 
       cap09-contwr.1-0 ; 
       cap09-engine0.1-0 ; 
       cap09-fuselg.1-0 ; 
       cap09-gun.7-0 ; 
       cap09-lbengine.1-0 ; 
       cap09-ltengine.1-0 ; 
       cap09-rbengine.1-0 ; 
       cap09-rtengine.1-0 ; 
       cap09-SS1.1-0 ; 
       cap09-SSb1.1-0 ; 
       cap09-SSla.1-0 ; 
       cap09-SSlm.1-0 ; 
       cap09-SSra.1-0 ; 
       cap09-SSrm.1-0 ; 
       cap09-SSt0.1-0 ; 
       cap09-SSt1.1-0 ; 
       cap09-SSt2.1-0 ; 
       cap09-SSt3.1-0 ; 
       cap09-tlndpad1.1-0 ; 
       cap09-tlndpad2.1-0 ; 
       cap09-tlndpad3.1-0 ; 
       cap09-turatt.1-0 ; 
       cap09-wepemt1.1-0 ; 
       cap09-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-add_gun.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       add_gun-t2d10_1.1-0 ; 
       add_gun-t2d12_1.1-0 ; 
       add_gun-t2d13_1.1-0 ; 
       add_gun-t2d16_1.1-0 ; 
       add_gun-t2d17_1.1-0 ; 
       add_gun-t2d19_1.1-0 ; 
       add_gun-t2d21_1.1-0 ; 
       add_gun-t2d22_1.1-0 ; 
       add_gun-t2d23_1.1-0 ; 
       add_gun-t2d24_1.1-0 ; 
       add_gun-t2d25_1.1-0 ; 
       add_gun-t2d26_1.1-0 ; 
       add_gun-t2d27_1.1-0 ; 
       add_gun-t2d28_1.1-0 ; 
       add_gun-t2d32_1.1-0 ; 
       add_gun-t2d33_1.1-0 ; 
       add_gun-t2d34_1.1-0 ; 
       add_gun-t2d35_1.1-0 ; 
       add_gun-t2d36_1.1-0 ; 
       add_gun-t2d39.1-0 ; 
       add_gun-t2d40.1-0 ; 
       add_gun-t2d41.1-0 ; 
       add_gun-t2d42.1-0 ; 
       add_gun-t2d42_1.1-0 ; 
       add_gun-t2d43.1-0 ; 
       add_gun-t2d44.1-0 ; 
       add_gun-t2d45.1-0 ; 
       add_gun-t2d46.1-0 ; 
       add_gun-t2d7_1.1-0 ; 
       add_gun-t2d8_1.1-0 ; 
       add_gun-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       0 2 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 1 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 0 110 ; 
       11 4 110 ; 
       12 7 110 ; 
       13 4 110 ; 
       14 9 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 32 300 ; 
       2 0 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       4 24 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       6 33 300 ; 
       6 2 300 ; 
       6 5 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 3 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       8 33 300 ; 
       8 1 300 ; 
       8 4 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       9 33 300 ; 
       9 23 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       9 27 300 ; 
       10 35 300 ; 
       11 43 300 ; 
       12 36 300 ; 
       13 37 300 ; 
       14 38 300 ; 
       15 39 300 ; 
       17 40 300 ; 
       18 41 300 ; 
       19 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 29 401 ; 
       2 30 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 27 401 ; 
       9 5 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       34 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       1 SCHEM 21.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       15 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 0 -8 0 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 40 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
