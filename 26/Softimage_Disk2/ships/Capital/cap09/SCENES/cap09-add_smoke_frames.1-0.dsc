SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_smoke_frames-cap09.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.34-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_smoke_frames-light1.1-0 ROOT ; 
       add_smoke_frames-light2.1-0 ROOT ; 
       add_smoke_frames-light3.1-0 ROOT ; 
       add_smoke_frames-light4.1-0 ROOT ; 
       add_smoke_frames-light5.1-0 ROOT ; 
       add_smoke_frames-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_smoke_frames-default1.1-0 ; 
       add_smoke_frames-gun_barrel1.1-0 ; 
       add_smoke_frames-gun_base.1-0 ; 
       add_smoke_frames-gun_core1.1-0 ; 
       add_smoke_frames-mat100.1-0 ; 
       add_smoke_frames-mat101.1-0 ; 
       add_smoke_frames-mat102.1-0 ; 
       add_smoke_frames-mat104.1-0 ; 
       add_smoke_frames-mat105.1-0 ; 
       add_smoke_frames-mat108.1-0 ; 
       add_smoke_frames-mat109.1-0 ; 
       add_smoke_frames-mat110.1-0 ; 
       add_smoke_frames-mat111.1-0 ; 
       add_smoke_frames-mat113.1-0 ; 
       add_smoke_frames-mat114.1-0 ; 
       add_smoke_frames-mat115.1-0 ; 
       add_smoke_frames-mat116.1-0 ; 
       add_smoke_frames-mat117.1-0 ; 
       add_smoke_frames-mat118.1-0 ; 
       add_smoke_frames-mat119.1-0 ; 
       add_smoke_frames-mat120.1-0 ; 
       add_smoke_frames-mat124.1-0 ; 
       add_smoke_frames-mat125.1-0 ; 
       add_smoke_frames-mat126.1-0 ; 
       add_smoke_frames-mat127.1-0 ; 
       add_smoke_frames-mat128.1-0 ; 
       add_smoke_frames-mat131.1-0 ; 
       add_smoke_frames-mat131_1.1-0 ; 
       add_smoke_frames-mat132.1-0 ; 
       add_smoke_frames-mat133.1-0 ; 
       add_smoke_frames-mat134.1-0 ; 
       add_smoke_frames-mat136.1-0 ; 
       add_smoke_frames-mat137.1-0 ; 
       add_smoke_frames-mat138.1-0 ; 
       add_smoke_frames-mat139.1-0 ; 
       add_smoke_frames-mat140.1-0 ; 
       add_smoke_frames-mat141.1-0 ; 
       add_smoke_frames-mat92.1-0 ; 
       add_smoke_frames-mat99.1-0 ; 
       add_smoke_frames-nose_white-center.1-1.1-0 ; 
       add_smoke_frames-port_red-left.1-1.1-0 ; 
       add_smoke_frames-port_red-left.1-2.1-0 ; 
       add_smoke_frames-starbord_green-right.1-1.1-0 ; 
       add_smoke_frames-starbord_green-right.1-2.1-0 ; 
       add_smoke_frames-starbord_green-right.1-3.1-0 ; 
       add_smoke_frames-starbord_green-right.1-4.1-0 ; 
       add_smoke_frames-starbord_green-right.1-5.1-0 ; 
       add_smoke_frames-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       add_smoke_frames-antenn.1-0 ; 
       add_smoke_frames-blthrust.1-0 ; 
       add_smoke_frames-brthrust.1-0 ; 
       add_smoke_frames-cap09.1-0 ROOT ; 
       add_smoke_frames-cockpt.1-0 ; 
       add_smoke_frames-contwr.1-0 ; 
       add_smoke_frames-cube1.1-0 ; 
       add_smoke_frames-cube1_1.2-0 ; 
       add_smoke_frames-engine0.1-0 ; 
       add_smoke_frames-fuselg.1-0 ; 
       add_smoke_frames-gun.7-0 ; 
       add_smoke_frames-lbengine.1-0 ; 
       add_smoke_frames-lsmoke.1-0 ; 
       add_smoke_frames-ltengine.1-0 ; 
       add_smoke_frames-missemt.1-0 ; 
       add_smoke_frames-rbengine.1-0 ; 
       add_smoke_frames-rsmoke.1-0 ; 
       add_smoke_frames-rtengine.1-0 ; 
       add_smoke_frames-SS1.1-0 ; 
       add_smoke_frames-SSb1.1-0 ; 
       add_smoke_frames-SSla.1-0 ; 
       add_smoke_frames-SSlm.1-0 ; 
       add_smoke_frames-SSra.1-0 ; 
       add_smoke_frames-SSrm.1-0 ; 
       add_smoke_frames-SSt0.1-0 ; 
       add_smoke_frames-SSt1.1-0 ; 
       add_smoke_frames-SSt2.1-0 ; 
       add_smoke_frames-SSt3.1-0 ; 
       add_smoke_frames-tlndpad1.1-0 ; 
       add_smoke_frames-tlndpad2.1-0 ; 
       add_smoke_frames-tlndpad3.1-0 ; 
       add_smoke_frames-tlthrust.1-0 ; 
       add_smoke_frames-trail.1-0 ; 
       add_smoke_frames-trthrust.1-0 ; 
       add_smoke_frames-turatt.1-0 ; 
       add_smoke_frames-turwepemt1.1-0 ; 
       add_smoke_frames-turwepemt2.1-0 ; 
       add_smoke_frames-wepemt.1-0 ; 
       add_smoke_frames-wepemt1.1-0 ; 
       add_smoke_frames-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-add_smoke_frames.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_smoke_frames-t2d10_1.1-0 ; 
       add_smoke_frames-t2d12_1.1-0 ; 
       add_smoke_frames-t2d13_1.1-0 ; 
       add_smoke_frames-t2d16_1.1-0 ; 
       add_smoke_frames-t2d17_1.1-0 ; 
       add_smoke_frames-t2d19_1.1-0 ; 
       add_smoke_frames-t2d21_1.1-0 ; 
       add_smoke_frames-t2d22_1.1-0 ; 
       add_smoke_frames-t2d23_1.1-0 ; 
       add_smoke_frames-t2d24_1.1-0 ; 
       add_smoke_frames-t2d25_1.1-0 ; 
       add_smoke_frames-t2d26_1.1-0 ; 
       add_smoke_frames-t2d27_1.1-0 ; 
       add_smoke_frames-t2d28_1.1-0 ; 
       add_smoke_frames-t2d32_1.1-0 ; 
       add_smoke_frames-t2d33_1.1-0 ; 
       add_smoke_frames-t2d34_1.1-0 ; 
       add_smoke_frames-t2d35_1.1-0 ; 
       add_smoke_frames-t2d36_1.1-0 ; 
       add_smoke_frames-t2d39.1-0 ; 
       add_smoke_frames-t2d40.1-0 ; 
       add_smoke_frames-t2d41.1-0 ; 
       add_smoke_frames-t2d42.1-0 ; 
       add_smoke_frames-t2d42_1.1-0 ; 
       add_smoke_frames-t2d43.1-0 ; 
       add_smoke_frames-t2d44.1-0 ; 
       add_smoke_frames-t2d45.1-0 ; 
       add_smoke_frames-t2d46.1-0 ; 
       add_smoke_frames-t2d47.1-0 ; 
       add_smoke_frames-t2d48.1-0 ; 
       add_smoke_frames-t2d49.1-0 ; 
       add_smoke_frames-t2d50.1-0 ; 
       add_smoke_frames-t2d51.1-0 ; 
       add_smoke_frames-t2d7_1.1-0 ; 
       add_smoke_frames-t2d8_1.1-0 ; 
       add_smoke_frames-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       9 3 110 ; 
       10 9 110 ; 
       11 8 110 ; 
       13 8 110 ; 
       14 3 110 ; 
       15 8 110 ; 
       17 8 110 ; 
       18 0 110 ; 
       19 9 110 ; 
       20 13 110 ; 
       21 9 110 ; 
       22 17 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 9 110 ; 
       29 9 110 ; 
       30 9 110 ; 
       31 3 110 ; 
       32 3 110 ; 
       33 3 110 ; 
       34 9 110 ; 
       35 6 110 ; 
       36 7 110 ; 
       37 10 110 ; 
       38 9 110 ; 
       39 9 110 ; 
       16 3 110 ; 
       12 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       6 36 300 ; 
       7 35 300 ; 
       9 27 300 ; 
       9 31 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       10 1 300 ; 
       11 37 300 ; 
       11 5 300 ; 
       11 8 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       13 6 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       15 37 300 ; 
       15 4 300 ; 
       15 7 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       15 16 300 ; 
       17 37 300 ; 
       17 26 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       17 30 300 ; 
       18 39 300 ; 
       19 47 300 ; 
       20 40 300 ; 
       21 41 300 ; 
       22 42 300 ; 
       23 43 300 ; 
       25 44 300 ; 
       26 45 300 ; 
       27 46 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       38 33 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 87.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       4 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 50 -4 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 45 -4 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       14 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 25 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 25 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 10 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 12.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 35 -4 0 MPRFLG 0 ; 
       31 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 40 -4 0 MPRFLG 0 ; 
       39 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 72.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
