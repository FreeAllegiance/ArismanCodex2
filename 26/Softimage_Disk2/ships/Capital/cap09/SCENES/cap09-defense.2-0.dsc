SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       defense-cap09.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.32-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       defense-light1.2-0 ROOT ; 
       defense-light2.2-0 ROOT ; 
       defense-light3.2-0 ROOT ; 
       defense-light4.2-0 ROOT ; 
       defense-light5.2-0 ROOT ; 
       defense-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       defense-default1.1-0 ; 
       defense-gun_barrel1.1-0 ; 
       defense-gun_base.1-0 ; 
       defense-gun_core1.1-0 ; 
       defense-mat100.1-0 ; 
       defense-mat101.1-0 ; 
       defense-mat102.1-0 ; 
       defense-mat104.1-0 ; 
       defense-mat105.1-0 ; 
       defense-mat108.1-0 ; 
       defense-mat109.1-0 ; 
       defense-mat110.1-0 ; 
       defense-mat111.1-0 ; 
       defense-mat113.1-0 ; 
       defense-mat114.1-0 ; 
       defense-mat115.1-0 ; 
       defense-mat116.1-0 ; 
       defense-mat117.1-0 ; 
       defense-mat118.1-0 ; 
       defense-mat119.1-0 ; 
       defense-mat120.1-0 ; 
       defense-mat124.1-0 ; 
       defense-mat125.1-0 ; 
       defense-mat126.1-0 ; 
       defense-mat127.1-0 ; 
       defense-mat128.1-0 ; 
       defense-mat131.1-0 ; 
       defense-mat131_1.1-0 ; 
       defense-mat132.1-0 ; 
       defense-mat133.1-0 ; 
       defense-mat134.1-0 ; 
       defense-mat136.1-0 ; 
       defense-mat137.1-0 ; 
       defense-mat138.1-0 ; 
       defense-mat139.1-0 ; 
       defense-mat140.1-0 ; 
       defense-mat141.1-0 ; 
       defense-mat142.1-0 ; 
       defense-mat143.1-0 ; 
       defense-mat144.1-0 ; 
       defense-mat92.1-0 ; 
       defense-mat99.1-0 ; 
       defense-nose_white-center.1-1.1-0 ; 
       defense-port_red-left.1-1.1-0 ; 
       defense-port_red-left.1-2.1-0 ; 
       defense-starbord_green-right.1-1.1-0 ; 
       defense-starbord_green-right.1-2.1-0 ; 
       defense-starbord_green-right.1-3.1-0 ; 
       defense-starbord_green-right.1-4.1-0 ; 
       defense-starbord_green-right.1-5.1-0 ; 
       defense-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       defense-antenn.1-0 ; 
       defense-blthrust.1-0 ; 
       defense-brthrust.1-0 ; 
       defense-cap09.1-0 ROOT ; 
       defense-cockpt.1-0 ; 
       defense-cone1.1-0 ROOT ; 
       defense-cone2.1-0 ROOT ; 
       defense-cone3.1-0 ROOT ; 
       defense-contwr.1-0 ; 
       defense-cube1.1-0 ; 
       defense-cube1_1.2-0 ; 
       defense-engine0.1-0 ; 
       defense-fuselg.1-0 ; 
       defense-gun.7-0 ; 
       defense-lbengine.1-0 ; 
       defense-ltengine.1-0 ; 
       defense-missemt.1-0 ; 
       defense-rbengine.1-0 ; 
       defense-rtengine.1-0 ; 
       defense-SS1.1-0 ; 
       defense-SSb1.1-0 ; 
       defense-SSla.1-0 ; 
       defense-SSlm.1-0 ; 
       defense-SSra.1-0 ; 
       defense-SSrm.1-0 ; 
       defense-SSt0.1-0 ; 
       defense-SSt1.1-0 ; 
       defense-SSt2.1-0 ; 
       defense-SSt3.1-0 ; 
       defense-tlndpad1.1-0 ; 
       defense-tlndpad2.1-0 ; 
       defense-tlndpad3.1-0 ; 
       defense-tlthrust.1-0 ; 
       defense-trail.1-0 ; 
       defense-trthrust.1-0 ; 
       defense-turatt.1-0 ; 
       defense-turwepemt1.1-0 ; 
       defense-turwepemt2.1-0 ; 
       defense-wepemt.1-0 ; 
       defense-wepemt1.1-0 ; 
       defense-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-defense.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       defense-t2d10_1.1-0 ; 
       defense-t2d12_1.1-0 ; 
       defense-t2d13_1.1-0 ; 
       defense-t2d16_1.1-0 ; 
       defense-t2d17_1.1-0 ; 
       defense-t2d19_1.1-0 ; 
       defense-t2d21_1.1-0 ; 
       defense-t2d22_1.1-0 ; 
       defense-t2d23_1.1-0 ; 
       defense-t2d24_1.1-0 ; 
       defense-t2d25_1.1-0 ; 
       defense-t2d26_1.1-0 ; 
       defense-t2d27_1.1-0 ; 
       defense-t2d28_1.1-0 ; 
       defense-t2d32_1.1-0 ; 
       defense-t2d33_1.1-0 ; 
       defense-t2d34_1.1-0 ; 
       defense-t2d35_1.1-0 ; 
       defense-t2d36_1.1-0 ; 
       defense-t2d39.1-0 ; 
       defense-t2d40.1-0 ; 
       defense-t2d41.1-0 ; 
       defense-t2d42.1-0 ; 
       defense-t2d42_1.1-0 ; 
       defense-t2d43.1-0 ; 
       defense-t2d44.1-0 ; 
       defense-t2d45.1-0 ; 
       defense-t2d46.1-0 ; 
       defense-t2d47.1-0 ; 
       defense-t2d48.1-0 ; 
       defense-t2d49.1-0 ; 
       defense-t2d50.1-0 ; 
       defense-t2d51.1-0 ; 
       defense-t2d7_1.1-0 ; 
       defense-t2d8_1.1-0 ; 
       defense-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 12 110 ; 
       0 8 110 ; 
       8 12 110 ; 
       11 12 110 ; 
       12 3 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       17 11 110 ; 
       18 11 110 ; 
       19 0 110 ; 
       20 12 110 ; 
       21 15 110 ; 
       22 12 110 ; 
       23 18 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       10 12 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       35 12 110 ; 
       39 12 110 ; 
       40 12 110 ; 
       9 12 110 ; 
       33 3 110 ; 
       34 3 110 ; 
       32 3 110 ; 
       2 3 110 ; 
       1 3 110 ; 
       4 3 110 ; 
       16 3 110 ; 
       36 9 110 ; 
       37 10 110 ; 
       38 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 2 300 ; 
       13 3 300 ; 
       13 1 300 ; 
       8 0 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       12 27 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       14 40 300 ; 
       14 5 300 ; 
       14 8 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       15 40 300 ; 
       15 41 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       17 40 300 ; 
       17 4 300 ; 
       17 7 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       18 40 300 ; 
       18 26 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       19 42 300 ; 
       20 50 300 ; 
       21 43 300 ; 
       22 44 300 ; 
       23 45 300 ; 
       24 46 300 ; 
       26 47 300 ; 
       27 48 300 ; 
       28 49 300 ; 
       10 35 300 ; 
       9 36 300 ; 
       5 39 300 ; 
       6 38 300 ; 
       7 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       2 28 401 ; 
       41 33 401 ; 
       3 29 401 ; 
       1 30 401 ; 
       35 31 401 ; 
       36 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 MPRFLG 0 ; 
       29 SCHEM 0 -8 0 MPRFLG 0 ; 
       30 SCHEM 30 -8 0 MPRFLG 0 ; 
       31 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 35 -8 0 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 40 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 67.5 0 0 DISPLAY 1 2 SRT 5.95 5.95 5.95 0 0 0 0 -25.25137 21.18714 MPRFLG 0 ; 
       6 SCHEM 70 0 0 DISPLAY 1 2 SRT 5.95 5.95 5.95 3.141593 0 0 0 25.63871 3.126014 MPRFLG 0 ; 
       7 SCHEM 72.5 0 0 DISPLAY 1 2 SRT 5.95 5.95 5.95 -1.570796 0 0 -7.384752 0.171197 57.53644 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 80 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
