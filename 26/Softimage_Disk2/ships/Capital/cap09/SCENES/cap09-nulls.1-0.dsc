SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap09-cap09.24-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.24-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       nulls-light1.1-0 ROOT ; 
       nulls-light2.1-0 ROOT ; 
       nulls-light3.1-0 ROOT ; 
       nulls-light4.1-0 ROOT ; 
       nulls-light5.1-0 ROOT ; 
       nulls-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       nulls-default1.1-0 ; 
       nulls-gun_barrel1.1-0 ; 
       nulls-gun_base.1-0 ; 
       nulls-gun_core1.1-0 ; 
       nulls-mat100.1-0 ; 
       nulls-mat101.1-0 ; 
       nulls-mat102.1-0 ; 
       nulls-mat104.1-0 ; 
       nulls-mat105.1-0 ; 
       nulls-mat108.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat116.1-0 ; 
       nulls-mat117.1-0 ; 
       nulls-mat118.1-0 ; 
       nulls-mat119.1-0 ; 
       nulls-mat120.1-0 ; 
       nulls-mat124.1-0 ; 
       nulls-mat125.1-0 ; 
       nulls-mat126.1-0 ; 
       nulls-mat127.1-0 ; 
       nulls-mat128.1-0 ; 
       nulls-mat131.1-0 ; 
       nulls-mat131_1.1-0 ; 
       nulls-mat132.1-0 ; 
       nulls-mat133.1-0 ; 
       nulls-mat134.1-0 ; 
       nulls-mat136.1-0 ; 
       nulls-mat137.1-0 ; 
       nulls-mat138.1-0 ; 
       nulls-mat139.1-0 ; 
       nulls-mat140.1-0 ; 
       nulls-mat141.1-0 ; 
       nulls-mat92.1-0 ; 
       nulls-mat99.1-0 ; 
       nulls-nose_white-center.1-1.1-0 ; 
       nulls-port_red-left.1-1.1-0 ; 
       nulls-port_red-left.1-2.1-0 ; 
       nulls-starbord_green-right.1-1.1-0 ; 
       nulls-starbord_green-right.1-2.1-0 ; 
       nulls-starbord_green-right.1-3.1-0 ; 
       nulls-starbord_green-right.1-4.1-0 ; 
       nulls-starbord_green-right.1-5.1-0 ; 
       nulls-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       cap09-antenn.1-0 ; 
       cap09-blthrust.1-0 ; 
       cap09-brthrust.1-0 ; 
       cap09-cap09.16-0 ROOT ; 
       cap09-contwr.1-0 ; 
       cap09-cube1.1-0 ; 
       cap09-cube1_1.2-0 ; 
       cap09-engine0.1-0 ; 
       cap09-fuselg.1-0 ; 
       cap09-gun.7-0 ; 
       cap09-lbengine.1-0 ; 
       cap09-ltengine.1-0 ; 
       cap09-rbengine.1-0 ; 
       cap09-rtengine.1-0 ; 
       cap09-SS1.1-0 ; 
       cap09-SSb1.1-0 ; 
       cap09-SSla.1-0 ; 
       cap09-SSlm.1-0 ; 
       cap09-SSra.1-0 ; 
       cap09-SSrm.1-0 ; 
       cap09-SSt0.1-0 ; 
       cap09-SSt1.1-0 ; 
       cap09-SSt2.1-0 ; 
       cap09-SSt3.1-0 ; 
       cap09-tlndpad1.1-0 ; 
       cap09-tlndpad2.1-0 ; 
       cap09-tlndpad3.1-0 ; 
       cap09-tlthrust.1-0 ; 
       cap09-trail.1-0 ; 
       cap09-trthrust.1-0 ; 
       cap09-turatt.1-0 ; 
       cap09-wepemt1.1-0 ; 
       cap09-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       nulls-t2d10_1.1-0 ; 
       nulls-t2d12_1.1-0 ; 
       nulls-t2d13_1.1-0 ; 
       nulls-t2d16_1.1-0 ; 
       nulls-t2d17_1.1-0 ; 
       nulls-t2d19_1.1-0 ; 
       nulls-t2d21_1.1-0 ; 
       nulls-t2d22_1.1-0 ; 
       nulls-t2d23_1.1-0 ; 
       nulls-t2d24_1.1-0 ; 
       nulls-t2d25_1.1-0 ; 
       nulls-t2d26_1.1-0 ; 
       nulls-t2d27_1.1-0 ; 
       nulls-t2d28_1.1-0 ; 
       nulls-t2d32_1.1-0 ; 
       nulls-t2d33_1.1-0 ; 
       nulls-t2d34_1.1-0 ; 
       nulls-t2d35_1.1-0 ; 
       nulls-t2d36_1.1-0 ; 
       nulls-t2d39.1-0 ; 
       nulls-t2d40.1-0 ; 
       nulls-t2d41.1-0 ; 
       nulls-t2d42.1-0 ; 
       nulls-t2d42_1.1-0 ; 
       nulls-t2d43.1-0 ; 
       nulls-t2d44.1-0 ; 
       nulls-t2d45.1-0 ; 
       nulls-t2d46.1-0 ; 
       nulls-t2d47.1-0 ; 
       nulls-t2d48.1-0 ; 
       nulls-t2d49.1-0 ; 
       nulls-t2d50.1-0 ; 
       nulls-t2d51.1-0 ; 
       nulls-t2d7_1.1-0 ; 
       nulls-t2d8_1.1-0 ; 
       nulls-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 8 110 ; 
       0 4 110 ; 
       4 8 110 ; 
       7 8 110 ; 
       8 3 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 0 110 ; 
       15 8 110 ; 
       16 11 110 ; 
       17 8 110 ; 
       18 13 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       6 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       5 8 110 ; 
       28 3 110 ; 
       29 3 110 ; 
       27 3 110 ; 
       2 3 110 ; 
       1 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 2 300 ; 
       9 3 300 ; 
       9 1 300 ; 
       4 0 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       8 27 300 ; 
       8 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       10 37 300 ; 
       10 5 300 ; 
       10 8 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       11 37 300 ; 
       11 38 300 ; 
       11 6 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       12 37 300 ; 
       12 4 300 ; 
       12 7 300 ; 
       12 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 37 300 ; 
       13 26 300 ; 
       13 28 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       14 39 300 ; 
       15 47 300 ; 
       16 40 300 ; 
       17 41 300 ; 
       18 42 300 ; 
       19 43 300 ; 
       21 44 300 ; 
       22 45 300 ; 
       23 46 300 ; 
       6 35 300 ; 
       5 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       2 28 401 ; 
       38 33 401 ; 
       3 29 401 ; 
       1 30 401 ; 
       35 31 401 ; 
       36 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 45 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 32.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 26.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 30 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 25 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 7.5 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 15 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 10 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 12.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 47.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 40 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 42.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 52.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 55 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 57.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 62.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
