SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-cap09.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.26-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       nulls-light1.3-0 ROOT ; 
       nulls-light2.3-0 ROOT ; 
       nulls-light3.3-0 ROOT ; 
       nulls-light4.3-0 ROOT ; 
       nulls-light5.3-0 ROOT ; 
       nulls-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       nulls-default1.1-0 ; 
       nulls-gun_barrel1.1-0 ; 
       nulls-gun_base.1-0 ; 
       nulls-gun_core1.1-0 ; 
       nulls-mat100.1-0 ; 
       nulls-mat101.1-0 ; 
       nulls-mat102.1-0 ; 
       nulls-mat104.1-0 ; 
       nulls-mat105.1-0 ; 
       nulls-mat108.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat116.1-0 ; 
       nulls-mat117.1-0 ; 
       nulls-mat118.1-0 ; 
       nulls-mat119.1-0 ; 
       nulls-mat120.1-0 ; 
       nulls-mat124.1-0 ; 
       nulls-mat125.1-0 ; 
       nulls-mat126.1-0 ; 
       nulls-mat127.1-0 ; 
       nulls-mat128.1-0 ; 
       nulls-mat131.1-0 ; 
       nulls-mat131_1.1-0 ; 
       nulls-mat132.1-0 ; 
       nulls-mat133.1-0 ; 
       nulls-mat134.1-0 ; 
       nulls-mat136.1-0 ; 
       nulls-mat137.1-0 ; 
       nulls-mat138.1-0 ; 
       nulls-mat139.1-0 ; 
       nulls-mat140.1-0 ; 
       nulls-mat141.1-0 ; 
       nulls-mat92.1-0 ; 
       nulls-mat99.1-0 ; 
       nulls-nose_white-center.1-1.1-0 ; 
       nulls-port_red-left.1-1.1-0 ; 
       nulls-port_red-left.1-2.1-0 ; 
       nulls-starbord_green-right.1-1.1-0 ; 
       nulls-starbord_green-right.1-2.1-0 ; 
       nulls-starbord_green-right.1-3.1-0 ; 
       nulls-starbord_green-right.1-4.1-0 ; 
       nulls-starbord_green-right.1-5.1-0 ; 
       nulls-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       nulls-antenn.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-cap09.2-0 ROOT ; 
       nulls-cockpt.1-0 ; 
       nulls-contwr.1-0 ; 
       nulls-cube1.1-0 ; 
       nulls-cube1_1.2-0 ; 
       nulls-engine0.1-0 ; 
       nulls-fuselg.1-0 ; 
       nulls-gun.7-0 ; 
       nulls-lbengine.1-0 ; 
       nulls-ltengine.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rbengine.1-0 ; 
       nulls-rtengine.1-0 ; 
       nulls-SS1.1-0 ; 
       nulls-SSb1.1-0 ; 
       nulls-SSla.1-0 ; 
       nulls-SSlm.1-0 ; 
       nulls-SSra.1-0 ; 
       nulls-SSrm.1-0 ; 
       nulls-SSt0.1-0 ; 
       nulls-SSt1.1-0 ; 
       nulls-SSt2.1-0 ; 
       nulls-SSt3.1-0 ; 
       nulls-tlndpad1.1-0 ; 
       nulls-tlndpad2.1-0 ; 
       nulls-tlndpad3.1-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
       nulls-turatt.1-0 ; 
       nulls-turwepemt1.1-0 ; 
       nulls-turwepemt2.1-0 ; 
       nulls-wepemt.1-0 ; 
       nulls-wepemt1.1-0 ; 
       nulls-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       nulls-t2d10_1.1-0 ; 
       nulls-t2d12_1.1-0 ; 
       nulls-t2d13_1.1-0 ; 
       nulls-t2d16_1.1-0 ; 
       nulls-t2d17_1.1-0 ; 
       nulls-t2d19_1.1-0 ; 
       nulls-t2d21_1.1-0 ; 
       nulls-t2d22_1.1-0 ; 
       nulls-t2d23_1.1-0 ; 
       nulls-t2d24_1.1-0 ; 
       nulls-t2d25_1.1-0 ; 
       nulls-t2d26_1.1-0 ; 
       nulls-t2d27_1.1-0 ; 
       nulls-t2d28_1.1-0 ; 
       nulls-t2d32_1.1-0 ; 
       nulls-t2d33_1.1-0 ; 
       nulls-t2d34_1.1-0 ; 
       nulls-t2d35_1.1-0 ; 
       nulls-t2d36_1.1-0 ; 
       nulls-t2d39.1-0 ; 
       nulls-t2d40.1-0 ; 
       nulls-t2d41.1-0 ; 
       nulls-t2d42.1-0 ; 
       nulls-t2d42_1.1-0 ; 
       nulls-t2d43.1-0 ; 
       nulls-t2d44.1-0 ; 
       nulls-t2d45.1-0 ; 
       nulls-t2d46.1-0 ; 
       nulls-t2d47.1-0 ; 
       nulls-t2d48.1-0 ; 
       nulls-t2d49.1-0 ; 
       nulls-t2d50.1-0 ; 
       nulls-t2d51.1-0 ; 
       nulls-t2d7_1.1-0 ; 
       nulls-t2d8_1.1-0 ; 
       nulls-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 9 110 ; 
       0 5 110 ; 
       5 9 110 ; 
       8 9 110 ; 
       9 3 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 0 110 ; 
       17 9 110 ; 
       18 12 110 ; 
       19 9 110 ; 
       20 15 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       7 9 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 9 110 ; 
       32 9 110 ; 
       36 9 110 ; 
       37 9 110 ; 
       6 9 110 ; 
       30 3 110 ; 
       31 3 110 ; 
       29 3 110 ; 
       2 3 110 ; 
       1 3 110 ; 
       4 3 110 ; 
       13 3 110 ; 
       33 6 110 ; 
       34 7 110 ; 
       35 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 2 300 ; 
       10 3 300 ; 
       10 1 300 ; 
       5 0 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       9 27 300 ; 
       9 31 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       11 37 300 ; 
       11 5 300 ; 
       11 8 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       12 37 300 ; 
       12 38 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       14 37 300 ; 
       14 4 300 ; 
       14 7 300 ; 
       14 13 300 ; 
       14 14 300 ; 
       14 15 300 ; 
       14 16 300 ; 
       15 37 300 ; 
       15 26 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       15 30 300 ; 
       16 39 300 ; 
       17 47 300 ; 
       18 40 300 ; 
       19 41 300 ; 
       20 42 300 ; 
       21 43 300 ; 
       23 44 300 ; 
       24 45 300 ; 
       25 46 300 ; 
       7 35 300 ; 
       6 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       2 28 401 ; 
       38 33 401 ; 
       3 29 401 ; 
       1 30 401 ; 
       35 31 401 ; 
       36 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 25 -10 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 20 -10 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 10 -8 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       7 SCHEM 45 -8 0 MPRFLG 0 ; 
       26 SCHEM 0 -8 0 MPRFLG 0 ; 
       27 SCHEM 30 -8 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 35 -8 0 MPRFLG 0 ; 
       36 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 40 -8 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 47.5 -10 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       34 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
