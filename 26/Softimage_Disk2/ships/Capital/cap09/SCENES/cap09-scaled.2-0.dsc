SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       scaled-light1.2-0 ROOT ; 
       scaled-light2.2-0 ROOT ; 
       scaled-light3.2-0 ROOT ; 
       scaled-light4.2-0 ROOT ; 
       scaled-light5.2-0 ROOT ; 
       scaled-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       scaled-default1.1-0 ; 
       scaled-gun_barrel1.1-0 ; 
       scaled-gun_base.1-0 ; 
       scaled-gun_core1.1-0 ; 
       scaled-mat100.1-0 ; 
       scaled-mat101.1-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat104.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat108.1-0 ; 
       scaled-mat109.1-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat111.1-0 ; 
       scaled-mat113.1-0 ; 
       scaled-mat114.1-0 ; 
       scaled-mat115.1-0 ; 
       scaled-mat116.1-0 ; 
       scaled-mat117.1-0 ; 
       scaled-mat118.1-0 ; 
       scaled-mat119.1-0 ; 
       scaled-mat120.1-0 ; 
       scaled-mat124.1-0 ; 
       scaled-mat125.1-0 ; 
       scaled-mat126.1-0 ; 
       scaled-mat127.1-0 ; 
       scaled-mat128.1-0 ; 
       scaled-mat131.1-0 ; 
       scaled-mat131_1.1-0 ; 
       scaled-mat132.1-0 ; 
       scaled-mat133.1-0 ; 
       scaled-mat134.1-0 ; 
       scaled-mat136.1-0 ; 
       scaled-mat137.1-0 ; 
       scaled-mat138.1-0 ; 
       scaled-mat139.1-0 ; 
       scaled-mat140.1-0 ; 
       scaled-mat141.1-0 ; 
       scaled-mat142.1-0 ; 
       scaled-mat145.1-0 ; 
       scaled-mat146.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-mat99.1-0 ; 
       scaled-nose_white-center.1-1.1-0 ; 
       scaled-port_red-left.1-1.1-0 ; 
       scaled-port_red-left.1-2.1-0 ; 
       scaled-starbord_green-right.1-1.1-0 ; 
       scaled-starbord_green-right.1-2.1-0 ; 
       scaled-starbord_green-right.1-3.1-0 ; 
       scaled-starbord_green-right.1-4.1-0 ; 
       scaled-starbord_green-right.1-5.1-0 ; 
       scaled-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       scaled-175deg.2-0 ROOT ; 
       scaled-175deg1.2-0 ROOT ; 
       scaled-175deg2.2-0 ROOT ; 
       scaled-180deg.2-0 ROOT ; 
       scaled-antenn.1-0 ; 
       scaled-blthrust.1-0 ; 
       scaled-brthrust.1-0 ; 
       scaled-cap09.2-0 ROOT ; 
       scaled-cockpt.1-0 ; 
       scaled-cone3.2-0 ; 
       scaled-contwr.1-0 ; 
       scaled-cube1.1-0 ; 
       scaled-cube1_1.2-0 ; 
       scaled-cube1_4.1-0 ; 
       scaled-cube1_6.1-0 ; 
       scaled-engine0.1-0 ; 
       scaled-fuselg.1-0 ; 
       scaled-gun.7-0 ; 
       scaled-lbengine.1-0 ; 
       scaled-ltengine.1-0 ; 
       scaled-missemt.1-0 ; 
       scaled-rbengine.1-0 ; 
       scaled-rtengine.1-0 ; 
       scaled-scale_cube.2-0 ROOT ; 
       scaled-SS1.1-0 ; 
       scaled-SSb1.1-0 ; 
       scaled-SSla.1-0 ; 
       scaled-SSlm.1-0 ; 
       scaled-SSra.1-0 ; 
       scaled-SSrm.1-0 ; 
       scaled-SSt0.1-0 ; 
       scaled-SSt1.1-0 ; 
       scaled-SSt2.1-0 ; 
       scaled-SSt3.1-0 ; 
       scaled-tlndpad1.1-0 ; 
       scaled-tlndpad2.1-0 ; 
       scaled-tlndpad3.1-0 ; 
       scaled-tlthrust.1-0 ; 
       scaled-trail.1-0 ; 
       scaled-trthrust.1-0 ; 
       scaled-turatt.1-0 ; 
       scaled-turwepemt1.1-0 ; 
       scaled-turwepemt2.1-0 ; 
       scaled-turwepemt3.1-0 ; 
       scaled-turwepemt4.1-0 ; 
       scaled-wepemt.1-0 ; 
       scaled-wepemt1.1-0 ; 
       scaled-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-scaled.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       scaled-t2d10_1.1-0 ; 
       scaled-t2d12_1.1-0 ; 
       scaled-t2d13_1.1-0 ; 
       scaled-t2d16_1.1-0 ; 
       scaled-t2d17_1.1-0 ; 
       scaled-t2d19_1.1-0 ; 
       scaled-t2d21_1.1-0 ; 
       scaled-t2d22_1.1-0 ; 
       scaled-t2d23_1.1-0 ; 
       scaled-t2d24_1.1-0 ; 
       scaled-t2d25_1.1-0 ; 
       scaled-t2d26_1.1-0 ; 
       scaled-t2d27_1.1-0 ; 
       scaled-t2d28_1.1-0 ; 
       scaled-t2d32_1.1-0 ; 
       scaled-t2d33_1.1-0 ; 
       scaled-t2d34_1.1-0 ; 
       scaled-t2d35_1.1-0 ; 
       scaled-t2d36_1.1-0 ; 
       scaled-t2d39.1-0 ; 
       scaled-t2d40.1-0 ; 
       scaled-t2d41.1-0 ; 
       scaled-t2d42.1-0 ; 
       scaled-t2d42_1.1-0 ; 
       scaled-t2d43.1-0 ; 
       scaled-t2d44.1-0 ; 
       scaled-t2d45.1-0 ; 
       scaled-t2d46.1-0 ; 
       scaled-t2d47.1-0 ; 
       scaled-t2d48.1-0 ; 
       scaled-t2d49.1-0 ; 
       scaled-t2d50.1-0 ; 
       scaled-t2d51.1-0 ; 
       scaled-t2d54.1-0 ; 
       scaled-t2d55.1-0 ; 
       scaled-t2d7_1.1-0 ; 
       scaled-t2d8_1.1-0 ; 
       scaled-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 10 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       16 7 110 ; 
       17 16 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 7 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       24 4 110 ; 
       25 16 110 ; 
       26 19 110 ; 
       27 16 110 ; 
       28 22 110 ; 
       29 16 110 ; 
       30 16 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 16 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 7 110 ; 
       38 7 110 ; 
       39 7 110 ; 
       40 16 110 ; 
       41 11 110 ; 
       42 12 110 ; 
       43 13 110 ; 
       44 14 110 ; 
       45 17 110 ; 
       46 16 110 ; 
       47 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 37 300 ; 
       10 0 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       11 36 300 ; 
       12 35 300 ; 
       13 38 300 ; 
       14 39 300 ; 
       16 27 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       16 33 300 ; 
       16 34 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 1 300 ; 
       18 40 300 ; 
       18 5 300 ; 
       18 8 300 ; 
       18 17 300 ; 
       18 18 300 ; 
       18 19 300 ; 
       18 20 300 ; 
       19 40 300 ; 
       19 41 300 ; 
       19 6 300 ; 
       19 9 300 ; 
       19 10 300 ; 
       19 11 300 ; 
       19 12 300 ; 
       21 40 300 ; 
       21 4 300 ; 
       21 7 300 ; 
       21 13 300 ; 
       21 14 300 ; 
       21 15 300 ; 
       21 16 300 ; 
       22 40 300 ; 
       22 26 300 ; 
       22 28 300 ; 
       22 29 300 ; 
       22 30 300 ; 
       24 42 300 ; 
       25 50 300 ; 
       26 43 300 ; 
       27 44 300 ; 
       28 45 300 ; 
       29 46 300 ; 
       31 47 300 ; 
       32 48 300 ; 
       33 49 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 36 401 ; 
       5 37 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       41 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.25499 -11.9218 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 0.23 0 0 0 4.485926 20.70409 MPRFLG 0 ; 
       1 SCHEM 52.22756 -12.11349 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 -3.121593 -0.0001202177 -1.709598 -6.514419 -0.7762083 -13.76361 MPRFLG 0 ; 
       2 SCHEM 49.93054 -12.012 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 -0.01999989 -0.0001202177 -1.431994 6.514419 -0.7762083 -13.76361 MPRFLG 0 ; 
       3 SCHEM 44.82708 -11.94812 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 3.189999 0 0 0 -4.065748 -4.588192 MPRFLG 0 ; 
       4 SCHEM 15 -10 0 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 33.60908 1.032502 0 USR MPRFLG 0 ; 
       10 SCHEM 15 -8 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 MPRFLG 0 ; 
       13 SCHEM 49.88191 -8.018543 0 USR MPRFLG 0 ; 
       14 SCHEM 52.07655 -7.939267 0 USR MPRFLG 0 ; 
       15 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 25 -10 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 20 -10 0 MPRFLG 0 ; 
       22 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 37.00958 2.33194 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 10 -8 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 30.08178 -9.189041 0 USR MPRFLG 0 ; 
       35 SCHEM 30 -8 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 35 -8 0 MPRFLG 0 ; 
       41 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 50.06544 -9.972256 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 52.26009 -9.892979 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 40 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 138.2317 10.6387 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 319.9313 47.95318 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 138.2317 8.638697 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 319.9313 45.95318 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
