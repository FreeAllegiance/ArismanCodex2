SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.8-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       scaled-default1.1-0 ; 
       scaled-gun_barrel1.1-0 ; 
       scaled-gun_base.1-0 ; 
       scaled-gun_core1.1-0 ; 
       scaled-mat100.1-0 ; 
       scaled-mat101.1-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat104.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat108.1-0 ; 
       scaled-mat109.1-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat111.1-0 ; 
       scaled-mat113.1-0 ; 
       scaled-mat114.1-0 ; 
       scaled-mat115.1-0 ; 
       scaled-mat116.1-0 ; 
       scaled-mat117.1-0 ; 
       scaled-mat118.1-0 ; 
       scaled-mat119.1-0 ; 
       scaled-mat120.1-0 ; 
       scaled-mat124.1-0 ; 
       scaled-mat125.1-0 ; 
       scaled-mat126.1-0 ; 
       scaled-mat127.1-0 ; 
       scaled-mat128.1-0 ; 
       scaled-mat131.1-0 ; 
       scaled-mat131_1.1-0 ; 
       scaled-mat132.1-0 ; 
       scaled-mat133.1-0 ; 
       scaled-mat134.1-0 ; 
       scaled-mat136.1-0 ; 
       scaled-mat137.1-0 ; 
       scaled-mat138.1-0 ; 
       scaled-mat139.1-0 ; 
       scaled-mat140.1-0 ; 
       scaled-mat141.1-0 ; 
       scaled-mat145.1-0 ; 
       scaled-mat146.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-mat99.1-0 ; 
       scaled-nose_white-center.1-1.1-0 ; 
       scaled-port_red-left.1-1.1-0 ; 
       scaled-port_red-left.1-2.1-0 ; 
       scaled-starbord_green-right.1-1.1-0 ; 
       scaled-starbord_green-right.1-2.1-0 ; 
       scaled-starbord_green-right.1-3.1-0 ; 
       scaled-starbord_green-right.1-4.1-0 ; 
       scaled-starbord_green-right.1-5.1-0 ; 
       scaled-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       scaled-antenn.1-0 ; 
       scaled-blthrust.1-0 ; 
       scaled-brthrust.1-0 ; 
       scaled-cap09.4-0 ROOT ; 
       scaled-cockpt.1-0 ; 
       scaled-contwr.1-0 ; 
       scaled-cube1.1-0 ; 
       scaled-cube1_1.2-0 ; 
       scaled-cube1_4.1-0 ; 
       scaled-cube1_6.1-0 ; 
       scaled-engine0.1-0 ; 
       scaled-fuselg.1-0 ; 
       scaled-gun.7-0 ; 
       scaled-lbengine.1-0 ; 
       scaled-ltengine.1-0 ; 
       scaled-missemt.1-0 ; 
       scaled-rbengine.1-0 ; 
       scaled-rtengine.1-0 ; 
       scaled-SS1.1-0 ; 
       scaled-SSb1.1-0 ; 
       scaled-SSla.1-0 ; 
       scaled-SSlm.1-0 ; 
       scaled-SSra.1-0 ; 
       scaled-SSrm.1-0 ; 
       scaled-SSt0.1-0 ; 
       scaled-SSt1.1-0 ; 
       scaled-SSt2.1-0 ; 
       scaled-SSt3.1-0 ; 
       scaled-tlndpad1.1-0 ; 
       scaled-tlndpad2.1-0 ; 
       scaled-tlndpad3.1-0 ; 
       scaled-tlthrust.1-0 ; 
       scaled-trail.1-0 ; 
       scaled-trthrust.1-0 ; 
       scaled-turatt.1-0 ; 
       scaled-turwepemt1.1-0 ; 
       scaled-turwepemt2.1-0 ; 
       scaled-turwepemt3.1-0 ; 
       scaled-turwepemt4.1-0 ; 
       scaled-wepemt.1-0 ; 
       scaled-wepemt1.1-0 ; 
       scaled-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-scaled.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       scaled-t2d10_1.2-0 ; 
       scaled-t2d12_1.2-0 ; 
       scaled-t2d13_1.2-0 ; 
       scaled-t2d16_1.2-0 ; 
       scaled-t2d17_1.2-0 ; 
       scaled-t2d19_1.2-0 ; 
       scaled-t2d21_1.2-0 ; 
       scaled-t2d22_1.2-0 ; 
       scaled-t2d23_1.2-0 ; 
       scaled-t2d24_1.2-0 ; 
       scaled-t2d25_1.2-0 ; 
       scaled-t2d26_1.2-0 ; 
       scaled-t2d27_1.2-0 ; 
       scaled-t2d28_1.2-0 ; 
       scaled-t2d32_1.2-0 ; 
       scaled-t2d33_1.2-0 ; 
       scaled-t2d34_1.2-0 ; 
       scaled-t2d35_1.2-0 ; 
       scaled-t2d36_1.2-0 ; 
       scaled-t2d39.2-0 ; 
       scaled-t2d40.2-0 ; 
       scaled-t2d41.2-0 ; 
       scaled-t2d42.2-0 ; 
       scaled-t2d42_1.2-0 ; 
       scaled-t2d43.2-0 ; 
       scaled-t2d44.2-0 ; 
       scaled-t2d45.2-0 ; 
       scaled-t2d46.2-0 ; 
       scaled-t2d47.2-0 ; 
       scaled-t2d48.2-0 ; 
       scaled-t2d49.2-0 ; 
       scaled-t2d50.2-0 ; 
       scaled-t2d51.2-0 ; 
       scaled-t2d54.2-0 ; 
       scaled-t2d55.2-0 ; 
       scaled-t2d7_1.2-0 ; 
       scaled-t2d8_1.2-0 ; 
       scaled-t2d9_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 3 110 ; 
       12 11 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 3 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       18 0 110 ; 
       19 11 110 ; 
       20 14 110 ; 
       21 11 110 ; 
       22 17 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 11 110 ; 
       29 11 110 ; 
       30 11 110 ; 
       31 3 110 ; 
       32 3 110 ; 
       33 3 110 ; 
       34 11 110 ; 
       35 6 110 ; 
       36 7 110 ; 
       37 8 110 ; 
       38 9 110 ; 
       39 12 110 ; 
       40 11 110 ; 
       41 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       6 36 300 ; 
       7 35 300 ; 
       8 37 300 ; 
       9 38 300 ; 
       11 27 300 ; 
       11 31 300 ; 
       11 32 300 ; 
       11 33 300 ; 
       11 34 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       12 1 300 ; 
       13 39 300 ; 
       13 5 300 ; 
       13 8 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       14 39 300 ; 
       14 40 300 ; 
       14 6 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       16 39 300 ; 
       16 4 300 ; 
       16 7 300 ; 
       16 13 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       17 39 300 ; 
       17 26 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       17 30 300 ; 
       18 41 300 ; 
       19 49 300 ; 
       20 42 300 ; 
       21 43 300 ; 
       22 44 300 ; 
       23 45 300 ; 
       25 46 300 ; 
       26 47 300 ; 
       27 48 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 36 401 ; 
       5 37 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       40 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 67.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 65 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 DISPLAY 3 2 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       4 SCHEM 70 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 50 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 21.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 28.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 45 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 72.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 40 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 17.5 -8 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 2.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 22.5 -8 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 12.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 10 -6 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 42.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 62.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 57.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 55 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 52.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 47.5 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 50 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 45 -6 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 35 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
