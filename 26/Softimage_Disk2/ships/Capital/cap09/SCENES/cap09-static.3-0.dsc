SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       static-default1.2-0 ; 
       static-gun_barrel1.2-0 ; 
       static-gun_base.2-0 ; 
       static-gun_core1.2-0 ; 
       static-mat100.2-0 ; 
       static-mat101.2-0 ; 
       static-mat102.2-0 ; 
       static-mat104.2-0 ; 
       static-mat105.2-0 ; 
       static-mat108.2-0 ; 
       static-mat109.2-0 ; 
       static-mat110.2-0 ; 
       static-mat111.2-0 ; 
       static-mat113.2-0 ; 
       static-mat114.2-0 ; 
       static-mat115.2-0 ; 
       static-mat116.2-0 ; 
       static-mat117.2-0 ; 
       static-mat118.2-0 ; 
       static-mat119.2-0 ; 
       static-mat120.2-0 ; 
       static-mat124.2-0 ; 
       static-mat125.2-0 ; 
       static-mat126.2-0 ; 
       static-mat127.2-0 ; 
       static-mat128.2-0 ; 
       static-mat131.2-0 ; 
       static-mat131_1.2-0 ; 
       static-mat132.2-0 ; 
       static-mat133.2-0 ; 
       static-mat134.2-0 ; 
       static-mat136.2-0 ; 
       static-mat137.2-0 ; 
       static-mat138.2-0 ; 
       static-mat139.2-0 ; 
       static-mat92.2-0 ; 
       static-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       static-cap09.2-0 ROOT ; 
       static-contwr.1-0 ; 
       static-engine0.1-0 ; 
       static-fuselg.1-0 ; 
       static-gun.7-0 ; 
       static-lbengine.1-0 ; 
       static-ltengine.1-0 ; 
       static-rbengine.1-0 ; 
       static-rtengine.1-0 ; 
       static-SSt0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       static-t2d10_1.2-0 ; 
       static-t2d12_1.2-0 ; 
       static-t2d13_1.2-0 ; 
       static-t2d16_1.2-0 ; 
       static-t2d17_1.2-0 ; 
       static-t2d19_1.2-0 ; 
       static-t2d21_1.2-0 ; 
       static-t2d22_1.2-0 ; 
       static-t2d23_1.2-0 ; 
       static-t2d24_1.2-0 ; 
       static-t2d25_1.2-0 ; 
       static-t2d26_1.2-0 ; 
       static-t2d27_1.2-0 ; 
       static-t2d28_1.2-0 ; 
       static-t2d32_1.2-0 ; 
       static-t2d33_1.2-0 ; 
       static-t2d34_1.2-0 ; 
       static-t2d35_1.2-0 ; 
       static-t2d36_1.2-0 ; 
       static-t2d39.2-0 ; 
       static-t2d40.2-0 ; 
       static-t2d41.2-0 ; 
       static-t2d42.2-0 ; 
       static-t2d42_1.2-0 ; 
       static-t2d43.2-0 ; 
       static-t2d44.2-0 ; 
       static-t2d45.2-0 ; 
       static-t2d46.2-0 ; 
       static-t2d47.2-0 ; 
       static-t2d48.2-0 ; 
       static-t2d49.2-0 ; 
       static-t2d7_1.2-0 ; 
       static-t2d8_1.2-0 ; 
       static-t2d9_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 3 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       3 27 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 1 300 ; 
       5 35 300 ; 
       5 5 300 ; 
       5 8 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 6 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       7 35 300 ; 
       7 4 300 ; 
       7 7 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       8 35 300 ; 
       8 26 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 32 401 ; 
       5 33 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       36 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 25 -10 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 20 -10 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
