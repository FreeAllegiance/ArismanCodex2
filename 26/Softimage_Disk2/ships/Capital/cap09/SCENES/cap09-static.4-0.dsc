SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.9-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       static-default1.3-0 ; 
       static-gun_barrel1.3-0 ; 
       static-gun_base.3-0 ; 
       static-gun_core1.3-0 ; 
       static-mat100.3-0 ; 
       static-mat101.3-0 ; 
       static-mat102.3-0 ; 
       static-mat104.3-0 ; 
       static-mat105.3-0 ; 
       static-mat108.3-0 ; 
       static-mat109.3-0 ; 
       static-mat110.3-0 ; 
       static-mat111.3-0 ; 
       static-mat113.3-0 ; 
       static-mat114.3-0 ; 
       static-mat115.3-0 ; 
       static-mat116.3-0 ; 
       static-mat117.3-0 ; 
       static-mat118.3-0 ; 
       static-mat119.3-0 ; 
       static-mat120.3-0 ; 
       static-mat124.3-0 ; 
       static-mat125.3-0 ; 
       static-mat126.3-0 ; 
       static-mat127.3-0 ; 
       static-mat128.3-0 ; 
       static-mat131.3-0 ; 
       static-mat131_1.3-0 ; 
       static-mat132.3-0 ; 
       static-mat133.3-0 ; 
       static-mat134.3-0 ; 
       static-mat136.3-0 ; 
       static-mat137.3-0 ; 
       static-mat138.3-0 ; 
       static-mat139.3-0 ; 
       static-mat140.1-0 ; 
       static-mat141.1-0 ; 
       static-mat145.1-0 ; 
       static-mat146.1-0 ; 
       static-mat92.3-0 ; 
       static-mat99.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       static-cap09.3-0 ROOT ; 
       static-contwr.1-0 ; 
       static-cube1.1-0 ; 
       static-cube1_1.2-0 ; 
       static-cube1_4.1-0 ; 
       static-cube1_6.1-0 ; 
       static-engine0.1-0 ; 
       static-fuselg.1-0 ; 
       static-gun.7-0 ; 
       static-lbengine.1-0 ; 
       static-ltengine.1-0 ; 
       static-rbengine.1-0 ; 
       static-rtengine.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       static-t2d10_1.3-0 ; 
       static-t2d12_1.3-0 ; 
       static-t2d13_1.3-0 ; 
       static-t2d16_1.3-0 ; 
       static-t2d17_1.3-0 ; 
       static-t2d19_1.3-0 ; 
       static-t2d21_1.3-0 ; 
       static-t2d22_1.3-0 ; 
       static-t2d23_1.3-0 ; 
       static-t2d24_1.3-0 ; 
       static-t2d25_1.3-0 ; 
       static-t2d26_1.3-0 ; 
       static-t2d27_1.3-0 ; 
       static-t2d28_1.3-0 ; 
       static-t2d32_1.3-0 ; 
       static-t2d33_1.3-0 ; 
       static-t2d34_1.3-0 ; 
       static-t2d35_1.3-0 ; 
       static-t2d36_1.3-0 ; 
       static-t2d39.3-0 ; 
       static-t2d40.3-0 ; 
       static-t2d41.3-0 ; 
       static-t2d42.3-0 ; 
       static-t2d42_1.3-0 ; 
       static-t2d43.3-0 ; 
       static-t2d44.3-0 ; 
       static-t2d45.3-0 ; 
       static-t2d46.3-0 ; 
       static-t2d47.3-0 ; 
       static-t2d48.3-0 ; 
       static-t2d49.3-0 ; 
       static-t2d50.1-0 ; 
       static-t2d51.1-0 ; 
       static-t2d54.1-0 ; 
       static-t2d55.1-0 ; 
       static-t2d7_1.3-0 ; 
       static-t2d8_1.3-0 ; 
       static-t2d9_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       2 36 300 ; 
       3 35 300 ; 
       4 37 300 ; 
       5 38 300 ; 
       7 27 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 1 300 ; 
       9 39 300 ; 
       9 5 300 ; 
       9 8 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       10 39 300 ; 
       10 40 300 ; 
       10 6 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       11 39 300 ; 
       11 4 300 ; 
       11 7 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       12 39 300 ; 
       12 26 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       12 30 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 36 401 ; 
       5 37 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       40 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 0 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
