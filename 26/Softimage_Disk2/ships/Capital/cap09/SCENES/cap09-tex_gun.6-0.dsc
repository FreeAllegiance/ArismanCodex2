SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap09-cap09.15-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.15-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       tex_gun-default1.1-0 ; 
       tex_gun-gun_barrel1.2-0 ; 
       tex_gun-gun_base.1-0 ; 
       tex_gun-gun_core1.3-0 ; 
       tex_gun-mat100.1-0 ; 
       tex_gun-mat101.1-0 ; 
       tex_gun-mat102.1-0 ; 
       tex_gun-mat104.1-0 ; 
       tex_gun-mat105.1-0 ; 
       tex_gun-mat108.1-0 ; 
       tex_gun-mat109.1-0 ; 
       tex_gun-mat110.1-0 ; 
       tex_gun-mat111.1-0 ; 
       tex_gun-mat113.1-0 ; 
       tex_gun-mat114.1-0 ; 
       tex_gun-mat115.1-0 ; 
       tex_gun-mat116.1-0 ; 
       tex_gun-mat117.1-0 ; 
       tex_gun-mat118.1-0 ; 
       tex_gun-mat119.1-0 ; 
       tex_gun-mat120.1-0 ; 
       tex_gun-mat124.1-0 ; 
       tex_gun-mat125.1-0 ; 
       tex_gun-mat126.1-0 ; 
       tex_gun-mat127.1-0 ; 
       tex_gun-mat128.1-0 ; 
       tex_gun-mat131.1-0 ; 
       tex_gun-mat131_1.1-0 ; 
       tex_gun-mat132.1-0 ; 
       tex_gun-mat133.1-0 ; 
       tex_gun-mat134.1-0 ; 
       tex_gun-mat136.1-0 ; 
       tex_gun-mat137.1-0 ; 
       tex_gun-mat138.1-0 ; 
       tex_gun-mat139.1-0 ; 
       tex_gun-mat92.1-0 ; 
       tex_gun-mat99.1-0 ; 
       tex_gun-nose_white-center.1-1.1-0 ; 
       tex_gun-port_red-left.1-1.1-0 ; 
       tex_gun-port_red-left.1-2.1-0 ; 
       tex_gun-starbord_green-right.1-1.1-0 ; 
       tex_gun-starbord_green-right.1-2.1-0 ; 
       tex_gun-starbord_green-right.1-3.1-0 ; 
       tex_gun-starbord_green-right.1-4.1-0 ; 
       tex_gun-starbord_green-right.1-5.1-0 ; 
       tex_gun-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       cap09-antenn.1-0 ; 
       cap09-cap09.10-0 ROOT ; 
       cap09-contwr.1-0 ; 
       cap09-engine0.1-0 ; 
       cap09-fuselg.1-0 ; 
       cap09-gun.7-0 ; 
       cap09-lbengine.1-0 ; 
       cap09-ltengine.1-0 ; 
       cap09-rbengine.1-0 ; 
       cap09-rtengine.1-0 ; 
       cap09-SS1.1-0 ; 
       cap09-SSb1.1-0 ; 
       cap09-SSla.1-0 ; 
       cap09-SSlm.1-0 ; 
       cap09-SSra.1-0 ; 
       cap09-SSrm.1-0 ; 
       cap09-SSt0.1-0 ; 
       cap09-SSt1.1-0 ; 
       cap09-SSt2.1-0 ; 
       cap09-SSt3.1-0 ; 
       cap09-tlndpad1.1-0 ; 
       cap09-tlndpad2.1-0 ; 
       cap09-tlndpad3.1-0 ; 
       cap09-turatt.1-0 ; 
       cap09-wepemt1.1-0 ; 
       cap09-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-tex_gun.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       tex_gun-t2d10_1.1-0 ; 
       tex_gun-t2d12_1.1-0 ; 
       tex_gun-t2d13_1.1-0 ; 
       tex_gun-t2d16_1.1-0 ; 
       tex_gun-t2d17_1.1-0 ; 
       tex_gun-t2d19_1.1-0 ; 
       tex_gun-t2d21_1.1-0 ; 
       tex_gun-t2d22_1.1-0 ; 
       tex_gun-t2d23_1.1-0 ; 
       tex_gun-t2d24_1.1-0 ; 
       tex_gun-t2d25_1.1-0 ; 
       tex_gun-t2d26_1.1-0 ; 
       tex_gun-t2d27_1.1-0 ; 
       tex_gun-t2d28_1.1-0 ; 
       tex_gun-t2d32_1.1-0 ; 
       tex_gun-t2d33_1.1-0 ; 
       tex_gun-t2d34_1.1-0 ; 
       tex_gun-t2d35_1.1-0 ; 
       tex_gun-t2d36_1.1-0 ; 
       tex_gun-t2d39.1-0 ; 
       tex_gun-t2d40.1-0 ; 
       tex_gun-t2d41.1-0 ; 
       tex_gun-t2d42.1-0 ; 
       tex_gun-t2d42_1.1-0 ; 
       tex_gun-t2d43.1-0 ; 
       tex_gun-t2d44.1-0 ; 
       tex_gun-t2d45.1-0 ; 
       tex_gun-t2d46.1-0 ; 
       tex_gun-t2d47.2-0 ; 
       tex_gun-t2d48.1-0 ; 
       tex_gun-t2d49.1-0 ; 
       tex_gun-t2d7_1.1-0 ; 
       tex_gun-t2d8_1.1-0 ; 
       tex_gun-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       0 2 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 1 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 0 110 ; 
       11 4 110 ; 
       12 7 110 ; 
       13 4 110 ; 
       14 9 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 2 300 ; 
       5 3 300 ; 
       5 1 300 ; 
       2 0 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       4 27 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       6 35 300 ; 
       6 5 300 ; 
       6 8 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 6 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       8 35 300 ; 
       8 4 300 ; 
       8 7 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       9 35 300 ; 
       9 26 300 ; 
       9 28 300 ; 
       9 29 300 ; 
       9 30 300 ; 
       10 37 300 ; 
       11 45 300 ; 
       12 38 300 ; 
       13 39 300 ; 
       14 40 300 ; 
       15 41 300 ; 
       17 42 300 ; 
       18 43 300 ; 
       19 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 32 401 ; 
       5 33 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       2 28 401 ; 
       36 31 401 ; 
       3 29 401 ; 
       1 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 112.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       1 SCHEM 63.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 85 -10 0 MPRFLG 0 ; 
       7 SCHEM 40 -10 0 MPRFLG 0 ; 
       8 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 70 -10 0 MPRFLG 0 ; 
       10 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 95 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 65 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       15 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 0 -8 0 MPRFLG 0 ; 
       21 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 100 -8 0 MPRFLG 0 ; 
       23 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 105 -8 0 MPRFLG 0 ; 
       25 SCHEM 107.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 32.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 90 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 130 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 129 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
