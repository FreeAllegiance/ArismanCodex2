SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap09-cap09.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.23-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       tex_turret-light1.4-0 ROOT ; 
       tex_turret-light2.4-0 ROOT ; 
       tex_turret-light3.4-0 ROOT ; 
       tex_turret-light4.4-0 ROOT ; 
       tex_turret-light5.4-0 ROOT ; 
       tex_turret-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       tex_turret-default1.1-0 ; 
       tex_turret-gun_barrel1.1-0 ; 
       tex_turret-gun_base.1-0 ; 
       tex_turret-gun_core1.1-0 ; 
       tex_turret-mat100.1-0 ; 
       tex_turret-mat101.1-0 ; 
       tex_turret-mat102.1-0 ; 
       tex_turret-mat104.1-0 ; 
       tex_turret-mat105.1-0 ; 
       tex_turret-mat108.1-0 ; 
       tex_turret-mat109.1-0 ; 
       tex_turret-mat110.1-0 ; 
       tex_turret-mat111.1-0 ; 
       tex_turret-mat113.1-0 ; 
       tex_turret-mat114.1-0 ; 
       tex_turret-mat115.1-0 ; 
       tex_turret-mat116.1-0 ; 
       tex_turret-mat117.1-0 ; 
       tex_turret-mat118.1-0 ; 
       tex_turret-mat119.1-0 ; 
       tex_turret-mat120.1-0 ; 
       tex_turret-mat124.1-0 ; 
       tex_turret-mat125.1-0 ; 
       tex_turret-mat126.1-0 ; 
       tex_turret-mat127.1-0 ; 
       tex_turret-mat128.1-0 ; 
       tex_turret-mat131.1-0 ; 
       tex_turret-mat131_1.1-0 ; 
       tex_turret-mat132.1-0 ; 
       tex_turret-mat133.1-0 ; 
       tex_turret-mat134.1-0 ; 
       tex_turret-mat136.1-0 ; 
       tex_turret-mat137.1-0 ; 
       tex_turret-mat138.1-0 ; 
       tex_turret-mat139.1-0 ; 
       tex_turret-mat140.1-0 ; 
       tex_turret-mat141.1-0 ; 
       tex_turret-mat92.1-0 ; 
       tex_turret-mat99.1-0 ; 
       tex_turret-nose_white-center.1-1.1-0 ; 
       tex_turret-port_red-left.1-1.1-0 ; 
       tex_turret-port_red-left.1-2.1-0 ; 
       tex_turret-starbord_green-right.1-1.1-0 ; 
       tex_turret-starbord_green-right.1-2.1-0 ; 
       tex_turret-starbord_green-right.1-3.1-0 ; 
       tex_turret-starbord_green-right.1-4.1-0 ; 
       tex_turret-starbord_green-right.1-5.1-0 ; 
       tex_turret-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       cap09-antenn.1-0 ; 
       cap09-cap09.15-0 ROOT ; 
       cap09-contwr.1-0 ; 
       cap09-cube1.1-0 ; 
       cap09-cube1_1.2-0 ; 
       cap09-engine0.1-0 ; 
       cap09-fuselg.1-0 ; 
       cap09-gun.7-0 ; 
       cap09-lbengine.1-0 ; 
       cap09-ltengine.1-0 ; 
       cap09-rbengine.1-0 ; 
       cap09-rtengine.1-0 ; 
       cap09-SS1.1-0 ; 
       cap09-SSb1.1-0 ; 
       cap09-SSla.1-0 ; 
       cap09-SSlm.1-0 ; 
       cap09-SSra.1-0 ; 
       cap09-SSrm.1-0 ; 
       cap09-SSt0.1-0 ; 
       cap09-SSt1.1-0 ; 
       cap09-SSt2.1-0 ; 
       cap09-SSt3.1-0 ; 
       cap09-tlndpad1.1-0 ; 
       cap09-tlndpad2.1-0 ; 
       cap09-tlndpad3.1-0 ; 
       cap09-turatt.1-0 ; 
       cap09-wepemt1.1-0 ; 
       cap09-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-tex_turret.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       tex_turret-t2d10_1.1-0 ; 
       tex_turret-t2d12_1.1-0 ; 
       tex_turret-t2d13_1.1-0 ; 
       tex_turret-t2d16_1.1-0 ; 
       tex_turret-t2d17_1.1-0 ; 
       tex_turret-t2d19_1.1-0 ; 
       tex_turret-t2d21_1.1-0 ; 
       tex_turret-t2d22_1.1-0 ; 
       tex_turret-t2d23_1.1-0 ; 
       tex_turret-t2d24_1.1-0 ; 
       tex_turret-t2d25_1.1-0 ; 
       tex_turret-t2d26_1.1-0 ; 
       tex_turret-t2d27_1.1-0 ; 
       tex_turret-t2d28_1.1-0 ; 
       tex_turret-t2d32_1.1-0 ; 
       tex_turret-t2d33_1.1-0 ; 
       tex_turret-t2d34_1.1-0 ; 
       tex_turret-t2d35_1.1-0 ; 
       tex_turret-t2d36_1.1-0 ; 
       tex_turret-t2d39.1-0 ; 
       tex_turret-t2d40.1-0 ; 
       tex_turret-t2d41.1-0 ; 
       tex_turret-t2d42.1-0 ; 
       tex_turret-t2d42_1.1-0 ; 
       tex_turret-t2d43.1-0 ; 
       tex_turret-t2d44.1-0 ; 
       tex_turret-t2d45.1-0 ; 
       tex_turret-t2d46.1-0 ; 
       tex_turret-t2d47.1-0 ; 
       tex_turret-t2d48.1-0 ; 
       tex_turret-t2d49.1-0 ; 
       tex_turret-t2d50.1-0 ; 
       tex_turret-t2d51.1-0 ; 
       tex_turret-t2d7_1.1-0 ; 
       tex_turret-t2d8_1.1-0 ; 
       tex_turret-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 6 110 ; 
       0 2 110 ; 
       2 6 110 ; 
       5 6 110 ; 
       6 1 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 0 110 ; 
       13 6 110 ; 
       14 9 110 ; 
       15 6 110 ; 
       16 11 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       4 6 110 ; 
       22 6 110 ; 
       23 6 110 ; 
       24 6 110 ; 
       25 6 110 ; 
       26 6 110 ; 
       27 6 110 ; 
       3 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 2 300 ; 
       7 3 300 ; 
       7 1 300 ; 
       2 0 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       6 27 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       8 37 300 ; 
       8 5 300 ; 
       8 8 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 6 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       10 37 300 ; 
       10 4 300 ; 
       10 7 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       11 37 300 ; 
       11 26 300 ; 
       11 28 300 ; 
       11 29 300 ; 
       11 30 300 ; 
       12 39 300 ; 
       13 47 300 ; 
       14 40 300 ; 
       15 41 300 ; 
       16 42 300 ; 
       17 43 300 ; 
       19 44 300 ; 
       20 45 300 ; 
       21 46 300 ; 
       4 35 300 ; 
       3 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       2 28 401 ; 
       38 33 401 ; 
       3 29 401 ; 
       1 30 401 ; 
       35 31 401 ; 
       36 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       5 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 25 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 20 -10 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 10 -8 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 45 -8 0 MPRFLG 0 ; 
       22 SCHEM 0 -8 0 MPRFLG 0 ; 
       23 SCHEM 30 -8 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 35 -8 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 40 -8 0 MPRFLG 0 ; 
       3 SCHEM 47.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
