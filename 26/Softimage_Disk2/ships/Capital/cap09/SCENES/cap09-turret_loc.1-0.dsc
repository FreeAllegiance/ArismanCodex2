SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       turret_loc-cap09.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigateP-cam_int1.37-0 ROOT ; 
       kez_frigateP-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       turret_loc-light1.1-0 ROOT ; 
       turret_loc-light2.1-0 ROOT ; 
       turret_loc-light3.1-0 ROOT ; 
       turret_loc-light4.1-0 ROOT ; 
       turret_loc-light5.1-0 ROOT ; 
       turret_loc-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       turret_loc-default1.1-0 ; 
       turret_loc-gun_barrel1.1-0 ; 
       turret_loc-gun_base.1-0 ; 
       turret_loc-gun_core1.1-0 ; 
       turret_loc-mat100.1-0 ; 
       turret_loc-mat101.1-0 ; 
       turret_loc-mat102.1-0 ; 
       turret_loc-mat104.1-0 ; 
       turret_loc-mat105.1-0 ; 
       turret_loc-mat108.1-0 ; 
       turret_loc-mat109.1-0 ; 
       turret_loc-mat110.1-0 ; 
       turret_loc-mat111.1-0 ; 
       turret_loc-mat113.1-0 ; 
       turret_loc-mat114.1-0 ; 
       turret_loc-mat115.1-0 ; 
       turret_loc-mat116.1-0 ; 
       turret_loc-mat117.1-0 ; 
       turret_loc-mat118.1-0 ; 
       turret_loc-mat119.1-0 ; 
       turret_loc-mat120.1-0 ; 
       turret_loc-mat124.1-0 ; 
       turret_loc-mat125.1-0 ; 
       turret_loc-mat126.1-0 ; 
       turret_loc-mat127.1-0 ; 
       turret_loc-mat128.1-0 ; 
       turret_loc-mat131.1-0 ; 
       turret_loc-mat131_1.1-0 ; 
       turret_loc-mat132.1-0 ; 
       turret_loc-mat133.1-0 ; 
       turret_loc-mat134.1-0 ; 
       turret_loc-mat136.1-0 ; 
       turret_loc-mat137.1-0 ; 
       turret_loc-mat138.1-0 ; 
       turret_loc-mat139.1-0 ; 
       turret_loc-mat140.1-0 ; 
       turret_loc-mat141.1-0 ; 
       turret_loc-mat142.1-0 ; 
       turret_loc-mat143.1-0 ; 
       turret_loc-mat144.1-0 ; 
       turret_loc-mat145.1-0 ; 
       turret_loc-mat146.1-0 ; 
       turret_loc-mat147.1-0 ; 
       turret_loc-mat92.1-0 ; 
       turret_loc-mat99.1-0 ; 
       turret_loc-nose_white-center.1-1.1-0 ; 
       turret_loc-port_red-left.1-1.1-0 ; 
       turret_loc-port_red-left.1-2.1-0 ; 
       turret_loc-starbord_green-right.1-1.1-0 ; 
       turret_loc-starbord_green-right.1-2.1-0 ; 
       turret_loc-starbord_green-right.1-3.1-0 ; 
       turret_loc-starbord_green-right.1-4.1-0 ; 
       turret_loc-starbord_green-right.1-5.1-0 ; 
       turret_loc-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       arc-cone.1-0 ROOT ; 
       turret_loc-antenn.1-0 ; 
       turret_loc-blthrust.1-0 ; 
       turret_loc-brthrust.1-0 ; 
       turret_loc-cap09.1-0 ROOT ; 
       turret_loc-cockpt.1-0 ; 
       turret_loc-cone1.1-0 ROOT ; 
       turret_loc-cone2.1-0 ROOT ; 
       turret_loc-cone3.1-0 ROOT ; 
       turret_loc-cone4.1-0 ROOT ; 
       turret_loc-cone5.1-0 ROOT ; 
       turret_loc-contwr.1-0 ; 
       turret_loc-cube1.1-0 ; 
       turret_loc-cube1_1.2-0 ; 
       turret_loc-engine0.1-0 ; 
       turret_loc-fuselg.1-0 ; 
       turret_loc-gun.7-0 ; 
       turret_loc-lbengine.1-0 ; 
       turret_loc-ltengine.1-0 ; 
       turret_loc-missemt.1-0 ; 
       turret_loc-rbengine.1-0 ; 
       turret_loc-rtengine.1-0 ; 
       turret_loc-SS1.1-0 ; 
       turret_loc-SSb1.1-0 ; 
       turret_loc-SSla.1-0 ; 
       turret_loc-SSlm.1-0 ; 
       turret_loc-SSra.1-0 ; 
       turret_loc-SSrm.1-0 ; 
       turret_loc-SSt0.1-0 ; 
       turret_loc-SSt1.1-0 ; 
       turret_loc-SSt2.1-0 ; 
       turret_loc-SSt3.1-0 ; 
       turret_loc-tlndpad1.1-0 ; 
       turret_loc-tlndpad2.1-0 ; 
       turret_loc-tlndpad3.1-0 ; 
       turret_loc-tlthrust.1-0 ; 
       turret_loc-trail.1-0 ; 
       turret_loc-trthrust.1-0 ; 
       turret_loc-turatt.1-0 ; 
       turret_loc-turwepemt1.1-0 ; 
       turret_loc-turwepemt2.1-0 ; 
       turret_loc-wepemt.1-0 ; 
       turret_loc-wepemt1.1-0 ; 
       turret_loc-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       turret_loc-t2d10_1.1-0 ; 
       turret_loc-t2d12_1.1-0 ; 
       turret_loc-t2d13_1.1-0 ; 
       turret_loc-t2d16_1.1-0 ; 
       turret_loc-t2d17_1.1-0 ; 
       turret_loc-t2d19_1.1-0 ; 
       turret_loc-t2d21_1.1-0 ; 
       turret_loc-t2d22_1.1-0 ; 
       turret_loc-t2d23_1.1-0 ; 
       turret_loc-t2d24_1.1-0 ; 
       turret_loc-t2d25_1.1-0 ; 
       turret_loc-t2d26_1.1-0 ; 
       turret_loc-t2d27_1.1-0 ; 
       turret_loc-t2d28_1.1-0 ; 
       turret_loc-t2d32_1.1-0 ; 
       turret_loc-t2d33_1.1-0 ; 
       turret_loc-t2d34_1.1-0 ; 
       turret_loc-t2d35_1.1-0 ; 
       turret_loc-t2d36_1.1-0 ; 
       turret_loc-t2d39.1-0 ; 
       turret_loc-t2d40.1-0 ; 
       turret_loc-t2d41.1-0 ; 
       turret_loc-t2d42.1-0 ; 
       turret_loc-t2d42_1.1-0 ; 
       turret_loc-t2d43.1-0 ; 
       turret_loc-t2d44.1-0 ; 
       turret_loc-t2d45.1-0 ; 
       turret_loc-t2d46.1-0 ; 
       turret_loc-t2d47.1-0 ; 
       turret_loc-t2d48.1-0 ; 
       turret_loc-t2d49.1-0 ; 
       turret_loc-t2d50.1-0 ; 
       turret_loc-t2d51.1-0 ; 
       turret_loc-t2d7_1.1-0 ; 
       turret_loc-t2d8_1.1-0 ; 
       turret_loc-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 11 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 4 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 4 110 ; 
       16 15 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 4 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 1 110 ; 
       23 15 110 ; 
       24 18 110 ; 
       25 15 110 ; 
       26 21 110 ; 
       27 15 110 ; 
       28 15 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 15 110 ; 
       33 15 110 ; 
       34 15 110 ; 
       35 4 110 ; 
       36 4 110 ; 
       37 4 110 ; 
       38 15 110 ; 
       39 12 110 ; 
       40 13 110 ; 
       41 16 110 ; 
       42 15 110 ; 
       43 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 41 300 ; 
       10 42 300 ; 
       6 39 300 ; 
       7 38 300 ; 
       8 37 300 ; 
       11 0 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 24 300 ; 
       11 25 300 ; 
       12 36 300 ; 
       13 35 300 ; 
       15 27 300 ; 
       15 31 300 ; 
       15 32 300 ; 
       15 33 300 ; 
       15 34 300 ; 
       16 2 300 ; 
       16 3 300 ; 
       16 1 300 ; 
       17 43 300 ; 
       17 5 300 ; 
       17 8 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       18 43 300 ; 
       18 44 300 ; 
       18 6 300 ; 
       18 9 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       18 12 300 ; 
       20 43 300 ; 
       20 4 300 ; 
       20 7 300 ; 
       20 13 300 ; 
       20 14 300 ; 
       20 15 300 ; 
       20 16 300 ; 
       21 43 300 ; 
       21 26 300 ; 
       21 28 300 ; 
       21 29 300 ; 
       21 30 300 ; 
       22 45 300 ; 
       23 53 300 ; 
       24 46 300 ; 
       25 47 300 ; 
       26 48 300 ; 
       27 49 300 ; 
       29 50 300 ; 
       30 51 300 ; 
       31 52 300 ; 
       0 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       44 33 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 80 0 0 SRT 12.45715 12.45715 12.45715 -3.141593 0 0 0 -4.840581 -1.919708 MPRFLG 0 ; 
       10 SCHEM 85 0 0 SRT 12.45715 12.45715 12.45715 1.570796 0 0 -7.375248 0.1476705 37.75907 MPRFLG 0 ; 
       1 SCHEM 15 -10 0 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       5 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 5.95 5.95 5.95 0 0 0 0 -25.25137 21.18714 MPRFLG 0 ; 
       7 SCHEM 70 0 0 DISPLAY 0 0 SRT 5.95 5.95 5.95 3.141593 0 0 0 25.63871 3.126014 MPRFLG 0 ; 
       8 SCHEM 72.5 0 0 DISPLAY 0 0 SRT 5.95 5.95 5.95 -1.570796 0 0 -7.384752 0.171197 57.53644 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 45 -8 0 MPRFLG 0 ; 
       14 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 25 -10 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 MPRFLG 0 ; 
       29 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       31 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 0 -8 0 MPRFLG 0 ; 
       33 SCHEM 30 -8 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35 -8 0 MPRFLG 0 ; 
       39 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 40 -8 0 MPRFLG 0 ; 
       0 SCHEM 75 0 0 DISPLAY 1 2 SRT 12.45715 12.45715 12.45715 0 0 0 0 5.439171 -1.919708 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 80 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 87.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
