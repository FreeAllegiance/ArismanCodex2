SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       turret_loc-cap09.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       turret_loc-light1.2-0 ROOT ; 
       turret_loc-light2.2-0 ROOT ; 
       turret_loc-light3.2-0 ROOT ; 
       turret_loc-light4.2-0 ROOT ; 
       turret_loc-light5.2-0 ROOT ; 
       turret_loc-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       turret_loc-default1.2-0 ; 
       turret_loc-gun_barrel1.2-0 ; 
       turret_loc-gun_base.2-0 ; 
       turret_loc-gun_core1.2-0 ; 
       turret_loc-mat100.2-0 ; 
       turret_loc-mat101.2-0 ; 
       turret_loc-mat102.2-0 ; 
       turret_loc-mat104.2-0 ; 
       turret_loc-mat105.2-0 ; 
       turret_loc-mat108.2-0 ; 
       turret_loc-mat109.2-0 ; 
       turret_loc-mat110.2-0 ; 
       turret_loc-mat111.2-0 ; 
       turret_loc-mat113.2-0 ; 
       turret_loc-mat114.2-0 ; 
       turret_loc-mat115.2-0 ; 
       turret_loc-mat116.2-0 ; 
       turret_loc-mat117.2-0 ; 
       turret_loc-mat118.2-0 ; 
       turret_loc-mat119.2-0 ; 
       turret_loc-mat120.2-0 ; 
       turret_loc-mat124.2-0 ; 
       turret_loc-mat125.2-0 ; 
       turret_loc-mat126.2-0 ; 
       turret_loc-mat127.2-0 ; 
       turret_loc-mat128.2-0 ; 
       turret_loc-mat131.2-0 ; 
       turret_loc-mat131_1.2-0 ; 
       turret_loc-mat132.2-0 ; 
       turret_loc-mat133.2-0 ; 
       turret_loc-mat134.2-0 ; 
       turret_loc-mat136.2-0 ; 
       turret_loc-mat137.2-0 ; 
       turret_loc-mat138.2-0 ; 
       turret_loc-mat139.2-0 ; 
       turret_loc-mat140.2-0 ; 
       turret_loc-mat141.2-0 ; 
       turret_loc-mat142.2-0 ; 
       turret_loc-mat92.2-0 ; 
       turret_loc-mat99.2-0 ; 
       turret_loc-nose_white-center.1-1.2-0 ; 
       turret_loc-port_red-left.1-1.2-0 ; 
       turret_loc-port_red-left.1-2.2-0 ; 
       turret_loc-starbord_green-right.1-1.2-0 ; 
       turret_loc-starbord_green-right.1-2.2-0 ; 
       turret_loc-starbord_green-right.1-3.2-0 ; 
       turret_loc-starbord_green-right.1-4.2-0 ; 
       turret_loc-starbord_green-right.1-5.2-0 ; 
       turret_loc-starbord_green-right.1-6.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       turcone-175deg.1-0 ROOT ; 
       turcone1-180deg.1-0 ROOT ; 
       turret_loc-antenn.1-0 ; 
       turret_loc-blthrust.1-0 ; 
       turret_loc-brthrust.1-0 ; 
       turret_loc-cap09.2-0 ROOT ; 
       turret_loc-cockpt.1-0 ; 
       turret_loc-cone3.2-0 ROOT ; 
       turret_loc-contwr.1-0 ; 
       turret_loc-cube1.1-0 ; 
       turret_loc-cube1_1.2-0 ; 
       turret_loc-engine0.1-0 ; 
       turret_loc-fuselg.1-0 ; 
       turret_loc-gun.7-0 ; 
       turret_loc-lbengine.1-0 ; 
       turret_loc-ltengine.1-0 ; 
       turret_loc-missemt.1-0 ; 
       turret_loc-rbengine.1-0 ; 
       turret_loc-rtengine.1-0 ; 
       turret_loc-scale_cube.1-0 ROOT ; 
       turret_loc-SS1.1-0 ; 
       turret_loc-SSb1.1-0 ; 
       turret_loc-SSla.1-0 ; 
       turret_loc-SSlm.1-0 ; 
       turret_loc-SSra.1-0 ; 
       turret_loc-SSrm.1-0 ; 
       turret_loc-SSt0.1-0 ; 
       turret_loc-SSt1.1-0 ; 
       turret_loc-SSt2.1-0 ; 
       turret_loc-SSt3.1-0 ; 
       turret_loc-tlndpad1.1-0 ; 
       turret_loc-tlndpad2.1-0 ; 
       turret_loc-tlndpad3.1-0 ; 
       turret_loc-tlthrust.1-0 ; 
       turret_loc-trail.1-0 ; 
       turret_loc-trthrust.1-0 ; 
       turret_loc-turatt.1-0 ; 
       turret_loc-turwepemt1.1-0 ; 
       turret_loc-turwepemt2.1-0 ; 
       turret_loc-wepemt.1-0 ; 
       turret_loc-wepemt1.1-0 ; 
       turret_loc-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap09/PICTURES/cap09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-turret_loc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       turret_loc-t2d10_1.2-0 ; 
       turret_loc-t2d12_1.2-0 ; 
       turret_loc-t2d13_1.2-0 ; 
       turret_loc-t2d16_1.2-0 ; 
       turret_loc-t2d17_1.2-0 ; 
       turret_loc-t2d19_1.2-0 ; 
       turret_loc-t2d21_1.2-0 ; 
       turret_loc-t2d22_1.2-0 ; 
       turret_loc-t2d23_1.2-0 ; 
       turret_loc-t2d24_1.2-0 ; 
       turret_loc-t2d25_1.2-0 ; 
       turret_loc-t2d26_1.2-0 ; 
       turret_loc-t2d27_1.2-0 ; 
       turret_loc-t2d28_1.2-0 ; 
       turret_loc-t2d32_1.2-0 ; 
       turret_loc-t2d33_1.2-0 ; 
       turret_loc-t2d34_1.2-0 ; 
       turret_loc-t2d35_1.2-0 ; 
       turret_loc-t2d36_1.2-0 ; 
       turret_loc-t2d39.2-0 ; 
       turret_loc-t2d40.2-0 ; 
       turret_loc-t2d41.2-0 ; 
       turret_loc-t2d42.2-0 ; 
       turret_loc-t2d42_1.2-0 ; 
       turret_loc-t2d43.2-0 ; 
       turret_loc-t2d44.2-0 ; 
       turret_loc-t2d45.2-0 ; 
       turret_loc-t2d46.2-0 ; 
       turret_loc-t2d47.2-0 ; 
       turret_loc-t2d48.2-0 ; 
       turret_loc-t2d49.2-0 ; 
       turret_loc-t2d50.2-0 ; 
       turret_loc-t2d51.2-0 ; 
       turret_loc-t2d7_1.2-0 ; 
       turret_loc-t2d8_1.2-0 ; 
       turret_loc-t2d9_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 8 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       8 12 110 ; 
       9 12 110 ; 
       10 12 110 ; 
       11 12 110 ; 
       12 5 110 ; 
       13 12 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 5 110 ; 
       17 11 110 ; 
       18 11 110 ; 
       20 2 110 ; 
       21 12 110 ; 
       22 15 110 ; 
       23 12 110 ; 
       24 18 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 5 110 ; 
       34 5 110 ; 
       35 5 110 ; 
       36 12 110 ; 
       37 9 110 ; 
       38 10 110 ; 
       39 13 110 ; 
       40 12 110 ; 
       41 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 37 300 ; 
       8 0 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       9 36 300 ; 
       10 35 300 ; 
       12 27 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       13 1 300 ; 
       14 38 300 ; 
       14 5 300 ; 
       14 8 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       15 38 300 ; 
       15 39 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       17 38 300 ; 
       17 4 300 ; 
       17 7 300 ; 
       17 13 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       18 38 300 ; 
       18 26 300 ; 
       18 28 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       20 40 300 ; 
       21 48 300 ; 
       22 41 300 ; 
       23 42 300 ; 
       24 43 300 ; 
       25 44 300 ; 
       27 45 300 ; 
       28 46 300 ; 
       29 47 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 30 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 34 401 ; 
       5 35 401 ; 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 27 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       39 33 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       19 SCHEM 37.00958 2.33194 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -10 0 MPRFLG 0 ; 
       3 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 SRT 1 1 1 -6.357303e-008 0 0 0 -5.805163e-008 4.776659e-007 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 33.60908 1.032502 0 USR SRT 5.95 5.95 5.95 -1.570796 0 0 -7.384752 0.171197 57.53644 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 MPRFLG 0 ; 
       11 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 2.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       29 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 0 -8 0 MPRFLG 0 ; 
       31 SCHEM 30 -8 0 MPRFLG 0 ; 
       32 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 35 -8 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 40 -8 0 MPRFLG 0 ; 
       0 SCHEM 31.77292 2.278067 0 USR WIRECOL 7 7 SRT 7.695043 7.695043 7.695043 0.08000001 0 0 0 5.604937 21.47559 MPRFLG 0 ; 
       1 SCHEM 34.50958 2.33194 0 USR WIRECOL 7 7 SRT 7.695043 7.695043 7.695043 3.189999 0 0 0 -4.602011 -5.379458 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
