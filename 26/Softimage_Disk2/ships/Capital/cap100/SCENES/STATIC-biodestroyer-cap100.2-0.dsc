SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.28-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       biodestroyer_cap100-mat101.2-0 ; 
       biodestroyer_cap100-mat121.1-0 ; 
       biodestroyer_cap100-mat122.1-0 ; 
       biodestroyer_cap100-mat123.1-0 ; 
       biodestroyer_cap100-mat124.1-0 ; 
       biodestroyer_cap100-mat125.1-0 ; 
       biodestroyer_cap100-mat126.1-0 ; 
       biodestroyer_cap100-mat128.1-0 ; 
       biodestroyer_cap100-mat130.1-0 ; 
       biodestroyer_cap100-mat131.1-0 ; 
       biodestroyer_cap100-mat132.1-0 ; 
       biodestroyer_cap100-mat133.1-0 ; 
       biodestroyer_cap100-mat134.1-0 ; 
       biodestroyer_cap100-mat135.1-0 ; 
       biodestroyer_cap100-mat136.1-0 ; 
       biodestroyer_cap100-mat137.1-0 ; 
       biodestroyer_cap100-mat138.1-0 ; 
       biodestroyer_cap100-mat140.1-0 ; 
       biodestroyer_cap100-mat142.1-0 ; 
       biodestroyer_cap100-mat143.1-0 ; 
       biodestroyer_cap100-mat144.1-0 ; 
       biodestroyer_cap100-mat145.1-0 ; 
       biodestroyer_cap100-mat146.1-0 ; 
       biodestroyer_cap100-mat147.1-0 ; 
       biodestroyer_cap100-mat148.1-0 ; 
       biodestroyer_cap100-mat82.4-0 ; 
       biodestroyer_cap100-mat83.1-0 ; 
       biodestroyer_cap100-mat84.1-0 ; 
       biodestroyer_cap100-mat91.3-0 ; 
       biodestroyer_cap100-mat92.2-0 ; 
       biodestroyer_cap100-mat93.3-0 ; 
       biodestroyer_cap100-mat94.2-0 ; 
       biodestroyer_cap100-mat95.2-0 ; 
       biodestroyer_cap100-mat96.2-0 ; 
       biodestroyer_cap100-mat97.3-0 ; 
       biodestroyer_cap100-mat98.3-0 ; 
       biodestroyer_cap100-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       destroyer-cube1.26-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru10.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru3.1-0 ; 
       destroyer-extru38.1-0 ; 
       destroyer-extru39.1-0 ; 
       destroyer-extru40.1-0 ; 
       destroyer-extru41.1-0 ; 
       destroyer-extru42.1-0 ; 
       destroyer-extru43.1-0 ; 
       destroyer-extru44.1-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-sphere4.1-0 ; 
       destroyer-sphere5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap100/PICTURES/cap100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-biodestroyer-cap100.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       biodestroyer_cap100-t2d1.4-0 ; 
       biodestroyer_cap100-t2d10.5-0 ; 
       biodestroyer_cap100-t2d11.5-0 ; 
       biodestroyer_cap100-t2d12.6-0 ; 
       biodestroyer_cap100-t2d13.6-0 ; 
       biodestroyer_cap100-t2d14.6-0 ; 
       biodestroyer_cap100-t2d15.7-0 ; 
       biodestroyer_cap100-t2d16.6-0 ; 
       biodestroyer_cap100-t2d17.5-0 ; 
       biodestroyer_cap100-t2d2.4-0 ; 
       biodestroyer_cap100-t2d36.3-0 ; 
       biodestroyer_cap100-t2d37.3-0 ; 
       biodestroyer_cap100-t2d38.3-0 ; 
       biodestroyer_cap100-t2d39.3-0 ; 
       biodestroyer_cap100-t2d40.3-0 ; 
       biodestroyer_cap100-t2d41.3-0 ; 
       biodestroyer_cap100-t2d42.3-0 ; 
       biodestroyer_cap100-t2d44.2-0 ; 
       biodestroyer_cap100-t2d46.2-0 ; 
       biodestroyer_cap100-t2d47.2-0 ; 
       biodestroyer_cap100-t2d48.2-0 ; 
       biodestroyer_cap100-t2d49.2-0 ; 
       biodestroyer_cap100-t2d50.2-0 ; 
       biodestroyer_cap100-t2d51.2-0 ; 
       biodestroyer_cap100-t2d52.2-0 ; 
       biodestroyer_cap100-t2d53.2-0 ; 
       biodestroyer_cap100-t2d54.2-0 ; 
       biodestroyer_cap100-t2d56.2-0 ; 
       biodestroyer_cap100-t2d58.2-0 ; 
       biodestroyer_cap100-t2d59.2-0 ; 
       biodestroyer_cap100-t2d60.2-0 ; 
       biodestroyer_cap100-t2d61.2-0 ; 
       biodestroyer_cap100-t2d62.2-0 ; 
       biodestroyer_cap100-t2d63.2-0 ; 
       biodestroyer_cap100-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 17 110 ; 
       5 16 110 ; 
       6 7 110 ; 
       7 15 110 ; 
       8 0 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 0 110 ; 
       13 15 110 ; 
       14 13 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 8 110 ; 
       19 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 25 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       1 2 300 ; 
       2 3 300 ; 
       3 0 300 ; 
       4 1 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       9 8 300 ; 
       9 9 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       14 22 300 ; 
       14 23 300 ; 
       14 24 300 ; 
       18 7 300 ; 
       19 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 11 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 26 401 ; 
       17 27 401 ; 
       19 28 401 ; 
       20 29 401 ; 
       21 30 401 ; 
       22 31 401 ; 
       23 32 401 ; 
       24 33 401 ; 
       25 34 401 ; 
       26 0 401 ; 
       27 9 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       31 3 401 ; 
       32 4 401 ; 
       33 5 401 ; 
       34 8 401 ; 
       35 6 401 ; 
       36 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 2.306939 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 MPRFLG 0 ; 
       12 SCHEM 30 -2 0 MPRFLG 0 ; 
       13 SCHEM 25 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
