SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.23-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       biodestroyer_cap100-mat100.2-0 ; 
       biodestroyer_cap100-mat101.2-0 ; 
       biodestroyer_cap100-mat103.1-0 ; 
       biodestroyer_cap100-mat104.1-0 ; 
       biodestroyer_cap100-mat105.1-0 ; 
       biodestroyer_cap100-mat106.1-0 ; 
       biodestroyer_cap100-mat107.1-0 ; 
       biodestroyer_cap100-mat108.1-0 ; 
       biodestroyer_cap100-mat109.1-0 ; 
       biodestroyer_cap100-mat110.1-0 ; 
       biodestroyer_cap100-mat111.1-0 ; 
       biodestroyer_cap100-mat112.1-0 ; 
       biodestroyer_cap100-mat113.1-0 ; 
       biodestroyer_cap100-mat114.1-0 ; 
       biodestroyer_cap100-mat115.1-0 ; 
       biodestroyer_cap100-mat116.1-0 ; 
       biodestroyer_cap100-mat117.1-0 ; 
       biodestroyer_cap100-mat118.1-0 ; 
       biodestroyer_cap100-mat119.1-0 ; 
       biodestroyer_cap100-mat120.1-0 ; 
       biodestroyer_cap100-mat121.1-0 ; 
       biodestroyer_cap100-mat122.1-0 ; 
       biodestroyer_cap100-mat123.1-0 ; 
       biodestroyer_cap100-mat81.1-0 ; 
       biodestroyer_cap100-mat82.4-0 ; 
       biodestroyer_cap100-mat83.1-0 ; 
       biodestroyer_cap100-mat84.1-0 ; 
       biodestroyer_cap100-mat85.1-0 ; 
       biodestroyer_cap100-mat86.1-0 ; 
       biodestroyer_cap100-mat87.1-0 ; 
       biodestroyer_cap100-mat88.1-0 ; 
       biodestroyer_cap100-mat89.1-0 ; 
       biodestroyer_cap100-mat90.1-0 ; 
       biodestroyer_cap100-mat91.3-0 ; 
       biodestroyer_cap100-mat92.2-0 ; 
       biodestroyer_cap100-mat93.3-0 ; 
       biodestroyer_cap100-mat94.2-0 ; 
       biodestroyer_cap100-mat95.2-0 ; 
       biodestroyer_cap100-mat96.2-0 ; 
       biodestroyer_cap100-mat97.3-0 ; 
       biodestroyer_cap100-mat98.3-0 ; 
       biodestroyer_cap100-mat99.2-0 ; 
       edit_nulls-mat70.1-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.1-0 ; 
       utl101-mat78.1-0 ; 
       utl101-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       destroyer-bthrust.1-0 ; 
       destroyer-cockpt.1-0 ; 
       destroyer-cube1.21-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru1.1-0 ; 
       destroyer-extru10.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru3.1-0 ; 
       destroyer-extru32.1-0 ; 
       destroyer-extru33.1-0 ; 
       destroyer-extru34.1-0 ; 
       destroyer-extru35.1-0 ; 
       destroyer-extru36.1-0 ; 
       destroyer-extru37.1-0 ; 
       destroyer-lsmoke.1-0 ; 
       destroyer-lthrust.1-0 ; 
       destroyer-lwepemt.1-0 ; 
       destroyer-missemt.1-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-octa1.1-0 ; 
       destroyer-octa7.1-0 ; 
       destroyer-octa8.1-0 ; 
       destroyer-octa9.1-0 ; 
       destroyer-rsmoke.1-0 ; 
       destroyer-rthrust.1-0 ; 
       destroyer-rwepemt.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-SS01.1-0 ; 
       destroyer-SS02.1-0 ; 
       destroyer-SS03.1-0 ; 
       destroyer-SS04.1-0 ; 
       destroyer-SS05.1-0 ; 
       destroyer-SS06.1-0 ; 
       destroyer-trail.1-0 ; 
       destroyer-tthrust.1-0 ; 
       destroyer-turwepemt1.1-0 ; 
       destroyer-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap100/PICTURES/cap100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-biodestroyer-cap100.21-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       biodestroyer_cap100-t2d1.1-0 ; 
       biodestroyer_cap100-t2d10.3-0 ; 
       biodestroyer_cap100-t2d11.3-0 ; 
       biodestroyer_cap100-t2d12.3-0 ; 
       biodestroyer_cap100-t2d13.3-0 ; 
       biodestroyer_cap100-t2d14.3-0 ; 
       biodestroyer_cap100-t2d15.4-0 ; 
       biodestroyer_cap100-t2d16.3-0 ; 
       biodestroyer_cap100-t2d17.2-0 ; 
       biodestroyer_cap100-t2d18.1-0 ; 
       biodestroyer_cap100-t2d19.1-0 ; 
       biodestroyer_cap100-t2d2.1-0 ; 
       biodestroyer_cap100-t2d20.1-0 ; 
       biodestroyer_cap100-t2d21.1-0 ; 
       biodestroyer_cap100-t2d22.1-0 ; 
       biodestroyer_cap100-t2d23.1-0 ; 
       biodestroyer_cap100-t2d24.1-0 ; 
       biodestroyer_cap100-t2d25.1-0 ; 
       biodestroyer_cap100-t2d26.1-0 ; 
       biodestroyer_cap100-t2d27.1-0 ; 
       biodestroyer_cap100-t2d28.1-0 ; 
       biodestroyer_cap100-t2d29.1-0 ; 
       biodestroyer_cap100-t2d3.1-0 ; 
       biodestroyer_cap100-t2d30.1-0 ; 
       biodestroyer_cap100-t2d31.1-0 ; 
       biodestroyer_cap100-t2d32.1-0 ; 
       biodestroyer_cap100-t2d33.1-0 ; 
       biodestroyer_cap100-t2d34.1-0 ; 
       biodestroyer_cap100-t2d35.1-0 ; 
       biodestroyer_cap100-t2d36.1-0 ; 
       biodestroyer_cap100-t2d37.1-0 ; 
       biodestroyer_cap100-t2d38.1-0 ; 
       biodestroyer_cap100-t2d39.1-0 ; 
       biodestroyer_cap100-t2d4.1-0 ; 
       biodestroyer_cap100-t2d5.1-0 ; 
       biodestroyer_cap100-t2d6.1-0 ; 
       biodestroyer_cap100-t2d7.1-0 ; 
       biodestroyer_cap100-t2d8.1-0 ; 
       biodestroyer_cap100-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       5 23 110 ; 
       4 23 110 ; 
       7 21 110 ; 
       8 22 110 ; 
       9 10 110 ; 
       6 23 110 ; 
       25 7 110 ; 
       16 2 110 ; 
       10 21 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 7 110 ; 
       3 23 110 ; 
       26 16 110 ; 
       32 16 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       27 16 110 ; 
       31 7 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 2 110 ; 
       14 21 110 ; 
       15 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 24 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       5 1 300 ; 
       4 22 300 ; 
       7 0 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       9 39 300 ; 
       9 40 300 ; 
       9 41 300 ; 
       6 20 300 ; 
       25 13 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       10 37 300 ; 
       10 38 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       24 12 300 ; 
       3 21 300 ; 
       26 17 300 ; 
       32 18 300 ; 
       27 19 300 ; 
       31 11 300 ; 
       33 42 300 ; 
       34 43 300 ; 
       35 23 300 ; 
       36 44 300 ; 
       37 46 300 ; 
       38 45 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       24 38 401 ; 
       25 0 401 ; 
       26 11 401 ; 
       27 22 401 ; 
       28 33 401 ; 
       29 34 401 ; 
       30 35 401 ; 
       31 36 401 ; 
       32 37 401 ; 
       33 1 401 ; 
       34 2 401 ; 
       36 3 401 ; 
       37 4 401 ; 
       38 5 401 ; 
       39 8 401 ; 
       40 6 401 ; 
       41 7 401 ; 
       0 18 401 ; 
       1 29 401 ; 
       13 21 401 ; 
       3 9 401 ; 
       4 10 401 ; 
       5 12 401 ; 
       6 13 401 ; 
       7 14 401 ; 
       8 15 401 ; 
       9 16 401 ; 
       10 17 401 ; 
       11 19 401 ; 
       12 20 401 ; 
       14 23 401 ; 
       15 24 401 ; 
       16 25 401 ; 
       17 26 401 ; 
       18 27 401 ; 
       19 28 401 ; 
       20 30 401 ; 
       21 31 401 ; 
       22 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 76.25 0 0 SRT 0.2523824 0.5869358 0.5869358 0 0 0 0 0 -2.49162 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 136.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 71.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       22 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       23 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       24 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 55 -4 0 MPRFLG 0 ; 
       26 SCHEM 130 -4 0 MPRFLG 0 ; 
       32 SCHEM 132.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 135 -4 0 MPRFLG 0 ; 
       31 SCHEM 100 -6 0 MPRFLG 0 ; 
       33 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 120 -4 0 MPRFLG 0 ; 
       15 SCHEM 115 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       23 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 147.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 147.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 150 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 145 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 140 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
