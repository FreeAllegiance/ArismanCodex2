SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       biodestroyer-mat81.1-0 ; 
       edit_nulls-mat70.1-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.1-0 ; 
       utl101-mat78.1-0 ; 
       utl101-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       destroyer-bthrust.1-0 ; 
       destroyer-cockpt.1-0 ; 
       destroyer-cube1.2-0 ROOT ; 
       destroyer-cyl2.1-0 ; 
       destroyer-cyl4.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl8.1-0 ; 
       destroyer-extru1.1-0 ; 
       destroyer-extru10.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru25.1-0 ; 
       destroyer-extru26.1-0 ; 
       destroyer-extru28.1-0 ; 
       destroyer-extru29.1-0 ; 
       destroyer-extru3.1-0 ; 
       destroyer-extru30.1-0 ; 
       destroyer-extru31.1-0 ; 
       destroyer-lsmoke.1-0 ; 
       destroyer-lthrust.1-0 ; 
       destroyer-lwepemt.1-0 ; 
       destroyer-missemt.1-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-octa1.1-0 ; 
       destroyer-octa5.1-0 ; 
       destroyer-octa6.1-0 ; 
       destroyer-octa7.1-0 ; 
       destroyer-rsmoke.1-0 ; 
       destroyer-rthrust.1-0 ; 
       destroyer-rwepemt.1-0 ; 
       destroyer-sphere1.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-SS01.1-0 ; 
       destroyer-SS02.1-0 ; 
       destroyer-SS03.1-0 ; 
       destroyer-SS04.1-0 ; 
       destroyer-SS05.1-0 ; 
       destroyer-SS06.1-0 ; 
       destroyer-trail.1-0 ; 
       destroyer-tthrust.1-0 ; 
       destroyer-turwepemt1.1-0 ; 
       destroyer-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-biodestroyer.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       35 2 110 ; 
       3 23 110 ; 
       4 23 110 ; 
       5 23 110 ; 
       6 23 110 ; 
       7 21 110 ; 
       8 22 110 ; 
       9 14 110 ; 
       10 21 110 ; 
       11 10 110 ; 
       12 21 110 ; 
       13 22 110 ; 
       14 21 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 12 110 ; 
       32 7 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       35 0 300 ; 
       33 1 300 ; 
       34 2 300 ; 
       36 3 300 ; 
       37 5 300 ; 
       38 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000002 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 31.94729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34.44729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -0.639985 7.845662 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       2 SCHEM 14.58843 -1.780122 0 USR SRT 0.2523824 0.5869358 0.5869358 0 0 0 0 0 -2.49162 MPRFLG 0 ; 
       3 SCHEM -1.832637 -9.263373 0 MPRFLG 0 ; 
       4 SCHEM 0.6673632 -9.263373 0 MPRFLG 0 ; 
       5 SCHEM -4.332637 -9.263373 0 MPRFLG 0 ; 
       6 SCHEM 3.167367 -9.263373 0 MPRFLG 0 ; 
       7 SCHEM 22.89942 -7.253805 0 MPRFLG 0 ; 
       8 SCHEM 5.399423 -9.263373 0 MPRFLG 0 ; 
       9 SCHEM 15.39942 -9.253805 0 MPRFLG 0 ; 
       10 SCHEM 17.89941 -7.253805 0 MPRFLG 0 ; 
       11 SCHEM 17.89941 -9.253805 0 MPRFLG 0 ; 
       12 SCHEM 30.39942 -7.253805 0 MPRFLG 0 ; 
       13 SCHEM 7.899423 -9.263373 0 MPRFLG 0 ; 
       14 SCHEM 15.39942 -7.253805 0 MPRFLG 0 ; 
       15 SCHEM 10.39942 -9.263373 0 MPRFLG 0 ; 
       16 SCHEM 12.89942 -9.263373 0 MPRFLG 0 ; 
       17 SCHEM 6.947293 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4.447293 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9.447291 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24.44729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24.14942 -5.253805 0 USR MPRFLG 0 ; 
       22 SCHEM 9.149422 -7.263373 0 USR MPRFLG 0 ; 
       23 SCHEM -0.5826368 -7.263373 0 USR MPRFLG 0 ; 
       24 SCHEM 20.39941 -9.253805 0 MPRFLG 0 ; 
       25 SCHEM 22.89942 -9.253805 0 MPRFLG 0 ; 
       26 SCHEM 27.89942 -9.253805 0 MPRFLG 0 ; 
       27 SCHEM 30.39942 -9.253805 0 MPRFLG 0 ; 
       28 SCHEM 26.94729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14.44729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.94729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.89942 -9.253805 0 MPRFLG 0 ; 
       32 SCHEM 25.39942 -9.253805 0 MPRFLG 0 ; 
       33 SCHEM -2.972177 7.795155 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM -3.01213 7.085079 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM -0.7128341 7.046446 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 1.599674 7.821894 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 1.639939 6.992967 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 21.94728 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29.44729 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19.44728 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.94728 7.875374 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 114.6095 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 157.4571 -3.820249 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 119.6095 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 122.1095 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
