SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.31-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       biodestroyer_cap100-mat101.2-0 ; 
       biodestroyer_cap100-mat121.1-0 ; 
       biodestroyer_cap100-mat122.1-0 ; 
       biodestroyer_cap100-mat123.1-0 ; 
       biodestroyer_cap100-mat124.1-0 ; 
       biodestroyer_cap100-mat125.1-0 ; 
       biodestroyer_cap100-mat126.1-0 ; 
       biodestroyer_cap100-mat127.1-0 ; 
       biodestroyer_cap100-mat128.1-0 ; 
       biodestroyer_cap100-mat129.1-0 ; 
       biodestroyer_cap100-mat130.1-0 ; 
       biodestroyer_cap100-mat131.1-0 ; 
       biodestroyer_cap100-mat132.1-0 ; 
       biodestroyer_cap100-mat133.1-0 ; 
       biodestroyer_cap100-mat134.1-0 ; 
       biodestroyer_cap100-mat135.1-0 ; 
       biodestroyer_cap100-mat136.1-0 ; 
       biodestroyer_cap100-mat137.1-0 ; 
       biodestroyer_cap100-mat138.1-0 ; 
       biodestroyer_cap100-mat139.1-0 ; 
       biodestroyer_cap100-mat140.1-0 ; 
       biodestroyer_cap100-mat141.1-0 ; 
       biodestroyer_cap100-mat142.1-0 ; 
       biodestroyer_cap100-mat143.1-0 ; 
       biodestroyer_cap100-mat144.1-0 ; 
       biodestroyer_cap100-mat145.1-0 ; 
       biodestroyer_cap100-mat146.1-0 ; 
       biodestroyer_cap100-mat147.1-0 ; 
       biodestroyer_cap100-mat148.1-0 ; 
       biodestroyer_cap100-mat81.1-0 ; 
       biodestroyer_cap100-mat82.4-0 ; 
       biodestroyer_cap100-mat83.1-0 ; 
       biodestroyer_cap100-mat84.1-0 ; 
       biodestroyer_cap100-mat91.3-0 ; 
       biodestroyer_cap100-mat92.2-0 ; 
       biodestroyer_cap100-mat93.3-0 ; 
       biodestroyer_cap100-mat94.2-0 ; 
       biodestroyer_cap100-mat95.2-0 ; 
       biodestroyer_cap100-mat96.2-0 ; 
       biodestroyer_cap100-mat97.3-0 ; 
       biodestroyer_cap100-mat98.3-0 ; 
       biodestroyer_cap100-mat99.2-0 ; 
       edit_nulls-mat70.1-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.1-0 ; 
       utl101-mat78.1-0 ; 
       utl101-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       destroyer-bthrust.1-0 ; 
       destroyer-cockpt.1-0 ; 
       destroyer-cube1.28-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru10.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru3.1-0 ; 
       destroyer-extru38.1-0 ; 
       destroyer-extru39.1-0 ; 
       destroyer-extru40.1-0 ; 
       destroyer-extru41.1-0 ; 
       destroyer-extru42.1-0 ; 
       destroyer-extru43.1-0 ; 
       destroyer-extru44.1-0 ; 
       destroyer-lsmoke.1-0 ; 
       destroyer-lthrust.1-0 ; 
       destroyer-lwepemt.1-0 ; 
       destroyer-missemt.1-0 ; 
       destroyer-mwepemt1.1-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-octa10.1-0 ; 
       destroyer-octa11.1-0 ; 
       destroyer-octa12.1-0 ; 
       destroyer-octa13.1-0 ; 
       destroyer-rsmoke.1-0 ; 
       destroyer-rthrust.1-0 ; 
       destroyer-rwepemt.1-0 ; 
       destroyer-sphere4.1-0 ; 
       destroyer-sphere5.1-0 ; 
       destroyer-SS01.1-0 ; 
       destroyer-SS02.1-0 ; 
       destroyer-SS03.1-0 ; 
       destroyer-SS04.1-0 ; 
       destroyer-SS05.1-0 ; 
       destroyer-SS06.1-0 ; 
       destroyer-trail.1-0 ; 
       destroyer-tthrust.1-0 ; 
       destroyer-turwepemt1.1-0 ; 
       destroyer-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap100/PICTURES/cap100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       scale_to_6_si-biodestroyer-cap100.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       biodestroyer_cap100-t2d1.6-0 ; 
       biodestroyer_cap100-t2d10.7-0 ; 
       biodestroyer_cap100-t2d11.7-0 ; 
       biodestroyer_cap100-t2d12.8-0 ; 
       biodestroyer_cap100-t2d13.8-0 ; 
       biodestroyer_cap100-t2d14.8-0 ; 
       biodestroyer_cap100-t2d15.9-0 ; 
       biodestroyer_cap100-t2d16.8-0 ; 
       biodestroyer_cap100-t2d17.7-0 ; 
       biodestroyer_cap100-t2d2.6-0 ; 
       biodestroyer_cap100-t2d36.5-0 ; 
       biodestroyer_cap100-t2d37.5-0 ; 
       biodestroyer_cap100-t2d38.5-0 ; 
       biodestroyer_cap100-t2d39.5-0 ; 
       biodestroyer_cap100-t2d40.5-0 ; 
       biodestroyer_cap100-t2d41.5-0 ; 
       biodestroyer_cap100-t2d42.5-0 ; 
       biodestroyer_cap100-t2d43.4-0 ; 
       biodestroyer_cap100-t2d44.4-0 ; 
       biodestroyer_cap100-t2d45.4-0 ; 
       biodestroyer_cap100-t2d46.4-0 ; 
       biodestroyer_cap100-t2d47.4-0 ; 
       biodestroyer_cap100-t2d48.4-0 ; 
       biodestroyer_cap100-t2d49.4-0 ; 
       biodestroyer_cap100-t2d50.4-0 ; 
       biodestroyer_cap100-t2d51.4-0 ; 
       biodestroyer_cap100-t2d52.4-0 ; 
       biodestroyer_cap100-t2d53.4-0 ; 
       biodestroyer_cap100-t2d54.4-0 ; 
       biodestroyer_cap100-t2d55.4-0 ; 
       biodestroyer_cap100-t2d56.4-0 ; 
       biodestroyer_cap100-t2d57.4-0 ; 
       biodestroyer_cap100-t2d58.4-0 ; 
       biodestroyer_cap100-t2d59.4-0 ; 
       biodestroyer_cap100-t2d60.4-0 ; 
       biodestroyer_cap100-t2d61.4-0 ; 
       biodestroyer_cap100-t2d62.4-0 ; 
       biodestroyer_cap100-t2d63.4-0 ; 
       biodestroyer_cap100-t2d9.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 24 110 ; 
       4 24 110 ; 
       5 24 110 ; 
       6 24 110 ; 
       7 23 110 ; 
       8 9 110 ; 
       9 22 110 ; 
       10 2 110 ; 
       11 23 110 ; 
       12 23 110 ; 
       13 23 110 ; 
       14 2 110 ; 
       15 22 110 ; 
       16 15 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 2 110 ; 
       43 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 30 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       3 2 300 ; 
       4 3 300 ; 
       5 0 300 ; 
       6 1 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       12 12 300 ; 
       12 13 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       14 16 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       15 22 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       15 25 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       16 28 300 ; 
       25 7 300 ; 
       26 9 300 ; 
       27 19 300 ; 
       28 21 300 ; 
       32 8 300 ; 
       33 20 300 ; 
       34 42 300 ; 
       35 43 300 ; 
       36 29 300 ; 
       37 44 300 ; 
       38 46 300 ; 
       39 45 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 11 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 26 401 ; 
       17 27 401 ; 
       18 28 401 ; 
       19 29 401 ; 
       20 30 401 ; 
       21 31 401 ; 
       23 32 401 ; 
       24 33 401 ; 
       25 34 401 ; 
       26 35 401 ; 
       27 36 401 ; 
       28 37 401 ; 
       30 38 401 ; 
       31 0 401 ; 
       32 9 401 ; 
       33 1 401 ; 
       34 2 401 ; 
       36 3 401 ; 
       37 4 401 ; 
       38 5 401 ; 
       39 8 401 ; 
       40 6 401 ; 
       41 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 76.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 2.306939 MPRFLG 0 ; 
       3 SCHEM 55 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 57.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 61.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 82.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 87.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 121.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 66.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 71.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 76.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 136.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 105 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 100 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 37.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 23.68651 -0.02348962 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 96.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 68.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 53.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 115 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 120 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 130 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 135 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 40 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 77.61836 9.044823 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 79.91888 9.153368 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 2.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 10 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 7.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 12.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 15 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 35 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 42.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 75.07819 9.070336 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 72.57819 9.070336 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 147.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 150 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 147.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 150 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 140 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 132.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 145 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
