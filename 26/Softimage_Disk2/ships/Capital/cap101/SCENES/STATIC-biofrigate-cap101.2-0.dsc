SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.27-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       biofrigate_cap101-mat100.2-0 ; 
       biofrigate_cap101-mat101.1-0 ; 
       biofrigate_cap101-mat102.1-0 ; 
       biofrigate_cap101-mat103.1-0 ; 
       biofrigate_cap101-mat104.1-0 ; 
       biofrigate_cap101-mat119.1-0 ; 
       biofrigate_cap101-mat132.1-0 ; 
       biofrigate_cap101-mat133.1-0 ; 
       biofrigate_cap101-mat134.1-0 ; 
       biofrigate_cap101-mat135.1-0 ; 
       biofrigate_cap101-mat136.1-0 ; 
       biofrigate_cap101-mat137.1-0 ; 
       biofrigate_cap101-mat138.1-0 ; 
       biofrigate_cap101-mat139.1-0 ; 
       biofrigate_cap101-mat140.1-0 ; 
       biofrigate_cap101-mat141.1-0 ; 
       biofrigate_cap101-mat142.1-0 ; 
       biofrigate_cap101-mat143.1-0 ; 
       biofrigate_cap101-mat144.1-0 ; 
       biofrigate_cap101-mat145.1-0 ; 
       biofrigate_cap101-mat146.1-0 ; 
       biofrigate_cap101-mat147.1-0 ; 
       biofrigate_cap101-mat148.1-0 ; 
       biofrigate_cap101-mat149.1-0 ; 
       biofrigate_cap101-mat150.1-0 ; 
       biofrigate_cap101-mat151.1-0 ; 
       biofrigate_cap101-mat93.1-0 ; 
       biofrigate_cap101-mat94.1-0 ; 
       biofrigate_cap101-mat95.1-0 ; 
       biofrigate_cap101-mat96.3-0 ; 
       biofrigate_cap101-mat97.2-0 ; 
       biofrigate_cap101-mat98.3-0 ; 
       biofrigate_cap101-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       destroyer-cube1_2.19-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru51.1-0 ; 
       destroyer-extru52.1-0 ; 
       destroyer-extru53.1-0 ; 
       destroyer-extru54.1-0 ; 
       destroyer-extru55.1-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-sphere14.1-0 ; 
       destroyer-sphere15.1-0 ; 
       destroyer-sphere16.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-sphere8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap101/PICTURES/cap101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-biofrigate-cap101.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       biofrigate_cap101-t2d10.4-0 ; 
       biofrigate_cap101-t2d11.3-0 ; 
       biofrigate_cap101-t2d12.3-0 ; 
       biofrigate_cap101-t2d13.3-0 ; 
       biofrigate_cap101-t2d14.3-0 ; 
       biofrigate_cap101-t2d15.2-0 ; 
       biofrigate_cap101-t2d16.2-0 ; 
       biofrigate_cap101-t2d17.2-0 ; 
       biofrigate_cap101-t2d27.2-0 ; 
       biofrigate_cap101-t2d28.2-0 ; 
       biofrigate_cap101-t2d29.2-0 ; 
       biofrigate_cap101-t2d30.3-0 ; 
       biofrigate_cap101-t2d44.3-0 ; 
       biofrigate_cap101-t2d45.3-0 ; 
       biofrigate_cap101-t2d46.3-0 ; 
       biofrigate_cap101-t2d47.2-0 ; 
       biofrigate_cap101-t2d48.2-0 ; 
       biofrigate_cap101-t2d49.2-0 ; 
       biofrigate_cap101-t2d50.2-0 ; 
       biofrigate_cap101-t2d51.2-0 ; 
       biofrigate_cap101-t2d52.2-0 ; 
       biofrigate_cap101-t2d53.2-0 ; 
       biofrigate_cap101-t2d54.2-0 ; 
       biofrigate_cap101-t2d55.2-0 ; 
       biofrigate_cap101-t2d56.2-0 ; 
       biofrigate_cap101-t2d57.2-0 ; 
       biofrigate_cap101-t2d58.2-0 ; 
       biofrigate_cap101-t2d59.2-0 ; 
       biofrigate_cap101-t2d60.2-0 ; 
       biofrigate_cap101-t2d61.2-0 ; 
       biofrigate_cap101-t2d62.2-0 ; 
       biofrigate_cap101-t2d63.2-0 ; 
       biofrigate_cap101-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 11 110 ; 
       14 0 110 ; 
       10 11 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       7 12 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       6 12 110 ; 
       8 12 110 ; 
       17 0 110 ; 
       18 5 110 ; 
       9 12 110 ; 
       19 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 31 300 ; 
       0 32 300 ; 
       0 0 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 1 300 ; 
       4 2 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       14 10 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       16 25 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       17 9 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       19 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       11 17 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       6 12 401 ; 
       7 13 401 ; 
       8 14 401 ; 
       26 11 401 ; 
       27 32 401 ; 
       28 0 401 ; 
       29 9 401 ; 
       30 1 401 ; 
       31 8 401 ; 
       32 2 401 ; 
       5 10 401 ; 
       25 31 401 ; 
       9 15 401 ; 
       10 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 2.254684 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 35 -2 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       11 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
