SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.31-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       biofrigate_cap101-mat100_1.1-0 ; 
       biofrigate_cap101-mat101_1.1-0 ; 
       biofrigate_cap101-mat102_1.1-0 ; 
       biofrigate_cap101-mat103_1.1-0 ; 
       biofrigate_cap101-mat104_1.1-0 ; 
       biofrigate_cap101-mat119_1.1-0 ; 
       biofrigate_cap101-mat132_1.1-0 ; 
       biofrigate_cap101-mat133_1.1-0 ; 
       biofrigate_cap101-mat134_1.1-0 ; 
       biofrigate_cap101-mat135_1.1-0 ; 
       biofrigate_cap101-mat136_1.1-0 ; 
       biofrigate_cap101-mat137_1.1-0 ; 
       biofrigate_cap101-mat138_1.1-0 ; 
       biofrigate_cap101-mat139_1.1-0 ; 
       biofrigate_cap101-mat140_1.1-0 ; 
       biofrigate_cap101-mat141_1.1-0 ; 
       biofrigate_cap101-mat142_1.1-0 ; 
       biofrigate_cap101-mat143_1.1-0 ; 
       biofrigate_cap101-mat144_1.1-0 ; 
       biofrigate_cap101-mat145_1.1-0 ; 
       biofrigate_cap101-mat146_1.1-0 ; 
       biofrigate_cap101-mat147_1.1-0 ; 
       biofrigate_cap101-mat148_1.1-0 ; 
       biofrigate_cap101-mat149_1.1-0 ; 
       biofrigate_cap101-mat150_1.1-0 ; 
       biofrigate_cap101-mat151_1.1-0 ; 
       biofrigate_cap101-mat93_1.1-0 ; 
       biofrigate_cap101-mat94_1.1-0 ; 
       biofrigate_cap101-mat95_1.1-0 ; 
       biofrigate_cap101-mat96_1.1-0 ; 
       biofrigate_cap101-mat97_1.1-0 ; 
       biofrigate_cap101-mat98_1.1-0 ; 
       biofrigate_cap101-mat99_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       destroyer-cube1_2.24-0 ROOT ; 
       destroyer-cyl10_2.1-0 ; 
       destroyer-cyl11_2.1-0 ; 
       destroyer-cyl6_2.1-0 ; 
       destroyer-cyl9_2.1-0 ; 
       destroyer-extru2_2.1-0 ; 
       destroyer-extru51_1.1-0 ; 
       destroyer-extru52_1.1-0 ; 
       destroyer-extru53_1.1-0 ; 
       destroyer-extru54_1.1-0 ; 
       destroyer-extru55_1.1-0 ; 
       destroyer-null1_2.1-0 ; 
       destroyer-null2_2.1-0 ; 
       destroyer-null3_2.1-0 ; 
       destroyer-sphere14_1.1-0 ; 
       destroyer-sphere15_1.1-0 ; 
       destroyer-sphere16_1.1-0 ; 
       destroyer-sphere2_1.1-0 ; 
       destroyer-sphere3_1.1-0 ; 
       destroyer-sphere8_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap101/PICTURES/cap101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-biofrigate-cap101.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       biofrigate_cap101-t2d10.4-0 ; 
       biofrigate_cap101-t2d11.3-0 ; 
       biofrigate_cap101-t2d12.6-0 ; 
       biofrigate_cap101-t2d13.6-0 ; 
       biofrigate_cap101-t2d14.3-0 ; 
       biofrigate_cap101-t2d15.2-0 ; 
       biofrigate_cap101-t2d16.2-0 ; 
       biofrigate_cap101-t2d17.2-0 ; 
       biofrigate_cap101-t2d27.5-0 ; 
       biofrigate_cap101-t2d28.2-0 ; 
       biofrigate_cap101-t2d29.2-0 ; 
       biofrigate_cap101-t2d30.3-0 ; 
       biofrigate_cap101-t2d44.3-0 ; 
       biofrigate_cap101-t2d45.3-0 ; 
       biofrigate_cap101-t2d46.3-0 ; 
       biofrigate_cap101-t2d47.2-0 ; 
       biofrigate_cap101-t2d48.2-0 ; 
       biofrigate_cap101-t2d49.2-0 ; 
       biofrigate_cap101-t2d50.2-0 ; 
       biofrigate_cap101-t2d51.2-0 ; 
       biofrigate_cap101-t2d52.2-0 ; 
       biofrigate_cap101-t2d53.2-0 ; 
       biofrigate_cap101-t2d54.2-0 ; 
       biofrigate_cap101-t2d55.2-0 ; 
       biofrigate_cap101-t2d56.2-0 ; 
       biofrigate_cap101-t2d57.2-0 ; 
       biofrigate_cap101-t2d58.2-0 ; 
       biofrigate_cap101-t2d59.2-0 ; 
       biofrigate_cap101-t2d60.2-0 ; 
       biofrigate_cap101-t2d61.2-0 ; 
       biofrigate_cap101-t2d62.2-0 ; 
       biofrigate_cap101-t2d63.2-0 ; 
       biofrigate_cap101-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 11 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       9 12 110 ; 
       10 11 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 0 110 ; 
       18 5 110 ; 
       19 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 31 300 ; 
       0 32 300 ; 
       0 0 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 1 300 ; 
       4 2 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       14 10 300 ; 
       15 23 300 ; 
       15 24 300 ; 
       16 25 300 ; 
       17 9 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       19 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 10 401 ; 
       6 12 401 ; 
       7 13 401 ; 
       8 14 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 17 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       25 31 401 ; 
       26 11 401 ; 
       27 32 401 ; 
       28 0 401 ; 
       29 9 401 ; 
       30 1 401 ; 
       31 8 401 ; 
       32 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 103.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 112 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 114.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 107 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 109.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 88.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 97 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 99.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 102 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 104.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 93.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 90.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 100.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 110.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 119.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 92 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 94.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 117 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 87 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 89.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 121 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 106 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 108.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 111 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 113.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 88.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 116 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 118.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 103.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 103.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 103.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 93.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 86 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 86 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 121 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 121 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 86 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 121 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 121 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 106 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 108.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 111 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 113.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 121 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 86 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 88.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 116 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 118.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 91 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 91 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 93.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
