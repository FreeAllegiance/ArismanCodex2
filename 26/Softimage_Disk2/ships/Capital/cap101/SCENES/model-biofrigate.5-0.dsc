SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.2-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.2-0 ; 
       utl101-mat77.2-0 ; 
       utl101-mat78.2-0 ; 
       utl101-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       biofrigate-160deg1.1-0 ROOT ; 
       biofrigate-175deg1.1-0 ROOT ; 
       destroyer-bthrust.3-0 ; 
       destroyer-cockpt.3-0 ; 
       destroyer-cube1_2.3-0 ROOT ; 
       destroyer-cyl2.1-0 ; 
       destroyer-cyl4.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl8.1-0 ; 
       destroyer-extru10.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru37.1-0 ; 
       destroyer-extru38.1-0 ; 
       destroyer-extru39.1-0 ; 
       destroyer-extru40.1-0 ; 
       destroyer-lsmoke.3-0 ; 
       destroyer-lthrust.3-0 ; 
       destroyer-missemt.3-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-rsmoke.3-0 ; 
       destroyer-rthrust.3-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-sphere6.1-0 ; 
       destroyer-sphere7.1-0 ; 
       destroyer-sphere8.1-0 ; 
       destroyer-sphere9.1-0 ; 
       destroyer-SS01.3-0 ; 
       destroyer-SS02.3-0 ; 
       destroyer-SS03.3-0 ; 
       destroyer-SS04.3-0 ; 
       destroyer-SS05.3-0 ; 
       destroyer-SS06.3-0 ; 
       destroyer-trail.3-0 ; 
       destroyer-tthrust.3-0 ; 
       destroyer-turwepemt1.3-0 ; 
       destroyer-turwepemt2.3-0 ; 
       destroyer-turwepemt3.1-0 ; 
       destroyer-turwepemt4.1-0 ; 
       turcone-160deg.1-0 ROOT ; 
       turcone-175deg.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-biofrigate.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 20 110 ; 
       6 20 110 ; 
       7 20 110 ; 
       8 20 110 ; 
       9 19 110 ; 
       10 18 110 ; 
       11 18 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 19 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       24 10 110 ; 
       25 11 110 ; 
       39 4 110 ; 
       40 4 110 ; 
       26 4 110 ; 
       27 10 110 ; 
       28 11 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       21 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       15 4 110 ; 
       22 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 4 110 ; 
       32 4 110 ; 
       23 4 110 ; 
       33 4 110 ; 
       34 4 110 ; 
       35 4 110 ; 
       36 4 110 ; 
       37 4 110 ; 
       38 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       29 0 300 ; 
       30 1 300 ; 
       31 3 300 ; 
       32 2 300 ; 
       33 5 300 ; 
       34 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 131.6819 0.06886934 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 131.6819 -1.93113 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 158.1555 -1.399433 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 144.4055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 146.9055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 141.9055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 149.4055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 151.9055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 163.1554 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 168.1555 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 154.4055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 156.9055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 159.4055 -5.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 169.5133 4.73156 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.542001 3.542001 3.542001 7.806846e-007 1.626817 7.794599e-007 5.216277 0 -4.750401 MPRFLG 0 ; 
       18 SCHEM 165.6555 -3.399433 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 155.6555 -3.399433 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 145.6555 -3.399433 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 164.2794 4.809974 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.468867 3.468867 3.468867 -1.070796 0 0 0 8.644958 2.460105 MPRFLG 0 ; 
       1 SCHEM 166.908 4.727471 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.468867 3.468867 3.468867 -2.070797 0 0 0 -8.644958 2.460105 MPRFLG 0 ; 
       24 SCHEM 161.9054 -7.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 166.9055 -7.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 171.9089 4.811457 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.542001 3.542001 3.542001 -7.806991e-007 1.514777 -7.794744e-007 -5.216277 0 -4.750401 MPRFLG 0 ; 
       39 SCHEM 169.4329 3.948831 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 171.8215 3.876628 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 174.4055 -3.399433 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 164.4055 -7.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 169.4055 -7.399434 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 157.9413 1.721967 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 151.2384 4.049966 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 159.0797 3.744272 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 156.1979 3.087553 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 146.8599 3.477658 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 156.1992 3.728382 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 159.129 3.117713 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 153.2079 8.169942 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 153.1873 7.488785 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 155.5105 8.25168 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 155.4626 7.488785 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 171.9055 -3.399433 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 157.8948 8.224434 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 157.847 7.461539 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 151.021 3.174349 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 157.7329 5.213964 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 164.2884 3.84657 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 166.8712 3.874748 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 94.63045 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 92.13045 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 157.7399 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 160.2399 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 162.7399 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 165.2399 -3.873728 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
