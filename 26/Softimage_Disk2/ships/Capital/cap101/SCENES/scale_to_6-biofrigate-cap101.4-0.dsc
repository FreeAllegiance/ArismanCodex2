SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.30-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       biofrigate_cap101-mat100.2-0 ; 
       biofrigate_cap101-mat101.1-0 ; 
       biofrigate_cap101-mat102.1-0 ; 
       biofrigate_cap101-mat103.1-0 ; 
       biofrigate_cap101-mat104.1-0 ; 
       biofrigate_cap101-mat119.1-0 ; 
       biofrigate_cap101-mat132.1-0 ; 
       biofrigate_cap101-mat133.1-0 ; 
       biofrigate_cap101-mat134.1-0 ; 
       biofrigate_cap101-mat135.1-0 ; 
       biofrigate_cap101-mat136.1-0 ; 
       biofrigate_cap101-mat137.1-0 ; 
       biofrigate_cap101-mat138.1-0 ; 
       biofrigate_cap101-mat139.1-0 ; 
       biofrigate_cap101-mat140.1-0 ; 
       biofrigate_cap101-mat141.1-0 ; 
       biofrigate_cap101-mat142.1-0 ; 
       biofrigate_cap101-mat143.1-0 ; 
       biofrigate_cap101-mat144.1-0 ; 
       biofrigate_cap101-mat145.1-0 ; 
       biofrigate_cap101-mat146.1-0 ; 
       biofrigate_cap101-mat147.1-0 ; 
       biofrigate_cap101-mat148.1-0 ; 
       biofrigate_cap101-mat149.1-0 ; 
       biofrigate_cap101-mat150.1-0 ; 
       biofrigate_cap101-mat151.1-0 ; 
       biofrigate_cap101-mat93.1-0 ; 
       biofrigate_cap101-mat94.1-0 ; 
       biofrigate_cap101-mat95.1-0 ; 
       biofrigate_cap101-mat96.3-0 ; 
       biofrigate_cap101-mat97.2-0 ; 
       biofrigate_cap101-mat98.3-0 ; 
       biofrigate_cap101-mat99.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       utl101-mat71.2-0 ; 
       utl101-mat75.2-0 ; 
       utl101-mat77.2-0 ; 
       utl101-mat78.2-0 ; 
       utl101-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       destroyer-bthrust.3-0 ; 
       destroyer-cockpt.3-0 ; 
       destroyer-cube1_2.22-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru51.1-0 ; 
       destroyer-extru52.1-0 ; 
       destroyer-extru53.1-0 ; 
       destroyer-extru54.1-0 ; 
       destroyer-extru55.1-0 ; 
       destroyer-lsmoke.3-0 ; 
       destroyer-lthrust.3-0 ; 
       destroyer-missemt.3-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-rsmoke.3-0 ; 
       destroyer-rthrust.3-0 ; 
       destroyer-sphere14.1-0 ; 
       destroyer-sphere15.1-0 ; 
       destroyer-sphere16.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-sphere8.1-0 ; 
       destroyer-SS01.3-0 ; 
       destroyer-SS02.3-0 ; 
       destroyer-SS03.3-0 ; 
       destroyer-SS04.3-0 ; 
       destroyer-SS05.3-0 ; 
       destroyer-SS06.3-0 ; 
       destroyer-trail.3-0 ; 
       destroyer-tthrust.3-0 ; 
       destroyer-turwepemt1.3-0 ; 
       destroyer-turwepemt2.3-0 ; 
       destroyer-turwepemt3.1-0 ; 
       destroyer-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap101/PICTURES/cap101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       scale_to_6-biofrigate-cap101.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       biofrigate_cap101-t2d10.6-0 ; 
       biofrigate_cap101-t2d11.5-0 ; 
       biofrigate_cap101-t2d12.5-0 ; 
       biofrigate_cap101-t2d13.5-0 ; 
       biofrigate_cap101-t2d14.5-0 ; 
       biofrigate_cap101-t2d15.4-0 ; 
       biofrigate_cap101-t2d16.4-0 ; 
       biofrigate_cap101-t2d17.4-0 ; 
       biofrigate_cap101-t2d27.4-0 ; 
       biofrigate_cap101-t2d28.4-0 ; 
       biofrigate_cap101-t2d29.4-0 ; 
       biofrigate_cap101-t2d30.5-0 ; 
       biofrigate_cap101-t2d44.5-0 ; 
       biofrigate_cap101-t2d45.5-0 ; 
       biofrigate_cap101-t2d46.5-0 ; 
       biofrigate_cap101-t2d47.4-0 ; 
       biofrigate_cap101-t2d48.4-0 ; 
       biofrigate_cap101-t2d49.4-0 ; 
       biofrigate_cap101-t2d50.4-0 ; 
       biofrigate_cap101-t2d51.4-0 ; 
       biofrigate_cap101-t2d52.4-0 ; 
       biofrigate_cap101-t2d53.4-0 ; 
       biofrigate_cap101-t2d54.4-0 ; 
       biofrigate_cap101-t2d55.4-0 ; 
       biofrigate_cap101-t2d56.4-0 ; 
       biofrigate_cap101-t2d57.4-0 ; 
       biofrigate_cap101-t2d58.4-0 ; 
       biofrigate_cap101-t2d59.4-0 ; 
       biofrigate_cap101-t2d60.4-0 ; 
       biofrigate_cap101-t2d61.4-0 ; 
       biofrigate_cap101-t2d62.4-0 ; 
       biofrigate_cap101-t2d63.4-0 ; 
       biofrigate_cap101-t2d9.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 18 110 ; 
       4 18 110 ; 
       5 18 110 ; 
       6 18 110 ; 
       7 16 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 16 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 2 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 31 300 ; 
       2 32 300 ; 
       2 0 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       21 10 300 ; 
       22 23 300 ; 
       22 24 300 ; 
       23 25 300 ; 
       24 9 300 ; 
       25 29 300 ; 
       25 30 300 ; 
       26 5 300 ; 
       27 33 300 ; 
       28 34 300 ; 
       29 36 300 ; 
       30 35 300 ; 
       31 38 300 ; 
       32 37 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 10 401 ; 
       6 12 401 ; 
       7 13 401 ; 
       8 14 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 17 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       25 31 401 ; 
       26 11 401 ; 
       27 32 401 ; 
       28 0 401 ; 
       29 9 401 ; 
       30 1 401 ; 
       31 8 401 ; 
       32 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 2.254684 MPRFLG 0 ; 
       3 SCHEM 75 -4 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 70 -4 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 65 -4 0 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 80 -2 0 MPRFLG 0 ; 
       25 SCHEM 50 -6 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 81.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 81.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
