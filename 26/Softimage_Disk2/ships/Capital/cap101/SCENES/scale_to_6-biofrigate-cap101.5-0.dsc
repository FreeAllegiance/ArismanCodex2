SDSC3.71
ELEMENTS 
    CHAPTER MODELS NBELEM 39     
       destroyer-bthrust_1.3-0 ; 
       destroyer-cockpt_1.3-0 ; 
       destroyer-cube1_2.23-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru51.1-0 ; 
       destroyer-extru52.1-0 ; 
       destroyer-extru53.1-0 ; 
       destroyer-extru54.1-0 ; 
       destroyer-extru55.1-0 ; 
       destroyer-lsmoke_1.3-0 ; 
       destroyer-lthrust_1.3-0 ; 
       destroyer-missemt_1.3-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-rsmoke_1.3-0 ; 
       destroyer-rthrust_1.3-0 ; 
       destroyer-sphere14.1-0 ; 
       destroyer-sphere15.1-0 ; 
       destroyer-sphere16.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-sphere8.1-0 ; 
       destroyer-SS01_1.3-0 ; 
       destroyer-SS02_1.3-0 ; 
       destroyer-SS03_1.3-0 ; 
       destroyer-SS04_1.3-0 ; 
       destroyer-SS05_1.3-0 ; 
       destroyer-SS06_1.3-0 ; 
       destroyer-trail_1.3-0 ; 
       destroyer-tthrust_1.3-0 ; 
       destroyer-turwepemt1_1.3-0 ; 
       destroyer-turwepemt2_1.3-0 ; 
       destroyer-turwepemt3.1-0 ; 
       destroyer-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap101/PICTURES/cap101 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 18 110 ; 
       4 18 110 ; 
       5 18 110 ; 
       6 18 110 ; 
       7 16 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 16 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 2 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER MODELS 
       0 SCHEM 40 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 77.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 51.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 62.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 65 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 56.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 53.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 63.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 73.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 82.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 80 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 52.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 20 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 17.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 25 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 37.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
