SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       biofrigate_cap101-cam_int1.1-0 ROOT ; 
       biofrigate_cap101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       biofrigate_cap101-mat1.1-0 ; 
       biofrigate_cap101-mat10.1-0 ; 
       biofrigate_cap101-mat11.1-0 ; 
       biofrigate_cap101-mat12.1-0 ; 
       biofrigate_cap101-mat13.1-0 ; 
       biofrigate_cap101-mat14.1-0 ; 
       biofrigate_cap101-mat15.1-0 ; 
       biofrigate_cap101-mat16.1-0 ; 
       biofrigate_cap101-mat17.1-0 ; 
       biofrigate_cap101-mat18.1-0 ; 
       biofrigate_cap101-mat19.1-0 ; 
       biofrigate_cap101-mat2.1-0 ; 
       biofrigate_cap101-mat20.1-0 ; 
       biofrigate_cap101-mat21.1-0 ; 
       biofrigate_cap101-mat22.1-0 ; 
       biofrigate_cap101-mat23.1-0 ; 
       biofrigate_cap101-mat24.1-0 ; 
       biofrigate_cap101-mat25.1-0 ; 
       biofrigate_cap101-mat26.1-0 ; 
       biofrigate_cap101-mat27.1-0 ; 
       biofrigate_cap101-mat28.1-0 ; 
       biofrigate_cap101-mat29.1-0 ; 
       biofrigate_cap101-mat3.1-0 ; 
       biofrigate_cap101-mat30.1-0 ; 
       biofrigate_cap101-mat31.1-0 ; 
       biofrigate_cap101-mat32.1-0 ; 
       biofrigate_cap101-mat33.1-0 ; 
       biofrigate_cap101-mat34.1-0 ; 
       biofrigate_cap101-mat35.1-0 ; 
       biofrigate_cap101-mat36.1-0 ; 
       biofrigate_cap101-mat37.1-0 ; 
       biofrigate_cap101-mat38.1-0 ; 
       biofrigate_cap101-mat39.1-0 ; 
       biofrigate_cap101-mat4.1-0 ; 
       biofrigate_cap101-mat5.1-0 ; 
       biofrigate_cap101-mat6.1-0 ; 
       biofrigate_cap101-mat7.1-0 ; 
       biofrigate_cap101-mat8.1-0 ; 
       biofrigate_cap101-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       destroyer-bthrust.3-0 ; 
       destroyer-cockpt.3-0 ; 
       destroyer-cube1_2.25-0 ROOT ; 
       destroyer-cyl10.1-0 ; 
       destroyer-cyl11.1-0 ; 
       destroyer-cyl6.1-0 ; 
       destroyer-cyl9.1-0 ; 
       destroyer-extru2.1-0 ; 
       destroyer-extru51.1-0 ; 
       destroyer-extru52.1-0 ; 
       destroyer-extru53.1-0 ; 
       destroyer-extru54.1-0 ; 
       destroyer-extru55.1-0 ; 
       destroyer-lsmoke.3-0 ; 
       destroyer-lthrust.3-0 ; 
       destroyer-missemt.3-0 ; 
       destroyer-null1.1-0 ; 
       destroyer-null2.1-0 ; 
       destroyer-null3.1-0 ; 
       destroyer-rsmoke.3-0 ; 
       destroyer-rthrust.3-0 ; 
       destroyer-sphere14.1-0 ; 
       destroyer-sphere15.1-0 ; 
       destroyer-sphere16.1-0 ; 
       destroyer-sphere2.1-0 ; 
       destroyer-sphere3.1-0 ; 
       destroyer-sphere8.1-0 ; 
       destroyer-SS01.3-0 ; 
       destroyer-SS02.3-0 ; 
       destroyer-SS03.3-0 ; 
       destroyer-SS04.3-0 ; 
       destroyer-SS05.3-0 ; 
       destroyer-SS06.3-0 ; 
       destroyer-trail.3-0 ; 
       destroyer-tthrust.3-0 ; 
       destroyer-turwepemt1.3-0 ; 
       destroyer-turwepemt2.3-0 ; 
       destroyer-turwepemt3.1-0 ; 
       destroyer-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap101/PICTURES/cap101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       scale_to_6-biofrigate-cap101.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       biofrigate_cap101-t2d1.3-0 ; 
       biofrigate_cap101-t2d10.7-0 ; 
       biofrigate_cap101-t2d11.6-0 ; 
       biofrigate_cap101-t2d12.7-0 ; 
       biofrigate_cap101-t2d13.7-0 ; 
       biofrigate_cap101-t2d14.6-0 ; 
       biofrigate_cap101-t2d15.5-0 ; 
       biofrigate_cap101-t2d16.5-0 ; 
       biofrigate_cap101-t2d17.5-0 ; 
       biofrigate_cap101-t2d18.2-0 ; 
       biofrigate_cap101-t2d19.2-0 ; 
       biofrigate_cap101-t2d2.2-0 ; 
       biofrigate_cap101-t2d20.2-0 ; 
       biofrigate_cap101-t2d21.3-0 ; 
       biofrigate_cap101-t2d22.3-0 ; 
       biofrigate_cap101-t2d23.2-0 ; 
       biofrigate_cap101-t2d24.2-0 ; 
       biofrigate_cap101-t2d25.2-0 ; 
       biofrigate_cap101-t2d26.2-0 ; 
       biofrigate_cap101-t2d27.6-0 ; 
       biofrigate_cap101-t2d28.5-0 ; 
       biofrigate_cap101-t2d29.5-0 ; 
       biofrigate_cap101-t2d3.3-0 ; 
       biofrigate_cap101-t2d30.6-0 ; 
       biofrigate_cap101-t2d31.2-0 ; 
       biofrigate_cap101-t2d32.2-0 ; 
       biofrigate_cap101-t2d33.2-0 ; 
       biofrigate_cap101-t2d4.3-0 ; 
       biofrigate_cap101-t2d5.2-0 ; 
       biofrigate_cap101-t2d6.2-0 ; 
       biofrigate_cap101-t2d7.2-0 ; 
       biofrigate_cap101-t2d8.2-0 ; 
       biofrigate_cap101-t2d9.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 18 110 ; 
       4 18 110 ; 
       5 18 110 ; 
       6 18 110 ; 
       7 16 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 16 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 2 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       2 11 300 ; 
       2 22 300 ; 
       3 23 300 ; 
       4 21 300 ; 
       5 20 300 ; 
       6 24 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       11 10 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       21 32 300 ; 
       22 4 300 ; 
       22 5 300 ; 
       23 6 300 ; 
       24 25 300 ; 
       25 36 300 ; 
       25 37 300 ; 
       26 38 300 ; 
       27 31 300 ; 
       28 30 300 ; 
       29 29 300 ; 
       30 28 300 ; 
       31 27 300 ; 
       32 26 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       11 11 401 ; 
       22 22 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       32 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 75 -4 0 MPRFLG 0 ; 
       4 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 70 -4 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 65 -4 0 MPRFLG 0 ; 
       11 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 80 -2 0 MPRFLG 0 ; 
       25 SCHEM 50 -6 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 84 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 81.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 84 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 81.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
