SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.28-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       biocruiser_cap102-mat100.2-0 ; 
       biocruiser_cap102-mat101.2-0 ; 
       biocruiser_cap102-mat102.2-0 ; 
       biocruiser_cap102-mat103.2-0 ; 
       biocruiser_cap102-mat104.1-0 ; 
       biocruiser_cap102-mat105.1-0 ; 
       biocruiser_cap102-mat106.1-0 ; 
       biocruiser_cap102-mat116.1-0 ; 
       biocruiser_cap102-mat117.1-0 ; 
       biocruiser_cap102-mat118.1-0 ; 
       biocruiser_cap102-mat119.1-0 ; 
       biocruiser_cap102-mat122.1-0 ; 
       biocruiser_cap102-mat123.1-0 ; 
       biocruiser_cap102-mat83.3-0 ; 
       biocruiser_cap102-mat84.3-0 ; 
       biocruiser_cap102-mat85.3-0 ; 
       biocruiser_cap102-mat86.3-0 ; 
       biocruiser_cap102-mat95.2-0 ; 
       biocruiser_cap102-mat97.2-0 ; 
       biocruiser_cap102-mat98.2-0 ; 
       biocruiser_cap102-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       cruiser-cyl13.20-0 ROOT ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-cyl24.1-0 ; 
       cruiser-cyl25.1-0 ; 
       cruiser-cyl26.1-0 ; 
       cruiser-cyl27.1-0 ; 
       cruiser-cyl28.1-0 ; 
       cruiser-cyl29.1-0 ; 
       cruiser-extru17.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9_1.2-0 ; 
       cruiser-extru9_4.1-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-null4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-biocruiser-cap102.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       biocruiser_cap102-t2d1.3-0 ; 
       biocruiser_cap102-t2d13.2-0 ; 
       biocruiser_cap102-t2d14.2-0 ; 
       biocruiser_cap102-t2d15.2-0 ; 
       biocruiser_cap102-t2d16.1-0 ; 
       biocruiser_cap102-t2d17.1-0 ; 
       biocruiser_cap102-t2d18.1-0 ; 
       biocruiser_cap102-t2d19.1-0 ; 
       biocruiser_cap102-t2d2.3-0 ; 
       biocruiser_cap102-t2d20.2-0 ; 
       biocruiser_cap102-t2d21.1-0 ; 
       biocruiser_cap102-t2d22.1-0 ; 
       biocruiser_cap102-t2d23.1-0 ; 
       biocruiser_cap102-t2d3.3-0 ; 
       biocruiser_cap102-t2d33.1-0 ; 
       biocruiser_cap102-t2d34.1-0 ; 
       biocruiser_cap102-t2d35.1-0 ; 
       biocruiser_cap102-t2d36.1-0 ; 
       biocruiser_cap102-t2d39.1-0 ; 
       biocruiser_cap102-t2d4.3-0 ; 
       biocruiser_cap102-t2d40.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 14 110 ; 
       12 14 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       10 14 110 ; 
       11 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 20 300 ; 
       2 3 300 ; 
       3 0 300 ; 
       4 1 300 ; 
       5 2 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       8 6 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       15 19 401 ; 
       16 13 401 ; 
       0 5 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       3 9 401 ; 
       4 10 401 ; 
       5 11 401 ; 
       13 8 401 ; 
       14 0 401 ; 
       6 12 401 ; 
       11 18 401 ; 
       12 20 401 ; 
       7 14 401 ; 
       8 15 401 ; 
       9 16 401 ; 
       10 17 401 ; 
       17 3 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       15 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
