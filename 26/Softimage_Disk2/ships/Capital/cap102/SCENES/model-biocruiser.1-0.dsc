SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.1-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       biocruiser-mat81.1-0 ; 
       biocruiser-mat82.1-0 ; 
       cruiser-mat71.1-0 ; 
       cruiser-mat75.1-0 ; 
       cruiser-mat77.1-0 ; 
       cruiser-mat78.1-0 ; 
       cruiser-mat80.1-0 ; 
       edit_nulls-mat70.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.1-0 ROOT ; 
       cruiser-cyl15.1-0 ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl17.1-0 ; 
       cruiser-cyl18.1-0 ; 
       cruiser-cyl19.1-0 ; 
       cruiser-cyl21.1-0 ; 
       cruiser-cyl22.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-extru1.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru8.1-0 ; 
       cruiser-extru9.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-lwepemt.1-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null2.1-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa11.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa14.1-0 ; 
       cruiser-octa15.1-0 ; 
       cruiser-octa16.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-rwepemt.5-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-biocruiser.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 11 110 ; 
       24 11 110 ; 
       22 10 110 ; 
       23 13 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       17 1 110 ; 
       2 20 110 ; 
       3 20 110 ; 
       4 20 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       7 20 110 ; 
       8 20 110 ; 
       9 20 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       0 1 110 ; 
       16 1 110 ; 
       18 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       39 1 110 ; 
       15 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       37 0 300 ; 
       38 1 300 ; 
       31 7 300 ; 
       32 2 300 ; 
       33 4 300 ; 
       34 3 300 ; 
       35 6 300 ; 
       36 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 64.65402 36.18902 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 64.65402 34.18902 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 65.90626 27.48934 0 MPRFLG 0 ; 
       24 SCHEM 68.40626 27.48934 0 MPRFLG 0 ; 
       22 SCHEM 63.40625 27.48934 0 MPRFLG 0 ; 
       23 SCHEM 75.90626 27.48934 0 MPRFLG 0 ; 
       25 SCHEM 70.90626 27.48934 0 MPRFLG 0 ; 
       26 SCHEM 73.40626 27.48934 0 MPRFLG 0 ; 
       37 SCHEM 87.05684 37.68092 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 87.0466 36.98018 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 76.3865 36.97858 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 79.65626 33.48935 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 88.40626 29.48934 0 MPRFLG 0 ; 
       3 SCHEM 78.40626 29.48934 0 MPRFLG 0 ; 
       4 SCHEM 80.90626 29.48934 0 MPRFLG 0 ; 
       5 SCHEM 83.40626 29.48934 0 MPRFLG 0 ; 
       6 SCHEM 85.90626 29.48934 0 MPRFLG 0 ; 
       7 SCHEM 90.90626 29.48934 0 MPRFLG 0 ; 
       8 SCHEM 93.40626 29.48934 0 MPRFLG 0 ; 
       9 SCHEM 95.90626 29.48934 0 MPRFLG 0 ; 
       10 SCHEM 63.40625 29.48934 0 MPRFLG 0 ; 
       11 SCHEM 67.15626 29.48934 0 MPRFLG 0 ; 
       12 SCHEM 72.15626 29.48934 0 MPRFLG 0 ; 
       13 SCHEM 75.90626 29.48934 0 MPRFLG 0 ; 
       14 SCHEM 67.95821 35.57555 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 69.65626 31.48934 0 MPRFLG 0 ; 
       20 SCHEM 87.15626 31.48934 0 MPRFLG 0 ; 
       27 SCHEM 70.41273 35.54606 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 70.49197 36.86184 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 70.44898 36.25385 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 69.13042 38.65475 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 68.01794 36.28013 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 75.35922 39.54839 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 74.28764 36.96886 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 79.93728 37.57021 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 79.97732 36.93062 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 82.25643 37.6239 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 82.14887 36.98431 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 84.63666 37.66589 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 84.60628 36.96796 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 69.07923 38.01507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.99197 36.86184 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 51.64573 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59.14573 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.64573 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 71.64574 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 74.14574 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49.14573 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 89.26138 5.661594 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 86.79176 6.359524 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
