SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.16-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       biocruiser-mat81.1-0 ; 
       biocruiser-mat82.1-0 ; 
       biocruiser-mat83.1-0 ; 
       biocruiser-mat84.1-0 ; 
       cruiser-mat71.1-0 ; 
       cruiser-mat75.1-0 ; 
       cruiser-mat77.1-0 ; 
       cruiser-mat78.1-0 ; 
       cruiser-mat80.1-0 ; 
       edit_nulls-mat70.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.13-0 ROOT ; 
       cruiser-cyl15.1-0 ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl17.1-0 ; 
       cruiser-cyl18.1-0 ; 
       cruiser-cyl19.1-0 ; 
       cruiser-cyl21.1-0 ; 
       cruiser-cyl22.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-extru1.1-0 ; 
       cruiser-extru10.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-lwepemt.1-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null2.1-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa11.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa14.1-0 ; 
       cruiser-octa17.1-0 ; 
       cruiser-octa18.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-rwepemt.5-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model_2-biocruiser.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       biocruiser-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 19 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       0 1 110 ; 
       2 20 110 ; 
       3 20 110 ; 
       4 20 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       7 20 110 ; 
       8 20 110 ; 
       9 20 110 ; 
       10 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 12 110 ; 
       22 10 110 ; 
       23 13 110 ; 
       24 12 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 2 300 ; 
       12 3 300 ; 
       31 9 300 ; 
       32 4 300 ; 
       33 6 300 ; 
       34 5 300 ; 
       35 8 300 ; 
       36 7 300 ; 
       37 0 300 ; 
       38 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 86.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 87.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 85 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 45 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 55 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 65 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 67.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 76.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 82.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 78.75 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 58.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 72.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 70 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 82.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 30 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 32.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 42.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 40 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 47.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 45 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 47.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 40 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
