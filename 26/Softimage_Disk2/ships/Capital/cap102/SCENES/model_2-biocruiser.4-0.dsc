SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.17-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       biocruiser-light1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       biocruiser-mat83.2-0 ; 
       biocruiser-mat84.2-0 ; 
       biocruiser-mat85.1-0 ; 
       biocruiser-mat86.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       cruiser-extru5.1-0 ROOT ; 
       cruiser-extru9.1-0 ROOT ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa14.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model_2-biocruiser.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       biocruiser-t2d1.2-0 ; 
       biocruiser-t2d2.1-0 ; 
       biocruiser-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       3 1 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 2 401 ; 
       0 1 401 ; 
       1 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 DISPLAY 0 0 SRT 0.7376601 1 1 1.509958e-007 0 0 0 6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 DISPLAY 1 2 SRT 0.3467986 1.056831 1.214943 0 -3.031593 0 -4.870955 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 17.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
