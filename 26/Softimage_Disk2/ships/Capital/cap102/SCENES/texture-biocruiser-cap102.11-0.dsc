SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.12-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       biocruiser_cap102-mat100.2-0 ; 
       biocruiser_cap102-mat101.2-0 ; 
       biocruiser_cap102-mat102.2-0 ; 
       biocruiser_cap102-mat103.2-0 ; 
       biocruiser_cap102-mat104.1-0 ; 
       biocruiser_cap102-mat105.1-0 ; 
       biocruiser_cap102-mat106.1-0 ; 
       biocruiser_cap102-mat107.1-0 ; 
       biocruiser_cap102-mat108.1-0 ; 
       biocruiser_cap102-mat109.1-0 ; 
       biocruiser_cap102-mat81.2-0 ; 
       biocruiser_cap102-mat82.2-0 ; 
       biocruiser_cap102-mat83.2-0 ; 
       biocruiser_cap102-mat84.2-0 ; 
       biocruiser_cap102-mat85.2-0 ; 
       biocruiser_cap102-mat86.2-0 ; 
       biocruiser_cap102-mat87.2-0 ; 
       biocruiser_cap102-mat88.2-0 ; 
       biocruiser_cap102-mat89.2-0 ; 
       biocruiser_cap102-mat90.2-0 ; 
       biocruiser_cap102-mat91.2-0 ; 
       biocruiser_cap102-mat95.2-0 ; 
       biocruiser_cap102-mat97.2-0 ; 
       biocruiser_cap102-mat98.2-0 ; 
       biocruiser_cap102-mat99.2-0 ; 
       cruiser-mat71.2-0 ; 
       cruiser-mat75.2-0 ; 
       cruiser-mat77.2-0 ; 
       cruiser-mat78.2-0 ; 
       cruiser-mat80.2-0 ; 
       edit_nulls-mat70.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.9-0 ROOT ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-cyl24.1-0 ; 
       cruiser-cyl25.1-0 ; 
       cruiser-cyl26.1-0 ; 
       cruiser-cyl27.1-0 ; 
       cruiser-cyl28.1-0 ; 
       cruiser-cyl29.1-0 ; 
       cruiser-extru10.1-0 ; 
       cruiser-extru12.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-lwepemt.1-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null2.1-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa14.1-0 ; 
       cruiser-octa17.1-0 ; 
       cruiser-octa18.1-0 ; 
       cruiser-octa20.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-rwepemt.5-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-biocruiser-cap102.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       biocruiser_cap102-t2d1.2-0 ; 
       biocruiser_cap102-t2d13.2-0 ; 
       biocruiser_cap102-t2d14.2-0 ; 
       biocruiser_cap102-t2d15.2-0 ; 
       biocruiser_cap102-t2d16.1-0 ; 
       biocruiser_cap102-t2d17.1-0 ; 
       biocruiser_cap102-t2d18.1-0 ; 
       biocruiser_cap102-t2d19.1-0 ; 
       biocruiser_cap102-t2d2.2-0 ; 
       biocruiser_cap102-t2d20.2-0 ; 
       biocruiser_cap102-t2d21.1-0 ; 
       biocruiser_cap102-t2d22.1-0 ; 
       biocruiser_cap102-t2d23.1-0 ; 
       biocruiser_cap102-t2d24.1-0 ; 
       biocruiser_cap102-t2d25.1-0 ; 
       biocruiser_cap102-t2d26.1-0 ; 
       biocruiser_cap102-t2d3.2-0 ; 
       biocruiser_cap102-t2d4.2-0 ; 
       biocruiser_cap102-t2d5.2-0 ; 
       biocruiser_cap102-t2d6.2-0 ; 
       biocruiser_cap102-t2d7.4-0 ; 
       biocruiser_cap102-t2d8.4-0 ; 
       biocruiser_cap102-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       11 19 110 ; 
       2 20 110 ; 
       26 11 110 ; 
       3 20 110 ; 
       4 20 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       10 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 12 110 ; 
       22 13 110 ; 
       23 12 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       7 20 110 ; 
       8 20 110 ; 
       9 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       2 24 300 ; 
       3 3 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       12 12 300 ; 
       12 13 300 ; 
       12 14 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       31 30 300 ; 
       32 25 300 ; 
       33 27 300 ; 
       34 26 300 ; 
       35 29 300 ; 
       36 28 300 ; 
       37 10 300 ; 
       38 11 300 ; 
       7 4 300 ; 
       8 5 300 ; 
       9 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       3 9 401 ; 
       12 0 401 ; 
       13 8 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 19 401 ; 
       18 20 401 ; 
       19 21 401 ; 
       20 22 401 ; 
       7 13 401 ; 
       8 14 401 ; 
       9 15 401 ; 
       21 3 401 ; 
       22 1 401 ; 
       23 2 401 ; 
       24 4 401 ; 
       4 10 401 ; 
       5 11 401 ; 
       6 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 100 -4 0 MPRFLG 0 ; 
       26 SCHEM 85 -6 0 MPRFLG 0 ; 
       3 SCHEM 105 -4 0 MPRFLG 0 ; 
       4 SCHEM 95 -4 0 MPRFLG 0 ; 
       5 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 MPRFLG 0 ; 
       13 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 103.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 50 -6 0 MPRFLG 0 ; 
       22 SCHEM 75 -6 0 MPRFLG 0 ; 
       23 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 65 -6 0 MPRFLG 0 ; 
       25 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 110 -4 0 MPRFLG 0 ; 
       9 SCHEM 112.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 120 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 115 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
