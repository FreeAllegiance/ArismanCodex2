SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.10-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       biocruiser_cap102-mat100.2-0 ; 
       biocruiser_cap102-mat101.2-0 ; 
       biocruiser_cap102-mat102.2-0 ; 
       biocruiser_cap102-mat103.2-0 ; 
       biocruiser_cap102-mat104.1-0 ; 
       biocruiser_cap102-mat105.1-0 ; 
       biocruiser_cap102-mat106.1-0 ; 
       biocruiser_cap102-mat81.2-0 ; 
       biocruiser_cap102-mat82.2-0 ; 
       biocruiser_cap102-mat83.2-0 ; 
       biocruiser_cap102-mat84.2-0 ; 
       biocruiser_cap102-mat85.2-0 ; 
       biocruiser_cap102-mat86.2-0 ; 
       biocruiser_cap102-mat87.2-0 ; 
       biocruiser_cap102-mat88.2-0 ; 
       biocruiser_cap102-mat89.2-0 ; 
       biocruiser_cap102-mat90.2-0 ; 
       biocruiser_cap102-mat91.2-0 ; 
       biocruiser_cap102-mat92.2-0 ; 
       biocruiser_cap102-mat93.2-0 ; 
       biocruiser_cap102-mat94.2-0 ; 
       biocruiser_cap102-mat95.2-0 ; 
       biocruiser_cap102-mat97.2-0 ; 
       biocruiser_cap102-mat98.2-0 ; 
       biocruiser_cap102-mat99.2-0 ; 
       cruiser-mat71.2-0 ; 
       cruiser-mat75.2-0 ; 
       cruiser-mat77.2-0 ; 
       cruiser-mat78.2-0 ; 
       cruiser-mat80.2-0 ; 
       edit_nulls-mat70.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.7-0 ROOT ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-cyl24.1-0 ; 
       cruiser-cyl25.1-0 ; 
       cruiser-cyl26.1-0 ; 
       cruiser-cyl27.1-0 ; 
       cruiser-cyl28.1-0 ; 
       cruiser-cyl29.1-0 ; 
       cruiser-extru10.1-0 ; 
       cruiser-extru11.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-lwepemt.1-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null2.1-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa14.1-0 ; 
       cruiser-octa17.1-0 ; 
       cruiser-octa18.1-0 ; 
       cruiser-octa19.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-rwepemt.5-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-biocruiser-cap102.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       biocruiser_cap102-t2d1.2-0 ; 
       biocruiser_cap102-t2d10.2-0 ; 
       biocruiser_cap102-t2d11.2-0 ; 
       biocruiser_cap102-t2d12.2-0 ; 
       biocruiser_cap102-t2d13.2-0 ; 
       biocruiser_cap102-t2d14.2-0 ; 
       biocruiser_cap102-t2d15.2-0 ; 
       biocruiser_cap102-t2d16.1-0 ; 
       biocruiser_cap102-t2d17.1-0 ; 
       biocruiser_cap102-t2d18.1-0 ; 
       biocruiser_cap102-t2d19.1-0 ; 
       biocruiser_cap102-t2d2.2-0 ; 
       biocruiser_cap102-t2d20.2-0 ; 
       biocruiser_cap102-t2d21.1-0 ; 
       biocruiser_cap102-t2d22.1-0 ; 
       biocruiser_cap102-t2d23.1-0 ; 
       biocruiser_cap102-t2d3.2-0 ; 
       biocruiser_cap102-t2d4.2-0 ; 
       biocruiser_cap102-t2d5.2-0 ; 
       biocruiser_cap102-t2d6.2-0 ; 
       biocruiser_cap102-t2d7.2-0 ; 
       biocruiser_cap102-t2d8.2-0 ; 
       biocruiser_cap102-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 20 110 ; 
       3 20 110 ; 
       4 20 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 12 110 ; 
       22 13 110 ; 
       23 12 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 11 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       7 20 110 ; 
       8 20 110 ; 
       9 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       2 24 300 ; 
       3 3 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       31 30 300 ; 
       32 25 300 ; 
       33 27 300 ; 
       34 26 300 ; 
       35 29 300 ; 
       36 28 300 ; 
       37 7 300 ; 
       38 8 300 ; 
       7 4 300 ; 
       8 5 300 ; 
       9 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 12 401 ; 
       9 0 401 ; 
       10 11 401 ; 
       11 16 401 ; 
       12 17 401 ; 
       13 18 401 ; 
       14 19 401 ; 
       15 20 401 ; 
       16 21 401 ; 
       17 22 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 6 401 ; 
       22 4 401 ; 
       23 5 401 ; 
       24 7 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 100.1349 -4 0 MPRFLG 0 ; 
       3 SCHEM 105.1349 -4 0 MPRFLG 0 ; 
       4 SCHEM 95.13492 -4 0 MPRFLG 0 ; 
       5 SCHEM 97.63492 -4 0 MPRFLG 0 ; 
       6 SCHEM 102.6349 -4 0 MPRFLG 0 ; 
       10 SCHEM 77.63492 -4 0 MPRFLG 0 ; 
       11 SCHEM 88.88492 -4 0 MPRFLG 0 ; 
       12 SCHEM 55.13492 -4 0 MPRFLG 0 ; 
       13 SCHEM 66.38492 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 71.38492 -2 0 MPRFLG 0 ; 
       20 SCHEM 103.8849 -2 0 MPRFLG 0 ; 
       21 SCHEM 50.13492 -6 0 MPRFLG 0 ; 
       22 SCHEM 62.63492 -6 0 MPRFLG 0 ; 
       23 SCHEM 52.63492 -6 0 MPRFLG 0 ; 
       24 SCHEM 75.13492 -6 0 MPRFLG 0 ; 
       25 SCHEM 72.63492 -6 0 MPRFLG 0 ; 
       26 SCHEM 85.13492 -6 0 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 30.13492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 32.63492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 37.63492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 35.13492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 42.63492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 40.13492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 47.63492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 45.13492 -2 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 107.6349 -4 0 MPRFLG 0 ; 
       8 SCHEM 110.1349 -4 0 MPRFLG 0 ; 
       9 SCHEM 112.6349 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 95.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 97.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 102.6349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 105.1349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.63492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45.13492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 57.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 80.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 67.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 70.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 87.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 90.13492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.63492 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 115.1349 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 117.6349 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 120.1349 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 100.1349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 32.63492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35.13492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.63492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 40.13492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.63492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 30.13492 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 107.6349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 110.1349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.6349 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 87.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 90.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 92.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 117.6349 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 120.1349 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 115.1349 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 100.1349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 95.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 102.6349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 105.1349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 77.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 82.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 67.63492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 70.13492 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 107.6349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 110.1349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 112.6349 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
