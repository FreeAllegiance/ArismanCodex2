SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cruiser-cam_int1.25-0 ROOT ; 
       cruiser-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       biocruiser-mat116.1-0 ; 
       biocruiser-mat117.2-0 ; 
       biocruiser-mat118.2-0 ; 
       biocruiser-mat119.2-0 ; 
       biocruiser-mat120.1-0 ; 
       biocruiser-mat121.1-0 ; 
       biocruiser-mat122.1-0 ; 
       biocruiser-mat123.1-0 ; 
       biocruiser-mat124.1-0 ; 
       biocruiser-mat125.1-0 ; 
       biocruiser-mat126.1-0 ; 
       biocruiser-mat127.1-0 ; 
       biocruiser-mat83.2-0 ; 
       biocruiser-mat84.2-0 ; 
       biocruiser-mat85.3-0 ; 
       biocruiser-mat86.3-0 ; 
       biocruiser_cap102-mat100.2-0 ; 
       biocruiser_cap102-mat101.2-0 ; 
       biocruiser_cap102-mat102.2-0 ; 
       biocruiser_cap102-mat103.2-0 ; 
       biocruiser_cap102-mat104.1-0 ; 
       biocruiser_cap102-mat105.1-0 ; 
       biocruiser_cap102-mat106.1-0 ; 
       biocruiser_cap102-mat81.2-0 ; 
       biocruiser_cap102-mat82.2-0 ; 
       biocruiser_cap102-mat95.2-0 ; 
       biocruiser_cap102-mat97.2-0 ; 
       biocruiser_cap102-mat98.2-0 ; 
       biocruiser_cap102-mat99.2-0 ; 
       cruiser-mat71.2-0 ; 
       cruiser-mat75.2-0 ; 
       cruiser-mat77.2-0 ; 
       cruiser-mat78.2-0 ; 
       cruiser-mat80.2-0 ; 
       edit_nulls-mat70.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.18-0 ROOT ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-cyl24.1-0 ; 
       cruiser-cyl25.1-0 ; 
       cruiser-cyl26.1-0 ; 
       cruiser-cyl27.1-0 ; 
       cruiser-cyl28.1-0 ; 
       cruiser-cyl29.1-0 ; 
       cruiser-extru17.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9_1.2-0 ; 
       cruiser-extru9_4.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-lwepemt.1-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-null4.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa31.1-0 ; 
       cruiser-octa32.1-0 ; 
       cruiser-octa33.1-0 ; 
       cruiser-octa35.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-rwepemt.5-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture_2-biocruiser.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       biocruiser-t2d1.3-0 ; 
       biocruiser-t2d2.2-0 ; 
       biocruiser-t2d3.4-0 ; 
       biocruiser-t2d33.2-0 ; 
       biocruiser-t2d34.2-0 ; 
       biocruiser-t2d35.2-0 ; 
       biocruiser-t2d36.2-0 ; 
       biocruiser-t2d37.1-0 ; 
       biocruiser-t2d38.1-0 ; 
       biocruiser-t2d39.1-0 ; 
       biocruiser-t2d4.3-0 ; 
       biocruiser-t2d40.1-0 ; 
       biocruiser-t2d41.1-0 ; 
       biocruiser-t2d42.1-0 ; 
       biocruiser-t2d43.1-0 ; 
       biocruiser-t2d44.1-0 ; 
       biocruiser_cap102-t2d13.2-0 ; 
       biocruiser_cap102-t2d14.2-0 ; 
       biocruiser_cap102-t2d15.2-0 ; 
       biocruiser_cap102-t2d16.1-0 ; 
       biocruiser_cap102-t2d17.1-0 ; 
       biocruiser_cap102-t2d18.1-0 ; 
       biocruiser_cap102-t2d19.1-0 ; 
       biocruiser_cap102-t2d20.2-0 ; 
       biocruiser_cap102-t2d21.1-0 ; 
       biocruiser_cap102-t2d22.1-0 ; 
       biocruiser_cap102-t2d23.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 19 110 ; 
       3 19 110 ; 
       4 19 110 ; 
       5 19 110 ; 
       6 19 110 ; 
       7 19 110 ; 
       8 19 110 ; 
       9 19 110 ; 
       23 11 110 ; 
       10 20 110 ; 
       13 20 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       26 13 110 ; 
       19 1 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       20 1 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       21 11 110 ; 
       22 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       2 28 300 ; 
       3 19 300 ; 
       4 16 300 ; 
       5 17 300 ; 
       6 18 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 22 300 ; 
       23 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       26 11 300 ; 
       24 8 300 ; 
       25 9 300 ; 
       31 34 300 ; 
       32 29 300 ; 
       33 31 300 ; 
       34 30 300 ; 
       35 33 300 ; 
       36 32 300 ; 
       37 23 300 ; 
       38 24 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 0 300 ; 
       21 4 300 ; 
       22 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       14 10 401 ; 
       15 2 401 ; 
       16 20 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       12 1 401 ; 
       13 0 401 ; 
       22 26 401 ; 
       6 9 401 ; 
       7 11 401 ; 
       0 3 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       8 12 401 ; 
       9 13 401 ; 
       10 14 401 ; 
       11 15 401 ; 
       25 18 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 58.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 95 -4 0 MPRFLG 0 ; 
       3 SCHEM 100 -4 0 MPRFLG 0 ; 
       4 SCHEM 90 -4 0 MPRFLG 0 ; 
       5 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 105 -4 0 MPRFLG 0 ; 
       9 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 MPRFLG 0 ; 
       10 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 98.75 -2 0 MPRFLG 0 ; 
       24 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 75 -6 0 MPRFLG 0 ; 
       27 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 87.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 85 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       14 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 110 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 110 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 112.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 115 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
