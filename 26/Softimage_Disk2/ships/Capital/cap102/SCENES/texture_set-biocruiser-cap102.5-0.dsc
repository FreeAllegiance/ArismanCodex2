SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       biocruiser_cap102-mat100.3-0 ; 
       biocruiser_cap102-mat101.3-0 ; 
       biocruiser_cap102-mat102.3-0 ; 
       biocruiser_cap102-mat103.3-0 ; 
       biocruiser_cap102-mat104.2-0 ; 
       biocruiser_cap102-mat105.2-0 ; 
       biocruiser_cap102-mat106.2-0 ; 
       biocruiser_cap102-mat116.2-0 ; 
       biocruiser_cap102-mat117.2-0 ; 
       biocruiser_cap102-mat118.2-0 ; 
       biocruiser_cap102-mat119.2-0 ; 
       biocruiser_cap102-mat120.2-0 ; 
       biocruiser_cap102-mat121.2-0 ; 
       biocruiser_cap102-mat122.2-0 ; 
       biocruiser_cap102-mat123.2-0 ; 
       biocruiser_cap102-mat124.2-0 ; 
       biocruiser_cap102-mat125.2-0 ; 
       biocruiser_cap102-mat126.2-0 ; 
       biocruiser_cap102-mat127.2-0 ; 
       biocruiser_cap102-mat81.3-0 ; 
       biocruiser_cap102-mat82.3-0 ; 
       biocruiser_cap102-mat83.4-0 ; 
       biocruiser_cap102-mat84.4-0 ; 
       biocruiser_cap102-mat85.4-0 ; 
       biocruiser_cap102-mat86.4-0 ; 
       biocruiser_cap102-mat95.3-0 ; 
       biocruiser_cap102-mat97.3-0 ; 
       biocruiser_cap102-mat98.3-0 ; 
       biocruiser_cap102-mat99.3-0 ; 
       cruiser-mat71.3-0 ; 
       cruiser-mat75.3-0 ; 
       cruiser-mat77.3-0 ; 
       cruiser-mat78.3-0 ; 
       cruiser-mat80.3-0 ; 
       edit_nulls-mat70.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       cruiser-cockpt.5-0 ; 
       cruiser-cyl13.22-0 ROOT ; 
       cruiser-cyl16.1-0 ; 
       cruiser-cyl23.1-0 ; 
       cruiser-cyl24.1-0 ; 
       cruiser-cyl25.1-0 ; 
       cruiser-cyl26.1-0 ; 
       cruiser-cyl27.1-0 ; 
       cruiser-cyl28.1-0 ; 
       cruiser-cyl29.1-0 ; 
       cruiser-extru17.1-0 ; 
       cruiser-extru5.1-0 ; 
       cruiser-extru9_1.2-0 ; 
       cruiser-extru9_4.1-0 ; 
       cruiser-lbthrust.2-0 ; 
       cruiser-lsmoke.2-0 ; 
       cruiser-ltthrust.2-0 ; 
       cruiser-missemt.5-0 ; 
       cruiser-null3.1-0 ; 
       cruiser-null4.1-0 ; 
       cruiser-octa10.1-0 ; 
       cruiser-octa13.1-0 ; 
       cruiser-octa31.1-0 ; 
       cruiser-octa32.1-0 ; 
       cruiser-octa33.1-0 ; 
       cruiser-octa35.1-0 ; 
       cruiser-rbthrust.2-0 ; 
       cruiser-rsmoke.2-0 ; 
       cruiser-rtthrust.2-0 ; 
       cruiser-SS01.5-0 ; 
       cruiser-SS02.5-0 ; 
       cruiser-SS03.5-0 ; 
       cruiser-SS04.5-0 ; 
       cruiser-SS05.5-0 ; 
       cruiser-SS06.5-0 ; 
       cruiser-SS07.1-0 ; 
       cruiser-SS08.1-0 ; 
       cruiser-trail.5-0 ; 
       cruiser-turwepemt1.1-0 ; 
       cruiser-turwepemt2.1-0 ; 
       cruiser-turwepemt3.1-0 ; 
       cruiser-turwepemt4.1-0 ; 
       cruiser-wepemt.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap102/PICTURES/cap102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture_set-biocruiser-cap102.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       biocruiser_cap102-t2d1.5-0 ; 
       biocruiser_cap102-t2d13.4-0 ; 
       biocruiser_cap102-t2d14.4-0 ; 
       biocruiser_cap102-t2d15.4-0 ; 
       biocruiser_cap102-t2d16.3-0 ; 
       biocruiser_cap102-t2d17.3-0 ; 
       biocruiser_cap102-t2d18.3-0 ; 
       biocruiser_cap102-t2d19.3-0 ; 
       biocruiser_cap102-t2d2.5-0 ; 
       biocruiser_cap102-t2d20.4-0 ; 
       biocruiser_cap102-t2d21.3-0 ; 
       biocruiser_cap102-t2d22.3-0 ; 
       biocruiser_cap102-t2d23.3-0 ; 
       biocruiser_cap102-t2d3.5-0 ; 
       biocruiser_cap102-t2d33.3-0 ; 
       biocruiser_cap102-t2d34.3-0 ; 
       biocruiser_cap102-t2d35.3-0 ; 
       biocruiser_cap102-t2d36.3-0 ; 
       biocruiser_cap102-t2d37.3-0 ; 
       biocruiser_cap102-t2d38.3-0 ; 
       biocruiser_cap102-t2d39.3-0 ; 
       biocruiser_cap102-t2d4.5-0 ; 
       biocruiser_cap102-t2d40.3-0 ; 
       biocruiser_cap102-t2d41.3-0 ; 
       biocruiser_cap102-t2d42.3-0 ; 
       biocruiser_cap102-t2d43.3-0 ; 
       biocruiser_cap102-t2d44.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 18 110 ; 
       3 18 110 ; 
       4 18 110 ; 
       5 18 110 ; 
       6 18 110 ; 
       7 18 110 ; 
       8 18 110 ; 
       9 18 110 ; 
       10 19 110 ; 
       11 19 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 11 110 ; 
       21 12 110 ; 
       22 11 110 ; 
       23 10 110 ; 
       24 10 110 ; 
       25 13 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       40 1 110 ; 
       41 1 110 ; 
       42 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       2 28 300 ; 
       3 3 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       7 4 300 ; 
       8 5 300 ; 
       9 6 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       20 11 300 ; 
       21 17 300 ; 
       22 12 300 ; 
       23 15 300 ; 
       24 16 300 ; 
       25 18 300 ; 
       29 34 300 ; 
       30 29 300 ; 
       31 31 300 ; 
       32 30 300 ; 
       33 33 300 ; 
       34 32 300 ; 
       35 19 300 ; 
       36 20 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       3 9 401 ; 
       4 10 401 ; 
       5 11 401 ; 
       6 12 401 ; 
       7 14 401 ; 
       8 15 401 ; 
       9 16 401 ; 
       10 17 401 ; 
       11 18 401 ; 
       12 19 401 ; 
       13 20 401 ; 
       14 22 401 ; 
       15 23 401 ; 
       16 24 401 ; 
       17 25 401 ; 
       18 26 401 ; 
       21 8 401 ; 
       22 0 401 ; 
       23 21 401 ; 
       24 13 401 ; 
       25 3 401 ; 
       26 1 401 ; 
       27 2 401 ; 
       28 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 62.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 100 -4 0 MPRFLG 0 ; 
       6 SCHEM 105 -4 0 MPRFLG 0 ; 
       7 SCHEM 110 -4 0 MPRFLG 0 ; 
       8 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 115 -4 0 MPRFLG 0 ; 
       10 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 63.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 106.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       20 SCHEM 60 -6 0 MPRFLG 0 ; 
       21 SCHEM 50 -6 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 80 -6 0 MPRFLG 0 ; 
       24 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 MPRFLG 0 ; 
       26 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 90 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 95 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 92.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 57.39288 4.664857 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 59.86837 4.827802 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.85897 4.691188 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 65.49839 4.714672 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 122.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 120 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 122.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
