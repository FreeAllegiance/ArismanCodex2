SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.28-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       apc-mat71.3-0 ; 
       apc-mat75.3-0 ; 
       apc-mat77.3-0 ; 
       apc-mat78.3-0 ; 
       apc-mat80.3-0 ; 
       edit_nulls-mat70.3-0 ; 
       support_final-mat100.1-0 ; 
       support_final-mat100_1.1-0 ; 
       support_final-mat101.1-0 ; 
       support_final-mat101_1.1-0 ; 
       support_final-mat102.1-0 ; 
       support_final-mat102_1.1-0 ; 
       support_final-mat103.1-0 ; 
       support_final-mat103_1.1-0 ; 
       support_final-mat104.3-0 ; 
       support_final-mat104_1.1-0 ; 
       support_final-mat105.1-0 ; 
       support_final-mat105_1.1-0 ; 
       support_final-mat81.3-0 ; 
       support_final-mat81_1.1-0 ; 
       support_final-mat82.3-0 ; 
       support_final-mat82_1.1-0 ; 
       support_final-mat83.2-0 ; 
       support_final-mat83_1.1-0 ; 
       support_final-mat84.1-0 ; 
       support_final-mat84_1.1-0 ; 
       support_final-mat85.1-0 ; 
       support_final-mat85_1.1-0 ; 
       support_final-mat86.1-0 ; 
       support_final-mat86_1.1-0 ; 
       support_final-mat87.2-0 ; 
       support_final-mat87_1.1-0 ; 
       support_final-mat88.3-0 ; 
       support_final-mat88_1.1-0 ; 
       support_final-mat90.1-0 ; 
       support_final-mat90_1.1-0 ; 
       support_final-mat91.2-0 ; 
       support_final-mat91_1.1-0 ; 
       support_final-mat93.1-0 ; 
       support_final-mat93_1.1-0 ; 
       support_final-mat94.1-0 ; 
       support_final-mat94_1.1-0 ; 
       support_final-mat95.1-0 ; 
       support_final-mat95_1.1-0 ; 
       support_final-mat96.1-0 ; 
       support_final-mat96_1.1-0 ; 
       support_final-mat97.1-0 ; 
       support_final-mat97_1.1-0 ; 
       support_final-mat98.1-0 ; 
       support_final-mat98_1.1-0 ; 
       support_final-mat99.1-0 ; 
       support_final-mat99_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       TEST1-blthrust.2-0 ; 
       TEST1-brthrust.2-0 ; 
       TEST1-canopy_1_1_1.22-0 ROOT ; 
       TEST1-canopy_1_1_1_1.1-0 ROOT ; 
       TEST1-cockpt.2-0 ; 
       TEST1-cube3.1-0 ; 
       TEST1-cube3_1.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl1_1.1-0 ; 
       TEST1-cyl3.1-0 ; 
       TEST1-cyl3_1.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-engine-nib_1.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-extru2_1_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine4_1.1-0 ; 
       TEST1-l-t-engine7.1-0 ; 
       TEST1-l-t-engine7_1.1-0 ; 
       TEST1-lsmoke.2-0 ; 
       TEST1-lwepemt.2-0 ; 
       TEST1-missemt.2-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-null1_1.1-0 ; 
       TEST1-rsmoke.2-0 ; 
       TEST1-rt-wing4.1-0 ; 
       TEST1-rt-wing4_1.1-0 ; 
       TEST1-rt-wing7.1-0 ; 
       TEST1-rt-wing7_1.1-0 ; 
       TEST1-rwepemt.2-0 ; 
       TEST1-SS01.2-0 ; 
       TEST1-SS02.2-0 ; 
       TEST1-SS03.2-0 ; 
       TEST1-SS04.2-0 ; 
       TEST1-SS05.2-0 ; 
       TEST1-SS06.2-0 ; 
       TEST1-tlthrust.2-0 ; 
       TEST1-trail.2-0 ; 
       TEST1-trthrust.2-0 ; 
       TEST1-zz_base_1.9-0 ; 
       TEST1-zz_base_1_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap103/PICTURES/cap103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-support_final.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       support_final-t2d1.5-0 ; 
       support_final-t2d1_1.1-0 ; 
       support_final-t2d10.1-0 ; 
       support_final-t2d10_1.1-0 ; 
       support_final-t2d11.1-0 ; 
       support_final-t2d11_1.1-0 ; 
       support_final-t2d12.3-0 ; 
       support_final-t2d12_1.1-0 ; 
       support_final-t2d13.3-0 ; 
       support_final-t2d13_1.1-0 ; 
       support_final-t2d14.1-0 ; 
       support_final-t2d14_1.1-0 ; 
       support_final-t2d15.1-0 ; 
       support_final-t2d15_1.1-0 ; 
       support_final-t2d16.1-0 ; 
       support_final-t2d16_1.1-0 ; 
       support_final-t2d17.1-0 ; 
       support_final-t2d17_1.1-0 ; 
       support_final-t2d18.1-0 ; 
       support_final-t2d18_1.1-0 ; 
       support_final-t2d19.1-0 ; 
       support_final-t2d19_1.1-0 ; 
       support_final-t2d2.4-0 ; 
       support_final-t2d2_1.1-0 ; 
       support_final-t2d20.1-0 ; 
       support_final-t2d20_1.1-0 ; 
       support_final-t2d21.1-0 ; 
       support_final-t2d21_1.1-0 ; 
       support_final-t2d22.1-0 ; 
       support_final-t2d22_1.1-0 ; 
       support_final-t2d23.1-0 ; 
       support_final-t2d23_1.1-0 ; 
       support_final-t2d24.1-0 ; 
       support_final-t2d24_1.1-0 ; 
       support_final-t2d3.4-0 ; 
       support_final-t2d3_1.1-0 ; 
       support_final-t2d5.4-0 ; 
       support_final-t2d5_1.1-0 ; 
       support_final-t2d6.4-0 ; 
       support_final-t2d6_1.1-0 ; 
       support_final-t2d7.3-0 ; 
       support_final-t2d7_1.1-0 ; 
       support_final-t2d8.1-0 ; 
       support_final-t2d8_1.1-0 ; 
       support_final-t2d9.1-0 ; 
       support_final-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       6 3 110 ; 
       4 2 110 ; 
       8 23 110 ; 
       5 2 110 ; 
       7 22 110 ; 
       10 23 110 ; 
       9 22 110 ; 
       12 3 110 ; 
       11 2 110 ; 
       14 3 110 ; 
       13 2 110 ; 
       16 23 110 ; 
       15 22 110 ; 
       18 23 110 ; 
       17 22 110 ; 
       23 3 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       27 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       26 3 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       35 2 110 ; 
       36 2 110 ; 
       37 2 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       28 3 110 ; 
       40 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 19 300 ; 
       3 21 300 ; 
       3 31 300 ; 
       2 18 300 ; 
       2 20 300 ; 
       2 30 300 ; 
       6 15 300 ; 
       8 7 300 ; 
       8 9 300 ; 
       5 14 300 ; 
       7 6 300 ; 
       7 8 300 ; 
       10 11 300 ; 
       10 13 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       12 37 300 ; 
       12 17 300 ; 
       11 36 300 ; 
       11 16 300 ; 
       14 33 300 ; 
       13 32 300 ; 
       16 45 300 ; 
       16 47 300 ; 
       15 44 300 ; 
       15 46 300 ; 
       18 49 300 ; 
       18 51 300 ; 
       17 48 300 ; 
       17 50 300 ; 
       25 34 300 ; 
       25 38 300 ; 
       27 40 300 ; 
       27 42 300 ; 
       30 5 300 ; 
       31 0 300 ; 
       32 2 300 ; 
       26 35 300 ; 
       26 39 300 ; 
       33 1 300 ; 
       34 4 300 ; 
       35 3 300 ; 
       39 22 300 ; 
       39 24 300 ; 
       39 26 300 ; 
       39 28 300 ; 
       28 41 300 ; 
       28 43 300 ; 
       40 23 300 ; 
       40 25 300 ; 
       40 27 300 ; 
       40 29 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 18 401 ; 
       8 20 401 ; 
       10 24 401 ; 
       12 26 401 ; 
       14 32 401 ; 
       16 28 401 ; 
       18 40 401 ; 
       20 0 401 ; 
       22 36 401 ; 
       24 22 401 ; 
       26 34 401 ; 
       28 8 401 ; 
       30 38 401 ; 
       32 6 401 ; 
       34 44 401 ; 
       36 30 401 ; 
       38 42 401 ; 
       40 2 401 ; 
       42 4 401 ; 
       44 10 401 ; 
       46 12 401 ; 
       48 14 401 ; 
       50 16 401 ; 
       7 19 401 ; 
       9 21 401 ; 
       11 25 401 ; 
       13 27 401 ; 
       15 33 401 ; 
       17 29 401 ; 
       19 41 401 ; 
       21 1 401 ; 
       23 37 401 ; 
       25 23 401 ; 
       27 35 401 ; 
       29 9 401 ; 
       31 39 401 ; 
       33 7 401 ; 
       35 45 401 ; 
       37 31 401 ; 
       39 43 401 ; 
       41 3 401 ; 
       43 5 401 ; 
       45 11 401 ; 
       47 13 401 ; 
       49 15 401 ; 
       51 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 108.8918 3.84819 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.61649 5.497414 0 USR DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 119.9585 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 102 -4 0 MPRFLG 0 ; 
       5 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 54.54145 -4 0 MPRFLG 0 ; 
       10 SCHEM 104.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 59.54145 -4 0 MPRFLG 0 ; 
       12 SCHEM 114.9585 -2 0 MPRFLG 0 ; 
       11 SCHEM 78.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 107.4585 -2 0 MPRFLG 0 ; 
       13 SCHEM 65 -2 0 MPRFLG 0 ; 
       16 SCHEM 99.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 97 -4 0 USR MPRFLG 0 ; 
       17 SCHEM 49.54145 -4 0 USR MPRFLG 0 ; 
       23 SCHEM 101.2085 -2 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       27 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 109.9585 -2 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       28 SCHEM 112.4585 -2 0 MPRFLG 0 ; 
       40 SCHEM 117.4585 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 53.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 58.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 48.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 50.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 103.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 103.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 118.9585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 113.9585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 121.4585 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 121.4585 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 116.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 116.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 116.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 116.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 121.4585 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 106.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 108.9585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 113.9585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 108.9585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 111.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 111.4585 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 96 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 48.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 50.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 53.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 55.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 58.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 60.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 121.4585 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 111.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 111.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 106.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 116.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 96 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 116.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 113.9585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 113.9585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 118.9585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 116.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 116.4585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 121.4585 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 121.4585 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 108.9585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 108.9585 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
