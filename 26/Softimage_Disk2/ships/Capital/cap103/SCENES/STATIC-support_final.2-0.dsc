SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.27-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       support_final-mat100.1-0 ; 
       support_final-mat101.1-0 ; 
       support_final-mat102.1-0 ; 
       support_final-mat103.1-0 ; 
       support_final-mat104.3-0 ; 
       support_final-mat105.1-0 ; 
       support_final-mat81.3-0 ; 
       support_final-mat82.3-0 ; 
       support_final-mat83.2-0 ; 
       support_final-mat84.1-0 ; 
       support_final-mat85.1-0 ; 
       support_final-mat86.1-0 ; 
       support_final-mat87.2-0 ; 
       support_final-mat88.3-0 ; 
       support_final-mat90.1-0 ; 
       support_final-mat91.2-0 ; 
       support_final-mat93.1-0 ; 
       support_final-mat94.1-0 ; 
       support_final-mat95.1-0 ; 
       support_final-mat96.1-0 ; 
       support_final-mat97.1-0 ; 
       support_final-mat98.1-0 ; 
       support_final-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       TEST1-canopy_1_1_1.21-0 ROOT ; 
       TEST1-cube3.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl3.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine7.1-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-rt-wing4.1-0 ; 
       TEST1-rt-wing7.1-0 ; 
       TEST1-zz_base_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap103/PICTURES/cap103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-support_final.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       support_final-t2d1.4-0 ; 
       support_final-t2d10.1-0 ; 
       support_final-t2d11.1-0 ; 
       support_final-t2d12.3-0 ; 
       support_final-t2d13.3-0 ; 
       support_final-t2d14.1-0 ; 
       support_final-t2d15.1-0 ; 
       support_final-t2d16.1-0 ; 
       support_final-t2d17.1-0 ; 
       support_final-t2d18.1-0 ; 
       support_final-t2d19.1-0 ; 
       support_final-t2d2.4-0 ; 
       support_final-t2d20.1-0 ; 
       support_final-t2d21.1-0 ; 
       support_final-t2d22.1-0 ; 
       support_final-t2d23.1-0 ; 
       support_final-t2d24.1-0 ; 
       support_final-t2d3.4-0 ; 
       support_final-t2d5.4-0 ; 
       support_final-t2d6.3-0 ; 
       support_final-t2d7.2-0 ; 
       support_final-t2d8.1-0 ; 
       support_final-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       0 7 300 ; 
       0 12 300 ; 
       1 4 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       4 15 300 ; 
       4 5 300 ; 
       5 13 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       7 21 300 ; 
       7 22 300 ; 
       9 14 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 10 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 16 401 ; 
       5 14 401 ; 
       6 20 401 ; 
       7 0 401 ; 
       8 18 401 ; 
       9 11 401 ; 
       10 17 401 ; 
       11 4 401 ; 
       12 19 401 ; 
       13 3 401 ; 
       14 22 401 ; 
       15 15 401 ; 
       16 21 401 ; 
       17 1 401 ; 
       18 2 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 14.20855 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.5966939 MPRFLG 0 ; 
       1 SCHEM 25.45855 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 20.45855 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.95855 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 USR MPRFLG 0 ; 
       8 SCHEM 6.708549 -2 0 MPRFLG 0 ; 
       9 SCHEM 15.45855 -2 0 MPRFLG 0 ; 
       10 SCHEM 17.95855 -2 0 MPRFLG 0 ; 
       11 SCHEM 22.95855 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.45855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19.45855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.95855 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.95855 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.95855 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14.45855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19.45855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14.45855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.95855 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.95855 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19.45855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19.45855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24.45855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.95855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.95855 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.95855 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14.45855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14.45855 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
