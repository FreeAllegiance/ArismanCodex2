SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.25-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       apc-mat71.3-0 ; 
       apc-mat75.3-0 ; 
       apc-mat77.3-0 ; 
       apc-mat78.3-0 ; 
       apc-mat80.3-0 ; 
       edit_nulls-mat70.3-0 ; 
       support_final-mat100.1-0 ; 
       support_final-mat101.1-0 ; 
       support_final-mat102.1-0 ; 
       support_final-mat103.1-0 ; 
       support_final-mat104.3-0 ; 
       support_final-mat105.1-0 ; 
       support_final-mat81.3-0 ; 
       support_final-mat82.3-0 ; 
       support_final-mat83.2-0 ; 
       support_final-mat84.1-0 ; 
       support_final-mat85.1-0 ; 
       support_final-mat86.1-0 ; 
       support_final-mat87.2-0 ; 
       support_final-mat88.3-0 ; 
       support_final-mat90.1-0 ; 
       support_final-mat91.2-0 ; 
       support_final-mat93.1-0 ; 
       support_final-mat94.1-0 ; 
       support_final-mat95.1-0 ; 
       support_final-mat96.1-0 ; 
       support_final-mat97.1-0 ; 
       support_final-mat98.1-0 ; 
       support_final-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       TEST1-blthrust.2-0 ; 
       TEST1-brthrust.2-0 ; 
       TEST1-canopy_1_1_1.20-0 ROOT ; 
       TEST1-cockpt.2-0 ; 
       TEST1-cube3.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl3.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine7.1-0 ; 
       TEST1-lsmoke.2-0 ; 
       TEST1-lwepemt.2-0 ; 
       TEST1-missemt.2-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-rsmoke.2-0 ; 
       TEST1-rt-wing4.1-0 ; 
       TEST1-rt-wing7.1-0 ; 
       TEST1-rwepemt.2-0 ; 
       TEST1-SS01.2-0 ; 
       TEST1-SS02.2-0 ; 
       TEST1-SS03.2-0 ; 
       TEST1-SS04.2-0 ; 
       TEST1-SS05.2-0 ; 
       TEST1-SS06.2-0 ; 
       TEST1-tlthrust.2-0 ; 
       TEST1-trail.2-0 ; 
       TEST1-trthrust.2-0 ; 
       TEST1-zz_base_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap103/PICTURES/cap103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-support_final.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       support_final-t2d1.4-0 ; 
       support_final-t2d10.1-0 ; 
       support_final-t2d11.1-0 ; 
       support_final-t2d12.3-0 ; 
       support_final-t2d13.3-0 ; 
       support_final-t2d14.1-0 ; 
       support_final-t2d15.1-0 ; 
       support_final-t2d16.1-0 ; 
       support_final-t2d17.1-0 ; 
       support_final-t2d18.1-0 ; 
       support_final-t2d19.1-0 ; 
       support_final-t2d2.4-0 ; 
       support_final-t2d20.1-0 ; 
       support_final-t2d21.1-0 ; 
       support_final-t2d22.1-0 ; 
       support_final-t2d23.1-0 ; 
       support_final-t2d24.1-0 ; 
       support_final-t2d3.4-0 ; 
       support_final-t2d5.4-0 ; 
       support_final-t2d6.3-0 ; 
       support_final-t2d7.2-0 ; 
       support_final-t2d8.1-0 ; 
       support_final-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 12 300 ; 
       2 13 300 ; 
       2 18 300 ; 
       4 10 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       7 21 300 ; 
       7 11 300 ; 
       8 19 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       10 27 300 ; 
       10 28 300 ; 
       16 20 300 ; 
       16 22 300 ; 
       17 23 300 ; 
       17 24 300 ; 
       19 5 300 ; 
       20 0 300 ; 
       21 2 300 ; 
       22 1 300 ; 
       23 4 300 ; 
       24 3 300 ; 
       28 14 300 ; 
       28 15 300 ; 
       28 16 300 ; 
       28 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 9 401 ; 
       7 10 401 ; 
       8 12 401 ; 
       9 13 401 ; 
       10 16 401 ; 
       11 14 401 ; 
       12 20 401 ; 
       13 0 401 ; 
       14 18 401 ; 
       15 11 401 ; 
       16 17 401 ; 
       17 4 401 ; 
       18 19 401 ; 
       19 3 401 ; 
       20 22 401 ; 
       21 15 401 ; 
       22 21 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 5 401 ; 
       26 6 401 ; 
       27 7 401 ; 
       28 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.25 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.5966939 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 54.54145 -4 0 MPRFLG 0 ; 
       6 SCHEM 59.54145 -4 0 MPRFLG 0 ; 
       7 SCHEM 78.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 65 -2 0 MPRFLG 0 ; 
       9 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 49.54145 -4 0 USR MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 86.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 53.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 55.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 58.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 48.29145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 50.79145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 48.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 53.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 55.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 58.29145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60.79145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
