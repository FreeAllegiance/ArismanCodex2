SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.3-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       apc-mat71.1-0 ; 
       apc-mat75.1-0 ; 
       apc-mat77.1-0 ; 
       apc-mat78.1-0 ; 
       apc-mat80.1-0 ; 
       edit_nulls-mat70.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       TEST1-blthrust.2-0 ; 
       TEST1-brthrust.2-0 ; 
       TEST1-canopy_1_1.3-0 ROOT ; 
       TEST1-cockpt.2-0 ; 
       TEST1-cube3.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl2.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine6.1-0 ; 
       TEST1-lsmoke.2-0 ; 
       TEST1-lwepemt.2-0 ; 
       TEST1-missemt.2-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-rsmoke.2-0 ; 
       TEST1-rt-wing4.1-0 ; 
       TEST1-rt-wing8.1-0 ; 
       TEST1-rwepemt.2-0 ; 
       TEST1-SS01.2-0 ; 
       TEST1-SS02.2-0 ; 
       TEST1-SS03.2-0 ; 
       TEST1-SS04.2-0 ; 
       TEST1-SS05.2-0 ; 
       TEST1-SS06.2-0 ; 
       TEST1-tetra1.1-0 ; 
       TEST1-tetra3.1-0 ; 
       TEST1-tlthrust.2-0 ; 
       TEST1-trail.2-0 ; 
       TEST1-trthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bio_support.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       25 2 110 ; 
       17 2 110 ; 
       26 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       19 5 300 ; 
       20 0 300 ; 
       21 2 300 ; 
       22 1 300 ; 
       23 4 300 ; 
       24 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.949178 -14.50516 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.55741 -14.54123 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.9859 0.5503789 0 USR SRT 1 1 1 0 0 0 0 0.3104329 0.5966939 MPRFLG 0 ; 
       3 SCHEM 6.353393 -13.76261 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.48589 -1.449623 0 MPRFLG 0 ; 
       5 SCHEM 17.9859 -3.449623 0 MPRFLG 0 ; 
       6 SCHEM 20.4859 -3.449623 0 MPRFLG 0 ; 
       25 SCHEM 25.4859 -1.449623 0 MPRFLG 0 ; 
       17 SCHEM 27.9859 -1.449623 0 MPRFLG 0 ; 
       26 SCHEM 30.4859 -1.449623 0 MPRFLG 0 ; 
       7 SCHEM 7.985893 -1.449623 0 MPRFLG 0 ; 
       8 SCHEM 5.485883 -1.449623 0 MPRFLG 0 ; 
       9 SCHEM 15.4859 -3.449623 0 MPRFLG 0 ; 
       10 SCHEM 12.9859 -3.449623 0 MPRFLG 0 ; 
       11 SCHEM 9.013837 -12.88182 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 0.775869 -13.85571 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.833773 -13.19791 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.7359 -1.449623 0 MPRFLG 0 ; 
       15 SCHEM 11.54991 -12.84575 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.9859 -1.449623 0 MPRFLG 0 ; 
       18 SCHEM 2.898504 -13.85432 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 13.94975 -12.86331 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 13.96704 -13.57654 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 16.20172 -12.84234 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 16.21901 -13.6185 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 18.49194 -12.80038 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 18.55858 -13.6185 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 8.938298 -13.78367 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10.26553 -11.7996 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.56081 -13.71152 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 89.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 91.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 96.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 94.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 86.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
