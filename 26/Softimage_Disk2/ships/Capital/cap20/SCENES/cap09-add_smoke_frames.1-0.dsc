SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_frames-body.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.125-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       add_frames-body_front1.1-0 ; 
       add_frames-body_side.1-0 ; 
       add_frames-cockpit1.1-0 ; 
       add_frames-engines1.1-0 ; 
       add_frames-engines2.1-0 ; 
       add_frames-gun_middle.1-0 ; 
       add_frames-gun_middle1.1-0 ; 
       add_frames-gun_side.1-0 ; 
       add_frames-gun_side1.1-0 ; 
       add_frames-head.1-0 ; 
       add_frames-holes1.1-0 ; 
       add_frames-mat101.1-0 ; 
       add_frames-mat105.1-0 ; 
       add_frames-mat106.1-0 ; 
       add_frames-mat107.1-0 ; 
       add_frames-mat108.1-0 ; 
       add_frames-mat96.1-0 ; 
       add_frames-mat99.1-0 ; 
       add_frames-neck.1-0 ; 
       add_frames-top_of_head1.1-0 ; 
       add_frames-vents1.1-0 ; 
       add_smoke_frames-nose_white-center.1-1.1-0 ; 
       add_smoke_frames-port_red-left.1-1.1-0 ; 
       add_smoke_frames-starbord_green-right.1-1.1-0 ; 
       add_smoke_frames-starbord_green-right.1-2.1-0 ; 
       add_smoke_frames-starbord_green-right.1-3.1-0 ; 
       add_smoke_frames-starbord_green-right.1-4.1-0 ; 
       add_smoke_frames-starbord_green-right.1-5.1-0 ; 
       add_smoke_frames-starbord_green-right.1-6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       add_frames-body.4-0 ROOT ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap09-add_smoke_frames.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       add_frames-t2d11.1-0 ; 
       add_frames-t2d14.1-0 ; 
       add_frames-t2d15.1-0 ; 
       add_frames-t2d16.1-0 ; 
       add_frames-t2d17.1-0 ; 
       add_frames-t2d2.1-0 ; 
       add_frames-t2d20.1-0 ; 
       add_frames-t2d21.1-0 ; 
       add_frames-t2d22.1-0 ; 
       add_frames-t2d23.1-0 ; 
       add_frames-t2d24.1-0 ; 
       add_frames-t2d25.1-0 ; 
       add_frames-t2d26.1-0 ; 
       add_frames-t2d27.1-0 ; 
       add_frames-t2d28.1-0 ; 
       add_frames-t2d29.1-0 ; 
       add_frames-t2d30.1-0 ; 
       add_frames-t2d31.1-0 ; 
       add_frames-t2d5.1-0 ; 
       add_frames-t2d6.1-0 ; 
       add_frames-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 13 300 ; 
       1 4 300 ; 
       2 8 300 ; 
       2 6 300 ; 
       3 17 300 ; 
       3 3 300 ; 
       4 11 300 ; 
       5 7 300 ; 
       5 5 300 ; 
       15 16 300 ; 
       16 21 300 ; 
       17 28 300 ; 
       18 22 300 ; 
       19 23 300 ; 
       20 24 300 ; 
       21 25 300 ; 
       22 26 300 ; 
       23 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       19 10 401 ; 
       20 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 10 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 11 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
