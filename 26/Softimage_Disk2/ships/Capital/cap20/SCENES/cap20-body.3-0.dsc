SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       body-cyl1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.19-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       kez_frigate_F-mat59.2-0 ; 
       kez_frigate_F-mat94.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       body-cube1.2-0 ROOT ; 
       body-cube2.2-0 ; 
       body-cube3.2-0 ROOT ; 
       body-cube4.1-0 ROOT ; 
       body-cyl1.3-0 ROOT ; 
       body-slicer.1-0 ; 
       body-spline1.2-0 ROOT ; 
       body-spline10.1-0 ; 
       body-spline2.2-0 ROOT ; 
       body-spline4.2-0 ROOT ; 
       body-spline5.2-0 ROOT ; 
       body-spline6.1-0 ; 
       body-spline7.2-0 ROOT ; 
       body-spline8.1-0 ; 
       body-spline9.1-0 ; 
       body-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-body.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       body-t2d81.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 4 110 ; 
       5 1 110 ; 
       7 12 110 ; 
       11 12 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       15 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1.312 0.792 1 0 0 0 0 -14.53086 -27.21434 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 SRT 0.852 1 1 -0.06986339 -0.2297942 0.2980658 2.958579 -17.42844 -39.41267 MPRFLG 0 ; 
       4 SCHEM 3.75 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.81878 2.477799 MPRFLG 0 ; 
       5 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 30 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 20 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 25 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 27.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 35 0 0 SRT 0.852 0.9999999 1 0.06986339 -2.911798 -0.2980658 -2.958579 -17.42844 -39.41267 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
