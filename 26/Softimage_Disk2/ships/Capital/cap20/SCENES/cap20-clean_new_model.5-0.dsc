SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       clean_new_model-bmerge4.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.79-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       clean_new_model-engines1.1-0 ; 
       clean_new_model-engines2.1-0 ; 
       clean_new_model-holes4.1-0 ; 
       clean_new_model-mat101.1-0 ; 
       clean_new_model-mat102.1-0 ; 
       clean_new_model-mat103.1-0 ; 
       clean_new_model-mat104.1-0 ; 
       clean_new_model-mat110.1-0 ; 
       clean_new_model-mat111.1-0 ; 
       clean_new_model-mat112.1-0 ; 
       clean_new_model-mat96.1-0 ; 
       clean_new_model-mat99.1-0 ; 
       kez_frigate_F-mat94.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       clean_new_model-bmerge4.4-0 ROOT ; 
       clean_new_model-cube3.2-0 ; 
       clean_new_model-cube5.1-0 ; 
       clean_new_model-cube6.1-0 ; 
       clean_new_model-cube8.1-0 ; 
       clean_new_model-slicer.1-0 ; 
       clean_new_model-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-clean_new_model.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       clean_new_model-t2d10.1-0 ; 
       clean_new_model-t2d11.1-0 ; 
       clean_new_model-t2d12.1-0 ; 
       clean_new_model-t2d13.1-0 ; 
       clean_new_model-t2d2.1-0 ; 
       clean_new_model-t2d5.1-0 ; 
       clean_new_model-t2d6.1-0 ; 
       clean_new_model-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 11 300 ; 
       1 0 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       5 10 300 ; 
       6 12 300 ; 
       4 6 300 ; 
       4 1 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       6 2 401 ; 
       1 3 401 ; 
       3 7 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       10 4 401 ; 
       11 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 -14.05778 -9.35677 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       12 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 24 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
