SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       gun-cyl1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.24-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       kez_frigate_F-mat59.2-0 ; 
       kez_frigate_F-mat94.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       gun-cube1.2-0 ; 
       gun-cube2.2-0 ; 
       gun-cube3.2-0 ; 
       gun-cube4.1-0 ; 
       gun-cube5.1-0 ; 
       gun-cube6.1-0 ; 
       gun-cyl1.4-0 ROOT ; 
       gun-slicer.1-0 ; 
       gun-spline1.1-0 ; 
       gun-spline10.1-0 ; 
       gun-spline2.1-0 ; 
       gun-spline4.1-0 ; 
       gun-spline5.1-0 ; 
       gun-spline6.1-0 ; 
       gun-spline7.2-0 ROOT ; 
       gun-spline8.1-0 ; 
       gun-spline9.1-0 ; 
       gun-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-gun.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       gun-t2d81.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       7 1 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 7 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 0 300 ; 
       17 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.81878 2.477799 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 11.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
