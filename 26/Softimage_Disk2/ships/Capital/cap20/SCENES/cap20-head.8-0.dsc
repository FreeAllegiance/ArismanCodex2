SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       head-cube2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.10-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       head-mat100.1-0 ; 
       head-mat102.1-0 ; 
       head-mat103.1-0 ; 
       head-mat104.1-0 ; 
       head-mat105.1-0 ; 
       head-mat97.1-0 ; 
       head-mat98.1-0 ; 
       head-mat99.1-0 ; 
       kez_frigate_F-mat55.1-0 ; 
       kez_frigate_F-mat59.1-0 ; 
       kez_frigate_F-mat94.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       cap01-cap01.6-0 ROOT ; 
       cap01-ffuselg.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-shield.1-0 ; 
       head-circle1.2-0 ROOT ; 
       head-cube1.1-0 ROOT ; 
       head-cube2.8-0 ROOT ; 
       head-cyl1.2-0 ROOT ; 
       head-slicer.1-0 ; 
       head-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-head.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       head-t2d78.1-0 ; 
       head-t2d79.1-0 ; 
       head-t2d80.1-0 ; 
       head-t2d81.2-0 ; 
       head-t2d84.1-0 ; 
       head-t2d85.1-0 ; 
       head-t2d86.1-0 ; 
       head-t2d87.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       8 6 110 ; 
       9 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 8 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 4 300 ; 
       8 9 300 ; 
       9 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 3 401 ; 
       1 6 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 7 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 0 0 SRT 2.09 2.09 2.09 0 0 0 0 0.04942624 5.590769 MPRFLG 0 ; 
       4 SCHEM 15 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 3.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 -17.18949 MPRFLG 0 ; 
       2 SCHEM 3.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 USR MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
