SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       mapping-cyl1.35-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.65-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       kez_frigate_F-mat94.3-0 ; 
       mapping-engines1.3-0 ; 
       mapping-engines2.3-0 ; 
       mapping-holes1.1-0 ; 
       mapping-mat101.2-0 ; 
       mapping-mat102.3-0 ; 
       mapping-mat103.2-0 ; 
       mapping-mat104.1-0 ; 
       mapping-mat95.2-0 ; 
       mapping-mat96.2-0 ; 
       mapping-mat97.3-0 ; 
       mapping-mat98.2-0 ; 
       mapping-mat99.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       mapping-cube1.2-0 ; 
       mapping-cube2.2-0 ; 
       mapping-cube3.2-0 ; 
       mapping-cube5.1-0 ; 
       mapping-cube6.1-0 ; 
       mapping-cube8.1-0 ROOT ; 
       mapping-cyl1.26-0 ROOT ; 
       mapping-slicer.1-0 ; 
       mapping-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-mapping.35-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       mapping-t2d1.3-0 ; 
       mapping-t2d10.3-0 ; 
       mapping-t2d11.2-0 ; 
       mapping-t2d12.1-0 ; 
       mapping-t2d13.1-0 ; 
       mapping-t2d14.1-0 ; 
       mapping-t2d2.2-0 ; 
       mapping-t2d3.4-0 ; 
       mapping-t2d4.2-0 ; 
       mapping-t2d5.5-0 ; 
       mapping-t2d6.4-0 ; 
       mapping-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       7 1 110 ; 
       8 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       1 8 300 ; 
       2 12 300 ; 
       2 1 300 ; 
       3 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       6 10 300 ; 
       6 3 300 ; 
       7 9 300 ; 
       8 0 300 ; 
       5 7 300 ; 
       5 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 10 401 ; 
       7 3 401 ; 
       2 4 401 ; 
       4 11 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 0 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       3 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.80566 2.473333 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 31.25 0 0 SRT 0.852 0.9999999 1 0.06986339 -2.911798 -0.2980658 -2.958579 -17.42846 -39.41271 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 24 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
