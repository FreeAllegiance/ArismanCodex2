SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       mapping-cyl1.38-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.68-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       kez_frigate_F-mat94.3-0 ; 
       mapping-engines1.3-0 ; 
       mapping-engines2.3-0 ; 
       mapping-holes1.1-0 ; 
       mapping-holes2.1-0 ; 
       mapping-holes3.1-0 ; 
       mapping-mat101.2-0 ; 
       mapping-mat102.3-0 ; 
       mapping-mat103.2-0 ; 
       mapping-mat104.1-0 ; 
       mapping-mat105.1-0 ; 
       mapping-mat106.1-0 ; 
       mapping-mat107.1-0 ; 
       mapping-mat108.1-0 ; 
       mapping-mat95.2-0 ; 
       mapping-mat96.2-0 ; 
       mapping-mat97.3-0 ; 
       mapping-mat98.2-0 ; 
       mapping-mat99.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       mapping-bmerge2.1-0 ROOT ; 
       mapping-cube1.2-0 ; 
       mapping-cube2.2-0 ; 
       mapping-cube3.2-0 ; 
       mapping-cube5.1-0 ; 
       mapping-cube6.1-0 ; 
       mapping-cube8.1-0 ; 
       mapping-cube9.1-0 ROOT ; 
       mapping-cyl1.27-0 ROOT ; 
       mapping-slicer.1-0 ; 
       mapping-SSfb.1-0 ; 
       mapping1-cyl1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-mapping.38-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       mapping-t2d1.3-0 ; 
       mapping-t2d10.3-0 ; 
       mapping-t2d11.2-0 ; 
       mapping-t2d12.1-0 ; 
       mapping-t2d13.1-0 ; 
       mapping-t2d14.1-0 ; 
       mapping-t2d15.1-0 ; 
       mapping-t2d16.1-0 ; 
       mapping-t2d17.1-0 ; 
       mapping-t2d2.2-0 ; 
       mapping-t2d3.4-0 ; 
       mapping-t2d4.2-0 ; 
       mapping-t2d5.5-0 ; 
       mapping-t2d6.4-0 ; 
       mapping-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 8 110 ; 
       2 8 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       6 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 17 300 ; 
       2 14 300 ; 
       3 18 300 ; 
       3 1 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       11 10 300 ; 
       11 4 300 ; 
       8 16 300 ; 
       8 3 300 ; 
       9 15 300 ; 
       10 0 300 ; 
       7 11 300 ; 
       6 9 300 ; 
       6 2 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       0 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 13 401 ; 
       9 3 401 ; 
       2 4 401 ; 
       6 14 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       14 0 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       3 5 401 ; 
       10 6 401 ; 
       4 7 401 ; 
       11 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 18.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 43.75 0 0 DISPLAY 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.80566 2.473333 MPRFLG 0 ; 
       8 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.80566 2.473333 MPRFLG 0 ; 
       9 SCHEM 3.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 47.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -13.86822 -3.112968 MPRFLG 0 ; 
       6 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 37.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.0266645 -14.26802 1.170247 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
