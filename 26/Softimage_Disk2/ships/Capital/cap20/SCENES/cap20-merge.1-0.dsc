SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       merge-cyl1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.69-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       kez_frigate_F-mat94.3-0 ; 
       merge-engines1.1-0 ; 
       merge-engines2.1-0 ; 
       merge-holes1.1-0 ; 
       merge-holes3.1-0 ; 
       merge-mat101.1-0 ; 
       merge-mat102.1-0 ; 
       merge-mat103.1-0 ; 
       merge-mat104.1-0 ; 
       merge-mat107.1-0 ; 
       merge-mat108.1-0 ; 
       merge-mat109.1-0 ; 
       merge-mat95.1-0 ; 
       merge-mat96.1-0 ; 
       merge-mat97.1-0 ; 
       merge-mat98.1-0 ; 
       merge-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       merge-bmerge2.1-0 ROOT ; 
       merge-cube1.2-0 ; 
       merge-cube10.1-0 ROOT ; 
       merge-cube2.2-0 ; 
       merge-cube3.2-0 ; 
       merge-cube5.1-0 ; 
       merge-cube6.1-0 ; 
       merge-cube8.1-0 ; 
       merge-cyl1.1-0 ROOT ; 
       merge-slicer.1-0 ; 
       merge-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-merge.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       merge-t2d1.1-0 ; 
       merge-t2d10.1-0 ; 
       merge-t2d11.1-0 ; 
       merge-t2d12.1-0 ; 
       merge-t2d13.1-0 ; 
       merge-t2d14.1-0 ; 
       merge-t2d15.1-0 ; 
       merge-t2d2.1-0 ; 
       merge-t2d3.1-0 ; 
       merge-t2d4.1-0 ; 
       merge-t2d5.1-0 ; 
       merge-t2d6.1-0 ; 
       merge-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 8 110 ; 
       3 8 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       9 3 110 ; 
       10 9 110 ; 
       7 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 15 300 ; 
       3 12 300 ; 
       4 16 300 ; 
       4 1 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       8 14 300 ; 
       8 3 300 ; 
       9 13 300 ; 
       10 0 300 ; 
       7 8 300 ; 
       7 2 300 ; 
       2 11 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       8 3 401 ; 
       2 4 401 ; 
       5 12 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       12 0 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       3 5 401 ; 
       11 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 18.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.80566 2.473333 MPRFLG 0 ; 
       9 SCHEM 3.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 SRT 1.312 0.792 1 0 0 0 0 -14.53088 -27.21438 MPRFLG 0 ; 
       0 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.0266645 -14.26802 1.170247 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 51.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
