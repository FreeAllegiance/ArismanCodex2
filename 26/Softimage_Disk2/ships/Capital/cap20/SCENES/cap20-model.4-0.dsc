SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       model-cyl1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.29-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       kez_frigate_F-mat94.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       model-cube1.2-0 ; 
       model-cube2.2-0 ; 
       model-cube3.2-0 ; 
       model-cube4.1-0 ; 
       model-cube5.1-0 ; 
       model-cube6.1-0 ; 
       model-cyl1.3-0 ROOT ; 
       model-slicer.1-0 ; 
       model-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-model.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       7 1 110 ; 
       8 7 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 8.75 0 0 SRT 2.09 2.09 2.09 0 0 0 0 -13.81878 2.477799 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 16.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
