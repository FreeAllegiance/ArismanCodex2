SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       cap01-cap01.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.2-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       kez_frigate_F-mat55.1-0 ; 
       kez_frigate_F-mat59.1-0 ; 
       kez_frigate_F-mat94.1-0 ; 
       new_texture-mat100.1-0 ; 
       new_texture-mat102.1-0 ; 
       new_texture-mat103.1-0 ; 
       new_texture-mat104.1-0 ; 
       new_texture-mat105.1-0 ; 
       new_texture-mat96.1-0 ; 
       new_texture-mat97.1-0 ; 
       new_texture-mat98.1-0 ; 
       new_texture-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       cap01-cap01.2-0 ROOT ; 
       cap01-ffuselg.1-0 ; 
       cap01-lwepemt1.1-0 ; 
       cap01-lwepemt2.1-0 ; 
       cap01-lwepemt3.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-rwep1.1-0 ; 
       cap01-rwepemt1.1-0 ; 
       cap01-rwepemt2.1-0 ; 
       cap01-rwepemt3.1-0 ; 
       cap01-shield.1-0 ; 
       cap01-slicer.1-0 ; 
       cap01-SSfb.1-0 ; 
       cap01-trail.1-0 ; 
       cap01-turatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-new_texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       new_texture-t2d77.2-0 ; 
       new_texture-t2d78.2-0 ; 
       new_texture-t2d79.2-0 ; 
       new_texture-t2d80.2-0 ; 
       new_texture-t2d81.2-0 ; 
       new_texture-t2d84.2-0 ; 
       new_texture-t2d85.2-0 ; 
       new_texture-t2d86.2-0 ; 
       new_texture-t2d87.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       6 8 300 ; 
       10 7 300 ; 
       11 1 300 ; 
       12 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 4 401 ; 
       4 7 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 8 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 10 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -10 0 MPRFLG 0 ; 
       1 SCHEM 30 -8 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 35 -8 0 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 USR MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
