SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       add_frames-body_front1.3-0 ; 
       add_frames-body_side.3-0 ; 
       add_frames-cockpit1.3-0 ; 
       add_frames-engines1.3-0 ; 
       add_frames-engines2.3-0 ; 
       add_frames-gun_middle.3-0 ; 
       add_frames-gun_middle1.3-0 ; 
       add_frames-gun_side.3-0 ; 
       add_frames-gun_side1.3-0 ; 
       add_frames-head.3-0 ; 
       add_frames-holes1.3-0 ; 
       add_frames-mat101.3-0 ; 
       add_frames-mat105.3-0 ; 
       add_frames-mat106.3-0 ; 
       add_frames-mat107.3-0 ; 
       add_frames-mat108.3-0 ; 
       add_frames-mat96.3-0 ; 
       add_frames-mat99.3-0 ; 
       add_frames-neck.3-0 ; 
       add_frames-nose_white-center.1-1.3-0 ; 
       add_frames-port_red-left.1-1.3-0 ; 
       add_frames-starbord_green-right.1-1.3-0 ; 
       add_frames-starbord_green-right.1-2.3-0 ; 
       add_frames-starbord_green-right.1-3.3-0 ; 
       add_frames-starbord_green-right.1-4.3-0 ; 
       add_frames-starbord_green-right.1-5.3-0 ; 
       add_frames-starbord_green-right.1-6.3-0 ; 
       add_frames-top_of_head1.3-0 ; 
       add_frames-vents1.3-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat111.1-0 ; 
       scaled-mat112.1-0 ; 
       scaled-mat18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       add_frames-130deg.3-0 ROOT ; 
       add_frames-170deg1.3-0 ROOT ; 
       add_frames-170deg2.3-0 ROOT ; 
       add_frames-175deg.3-0 ROOT ; 
       add_frames-body_1.6-0 ROOT ; 
       add_frames-cone1.1-0 ; 
       add_frames-cone1_1.1-0 ; 
       add_frames-cone1_2.1-0 ; 
       add_frames-cone5.1-0 ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
       add_frames-trail.1-0 ; 
       add_frames-turwepemt1.1-0 ; 
       add_frames-turwepemt2.1-0 ; 
       add_frames-turwepemt3.1-0 ; 
       add_frames-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-scaled.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames-t2d11.4-0 ; 
       add_frames-t2d14.5-0 ; 
       add_frames-t2d15.5-0 ; 
       add_frames-t2d16.5-0 ; 
       add_frames-t2d17.5-0 ; 
       add_frames-t2d2.4-0 ; 
       add_frames-t2d20.4-0 ; 
       add_frames-t2d21.5-0 ; 
       add_frames-t2d22.5-0 ; 
       add_frames-t2d23.5-0 ; 
       add_frames-t2d24.5-0 ; 
       add_frames-t2d25.5-0 ; 
       add_frames-t2d26.4-0 ; 
       add_frames-t2d27.4-0 ; 
       add_frames-t2d28.5-0 ; 
       add_frames-t2d29.5-0 ; 
       add_frames-t2d30.4-0 ; 
       add_frames-t2d31.4-0 ; 
       add_frames-t2d5.4-0 ; 
       add_frames-t2d6.4-0 ; 
       add_frames-t2d9.4-0 ; 
       scaled-t2d32.1-0 ; 
       scaled-t2d34.1-0 ; 
       scaled-t2d35.1-0 ; 
       scaled-t2d36.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
       26 4 110 ; 
       27 4 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 4 110 ; 
       32 4 110 ; 
       33 5 110 ; 
       34 6 110 ; 
       35 8 110 ; 
       36 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 12 300 ; 
       4 9 300 ; 
       4 18 300 ; 
       4 1 300 ; 
       4 10 300 ; 
       4 2 300 ; 
       4 28 300 ; 
       4 27 300 ; 
       4 0 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       5 32 300 ; 
       6 29 300 ; 
       7 30 300 ; 
       8 31 300 ; 
       9 13 300 ; 
       9 4 300 ; 
       10 8 300 ; 
       10 6 300 ; 
       11 17 300 ; 
       11 3 300 ; 
       12 11 300 ; 
       13 7 300 ; 
       13 5 300 ; 
       23 16 300 ; 
       24 19 300 ; 
       25 26 300 ; 
       26 20 300 ; 
       27 21 300 ; 
       28 22 300 ; 
       29 23 300 ; 
       30 24 300 ; 
       31 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 21 400 ; 
       6 22 400 ; 
       7 23 400 ; 
       8 24 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       27 10 401 ; 
       28 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 338.9694 22.3499 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 338.9694 20.3499 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 283.475 10.2817 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 12.22132 12.22132 12.22132 0.53 0 0 0 5.034175 -8.229152 MPRFLG 0 ; 
       1 SCHEM 290.7369 10.20931 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.415228 3.415228 3.415228 0.1044866 -0.09664719 2.141833 -4.241687 -0.6467642 22.05649 MPRFLG 0 ; 
       2 SCHEM 294.1566 10.26452 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 3.415228 3.415228 3.415228 3.037106 -0.09664719 0.9997597 4.241687 -0.6467642 22.05649 MPRFLG 0 ; 
       3 SCHEM 286.7829 10.22988 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 12.22132 12.22132 12.22132 4.97239 0 0 0 2.249137 -28.47071 MPRFLG 0 ; 
       4 SCHEM 290.5525 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -2.277428 MPRFLG 0 ; 
       5 SCHEM 275.3297 -8.438475 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 278.3056 -8.479252 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 282.0972 -8.485817 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 285.6791 -8.342209 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 291.8025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 294.3025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 281.8025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 284.3025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 289.3025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 306.8025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 309.3025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 299.3025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 304.3025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 316.8025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 311.8025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 314.3025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 296.8025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 301.8025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 286.8025 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 271.8025 -2 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 279.3025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 274.3025 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 276.8025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 261.8025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 269.3025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 264.3025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 266.8025 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 319.3025 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 275.4205 -11.44993 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 278.3963 -11.49071 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 285.7699 -11.35367 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 282.1879 -11.49728 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 282.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 292.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 289.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 294.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 289.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 294.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 284.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 292.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 287.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 282.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 272.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 274.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 277.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 262.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 269.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 264.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 267.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 279.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 59.82404 80.2588 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 52.32404 80.2588 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 111.206 170.7234 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM -13.81561 -9.766262 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 289.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 287.0525 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 289.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 292.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 292.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 294.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 294.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 282.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 282.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 284.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM -11.31561 -9.766262 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 62.32404 80.2588 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 54.82404 80.2588 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 113.706 170.7234 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
