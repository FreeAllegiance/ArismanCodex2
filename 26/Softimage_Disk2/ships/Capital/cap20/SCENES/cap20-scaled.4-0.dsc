SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       add_frames-body_front1.3-0 ; 
       add_frames-body_side.3-0 ; 
       add_frames-cockpit1.3-0 ; 
       add_frames-engines1.3-0 ; 
       add_frames-engines2.3-0 ; 
       add_frames-gun_middle.3-0 ; 
       add_frames-gun_middle1.3-0 ; 
       add_frames-gun_side.3-0 ; 
       add_frames-gun_side1.3-0 ; 
       add_frames-head.3-0 ; 
       add_frames-holes1.3-0 ; 
       add_frames-mat101.3-0 ; 
       add_frames-mat105.3-0 ; 
       add_frames-mat106.3-0 ; 
       add_frames-mat107.3-0 ; 
       add_frames-mat108.3-0 ; 
       add_frames-mat96.3-0 ; 
       add_frames-mat99.3-0 ; 
       add_frames-neck.3-0 ; 
       add_frames-nose_white-center.1-1.3-0 ; 
       add_frames-port_red-left.1-1.3-0 ; 
       add_frames-starbord_green-right.1-1.3-0 ; 
       add_frames-starbord_green-right.1-2.3-0 ; 
       add_frames-starbord_green-right.1-3.3-0 ; 
       add_frames-starbord_green-right.1-4.3-0 ; 
       add_frames-starbord_green-right.1-5.3-0 ; 
       add_frames-starbord_green-right.1-6.3-0 ; 
       add_frames-top_of_head1.3-0 ; 
       add_frames-vents1.3-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat111.1-0 ; 
       scaled-mat112.1-0 ; 
       scaled-mat18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       add_frames-body_1.7-0 ROOT ; 
       add_frames-cone1.1-0 ; 
       add_frames-cone1_1.1-0 ; 
       add_frames-cone1_2.1-0 ; 
       add_frames-cone5.1-0 ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
       add_frames-trail.1-0 ; 
       add_frames-turwepemt1.1-0 ; 
       add_frames-turwepemt2.1-0 ; 
       add_frames-turwepemt3.1-0 ; 
       add_frames-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-scaled.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames-t2d11.5-0 ; 
       add_frames-t2d14.6-0 ; 
       add_frames-t2d15.6-0 ; 
       add_frames-t2d16.6-0 ; 
       add_frames-t2d17.6-0 ; 
       add_frames-t2d2.5-0 ; 
       add_frames-t2d20.5-0 ; 
       add_frames-t2d21.6-0 ; 
       add_frames-t2d22.6-0 ; 
       add_frames-t2d23.6-0 ; 
       add_frames-t2d24.6-0 ; 
       add_frames-t2d25.6-0 ; 
       add_frames-t2d26.5-0 ; 
       add_frames-t2d27.5-0 ; 
       add_frames-t2d28.6-0 ; 
       add_frames-t2d29.6-0 ; 
       add_frames-t2d30.5-0 ; 
       add_frames-t2d31.5-0 ; 
       add_frames-t2d5.5-0 ; 
       add_frames-t2d6.5-0 ; 
       add_frames-t2d9.5-0 ; 
       scaled-t2d32.2-0 ; 
       scaled-t2d34.2-0 ; 
       scaled-t2d35.2-0 ; 
       scaled-t2d36.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 1 110 ; 
       30 2 110 ; 
       31 4 110 ; 
       32 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 28 300 ; 
       0 27 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 32 300 ; 
       2 29 300 ; 
       3 30 300 ; 
       4 31 300 ; 
       5 13 300 ; 
       5 4 300 ; 
       6 8 300 ; 
       6 6 300 ; 
       7 17 300 ; 
       7 3 300 ; 
       8 11 300 ; 
       9 7 300 ; 
       9 5 300 ; 
       19 16 300 ; 
       20 19 300 ; 
       21 26 300 ; 
       22 20 300 ; 
       23 21 300 ; 
       24 22 300 ; 
       25 23 300 ; 
       26 24 300 ; 
       27 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 21 400 ; 
       2 22 400 ; 
       3 23 400 ; 
       4 24 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       27 10 401 ; 
       28 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -0.07064227 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 35 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 57.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 55 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 67.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 62.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 65 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 52.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 37.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 12.5 -2 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 15 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 22.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 2.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 10 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 7.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 70 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 17.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 20 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 30 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 25 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
