SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       add_frames-body_front1.3-0 ; 
       add_frames-body_side.3-0 ; 
       add_frames-cockpit1.3-0 ; 
       add_frames-engines1.3-0 ; 
       add_frames-engines2.3-0 ; 
       add_frames-gun_middle.3-0 ; 
       add_frames-gun_middle1.3-0 ; 
       add_frames-gun_side.3-0 ; 
       add_frames-gun_side1.3-0 ; 
       add_frames-head.3-0 ; 
       add_frames-holes1.3-0 ; 
       add_frames-mat101.3-0 ; 
       add_frames-mat105.3-0 ; 
       add_frames-mat106.3-0 ; 
       add_frames-mat107.3-0 ; 
       add_frames-mat108.3-0 ; 
       add_frames-mat96.3-0 ; 
       add_frames-mat99.3-0 ; 
       add_frames-neck.3-0 ; 
       add_frames-top_of_head1.3-0 ; 
       add_frames-vents1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       add_frames-body_1.4-0 ROOT ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-slicer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       add_frames-t2d11.4-0 ; 
       add_frames-t2d14.5-0 ; 
       add_frames-t2d15.5-0 ; 
       add_frames-t2d16.5-0 ; 
       add_frames-t2d17.5-0 ; 
       add_frames-t2d2.4-0 ; 
       add_frames-t2d20.4-0 ; 
       add_frames-t2d21.5-0 ; 
       add_frames-t2d22.5-0 ; 
       add_frames-t2d23.5-0 ; 
       add_frames-t2d24.5-0 ; 
       add_frames-t2d25.5-0 ; 
       add_frames-t2d26.4-0 ; 
       add_frames-t2d27.4-0 ; 
       add_frames-t2d28.5-0 ; 
       add_frames-t2d29.5-0 ; 
       add_frames-t2d30.4-0 ; 
       add_frames-t2d31.4-0 ; 
       add_frames-t2d5.4-0 ; 
       add_frames-t2d6.4-0 ; 
       add_frames-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 13 300 ; 
       1 4 300 ; 
       2 8 300 ; 
       2 6 300 ; 
       3 17 300 ; 
       3 3 300 ; 
       4 11 300 ; 
       5 7 300 ; 
       5 5 300 ; 
       6 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       19 10 401 ; 
       20 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 338.9694 22.3499 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 338.9694 20.3499 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 290.5525 0 0 SRT 1 1 1 0 0 0 0 0 -2.277428 MPRFLG 0 ; 
       1 SCHEM 291.8025 -2 0 MPRFLG 0 ; 
       2 SCHEM 294.3025 -2 0 MPRFLG 0 ; 
       3 SCHEM 281.8025 -2 0 MPRFLG 0 ; 
       4 SCHEM 284.3025 -2 0 MPRFLG 0 ; 
       5 SCHEM 289.3025 -2 0 MPRFLG 0 ; 
       6 SCHEM 286.8025 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 282.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 292.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 289.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 294.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 289.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 294.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 284.5525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 292.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 287.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 282.0525 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 319.5525 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 289.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 287.0525 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 289.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 292.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 292.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 319.5525 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 294.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 294.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 282.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 282.0525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 284.5525 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
