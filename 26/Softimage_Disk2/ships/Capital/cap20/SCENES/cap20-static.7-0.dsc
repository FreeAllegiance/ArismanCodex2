SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.15-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       add_frames-body_front1.3-0 ; 
       add_frames-body_side.3-0 ; 
       add_frames-cockpit1.3-0 ; 
       add_frames-engines1.3-0 ; 
       add_frames-engines2.3-0 ; 
       add_frames-gun_middle.3-0 ; 
       add_frames-gun_middle1.3-0 ; 
       add_frames-gun_side.3-0 ; 
       add_frames-gun_side1.3-0 ; 
       add_frames-head.3-0 ; 
       add_frames-holes1.3-0 ; 
       add_frames-mat101.3-0 ; 
       add_frames-mat105.3-0 ; 
       add_frames-mat106.3-0 ; 
       add_frames-mat107.3-0 ; 
       add_frames-mat108.3-0 ; 
       add_frames-mat96.3-0 ; 
       add_frames-mat99.3-0 ; 
       add_frames-neck.3-0 ; 
       add_frames-top_of_head1.3-0 ; 
       add_frames-vents1.3-0 ; 
       static-mat110.1-0 ; 
       static-mat111.1-0 ; 
       static-mat112.1-0 ; 
       static-mat18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       add_frames-body_1_1.4-0 ROOT ; 
       add_frames-cone1.1-0 ; 
       add_frames-cone1_1.1-0 ; 
       add_frames-cone1_2.1-0 ; 
       add_frames-cone5.1-0 ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-slicer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-static.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_frames-t2d11.8-0 ; 
       add_frames-t2d14.10-0 ; 
       add_frames-t2d15.10-0 ; 
       add_frames-t2d16.10-0 ; 
       add_frames-t2d17.10-0 ; 
       add_frames-t2d2.8-0 ; 
       add_frames-t2d20.8-0 ; 
       add_frames-t2d21.10-0 ; 
       add_frames-t2d22.10-0 ; 
       add_frames-t2d23.10-0 ; 
       add_frames-t2d24.10-0 ; 
       add_frames-t2d25.10-0 ; 
       add_frames-t2d26.8-0 ; 
       add_frames-t2d27.8-0 ; 
       add_frames-t2d28.10-0 ; 
       add_frames-t2d29.10-0 ; 
       add_frames-t2d30.8-0 ; 
       add_frames-t2d31.8-0 ; 
       add_frames-t2d5.8-0 ; 
       add_frames-t2d6.8-0 ; 
       add_frames-t2d9.8-0 ; 
       static-t2d32.1-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d36.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 24 300 ; 
       2 21 300 ; 
       3 22 300 ; 
       4 23 300 ; 
       5 13 300 ; 
       5 4 300 ; 
       6 8 300 ; 
       6 6 300 ; 
       7 17 300 ; 
       7 3 300 ; 
       8 11 300 ; 
       9 7 300 ; 
       9 5 300 ; 
       10 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 21 400 ; 
       2 22 400 ; 
       3 23 400 ; 
       4 24 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       19 10 401 ; 
       20 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
