SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       texture-body.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.120-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       kez_frigate_F-mat94.4-0 ; 
       texture-body_front1.2-0 ; 
       texture-body_side.8-0 ; 
       texture-cockpit1.6-0 ; 
       texture-engines1.2-0 ; 
       texture-engines2.4-0 ; 
       texture-gun_middle.3-0 ; 
       texture-gun_middle1.1-0 ; 
       texture-gun_side.3-0 ; 
       texture-gun_side1.1-0 ; 
       texture-head.8-0 ; 
       texture-holes1.9-0 ; 
       texture-mat101.2-0 ; 
       texture-mat105.12-0 ; 
       texture-mat106.5-0 ; 
       texture-mat107.4-0 ; 
       texture-mat108.3-0 ; 
       texture-mat96.2-0 ; 
       texture-mat99.2-0 ; 
       texture-neck.8-0 ; 
       texture-top_of_head1.3-0 ; 
       texture-vents1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       texture-body.26-0 ROOT ; 
       texture-cube12.1-0 ; 
       texture-cube13.1-0 ; 
       texture-cube3.2-0 ; 
       texture-cube5.1-0 ; 
       texture-cube6.1-0 ; 
       texture-slicer.1-0 ; 
       texture-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-texture.39-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       texture-t2d11.5-0 ; 
       texture-t2d14.18-0 ; 
       texture-t2d15.17-0 ; 
       texture-t2d16.16-0 ; 
       texture-t2d17.15-0 ; 
       texture-t2d2.3-0 ; 
       texture-t2d20.4-0 ; 
       texture-t2d21.12-0 ; 
       texture-t2d22.9-0 ; 
       texture-t2d23.7-0 ; 
       texture-t2d24.6-0 ; 
       texture-t2d25.3-0 ; 
       texture-t2d26.2-0 ; 
       texture-t2d27.2-0 ; 
       texture-t2d28.1-0 ; 
       texture-t2d29.1-0 ; 
       texture-t2d30.1-0 ; 
       texture-t2d31.1-0 ; 
       texture-t2d5.6-0 ; 
       texture-t2d6.6-0 ; 
       texture-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       2 0 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 10 300 ; 
       0 19 300 ; 
       0 2 300 ; 
       0 11 300 ; 
       0 3 300 ; 
       0 21 300 ; 
       0 20 300 ; 
       0 1 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       3 18 300 ; 
       3 4 300 ; 
       4 12 300 ; 
       5 8 300 ; 
       5 6 300 ; 
       2 9 300 ; 
       2 7 300 ; 
       6 17 300 ; 
       7 0 300 ; 
       1 14 300 ; 
       1 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       2 3 401 ; 
       3 7 401 ; 
       4 19 401 ; 
       14 12 401 ; 
       6 0 401 ; 
       8 6 401 ; 
       10 1 401 ; 
       11 4 401 ; 
       12 20 401 ; 
       13 9 401 ; 
       5 13 401 ; 
       17 5 401 ; 
       18 18 401 ; 
       19 2 401 ; 
       20 10 401 ; 
       21 8 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       9 16 401 ; 
       7 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 21.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
