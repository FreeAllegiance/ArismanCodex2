SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       tube-cube2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.13-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       kez_frigate_F-mat55.1-0 ; 
       kez_frigate_F-mat59.1-0 ; 
       kez_frigate_F-mat94.1-0 ; 
       tube-mat100.1-0 ; 
       tube-mat102.1-0 ; 
       tube-mat103.1-0 ; 
       tube-mat104.1-0 ; 
       tube-mat105.1-0 ; 
       tube-mat97.1-0 ; 
       tube-mat98.1-0 ; 
       tube-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       cap01-cap01.6-0 ROOT ; 
       cap01-ffuselg.1-0 ; 
       cap01-mfuselg.1-0 ; 
       cap01-shield.1-0 ; 
       tube-cube1.1-0 ROOT ; 
       tube-cube2.1-0 ROOT ; 
       tube-cyl1.1-0 ROOT ; 
       tube-slicer.1-0 ; 
       tube-spline1.1-0 ROOT ; 
       tube-spline2.1-0 ROOT ; 
       tube-spline4.1-0 ROOT ; 
       tube-spline5.1-0 ROOT ; 
       tube-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-tube.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       tube-t2d78.1-0 ; 
       tube-t2d79.1-0 ; 
       tube-t2d80.1-0 ; 
       tube-t2d81.1-0 ; 
       tube-t2d84.1-0 ; 
       tube-t2d85.1-0 ; 
       tube-t2d86.1-0 ; 
       tube-t2d87.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       7 5 110 ; 
       12 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       2 6 300 ; 
       3 7 300 ; 
       7 1 300 ; 
       12 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       4 6 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 7 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       10 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 0 0 DISPLAY 1 2 SRT 2.09 2.09 2.09 0 0 0 0 0.04942624 5.590769 MPRFLG 0 ; 
       0 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -17.18949 MPRFLG 0 ; 
       10 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 MPRFLG 0 ; 
       10 SCHEM 4 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 USR MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
