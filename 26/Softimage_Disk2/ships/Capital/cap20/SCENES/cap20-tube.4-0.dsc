SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       tube-cyl1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.16-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       kez_frigate_F-mat59.1-0 ; 
       kez_frigate_F-mat94.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       tube-cube1.1-0 ROOT ; 
       tube-cube2.2-0 ; 
       tube-cyl1.3-0 ROOT ; 
       tube-slicer.1-0 ; 
       tube-spline1.1-0 ROOT ; 
       tube-spline2.1-0 ROOT ; 
       tube-spline4.1-0 ROOT ; 
       tube-spline5.1-0 ROOT ; 
       tube-SSfb.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-tube.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       tube-t2d81.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 1 110 ; 
       8 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       8 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 3.75 0 0 SRT 2.09 2.09 2.09 0 0 0 0 0.04942624 5.590769 MPRFLG 0 ; 
       4 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 -17.18949 MPRFLG 0 ; 
       6 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 6.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
