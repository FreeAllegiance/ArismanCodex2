SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_frames-body_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       kez_frigate_F-cam_int1.137-0 ROOT ; 
       kez_frigate_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_frames-body_front1.2-0 ; 
       add_frames-body_side.2-0 ; 
       add_frames-cockpit1.2-0 ; 
       add_frames-engines1.2-0 ; 
       add_frames-engines2.2-0 ; 
       add_frames-gun_middle.2-0 ; 
       add_frames-gun_middle1.2-0 ; 
       add_frames-gun_side.2-0 ; 
       add_frames-gun_side1.2-0 ; 
       add_frames-head.2-0 ; 
       add_frames-holes1.2-0 ; 
       add_frames-mat101.2-0 ; 
       add_frames-mat105.2-0 ; 
       add_frames-mat106.2-0 ; 
       add_frames-mat107.2-0 ; 
       add_frames-mat108.2-0 ; 
       add_frames-mat96.2-0 ; 
       add_frames-mat99.2-0 ; 
       add_frames-neck.2-0 ; 
       add_frames-nose_white-center.1-1.2-0 ; 
       add_frames-port_red-left.1-1.2-0 ; 
       add_frames-starbord_green-right.1-1.2-0 ; 
       add_frames-starbord_green-right.1-2.2-0 ; 
       add_frames-starbord_green-right.1-3.2-0 ; 
       add_frames-starbord_green-right.1-4.2-0 ; 
       add_frames-starbord_green-right.1-5.2-0 ; 
       add_frames-starbord_green-right.1-6.2-0 ; 
       add_frames-top_of_head1.2-0 ; 
       add_frames-vents1.2-0 ; 
       turret_loc-mat109.1-0 ; 
       turret_loc-mat111.1-0 ; 
       turret_loc-mat112.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       add_frames-body_1.1-0 ROOT ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
       add_frames-trail.1-0 ; 
       arc-cone_1.1-0 ROOT ; 
       arc1-cone_1.1-0 ROOT ; 
       turret_loc-cone_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-turret_loc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       add_frames-t2d11.2-0 ; 
       add_frames-t2d14.3-0 ; 
       add_frames-t2d15.3-0 ; 
       add_frames-t2d16.3-0 ; 
       add_frames-t2d17.3-0 ; 
       add_frames-t2d2.2-0 ; 
       add_frames-t2d20.2-0 ; 
       add_frames-t2d21.3-0 ; 
       add_frames-t2d22.3-0 ; 
       add_frames-t2d23.3-0 ; 
       add_frames-t2d24.3-0 ; 
       add_frames-t2d25.3-0 ; 
       add_frames-t2d26.2-0 ; 
       add_frames-t2d27.2-0 ; 
       add_frames-t2d28.3-0 ; 
       add_frames-t2d29.3-0 ; 
       add_frames-t2d30.2-0 ; 
       add_frames-t2d31.2-0 ; 
       add_frames-t2d5.2-0 ; 
       add_frames-t2d6.2-0 ; 
       add_frames-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 28 300 ; 
       0 27 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 13 300 ; 
       1 4 300 ; 
       27 31 300 ; 
       25 29 300 ; 
       2 8 300 ; 
       2 6 300 ; 
       3 17 300 ; 
       3 3 300 ; 
       4 11 300 ; 
       5 7 300 ; 
       5 5 300 ; 
       15 16 300 ; 
       16 19 300 ; 
       17 26 300 ; 
       18 20 300 ; 
       19 21 300 ; 
       20 22 300 ; 
       21 23 300 ; 
       22 24 300 ; 
       23 25 300 ; 
       26 30 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       27 10 401 ; 
       28 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 54.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 55.75 -2 0 MPRFLG 0 ; 
       27 SCHEM 88.25 0 0 DISPLAY 1 2 SRT 15.72092 15.72092 15.72092 2.721593 0 0 0 -4.470916 -5.26236 MPRFLG 0 ; 
       25 SCHEM 20 0 0 DISPLAY 1 2 SRT 15.72092 15.72092 15.72092 0.42 0 0 0 4.470916 -5.26236 MPRFLG 0 ; 
       2 SCHEM 58.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 45.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 48.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 53.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 70.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 73.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 63.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 68.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 80.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 78.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 60.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 35.75 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 43.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 38.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 40.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 25.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 33.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 28.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 30.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 83.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 85.75 0 0 DISPLAY 1 2 SRT 15.72092 15.72092 15.72092 -1.570796 0 0 0 2.702856 -30.77293 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 53.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 58.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 53.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 58.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 48.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 88.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 33.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 28.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 43.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 83.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 53.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 53.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 56 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 83.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 58.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 58.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 48.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 83.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 11 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
