SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_frames-body_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       add_frames-body_front1.3-0 ; 
       add_frames-body_side.3-0 ; 
       add_frames-cockpit1.3-0 ; 
       add_frames-engines1.3-0 ; 
       add_frames-engines2.3-0 ; 
       add_frames-gun_middle.3-0 ; 
       add_frames-gun_middle1.3-0 ; 
       add_frames-gun_side.3-0 ; 
       add_frames-gun_side1.3-0 ; 
       add_frames-head.3-0 ; 
       add_frames-holes1.3-0 ; 
       add_frames-mat101.3-0 ; 
       add_frames-mat105.3-0 ; 
       add_frames-mat106.3-0 ; 
       add_frames-mat107.3-0 ; 
       add_frames-mat108.3-0 ; 
       add_frames-mat96.3-0 ; 
       add_frames-mat99.3-0 ; 
       add_frames-neck.3-0 ; 
       add_frames-nose_white-center.1-1.3-0 ; 
       add_frames-port_red-left.1-1.3-0 ; 
       add_frames-starbord_green-right.1-1.3-0 ; 
       add_frames-starbord_green-right.1-2.3-0 ; 
       add_frames-starbord_green-right.1-3.3-0 ; 
       add_frames-starbord_green-right.1-4.3-0 ; 
       add_frames-starbord_green-right.1-5.3-0 ; 
       add_frames-starbord_green-right.1-6.3-0 ; 
       add_frames-top_of_head1.3-0 ; 
       add_frames-vents1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       add_frames-body_1.2-0 ROOT ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
       add_frames-trail.1-0 ; 
       turcone-130deg.1-0 ROOT ; 
       turcone-160deg.1-0 ROOT ; 
       turcone-170deg.1-0 ROOT ; 
       turcone-175deg.1-0 ROOT ; 
       turret_loc-170deg1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Capital/cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-turret_loc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       add_frames-t2d11.3-0 ; 
       add_frames-t2d14.4-0 ; 
       add_frames-t2d15.4-0 ; 
       add_frames-t2d16.4-0 ; 
       add_frames-t2d17.4-0 ; 
       add_frames-t2d2.3-0 ; 
       add_frames-t2d20.3-0 ; 
       add_frames-t2d21.4-0 ; 
       add_frames-t2d22.4-0 ; 
       add_frames-t2d23.4-0 ; 
       add_frames-t2d24.4-0 ; 
       add_frames-t2d25.4-0 ; 
       add_frames-t2d26.3-0 ; 
       add_frames-t2d27.3-0 ; 
       add_frames-t2d28.4-0 ; 
       add_frames-t2d29.4-0 ; 
       add_frames-t2d30.3-0 ; 
       add_frames-t2d31.3-0 ; 
       add_frames-t2d5.3-0 ; 
       add_frames-t2d6.3-0 ; 
       add_frames-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 28 300 ; 
       0 27 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 13 300 ; 
       1 4 300 ; 
       2 8 300 ; 
       2 6 300 ; 
       3 17 300 ; 
       3 3 300 ; 
       4 11 300 ; 
       5 7 300 ; 
       5 5 300 ; 
       15 16 300 ; 
       16 19 300 ; 
       17 26 300 ; 
       18 20 300 ; 
       19 21 300 ; 
       20 22 300 ; 
       21 23 300 ; 
       22 24 300 ; 
       23 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       1 3 401 ; 
       2 7 401 ; 
       3 19 401 ; 
       4 13 401 ; 
       5 0 401 ; 
       6 17 401 ; 
       7 6 401 ; 
       8 16 401 ; 
       9 1 401 ; 
       10 4 401 ; 
       11 20 401 ; 
       12 9 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 5 401 ; 
       17 18 401 ; 
       18 2 401 ; 
       27 10 401 ; 
       28 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 52 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 53.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 55.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 43.25 -2 0 MPRFLG 0 ; 
       29 SCHEM 101.618 6.607599 0 WIRECOL 7 7 SRT 14.81846 14.81846 14.81846 0.1044866 -0.09664719 2.141833 -4.133896 -0.6133553 25.74933 MPRFLG 0 ; 
       4 SCHEM 45.75 -2 0 MPRFLG 0 ; 
       26 SCHEM 84.28115 3.603834 0 WIRECOL 7 7 DISPLAY 0 0 SRT 13.92197 13.92197 13.92197 2.921105 -0.09664719 0.99976 4.133896 -0.6133553 25.74933 MPRFLG 0 ; 
       27 SCHEM 86.78115 3.603834 0 WIRECOL 7 7 SRT 14.81846 14.81846 14.81846 3.037106 -0.09664719 0.99976 4.133896 -0.6133553 25.74933 MPRFLG 0 ; 
       5 SCHEM 50.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 68.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 78.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 73.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 75.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 58.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 63.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 48.25 -2 0 MPRFLG 0 ; 
       16 SCHEM 33.25 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 40.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 35.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 38.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 23.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 30.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 25.75 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 28.25 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 80.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 48.78414 6.607599 0 USR WIRECOL 7 7 SRT 14.74082 14.74082 14.74082 0.53 0 0 0 6.103373 -7.144192 MPRFLG 0 ; 
       28 SCHEM 50.64719 6.607599 0 USR WIRECOL 7 7 SRT 14.74082 14.74082 14.74082 4.97239 0 0 0 3.752808 -31.28118 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 43.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 53.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 51 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 51 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 53.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 48.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 43.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 33.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 23.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 28.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 81 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 48.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 51 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 53.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 53.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 56 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 56 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 43.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 43.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 81 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
