SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.4-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       poly-grid1.1-0 ; 
       poly-grid2.1-0 ROOT ; 
       poly-torus10.1-0 ; 
       poly-torus11.1-0 ; 
       poly-torus13.1-0 ; 
       poly-torus14.1-0 ; 
       poly-torus16.1-0 ; 
       poly-torus17.1-0 ROOT ; 
       poly-torus6.3-0 ROOT ; 
       poly-torus7.1-0 ; 
       poly-torus8.1-0 ; 
       poly-torus9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       yy-poly.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -3.577457 0 3.577457 MPRFLG 0 ; 
       7 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
