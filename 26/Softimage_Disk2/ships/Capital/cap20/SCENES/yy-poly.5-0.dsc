SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       poly-cam_int1.5-0 ROOT ; 
       poly-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       poly-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       poly-nurbs2.1-0 ; 
       poly-nurbs5.1-0 ROOT ; 
       poly-torus10.1-0 ; 
       poly-torus11.1-0 ; 
       poly-torus13.1-0 ; 
       poly-torus14.1-0 ; 
       poly-torus16.1-0 ; 
       poly-torus7.1-0 ; 
       poly-torus8.1-0 ; 
       poly-torus9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       yy-poly.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 0 300 ; 
       7 0 300 ; 
       8 0 300 ; 
       9 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
