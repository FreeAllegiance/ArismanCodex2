SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.69-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       cap300_belter_destroyer-mat108.1-0 ; 
       cap300_belter_destroyer-mat109.1-0 ; 
       cap300_belter_destroyer-mat110.1-0 ; 
       cap300_belter_destroyer-mat111.1-0 ; 
       cap300_belter_destroyer-mat112.1-0 ; 
       cap300_belter_destroyer-mat113.1-0 ; 
       cap300_belter_destroyer-mat114.3-0 ; 
       cap300_belter_destroyer-mat115.3-0 ; 
       cap300_belter_destroyer-mat116.3-0 ; 
       cap300_belter_destroyer-mat117.2-0 ; 
       cap300_belter_destroyer-mat118.2-0 ; 
       cap300_belter_destroyer-mat119.2-0 ; 
       cap300_belter_destroyer-mat120.2-0 ; 
       cap300_belter_destroyer-mat121.2-0 ; 
       cap300_belter_destroyer-mat122.2-0 ; 
       cap300_belter_destroyer-mat123.2-0 ; 
       cap300_belter_destroyer-mat124.2-0 ; 
       cap300_belter_destroyer-mat125.1-0 ; 
       cap300_belter_destroyer-mat126.1-0 ; 
       cap300_belter_destroyer-mat127.1-0 ; 
       cap300_belter_destroyer-mat128.1-0 ; 
       cap300_belter_destroyer-mat129.1-0 ; 
       cap300_belter_destroyer-mat130.1-0 ; 
       cap300_belter_destroyer-mat131.1-0 ; 
       cap300_belter_destroyer-mat132.1-0 ; 
       cap300_belter_destroyer-mat141.1-0 ; 
       cap300_belter_destroyer-mat142.1-0 ; 
       cap300_belter_destroyer-mat143.1-0 ; 
       cap300_belter_destroyer-mat144.2-0 ; 
       cap300_belter_destroyer-mat145.1-0 ; 
       cap300_belter_destroyer-mat146.1-0 ; 
       cap300_belter_destroyer-mat147.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       cap300_belter_destroyer-cube1.1-0 ; 
       cap300_belter_destroyer-cube2.1-0 ; 
       cap300_belter_destroyer-cube4.1-0 ; 
       cap300_belter_destroyer-cyl1_10.1-0 ; 
       cap300_belter_destroyer-cyl1_5.1-0 ; 
       cap300_belter_destroyer-cyl1_8.1-0 ; 
       cap300_belter_destroyer-cyl1_9.1-0 ; 
       cap300_belter_destroyer-cyl2.1-0 ; 
       cap300_belter_destroyer-cyl3.1-0 ; 
       cap300_belter_destroyer-extru1.1-0 ; 
       cap300_belter_destroyer-extru2.3-0 ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-null1.49-0 ROOT ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap300/PICTURES/cap300 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-cap300-belter_destroyer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       cap300_belter_destroyer-t2d26.1-0 ; 
       cap300_belter_destroyer-t2d27.1-0 ; 
       cap300_belter_destroyer-t2d28.1-0 ; 
       cap300_belter_destroyer-t2d29.1-0 ; 
       cap300_belter_destroyer-t2d30.1-0 ; 
       cap300_belter_destroyer-t2d31.1-0 ; 
       cap300_belter_destroyer-t2d32.4-0 ; 
       cap300_belter_destroyer-t2d33.4-0 ; 
       cap300_belter_destroyer-t2d34.4-0 ; 
       cap300_belter_destroyer-t2d35.2-0 ; 
       cap300_belter_destroyer-t2d36.2-0 ; 
       cap300_belter_destroyer-t2d37.2-0 ; 
       cap300_belter_destroyer-t2d38.3-0 ; 
       cap300_belter_destroyer-t2d39.3-0 ; 
       cap300_belter_destroyer-t2d40.3-0 ; 
       cap300_belter_destroyer-t2d41.3-0 ; 
       cap300_belter_destroyer-t2d42.3-0 ; 
       cap300_belter_destroyer-t2d43.1-0 ; 
       cap300_belter_destroyer-t2d44.1-0 ; 
       cap300_belter_destroyer-t2d45.2-0 ; 
       cap300_belter_destroyer-t2d46.2-0 ; 
       cap300_belter_destroyer-t2d47.1-0 ; 
       cap300_belter_destroyer-t2d48.1-0 ; 
       cap300_belter_destroyer-t2d49.1-0 ; 
       cap300_belter_destroyer-t2d50.1-0 ; 
       cap300_belter_destroyer-t2d51.1-0 ; 
       cap300_belter_destroyer-t2d52.2-0 ; 
       cap300_belter_destroyer-t2d53.1-0 ; 
       cap300_belter_destroyer-t2d54.2-0 ; 
       cap300_belter_destroyer-t2d55.1-0 ; 
       cap300_belter_destroyer-t2d56.1-0 ; 
       cap300_belter_destroyer-t2d57.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 10 110 ; 
       2 0 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 12 110 ; 
       11 8 110 ; 
       13 10 110 ; 
       14 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 13 300 ; 
       0 26 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       2 31 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 0 300 ; 
       5 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       8 29 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 27 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 28 300 ; 
       10 30 300 ; 
       11 25 300 ; 
       13 21 300 ; 
       14 24 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 65 -4 0 MPRFLG 0 ; 
       14 SCHEM 67.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
