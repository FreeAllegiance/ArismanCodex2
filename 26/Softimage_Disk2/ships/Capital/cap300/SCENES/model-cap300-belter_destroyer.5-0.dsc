SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.7-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       cap300_belter_destroyer-blthrust.1-0 ; 
       cap300_belter_destroyer-brthrust.1-0 ; 
       cap300_belter_destroyer-cockpt.1-0 ; 
       cap300_belter_destroyer-cube1.1-0 ; 
       cap300_belter_destroyer-cube2.1-0 ; 
       cap300_belter_destroyer-cube4.1-0 ; 
       cap300_belter_destroyer-cyl1.1-0 ; 
       cap300_belter_destroyer-cyl1_1.1-0 ; 
       cap300_belter_destroyer-cyl1_2.1-0 ; 
       cap300_belter_destroyer-cyl1_3.1-0 ; 
       cap300_belter_destroyer-cyl1_4.1-0 ; 
       cap300_belter_destroyer-cyl12.1-0 ; 
       cap300_belter_destroyer-cyl2.1-0 ; 
       cap300_belter_destroyer-cyl3.1-0 ; 
       cap300_belter_destroyer-extru1.1-0 ; 
       cap300_belter_destroyer-extru2.3-0 ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-lwepemt.1-0 ; 
       cap300_belter_destroyer-missemt.1-0 ; 
       cap300_belter_destroyer-mwepemt.1-0 ; 
       cap300_belter_destroyer-null1.4-0 ROOT ; 
       cap300_belter_destroyer-rsthrust.1-0 ; 
       cap300_belter_destroyer-rwepemt.1-0 ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere4.1-0 ; 
       cap300_belter_destroyer-SS01.1-0 ; 
       cap300_belter_destroyer-SS02.1-0 ; 
       cap300_belter_destroyer-SS03.1-0 ; 
       cap300_belter_destroyer-SS04.1-0 ; 
       cap300_belter_destroyer-SS05.1-0 ; 
       cap300_belter_destroyer-SS06.1-0 ; 
       cap300_belter_destroyer-SS07.1-0 ; 
       cap300_belter_destroyer-SS08.1-0 ; 
       cap300_belter_destroyer-tlsmoke.1-0 ; 
       cap300_belter_destroyer-tlthrust.1-0 ; 
       cap300_belter_destroyer-trail.1-0 ; 
       cap300_belter_destroyer-trsmoke.1-0 ; 
       cap300_belter_destroyer-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap300-belter_destroyer.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 20 110 ; 
       2 20 110 ; 
       3 14 110 ; 
       4 15 110 ; 
       5 3 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 20 110 ; 
       16 13 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       33 34 111 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 0 300 ; 
       26 1 300 ; 
       27 3 300 ; 
       28 2 300 ; 
       29 5 300 ; 
       30 4 300 ; 
       31 6 300 ; 
       32 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 229.4177 16.21856 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 229.4177 14.21856 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 212.1164 5.540003 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 215.7852 5.573097 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 221.9608 9.8344 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 247.0279 21.11046 0 MPRFLG 0 ; 
       4 SCHEM 250.7779 23.11046 0 MPRFLG 0 ; 
       5 SCHEM 247.0279 19.11046 0 MPRFLG 0 ; 
       6 SCHEM 233.2779 21.11046 0 MPRFLG 0 ; 
       7 SCHEM 238.2779 21.11046 0 MPRFLG 0 ; 
       8 SCHEM 240.7779 21.11046 0 MPRFLG 0 ; 
       9 SCHEM 243.2779 21.11046 0 MPRFLG 0 ; 
       10 SCHEM 245.7779 17.11046 0 MPRFLG 0 ; 
       11 SCHEM 248.2779 17.11046 0 MPRFLG 0 ; 
       12 SCHEM 230.7779 23.11046 0 MPRFLG 0 ; 
       13 SCHEM 238.2779 23.11046 0 MPRFLG 0 ; 
       14 SCHEM 247.0279 23.11046 0 MPRFLG 0 ; 
       15 SCHEM 243.2779 25.11046 0 USR MPRFLG 0 ; 
       16 SCHEM 235.7779 21.11046 0 MPRFLG 0 ; 
       17 SCHEM 224.5143 8.717731 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 222.0143 8.717731 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 222.0391 8.050734 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 225.5276 30.32864 0 USR SRT 0.3007 0.3007 0.3007 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 213.9356 8.956538 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 219.5143 8.717731 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 255.7779 23.11046 0 MPRFLG 0 ; 
       24 SCHEM 253.2779 23.11046 0 MPRFLG 0 ; 
       25 SCHEM 209.3246 23.00282 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 209.3873 22.23665 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 211.7619 23.05566 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 211.7189 22.21023 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 214.1827 22.94998 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 214.2257 22.23665 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 216.555 22.87839 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 216.4892 22.21658 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 212.1463 7.231144 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 212.1691 6.365113 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 213.5056 10.96898 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 215.9349 7.230182 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 215.8391 6.48544 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
