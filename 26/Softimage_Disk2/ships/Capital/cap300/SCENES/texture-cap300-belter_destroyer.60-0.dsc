SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.67-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       cap300_belter_destroyer-mat108.1-0 ; 
       cap300_belter_destroyer-mat109.1-0 ; 
       cap300_belter_destroyer-mat110.1-0 ; 
       cap300_belter_destroyer-mat111.1-0 ; 
       cap300_belter_destroyer-mat112.1-0 ; 
       cap300_belter_destroyer-mat113.1-0 ; 
       cap300_belter_destroyer-mat114.3-0 ; 
       cap300_belter_destroyer-mat115.3-0 ; 
       cap300_belter_destroyer-mat116.3-0 ; 
       cap300_belter_destroyer-mat117.2-0 ; 
       cap300_belter_destroyer-mat118.2-0 ; 
       cap300_belter_destroyer-mat119.2-0 ; 
       cap300_belter_destroyer-mat120.2-0 ; 
       cap300_belter_destroyer-mat121.2-0 ; 
       cap300_belter_destroyer-mat122.2-0 ; 
       cap300_belter_destroyer-mat123.2-0 ; 
       cap300_belter_destroyer-mat124.2-0 ; 
       cap300_belter_destroyer-mat125.1-0 ; 
       cap300_belter_destroyer-mat126.1-0 ; 
       cap300_belter_destroyer-mat127.1-0 ; 
       cap300_belter_destroyer-mat128.1-0 ; 
       cap300_belter_destroyer-mat129.1-0 ; 
       cap300_belter_destroyer-mat130.1-0 ; 
       cap300_belter_destroyer-mat131.1-0 ; 
       cap300_belter_destroyer-mat132.1-0 ; 
       cap300_belter_destroyer-mat133.1-0 ; 
       cap300_belter_destroyer-mat134.1-0 ; 
       cap300_belter_destroyer-mat135.1-0 ; 
       cap300_belter_destroyer-mat136.1-0 ; 
       cap300_belter_destroyer-mat137.1-0 ; 
       cap300_belter_destroyer-mat138.1-0 ; 
       cap300_belter_destroyer-mat139.1-0 ; 
       cap300_belter_destroyer-mat140.1-0 ; 
       cap300_belter_destroyer-mat141.1-0 ; 
       cap300_belter_destroyer-mat142.1-0 ; 
       cap300_belter_destroyer-mat143.1-0 ; 
       cap300_belter_destroyer-mat144.2-0 ; 
       cap300_belter_destroyer-mat145.1-0 ; 
       cap300_belter_destroyer-mat146.1-0 ; 
       cap300_belter_destroyer-mat147.1-0 ; 
       cap300_belter_destroyer-mat148.1-0 ; 
       cap300_belter_destroyer-mat149.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       cap300_belter_destroyer-blthrust.1-0 ; 
       cap300_belter_destroyer-brthrust.1-0 ; 
       cap300_belter_destroyer-cockpt.1-0 ; 
       cap300_belter_destroyer-cube1.1-0 ; 
       cap300_belter_destroyer-cube2.1-0 ; 
       cap300_belter_destroyer-cube4.1-0 ; 
       cap300_belter_destroyer-cyl1_10.1-0 ; 
       cap300_belter_destroyer-cyl1_5.1-0 ; 
       cap300_belter_destroyer-cyl1_8.1-0 ; 
       cap300_belter_destroyer-cyl1_9.1-0 ; 
       cap300_belter_destroyer-cyl12.1-0 ; 
       cap300_belter_destroyer-cyl13.1-0 ; 
       cap300_belter_destroyer-cyl2.1-0 ; 
       cap300_belter_destroyer-cyl3.1-0 ; 
       cap300_belter_destroyer-extru1.1-0 ; 
       cap300_belter_destroyer-extru2.3-0 ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-lwepemt.1-0 ; 
       cap300_belter_destroyer-missemt.1-0 ; 
       cap300_belter_destroyer-mwepemt.1-0 ; 
       cap300_belter_destroyer-null1.48-0 ROOT ; 
       cap300_belter_destroyer-rsthrust.1-0 ; 
       cap300_belter_destroyer-rwepemt.1-0 ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere5.1-0 ; 
       cap300_belter_destroyer-SS01.1-0 ; 
       cap300_belter_destroyer-SS02.1-0 ; 
       cap300_belter_destroyer-SS03.1-0 ; 
       cap300_belter_destroyer-SS04.1-0 ; 
       cap300_belter_destroyer-SS05.1-0 ; 
       cap300_belter_destroyer-SS06.1-0 ; 
       cap300_belter_destroyer-SS07.1-0 ; 
       cap300_belter_destroyer-SS08.1-0 ; 
       cap300_belter_destroyer-tlsmoke.1-0 ; 
       cap300_belter_destroyer-tlthrust.1-0 ; 
       cap300_belter_destroyer-trail.1-0 ; 
       cap300_belter_destroyer-trsmoke.1-0 ; 
       cap300_belter_destroyer-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap300/PICTURES/cap300 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-cap300-belter_destroyer.60-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       cap300_belter_destroyer-t2d26.1-0 ; 
       cap300_belter_destroyer-t2d27.1-0 ; 
       cap300_belter_destroyer-t2d28.1-0 ; 
       cap300_belter_destroyer-t2d29.1-0 ; 
       cap300_belter_destroyer-t2d30.1-0 ; 
       cap300_belter_destroyer-t2d31.1-0 ; 
       cap300_belter_destroyer-t2d32.4-0 ; 
       cap300_belter_destroyer-t2d33.4-0 ; 
       cap300_belter_destroyer-t2d34.4-0 ; 
       cap300_belter_destroyer-t2d35.2-0 ; 
       cap300_belter_destroyer-t2d36.2-0 ; 
       cap300_belter_destroyer-t2d37.2-0 ; 
       cap300_belter_destroyer-t2d38.3-0 ; 
       cap300_belter_destroyer-t2d39.3-0 ; 
       cap300_belter_destroyer-t2d40.3-0 ; 
       cap300_belter_destroyer-t2d41.3-0 ; 
       cap300_belter_destroyer-t2d42.3-0 ; 
       cap300_belter_destroyer-t2d43.1-0 ; 
       cap300_belter_destroyer-t2d44.1-0 ; 
       cap300_belter_destroyer-t2d45.2-0 ; 
       cap300_belter_destroyer-t2d46.2-0 ; 
       cap300_belter_destroyer-t2d47.1-0 ; 
       cap300_belter_destroyer-t2d48.1-0 ; 
       cap300_belter_destroyer-t2d49.1-0 ; 
       cap300_belter_destroyer-t2d50.1-0 ; 
       cap300_belter_destroyer-t2d51.1-0 ; 
       cap300_belter_destroyer-t2d52.2-0 ; 
       cap300_belter_destroyer-t2d53.1-0 ; 
       cap300_belter_destroyer-t2d54.2-0 ; 
       cap300_belter_destroyer-t2d55.1-0 ; 
       cap300_belter_destroyer-t2d56.1-0 ; 
       cap300_belter_destroyer-t2d57.1-0 ; 
       cap300_belter_destroyer-t2d58.1-0 ; 
       cap300_belter_destroyer-t2d59.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 20 110 ; 
       2 20 110 ; 
       3 14 110 ; 
       4 15 110 ; 
       5 3 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 20 110 ; 
       16 13 110 ; 
       17 20 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 12 300 ; 
       3 13 300 ; 
       3 34 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       5 39 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 40 300 ; 
       11 41 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 37 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 35 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 36 300 ; 
       15 38 300 ; 
       16 33 300 ; 
       23 21 300 ; 
       24 24 300 ; 
       25 28 300 ; 
       26 27 300 ; 
       27 29 300 ; 
       28 30 300 ; 
       29 31 300 ; 
       30 32 300 ; 
       31 26 300 ; 
       32 25 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 33 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 MPRFLG 0 ; 
       2 SCHEM 45 -2 0 MPRFLG 0 ; 
       3 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 118.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 95 -8 0 MPRFLG 0 ; 
       6 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 73.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 68.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 92.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 95 -10 0 MPRFLG 0 ; 
       12 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 75 -4 0 MPRFLG 0 ; 
       14 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       16 SCHEM 60 -6 0 MPRFLG 0 ; 
       17 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 50 -2 0 MPRFLG 0 ; 
       20 SCHEM 70 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 125 -4 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 5 -2 0 MPRFLG 0 ; 
       27 SCHEM 10 -2 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 25 -2 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 40 -2 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       33 SCHEM 15 -2 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       35 SCHEM 20 -2 0 MPRFLG 0 ; 
       36 SCHEM 35 -2 0 MPRFLG 0 ; 
       37 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
