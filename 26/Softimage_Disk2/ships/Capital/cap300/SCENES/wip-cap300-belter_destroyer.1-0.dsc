SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.1-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       cap300_belter_destroyer-cube1.1-0 ; 
       cap300_belter_destroyer-cyl1.1-0 ROOT ; 
       cap300_belter_destroyer-cyl2.1-0 ROOT ; 
       cap300_belter_destroyer-cyl3.1-0 ; 
       cap300_belter_destroyer-extru1.1-0 ; 
       cap300_belter_destroyer-extru2.1-0 ROOT ; 
       cap300_belter_destroyer-extru3.1-0 ROOT ; 
       cap300_belter_destroyer-sphere1.1-0 ; 
       cap300_belter_destroyer-sphere2.1-0 ROOT ; 
       cap300_belter_destroyer-spline1.1-0 ROOT ; 
       cap300_belter_destroyer-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap300-belter_destroyer.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 5 110 ; 
       0 4 110 ; 
       7 5 110 ; 
       3 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 MPRFLG 0 ; 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 1.570796 -13.17322 1.995316 -7.816543 MPRFLG 0 ; 
       2 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 1.570796 -13.58605 1.995316 -7.695909 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 0 0 DISPLAY 0 0 SRT 0.2224231 0.3985126 0.5158316 1.570796 0 0 3.141153 1.969442 -2.747298 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1.19424 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 22.5 0 0 SRT 1.301009 1.301009 1.752028 0 1.570796 0 -3.222817 0.001139998 -12.23854 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
