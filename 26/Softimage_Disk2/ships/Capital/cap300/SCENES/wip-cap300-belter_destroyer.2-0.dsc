SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.2-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       cap300_belter_destroyer-cube1.1-0 ; 
       cap300_belter_destroyer-cube2.1-0 ; 
       cap300_belter_destroyer-cube3.1-0 ROOT ; 
       cap300_belter_destroyer-cyl1.1-0 ; 
       cap300_belter_destroyer-cyl1_1.1-0 ; 
       cap300_belter_destroyer-cyl1_2.1-0 ; 
       cap300_belter_destroyer-cyl1_3.1-0 ; 
       cap300_belter_destroyer-cyl2.1-0 ; 
       cap300_belter_destroyer-cyl3.1-0 ; 
       cap300_belter_destroyer-cyl4.1-0 ROOT ; 
       cap300_belter_destroyer-extru1.1-0 ; 
       cap300_belter_destroyer-extru2.2-0 ROOT ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-sphere1.1-0 ROOT ; 
       cap300_belter_destroyer-sphere2.1-0 ROOT ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere4.1-0 ; 
       cap300_belter_destroyer-spline1.1-0 ROOT ; 
       cap300_belter_destroyer-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap300-belter_destroyer.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 11 110 ; 
       0 10 110 ; 
       3 8 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       12 8 110 ; 
       1 11 110 ; 
       4 8 110 ; 
       6 8 110 ; 
       5 8 110 ; 
       15 11 110 ; 
       16 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 10 -2 0 DISPLAY 0 0 SRT 0.2226513 0.2664818 0.617345 0 0 0 -4.736667 -0.02870262 -10.9074 MPRFLG 0 ; 
       14 SCHEM 5 0 0 DISPLAY 0 0 SRT 0.2224231 0.3985126 0.5158316 1.570796 0 0 3.141153 1.969442 -2.747298 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 MPRFLG 0 ; 
       18 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 23.75 0 0 SRT 1 1 1.19424 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 MPRFLG 0 ; 
       9 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0.5235988 -13.00079 2.000541 -7.816543 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 40 0 0 DISPLAY 0 0 SRT 0.3011105 1 1 0 0 0 1.097036 0.1005615 0 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
