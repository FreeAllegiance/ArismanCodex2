SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.63-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       cap301_belter_frigate-mat100.2-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap301_belter_frigate-mat102.1-0 ; 
       cap301_belter_frigate-mat103.1-0 ; 
       cap301_belter_frigate-mat104.1-0 ; 
       cap301_belter_frigate-mat105.1-0 ; 
       cap301_belter_frigate-mat106.1-0 ; 
       cap301_belter_frigate-mat107.1-0 ; 
       cap301_belter_frigate-mat108.1-0 ; 
       cap301_belter_frigate-mat109.1-0 ; 
       cap301_belter_frigate-mat110.1-0 ; 
       cap301_belter_frigate-mat83.3-0 ; 
       cap301_belter_frigate-mat84.2-0 ; 
       cap301_belter_frigate-mat85.1-0 ; 
       cap301_belter_frigate-mat86.2-0 ; 
       cap301_belter_frigate-mat87.3-0 ; 
       cap301_belter_frigate-mat88.2-0 ; 
       cap301_belter_frigate-mat89.3-0 ; 
       cap301_belter_frigate-mat90.2-0 ; 
       cap301_belter_frigate-mat91.6-0 ; 
       cap301_belter_frigate-mat92.5-0 ; 
       cap301_belter_frigate-mat93.4-0 ; 
       cap301_belter_frigate-mat94.3-0 ; 
       cap301_belter_frigate-mat95.3-0 ; 
       cap301_belter_frigate-mat96.2-0 ; 
       cap301_belter_frigate-mat97.1-0 ; 
       cap301_belter_frigate-mat98.1-0 ; 
       cap301_belter_frigate-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       cap301_belter_frigate-cap302.45-0 ROOT ; 
       cap301_belter_frigate-cube18.1-0 ; 
       cap301_belter_frigate-cube20.1-0 ; 
       cap301_belter_frigate-cube21.1-0 ; 
       cap301_belter_frigate-cube22.1-0 ; 
       cap301_belter_frigate-cube23.1-0 ; 
       cap301_belter_frigate-cube24.1-0 ; 
       cap301_belter_frigate-cube25.1-0 ; 
       cap301_belter_frigate-cube27.1-0 ; 
       cap301_belter_frigate-cube6.2-0 ; 
       cap301_belter_frigate-cube6_1.1-0 ; 
       cap301_belter_frigate-cyl14.1-0 ; 
       cap301_belter_frigate-cyl15.1-0 ; 
       cap301_belter_frigate-sphere10.1-0 ; 
       cap301_belter_frigate-sphere11.1-0 ; 
       cap301_belter_frigate-sphere12.1-0 ; 
       cap301_belter_frigate-sphere13.1-0 ; 
       cap301_belter_frigate-sphere6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap301/PICTURES/cap301 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-cap301-belter_frigate.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       cap301_belter_frigate-t2d1.5-0 ; 
       cap301_belter_frigate-t2d10.4-0 ; 
       cap301_belter_frigate-t2d11.5-0 ; 
       cap301_belter_frigate-t2d12.5-0 ; 
       cap301_belter_frigate-t2d13.4-0 ; 
       cap301_belter_frigate-t2d14.3-0 ; 
       cap301_belter_frigate-t2d15.1-0 ; 
       cap301_belter_frigate-t2d16.1-0 ; 
       cap301_belter_frigate-t2d17.2-0 ; 
       cap301_belter_frigate-t2d18.2-0 ; 
       cap301_belter_frigate-t2d19.1-0 ; 
       cap301_belter_frigate-t2d2.4-0 ; 
       cap301_belter_frigate-t2d20.1-0 ; 
       cap301_belter_frigate-t2d21.2-0 ; 
       cap301_belter_frigate-t2d22.2-0 ; 
       cap301_belter_frigate-t2d23.2-0 ; 
       cap301_belter_frigate-t2d24.1-0 ; 
       cap301_belter_frigate-t2d25.1-0 ; 
       cap301_belter_frigate-t2d26.2-0 ; 
       cap301_belter_frigate-t2d27.1-0 ; 
       cap301_belter_frigate-t2d28.1-0 ; 
       cap301_belter_frigate-t2d3.3-0 ; 
       cap301_belter_frigate-t2d4.2-0 ; 
       cap301_belter_frigate-t2d5.5-0 ; 
       cap301_belter_frigate-t2d6.3-0 ; 
       cap301_belter_frigate-t2d7.5-0 ; 
       cap301_belter_frigate-t2d8.4-0 ; 
       cap301_belter_frigate-t2d9.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       2 9 110 ; 
       3 2 110 ; 
       4 9 110 ; 
       5 4 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 4 110 ; 
       9 0 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 2 110 ; 
       13 3 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 2 110 ; 
       17 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 14 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 27 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       4 22 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       5 9 300 ; 
       6 8 300 ; 
       7 7 300 ; 
       8 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 5 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 6 300 ; 
       12 0 300 ; 
       12 1 300 ; 
       13 21 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       13 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 10 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 20 401 ; 
       11 0 401 ; 
       12 11 401 ; 
       13 21 401 ; 
       14 22 401 ; 
       15 23 401 ; 
       16 24 401 ; 
       17 25 401 ; 
       18 26 401 ; 
       19 27 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       22 3 401 ; 
       23 4 401 ; 
       24 5 401 ; 
       25 6 401 ; 
       26 7 401 ; 
       27 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 60 -4 0 MPRFLG 0 ; 
       2 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 41.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 70 -4 0 MPRFLG 0 ; 
       15 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 MPRFLG 0 ; 
       17 SCHEM 62.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
