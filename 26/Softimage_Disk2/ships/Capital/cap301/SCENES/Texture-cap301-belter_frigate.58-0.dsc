SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.61-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       cap301_belter_frigate-mat100.2-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap301_belter_frigate-mat102.1-0 ; 
       cap301_belter_frigate-mat103.1-0 ; 
       cap301_belter_frigate-mat104.1-0 ; 
       cap301_belter_frigate-mat105.1-0 ; 
       cap301_belter_frigate-mat106.1-0 ; 
       cap301_belter_frigate-mat107.1-0 ; 
       cap301_belter_frigate-mat108.1-0 ; 
       cap301_belter_frigate-mat109.1-0 ; 
       cap301_belter_frigate-mat110.1-0 ; 
       cap301_belter_frigate-mat83.3-0 ; 
       cap301_belter_frigate-mat84.2-0 ; 
       cap301_belter_frigate-mat85.1-0 ; 
       cap301_belter_frigate-mat86.2-0 ; 
       cap301_belter_frigate-mat87.3-0 ; 
       cap301_belter_frigate-mat88.2-0 ; 
       cap301_belter_frigate-mat89.3-0 ; 
       cap301_belter_frigate-mat90.2-0 ; 
       cap301_belter_frigate-mat91.6-0 ; 
       cap301_belter_frigate-mat92.5-0 ; 
       cap301_belter_frigate-mat93.4-0 ; 
       cap301_belter_frigate-mat94.3-0 ; 
       cap301_belter_frigate-mat95.3-0 ; 
       cap301_belter_frigate-mat96.2-0 ; 
       cap301_belter_frigate-mat97.1-0 ; 
       cap301_belter_frigate-mat98.1-0 ; 
       cap301_belter_frigate-mat99.1-0 ; 
       edit_nulls-mat70_1.2-0 ; 
       fig33_belter_recovery-mat71_1.2-0 ; 
       fig33_belter_recovery-mat75_1.2-0 ; 
       fig33_belter_recovery-mat77_1.2-0 ; 
       fig33_belter_recovery-mat78_1.2-0 ; 
       fig33_belter_recovery-mat80_1.2-0 ; 
       fig33_belter_recovery-mat81_1.2-0 ; 
       fig33_belter_recovery-mat82_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cap301_belter_frigate-blsmoke.1-0 ; 
       cap301_belter_frigate-blthrust.1-0 ; 
       cap301_belter_frigate-brthrust.1-0 ; 
       cap301_belter_frigate-cap302.43-0 ROOT ; 
       cap301_belter_frigate-cockpt.1-0 ; 
       cap301_belter_frigate-cube18.1-0 ; 
       cap301_belter_frigate-cube20.1-0 ; 
       cap301_belter_frigate-cube21.1-0 ; 
       cap301_belter_frigate-cube22.1-0 ; 
       cap301_belter_frigate-cube23.1-0 ; 
       cap301_belter_frigate-cube24.1-0 ; 
       cap301_belter_frigate-cube25.1-0 ; 
       cap301_belter_frigate-cube27.1-0 ; 
       cap301_belter_frigate-cube6.2-0 ; 
       cap301_belter_frigate-cube6_1.1-0 ; 
       cap301_belter_frigate-cyl14.1-0 ; 
       cap301_belter_frigate-cyl15.1-0 ; 
       cap301_belter_frigate-missemt.1-0 ; 
       cap301_belter_frigate-sphere10.1-0 ; 
       cap301_belter_frigate-sphere11.1-0 ; 
       cap301_belter_frigate-sphere12.1-0 ; 
       cap301_belter_frigate-sphere13.1-0 ; 
       cap301_belter_frigate-sphere6.1-0 ; 
       cap301_belter_frigate-SS01.1-0 ; 
       cap301_belter_frigate-SS02.1-0 ; 
       cap301_belter_frigate-SS03.1-0 ; 
       cap301_belter_frigate-SS04.1-0 ; 
       cap301_belter_frigate-SS05.1-0 ; 
       cap301_belter_frigate-SS06.1-0 ; 
       cap301_belter_frigate-SS07.1-0 ; 
       cap301_belter_frigate-SS08.1-0 ; 
       cap301_belter_frigate-tlsmoke.1-0 ; 
       cap301_belter_frigate-tlthrust.1-0 ; 
       cap301_belter_frigate-trail.1-0 ; 
       cap301_belter_frigate-trthrust.1-0 ; 
       cap301_belter_frigate-turwepemt1.1-0 ; 
       cap301_belter_frigate-turwepemt2.1-0 ; 
       cap301_belter_frigate-turwepemt3.1-0 ; 
       cap301_belter_frigate-turwepemt4.1-0 ; 
       cap301_belter_frigate-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap301/PICTURES/cap301 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-cap301-belter_frigate.58-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       cap301_belter_frigate-t2d1.5-0 ; 
       cap301_belter_frigate-t2d10.4-0 ; 
       cap301_belter_frigate-t2d11.5-0 ; 
       cap301_belter_frigate-t2d12.5-0 ; 
       cap301_belter_frigate-t2d13.4-0 ; 
       cap301_belter_frigate-t2d14.3-0 ; 
       cap301_belter_frigate-t2d15.1-0 ; 
       cap301_belter_frigate-t2d16.1-0 ; 
       cap301_belter_frigate-t2d17.2-0 ; 
       cap301_belter_frigate-t2d18.2-0 ; 
       cap301_belter_frigate-t2d19.1-0 ; 
       cap301_belter_frigate-t2d2.4-0 ; 
       cap301_belter_frigate-t2d20.1-0 ; 
       cap301_belter_frigate-t2d21.2-0 ; 
       cap301_belter_frigate-t2d22.2-0 ; 
       cap301_belter_frigate-t2d23.2-0 ; 
       cap301_belter_frigate-t2d24.1-0 ; 
       cap301_belter_frigate-t2d25.1-0 ; 
       cap301_belter_frigate-t2d26.2-0 ; 
       cap301_belter_frigate-t2d27.1-0 ; 
       cap301_belter_frigate-t2d28.1-0 ; 
       cap301_belter_frigate-t2d3.3-0 ; 
       cap301_belter_frigate-t2d4.2-0 ; 
       cap301_belter_frigate-t2d5.5-0 ; 
       cap301_belter_frigate-t2d6.3-0 ; 
       cap301_belter_frigate-t2d7.5-0 ; 
       cap301_belter_frigate-t2d8.4-0 ; 
       cap301_belter_frigate-t2d9.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       0 1 111 ; 
       1 3 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 13 110 ; 
       6 13 110 ; 
       7 6 110 ; 
       8 13 110 ; 
       9 8 110 ; 
       10 13 110 ; 
       11 13 110 ; 
       13 3 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 6 110 ; 
       17 3 110 ; 
       18 7 110 ; 
       19 13 110 ; 
       20 13 110 ; 
       21 6 110 ; 
       22 13 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       31 3 110 ; 
       31 32 111 ; 
       32 3 110 ; 
       33 3 110 ; 
       34 3 110 ; 
       35 3 110 ; 
       36 3 110 ; 
       37 3 110 ; 
       38 3 110 ; 
       39 3 110 ; 
       12 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 14 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 27 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       8 22 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       9 9 300 ; 
       10 8 300 ; 
       11 7 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       14 15 300 ; 
       14 16 300 ; 
       14 5 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       15 6 300 ; 
       16 0 300 ; 
       16 1 300 ; 
       18 21 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 2 300 ; 
       23 28 300 ; 
       24 29 300 ; 
       25 31 300 ; 
       26 30 300 ; 
       27 33 300 ; 
       28 32 300 ; 
       29 34 300 ; 
       30 35 300 ; 
       12 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 0 401 ; 
       12 11 401 ; 
       13 21 401 ; 
       14 22 401 ; 
       15 23 401 ; 
       16 24 401 ; 
       17 25 401 ; 
       18 26 401 ; 
       19 27 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       22 3 401 ; 
       23 4 401 ; 
       24 5 401 ; 
       25 6 401 ; 
       26 7 401 ; 
       27 8 401 ; 
       0 9 401 ; 
       1 10 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 68.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 105 -4 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 81.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 MPRFLG 0 ; 
       10 SCHEM 110 -4 0 MPRFLG 0 ; 
       11 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 70 -4 0 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 MPRFLG 0 ; 
       16 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 78.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 115 -4 0 MPRFLG 0 ; 
       20 SCHEM 117.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 95 -6 0 MPRFLG 0 ; 
       22 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 132.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 135 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 127.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
