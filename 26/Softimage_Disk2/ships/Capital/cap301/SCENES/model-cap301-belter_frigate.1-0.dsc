SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.3-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70_1.1-0 ; 
       fig33_belter_recovery-mat71_1.1-0 ; 
       fig33_belter_recovery-mat75_1.1-0 ; 
       fig33_belter_recovery-mat77_1.1-0 ; 
       fig33_belter_recovery-mat78_1.1-0 ; 
       fig33_belter_recovery-mat80_1.1-0 ; 
       fig33_belter_recovery-mat81_1.1-0 ; 
       fig33_belter_recovery-mat82_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       cap301_belter_frigate-blsmoke.1-0 ; 
       cap301_belter_frigate-blthrust.1-0 ; 
       cap301_belter_frigate-brthrust.1-0 ; 
       cap301_belter_frigate-cap302.1-0 ROOT ; 
       cap301_belter_frigate-cockpt.1-0 ; 
       cap301_belter_frigate-cube13.1-0 ; 
       cap301_belter_frigate-cube18.1-0 ; 
       cap301_belter_frigate-cube20.1-0 ; 
       cap301_belter_frigate-cube21.1-0 ; 
       cap301_belter_frigate-cube22.1-0 ; 
       cap301_belter_frigate-cube23.1-0 ; 
       cap301_belter_frigate-cube24.1-0 ; 
       cap301_belter_frigate-cube25.1-0 ; 
       cap301_belter_frigate-cube6.2-0 ; 
       cap301_belter_frigate-cube6_1.1-0 ; 
       cap301_belter_frigate-cyl14.1-0 ; 
       cap301_belter_frigate-cyl15.1-0 ; 
       cap301_belter_frigate-missemt.1-0 ; 
       cap301_belter_frigate-sphere10.1-0 ; 
       cap301_belter_frigate-sphere11.1-0 ; 
       cap301_belter_frigate-sphere12.1-0 ; 
       cap301_belter_frigate-sphere13.1-0 ; 
       cap301_belter_frigate-sphere6.1-0 ; 
       cap301_belter_frigate-SS01.1-0 ; 
       cap301_belter_frigate-SS02.1-0 ; 
       cap301_belter_frigate-SS03.1-0 ; 
       cap301_belter_frigate-SS04.1-0 ; 
       cap301_belter_frigate-SS05.1-0 ; 
       cap301_belter_frigate-SS06.1-0 ; 
       cap301_belter_frigate-SS07.1-0 ; 
       cap301_belter_frigate-SS08.1-0 ; 
       cap301_belter_frigate-tlsmoke.1-0 ; 
       cap301_belter_frigate-tlthrust.1-0 ; 
       cap301_belter_frigate-trail.1-0 ; 
       cap301_belter_frigate-trthrust.1-0 ; 
       cap301_belter_frigate-turwepemt1.1-0 ; 
       cap301_belter_frigate-turwepemt2.1-0 ; 
       cap301_belter_frigate-turwepemt3.1-0 ; 
       cap301_belter_frigate-turwepemt4.1-0 ; 
       cap301_belter_frigate-wepemt.1-0 ; 
       turcone-175deg.2-0 ROOT ; 
       turcone2-175deg.2-0 ROOT ; 
       turcone4-175deg.2-0 ROOT ; 
       turcone5-175deg.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap301-belter_frigate.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 13 110 ; 
       12 13 110 ; 
       20 13 110 ; 
       21 7 110 ; 
       32 3 110 ; 
       1 3 110 ; 
       34 3 110 ; 
       0 3 110 ; 
       0 1 111 ; 
       4 3 110 ; 
       17 3 110 ; 
       31 3 110 ; 
       31 32 111 ; 
       2 3 110 ; 
       22 13 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       33 3 110 ; 
       35 3 110 ; 
       36 3 110 ; 
       37 3 110 ; 
       38 3 110 ; 
       39 3 110 ; 
       5 9 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 7 110 ; 
       9 13 110 ; 
       10 9 110 ; 
       13 3 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 7 110 ; 
       18 8 110 ; 
       19 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       23 0 300 ; 
       24 1 300 ; 
       25 3 300 ; 
       26 2 300 ; 
       27 5 300 ; 
       28 4 300 ; 
       29 6 300 ; 
       30 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 323.9759 -13.11055 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 323.9759 -15.11055 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 366.5558 -5.086864 0 USR MPRFLG 0 ; 
       12 SCHEM 369.0558 -5.086864 0 USR MPRFLG 0 ; 
       20 SCHEM 375.1255 -4.725019 0 USR MPRFLG 0 ; 
       21 SCHEM 361.5558 -7.086863 0 USR MPRFLG 0 ; 
       32 SCHEM 338.9193 -52.07442 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 338.8936 -52.77734 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 343.6749 -52.08025 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 338.9211 -54.1353 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 348.0022 -27.06664 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 347.6731 -47.85057 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 336.3779 -50.15261 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 338.8914 -53.49652 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 343.5835 -52.69444 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 363.4961 -8.820765 0 USR MPRFLG 0 ; 
       23 SCHEM 351.2758 -39.89801 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 351.3158 -40.53761 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 353.5949 -39.84433 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 353.4874 -40.48391 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 355.9751 -39.80233 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 355.9448 -40.50027 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 358.379 -39.83464 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 358.329 -40.561 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 347.6581 -47.04156 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 368.8063 -54.27313 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 377.9696 -49.82647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 380.8718 -49.82647 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 366.5023 -51.63293 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 336.4525 -49.28306 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 349.0558 -7.086863 0 USR MPRFLG 0 ; 
       6 SCHEM 361.5558 -5.086864 0 USR MPRFLG 0 ; 
       7 SCHEM 359.0558 -5.086864 0 USR MPRFLG 0 ; 
       8 SCHEM 356.5558 -7.086863 0 USR MPRFLG 0 ; 
       9 SCHEM 350.3058 -5.086864 0 USR MPRFLG 0 ; 
       10 SCHEM 351.5558 -7.086863 0 USR MPRFLG 0 ; 
       13 SCHEM 362.6918 4.00114 0 USR MPRFLG 0 ; 
       14 SCHEM 354.0558 -5.086864 0 USR MPRFLG 0 ; 
       15 SCHEM 346.5558 -5.086864 0 USR MPRFLG 0 ; 
       16 SCHEM 359.0558 -7.086863 0 USR MPRFLG 0 ; 
       18 SCHEM 356.5558 -9.086864 0 USR MPRFLG 0 ; 
       19 SCHEM 372.4514 -4.811601 0 USR MPRFLG 0 ; 
       40 SCHEM 359.9828 -26.96797 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2 -1.310796 0 0 -0.03563694 -1.72772 2.34242 MPRFLG 0 ; 
       41 SCHEM 369.1591 -22.77698 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.12 4.502388 3.354334e-007 3.141593 1.558876 -2.824206 -9.120399 MPRFLG 0 ; 
       42 SCHEM 371.8218 -22.89777 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.229487 2.229487 2.229434 0 1.570796 0 2.562784 -2.110495 -8.556831 MPRFLG 0 ; 
       43 SCHEM 358.1573 -24.88007 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 -5.100507e-007 1.650796 3.141592 -1.560829 -1.664017 -8.915338 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 287.7932 -14.23683 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 287.8332 -14.87642 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 290.0048 -14.82273 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 290.1123 -14.18314 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 292.4622 -14.83908 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 292.4926 -14.14114 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 294.8964 -14.17344 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 294.8464 -14.89981 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
