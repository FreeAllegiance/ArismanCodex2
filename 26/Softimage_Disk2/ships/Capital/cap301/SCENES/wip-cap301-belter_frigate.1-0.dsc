SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.1-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.1-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
       fig33_belter_recovery1-cube13.1-0 ; 
       fig33_belter_recovery1-cube16.1-0 ; 
       fig33_belter_recovery1-cube17.1-0 ; 
       fig33_belter_recovery1-cube18.1-0 ; 
       fig33_belter_recovery1-cube19.1-0 ; 
       fig33_belter_recovery1-cube6.1-0 ROOT ; 
       fig33_belter_recovery1-cube6_1.1-0 ; 
       fig33_belter_recovery1-cyl1_10.1-0 ; 
       fig33_belter_recovery1-cyl1_11.1-0 ; 
       fig33_belter_recovery1-cyl1_12.1-0 ; 
       fig33_belter_recovery1-cyl1_13.1-0 ; 
       fig33_belter_recovery1-cyl1_8.1-0 ; 
       fig33_belter_recovery1-sphere5.1-0 ; 
       fig33_belter_recovery1-sphere6.1-0 ROOT ; 
       fig33_belter_recovery1-sphere7.1-0 ROOT ; 
       fig33_belter_recovery1-sphere8.1-0 ROOT ; 
       fig33_belter_recovery1-sphere9.1-0 ROOT ; 
       fig33_belter_recovery2-cube6.1-0 ROOT ; 
       fig33_belter_recovery3-cube20.1-0 ; 
       fig33_belter_recovery3-cube21.1-0 ; 
       fig33_belter_recovery3-cube22.1-0 ; 
       fig33_belter_recovery3-cube6.1-0 ROOT ; 
       fig33_belter_recovery3-cube6_1.1-0 ; 
       fig33_belter_recovery3-cyl14.1-0 ; 
       fig33_belter_recovery3-sphere10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap301-belter_frigate.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       43 41 110 ; 
       21 22 110 ; 
       22 25 110 ; 
       30 25 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       44 39 110 ; 
       38 41 110 ; 
       39 38 110 ; 
       40 41 110 ; 
       42 41 110 ; 
       0 7 110 ; 
       0 1 111 ; 
       1 7 110 ; 
       2 7 110 ; 
       20 25 110 ; 
       31 32 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       6 8 111 ; 
       6 5 111 ; 
       8 7 110 ; 
       32 25 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       18 19 111 ; 
       19 7 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 0 300 ; 
       10 1 300 ; 
       11 3 300 ; 
       12 2 300 ; 
       13 5 300 ; 
       14 4 300 ; 
       15 6 300 ; 
       16 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       33 SCHEM 33.30963 -1.337178 0 USR DISPLAY 0 0 SRT 0.117364 0.09468459 0.117364 0 0 0 -0.9630483 -1.030138 -8.892716 MPRFLG 0 ; 
       27 SCHEM 7.42799 -14.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 9.927989 -14.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 12.42798 -14.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 39.44421 -1.68149 0 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 22.42798 -14.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 22.42798 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 35.29385 -2.288176 0 USR SRT 0.117364 0.09468459 0.117364 3.141593 0 0 -1.718289 2.303142 1.415792 MPRFLG 0 ; 
       30 SCHEM 27.42798 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 32.71162 -2.005783 0 USR SRT 0.1161904 0.09373774 0.1161904 1.570796 1.570796 0 -2.637809 1.697221 1.677076 MPRFLG 0 ; 
       36 SCHEM 35.45664 -1.057692 0 USR SRT 0.1161904 0.09373774 0.1161904 -0.7853983 1.570796 0.7853982 -1.557357 2.007872 2.508617 MPRFLG 0 ; 
       23 SCHEM 29.92799 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 32.42799 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 47.53825 -2.234916 0 MPRFLG 0 ; 
       37 SCHEM 13.68555 30.50466 0 USR DISPLAY 0 0 SRT 0.731064 0.776 1 0 0 0 -0.777372 -2.13863 0.003023505 MPRFLG 0 ; 
       38 SCHEM 47.53825 1.765084 0 USR MPRFLG 0 ; 
       39 SCHEM 47.53825 -0.2349156 0 MPRFLG 0 ; 
       40 SCHEM 41.94421 -1.68149 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 40.28485 1.094116 0 USR SRT 0.925527 1.089504 1 0 0 0 -0.06378517 -2.344778 0.4092499 MPRFLG 0 ; 
       42 SCHEM 47.24242 4.1315 0 USR MPRFLG 0 ; 
       0 SCHEM 15.77859 -9.502227 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 13.57927 -9.57619 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9.486986 -5.901116 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 2.42799 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 4.927989 -14.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 8.467607 -8.907054 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 8.410707 -9.579569 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 16.91921 -5.332502 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 15.752 -6.311308 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 5.253734 0.7280385 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 16.76196 -7.275861 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 4.927989 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM -5.747458 -10.39818 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM -5.707415 -11.03777 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM -3.428298 -10.34449 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM -3.535852 -10.98408 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM -1.048068 -10.3025 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM -1.078448 -11.00043 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 1.355752 -10.3348 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 1.305772 -11.06116 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 9.472017 -5.092104 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 15.74891 -8.863462 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 13.62866 -8.85351 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 17.42797 -10.08345 0 USR DISPLAY 0 0 SRT 0.731064 1 1 0 0 0 0.002161118 0.03731348 0.003023505 MPRFLG 0 ; 
       26 SCHEM 11.17798 -12.08345 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64.77222 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.3026 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
