SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.2-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       edit_nulls-mat70.1-0 ; 
       edit_nulls-mat70_1.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat71_1.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat75_1.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat77_1.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat78_1.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat80_1.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat81_1.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
       fig33_belter_recovery-mat82_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 86     
       cap300_belter_destroyer-cyl1.1-0 ; 
       cap300_belter_destroyer-cyl1_1.1-0 ; 
       cap300_belter_destroyer-cyl1_2.1-0 ; 
       cap300_belter_destroyer-cyl1_3.1-0 ; 
       cap300_belter_destroyer-cyl3.1-0 ROOT ; 
       cap300_belter_destroyer1-cyl3.1-0 ROOT ; 
       cap302-1thrust.1-0 ; 
       cap302-2thrust.1-0 ; 
       cap302-3thrust.1-0 ; 
       cap302-bsmoke.1-0 ; 
       cap302-bthrust.1-0 ; 
       cap302-cap302.1-0 ROOT ; 
       cap302-cockpt.1-0 ; 
       cap302-missemt.1-0 ; 
       cap302-rsmoke.1-0 ; 
       cap302-rthrust.1-0 ; 
       cap302-sphere6.1-0 ROOT ; 
       cap302-sphere7.1-0 ROOT ; 
       cap302-SS01.1-0 ; 
       cap302-SS02.1-0 ; 
       cap302-SS03.1-0 ; 
       cap302-SS04.1-0 ; 
       cap302-SS05.1-0 ; 
       cap302-SS06.1-0 ; 
       cap302-SS07.1-0 ; 
       cap302-SS08.1-0 ; 
       cap302-trail.1-0 ; 
       cap302-turwepemt1.1-0 ; 
       cap302-turwepemt2.1-0 ; 
       cap302-turwepemt3.1-0 ; 
       cap302-turwepemt4.1-0 ; 
       cap302-wepemt.1-0 ; 
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.2-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
       fig33_belter_recovery1-cube13.1-0 ; 
       fig33_belter_recovery1-cube16.1-0 ; 
       fig33_belter_recovery1-cube17.1-0 ; 
       fig33_belter_recovery1-cube18.1-0 ; 
       fig33_belter_recovery1-cube19.1-0 ; 
       fig33_belter_recovery1-cube6.2-0 ROOT ; 
       fig33_belter_recovery1-cube6_1.1-0 ; 
       fig33_belter_recovery1-cyl1_10.1-0 ; 
       fig33_belter_recovery1-cyl1_11.1-0 ; 
       fig33_belter_recovery1-cyl1_12.1-0 ; 
       fig33_belter_recovery1-cyl1_13.1-0 ; 
       fig33_belter_recovery1-cyl1_8.1-0 ; 
       fig33_belter_recovery1-sphere5.1-0 ; 
       fig33_belter_recovery1-sphere6.2-0 ROOT ; 
       fig33_belter_recovery1-sphere7.2-0 ROOT ; 
       fig33_belter_recovery1-sphere8.2-0 ROOT ; 
       fig33_belter_recovery1-sphere9.2-0 ROOT ; 
       fig33_belter_recovery2-cube6.2-0 ROOT ; 
       fig33_belter_recovery3-cube13.1-0 ; 
       fig33_belter_recovery3-cube18.1-0 ; 
       fig33_belter_recovery3-cube20.1-0 ; 
       fig33_belter_recovery3-cube21.1-0 ; 
       fig33_belter_recovery3-cube22.1-0 ; 
       fig33_belter_recovery3-cube23.1-0 ; 
       fig33_belter_recovery3-cube6.2-0 ROOT ; 
       fig33_belter_recovery3-cube6_1.1-0 ; 
       fig33_belter_recovery3-cyl14.1-0 ; 
       fig33_belter_recovery3-cyl15.1-0 ; 
       fig33_belter_recovery3-sphere10.1-0 ; 
       fig33_belter_recovery3-sphere11.1-0 ; 
       turcone-175deg.1-0 ROOT ; 
       turcone2-175deg.1-0 ROOT ; 
       turcone4-175deg.1-0 ROOT ; 
       turcone5-175deg.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap301-belter_frigate.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       9 10 111 ; 
       10 11 110 ; 
       12 11 110 ; 
       70 74 110 ; 
       81 76 110 ; 
       71 76 110 ; 
       79 72 110 ; 
       75 74 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       32 39 110 ; 
       32 33 111 ; 
       15 11 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       27 82 111 ; 
       27 82 114 ; 
       28 11 110 ; 
       28 83 114 ; 
       28 83 111 ; 
       29 11 110 ; 
       29 84 111 ; 
       29 84 114 ; 
       30 11 110 ; 
       30 85 114 ; 
       30 85 111 ; 
       31 11 110 ; 
       33 39 110 ; 
       34 39 110 ; 
       35 39 110 ; 
       36 39 110 ; 
       37 39 110 ; 
       38 39 110 ; 
       38 40 111 ; 
       38 37 111 ; 
       40 39 110 ; 
       41 39 110 ; 
       42 39 110 ; 
       43 39 110 ; 
       44 39 110 ; 
       45 39 110 ; 
       46 39 110 ; 
       47 39 110 ; 
       48 39 110 ; 
       49 39 110 ; 
       50 39 110 ; 
       50 51 111 ; 
       51 39 110 ; 
       52 57 110 ; 
       53 54 110 ; 
       54 57 110 ; 
       55 57 110 ; 
       56 57 110 ; 
       58 57 110 ; 
       59 58 110 ; 
       60 58 110 ; 
       61 58 110 ; 
       62 57 110 ; 
       63 64 110 ; 
       64 57 110 ; 
       72 76 110 ; 
       73 72 110 ; 
       74 76 110 ; 
       77 76 110 ; 
       78 76 110 ; 
       80 73 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       18 1 300 ; 
       19 3 300 ; 
       20 7 300 ; 
       21 5 300 ; 
       22 11 300 ; 
       23 9 300 ; 
       24 13 300 ; 
       25 15 300 ; 
       41 0 300 ; 
       42 2 300 ; 
       43 6 300 ; 
       44 4 300 ; 
       45 10 300 ; 
       46 8 300 ; 
       47 12 300 ; 
       48 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 318.1494 32.6442 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 318.1494 30.6442 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 324.7889 -46.54484 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 327.0445 -46.55067 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 329.5445 -46.55067 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 327.8641 -44.86681 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 325.6648 -44.94077 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 333.8718 -28.9031 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 326.4946 -43.20681 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 338.4933 3.530006 0 MPRFLG 0 ; 
       81 SCHEM 398.1704 32.6442 0 USR MPRFLG 0 ; 
       5 SCHEM 400.6704 32.6442 0 USR DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 5.593464 -1.805677 -8.735506 MPRFLG 0 ; 
       71 SCHEM 350.9933 5.530006 0 MPRFLG 0 ; 
       79 SCHEM 348.4933 3.530006 0 MPRFLG 0 ; 
       75 SCHEM 340.9933 3.530006 0 MPRFLG 0 ; 
       0 SCHEM 330.934 -5.197095 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 335.934 -5.197095 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 338.434 -5.197095 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 340.934 -5.197095 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 335.934 -3.197095 0 USR DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 3.195365 -0.8200319 -2.660158 MPRFLG 0 ; 
       13 SCHEM 322.2475 -44.62303 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 327.8344 -44.22804 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 314.1284 -45.39536 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 325.7142 -44.21809 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 328.5888 -0.7829365 0 USR SRT 1 1 1 0 0 0 -0.06378517 -2.130682 2.28112 MPRFLG 0 ; 
       17 SCHEM 341.0889 -0.7829365 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 -1.97653 2.814987 -0.2275221 MPRFLG 0 ; 
       18 SCHEM 321.943 -48.38214 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 321.983 -49.02174 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 324.2621 -48.32846 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 324.1546 -48.96805 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 326.6423 -48.28646 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 326.612 -48.9844 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 329.0462 -48.31877 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 328.9962 -49.04513 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 326.4796 -42.3978 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 323.4024 -32.97911 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 325.4788 -32.97911 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 328.381 -32.97911 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 330.881 -32.97911 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 322.3221 -43.75348 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 306.8564 1.591117 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 -1.610796 0 0 0 1.140741 -7.108673 MPRFLG 0 ; 
       83 SCHEM 307.4619 2.662008 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 4.84239 -1.13583e-008 3.141593 0 -0.8713017 0.983948 MPRFLG 0 ; 
       84 SCHEM 302.9427 1.842513 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 0 1.570796 0 2.713119 0.2436029 -3.834418 MPRFLG 0 ; 
       85 SCHEM 304.0768 2.662008 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 -2.799527e-007 1.430795 3.141593 -1.133453 0.1806884 2.491355 MPRFLG 0 ; 
       33 SCHEM 311.9291 -45.46932 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 307.8368 -41.79425 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 306.8174 -44.80019 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 306.7605 -45.4727 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 315.269 -41.22563 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 314.1018 -42.20444 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 303.6035 -35.16509 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 315.1118 -43.16899 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 292.6023 -46.29131 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 292.6424 -46.9309 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 294.9215 -46.23763 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 294.8139 -46.87721 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 297.3017 -46.19563 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 297.2713 -46.89356 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 299.7055 -46.22794 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 299.6556 -46.95429 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 307.8218 -40.98524 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 314.0987 -44.75659 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 311.9785 -44.74664 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 300.3603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 320.3603 -25.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 320.3603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 327.8603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 330.3603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 315.3603 -21.88498 0 USR DISPLAY 0 0 SRT 0.731064 1 1 0 0 0 0.002161118 0.03731348 0.003023505 MPRFLG 0 ; 
       58 SCHEM 309.1103 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 305.3603 -25.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 307.8603 -25.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 310.3603 -25.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 325.3603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 302.8603 -25.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 302.8603 -23.88499 0 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 325.1859 4.253023 0 USR DISPLAY 0 0 SRT 0.117364 0.09468459 0.117364 0 0 0 -0.9630483 -1.030138 -8.892716 MPRFLG 0 ; 
       66 SCHEM 327.1701 3.302026 0 USR DISPLAY 0 0 SRT 0.117364 0.09468459 0.117364 3.141593 0 0 -1.718289 2.303142 1.415792 MPRFLG 0 ; 
       67 SCHEM 324.5879 3.584418 0 USR DISPLAY 0 0 SRT 0.1161904 0.09373774 0.1161904 1.570796 1.570796 0 -2.637809 1.697221 1.677076 MPRFLG 0 ; 
       68 SCHEM 327.3329 4.532509 0 USR DISPLAY 0 0 SRT 0.1161904 0.09373774 0.1161904 -0.7853983 1.570796 0.7853982 -1.557357 2.007872 2.508617 MPRFLG 0 ; 
       69 SCHEM 323.4193 7.212152 0 USR DISPLAY 0 0 SRT 0.731064 0.776 1 0 0 0 -0.777372 -2.13863 0.003023505 MPRFLG 0 ; 
       72 SCHEM 347.2433 5.530006 0 MPRFLG 0 ; 
       73 SCHEM 345.9933 3.530006 0 MPRFLG 0 ; 
       74 SCHEM 339.7433 5.530006 0 MPRFLG 0 ; 
       76 SCHEM 343.4933 7.530006 0 USR SRT 0.925527 1.089504 1 0 0 0 -0.06378517 -2.344778 0.4092499 MPRFLG 0 ; 
       77 SCHEM 343.4933 5.530006 0 MPRFLG 0 ; 
       78 SCHEM 335.9933 5.530006 0 MPRFLG 0 ; 
       80 SCHEM 345.9933 1.530006 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 287.7932 -14.23683 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 287.8332 -14.87642 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 290.0048 -14.82273 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 290.1123 -14.18314 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 292.4622 -14.83908 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 292.4926 -14.14114 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 294.8964 -14.17344 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 294.8464 -14.89981 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 338.3678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 340.8678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 348.3678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 345.8678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 360.8678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 363.3678 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 393.14 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 395.6704 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
