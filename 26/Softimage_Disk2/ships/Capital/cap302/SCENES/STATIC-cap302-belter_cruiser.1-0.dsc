SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.72-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       cap302_belter_cruiser-mat100.1-0 ; 
       cap302_belter_cruiser-mat101.2-0 ; 
       cap302_belter_cruiser-mat102.1-0 ; 
       cap302_belter_cruiser-mat103.1-0 ; 
       cap302_belter_cruiser-mat104.1-0 ; 
       cap302_belter_cruiser-mat105.1-0 ; 
       cap302_belter_cruiser-mat106.1-0 ; 
       cap302_belter_cruiser-mat108.1-0 ; 
       cap302_belter_cruiser-mat109.1-0 ; 
       cap302_belter_cruiser-mat112.1-0 ; 
       cap302_belter_cruiser-mat83.3-0 ; 
       cap302_belter_cruiser-mat84.2-0 ; 
       cap302_belter_cruiser-mat85.3-0 ; 
       cap302_belter_cruiser-mat86.3-0 ; 
       cap302_belter_cruiser-mat87.2-0 ; 
       cap302_belter_cruiser-mat88.1-0 ; 
       cap302_belter_cruiser-mat89.2-0 ; 
       cap302_belter_cruiser-mat90.2-0 ; 
       cap302_belter_cruiser-mat91.2-0 ; 
       cap302_belter_cruiser-mat92.2-0 ; 
       cap302_belter_cruiser-mat93.2-0 ; 
       cap302_belter_cruiser-mat94.1-0 ; 
       cap302_belter_cruiser-mat95.1-0 ; 
       cap302_belter_cruiser-mat96.1-0 ; 
       cap302_belter_cruiser-mat97.3-0 ; 
       cap302_belter_cruiser-mat98.3-0 ; 
       cap302_belter_cruiser-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       cap302-cap302.51-0 ROOT ; 
       cap302-cube13.1-0 ; 
       cap302-cube16.1-0 ; 
       cap302-cube17.1-0 ; 
       cap302-cube18.1-0 ; 
       cap302-cube19.1-0 ; 
       cap302-cube20.1-0 ; 
       cap302-cube22.1-0 ; 
       cap302-cube6.4-0 ; 
       cap302-cube6_1.1-0 ; 
       cap302-cyl1_10.1-0 ; 
       cap302-cyl1_13.1-0 ; 
       cap302-cyl1_14.1-0 ; 
       cap302-cyl1_15.1-0 ; 
       cap302-cyl1_8.1-0 ; 
       cap302-cyl14.1-0 ; 
       cap302-sphere5.1-0 ; 
       cap302-sphere6.1-0 ; 
       cap302-sphere7.1-0 ; 
       cap302-sphere8.1-0 ; 
       cap302-sphere9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap302/PICTURES/cap302 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-cap302-belter_cruiser.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       cap302_belter_cruiser-t2d1.5-0 ; 
       cap302_belter_cruiser-t2d10.2-0 ; 
       cap302_belter_cruiser-t2d11.2-0 ; 
       cap302_belter_cruiser-t2d12.2-0 ; 
       cap302_belter_cruiser-t2d13.3-0 ; 
       cap302_belter_cruiser-t2d14.2-0 ; 
       cap302_belter_cruiser-t2d15.3-0 ; 
       cap302_belter_cruiser-t2d16.1-0 ; 
       cap302_belter_cruiser-t2d17.2-0 ; 
       cap302_belter_cruiser-t2d18.2-0 ; 
       cap302_belter_cruiser-t2d19.1-0 ; 
       cap302_belter_cruiser-t2d2.3-0 ; 
       cap302_belter_cruiser-t2d20.1-0 ; 
       cap302_belter_cruiser-t2d21.1-0 ; 
       cap302_belter_cruiser-t2d22.1-0 ; 
       cap302_belter_cruiser-t2d23.1-0 ; 
       cap302_belter_cruiser-t2d24.1-0 ; 
       cap302_belter_cruiser-t2d25.2-0 ; 
       cap302_belter_cruiser-t2d26.2-0 ; 
       cap302_belter_cruiser-t2d29.1-0 ; 
       cap302_belter_cruiser-t2d3.3-0 ; 
       cap302_belter_cruiser-t2d4.6-0 ; 
       cap302_belter_cruiser-t2d5.3-0 ; 
       cap302_belter_cruiser-t2d6.2-0 ; 
       cap302_belter_cruiser-t2d7.1-0 ; 
       cap302_belter_cruiser-t2d8.3-0 ; 
       cap302_belter_cruiser-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 8 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 16 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 9 110 ; 
       20 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 25 300 ; 
       2 16 300 ; 
       3 23 300 ; 
       4 0 300 ; 
       5 24 300 ; 
       6 22 300 ; 
       7 21 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       9 9 300 ; 
       10 18 300 ; 
       10 5 300 ; 
       11 26 300 ; 
       12 19 300 ; 
       12 4 300 ; 
       13 20 300 ; 
       13 3 300 ; 
       14 17 300 ; 
       14 2 300 ; 
       15 12 300 ; 
       15 1 300 ; 
       15 6 300 ; 
       16 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 0 401 ; 
       11 11 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 22 401 ; 
       15 23 401 ; 
       16 24 401 ; 
       17 25 401 ; 
       18 26 401 ; 
       19 1 401 ; 
       20 2 401 ; 
       21 3 401 ; 
       22 4 401 ; 
       23 5 401 ; 
       24 6 401 ; 
       25 7 401 ; 
       26 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 50 -4 0 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       14 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 55 -4 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
