SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.75-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       cap302_belter_cruiser-mat100_1.1-0 ; 
       cap302_belter_cruiser-mat101_1.1-0 ; 
       cap302_belter_cruiser-mat102_1.1-0 ; 
       cap302_belter_cruiser-mat103_1.1-0 ; 
       cap302_belter_cruiser-mat104_1.1-0 ; 
       cap302_belter_cruiser-mat105_1.1-0 ; 
       cap302_belter_cruiser-mat106_1.1-0 ; 
       cap302_belter_cruiser-mat108_1.1-0 ; 
       cap302_belter_cruiser-mat109_1.1-0 ; 
       cap302_belter_cruiser-mat112_1.1-0 ; 
       cap302_belter_cruiser-mat83_1.1-0 ; 
       cap302_belter_cruiser-mat84_1.1-0 ; 
       cap302_belter_cruiser-mat85_1.1-0 ; 
       cap302_belter_cruiser-mat86_1.1-0 ; 
       cap302_belter_cruiser-mat87_1.1-0 ; 
       cap302_belter_cruiser-mat88_1.1-0 ; 
       cap302_belter_cruiser-mat89_1.1-0 ; 
       cap302_belter_cruiser-mat90_1.1-0 ; 
       cap302_belter_cruiser-mat91_1.1-0 ; 
       cap302_belter_cruiser-mat92_1.1-0 ; 
       cap302_belter_cruiser-mat93_1.1-0 ; 
       cap302_belter_cruiser-mat94_1.1-0 ; 
       cap302_belter_cruiser-mat95_1.1-0 ; 
       cap302_belter_cruiser-mat96_1.1-0 ; 
       cap302_belter_cruiser-mat97_1.1-0 ; 
       cap302_belter_cruiser-mat98_1.1-0 ; 
       cap302_belter_cruiser-mat99_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       cap302-cap302.54-0 ROOT ; 
       cap302-cube13_1.1-0 ; 
       cap302-cube16_1.1-0 ; 
       cap302-cube17_1.1-0 ; 
       cap302-cube18_1.1-0 ; 
       cap302-cube19_1.1-0 ; 
       cap302-cube20_1.1-0 ; 
       cap302-cube22_1.1-0 ; 
       cap302-cube6_1_1.1-0 ; 
       cap302-cube6_3.4-0 ; 
       cap302-cyl1_10_1.1-0 ; 
       cap302-cyl1_13_1.1-0 ; 
       cap302-cyl1_14_1.1-0 ; 
       cap302-cyl1_15_1.1-0 ; 
       cap302-cyl1_8_1.1-0 ; 
       cap302-cyl14_1.1-0 ; 
       cap302-sphere5_1.1-0 ; 
       cap302-sphere6_1.1-0 ; 
       cap302-sphere7_1.1-0 ; 
       cap302-sphere8_1.1-0 ; 
       cap302-sphere9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap302/PICTURES/cap302 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-cap302-belter_cruiser.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       cap302_belter_cruiser-t2d1.7-0 ; 
       cap302_belter_cruiser-t2d10.4-0 ; 
       cap302_belter_cruiser-t2d11.4-0 ; 
       cap302_belter_cruiser-t2d12.4-0 ; 
       cap302_belter_cruiser-t2d13.5-0 ; 
       cap302_belter_cruiser-t2d14.4-0 ; 
       cap302_belter_cruiser-t2d15.5-0 ; 
       cap302_belter_cruiser-t2d16.3-0 ; 
       cap302_belter_cruiser-t2d17.4-0 ; 
       cap302_belter_cruiser-t2d18.4-0 ; 
       cap302_belter_cruiser-t2d19.3-0 ; 
       cap302_belter_cruiser-t2d2.5-0 ; 
       cap302_belter_cruiser-t2d20.3-0 ; 
       cap302_belter_cruiser-t2d21.3-0 ; 
       cap302_belter_cruiser-t2d22.3-0 ; 
       cap302_belter_cruiser-t2d23.3-0 ; 
       cap302_belter_cruiser-t2d24.3-0 ; 
       cap302_belter_cruiser-t2d25.4-0 ; 
       cap302_belter_cruiser-t2d26.4-0 ; 
       cap302_belter_cruiser-t2d29.3-0 ; 
       cap302_belter_cruiser-t2d3.5-0 ; 
       cap302_belter_cruiser-t2d4.8-0 ; 
       cap302_belter_cruiser-t2d5.5-0 ; 
       cap302_belter_cruiser-t2d6.4-0 ; 
       cap302_belter_cruiser-t2d7.3-0 ; 
       cap302_belter_cruiser-t2d8.5-0 ; 
       cap302_belter_cruiser-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       9 0 110 ; 
       8 9 110 ; 
       10 8 110 ; 
       11 9 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 16 110 ; 
       15 9 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       19 8 110 ; 
       20 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 25 300 ; 
       2 16 300 ; 
       3 23 300 ; 
       4 0 300 ; 
       5 24 300 ; 
       6 22 300 ; 
       7 21 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 9 300 ; 
       10 18 300 ; 
       10 5 300 ; 
       11 26 300 ; 
       12 19 300 ; 
       12 4 300 ; 
       13 20 300 ; 
       13 3 300 ; 
       14 17 300 ; 
       14 2 300 ; 
       15 12 300 ; 
       15 1 300 ; 
       15 6 300 ; 
       16 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 0 401 ; 
       11 11 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 22 401 ; 
       15 23 401 ; 
       16 24 401 ; 
       17 25 401 ; 
       18 26 401 ; 
       19 1 401 ; 
       20 2 401 ; 
       21 3 401 ; 
       22 4 401 ; 
       23 5 401 ; 
       24 6 401 ; 
       25 7 401 ; 
       26 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 50 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 13.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 57.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 21.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 26.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 6.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 42.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 55 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 47.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
