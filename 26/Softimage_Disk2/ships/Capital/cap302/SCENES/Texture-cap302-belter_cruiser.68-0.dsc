SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.71-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       cap302_belter_cruiser-mat100.1-0 ; 
       cap302_belter_cruiser-mat101.2-0 ; 
       cap302_belter_cruiser-mat102.1-0 ; 
       cap302_belter_cruiser-mat103.1-0 ; 
       cap302_belter_cruiser-mat104.1-0 ; 
       cap302_belter_cruiser-mat105.1-0 ; 
       cap302_belter_cruiser-mat106.1-0 ; 
       cap302_belter_cruiser-mat107.1-0 ; 
       cap302_belter_cruiser-mat108.1-0 ; 
       cap302_belter_cruiser-mat109.1-0 ; 
       cap302_belter_cruiser-mat110.1-0 ; 
       cap302_belter_cruiser-mat111.1-0 ; 
       cap302_belter_cruiser-mat112.1-0 ; 
       cap302_belter_cruiser-mat83.3-0 ; 
       cap302_belter_cruiser-mat84.2-0 ; 
       cap302_belter_cruiser-mat85.3-0 ; 
       cap302_belter_cruiser-mat86.3-0 ; 
       cap302_belter_cruiser-mat87.2-0 ; 
       cap302_belter_cruiser-mat88.1-0 ; 
       cap302_belter_cruiser-mat89.2-0 ; 
       cap302_belter_cruiser-mat90.2-0 ; 
       cap302_belter_cruiser-mat91.2-0 ; 
       cap302_belter_cruiser-mat92.2-0 ; 
       cap302_belter_cruiser-mat93.2-0 ; 
       cap302_belter_cruiser-mat94.1-0 ; 
       cap302_belter_cruiser-mat95.1-0 ; 
       cap302_belter_cruiser-mat96.1-0 ; 
       cap302_belter_cruiser-mat97.3-0 ; 
       cap302_belter_cruiser-mat98.3-0 ; 
       cap302_belter_cruiser-mat99.2-0 ; 
       edit_nulls-mat70.3-0 ; 
       fig33_belter_recovery-mat71.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       cap302-bsmoke.1-0 ; 
       cap302-bthrust.1-0 ; 
       cap302-cap302.50-0 ROOT ; 
       cap302-cockpt.1-0 ; 
       cap302-cube13.1-0 ; 
       cap302-cube16.1-0 ; 
       cap302-cube17.1-0 ; 
       cap302-cube18.1-0 ; 
       cap302-cube19.1-0 ; 
       cap302-cube20.1-0 ; 
       cap302-cube22.1-0 ; 
       cap302-cube6.4-0 ; 
       cap302-cube6_1.1-0 ; 
       cap302-cyl1_10.1-0 ; 
       cap302-cyl1_13.1-0 ; 
       cap302-cyl1_14.1-0 ; 
       cap302-cyl1_15.1-0 ; 
       cap302-cyl1_8.1-0 ; 
       cap302-cyl14.1-0 ; 
       cap302-missemt.1-0 ; 
       cap302-rsmoke.1-0 ; 
       cap302-rthrust.1-0 ; 
       cap302-sphere5.1-0 ; 
       cap302-sphere6.1-0 ; 
       cap302-sphere7.1-0 ; 
       cap302-sphere8.1-0 ; 
       cap302-sphere9.1-0 ; 
       cap302-SS01.1-0 ; 
       cap302-SS02.1-0 ; 
       cap302-SS03.1-0 ; 
       cap302-SS04.1-0 ; 
       cap302-SS05.1-0 ; 
       cap302-SS06.1-0 ; 
       cap302-SS07.1-0 ; 
       cap302-SS08.1-0 ; 
       cap302-tetra1.1-0 ; 
       cap302-tetra2.1-0 ; 
       cap302-thrust.1-0 ; 
       cap302-thrust2.1-0 ; 
       cap302-thrust3.1-0 ; 
       cap302-trail.1-0 ; 
       cap302-turwepemt1.1-0 ; 
       cap302-turwepemt2.1-0 ; 
       cap302-turwepemt3.1-0 ; 
       cap302-turwepemt4.1-0 ; 
       cap302-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap302/PICTURES/cap302 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-cap302-belter_cruiser.68-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       cap302_belter_cruiser-t2d1.5-0 ; 
       cap302_belter_cruiser-t2d10.2-0 ; 
       cap302_belter_cruiser-t2d11.2-0 ; 
       cap302_belter_cruiser-t2d12.2-0 ; 
       cap302_belter_cruiser-t2d13.3-0 ; 
       cap302_belter_cruiser-t2d14.2-0 ; 
       cap302_belter_cruiser-t2d15.3-0 ; 
       cap302_belter_cruiser-t2d16.1-0 ; 
       cap302_belter_cruiser-t2d17.2-0 ; 
       cap302_belter_cruiser-t2d18.2-0 ; 
       cap302_belter_cruiser-t2d19.1-0 ; 
       cap302_belter_cruiser-t2d2.3-0 ; 
       cap302_belter_cruiser-t2d20.1-0 ; 
       cap302_belter_cruiser-t2d21.1-0 ; 
       cap302_belter_cruiser-t2d22.1-0 ; 
       cap302_belter_cruiser-t2d23.1-0 ; 
       cap302_belter_cruiser-t2d24.1-0 ; 
       cap302_belter_cruiser-t2d25.2-0 ; 
       cap302_belter_cruiser-t2d26.2-0 ; 
       cap302_belter_cruiser-t2d27.2-0 ; 
       cap302_belter_cruiser-t2d28.1-0 ; 
       cap302_belter_cruiser-t2d29.1-0 ; 
       cap302_belter_cruiser-t2d3.3-0 ; 
       cap302_belter_cruiser-t2d4.6-0 ; 
       cap302_belter_cruiser-t2d5.3-0 ; 
       cap302_belter_cruiser-t2d6.2-0 ; 
       cap302_belter_cruiser-t2d7.1-0 ; 
       cap302_belter_cruiser-t2d8.3-0 ; 
       cap302_belter_cruiser-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       37 2 110 ; 
       38 2 110 ; 
       39 2 110 ; 
       0 2 110 ; 
       0 1 111 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 11 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 22 110 ; 
       18 11 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 12 110 ; 
       26 11 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 2 110 ; 
       40 2 110 ; 
       41 2 110 ; 
       42 2 110 ; 
       43 2 110 ; 
       44 2 110 ; 
       45 2 110 ; 
       35 5 110 ; 
       36 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       37 7 300 ; 
       38 7 300 ; 
       39 7 300 ; 
       0 7 300 ; 
       1 7 300 ; 
       3 7 300 ; 
       4 28 300 ; 
       5 19 300 ; 
       6 26 300 ; 
       7 0 300 ; 
       8 27 300 ; 
       9 25 300 ; 
       10 24 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       12 16 300 ; 
       12 17 300 ; 
       12 12 300 ; 
       13 21 300 ; 
       13 5 300 ; 
       14 29 300 ; 
       15 22 300 ; 
       15 4 300 ; 
       16 23 300 ; 
       16 3 300 ; 
       17 20 300 ; 
       17 2 300 ; 
       18 15 300 ; 
       18 1 300 ; 
       18 6 300 ; 
       19 7 300 ; 
       20 7 300 ; 
       21 7 300 ; 
       22 18 300 ; 
       27 30 300 ; 
       28 31 300 ; 
       29 30 300 ; 
       30 31 300 ; 
       31 30 300 ; 
       32 31 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       40 7 300 ; 
       41 7 300 ; 
       42 7 300 ; 
       43 7 300 ; 
       44 7 300 ; 
       45 7 300 ; 
       35 10 300 ; 
       36 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 16 401 ; 
       13 0 401 ; 
       14 11 401 ; 
       15 22 401 ; 
       16 23 401 ; 
       17 24 401 ; 
       18 25 401 ; 
       19 26 401 ; 
       20 27 401 ; 
       21 28 401 ; 
       22 1 401 ; 
       23 2 401 ; 
       24 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       28 7 401 ; 
       29 15 401 ; 
       8 17 401 ; 
       9 18 401 ; 
       10 19 401 ; 
       11 20 401 ; 
       12 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       37 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 70 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 MPRFLG 0 ; 
       5 SCHEM 110 -4 0 MPRFLG 0 ; 
       6 SCHEM 105 -4 0 MPRFLG 0 ; 
       7 SCHEM 120 -4 0 MPRFLG 0 ; 
       8 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 125 -4 0 MPRFLG 0 ; 
       10 SCHEM 127.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 98.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 80 -4 0 MPRFLG 0 ; 
       13 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       14 SCHEM 117.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -4 0 MPRFLG 0 ; 
       23 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 115 -4 0 MPRFLG 0 ; 
       25 SCHEM 75 -6 0 MPRFLG 0 ; 
       26 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 110 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.31533 -5.906736 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24.02675 -15.76159 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.26635 -15.805 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 140 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 142.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
