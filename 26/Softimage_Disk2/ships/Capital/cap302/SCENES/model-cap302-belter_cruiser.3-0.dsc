SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.2-0 ; 
       fig33_belter_recovery-mat71.2-0 ; 
       fig33_belter_recovery-mat75.2-0 ; 
       fig33_belter_recovery-mat77.2-0 ; 
       fig33_belter_recovery-mat78.2-0 ; 
       fig33_belter_recovery-mat80.2-0 ; 
       fig33_belter_recovery-mat81.2-0 ; 
       fig33_belter_recovery-mat82.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       cap302-1thrust.1-0 ; 
       cap302-2thrust.1-0 ; 
       cap302-3thrust.1-0 ; 
       cap302-bsmoke.1-0 ; 
       cap302-bthrust.1-0 ; 
       cap302-cap302.3-0 ROOT ; 
       cap302-cockpt.1-0 ; 
       cap302-cube13.1-0 ; 
       cap302-cube16.1-0 ; 
       cap302-cube17.1-0 ; 
       cap302-cube18.1-0 ; 
       cap302-cube19.1-0 ; 
       cap302-cube20.1-0 ; 
       cap302-cube22.1-0 ; 
       cap302-cube6.4-0 ; 
       cap302-cube6_1.1-0 ; 
       cap302-cyl1_10.1-0 ; 
       cap302-cyl1_11.1-0 ; 
       cap302-cyl1_12.1-0 ; 
       cap302-cyl1_13.1-0 ; 
       cap302-cyl1_8.1-0 ; 
       cap302-cyl14.1-0 ; 
       cap302-missemt.1-0 ; 
       cap302-rsmoke.1-0 ; 
       cap302-rthrust.1-0 ; 
       cap302-sphere5.1-0 ; 
       cap302-sphere6.1-0 ; 
       cap302-sphere7.1-0 ; 
       cap302-sphere8.1-0 ; 
       cap302-sphere9.1-0 ; 
       cap302-SS01.1-0 ; 
       cap302-SS02.1-0 ; 
       cap302-SS03.1-0 ; 
       cap302-SS04.1-0 ; 
       cap302-SS05.1-0 ; 
       cap302-SS06.1-0 ; 
       cap302-SS07.1-0 ; 
       cap302-SS08.1-0 ; 
       cap302-trail.1-0 ; 
       cap302-turwepemt1.1-0 ; 
       cap302-turwepemt2.1-0 ; 
       cap302-turwepemt3.1-0 ; 
       cap302-turwepemt4.1-0 ; 
       cap302-wepemt.1-0 ; 
       turcone-175deg.3-0 ROOT ; 
       turcone2-175deg.3-0 ROOT ; 
       turcone4-175deg.3-0 ROOT ; 
       turcone5-175deg.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap302-belter_cruiser.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       3 4 111 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       14 5 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 14 110 ; 
       20 25 110 ; 
       21 14 110 ; 
       22 5 110 ; 
       23 5 110 ; 
       24 5 110 ; 
       25 14 110 ; 
       26 14 110 ; 
       27 14 110 ; 
       28 15 110 ; 
       29 14 110 ; 
       30 5 110 ; 
       31 5 110 ; 
       32 5 110 ; 
       33 5 110 ; 
       34 5 110 ; 
       35 5 110 ; 
       36 5 110 ; 
       37 5 110 ; 
       38 5 110 ; 
       39 5 110 ; 
       39 44 111 ; 
       39 44 114 ; 
       40 5 110 ; 
       40 45 114 ; 
       40 45 111 ; 
       41 5 110 ; 
       41 46 111 ; 
       41 46 114 ; 
       42 5 110 ; 
       42 47 114 ; 
       42 47 111 ; 
       43 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       30 0 300 ; 
       31 1 300 ; 
       32 3 300 ; 
       33 2 300 ; 
       34 5 300 ; 
       35 4 300 ; 
       36 6 300 ; 
       37 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.76962 -23.71038 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39.02521 -23.71621 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.52521 -23.71621 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.84485 -22.03235 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.64553 -22.10631 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45.85254 -6.068644 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 38.47533 -20.37235 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49.25296 -15.29756 0 MPRFLG 0 ; 
       8 SCHEM 74.25296 -15.29756 0 MPRFLG 0 ; 
       9 SCHEM 71.75296 -15.29756 0 MPRFLG 0 ; 
       10 SCHEM 81.75296 -15.29756 0 MPRFLG 0 ; 
       11 SCHEM 84.25296 -15.29756 0 MPRFLG 0 ; 
       12 SCHEM 86.75296 -15.29756 0 MPRFLG 0 ; 
       13 SCHEM 89.25296 -15.29756 0 MPRFLG 0 ; 
       14 SCHEM 69.25296 -13.29756 0 USR MPRFLG 0 ; 
       15 SCHEM 58.00296 -15.29756 0 MPRFLG 0 ; 
       16 SCHEM 54.25296 -17.29756 0 MPRFLG 0 ; 
       17 SCHEM 56.75296 -17.29756 0 MPRFLG 0 ; 
       18 SCHEM 59.25296 -17.29756 0 MPRFLG 0 ; 
       19 SCHEM 79.25296 -15.29756 0 MPRFLG 0 ; 
       20 SCHEM 51.75296 -17.29756 0 MPRFLG 0 ; 
       21 SCHEM 66.75296 -15.29756 0 MPRFLG 0 ; 
       22 SCHEM 34.22821 -21.78857 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39.81517 -21.39358 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 37.69492 -21.38363 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 51.75296 -15.29756 0 MPRFLG 0 ; 
       26 SCHEM 64.25296 -15.29756 0 MPRFLG 0 ; 
       27 SCHEM 76.75296 -15.29756 0 MPRFLG 0 ; 
       28 SCHEM 61.75296 -17.29756 0 MPRFLG 0 ; 
       29 SCHEM 69.25296 -15.29756 0 MPRFLG 0 ; 
       30 SCHEM 33.92368 -25.54769 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 33.96373 -26.18728 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 36.24284 -25.494 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 36.13529 -26.13359 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 38.62307 -25.45201 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 38.59269 -26.14994 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 41.0269 -25.48431 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 40.97692 -26.21067 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 38.46036 -19.56334 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 35.38312 -10.14465 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 37.45953 -10.14465 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 40.36167 -10.14465 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 42.86167 -10.14465 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34.30288 -20.91902 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 59.82547 1.591117 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 -1.590796 0 0 0 1.039569 -7.108673 MPRFLG 0 ; 
       45 SCHEM 60.43103 2.662008 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 4.84239 -1.13583e-008 3.141593 0 -0.8713017 0.983948 MPRFLG 0 ; 
       46 SCHEM 55.91176 1.842513 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 0 1.570796 0 2.645816 0.2436029 -5.28236 MPRFLG 0 ; 
       47 SCHEM 57.04594 2.662008 0 USR WIRECOL 7 7 DISPLAY 0 0 SRT 2.136792 2.136792 2.13 -2.034893e-007 1.458793 3.141593 -1.172995 0.1806884 2.491355 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40.76229 -14.23683 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40.80233 -14.87642 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.97389 -14.82273 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 43.08144 -14.18314 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45.4313 -14.83908 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45.46168 -14.14114 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.8655 -14.17344 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.81552 -14.89981 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
