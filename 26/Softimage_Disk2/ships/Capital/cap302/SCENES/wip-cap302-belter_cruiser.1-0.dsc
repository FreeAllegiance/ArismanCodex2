SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.1-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       cap302_belter_cruiser-cyl1_9.1-0 ROOT ; 
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ROOT ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.1-0 ROOT ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube8.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ROOT ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.1-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ROOT ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
       fig33_belter_recovery1-cube6.1-0 ROOT ; 
       fig33_belter_recovery2-cube6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap302-belter_cruiser.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       35 26 110 ; 
       36 26 110 ; 
       38 26 110 ; 
       38 39 111 ; 
       4 21 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       10 11 110 ; 
       12 10 110 ; 
       13 20 110 ; 
       14 20 110 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 28 110 ; 
       18 10 110 ; 
       20 10 110 ; 
       21 11 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 26 110 ; 
       25 26 110 ; 
       25 27 111 ; 
       25 24 111 ; 
       1 26 110 ; 
       1 2 111 ; 
       27 26 110 ; 
       29 26 110 ; 
       30 26 110 ; 
       31 26 110 ; 
       32 26 110 ; 
       33 26 110 ; 
       34 26 110 ; 
       37 26 110 ; 
       39 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       35 6 300 ; 
       36 7 300 ; 
       29 0 300 ; 
       30 1 300 ; 
       31 3 300 ; 
       32 2 300 ; 
       33 5 300 ; 
       34 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 41.79016 -6.808332 0 USR SRT 0.731064 1 1 0 0 0 0.002161118 0.03731348 0.003023505 MPRFLG 0 ; 
       35 SCHEM 12.45372 -1.769249 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 12.40374 -2.49561 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 26.84687 -0.2979131 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 16.3517 9.293587 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 69.8026 13.6452 0 USR DISPLAY 1 2 SRT 0.838 1 1 0 0 0 1.557672 0.03731348 0.003023505 MPRFLG 0 ; 
       0 SCHEM 72.3026 13.6452 0 SRT 1.164245 1.164245 1.164245 6.357303e-008 3.141593 1.748456e-007 -1.68134 0.1974931 -6.206285 MPRFLG 0 ; 
       4 SCHEM 22.00789 -9.313034 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 20.75789 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 23.25789 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 12.0079 -7.313032 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 17.0079 -5.313036 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0.7619413 0.03731348 2.633133 MPRFLG 0 ; 
       12 SCHEM 5.757897 -9.313034 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 15.75791 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 18.2579 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 8.257907 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 10.7579 -11.31303 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 40.4654 -3.681664 0 MPRFLG 0 ; 
       18 SCHEM 9.507905 -9.313034 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 31.62668 2.806827 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0.8501843 0.05979379 3.897029 MPRFLG 0 ; 
       20 SCHEM 17.0079 -9.313034 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 22.00789 -7.313032 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 40.4654 -1.681664 0 USR SRT 1.164245 1.164245 1.164245 6.357303e-008 -2.778865e-015 3.141593 0.001430999 -0.555638 -3.889156 MPRFLG 0 ; 
       2 SCHEM 24.67723 -1.010641 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 20.58495 2.664433 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 25.75787 -7.313032 0 SRT 0.7559999 0.7559999 0.7559999 0 0 1.570796 1.679944 -0.2374615 3.339293 MPRFLG 0 ; 
       8 SCHEM 28.25786 -7.313032 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 30.75787 -7.313032 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 19.56557 -0.3415046 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 19.50867 -1.01402 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 28.01717 3.233047 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 26.84997 2.254242 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 26.87655 -0.9366774 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 27.85992 1.289688 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 5.350509 -1.832631 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5.390552 -2.47222 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 7.669669 -1.778944 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 7.562115 -2.418534 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 10.0499 -1.736948 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 10.01952 -2.434884 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 20.56998 3.473445 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 24.72662 -0.2879605 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64.77222 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.3026 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
