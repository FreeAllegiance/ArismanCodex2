SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.4-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.2-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
       fig33_belter_recovery1-cube13.1-0 ; 
       fig33_belter_recovery1-cube16.1-0 ; 
       fig33_belter_recovery1-cube17.1-0 ; 
       fig33_belter_recovery1-cube18.1-0 ; 
       fig33_belter_recovery1-cube19.1-0 ; 
       fig33_belter_recovery1-cube6.4-0 ROOT ; 
       fig33_belter_recovery1-cube6_1.1-0 ; 
       fig33_belter_recovery1-cyl1_10.1-0 ; 
       fig33_belter_recovery1-cyl1_11.1-0 ; 
       fig33_belter_recovery1-cyl1_12.1-0 ; 
       fig33_belter_recovery1-cyl1_13.1-0 ; 
       fig33_belter_recovery1-cyl1_8.1-0 ; 
       fig33_belter_recovery1-cyl14.1-0 ; 
       fig33_belter_recovery1-sphere5.1-0 ; 
       fig33_belter_recovery1-sphere6.1-0 ; 
       fig33_belter_recovery1-sphere7.1-0 ; 
       fig33_belter_recovery1-sphere8.1-0 ; 
       fig33_belter_recovery1-sphere9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap302-belter_cruiser.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       34 25 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       32 25 110 ; 
       21 22 110 ; 
       22 25 110 ; 
       35 25 110 ; 
       30 25 110 ; 
       36 26 110 ; 
       37 21 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       0 7 110 ; 
       0 1 111 ; 
       1 7 110 ; 
       2 7 110 ; 
       20 25 110 ; 
       31 33 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       6 8 111 ; 
       6 5 111 ; 
       8 7 110 ; 
       33 25 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       18 19 111 ; 
       19 7 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 0 300 ; 
       10 1 300 ; 
       11 3 300 ; 
       12 2 300 ; 
       13 5 300 ; 
       14 4 300 ; 
       15 6 300 ; 
       16 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       34 SCHEM 26.17715 -7.043163 0 MPRFLG 0 ; 
       27 SCHEM 16.17717 -9.043164 0 MPRFLG 0 ; 
       28 SCHEM 18.67717 -9.043164 0 MPRFLG 0 ; 
       29 SCHEM 21.17715 -9.043164 0 MPRFLG 0 ; 
       32 SCHEM 28.67715 -7.043163 0 MPRFLG 0 ; 
       21 SCHEM 31.17715 -9.043164 0 MPRFLG 0 ; 
       22 SCHEM 31.17715 -7.043163 0 MPRFLG 0 ; 
       35 SCHEM 33.67715 -7.043163 0 MPRFLG 0 ; 
       30 SCHEM 36.17716 -7.043163 0 MPRFLG 0 ; 
       36 SCHEM 23.67715 -9.043164 0 MPRFLG 0 ; 
       37 SCHEM 31.17715 -11.04316 0 MPRFLG 0 ; 
       23 SCHEM 38.67716 -7.043163 0 MPRFLG 0 ; 
       24 SCHEM 41.17716 -7.043163 0 MPRFLG 0 ; 
       0 SCHEM 25.66896 0.6734463 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 23.46964 0.5994827 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 19.37736 4.274558 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 11.17717 -7.043163 0 MPRFLG 0 ; 
       31 SCHEM 13.67717 -9.043164 0 MPRFLG 0 ; 
       3 SCHEM 18.35798 1.268619 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 18.30108 0.5961038 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 26.80958 4.843172 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 25.64238 3.864365 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15.14411 10.90371 0 USR DISPLAY 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 26.65233 2.899812 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 13.67717 -7.043163 0 MPRFLG 0 ; 
       9 SCHEM 4.142916 -0.2225074 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 4.182959 -0.8620962 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 6.462075 -0.1688203 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 6.354522 -0.8084103 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 8.842306 -0.1268243 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 8.811926 -0.8247603 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 11.24613 -0.1591253 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 11.19615 -0.8854862 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 19.36239 5.083569 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 25.63928 1.312211 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 23.51903 1.322163 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 26.17715 -5.043164 0 USR SRT 0.731064 1 1 0 0 0 0.002161118 0.03731348 0.003023505 MPRFLG 0 ; 
       26 SCHEM 19.92716 -7.043163 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64.77222 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.3026 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
