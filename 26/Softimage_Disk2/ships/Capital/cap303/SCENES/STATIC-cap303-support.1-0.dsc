SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.7-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       cap303_support-mat136.3-0 ; 
       cap303_support-mat137.3-0 ; 
       cap303_support-mat139.3-0 ; 
       cap303_support-mat140.3-0 ; 
       cap303_support-mat141.3-0 ; 
       cap303_support-mat142.3-0 ; 
       cap303_support-mat143.3-0 ; 
       cap303_support-mat144.3-0 ; 
       cap303_support-mat145.3-0 ; 
       cap303_support-mat146.3-0 ; 
       cap303_support-mat147.2-0 ; 
       cap303_support-mat148.2-0 ; 
       cap303_support-mat149.2-0 ; 
       cap303_support-mat150.2-0 ; 
       cap303_support-mat151.2-0 ; 
       cap303_support-mat153.2-0 ; 
       cap303_support-mat154.2-0 ; 
       cap303_support-mat155.2-0 ; 
       cap303_support-mat156.2-0 ; 
       cap303_support-mat157.2-0 ; 
       cap303_support-mat158.2-0 ; 
       cap303_support-mat160.1-0 ; 
       cap303_support-mat161.1-0 ; 
       cap303_support-mat162.1-0 ; 
       cap303_support-mat163.1-0 ; 
       cap303_support-mat164.1-0 ; 
       cap303_support-mat165.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_3.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-root.13-0 ROOT ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap303/PICTURES/cap303 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-cap303-support.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       cap303_support-t2d1.2-0 ; 
       cap303_support-t2d10.2-0 ; 
       cap303_support-t2d11.2-0 ; 
       cap303_support-t2d12.2-0 ; 
       cap303_support-t2d13.2-0 ; 
       cap303_support-t2d14.2-0 ; 
       cap303_support-t2d15.2-0 ; 
       cap303_support-t2d16.2-0 ; 
       cap303_support-t2d17.2-0 ; 
       cap303_support-t2d18.2-0 ; 
       cap303_support-t2d19.2-0 ; 
       cap303_support-t2d2.2-0 ; 
       cap303_support-t2d20.2-0 ; 
       cap303_support-t2d21.2-0 ; 
       cap303_support-t2d23.2-0 ; 
       cap303_support-t2d24.1-0 ; 
       cap303_support-t2d25.1-0 ; 
       cap303_support-t2d26.1-0 ; 
       cap303_support-t2d28.1-0 ; 
       cap303_support-t2d29.1-0 ; 
       cap303_support-t2d30.1-0 ; 
       cap303_support-t2d4.2-0 ; 
       cap303_support-t2d5.2-0 ; 
       cap303_support-t2d6.2-0 ; 
       cap303_support-t2d7.2-0 ; 
       cap303_support-t2d8.2-0 ; 
       cap303_support-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 9 110 ; 
       2 8 110 ; 
       3 13 110 ; 
       4 9 110 ; 
       5 1 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 9 110 ; 
       9 16 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 5 110 ; 
       15 12 110 ; 
       17 9 110 ; 
       18 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       1 6 300 ; 
       2 13 300 ; 
       3 17 300 ; 
       4 7 300 ; 
       4 12 300 ; 
       5 3 300 ; 
       6 1 300 ; 
       6 20 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       8 0 300 ; 
       8 26 300 ; 
       9 10 300 ; 
       9 21 300 ; 
       10 8 300 ; 
       11 9 300 ; 
       12 5 300 ; 
       12 18 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       13 16 300 ; 
       14 4 300 ; 
       15 11 300 ; 
       15 19 300 ; 
       17 15 300 ; 
       18 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 11 401 ; 
       2 21 401 ; 
       3 22 401 ; 
       4 23 401 ; 
       5 24 401 ; 
       6 25 401 ; 
       7 26 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 12 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 25 -6 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 MPRFLG 0 ; 
       16 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 10 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
