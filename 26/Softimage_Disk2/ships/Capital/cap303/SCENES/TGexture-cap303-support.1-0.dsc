SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.4-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       cap303_support-mat135.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       root-bsmoke.1-0 ; 
       root-cockpt.1-0 ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-lwepemt.1-0 ; 
       root-missemt.1-0 ; 
       root-null2.1-0 ; 
       root-root.10-0 ROOT ; 
       root-rsmoke.1-0 ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-SS9.1-0 ; 
       root-tetra1.2-0 ; 
       root-tlsmoke.1-0 ; 
       root-tmsmoke.1-0 ; 
       root-tmthrust.1-0 ; 
       root-trail.1-0 ; 
       root-trsmoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TGexture-cap303-support.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 20 110 ; 
       2 14 110 ; 
       3 11 110 ; 
       4 10 110 ; 
       5 15 110 ; 
       6 11 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 11 110 ; 
       11 21 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 14 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 21 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 11 110 ; 
       26 7 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 3 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       27 1 300 ; 
       28 2 300 ; 
       29 4 300 ; 
       30 3 300 ; 
       31 6 300 ; 
       32 5 300 ; 
       33 7 300 ; 
       34 8 300 ; 
       35 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 MPRFLG 0 ; 
       3 SCHEM 65 -4 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 70 -6 0 MPRFLG 0 ; 
       6 SCHEM 60 -4 0 MPRFLG 0 ; 
       7 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 75 -6 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 70 -2 0 MPRFLG 0 ; 
       12 SCHEM 80 -6 0 MPRFLG 0 ; 
       13 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 70 -4 0 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 43.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 85 -4 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 40 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 45 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
