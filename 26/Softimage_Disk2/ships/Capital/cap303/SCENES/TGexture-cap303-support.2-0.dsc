SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       cap303_support-mat135.3-0 ; 
       cap303_support-mat136.2-0 ; 
       cap303_support-mat137.2-0 ; 
       cap303_support-mat138.2-0 ; 
       cap303_support-mat139.2-0 ; 
       cap303_support-mat140.2-0 ; 
       cap303_support-mat141.2-0 ; 
       cap303_support-mat142.2-0 ; 
       cap303_support-mat143.2-0 ; 
       cap303_support-mat144.2-0 ; 
       cap303_support-mat145.2-0 ; 
       cap303_support-mat146.2-0 ; 
       cap303_support-mat147.1-0 ; 
       cap303_support-mat148.1-0 ; 
       cap303_support-mat149.1-0 ; 
       cap303_support-mat150.1-0 ; 
       cap303_support-mat151.1-0 ; 
       cap303_support-mat152.1-0 ; 
       cap303_support-mat153.1-0 ; 
       cap303_support-mat154.1-0 ; 
       cap303_support-mat155.1-0 ; 
       cap303_support-mat156.1-0 ; 
       cap303_support-mat157.1-0 ; 
       cap303_support-mat158.1-0 ; 
       cap303_support-mat159.1-0 ; 
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
       fig30_belter_ftr-mat81.3-0 ; 
       fig30_belter_ftr-mat82.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       root-bsmoke.1-0 ; 
       root-cockpt.1-0 ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-lwepemt.1-0 ; 
       root-missemt.1-0 ; 
       root-null2.1-0 ; 
       root-root.11-0 ROOT ; 
       root-rsmoke.1-0 ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-SS9.1-0 ; 
       root-tetra1.2-0 ; 
       root-tlsmoke.1-0 ; 
       root-tmsmoke.1-0 ; 
       root-tmthrust.1-0 ; 
       root-trail.1-0 ; 
       root-trsmoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap303/PICTURES/cap303 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TGexture-cap303-support.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       cap303_support-t2d1.1-0 ; 
       cap303_support-t2d10.1-0 ; 
       cap303_support-t2d11.1-0 ; 
       cap303_support-t2d12.1-0 ; 
       cap303_support-t2d13.1-0 ; 
       cap303_support-t2d14.1-0 ; 
       cap303_support-t2d15.1-0 ; 
       cap303_support-t2d16.1-0 ; 
       cap303_support-t2d17.1-0 ; 
       cap303_support-t2d18.1-0 ; 
       cap303_support-t2d19.1-0 ; 
       cap303_support-t2d2.1-0 ; 
       cap303_support-t2d20.1-0 ; 
       cap303_support-t2d21.1-0 ; 
       cap303_support-t2d22.1-0 ; 
       cap303_support-t2d23.1-0 ; 
       cap303_support-t2d3.1-0 ; 
       cap303_support-t2d4.1-0 ; 
       cap303_support-t2d5.1-0 ; 
       cap303_support-t2d6.1-0 ; 
       cap303_support-t2d7.1-0 ; 
       cap303_support-t2d8.1-0 ; 
       cap303_support-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 20 110 ; 
       2 14 110 ; 
       3 11 110 ; 
       4 10 110 ; 
       5 15 110 ; 
       6 11 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 11 110 ; 
       11 21 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 14 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 21 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 11 110 ; 
       26 7 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 3 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 16 300 ; 
       3 8 300 ; 
       4 15 300 ; 
       5 20 300 ; 
       6 9 300 ; 
       6 14 300 ; 
       7 5 300 ; 
       8 2 300 ; 
       8 23 300 ; 
       9 3 300 ; 
       9 24 300 ; 
       10 1 300 ; 
       11 12 300 ; 
       12 10 300 ; 
       13 11 300 ; 
       14 7 300 ; 
       14 21 300 ; 
       15 19 300 ; 
       16 6 300 ; 
       17 13 300 ; 
       17 22 300 ; 
       25 18 300 ; 
       26 4 300 ; 
       27 25 300 ; 
       28 26 300 ; 
       29 28 300 ; 
       30 27 300 ; 
       31 30 300 ; 
       32 29 300 ; 
       33 31 300 ; 
       34 32 300 ; 
       35 0 300 ; 
       36 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 11 401 ; 
       3 16 401 ; 
       4 17 401 ; 
       5 18 401 ; 
       6 19 401 ; 
       7 20 401 ; 
       8 21 401 ; 
       9 22 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
       14 5 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       18 12 401 ; 
       19 8 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 13 401 ; 
       23 15 401 ; 
       24 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 MPRFLG 0 ; 
       3 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 90 -6 0 MPRFLG 0 ; 
       5 SCHEM 85 -6 0 MPRFLG 0 ; 
       6 SCHEM 68.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 75 -6 0 MPRFLG 0 ; 
       8 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 83.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 MPRFLG 0 ; 
       14 SCHEM 60 -4 0 MPRFLG 0 ; 
       15 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 75 -8 0 MPRFLG 0 ; 
       17 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 110 -4 0 MPRFLG 0 ; 
       26 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 40 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 45 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 80 -6 0 MPRFLG 0 ; 
       37 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
