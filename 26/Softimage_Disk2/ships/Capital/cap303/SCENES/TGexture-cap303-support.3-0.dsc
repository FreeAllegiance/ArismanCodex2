SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.6-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       cap303_support-mat135.4-0 ; 
       cap303_support-mat136.3-0 ; 
       cap303_support-mat137.3-0 ; 
       cap303_support-mat139.3-0 ; 
       cap303_support-mat140.3-0 ; 
       cap303_support-mat141.3-0 ; 
       cap303_support-mat142.3-0 ; 
       cap303_support-mat143.3-0 ; 
       cap303_support-mat144.3-0 ; 
       cap303_support-mat145.3-0 ; 
       cap303_support-mat146.3-0 ; 
       cap303_support-mat147.2-0 ; 
       cap303_support-mat148.2-0 ; 
       cap303_support-mat149.2-0 ; 
       cap303_support-mat150.2-0 ; 
       cap303_support-mat151.2-0 ; 
       cap303_support-mat152.2-0 ; 
       cap303_support-mat153.2-0 ; 
       cap303_support-mat154.2-0 ; 
       cap303_support-mat155.2-0 ; 
       cap303_support-mat156.2-0 ; 
       cap303_support-mat157.2-0 ; 
       cap303_support-mat158.2-0 ; 
       cap303_support-mat160.1-0 ; 
       cap303_support-mat161.1-0 ; 
       cap303_support-mat162.1-0 ; 
       cap303_support-mat163.1-0 ; 
       cap303_support-mat164.1-0 ; 
       cap303_support-mat165.1-0 ; 
       edit_nulls-mat70.4-0 ; 
       fig20_biofighter-mat71.4-0 ; 
       fig20_biofighter-mat75.4-0 ; 
       fig20_biofighter-mat77.4-0 ; 
       fig20_biofighter-mat78.4-0 ; 
       fig20_biofighter-mat80.4-0 ; 
       fig30_belter_ftr-mat81.4-0 ; 
       fig30_belter_ftr-mat82.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       root-bsmoke.1-0 ; 
       root-cockpt.1-0 ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_3.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-lwepemt.1-0 ; 
       root-missemt.1-0 ; 
       root-null2.1-0 ; 
       root-root.12-0 ROOT ; 
       root-rsmoke.1-0 ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-SS9.1-0 ; 
       root-tetra1.2-0 ; 
       root-tlsmoke.1-0 ; 
       root-tmsmoke.1-0 ; 
       root-tmthrust.1-0 ; 
       root-trail.1-0 ; 
       root-trsmoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Capital/cap303/PICTURES/cap303 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TGexture-cap303-support.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       cap303_support-t2d1.2-0 ; 
       cap303_support-t2d10.2-0 ; 
       cap303_support-t2d11.2-0 ; 
       cap303_support-t2d12.2-0 ; 
       cap303_support-t2d13.2-0 ; 
       cap303_support-t2d14.2-0 ; 
       cap303_support-t2d15.2-0 ; 
       cap303_support-t2d16.2-0 ; 
       cap303_support-t2d17.2-0 ; 
       cap303_support-t2d18.2-0 ; 
       cap303_support-t2d19.2-0 ; 
       cap303_support-t2d2.2-0 ; 
       cap303_support-t2d20.2-0 ; 
       cap303_support-t2d21.2-0 ; 
       cap303_support-t2d23.2-0 ; 
       cap303_support-t2d24.1-0 ; 
       cap303_support-t2d25.1-0 ; 
       cap303_support-t2d26.1-0 ; 
       cap303_support-t2d27.1-0 ; 
       cap303_support-t2d28.1-0 ; 
       cap303_support-t2d29.1-0 ; 
       cap303_support-t2d30.1-0 ; 
       cap303_support-t2d4.2-0 ; 
       cap303_support-t2d5.2-0 ; 
       cap303_support-t2d6.2-0 ; 
       cap303_support-t2d7.2-0 ; 
       cap303_support-t2d8.2-0 ; 
       cap303_support-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 10 110 ; 
       0 20 110 ; 
       1 20 110 ; 
       2 14 110 ; 
       3 11 110 ; 
       4 10 110 ; 
       5 15 110 ; 
       6 11 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       10 11 110 ; 
       11 21 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 14 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 21 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 11 110 ; 
       26 7 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 3 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 24 300 ; 
       9 25 300 ; 
       2 15 300 ; 
       3 7 300 ; 
       4 14 300 ; 
       5 19 300 ; 
       6 8 300 ; 
       6 13 300 ; 
       7 4 300 ; 
       8 2 300 ; 
       8 22 300 ; 
       10 1 300 ; 
       10 28 300 ; 
       11 11 300 ; 
       11 23 300 ; 
       12 9 300 ; 
       13 10 300 ; 
       14 6 300 ; 
       14 20 300 ; 
       14 26 300 ; 
       14 27 300 ; 
       15 18 300 ; 
       16 5 300 ; 
       17 12 300 ; 
       17 21 300 ; 
       25 17 300 ; 
       26 3 300 ; 
       27 29 300 ; 
       28 30 300 ; 
       29 32 300 ; 
       30 31 300 ; 
       31 34 300 ; 
       32 33 300 ; 
       33 35 300 ; 
       34 36 300 ; 
       35 0 300 ; 
       36 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 11 401 ; 
       26 19 401 ; 
       3 22 401 ; 
       4 23 401 ; 
       5 24 401 ; 
       6 25 401 ; 
       7 26 401 ; 
       8 27 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 18 401 ; 
       17 12 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       27 20 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       28 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 108.75 -6 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 MPRFLG 0 ; 
       3 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 95 -6 0 MPRFLG 0 ; 
       5 SCHEM 90 -6 0 MPRFLG 0 ; 
       6 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 80 -6 0 MPRFLG 0 ; 
       8 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 MPRFLG 0 ; 
       14 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 91.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 80 -8 0 MPRFLG 0 ; 
       17 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 115 -4 0 MPRFLG 0 ; 
       26 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 40 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 45 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 85 -6 0 MPRFLG 0 ; 
       37 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 122.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 125 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
