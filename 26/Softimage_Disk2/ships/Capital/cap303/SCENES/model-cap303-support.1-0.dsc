SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.5-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       cap301_belter_frigate-mat100.1-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap303_support-mat128.1-0 ; 
       cap303_support-mat129.1-0 ; 
       cap303_support-mat130.1-0 ; 
       cap303_support-mat131.1-0 ; 
       cap303_support-mat132.1-0 ; 
       cap303_support-mat133.1-0 ; 
       cap303_support-mat134.1-0 ; 
       fig33_belter_recovery-mat110.2-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.2-0 ; 
       fig33_belter_recovery-mat115.2-0 ; 
       fig33_belter_recovery-mat118.2-0 ; 
       fig33_belter_recovery-mat95.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-root.6-0 ROOT ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap303-support.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       cap301_belter_frigate-t2d18.2-0 ; 
       cap301_belter_frigate-t2d19.2-0 ; 
       cap303_support-t2d59.2-0 ; 
       cap303_support-t2d60.2-0 ; 
       cap303_support-t2d61.2-0 ; 
       cap303_support-t2d62.2-0 ; 
       cap303_support-t2d63.2-0 ; 
       cap303_support-t2d64.2-0 ; 
       cap303_support-t2d65.2-0 ; 
       fig33_belter_recovery-t2d13.3-0 ; 
       fig33_belter_recovery-t2d28.3-0 ; 
       fig33_belter_recovery-t2d29.3-0 ; 
       fig33_belter_recovery-t2d30.3-0 ; 
       fig33_belter_recovery-t2d33.3-0 ; 
       fig33_belter_recovery-t2d36.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 8 110 ; 
       0 11 110 ; 
       1 9 110 ; 
       2 8 110 ; 
       3 12 110 ; 
       4 9 110 ; 
       5 1 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 5 110 ; 
       14 11 110 ; 
       16 9 110 ; 
       17 5 110 ; 
       18 1 110 ; 
       8 9 110 ; 
       9 15 110 ; 
       6 8 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       2 13 300 ; 
       3 9 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       14 0 300 ; 
       14 1 300 ; 
       16 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1272.875 -6.391453 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1272.875 -8.391453 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 1309.845 -9.02719 0 MPRFLG 0 ; 
       15 SCHEM 1296.065 -3.024395 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 1284.845 -9.02719 0 MPRFLG 0 ; 
       1 SCHEM 1294.845 -7.02719 0 MPRFLG 0 ; 
       2 SCHEM 1302.345 -9.02719 0 MPRFLG 0 ; 
       3 SCHEM 1299.845 -9.02719 0 MPRFLG 0 ; 
       4 SCHEM 1289.845 -7.02719 0 MPRFLG 0 ; 
       5 SCHEM 1293.595 -9.02719 0 MPRFLG 0 ; 
       11 SCHEM 1286.095 -7.02719 0 MPRFLG 0 ; 
       12 SCHEM 1299.845 -7.02719 0 MPRFLG 0 ; 
       13 SCHEM 1294.845 -11.02719 0 MPRFLG 0 ; 
       14 SCHEM 1287.345 -9.02719 0 MPRFLG 0 ; 
       16 SCHEM 1282.345 -7.02719 0 MPRFLG 0 ; 
       17 SCHEM 1292.345 -11.02719 0 MPRFLG 0 ; 
       18 SCHEM 1297.345 -9.02719 0 MPRFLG 0 ; 
       8 SCHEM 1306.095 -7.02719 0 MPRFLG 0 ; 
       9 SCHEM 1296.095 -5.02719 0 USR MPRFLG 0 ; 
       6 SCHEM 1304.845 -9.02719 0 MPRFLG 0 ; 
       7 SCHEM 1307.345 -9.02719 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 307.7748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 610.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 613.4772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 615.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 27.36749 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 307.7748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 610.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 613.4772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 615.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.36749 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
