SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.1-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       cap301_belter_frigate-mat100.2-0 ; 
       cap301_belter_frigate-mat101.2-0 ; 
       cap303_support-mat128.2-0 ; 
       cap303_support-mat129.2-0 ; 
       cap303_support-mat130.2-0 ; 
       cap303_support-mat131.2-0 ; 
       cap303_support-mat132.2-0 ; 
       cap303_support-mat133.2-0 ; 
       cap303_support-mat134.2-0 ; 
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
       fig33_belter_recovery-mat110.3-0 ; 
       fig33_belter_recovery-mat111.3-0 ; 
       fig33_belter_recovery-mat112.3-0 ; 
       fig33_belter_recovery-mat115.3-0 ; 
       fig33_belter_recovery-mat118.3-0 ; 
       fig33_belter_recovery-mat95.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       fig30_belter_ftr-cockpt.1-0 ROOT ; 
       fig30_belter_ftr-lsmoke.1-0 ROOT ; 
       fig30_belter_ftr-lthrust.1-0 ROOT ; 
       fig30_belter_ftr-lwepemt.1-0 ROOT ; 
       fig30_belter_ftr-missemt.1-0 ROOT ; 
       fig30_belter_ftr-rsmoke.1-0 ROOT ; 
       fig30_belter_ftr-rthrust.1-0 ROOT ; 
       fig30_belter_ftr-rwepemt.1-0 ROOT ; 
       fig30_belter_ftr-SS01.1-0 ROOT ; 
       fig30_belter_ftr-SS02.1-0 ROOT ; 
       fig30_belter_ftr-SS03.1-0 ROOT ; 
       fig30_belter_ftr-SS04.1-0 ROOT ; 
       fig30_belter_ftr-SS05.1-0 ROOT ; 
       fig30_belter_ftr-SS06.1-0 ROOT ; 
       fig30_belter_ftr-SS07.1-0 ROOT ; 
       fig30_belter_ftr-SS08.1-0 ROOT ; 
       fig30_belter_ftr-trail.1-0 ROOT ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-root.7-0 ROOT ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap303-support.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       cap301_belter_frigate-t2d18.3-0 ; 
       cap301_belter_frigate-t2d19.3-0 ; 
       cap303_support-t2d59.3-0 ; 
       cap303_support-t2d60.3-0 ; 
       cap303_support-t2d61.3-0 ; 
       cap303_support-t2d62.3-0 ; 
       cap303_support-t2d63.3-0 ; 
       cap303_support-t2d64.3-0 ; 
       cap303_support-t2d65.3-0 ; 
       fig33_belter_recovery-t2d13.4-0 ; 
       fig33_belter_recovery-t2d28.4-0 ; 
       fig33_belter_recovery-t2d29.4-0 ; 
       fig33_belter_recovery-t2d30.4-0 ; 
       fig33_belter_recovery-t2d33.4-0 ; 
       fig33_belter_recovery-t2d36.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       27 25 110 ; 
       17 28 110 ; 
       18 26 110 ; 
       19 25 110 ; 
       20 29 110 ; 
       21 26 110 ; 
       22 18 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 22 110 ; 
       31 28 110 ; 
       33 26 110 ; 
       34 22 110 ; 
       35 18 110 ; 
       25 26 110 ; 
       26 32 110 ; 
       23 25 110 ; 
       24 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 20 300 ; 
       19 21 300 ; 
       20 17 300 ; 
       21 5 300 ; 
       21 6 300 ; 
       21 7 300 ; 
       21 8 300 ; 
       28 2 300 ; 
       28 3 300 ; 
       28 4 300 ; 
       29 18 300 ; 
       29 19 300 ; 
       31 0 300 ; 
       31 1 300 ; 
       33 22 300 ; 
       8 9 300 ; 
       9 10 300 ; 
       10 12 300 ; 
       11 11 300 ; 
       12 14 300 ; 
       13 13 300 ; 
       14 15 300 ; 
       15 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       27 SCHEM 31.16601 1.63636 0 MPRFLG 0 ; 
       32 SCHEM 17.41601 7.636359 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 6.166006 1.63636 0 MPRFLG 0 ; 
       18 SCHEM 16.16601 3.63636 0 MPRFLG 0 ; 
       19 SCHEM 23.66601 1.63636 0 MPRFLG 0 ; 
       20 SCHEM 21.16601 1.63636 0 MPRFLG 0 ; 
       21 SCHEM 11.16601 3.63636 0 MPRFLG 0 ; 
       22 SCHEM 14.91601 1.63636 0 MPRFLG 0 ; 
       28 SCHEM 7.416006 3.63636 0 MPRFLG 0 ; 
       29 SCHEM 21.16601 3.63636 0 MPRFLG 0 ; 
       30 SCHEM 16.16601 -0.3636399 0 MPRFLG 0 ; 
       31 SCHEM 8.666007 1.63636 0 MPRFLG 0 ; 
       33 SCHEM 3.666006 3.63636 0 MPRFLG 0 ; 
       34 SCHEM 13.66601 -0.3636399 0 MPRFLG 0 ; 
       35 SCHEM 18.66601 1.63636 0 MPRFLG 0 ; 
       0 SCHEM 19.70933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -1.787128e-022 0.3111044 2.493818 MPRFLG 0 ; 
       25 SCHEM 27.41601 3.63636 0 MPRFLG 0 ; 
       26 SCHEM 17.41601 5.63636 0 MPRFLG 0 ; 
       23 SCHEM 26.16601 1.63636 0 MPRFLG 0 ; 
       24 SCHEM 28.66601 1.63636 0 MPRFLG 0 ; 
       1 SCHEM 7.20933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.881877 8.272682e-014 -3.235957 MPRFLG 0 ; 
       2 SCHEM 4.709332 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.881877 8.272682e-014 -3.235957 MPRFLG 0 ; 
       3 SCHEM 24.70932 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.7786173 -0.6693835 2.729695 MPRFLG 0 ; 
       4 SCHEM 22.20933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 9.987657e-023 -0.501887 2.676266 MPRFLG 0 ; 
       5 SCHEM 14.70933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.667996 1.465971e-013 -3.221422 MPRFLG 0 ; 
       6 SCHEM 12.20933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.667996 1.465971e-013 -3.221422 MPRFLG 0 ; 
       7 SCHEM 17.20933 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.7786174 -0.6693835 2.729695 MPRFLG 0 ; 
       8 SCHEM 6.499357 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.4004749 0.1942236 3.193892 MPRFLG 0 ; 
       9 SCHEM 8.999362 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.400804 0.1942236 3.193892 MPRFLG 0 ; 
       10 SCHEM 13.99937 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 2.671272 0.000347077 -0.9917474 MPRFLG 0 ; 
       11 SCHEM 11.49937 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -2.47545 0.000347077 -0.9917474 MPRFLG 0 ; 
       12 SCHEM 16.49937 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 0.6240765 0.8088385 0.08614995 MPRFLG 0 ; 
       13 SCHEM 18.99937 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.6240727 0.8088385 0.08614995 MPRFLG 0 ; 
       14 SCHEM 23.99936 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.5418239 -0.7611478 0.1765151 MPRFLG 0 ; 
       15 SCHEM 21.49937 -6.353623 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.5393358 -0.7611526 0.1765151 MPRFLG 0 ; 
       16 SCHEM 9.709332 -2.811128 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 2.82499e-015 -1.753179 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 79.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 82 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 84.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 87 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 92 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 89.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 40 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 45 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 97 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 94.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 40 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 5 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 45 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
