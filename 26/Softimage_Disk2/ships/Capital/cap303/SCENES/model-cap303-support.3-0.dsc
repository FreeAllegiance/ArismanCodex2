SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       cap301_belter_frigate-mat100.2-0 ; 
       cap301_belter_frigate-mat101.2-0 ; 
       cap303_support-mat128.2-0 ; 
       cap303_support-mat129.2-0 ; 
       cap303_support-mat130.2-0 ; 
       cap303_support-mat131.2-0 ; 
       cap303_support-mat132.2-0 ; 
       cap303_support-mat133.2-0 ; 
       cap303_support-mat134.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
       fig33_belter_recovery-mat110.3-0 ; 
       fig33_belter_recovery-mat111.3-0 ; 
       fig33_belter_recovery-mat112.3-0 ; 
       fig33_belter_recovery-mat115.3-0 ; 
       fig33_belter_recovery-mat118.3-0 ; 
       fig33_belter_recovery-mat95.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       cap303_support-bsmoke.1-0 ROOT ; 
       cap303_support-tlsmoke.1-0 ROOT ; 
       cap303_support-tmsmoke.1-0 ROOT ; 
       fig30_belter_ftr-cockpt.1-0 ; 
       fig30_belter_ftr-lwepemt.1-0 ; 
       fig30_belter_ftr-missemt.1-0 ; 
       fig30_belter_ftr-null2.1-0 ROOT ; 
       fig30_belter_ftr-rsmoke.1-0 ; 
       fig30_belter_ftr-rthrust.1-0 ; 
       fig30_belter_ftr-rwepemt.1-0 ; 
       fig30_belter_ftr-SS01.1-0 ; 
       fig30_belter_ftr-SS02.1-0 ; 
       fig30_belter_ftr-SS03.1-0 ; 
       fig30_belter_ftr-SS04.1-0 ; 
       fig30_belter_ftr-SS05.1-0 ; 
       fig30_belter_ftr-SS06.1-0 ; 
       fig30_belter_ftr-SS07.1-0 ; 
       fig30_belter_ftr-SS08.1-0 ; 
       fig30_belter_ftr-tmthrust.1-0 ; 
       fig30_belter_ftr-trail.1-0 ; 
       fig30_belter_ftr-trsmoke.1-0 ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-root.8-0 ROOT ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-tetra1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap303-support.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       cap301_belter_frigate-t2d18.3-0 ; 
       cap301_belter_frigate-t2d19.3-0 ; 
       cap303_support-t2d59.3-0 ; 
       cap303_support-t2d60.3-0 ; 
       cap303_support-t2d61.3-0 ; 
       cap303_support-t2d62.3-0 ; 
       cap303_support-t2d63.3-0 ; 
       cap303_support-t2d64.3-0 ; 
       cap303_support-t2d65.3-0 ; 
       fig33_belter_recovery-t2d13.4-0 ; 
       fig33_belter_recovery-t2d28.4-0 ; 
       fig33_belter_recovery-t2d29.4-0 ; 
       fig33_belter_recovery-t2d30.4-0 ; 
       fig33_belter_recovery-t2d33.4-0 ; 
       fig33_belter_recovery-t2d36.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 6 110 ; 
       20 6 110 ; 
       18 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       19 6 110 ; 
       21 32 110 ; 
       22 30 110 ; 
       23 29 110 ; 
       24 33 110 ; 
       25 30 110 ; 
       26 22 110 ; 
       27 29 110 ; 
       28 29 110 ; 
       29 30 110 ; 
       30 36 110 ; 
       31 29 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 26 110 ; 
       35 32 110 ; 
       37 30 110 ; 
       38 26 110 ; 
       39 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 9 300 ; 
       11 10 300 ; 
       12 12 300 ; 
       13 11 300 ; 
       14 14 300 ; 
       15 13 300 ; 
       16 15 300 ; 
       17 16 300 ; 
       21 20 300 ; 
       23 21 300 ; 
       24 17 300 ; 
       25 5 300 ; 
       25 6 300 ; 
       25 7 300 ; 
       25 8 300 ; 
       32 2 300 ; 
       32 3 300 ; 
       32 4 300 ; 
       33 18 300 ; 
       33 19 300 ; 
       35 0 300 ; 
       35 1 300 ; 
       37 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.010351 -8.285021 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -4.524116 -0.783548 -4.979259 MPRFLG 0 ; 
       2 SCHEM 4.952481 -11.27713 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.01438652 -0.297839 -12.81706 MPRFLG 0 ; 
       1 SCHEM 7.452476 -11.27713 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.832238 -0.297839 -12.81706 MPRFLG 0 ; 
       3 SCHEM 12.94616 -7.50882 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.326088 -11.27277 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4.922637 -12.22209 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15.49966 -8.625481 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.99967 -8.625481 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 23.41698 2.034935 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 6.946001 -7.648149 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.938196 -8.607361 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10.49967 -8.625481 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 212.8742 -72.81393 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 212.9368 -73.5801 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 215.3114 -72.76109 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 215.2685 -73.60652 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 217.7322 -72.86678 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 217.7752 -73.5801 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 220.1046 -72.93836 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 220.0387 -73.60017 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 4.490945 -6.374232 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4.917322 3.346074 0 MPRFLG 0 ; 
       22 SCHEM 14.91732 5.346075 0 MPRFLG 0 ; 
       23 SCHEM 22.41733 3.346074 0 MPRFLG 0 ; 
       24 SCHEM 19.91733 3.346074 0 MPRFLG 0 ; 
       25 SCHEM 9.917322 5.346075 0 MPRFLG 0 ; 
       26 SCHEM 13.66732 3.346074 0 MPRFLG 0 ; 
       27 SCHEM 24.91733 3.346074 0 MPRFLG 0 ; 
       28 SCHEM 27.41733 3.346074 0 MPRFLG 0 ; 
       29 SCHEM 26.16733 5.346075 0 MPRFLG 0 ; 
       30 SCHEM 16.16733 7.346075 0 MPRFLG 0 ; 
       31 SCHEM 29.91733 3.346074 0 MPRFLG 0 ; 
       32 SCHEM 6.167322 5.346075 0 MPRFLG 0 ; 
       33 SCHEM 19.91733 5.346075 0 MPRFLG 0 ; 
       34 SCHEM 14.91732 1.346074 0 MPRFLG 0 ; 
       35 SCHEM 7.417322 3.346074 0 MPRFLG 0 ; 
       36 SCHEM 16.16733 9.346073 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       37 SCHEM 2.417321 5.346075 0 MPRFLG 0 ; 
       38 SCHEM 12.41732 1.346074 0 MPRFLG 0 ; 
       39 SCHEM 17.41733 3.346074 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       9 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 99.37138 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 101.8714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 104.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 106.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 109.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 119.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 111.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 114.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 116.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 129.3714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 134.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 131.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 96.87138 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 136.8714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 94.37138 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 99.37138 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 101.8714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 104.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 106.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 109.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 119.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 111.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 114.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 116.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 94.37138 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 129.3714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 134.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 131.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96.87138 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 136.8714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
