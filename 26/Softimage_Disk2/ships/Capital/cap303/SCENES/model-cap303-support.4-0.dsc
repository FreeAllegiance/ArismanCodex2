SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.3-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       cap301_belter_frigate-mat100.2-0 ; 
       cap301_belter_frigate-mat101.2-0 ; 
       cap303_support-mat128.2-0 ; 
       cap303_support-mat129.2-0 ; 
       cap303_support-mat130.2-0 ; 
       cap303_support-mat131.2-0 ; 
       cap303_support-mat132.2-0 ; 
       cap303_support-mat133.2-0 ; 
       cap303_support-mat134.2-0 ; 
       cap303_support-mat135.2-0 ; 
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
       fig33_belter_recovery-mat110.3-0 ; 
       fig33_belter_recovery-mat111.3-0 ; 
       fig33_belter_recovery-mat112.3-0 ; 
       fig33_belter_recovery-mat115.3-0 ; 
       fig33_belter_recovery-mat118.3-0 ; 
       fig33_belter_recovery-mat95.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       root-bsmoke.1-0 ; 
       root-cockpt.1-0 ; 
       root-cube15.1-0 ; 
       root-cube15_1.2-0 ; 
       root-cube18.1-0 ; 
       root-cube19.1-0 ; 
       root-cube2_1_3_1.2-0 ; 
       root-cube33.1-0 ; 
       root-cube41_1.1-0 ; 
       root-cube41_2.1-0 ; 
       root-cube42.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube6_3.1-0 ; 
       root-cube7.1-0 ; 
       root-cyl1_3.1-0 ; 
       root-cyl15.1-0 ; 
       root-lwepemt.1-0 ; 
       root-missemt.1-0 ; 
       root-null2.1-0 ; 
       root-root.9-0 ROOT ; 
       root-rsmoke.1-0 ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere5.1-0 ; 
       root-sphere7.2-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-SS9.1-0 ; 
       root-tetra1.2-0 ; 
       root-tlsmoke.1-0 ; 
       root-tmsmoke.1-0 ; 
       root-tmthrust.1-0 ; 
       root-trail.1-0 ; 
       root-trsmoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-cap303-support.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       cap301_belter_frigate-t2d18.4-0 ; 
       cap301_belter_frigate-t2d19.4-0 ; 
       cap303_support-t2d59.4-0 ; 
       cap303_support-t2d60.4-0 ; 
       cap303_support-t2d61.4-0 ; 
       cap303_support-t2d62.4-0 ; 
       cap303_support-t2d63.4-0 ; 
       cap303_support-t2d64.4-0 ; 
       cap303_support-t2d65.4-0 ; 
       fig33_belter_recovery-t2d13.5-0 ; 
       fig33_belter_recovery-t2d28.5-0 ; 
       fig33_belter_recovery-t2d29.5-0 ; 
       fig33_belter_recovery-t2d30.5-0 ; 
       fig33_belter_recovery-t2d33.5-0 ; 
       fig33_belter_recovery-t2d36.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       37 20 110 ; 
       38 20 110 ; 
       1 20 110 ; 
       18 20 110 ; 
       19 20 110 ; 
       20 11 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       39 20 110 ; 
       40 20 110 ; 
       41 20 110 ; 
       2 14 110 ; 
       3 11 110 ; 
       4 10 110 ; 
       5 15 110 ; 
       6 11 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 11 110 ; 
       11 21 110 ; 
       12 10 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 14 110 ; 
       25 11 110 ; 
       26 7 110 ; 
       36 3 110 ; 
       35 20 110 ; 
       13 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       27 10 300 ; 
       28 11 300 ; 
       29 13 300 ; 
       30 12 300 ; 
       31 15 300 ; 
       32 14 300 ; 
       33 16 300 ; 
       34 17 300 ; 
       2 21 300 ; 
       4 22 300 ; 
       5 18 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       15 19 300 ; 
       15 20 300 ; 
       17 0 300 ; 
       17 1 300 ; 
       25 23 300 ; 
       35 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.14091 -14.86997 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.89038 -17.0015 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 8.831244 -17.60761 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.15746 -12.25813 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19.71096 -13.3748 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 17.21097 -13.3748 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.8194 -3.834364 0 USR MPRFLG 0 ; 
       22 SCHEM 11.1573 -12.39746 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.14949 -13.35667 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14.71097 -13.3748 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 30.30429 -7.402053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 30.36688 -8.168227 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 32.74148 -7.34922 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 32.69857 -8.194648 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 35.16228 -7.454909 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 35.2053 -8.168227 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 37.53468 -7.526488 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 37.46878 -8.188293 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 9.133935 -16.97141 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 8.702243 -11.12355 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.401324 -16.87734 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4.917322 3.346074 0 MPRFLG 0 ; 
       3 SCHEM 14.91732 5.346075 0 MPRFLG 0 ; 
       4 SCHEM 22.41733 3.346074 0 MPRFLG 0 ; 
       5 SCHEM 19.91733 3.346074 0 MPRFLG 0 ; 
       6 SCHEM 9.917322 5.346075 0 MPRFLG 0 ; 
       7 SCHEM 13.66732 3.346074 0 MPRFLG 0 ; 
       8 SCHEM 24.91733 3.346074 0 MPRFLG 0 ; 
       9 SCHEM 27.41733 3.346074 0 MPRFLG 0 ; 
       10 SCHEM 26.16733 5.346075 0 MPRFLG 0 ; 
       11 SCHEM 16.16733 7.346075 0 MPRFLG 0 ; 
       12 SCHEM 29.91733 3.346074 0 MPRFLG 0 ; 
       14 SCHEM 6.167322 5.346075 0 MPRFLG 0 ; 
       15 SCHEM 19.91733 5.346075 0 MPRFLG 0 ; 
       16 SCHEM 14.91732 1.346074 0 MPRFLG 0 ; 
       17 SCHEM 7.417322 3.346074 0 MPRFLG 0 ; 
       21 SCHEM 16.16733 9.346073 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 2.417321 5.346075 0 MPRFLG 0 ; 
       26 SCHEM 12.41732 1.346074 0 MPRFLG 0 ; 
       36 SCHEM 17.41733 3.346074 0 MPRFLG 0 ; 
       35 SCHEM 39.92328 -7.533167 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 38.21198 9.346073 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 99.37138 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 101.8714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 104.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 106.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 109.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 119.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 111.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 114.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 116.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 129.3714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 134.3714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 131.8714 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 96.87138 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 136.8714 -8.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 94.37138 -6.002795 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.90273 26.90972 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 99.37138 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 101.8714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 104.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 106.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 109.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 119.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 111.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 114.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 116.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 94.37138 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 129.3714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 134.3714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 131.8714 -8.002795 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 96.87138 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 136.8714 -10.0028 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
