SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       cap303_support-mat119.1-0 ; 
       cap303_support-mat120.1-0 ; 
       cap303_support-mat121.1-0 ; 
       cap303_support-mat122.1-0 ; 
       fig31_belter_scout-mat86.1-0 ; 
       fig31_belter_scout-mat87.1-0 ; 
       fig31_belter_scout-mat95.1-0 ; 
       fig31_belter_scout-mat96.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig33_belter_recovery-mat108.1-0 ; 
       fig33_belter_recovery-mat109.1-0 ; 
       fig33_belter_recovery-mat110.1-0 ; 
       fig33_belter_recovery-mat111.1-0 ; 
       fig33_belter_recovery-mat112.1-0 ; 
       fig33_belter_recovery-mat113.1-0 ; 
       fig33_belter_recovery-mat114.1-0 ; 
       fig33_belter_recovery-mat115.1-0 ; 
       fig33_belter_recovery-mat117.1-0 ; 
       fig33_belter_recovery-mat118.1-0 ; 
       fig33_belter_recovery-mat95.1-0 ; 
       fig33_belter_recovery-mat96.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       cap303_support-cube2_1_2.1-0 ROOT ; 
       fig31_belter_scout-cube4.1-0 ROOT ; 
       fig31_belter_scout-cyl1.1-0 ; 
       fig31_belter_scout-scout2.1-0 ROOT ; 
       fig32_belter_stealth-cyl1_1.1-0 ROOT ; 
       fig33_belter_recovery-cube13.1-0 ROOT ; 
       fig33_belter_recovery-cube14.1-0 ROOT ; 
       fig33_belter_recovery-cube15.1-0 ROOT ; 
       fig33_belter_recovery-cube17.1-0 ROOT ; 
       fig33_belter_recovery-cube18.1-0 ROOT ; 
       fig33_belter_recovery-cube19.1-0 ROOT ; 
       fig33_belter_recovery-cube7.1-0 ROOT ; 
       fig33_belter_recovery-cube9.1-0 ROOT ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       parts-cap303-support.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       cap303_support-t2d37.1-0 ; 
       cap303_support-t2d38.1-0 ; 
       cap303_support-t2d39.1-0 ; 
       cap303_support-t2d40.1-0 ; 
       fig31_belter_scout-t2d12.1-0 ; 
       fig31_belter_scout-t2d13.1-0 ; 
       fig31_belter_scout-t2d3.1-0 ; 
       fig31_belter_scout-t2d4.1-0 ; 
       fig32_belter_stealth-t2d12.1-0 ; 
       fig32_belter_stealth-t2d16.1-0 ; 
       fig33_belter_recovery-t2d13.1-0 ; 
       fig33_belter_recovery-t2d14.1-0 ; 
       fig33_belter_recovery-t2d26.1-0 ; 
       fig33_belter_recovery-t2d27.1-0 ; 
       fig33_belter_recovery-t2d28.1-0 ; 
       fig33_belter_recovery-t2d29.1-0 ; 
       fig33_belter_recovery-t2d30.1-0 ; 
       fig33_belter_recovery-t2d31.1-0 ; 
       fig33_belter_recovery-t2d32.1-0 ; 
       fig33_belter_recovery-t2d33.1-0 ; 
       fig33_belter_recovery-t2d35.1-0 ; 
       fig33_belter_recovery-t2d36.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       13 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 6 300 ; 
       2 7 300 ; 
       5 15 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       10 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       12 11 300 ; 
       13 21 300 ; 
       13 10 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       14 20 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 12 401 ; 
       11 13 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 19 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       18 20 401 ; 
       19 21 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 0 0 SRT 1.1356 1.1356 1.1356 0 0 0 1.965659 0.194812 8.690753 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 0.2339999 0.2339999 0.2339999 0 0 0 2.063391 -0.3921629 9.29509 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 1.615559 -0.2374616 3.228479 MPRFLG 0 ; 
       6 SCHEM 22.5 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 1.615559 -0.4403909 3.215108 MPRFLG 0 ; 
       7 SCHEM 25 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 2.71634 -0.1464911 7.180466 MPRFLG 0 ; 
       8 SCHEM 27.5 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 1.935521 0.8267608 5.290966 MPRFLG 0 ; 
       9 SCHEM 30 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 2.072864 0.8035671 6.580021 MPRFLG 0 ; 
       3 SCHEM 5 0 0 SRT 0.1939999 0.1939999 0.1939999 0 0 0 0.4998045 -0.2778366 6.604116 MPRFLG 0 ; 
       10 SCHEM 17.5 0 0 SRT 0.7559999 0.7559999 0.7559999 0 3.141593 0 2.636703 0.3415902 5.49426 MPRFLG 0 ; 
       11 SCHEM 10 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 2.184948 0.6966768 6.461172 MPRFLG 0 ; 
       12 SCHEM 15 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 1.342897 0.3415902 5.49426 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 3.151221 0.1960239 2.940848 MPRFLG 0 ; 
       14 SCHEM 12.5 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 2.613946 0.03596889 6.334943 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
