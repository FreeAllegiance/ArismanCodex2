SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ss303_ordinance-cam_int1.3-0 ROOT ; 
       ss303_ordinance-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       cap303_support-mat119.1-0 ; 
       cap303_support-mat120.1-0 ; 
       cap303_support-mat121.1-0 ; 
       cap303_support-mat122.1-0 ; 
       cap303_support-mat123.1-0 ; 
       cap303_support-mat124.1-0 ; 
       cap303_support-mat125.1-0 ; 
       cap303_support-mat126.1-0 ; 
       cap303_support-mat127.1-0 ; 
       fig31_belter_scout-mat86.1-0 ; 
       fig31_belter_scout-mat87.1-0 ; 
       fig31_belter_scout-mat95.1-0 ; 
       fig31_belter_scout-mat96.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig33_belter_recovery-mat108.1-0 ; 
       fig33_belter_recovery-mat109.1-0 ; 
       fig33_belter_recovery-mat110.1-0 ; 
       fig33_belter_recovery-mat111.1-0 ; 
       fig33_belter_recovery-mat112.1-0 ; 
       fig33_belter_recovery-mat113.1-0 ; 
       fig33_belter_recovery-mat114.1-0 ; 
       fig33_belter_recovery-mat115.1-0 ; 
       fig33_belter_recovery-mat117.1-0 ; 
       fig33_belter_recovery-mat118.1-0 ; 
       fig33_belter_recovery-mat95.1-0 ; 
       fig33_belter_recovery-mat96.1-0 ; 
       ss305_electronic-mat58.1-0 ; 
       ss305_elect_station-white_strobe1_5.1-0 ; 
       ss305_elect_station-white_strobe1_9.1-0 ; 
       ss306_ripcord-mat5.1-0 ; 
       ss306_ripcord-white_strobe1_10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       cap303_support-cube12_1.1-0 ROOT ; 
       cap303_support-cube13.1-0 ROOT ; 
       cap303_support-cube14.1-0 ROOT ; 
       cap303_support-cube15.1-0 ; 
       cap303_support-cube15_1.2-0 ; 
       cap303_support-cube15_2.1-0 ROOT ; 
       cap303_support-cube17.1-0 ROOT ; 
       cap303_support-cube18.1-0 ; 
       cap303_support-cube19.1-0 ; 
       cap303_support-cube2_1_2.1-0 ; 
       cap303_support-cube28.2-0 ROOT ; 
       cap303_support-cube33.1-0 ; 
       cap303_support-cube34.2-0 ; 
       cap303_support-cube36.1-0 ; 
       cap303_support-cube4.1-0 ROOT ; 
       cap303_support-cube6_2.2-0 ROOT ; 
       cap303_support-cube7.1-0 ; 
       cap303_support-cube9.1-0 ROOT ; 
       cap303_support-cyl1.1-0 ; 
       cap303_support-cyl1_1.1-0 ; 
       cap303_support-cyl1_3.1-0 ; 
       cap303_support-cyl1_8.1-0 ROOT ; 
       cap303_support-cyl1_9.1-0 ; 
       cap303_support-cyl16.2-0 ROOT ; 
       cap303_support-extru51.3-0 ROOT ; 
       cap303_support-null20.2-0 ROOT ; 
       cap303_support-scout2.1-0 ROOT ; 
       cap303_support-sphere5.1-0 ; 
       cap303_support-sphere7.2-0 ; 
       cap303_support-sphere8_1.2-0 ; 
       cap303_support-sphere9.1-0 ; 
       cap303_support-spline1.3-0 ROOT ; 
       cap303_support-tetra1.2-0 ; 
       cap303_support-tetra2.1-0 ; 
       root-cube11.3-0 ROOT ; 
       root-cube12.3-0 ROOT ; 
       root-cube13.3-0 ROOT ; 
       root-cube14.3-0 ROOT ; 
       root-cube35.3-0 ROOT ; 
       root-extru48.1-0 ; 
       root-extru50.3-0 ROOT ; 
       root-null18_4.3-0 ; 
       root-null19_4.3-0 ; 
       root-root.3-0 ROOT ; 
       root-sphere6_1.7-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap303-support.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       cap303_support-t2d37.1-0 ; 
       cap303_support-t2d38.1-0 ; 
       cap303_support-t2d39.1-0 ; 
       cap303_support-t2d40.1-0 ; 
       cap303_support-t2d41.1-0 ; 
       cap303_support-t2d42.1-0 ; 
       cap303_support-t2d43.1-0 ; 
       cap303_support-t2d44.1-0 ; 
       cap303_support-t2d45.1-0 ; 
       fig31_belter_scout-t2d12.1-0 ; 
       fig31_belter_scout-t2d13.1-0 ; 
       fig31_belter_scout-t2d3.1-0 ; 
       fig31_belter_scout-t2d4.1-0 ; 
       fig32_belter_stealth-t2d12.1-0 ; 
       fig32_belter_stealth-t2d16.1-0 ; 
       fig33_belter_recovery-t2d13.1-0 ; 
       fig33_belter_recovery-t2d14.1-0 ; 
       fig33_belter_recovery-t2d26.1-0 ; 
       fig33_belter_recovery-t2d27.1-0 ; 
       fig33_belter_recovery-t2d28.1-0 ; 
       fig33_belter_recovery-t2d29.1-0 ; 
       fig33_belter_recovery-t2d30.1-0 ; 
       fig33_belter_recovery-t2d31.1-0 ; 
       fig33_belter_recovery-t2d32.1-0 ; 
       fig33_belter_recovery-t2d33.1-0 ; 
       fig33_belter_recovery-t2d35.1-0 ; 
       fig33_belter_recovery-t2d36.1-0 ; 
       ss305_electronic-t2d58.2-0 ; 
       ss306_ripcord-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 25 110 ; 
       7 25 110 ; 
       8 16 110 ; 
       9 25 110 ; 
       16 25 110 ; 
       18 14 110 ; 
       19 25 110 ; 
       27 25 110 ; 
       4 9 110 ; 
       11 4 110 ; 
       12 9 110 ; 
       20 11 110 ; 
       39 44 110 ; 
       41 44 110 ; 
       42 44 110 ; 
       44 43 110 ; 
       28 11 110 ; 
       29 9 110 ; 
       45 41 110 ; 
       46 41 110 ; 
       47 41 110 ; 
       48 41 110 ; 
       49 42 110 ; 
       50 42 110 ; 
       51 42 110 ; 
       52 42 110 ; 
       53 42 110 ; 
       32 4 110 ; 
       13 5 110 ; 
       22 13 110 ; 
       30 13 110 ; 
       33 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 20 300 ; 
       2 21 300 ; 
       3 22 300 ; 
       6 23 300 ; 
       7 24 300 ; 
       8 17 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       14 11 300 ; 
       15 4 300 ; 
       15 5 300 ; 
       15 6 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       17 16 300 ; 
       18 12 300 ; 
       19 13 300 ; 
       19 14 300 ; 
       21 26 300 ; 
       21 15 300 ; 
       26 9 300 ; 
       26 10 300 ; 
       27 25 300 ; 
       39 27 300 ; 
       44 30 300 ; 
       45 29 300 ; 
       46 29 300 ; 
       47 29 300 ; 
       48 29 300 ; 
       49 28 300 ; 
       50 28 300 ; 
       51 28 300 ; 
       52 28 300 ; 
       53 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 11 401 ; 
       10 12 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 19 401 ; 
       18 20 401 ; 
       19 21 401 ; 
       20 22 401 ; 
       21 23 401 ; 
       22 24 401 ; 
       23 25 401 ; 
       24 26 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 27 401 ; 
       30 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 20 0 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -4.38791 -11.57327 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -5.696227 -11.65947 MPRFLG 0 ; 
       3 SCHEM 19.64774 18.32404 0 MPRFLG 0 ; 
       6 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -32.79261 10.38918 4.769251 MPRFLG 0 ; 
       7 SCHEM 22.14774 18.32404 0 MPRFLG 0 ; 
       8 SCHEM 17.14774 18.32404 0 MPRFLG 0 ; 
       9 SCHEM 29.64774 18.32404 0 MPRFLG 0 ; 
       10 SCHEM 20.4397 -5.145172 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.04245165 3.543613 0.1017723 MPRFLG 0 ; 
       14 SCHEM 7.147742 18.32404 0 DISPLAY 0 0 SRT 0.4023952 0.4023952 0.4023952 0 0 0 0.9902803 -2.954316 18.01709 MPRFLG 0 ; 
       15 SCHEM 17.9397 -5.145172 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.20261 4.215007 2.398645 MPRFLG 0 ; 
       16 SCHEM 9.647742 18.32404 0 MPRFLG 0 ; 
       17 SCHEM 14.64774 18.32404 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -5.147649 -2.075261 7.59161 MPRFLG 0 ; 
       18 SCHEM 7.147742 16.32404 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 4.647741 18.32404 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -2 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 -6.283185 -25.77269 0.06856466 0.7582379 MPRFLG 0 ; 
       23 SCHEM 22.9397 -5.145172 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 0 0 -3.697744 3.626225 -3.783015 MPRFLG 0 ; 
       24 SCHEM 53.73915 21.2482 0 USR DISPLAY 0 0 SRT 10.10422 1.725192 1.725192 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 19.64774 20.32404 0 USR DISPLAY 0 0 SRT 6.44716 6.44716 6.44716 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 34.64774 14.32404 0 DISPLAY 0 0 SRT 0.5855406 0.5855406 0.5855406 4.39534e-008 0 -3.141593 -9.975723 -2.848454 -9.897876 MPRFLG 0 ; 
       27 SCHEM 12.14774 18.32404 0 MPRFLG 0 ; 
       31 SCHEM 70.51816 10.33117 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 53.01816 6.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -48.95172 9.585498 7.333853 MPRFLG 0 ; 
       35 SCHEM 43.01816 4.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 15.4482 -0.8918665 7.428855 MPRFLG 0 ; 
       0 SCHEM 65.51816 4.813061 0 DISPLAY 0 0 SRT 0.9999999 0.9999999 0.9999999 0 8.85024e-007 0 -19.00443 11.67541 -12.789 MPRFLG 0 ; 
       36 SCHEM 45.51816 4.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.0654015 -16.31843 7.393187 MPRFLG 0 ; 
       37 SCHEM 48.01816 4.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.06542518 14.66221 7.393198 MPRFLG 0 ; 
       4 SCHEM 27.14774 16.32404 0 MPRFLG 0 ; 
       11 SCHEM 25.89774 14.32404 0 MPRFLG 0 ; 
       12 SCHEM 34.64774 16.32404 0 MPRFLG 0 ; 
       38 SCHEM 50.51816 4.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.60077 -0.8918437 7.428874 MPRFLG 0 ; 
       20 SCHEM 27.14774 12.32404 0 MPRFLG 0 ; 
       39 SCHEM 58.01816 6.813061 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 68.01816 4.813061 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -21.06111 25.55983 -7.28371 MPRFLG 0 ; 
       41 SCHEM 58.71256 -2.835424 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 46.03421 -1.782466 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 55.3127 10.33117 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       44 SCHEM 55.51816 8.813061 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 24.64774 12.32404 0 MPRFLG 0 ; 
       29 SCHEM 32.14774 16.32404 0 MPRFLG 0 ; 
       45 SCHEM 54.96256 -4.835425 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 57.46256 -4.835425 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 59.96256 -4.835425 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 62.46256 -4.835425 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 49.09506 -5.453141 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 46.59506 -5.453141 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 44.09507 -5.453141 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 41.59507 -5.453141 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 51.28487 -5.450098 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 29.64774 14.32404 0 MPRFLG 0 ; 
       5 SCHEM 75.51816 21.2482 0 DISPLAY 0 0 SRT 1 1 1 0 0 0.13 20.09531 22.6669 -6.428631 MPRFLG 0 ; 
       13 SCHEM 74.26816 19.2482 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 75.51816 17.2482 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 73.01816 17.2482 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 78.01816 19.2482 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 259.3538 -81.6575 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 68.49998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 70.99998 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 41.38364 0.2948585 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94.96951 10.2662 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 259.3538 -83.6575 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.35529 -0.741251 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
