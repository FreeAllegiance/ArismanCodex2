SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.1-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       cap301_belter_frigate-mat100.1-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap303_support-mat119.2-0 ; 
       cap303_support-mat120.2-0 ; 
       cap303_support-mat121.2-0 ; 
       cap303_support-mat122.2-0 ; 
       cap303_support-mat123.2-0 ; 
       cap303_support-mat124.2-0 ; 
       cap303_support-mat125.2-0 ; 
       cap303_support-mat126.2-0 ; 
       cap303_support-mat127.2-0 ; 
       cap303_support-mat128.1-0 ; 
       cap303_support-mat129.1-0 ; 
       cap303_support-mat130.1-0 ; 
       fig31_belter_scout-mat86.2-0 ; 
       fig31_belter_scout-mat87.2-0 ; 
       fig31_belter_scout-mat95.2-0 ; 
       fig31_belter_scout-mat96.2-0 ; 
       fig32_belter_stealth-mat94.2-0 ; 
       fig32_belter_stealth-mat98.2-0 ; 
       fig33_belter_recovery-mat108.2-0 ; 
       fig33_belter_recovery-mat109.2-0 ; 
       fig33_belter_recovery-mat110.2-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.2-0 ; 
       fig33_belter_recovery-mat113.2-0 ; 
       fig33_belter_recovery-mat114.2-0 ; 
       fig33_belter_recovery-mat115.2-0 ; 
       fig33_belter_recovery-mat117.2-0 ; 
       fig33_belter_recovery-mat118.2-0 ; 
       fig33_belter_recovery-mat95.2-0 ; 
       fig33_belter_recovery-mat96.2-0 ; 
       ss305_electronic-mat58.2-0 ; 
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       cap303_support-cube12_1.2-0 ROOT ; 
       cap303_support-cube13.2-0 ROOT ; 
       cap303_support-cube14.2-0 ROOT ; 
       cap303_support-cube15.1-0 ; 
       cap303_support-cube15_1.2-0 ; 
       cap303_support-cube15_2.2-0 ROOT ; 
       cap303_support-cube17.2-0 ROOT ; 
       cap303_support-cube18.1-0 ; 
       cap303_support-cube19.1-0 ; 
       cap303_support-cube2_1_2.1-0 ; 
       cap303_support-cube28.3-0 ROOT ; 
       cap303_support-cube33.1-0 ; 
       cap303_support-cube34.2-0 ; 
       cap303_support-cube36.1-0 ; 
       cap303_support-cube4.2-0 ROOT ; 
       cap303_support-cube6_2.3-0 ROOT ; 
       cap303_support-cube6_3.1-0 ; 
       cap303_support-cube7.1-0 ; 
       cap303_support-cube9.2-0 ROOT ; 
       cap303_support-cyl1.1-0 ; 
       cap303_support-cyl1_1.1-0 ; 
       cap303_support-cyl1_3.1-0 ; 
       cap303_support-cyl1_8.2-0 ROOT ; 
       cap303_support-cyl1_9.1-0 ; 
       cap303_support-cyl15.1-0 ; 
       cap303_support-cyl16.3-0 ROOT ; 
       cap303_support-extru51.4-0 ROOT ; 
       cap303_support-null20.3-0 ROOT ; 
       cap303_support-scout2.2-0 ROOT ; 
       cap303_support-sphere5.1-0 ; 
       cap303_support-sphere7.2-0 ; 
       cap303_support-sphere8_1.2-0 ; 
       cap303_support-sphere9.1-0 ; 
       cap303_support-spline1.4-0 ROOT ; 
       cap303_support-tetra1.2-0 ; 
       cap303_support-tetra2.1-0 ; 
       root-cube11.4-0 ROOT ; 
       root-cube12.4-0 ROOT ; 
       root-cube13.4-0 ROOT ; 
       root-cube14.4-0 ROOT ; 
       root-cube35.4-0 ROOT ; 
       root-extru48.1-0 ; 
       root-extru50.4-0 ROOT ; 
       root-null18_4.3-0 ; 
       root-null19_4.3-0 ; 
       root-root.4-0 ROOT ; 
       root-sphere6_1.7-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap303-support.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       cap301_belter_frigate-t2d18.1-0 ; 
       cap301_belter_frigate-t2d19.1-0 ; 
       cap303_support-t2d37.2-0 ; 
       cap303_support-t2d38.2-0 ; 
       cap303_support-t2d39.2-0 ; 
       cap303_support-t2d40.2-0 ; 
       cap303_support-t2d41.2-0 ; 
       cap303_support-t2d42.2-0 ; 
       cap303_support-t2d43.2-0 ; 
       cap303_support-t2d44.2-0 ; 
       cap303_support-t2d45.2-0 ; 
       cap303_support-t2d59.1-0 ; 
       cap303_support-t2d60.1-0 ; 
       cap303_support-t2d61.1-0 ; 
       fig31_belter_scout-t2d12.2-0 ; 
       fig31_belter_scout-t2d13.2-0 ; 
       fig31_belter_scout-t2d3.2-0 ; 
       fig31_belter_scout-t2d4.2-0 ; 
       fig32_belter_stealth-t2d12.2-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig33_belter_recovery-t2d13.2-0 ; 
       fig33_belter_recovery-t2d14.2-0 ; 
       fig33_belter_recovery-t2d26.2-0 ; 
       fig33_belter_recovery-t2d27.2-0 ; 
       fig33_belter_recovery-t2d28.2-0 ; 
       fig33_belter_recovery-t2d29.2-0 ; 
       fig33_belter_recovery-t2d30.2-0 ; 
       fig33_belter_recovery-t2d31.2-0 ; 
       fig33_belter_recovery-t2d32.2-0 ; 
       fig33_belter_recovery-t2d33.2-0 ; 
       fig33_belter_recovery-t2d35.2-0 ; 
       fig33_belter_recovery-t2d36.2-0 ; 
       ss305_electronic-t2d58.3-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 27 110 ; 
       7 27 110 ; 
       8 17 110 ; 
       9 27 110 ; 
       17 27 110 ; 
       19 14 110 ; 
       20 12 110 ; 
       29 27 110 ; 
       4 9 110 ; 
       11 4 110 ; 
       12 9 110 ; 
       21 11 110 ; 
       41 46 110 ; 
       43 46 110 ; 
       44 46 110 ; 
       46 45 110 ; 
       30 11 110 ; 
       31 9 110 ; 
       47 43 110 ; 
       48 43 110 ; 
       49 43 110 ; 
       50 43 110 ; 
       51 44 110 ; 
       52 44 110 ; 
       53 44 110 ; 
       54 44 110 ; 
       55 44 110 ; 
       34 4 110 ; 
       13 5 110 ; 
       23 13 110 ; 
       32 13 110 ; 
       35 5 110 ; 
       16 9 110 ; 
       24 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 25 300 ; 
       2 26 300 ; 
       3 27 300 ; 
       6 28 300 ; 
       7 29 300 ; 
       8 22 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       14 16 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       17 23 300 ; 
       17 24 300 ; 
       18 21 300 ; 
       19 17 300 ; 
       20 18 300 ; 
       20 19 300 ; 
       22 31 300 ; 
       22 20 300 ; 
       28 14 300 ; 
       28 15 300 ; 
       29 30 300 ; 
       41 32 300 ; 
       46 35 300 ; 
       47 34 300 ; 
       48 34 300 ; 
       49 34 300 ; 
       50 34 300 ; 
       51 33 300 ; 
       52 33 300 ; 
       53 33 300 ; 
       54 33 300 ; 
       55 36 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       24 0 300 ; 
       24 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 22 401 ; 
       21 23 401 ; 
       22 24 401 ; 
       23 25 401 ; 
       24 26 401 ; 
       25 27 401 ; 
       26 28 401 ; 
       27 29 401 ; 
       28 30 401 ; 
       29 31 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 32 401 ; 
       35 33 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 18.66487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -4.38791 -11.57327 MPRFLG 0 ; 
       2 SCHEM 7.5 -17.78431 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -5.696227 -11.65947 MPRFLG 0 ; 
       3 SCHEM 4.002826 10.93445 0 MPRFLG 0 ; 
       6 SCHEM 21.16487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -32.79261 10.38918 4.769251 MPRFLG 0 ; 
       7 SCHEM 6.502826 10.93445 0 MPRFLG 0 ; 
       8 SCHEM -5.997175 8.93445 0 MPRFLG 0 ; 
       9 SCHEM 26.50283 10.93445 0 MPRFLG 0 ; 
       10 SCHEM 16.16487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.04245165 3.543613 0.1017723 MPRFLG 0 ; 
       14 SCHEM 1.164862 -20.16392 0 USR DISPLAY 0 0 SRT 0.4023952 0.4023952 0.4023952 0 0 0 0.9902803 -2.954316 18.01709 MPRFLG 0 ; 
       15 SCHEM 3.664862 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.20261 4.215007 2.398645 MPRFLG 0 ; 
       17 SCHEM -3.497175 10.93445 0 MPRFLG 0 ; 
       18 SCHEM 6.164861 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -5.147649 -2.075261 7.59161 MPRFLG 0 ; 
       19 SCHEM 1.164862 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 20.25283 6.93445 0 MPRFLG 0 ; 
       22 SCHEM 8.66487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 -6.283185 -25.77269 0.06856466 0.7582379 MPRFLG 0 ; 
       25 SCHEM 11.16487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 0 0 -3.697744 3.626225 -3.783015 MPRFLG 0 ; 
       26 SCHEM 13.66487 -20.16392 0 USR DISPLAY 0 0 SRT 10.10422 1.725192 1.725192 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 19.00283 12.93445 0 USR DISPLAY 0 0 SRT 1.171526 1.171526 1.171526 0 0 0 -0.4286826 0.6350855 0 MPRFLG 0 ; 
       28 SCHEM 10 -17.78431 0 DISPLAY 0 0 SRT 0.5855406 0.5855406 0.5855406 4.39534e-008 0 -3.141593 -9.975723 -2.848454 -9.897876 MPRFLG 0 ; 
       29 SCHEM 1.502825 10.93445 0 MPRFLG 0 ; 
       33 SCHEM 23.66487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       36 SCHEM 26.16486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -48.95172 9.585498 7.333853 MPRFLG 0 ; 
       37 SCHEM 28.66486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 15.4482 -0.8918665 7.428855 MPRFLG 0 ; 
       0 SCHEM 31.16486 -20.16392 0 USR DISPLAY 0 0 SRT 0.9999999 0.9999999 0.9999999 0 8.85024e-007 0 -19.00443 11.67541 -12.789 MPRFLG 0 ; 
       38 SCHEM 33.66486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.0654015 -16.31843 7.393187 MPRFLG 0 ; 
       39 SCHEM 0 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.06542518 14.66221 7.393198 MPRFLG 0 ; 
       4 SCHEM 11.50282 8.93445 0 MPRFLG 0 ; 
       11 SCHEM 10.25282 6.93445 0 MPRFLG 0 ; 
       12 SCHEM 20.25283 8.93445 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.60077 -0.8918437 7.428874 MPRFLG 0 ; 
       21 SCHEM 11.50282 4.93445 0 MPRFLG 0 ; 
       41 SCHEM 14.43914 -3.518109 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 5 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -21.06111 25.55983 -7.28371 MPRFLG 0 ; 
       43 SCHEM 19.61749 -13.16659 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 6.93914 -12.11364 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 16.21763 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       46 SCHEM 16.42309 -1.518109 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 9.002824 4.93445 0 MPRFLG 0 ; 
       31 SCHEM 16.50282 8.93445 0 MPRFLG 0 ; 
       47 SCHEM 15.86749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 18.36749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 20.86749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 23.36749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 9.999989 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 7.499989 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 5 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 2.5 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 12.1898 -15.78127 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 14.00282 6.93445 0 MPRFLG 0 ; 
       5 SCHEM -3.835138 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0.13 20.09531 22.6669 -6.428631 MPRFLG 0 ; 
       13 SCHEM -5.085138 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM -3.835138 -24.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM -6.335138 -24.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM -1.335138 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 29.00283 8.93445 0 MPRFLG 0 ; 
       24 SCHEM 25.25283 6.93445 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 62.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 27.36749 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 74.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 32.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 215.7748 -91.98868 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 8.999989 -17.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14.86749 -17.1666 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 2.28857 -10.03631 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.1898 -17.78127 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 307.7748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 57.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 62.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.36749 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 74.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 32.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 77.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 215.7748 -93.98868 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 2.26022 -11.07242 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 307.7748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
