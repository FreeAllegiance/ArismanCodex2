SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.2-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       cap301_belter_frigate-mat100.1-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap303_support-mat119.2-0 ; 
       cap303_support-mat120.2-0 ; 
       cap303_support-mat121.2-0 ; 
       cap303_support-mat122.2-0 ; 
       cap303_support-mat123.2-0 ; 
       cap303_support-mat124.2-0 ; 
       cap303_support-mat125.2-0 ; 
       cap303_support-mat126.2-0 ; 
       cap303_support-mat127.2-0 ; 
       cap303_support-mat128.1-0 ; 
       cap303_support-mat129.1-0 ; 
       cap303_support-mat130.1-0 ; 
       cap303_support-mat131.1-0 ; 
       cap303_support-mat132.1-0 ; 
       cap303_support-mat133.1-0 ; 
       cap303_support-mat134.1-0 ; 
       cap303_support-mat135.1-0 ; 
       cap303_support-mat136.1-0 ; 
       cap303_support-mat137.1-0 ; 
       cap303_support-mat138.1-0 ; 
       cap303_support-mat139.1-0 ; 
       cap303_support-mat140.1-0 ; 
       fig31_belter_scout-mat86.2-0 ; 
       fig31_belter_scout-mat87.2-0 ; 
       fig31_belter_scout-mat95.2-0 ; 
       fig31_belter_scout-mat96.2-0 ; 
       fig32_belter_stealth-mat94.2-0 ; 
       fig32_belter_stealth-mat98.2-0 ; 
       fig33_belter_recovery-mat108.2-0 ; 
       fig33_belter_recovery-mat109.2-0 ; 
       fig33_belter_recovery-mat110.2-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.2-0 ; 
       fig33_belter_recovery-mat113.2-0 ; 
       fig33_belter_recovery-mat114.2-0 ; 
       fig33_belter_recovery-mat115.2-0 ; 
       fig33_belter_recovery-mat117.2-0 ; 
       fig33_belter_recovery-mat118.2-0 ; 
       fig33_belter_recovery-mat95.2-0 ; 
       fig33_belter_recovery-mat96.2-0 ; 
       ss305_electronic-mat58.2-0 ; 
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 59     
       cap303_support-cube12_1.2-0 ROOT ; 
       cap303_support-cube13.2-0 ROOT ; 
       cap303_support-cube14.2-0 ROOT ; 
       cap303_support-cube15.1-0 ; 
       cap303_support-cube15_1.2-0 ; 
       cap303_support-cube15_2.2-0 ROOT ; 
       cap303_support-cube17.2-0 ROOT ; 
       cap303_support-cube18.1-0 ; 
       cap303_support-cube19.1-0 ; 
       cap303_support-cube2_1_2.1-0 ; 
       cap303_support-cube2_1_3.1-0 ROOT ; 
       cap303_support-cube2_1_4.1-0 ROOT ; 
       cap303_support-cube28.3-0 ROOT ; 
       cap303_support-cube33.1-0 ; 
       cap303_support-cube34.2-0 ; 
       cap303_support-cube36.1-0 ; 
       cap303_support-cube37.1-0 ROOT ; 
       cap303_support-cube4.2-0 ROOT ; 
       cap303_support-cube6_2.3-0 ROOT ; 
       cap303_support-cube6_3.1-0 ; 
       cap303_support-cube7.1-0 ; 
       cap303_support-cube9.2-0 ROOT ; 
       cap303_support-cyl1.1-0 ; 
       cap303_support-cyl1_1.1-0 ; 
       cap303_support-cyl1_3.1-0 ; 
       cap303_support-cyl1_8.2-0 ROOT ; 
       cap303_support-cyl1_9.1-0 ; 
       cap303_support-cyl15.1-0 ; 
       cap303_support-cyl16.3-0 ROOT ; 
       cap303_support-extru51.4-0 ROOT ; 
       cap303_support-null20.4-0 ROOT ; 
       cap303_support-scout2.2-0 ROOT ; 
       cap303_support-sphere5.1-0 ; 
       cap303_support-sphere7.2-0 ; 
       cap303_support-sphere8_1.2-0 ; 
       cap303_support-sphere9.1-0 ; 
       cap303_support-spline1.4-0 ROOT ; 
       cap303_support-tetra1.2-0 ; 
       cap303_support-tetra2.1-0 ; 
       root-cube11.4-0 ROOT ; 
       root-cube12.4-0 ROOT ; 
       root-cube13.4-0 ROOT ; 
       root-cube14.4-0 ROOT ; 
       root-cube35.4-0 ROOT ; 
       root-extru48.1-0 ; 
       root-extru50.4-0 ROOT ; 
       root-null18_4.3-0 ; 
       root-null19_4.3-0 ; 
       root-root.4-0 ROOT ; 
       root-sphere6_1.7-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-cap303-support.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       cap301_belter_frigate-t2d18.1-0 ; 
       cap301_belter_frigate-t2d19.1-0 ; 
       cap303_support-t2d37.2-0 ; 
       cap303_support-t2d38.2-0 ; 
       cap303_support-t2d39.2-0 ; 
       cap303_support-t2d40.2-0 ; 
       cap303_support-t2d41.2-0 ; 
       cap303_support-t2d42.2-0 ; 
       cap303_support-t2d43.2-0 ; 
       cap303_support-t2d44.2-0 ; 
       cap303_support-t2d45.2-0 ; 
       cap303_support-t2d59.1-0 ; 
       cap303_support-t2d60.1-0 ; 
       cap303_support-t2d61.1-0 ; 
       cap303_support-t2d62.1-0 ; 
       cap303_support-t2d63.1-0 ; 
       cap303_support-t2d64.1-0 ; 
       cap303_support-t2d65.1-0 ; 
       cap303_support-t2d66.1-0 ; 
       cap303_support-t2d67.1-0 ; 
       cap303_support-t2d68.1-0 ; 
       cap303_support-t2d69.1-0 ; 
       cap303_support-t2d70.1-0 ; 
       cap303_support-t2d71.1-0 ; 
       fig31_belter_scout-t2d12.2-0 ; 
       fig31_belter_scout-t2d13.2-0 ; 
       fig31_belter_scout-t2d3.2-0 ; 
       fig31_belter_scout-t2d4.2-0 ; 
       fig32_belter_stealth-t2d12.2-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig33_belter_recovery-t2d13.2-0 ; 
       fig33_belter_recovery-t2d14.2-0 ; 
       fig33_belter_recovery-t2d26.2-0 ; 
       fig33_belter_recovery-t2d27.2-0 ; 
       fig33_belter_recovery-t2d28.2-0 ; 
       fig33_belter_recovery-t2d29.2-0 ; 
       fig33_belter_recovery-t2d30.2-0 ; 
       fig33_belter_recovery-t2d31.2-0 ; 
       fig33_belter_recovery-t2d32.2-0 ; 
       fig33_belter_recovery-t2d33.2-0 ; 
       fig33_belter_recovery-t2d35.2-0 ; 
       fig33_belter_recovery-t2d36.2-0 ; 
       ss305_electronic-t2d58.3-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 30 110 ; 
       7 30 110 ; 
       8 20 110 ; 
       9 30 110 ; 
       20 30 110 ; 
       22 17 110 ; 
       23 14 110 ; 
       32 30 110 ; 
       4 9 110 ; 
       13 4 110 ; 
       14 9 110 ; 
       24 13 110 ; 
       44 49 110 ; 
       46 49 110 ; 
       47 49 110 ; 
       49 48 110 ; 
       33 13 110 ; 
       34 9 110 ; 
       50 46 110 ; 
       51 46 110 ; 
       52 46 110 ; 
       53 46 110 ; 
       54 47 110 ; 
       55 47 110 ; 
       56 47 110 ; 
       57 47 110 ; 
       58 47 110 ; 
       37 4 110 ; 
       15 5 110 ; 
       26 15 110 ; 
       35 15 110 ; 
       38 5 110 ; 
       19 9 110 ; 
       27 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 35 300 ; 
       2 36 300 ; 
       3 37 300 ; 
       6 38 300 ; 
       7 39 300 ; 
       8 32 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       17 26 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       20 33 300 ; 
       20 34 300 ; 
       21 31 300 ; 
       22 27 300 ; 
       23 28 300 ; 
       23 29 300 ; 
       25 41 300 ; 
       25 30 300 ; 
       31 24 300 ; 
       31 25 300 ; 
       32 40 300 ; 
       44 42 300 ; 
       49 45 300 ; 
       50 44 300 ; 
       51 44 300 ; 
       52 44 300 ; 
       53 44 300 ; 
       54 43 300 ; 
       55 43 300 ; 
       56 43 300 ; 
       57 43 300 ; 
       58 46 300 ; 
       19 11 300 ; 
       19 12 300 ; 
       19 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       16 22 300 ; 
       16 23 300 ; 
       27 0 300 ; 
       27 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       24 26 401 ; 
       25 27 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 32 401 ; 
       31 33 401 ; 
       32 34 401 ; 
       33 35 401 ; 
       34 36 401 ; 
       35 37 401 ; 
       36 38 401 ; 
       37 39 401 ; 
       38 40 401 ; 
       39 41 401 ; 
       40 30 401 ; 
       41 31 401 ; 
       42 42 401 ; 
       45 43 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 18.66487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -4.38791 -11.57327 MPRFLG 0 ; 
       2 SCHEM 7.5 -17.78431 0 DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -5.696227 -11.65947 MPRFLG 0 ; 
       3 SCHEM 4.002826 10.93445 0 MPRFLG 0 ; 
       6 SCHEM 21.16487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -32.79261 10.38918 4.769251 MPRFLG 0 ; 
       7 SCHEM 6.502826 10.93445 0 MPRFLG 0 ; 
       8 SCHEM -5.997175 8.93445 0 MPRFLG 0 ; 
       9 SCHEM 26.50283 10.93445 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 16.16487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.04245165 3.543613 0.1017723 MPRFLG 0 ; 
       17 SCHEM 1.164862 -20.16392 0 USR DISPLAY 0 0 SRT 0.4023952 0.4023952 0.4023952 0 0 0 0.9902803 -2.954316 18.01709 MPRFLG 0 ; 
       18 SCHEM 3.664862 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.20261 4.215007 2.398645 MPRFLG 0 ; 
       20 SCHEM -3.497175 10.93445 0 MPRFLG 0 ; 
       21 SCHEM 6.164861 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -5.147649 -2.075261 7.59161 MPRFLG 0 ; 
       22 SCHEM 1.164862 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 20.25283 6.93445 0 MPRFLG 0 ; 
       25 SCHEM 8.66487 -20.16392 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 -6.283185 -25.77269 0.06856466 0.7582379 MPRFLG 0 ; 
       28 SCHEM 11.16487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 0 0 -3.697744 3.626225 -3.783015 MPRFLG 0 ; 
       29 SCHEM 13.66487 -20.16392 0 USR DISPLAY 0 0 SRT 10.10422 1.725192 1.725192 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 19.00283 12.93445 0 USR DISPLAY 0 0 SRT 1.171526 1.171526 1.171526 0 0 0 -0.4286826 0.6350855 0 MPRFLG 0 ; 
       31 SCHEM 10 -17.78431 0 DISPLAY 0 0 SRT 0.5855406 0.5855406 0.5855406 4.39534e-008 0 -3.141593 -9.975723 -2.848454 -9.897876 MPRFLG 0 ; 
       32 SCHEM 1.502825 10.93445 0 MPRFLG 0 ; 
       36 SCHEM 23.66487 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 26.16486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -48.95172 9.585498 7.333853 MPRFLG 0 ; 
       40 SCHEM 28.66486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 15.4482 -0.8918665 7.428855 MPRFLG 0 ; 
       0 SCHEM 31.16486 -20.16392 0 USR DISPLAY 0 0 SRT 0.9999999 0.9999999 0.9999999 0 8.85024e-007 0 -19.00443 11.67541 -12.789 MPRFLG 0 ; 
       41 SCHEM 33.66486 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.0654015 -16.31843 7.393187 MPRFLG 0 ; 
       42 SCHEM 0 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.06542518 14.66221 7.393198 MPRFLG 0 ; 
       4 SCHEM 11.50282 8.93445 0 MPRFLG 0 ; 
       13 SCHEM 10.25282 6.93445 0 MPRFLG 0 ; 
       14 SCHEM 20.25283 8.93445 0 MPRFLG 0 ; 
       43 SCHEM 2.5 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.60077 -0.8918437 7.428874 MPRFLG 0 ; 
       24 SCHEM 11.50282 4.93445 0 MPRFLG 0 ; 
       44 SCHEM 14.43914 -3.518109 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 5 -17.78431 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -21.06111 25.55983 -7.28371 MPRFLG 0 ; 
       46 SCHEM 19.61749 -13.16659 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 6.93914 -12.11364 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 16.21763 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       49 SCHEM 16.42309 -1.518109 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 9.002824 4.93445 0 MPRFLG 0 ; 
       34 SCHEM 16.50282 8.93445 0 MPRFLG 0 ; 
       50 SCHEM 15.86749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 18.36749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 20.86749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 23.36749 -15.1666 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 9.999989 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 7.499989 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 5 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 2.5 -15.78431 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 12.1898 -15.78127 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 14.00282 6.93445 0 MPRFLG 0 ; 
       5 SCHEM -3.835138 -20.16392 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0.13 20.09531 22.6669 -6.428631 MPRFLG 0 ; 
       15 SCHEM -5.085138 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM -3.835138 -24.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM -6.335138 -24.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM -1.335138 -22.16392 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 29.00283 8.93445 0 MPRFLG 0 ; 
       10 SCHEM 29.12406 15.8611 0 USR SRT 1.330386 1.330386 1.330386 0 0 0 -0.4588451 0.7590437 2.137006 MPRFLG 0 ; 
       11 SCHEM 644.3419 15.8611 0 USR SRT 1.62307 1.447779 1.62307 0 0 0 -0.4588451 0.7590437 5.948293 MPRFLG 0 ; 
       16 SCHEM 666.8419 15.8611 0 DISPLAY 1 2 SRT 0.5296328 0.5296328 0.5296328 0 0 -6.283185 0.05910544 0.6890596 3.827522 MPRFLG 0 ; 
       27 SCHEM 25.25283 6.93445 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 62.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 27.36749 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 74.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 32.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 215.7748 -91.98868 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 8.999989 -17.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14.86749 -17.1666 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 2.28857 -10.03631 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.1898 -17.78127 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 307.7748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 700.2066 0.926651 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 700.2066 0.926651 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 57.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 62.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 27.36749 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 74.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 32.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 77.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 215.7748 -93.98868 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 2.26022 -11.07242 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 700.2066 -1.073349 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 307.7748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 700.2066 -1.073349 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
