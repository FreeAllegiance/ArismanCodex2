SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.4-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 53     
       cap301_belter_frigate-mat100.1-0 ; 
       cap301_belter_frigate-mat101.1-0 ; 
       cap303_support-mat119.2-0 ; 
       cap303_support-mat120.2-0 ; 
       cap303_support-mat121.2-0 ; 
       cap303_support-mat122.2-0 ; 
       cap303_support-mat123.2-0 ; 
       cap303_support-mat124.2-0 ; 
       cap303_support-mat125.2-0 ; 
       cap303_support-mat126.2-0 ; 
       cap303_support-mat127.2-0 ; 
       cap303_support-mat128.1-0 ; 
       cap303_support-mat129.1-0 ; 
       cap303_support-mat130.1-0 ; 
       cap303_support-mat131.1-0 ; 
       cap303_support-mat132.1-0 ; 
       cap303_support-mat133.1-0 ; 
       cap303_support-mat134.1-0 ; 
       cap303_support-mat135.1-0 ; 
       cap303_support-mat136.1-0 ; 
       cap303_support-mat137.1-0 ; 
       cap303_support-mat138.1-0 ; 
       cap303_support-mat139.1-0 ; 
       cap303_support-mat140.1-0 ; 
       cap303_support-mat141.1-0 ; 
       cap303_support-mat142.1-0 ; 
       cap303_support-mat143.1-0 ; 
       cap303_support-mat144.1-0 ; 
       cap303_support-mat145.1-0 ; 
       cap303_support-mat146.1-0 ; 
       fig31_belter_scout-mat86.2-0 ; 
       fig31_belter_scout-mat87.2-0 ; 
       fig31_belter_scout-mat95.2-0 ; 
       fig31_belter_scout-mat96.2-0 ; 
       fig32_belter_stealth-mat94.2-0 ; 
       fig32_belter_stealth-mat98.2-0 ; 
       fig33_belter_recovery-mat108.2-0 ; 
       fig33_belter_recovery-mat109.2-0 ; 
       fig33_belter_recovery-mat110.2-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.2-0 ; 
       fig33_belter_recovery-mat113.2-0 ; 
       fig33_belter_recovery-mat114.2-0 ; 
       fig33_belter_recovery-mat115.2-0 ; 
       fig33_belter_recovery-mat117.2-0 ; 
       fig33_belter_recovery-mat118.2-0 ; 
       fig33_belter_recovery-mat95.2-0 ; 
       fig33_belter_recovery-mat96.2-0 ; 
       ss305_electronic-mat58.2-0 ; 
       ss305_elect_station-white_strobe1_5.2-0 ; 
       ss305_elect_station-white_strobe1_9.2-0 ; 
       ss306_ripcord-mat5.2-0 ; 
       ss306_ripcord-white_strobe1_10.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       cap303_support-cube12_1.3-0 ROOT ; 
       cap303_support-cube13.3-0 ROOT ; 
       cap303_support-cube14.3-0 ROOT ; 
       cap303_support-cube15.1-0 ; 
       cap303_support-cube15_1.2-0 ; 
       cap303_support-cube15_2.3-0 ROOT ; 
       cap303_support-cube17.3-0 ROOT ; 
       cap303_support-cube18.1-0 ; 
       cap303_support-cube19.1-0 ; 
       cap303_support-cube2_1_2.1-0 ; 
       cap303_support-cube2_1_3.1-0 ; 
       cap303_support-cube2_1_3_1.2-0 ; 
       cap303_support-cube2_1_4.2-0 ROOT ; 
       cap303_support-cube28.4-0 ROOT ; 
       cap303_support-cube33.1-0 ; 
       cap303_support-cube34.2-0 ; 
       cap303_support-cube36.1-0 ; 
       cap303_support-cube37.2-0 ROOT ; 
       cap303_support-cube38.1-0 ; 
       cap303_support-cube39.1-0 ; 
       cap303_support-cube4.3-0 ROOT ; 
       cap303_support-cube40.1-0 ; 
       cap303_support-cube41.2-0 ROOT ; 
       cap303_support-cube41_1.1-0 ; 
       cap303_support-cube41_2.1-0 ; 
       cap303_support-cube42.1-0 ; 
       cap303_support-cube43.1-0 ROOT ; 
       cap303_support-cube6_2.4-0 ROOT ; 
       cap303_support-cube6_3.1-0 ; 
       cap303_support-cube7.1-0 ; 
       cap303_support-cube9.3-0 ROOT ; 
       cap303_support-cyl1.1-0 ; 
       cap303_support-cyl1_1.1-0 ; 
       cap303_support-cyl1_1_1.1-0 ROOT ; 
       cap303_support-cyl1_3.1-0 ; 
       cap303_support-cyl1_8.3-0 ROOT ; 
       cap303_support-cyl1_9.1-0 ; 
       cap303_support-cyl15.1-0 ; 
       cap303_support-cyl16.4-0 ROOT ; 
       cap303_support-extru51.5-0 ROOT ; 
       cap303_support-null20.6-0 ROOT ; 
       cap303_support-scout2.3-0 ROOT ; 
       cap303_support-sphere5.1-0 ; 
       cap303_support-sphere7.2-0 ; 
       cap303_support-sphere8_1.1-0 ROOT ; 
       cap303_support-sphere8_2.1-0 ROOT ; 
       cap303_support-sphere9.1-0 ; 
       cap303_support-spline1.5-0 ROOT ; 
       cap303_support-tetra1.2-0 ; 
       cap303_support-tetra2.1-0 ; 
       root-cube11.5-0 ROOT ; 
       root-cube12.5-0 ROOT ; 
       root-cube13.5-0 ROOT ; 
       root-cube14.5-0 ROOT ; 
       root-cube35.5-0 ROOT ; 
       root-extru48.1-0 ; 
       root-extru50.5-0 ROOT ; 
       root-null18_4.3-0 ; 
       root-null19_4.3-0 ; 
       root-root.5-0 ROOT ; 
       root-sphere6_1.7-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_37.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bgrnd03 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/bom30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/cap301 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/fig33 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/cap303/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wiphold-cap303-support.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       cap301_belter_frigate-t2d18.1-0 ; 
       cap301_belter_frigate-t2d19.1-0 ; 
       cap303_support-t2d37.2-0 ; 
       cap303_support-t2d38.2-0 ; 
       cap303_support-t2d39.2-0 ; 
       cap303_support-t2d40.2-0 ; 
       cap303_support-t2d41.2-0 ; 
       cap303_support-t2d42.2-0 ; 
       cap303_support-t2d43.2-0 ; 
       cap303_support-t2d44.2-0 ; 
       cap303_support-t2d45.2-0 ; 
       cap303_support-t2d59.1-0 ; 
       cap303_support-t2d60.1-0 ; 
       cap303_support-t2d61.1-0 ; 
       cap303_support-t2d62.1-0 ; 
       cap303_support-t2d63.1-0 ; 
       cap303_support-t2d64.1-0 ; 
       cap303_support-t2d65.1-0 ; 
       cap303_support-t2d66.1-0 ; 
       cap303_support-t2d67.1-0 ; 
       cap303_support-t2d68.1-0 ; 
       cap303_support-t2d69.1-0 ; 
       cap303_support-t2d70.1-0 ; 
       cap303_support-t2d71.1-0 ; 
       cap303_support-t2d72.1-0 ; 
       cap303_support-t2d73.1-0 ; 
       cap303_support-t2d74.1-0 ; 
       cap303_support-t2d75.1-0 ; 
       cap303_support-t2d76.1-0 ; 
       cap303_support-t2d77.1-0 ; 
       fig31_belter_scout-t2d12.2-0 ; 
       fig31_belter_scout-t2d13.2-0 ; 
       fig31_belter_scout-t2d3.2-0 ; 
       fig31_belter_scout-t2d4.2-0 ; 
       fig32_belter_stealth-t2d12.2-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig33_belter_recovery-t2d13.2-0 ; 
       fig33_belter_recovery-t2d14.2-0 ; 
       fig33_belter_recovery-t2d26.2-0 ; 
       fig33_belter_recovery-t2d27.2-0 ; 
       fig33_belter_recovery-t2d28.2-0 ; 
       fig33_belter_recovery-t2d29.2-0 ; 
       fig33_belter_recovery-t2d30.2-0 ; 
       fig33_belter_recovery-t2d31.2-0 ; 
       fig33_belter_recovery-t2d32.2-0 ; 
       fig33_belter_recovery-t2d33.2-0 ; 
       fig33_belter_recovery-t2d35.2-0 ; 
       fig33_belter_recovery-t2d36.2-0 ; 
       ss305_electronic-t2d58.3-0 ; 
       ss306_ripcord-t2d7.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 28 110 ; 
       4 26 110 ; 
       7 25 110 ; 
       8 29 110 ; 
       9 40 110 ; 
       11 26 110 ; 
       14 4 110 ; 
       15 9 110 ; 
       16 5 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       31 20 110 ; 
       32 22 110 ; 
       34 14 110 ; 
       36 16 110 ; 
       37 28 110 ; 
       42 26 110 ; 
       43 14 110 ; 
       46 16 110 ; 
       48 4 110 ; 
       18 11 110 ; 
       19 18 110 ; 
       21 18 110 ; 
       25 26 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       10 26 110 ; 
       49 5 110 ; 
       55 60 110 ; 
       57 60 110 ; 
       58 60 110 ; 
       60 59 110 ; 
       61 57 110 ; 
       62 57 110 ; 
       63 57 110 ; 
       64 57 110 ; 
       65 58 110 ; 
       66 58 110 ; 
       67 58 110 ; 
       68 58 110 ; 
       69 58 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 41 300 ; 
       2 42 300 ; 
       3 43 300 ; 
       6 44 300 ; 
       7 45 300 ; 
       8 38 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       17 22 300 ; 
       17 23 300 ; 
       20 32 300 ; 
       27 6 300 ; 
       27 7 300 ; 
       27 8 300 ; 
       28 11 300 ; 
       28 12 300 ; 
       28 13 300 ; 
       29 39 300 ; 
       29 40 300 ; 
       30 37 300 ; 
       31 33 300 ; 
       32 34 300 ; 
       32 35 300 ; 
       35 47 300 ; 
       35 36 300 ; 
       37 0 300 ; 
       37 1 300 ; 
       41 30 300 ; 
       41 31 300 ; 
       42 46 300 ; 
       33 24 300 ; 
       33 25 300 ; 
       10 26 300 ; 
       10 27 300 ; 
       10 28 300 ; 
       10 29 300 ; 
       55 48 300 ; 
       60 51 300 ; 
       61 50 300 ; 
       62 50 300 ; 
       63 50 300 ; 
       64 50 300 ; 
       65 49 300 ; 
       66 49 300 ; 
       67 49 300 ; 
       68 49 300 ; 
       69 52 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       30 32 401 ; 
       31 33 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 38 401 ; 
       37 39 401 ; 
       38 40 401 ; 
       39 41 401 ; 
       40 42 401 ; 
       41 43 401 ; 
       42 44 401 ; 
       43 45 401 ; 
       44 46 401 ; 
       45 47 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 48 401 ; 
       51 49 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1272.875 -6.391453 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1272.875 -8.391453 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 1304.04 -26.55537 0 USR DISPLAY 0 0 SRT 0.9999999 0.9999999 0.9999999 0 8.85024e-007 0 -19.00443 11.67541 -12.789 MPRFLG 0 ; 
       1 SCHEM 1291.54 -26.55537 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -4.38791 -11.57327 MPRFLG 0 ; 
       2 SCHEM 1280.375 -24.17577 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -4.644094 -5.696227 -11.65947 MPRFLG 0 ; 
       3 SCHEM 1280.48 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 1292.98 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 1269.04 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0.13 20.09531 22.6669 -6.428631 MPRFLG 0 ; 
       6 SCHEM 1294.04 -26.55537 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -32.79261 10.38918 4.769251 MPRFLG 0 ; 
       7 SCHEM 1300.48 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 1297.98 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 1299.604 27.7648 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 1286.73 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 1314.666 19.8246 0 USR DISPLAY 0 0 SRT 1.62307 1.447779 1.62307 0 0 0 -0.4588451 0.7590437 5.948293 MPRFLG 0 ; 
       13 SCHEM 1289.04 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.04245165 3.543613 0.1017723 MPRFLG 0 ; 
       14 SCHEM 1291.73 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 1293.354 25.7648 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 1267.79 -28.55537 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 1335.588 11.9337 0 USR DISPLAY 0 0 SRT 0.5296328 0.5296328 0.5296328 0 0 -6.283185 1.748445 -3.067788 3.736258 MPRFLG 0 ; 
       20 SCHEM 1274.04 -26.55537 0 USR DISPLAY 0 0 SRT 0.4023952 0.4023952 0.4023952 0 0 0 0.9902803 -2.954316 18.01709 MPRFLG 0 ; 
       27 SCHEM 1276.54 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.20261 4.215007 2.398645 MPRFLG 0 ; 
       28 SCHEM 1281.73 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 1297.98 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 1279.04 -26.55537 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 0 -5.147649 -2.075261 7.59161 MPRFLG 0 ; 
       31 SCHEM 1274.04 -28.55537 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 1293.128 0.5429989 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 1292.98 11.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 1281.54 -26.55537 0 USR DISPLAY 0 0 SRT 4.874052 4.874052 4.874052 0 0 -6.283185 -25.77269 0.06856466 0.7582379 MPRFLG 0 ; 
       36 SCHEM 1269.04 -30.55538 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 1282.98 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 1284.04 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 0 0 -3.697744 3.626225 -3.783015 MPRFLG 0 ; 
       39 SCHEM 1286.54 -26.55537 0 USR DISPLAY 0 0 SRT 10.10422 1.725192 1.725192 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 1292.104 29.76479 0 USR DISPLAY 0 0 SRT 1.171526 1.171526 1.171526 3.117658e-013 0 2.384186e-007 -0.4286826 0.6350865 8.243009e-008 MPRFLG 0 ; 
       41 SCHEM 1282.875 -24.17577 0 USR DISPLAY 0 0 SRT 0.5855406 0.5855406 0.5855406 4.39534e-008 0 -3.141593 -9.975723 -2.848454 -9.897876 MPRFLG 0 ; 
       42 SCHEM 1277.98 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 1290.48 11.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 1289.378 2.543 0 USR DISPLAY 0 0 SRT 0.0276877 0.0276877 0.0276877 1.570796 0 2.384186e-007 -0.4976784 1.138311 -0.4422202 MPRFLG 0 ; 
       46 SCHEM 1266.54 -30.55538 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 1296.54 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       48 SCHEM 1295.48 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 1286.73 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 1285.48 11.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 1287.98 11.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 1324.335 -5.32324 0 USR DISPLAY 0 0 SRT 0.03218348 0.03218348 0.03218348 0 0.784 2.384186e-007 -1.006606 0.8787996 0.2025513 MPRFLG 0 ; 
       25 SCHEM 1302.98 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 1292.98 17.53399 0 USR DISPLAY 3 2 SRT 0.03515647 0.01153499 0.1177368 3.141593 -3.141593 0 -0.3911137 0.6609915 0.02368122 MPRFLG 0 ; 
       23 SCHEM 1302.98 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 1306.063 29.13935 0 USR DISPLAY 0 0 SRT 0.1099442 0.1099442 0.2009275 3.265861e-013 -1.685872e-007 1.509957e-007 -0.3561824 0.8634993 0.0194269 MPRFLG 0 ; 
       45 SCHEM 1328.546 14.87754 0 DISPLAY 0 0 SRT 0.01899376 0.01899376 0.01899376 1.570797 -2.943209e-014 -8.74228e-008 -0.7236417 1.790077 1.191041 MPRFLG 0 ; 
       24 SCHEM 1305.48 13.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 1307.98 15.53398 0 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 1271.54 -28.55537 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 1299.04 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -48.95172 9.585498 7.333853 MPRFLG 0 ; 
       51 SCHEM 1301.54 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 15.4482 -0.8918665 7.428855 MPRFLG 0 ; 
       52 SCHEM 1306.54 -26.55537 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.0654015 -16.31843 7.393187 MPRFLG 0 ; 
       53 SCHEM 1272.875 -24.17577 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.06542518 14.66221 7.393198 MPRFLG 0 ; 
       54 SCHEM 1275.375 -24.17577 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.60077 -0.8918437 7.428874 MPRFLG 0 ; 
       55 SCHEM 1287.315 -9.909562 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 1277.875 -24.17577 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -21.06111 25.55983 -7.28371 MPRFLG 0 ; 
       57 SCHEM 1292.493 -19.55804 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 1279.814 -18.50509 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 1289.093 -6.391453 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       60 SCHEM 1289.298 -7.909561 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 1288.743 -21.55806 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 1291.243 -21.55806 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 1293.743 -21.55806 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 1296.243 -21.55806 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 1282.875 -22.17576 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 1280.375 -22.17576 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 1277.875 -22.17576 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 1275.375 -22.17576 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 1285.065 -22.17272 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 307.7748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 641.8419 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1257.06 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 700.2066 0.926651 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 700.2066 0.926651 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 59.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 57.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 62.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 27.36749 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 74.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -19.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 32.36749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 77.36749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29.86749 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 64.86749 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 215.7748 -91.98868 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 8.999989 -17.78431 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14.86749 -17.1666 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 2.28857 -10.03631 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 11.1898 -17.78127 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.12024 21.70179 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.12024 21.70179 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 702.9307 24.21514 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 702.9307 24.21514 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 702.9307 24.21514 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 702.9307 24.21514 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 307.7748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 310.2748 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 72.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 610.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 613.4772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 615.9772 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 641.8419 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1257.06 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 700.2066 -1.073349 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 700.2066 -1.073349 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 59.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 57.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 64.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 62.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 27.36749 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 29.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 74.86749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -21.78431 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 32.36749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 77.36749 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 34.86749 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 215.7748 -93.98868 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 2.26022 -11.07242 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.12024 19.70179 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 46.12024 19.70179 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 702.9307 22.21514 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 702.9307 22.21514 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 702.9307 22.21514 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 702.9307 22.21514 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
