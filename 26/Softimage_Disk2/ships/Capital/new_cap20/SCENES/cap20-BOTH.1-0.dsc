SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       add_frames-body_front1.1-0 ; 
       add_frames-body_front1_1.1-0 ; 
       add_frames-body_side.1-0 ; 
       add_frames-body_side_1.1-0 ; 
       add_frames-cockpit1.1-0 ; 
       add_frames-cockpit1_1.1-0 ; 
       add_frames-engines1.1-0 ; 
       add_frames-engines1_1.1-0 ; 
       add_frames-engines2.1-0 ; 
       add_frames-engines2_1.1-0 ; 
       add_frames-gun_middle.1-0 ; 
       add_frames-gun_middle_1.1-0 ; 
       add_frames-gun_middle1.1-0 ; 
       add_frames-gun_middle1_1.1-0 ; 
       add_frames-gun_side.1-0 ; 
       add_frames-gun_side_1.1-0 ; 
       add_frames-gun_side1.1-0 ; 
       add_frames-gun_side1_1.1-0 ; 
       add_frames-head.1-0 ; 
       add_frames-head_1.1-0 ; 
       add_frames-holes1.1-0 ; 
       add_frames-holes1_1.1-0 ; 
       add_frames-mat101.1-0 ; 
       add_frames-mat101_1.1-0 ; 
       add_frames-mat105.1-0 ; 
       add_frames-mat105_1.1-0 ; 
       add_frames-mat106.1-0 ; 
       add_frames-mat106_1.1-0 ; 
       add_frames-mat107.1-0 ; 
       add_frames-mat107_1.1-0 ; 
       add_frames-mat108.1-0 ; 
       add_frames-mat108_1.1-0 ; 
       add_frames-mat96.1-0 ; 
       add_frames-mat96_1.1-0 ; 
       add_frames-mat99.1-0 ; 
       add_frames-mat99_1.1-0 ; 
       add_frames-neck.1-0 ; 
       add_frames-neck_1.1-0 ; 
       add_frames-nose_white-center.1-1.1-0 ; 
       add_frames-port_red-left.1-1.1-0 ; 
       add_frames-starbord_green-right.1-1.1-0 ; 
       add_frames-starbord_green-right.1-2.1-0 ; 
       add_frames-starbord_green-right.1-3.1-0 ; 
       add_frames-starbord_green-right.1-4.1-0 ; 
       add_frames-starbord_green-right.1-5.1-0 ; 
       add_frames-starbord_green-right.1-6.1-0 ; 
       add_frames-top_of_head1.1-0 ; 
       add_frames-top_of_head1_1.1-0 ; 
       add_frames-vents1.1-0 ; 
       add_frames-vents1_1.1-0 ; 
       BOTH-mat110.1-0 ; 
       BOTH-mat111.1-0 ; 
       BOTH-mat112.1-0 ; 
       BOTH-mat18.1-0 ; 
       STATIC-mat110.1-0 ; 
       STATIC-mat111.1-0 ; 
       STATIC-mat112.1-0 ; 
       STATIC-mat18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       add_frames-body_1.1-0 ROOT ; 
       add_frames-body_1_1.1-0 ROOT ; 
       add_frames-cone1.1-0 ; 
       add_frames-cone1_1.1-0 ; 
       add_frames-cone1_1_1.1-0 ; 
       add_frames-cone1_2.1-0 ; 
       add_frames-cone1_2_1.1-0 ; 
       add_frames-cone1_5.1-0 ; 
       add_frames-cone5.1-0 ; 
       add_frames-cone5_1.1-0 ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube12_1.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube13_1.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube3_1.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube5_1.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-cube6_1.1-0 ; 
       add_frames-Lwepemt1.1-0 ; 
       add_frames-Lwepemt2.1-0 ; 
       add_frames-L_smoke.1-0 ; 
       add_frames-L_thrust.1-0 ; 
       add_frames-missemt.1-0 ; 
       add_frames-Rwepemt1.1-0 ; 
       add_frames-Rwepemt2.1-0 ; 
       add_frames-R_smoke.1-0 ; 
       add_frames-R_thrust.1-0 ; 
       add_frames-slicer.1-0 ; 
       add_frames-slicer_1.1-0 ; 
       add_frames-SS1.1-0 ; 
       add_frames-SSb1.1-0 ; 
       add_frames-SSla.1-0 ; 
       add_frames-SSra.1-0 ; 
       add_frames-SSrm.1-0 ; 
       add_frames-SSt1.1-0 ; 
       add_frames-SSt2.1-0 ; 
       add_frames-SSt3.1-0 ; 
       add_frames-trail.1-0 ; 
       add_frames-turwepemt1.1-0 ; 
       add_frames-turwepemt2.1-0 ; 
       add_frames-turwepemt3.1-0 ; 
       add_frames-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/new_cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       add_frames-t2d11.1-0 ; 
       add_frames-t2d11_1.1-0 ; 
       add_frames-t2d14.1-0 ; 
       add_frames-t2d14_1.1-0 ; 
       add_frames-t2d15.1-0 ; 
       add_frames-t2d15_1.1-0 ; 
       add_frames-t2d16.1-0 ; 
       add_frames-t2d16_1.1-0 ; 
       add_frames-t2d17.1-0 ; 
       add_frames-t2d17_1.1-0 ; 
       add_frames-t2d2.1-0 ; 
       add_frames-t2d2_1.1-0 ; 
       add_frames-t2d20.1-0 ; 
       add_frames-t2d20_1.1-0 ; 
       add_frames-t2d21.1-0 ; 
       add_frames-t2d21_1.1-0 ; 
       add_frames-t2d22.1-0 ; 
       add_frames-t2d22_1.1-0 ; 
       add_frames-t2d23.1-0 ; 
       add_frames-t2d23_1.1-0 ; 
       add_frames-t2d24.1-0 ; 
       add_frames-t2d24_1.1-0 ; 
       add_frames-t2d25.1-0 ; 
       add_frames-t2d25_1.1-0 ; 
       add_frames-t2d26.1-0 ; 
       add_frames-t2d26_1.1-0 ; 
       add_frames-t2d27.1-0 ; 
       add_frames-t2d27_1.1-0 ; 
       add_frames-t2d28.1-0 ; 
       add_frames-t2d28_1.1-0 ; 
       add_frames-t2d29.1-0 ; 
       add_frames-t2d29_1.1-0 ; 
       add_frames-t2d30.1-0 ; 
       add_frames-t2d30_1.1-0 ; 
       add_frames-t2d31.1-0 ; 
       add_frames-t2d31_1.1-0 ; 
       add_frames-t2d5.1-0 ; 
       add_frames-t2d5_1.1-0 ; 
       add_frames-t2d6.1-0 ; 
       add_frames-t2d6_1.1-0 ; 
       add_frames-t2d9.1-0 ; 
       add_frames-t2d9_1.1-0 ; 
       BOTH-t2d32.1-0 ; 
       BOTH-t2d34.1-0 ; 
       BOTH-t2d35.1-0 ; 
       BOTH-t2d36.1-0 ; 
       STATIC-t2d32.1-0 ; 
       STATIC-t2d34.1-0 ; 
       STATIC-t2d35.1-0 ; 
       STATIC-t2d36.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 1 110 ; 
       9 0 110 ; 
       10 1 110 ; 
       11 0 110 ; 
       12 1 110 ; 
       13 0 110 ; 
       14 1 110 ; 
       15 0 110 ; 
       16 1 110 ; 
       17 0 110 ; 
       18 1 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 0 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       39 1 110 ; 
       40 2 110 ; 
       41 3 110 ; 
       42 8 110 ; 
       43 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 25 300 ; 
       0 19 300 ; 
       0 37 300 ; 
       0 3 300 ; 
       0 21 300 ; 
       0 5 300 ; 
       0 49 300 ; 
       0 47 300 ; 
       0 1 300 ; 
       0 29 300 ; 
       0 31 300 ; 
       1 24 300 ; 
       1 18 300 ; 
       1 36 300 ; 
       1 2 300 ; 
       1 20 300 ; 
       1 4 300 ; 
       1 48 300 ; 
       1 46 300 ; 
       1 0 300 ; 
       1 28 300 ; 
       1 30 300 ; 
       2 53 300 ; 
       3 50 300 ; 
       4 54 300 ; 
       5 51 300 ; 
       6 55 300 ; 
       7 57 300 ; 
       8 52 300 ; 
       9 56 300 ; 
       10 26 300 ; 
       10 8 300 ; 
       11 27 300 ; 
       11 9 300 ; 
       12 16 300 ; 
       12 12 300 ; 
       13 17 300 ; 
       13 13 300 ; 
       14 34 300 ; 
       14 6 300 ; 
       15 35 300 ; 
       15 7 300 ; 
       16 22 300 ; 
       17 23 300 ; 
       18 14 300 ; 
       18 10 300 ; 
       19 15 300 ; 
       19 11 300 ; 
       29 32 300 ; 
       30 33 300 ; 
       31 38 300 ; 
       32 45 300 ; 
       33 39 300 ; 
       34 40 300 ; 
       35 41 300 ; 
       36 42 300 ; 
       37 43 300 ; 
       38 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 42 400 ; 
       3 43 400 ; 
       4 47 400 ; 
       5 44 400 ; 
       6 48 400 ; 
       7 46 400 ; 
       8 45 400 ; 
       9 49 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 22 401 ; 
       1 23 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 38 401 ; 
       7 39 401 ; 
       8 26 401 ; 
       9 27 401 ; 
       10 0 401 ; 
       11 1 401 ; 
       12 34 401 ; 
       13 35 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 32 401 ; 
       17 33 401 ; 
       18 2 401 ; 
       19 3 401 ; 
       20 8 401 ; 
       21 9 401 ; 
       22 40 401 ; 
       23 41 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 10 401 ; 
       33 11 401 ; 
       34 36 401 ; 
       35 37 401 ; 
       36 4 401 ; 
       37 5 401 ; 
       46 20 401 ; 
       47 21 401 ; 
       48 16 401 ; 
       49 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 85.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 77 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 79.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 74.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 MPRFLG 0 ; 
       9 SCHEM 82 -2 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 94.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 45 -2 0 MPRFLG 0 ; 
       13 SCHEM 97 -2 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 84.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 35 -2 0 MPRFLG 0 ; 
       17 SCHEM 87 -2 0 MPRFLG 0 ; 
       18 SCHEM 40 -2 0 MPRFLG 0 ; 
       19 SCHEM 92 -2 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 89.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 22.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 10 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 83.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 91 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 96 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 91 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 96 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 86 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 88.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 83.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 98.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 78.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 81 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 73.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 91 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 88.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 91 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 98.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 96 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 96 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 83.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 83.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 86 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 73.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 76 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 78.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 81 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
