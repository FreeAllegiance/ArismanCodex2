SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       add_frames-body_front1.2-0 ; 
       add_frames-body_side.2-0 ; 
       add_frames-cockpit1.2-0 ; 
       add_frames-engines1.1-0 ; 
       add_frames-engines2.1-0 ; 
       add_frames-gun_middle.1-0 ; 
       add_frames-gun_middle1.1-0 ; 
       add_frames-gun_side.1-0 ; 
       add_frames-gun_side1.1-0 ; 
       add_frames-head.2-0 ; 
       add_frames-holes1.2-0 ; 
       add_frames-mat101.1-0 ; 
       add_frames-mat105.2-0 ; 
       add_frames-mat106.1-0 ; 
       add_frames-mat107.2-0 ; 
       add_frames-mat108.2-0 ; 
       add_frames-mat96.1-0 ; 
       add_frames-mat99.1-0 ; 
       add_frames-neck.2-0 ; 
       add_frames-top_of_head1.2-0 ; 
       add_frames-vents1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       add_frames-body_1_1.3-0 ROOT ; 
       add_frames-cube12.1-0 ; 
       add_frames-cube13.1-0 ; 
       add_frames-cube3.2-0 ; 
       add_frames-cube5.1-0 ; 
       add_frames-cube6.1-0 ; 
       add_frames-slicer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Capital/new_cap20/PICTURES/cap20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       cap20-st.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       add_frames-t2d11.1-0 ; 
       add_frames-t2d14.2-0 ; 
       add_frames-t2d16.2-0 ; 
       add_frames-t2d17.2-0 ; 
       add_frames-t2d2.1-0 ; 
       add_frames-t2d20.1-0 ; 
       add_frames-t2d21.2-0 ; 
       add_frames-t2d22.2-0 ; 
       add_frames-t2d23.2-0 ; 
       add_frames-t2d24.2-0 ; 
       add_frames-t2d25.2-0 ; 
       add_frames-t2d26.1-0 ; 
       add_frames-t2d27.1-0 ; 
       add_frames-t2d28.2-0 ; 
       add_frames-t2d29.2-0 ; 
       add_frames-t2d30.2-0 ; 
       add_frames-t2d31.2-0 ; 
       add_frames-t2d5.2-0 ; 
       add_frames-t2d6.2-0 ; 
       add_frames-t2d9.2-0 ; 
       add_frames-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 9 300 ; 
       0 18 300 ; 
       0 1 300 ; 
       0 10 300 ; 
       0 2 300 ; 
       0 20 300 ; 
       0 19 300 ; 
       0 0 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       1 13 300 ; 
       1 4 300 ; 
       2 8 300 ; 
       2 6 300 ; 
       3 17 300 ; 
       3 3 300 ; 
       4 11 300 ; 
       5 7 300 ; 
       5 5 300 ; 
       6 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 2 401 ; 
       2 6 401 ; 
       3 18 401 ; 
       4 12 401 ; 
       5 0 401 ; 
       6 16 401 ; 
       7 5 401 ; 
       8 15 401 ; 
       9 1 401 ; 
       10 3 401 ; 
       11 19 401 ; 
       12 8 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 4 401 ; 
       17 17 401 ; 
       18 20 401 ; 
       19 9 401 ; 
       20 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 58.38626 -31.21652 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55.89973 -31.21652 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 53.1468 -31.12772 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50.21629 -31.12772 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.81856 -31.12771 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45.24326 -31.57174 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.66792 -31.30532 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39.38219 -31.57174 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.09644 -31.74934 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.81069 -31.66054 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29.61374 -31.74934 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 50.48269 -34.01575 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 36.54047 -34.37097 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.07736 -33.66055 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 48.17378 -34.19337 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 53.32442 -33.83814 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29.34733 -34.01575 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45.24325 -34.37097 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 32.81069 -34.19337 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 58.65268 -33.57174 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 42.40151 -33.92695 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39.47099 -34.28217 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
