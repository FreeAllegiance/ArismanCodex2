SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.9-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 93     
       destroy-default3_1.1-0 ; 
       destroy-default4_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat18_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat20_1.1-0 ; 
       destroy-mat21_1.1-0 ; 
       destroy-mat22_1.1-0 ; 
       destroy-mat23_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       Done_reduce-mat100.1-0 ; 
       Done_reduce-mat101.1-0 ; 
       Done_reduce-mat102.1-0 ; 
       Done_reduce-mat103.1-0 ; 
       Done_reduce-mat104.1-0 ; 
       Done_reduce-mat105.1-0 ; 
       Done_reduce-mat106.1-0 ; 
       Done_reduce-mat107.1-0 ; 
       Done_reduce-mat108.1-0 ; 
       Done_reduce-mat109.1-0 ; 
       Done_reduce-mat110.1-0 ; 
       Done_reduce-mat111.1-0 ; 
       Done_reduce-mat112.1-0 ; 
       Done_reduce-mat113.1-0 ; 
       Done_reduce-mat114.1-0 ; 
       Done_reduce-mat115.1-0 ; 
       Done_reduce-mat116.1-0 ; 
       Done_reduce-mat117.1-0 ; 
       Done_reduce-mat118.1-0 ; 
       Done_reduce-mat119.1-0 ; 
       Done_reduce-mat12_2.1-0 ; 
       Done_reduce-mat120.1-0 ; 
       Done_reduce-mat49.1-0 ; 
       Done_reduce-mat51.1-0 ; 
       Done_reduce-mat52.1-0 ; 
       Done_reduce-mat53.1-0 ; 
       Done_reduce-mat54.1-0 ; 
       Done_reduce-mat55.1-0 ; 
       Done_reduce-mat56.1-0 ; 
       Done_reduce-mat57.1-0 ; 
       Done_reduce-mat58.1-0 ; 
       Done_reduce-mat59.1-0 ; 
       Done_reduce-mat60.1-0 ; 
       Done_reduce-mat61.1-0 ; 
       Done_reduce-mat62.1-0 ; 
       Done_reduce-mat63.1-0 ; 
       Done_reduce-mat64.1-0 ; 
       Done_reduce-mat65.1-0 ; 
       Done_reduce-mat66.1-0 ; 
       Done_reduce-mat67.1-0 ; 
       Done_reduce-mat68.1-0 ; 
       Done_reduce-mat69.1-0 ; 
       Done_reduce-mat70.1-0 ; 
       Done_reduce-mat71.1-0 ; 
       Done_reduce-mat72.1-0 ; 
       Done_reduce-mat73.1-0 ; 
       Done_reduce-mat74.1-0 ; 
       Done_reduce-mat75.1-0 ; 
       Done_reduce-mat85.1-0 ; 
       Done_reduce-mat86.1-0 ; 
       Done_reduce-mat87.1-0 ; 
       Done_reduce-mat88.1-0 ; 
       Done_reduce-mat89.1-0 ; 
       Done_reduce-mat90.1-0 ; 
       Done_reduce-mat91.1-0 ; 
       Done_reduce-mat92.1-0 ; 
       Done_reduce-mat93.1-0 ; 
       Done_reduce-mat94.1-0 ; 
       Done_reduce-mat95.1-0 ; 
       Done_reduce-mat96.1-0 ; 
       Done_reduce-mat97.1-0 ; 
       Done_reduce-mat98.1-0 ; 
       Done_reduce-mat99.1-0 ; 
       il_heavyfighterx_sAT-mat46.1-0 ; 
       il_heavyfighterx_sAT-mat47.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.8-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf3.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLr1.1-0 ; 
       fig01-LLr2.1-0 ; 
       fig01-LLr3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr1.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSr.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-Done_reduce.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 82     
       destroy-2d1_1_1.1-0 ; 
       destroy-t2d1_1_1.1-0 ; 
       destroy-t2d10_1_1.1-0 ; 
       destroy-t2d11_1_1.1-0 ; 
       destroy-t2d12_1_1.1-0 ; 
       destroy-t2d13_1_1.1-0 ; 
       destroy-t2d2_1_1.1-0 ; 
       destroy-t2d20_1_1.1-0 ; 
       destroy-t2d22_1_1.1-0 ; 
       destroy-t2d23_1_1.1-0 ; 
       destroy-t2d24_1_1.1-0 ; 
       destroy-t2d26_1_1.1-0 ; 
       destroy-t2d27_1_1.1-0 ; 
       destroy-t2d28_1_1.1-0 ; 
       destroy-t2d29_1_1.1-0 ; 
       destroy-t2d3_1_1.1-0 ; 
       destroy-t2d30_1_1.1-0 ; 
       destroy-t2d31_1_1.1-0 ; 
       destroy-t2d32_1_1.1-0 ; 
       destroy-t2d33_1_1.1-0 ; 
       destroy-t2d4_1_1.1-0 ; 
       destroy-t2d5_1_1.1-0 ; 
       destroy-t2d6_1_1.1-0 ; 
       destroy-t2d7_1_1.1-0 ; 
       destroy-t2d8_1_1.1-0 ; 
       destroy-t2d9_1_1_1.1-0 ; 
       Done_reduce-t2d100.1-0 ; 
       Done_reduce-t2d101.1-0 ; 
       Done_reduce-t2d102.1-0 ; 
       Done_reduce-t2d103.1-0 ; 
       Done_reduce-t2d104.1-0 ; 
       Done_reduce-t2d105.1-0 ; 
       Done_reduce-t2d13_1_2.1-0 ; 
       Done_reduce-t2d44.1-0 ; 
       Done_reduce-t2d45.1-0 ; 
       Done_reduce-t2d46.1-0 ; 
       Done_reduce-t2d47.1-0 ; 
       Done_reduce-t2d48.1-0 ; 
       Done_reduce-t2d49.1-0 ; 
       Done_reduce-t2d50.1-0 ; 
       Done_reduce-t2d51.1-0 ; 
       Done_reduce-t2d52.1-0 ; 
       Done_reduce-t2d53.1-0 ; 
       Done_reduce-t2d54.1-0 ; 
       Done_reduce-t2d55.1-0 ; 
       Done_reduce-t2d56.1-0 ; 
       Done_reduce-t2d57.1-0 ; 
       Done_reduce-t2d58.1-0 ; 
       Done_reduce-t2d59.1-0 ; 
       Done_reduce-t2d60.1-0 ; 
       Done_reduce-t2d61.1-0 ; 
       Done_reduce-t2d70.1-0 ; 
       Done_reduce-t2d71.1-0 ; 
       Done_reduce-t2d72.1-0 ; 
       Done_reduce-t2d73.1-0 ; 
       Done_reduce-t2d74.1-0 ; 
       Done_reduce-t2d75.1-0 ; 
       Done_reduce-t2d76.1-0 ; 
       Done_reduce-t2d77.1-0 ; 
       Done_reduce-t2d78.1-0 ; 
       Done_reduce-t2d79.1-0 ; 
       Done_reduce-t2d80.1-0 ; 
       Done_reduce-t2d82.1-0 ; 
       Done_reduce-t2d83.1-0 ; 
       Done_reduce-t2d84.1-0 ; 
       Done_reduce-t2d85.1-0 ; 
       Done_reduce-t2d86.1-0 ; 
       Done_reduce-t2d87.1-0 ; 
       Done_reduce-t2d88.1-0 ; 
       Done_reduce-t2d89.1-0 ; 
       Done_reduce-t2d90.1-0 ; 
       Done_reduce-t2d91.1-0 ; 
       Done_reduce-t2d92.1-0 ; 
       Done_reduce-t2d93.1-0 ; 
       Done_reduce-t2d94.1-0 ; 
       Done_reduce-t2d95.1-0 ; 
       Done_reduce-t2d96.1-0 ; 
       Done_reduce-t2d97.1-0 ; 
       Done_reduce-t2d98.1-0 ; 
       Done_reduce-t2d99.1-0 ; 
       il_heavyfighterx_sAT-t2d42.1-0 ; 
       il_heavyfighterx_sAT-t2d43.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       0 46 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 21 110 ; 
       8 2 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 22 110 ; 
       20 40 110 ; 
       21 45 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 29 110 ; 
       25 2 110 ; 
       26 4 110 ; 
       27 30 110 ; 
       28 40 110 ; 
       29 45 110 ; 
       30 29 110 ; 
       31 4 110 ; 
       32 21 110 ; 
       33 8 110 ; 
       34 29 110 ; 
       35 25 110 ; 
       36 41 110 ; 
       37 42 110 ; 
       38 43 110 ; 
       39 44 110 ; 
       40 1 110 ; 
       41 9 110 ; 
       42 9 110 ; 
       43 9 110 ; 
       44 9 110 ; 
       45 1 110 ; 
       46 4 110 ; 
       47 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 86 300 ; 
       3 87 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       7 56 300 ; 
       7 57 300 ; 
       7 58 300 ; 
       7 59 300 ; 
       8 80 300 ; 
       8 81 300 ; 
       8 82 300 ; 
       8 49 300 ; 
       9 89 300 ; 
       10 23 300 ; 
       11 5 300 ; 
       12 6 300 ; 
       13 0 300 ; 
       14 7 300 ; 
       15 8 300 ; 
       16 1 300 ; 
       17 9 300 ; 
       18 10 300 ; 
       20 2 300 ; 
       20 25 300 ; 
       20 91 300 ; 
       20 77 300 ; 
       21 68 300 ; 
       21 69 300 ; 
       21 70 300 ; 
       21 71 300 ; 
       22 52 300 ; 
       22 53 300 ; 
       22 54 300 ; 
       22 55 300 ; 
       24 64 300 ; 
       24 65 300 ; 
       24 66 300 ; 
       24 67 300 ; 
       25 83 300 ; 
       25 84 300 ; 
       25 85 300 ; 
       25 47 300 ; 
       26 4 300 ; 
       26 19 300 ; 
       26 20 300 ; 
       26 21 300 ; 
       26 22 300 ; 
       26 88 300 ; 
       28 3 300 ; 
       28 24 300 ; 
       28 92 300 ; 
       28 76 300 ; 
       29 72 300 ; 
       29 73 300 ; 
       29 74 300 ; 
       29 75 300 ; 
       30 60 300 ; 
       30 61 300 ; 
       30 62 300 ; 
       30 63 300 ; 
       31 27 300 ; 
       32 51 300 ; 
       33 26 300 ; 
       34 50 300 ; 
       35 26 300 ; 
       36 37 300 ; 
       36 38 300 ; 
       36 39 300 ; 
       37 43 300 ; 
       37 44 300 ; 
       38 45 300 ; 
       38 46 300 ; 
       39 40 300 ; 
       39 41 300 ; 
       39 42 300 ; 
       41 30 300 ; 
       41 31 300 ; 
       41 32 300 ; 
       42 35 300 ; 
       42 36 300 ; 
       43 33 300 ; 
       43 34 300 ; 
       44 90 300 ; 
       44 28 300 ; 
       44 29 300 ; 
       45 48 300 ; 
       46 78 300 ; 
       46 79 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 1 400 ; 
       8 5 400 ; 
       10 7 400 ; 
       11 8 400 ; 
       12 62 400 ; 
       13 12 400 ; 
       14 9 400 ; 
       15 10 400 ; 
       16 11 400 ; 
       17 13 400 ; 
       18 14 400 ; 
       20 16 400 ; 
       25 32 400 ; 
       26 25 400 ; 
       28 17 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       12 6 401 ; 
       13 15 401 ; 
       14 20 401 ; 
       15 21 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 0 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       91 80 401 ; 
       92 81 401 ; 
       28 65 401 ; 
       29 66 401 ; 
       30 67 401 ; 
       31 68 401 ; 
       32 69 401 ; 
       33 70 401 ; 
       34 71 401 ; 
       35 72 401 ; 
       36 73 401 ; 
       37 74 401 ; 
       38 75 401 ; 
       39 76 401 ; 
       40 77 401 ; 
       41 78 401 ; 
       42 79 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       49 31 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 59 401 ; 
       61 39 401 ; 
       62 40 401 ; 
       63 41 401 ; 
       65 42 401 ; 
       66 43 401 ; 
       67 44 401 ; 
       69 45 401 ; 
       70 46 401 ; 
       71 47 401 ; 
       73 48 401 ; 
       74 49 401 ; 
       75 50 401 ; 
       76 51 401 ; 
       77 52 401 ; 
       78 53 401 ; 
       79 54 401 ; 
       81 55 401 ; 
       82 56 401 ; 
       84 57 401 ; 
       85 58 401 ; 
       87 60 401 ; 
       88 61 401 ; 
       89 63 401 ; 
       90 64 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 142.5 -4 0 SRT 1 1 1 6.370452e-005 -1.569898 0.002393199 0 0 0 MPRFLG 0 ; 
       2 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 233.75 -6 0 MPRFLG 0 ; 
       0 SCHEM 187.5 -10 0 USR MPRFLG 0 ; 
       5 SCHEM 158.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 60 -12 0 MPRFLG 0 ; 
       7 SCHEM 65 -10 0 MPRFLG 0 ; 
       8 SCHEM 36.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 237.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       11 SCHEM 138.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 143.75 -10 0 MPRFLG 0 ; 
       13 SCHEM 158.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 153.75 -10 0 MPRFLG 0 ; 
       15 SCHEM 158.75 -10 0 MPRFLG 0 ; 
       16 SCHEM 173.75 -8 0 MPRFLG 0 ; 
       17 SCHEM 168.75 -10 0 MPRFLG 0 ; 
       18 SCHEM 173.75 -10 0 MPRFLG 0 ; 
       19 SCHEM 75 -12 0 MPRFLG 0 ; 
       20 SCHEM 5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 80 -10 0 MPRFLG 0 ; 
       23 SCHEM 100 -12 0 MPRFLG 0 ; 
       24 SCHEM 105 -10 0 MPRFLG 0 ; 
       25 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       26 SCHEM 200 -8 0 MPRFLG 0 ; 
       27 SCHEM 112.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 115 -8 0 MPRFLG 0 ; 
       30 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 210 -8 0 MPRFLG 0 ; 
       32 SCHEM 72.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 30 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 97.5 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 45 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 215 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 228.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 238.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 250 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       41 SCHEM 218.75 -10 0 MPRFLG 0 ; 
       42 SCHEM 231.25 -10 0 MPRFLG 0 ; 
       43 SCHEM 241.25 -10 0 MPRFLG 0 ; 
       44 SCHEM 253.75 -10 0 MPRFLG 0 ; 
       45 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 186.25 -8 0 MPRFLG 0 ; 
       47 SCHEM 182.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 207.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 152.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 277.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 280 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 282.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 285 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 275 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 265 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 267.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 270 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 205 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 202.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 200 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 197.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 147.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 210 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 257.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 260 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 220 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 222.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 225 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 242.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 245 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 232.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 235 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 212.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 217.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 247.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 250 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 252.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 227.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 230 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 237.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 240 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 190 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 192.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 195 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 262.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 255 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 197.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 272.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 205 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 202.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 200 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 280 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 170 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 175 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 282.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 285 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 275 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 265 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 267.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 270 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       80 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       81 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 227.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 230 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 237.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 240 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 115 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 107.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 190 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 192.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 195 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 262.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 255 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 257.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 260 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 220 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 222.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 225 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 242.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 245 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       72 SCHEM 232.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       73 SCHEM 235 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       74 SCHEM 212.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       75 SCHEM 215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       76 SCHEM 217.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       77 SCHEM 247.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       78 SCHEM 250 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       79 SCHEM 252.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 286.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
