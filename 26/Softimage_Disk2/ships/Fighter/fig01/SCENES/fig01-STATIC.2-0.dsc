SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.69-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.69-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       scaled-light1.3-0 ROOT ; 
       scaled-light2.3-0 ROOT ; 
       scaled-light3.3-0 ROOT ; 
       scaled-light4.3-0 ROOT ; 
       scaled-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat9.2-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat107.1-0 ; 
       scaled-mat116.1-0 ; 
       scaled-mat117.1-0 ; 
       scaled-mat118.1-0 ; 
       scaled-mat119.1-0 ; 
       scaled-mat12_2.1-0 ; 
       scaled-mat120.1-0 ; 
       scaled-mat121.1-0 ; 
       scaled-mat122.1-0 ; 
       scaled-mat123.1-0 ; 
       scaled-mat124.1-0 ; 
       scaled-mat125.1-0 ; 
       scaled-mat51.1-0 ; 
       scaled-mat52.1-0 ; 
       scaled-mat56.1-0 ; 
       scaled-mat87.1-0 ; 
       scaled-mat89.1-0 ; 
       scaled-mat90.1-0 ; 
       scaled-mat95.1-0 ; 
       scaled-mat96.1-0 ; 
       scaled-mat97.1-0 ; 
       scaled-mat98.1-0 ; 
       scaled-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.59-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lengine.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rengine.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       destroy-2d1_1_1.9-0 ; 
       destroy-t2d10_1_1.9-0 ; 
       destroy-t2d11_1_1.9-0 ; 
       destroy-t2d12_1_1.9-0 ; 
       destroy-t2d2_1_1.11-0 ; 
       destroy-t2d3_1_1.11-0 ; 
       destroy-t2d4_1_1.11-0 ; 
       destroy-t2d5_1_1.11-0 ; 
       destroy-t2d6_1_1.11-0 ; 
       destroy-t2d7_1_1.11-0 ; 
       destroy-t2d8_1_1.11-0 ; 
       scaled-t2d106.2-0 ; 
       scaled-t2d107.2-0 ; 
       scaled-t2d108.2-0 ; 
       scaled-t2d109.2-0 ; 
       scaled-t2d110.2-0 ; 
       scaled-t2d111.2-0 ; 
       scaled-t2d112.2-0 ; 
       scaled-t2d113.2-0 ; 
       scaled-t2d114.2-0 ; 
       scaled-t2d115.2-0 ; 
       scaled-t2d116.2-0 ; 
       scaled-t2d117.2-0 ; 
       scaled-t2d118.2-0 ; 
       scaled-t2d72.2-0 ; 
       scaled-t2d74.2-0 ; 
       scaled-t2d79.2-0 ; 
       scaled-t2d80.2-0 ; 
       scaled-t2d83.2-0 ; 
       scaled-t2d87.2-0 ; 
       scaled-t2d90.2-0 ; 
       scaled-t2d92.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 32 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 11 110 ; 
       7 26 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 12 110 ; 
       11 31 110 ; 
       12 11 110 ; 
       13 14 110 ; 
       14 19 110 ; 
       15 26 110 ; 
       16 3 110 ; 
       17 4 110 ; 
       18 20 110 ; 
       19 31 110 ; 
       20 19 110 ; 
       21 4 110 ; 
       22 11 110 ; 
       23 8 110 ; 
       24 19 110 ; 
       25 16 110 ; 
       26 1 110 ; 
       27 9 110 ; 
       28 9 110 ; 
       29 9 110 ; 
       30 9 110 ; 
       31 1 110 ; 
       32 4 110 ; 
       33 32 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 35 300 ; 
       3 36 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       6 31 300 ; 
       7 20 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       9 38 300 ; 
       11 21 300 ; 
       12 30 300 ; 
       14 28 300 ; 
       15 24 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       17 0 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       17 12 300 ; 
       17 37 300 ; 
       19 25 300 ; 
       20 26 300 ; 
       21 14 300 ; 
       22 29 300 ; 
       23 13 300 ; 
       24 27 300 ; 
       25 13 300 ; 
       27 15 300 ; 
       28 17 300 ; 
       29 16 300 ; 
       30 39 300 ; 
       31 22 300 ; 
       32 32 300 ; 
       33 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 0 401 ; 
       15 29 401 ; 
       16 30 401 ; 
       17 31 401 ; 
       19 13 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       28 23 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       32 24 401 ; 
       34 25 401 ; 
       35 12 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 28 401 ; 
       39 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 50 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -10 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -10 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0.7736091 MPRFLG 0 ; 
       2 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       17 SCHEM 35 -4 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 25 -4 0 MPRFLG 0 ; 
       20 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 MPRFLG 0 ; 
       25 SCHEM 10 -8 0 MPRFLG 0 ; 
       26 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       27 SCHEM 40 -6 0 MPRFLG 0 ; 
       28 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 45 -6 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       32 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 30 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 33 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
