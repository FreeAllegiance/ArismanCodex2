SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.38-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.38-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.38-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       check-light1.1-0 ROOT ; 
       check-light2.1-0 ROOT ; 
       check-light3.1-0 ROOT ; 
       check-light4.1-0 ROOT ; 
       check-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       check-mat102.1-0 ; 
       check-mat105.1-0 ; 
       check-mat107.1-0 ; 
       check-mat109.1-0 ; 
       check-mat115.1-0 ; 
       check-mat116.1-0 ; 
       check-mat117.1-0 ; 
       check-mat118.1-0 ; 
       check-mat119.1-0 ; 
       check-mat12_2.1-0 ; 
       check-mat120.1-0 ; 
       check-mat121.1-0 ; 
       check-mat122.1-0 ; 
       check-mat123.1-0 ; 
       check-mat124.1-0 ; 
       check-mat125.1-0 ; 
       check-mat19_2.1-0 ; 
       check-mat19_3.1-0 ; 
       check-mat19_4.1-0 ; 
       check-mat19_5.1-0 ; 
       check-mat19_6.1-0 ; 
       check-mat37_2.1-0 ; 
       check-mat37_3.1-0 ; 
       check-mat51.1-0 ; 
       check-mat52.1-0 ; 
       check-mat56.1-0 ; 
       check-mat87.1-0 ; 
       check-mat89.1-0 ; 
       check-mat90.1-0 ; 
       check-mat95.1-0 ; 
       check-mat96.1-0 ; 
       check-mat97.1-0 ; 
       check-mat98.1-0 ; 
       check-mat99.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       fig01-cockpt.1-0 ; 
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.36-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf4.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLR1.1-0 ; 
       fig01-LLR2.1-0 ; 
       fig01-LLR3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
       fig01-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-check.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       check-t2d106.1-0 ; 
       check-t2d107.1-0 ; 
       check-t2d108.1-0 ; 
       check-t2d109.1-0 ; 
       check-t2d110.1-0 ; 
       check-t2d111.1-0 ; 
       check-t2d112.1-0 ; 
       check-t2d113.1-0 ; 
       check-t2d114.1-0 ; 
       check-t2d115.1-0 ; 
       check-t2d116.1-0 ; 
       check-t2d117.1-0 ; 
       check-t2d118.1-0 ; 
       check-t2d119.1-0 ; 
       check-t2d120.1-0 ; 
       check-t2d121.1-0 ; 
       check-t2d122.1-0 ; 
       check-t2d123.1-0 ; 
       check-t2d124.1-0 ; 
       check-t2d125.1-0 ; 
       check-t2d126.1-0 ; 
       check-t2d127.1-0 ; 
       check-t2d72.1-0 ; 
       check-t2d74.1-0 ; 
       check-t2d79.1-0 ; 
       check-t2d80.1-0 ; 
       check-t2d83.1-0 ; 
       check-t2d87.1-0 ; 
       check-t2d90.1-0 ; 
       check-t2d92.1-0 ; 
       destroy-2d1_1_1.6-0 ; 
       destroy-t2d10_1_1.6-0 ; 
       destroy-t2d11_1_1.6-0 ; 
       destroy-t2d12_1_1.6-0 ; 
       destroy-t2d2_1_1.8-0 ; 
       destroy-t2d3_1_1.8-0 ; 
       destroy-t2d4_1_1.8-0 ; 
       destroy-t2d5_1_1.8-0 ; 
       destroy-t2d6_1_1.8-0 ; 
       destroy-t2d7_1_1.8-0 ; 
       destroy-t2d8_1_1.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       50 2 110 ; 
       41 2 110 ; 
       0 2 110 ; 
       1 48 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 8 110 ; 
       8 22 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 6 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 6 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 23 110 ; 
       21 42 110 ; 
       22 47 110 ; 
       23 22 110 ; 
       24 25 110 ; 
       25 30 110 ; 
       26 4 110 ; 
       27 5 110 ; 
       28 31 110 ; 
       29 42 110 ; 
       30 47 110 ; 
       31 30 110 ; 
       32 5 110 ; 
       33 22 110 ; 
       34 9 110 ; 
       35 30 110 ; 
       36 26 110 ; 
       37 43 110 ; 
       38 44 110 ; 
       39 45 110 ; 
       40 46 110 ; 
       42 2 110 ; 
       43 10 110 ; 
       44 10 110 ; 
       45 10 110 ; 
       46 10 110 ; 
       47 2 110 ; 
       48 5 110 ; 
       49 48 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 29 300 ; 
       4 30 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       5 38 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       5 43 300 ; 
       8 25 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       10 32 300 ; 
       11 48 300 ; 
       12 35 300 ; 
       13 16 300 ; 
       14 22 300 ; 
       15 19 300 ; 
       16 20 300 ; 
       17 21 300 ; 
       18 17 300 ; 
       19 18 300 ; 
       21 7 300 ; 
       22 8 300 ; 
       23 24 300 ; 
       25 15 300 ; 
       26 5 300 ; 
       26 6 300 ; 
       27 34 300 ; 
       27 44 300 ; 
       27 45 300 ; 
       27 46 300 ; 
       27 47 300 ; 
       27 31 300 ; 
       29 11 300 ; 
       30 12 300 ; 
       31 13 300 ; 
       32 50 300 ; 
       33 23 300 ; 
       34 49 300 ; 
       35 14 300 ; 
       36 49 300 ; 
       37 3 300 ; 
       38 4 300 ; 
       39 4 300 ; 
       40 3 300 ; 
       43 0 300 ; 
       44 2 300 ; 
       45 1 300 ; 
       46 33 300 ; 
       47 9 300 ; 
       48 26 300 ; 
       49 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       35 14 401 ; 
       36 0 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 36 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       42 39 401 ; 
       43 40 401 ; 
       44 31 401 ; 
       45 32 401 ; 
       46 33 401 ; 
       47 30 401 ; 
       48 13 401 ; 
       0 27 401 ; 
       1 28 401 ; 
       2 29 401 ; 
       6 2 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       15 12 401 ; 
       16 15 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 20 401 ; 
       20 21 401 ; 
       21 16 401 ; 
       22 19 401 ; 
       24 6 401 ; 
       25 7 401 ; 
       26 22 401 ; 
       28 23 401 ; 
       29 1 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       50 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 35 -6 0 MPRFLG 0 ; 
       19 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 10 -6 0 MPRFLG 0 ; 
       27 SCHEM 50 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 25 -4 0 MPRFLG 0 ; 
       31 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 25 -6 0 MPRFLG 0 ; 
       36 SCHEM 10 -8 0 MPRFLG 0 ; 
       37 SCHEM 55 -8 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 60 -8 0 MPRFLG 0 ; 
       40 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       43 SCHEM 55 -6 0 MPRFLG 0 ; 
       44 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       45 SCHEM 60 -6 0 MPRFLG 0 ; 
       46 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       47 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       48 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       49 SCHEM 45 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       30 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 48 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
