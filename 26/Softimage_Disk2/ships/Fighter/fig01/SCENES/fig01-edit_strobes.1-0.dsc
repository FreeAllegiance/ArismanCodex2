SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.55-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.63-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.63-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       edit_strobes-light1.1-0 ROOT ; 
       edit_strobes-light2.1-0 ROOT ; 
       edit_strobes-light3.1-0 ROOT ; 
       edit_strobes-light4.1-0 ROOT ; 
       edit_strobes-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-mat16_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat9.2-0 ; 
       edit_strobes-mat102.1-0 ; 
       edit_strobes-mat105.1-0 ; 
       edit_strobes-mat107.1-0 ; 
       edit_strobes-mat109.1-0 ; 
       edit_strobes-mat115.1-0 ; 
       edit_strobes-mat116.1-0 ; 
       edit_strobes-mat117.1-0 ; 
       edit_strobes-mat118.1-0 ; 
       edit_strobes-mat119.1-0 ; 
       edit_strobes-mat12_2.1-0 ; 
       edit_strobes-mat120.1-0 ; 
       edit_strobes-mat121.1-0 ; 
       edit_strobes-mat122.1-0 ; 
       edit_strobes-mat123.1-0 ; 
       edit_strobes-mat124.1-0 ; 
       edit_strobes-mat125.1-0 ; 
       edit_strobes-mat19_2.1-0 ; 
       edit_strobes-mat19_3.1-0 ; 
       edit_strobes-mat19_4.1-0 ; 
       edit_strobes-mat19_5.1-0 ; 
       edit_strobes-mat19_6.1-0 ; 
       edit_strobes-mat37_2.1-0 ; 
       edit_strobes-mat37_3.1-0 ; 
       edit_strobes-mat51.1-0 ; 
       edit_strobes-mat52.1-0 ; 
       edit_strobes-mat56.1-0 ; 
       edit_strobes-mat87.1-0 ; 
       edit_strobes-mat89.1-0 ; 
       edit_strobes-mat90.1-0 ; 
       edit_strobes-mat95.1-0 ; 
       edit_strobes-mat96.1-0 ; 
       edit_strobes-mat97.1-0 ; 
       edit_strobes-mat98.1-0 ; 
       edit_strobes-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       fig01-cockpt.1-0 ; 
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.55-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lengine.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf4.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLR1.1-0 ; 
       fig01-LLR2.1-0 ; 
       fig01-LLR3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rengine.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-smoke.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
       fig01-trail.1-0 ; 
       fig01-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-edit_strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1_1.7-0 ; 
       destroy-t2d10_1_1.7-0 ; 
       destroy-t2d11_1_1.7-0 ; 
       destroy-t2d12_1_1.7-0 ; 
       destroy-t2d2_1_1.9-0 ; 
       destroy-t2d3_1_1.9-0 ; 
       destroy-t2d4_1_1.9-0 ; 
       destroy-t2d5_1_1.9-0 ; 
       destroy-t2d6_1_1.9-0 ; 
       destroy-t2d7_1_1.9-0 ; 
       destroy-t2d8_1_1.9-0 ; 
       edit_strobes-t2d106.1-0 ; 
       edit_strobes-t2d107.1-0 ; 
       edit_strobes-t2d108.1-0 ; 
       edit_strobes-t2d109.1-0 ; 
       edit_strobes-t2d110.1-0 ; 
       edit_strobes-t2d111.1-0 ; 
       edit_strobes-t2d112.1-0 ; 
       edit_strobes-t2d113.1-0 ; 
       edit_strobes-t2d114.1-0 ; 
       edit_strobes-t2d115.1-0 ; 
       edit_strobes-t2d116.1-0 ; 
       edit_strobes-t2d117.1-0 ; 
       edit_strobes-t2d118.1-0 ; 
       edit_strobes-t2d119.1-0 ; 
       edit_strobes-t2d120.1-0 ; 
       edit_strobes-t2d121.1-0 ; 
       edit_strobes-t2d122.1-0 ; 
       edit_strobes-t2d123.1-0 ; 
       edit_strobes-t2d124.1-0 ; 
       edit_strobes-t2d125.1-0 ; 
       edit_strobes-t2d126.1-0 ; 
       edit_strobes-t2d127.1-0 ; 
       edit_strobes-t2d72.1-0 ; 
       edit_strobes-t2d74.1-0 ; 
       edit_strobes-t2d79.1-0 ; 
       edit_strobes-t2d80.1-0 ; 
       edit_strobes-t2d83.1-0 ; 
       edit_strobes-t2d87.1-0 ; 
       edit_strobes-t2d90.1-0 ; 
       edit_strobes-t2d92.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 50 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 8 110 ; 
       8 23 110 ; 
       9 44 110 ; 
       10 4 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 6 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 6 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 24 110 ; 
       22 9 110 ; 
       23 49 110 ; 
       24 23 110 ; 
       25 26 110 ; 
       26 32 110 ; 
       27 44 110 ; 
       28 4 110 ; 
       29 5 110 ; 
       30 33 110 ; 
       31 27 110 ; 
       32 49 110 ; 
       33 32 110 ; 
       34 29 110 ; 
       35 5 110 ; 
       36 23 110 ; 
       37 10 110 ; 
       38 32 110 ; 
       39 28 110 ; 
       40 45 110 ; 
       41 46 110 ; 
       42 47 110 ; 
       43 48 110 ; 
       44 2 110 ; 
       45 11 110 ; 
       46 11 110 ; 
       47 11 110 ; 
       48 11 110 ; 
       49 2 110 ; 
       50 5 110 ; 
       51 50 110 ; 
       52 2 110 ; 
       53 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 46 300 ; 
       4 47 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       8 42 300 ; 
       9 24 300 ; 
       10 44 300 ; 
       10 45 300 ; 
       11 49 300 ; 
       12 14 300 ; 
       13 1 300 ; 
       14 33 300 ; 
       15 39 300 ; 
       16 36 300 ; 
       17 37 300 ; 
       18 38 300 ; 
       19 34 300 ; 
       20 35 300 ; 
       23 25 300 ; 
       24 41 300 ; 
       26 32 300 ; 
       27 28 300 ; 
       28 22 300 ; 
       28 23 300 ; 
       29 0 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       29 48 300 ; 
       32 29 300 ; 
       33 30 300 ; 
       35 16 300 ; 
       36 40 300 ; 
       37 15 300 ; 
       38 31 300 ; 
       39 15 300 ; 
       40 20 300 ; 
       41 21 300 ; 
       42 21 300 ; 
       43 20 300 ; 
       45 17 300 ; 
       46 19 300 ; 
       47 18 300 ; 
       48 50 300 ; 
       49 26 300 ; 
       50 43 300 ; 
       51 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 11 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 0 401 ; 
       14 24 401 ; 
       17 38 401 ; 
       18 39 401 ; 
       19 40 401 ; 
       23 13 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       32 23 401 ; 
       33 26 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 27 401 ; 
       39 30 401 ; 
       41 17 401 ; 
       42 18 401 ; 
       43 33 401 ; 
       45 34 401 ; 
       46 12 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 157.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 80 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 118.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -10 0 MPRFLG 0 ; 
       8 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 122.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 MPRFLG 0 ; 
       13 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 MPRFLG 0 ; 
       15 SCHEM 80 -6 0 MPRFLG 0 ; 
       16 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 80 -8 0 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       24 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 45 -10 0 MPRFLG 0 ; 
       26 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       27 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 100 -6 0 MPRFLG 0 ; 
       30 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       33 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       34 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 110 -6 0 MPRFLG 0 ; 
       36 SCHEM 35 -8 0 MPRFLG 0 ; 
       37 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 50 -8 0 MPRFLG 0 ; 
       39 SCHEM 10 -10 0 MPRFLG 0 ; 
       40 SCHEM 112.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 117.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 122.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 127.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       45 SCHEM 113.75 -8 0 MPRFLG 0 ; 
       46 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       47 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       48 SCHEM 128.75 -8 0 MPRFLG 0 ; 
       49 SCHEM 45 -4 0 MPRFLG 0 ; 
       50 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 85 -8 0 MPRFLG 0 ; 
       52 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 160 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 119.1642 -14.34448 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 121.7644 -14.3814 0 USR WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 99 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 135 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 140 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 142.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 152.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 161.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
