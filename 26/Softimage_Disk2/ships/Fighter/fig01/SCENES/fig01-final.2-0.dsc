SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.45-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       final-light1.2-0 ROOT ; 
       final-light2.2-0 ROOT ; 
       final-light3.2-0 ROOT ; 
       final-light4.2-0 ROOT ; 
       final-light5.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       destroy-mat16_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       final-mat102.1-0 ; 
       final-mat105.1-0 ; 
       final-mat107.1-0 ; 
       final-mat109.1-0 ; 
       final-mat115.1-0 ; 
       final-mat116.1-0 ; 
       final-mat117.1-0 ; 
       final-mat118.1-0 ; 
       final-mat119.1-0 ; 
       final-mat12_2.1-0 ; 
       final-mat120.1-0 ; 
       final-mat121.1-0 ; 
       final-mat122.1-0 ; 
       final-mat123.1-0 ; 
       final-mat124.1-0 ; 
       final-mat125.1-0 ; 
       final-mat51.1-0 ; 
       final-mat52.1-0 ; 
       final-mat56.1-0 ; 
       final-mat87.1-0 ; 
       final-mat89.1-0 ; 
       final-mat90.1-0 ; 
       final-mat95.1-0 ; 
       final-mat96.1-0 ; 
       final-mat97.1-0 ; 
       final-mat98.1-0 ; 
       final-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       fig01-cockpt.1-0 ; 
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.43-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
       fig01-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-final.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       destroy-2d1_1_1.6-0 ; 
       destroy-t2d10_1_1.6-0 ; 
       destroy-t2d11_1_1.6-0 ; 
       destroy-t2d12_1_1.6-0 ; 
       destroy-t2d2_1_1.8-0 ; 
       destroy-t2d3_1_1.8-0 ; 
       destroy-t2d4_1_1.8-0 ; 
       destroy-t2d5_1_1.8-0 ; 
       destroy-t2d6_1_1.8-0 ; 
       destroy-t2d7_1_1.8-0 ; 
       destroy-t2d8_1_1.8-0 ; 
       final-t2d106.1-0 ; 
       final-t2d107.1-0 ; 
       final-t2d108.1-0 ; 
       final-t2d109.1-0 ; 
       final-t2d110.1-0 ; 
       final-t2d111.1-0 ; 
       final-t2d112.1-0 ; 
       final-t2d113.1-0 ; 
       final-t2d114.1-0 ; 
       final-t2d115.1-0 ; 
       final-t2d116.1-0 ; 
       final-t2d117.1-0 ; 
       final-t2d118.1-0 ; 
       final-t2d72.1-0 ; 
       final-t2d74.1-0 ; 
       final-t2d79.1-0 ; 
       final-t2d80.1-0 ; 
       final-t2d83.1-0 ; 
       final-t2d87.1-0 ; 
       final-t2d90.1-0 ; 
       final-t2d92.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       40 2 110 ; 
       31 2 110 ; 
       0 2 110 ; 
       1 38 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 7 110 ; 
       7 12 110 ; 
       8 4 110 ; 
       9 5 110 ; 
       10 13 110 ; 
       11 32 110 ; 
       12 37 110 ; 
       13 12 110 ; 
       14 15 110 ; 
       15 20 110 ; 
       16 4 110 ; 
       17 5 110 ; 
       18 21 110 ; 
       19 32 110 ; 
       20 37 110 ; 
       21 20 110 ; 
       22 5 110 ; 
       23 12 110 ; 
       24 8 110 ; 
       25 20 110 ; 
       26 16 110 ; 
       27 33 110 ; 
       28 34 110 ; 
       29 35 110 ; 
       30 36 110 ; 
       32 2 110 ; 
       33 9 110 ; 
       34 9 110 ; 
       35 9 110 ; 
       36 9 110 ; 
       37 2 110 ; 
       38 5 110 ; 
       39 38 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 37 300 ; 
       4 38 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       7 33 300 ; 
       8 35 300 ; 
       8 36 300 ; 
       9 40 300 ; 
       11 22 300 ; 
       12 23 300 ; 
       13 32 300 ; 
       15 30 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       17 0 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       17 12 300 ; 
       17 39 300 ; 
       19 26 300 ; 
       20 27 300 ; 
       21 28 300 ; 
       22 14 300 ; 
       23 31 300 ; 
       24 13 300 ; 
       25 29 300 ; 
       26 13 300 ; 
       27 18 300 ; 
       28 19 300 ; 
       29 19 300 ; 
       30 18 300 ; 
       33 15 300 ; 
       34 17 300 ; 
       35 16 300 ; 
       36 41 300 ; 
       37 24 300 ; 
       38 34 300 ; 
       39 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 0 401 ; 
       15 29 401 ; 
       16 30 401 ; 
       17 31 401 ; 
       21 13 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       30 23 401 ; 
       32 17 401 ; 
       33 18 401 ; 
       34 24 401 ; 
       36 25 401 ; 
       37 12 401 ; 
       38 26 401 ; 
       39 27 401 ; 
       40 28 401 ; 
       41 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       17 SCHEM 50 -4 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 25 -4 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 MPRFLG 0 ; 
       27 SCHEM 55 -8 0 MPRFLG 0 ; 
       28 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 60 -8 0 MPRFLG 0 ; 
       30 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 MPRFLG 0 ; 
       34 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 60 -6 0 MPRFLG 0 ; 
       36 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       38 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       39 SCHEM 45 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 56.29118 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 53.79118 -14.40205 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 48 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 56.29118 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
