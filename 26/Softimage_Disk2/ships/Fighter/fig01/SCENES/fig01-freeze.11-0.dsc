SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.30-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       freeze-light1.11-0 ROOT ; 
       freeze-light2.11-0 ROOT ; 
       freeze-light3.11-0 ROOT ; 
       freeze-light4.11-0 ROOT ; 
       freeze-light5.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-default3_1.1-0 ; 
       destroy-default4_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat18_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat20_1.1-0 ; 
       destroy-mat21_1.1-0 ; 
       destroy-mat22_1.1-0 ; 
       destroy-mat23_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       freeze-mat102.2-0 ; 
       freeze-mat105.2-0 ; 
       freeze-mat107.2-0 ; 
       freeze-mat109.1-0 ; 
       freeze-mat115.1-0 ; 
       freeze-mat116.1-0 ; 
       freeze-mat117.1-0 ; 
       freeze-mat118.1-0 ; 
       freeze-mat119.2-0 ; 
       freeze-mat12_2.1-0 ; 
       freeze-mat120.2-0 ; 
       freeze-mat121.1-0 ; 
       freeze-mat122.1-0 ; 
       freeze-mat123.1-0 ; 
       freeze-mat124.1-0 ; 
       freeze-mat125.1-0 ; 
       freeze-mat51.1-0 ; 
       freeze-mat52.2-0 ; 
       freeze-mat56.2-0 ; 
       freeze-mat87.2-0 ; 
       freeze-mat89.2-0 ; 
       freeze-mat90.2-0 ; 
       freeze-mat95.2-0 ; 
       freeze-mat96.2-0 ; 
       freeze-mat97.1-0 ; 
       freeze-mat98.1-0 ; 
       freeze-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.28-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf3.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLr1.1-0 ; 
       fig01-LLr2.1-0 ; 
       fig01-LLr3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-freeze.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1_1.6-0 ; 
       destroy-t2d10_1_1.6-0 ; 
       destroy-t2d11_1_1.6-0 ; 
       destroy-t2d12_1_1.6-0 ; 
       destroy-t2d2_1_1.7-0 ; 
       destroy-t2d20_1_1.3-0 ; 
       destroy-t2d22_1_1.3-0 ; 
       destroy-t2d23_1_1.3-0 ; 
       destroy-t2d24_1_1.3-0 ; 
       destroy-t2d26_1_1.3-0 ; 
       destroy-t2d27_1_1.3-0 ; 
       destroy-t2d28_1_1.3-0 ; 
       destroy-t2d29_1_1.3-0 ; 
       destroy-t2d3_1_1.7-0 ; 
       destroy-t2d4_1_1.7-0 ; 
       destroy-t2d5_1_1.7-0 ; 
       destroy-t2d6_1_1.7-0 ; 
       destroy-t2d7_1_1.7-0 ; 
       destroy-t2d8_1_1.7-0 ; 
       freeze-t2d106.2-0 ; 
       freeze-t2d107.1-0 ; 
       freeze-t2d108.1-0 ; 
       freeze-t2d109.1-0 ; 
       freeze-t2d110.1-0 ; 
       freeze-t2d111.1-0 ; 
       freeze-t2d112.1-0 ; 
       freeze-t2d113.1-0 ; 
       freeze-t2d114.1-0 ; 
       freeze-t2d115.1-0 ; 
       freeze-t2d116.1-0 ; 
       freeze-t2d117.1-0 ; 
       freeze-t2d118.1-0 ; 
       freeze-t2d72.2-0 ; 
       freeze-t2d74.2-0 ; 
       freeze-t2d79.2-0 ; 
       freeze-t2d80.2-0 ; 
       freeze-t2d82.1-0 ; 
       freeze-t2d83.1-0 ; 
       freeze-t2d87.2-0 ; 
       freeze-t2d90.2-0 ; 
       freeze-t2d92.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 46 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 21 110 ; 
       28 40 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       29 45 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 22 110 ; 
       20 40 110 ; 
       21 45 110 ; 
       22 21 110 ; 
       25 3 110 ; 
       26 4 110 ; 
       30 29 110 ; 
       31 4 110 ; 
       32 21 110 ; 
       33 8 110 ; 
       35 25 110 ; 
       36 41 110 ; 
       37 42 110 ; 
       38 43 110 ; 
       39 44 110 ; 
       40 1 110 ; 
       41 9 110 ; 
       42 9 110 ; 
       43 9 110 ; 
       44 9 110 ; 
       45 1 110 ; 
       46 4 110 ; 
       47 46 110 ; 
       27 30 110 ; 
       34 29 110 ; 
       24 29 110 ; 
       23 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 46 300 ; 
       3 47 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       7 42 300 ; 
       28 35 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 49 300 ; 
       10 21 300 ; 
       29 36 300 ; 
       11 3 300 ; 
       12 4 300 ; 
       13 0 300 ; 
       14 5 300 ; 
       15 6 300 ; 
       16 1 300 ; 
       17 7 300 ; 
       18 8 300 ; 
       20 31 300 ; 
       21 32 300 ; 
       22 41 300 ; 
       25 29 300 ; 
       25 30 300 ; 
       26 2 300 ; 
       26 17 300 ; 
       26 18 300 ; 
       26 19 300 ; 
       26 20 300 ; 
       26 48 300 ; 
       30 37 300 ; 
       31 23 300 ; 
       32 40 300 ; 
       33 22 300 ; 
       35 22 300 ; 
       36 27 300 ; 
       37 28 300 ; 
       38 28 300 ; 
       39 27 300 ; 
       41 24 300 ; 
       42 26 300 ; 
       43 25 300 ; 
       44 50 300 ; 
       45 33 300 ; 
       46 43 300 ; 
       47 34 300 ; 
       34 38 300 ; 
       24 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 5 400 ; 
       11 6 400 ; 
       12 36 400 ; 
       13 10 400 ; 
       14 7 400 ; 
       15 8 400 ; 
       16 9 400 ; 
       17 11 400 ; 
       18 12 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       31 23 401 ; 
       35 28 401 ; 
       9 19 401 ; 
       10 4 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 1 401 ; 
       18 2 401 ; 
       19 3 401 ; 
       20 0 401 ; 
       36 29 401 ; 
       32 24 401 ; 
       24 38 401 ; 
       25 39 401 ; 
       26 40 401 ; 
       30 21 401 ; 
       37 30 401 ; 
       41 25 401 ; 
       34 27 401 ; 
       39 31 401 ; 
       42 26 401 ; 
       43 32 401 ; 
       45 33 401 ; 
       46 20 401 ; 
       47 34 401 ; 
       48 35 401 ; 
       49 37 401 ; 
       50 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 105 -8 0 MPRFLG 0 ; 
       1 SCHEM 83.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 135 -4 0 MPRFLG 0 ; 
       5 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 MPRFLG 0 ; 
       7 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 137.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       29 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 58.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 63.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 73.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 78.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 88.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 93.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 0 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       21 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 116.25 -6 0 MPRFLG 0 ; 
       30 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 125 -6 0 MPRFLG 0 ; 
       32 SCHEM 30 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 12.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 127.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 142.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       41 SCHEM 128.75 -8 0 MPRFLG 0 ; 
       42 SCHEM 133.75 -8 0 MPRFLG 0 ; 
       43 SCHEM 138.75 -8 0 MPRFLG 0 ; 
       44 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       45 SCHEM 40 -4 0 MPRFLG 0 ; 
       46 SCHEM 105 -6 0 MPRFLG 0 ; 
       47 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 45 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 40 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 114 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 84 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 152.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 155 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 167.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 169 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 51 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
