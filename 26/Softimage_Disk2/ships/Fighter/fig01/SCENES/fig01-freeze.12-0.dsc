SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.31-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.31-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.31-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       freeze-light1.12-0 ROOT ; 
       freeze-light2.12-0 ROOT ; 
       freeze-light3.12-0 ROOT ; 
       freeze-light4.12-0 ROOT ; 
       freeze-light5.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-default3_1.1-0 ; 
       destroy-default4_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat20_1.1-0 ; 
       destroy-mat21_1.1-0 ; 
       destroy-mat22_1.1-0 ; 
       destroy-mat23_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       freeze-mat102.2-0 ; 
       freeze-mat105.2-0 ; 
       freeze-mat107.2-0 ; 
       freeze-mat109.1-0 ; 
       freeze-mat115.1-0 ; 
       freeze-mat116.1-0 ; 
       freeze-mat117.1-0 ; 
       freeze-mat118.1-0 ; 
       freeze-mat119.2-0 ; 
       freeze-mat12_2.1-0 ; 
       freeze-mat120.2-0 ; 
       freeze-mat121.1-0 ; 
       freeze-mat122.1-0 ; 
       freeze-mat123.1-0 ; 
       freeze-mat124.1-0 ; 
       freeze-mat125.1-0 ; 
       freeze-mat19_2.1-0 ; 
       freeze-mat51.1-0 ; 
       freeze-mat52.2-0 ; 
       freeze-mat56.2-0 ; 
       freeze-mat87.2-0 ; 
       freeze-mat89.2-0 ; 
       freeze-mat90.2-0 ; 
       freeze-mat95.2-0 ; 
       freeze-mat96.2-0 ; 
       freeze-mat97.1-0 ; 
       freeze-mat98.1-0 ; 
       freeze-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.29-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf4.1-0 ; 
       fig01-LLl1.1-0 ROOT ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLr1.1-0 ROOT ; 
       fig01-LLr2.1-0 ; 
       fig01-LLr3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-freeze.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1_1.6-0 ; 
       destroy-t2d10_1_1.6-0 ; 
       destroy-t2d11_1_1.6-0 ; 
       destroy-t2d12_1_1.6-0 ; 
       destroy-t2d2_1_1.7-0 ; 
       destroy-t2d23_1_1.3-0 ; 
       destroy-t2d24_1_1.3-0 ; 
       destroy-t2d26_1_1.3-0 ; 
       destroy-t2d27_1_1.3-0 ; 
       destroy-t2d28_1_1.3-0 ; 
       destroy-t2d29_1_1.3-0 ; 
       destroy-t2d3_1_1.7-0 ; 
       destroy-t2d4_1_1.7-0 ; 
       destroy-t2d5_1_1.7-0 ; 
       destroy-t2d6_1_1.7-0 ; 
       destroy-t2d7_1_1.7-0 ; 
       destroy-t2d8_1_1.7-0 ; 
       freeze-t2d106.2-0 ; 
       freeze-t2d107.1-0 ; 
       freeze-t2d108.1-0 ; 
       freeze-t2d109.1-0 ; 
       freeze-t2d110.1-0 ; 
       freeze-t2d111.1-0 ; 
       freeze-t2d112.1-0 ; 
       freeze-t2d113.1-0 ; 
       freeze-t2d114.1-0 ; 
       freeze-t2d115.1-0 ; 
       freeze-t2d116.1-0 ; 
       freeze-t2d117.1-0 ; 
       freeze-t2d118.1-0 ; 
       freeze-t2d119.1-0 ; 
       freeze-t2d120.1-0 ; 
       freeze-t2d121.1-0 ; 
       freeze-t2d72.2-0 ; 
       freeze-t2d74.2-0 ; 
       freeze-t2d79.2-0 ; 
       freeze-t2d80.2-0 ; 
       freeze-t2d83.1-0 ; 
       freeze-t2d87.2-0 ; 
       freeze-t2d90.2-0 ; 
       freeze-t2d92.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 46 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 21 110 ; 
       28 40 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       29 45 110 ; 
       11 10 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 22 110 ; 
       20 40 110 ; 
       21 45 110 ; 
       22 21 110 ; 
       12 10 110 ; 
       25 3 110 ; 
       26 4 110 ; 
       30 29 110 ; 
       31 4 110 ; 
       32 21 110 ; 
       33 8 110 ; 
       35 25 110 ; 
       36 41 110 ; 
       37 42 110 ; 
       38 43 110 ; 
       39 44 110 ; 
       40 1 110 ; 
       41 9 110 ; 
       42 9 110 ; 
       43 9 110 ; 
       44 9 110 ; 
       45 1 110 ; 
       46 4 110 ; 
       47 46 110 ; 
       27 30 110 ; 
       34 29 110 ; 
       24 29 110 ; 
       23 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 46 300 ; 
       3 47 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       7 42 300 ; 
       28 34 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 49 300 ; 
       10 20 300 ; 
       29 35 300 ; 
       11 3 300 ; 
       13 0 300 ; 
       14 4 300 ; 
       15 5 300 ; 
       16 1 300 ; 
       17 6 300 ; 
       18 7 300 ; 
       20 30 300 ; 
       21 31 300 ; 
       22 41 300 ; 
       12 39 300 ; 
       25 28 300 ; 
       25 29 300 ; 
       26 2 300 ; 
       26 16 300 ; 
       26 17 300 ; 
       26 18 300 ; 
       26 19 300 ; 
       26 48 300 ; 
       30 36 300 ; 
       31 22 300 ; 
       32 40 300 ; 
       33 21 300 ; 
       35 21 300 ; 
       36 26 300 ; 
       37 27 300 ; 
       38 27 300 ; 
       39 26 300 ; 
       41 23 300 ; 
       42 25 300 ; 
       43 24 300 ; 
       44 50 300 ; 
       45 32 300 ; 
       46 43 300 ; 
       47 33 300 ; 
       34 37 300 ; 
       24 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       13 8 400 ; 
       14 5 400 ; 
       15 6 400 ; 
       16 7 400 ; 
       17 9 400 ; 
       18 10 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       30 21 401 ; 
       34 26 401 ; 
       3 31 401 ; 
       8 17 401 ; 
       9 4 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
       15 16 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 3 401 ; 
       19 0 401 ; 
       20 30 401 ; 
       35 27 401 ; 
       31 22 401 ; 
       23 38 401 ; 
       24 39 401 ; 
       25 40 401 ; 
       29 19 401 ; 
       36 28 401 ; 
       41 23 401 ; 
       33 25 401 ; 
       38 29 401 ; 
       42 24 401 ; 
       39 32 401 ; 
       43 33 401 ; 
       45 34 401 ; 
       46 18 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 16.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 65 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 60 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 MPRFLG 0 ; 
       7 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 100 -6 0 MPRFLG 0 ; 
       10 SCHEM 60 -6 0 MPRFLG 0 ; 
       29 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 6.25 -20 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.4103305 -0.4019272 -0.004356742 MPRFLG 0 ; 
       14 SCHEM 1.25 -22 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 6.25 -22 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -14 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.4084956 -0.4039051 -0.0036093 MPRFLG 0 ; 
       17 SCHEM 1.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 6.25 -16 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 0 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       21 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 60 -8 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       30 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 30 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 12.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 90 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 105 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       41 SCHEM 91.25 -8 0 MPRFLG 0 ; 
       42 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       43 SCHEM 101.25 -8 0 MPRFLG 0 ; 
       44 SCHEM 106.25 -8 0 MPRFLG 0 ; 
       45 SCHEM 40 -4 0 MPRFLG 0 ; 
       46 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       47 SCHEM 65 -8 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 45 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 40 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 76.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 0 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -22 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 0 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 120 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 131.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 51 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
