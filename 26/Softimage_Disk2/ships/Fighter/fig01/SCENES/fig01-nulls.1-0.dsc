SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.34-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.34-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.34-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       nulls-light1.1-0 ROOT ; 
       nulls-light2.1-0 ROOT ; 
       nulls-light3.1-0 ROOT ; 
       nulls-light4.1-0 ROOT ; 
       nulls-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-mat16_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       nulls-mat102.1-0 ; 
       nulls-mat105.1-0 ; 
       nulls-mat107.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat116.1-0 ; 
       nulls-mat117.1-0 ; 
       nulls-mat118.1-0 ; 
       nulls-mat119.1-0 ; 
       nulls-mat12_2.1-0 ; 
       nulls-mat120.1-0 ; 
       nulls-mat121.1-0 ; 
       nulls-mat122.1-0 ; 
       nulls-mat123.1-0 ; 
       nulls-mat124.1-0 ; 
       nulls-mat125.1-0 ; 
       nulls-mat19_2.1-0 ; 
       nulls-mat19_3.1-0 ; 
       nulls-mat19_4.1-0 ; 
       nulls-mat19_5.1-0 ; 
       nulls-mat19_6.1-0 ; 
       nulls-mat37_2.1-0 ; 
       nulls-mat37_3.1-0 ; 
       nulls-mat51.1-0 ; 
       nulls-mat52.1-0 ; 
       nulls-mat56.1-0 ; 
       nulls-mat87.1-0 ; 
       nulls-mat89.1-0 ; 
       nulls-mat90.1-0 ; 
       nulls-mat95.1-0 ; 
       nulls-mat96.1-0 ; 
       nulls-mat97.1-0 ; 
       nulls-mat98.1-0 ; 
       nulls-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 50     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.32-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf4.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLR1.1-0 ; 
       fig01-LLR2.1-0 ; 
       fig01-LLR3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
       fig01-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1_1.6-0 ; 
       destroy-t2d10_1_1.6-0 ; 
       destroy-t2d11_1_1.6-0 ; 
       destroy-t2d12_1_1.6-0 ; 
       destroy-t2d2_1_1.7-0 ; 
       destroy-t2d3_1_1.7-0 ; 
       destroy-t2d4_1_1.7-0 ; 
       destroy-t2d5_1_1.7-0 ; 
       destroy-t2d6_1_1.7-0 ; 
       destroy-t2d7_1_1.7-0 ; 
       destroy-t2d8_1_1.7-0 ; 
       nulls-t2d106.1-0 ; 
       nulls-t2d107.1-0 ; 
       nulls-t2d108.1-0 ; 
       nulls-t2d109.1-0 ; 
       nulls-t2d110.1-0 ; 
       nulls-t2d111.1-0 ; 
       nulls-t2d112.1-0 ; 
       nulls-t2d113.1-0 ; 
       nulls-t2d114.1-0 ; 
       nulls-t2d115.1-0 ; 
       nulls-t2d116.1-0 ; 
       nulls-t2d117.1-0 ; 
       nulls-t2d118.1-0 ; 
       nulls-t2d119.1-0 ; 
       nulls-t2d120.1-0 ; 
       nulls-t2d121.1-0 ; 
       nulls-t2d122.1-0 ; 
       nulls-t2d123.1-0 ; 
       nulls-t2d124.1-0 ; 
       nulls-t2d125.1-0 ; 
       nulls-t2d126.1-0 ; 
       nulls-t2d127.1-0 ; 
       nulls-t2d72.1-0 ; 
       nulls-t2d74.1-0 ; 
       nulls-t2d79.1-0 ; 
       nulls-t2d80.1-0 ; 
       nulls-t2d83.1-0 ; 
       nulls-t2d87.1-0 ; 
       nulls-t2d90.1-0 ; 
       nulls-t2d92.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       49 1 110 ; 
       40 1 110 ; 
       0 47 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 21 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 22 110 ; 
       20 41 110 ; 
       21 46 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 29 110 ; 
       25 3 110 ; 
       26 4 110 ; 
       27 30 110 ; 
       28 41 110 ; 
       29 46 110 ; 
       30 29 110 ; 
       31 4 110 ; 
       32 21 110 ; 
       33 8 110 ; 
       34 29 110 ; 
       35 25 110 ; 
       36 42 110 ; 
       37 43 110 ; 
       38 44 110 ; 
       39 45 110 ; 
       41 1 110 ; 
       42 9 110 ; 
       43 9 110 ; 
       44 9 110 ; 
       45 9 110 ; 
       46 1 110 ; 
       47 4 110 ; 
       48 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 46 300 ; 
       3 47 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       7 42 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 49 300 ; 
       10 14 300 ; 
       11 1 300 ; 
       12 33 300 ; 
       13 39 300 ; 
       14 36 300 ; 
       15 37 300 ; 
       16 38 300 ; 
       17 34 300 ; 
       18 35 300 ; 
       20 24 300 ; 
       21 25 300 ; 
       22 41 300 ; 
       24 32 300 ; 
       25 22 300 ; 
       25 23 300 ; 
       26 0 300 ; 
       26 10 300 ; 
       26 11 300 ; 
       26 12 300 ; 
       26 13 300 ; 
       26 48 300 ; 
       28 28 300 ; 
       29 29 300 ; 
       30 30 300 ; 
       31 16 300 ; 
       32 40 300 ; 
       33 15 300 ; 
       34 31 300 ; 
       35 15 300 ; 
       36 20 300 ; 
       37 21 300 ; 
       38 21 300 ; 
       39 20 300 ; 
       42 17 300 ; 
       43 19 300 ; 
       44 18 300 ; 
       45 50 300 ; 
       46 26 300 ; 
       47 43 300 ; 
       48 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 11 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 0 401 ; 
       14 24 401 ; 
       17 38 401 ; 
       18 39 401 ; 
       19 40 401 ; 
       23 13 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       32 23 401 ; 
       33 26 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 27 401 ; 
       39 30 401 ; 
       41 17 401 ; 
       42 18 401 ; 
       43 33 401 ; 
       45 34 401 ; 
       46 12 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       49 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 35 -6 0 MPRFLG 0 ; 
       18 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 20 -6 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 10 -6 0 MPRFLG 0 ; 
       26 SCHEM 50 -4 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 25 -4 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 MPRFLG 0 ; 
       35 SCHEM 10 -8 0 MPRFLG 0 ; 
       36 SCHEM 55 -8 0 MPRFLG 0 ; 
       37 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 60 -8 0 MPRFLG 0 ; 
       39 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       42 SCHEM 55 -6 0 MPRFLG 0 ; 
       43 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 60 -6 0 MPRFLG 0 ; 
       45 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       47 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       48 SCHEM 45 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 48 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 64 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 16 16 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
