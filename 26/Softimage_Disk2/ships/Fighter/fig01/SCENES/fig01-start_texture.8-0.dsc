SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig01-fig01.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.19-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       start_texture-light1.7-0 ROOT ; 
       start_texture-light2.7-0 ROOT ; 
       start_texture-light3.6-0 ROOT ; 
       start_texture-light4.6-0 ROOT ; 
       start_texture-light5.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 84     
       destroy-default3_1.1-0 ; 
       destroy-default4_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat18_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat20_1.1-0 ; 
       destroy-mat21_1.1-0 ; 
       destroy-mat22_1.1-0 ; 
       destroy-mat23_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat9.1-0 ; 
       il_heavyfighterx_sAT-mat46.1-0 ; 
       il_heavyfighterx_sAT-mat47.1-0 ; 
       start_texture-mat100.2-0 ; 
       start_texture-mat102.1-0 ; 
       start_texture-mat103.1-0 ; 
       start_texture-mat104.1-0 ; 
       start_texture-mat105.1-0 ; 
       start_texture-mat106.1-0 ; 
       start_texture-mat107.1-0 ; 
       start_texture-mat108.1-0 ; 
       start_texture-mat109.2-0 ; 
       start_texture-mat115.2-0 ; 
       start_texture-mat119.1-0 ; 
       start_texture-mat12_2.1-0 ; 
       start_texture-mat120.1-0 ; 
       start_texture-mat49.1-0 ; 
       start_texture-mat51.1-0 ; 
       start_texture-mat52.1-0 ; 
       start_texture-mat53.1-0 ; 
       start_texture-mat54.1-0 ; 
       start_texture-mat55.1-0 ; 
       start_texture-mat56.1-0 ; 
       start_texture-mat57.1-0 ; 
       start_texture-mat58.1-0 ; 
       start_texture-mat59.1-0 ; 
       start_texture-mat60.1-0 ; 
       start_texture-mat61.1-0 ; 
       start_texture-mat62.1-0 ; 
       start_texture-mat63.1-0 ; 
       start_texture-mat64.1-0 ; 
       start_texture-mat65.1-0 ; 
       start_texture-mat66.1-0 ; 
       start_texture-mat67.1-0 ; 
       start_texture-mat68.1-0 ; 
       start_texture-mat69.1-0 ; 
       start_texture-mat70.1-0 ; 
       start_texture-mat71.1-0 ; 
       start_texture-mat72.1-0 ; 
       start_texture-mat73.1-0 ; 
       start_texture-mat74.1-0 ; 
       start_texture-mat75.1-0 ; 
       start_texture-mat85.1-0 ; 
       start_texture-mat86.1-0 ; 
       start_texture-mat87.1-0 ; 
       start_texture-mat88.1-0 ; 
       start_texture-mat89.1-0 ; 
       start_texture-mat90.1-0 ; 
       start_texture-mat91.1-0 ; 
       start_texture-mat92.1-0 ; 
       start_texture-mat93.1-0 ; 
       start_texture-mat94.1-0 ; 
       start_texture-mat95.1-0 ; 
       start_texture-mat96.1-0 ; 
       start_texture-mat97.1-0 ; 
       start_texture-mat98.1-0 ; 
       start_texture-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.18-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf3.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLr1.1-0 ; 
       fig01-LLr2.1-0 ; 
       fig01-LLr3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr1.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSr.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-start_texture.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 69     
       destroy-2d1_1_1.4-0 ; 
       destroy-t2d10_1_1.4-0 ; 
       destroy-t2d11_1_1.4-0 ; 
       destroy-t2d12_1_1.4-0 ; 
       destroy-t2d13_1_1.2-0 ; 
       destroy-t2d2_1_1.5-0 ; 
       destroy-t2d20_1_1.2-0 ; 
       destroy-t2d22_1_1.2-0 ; 
       destroy-t2d23_1_1.2-0 ; 
       destroy-t2d24_1_1.2-0 ; 
       destroy-t2d26_1_1.2-0 ; 
       destroy-t2d27_1_1.2-0 ; 
       destroy-t2d28_1_1.2-0 ; 
       destroy-t2d29_1_1.2-0 ; 
       destroy-t2d3_1_1.5-0 ; 
       destroy-t2d30_1_1.2-0 ; 
       destroy-t2d31_1_1.2-0 ; 
       destroy-t2d32_1_1.2-0 ; 
       destroy-t2d33_1_1.2-0 ; 
       destroy-t2d4_1_1.5-0 ; 
       destroy-t2d5_1_1.5-0 ; 
       destroy-t2d6_1_1.5-0 ; 
       destroy-t2d7_1_1.5-0 ; 
       destroy-t2d8_1_1.5-0 ; 
       il_heavyfighterx_sAT-t2d42.2-0 ; 
       il_heavyfighterx_sAT-t2d43.2-0 ; 
       start_texture-qs.2-0 ; 
       start_texture-t2d104.1-0 ; 
       start_texture-t2d105.1-0 ; 
       start_texture-t2d106.3-0 ; 
       start_texture-t2d13_1_2.1-0 ; 
       start_texture-t2d44.1-0 ; 
       start_texture-t2d45.1-0 ; 
       start_texture-t2d46.1-0 ; 
       start_texture-t2d47.1-0 ; 
       start_texture-t2d48.1-0 ; 
       start_texture-t2d49.1-0 ; 
       start_texture-t2d50.1-0 ; 
       start_texture-t2d51.1-0 ; 
       start_texture-t2d52.1-0 ; 
       start_texture-t2d53.1-0 ; 
       start_texture-t2d54.1-0 ; 
       start_texture-t2d55.1-0 ; 
       start_texture-t2d56.1-0 ; 
       start_texture-t2d57.1-0 ; 
       start_texture-t2d58.1-0 ; 
       start_texture-t2d59.1-0 ; 
       start_texture-t2d60.1-0 ; 
       start_texture-t2d61.1-0 ; 
       start_texture-t2d70.1-0 ; 
       start_texture-t2d71.1-0 ; 
       start_texture-t2d72.1-0 ; 
       start_texture-t2d73.1-0 ; 
       start_texture-t2d74.1-0 ; 
       start_texture-t2d75.1-0 ; 
       start_texture-t2d76.1-0 ; 
       start_texture-t2d77.1-0 ; 
       start_texture-t2d78.1-0 ; 
       start_texture-t2d79.1-0 ; 
       start_texture-t2d80.3-0 ; 
       start_texture-t2d82.1-0 ; 
       start_texture-t2d83.2-0 ; 
       start_texture-t2d85.3-0 ; 
       start_texture-t2d87.3-0 ; 
       start_texture-t2d88.3-0 ; 
       start_texture-t2d89.3-0 ; 
       start_texture-t2d90.2-0 ; 
       start_texture-t2d91.2-0 ; 
       start_texture-t2d92.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 46 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       7 21 110 ; 
       8 2 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 5 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 22 110 ; 
       20 40 110 ; 
       21 45 110 ; 
       22 21 110 ; 
       23 24 110 ; 
       24 29 110 ; 
       25 2 110 ; 
       26 4 110 ; 
       27 30 110 ; 
       28 40 110 ; 
       29 45 110 ; 
       30 29 110 ; 
       31 4 110 ; 
       32 21 110 ; 
       33 8 110 ; 
       34 29 110 ; 
       35 25 110 ; 
       36 41 110 ; 
       37 42 110 ; 
       38 43 110 ; 
       39 44 110 ; 
       40 1 110 ; 
       41 9 110 ; 
       42 9 110 ; 
       43 9 110 ; 
       44 9 110 ; 
       45 1 110 ; 
       46 4 110 ; 
       47 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 79 300 ; 
       3 80 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       8 73 300 ; 
       8 74 300 ; 
       8 75 300 ; 
       8 42 300 ; 
       9 82 300 ; 
       10 23 300 ; 
       11 5 300 ; 
       12 6 300 ; 
       13 0 300 ; 
       14 7 300 ; 
       15 8 300 ; 
       16 1 300 ; 
       17 9 300 ; 
       18 10 300 ; 
       20 2 300 ; 
       20 25 300 ; 
       20 28 300 ; 
       20 70 300 ; 
       21 61 300 ; 
       21 62 300 ; 
       21 63 300 ; 
       21 64 300 ; 
       22 45 300 ; 
       22 46 300 ; 
       22 47 300 ; 
       22 48 300 ; 
       24 57 300 ; 
       24 58 300 ; 
       24 59 300 ; 
       24 60 300 ; 
       25 76 300 ; 
       25 77 300 ; 
       25 78 300 ; 
       25 40 300 ; 
       26 4 300 ; 
       26 19 300 ; 
       26 20 300 ; 
       26 21 300 ; 
       26 22 300 ; 
       26 81 300 ; 
       28 3 300 ; 
       28 24 300 ; 
       28 29 300 ; 
       28 69 300 ; 
       29 65 300 ; 
       29 66 300 ; 
       29 67 300 ; 
       29 68 300 ; 
       30 53 300 ; 
       30 54 300 ; 
       30 55 300 ; 
       30 56 300 ; 
       31 27 300 ; 
       32 44 300 ; 
       33 26 300 ; 
       34 43 300 ; 
       35 26 300 ; 
       36 38 300 ; 
       37 39 300 ; 
       38 39 300 ; 
       39 38 300 ; 
       41 31 300 ; 
       41 32 300 ; 
       41 33 300 ; 
       42 36 300 ; 
       42 37 300 ; 
       43 34 300 ; 
       43 35 300 ; 
       44 83 300 ; 
       44 30 300 ; 
       45 41 300 ; 
       46 71 300 ; 
       46 72 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 4 400 ; 
       10 6 400 ; 
       11 7 400 ; 
       12 60 400 ; 
       13 11 400 ; 
       14 8 400 ; 
       15 9 400 ; 
       16 10 400 ; 
       17 12 400 ; 
       18 13 400 ; 
       20 15 400 ; 
       25 30 400 ; 
       28 16 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       11 29 401 ; 
       12 5 401 ; 
       13 14 401 ; 
       14 19 401 ; 
       15 20 401 ; 
       16 21 401 ; 
       17 22 401 ; 
       18 23 401 ; 
       19 1 401 ; 
       20 2 401 ; 
       21 3 401 ; 
       22 0 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 62 401 ; 
       31 63 401 ; 
       32 64 401 ; 
       33 65 401 ; 
       34 66 401 ; 
       35 67 401 ; 
       36 68 401 ; 
       37 26 401 ; 
       40 27 401 ; 
       42 28 401 ; 
       46 31 401 ; 
       47 32 401 ; 
       48 33 401 ; 
       50 34 401 ; 
       51 35 401 ; 
       52 36 401 ; 
       53 57 401 ; 
       54 37 401 ; 
       55 38 401 ; 
       56 39 401 ; 
       58 40 401 ; 
       59 41 401 ; 
       60 42 401 ; 
       62 43 401 ; 
       63 44 401 ; 
       64 45 401 ; 
       66 46 401 ; 
       67 47 401 ; 
       68 48 401 ; 
       69 49 401 ; 
       70 50 401 ; 
       71 51 401 ; 
       72 52 401 ; 
       74 53 401 ; 
       75 54 401 ; 
       77 55 401 ; 
       78 56 401 ; 
       80 58 401 ; 
       81 59 401 ; 
       82 61 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16.9463 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16.9463 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16.9463 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16.9463 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16.9463 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 185 -8 0 MPRFLG 0 ; 
       1 SCHEM 131.25 -2 0 SRT 1 1 1 6.370452e-005 -1.569898 0.002393199 0 0 0 MPRFLG 0 ; 
       2 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 222.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 158.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 -10 0 MPRFLG 0 ; 
       7 SCHEM 65 -8 0 MPRFLG 0 ; 
       8 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 226.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 143.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 138.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 143.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 158.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 153.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 158.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 173.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 168.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 173.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 75 -10 0 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 80 -8 0 MPRFLG 0 ; 
       23 SCHEM 100 -10 0 MPRFLG 0 ; 
       24 SCHEM 105 -8 0 MPRFLG 0 ; 
       25 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       26 SCHEM 198.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 112.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 115 -6 0 MPRFLG 0 ; 
       30 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 207.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 72.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 30 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 97.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 45 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 210.535 -10.22929 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       37 SCHEM 220 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 227.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 234.2851 -10.22929 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       40 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       41 SCHEM 213.75 -8 0 MPRFLG 0 ; 
       42 SCHEM 222.5 -8 0 MPRFLG 0 ; 
       43 SCHEM 230 -8 0 MPRFLG 0 ; 
       44 SCHEM 237.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       45 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       46 SCHEM 186.25 -6 0 MPRFLG 0 ; 
       47 SCHEM 182.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 205 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 167.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 255 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 257.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 260 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 262.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 252.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 245 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 247.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 250 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 202.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.90878 -14.9463 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 207.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 239.2851 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 213.035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 215.535 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 218.035 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 230 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 232.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 222.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 225 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 210.535 -12.22929 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 220 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 242.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 236.7851 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 195 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 202.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 200 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 197.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 257.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 180 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 164.6826 -8.899326 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 260 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 0 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 262.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 252.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 245 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 247.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 250 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 225 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 255 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 117.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 187.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 145 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 242.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 239.2851 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 213.035 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 215.535 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 218.035 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 230 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 232.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 222.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 264 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 69 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
