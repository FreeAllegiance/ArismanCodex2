SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.55-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.55-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       static-light1.4-0 ROOT ; 
       static-light2.4-0 ROOT ; 
       static-light3.4-0 ROOT ; 
       static-light4.4-0 ROOT ; 
       static-light5.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-mat16_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat9.2-0 ; 
       static-mat102.3-0 ; 
       static-mat105.3-0 ; 
       static-mat107.3-0 ; 
       static-mat109.3-0 ; 
       static-mat115.3-0 ; 
       static-mat116.3-0 ; 
       static-mat117.3-0 ; 
       static-mat118.3-0 ; 
       static-mat119.3-0 ; 
       static-mat12_2.3-0 ; 
       static-mat120.3-0 ; 
       static-mat121.3-0 ; 
       static-mat122.3-0 ; 
       static-mat123.3-0 ; 
       static-mat124.3-0 ; 
       static-mat125.3-0 ; 
       static-mat19_2.1-0 ; 
       static-mat19_3.1-0 ; 
       static-mat19_4.1-0 ; 
       static-mat19_5.1-0 ; 
       static-mat19_6.1-0 ; 
       static-mat37_2.1-0 ; 
       static-mat37_3.1-0 ; 
       static-mat51.3-0 ; 
       static-mat52.3-0 ; 
       static-mat56.3-0 ; 
       static-mat87.3-0 ; 
       static-mat89.3-0 ; 
       static-mat90.3-0 ; 
       static-mat95.3-0 ; 
       static-mat96.3-0 ; 
       static-mat97.3-0 ; 
       static-mat98.3-0 ; 
       static-mat99.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       fig01-cockpt.1-0 ; 
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.49-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-landgr0.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lengine.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-LLf1.1-0 ; 
       fig01-LLf2.1-0 ; 
       fig01-LLf4.1-0 ; 
       fig01-LLl1.1-0 ; 
       fig01-LLl2.1-0 ; 
       fig01-LLl3.1-0 ; 
       fig01-LLR1.1-0 ; 
       fig01-LLR2.1-0 ; 
       fig01-LLR3.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-lthrust.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rengine.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rthrust.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-smoke.1-0 ; 
       fig01-SSf.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-SSt1.1-0 ; 
       fig01-SSt2.1-0 ; 
       fig01-SSt3.1-0 ; 
       fig01-SSt4.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
       fig01-trail.1-0 ; 
       fig01-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1_1.7-0 ; 
       destroy-t2d10_1_1.7-0 ; 
       destroy-t2d11_1_1.7-0 ; 
       destroy-t2d12_1_1.7-0 ; 
       destroy-t2d2_1_1.9-0 ; 
       destroy-t2d3_1_1.9-0 ; 
       destroy-t2d4_1_1.9-0 ; 
       destroy-t2d5_1_1.9-0 ; 
       destroy-t2d6_1_1.9-0 ; 
       destroy-t2d7_1_1.9-0 ; 
       destroy-t2d8_1_1.9-0 ; 
       static-t2d106.3-0 ; 
       static-t2d107.3-0 ; 
       static-t2d108.3-0 ; 
       static-t2d109.3-0 ; 
       static-t2d110.3-0 ; 
       static-t2d111.3-0 ; 
       static-t2d112.3-0 ; 
       static-t2d113.3-0 ; 
       static-t2d114.3-0 ; 
       static-t2d115.3-0 ; 
       static-t2d116.3-0 ; 
       static-t2d117.3-0 ; 
       static-t2d118.3-0 ; 
       static-t2d119.1-0 ; 
       static-t2d120.1-0 ; 
       static-t2d121.1-0 ; 
       static-t2d122.1-0 ; 
       static-t2d123.1-0 ; 
       static-t2d124.1-0 ; 
       static-t2d125.1-0 ; 
       static-t2d126.1-0 ; 
       static-t2d127.1-0 ; 
       static-t2d72.3-0 ; 
       static-t2d74.3-0 ; 
       static-t2d79.3-0 ; 
       static-t2d80.3-0 ; 
       static-t2d83.3-0 ; 
       static-t2d87.3-0 ; 
       static-t2d90.3-0 ; 
       static-t2d92.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 50 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 8 110 ; 
       8 23 110 ; 
       9 44 110 ; 
       10 4 110 ; 
       11 5 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 6 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 6 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 24 110 ; 
       22 9 110 ; 
       23 49 110 ; 
       24 23 110 ; 
       25 26 110 ; 
       26 32 110 ; 
       27 44 110 ; 
       28 4 110 ; 
       29 5 110 ; 
       30 33 110 ; 
       31 27 110 ; 
       32 49 110 ; 
       33 32 110 ; 
       35 5 110 ; 
       36 23 110 ; 
       37 10 110 ; 
       38 32 110 ; 
       39 28 110 ; 
       40 45 110 ; 
       41 46 110 ; 
       42 47 110 ; 
       43 48 110 ; 
       44 2 110 ; 
       45 11 110 ; 
       46 11 110 ; 
       47 11 110 ; 
       48 11 110 ; 
       49 2 110 ; 
       50 5 110 ; 
       51 50 110 ; 
       52 2 110 ; 
       53 2 110 ; 
       34 29 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 46 300 ; 
       4 47 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       8 42 300 ; 
       9 24 300 ; 
       10 44 300 ; 
       10 45 300 ; 
       11 49 300 ; 
       12 14 300 ; 
       13 1 300 ; 
       14 33 300 ; 
       15 39 300 ; 
       16 36 300 ; 
       17 37 300 ; 
       18 38 300 ; 
       19 34 300 ; 
       20 35 300 ; 
       23 25 300 ; 
       24 41 300 ; 
       26 32 300 ; 
       27 28 300 ; 
       28 22 300 ; 
       28 23 300 ; 
       29 0 300 ; 
       29 10 300 ; 
       29 11 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       29 48 300 ; 
       32 29 300 ; 
       33 30 300 ; 
       35 16 300 ; 
       36 40 300 ; 
       37 15 300 ; 
       38 31 300 ; 
       39 15 300 ; 
       40 20 300 ; 
       41 21 300 ; 
       42 21 300 ; 
       43 20 300 ; 
       45 17 300 ; 
       46 19 300 ; 
       47 18 300 ; 
       48 50 300 ; 
       49 26 300 ; 
       50 43 300 ; 
       51 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 25 401 ; 
       2 11 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 0 401 ; 
       14 24 401 ; 
       17 38 401 ; 
       18 39 401 ; 
       19 40 401 ; 
       23 13 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       32 23 401 ; 
       33 26 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 27 401 ; 
       39 30 401 ; 
       41 17 401 ; 
       42 18 401 ; 
       43 33 401 ; 
       45 34 401 ; 
       46 12 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 25 -8 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 MPRFLG 0 ; 
       20 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 15 -10 0 MPRFLG 0 ; 
       22 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 15 -8 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 5 -8 0 MPRFLG 0 ; 
       29 SCHEM 45 -6 0 MPRFLG 0 ; 
       30 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20 -6 0 MPRFLG 0 ; 
       33 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 20 -8 0 MPRFLG 0 ; 
       39 SCHEM 5 -10 0 MPRFLG 0 ; 
       40 SCHEM 50 -10 0 MPRFLG 0 ; 
       41 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 55 -10 0 MPRFLG 0 ; 
       43 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       44 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       45 SCHEM 50 -8 0 MPRFLG 0 ; 
       46 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 55 -8 0 MPRFLG 0 ; 
       48 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       49 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       50 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       51 SCHEM 40 -8 0 MPRFLG 0 ; 
       52 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
