SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.56-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.56-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       done_lite-mat11.2-0 ; 
       static-mat116.3-0 ; 
       static-mat117.3-0 ; 
       static-mat118.3-0 ; 
       static-mat119.3-0 ; 
       static-mat12_2.3-0 ; 
       static-mat121.3-0 ; 
       static-mat122.3-0 ; 
       static-mat123.3-0 ; 
       static-mat124.3-0 ; 
       static-mat125.3-0 ; 
       static-mat126.1-0 ; 
       static-mat51.3-0 ; 
       static-mat52.3-0 ; 
       static-mat56.3-0 ; 
       static-mat87.3-0 ; 
       static-mat89.3-0 ; 
       static-mat90.3-0 ; 
       static-mat95.3-0 ; 
       static-mat96.3-0 ; 
       static-mat97.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       fig01-fig01.50-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-lbcrgatt.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lengine.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-ltcrgatt.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbcrgatt.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rengine.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtcrgatt.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-SSl.1-0 ; 
       fig01-SSla.1-0 ; 
       fig01-SSR.1-0 ; 
       fig01-SSra.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       destroy-2d1_1_1.7-0 ; 
       destroy-t2d10_1_1.7-0 ; 
       destroy-t2d11_1_1.7-0 ; 
       destroy-t2d12_1_1.7-0 ; 
       destroy-t2d2_1_1.9-0 ; 
       destroy-t2d3_1_1.9-0 ; 
       destroy-t2d4_1_1.9-0 ; 
       destroy-t2d5_1_1.9-0 ; 
       destroy-t2d6_1_1.9-0 ; 
       destroy-t2d7_1_1.9-0 ; 
       destroy-t2d8_1_1.9-0 ; 
       static-t2d106.3-0 ; 
       static-t2d107.3-0 ; 
       static-t2d108.3-0 ; 
       static-t2d110.3-0 ; 
       static-t2d111.3-0 ; 
       static-t2d112.3-0 ; 
       static-t2d113.3-0 ; 
       static-t2d115.3-0 ; 
       static-t2d116.3-0 ; 
       static-t2d117.3-0 ; 
       static-t2d118.3-0 ; 
       static-t2d128.1-0 ; 
       static-t2d72.3-0 ; 
       static-t2d74.3-0 ; 
       static-t2d79.3-0 ; 
       static-t2d80.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 5 110 ; 
       5 9 110 ; 
       6 23 110 ; 
       7 2 110 ; 
       8 10 110 ; 
       9 24 110 ; 
       10 9 110 ; 
       11 12 110 ; 
       12 17 110 ; 
       13 23 110 ; 
       14 2 110 ; 
       15 3 110 ; 
       16 18 110 ; 
       17 24 110 ; 
       18 17 110 ; 
       19 9 110 ; 
       20 7 110 ; 
       21 17 110 ; 
       22 14 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       25 3 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 31 300 ; 
       2 32 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       5 27 300 ; 
       6 16 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       9 17 300 ; 
       10 26 300 ; 
       12 23 300 ; 
       13 19 300 ; 
       14 14 300 ; 
       14 15 300 ; 
       15 0 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 12 300 ; 
       15 33 300 ; 
       17 20 300 ; 
       18 21 300 ; 
       19 25 300 ; 
       20 13 300 ; 
       21 22 300 ; 
       22 13 300 ; 
       24 18 300 ; 
       25 28 300 ; 
       26 24 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 0 401 ; 
       24 22 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       23 21 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 23 401 ; 
       30 24 401 ; 
       31 12 401 ; 
       32 25 401 ; 
       33 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -10 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 0 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 5 -8 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 20 -6 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 20 -8 0 MPRFLG 0 ; 
       22 SCHEM 5 -10 0 MPRFLG 0 ; 
       23 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 25 -6 0 MPRFLG 0 ; 
       26 SCHEM 25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
