SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.67-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.67-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       static-mat102.4-0 ; 
       static-mat105.4-0 ; 
       static-mat107.4-0 ; 
       static-mat116.4-0 ; 
       static-mat117.4-0 ; 
       static-mat118.4-0 ; 
       static-mat119.4-0 ; 
       static-mat12_2.4-0 ; 
       static-mat120.4-0 ; 
       static-mat121.4-0 ; 
       static-mat122.4-0 ; 
       static-mat123.4-0 ; 
       static-mat125.4-0 ; 
       static-mat52.4-0 ; 
       static-mat56.4-0 ; 
       static-mat87.4-0 ; 
       static-mat89.4-0 ; 
       static-mat90.4-0 ; 
       static-mat95.4-0 ; 
       static-mat96.4-0 ; 
       static-mat97.4-0 ; 
       static-mat98.4-0 ; 
       static-mat99.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       fig01-fcrgatt.1-0 ; 
       fig01-fig01.58-0 ROOT ; 
       fig01-finzzz0.1-0 ; 
       fig01-finzzz1.1-0 ; 
       fig01-fuselg1.1-0 ; 
       fig01-lbtractr.1-0 ; 
       fig01-lengine.1-0 ; 
       fig01-lfinzzz.1-0 ; 
       fig01-lights1.1-0 ; 
       fig01-ltractr1.1-0 ; 
       fig01-lttractr.1-0 ; 
       fig01-rbtractr.1-0 ; 
       fig01-rengine.1-0 ; 
       fig01-rfinzzz.1-0 ; 
       fig01-rfuselg.1-0 ; 
       fig01-rtractr.1-0 ; 
       fig01-rttractr.1-0 ; 
       fig01-thrust0.1-0 ; 
       fig01-tlights1.1-0 ; 
       fig01-tlights2.1-0 ; 
       fig01-tlights3.1-0 ; 
       fig01-tlights4.1-0 ; 
       fig01-tractr0.1-0 ; 
       fig01-tractr1.1-0 ; 
       fig01-tractr2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig01/PICTURES/fig01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig01-static.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       destroy-2d1_1_1.8-0 ; 
       destroy-t2d10_1_1.8-0 ; 
       destroy-t2d11_1_1.8-0 ; 
       destroy-t2d12_1_1.8-0 ; 
       destroy-t2d2_1_1.10-0 ; 
       destroy-t2d3_1_1.10-0 ; 
       destroy-t2d4_1_1.10-0 ; 
       destroy-t2d5_1_1.10-0 ; 
       destroy-t2d6_1_1.10-0 ; 
       destroy-t2d7_1_1.10-0 ; 
       destroy-t2d8_1_1.10-0 ; 
       static-t2d106.4-0 ; 
       static-t2d107.4-0 ; 
       static-t2d108.4-0 ; 
       static-t2d109.4-0 ; 
       static-t2d110.4-0 ; 
       static-t2d111.4-0 ; 
       static-t2d112.4-0 ; 
       static-t2d113.4-0 ; 
       static-t2d114.4-0 ; 
       static-t2d115.4-0 ; 
       static-t2d116.4-0 ; 
       static-t2d117.4-0 ; 
       static-t2d118.4-0 ; 
       static-t2d72.4-0 ; 
       static-t2d74.4-0 ; 
       static-t2d79.4-0 ; 
       static-t2d80.4-0 ; 
       static-t2d83.4-0 ; 
       static-t2d87.4-0 ; 
       static-t2d90.4-0 ; 
       static-t2d92.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 23 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 9 110 ; 
       6 17 110 ; 
       7 3 110 ; 
       8 4 110 ; 
       9 22 110 ; 
       10 9 110 ; 
       11 15 110 ; 
       12 17 110 ; 
       13 3 110 ; 
       14 4 110 ; 
       15 22 110 ; 
       16 15 110 ; 
       17 1 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 1 110 ; 
       23 4 110 ; 
       24 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 31 300 ; 
       3 32 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       5 27 300 ; 
       6 18 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       8 34 300 ; 
       9 19 300 ; 
       10 26 300 ; 
       11 25 300 ; 
       12 22 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       14 0 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 33 300 ; 
       15 23 300 ; 
       16 24 300 ; 
       18 13 300 ; 
       19 15 300 ; 
       20 14 300 ; 
       21 35 300 ; 
       22 20 300 ; 
       23 28 300 ; 
       24 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 11 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 0 401 ; 
       13 29 401 ; 
       14 30 401 ; 
       15 31 401 ; 
       17 13 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 24 401 ; 
       30 25 401 ; 
       31 12 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0.7736091 MPRFLG 0 ; 
       2 SCHEM 11.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 36.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 21.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 32.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 18.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 26.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 28 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
