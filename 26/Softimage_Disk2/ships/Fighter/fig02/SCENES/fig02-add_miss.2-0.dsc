SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_miss-fig02.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.38-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.38-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       add_miss-mat46.1-0 ; 
       add_miss-mat47.1-0 ; 
       add_miss-mat63.1-0 ; 
       add_miss-mat64.1-0 ; 
       add_miss-mat65.1-0 ; 
       add_miss-mat66.1-0 ; 
       add_miss-mat69.1-0 ; 
       add_miss-mat70.1-0 ; 
       add_miss-mat71.1-0 ; 
       add_miss-mat72.1-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       add_miss-afuselg.1-0 ; 
       add_miss-cockpt.1-0 ; 
       add_miss-ffuselg.1-0 ; 
       add_miss-fgun.1-0 ; 
       add_miss-fig02.2-0 ROOT ; 
       add_miss-finzzz0.1-0 ; 
       add_miss-finzzz1.1-0 ; 
       add_miss-fwepemt.1-0 ; 
       add_miss-fwepmnt.1-0 ; 
       add_miss-l-gun.1-0 ; 
       add_miss-lbooster.1-0 ; 
       add_miss-lfinzzz.1-0 ; 
       add_miss-lsmoke.1-0 ; 
       add_miss-lthrust.1-0 ; 
       add_miss-lwepatt.1-0 ; 
       add_miss-lwepemt.1-0 ; 
       add_miss-lwingzz0.1-0 ; 
       add_miss-lwingzz1.1-0 ; 
       add_miss-lwingzz2.1-0 ; 
       add_miss-lwingzz3.1-0 ; 
       add_miss-lwingzz4.1-0 ; 
       add_miss-missemt.1-0 ; 
       add_miss-r-gun.1-0 ; 
       add_miss-rbooster.1-0 ; 
       add_miss-rfinzzz.1-0 ; 
       add_miss-rsmoke.1-0 ; 
       add_miss-rthrust.1-0 ; 
       add_miss-rwepatt.1-0 ; 
       add_miss-rwepemt.1-0 ; 
       add_miss-rwingzz0.1-0 ; 
       add_miss-rwingzz1.1-0 ; 
       add_miss-rwingzz2.1-0 ; 
       add_miss-rwingzz3.1-0 ; 
       add_miss-rwingzz4.1-0 ; 
       add_miss-SSal.1-0 ; 
       add_miss-SSar.1-0 ; 
       add_miss-SSf.1-0 ; 
       add_miss-SSr.1-0 ; 
       add_miss-SSr1.1-0 ; 
       add_miss-tgun.1-0 ; 
       add_miss-thrust0.1-0 ; 
       add_miss-trail.1-0 ; 
       add_miss-twepemt.1-0 ; 
       add_miss-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-add_miss.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_miss-t2d42.1-0 ; 
       add_miss-t2d43.1-0 ; 
       add_miss-t2d44.1-0 ; 
       add_miss-t2d45.1-0 ; 
       add_miss-t2d85.1-0 ; 
       add_miss-t2d86.1-0 ; 
       add_miss-t2d87.1-0 ; 
       add_miss-t2d88.1-0 ; 
       add_miss-zt2d1.1-0 ; 
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 40 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 10 110 ; 
       14 4 110 ; 
       15 9 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 4 110 ; 
       22 2 110 ; 
       23 40 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 23 110 ; 
       27 4 110 ; 
       28 22 110 ; 
       29 4 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 11 110 ; 
       35 24 110 ; 
       36 2 110 ; 
       37 31 110 ; 
       38 18 110 ; 
       39 2 110 ; 
       40 4 110 ; 
       41 4 110 ; 
       42 39 110 ; 
       43 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       3 9 300 ; 
       6 30 300 ; 
       9 6 300 ; 
       10 13 300 ; 
       10 32 300 ; 
       10 0 300 ; 
       11 15 300 ; 
       11 3 300 ; 
       18 33 300 ; 
       18 34 300 ; 
       18 36 300 ; 
       19 38 300 ; 
       20 37 300 ; 
       22 7 300 ; 
       23 14 300 ; 
       23 31 300 ; 
       23 1 300 ; 
       24 16 300 ; 
       24 4 300 ; 
       31 10 300 ; 
       31 35 300 ; 
       32 11 300 ; 
       33 12 300 ; 
       34 41 300 ; 
       35 40 300 ; 
       36 39 300 ; 
       37 2 300 ; 
       38 5 300 ; 
       39 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 35 400 ; 
       2 10 400 ; 
       6 16 400 ; 
       10 19 400 ; 
       11 14 400 ; 
       19 24 400 ; 
       20 23 400 ; 
       23 20 400 ; 
       24 15 400 ; 
       32 25 400 ; 
       33 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 8 401 ; 
       4 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 28 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 29 401 ; 
       22 31 401 ; 
       23 32 401 ; 
       24 33 401 ; 
       25 34 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 13 401 ; 
       29 9 401 ; 
       31 21 401 ; 
       32 22 401 ; 
       34 27 401 ; 
       35 30 401 ; 
       36 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 61.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 36.25 0 0 SRT 1 1 1 6.264869e-005 -1.569883 0.002353534 0 0 0.9161164 MPRFLG 0 ; 
       5 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 53.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 66.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 8.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 36.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       21 SCHEM 68.75 -2 0 USR WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 11.25 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       24 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 63.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       28 SCHEM 51.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       30 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       33 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       34 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       35 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       36 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       38 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       39 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       40 SCHEM 10 -2 0 MPRFLG 0 ; 
       41 SCHEM 58.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 48.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 69 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
