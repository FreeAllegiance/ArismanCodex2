SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_more_guns-null3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.10-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       add_more_guns-mat46.1-0 ; 
       add_more_guns-mat47.1-0 ; 
       add_more_guns-mat63.1-0 ; 
       add_more_guns-mat64.1-0 ; 
       add_more_guns-mat65.1-0 ; 
       add_more_guns-mat66.1-0 ; 
       add_more_guns-mat69.1-0 ; 
       add_more_guns-mat70.1-0 ; 
       add_more_guns-mat71.1-0 ; 
       add_more_guns-mat72.2-0 ; 
       destroy-default3_1.2-0 ; 
       destroy-default4_1.2-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat18_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat20_1.2-0 ; 
       destroy-mat21_1.2-0 ; 
       destroy-mat22_1.2-0 ; 
       destroy-mat23_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 52     
       add_more_guns-afuselg.1-0 ; 
       add_more_guns-cockpt.1-0 ; 
       add_more_guns-cyl1.1-0 ; 
       add_more_guns-cyl3.1-0 ; 
       add_more_guns-ffuselg.1-0 ; 
       add_more_guns-fig02.1-0 ; 
       add_more_guns-finzzz0.1-0 ; 
       add_more_guns-finzzz1.1-0 ; 
       add_more_guns-fwepemt.1-0 ; 
       add_more_guns-fwepmnt.1-0 ; 
       add_more_guns-l-gun.1-0 ; 
       add_more_guns-landgr0.1-0 ; 
       add_more_guns-lbooster.1-0 ; 
       add_more_guns-lfinzzz.1-0 ; 
       add_more_guns-LLf1.1-0 ; 
       add_more_guns-LLf2.1-0 ; 
       add_more_guns-LLf3.1-0 ; 
       add_more_guns-LLl1.1-0 ; 
       add_more_guns-LLl2.1-0 ; 
       add_more_guns-LLl3.1-0 ; 
       add_more_guns-LLr1.1-0 ; 
       add_more_guns-LLr2.1-0 ; 
       add_more_guns-LLr3.1-0 ; 
       add_more_guns-lthrust.1-0 ; 
       add_more_guns-lwepatt.1-0 ; 
       add_more_guns-lwepemt.1-0 ; 
       add_more_guns-lwingzz0.1-0 ; 
       add_more_guns-lwingzz1.1-0 ; 
       add_more_guns-lwingzz2.1-0 ; 
       add_more_guns-lwingzz3.1-0 ; 
       add_more_guns-lwingzz4.1-0 ; 
       add_more_guns-null3.2-0 ROOT ; 
       add_more_guns-r-gun.1-0 ; 
       add_more_guns-rbooster.1-0 ; 
       add_more_guns-rfinzzz.1-0 ; 
       add_more_guns-rthrust.1-0 ; 
       add_more_guns-rwepatt.1-0 ; 
       add_more_guns-rwepemt.1-0 ; 
       add_more_guns-rwingzz0.1-0 ; 
       add_more_guns-rwingzz1.1-0 ; 
       add_more_guns-rwingzz2.1-0 ; 
       add_more_guns-rwingzz3.1-0 ; 
       add_more_guns-rwingzz4.1-0 ; 
       add_more_guns-SSal.1-0 ; 
       add_more_guns-SSar.1-0 ; 
       add_more_guns-SSf.1-0 ; 
       add_more_guns-SSr.1-0 ; 
       add_more_guns-SSr1.1-0 ; 
       add_more_guns-thrust0.1-0 ; 
       add_more_guns-trail.1-0 ; 
       add_more_guns-twepemt.1-0 ; 
       add_more_guns-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-add_more_guns.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       add_more_guns-t2d42.1-0 ; 
       add_more_guns-t2d43.1-0 ; 
       add_more_guns-t2d44.1-0 ; 
       add_more_guns-t2d45.1-0 ; 
       add_more_guns-t2d85.1-0 ; 
       add_more_guns-t2d86.1-0 ; 
       add_more_guns-t2d87.2-0 ; 
       add_more_guns-t2d88.2-0 ; 
       add_more_guns-zt2d1.1-0 ; 
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d20_1.2-0 ; 
       destroy-t2d21_1.2-0 ; 
       destroy-t2d22_1.2-0 ; 
       destroy-t2d23_1.3-0 ; 
       destroy-t2d24_1.3-0 ; 
       destroy-t2d26_1.3-0 ; 
       destroy-t2d27_1.3-0 ; 
       destroy-t2d28_1.3-0 ; 
       destroy-t2d29_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 4 110 ; 
       0 4 110 ; 
       1 5 110 ; 
       4 5 110 ; 
       5 31 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 5 110 ; 
       12 48 110 ; 
       13 6 110 ; 
       14 11 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 19 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 22 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 12 110 ; 
       24 5 110 ; 
       25 10 110 ; 
       26 5 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       32 4 110 ; 
       33 48 110 ; 
       34 6 110 ; 
       35 33 110 ; 
       36 5 110 ; 
       37 32 110 ; 
       38 5 110 ; 
       39 38 110 ; 
       40 39 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 13 110 ; 
       44 34 110 ; 
       45 4 110 ; 
       46 40 110 ; 
       47 28 110 ; 
       48 5 110 ; 
       49 5 110 ; 
       50 4 110 ; 
       51 4 110 ; 
       2 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 9 300 ; 
       0 19 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       7 38 300 ; 
       10 6 300 ; 
       12 15 300 ; 
       12 41 300 ; 
       12 0 300 ; 
       13 17 300 ; 
       13 3 300 ; 
       14 39 300 ; 
       15 20 300 ; 
       16 21 300 ; 
       17 10 300 ; 
       18 22 300 ; 
       19 23 300 ; 
       20 11 300 ; 
       21 24 300 ; 
       22 25 300 ; 
       28 42 300 ; 
       28 43 300 ; 
       28 45 300 ; 
       29 47 300 ; 
       30 46 300 ; 
       32 7 300 ; 
       33 16 300 ; 
       33 40 300 ; 
       33 1 300 ; 
       34 18 300 ; 
       34 4 300 ; 
       40 12 300 ; 
       40 44 300 ; 
       41 13 300 ; 
       42 14 300 ; 
       43 50 300 ; 
       44 49 300 ; 
       45 48 300 ; 
       46 2 300 ; 
       47 5 300 ; 
       2 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 44 400 ; 
       4 10 400 ; 
       7 16 400 ; 
       12 28 400 ; 
       13 14 400 ; 
       14 18 400 ; 
       15 20 400 ; 
       16 19 400 ; 
       17 24 400 ; 
       18 21 400 ; 
       19 22 400 ; 
       20 23 400 ; 
       21 25 400 ; 
       22 26 400 ; 
       29 33 400 ; 
       30 32 400 ; 
       33 29 400 ; 
       34 15 400 ; 
       41 34 400 ; 
       42 35 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       31 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 8 401 ; 
       4 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       12 37 401 ; 
       27 17 401 ; 
       28 27 401 ; 
       29 38 401 ; 
       30 40 401 ; 
       31 41 401 ; 
       32 42 401 ; 
       33 43 401 ; 
       34 11 401 ; 
       35 12 401 ; 
       36 13 401 ; 
       37 9 401 ; 
       40 30 401 ; 
       41 31 401 ; 
       43 36 401 ; 
       44 39 401 ; 
       45 2 401 ; 
       8 6 401 ; 
       9 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 90 0 0 MPRFLG 0 ; 
       0 SCHEM 60 -6 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 MPRFLG 0 ; 
       5 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 MPRFLG 0 ; 
       17 SCHEM 45 -8 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 45 -6 0 MPRFLG 0 ; 
       20 SCHEM 50 -8 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 50 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 5 -4 0 MPRFLG 0 ; 
       25 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 30 -10 0 MPRFLG 0 ; 
       30 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       34 SCHEM 20 -6 0 MPRFLG 0 ; 
       35 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 25 -4 0 MPRFLG 0 ; 
       39 SCHEM 25 -6 0 MPRFLG 0 ; 
       40 SCHEM 25 -8 0 MPRFLG 0 ; 
       41 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       42 SCHEM 25 -10 0 MPRFLG 0 ; 
       43 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       44 SCHEM 20 -8 0 MPRFLG 0 ; 
       45 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       47 SCHEM 35 -10 0 MPRFLG 0 ; 
       48 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       49 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 75 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54.81975 -8.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 69.81975 -8.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 92.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54.81975 -10.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 69.81975 -10.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 80 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 95 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 82 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
