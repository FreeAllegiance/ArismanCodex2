SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       adjust_nulls-null3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.14-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       adjust_nulls-mat46.1-0 ; 
       adjust_nulls-mat47.1-0 ; 
       adjust_nulls-mat63.1-0 ; 
       adjust_nulls-mat64.1-0 ; 
       adjust_nulls-mat65.1-0 ; 
       adjust_nulls-mat66.1-0 ; 
       adjust_nulls-mat69.1-0 ; 
       adjust_nulls-mat70.1-0 ; 
       adjust_nulls-mat71.1-0 ; 
       adjust_nulls-mat72.1-0 ; 
       destroy-default3_1.2-0 ; 
       destroy-default4_1.2-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat18_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat20_1.2-0 ; 
       destroy-mat21_1.2-0 ; 
       destroy-mat22_1.2-0 ; 
       destroy-mat23_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       adjust_nulls-afuselg.1-0 ; 
       adjust_nulls-cockpt.1-0 ; 
       adjust_nulls-ffuselg.1-0 ; 
       adjust_nulls-fgun.1-0 ; 
       adjust_nulls-fig02.1-0 ; 
       adjust_nulls-finzzz0.1-0 ; 
       adjust_nulls-finzzz1.1-0 ; 
       adjust_nulls-fwepemt.1-0 ; 
       adjust_nulls-fwepmnt.1-0 ; 
       adjust_nulls-l-gun.1-0 ; 
       adjust_nulls-landgr0.1-0 ; 
       adjust_nulls-lbooster.1-0 ; 
       adjust_nulls-lfinzzz.1-0 ; 
       adjust_nulls-LLf1.1-0 ; 
       adjust_nulls-LLf2.1-0 ; 
       adjust_nulls-LLf3.1-0 ; 
       adjust_nulls-LLl1.1-0 ; 
       adjust_nulls-LLl2.1-0 ; 
       adjust_nulls-LLl3.1-0 ; 
       adjust_nulls-LLr1.1-0 ; 
       adjust_nulls-LLr2.1-0 ; 
       adjust_nulls-LLr3.1-0 ; 
       adjust_nulls-lthrust.1-0 ; 
       adjust_nulls-lwepatt.1-0 ; 
       adjust_nulls-lwepemt.1-0 ; 
       adjust_nulls-lwingzz0.1-0 ; 
       adjust_nulls-lwingzz1.1-0 ; 
       adjust_nulls-lwingzz2.1-0 ; 
       adjust_nulls-lwingzz3.1-0 ; 
       adjust_nulls-lwingzz4.1-0 ; 
       adjust_nulls-missemt.1-0 ; 
       adjust_nulls-null3.1-0 ROOT ; 
       adjust_nulls-r-gun.1-0 ; 
       adjust_nulls-rbooster.1-0 ; 
       adjust_nulls-rfinzzz.1-0 ; 
       adjust_nulls-rthrust.1-0 ; 
       adjust_nulls-rwepatt.1-0 ; 
       adjust_nulls-rwepemt.1-0 ; 
       adjust_nulls-rwingzz0.1-0 ; 
       adjust_nulls-rwingzz1.1-0 ; 
       adjust_nulls-rwingzz2.1-0 ; 
       adjust_nulls-rwingzz3.1-0 ; 
       adjust_nulls-rwingzz4.1-0 ; 
       adjust_nulls-SSal.1-0 ; 
       adjust_nulls-SSar.1-0 ; 
       adjust_nulls-SSf.1-0 ; 
       adjust_nulls-SSr.1-0 ; 
       adjust_nulls-SSr1.1-0 ; 
       adjust_nulls-tgun.1-0 ; 
       adjust_nulls-thrust0.1-0 ; 
       adjust_nulls-trail.1-0 ; 
       adjust_nulls-twepemt.1-0 ; 
       adjust_nulls-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-adjust_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       adjust_nulls-t2d42.1-0 ; 
       adjust_nulls-t2d43.1-0 ; 
       adjust_nulls-t2d44.1-0 ; 
       adjust_nulls-t2d45.1-0 ; 
       adjust_nulls-t2d85.1-0 ; 
       adjust_nulls-t2d86.1-0 ; 
       adjust_nulls-t2d87.1-0 ; 
       adjust_nulls-t2d88.1-0 ; 
       adjust_nulls-zt2d1.1-0 ; 
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d20_1.2-0 ; 
       destroy-t2d21_1.2-0 ; 
       destroy-t2d22_1.2-0 ; 
       destroy-t2d23_1.3-0 ; 
       destroy-t2d24_1.3-0 ; 
       destroy-t2d26_1.3-0 ; 
       destroy-t2d27_1.3-0 ; 
       destroy-t2d28_1.3-0 ; 
       destroy-t2d29_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       30 2 110 ; 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 31 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 4 110 ; 
       11 49 110 ; 
       12 5 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 18 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 21 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 11 110 ; 
       23 4 110 ; 
       24 9 110 ; 
       25 4 110 ; 
       26 25 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       32 2 110 ; 
       33 49 110 ; 
       34 5 110 ; 
       35 33 110 ; 
       36 4 110 ; 
       37 32 110 ; 
       38 4 110 ; 
       39 38 110 ; 
       40 39 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 12 110 ; 
       44 34 110 ; 
       45 2 110 ; 
       46 40 110 ; 
       47 27 110 ; 
       48 2 110 ; 
       49 4 110 ; 
       50 4 110 ; 
       51 48 110 ; 
       52 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 19 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       3 9 300 ; 
       6 38 300 ; 
       9 6 300 ; 
       11 15 300 ; 
       11 41 300 ; 
       11 0 300 ; 
       12 17 300 ; 
       12 3 300 ; 
       13 39 300 ; 
       14 20 300 ; 
       15 21 300 ; 
       16 10 300 ; 
       17 22 300 ; 
       18 23 300 ; 
       19 11 300 ; 
       20 24 300 ; 
       21 25 300 ; 
       27 42 300 ; 
       27 43 300 ; 
       27 45 300 ; 
       28 47 300 ; 
       29 46 300 ; 
       32 7 300 ; 
       33 16 300 ; 
       33 40 300 ; 
       33 1 300 ; 
       34 18 300 ; 
       34 4 300 ; 
       40 12 300 ; 
       40 44 300 ; 
       41 13 300 ; 
       42 14 300 ; 
       43 50 300 ; 
       44 49 300 ; 
       45 48 300 ; 
       46 2 300 ; 
       47 5 300 ; 
       48 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 44 400 ; 
       2 10 400 ; 
       6 16 400 ; 
       11 28 400 ; 
       12 14 400 ; 
       13 18 400 ; 
       14 20 400 ; 
       15 19 400 ; 
       16 24 400 ; 
       17 21 400 ; 
       18 22 400 ; 
       19 23 400 ; 
       20 25 400 ; 
       21 26 400 ; 
       28 33 400 ; 
       29 32 400 ; 
       33 29 400 ; 
       34 15 400 ; 
       41 34 400 ; 
       42 35 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       31 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 8 401 ; 
       4 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       12 37 401 ; 
       27 17 401 ; 
       28 27 401 ; 
       29 38 401 ; 
       30 40 401 ; 
       31 41 401 ; 
       32 42 401 ; 
       33 43 401 ; 
       34 11 401 ; 
       35 12 401 ; 
       36 13 401 ; 
       37 9 401 ; 
       40 30 401 ; 
       41 31 401 ; 
       43 36 401 ; 
       44 39 401 ; 
       45 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       30 SCHEM 67.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 55 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 72.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -6 0 DISPLAY 3 2 MPRFLG 0 ; 
       3 SCHEM 65 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 MPRFLG 0 ; 
       7 SCHEM 65 -10 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 50 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 35 -10 0 MPRFLG 0 ; 
       16 SCHEM 40 -10 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 40 -8 0 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 45 -8 0 MPRFLG 0 ; 
       22 SCHEM 5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 0 -6 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -10 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 25 -12 0 MPRFLG 0 ; 
       29 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       31 SCHEM 36.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       32 SCHEM 62.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 7.5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       34 SCHEM 15 -8 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 62.5 -10 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 20 -6 0 MPRFLG 0 ; 
       39 SCHEM 20 -8 0 MPRFLG 0 ; 
       40 SCHEM 20 -10 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       42 SCHEM 20 -12 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       44 SCHEM 15 -10 0 MPRFLG 0 ; 
       45 SCHEM 52.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       47 SCHEM 30 -12 0 MPRFLG 0 ; 
       48 SCHEM 57.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       50 SCHEM 70 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 57.5 -10 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 60 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49.81975 -10.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 64.81975 -10.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 49.81975 -12.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64.81975 -12.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
