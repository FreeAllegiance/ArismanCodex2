SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_animation-null3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       check_animation-mat46.1-0 ; 
       check_animation-mat47.1-0 ; 
       check_animation-mat63.1-0 ; 
       check_animation-mat64.1-0 ; 
       check_animation-mat65.1-0 ; 
       check_animation-mat66.1-0 ; 
       check_animation-mat69.1-0 ; 
       check_animation-mat70.1-0 ; 
       destroy-default3_1.2-0 ; 
       destroy-default4_1.2-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat18_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat20_1.2-0 ; 
       destroy-mat21_1.2-0 ; 
       destroy-mat22_1.2-0 ; 
       destroy-mat23_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 50     
       check_animation-afuselg.1-0 ; 
       check_animation-cockpt.1-0 ; 
       check_animation-ffuselg.1-0 ; 
       check_animation-fig02.1-0 ; 
       check_animation-finzzz0.1-0 ; 
       check_animation-finzzz1.1-0 ; 
       check_animation-fwepemt.1-0 ; 
       check_animation-fwepmnt.1-0 ; 
       check_animation-l-gun.1-0 ; 
       check_animation-landgr0.1-0 ; 
       check_animation-lbooster.1-0 ; 
       check_animation-lfinzzz.1-0 ; 
       check_animation-LLf1.1-0 ; 
       check_animation-LLf2.1-0 ; 
       check_animation-LLf3.1-0 ; 
       check_animation-LLl1.1-0 ; 
       check_animation-LLl2.1-0 ; 
       check_animation-LLl3.1-0 ; 
       check_animation-LLr1.1-0 ; 
       check_animation-LLr2.1-0 ; 
       check_animation-LLr3.1-0 ; 
       check_animation-lthrust.1-0 ; 
       check_animation-lwepatt.1-0 ; 
       check_animation-lwepemt.1-0 ; 
       check_animation-lwingzz0.1-0 ; 
       check_animation-lwingzz1.1-0 ; 
       check_animation-lwingzz2.1-0 ; 
       check_animation-lwingzz3.1-0 ; 
       check_animation-lwingzz4.1-0 ; 
       check_animation-null3.2-0 ROOT ; 
       check_animation-r-gun.1-0 ; 
       check_animation-rbooster.1-0 ; 
       check_animation-rfinzzz.1-0 ; 
       check_animation-rthrust.1-0 ; 
       check_animation-rwepatt.1-0 ; 
       check_animation-rwepemt.1-0 ; 
       check_animation-rwingzz0.1-0 ; 
       check_animation-rwingzz1.1-0 ; 
       check_animation-rwingzz2.1-0 ; 
       check_animation-rwingzz3.1-0 ; 
       check_animation-rwingzz4.1-0 ; 
       check_animation-SSal.1-0 ; 
       check_animation-SSar.1-0 ; 
       check_animation-SSf.1-0 ; 
       check_animation-SSr.1-0 ; 
       check_animation-SSr1.1-0 ; 
       check_animation-thrust0.1-0 ; 
       check_animation-trail.1-0 ; 
       check_animation-twepemt.1-0 ; 
       check_animation-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-check_animation.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       check_animation-t2d42.1-0 ; 
       check_animation-t2d43.1-0 ; 
       check_animation-t2d44.1-0 ; 
       check_animation-t2d45.1-0 ; 
       check_animation-t2d85.1-0 ; 
       check_animation-t2d86.1-0 ; 
       check_animation-zt2d1.1-0 ; 
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d20_1.2-0 ; 
       destroy-t2d21_1.2-0 ; 
       destroy-t2d22_1.2-0 ; 
       destroy-t2d23_1.3-0 ; 
       destroy-t2d24_1.3-0 ; 
       destroy-t2d26_1.3-0 ; 
       destroy-t2d27_1.3-0 ; 
       destroy-t2d28_1.3-0 ; 
       destroy-t2d29_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 7 110 ; 
       0 2 110 ; 
       2 3 110 ; 
       3 29 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       30 2 110 ; 
       9 3 110 ; 
       10 46 110 ; 
       11 4 110 ; 
       12 9 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 17 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 20 110 ; 
       19 9 110 ; 
       20 9 110 ; 
       21 10 110 ; 
       22 3 110 ; 
       24 3 110 ; 
       25 24 110 ; 
       26 25 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       31 46 110 ; 
       32 4 110 ; 
       33 31 110 ; 
       34 3 110 ; 
       36 3 110 ; 
       37 36 110 ; 
       38 37 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 11 110 ; 
       42 32 110 ; 
       43 2 110 ; 
       44 38 110 ; 
       45 26 110 ; 
       46 3 110 ; 
       49 2 110 ; 
       48 2 110 ; 
       47 3 110 ; 
       1 3 110 ; 
       23 8 110 ; 
       35 30 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 17 300 ; 
       0 32 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       5 36 300 ; 
       8 6 300 ; 
       30 7 300 ; 
       10 13 300 ; 
       10 39 300 ; 
       10 0 300 ; 
       11 15 300 ; 
       11 3 300 ; 
       12 37 300 ; 
       13 18 300 ; 
       14 19 300 ; 
       15 8 300 ; 
       16 20 300 ; 
       17 21 300 ; 
       18 9 300 ; 
       19 22 300 ; 
       20 23 300 ; 
       26 40 300 ; 
       26 41 300 ; 
       26 43 300 ; 
       27 45 300 ; 
       28 44 300 ; 
       31 14 300 ; 
       31 38 300 ; 
       31 1 300 ; 
       32 16 300 ; 
       32 4 300 ; 
       38 10 300 ; 
       38 42 300 ; 
       39 11 300 ; 
       40 12 300 ; 
       41 48 300 ; 
       42 47 300 ; 
       43 46 300 ; 
       44 2 300 ; 
       45 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 42 400 ; 
       2 8 400 ; 
       5 14 400 ; 
       10 26 400 ; 
       11 12 400 ; 
       12 16 400 ; 
       13 18 400 ; 
       14 17 400 ; 
       15 22 400 ; 
       16 19 400 ; 
       17 20 400 ; 
       18 21 400 ; 
       19 23 400 ; 
       20 24 400 ; 
       27 31 400 ; 
       28 30 400 ; 
       31 27 400 ; 
       32 13 400 ; 
       39 32 400 ; 
       40 33 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 35 401 ; 
       25 15 401 ; 
       26 25 401 ; 
       27 36 401 ; 
       28 38 401 ; 
       29 39 401 ; 
       30 40 401 ; 
       31 41 401 ; 
       32 9 401 ; 
       33 10 401 ; 
       34 11 401 ; 
       35 7 401 ; 
       38 28 401 ; 
       39 29 401 ; 
       41 34 401 ; 
       42 37 401 ; 
       43 2 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       3 6 401 ; 
       4 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 60 -6 0 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 43.75 -4 0 DISPLAY 3 2 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 38.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 37.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 40 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 45 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 42.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 45 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 47.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 5 -4 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 30 -10 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       32 SCHEM 20 -6 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 25 -4 0 MPRFLG 0 ; 
       37 SCHEM 25 -6 0 MPRFLG 0 ; 
       38 SCHEM 25 -8 0 MPRFLG 0 ; 
       39 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       40 SCHEM 25 -10 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 20 -8 0 MPRFLG 0 ; 
       43 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       45 SCHEM 35 -10 0 MPRFLG 0 ; 
       46 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       49 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54.81975 -8.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 69.81975 -8.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54.81975 -10.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 69.81975 -10.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 82 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
