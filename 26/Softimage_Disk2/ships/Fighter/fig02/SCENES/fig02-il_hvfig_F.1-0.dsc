SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       il_hvfig_F-null3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       il_hvfig_F-light1_1.1-0 ROOT ; 
       il_hvfig_F-light2_1.1-0 ROOT ; 
       il_hvfig_F-light3_1.1-0 ROOT ; 
       il_hvfig_F-light4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       destroy-default3_1.1-0 ; 
       destroy-default4_1.1-0 ; 
       destroy-default5_1_1.1-0 ; 
       destroy-mat10_1.1-0 ; 
       destroy-mat11_1.1-0 ; 
       destroy-mat12_1.1-0 ; 
       destroy-mat13_1.1-0 ; 
       destroy-mat14_1.1-0 ; 
       destroy-mat15_1.1-0 ; 
       destroy-mat16_1.1-0 ; 
       destroy-mat18_1.1-0 ; 
       destroy-mat19_1.1-0 ; 
       destroy-mat20_1.1-0 ; 
       destroy-mat21_1.1-0 ; 
       destroy-mat22_1.1-0 ; 
       destroy-mat23_1.1-0 ; 
       destroy-mat24_1.1-0 ; 
       destroy-mat25_1.1-0 ; 
       destroy-mat26_1.1-0 ; 
       destroy-mat27_1.1-0 ; 
       destroy-mat28_1.1-0 ; 
       destroy-mat29_1.1-0 ; 
       destroy-mat30_1.1-0 ; 
       destroy-mat31_1.1-0 ; 
       destroy-mat32_1.1-0 ; 
       destroy-mat33_1.1-0 ; 
       destroy-mat34_1.1-0 ; 
       destroy-mat35_1.1-0 ; 
       destroy-mat36_1.1-0 ; 
       destroy-mat37_1.1-0 ; 
       destroy-mat38_1.1-0 ; 
       destroy-mat39_1.1-0 ; 
       destroy-mat40_1.1-0 ; 
       destroy-mat41_1.1-0 ; 
       destroy-mat42_1.1-0 ; 
       destroy-mat43_1.1-0 ; 
       destroy-mat5_1.1-0 ; 
       destroy-mat6_1.1-0 ; 
       done_lite-.5.1-0 ; 
       done_lite-mat11.1-0 ; 
       done_lite-mat44.1-0 ; 
       il_hvfig_F-mat46.1-0 ; 
       il_hvfig_F-mat47.1-0 ; 
       il_hvfig_F-mat63.1-0 ; 
       il_hvfig_F-mat64.1-0 ; 
       il_hvfig_F-mat65.1-0 ; 
       il_hvfig_F-mat66.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       il_hvfig_F-afuselg.1-0 ; 
       il_hvfig_F-ffuselg.1-0 ; 
       il_hvfig_F-fig02.1-0 ; 
       il_hvfig_F-finzzz0.1-0 ; 
       il_hvfig_F-finzzz1.1-0 ; 
       il_hvfig_F-fwepmnt.1-0 ; 
       il_hvfig_F-landgr0.1-0 ; 
       il_hvfig_F-lbooster.1-0 ; 
       il_hvfig_F-lfinzzz.1-0 ; 
       il_hvfig_F-LLf1.1-0 ; 
       il_hvfig_F-LLf2.1-0 ; 
       il_hvfig_F-LLf3.1-0 ; 
       il_hvfig_F-LLl1.1-0 ; 
       il_hvfig_F-LLl2.1-0 ; 
       il_hvfig_F-LLl3.1-0 ; 
       il_hvfig_F-LLr1.1-0 ; 
       il_hvfig_F-LLr2.1-0 ; 
       il_hvfig_F-LLr3.1-0 ; 
       il_hvfig_F-lthrust.1-0 ; 
       il_hvfig_F-lwepatt.1-0 ; 
       il_hvfig_F-lwingzz0.1-0 ; 
       il_hvfig_F-lwingzz1.1-0 ; 
       il_hvfig_F-lwingzz2.1-0 ; 
       il_hvfig_F-lwingzz3.1-0 ; 
       il_hvfig_F-lwingzz4.1-0 ; 
       il_hvfig_F-null3.1-0 ROOT ; 
       il_hvfig_F-rbooster.1-0 ; 
       il_hvfig_F-rfinzzz.1-0 ; 
       il_hvfig_F-rwepatt.1-0 ; 
       il_hvfig_F-rwingzz0.1-0 ; 
       il_hvfig_F-rwingzz1.1-0 ; 
       il_hvfig_F-rwingzz2.1-0 ; 
       il_hvfig_F-rwingzz3.1-0 ; 
       il_hvfig_F-rwingzz4.1-0 ; 
       il_hvfig_F-SSal.1-0 ; 
       il_hvfig_F-SSar.1-0 ; 
       il_hvfig_F-SSf.1-0 ; 
       il_hvfig_F-SSr.1-0 ; 
       il_hvfig_F-SSr1.1-0 ; 
       il_hvfig_F-thrust0.1-0 ; 
       il_hvfig_F-twepmnt.1-0 ; 
       il_hvfig_F-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Art fixes/soft/bugfix-fig/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-il_hvfig_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       destroy-2d1_1.1-0 ; 
       destroy-t2d1_1.1-0 ; 
       destroy-t2d10_1.1-0 ; 
       destroy-t2d11_1.1-0 ; 
       destroy-t2d12_1.1-0 ; 
       destroy-t2d13_1.1-0 ; 
       destroy-t2d14_1.1-0 ; 
       destroy-t2d15_1.1-0 ; 
       destroy-t2d2_1.1-0 ; 
       destroy-t2d20_1.1-0 ; 
       destroy-t2d21_1.1-0 ; 
       destroy-t2d22_1.1-0 ; 
       destroy-t2d23_1.1-0 ; 
       destroy-t2d24_1.1-0 ; 
       destroy-t2d26_1.1-0 ; 
       destroy-t2d27_1.1-0 ; 
       destroy-t2d28_1.1-0 ; 
       destroy-t2d29_1.1-0 ; 
       destroy-t2d3_1.1-0 ; 
       destroy-t2d30_1.1-0 ; 
       destroy-t2d31_1.1-0 ; 
       destroy-t2d32_1.1-0 ; 
       destroy-t2d33_1.1-0 ; 
       destroy-t2d34_1.1-0 ; 
       destroy-t2d35_1.1-0 ; 
       destroy-t2d36_1.1-0 ; 
       destroy-t2d37_1.1-0 ; 
       destroy-t2d38_1.1-0 ; 
       destroy-t2d39_1.1-0 ; 
       destroy-t2d4_1.1-0 ; 
       destroy-t2d40_1.1-0 ; 
       destroy-t2d5_1.1-0 ; 
       destroy-t2d6_1.1-0 ; 
       destroy-t2d7_1.1-0 ; 
       destroy-t2d8_1.1-0 ; 
       destroy-t2d9_1_1.1-0 ; 
       il_hvfig_F-t2d42.1-0 ; 
       il_hvfig_F-t2d43.1-0 ; 
       il_hvfig_F-t2d44.1-0 ; 
       il_hvfig_F-t2d45.1-0 ; 
       il_hvfig_F-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 25 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 1 110 ; 
       6 2 110 ; 
       7 39 110 ; 
       8 3 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 6 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 7 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       26 39 110 ; 
       27 3 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 8 110 ; 
       35 27 110 ; 
       36 1 110 ; 
       37 31 110 ; 
       38 22 110 ; 
       39 2 110 ; 
       40 1 110 ; 
       41 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       4 28 300 ; 
       7 5 300 ; 
       7 31 300 ; 
       7 41 300 ; 
       8 7 300 ; 
       8 44 300 ; 
       9 29 300 ; 
       10 10 300 ; 
       11 11 300 ; 
       12 0 300 ; 
       13 12 300 ; 
       14 13 300 ; 
       15 1 300 ; 
       16 14 300 ; 
       17 15 300 ; 
       22 32 300 ; 
       22 33 300 ; 
       22 35 300 ; 
       23 37 300 ; 
       24 36 300 ; 
       26 6 300 ; 
       26 30 300 ; 
       26 42 300 ; 
       27 8 300 ; 
       27 45 300 ; 
       31 2 300 ; 
       31 34 300 ; 
       32 3 300 ; 
       33 4 300 ; 
       34 40 300 ; 
       35 39 300 ; 
       36 38 300 ; 
       37 43 300 ; 
       38 46 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 35 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       7 19 400 ; 
       8 5 400 ; 
       9 9 400 ; 
       10 11 400 ; 
       11 10 400 ; 
       12 15 400 ; 
       13 12 400 ; 
       14 13 400 ; 
       15 14 400 ; 
       16 16 400 ; 
       17 17 400 ; 
       23 24 400 ; 
       24 23 400 ; 
       26 20 400 ; 
       27 6 400 ; 
       32 25 400 ; 
       33 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       25 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 28 401 ; 
       17 8 401 ; 
       18 18 401 ; 
       19 29 401 ; 
       20 31 401 ; 
       21 32 401 ; 
       22 33 401 ; 
       23 34 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 4 401 ; 
       27 0 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       33 27 401 ; 
       34 30 401 ; 
       35 38 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       44 40 401 ; 
       45 39 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 88.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 70 -6 0 MPRFLG 0 ; 
       5 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 65 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 93.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 95 -8 0 MPRFLG 0 ; 
       12 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 100 -8 0 MPRFLG 0 ; 
       15 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 105 -8 0 MPRFLG 0 ; 
       18 SCHEM 65 -8 0 MPRFLG 0 ; 
       19 SCHEM 60 -4 0 MPRFLG 0 ; 
       20 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 87.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 85 -10 0 MPRFLG 0 ; 
       24 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 88.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 67.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       27 SCHEM 75 -6 0 MPRFLG 0 ; 
       28 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 80 -4 0 MPRFLG 0 ; 
       30 SCHEM 80 -6 0 MPRFLG 0 ; 
       31 SCHEM 80 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       33 SCHEM 80 -10 0 MPRFLG 0 ; 
       34 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 75 -8 0 MPRFLG 0 ; 
       36 SCHEM 110 -6 0 MPRFLG 0 ; 
       37 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 90 -10 0 MPRFLG 0 ; 
       39 SCHEM 66.25 -4 0 MPRFLG 0 ; 
       40 SCHEM 115 -6 0 MPRFLG 0 ; 
       41 SCHEM 117.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 101.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 106.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 96.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 99 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 104 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 96.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 86.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 84 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 71.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 89 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 119 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 96.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 94 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 91.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 96.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 99 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 106.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 101.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 101.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 104 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 119 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 111.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 119 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
