SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       new_nulls-fig02.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.21-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       destroy-default3_1.2-0 ; 
       destroy-default4_1.2-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat18_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat20_1.2-0 ; 
       destroy-mat21_1.2-0 ; 
       destroy-mat22_1.2-0 ; 
       destroy-mat23_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
       new_nulls-mat46.1-0 ; 
       new_nulls-mat47.1-0 ; 
       new_nulls-mat63.1-0 ; 
       new_nulls-mat64.1-0 ; 
       new_nulls-mat65.1-0 ; 
       new_nulls-mat66.1-0 ; 
       new_nulls-mat69.1-0 ; 
       new_nulls-mat70.1-0 ; 
       new_nulls-mat71.1-0 ; 
       new_nulls-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 52     
       new_nulls-afuselg.1-0 ; 
       new_nulls-cockpt.1-0 ; 
       new_nulls-ffuselg.1-0 ; 
       new_nulls-fgun.1-0 ; 
       new_nulls-fig02.3-0 ROOT ; 
       new_nulls-finzzz0.1-0 ; 
       new_nulls-finzzz1.1-0 ; 
       new_nulls-fwepemt.1-0 ; 
       new_nulls-fwepmnt.1-0 ; 
       new_nulls-l-gun.1-0 ; 
       new_nulls-landgr0.1-0 ; 
       new_nulls-lbooster.1-0 ; 
       new_nulls-lfinzzz.1-0 ; 
       new_nulls-LLf1.1-0 ; 
       new_nulls-LLf2.1-0 ; 
       new_nulls-LLf3.1-0 ; 
       new_nulls-LLl1.1-0 ; 
       new_nulls-LLl2.1-0 ; 
       new_nulls-LLl3.1-0 ; 
       new_nulls-LLr1.1-0 ; 
       new_nulls-LLr2.1-0 ; 
       new_nulls-LLr3.1-0 ; 
       new_nulls-lthrust.1-0 ; 
       new_nulls-lwepatt.1-0 ; 
       new_nulls-lwepemt.1-0 ; 
       new_nulls-lwingzz0.1-0 ; 
       new_nulls-lwingzz1.1-0 ; 
       new_nulls-lwingzz2.1-0 ; 
       new_nulls-lwingzz3.1-0 ; 
       new_nulls-lwingzz4.1-0 ; 
       new_nulls-missemt.1-0 ; 
       new_nulls-r-gun.1-0 ; 
       new_nulls-rbooster.1-0 ; 
       new_nulls-rfinzzz.1-0 ; 
       new_nulls-rthrust.1-0 ; 
       new_nulls-rwepatt.1-0 ; 
       new_nulls-rwepemt.1-0 ; 
       new_nulls-rwingzz0.1-0 ; 
       new_nulls-rwingzz1.1-0 ; 
       new_nulls-rwingzz2.1-0 ; 
       new_nulls-rwingzz3.1-0 ; 
       new_nulls-rwingzz4.1-0 ; 
       new_nulls-SSal.1-0 ; 
       new_nulls-SSar.1-0 ; 
       new_nulls-SSf.1-0 ; 
       new_nulls-SSr.1-0 ; 
       new_nulls-SSr1.1-0 ; 
       new_nulls-tgun.1-0 ; 
       new_nulls-thrust0.1-0 ; 
       new_nulls-trail.1-0 ; 
       new_nulls-twepemt.1-0 ; 
       new_nulls-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-new_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d20_1.2-0 ; 
       destroy-t2d21_1.2-0 ; 
       destroy-t2d22_1.2-0 ; 
       destroy-t2d23_1.3-0 ; 
       destroy-t2d24_1.3-0 ; 
       destroy-t2d26_1.3-0 ; 
       destroy-t2d27_1.3-0 ; 
       destroy-t2d28_1.3-0 ; 
       destroy-t2d29_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
       new_nulls-t2d42.1-0 ; 
       new_nulls-t2d43.1-0 ; 
       new_nulls-t2d44.1-0 ; 
       new_nulls-t2d45.1-0 ; 
       new_nulls-t2d85.1-0 ; 
       new_nulls-t2d86.1-0 ; 
       new_nulls-t2d87.1-0 ; 
       new_nulls-t2d88.1-0 ; 
       new_nulls-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 4 110 ; 
       11 48 110 ; 
       12 5 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 18 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 21 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 11 110 ; 
       23 4 110 ; 
       24 9 110 ; 
       25 4 110 ; 
       26 25 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 48 110 ; 
       33 5 110 ; 
       34 32 110 ; 
       35 4 110 ; 
       36 31 110 ; 
       37 4 110 ; 
       38 37 110 ; 
       39 38 110 ; 
       40 39 110 ; 
       41 39 110 ; 
       42 12 110 ; 
       43 33 110 ; 
       44 2 110 ; 
       45 39 110 ; 
       46 27 110 ; 
       47 2 110 ; 
       48 4 110 ; 
       49 4 110 ; 
       50 47 110 ; 
       51 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       3 50 300 ; 
       6 28 300 ; 
       9 47 300 ; 
       11 5 300 ; 
       11 31 300 ; 
       11 41 300 ; 
       12 7 300 ; 
       12 44 300 ; 
       13 29 300 ; 
       14 10 300 ; 
       15 11 300 ; 
       16 0 300 ; 
       17 12 300 ; 
       18 13 300 ; 
       19 1 300 ; 
       20 14 300 ; 
       21 15 300 ; 
       27 32 300 ; 
       27 33 300 ; 
       27 35 300 ; 
       28 37 300 ; 
       29 36 300 ; 
       31 48 300 ; 
       32 6 300 ; 
       32 30 300 ; 
       32 42 300 ; 
       33 8 300 ; 
       33 45 300 ; 
       39 2 300 ; 
       39 34 300 ; 
       40 3 300 ; 
       41 4 300 ; 
       42 40 300 ; 
       43 39 300 ; 
       44 38 300 ; 
       45 43 300 ; 
       46 46 300 ; 
       47 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 35 400 ; 
       2 1 400 ; 
       6 7 400 ; 
       11 19 400 ; 
       12 5 400 ; 
       13 9 400 ; 
       14 11 400 ; 
       15 10 400 ; 
       16 15 400 ; 
       17 12 400 ; 
       18 13 400 ; 
       19 14 400 ; 
       20 16 400 ; 
       21 17 400 ; 
       28 24 400 ; 
       29 23 400 ; 
       32 20 400 ; 
       33 6 400 ; 
       40 25 400 ; 
       41 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 28 401 ; 
       17 8 401 ; 
       18 18 401 ; 
       19 29 401 ; 
       20 31 401 ; 
       21 32 401 ; 
       22 33 401 ; 
       23 34 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 4 401 ; 
       27 0 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       33 27 401 ; 
       34 30 401 ; 
       35 38 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       44 44 401 ; 
       45 39 401 ; 
       47 40 401 ; 
       48 41 401 ; 
       49 42 401 ; 
       50 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 60 -4 0 MPRFLG 0 ; 
       1 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 70 -4 0 MPRFLG 0 ; 
       4 SCHEM 41.25 0 0 SRT 0.1528094 0.1528094 0.1528094 6.264869e-005 -1.569883 0.002353534 0 0 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 MPRFLG 0 ; 
       7 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55 -4 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 MPRFLG 0 ; 
       16 SCHEM 45 -6 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 MPRFLG 0 ; 
       19 SCHEM 50 -6 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 50 -4 0 MPRFLG 0 ; 
       22 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -2 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 30 -8 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 12.5 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       33 SCHEM 20 -4 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       36 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 25 -2 0 MPRFLG 0 ; 
       38 SCHEM 25 -4 0 MPRFLG 0 ; 
       39 SCHEM 25 -6 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 25 -8 0 MPRFLG 0 ; 
       42 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 20 -6 0 MPRFLG 0 ; 
       44 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       45 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       46 SCHEM 35 -8 0 MPRFLG 0 ; 
       47 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       48 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       49 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 65 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 54.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 69.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 74 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 54.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 69.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
