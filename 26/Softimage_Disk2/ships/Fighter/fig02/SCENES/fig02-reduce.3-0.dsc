SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       reduce-null3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.4-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       destroy-default3_1.2-0 ; 
       destroy-default4_1.2-0 ; 
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat18_1.2-0 ; 
       destroy-mat19_1.2-0 ; 
       destroy-mat20_1.2-0 ; 
       destroy-mat21_1.2-0 ; 
       destroy-mat22_1.2-0 ; 
       destroy-mat23_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat37_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
       reduce-mat46.1-0 ; 
       reduce-mat47.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       reduce-afuselg.1-0 ; 
       reduce-ffuselg.1-0 ; 
       reduce-fig02.1-0 ; 
       reduce-finzzz0.1-0 ; 
       reduce-finzzz1.1-0 ; 
       reduce-fwepmnt.1-0 ; 
       reduce-l-gun.1-0 ; 
       reduce-l-gun1.1-0 ; 
       reduce-landgr0.1-0 ; 
       reduce-lbooster.1-0 ; 
       reduce-lfinzzz.1-0 ; 
       reduce-LLf1.1-0 ; 
       reduce-LLf2.1-0 ; 
       reduce-LLf3.1-0 ; 
       reduce-LLl1.1-0 ; 
       reduce-LLl2.1-0 ; 
       reduce-LLl3.1-0 ; 
       reduce-LLr1.1-0 ; 
       reduce-LLr2.1-0 ; 
       reduce-LLr3.1-0 ; 
       reduce-lthrust.1-0 ; 
       reduce-lwepatt.1-0 ; 
       reduce-lwingzz0.1-0 ; 
       reduce-lwingzz1.1-0 ; 
       reduce-lwingzz2.1-0 ; 
       reduce-lwingzz3.1-0 ; 
       reduce-lwingzz4.1-0 ; 
       reduce-null3.3-0 ROOT ; 
       reduce-rbooster.1-0 ; 
       reduce-rfinzzz.1-0 ; 
       reduce-rthrust.1-0 ; 
       reduce-rwepatt.1-0 ; 
       reduce-rwingzz0.1-0 ; 
       reduce-rwingzz1.1-0 ; 
       reduce-rwingzz2.1-0 ; 
       reduce-rwingzz3.1-0 ; 
       reduce-rwingzz4.1-0 ; 
       reduce-SSal.1-0 ; 
       reduce-SSar.1-0 ; 
       reduce-SSf.1-0 ; 
       reduce-SSr.1-0 ; 
       reduce-SSr1.1-0 ; 
       reduce-thrust0.1-0 ; 
       reduce-twepmnt.1-0 ; 
       reduce-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-reduce.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d20_1.2-0 ; 
       destroy-t2d21_1.2-0 ; 
       destroy-t2d22_1.2-0 ; 
       destroy-t2d23_1.3-0 ; 
       destroy-t2d24_1.3-0 ; 
       destroy-t2d26_1.3-0 ; 
       destroy-t2d27_1.3-0 ; 
       destroy-t2d28_1.3-0 ; 
       destroy-t2d29_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d34_1.2-0 ; 
       destroy-t2d35_1.2-0 ; 
       destroy-t2d36_1.2-0 ; 
       destroy-t2d37_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
       reduce-t2d42.1-0 ; 
       reduce-t2d43.1-0 ; 
       reduce-t2d44.1-0 ; 
       reduce-t2d45.1-0 ; 
       reduce-t2d85.2-0 ; 
       reduce-t2d86.2-0 ; 
       reduce-zt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 27 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 2 110 ; 
       9 42 110 ; 
       10 3 110 ; 
       11 8 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 16 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 19 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 9 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 22 110 ; 
       24 23 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       28 42 110 ; 
       29 3 110 ; 
       30 28 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 32 110 ; 
       34 33 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 10 110 ; 
       38 29 110 ; 
       39 1 110 ; 
       40 34 110 ; 
       41 24 110 ; 
       42 2 110 ; 
       43 1 110 ; 
       44 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       4 28 300 ; 
       6 47 300 ; 
       7 48 300 ; 
       9 5 300 ; 
       9 31 300 ; 
       9 41 300 ; 
       10 7 300 ; 
       10 44 300 ; 
       11 29 300 ; 
       12 10 300 ; 
       13 11 300 ; 
       14 0 300 ; 
       15 12 300 ; 
       16 13 300 ; 
       17 1 300 ; 
       18 14 300 ; 
       19 15 300 ; 
       24 32 300 ; 
       24 33 300 ; 
       24 35 300 ; 
       25 37 300 ; 
       26 36 300 ; 
       28 6 300 ; 
       28 30 300 ; 
       28 42 300 ; 
       29 8 300 ; 
       29 45 300 ; 
       34 2 300 ; 
       34 34 300 ; 
       35 3 300 ; 
       36 4 300 ; 
       37 40 300 ; 
       38 39 300 ; 
       39 38 300 ; 
       40 43 300 ; 
       41 46 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 35 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       9 19 400 ; 
       10 5 400 ; 
       11 9 400 ; 
       12 11 400 ; 
       13 10 400 ; 
       14 15 400 ; 
       15 12 400 ; 
       16 13 400 ; 
       17 14 400 ; 
       18 16 400 ; 
       19 17 400 ; 
       25 24 400 ; 
       26 23 400 ; 
       28 20 400 ; 
       29 6 400 ; 
       35 25 400 ; 
       36 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       27 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 28 401 ; 
       17 8 401 ; 
       18 18 401 ; 
       19 29 401 ; 
       20 31 401 ; 
       21 32 401 ; 
       22 33 401 ; 
       23 34 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 4 401 ; 
       27 0 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       33 27 401 ; 
       34 30 401 ; 
       35 38 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       44 42 401 ; 
       45 39 401 ; 
       47 40 401 ; 
       48 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 55 -8 0 MPRFLG 0 ; 
       1 SCHEM 55 -6 0 MPRFLG 0 ; 
       2 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 MPRFLG 0 ; 
       5 SCHEM 50 -8 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 35 -10 0 MPRFLG 0 ; 
       14 SCHEM 40 -10 0 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 MPRFLG 0 ; 
       17 SCHEM 45 -10 0 MPRFLG 0 ; 
       18 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 45 -8 0 MPRFLG 0 ; 
       20 SCHEM 5 -10 0 MPRFLG 0 ; 
       21 SCHEM 0 -6 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 25 -12 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       27 SCHEM 31.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       29 SCHEM 15 -8 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 20 -6 0 MPRFLG 0 ; 
       33 SCHEM 20 -8 0 MPRFLG 0 ; 
       34 SCHEM 20 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 20 -12 0 MPRFLG 0 ; 
       37 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 15 -10 0 MPRFLG 0 ; 
       39 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       41 SCHEM 30 -12 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       43 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       44 SCHEM 60 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 38.96836 -11.29673 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44.0042 -11.76257 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.46836 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.5042 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 47.31975 -10.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 62.31975 -10.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.46836 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44.0042 -11.76257 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 38.96836 -11.29673 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 46.5042 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 47.31975 -12.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 62.31975 -12.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
