SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.36-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.36-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       destroy-default5_1_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       static-mat46.4-0 ; 
       static-mat47.4-0 ; 
       static-mat64.4-0 ; 
       static-mat65.4-0 ; 
       static-mat69.4-0 ; 
       static-mat70.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       static-afuselg.1-0 ; 
       static-ffuselg.1-0 ; 
       static-fig02.6-0 ROOT ; 
       static-finzzz0.1-0 ; 
       static-finzzz1.1-0 ; 
       static-l-gun.1-0 ; 
       static-lbooster.1-0 ; 
       static-lfinzzz.1-0 ; 
       static-lwingzz0.1-0 ; 
       static-lwingzz1.1-0 ; 
       static-lwingzz2.1-0 ; 
       static-r-gun.1-0 ; 
       static-rbooster.1-0 ; 
       static-rfinzzz.1-0 ; 
       static-rwingzz0.1-0 ; 
       static-rwingzz1.1-0 ; 
       static-rwingzz2.1-0 ; 
       static-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig02/PICTURES/fig02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02-static.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       destroy-2d1_1.3-0 ; 
       destroy-t2d1_1.3-0 ; 
       destroy-t2d10_1.3-0 ; 
       destroy-t2d11_1.3-0 ; 
       destroy-t2d12_1.3-0 ; 
       destroy-t2d13_1.2-0 ; 
       destroy-t2d14_1.2-0 ; 
       destroy-t2d15_1.2-0 ; 
       destroy-t2d2_1.3-0 ; 
       destroy-t2d3_1.3-0 ; 
       destroy-t2d30_1.2-0 ; 
       destroy-t2d31_1.2-0 ; 
       destroy-t2d32_1.2-0 ; 
       destroy-t2d33_1.2-0 ; 
       destroy-t2d38_1.2-0 ; 
       destroy-t2d39_1.2-0 ; 
       destroy-t2d4_1.3-0 ; 
       destroy-t2d40_1.2-0 ; 
       destroy-t2d5_1.3-0 ; 
       destroy-t2d6_1.3-0 ; 
       destroy-t2d7_1.3-0 ; 
       destroy-t2d8_1.3-0 ; 
       destroy-t2d9_1_1.3-0 ; 
       static-t2d42.4-0 ; 
       static-t2d43.4-0 ; 
       static-t2d44.4-0 ; 
       static-t2d45.4-0 ; 
       static-t2d85.4-0 ; 
       static-t2d86.4-0 ; 
       static-zt2d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 1 110 ; 
       6 17 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 1 110 ; 
       12 17 110 ; 
       13 3 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       17 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       4 18 300 ; 
       5 29 300 ; 
       6 1 300 ; 
       6 20 300 ; 
       6 25 300 ; 
       7 3 300 ; 
       7 27 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       10 24 300 ; 
       11 30 300 ; 
       12 2 300 ; 
       12 19 300 ; 
       12 26 300 ; 
       13 4 300 ; 
       13 28 300 ; 
       16 0 300 ; 
       16 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       6 10 400 ; 
       7 5 400 ; 
       12 11 400 ; 
       13 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 16 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 20 401 ; 
       13 21 401 ; 
       14 2 401 ; 
       15 3 401 ; 
       16 4 401 ; 
       17 0 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       22 14 401 ; 
       23 17 401 ; 
       24 25 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 29 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -4 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 36.25 0 0 SRT 1 1 1 6.264869e-005 -1.569883 0.002353534 0 0 0.9161164 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       13 SCHEM 20 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 MPRFLG 0 ; 
       17 SCHEM 11.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 54.81975 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54.81975 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
