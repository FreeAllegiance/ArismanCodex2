SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 7     
       fig03-fig03.52-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.63-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.63-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.63-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.63-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.63-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 61     
       BOTH-mat62.1-0 ; 
       BOTH-mat63.1-0 ; 
       BOTH-mat64.1-0 ; 
       BOTH-mat65.1-0 ; 
       BOTH-mat66.1-0 ; 
       BOTH-mat67.1-0 ; 
       BOTH-mat68.1-0 ; 
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat59_1.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat60_1.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_flap-mat61_1.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat1_1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat12_1.1-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat13_1.1-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat14_1.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat2_1.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat33_1.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat34_1.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat57_1.1-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat58_1.1-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat6_1.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat7_1.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat8_1.1-0 ; 
       reduce_wing-mat9.1-0 ; 
       reduce_wing-mat9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       fig03-alarmour.1-0 ; 
       fig03-alarmour_1.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-alarmour1_1.1-0 ; 
       fig03-armour.1-0 ; 
       fig03-armour_1.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-farmour_1.1-0 ; 
       fig03-fig03.54-0 ROOT ; 
       fig03-fig03_1.1-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour_1.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-frarmour5_1.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-ftfuselg_1.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-fuselg_1.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-lwingzz_1.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-rtwepemt.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-rwingzz_1.1-0 ; 
       fig03-smoke.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
       fig03-wingzz_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d47_1.1-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d48_1.1-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_flap-t2d49_1.1-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d1_1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d12_1.1-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d13_1.1-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d14_1.1-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d2_1.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d33_1.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d34_1.1-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d45_1.1-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d46_1.1-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d6_1.1-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d7_1.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d8_1.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
       reduce_wing-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 4 110 ; 
       4 9 110 ; 
       6 17 110 ; 
       7 9 110 ; 
       11 7 110 ; 
       13 7 110 ; 
       15 17 110 ; 
       17 9 110 ; 
       19 0 110 ; 
       20 52 110 ; 
       21 52 110 ; 
       22 21 110 ; 
       23 20 110 ; 
       24 53 110 ; 
       26 19 110 ; 
       27 51 110 ; 
       28 52 110 ; 
       29 52 110 ; 
       30 29 110 ; 
       31 28 110 ; 
       32 53 110 ; 
       34 9 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 17 110 ; 
       38 17 110 ; 
       39 24 110 ; 
       40 32 110 ; 
       41 0 110 ; 
       42 0 110 ; 
       43 0 110 ; 
       44 2 110 ; 
       45 2 110 ; 
       46 2 110 ; 
       47 2 110 ; 
       48 0 110 ; 
       49 17 110 ; 
       50 17 110 ; 
       51 2 110 ; 
       52 9 110 ; 
       53 9 110 ; 
       1 5 110 ; 
       3 5 110 ; 
       5 10 110 ; 
       8 10 110 ; 
       12 8 110 ; 
       14 8 110 ; 
       16 18 110 ; 
       18 10 110 ; 
       25 54 110 ; 
       33 54 110 ; 
       54 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 53 300 ; 
       0 55 300 ; 
       0 35 300 ; 
       2 7 300 ; 
       2 9 300 ; 
       2 11 300 ; 
       11 15 300 ; 
       11 17 300 ; 
       13 49 300 ; 
       13 51 300 ; 
       15 19 300 ; 
       17 57 300 ; 
       17 59 300 ; 
       17 33 300 ; 
       19 30 300 ; 
       19 31 300 ; 
       19 32 300 ; 
       20 24 300 ; 
       20 37 300 ; 
       20 38 300 ; 
       21 23 300 ; 
       21 39 300 ; 
       24 13 300 ; 
       28 21 300 ; 
       28 41 300 ; 
       29 22 300 ; 
       29 40 300 ; 
       32 25 300 ; 
       35 45 300 ; 
       36 44 300 ; 
       37 43 300 ; 
       38 42 300 ; 
       39 46 300 ; 
       40 48 300 ; 
       41 3 300 ; 
       42 4 300 ; 
       43 5 300 ; 
       44 47 300 ; 
       45 0 300 ; 
       46 1 300 ; 
       47 2 300 ; 
       48 6 300 ; 
       51 27 300 ; 
       51 28 300 ; 
       51 29 300 ; 
       1 54 300 ; 
       1 56 300 ; 
       1 36 300 ; 
       3 8 300 ; 
       3 10 300 ; 
       3 12 300 ; 
       12 16 300 ; 
       12 18 300 ; 
       14 50 300 ; 
       14 52 300 ; 
       16 20 300 ; 
       18 58 300 ; 
       18 60 300 ; 
       18 34 300 ; 
       25 14 300 ; 
       33 26 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       9 2 401 ; 
       11 4 401 ; 
       13 6 401 ; 
       15 8 401 ; 
       17 10 401 ; 
       19 12 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       35 28 401 ; 
       37 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
       49 35 401 ; 
       51 37 401 ; 
       53 39 401 ; 
       55 41 401 ; 
       57 43 401 ; 
       59 45 401 ; 
       8 1 401 ; 
       10 3 401 ; 
       12 5 401 ; 
       14 7 401 ; 
       16 9 401 ; 
       18 11 401 ; 
       20 13 401 ; 
       26 19 401 ; 
       34 27 401 ; 
       36 29 401 ; 
       50 36 401 ; 
       52 38 401 ; 
       54 40 401 ; 
       56 42 401 ; 
       58 44 401 ; 
       60 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 MPRFLG 0 ; 
       17 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       19 SCHEM 10 -6 0 MPRFLG 0 ; 
       20 SCHEM 60 -4 0 MPRFLG 0 ; 
       21 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 5 -4 0 MPRFLG 0 ; 
       26 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 55 -4 0 MPRFLG 0 ; 
       29 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 40 -4 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 35 -4 0 MPRFLG 0 ; 
       39 SCHEM 5 -6 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 12.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 15 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 17.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 27.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       45 SCHEM 32.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       46 SCHEM 30 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       47 SCHEM 25 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       48 SCHEM 20 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       49 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       52 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       53 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       1 SCHEM 89.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 92 -4 0 MPRFLG 0 ; 
       5 SCHEM 90.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 100.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 93.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 99.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 102 -4 0 MPRFLG 0 ; 
       16 SCHEM 94.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 94.5 -2 0 MPRFLG 0 ; 
       25 SCHEM 84.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 87 -4 0 MPRFLG 0 ; 
       54 SCHEM 85.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 83.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 98.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 93.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 86 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 96 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 101 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 88.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 96 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 96 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 83.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 98.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 93.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 86 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 96 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 88.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 101 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 88.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 88.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 96 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 96 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 135.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
