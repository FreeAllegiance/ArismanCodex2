SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.43-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.45-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.45-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.45-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.45-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_Logo-light3.2-0 ROOT ; 
       add_Logo-light4.2-0 ROOT ; 
       add_Logo-light5.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       add_Logo-mat62.1-0 ; 
       add_Logo-mat63.1-0 ; 
       add_Logo-mat64.1-0 ; 
       add_Logo-mat65.1-0 ; 
       add_Logo-mat66.1-0 ; 
       add_Logo-mat67.1-0 ; 
       add_Logo-mat68.1-0 ; 
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat21.1-0 ; 
       reduce_wing-mat22.1-0 ; 
       reduce_wing-mat24.1-0 ; 
       reduce_wing-mat25.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat55.1-0 ; 
       reduce_wing-mat56.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour0.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.39-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt1_1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwepemt2_1.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-rtwepemt.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt1_1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwepemt2_1.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-add_Logo.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d21.1-0 ; 
       reduce_wing-t2d22.1-0 ; 
       reduce_wing-t2d24.1-0 ; 
       reduce_wing-t2d25.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d43.1-0 ; 
       reduce_wing-t2d44.1-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 5 110 ; 
       3 9 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 9 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 0 110 ; 
       18 51 110 ; 
       19 51 110 ; 
       20 18 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 18 110 ; 
       24 52 110 ; 
       25 17 110 ; 
       26 50 110 ; 
       27 51 110 ; 
       28 51 110 ; 
       29 27 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 27 110 ; 
       33 52 110 ; 
       34 9 110 ; 
       35 9 110 ; 
       36 9 110 ; 
       37 9 110 ; 
       38 24 110 ; 
       39 33 110 ; 
       40 0 110 ; 
       41 0 110 ; 
       42 0 110 ; 
       43 1 110 ; 
       44 1 110 ; 
       45 1 110 ; 
       46 1 110 ; 
       47 0 110 ; 
       48 9 110 ; 
       49 9 110 ; 
       50 1 110 ; 
       51 5 110 ; 
       52 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 47 300 ; 
       0 48 300 ; 
       0 30 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       8 13 300 ; 
       9 49 300 ; 
       9 50 300 ; 
       9 29 300 ; 
       11 43 300 ; 
       12 22 300 ; 
       13 21 300 ; 
       14 44 300 ; 
       15 19 300 ; 
       16 20 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       17 28 300 ; 
       18 17 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       19 16 300 ; 
       19 33 300 ; 
       24 10 300 ; 
       27 14 300 ; 
       27 35 300 ; 
       28 15 300 ; 
       28 34 300 ; 
       33 18 300 ; 
       34 39 300 ; 
       35 38 300 ; 
       36 37 300 ; 
       37 36 300 ; 
       38 40 300 ; 
       39 42 300 ; 
       40 3 300 ; 
       41 4 300 ; 
       42 5 300 ; 
       43 41 300 ; 
       44 0 300 ; 
       45 1 300 ; 
       46 2 300 ; 
       47 6 300 ; 
       50 23 300 ; 
       50 24 300 ; 
       50 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       43 29 401 ; 
       44 30 401 ; 
       45 31 401 ; 
       46 32 401 ; 
       47 33 401 ; 
       48 34 401 ; 
       49 35 401 ; 
       50 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 160 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 162.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 165 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 MPRFLG 0 ; 
       2 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 153.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 78.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 151.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 156.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 75 -6 0 MPRFLG 0 ; 
       9 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 95 -6 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 95 -8 0 MPRFLG 0 ; 
       14 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 100 -8 0 MPRFLG 0 ; 
       16 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       18 SCHEM 132.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 143.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 127.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 140 -8 0 MPRFLG 0 ; 
       23 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       25 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 111.25 -6 0 MPRFLG 0 ; 
       28 SCHEM 121.25 -6 0 MPRFLG 0 ; 
       29 SCHEM 107.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       34 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 70 -6 0 MPRFLG 0 ; 
       36 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 65 -6 0 MPRFLG 0 ; 
       38 SCHEM 0 -8 0 MPRFLG 0 ; 
       39 SCHEM 5 -8 0 MPRFLG 0 ; 
       40 SCHEM 20 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 22.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 25 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 50 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 55 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       45 SCHEM 52.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       46 SCHEM 47.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       47 SCHEM 27.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       51 SCHEM 127.5 -4 0 MPRFLG 0 ; 
       52 SCHEM 3.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 145 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 159 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
