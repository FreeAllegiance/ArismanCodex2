SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.16-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       reduce_flap-light1.10-0 ROOT ; 
       reduce_flap-light2.10-0 ROOT ; 
       reduce_flap-light3.10-0 ROOT ; 
       reduce_flap-light4.10-0 ROOT ; 
       reduce_flap-light5.10-0 ROOT ; 
       reduce_flap-light6.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.1-0 ; 
       reduce_wing-mat13.1-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat21.1-0 ; 
       reduce_wing-mat22.1-0 ; 
       reduce_wing-mat24.1-0 ; 
       reduce_wing-mat25.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat49.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat55.1-0 ; 
       reduce_wing-mat56.1-0 ; 
       reduce_wing-mat57.1-0 ; 
       reduce_wing-mat58.1-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour0.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.15-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-tlwepemt.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-trwepemt.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-animation.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       reduce_flap-t2d47.1-0 ; 
       reduce_flap-t2d48.1-0 ; 
       reduce_flap-t2d49.1-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.1-0 ; 
       reduce_wing-t2d13.1-0 ; 
       reduce_wing-t2d14.1-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d21.1-0 ; 
       reduce_wing-t2d22.1-0 ; 
       reduce_wing-t2d24.1-0 ; 
       reduce_wing-t2d25.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.1-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d43.1-0 ; 
       reduce_wing-t2d44.1-0 ; 
       reduce_wing-t2d45.1-0 ; 
       reduce_wing-t2d46.1-0 ; 
       reduce_wing-t2d6.1-0 ; 
       reduce_wing-t2d7.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 8 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 0 110 ; 
       17 38 110 ; 
       18 38 110 ; 
       19 17 110 ; 
       20 18 110 ; 
       21 39 110 ; 
       22 38 110 ; 
       23 38 110 ; 
       24 22 110 ; 
       25 23 110 ; 
       26 39 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 8 110 ; 
       30 8 110 ; 
       31 21 110 ; 
       32 26 110 ; 
       33 0 110 ; 
       34 1 110 ; 
       35 16 110 ; 
       36 1 110 ; 
       37 36 110 ; 
       38 4 110 ; 
       39 4 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 41 300 ; 
       0 42 300 ; 
       0 23 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 39 300 ; 
       6 40 300 ; 
       7 6 300 ; 
       8 43 300 ; 
       8 44 300 ; 
       8 22 300 ; 
       10 37 300 ; 
       11 15 300 ; 
       12 14 300 ; 
       13 38 300 ; 
       14 12 300 ; 
       15 13 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       17 10 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       18 9 300 ; 
       18 26 300 ; 
       21 3 300 ; 
       22 7 300 ; 
       22 28 300 ; 
       23 8 300 ; 
       23 27 300 ; 
       26 11 300 ; 
       27 32 300 ; 
       28 31 300 ; 
       29 30 300 ; 
       30 29 300 ; 
       31 34 300 ; 
       32 36 300 ; 
       33 33 300 ; 
       34 35 300 ; 
       36 16 300 ; 
       36 17 300 ; 
       36 18 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 33 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 MPRFLG 0 ; 
       9 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 30 -8 0 MPRFLG 0 ; 
       13 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 MPRFLG 0 ; 
       16 SCHEM 5 -8 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 45 -8 0 MPRFLG 0 ; 
       21 SCHEM 0 -6 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 40 -6 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 40 -8 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 15 -6 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 20 -6 0 MPRFLG 0 ; 
       30 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 0 -8 0 MPRFLG 0 ; 
       32 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 5 -10 0 MPRFLG 0 ; 
       36 SCHEM 10 -8 0 MPRFLG 0 ; 
       37 SCHEM 10 -10 0 MPRFLG 0 ; 
       38 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       39 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 8.946757 -6.026289 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 83.0148 -7.602794 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 83.0148 -7.602794 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 85.5148 -7.602794 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 85.5148 -7.602794 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 55.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 83.0148 -9.602795 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 83.0148 -9.602795 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 85.5148 -9.602795 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 85.5148 -9.602795 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 55.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 55.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 88.0148 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 30 30 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
