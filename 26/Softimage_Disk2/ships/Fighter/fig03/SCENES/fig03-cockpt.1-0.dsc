SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.18-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       reduce_flap-light1.12-0 ROOT ; 
       reduce_flap-light2.12-0 ROOT ; 
       reduce_flap-light3.12-0 ROOT ; 
       reduce_flap-light4.12-0 ROOT ; 
       reduce_flap-light5.12-0 ROOT ; 
       reduce_flap-light6.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat21.1-0 ; 
       reduce_wing-mat22.1-0 ; 
       reduce_wing-mat24.1-0 ; 
       reduce_wing-mat25.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat49.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat55.1-0 ; 
       reduce_wing-mat56.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour0.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.17-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt1_1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwepemt2_1.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt1_1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwepemt2_1.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-tlwepemt.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-trwepemt.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-cockpt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       reduce_flap-t2d47.1-0 ; 
       reduce_flap-t2d48.1-0 ; 
       reduce_flap-t2d49.1-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.1-0 ; 
       reduce_wing-t2d13.1-0 ; 
       reduce_wing-t2d14.2-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d21.1-0 ; 
       reduce_wing-t2d22.1-0 ; 
       reduce_wing-t2d24.1-0 ; 
       reduce_wing-t2d25.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.1-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d43.1-0 ; 
       reduce_wing-t2d44.1-0 ; 
       reduce_wing-t2d45.1-0 ; 
       reduce_wing-t2d46.1-0 ; 
       reduce_wing-t2d6.1-0 ; 
       reduce_wing-t2d7.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 19 110 ; 
       23 18 110 ; 
       30 25 110 ; 
       28 26 110 ; 
       40 9 110 ; 
       42 9 110 ; 
       3 9 110 ; 
       0 2 110 ; 
       2 5 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 9 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 0 110 ; 
       18 45 110 ; 
       19 45 110 ; 
       20 18 110 ; 
       22 19 110 ; 
       24 46 110 ; 
       25 45 110 ; 
       26 45 110 ; 
       27 25 110 ; 
       29 26 110 ; 
       31 46 110 ; 
       32 9 110 ; 
       33 9 110 ; 
       34 9 110 ; 
       35 9 110 ; 
       36 24 110 ; 
       37 31 110 ; 
       38 0 110 ; 
       39 1 110 ; 
       41 17 110 ; 
       43 1 110 ; 
       44 43 110 ; 
       45 5 110 ; 
       46 5 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 41 300 ; 
       0 42 300 ; 
       0 23 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 6 300 ; 
       9 43 300 ; 
       9 44 300 ; 
       9 22 300 ; 
       11 37 300 ; 
       12 15 300 ; 
       13 14 300 ; 
       14 38 300 ; 
       15 12 300 ; 
       16 13 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 21 300 ; 
       18 10 300 ; 
       18 24 300 ; 
       18 25 300 ; 
       19 9 300 ; 
       19 26 300 ; 
       24 3 300 ; 
       25 7 300 ; 
       25 28 300 ; 
       26 8 300 ; 
       26 27 300 ; 
       31 11 300 ; 
       32 32 300 ; 
       33 31 300 ; 
       34 30 300 ; 
       35 29 300 ; 
       36 34 300 ; 
       37 36 300 ; 
       38 33 300 ; 
       39 35 300 ; 
       43 16 300 ; 
       43 17 300 ; 
       43 18 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       37 29 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 32 401 ; 
       41 33 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -12.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -14.02629 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.23539 -0.02205132 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 65 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 67.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 60 -6 0 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -4 0 MPRFLG 0 ; 
       10 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 MPRFLG 0 ; 
       14 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 MPRFLG 0 ; 
       18 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 0 -6 0 MPRFLG 0 ; 
       25 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 15 -6 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 20 -6 0 MPRFLG 0 ; 
       35 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       36 SCHEM 0 -8 0 MPRFLG 0 ; 
       37 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 10.19676 -8.026289 0 MPRFLG 0 ; 
       41 SCHEM 5 -10 0 MPRFLG 0 ; 
       43 SCHEM 7.696758 -8.026289 0 MPRFLG 0 ; 
       44 SCHEM 7.696758 -10.02629 0 MPRFLG 0 ; 
       45 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       46 SCHEM 1.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 8.946758 -6.026289 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9.196758 -10.02629 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9.196758 -10.02629 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9.196758 -10.02629 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9.196758 -10.02629 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.69676 -8.026289 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.69676 -8.026289 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.69676 -8.026289 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9.196758 -12.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9.196758 -12.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9.196758 -12.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.69676 -10.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.69676 -10.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.69676 -10.02629 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 30 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
