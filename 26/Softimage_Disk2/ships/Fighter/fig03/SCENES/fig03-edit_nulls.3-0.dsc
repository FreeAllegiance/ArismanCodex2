SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.47-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.57-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.57-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.57-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.57-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.57-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       edit_nulls-light3.3-0 ROOT ; 
       edit_nulls-light4.3-0 ROOT ; 
       edit_nulls-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       edit_nulls-mat62.1-0 ; 
       edit_nulls-mat63.1-0 ; 
       edit_nulls-mat64.1-0 ; 
       edit_nulls-mat65.1-0 ; 
       edit_nulls-mat66.1-0 ; 
       edit_nulls-mat67.1-0 ; 
       edit_nulls-mat68.1-0 ; 
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat21.1-0 ; 
       reduce_wing-mat22.1-0 ; 
       reduce_wing-mat24.1-0 ; 
       reduce_wing-mat25.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat55.1-0 ; 
       reduce_wing-mat56.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 50     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.48-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-rtwepemt.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-smoke.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-edit_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d21.1-0 ; 
       reduce_wing-t2d22.1-0 ; 
       reduce_wing-t2d24.1-0 ; 
       reduce_wing-t2d25.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d43.1-0 ; 
       reduce_wing-t2d44.1-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       30 5 110 ; 
       0 2 110 ; 
       1 2 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 9 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 0 110 ; 
       18 48 110 ; 
       19 48 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 49 110 ; 
       23 17 110 ; 
       24 47 110 ; 
       25 48 110 ; 
       26 48 110 ; 
       27 26 110 ; 
       28 25 110 ; 
       29 49 110 ; 
       31 9 110 ; 
       32 9 110 ; 
       33 9 110 ; 
       34 9 110 ; 
       35 22 110 ; 
       36 29 110 ; 
       37 0 110 ; 
       38 0 110 ; 
       39 0 110 ; 
       40 1 110 ; 
       41 1 110 ; 
       42 1 110 ; 
       43 1 110 ; 
       44 0 110 ; 
       45 5 110 ; 
       46 5 110 ; 
       47 1 110 ; 
       48 5 110 ; 
       49 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 47 300 ; 
       0 48 300 ; 
       0 30 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       8 13 300 ; 
       9 49 300 ; 
       9 50 300 ; 
       9 29 300 ; 
       11 43 300 ; 
       12 22 300 ; 
       13 21 300 ; 
       14 44 300 ; 
       15 19 300 ; 
       16 20 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       17 28 300 ; 
       18 17 300 ; 
       18 31 300 ; 
       18 32 300 ; 
       19 16 300 ; 
       19 33 300 ; 
       22 10 300 ; 
       25 14 300 ; 
       25 35 300 ; 
       26 15 300 ; 
       26 34 300 ; 
       29 18 300 ; 
       31 39 300 ; 
       32 38 300 ; 
       33 37 300 ; 
       34 36 300 ; 
       35 40 300 ; 
       36 42 300 ; 
       37 3 300 ; 
       38 4 300 ; 
       39 5 300 ; 
       40 41 300 ; 
       41 0 300 ; 
       42 1 300 ; 
       43 2 300 ; 
       44 6 300 ; 
       47 23 300 ; 
       47 24 300 ; 
       47 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       43 29 401 ; 
       44 30 401 ; 
       45 31 401 ; 
       46 32 401 ; 
       47 33 401 ; 
       48 34 401 ; 
       49 35 401 ; 
       50 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 87.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       30 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 15 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 75 -4 0 MPRFLG 0 ; 
       7 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 MPRFLG 0 ; 
       10 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 55 -6 0 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 60 -6 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 10 -6 0 MPRFLG 0 ; 
       18 SCHEM 70 -4 0 MPRFLG 0 ; 
       19 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -4 0 MPRFLG 0 ; 
       23 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -4 0 MPRFLG 0 ; 
       26 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 40 -4 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 35 -4 0 MPRFLG 0 ; 
       35 SCHEM 5 -6 0 MPRFLG 0 ; 
       36 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 12.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 15 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 17.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 27.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 32.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 30 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 25 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 20 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       45 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       48 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       49 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 81.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
