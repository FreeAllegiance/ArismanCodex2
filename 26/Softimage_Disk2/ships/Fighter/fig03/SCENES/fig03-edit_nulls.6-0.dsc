SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.50-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.60-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.60-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.60-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.60-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.60-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       edit_nulls-light3.6-0 ROOT ; 
       edit_nulls-light4.6-0 ROOT ; 
       edit_nulls-light5.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 45     
       edit_nulls-mat62.1-0 ; 
       edit_nulls-mat63.1-0 ; 
       edit_nulls-mat64.1-0 ; 
       edit_nulls-mat65.1-0 ; 
       edit_nulls-mat66.1-0 ; 
       edit_nulls-mat67.1-0 ; 
       edit_nulls-mat68.1-0 ; 
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.51-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-rtwepemt.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-smoke.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-edit_nulls.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 5 110 ; 
       0 2 110 ; 
       1 2 110 ; 
       2 5 110 ; 
       3 9 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 9 110 ; 
       9 5 110 ; 
       10 0 110 ; 
       11 41 110 ; 
       12 41 110 ; 
       13 12 110 ; 
       14 11 110 ; 
       15 42 110 ; 
       16 10 110 ; 
       17 40 110 ; 
       18 41 110 ; 
       19 41 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 42 110 ; 
       24 9 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 15 110 ; 
       29 22 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 1 110 ; 
       37 0 110 ; 
       38 9 110 ; 
       39 9 110 ; 
       40 1 110 ; 
       41 5 110 ; 
       42 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 41 300 ; 
       0 42 300 ; 
       0 26 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 13 300 ; 
       9 43 300 ; 
       9 44 300 ; 
       9 25 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       11 17 300 ; 
       11 27 300 ; 
       11 28 300 ; 
       12 16 300 ; 
       12 29 300 ; 
       15 10 300 ; 
       18 14 300 ; 
       18 31 300 ; 
       19 15 300 ; 
       19 30 300 ; 
       22 18 300 ; 
       24 35 300 ; 
       25 34 300 ; 
       26 33 300 ; 
       27 32 300 ; 
       28 36 300 ; 
       29 38 300 ; 
       30 3 300 ; 
       31 4 300 ; 
       32 5 300 ; 
       33 37 300 ; 
       34 0 300 ; 
       35 1 300 ; 
       36 2 300 ; 
       37 6 300 ; 
       40 19 300 ; 
       40 20 300 ; 
       40 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       43 29 401 ; 
       44 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 15 -4 0 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 MPRFLG 0 ; 
       9 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 MPRFLG 0 ; 
       11 SCHEM 60 -4 0 MPRFLG 0 ; 
       12 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -4 0 MPRFLG 0 ; 
       19 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 40 -4 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 35 -4 0 MPRFLG 0 ; 
       28 SCHEM 5 -6 0 MPRFLG 0 ; 
       29 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 12.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       31 SCHEM 15 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       33 SCHEM 27.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 30 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       36 SCHEM 25 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 20 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
