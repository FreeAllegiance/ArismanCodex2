SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig03-fig03.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       reduce-mat1.1-0 ; 
       reduce-mat10.1-0 ; 
       reduce-mat11.1-0 ; 
       reduce-mat12.1-0 ; 
       reduce-mat13.1-0 ; 
       reduce-mat14.1-0 ; 
       reduce-mat15.1-0 ; 
       reduce-mat16.1-0 ; 
       reduce-mat17.1-0 ; 
       reduce-mat18.1-0 ; 
       reduce-mat2.1-0 ; 
       reduce-mat21.1-0 ; 
       reduce-mat22.1-0 ; 
       reduce-mat24.1-0 ; 
       reduce-mat25.1-0 ; 
       reduce-mat26.1-0 ; 
       reduce-mat27.1-0 ; 
       reduce-mat28.1-0 ; 
       reduce-mat29.1-0 ; 
       reduce-mat3.1-0 ; 
       reduce-mat30.1-0 ; 
       reduce-mat31.1-0 ; 
       reduce-mat33.1-0 ; 
       reduce-mat34.1-0 ; 
       reduce-mat35.1-0 ; 
       reduce-mat36.1-0 ; 
       reduce-mat37.1-0 ; 
       reduce-mat38.1-0 ; 
       reduce-mat39.1-0 ; 
       reduce-mat4.1-0 ; 
       reduce-mat40.1-0 ; 
       reduce-mat44.1-0 ; 
       reduce-mat45.1-0 ; 
       reduce-mat46.1-0 ; 
       reduce-mat47.1-0 ; 
       reduce-mat49.1-0 ; 
       reduce-mat5.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat6.1-0 ; 
       reduce-mat7.1-0 ; 
       reduce-mat8.1-0 ; 
       reduce-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig03-alarmour.1-0 ; 
       fig03-ararmour.1-0 ; 
       fig03-armour0.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.2-0 ROOT ; 
       fig03-flarmour.1-0 ; 
       fig03-frarmour.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-tlwepemt.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-trwepemt.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-reduce.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       reduce-t2d1.1-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d15.1-0 ; 
       reduce-t2d16.1-0 ; 
       reduce-t2d17.1-0 ; 
       reduce-t2d18.1-0 ; 
       reduce-t2d2.1-0 ; 
       reduce-t2d21.1-0 ; 
       reduce-t2d22.1-0 ; 
       reduce-t2d24.1-0 ; 
       reduce-t2d25.1-0 ; 
       reduce-t2d26.1-0 ; 
       reduce-t2d27.1-0 ; 
       reduce-t2d28.1-0 ; 
       reduce-t2d29.1-0 ; 
       reduce-t2d3.1-0 ; 
       reduce-t2d30.1-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.1-0 ; 
       reduce-t2d36.1-0 ; 
       reduce-t2d37.1-0 ; 
       reduce-t2d38.1-0 ; 
       reduce-t2d39.1-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d40.1-0 ; 
       reduce-t2d43.1-0 ; 
       reduce-t2d44.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d6.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 8 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 0 110 ; 
       17 38 110 ; 
       18 38 110 ; 
       19 17 110 ; 
       20 18 110 ; 
       21 39 110 ; 
       22 38 110 ; 
       23 38 110 ; 
       24 22 110 ; 
       25 23 110 ; 
       26 39 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 8 110 ; 
       30 8 110 ; 
       31 21 110 ; 
       32 26 110 ; 
       33 0 110 ; 
       34 1 110 ; 
       35 16 110 ; 
       36 1 110 ; 
       37 36 110 ; 
       38 4 110 ; 
       39 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 42 300 ; 
       0 43 300 ; 
       0 23 300 ; 
       1 19 300 ; 
       1 29 300 ; 
       1 36 300 ; 
       1 24 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       8 22 300 ; 
       10 40 300 ; 
       11 14 300 ; 
       12 13 300 ; 
       13 41 300 ; 
       14 11 300 ; 
       15 12 300 ; 
       16 18 300 ; 
       16 20 300 ; 
       16 21 300 ; 
       17 9 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       18 8 300 ; 
       18 27 300 ; 
       21 0 300 ; 
       22 6 300 ; 
       22 30 300 ; 
       23 7 300 ; 
       23 28 300 ; 
       26 10 300 ; 
       27 34 300 ; 
       28 33 300 ; 
       29 32 300 ; 
       30 31 300 ; 
       31 37 300 ; 
       32 39 300 ; 
       33 35 300 ; 
       34 38 300 ; 
       36 15 300 ; 
       36 16 300 ; 
       36 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 29 401 ; 
       30 30 401 ; 
       36 33 401 ; 
       40 31 401 ; 
       41 32 401 ; 
       42 34 401 ; 
       43 35 401 ; 
       44 36 401 ; 
       45 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 30 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 MPRFLG 0 ; 
       13 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 50 -4 0 MPRFLG 0 ; 
       19 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 45 -4 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 45 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 20 -4 0 MPRFLG 0 ; 
       28 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 25 -4 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 5 -6 0 MPRFLG 0 ; 
       32 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 10 -8 0 MPRFLG 0 ; 
       36 SCHEM 15 -6 0 MPRFLG 0 ; 
       37 SCHEM 15 -8 0 MPRFLG 0 ; 
       38 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       39 SCHEM 6.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 108 108 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
