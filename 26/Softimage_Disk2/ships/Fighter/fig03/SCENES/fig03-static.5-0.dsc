SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.52-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.52-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.52-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.52-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.52-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       static-light3.3-0 ROOT ; 
       static-light4.3-0 ROOT ; 
       static-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat15.1-0 ; 
       reduce_wing-mat16.1-0 ; 
       reduce_wing-mat17.1-0 ; 
       reduce_wing-mat18.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat21.1-0 ; 
       reduce_wing-mat22.1-0 ; 
       reduce_wing-mat24.1-0 ; 
       reduce_wing-mat25.1-0 ; 
       reduce_wing-mat26.1-0 ; 
       reduce_wing-mat27.1-0 ; 
       reduce_wing-mat28.1-0 ; 
       reduce_wing-mat29.1-0 ; 
       reduce_wing-mat30.1-0 ; 
       reduce_wing-mat31.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat36.1-0 ; 
       reduce_wing-mat37.1-0 ; 
       reduce_wing-mat38.1-0 ; 
       reduce_wing-mat39.1-0 ; 
       reduce_wing-mat40.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat51.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat54.1-0 ; 
       reduce_wing-mat55.1-0 ; 
       reduce_wing-mat56.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
       static-mat62.4-0 ; 
       static-mat63.4-0 ; 
       static-mat64.4-0 ; 
       static-mat65.4-0 ; 
       static-mat66.4-0 ; 
       static-mat67.4-0 ; 
       static-mat68.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour.1-0 ; 
       fig03-cockpt.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.45-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-LL0.1-0 ; 
       fig03-LLl1.1-0 ; 
       fig03-LLl2.1-0 ; 
       fig03-LLl3.1-0 ; 
       fig03-LLr1.1-0 ; 
       fig03-LLr2.1-0 ; 
       fig03-LLr3.1-0 ; 
       fig03-lwepbar.1-0 ; 
       fig03-lwepbar1.1-0 ; 
       fig03-lwepbar2.1-0 ; 
       fig03-lwepemt1.1-0 ; 
       fig03-lwepemt2.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-rtwepemt.1-0 ; 
       fig03-rwepbar1.1-0 ; 
       fig03-rwepbar2.1-0 ; 
       fig03-rwepemt1.1-0 ; 
       fig03-rwepemt2.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SSl.1-0 ; 
       fig03-SSr.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-trwepbar.1-0 ; 
       fig03-weapon0.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d15.1-0 ; 
       reduce_wing-t2d16.1-0 ; 
       reduce_wing-t2d17.1-0 ; 
       reduce_wing-t2d18.1-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d21.1-0 ; 
       reduce_wing-t2d22.1-0 ; 
       reduce_wing-t2d24.1-0 ; 
       reduce_wing-t2d25.1-0 ; 
       reduce_wing-t2d26.1-0 ; 
       reduce_wing-t2d27.1-0 ; 
       reduce_wing-t2d28.1-0 ; 
       reduce_wing-t2d29.1-0 ; 
       reduce_wing-t2d30.1-0 ; 
       reduce_wing-t2d31.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d36.1-0 ; 
       reduce_wing-t2d37.1-0 ; 
       reduce_wing-t2d38.1-0 ; 
       reduce_wing-t2d39.1-0 ; 
       reduce_wing-t2d40.1-0 ; 
       reduce_wing-t2d43.1-0 ; 
       reduce_wing-t2d44.1-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 5 110 ; 
       3 9 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 9 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 0 110 ; 
       18 47 110 ; 
       19 47 110 ; 
       20 19 110 ; 
       21 18 110 ; 
       22 48 110 ; 
       23 17 110 ; 
       24 46 110 ; 
       25 47 110 ; 
       26 47 110 ; 
       27 26 110 ; 
       28 25 110 ; 
       29 48 110 ; 
       30 9 110 ; 
       31 9 110 ; 
       32 9 110 ; 
       33 9 110 ; 
       34 22 110 ; 
       35 29 110 ; 
       36 0 110 ; 
       37 0 110 ; 
       38 0 110 ; 
       39 1 110 ; 
       40 1 110 ; 
       41 1 110 ; 
       42 1 110 ; 
       43 0 110 ; 
       44 9 110 ; 
       45 9 110 ; 
       46 1 110 ; 
       47 5 110 ; 
       48 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 40 300 ; 
       0 41 300 ; 
       0 23 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       8 6 300 ; 
       9 42 300 ; 
       9 43 300 ; 
       9 22 300 ; 
       11 36 300 ; 
       12 15 300 ; 
       13 14 300 ; 
       14 37 300 ; 
       15 12 300 ; 
       16 13 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 21 300 ; 
       18 10 300 ; 
       18 24 300 ; 
       18 25 300 ; 
       19 9 300 ; 
       19 26 300 ; 
       22 3 300 ; 
       25 7 300 ; 
       25 28 300 ; 
       26 8 300 ; 
       26 27 300 ; 
       29 11 300 ; 
       30 32 300 ; 
       31 31 300 ; 
       32 30 300 ; 
       33 29 300 ; 
       34 33 300 ; 
       35 35 300 ; 
       36 47 300 ; 
       37 48 300 ; 
       38 49 300 ; 
       39 34 300 ; 
       40 44 300 ; 
       41 45 300 ; 
       42 46 300 ; 
       43 50 300 ; 
       46 16 300 ; 
       46 17 300 ; 
       46 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       36 29 401 ; 
       37 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
       42 35 401 ; 
       43 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 71.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 36.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 70 -6 0 MPRFLG 0 ; 
       7 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 50 -8 0 MPRFLG 0 ; 
       13 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 MPRFLG 0 ; 
       16 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 MPRFLG 0 ; 
       18 SCHEM 65 -6 0 MPRFLG 0 ; 
       19 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 0 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60 -6 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 35 -6 0 MPRFLG 0 ; 
       32 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 30 -6 0 MPRFLG 0 ; 
       34 SCHEM 0 -8 0 MPRFLG 0 ; 
       35 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 7.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 10 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       38 SCHEM 12.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 22.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 27.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 25 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 20 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 15 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 63.75 -4 0 MPRFLG 0 ; 
       48 SCHEM 1.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       44 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
