SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.54-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.54-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.54-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.54-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.54-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat1.1-0 ; 
       reduce_wing-mat12.2-0 ; 
       reduce_wing-mat13.2-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat2.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat57.2-0 ; 
       reduce_wing-mat58.2-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-armour.1-0 ; 
       fig03-farmour.1-0 ; 
       fig03-fig03.46-0 ROOT ; 
       fig03-frarmour.1-0 ; 
       fig03-frarmour5.1-0 ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-lwingzz.1-0 ; 
       fig03-rwingzz.1-0 ; 
       fig03-wingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig03/PICTURES/fig03 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig03-static.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       reduce_flap-t2d47.2-0 ; 
       reduce_flap-t2d48.2-0 ; 
       reduce_flap-t2d49.2-0 ; 
       reduce_wing-t2d1.1-0 ; 
       reduce_wing-t2d12.3-0 ; 
       reduce_wing-t2d13.3-0 ; 
       reduce_wing-t2d14.3-0 ; 
       reduce_wing-t2d2.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.2-0 ; 
       reduce_wing-t2d45.2-0 ; 
       reduce_wing-t2d46.2-0 ; 
       reduce_wing-t2d6.2-0 ; 
       reduce_wing-t2d7.2-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 8 110 ; 
       8 4 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       0 13 300 ; 
       0 9 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       7 6 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 8 300 ; 
       9 3 300 ; 
       10 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 1.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
