SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       ad_guns-mat73.1-0 ; 
       ad_guns-mat74.1-0 ; 
       ad_guns-mat75.1-0 ; 
       ad_guns-mat79.1-0 ; 
       ad_guns-mat80.1-0 ; 
       ad_guns-mat81.1-0 ; 
       ad_guns-mat82.1-0 ; 
       ad_guns-mat83.1-0 ; 
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat2.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       fig04-fig04.2-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-ad_guns.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       ad_guns-t2d66.1-0 ; 
       ad_guns-t2d67.1-0 ; 
       rix_fighter_sPt-t2d1.2-0 ; 
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d2.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.2-0 ; 
       rix_fighter_sPt-t2d45.2-0 ; 
       rix_fighter_sPt-t2d46.2-0 ; 
       rix_fighter_sPt-t2d47.2-0 ; 
       rix_fighter_sPt-t2d48.2-0 ; 
       rix_fighter_sPt-t2d49.2-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.2-0 ; 
       rix_fighter_sPt-t2d51.2-0 ; 
       rix_fighter_sPt-t2d52.2-0 ; 
       rix_fighter_sPt-t2d53.2-0 ; 
       rix_fighter_sPt-t2d54.2-0 ; 
       rix_fighter_sPt-t2d55.2-0 ; 
       rix_fighter_sPt-t2d56.2-0 ; 
       rix_fighter_sPt-t2d57.2-0 ; 
       rix_fighter_sPt-t2d58.2-0 ; 
       rix_fighter_sPt-t2d59.2-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.2-0 ; 
       rix_fighter_sPt-t2d61.2-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.2-0 ; 
       rix_fighter_sPt-t2d65.2-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       6 3 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 25 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 25 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 11 110 ; 
       24 18 110 ; 
       25 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 28 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       1 34 300 ; 
       1 8 300 ; 
       1 36 300 ; 
       1 0 300 ; 
       1 7 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 17 300 ; 
       5 9 300 ; 
       5 12 300 ; 
       6 27 300 ; 
       6 33 300 ; 
       6 37 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 35 300 ; 
       13 38 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       18 18 300 ; 
       18 19 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       19 3 300 ; 
       20 4 300 ; 
       21 5 300 ; 
       22 6 300 ; 
       23 1 300 ; 
       24 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 21 400 ; 
       6 28 400 ; 
       10 23 400 ; 
       11 20 400 ; 
       13 29 400 ; 
       17 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 14 401 ; 
       18 10 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 15 401 ; 
       24 16 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 25 401 ; 
       28 31 401 ; 
       30 30 401 ; 
       31 24 401 ; 
       32 26 401 ; 
       33 32 401 ; 
       35 19 401 ; 
       36 27 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       0 0 401 ; 
       7 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 0 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6.177001 0 USR MPRFLG 0 ; 
       8 SCHEM 30 -6.177001 0 USR MPRFLG 0 ; 
       9 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 40 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -6.177001 0 USR MPRFLG 0 ; 
       15 SCHEM 35 -6.177001 0 USR MPRFLG 0 ; 
       16 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 MPRFLG 0 ; 
       19 SCHEM 25 -4 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 20 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 40 -8 0 MPRFLG 0 ; 
       24 SCHEM 45 -8 0 MPRFLG 0 ; 
       25 SCHEM 41.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 37 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
