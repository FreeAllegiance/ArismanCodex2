SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.5-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       add_guns-mat73.1-0 ; 
       add_guns-mat74.1-0 ; 
       add_guns-mat75.1-0 ; 
       add_guns-mat79.1-0 ; 
       add_guns-mat80.1-0 ; 
       add_guns-mat81.1-0 ; 
       add_guns-mat82.1-0 ; 
       add_guns-mat83.1-0 ; 
       add_guns-mat86.1-0 ; 
       add_guns-mat87.1-0 ; 
       add_guns-mat88.1-0 ; 
       add_guns-mat89.1-0 ; 
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat2.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       fig04-fig04.4-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-gun2.1-0 ; 
       fig04-gun2_1.1-0 ; 
       fig04-gun2_2.1-0 ; 
       fig04-gun2_3.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-add_guns.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       add_guns-t2d66.1-0 ; 
       add_guns-t2d67.1-0 ; 
       add_guns-t2d68.1-0 ; 
       add_guns-t2d69.1-0 ; 
       add_guns-t2d70.1-0 ; 
       add_guns-t2d71.1-0 ; 
       rix_fighter_sPt-t2d1.2-0 ; 
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d2.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.2-0 ; 
       rix_fighter_sPt-t2d45.2-0 ; 
       rix_fighter_sPt-t2d46.2-0 ; 
       rix_fighter_sPt-t2d47.2-0 ; 
       rix_fighter_sPt-t2d48.2-0 ; 
       rix_fighter_sPt-t2d49.2-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.2-0 ; 
       rix_fighter_sPt-t2d51.2-0 ; 
       rix_fighter_sPt-t2d52.2-0 ; 
       rix_fighter_sPt-t2d53.2-0 ; 
       rix_fighter_sPt-t2d54.2-0 ; 
       rix_fighter_sPt-t2d55.2-0 ; 
       rix_fighter_sPt-t2d56.2-0 ; 
       rix_fighter_sPt-t2d57.2-0 ; 
       rix_fighter_sPt-t2d58.2-0 ; 
       rix_fighter_sPt-t2d59.2-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.2-0 ; 
       rix_fighter_sPt-t2d61.2-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.2-0 ; 
       rix_fighter_sPt-t2d65.2-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 6 110 ; 
       10 7 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 29 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 29 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 15 110 ; 
       28 22 110 ; 
       29 0 110 ; 
       3 11 110 ; 
       5 18 110 ; 
       4 19 110 ; 
       2 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 32 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       1 38 300 ; 
       1 12 300 ; 
       1 40 300 ; 
       1 0 300 ; 
       1 7 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       7 21 300 ; 
       9 13 300 ; 
       9 16 300 ; 
       10 31 300 ; 
       10 37 300 ; 
       10 41 300 ; 
       14 28 300 ; 
       14 29 300 ; 
       14 30 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       15 39 300 ; 
       17 42 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       21 19 300 ; 
       21 20 300 ; 
       22 22 300 ; 
       22 23 300 ; 
       22 24 300 ; 
       22 25 300 ; 
       23 3 300 ; 
       24 4 300 ; 
       25 5 300 ; 
       26 6 300 ; 
       27 1 300 ; 
       28 2 300 ; 
       3 9 300 ; 
       5 10 300 ; 
       4 11 300 ; 
       2 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 25 400 ; 
       10 32 400 ; 
       14 27 400 ; 
       15 24 400 ; 
       17 33 400 ; 
       21 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 18 401 ; 
       22 14 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 29 401 ; 
       32 35 401 ; 
       34 34 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 36 401 ; 
       39 23 401 ; 
       40 31 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       8 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 21.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 0 -6 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 25 -6 0 MPRFLG 0 ; 
       13 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 35 -8 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 40 -8 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 40 -10 0 MPRFLG 0 ; 
       29 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.48139 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34.98138 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14.98139 -7.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.48139 -7.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9.981386 -7.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.481386 -7.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 37.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 37.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 37.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 37.48138 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29.98139 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29.98139 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 30.28895 -10.08407 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30.28895 -10.08407 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 30.28895 -10.08407 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29.98139 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29.78164 -5.418401 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20.03029 -10.7314 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.70049 -10.91842 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25.07513 -10.74554 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.29896 -10.7314 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.48138 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.48138 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 37.48138 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 37.48138 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.48138 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29.98139 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29.98139 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 30.28895 -12.08407 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30.28895 -12.08407 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 30.28895 -12.08407 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29.98139 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29.98139 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29.78164 -5.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 32.48138 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 30.28895 -10.08407 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29.78164 -7.418401 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20.03029 -12.7314 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.70049 -12.91842 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25.07513 -12.74554 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.29896 -12.7314 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 37.48138 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 29 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
