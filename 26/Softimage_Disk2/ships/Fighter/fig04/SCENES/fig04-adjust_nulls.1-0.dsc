SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.12-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.12-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       adjust_nulls-mat73.1-0 ; 
       adjust_nulls-mat74.1-0 ; 
       adjust_nulls-mat75.1-0 ; 
       adjust_nulls-mat79.1-0 ; 
       adjust_nulls-mat80.1-0 ; 
       adjust_nulls-mat81.1-0 ; 
       adjust_nulls-mat82.1-0 ; 
       adjust_nulls-mat83.1-0 ; 
       adjust_nulls-mat86.1-0 ; 
       adjust_nulls-mat87.1-0 ; 
       adjust_nulls-mat88.1-0 ; 
       adjust_nulls-mat89.1-0 ; 
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat2.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       adjust_nulls-missemt.1-0 ROOT ; 
       fig04-cockpt.1-0 ; 
       fig04-fig04.11-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-lgun1.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepemt1.1-0 ; 
       fig04-lwepemt2.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rgun1.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepemt1.1-0 ; 
       fig04-rwepemt2.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-trail.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-adjust_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       adjust_nulls-t2d66.1-0 ; 
       adjust_nulls-t2d67.1-0 ; 
       adjust_nulls-t2d68.1-0 ; 
       adjust_nulls-t2d69.1-0 ; 
       adjust_nulls-t2d70.1-0 ; 
       adjust_nulls-t2d71.1-0 ; 
       rix_fighter_sPt-t2d1.2-0 ; 
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d2.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.2-0 ; 
       rix_fighter_sPt-t2d45.2-0 ; 
       rix_fighter_sPt-t2d46.2-0 ; 
       rix_fighter_sPt-t2d47.2-0 ; 
       rix_fighter_sPt-t2d48.2-0 ; 
       rix_fighter_sPt-t2d49.2-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.2-0 ; 
       rix_fighter_sPt-t2d51.2-0 ; 
       rix_fighter_sPt-t2d52.2-0 ; 
       rix_fighter_sPt-t2d53.2-0 ; 
       rix_fighter_sPt-t2d54.2-0 ; 
       rix_fighter_sPt-t2d55.2-0 ; 
       rix_fighter_sPt-t2d56.3-0 ; 
       rix_fighter_sPt-t2d57.2-0 ; 
       rix_fighter_sPt-t2d58.2-0 ; 
       rix_fighter_sPt-t2d59.3-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.3-0 ; 
       rix_fighter_sPt-t2d61.3-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.3-0 ; 
       rix_fighter_sPt-t2d65.3-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 5 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 36 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 5 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 5 110 ; 
       22 19 110 ; 
       23 20 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 36 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       31 3 110 ; 
       32 3 110 ; 
       33 17 110 ; 
       34 28 110 ; 
       35 2 110 ; 
       36 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 38 300 ; 
       3 12 300 ; 
       3 40 300 ; 
       3 0 300 ; 
       3 7 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 21 300 ; 
       7 9 300 ; 
       8 8 300 ; 
       9 13 300 ; 
       9 16 300 ; 
       10 31 300 ; 
       10 37 300 ; 
       10 41 300 ; 
       16 28 300 ; 
       16 29 300 ; 
       16 30 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       17 39 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       21 42 300 ; 
       21 14 300 ; 
       21 15 300 ; 
       27 19 300 ; 
       27 20 300 ; 
       28 22 300 ; 
       28 23 300 ; 
       28 24 300 ; 
       28 25 300 ; 
       29 3 300 ; 
       30 4 300 ; 
       31 5 300 ; 
       32 6 300 ; 
       33 1 300 ; 
       34 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 25 400 ; 
       10 32 400 ; 
       16 27 400 ; 
       17 24 400 ; 
       21 33 400 ; 
       27 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 18 401 ; 
       22 14 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 29 401 ; 
       32 35 401 ; 
       34 34 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 36 401 ; 
       39 23 401 ; 
       40 31 401 ; 
       41 37 401 ; 
       42 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 52.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 MPRFLG 0 ; 
       9 SCHEM 0 -4 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 28.1678 -6.074089 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30.34453 -6.053004 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -4 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 45 -6 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 MPRFLG 0 ; 
       19 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 35 -4 0 MPRFLG 0 ; 
       21 SCHEM 5 -6 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 40 -4 0 MPRFLG 0 ; 
       25 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 50 -6 0 MPRFLG 0 ; 
       29 SCHEM 20 -4 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 15 -4 0 MPRFLG 0 ; 
       32 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 50 -8 0 MPRFLG 0 ; 
       35 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       0 SCHEM 57.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0.6506749 -0.01042362 -0.2617634 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
