SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.11-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       check_strobes-mat73.1-0 ; 
       check_strobes-mat74.1-0 ; 
       check_strobes-mat75.1-0 ; 
       check_strobes-mat79.1-0 ; 
       check_strobes-mat80.1-0 ; 
       check_strobes-mat81.1-0 ; 
       check_strobes-mat82.1-0 ; 
       check_strobes-mat83.1-0 ; 
       check_strobes-mat86.1-0 ; 
       check_strobes-mat87.1-0 ; 
       check_strobes-mat88.1-0 ; 
       check_strobes-mat89.1-0 ; 
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat2.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       fig04-cockpt.1-0 ; 
       fig04-fig04.10-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-lgun1.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepemt1.1-0 ; 
       fig04-lwepemt2.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rgun1.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepemt1.1-0 ; 
       fig04-rwepemt2.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-trail.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-check_strobes.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       check_strobes-t2d66.2-0 ; 
       check_strobes-t2d67.2-0 ; 
       check_strobes-t2d68.2-0 ; 
       check_strobes-t2d69.2-0 ; 
       check_strobes-t2d70.2-0 ; 
       check_strobes-t2d71.2-0 ; 
       rix_fighter_sPt-t2d1.2-0 ; 
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d2.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.2-0 ; 
       rix_fighter_sPt-t2d45.2-0 ; 
       rix_fighter_sPt-t2d46.2-0 ; 
       rix_fighter_sPt-t2d47.2-0 ; 
       rix_fighter_sPt-t2d48.2-0 ; 
       rix_fighter_sPt-t2d49.2-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.2-0 ; 
       rix_fighter_sPt-t2d51.2-0 ; 
       rix_fighter_sPt-t2d52.2-0 ; 
       rix_fighter_sPt-t2d53.2-0 ; 
       rix_fighter_sPt-t2d54.2-0 ; 
       rix_fighter_sPt-t2d55.2-0 ; 
       rix_fighter_sPt-t2d56.3-0 ; 
       rix_fighter_sPt-t2d57.2-0 ; 
       rix_fighter_sPt-t2d58.2-0 ; 
       rix_fighter_sPt-t2d59.3-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.3-0 ; 
       rix_fighter_sPt-t2d61.3-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.3-0 ; 
       rix_fighter_sPt-t2d65.3-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 6 110 ; 
       11 7 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 35 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 4 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 4 110 ; 
       21 18 110 ; 
       22 19 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 35 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 16 110 ; 
       33 27 110 ; 
       34 1 110 ; 
       35 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 36 300 ; 
       2 38 300 ; 
       2 12 300 ; 
       2 40 300 ; 
       2 0 300 ; 
       2 7 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 21 300 ; 
       6 9 300 ; 
       7 8 300 ; 
       8 13 300 ; 
       8 16 300 ; 
       9 31 300 ; 
       9 37 300 ; 
       9 41 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       15 30 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       16 39 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 42 300 ; 
       20 14 300 ; 
       20 15 300 ; 
       26 19 300 ; 
       26 20 300 ; 
       27 22 300 ; 
       27 23 300 ; 
       27 24 300 ; 
       27 25 300 ; 
       28 3 300 ; 
       29 4 300 ; 
       30 5 300 ; 
       31 6 300 ; 
       32 1 300 ; 
       33 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 25 400 ; 
       9 32 400 ; 
       15 27 400 ; 
       16 24 400 ; 
       20 33 400 ; 
       26 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 18 401 ; 
       22 14 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 29 401 ; 
       32 35 401 ; 
       34 34 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 36 401 ; 
       39 23 401 ; 
       40 31 401 ; 
       41 37 401 ; 
       42 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 52.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 MPRFLG 0 ; 
       8 SCHEM 0 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 45 -6 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 10 -6 0 MPRFLG 0 ; 
       18 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 35 -4 0 MPRFLG 0 ; 
       20 SCHEM 5 -6 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 40 -4 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 50 -6 0 MPRFLG 0 ; 
       28 SCHEM 20 -4 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 15 -4 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 50 -8 0 MPRFLG 0 ; 
       34 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 46.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 51.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 51 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
