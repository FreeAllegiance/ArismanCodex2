SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig04-fig04.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       rix_fighter_sPAT-light1_2.1-0 ROOT ; 
       rix_fighter_sPAT-light2_2.1-0 ROOT ; 
       rix_fighter_sPAT-light3_2.1-0 ROOT ; 
       rix_fig_F-light1_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       rix_fighter_sPt-default1.1-0 ; 
       rix_fighter_sPt-mat1.1-0 ; 
       rix_fighter_sPt-mat10.1-0 ; 
       rix_fighter_sPt-mat11.1-0 ; 
       rix_fighter_sPt-mat2.1-0 ; 
       rix_fighter_sPt-mat3.1-0 ; 
       rix_fighter_sPt-mat4.1-0 ; 
       rix_fighter_sPt-mat48.1-0 ; 
       rix_fighter_sPt-mat49.1-0 ; 
       rix_fighter_sPt-mat5.1-0 ; 
       rix_fighter_sPt-mat50.1-0 ; 
       rix_fighter_sPt-mat51.1-0 ; 
       rix_fighter_sPt-mat52.1-0 ; 
       rix_fighter_sPt-mat53.1-0 ; 
       rix_fighter_sPt-mat54.1-0 ; 
       rix_fighter_sPt-mat55.1-0 ; 
       rix_fighter_sPt-mat56.1-0 ; 
       rix_fighter_sPt-mat57.1-0 ; 
       rix_fighter_sPt-mat58.1-0 ; 
       rix_fighter_sPt-mat6.1-0 ; 
       rix_fighter_sPt-mat65.1-0 ; 
       rix_fighter_sPt-mat66.1-0 ; 
       rix_fighter_sPt-mat67.1-0 ; 
       rix_fighter_sPt-mat68.1-0 ; 
       rix_fighter_sPt-mat69.1-0 ; 
       rix_fighter_sPt-mat7.1-0 ; 
       rix_fighter_sPt-mat70.1-0 ; 
       rix_fighter_sPt-mat71.1-0 ; 
       rix_fighter_sPt-mat72.1-0 ; 
       rix_fighter_sPt-mat8.1-0 ; 
       rix_fighter_sPt-mat9.1-0 ; 
       rix_fig_F-mat73.1-0 ; 
       rix_fig_F-mat74.1-0 ; 
       rix_fig_F-mat75.1-0 ; 
       rix_fig_F-mat79.1-0 ; 
       rix_fig_F-mat80.1-0 ; 
       rix_fig_F-mat81.1-0 ; 
       rix_fig_F-mat82.1-0 ; 
       rix_fig_F-mat83.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       fig04-fig04.1-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-rix_fig_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       rix_fighter_sPt-t2d1.1-0 ; 
       rix_fighter_sPt-t2d10.1-0 ; 
       rix_fighter_sPt-t2d11.1-0 ; 
       rix_fighter_sPt-t2d2.1-0 ; 
       rix_fighter_sPt-t2d3.1-0 ; 
       rix_fighter_sPt-t2d4.1-0 ; 
       rix_fighter_sPt-t2d44.1-0 ; 
       rix_fighter_sPt-t2d45.1-0 ; 
       rix_fighter_sPt-t2d46.1-0 ; 
       rix_fighter_sPt-t2d47.1-0 ; 
       rix_fighter_sPt-t2d48.1-0 ; 
       rix_fighter_sPt-t2d49.1-0 ; 
       rix_fighter_sPt-t2d5.1-0 ; 
       rix_fighter_sPt-t2d50.1-0 ; 
       rix_fighter_sPt-t2d51.1-0 ; 
       rix_fighter_sPt-t2d52.1-0 ; 
       rix_fighter_sPt-t2d53.1-0 ; 
       rix_fighter_sPt-t2d54.1-0 ; 
       rix_fighter_sPt-t2d55.1-0 ; 
       rix_fighter_sPt-t2d56.1-0 ; 
       rix_fighter_sPt-t2d57.1-0 ; 
       rix_fighter_sPt-t2d58.1-0 ; 
       rix_fighter_sPt-t2d59.1-0 ; 
       rix_fighter_sPt-t2d6.1-0 ; 
       rix_fighter_sPt-t2d60.1-0 ; 
       rix_fighter_sPt-t2d61.1-0 ; 
       rix_fighter_sPt-t2d62.1-0 ; 
       rix_fighter_sPt-t2d63.1-0 ; 
       rix_fighter_sPt-t2d64.1-0 ; 
       rix_fighter_sPt-t2d65.1-0 ; 
       rix_fighter_sPt-t2d7.1-0 ; 
       rix_fighter_sPt-t2d8.1-0 ; 
       rix_fighter_sPt-t2d9.1-0 ; 
       rix_fig_F-t2d66.1-0 ; 
       rix_fig_F-t2d67.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 11 110 ; 
       24 18 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       6 3 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 25 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 25 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       25 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 1 300 ; 
       5 4 300 ; 
       19 34 300 ; 
       20 35 300 ; 
       21 36 300 ; 
       22 37 300 ; 
       23 32 300 ; 
       24 33 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 26 300 ; 
       1 0 300 ; 
       1 28 300 ; 
       1 31 300 ; 
       1 38 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       6 19 300 ; 
       6 25 300 ; 
       6 29 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 27 300 ; 
       13 30 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       18 12 300 ; 
       18 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 19 400 ; 
       6 26 400 ; 
       10 21 400 ; 
       11 18 400 ; 
       13 27 400 ; 
       17 20 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       31 33 401 ; 
       38 34 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 12 401 ; 
       10 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 23 401 ; 
       20 29 401 ; 
       22 28 401 ; 
       23 22 401 ; 
       24 24 401 ; 
       25 30 401 ; 
       27 17 401 ; 
       28 25 401 ; 
       29 31 401 ; 
       30 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 0.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 MPRFLG 0 ; 
       20 SCHEM 41.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 MPRFLG 0 ; 
       22 SCHEM 36.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 94 -10 0 MPRFLG 0 ; 
       24 SCHEM 114 -10 0 MPRFLG 0 ; 
       0 SCHEM 61.5 -2 0 SRT 1 1 1 0 -3.141593 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 59 -4 0 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 MPRFLG 0 ; 
       4 SCHEM 24 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 7.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 46.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 MPRFLG 0 ; 
       9 SCHEM 94 -6 0 MPRFLG 0 ; 
       10 SCHEM 87.75 -8 0 MPRFLG 0 ; 
       11 SCHEM 99 -8 0 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 51.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 54 -6 0 MPRFLG 0 ; 
       16 SCHEM 115.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 109 -8 0 MPRFLG 0 ; 
       18 SCHEM 119 -8 0 MPRFLG 0 ; 
       25 SCHEM 104 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       31 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 114 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 106.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 109 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 124 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 121.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 119 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 116.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 104 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 101.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 91.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 89 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 99 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       33 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 106.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 109 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 124 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 119 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 116.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 104 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 101.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 91.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 89 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 99 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 96.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 79 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 125.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
