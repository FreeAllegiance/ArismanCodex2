SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.25-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat2.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
       static-mat73.3-0 ; 
       static-mat74.3-0 ; 
       static-mat75.3-0 ; 
       static-mat79.3-0 ; 
       static-mat80.3-0 ; 
       static-mat81.3-0 ; 
       static-mat82.3-0 ; 
       static-mat83.3-0 ; 
       static-mat86.3-0 ; 
       static-mat87.3-0 ; 
       static-mat88.3-0 ; 
       static-mat89.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       fig04-cockpt.1-0 ; 
       fig04-fig04.20-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lbwepatt.1-0 ; 
       fig04-lgun1.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-LL1.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwepemt1.1-0 ; 
       fig04-lwepemt2.1-0 ; 
       fig04-lwepmnt1.1-0 ; 
       fig04-lwepmnt2.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-missemt.1-0 ; 
       fig04-rbwepatt.1-0 ; 
       fig04-rgun1.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwepemt1.1-0 ; 
       fig04-rwepemt2.1-0 ; 
       fig04-rwepmnt1.1-0 ; 
       fig04-rwepmnt2.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-smoke.1-0 ; 
       fig04-SSb1.1-0 ; 
       fig04-SSb2.1-0 ; 
       fig04-SSb3.1-0 ; 
       fig04-SSb4.1-0 ; 
       fig04-SSl.1-0 ; 
       fig04-SSr.1-0 ; 
       fig04-thrust.1-0 ; 
       fig04-trail.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       rix_fighter_sPt-t2d1.2-0 ; 
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d2.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.2-0 ; 
       rix_fighter_sPt-t2d45.2-0 ; 
       rix_fighter_sPt-t2d46.2-0 ; 
       rix_fighter_sPt-t2d47.2-0 ; 
       rix_fighter_sPt-t2d48.2-0 ; 
       rix_fighter_sPt-t2d49.2-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.2-0 ; 
       rix_fighter_sPt-t2d51.2-0 ; 
       rix_fighter_sPt-t2d52.2-0 ; 
       rix_fighter_sPt-t2d53.2-0 ; 
       rix_fighter_sPt-t2d54.2-0 ; 
       rix_fighter_sPt-t2d55.2-0 ; 
       rix_fighter_sPt-t2d56.3-0 ; 
       rix_fighter_sPt-t2d57.2-0 ; 
       rix_fighter_sPt-t2d58.2-0 ; 
       rix_fighter_sPt-t2d59.3-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.3-0 ; 
       rix_fighter_sPt-t2d61.3-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.3-0 ; 
       rix_fighter_sPt-t2d65.3-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
       static-t2d66.3-0 ; 
       static-t2d67.3-0 ; 
       static-t2d68.3-0 ; 
       static-t2d69.3-0 ; 
       static-t2d70.3-0 ; 
       static-t2d71.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 4 110 ; 
       10 6 110 ; 
       11 7 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 38 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 5 110 ; 
       18 4 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 4 110 ; 
       22 19 110 ; 
       23 20 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 38 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       30 2 110 ; 
       31 2 110 ; 
       32 2 110 ; 
       33 2 110 ; 
       34 16 110 ; 
       35 28 110 ; 
       36 1 110 ; 
       37 1 110 ; 
       38 1 110 ; 
       29 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 26 300 ; 
       2 0 300 ; 
       2 28 300 ; 
       2 31 300 ; 
       2 38 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       4 9 300 ; 
       6 40 300 ; 
       7 39 300 ; 
       8 1 300 ; 
       8 4 300 ; 
       9 19 300 ; 
       9 25 300 ; 
       9 29 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       16 14 300 ; 
       16 15 300 ; 
       16 27 300 ; 
       19 41 300 ; 
       20 42 300 ; 
       21 30 300 ; 
       21 2 300 ; 
       21 3 300 ; 
       27 7 300 ; 
       27 8 300 ; 
       28 10 300 ; 
       28 11 300 ; 
       28 12 300 ; 
       28 13 300 ; 
       30 34 300 ; 
       31 35 300 ; 
       32 36 300 ; 
       33 37 300 ; 
       34 32 300 ; 
       35 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 19 400 ; 
       9 26 400 ; 
       15 21 400 ; 
       16 18 400 ; 
       21 27 400 ; 
       27 20 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       31 33 401 ; 
       38 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 12 401 ; 
       10 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 23 401 ; 
       20 29 401 ; 
       22 28 401 ; 
       23 22 401 ; 
       24 24 401 ; 
       25 30 401 ; 
       27 17 401 ; 
       28 25 401 ; 
       29 31 401 ; 
       30 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 35 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 50 -6 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -6 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 MPRFLG 0 ; 
       21 SCHEM 10 -6 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 45 -4 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 55 -6 0 MPRFLG 0 ; 
       30 SCHEM 25 -4 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 20 -4 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 55 -8 0 MPRFLG 0 ; 
       36 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 51.25 -2 0 MPRFLG 0 ; 
       29 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       31 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       33 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
