SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.5-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       static-default1.1-0 ; 
       static-mat10.1-0 ; 
       static-mat11.1-0 ; 
       static-mat12.1-0 ; 
       static-mat13.1-0 ; 
       static-mat14.1-0 ; 
       static-mat15.1-0 ; 
       static-mat16.1-0 ; 
       static-mat17.1-0 ; 
       static-mat19.1-0 ; 
       static-mat20.1-0 ; 
       static-mat21.1-0 ; 
       static-mat23.1-0 ; 
       static-mat24.1-0 ; 
       static-mat25.1-0 ; 
       static-mat26.1-0 ; 
       static-mat27.1-0 ; 
       static-mat28.1-0 ; 
       static-mat29.1-0 ; 
       static-mat3.1-0 ; 
       static-mat30.1-0 ; 
       static-mat31.1-0 ; 
       static-mat32.1-0 ; 
       static-mat33.1-0 ; 
       static-mat34.1-0 ; 
       static-mat4.1-0 ; 
       static-mat5.1-0 ; 
       static-mat6.1-0 ; 
       static-mat7.1-0 ; 
       static-mat8.1-0 ; 
       static-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       fig04-fig04.26-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-static.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       static-t2d10.1-0 ; 
       static-t2d11.1-0 ; 
       static-t2d12.1-0 ; 
       static-t2d13.1-0 ; 
       static-t2d14.1-0 ; 
       static-t2d15.1-0 ; 
       static-t2d16.1-0 ; 
       static-t2d17.1-0 ; 
       static-t2d18.1-0 ; 
       static-t2d19.1-0 ; 
       static-t2d20.1-0 ; 
       static-t2d21.1-0 ; 
       static-t2d22.1-0 ; 
       static-t2d23.1-0 ; 
       static-t2d24.1-0 ; 
       static-t2d25.1-0 ; 
       static-t2d26.1-0 ; 
       static-t2d27.1-0 ; 
       static-t2d28.1-0 ; 
       static-t2d29.1-0 ; 
       static-t2d3.1-0 ; 
       static-t2d30.1-0 ; 
       static-t2d31.1-0 ; 
       static-t2d32.1-0 ; 
       static-t2d33.1-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d4.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 12 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 3 110 ; 
       9 12 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 15 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 0 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       3 19 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 30 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       10 3 300 ; 
       10 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 26 400 ; 
       4 32 400 ; 
       6 17 400 ; 
       7 13 400 ; 
       8 3 400 ; 
       10 6 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       2 2 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 12 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 18 401 ; 
       17 19 401 ; 
       18 21 401 ; 
       19 20 401 ; 
       20 22 401 ; 
       22 23 401 ; 
       23 24 401 ; 
       24 25 401 ; 
       25 27 401 ; 
       26 28 401 ; 
       27 29 401 ; 
       28 30 401 ; 
       29 31 401 ; 
       30 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 -1.570796 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 3.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 3.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 11.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 13.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
