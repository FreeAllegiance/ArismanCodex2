SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.29-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.29-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       rix_fighter_sPt-default1.2-0 ; 
       rix_fighter_sPt-mat10.2-0 ; 
       rix_fighter_sPt-mat11.2-0 ; 
       rix_fighter_sPt-mat3.2-0 ; 
       rix_fighter_sPt-mat4.2-0 ; 
       rix_fighter_sPt-mat48.2-0 ; 
       rix_fighter_sPt-mat49.2-0 ; 
       rix_fighter_sPt-mat5.2-0 ; 
       rix_fighter_sPt-mat50.2-0 ; 
       rix_fighter_sPt-mat51.2-0 ; 
       rix_fighter_sPt-mat52.2-0 ; 
       rix_fighter_sPt-mat53.2-0 ; 
       rix_fighter_sPt-mat54.2-0 ; 
       rix_fighter_sPt-mat55.2-0 ; 
       rix_fighter_sPt-mat56.2-0 ; 
       rix_fighter_sPt-mat57.2-0 ; 
       rix_fighter_sPt-mat58.2-0 ; 
       rix_fighter_sPt-mat6.2-0 ; 
       rix_fighter_sPt-mat65.2-0 ; 
       rix_fighter_sPt-mat66.2-0 ; 
       rix_fighter_sPt-mat67.2-0 ; 
       rix_fighter_sPt-mat68.2-0 ; 
       rix_fighter_sPt-mat69.2-0 ; 
       rix_fighter_sPt-mat7.2-0 ; 
       rix_fighter_sPt-mat70.2-0 ; 
       rix_fighter_sPt-mat71.2-0 ; 
       rix_fighter_sPt-mat72.2-0 ; 
       rix_fighter_sPt-mat8.2-0 ; 
       rix_fighter_sPt-mat9.2-0 ; 
       static-mat73.4-0 ; 
       static-mat83.4-0 ; 
       static-mat86.4-0 ; 
       static-mat87.4-0 ; 
       static-mat88.4-0 ; 
       static-mat89.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       fig04-fig04.29-0 ROOT ; 
       fig04-fuselg.1-0 ; 
       fig04-landgr0.1-0 ; 
       fig04-landgr2.1-0 ; 
       fig04-lgun1.1-0 ; 
       fig04-lgun2.1-0 ; 
       fig04-llandgr.1-0 ; 
       fig04-lwingzz0.1-0 ; 
       fig04-lwingzz1.1-0 ; 
       fig04-lwingzz2.1-0 ; 
       fig04-rgun1.1-0 ; 
       fig04-rgun2.1-0 ; 
       fig04-rlandgr.1-0 ; 
       fig04-rwingzz0.1-0 ; 
       fig04-rwingzz1.1-0 ; 
       fig04-rwingzz2.1-0 ; 
       fig04-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig04/PICTURES/fig04 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig04-static.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       rix_fighter_sPt-t2d10.2-0 ; 
       rix_fighter_sPt-t2d11.2-0 ; 
       rix_fighter_sPt-t2d3.2-0 ; 
       rix_fighter_sPt-t2d4.2-0 ; 
       rix_fighter_sPt-t2d44.3-0 ; 
       rix_fighter_sPt-t2d45.3-0 ; 
       rix_fighter_sPt-t2d46.3-0 ; 
       rix_fighter_sPt-t2d47.3-0 ; 
       rix_fighter_sPt-t2d48.3-0 ; 
       rix_fighter_sPt-t2d49.3-0 ; 
       rix_fighter_sPt-t2d5.2-0 ; 
       rix_fighter_sPt-t2d50.3-0 ; 
       rix_fighter_sPt-t2d51.3-0 ; 
       rix_fighter_sPt-t2d52.3-0 ; 
       rix_fighter_sPt-t2d53.3-0 ; 
       rix_fighter_sPt-t2d54.3-0 ; 
       rix_fighter_sPt-t2d55.3-0 ; 
       rix_fighter_sPt-t2d56.4-0 ; 
       rix_fighter_sPt-t2d57.3-0 ; 
       rix_fighter_sPt-t2d58.3-0 ; 
       rix_fighter_sPt-t2d59.4-0 ; 
       rix_fighter_sPt-t2d6.2-0 ; 
       rix_fighter_sPt-t2d60.4-0 ; 
       rix_fighter_sPt-t2d61.4-0 ; 
       rix_fighter_sPt-t2d62.2-0 ; 
       rix_fighter_sPt-t2d63.2-0 ; 
       rix_fighter_sPt-t2d64.4-0 ; 
       rix_fighter_sPt-t2d65.4-0 ; 
       rix_fighter_sPt-t2d7.2-0 ; 
       rix_fighter_sPt-t2d8.2-0 ; 
       rix_fighter_sPt-t2d9.2-0 ; 
       static-t2d66.4-0 ; 
       static-t2d67.4-0 ; 
       static-t2d68.4-0 ; 
       static-t2d69.4-0 ; 
       static-t2d70.4-0 ; 
       static-t2d71.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 3 110 ; 
       7 16 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 3 110 ; 
       13 16 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 24 300 ; 
       1 0 300 ; 
       1 26 300 ; 
       1 29 300 ; 
       1 30 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 7 300 ; 
       4 32 300 ; 
       5 31 300 ; 
       6 17 300 ; 
       6 23 300 ; 
       6 27 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       10 33 300 ; 
       11 34 300 ; 
       12 28 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 17 400 ; 
       6 24 400 ; 
       8 19 400 ; 
       9 16 400 ; 
       12 25 400 ; 
       14 18 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 10 401 ; 
       8 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 21 401 ; 
       18 27 401 ; 
       20 26 401 ; 
       21 20 401 ; 
       22 22 401 ; 
       23 28 401 ; 
       25 15 401 ; 
       26 23 401 ; 
       27 29 401 ; 
       28 30 401 ; 
       29 31 401 ; 
       30 32 401 ; 
       31 33 401 ; 
       32 34 401 ; 
       33 35 401 ; 
       34 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 -1.570796 0 0 1.390758 MPRFLG 0 ; 
       1 SCHEM 16.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 28.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 26.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
