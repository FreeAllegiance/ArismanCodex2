SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 17     
       fig05-fig05.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.5-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       add_gun-mat69.1-0 ; 
       done_lite-mat7.1-0 ; 
       durethma_fighterbike_sa-mat48.1-0 ; 
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
       reduce-mat83.1-0 ; 
       reduce-mat84.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       fig05-fig05.4-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-l-gun.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lthrust1.1-0 ; 
       fig05-lthrust2.1-0 ; 
       fig05-lthrust3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthrust1.1-0 ; 
       fig05-rthrust2.1-0 ; 
       fig05-rthrust3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-wepemt.1-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Bomber/bom01a/PICTURES/bom01a ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-add_gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       add_gun-t2d85.1-0 ; 
       reduce-t2d1.1-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d15.1-0 ; 
       reduce-t2d16.1-0 ; 
       reduce-t2d18.1-0 ; 
       reduce-t2d19.1-0 ; 
       reduce-t2d2.1-0 ; 
       reduce-t2d20.1-0 ; 
       reduce-t2d21.1-0 ; 
       reduce-t2d22.1-0 ; 
       reduce-t2d23.1-0 ; 
       reduce-t2d24.1-0 ; 
       reduce-t2d25.1-0 ; 
       reduce-t2d26.1-0 ; 
       reduce-t2d27.1-0 ; 
       reduce-t2d28.1-0 ; 
       reduce-t2d29.1-0 ; 
       reduce-t2d3.1-0 ; 
       reduce-t2d30.1-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d32.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.1-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d6.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 28 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 0 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 11 110 ; 
       15 14 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 18 110 ; 
       22 21 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 15 110 ; 
       26 22 110 ; 
       27 4 110 ; 
       28 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 0 300 ; 
       2 3 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       6 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       9 16 300 ; 
       10 15 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 33 300 ; 
       11 34 300 ; 
       15 4 300 ; 
       16 18 300 ; 
       17 17 300 ; 
       18 12 300 ; 
       18 13 300 ; 
       18 14 300 ; 
       18 35 300 ; 
       18 36 300 ; 
       22 5 300 ; 
       23 38 300 ; 
       24 37 300 ; 
       25 1 300 ; 
       26 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 11 401 ; 
       5 22 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 32 401 ; 
       10 33 401 ; 
       11 34 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 28 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 16 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 30 -10 0 MPRFLG 0 ; 
       0 SCHEM 15 -4 0 SRT 1 1 1 0 0 0 0.0001 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -6 0 MPRFLG 0 ; 
       2 SCHEM 0 -8 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 20 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       12 SCHEM 20 -16 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -16 0 MPRFLG 0 ; 
       14 SCHEM 20 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       15 SCHEM 20 -14 0 WIRECOL 8 7 MPRFLG 0 ; 
       16 SCHEM 5 -6 0 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 MPRFLG 0 ; 
       18 SCHEM 5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -16 0 MPRFLG 0 ; 
       20 SCHEM 5 -16 0 MPRFLG 0 ; 
       21 SCHEM 5 -12 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 5 -14 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -16 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -16 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 30 -12 0 MPRFLG 0 ; 
       28 SCHEM 30 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 31.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 163.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 102.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 103.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 150.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 149.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 252.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 110.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 145.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 184.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 152.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 145.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 110.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 152.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 184.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 135.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 175.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
