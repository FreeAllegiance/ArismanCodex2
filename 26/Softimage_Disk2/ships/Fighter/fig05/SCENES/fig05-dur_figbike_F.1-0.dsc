SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 17     
       fig05-fig05.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       dur_figbike_F-light1.1-0 ROOT ; 
       dur_figbike_F-light2.1-0 ROOT ; 
       dur_figbike_F-light3.1-0 ROOT ; 
       dur_figbike_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       done_lite-mat7.1-0 ; 
       dur_figbike_F-mat49.1-0 ; 
       dur_figbike_F-mat50.1-0 ; 
       dur_figbike_F-mat51.1-0 ; 
       dur_figbike_F-mat52.1-0 ; 
       dur_figbike_F-mat53.1-0 ; 
       dur_figbike_F-mat54.1-0 ; 
       dur_figbike_F-mat55.1-0 ; 
       dur_figbike_F-mat56.1-0 ; 
       dur_figbike_F-mat57.1-0 ; 
       dur_figbike_F-mat58.1-0 ; 
       dur_figbike_F-mat59.1-0 ; 
       dur_figbike_F-mat60.1-0 ; 
       dur_figbike_F-mat61.1-0 ; 
       dur_figbike_F-mat62.1-0 ; 
       dur_figbike_F-mat63.1-0 ; 
       dur_figbike_F-mat64.1-0 ; 
       dur_figbike_F-mat65.1-0 ; 
       dur_figbike_F-mat66.1-0 ; 
       dur_figbike_F-mat67.1-0 ; 
       dur_figbike_F-mat68.1-0 ; 
       dur_figbike_F-mat69.1-0 ; 
       dur_figbike_F-mat70.1-0 ; 
       dur_figbike_F-mat71.1-0 ; 
       dur_figbike_F-mat72.1-0 ; 
       dur_figbike_F-mat73.1-0 ; 
       dur_figbike_F-mat74.1-0 ; 
       dur_figbike_F-mat75.1-0 ; 
       dur_figbike_F-mat76.1-0 ; 
       dur_figbike_F-mat77.1-0 ; 
       dur_figbike_F-mat78.1-0 ; 
       dur_figbike_F-mat79.1-0 ; 
       dur_figbike_F-mat80.1-0 ; 
       dur_figbike_F-mat81.1-0 ; 
       dur_figbike_F-mat82.1-0 ; 
       dur_figbike_F-mat83.1-0 ; 
       dur_figbike_F-mat84.1-0 ; 
       durethma_fighterbike_sa-mat48.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       fig05-fig05.1-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lthrust1.1-0 ; 
       fig05-lthrust2.1-0 ; 
       fig05-lthrust3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthrust1.1-0 ; 
       fig05-rthrust2.1-0 ; 
       fig05-rthrust3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-wepemt.1-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-dur_figbike_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       dur_figbike_F-t2d1.1-0 ; 
       dur_figbike_F-t2d10.1-0 ; 
       dur_figbike_F-t2d11.1-0 ; 
       dur_figbike_F-t2d12.1-0 ; 
       dur_figbike_F-t2d13.1-0 ; 
       dur_figbike_F-t2d14.1-0 ; 
       dur_figbike_F-t2d15.1-0 ; 
       dur_figbike_F-t2d16.1-0 ; 
       dur_figbike_F-t2d18.1-0 ; 
       dur_figbike_F-t2d19.1-0 ; 
       dur_figbike_F-t2d2.1-0 ; 
       dur_figbike_F-t2d20.1-0 ; 
       dur_figbike_F-t2d21.1-0 ; 
       dur_figbike_F-t2d22.1-0 ; 
       dur_figbike_F-t2d23.1-0 ; 
       dur_figbike_F-t2d24.1-0 ; 
       dur_figbike_F-t2d25.1-0 ; 
       dur_figbike_F-t2d26.1-0 ; 
       dur_figbike_F-t2d27.1-0 ; 
       dur_figbike_F-t2d28.1-0 ; 
       dur_figbike_F-t2d29.1-0 ; 
       dur_figbike_F-t2d3.1-0 ; 
       dur_figbike_F-t2d30.1-0 ; 
       dur_figbike_F-t2d31.1-0 ; 
       dur_figbike_F-t2d32.1-0 ; 
       dur_figbike_F-t2d33.1-0 ; 
       dur_figbike_F-t2d34.1-0 ; 
       dur_figbike_F-t2d35.1-0 ; 
       dur_figbike_F-t2d4.1-0 ; 
       dur_figbike_F-t2d5.1-0 ; 
       dur_figbike_F-t2d6.1-0 ; 
       dur_figbike_F-t2d7.1-0 ; 
       dur_figbike_F-t2d8.1-0 ; 
       dur_figbike_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       22 3 110 ; 
       23 3 110 ; 
       24 14 110 ; 
       25 21 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 0 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 0 110 ; 
       16 15 110 ; 
       17 16 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 17 110 ; 
       21 20 110 ; 
       26 3 110 ; 
       27 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 36 300 ; 
       23 35 300 ; 
       24 0 300 ; 
       25 37 300 ; 
       2 1 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       5 4 300 ; 
       6 5 300 ; 
       7 6 300 ; 
       8 14 300 ; 
       9 13 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       10 31 300 ; 
       10 32 300 ; 
       14 2 300 ; 
       15 16 300 ; 
       16 15 300 ; 
       17 10 300 ; 
       17 11 300 ; 
       17 12 300 ; 
       17 33 300 ; 
       17 34 300 ; 
       21 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 10 401 ; 
       3 21 401 ; 
       4 28 401 ; 
       5 29 401 ; 
       6 30 401 ; 
       7 31 401 ; 
       8 32 401 ; 
       9 33 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
       14 5 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       17 27 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 16 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       22 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 65 -6 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -14 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -14 0 WIRECOL 2 7 MPRFLG 0 ; 
       0 SCHEM 53.75 -2 0 SRT 1 1 1 0 0 0 0.0001 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -4 0 MPRFLG 0 ; 
       2 SCHEM 0 -6 0 MPRFLG 0 ; 
       3 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 MPRFLG 0 ; 
       6 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 MPRFLG 0 ; 
       8 SCHEM 50 -4 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       11 SCHEM 40 -14 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -14 0 MPRFLG 0 ; 
       13 SCHEM 41.25 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       14 SCHEM 41.25 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       15 SCHEM 15 -4 0 MPRFLG 0 ; 
       16 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -14 0 MPRFLG 0 ; 
       19 SCHEM 5 -14 0 MPRFLG 0 ; 
       20 SCHEM 6.25 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 6.25 -12 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 70 -6 0 MPRFLG 0 ; 
       27 SCHEM 72.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 109 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 161.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 100.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 101.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 148.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 147.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 250.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 108.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 143.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 182.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 150.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 143.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 108.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 150.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 182.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 133.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 173.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
