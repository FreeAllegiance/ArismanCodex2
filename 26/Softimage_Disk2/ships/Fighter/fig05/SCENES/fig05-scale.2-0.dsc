SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 17     
       fig05-fig05.27-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.31-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.31-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.31-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.31-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       scale-light1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       done_lite-mat7.1-0 ; 
       durethma_fighterbike_sa-mat48.1-0 ; 
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
       reduce-mat83.1-0 ; 
       reduce-mat84.1-0 ; 
       scale-mat69.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       fig05-cockpt.2-0 ; 
       fig05-fig05.27-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-l-gun.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lsmoke.1-0 ; 
       fig05-lthrust.2-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-missemt.1-0 ; 
       fig05-rsmoke.1-0 ; 
       fig05-rthrust.2-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-trail.2-0 ; 
       fig05-trthrust.1-0 ; 
       fig05-tthrust.2-0 ; 
       fig05-wepemt.2-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-scale.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       reduce-t2d1.2-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.2-0 ; 
       reduce-t2d14.2-0 ; 
       reduce-t2d15.2-0 ; 
       reduce-t2d16.2-0 ; 
       reduce-t2d18.2-0 ; 
       reduce-t2d19.2-0 ; 
       reduce-t2d2.2-0 ; 
       reduce-t2d20.2-0 ; 
       reduce-t2d21.2-0 ; 
       reduce-t2d22.2-0 ; 
       reduce-t2d23.2-0 ; 
       reduce-t2d24.2-0 ; 
       reduce-t2d25.2-0 ; 
       reduce-t2d26.2-0 ; 
       reduce-t2d27.2-0 ; 
       reduce-t2d28.2-0 ; 
       reduce-t2d29.2-0 ; 
       reduce-t2d3.2-0 ; 
       reduce-t2d30.2-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d32.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.2-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d6.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
       scale-t2d85.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 37 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 14 110 ; 
       18 17 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 22 110 ; 
       24 23 110 ; 
       25 28 110 ; 
       26 28 110 ; 
       27 24 110 ; 
       28 27 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 18 110 ; 
       32 28 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 5 110 ; 
       37 4 110 ; 
       19 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       5 38 300 ; 
       7 5 300 ; 
       8 6 300 ; 
       9 7 300 ; 
       12 15 300 ; 
       13 14 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 32 300 ; 
       14 33 300 ; 
       18 3 300 ; 
       22 17 300 ; 
       23 16 300 ; 
       24 11 300 ; 
       24 12 300 ; 
       24 13 300 ; 
       24 34 300 ; 
       24 35 300 ; 
       28 4 300 ; 
       29 37 300 ; 
       30 36 300 ; 
       31 0 300 ; 
       32 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 10 401 ; 
       4 21 401 ; 
       5 28 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 32 401 ; 
       10 33 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 27 401 ; 
       19 8 401 ; 
       20 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 16 401 ; 
       26 15 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 22 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       38 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0.0001 0 -0.3962504 MPRFLG 0 ; 
       2 SCHEM 0 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 0 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 30 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 49.24157 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 42.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 WIRECOL 8 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 20 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 22.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 WIRECOL 8 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 20 -12 0 WIRECOL 8 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 46.74157 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 37.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 5 -8 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 2.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 5 -10 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 5 -12 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 17.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 7.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 44.24157 -4 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 40 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 30 -10 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 27.27701 -10.02456 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 50.74157 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
