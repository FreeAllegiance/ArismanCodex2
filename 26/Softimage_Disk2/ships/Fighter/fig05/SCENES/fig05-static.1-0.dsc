SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 17     
       fig05-fig05.26-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.30-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       STATIC-light1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       fig05-fig05.26-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       reduce-t2d1.2-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.2-0 ; 
       reduce-t2d14.2-0 ; 
       reduce-t2d15.2-0 ; 
       reduce-t2d16.2-0 ; 
       reduce-t2d18.2-0 ; 
       reduce-t2d19.2-0 ; 
       reduce-t2d2.2-0 ; 
       reduce-t2d20.2-0 ; 
       reduce-t2d21.2-0 ; 
       reduce-t2d22.2-0 ; 
       reduce-t2d23.2-0 ; 
       reduce-t2d24.2-0 ; 
       reduce-t2d25.2-0 ; 
       reduce-t2d26.2-0 ; 
       reduce-t2d27.2-0 ; 
       reduce-t2d28.2-0 ; 
       reduce-t2d29.2-0 ; 
       reduce-t2d3.2-0 ; 
       reduce-t2d30.2-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d32.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.2-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d6.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 0 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 15 110 ; 
       17 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       8 13 300 ; 
       9 12 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       12 1 300 ; 
       13 15 300 ; 
       14 14 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 11 300 ; 
       15 32 300 ; 
       15 33 300 ; 
       17 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 10 401 ; 
       2 21 401 ; 
       3 28 401 ; 
       4 29 401 ; 
       5 30 401 ; 
       6 31 401 ; 
       7 32 401 ; 
       8 33 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 27 401 ; 
       17 8 401 ; 
       18 9 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 15 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0.0001 0 -0.3962504 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 21.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
