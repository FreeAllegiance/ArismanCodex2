SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.19-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       done_lite-mat7.1-0 ; 
       durethma_fighterbike_sa-mat48.1-0 ; 
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
       reduce-mat83.1-0 ; 
       reduce-mat84.1-0 ; 
       static-mat69.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       fig05-cockpt.2-0 ; 
       fig05-fig05.17-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-l-gun.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-lthrust.2-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthrust.2-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-trail.2-0 ; 
       fig05-tthrust.2-0 ; 
       fig05-wepemt.2-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       reduce-t2d1.1-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d15.1-0 ; 
       reduce-t2d16.1-0 ; 
       reduce-t2d18.1-0 ; 
       reduce-t2d19.1-0 ; 
       reduce-t2d2.1-0 ; 
       reduce-t2d20.1-0 ; 
       reduce-t2d21.1-0 ; 
       reduce-t2d22.1-0 ; 
       reduce-t2d23.1-0 ; 
       reduce-t2d24.1-0 ; 
       reduce-t2d25.1-0 ; 
       reduce-t2d26.1-0 ; 
       reduce-t2d27.1-0 ; 
       reduce-t2d28.1-0 ; 
       reduce-t2d29.1-0 ; 
       reduce-t2d3.1-0 ; 
       reduce-t2d30.1-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d32.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.1-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
       static-t2d85.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 32 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       10 1 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 12 110 ; 
       16 15 110 ; 
       18 1 110 ; 
       19 18 110 ; 
       20 19 110 ; 
       21 24 110 ; 
       22 24 110 ; 
       23 20 110 ; 
       24 23 110 ; 
       25 4 110 ; 
       26 4 110 ; 
       27 16 110 ; 
       28 24 110 ; 
       32 4 110 ; 
       9 1 110 ; 
       31 5 110 ; 
       0 1 110 ; 
       29 1 110 ; 
       17 1 110 ; 
       30 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 2 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       5 37 300 ; 
       7 5 300 ; 
       8 6 300 ; 
       10 14 300 ; 
       11 13 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       16 3 300 ; 
       18 16 300 ; 
       19 15 300 ; 
       20 10 300 ; 
       20 11 300 ; 
       20 12 300 ; 
       20 33 300 ; 
       20 34 300 ; 
       24 4 300 ; 
       25 36 300 ; 
       26 35 300 ; 
       27 0 300 ; 
       28 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       37 33 401 ; 
       2 0 401 ; 
       3 10 401 ; 
       4 21 401 ; 
       5 28 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 32 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
       14 5 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       17 27 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 16 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 21.25 -4 0 SRT 1 1 1 0 0 0 0.0001 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -6 0 USR MPRFLG 0 ; 
       3 SCHEM 0 -8 0 USR MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 USR MPRFLG 0 ; 
       5 SCHEM 30 -10 0 USR MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 USR MPRFLG 0 ; 
       7 SCHEM 10 -8 0 USR MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 USR MPRFLG 0 ; 
       10 SCHEM 20 -6 0 USR MPRFLG 0 ; 
       11 SCHEM 20 -8 0 USR MPRFLG 0 ; 
       12 SCHEM 20 -10 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       13 SCHEM 20 -16 0 USR MPRFLG 0 ; 
       14 SCHEM 22.5 -16 0 USR MPRFLG 0 ; 
       15 SCHEM 20 -12 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       16 SCHEM 20 -14 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 USR MPRFLG 0 ; 
       19 SCHEM 5 -8 0 USR MPRFLG 0 ; 
       20 SCHEM 5 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -16 0 USR MPRFLG 0 ; 
       22 SCHEM 5 -16 0 USR MPRFLG 0 ; 
       23 SCHEM 5 -12 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 5 -14 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 USR MPRFLG 0 ; 
       26 SCHEM 25 -8 0 USR MPRFLG 0 ; 
       27 SCHEM 17.5 -16 0 USR MPRFLG 0 ; 
       28 SCHEM 7.5 -16 0 USR MPRFLG 0 ; 
       32 SCHEM 30 -8 0 USR MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 32.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 35 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       37 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       33 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
