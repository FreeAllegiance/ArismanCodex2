SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.20-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.20-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.20-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       fig05-fig05.18-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig05/PICTURES/fig05 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       reduce-t2d1.1-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d15.1-0 ; 
       reduce-t2d16.1-0 ; 
       reduce-t2d18.1-0 ; 
       reduce-t2d19.1-0 ; 
       reduce-t2d2.1-0 ; 
       reduce-t2d20.1-0 ; 
       reduce-t2d21.1-0 ; 
       reduce-t2d22.1-0 ; 
       reduce-t2d23.1-0 ; 
       reduce-t2d24.1-0 ; 
       reduce-t2d25.1-0 ; 
       reduce-t2d26.1-0 ; 
       reduce-t2d27.1-0 ; 
       reduce-t2d28.1-0 ; 
       reduce-t2d29.1-0 ; 
       reduce-t2d3.1-0 ; 
       reduce-t2d30.1-0 ; 
       reduce-t2d31.1-0 ; 
       reduce-t2d32.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.1-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 12 300 ; 
       8 11 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 29 300 ; 
       9 30 300 ; 
       11 1 300 ; 
       12 14 300 ; 
       13 13 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       16 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 10 401 ; 
       2 21 401 ; 
       3 28 401 ; 
       4 29 401 ; 
       5 30 401 ; 
       6 31 401 ; 
       7 32 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 27 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 16 401 ; 
       23 15 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 21.25 -2 0 SRT 1 1 1 0 0 0 0.0001 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -4 0 USR MPRFLG 0 ; 
       2 SCHEM 0 -6 0 USR MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 USR MPRFLG 0 ; 
       5 SCHEM 10 -6 0 USR MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 USR MPRFLG 0 ; 
       7 SCHEM 20 -4 0 USR MPRFLG 0 ; 
       8 SCHEM 20 -6 0 USR MPRFLG 0 ; 
       9 SCHEM 20 -8 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       10 SCHEM 20 -10 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       11 SCHEM 20 -12 0 USR WIRECOL 8 7 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       13 SCHEM 5 -6 0 USR MPRFLG 0 ; 
       14 SCHEM 5 -8 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 5 -10 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 5 -12 0 USR WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
