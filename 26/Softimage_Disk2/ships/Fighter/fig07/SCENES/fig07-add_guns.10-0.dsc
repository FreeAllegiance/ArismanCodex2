SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig07-fig07.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_guns-cam_int1.10-0 ROOT ; 
       add_guns-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_guns-black1.1-0 ; 
       add_guns-blue_pure1.1-0 ; 
       add_guns-blue_pure2.1-0 ; 
       add_guns-default1.1-0 ; 
       add_guns-glass1.1-0 ; 
       add_guns-left.1-0 ; 
       add_guns-mat1.1-0 ; 
       add_guns-mat10.1-0 ; 
       add_guns-mat11.1-0 ; 
       add_guns-mat12.1-0 ; 
       add_guns-mat13.1-0 ; 
       add_guns-mat14.1-0 ; 
       add_guns-mat15.1-0 ; 
       add_guns-mat16.1-0 ; 
       add_guns-mat17.1-0 ; 
       add_guns-mat18.1-0 ; 
       add_guns-mat19.1-0 ; 
       add_guns-mat2.1-0 ; 
       add_guns-mat20.1-0 ; 
       add_guns-mat22.1-0 ; 
       add_guns-mat23.1-0 ; 
       add_guns-mat24.1-0 ; 
       add_guns-mat25.1-0 ; 
       add_guns-mat26.1-0 ; 
       add_guns-mat27.1-0 ; 
       add_guns-mat28.1-0 ; 
       add_guns-mat29.1-0 ; 
       add_guns-mat3.1-0 ; 
       add_guns-mat35.1-0 ; 
       add_guns-mat36.1-0 ; 
       add_guns-mat37.1-0 ; 
       add_guns-mat38.1-0 ; 
       add_guns-mat39.1-0 ; 
       add_guns-mat4.1-0 ; 
       add_guns-mat40.1-0 ; 
       add_guns-mat41.1-0 ; 
       add_guns-mat42.1-0 ; 
       add_guns-mat43.1-0 ; 
       add_guns-mat44.1-0 ; 
       add_guns-mat45.1-0 ; 
       add_guns-mat5.1-0 ; 
       add_guns-mat6.1-0 ; 
       add_guns-mat7.1-0 ; 
       add_guns-mat8.1-0 ; 
       add_guns-mat9.1-0 ; 
       add_guns-right.1-0 ; 
       add_guns-yellow1.1-0 ; 
       add_guns-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig07-cockpt.1-0 ; 
       fig07-cyl6.2-0 ; 
       fig07-fig07.8-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-LLl1.1-0 ; 
       fig07-LLl2.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rLL0.1-0 ; 
       fig07-rLL1.1-0 ; 
       fig07-rLL2.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-add_guns.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_guns-t2d1.1-0 ; 
       add_guns-t2d10.1-0 ; 
       add_guns-t2d11.1-0 ; 
       add_guns-t2d12.1-0 ; 
       add_guns-t2d13.1-0 ; 
       add_guns-t2d14.1-0 ; 
       add_guns-t2d15.1-0 ; 
       add_guns-t2d16.1-0 ; 
       add_guns-t2d17.1-0 ; 
       add_guns-t2d18.1-0 ; 
       add_guns-t2d19.1-0 ; 
       add_guns-t2d2.1-0 ; 
       add_guns-t2d21.1-0 ; 
       add_guns-t2d22.1-0 ; 
       add_guns-t2d23.1-0 ; 
       add_guns-t2d24.1-0 ; 
       add_guns-t2d25.1-0 ; 
       add_guns-t2d26.1-0 ; 
       add_guns-t2d27.1-0 ; 
       add_guns-t2d28.1-0 ; 
       add_guns-t2d3.1-0 ; 
       add_guns-t2d33.1-0 ; 
       add_guns-t2d34.1-0 ; 
       add_guns-t2d35.1-0 ; 
       add_guns-t2d36.1-0 ; 
       add_guns-t2d37.2-0 ; 
       add_guns-t2d38.4-0 ; 
       add_guns-t2d39.1-0 ; 
       add_guns-t2d4.1-0 ; 
       add_guns-t2d40.1-0 ; 
       add_guns-t2d41.1-0 ; 
       add_guns-t2d5.1-0 ; 
       add_guns-t2d6.1-0 ; 
       add_guns-t2d7.1-0 ; 
       add_guns-t2d8.1-0 ; 
       add_guns-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       22 21 110 ; 
       10 8 110 ; 
       9 8 110 ; 
       0 2 110 ; 
       37 2 110 ; 
       23 21 110 ; 
       1 38 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 14 110 ; 
       7 12 110 ; 
       8 4 110 ; 
       11 8 110 ; 
       12 13 110 ; 
       13 4 110 ; 
       14 13 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 4 110 ; 
       24 21 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 4 110 ; 
       32 4 110 ; 
       33 4 110 ; 
       34 4 110 ; 
       35 4 110 ; 
       36 35 110 ; 
       38 36 110 ; 
       39 38 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 37 300 ; 
       10 38 300 ; 
       9 39 300 ; 
       23 36 300 ; 
       1 35 300 ; 
       2 3 300 ; 
       3 44 300 ; 
       4 47 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       5 4 300 ; 
       5 13 300 ; 
       6 7 300 ; 
       7 28 300 ; 
       8 2 300 ; 
       8 6 300 ; 
       8 17 300 ; 
       8 27 300 ; 
       8 33 300 ; 
       12 29 300 ; 
       13 8 300 ; 
       14 30 300 ; 
       15 26 300 ; 
       16 25 300 ; 
       21 1 300 ; 
       21 40 300 ; 
       21 41 300 ; 
       21 42 300 ; 
       21 43 300 ; 
       25 24 300 ; 
       26 23 300 ; 
       31 5 300 ; 
       32 32 300 ; 
       33 45 300 ; 
       34 34 300 ; 
       35 46 300 ; 
       35 16 300 ; 
       36 0 300 ; 
       36 31 300 ; 
       38 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       33 28 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       40 31 401 ; 
       41 32 401 ; 
       42 33 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       37 27 401 ; 
       38 29 401 ; 
       39 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       22 SCHEM 31.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 59 -6 0 MPRFLG 0 ; 
       9 SCHEM 64 -6 0 MPRFLG 0 ; 
       0 SCHEM 66.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 69 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 29 -6 0 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 35.25 0 0 SRT 1 1 1 0.005999994 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 11.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 32.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 39 -4 0 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 MPRFLG 0 ; 
       8 SCHEM 55.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 52.75 -6 0 MPRFLG 0 ; 
       12 SCHEM 44 -6 0 MPRFLG 0 ; 
       13 SCHEM 42.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 54 -8 0 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 61.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 56.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 49 -6 0 MPRFLG 0 ; 
       20 SCHEM 46.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 22.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 20.25 -6 0 MPRFLG 0 ; 
       25 SCHEM 19 -8 0 MPRFLG 0 ; 
       26 SCHEM 21.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 MPRFLG 0 ; 
       28 SCHEM 26.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 34 -4 0 MPRFLG 0 ; 
       32 SCHEM 1.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 9 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 5.25 -4 0 MPRFLG 0 ; 
       36 SCHEM 5.25 -6 0 MPRFLG 0 ; 
       38 SCHEM 5.25 -8 0 MPRFLG 0 ; 
       39 SCHEM 4 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 8 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 65.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 70.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 38 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 65.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 38 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 8 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 8 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 18 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 50.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 53 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 65.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 43 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 45.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 43 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 8 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 0.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 65.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 8 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 5.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 28 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 33 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 10.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 35.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 8 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 65.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 38 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 8 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 8 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 65.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 65.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 18 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 50.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 53 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 43 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 45.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 43 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 8 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 28 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 65.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 33 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 33 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 33 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 33 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 10.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 70.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 65 65 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
