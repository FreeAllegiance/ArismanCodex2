SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       fig07-fig07.32-0 ; 
       fig07-rLL0.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       animation-cam_int1.6-0 ROOT ; 
       animation-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       animation-light1.6-0 ROOT ; 
       animation-light2.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       animation-black1.1-0 ; 
       animation-blue_pure1.1-0 ; 
       animation-blue_pure2.1-0 ; 
       animation-default1.1-0 ; 
       animation-glass1.1-0 ; 
       animation-left.1-0 ; 
       animation-mat1.1-0 ; 
       animation-mat10.1-0 ; 
       animation-mat11.1-0 ; 
       animation-mat12.1-0 ; 
       animation-mat13.1-0 ; 
       animation-mat14.1-0 ; 
       animation-mat15.1-0 ; 
       animation-mat16.1-0 ; 
       animation-mat17.1-0 ; 
       animation-mat18.1-0 ; 
       animation-mat19.1-0 ; 
       animation-mat2.1-0 ; 
       animation-mat20.1-0 ; 
       animation-mat22.1-0 ; 
       animation-mat23.1-0 ; 
       animation-mat24.1-0 ; 
       animation-mat25.1-0 ; 
       animation-mat26.1-0 ; 
       animation-mat27.1-0 ; 
       animation-mat293.1-0 ; 
       animation-mat294.1-0 ; 
       animation-mat295.1-0 ; 
       animation-mat296.1-0 ; 
       animation-mat297.1-0 ; 
       animation-mat298.1-0 ; 
       animation-mat3.1-0 ; 
       animation-mat35.1-0 ; 
       animation-mat36.1-0 ; 
       animation-mat37.1-0 ; 
       animation-mat38.1-0 ; 
       animation-mat39.1-0 ; 
       animation-mat4.1-0 ; 
       animation-mat40.1-0 ; 
       animation-mat41.1-0 ; 
       animation-mat42.1-0 ; 
       animation-mat43.1-0 ; 
       animation-mat44.1-0 ; 
       animation-mat45.1-0 ; 
       animation-mat5.1-0 ; 
       animation-mat6.1-0 ; 
       animation-mat7.1-0 ; 
       animation-mat8.1-0 ; 
       animation-mat9.1-0 ; 
       animation-right.1-0 ; 
       animation-yellow1.1-0 ; 
       animation-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       fig07-cockpt.1-0 ; 
       fig07-fig07.26-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LL1.1-0 ; 
       fig07-LL10.1-0 ; 
       fig07-LL11.1-0 ; 
       fig07-LL2.1-0 ; 
       fig07-LL3.1-0 ; 
       fig07-LL8.1-0 ; 
       fig07-LL9.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-lthrust.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepemt1.1-0 ; 
       fig07-lwepemt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-missemt.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rLL0.9-0 ROOT ; 
       fig07-rLL1.1-0 ; 
       fig07-rLL2.1-0 ; 
       fig07-rthrust.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepemt1.1-0 ; 
       fig07-rwepemt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-twepemt.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-animation.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       animation-t2d1.1-0 ; 
       animation-t2d10.1-0 ; 
       animation-t2d11.1-0 ; 
       animation-t2d12.1-0 ; 
       animation-t2d13.1-0 ; 
       animation-t2d14.1-0 ; 
       animation-t2d15.1-0 ; 
       animation-t2d16.1-0 ; 
       animation-t2d17.1-0 ; 
       animation-t2d18.1-0 ; 
       animation-t2d19.1-0 ; 
       animation-t2d2.1-0 ; 
       animation-t2d21.1-0 ; 
       animation-t2d22.1-0 ; 
       animation-t2d23.1-0 ; 
       animation-t2d24.1-0 ; 
       animation-t2d25.1-0 ; 
       animation-t2d26.1-0 ; 
       animation-t2d3.1-0 ; 
       animation-t2d33.1-0 ; 
       animation-t2d34.1-0 ; 
       animation-t2d35.1-0 ; 
       animation-t2d36.1-0 ; 
       animation-t2d37.1-0 ; 
       animation-t2d38.1-0 ; 
       animation-t2d39.1-0 ; 
       animation-t2d4.1-0 ; 
       animation-t2d40.1-0 ; 
       animation-t2d41.1-0 ; 
       animation-t2d42.2-0 ; 
       animation-t2d43.2-0 ; 
       animation-t2d44.2-0 ; 
       animation-t2d45.2-0 ; 
       animation-t2d46.2-0 ; 
       animation-t2d47.2-0 ; 
       animation-t2d5.1-0 ; 
       animation-t2d6.1-0 ; 
       animation-t2d7.1-0 ; 
       animation-t2d8.1-0 ; 
       animation-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 20 110 ; 
       6 18 110 ; 
       7 3 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 29 110 ; 
       11 10 110 ; 
       12 17 110 ; 
       13 12 110 ; 
       14 15 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 16 110 ; 
       18 19 110 ; 
       19 3 110 ; 
       20 19 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 8 110 ; 
       25 9 110 ; 
       26 7 110 ; 
       27 7 110 ; 
       28 7 110 ; 
       29 3 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       33 32 110 ; 
       34 33 110 ; 
       35 29 110 ; 
       36 29 110 ; 
       37 29 110 ; 
       38 30 110 ; 
       39 31 110 ; 
       40 29 110 ; 
       41 29 110 ; 
       42 3 110 ; 
       43 3 110 ; 
       44 3 110 ; 
       45 3 110 ; 
       46 3 110 ; 
       47 46 110 ; 
       48 50 110 ; 
       49 1 110 ; 
       50 47 110 ; 
       51 48 110 ; 
       52 50 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 48 300 ; 
       3 51 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 4 300 ; 
       4 13 300 ; 
       5 7 300 ; 
       6 32 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 17 300 ; 
       7 31 300 ; 
       7 37 300 ; 
       8 43 300 ; 
       9 42 300 ; 
       11 25 300 ; 
       12 29 300 ; 
       13 30 300 ; 
       14 26 300 ; 
       15 27 300 ; 
       17 28 300 ; 
       18 33 300 ; 
       19 8 300 ; 
       20 34 300 ; 
       29 1 300 ; 
       29 44 300 ; 
       29 45 300 ; 
       29 46 300 ; 
       29 47 300 ; 
       30 41 300 ; 
       31 40 300 ; 
       33 24 300 ; 
       34 23 300 ; 
       42 5 300 ; 
       43 36 300 ; 
       44 49 300 ; 
       45 38 300 ; 
       46 50 300 ; 
       46 16 300 ; 
       47 0 300 ; 
       47 35 300 ; 
       48 39 300 ; 
       50 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       32 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 29 401 ; 
       26 30 401 ; 
       27 31 401 ; 
       28 32 401 ; 
       29 33 401 ; 
       30 34 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       35 22 401 ; 
       37 26 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       41 25 401 ; 
       42 27 401 ; 
       43 28 401 ; 
       44 35 401 ; 
       45 36 401 ; 
       46 37 401 ; 
       47 38 401 ; 
       48 39 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 SRT 1 1 1 0.0430954 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -12 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 MPRFLG 0 ; 
       19 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60 -6 0 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 50 -6 0 MPRFLG 0 ; 
       28 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       30 SCHEM 30 -6 0 MPRFLG 0 ; 
       31 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 75 0 0 WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0.02199998 4.766652e-007 3.002507e-008 2.462933 1.150801 -0.2582069 MPRFLG 0 ; 
       33 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 20 -6 0 MPRFLG 0 ; 
       37 SCHEM 25 -6 0 MPRFLG 0 ; 
       38 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 15 -6 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 35 -4 0 MPRFLG 0 ; 
       43 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       44 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       45 SCHEM 10 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       46 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       47 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       48 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       49 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       51 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 74 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 69 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 76.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 54 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
