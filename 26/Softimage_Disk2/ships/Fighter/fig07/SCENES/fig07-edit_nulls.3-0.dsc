SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig07-fig07.38-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       edit_nulls-cam_int1.3-0 ROOT ; 
       edit_nulls-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       edit_nulls-light1.3-0 ROOT ; 
       edit_nulls-light2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       edit_nulls-black1.1-0 ; 
       edit_nulls-blue_pure1.1-0 ; 
       edit_nulls-blue_pure2.1-0 ; 
       edit_nulls-default1.1-0 ; 
       edit_nulls-glass1.1-0 ; 
       edit_nulls-left.1-0 ; 
       edit_nulls-mat1.1-0 ; 
       edit_nulls-mat10.1-0 ; 
       edit_nulls-mat11.1-0 ; 
       edit_nulls-mat12.1-0 ; 
       edit_nulls-mat13.1-0 ; 
       edit_nulls-mat14.1-0 ; 
       edit_nulls-mat15.1-0 ; 
       edit_nulls-mat16.1-0 ; 
       edit_nulls-mat17.1-0 ; 
       edit_nulls-mat18.1-0 ; 
       edit_nulls-mat19.1-0 ; 
       edit_nulls-mat2.1-0 ; 
       edit_nulls-mat20.1-0 ; 
       edit_nulls-mat22.1-0 ; 
       edit_nulls-mat23.1-0 ; 
       edit_nulls-mat24.1-0 ; 
       edit_nulls-mat25.1-0 ; 
       edit_nulls-mat3.1-0 ; 
       edit_nulls-mat35.1-0 ; 
       edit_nulls-mat36.1-0 ; 
       edit_nulls-mat37.1-0 ; 
       edit_nulls-mat38.1-0 ; 
       edit_nulls-mat39.1-0 ; 
       edit_nulls-mat4.1-0 ; 
       edit_nulls-mat40.1-0 ; 
       edit_nulls-mat41.1-0 ; 
       edit_nulls-mat42.1-0 ; 
       edit_nulls-mat43.1-0 ; 
       edit_nulls-mat44.1-0 ; 
       edit_nulls-mat45.1-0 ; 
       edit_nulls-mat5.1-0 ; 
       edit_nulls-mat6.1-0 ; 
       edit_nulls-mat7.1-0 ; 
       edit_nulls-mat8.1-0 ; 
       edit_nulls-mat9.1-0 ; 
       edit_nulls-right.1-0 ; 
       edit_nulls-yellow1.1-0 ; 
       edit_nulls-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       edit_nulls-cube1.1-0 ROOT ; 
       fig07-cockpt.1-0 ; 
       fig07-fig07.35-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-lthrust.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepemt1.1-0 ; 
       fig07-lwepemt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-missemt.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rthrust.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepemt1.1-0 ; 
       fig07-rwepemt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-smoke.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-twepemt.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-edit_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       edit_nulls-t2d1.1-0 ; 
       edit_nulls-t2d10.1-0 ; 
       edit_nulls-t2d11.1-0 ; 
       edit_nulls-t2d12.1-0 ; 
       edit_nulls-t2d13.1-0 ; 
       edit_nulls-t2d14.1-0 ; 
       edit_nulls-t2d15.1-0 ; 
       edit_nulls-t2d16.1-0 ; 
       edit_nulls-t2d17.1-0 ; 
       edit_nulls-t2d18.1-0 ; 
       edit_nulls-t2d19.1-0 ; 
       edit_nulls-t2d2.1-0 ; 
       edit_nulls-t2d21.1-0 ; 
       edit_nulls-t2d22.1-0 ; 
       edit_nulls-t2d23.1-0 ; 
       edit_nulls-t2d24.1-0 ; 
       edit_nulls-t2d3.1-0 ; 
       edit_nulls-t2d33.1-0 ; 
       edit_nulls-t2d34.1-0 ; 
       edit_nulls-t2d35.1-0 ; 
       edit_nulls-t2d36.1-0 ; 
       edit_nulls-t2d37.1-0 ; 
       edit_nulls-t2d38.1-0 ; 
       edit_nulls-t2d39.1-0 ; 
       edit_nulls-t2d4.1-0 ; 
       edit_nulls-t2d40.1-0 ; 
       edit_nulls-t2d41.1-0 ; 
       edit_nulls-t2d5.1-0 ; 
       edit_nulls-t2d6.1-0 ; 
       edit_nulls-t2d7.1-0 ; 
       edit_nulls-t2d8.1-0 ; 
       edit_nulls-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       32 2 110 ; 
       1 2 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 13 110 ; 
       7 11 110 ; 
       8 4 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 12 110 ; 
       12 4 110 ; 
       13 12 110 ; 
       14 2 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 4 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 2 110 ; 
       26 22 110 ; 
       27 22 110 ; 
       28 23 110 ; 
       29 24 110 ; 
       30 22 110 ; 
       31 22 110 ; 
       33 4 110 ; 
       34 4 110 ; 
       35 4 110 ; 
       36 4 110 ; 
       37 4 110 ; 
       38 37 110 ; 
       39 41 110 ; 
       40 2 110 ; 
       41 38 110 ; 
       42 39 110 ; 
       43 41 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       3 40 300 ; 
       4 43 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       5 4 300 ; 
       5 13 300 ; 
       6 7 300 ; 
       7 24 300 ; 
       8 2 300 ; 
       8 6 300 ; 
       8 17 300 ; 
       8 23 300 ; 
       8 29 300 ; 
       9 35 300 ; 
       10 34 300 ; 
       11 25 300 ; 
       12 8 300 ; 
       13 26 300 ; 
       22 1 300 ; 
       22 36 300 ; 
       22 37 300 ; 
       22 38 300 ; 
       22 39 300 ; 
       23 33 300 ; 
       24 32 300 ; 
       33 5 300 ; 
       34 28 300 ; 
       35 41 300 ; 
       36 30 300 ; 
       37 42 300 ; 
       37 16 300 ; 
       38 0 300 ; 
       38 27 300 ; 
       39 31 300 ; 
       41 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       29 24 401 ; 
       31 21 401 ; 
       32 22 401 ; 
       33 23 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 76.21788 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 78.71788 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       32 SCHEM 71.21788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 66.21788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.25 0 0 SRT 0.2512424 0.2512424 0.2512424 0.005999994 0 0 0 0 0.4867396 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 40 -8 0 MPRFLG 0 ; 
       8 SCHEM 50 -4 0 MPRFLG 0 ; 
       9 SCHEM 55 -6 0 MPRFLG 0 ; 
       10 SCHEM 50 -6 0 MPRFLG 0 ; 
       11 SCHEM 40 -6 0 MPRFLG 0 ; 
       12 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 63.71788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -6 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 MPRFLG 0 ; 
       25 SCHEM 61.21788 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 MPRFLG 0 ; 
       27 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 15 -6 0 MPRFLG 0 ; 
       31 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 MPRFLG 0 ; 
       34 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 10 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       37 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       38 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       40 SCHEM 68.71788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       42 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 5 -10 0 MPRFLG 0 ; 
       0 SCHEM 73.71788 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.71788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 72.71788 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
