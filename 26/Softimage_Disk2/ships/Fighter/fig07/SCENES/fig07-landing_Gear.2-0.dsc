SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       fig07-fig07.25-0 ; 
       fig07-rLL0.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       landing_Gear-cam_int1.2-0 ROOT ; 
       landing_Gear-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       landing_Gear-light1.2-0 ROOT ; 
       landing_Gear-light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       landing_Gear-black1.1-0 ; 
       landing_Gear-blue_pure1.1-0 ; 
       landing_Gear-blue_pure2.1-0 ; 
       landing_Gear-default1.1-0 ; 
       landing_Gear-glass1.1-0 ; 
       landing_Gear-left.1-0 ; 
       landing_Gear-mat1.1-0 ; 
       landing_Gear-mat10.1-0 ; 
       landing_Gear-mat11.1-0 ; 
       landing_Gear-mat12.1-0 ; 
       landing_Gear-mat13.1-0 ; 
       landing_Gear-mat14.1-0 ; 
       landing_Gear-mat15.1-0 ; 
       landing_Gear-mat16.1-0 ; 
       landing_Gear-mat17.1-0 ; 
       landing_Gear-mat18.1-0 ; 
       landing_Gear-mat19.1-0 ; 
       landing_Gear-mat2.1-0 ; 
       landing_Gear-mat20.1-0 ; 
       landing_Gear-mat22.1-0 ; 
       landing_Gear-mat23.1-0 ; 
       landing_Gear-mat24.1-0 ; 
       landing_Gear-mat25.1-0 ; 
       landing_Gear-mat26.1-0 ; 
       landing_Gear-mat27.1-0 ; 
       landing_Gear-mat293.1-0 ; 
       landing_Gear-mat294.1-0 ; 
       landing_Gear-mat295.1-0 ; 
       landing_Gear-mat3.1-0 ; 
       landing_Gear-mat35.1-0 ; 
       landing_Gear-mat36.1-0 ; 
       landing_Gear-mat37.1-0 ; 
       landing_Gear-mat38.1-0 ; 
       landing_Gear-mat39.1-0 ; 
       landing_Gear-mat4.1-0 ; 
       landing_Gear-mat40.1-0 ; 
       landing_Gear-mat41.1-0 ; 
       landing_Gear-mat42.1-0 ; 
       landing_Gear-mat43.1-0 ; 
       landing_Gear-mat44.1-0 ; 
       landing_Gear-mat45.1-0 ; 
       landing_Gear-mat5.1-0 ; 
       landing_Gear-mat6.1-0 ; 
       landing_Gear-mat7.1-0 ; 
       landing_Gear-mat8.1-0 ; 
       landing_Gear-mat9.1-0 ; 
       landing_Gear-right.1-0 ; 
       landing_Gear-yellow1.1-0 ; 
       landing_Gear-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       fig07-cockpt.1-0 ; 
       fig07-fig07.19-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LL1.1-0 ; 
       fig07-LL2.1-0 ; 
       fig07-LL3.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-lthrust.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepemt1.1-0 ; 
       fig07-lwepemt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-missemt.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rLL0.2-0 ROOT ; 
       fig07-rLL1.1-0 ; 
       fig07-rLL2.1-0 ; 
       fig07-rthrust.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepemt1.1-0 ; 
       fig07-rwepemt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-twepemt.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-landing_Gear.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       landing_Gear-t2d1.1-0 ; 
       landing_Gear-t2d10.1-0 ; 
       landing_Gear-t2d11.1-0 ; 
       landing_Gear-t2d12.1-0 ; 
       landing_Gear-t2d13.1-0 ; 
       landing_Gear-t2d14.1-0 ; 
       landing_Gear-t2d15.1-0 ; 
       landing_Gear-t2d16.1-0 ; 
       landing_Gear-t2d17.1-0 ; 
       landing_Gear-t2d18.1-0 ; 
       landing_Gear-t2d19.1-0 ; 
       landing_Gear-t2d2.1-0 ; 
       landing_Gear-t2d21.1-0 ; 
       landing_Gear-t2d22.1-0 ; 
       landing_Gear-t2d23.1-0 ; 
       landing_Gear-t2d24.1-0 ; 
       landing_Gear-t2d25.1-0 ; 
       landing_Gear-t2d26.1-0 ; 
       landing_Gear-t2d3.1-0 ; 
       landing_Gear-t2d33.1-0 ; 
       landing_Gear-t2d34.1-0 ; 
       landing_Gear-t2d35.1-0 ; 
       landing_Gear-t2d36.1-0 ; 
       landing_Gear-t2d37.1-0 ; 
       landing_Gear-t2d38.1-0 ; 
       landing_Gear-t2d39.1-0 ; 
       landing_Gear-t2d4.1-0 ; 
       landing_Gear-t2d40.1-0 ; 
       landing_Gear-t2d41.1-0 ; 
       landing_Gear-t2d42.1-0 ; 
       landing_Gear-t2d43.1-0 ; 
       landing_Gear-t2d44.1-0 ; 
       landing_Gear-t2d5.1-0 ; 
       landing_Gear-t2d6.1-0 ; 
       landing_Gear-t2d7.1-0 ; 
       landing_Gear-t2d8.1-0 ; 
       landing_Gear-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 16 110 ; 
       6 14 110 ; 
       7 3 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 10 110 ; 
       12 13 110 ; 
       13 11 110 ; 
       14 15 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 8 110 ; 
       21 9 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       25 3 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       29 28 110 ; 
       30 29 110 ; 
       31 25 110 ; 
       32 25 110 ; 
       33 25 110 ; 
       34 26 110 ; 
       35 27 110 ; 
       36 25 110 ; 
       37 25 110 ; 
       38 3 110 ; 
       39 3 110 ; 
       40 3 110 ; 
       41 3 110 ; 
       42 3 110 ; 
       43 42 110 ; 
       44 46 110 ; 
       45 1 110 ; 
       46 43 110 ; 
       47 44 110 ; 
       48 46 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 45 300 ; 
       3 48 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 4 300 ; 
       4 13 300 ; 
       5 7 300 ; 
       6 29 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 17 300 ; 
       7 28 300 ; 
       7 34 300 ; 
       8 40 300 ; 
       9 39 300 ; 
       11 25 300 ; 
       12 26 300 ; 
       13 27 300 ; 
       14 30 300 ; 
       15 8 300 ; 
       16 31 300 ; 
       25 1 300 ; 
       25 41 300 ; 
       25 42 300 ; 
       25 43 300 ; 
       25 44 300 ; 
       26 38 300 ; 
       27 37 300 ; 
       29 24 300 ; 
       30 23 300 ; 
       38 5 300 ; 
       39 33 300 ; 
       40 46 300 ; 
       41 35 300 ; 
       42 47 300 ; 
       42 16 300 ; 
       43 0 300 ; 
       43 32 300 ; 
       44 36 300 ; 
       46 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       28 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 29 401 ; 
       26 30 401 ; 
       27 31 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 22 401 ; 
       34 26 401 ; 
       36 23 401 ; 
       37 24 401 ; 
       38 25 401 ; 
       39 27 401 ; 
       40 28 401 ; 
       41 32 401 ; 
       42 33 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       45 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.25 0 0 SRT 1 1 1 0.04363323 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 55 -4 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 40 -6 0 MPRFLG 0 ; 
       17 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 60 -6 0 MPRFLG 0 ; 
       19 SCHEM 55 -6 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -6 0 MPRFLG 0 ; 
       23 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 30 -6 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 72.5 0 0 WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0.02199998 4.766652e-007 3.002507e-008 2.462933 0.4809282 -0.2582069 MPRFLG 0 ; 
       29 SCHEM 72.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20 -6 0 MPRFLG 0 ; 
       33 SCHEM 25 -6 0 MPRFLG 0 ; 
       34 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 15 -6 0 MPRFLG 0 ; 
       37 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       40 SCHEM 35 -4 0 MPRFLG 0 ; 
       41 SCHEM 10 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       42 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       43 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       44 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       45 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       47 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 71.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 57.25439 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -10.75339 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 57.25439 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 54 -12.75339 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 58 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
