SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig07-fig07.21-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       new_nulls-cam_int1.2-0 ROOT ; 
       new_nulls-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       new_nulls-black1.1-0 ; 
       new_nulls-blue_pure1.1-0 ; 
       new_nulls-blue_pure2.1-0 ; 
       new_nulls-default1.1-0 ; 
       new_nulls-glass1.1-0 ; 
       new_nulls-left.1-0 ; 
       new_nulls-mat1.1-0 ; 
       new_nulls-mat10.1-0 ; 
       new_nulls-mat11.1-0 ; 
       new_nulls-mat12.1-0 ; 
       new_nulls-mat13.1-0 ; 
       new_nulls-mat14.1-0 ; 
       new_nulls-mat15.1-0 ; 
       new_nulls-mat16.1-0 ; 
       new_nulls-mat17.1-0 ; 
       new_nulls-mat18.1-0 ; 
       new_nulls-mat19.1-0 ; 
       new_nulls-mat2.1-0 ; 
       new_nulls-mat20.1-0 ; 
       new_nulls-mat22.1-0 ; 
       new_nulls-mat23.1-0 ; 
       new_nulls-mat24.1-0 ; 
       new_nulls-mat25.1-0 ; 
       new_nulls-mat26.1-0 ; 
       new_nulls-mat27.1-0 ; 
       new_nulls-mat28.1-0 ; 
       new_nulls-mat29.1-0 ; 
       new_nulls-mat3.1-0 ; 
       new_nulls-mat35.1-0 ; 
       new_nulls-mat36.1-0 ; 
       new_nulls-mat37.1-0 ; 
       new_nulls-mat38.1-0 ; 
       new_nulls-mat39.1-0 ; 
       new_nulls-mat4.1-0 ; 
       new_nulls-mat40.1-0 ; 
       new_nulls-mat41.1-0 ; 
       new_nulls-mat42.1-0 ; 
       new_nulls-mat43.1-0 ; 
       new_nulls-mat44.1-0 ; 
       new_nulls-mat45.1-0 ; 
       new_nulls-mat5.1-0 ; 
       new_nulls-mat6.1-0 ; 
       new_nulls-mat7.1-0 ; 
       new_nulls-mat8.1-0 ; 
       new_nulls-mat9.1-0 ; 
       new_nulls-right.1-0 ; 
       new_nulls-yellow1.1-0 ; 
       new_nulls-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       fig07-cockpt.1-0 ; 
       fig07-fig07.16-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-LLl1.1-0 ; 
       fig07-LLl2.1-0 ; 
       fig07-lthrust.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepemt1.1-0 ; 
       fig07-lwepemt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-missemt.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rLL0.1-0 ; 
       fig07-rLL1.1-0 ; 
       fig07-rLL2.1-0 ; 
       fig07-rthrust.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepemt1.1-0 ; 
       fig07-rwepemt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-twepemt.1-0 ; 
       fig07-wepatt.1-0 ; 
       new_nulls-cube10.1-0 ROOT ; 
       new_nulls-cube3.1-0 ROOT ; 
       new_nulls-cube4.1-0 ROOT ; 
       new_nulls-cube5.1-0 ROOT ; 
       new_nulls-cube6.1-0 ROOT ; 
       new_nulls-cube7.1-0 ROOT ; 
       new_nulls-cube8.1-0 ROOT ; 
       new_nulls-cube9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-new_nulls.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       new_nulls-t2d1.1-0 ; 
       new_nulls-t2d10.1-0 ; 
       new_nulls-t2d11.1-0 ; 
       new_nulls-t2d12.1-0 ; 
       new_nulls-t2d13.1-0 ; 
       new_nulls-t2d14.1-0 ; 
       new_nulls-t2d15.1-0 ; 
       new_nulls-t2d16.1-0 ; 
       new_nulls-t2d17.1-0 ; 
       new_nulls-t2d18.1-0 ; 
       new_nulls-t2d19.1-0 ; 
       new_nulls-t2d2.1-0 ; 
       new_nulls-t2d21.1-0 ; 
       new_nulls-t2d22.1-0 ; 
       new_nulls-t2d23.1-0 ; 
       new_nulls-t2d24.1-0 ; 
       new_nulls-t2d25.1-0 ; 
       new_nulls-t2d26.1-0 ; 
       new_nulls-t2d27.1-0 ; 
       new_nulls-t2d28.1-0 ; 
       new_nulls-t2d3.1-0 ; 
       new_nulls-t2d33.1-0 ; 
       new_nulls-t2d34.1-0 ; 
       new_nulls-t2d35.1-0 ; 
       new_nulls-t2d36.1-0 ; 
       new_nulls-t2d37.1-0 ; 
       new_nulls-t2d38.1-0 ; 
       new_nulls-t2d39.1-0 ; 
       new_nulls-t2d4.1-0 ; 
       new_nulls-t2d40.1-0 ; 
       new_nulls-t2d41.1-0 ; 
       new_nulls-t2d5.1-0 ; 
       new_nulls-t2d6.1-0 ; 
       new_nulls-t2d7.1-0 ; 
       new_nulls-t2d8.1-0 ; 
       new_nulls-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 13 110 ; 
       6 11 110 ; 
       7 3 110 ; 
       44 1 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 12 110 ; 
       12 3 110 ; 
       13 12 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 8 110 ; 
       20 9 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 3 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 24 110 ; 
       31 24 110 ; 
       32 24 110 ; 
       33 25 110 ; 
       34 26 110 ; 
       35 24 110 ; 
       36 24 110 ; 
       37 3 110 ; 
       38 3 110 ; 
       39 3 110 ; 
       40 3 110 ; 
       41 3 110 ; 
       42 41 110 ; 
       43 45 110 ; 
       45 42 110 ; 
       46 43 110 ; 
       47 45 110 ; 
       0 1 110 ; 
       49 23 111 ; 
       49 23 114 ; 
       50 19 111 ; 
       50 19 114 ; 
       51 20 111 ; 
       51 20 114 ; 
       52 16 111 ; 
       52 16 114 ; 
       53 34 111 ; 
       53 34 114 ; 
       54 33 111 ; 
       54 33 114 ; 
       55 30 111 ; 
       55 30 114 ; 
       48 46 111 ; 
       48 46 114 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 44 300 ; 
       3 47 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 4 300 ; 
       4 13 300 ; 
       5 7 300 ; 
       6 28 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 17 300 ; 
       7 27 300 ; 
       7 33 300 ; 
       8 39 300 ; 
       9 38 300 ; 
       11 29 300 ; 
       12 8 300 ; 
       13 30 300 ; 
       14 26 300 ; 
       15 25 300 ; 
       24 1 300 ; 
       24 40 300 ; 
       24 41 300 ; 
       24 42 300 ; 
       24 43 300 ; 
       25 37 300 ; 
       26 36 300 ; 
       28 24 300 ; 
       29 23 300 ; 
       37 5 300 ; 
       38 32 300 ; 
       39 45 300 ; 
       40 34 300 ; 
       41 46 300 ; 
       41 16 300 ; 
       42 0 300 ; 
       42 31 300 ; 
       43 35 300 ; 
       45 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       33 28 401 ; 
       35 25 401 ; 
       36 26 401 ; 
       37 27 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
       41 32 401 ; 
       42 33 401 ; 
       43 34 401 ; 
       44 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 40 0 0 SRT 1 1 1 0.005999994 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 45 -8 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       44 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 70 -6 0 MPRFLG 0 ; 
       9 SCHEM 65 -6 0 MPRFLG 0 ; 
       10 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 45 -6 0 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 -6 0 MPRFLG 0 ; 
       22 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -4 0 MPRFLG 0 ; 
       25 SCHEM 35 -6 0 MPRFLG 0 ; 
       26 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       28 SCHEM 20 -8 0 MPRFLG 0 ; 
       29 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 MPRFLG 0 ; 
       32 SCHEM 30 -6 0 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 15 -6 0 MPRFLG 0 ; 
       36 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       39 SCHEM 40 -4 0 MPRFLG 0 ; 
       40 SCHEM 10 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       42 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       43 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       45 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       46 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 5 -10 0 MPRFLG 0 ; 
       0 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 72.96995 -7.146122 0 USR WIRECOL 1 7 SRT 1 1 1 0 0 0 0 0.126528 1.825619 MPRFLG 0 ; 
       50 SCHEM 70.55287 -9.030725 0 USR WIRECOL 1 7 SRT 1 1 1 -7.264185e-008 -1.348312e-008 5.668735e-008 5.130781 0.3091793 8.957738 MPRFLG 0 ; 
       51 SCHEM 65.35856 -8.863768 0 USR WIRECOL 1 7 SRT 1 1 1 -7.264185e-008 -1.348312e-008 5.668735e-008 3.991013 0.1110099 8.956916 MPRFLG 0 ; 
       52 SCHEM 50.10945 -7.162434 0 USR WIRECOL 1 7 SRT 1 1 1 0 -4.263255e-013 1.295519e-017 3.46152 0.660436 -0.942728 MPRFLG 0 ; 
       53 SCHEM 33.06808 -8.853252 0 USR WIRECOL 1 7 SRT 1 1 1 0.005999994 0 0 -5.128866 0.3091775 8.959274 MPRFLG 0 ; 
       54 SCHEM 35.56808 -8.853252 0 USR WIRECOL 1 7 SRT 1 1 1 0.005999994 0 0 -3.990951 0.1110114 8.960812 MPRFLG 0 ; 
       55 SCHEM 27.70621 -7.304927 0 USR WIRECOL 1 7 SRT 1 1 1 9.313058e-010 -4.263255e-013 1.295392e-017 3.46152 0.660436 -0.9427279 MPRFLG 0 ; 
       48 SCHEM 7.576819 -12.78362 0 USR WIRECOL 1 7 SRT 1 1 1 0.005999994 0 0 -1.810901e-006 4.024836 4.004941 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 79 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 79 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
