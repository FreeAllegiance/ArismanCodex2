SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig07-fig07.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       reduce-cam_int1.1-0 ROOT ; 
       reduce-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       reduce-light1.1-0 ROOT ; 
       reduce-light2.1-0 ROOT ; 
       reduce-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       reduce-black1.1-0 ; 
       reduce-blue_pure1.1-0 ; 
       reduce-blue_pure2.1-0 ; 
       reduce-default1.1-0 ; 
       reduce-glass1.1-0 ; 
       reduce-left.1-0 ; 
       reduce-mat1.1-0 ; 
       reduce-mat10.1-0 ; 
       reduce-mat11.1-0 ; 
       reduce-mat12.1-0 ; 
       reduce-mat13.1-0 ; 
       reduce-mat14.1-0 ; 
       reduce-mat15.1-0 ; 
       reduce-mat16.1-0 ; 
       reduce-mat17.1-0 ; 
       reduce-mat18.1-0 ; 
       reduce-mat19.1-0 ; 
       reduce-mat2.1-0 ; 
       reduce-mat20.1-0 ; 
       reduce-mat22.1-0 ; 
       reduce-mat23.1-0 ; 
       reduce-mat24.1-0 ; 
       reduce-mat25.1-0 ; 
       reduce-mat26.1-0 ; 
       reduce-mat27.1-0 ; 
       reduce-mat28.1-0 ; 
       reduce-mat29.1-0 ; 
       reduce-mat3.1-0 ; 
       reduce-mat35.1-0 ; 
       reduce-mat36.1-0 ; 
       reduce-mat37.1-0 ; 
       reduce-mat38.1-0 ; 
       reduce-mat39.1-0 ; 
       reduce-mat4.1-0 ; 
       reduce-mat40.1-0 ; 
       reduce-mat5.1-0 ; 
       reduce-mat6.1-0 ; 
       reduce-mat7.1-0 ; 
       reduce-mat8.1-0 ; 
       reduce-mat9.1-0 ; 
       reduce-right.1-0 ; 
       reduce-yellow1.1-0 ; 
       reduce-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       fig07-fig07.2-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-LLl1.1-0 ; 
       fig07-LLl2.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rLL0.1-0 ; 
       fig07-rLL1.1-0 ; 
       fig07-rLL2.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-reduce.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       reduce-t2d1.1-0 ; 
       reduce-t2d10.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d15.1-0 ; 
       reduce-t2d16.1-0 ; 
       reduce-t2d17.1-0 ; 
       reduce-t2d18.1-0 ; 
       reduce-t2d19.1-0 ; 
       reduce-t2d2.1-0 ; 
       reduce-t2d21.1-0 ; 
       reduce-t2d22.1-0 ; 
       reduce-t2d23.1-0 ; 
       reduce-t2d24.1-0 ; 
       reduce-t2d25.1-0 ; 
       reduce-t2d26.1-0 ; 
       reduce-t2d27.1-0 ; 
       reduce-t2d28.1-0 ; 
       reduce-t2d3.1-0 ; 
       reduce-t2d33.1-0 ; 
       reduce-t2d34.1-0 ; 
       reduce-t2d35.1-0 ; 
       reduce-t2d36.1-0 ; 
       reduce-t2d4.1-0 ; 
       reduce-t2d5.1-0 ; 
       reduce-t2d6.1-0 ; 
       reduce-t2d7.1-0 ; 
       reduce-t2d8.1-0 ; 
       reduce-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 10 110 ; 
       5 8 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 2 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 39 300 ; 
       2 42 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       3 4 300 ; 
       3 13 300 ; 
       4 7 300 ; 
       5 28 300 ; 
       6 2 300 ; 
       6 6 300 ; 
       6 17 300 ; 
       6 27 300 ; 
       6 33 300 ; 
       8 29 300 ; 
       9 8 300 ; 
       10 30 300 ; 
       11 26 300 ; 
       12 25 300 ; 
       17 1 300 ; 
       17 35 300 ; 
       17 36 300 ; 
       17 37 300 ; 
       17 38 300 ; 
       19 24 300 ; 
       20 23 300 ; 
       25 5 300 ; 
       26 32 300 ; 
       27 40 300 ; 
       28 34 300 ; 
       29 41 300 ; 
       29 16 300 ; 
       30 0 300 ; 
       30 31 300 ; 
       31 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       33 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.5 0 0 SRT 1 1 1 0.04363323 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 34 -8 0 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 45.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 45.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 35.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 34 -6 0 MPRFLG 0 ; 
       11 SCHEM 46.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 44 -8 0 MPRFLG 0 ; 
       13 SCHEM 51.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 MPRFLG 0 ; 
       15 SCHEM 41.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 39 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 17.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 24 -6 0 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 MPRFLG 0 ; 
       25 SCHEM 26.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       27 SCHEM 29 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       29 SCHEM 4 -4 0 MPRFLG 0 ; 
       30 SCHEM 4 -6 0 MPRFLG 0 ; 
       31 SCHEM 4 -8 0 MPRFLG 0 ; 
       32 SCHEM 4 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 53 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 53 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 53 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 33 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 53 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 18 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 15.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 43 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 45.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 53 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 35.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 38 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 35.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 0.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 53 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 5.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 25.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 8 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 28 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 5.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 53 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 53 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 33 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 38 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 53 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 53 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 18 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 15.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 43 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 45.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 53 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 35.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 38 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 35.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 5.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 53 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 25.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 25.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 25.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 25.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 8 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 53 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 65 1 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
