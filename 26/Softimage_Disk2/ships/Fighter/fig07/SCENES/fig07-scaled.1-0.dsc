SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig07-fig07.35-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       scaled-cam_int1.1-0 ROOT ; 
       scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       scaled-light1.1-0 ROOT ; 
       scaled-light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       scaled-black1.1-0 ; 
       scaled-blue_pure1.1-0 ; 
       scaled-blue_pure2.1-0 ; 
       scaled-default1.1-0 ; 
       scaled-glass1.1-0 ; 
       scaled-left.1-0 ; 
       scaled-mat1.1-0 ; 
       scaled-mat10.1-0 ; 
       scaled-mat11.1-0 ; 
       scaled-mat12.1-0 ; 
       scaled-mat13.1-0 ; 
       scaled-mat14.1-0 ; 
       scaled-mat15.1-0 ; 
       scaled-mat16.1-0 ; 
       scaled-mat17.1-0 ; 
       scaled-mat18.1-0 ; 
       scaled-mat19.1-0 ; 
       scaled-mat2.1-0 ; 
       scaled-mat20.1-0 ; 
       scaled-mat22.1-0 ; 
       scaled-mat23.1-0 ; 
       scaled-mat24.1-0 ; 
       scaled-mat25.1-0 ; 
       scaled-mat293.1-0 ; 
       scaled-mat294.1-0 ; 
       scaled-mat295.1-0 ; 
       scaled-mat296.1-0 ; 
       scaled-mat297.1-0 ; 
       scaled-mat298.1-0 ; 
       scaled-mat3.1-0 ; 
       scaled-mat35.1-0 ; 
       scaled-mat36.1-0 ; 
       scaled-mat37.1-0 ; 
       scaled-mat38.1-0 ; 
       scaled-mat39.1-0 ; 
       scaled-mat4.1-0 ; 
       scaled-mat40.1-0 ; 
       scaled-mat41.1-0 ; 
       scaled-mat42.1-0 ; 
       scaled-mat43.1-0 ; 
       scaled-mat44.1-0 ; 
       scaled-mat45.1-0 ; 
       scaled-mat5.1-0 ; 
       scaled-mat6.1-0 ; 
       scaled-mat7.1-0 ; 
       scaled-mat8.1-0 ; 
       scaled-mat9.1-0 ; 
       scaled-right.1-0 ; 
       scaled-yellow1.1-0 ; 
       scaled-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       fig07-cockpt.1-0 ; 
       fig07-fig07.31-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LL0.1-0 ; 
       fig07-LL1.1-0 ; 
       fig07-LL10.1-0 ; 
       fig07-LL11.1-0 ; 
       fig07-LL2.1-0 ; 
       fig07-LL3.1-0 ; 
       fig07-LL8.1-0 ; 
       fig07-LL9.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-lthrust.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepemt1.1-0 ; 
       fig07-lwepemt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-missemt.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rthrust.1-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepemt1.1-0 ; 
       fig07-rwepemt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSm.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-SSt.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-trail.1-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-twepemt.1-0 ; 
       fig07-wepatt.1-0 ; 
       scaled-cube1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       scaled-t2d1.1-0 ; 
       scaled-t2d10.1-0 ; 
       scaled-t2d11.1-0 ; 
       scaled-t2d12.1-0 ; 
       scaled-t2d13.1-0 ; 
       scaled-t2d14.1-0 ; 
       scaled-t2d15.1-0 ; 
       scaled-t2d16.1-0 ; 
       scaled-t2d17.1-0 ; 
       scaled-t2d18.1-0 ; 
       scaled-t2d19.1-0 ; 
       scaled-t2d2.1-0 ; 
       scaled-t2d21.1-0 ; 
       scaled-t2d22.1-0 ; 
       scaled-t2d23.1-0 ; 
       scaled-t2d24.1-0 ; 
       scaled-t2d3.1-0 ; 
       scaled-t2d33.1-0 ; 
       scaled-t2d34.1-0 ; 
       scaled-t2d35.1-0 ; 
       scaled-t2d36.1-0 ; 
       scaled-t2d37.1-0 ; 
       scaled-t2d38.1-0 ; 
       scaled-t2d39.1-0 ; 
       scaled-t2d4.1-0 ; 
       scaled-t2d40.1-0 ; 
       scaled-t2d41.1-0 ; 
       scaled-t2d42.1-0 ; 
       scaled-t2d43.1-0 ; 
       scaled-t2d44.1-0 ; 
       scaled-t2d45.1-0 ; 
       scaled-t2d46.1-0 ; 
       scaled-t2d47.1-0 ; 
       scaled-t2d5.1-0 ; 
       scaled-t2d6.1-0 ; 
       scaled-t2d7.1-0 ; 
       scaled-t2d8.1-0 ; 
       scaled-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 20 110 ; 
       6 18 110 ; 
       7 3 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 29 110 ; 
       11 10 110 ; 
       12 17 110 ; 
       13 12 110 ; 
       14 15 110 ; 
       15 11 110 ; 
       16 7 110 ; 
       17 16 110 ; 
       18 19 110 ; 
       19 3 110 ; 
       20 19 110 ; 
       21 7 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 8 110 ; 
       25 9 110 ; 
       26 7 110 ; 
       27 7 110 ; 
       28 7 110 ; 
       29 3 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 29 110 ; 
       35 30 110 ; 
       36 31 110 ; 
       37 29 110 ; 
       38 29 110 ; 
       39 3 110 ; 
       40 3 110 ; 
       41 3 110 ; 
       42 3 110 ; 
       43 3 110 ; 
       44 43 110 ; 
       45 47 110 ; 
       46 1 110 ; 
       47 44 110 ; 
       48 45 110 ; 
       49 47 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 46 300 ; 
       3 49 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       4 4 300 ; 
       4 13 300 ; 
       5 7 300 ; 
       6 30 300 ; 
       7 2 300 ; 
       7 6 300 ; 
       7 17 300 ; 
       7 29 300 ; 
       7 35 300 ; 
       8 41 300 ; 
       9 40 300 ; 
       11 23 300 ; 
       12 27 300 ; 
       13 28 300 ; 
       14 24 300 ; 
       15 25 300 ; 
       17 26 300 ; 
       18 31 300 ; 
       19 8 300 ; 
       20 32 300 ; 
       29 1 300 ; 
       29 42 300 ; 
       29 43 300 ; 
       29 44 300 ; 
       29 45 300 ; 
       30 39 300 ; 
       31 38 300 ; 
       39 5 300 ; 
       40 34 300 ; 
       41 47 300 ; 
       42 36 300 ; 
       43 48 300 ; 
       43 16 300 ; 
       44 0 300 ; 
       44 33 300 ; 
       45 37 300 ; 
       47 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 27 401 ; 
       24 28 401 ; 
       25 29 401 ; 
       26 30 401 ; 
       27 31 401 ; 
       28 32 401 ; 
       29 16 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       33 20 401 ; 
       35 24 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       39 23 401 ; 
       40 25 401 ; 
       41 26 401 ; 
       42 33 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       45 36 401 ; 
       46 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 77.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 80 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 DISPLAY 1 2 SRT 0.2512424 0.2512424 0.2512424 0.005999994 0 0 0 0 0.4867396 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 MPRFLG 0 ; 
       50 SCHEM 40.60733 6.347375 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -12 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -10 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 MPRFLG 0 ; 
       19 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60 -6 0 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 50 -6 0 MPRFLG 0 ; 
       28 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       30 SCHEM 30 -6 0 MPRFLG 0 ; 
       31 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 20 -6 0 MPRFLG 0 ; 
       34 SCHEM 25 -6 0 MPRFLG 0 ; 
       35 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 15 -6 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 35 -4 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       41 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       42 SCHEM 10 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       43 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       44 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       45 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       46 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       48 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 74 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 69 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 69 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 69 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 69 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 66.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
