SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.3-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       static-black1.2-0 ; 
       static-blue_pure1.2-0 ; 
       static-blue_pure2.2-0 ; 
       static-default1.2-0 ; 
       static-glass1.2-0 ; 
       static-mat1.2-0 ; 
       static-mat10.2-0 ; 
       static-mat12.2-0 ; 
       static-mat13.2-0 ; 
       static-mat14.2-0 ; 
       static-mat15.2-0 ; 
       static-mat16.2-0 ; 
       static-mat17.2-0 ; 
       static-mat18.2-0 ; 
       static-mat19.2-0 ; 
       static-mat2.2-0 ; 
       static-mat20.2-0 ; 
       static-mat22.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat3.2-0 ; 
       static-mat35.2-0 ; 
       static-mat38.2-0 ; 
       static-mat4.2-0 ; 
       static-mat41.2-0 ; 
       static-mat5.2-0 ; 
       static-mat6.2-0 ; 
       static-mat7.2-0 ; 
       static-mat8.2-0 ; 
       static-mat9.2-0 ; 
       static-yellow1.2-0 ; 
       static-yellow2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       fig07-fig07.30-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-null1.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-twepbas.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       static-t2d1.2-0 ; 
       static-t2d10.2-0 ; 
       static-t2d12.2-0 ; 
       static-t2d13.2-0 ; 
       static-t2d14.2-0 ; 
       static-t2d15.2-0 ; 
       static-t2d16.2-0 ; 
       static-t2d17.2-0 ; 
       static-t2d18.2-0 ; 
       static-t2d19.2-0 ; 
       static-t2d2.2-0 ; 
       static-t2d21.2-0 ; 
       static-t2d22.2-0 ; 
       static-t2d23.2-0 ; 
       static-t2d24.2-0 ; 
       static-t2d3.2-0 ; 
       static-t2d33.2-0 ; 
       static-t2d36.2-0 ; 
       static-t2d37.2-0 ; 
       static-t2d4.2-0 ; 
       static-t2d5.2-0 ; 
       static-t2d6.2-0 ; 
       static-t2d7.2-0 ; 
       static-t2d8.2-0 ; 
       static-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       11 12 110 ; 
       12 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 30 300 ; 
       2 32 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       3 4 300 ; 
       3 11 300 ; 
       4 6 300 ; 
       5 22 300 ; 
       6 2 300 ; 
       6 5 300 ; 
       6 15 300 ; 
       6 21 300 ; 
       6 24 300 ; 
       8 1 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       9 31 300 ; 
       9 14 300 ; 
       10 0 300 ; 
       10 23 300 ; 
       11 25 300 ; 
       12 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       6 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 10 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 19 401 ; 
       25 18 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0.005999994 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
