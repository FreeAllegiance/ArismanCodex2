SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.5-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       static-black1.3-0 ; 
       static-blue_pure1.3-0 ; 
       static-blue_pure2.3-0 ; 
       static-default1.3-0 ; 
       static-glass1.3-0 ; 
       static-mat1.3-0 ; 
       static-mat10.3-0 ; 
       static-mat11.2-0 ; 
       static-mat12.3-0 ; 
       static-mat13.3-0 ; 
       static-mat14.3-0 ; 
       static-mat15.3-0 ; 
       static-mat16.3-0 ; 
       static-mat17.3-0 ; 
       static-mat18.3-0 ; 
       static-mat19.3-0 ; 
       static-mat2.3-0 ; 
       static-mat20.3-0 ; 
       static-mat22.3-0 ; 
       static-mat23.3-0 ; 
       static-mat24.3-0 ; 
       static-mat25.3-0 ; 
       static-mat3.3-0 ; 
       static-mat35.3-0 ; 
       static-mat36.2-0 ; 
       static-mat37.2-0 ; 
       static-mat38.3-0 ; 
       static-mat4.3-0 ; 
       static-mat41.3-0 ; 
       static-mat5.3-0 ; 
       static-mat6.3-0 ; 
       static-mat7.3-0 ; 
       static-mat8.3-0 ; 
       static-mat9.3-0 ; 
       static-yellow1.3-0 ; 
       static-yellow2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       fig07-fig07.32-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-twepbas.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       static-t2d1.3-0 ; 
       static-t2d10.3-0 ; 
       static-t2d11.2-0 ; 
       static-t2d12.3-0 ; 
       static-t2d13.3-0 ; 
       static-t2d14.3-0 ; 
       static-t2d15.3-0 ; 
       static-t2d16.3-0 ; 
       static-t2d17.3-0 ; 
       static-t2d18.3-0 ; 
       static-t2d19.3-0 ; 
       static-t2d2.3-0 ; 
       static-t2d21.3-0 ; 
       static-t2d22.3-0 ; 
       static-t2d23.3-0 ; 
       static-t2d24.3-0 ; 
       static-t2d3.3-0 ; 
       static-t2d33.3-0 ; 
       static-t2d34.2-0 ; 
       static-t2d35.2-0 ; 
       static-t2d36.3-0 ; 
       static-t2d37.3-0 ; 
       static-t2d4.3-0 ; 
       static-t2d5.3-0 ; 
       static-t2d6.3-0 ; 
       static-t2d7.3-0 ; 
       static-t2d8.3-0 ; 
       static-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 9 110 ; 
       5 7 110 ; 
       6 2 110 ; 
       7 8 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 14 110 ; 
       14 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 33 300 ; 
       2 35 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       3 4 300 ; 
       3 12 300 ; 
       4 6 300 ; 
       5 23 300 ; 
       6 2 300 ; 
       6 5 300 ; 
       6 16 300 ; 
       6 22 300 ; 
       6 27 300 ; 
       7 24 300 ; 
       8 7 300 ; 
       9 25 300 ; 
       10 1 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 32 300 ; 
       11 34 300 ; 
       11 15 300 ; 
       12 0 300 ; 
       12 26 300 ; 
       13 28 300 ; 
       14 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 10 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 22 401 ; 
       28 21 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0.005999994 0 0 0 0 0.4867396 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
