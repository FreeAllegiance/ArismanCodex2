SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.6-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       static-black1.4-0 ; 
       static-blue_pure1.4-0 ; 
       static-blue_pure2.4-0 ; 
       static-default1.4-0 ; 
       static-glass1.4-0 ; 
       static-mat1.4-0 ; 
       static-mat10.4-0 ; 
       static-mat11.3-0 ; 
       static-mat12.4-0 ; 
       static-mat13.4-0 ; 
       static-mat14.4-0 ; 
       static-mat15.4-0 ; 
       static-mat16.4-0 ; 
       static-mat17.4-0 ; 
       static-mat18.4-0 ; 
       static-mat19.4-0 ; 
       static-mat2.4-0 ; 
       static-mat20.4-0 ; 
       static-mat22.4-0 ; 
       static-mat23.4-0 ; 
       static-mat24.4-0 ; 
       static-mat25.4-0 ; 
       static-mat3.4-0 ; 
       static-mat35.4-0 ; 
       static-mat36.3-0 ; 
       static-mat37.3-0 ; 
       static-mat38.4-0 ; 
       static-mat4.4-0 ; 
       static-mat41.4-0 ; 
       static-mat42.3-0 ; 
       static-mat43.3-0 ; 
       static-mat44.3-0 ; 
       static-mat45.3-0 ; 
       static-mat5.4-0 ; 
       static-mat6.4-0 ; 
       static-mat7.4-0 ; 
       static-mat8.4-0 ; 
       static-mat9.4-0 ; 
       static-yellow1.4-0 ; 
       static-yellow2.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       fig07-fig07.37-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-twepbas.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig07/PICTURES/fig07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       static-t2d1.4-0 ; 
       static-t2d10.4-0 ; 
       static-t2d11.3-0 ; 
       static-t2d12.4-0 ; 
       static-t2d13.4-0 ; 
       static-t2d14.4-0 ; 
       static-t2d15.4-0 ; 
       static-t2d16.4-0 ; 
       static-t2d17.4-0 ; 
       static-t2d18.4-0 ; 
       static-t2d19.4-0 ; 
       static-t2d2.4-0 ; 
       static-t2d21.4-0 ; 
       static-t2d22.4-0 ; 
       static-t2d23.4-0 ; 
       static-t2d24.4-0 ; 
       static-t2d3.4-0 ; 
       static-t2d33.4-0 ; 
       static-t2d34.3-0 ; 
       static-t2d35.3-0 ; 
       static-t2d36.4-0 ; 
       static-t2d37.4-0 ; 
       static-t2d38.3-0 ; 
       static-t2d39.3-0 ; 
       static-t2d4.4-0 ; 
       static-t2d40.3-0 ; 
       static-t2d41.3-0 ; 
       static-t2d5.4-0 ; 
       static-t2d6.4-0 ; 
       static-t2d7.4-0 ; 
       static-t2d8.4-0 ; 
       static-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 11 110 ; 
       5 9 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 10 110 ; 
       10 2 110 ; 
       11 10 110 ; 
       12 2 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 2 110 ; 
       16 15 110 ; 
       17 18 110 ; 
       18 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 37 300 ; 
       2 39 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       3 4 300 ; 
       3 12 300 ; 
       4 6 300 ; 
       5 23 300 ; 
       6 2 300 ; 
       6 5 300 ; 
       6 16 300 ; 
       6 22 300 ; 
       6 27 300 ; 
       7 32 300 ; 
       8 31 300 ; 
       9 24 300 ; 
       10 7 300 ; 
       11 25 300 ; 
       12 1 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       13 30 300 ; 
       14 29 300 ; 
       15 38 300 ; 
       15 15 300 ; 
       16 0 300 ; 
       16 26 300 ; 
       17 28 300 ; 
       18 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 11 401 ; 
       17 10 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 24 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 0 0 SRT 0.2512424 0.2512424 0.2512424 0.005999994 0 0 0 0 0.4867396 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 MPRFLG 0 ; 
       6 SCHEM 50 -4 0 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 MPRFLG 0 ; 
       8 SCHEM 50 -6 0 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 MPRFLG 0 ; 
       10 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 6.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 76.46788 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
