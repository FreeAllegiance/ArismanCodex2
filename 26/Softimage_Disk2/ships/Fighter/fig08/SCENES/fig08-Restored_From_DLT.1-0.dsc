SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.1-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       Restored_From_DLT-inf_light1.1-0 ROOT ; 
       Restored_From_DLT-light5_1_2_2.1-0 ROOT ; 
       Restored_From_DLT-light6_3_2_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       Restored_From_DLT-mat10.1-0 ; 
       Restored_From_DLT-mat11.1-0 ; 
       Restored_From_DLT-mat12.1-0 ; 
       Restored_From_DLT-mat13.1-0 ; 
       Restored_From_DLT-mat14.1-0 ; 
       Restored_From_DLT-mat15.1-0 ; 
       Restored_From_DLT-mat16.1-0 ; 
       Restored_From_DLT-mat17.1-0 ; 
       Restored_From_DLT-mat18.1-0 ; 
       Restored_From_DLT-mat19.1-0 ; 
       Restored_From_DLT-mat20.1-0 ; 
       Restored_From_DLT-mat21.1-0 ; 
       Restored_From_DLT-mat23.1-0 ; 
       Restored_From_DLT-mat24.1-0 ; 
       Restored_From_DLT-mat25.1-0 ; 
       Restored_From_DLT-mat26.1-0 ; 
       Restored_From_DLT-mat27.1-0 ; 
       Restored_From_DLT-mat28.1-0 ; 
       Restored_From_DLT-mat29.1-0 ; 
       Restored_From_DLT-mat3.1-0 ; 
       Restored_From_DLT-mat30.1-0 ; 
       Restored_From_DLT-mat31.1-0 ; 
       Restored_From_DLT-mat32.1-0 ; 
       Restored_From_DLT-mat33.1-0 ; 
       Restored_From_DLT-mat34.1-0 ; 
       Restored_From_DLT-mat39.1-0 ; 
       Restored_From_DLT-mat40.1-0 ; 
       Restored_From_DLT-mat41.1-0 ; 
       Restored_From_DLT-mat42.1-0 ; 
       Restored_From_DLT-mat43.1-0 ; 
       Restored_From_DLT-mat44.1-0 ; 
       Restored_From_DLT-mat45.1-0 ; 
       Restored_From_DLT-mat46.1-0 ; 
       Restored_From_DLT-mat47.1-0 ; 
       Restored_From_DLT-mat48.1-0 ; 
       Restored_From_DLT-mat49.1-0 ; 
       Restored_From_DLT-mat50.1-0 ; 
       Restored_From_DLT-mat8.1-0 ; 
       Restored_From_DLT-mat9.1-0 ; 
       Restored_From_DLT-nose_white-center.1-0.1-0 ; 
       Restored_From_DLT-nose_white-center.1-1.1-0 ; 
       Restored_From_DLT-port_red-left.1-0.1-0 ; 
       Restored_From_DLT-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig08-fig08.1-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.8-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/kezari_LF ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-Restored_From_DLT.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       Restored_From_DLT-t2d10.1-0 ; 
       Restored_From_DLT-t2d11.1-0 ; 
       Restored_From_DLT-t2d12.1-0 ; 
       Restored_From_DLT-t2d13.1-0 ; 
       Restored_From_DLT-t2d14.1-0 ; 
       Restored_From_DLT-t2d15.1-0 ; 
       Restored_From_DLT-t2d16.1-0 ; 
       Restored_From_DLT-t2d17.1-0 ; 
       Restored_From_DLT-t2d18.1-0 ; 
       Restored_From_DLT-t2d24.1-0 ; 
       Restored_From_DLT-t2d27.1-0 ; 
       Restored_From_DLT-t2d28.1-0 ; 
       Restored_From_DLT-t2d29.1-0 ; 
       Restored_From_DLT-t2d31.1-0 ; 
       Restored_From_DLT-t2d33.1-0 ; 
       Restored_From_DLT-t2d39.1-0 ; 
       Restored_From_DLT-t2d40.1-0 ; 
       Restored_From_DLT-t2d41.1-0 ; 
       Restored_From_DLT-t2d42.1-0 ; 
       Restored_From_DLT-t2d43.1-0 ; 
       Restored_From_DLT-t2d5.1-0 ; 
       Restored_From_DLT-t2d6.1-0 ; 
       Restored_From_DLT-t2d7.1-0 ; 
       Restored_From_DLT-t2d8.1-0 ; 
       Restored_From_DLT-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       24 1 110 ; 
       25 1 110 ; 
       26 3 110 ; 
       27 4 110 ; 
       28 5 110 ; 
       29 6 110 ; 
       30 7 110 ; 
       31 8 110 ; 
       32 1 110 ; 
       33 13 110 ; 
       34 14 110 ; 
       35 15 110 ; 
       36 16 110 ; 
       37 17 110 ; 
       38 18 110 ; 
       39 1 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 1 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       24 40 300 ; 
       25 41 300 ; 
       26 26 300 ; 
       27 25 300 ; 
       28 27 300 ; 
       29 28 300 ; 
       30 29 300 ; 
       31 30 300 ; 
       32 42 300 ; 
       33 31 300 ; 
       34 32 300 ; 
       35 33 300 ; 
       36 34 300 ; 
       37 35 300 ; 
       38 36 300 ; 
       39 39 300 ; 
       1 19 300 ; 
       1 37 300 ; 
       1 38 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       3 18 300 ; 
       4 20 300 ; 
       5 21 300 ; 
       6 22 300 ; 
       7 23 300 ; 
       8 24 300 ; 
       9 8 300 ; 
       9 11 300 ; 
       13 17 300 ; 
       14 16 300 ; 
       15 15 300 ; 
       16 14 300 ; 
       17 12 300 ; 
       18 13 300 ; 
       19 6 300 ; 
       19 7 300 ; 
       19 9 300 ; 
       19 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 11 400 ; 
       4 12 400 ; 
       6 13 400 ; 
       8 14 400 ; 
       9 5 400 ; 
       13 10 400 ; 
       16 9 400 ; 
       18 8 400 ; 
       19 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 21 401 ; 
       2 23 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       7 4 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 15 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       19 24 401 ; 
       21 18 401 ; 
       23 19 401 ; 
       37 22 401 ; 
       38 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       24 SCHEM 47.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 45 -8 0 MPRFLG 0 ; 
       27 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 40 -8 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 MPRFLG 0 ; 
       31 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 52.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 30 -8 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 25 -8 0 MPRFLG 0 ; 
       36 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 20 -8 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 55 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       0 SCHEM 28.75 0 0 SRT 0.4374 0.4374 0.4374 0 -1.192093e-007 0 0 -0.2177 0 MPRFLG 0 ; 
       1 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 40 -6 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 20 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 5 -4 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -6 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 23.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
