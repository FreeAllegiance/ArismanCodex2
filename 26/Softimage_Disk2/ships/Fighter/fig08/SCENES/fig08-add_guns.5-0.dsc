SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.17-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.32-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_guns-light1.4-0 ROOT ; 
       add_guns-light2.4-0 ROOT ; 
       add_guns-light3.4-0 ROOT ; 
       add_guns-light4.4-0 ROOT ; 
       add_guns-light5.4-0 ROOT ; 
       add_guns-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       add_guns-back1.1-0 ; 
       add_guns-base.1-0 ; 
       add_guns-base1.1-0 ; 
       add_guns-base2.1-0 ; 
       add_guns-bottom.1-0 ; 
       add_guns-caution1.1-0 ; 
       add_guns-caution2.1-0 ; 
       add_guns-front1.1-0 ; 
       add_guns-guns1.1-0 ; 
       add_guns-guns2.1-0 ; 
       add_guns-mat23.1-0 ; 
       add_guns-mat24.1-0 ; 
       add_guns-mat25.1-0 ; 
       add_guns-mat26.1-0 ; 
       add_guns-mat27.1-0 ; 
       add_guns-mat28.1-0 ; 
       add_guns-mat29.1-0 ; 
       add_guns-mat30.1-0 ; 
       add_guns-mat31.1-0 ; 
       add_guns-mat32.1-0 ; 
       add_guns-mat33.1-0 ; 
       add_guns-mat34.1-0 ; 
       add_guns-mat39.1-0 ; 
       add_guns-mat40.1-0 ; 
       add_guns-mat41.1-0 ; 
       add_guns-mat42.1-0 ; 
       add_guns-mat43.1-0 ; 
       add_guns-mat44.1-0 ; 
       add_guns-mat45.1-0 ; 
       add_guns-mat46.1-0 ; 
       add_guns-mat47.1-0 ; 
       add_guns-mat48.1-0 ; 
       add_guns-mat49.1-0 ; 
       add_guns-mat50.1-0 ; 
       add_guns-mat51.1-0 ; 
       add_guns-mat52.1-0 ; 
       add_guns-mat53.1-0 ; 
       add_guns-nose_white-center.1-0.1-0 ; 
       add_guns-nose_white-center.1-1.1-0 ; 
       add_guns-port_red-left.1-0.1-0 ; 
       add_guns-sides_and_bottom.1-0 ; 
       add_guns-starbord_green-right.1-0.1-0 ; 
       add_guns-top.1-0 ; 
       add_guns-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       add_guns-cube2.2-0 ROOT ; 
       add_guns-cube3.2-0 ROOT ; 
       add_guns-lwepemt2.1-0 ; 
       add_guns-lwepemt3.1-0 ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-fig08_3.17-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_guns.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_guns-t2d24.1-0 ; 
       add_guns-t2d27.1-0 ; 
       add_guns-t2d28.1-0 ; 
       add_guns-t2d29.1-0 ; 
       add_guns-t2d31.1-0 ; 
       add_guns-t2d33.1-0 ; 
       add_guns-t2d39.1-0 ; 
       add_guns-t2d40.1-0 ; 
       add_guns-t2d41.1-0 ; 
       add_guns-t2d42.1-0 ; 
       add_guns-t2d43.1-0 ; 
       add_guns-t2d44.1-0 ; 
       add_guns-t2d45.1-0 ; 
       add_guns-t2d46.1-0 ; 
       add_guns-t2d47.1-0 ; 
       add_guns-t2d48.1-0 ; 
       add_guns-t2d49.1-0 ; 
       add_guns-t2d50.1-0 ; 
       add_guns-t2d51.1-0 ; 
       add_guns-t2d52.1-0 ; 
       add_guns-t2d53.2-0 ; 
       add_guns-t2d54.1-0 ; 
       add_guns-t2d55.1-0 ; 
       add_guns-t2d6.1-0 ; 
       add_guns-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 1 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 7 110 ; 
       44 6 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       17 27 110 ; 
       18 27 110 ; 
       19 27 110 ; 
       20 27 110 ; 
       21 27 110 ; 
       22 27 110 ; 
       23 7 110 ; 
       45 6 110 ; 
       4 6 110 ; 
       27 7 110 ; 
       28 7 110 ; 
       29 7 110 ; 
       30 9 110 ; 
       31 10 110 ; 
       32 11 110 ; 
       33 12 110 ; 
       34 13 110 ; 
       35 14 110 ; 
       36 7 110 ; 
       37 17 110 ; 
       38 18 110 ; 
       39 19 110 ; 
       40 20 110 ; 
       41 21 110 ; 
       42 22 110 ; 
       43 7 110 ; 
       2 0 110 ; 
       16 5 110 ; 
       24 23 110 ; 
       5 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 4 300 ; 
       7 42 300 ; 
       7 1 300 ; 
       7 7 300 ; 
       7 40 300 ; 
       7 0 300 ; 
       7 43 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       11 18 300 ; 
       12 19 300 ; 
       13 20 300 ; 
       14 21 300 ; 
       15 3 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       17 15 300 ; 
       18 14 300 ; 
       19 13 300 ; 
       20 12 300 ; 
       21 10 300 ; 
       22 11 300 ; 
       23 2 300 ; 
       23 5 300 ; 
       23 8 300 ; 
       0 35 300 ; 
       28 38 300 ; 
       29 39 300 ; 
       30 23 300 ; 
       31 22 300 ; 
       32 24 300 ; 
       33 25 300 ; 
       34 26 300 ; 
       35 27 300 ; 
       36 41 300 ; 
       37 28 300 ; 
       38 29 300 ; 
       39 30 300 ; 
       40 31 300 ; 
       41 32 300 ; 
       42 33 300 ; 
       43 37 300 ; 
       5 34 300 ; 
       1 36 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 2 400 ; 
       10 3 400 ; 
       12 4 400 ; 
       14 5 400 ; 
       17 1 400 ; 
       20 0 400 ; 
       22 24 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       40 11 401 ; 
       42 23 401 ; 
       43 15 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       7 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 35 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 45 -6 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       44 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 15 -6 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 20 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 MPRFLG 0 ; 
       45 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 87.5 0 0 SRT 0.004186729 0.004186729 0.01025749 0 0 0 0.5416436 -0.223472 1.65528 MPRFLG 0 ; 
       27 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       28 SCHEM 25 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 35 -8 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 40 -8 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 45 -8 0 MPRFLG 0 ; 
       36 SCHEM 30 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 10 -8 0 MPRFLG 0 ; 
       39 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 15 -8 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 20 -8 0 MPRFLG 0 ; 
       43 SCHEM 32.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       2 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 55 -6 0 MPRFLG 0 ; 
       1 SCHEM 95 0 0 SRT 0.004186729 0.004186729 0.01025749 0 0 0 0.6144743 -0.2055868 1.535525 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 100 6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 107.5 6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       24 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 70 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 102.5 6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 110 6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
