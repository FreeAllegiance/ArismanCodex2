SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.18-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.33-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_guns-light1.5-0 ROOT ; 
       add_guns-light2.5-0 ROOT ; 
       add_guns-light3.5-0 ROOT ; 
       add_guns-light4.5-0 ROOT ; 
       add_guns-light5.5-0 ROOT ; 
       add_guns-light6.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_guns-back1.1-0 ; 
       add_guns-base.1-0 ; 
       add_guns-base1.1-0 ; 
       add_guns-base2.1-0 ; 
       add_guns-bottom.1-0 ; 
       add_guns-caution1.1-0 ; 
       add_guns-caution2.1-0 ; 
       add_guns-front1.1-0 ; 
       add_guns-guns1.1-0 ; 
       add_guns-guns2.1-0 ; 
       add_guns-mat23.1-0 ; 
       add_guns-mat24.1-0 ; 
       add_guns-mat25.1-0 ; 
       add_guns-mat26.1-0 ; 
       add_guns-mat27.1-0 ; 
       add_guns-mat28.1-0 ; 
       add_guns-mat29.1-0 ; 
       add_guns-mat30.1-0 ; 
       add_guns-mat31.1-0 ; 
       add_guns-mat32.1-0 ; 
       add_guns-mat33.1-0 ; 
       add_guns-mat34.1-0 ; 
       add_guns-mat39.1-0 ; 
       add_guns-mat40.1-0 ; 
       add_guns-mat41.1-0 ; 
       add_guns-mat42.1-0 ; 
       add_guns-mat43.1-0 ; 
       add_guns-mat44.1-0 ; 
       add_guns-mat45.1-0 ; 
       add_guns-mat46.1-0 ; 
       add_guns-mat47.1-0 ; 
       add_guns-mat48.1-0 ; 
       add_guns-mat49.1-0 ; 
       add_guns-mat50.1-0 ; 
       add_guns-mat51.1-0 ; 
       add_guns-mat52.1-0 ; 
       add_guns-mat53.1-0 ; 
       add_guns-mat54.1-0 ; 
       add_guns-mat55.1-0 ; 
       add_guns-mat56.1-0 ; 
       add_guns-nose_white-center.1-0.1-0 ; 
       add_guns-nose_white-center.1-1.1-0 ; 
       add_guns-port_red-left.1-0.1-0 ; 
       add_guns-sides_and_bottom.1-0 ; 
       add_guns-starbord_green-right.1-0.1-0 ; 
       add_guns-top.1-0 ; 
       add_guns-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.18-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_guns.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       add_guns-t2d24.1-0 ; 
       add_guns-t2d27.1-0 ; 
       add_guns-t2d28.1-0 ; 
       add_guns-t2d29.1-0 ; 
       add_guns-t2d31.1-0 ; 
       add_guns-t2d33.1-0 ; 
       add_guns-t2d39.1-0 ; 
       add_guns-t2d40.1-0 ; 
       add_guns-t2d41.1-0 ; 
       add_guns-t2d42.1-0 ; 
       add_guns-t2d43.1-0 ; 
       add_guns-t2d44.1-0 ; 
       add_guns-t2d45.1-0 ; 
       add_guns-t2d46.1-0 ; 
       add_guns-t2d47.1-0 ; 
       add_guns-t2d48.1-0 ; 
       add_guns-t2d49.1-0 ; 
       add_guns-t2d50.1-0 ; 
       add_guns-t2d51.1-0 ; 
       add_guns-t2d52.1-0 ; 
       add_guns-t2d53.2-0 ; 
       add_guns-t2d54.1-0 ; 
       add_guns-t2d55.1-0 ; 
       add_guns-t2d56.1-0 ; 
       add_guns-t2d57.1-0 ; 
       add_guns-t2d58.1-0 ; 
       add_guns-t2d6.1-0 ; 
       add_guns-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       19 3 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 8 110 ; 
       47 7 110 ; 
       28 5 110 ; 
       29 4 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 30 110 ; 
       25 30 110 ; 
       26 8 110 ; 
       48 7 110 ; 
       0 7 110 ; 
       2 16 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 10 110 ; 
       34 11 110 ; 
       35 12 110 ; 
       36 13 110 ; 
       37 14 110 ; 
       38 15 110 ; 
       39 8 110 ; 
       40 20 110 ; 
       41 21 110 ; 
       42 22 110 ; 
       43 23 110 ; 
       44 24 110 ; 
       45 25 110 ; 
       46 8 110 ; 
       18 2 110 ; 
       17 1 110 ; 
       27 6 110 ; 
       1 16 110 ; 
       3 16 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 4 300 ; 
       8 45 300 ; 
       8 1 300 ; 
       8 7 300 ; 
       8 43 300 ; 
       8 0 300 ; 
       8 46 300 ; 
       10 16 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       13 19 300 ; 
       14 20 300 ; 
       15 21 300 ; 
       16 3 300 ; 
       16 6 300 ; 
       16 9 300 ; 
       20 15 300 ; 
       21 14 300 ; 
       22 13 300 ; 
       23 12 300 ; 
       24 10 300 ; 
       25 11 300 ; 
       26 2 300 ; 
       26 5 300 ; 
       26 8 300 ; 
       2 35 300 ; 
       31 41 300 ; 
       32 42 300 ; 
       33 23 300 ; 
       34 22 300 ; 
       35 24 300 ; 
       36 25 300 ; 
       37 26 300 ; 
       38 27 300 ; 
       39 44 300 ; 
       40 28 300 ; 
       41 29 300 ; 
       42 30 300 ; 
       43 31 300 ; 
       44 32 300 ; 
       45 33 300 ; 
       46 40 300 ; 
       1 34 300 ; 
       3 36 300 ; 
       4 37 300 ; 
       5 38 300 ; 
       6 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 2 400 ; 
       11 3 400 ; 
       13 4 400 ; 
       15 5 400 ; 
       20 1 400 ; 
       23 0 400 ; 
       25 27 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       43 11 401 ; 
       45 26 401 ; 
       46 15 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       19 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 45 -10 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 35 -10 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 40 -10 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 50 -8 0 MPRFLG 0 ; 
       47 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 10 -10 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 15 -10 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       48 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50 -10 0 MPRFLG 0 ; 
       30 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 25 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 45 -12 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       35 SCHEM 35 -12 0 MPRFLG 0 ; 
       36 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 40 -12 0 MPRFLG 0 ; 
       38 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 20 -12 0 MPRFLG 0 ; 
       41 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       42 SCHEM 10 -12 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 15 -12 0 MPRFLG 0 ; 
       45 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       46 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
