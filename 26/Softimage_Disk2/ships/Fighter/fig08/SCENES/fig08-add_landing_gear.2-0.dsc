SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.21-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.36-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_landing_gear-light1.2-0 ROOT ; 
       add_landing_gear-light2.2-0 ROOT ; 
       add_landing_gear-light3.2-0 ROOT ; 
       add_landing_gear-light4.2-0 ROOT ; 
       add_landing_gear-light5.2-0 ROOT ; 
       add_landing_gear-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_landing_gear-back1.1-0 ; 
       add_landing_gear-base.1-0 ; 
       add_landing_gear-base1.1-0 ; 
       add_landing_gear-base2.1-0 ; 
       add_landing_gear-bottom.1-0 ; 
       add_landing_gear-caution1.1-0 ; 
       add_landing_gear-caution2.1-0 ; 
       add_landing_gear-front1.1-0 ; 
       add_landing_gear-guns1.1-0 ; 
       add_landing_gear-guns2.1-0 ; 
       add_landing_gear-mat23.1-0 ; 
       add_landing_gear-mat24.1-0 ; 
       add_landing_gear-mat25.1-0 ; 
       add_landing_gear-mat26.1-0 ; 
       add_landing_gear-mat27.1-0 ; 
       add_landing_gear-mat28.1-0 ; 
       add_landing_gear-mat29.1-0 ; 
       add_landing_gear-mat30.1-0 ; 
       add_landing_gear-mat31.1-0 ; 
       add_landing_gear-mat32.1-0 ; 
       add_landing_gear-mat33.1-0 ; 
       add_landing_gear-mat34.1-0 ; 
       add_landing_gear-mat39.1-0 ; 
       add_landing_gear-mat40.1-0 ; 
       add_landing_gear-mat41.1-0 ; 
       add_landing_gear-mat42.1-0 ; 
       add_landing_gear-mat43.1-0 ; 
       add_landing_gear-mat44.1-0 ; 
       add_landing_gear-mat45.1-0 ; 
       add_landing_gear-mat46.1-0 ; 
       add_landing_gear-mat47.1-0 ; 
       add_landing_gear-mat48.1-0 ; 
       add_landing_gear-mat49.1-0 ; 
       add_landing_gear-mat50.1-0 ; 
       add_landing_gear-mat51.1-0 ; 
       add_landing_gear-mat52.1-0 ; 
       add_landing_gear-mat53.1-0 ; 
       add_landing_gear-mat54.1-0 ; 
       add_landing_gear-mat55.1-0 ; 
       add_landing_gear-mat56.1-0 ; 
       add_landing_gear-nose_white-center.1-0.1-0 ; 
       add_landing_gear-nose_white-center.1-1.1-0 ; 
       add_landing_gear-port_red-left.1-0.1-0 ; 
       add_landing_gear-sides_and_bottom.1-0 ; 
       add_landing_gear-starbord_green-right.1-0.1-0 ; 
       add_landing_gear-top.1-0 ; 
       add_landing_gear-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       add_landing_gear-cube10.1-0 ROOT ; 
       add_landing_gear-cube11.1-0 ROOT ; 
       add_landing_gear-cube7.1-0 ROOT ; 
       add_landing_gear-cube8.1-0 ; 
       add_landing_gear-cube9.1-0 ROOT ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.20-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_landing_gear.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       add_landing_gear-t2d24.1-0 ; 
       add_landing_gear-t2d27.1-0 ; 
       add_landing_gear-t2d28.1-0 ; 
       add_landing_gear-t2d29.1-0 ; 
       add_landing_gear-t2d31.1-0 ; 
       add_landing_gear-t2d33.1-0 ; 
       add_landing_gear-t2d39.1-0 ; 
       add_landing_gear-t2d40.1-0 ; 
       add_landing_gear-t2d41.1-0 ; 
       add_landing_gear-t2d42.1-0 ; 
       add_landing_gear-t2d43.1-0 ; 
       add_landing_gear-t2d44.1-0 ; 
       add_landing_gear-t2d45.1-0 ; 
       add_landing_gear-t2d46.1-0 ; 
       add_landing_gear-t2d47.1-0 ; 
       add_landing_gear-t2d48.1-0 ; 
       add_landing_gear-t2d49.1-0 ; 
       add_landing_gear-t2d50.1-0 ; 
       add_landing_gear-t2d51.1-0 ; 
       add_landing_gear-t2d52.1-0 ; 
       add_landing_gear-t2d53.1-0 ; 
       add_landing_gear-t2d54.1-0 ; 
       add_landing_gear-t2d55.1-0 ; 
       add_landing_gear-t2d56.1-0 ; 
       add_landing_gear-t2d57.1-0 ; 
       add_landing_gear-t2d58.1-0 ; 
       add_landing_gear-t2d6.1-0 ; 
       add_landing_gear-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       24 8 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 13 110 ; 
       52 12 110 ; 
       33 10 110 ; 
       34 9 110 ; 
       25 35 110 ; 
       26 35 110 ; 
       27 35 110 ; 
       28 35 110 ; 
       29 35 110 ; 
       30 35 110 ; 
       31 13 110 ; 
       53 12 110 ; 
       5 12 110 ; 
       7 21 110 ; 
       35 13 110 ; 
       36 13 110 ; 
       37 13 110 ; 
       38 15 110 ; 
       39 16 110 ; 
       40 17 110 ; 
       41 18 110 ; 
       42 19 110 ; 
       43 20 110 ; 
       44 13 110 ; 
       45 25 110 ; 
       46 26 110 ; 
       47 27 110 ; 
       48 28 110 ; 
       49 29 110 ; 
       50 30 110 ; 
       51 13 110 ; 
       23 7 110 ; 
       22 6 110 ; 
       32 11 110 ; 
       6 21 110 ; 
       8 21 110 ; 
       9 31 110 ; 
       10 31 110 ; 
       11 31 110 ; 
       3 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 4 300 ; 
       13 45 300 ; 
       13 1 300 ; 
       13 7 300 ; 
       13 43 300 ; 
       13 0 300 ; 
       13 46 300 ; 
       15 16 300 ; 
       16 17 300 ; 
       17 18 300 ; 
       18 19 300 ; 
       19 20 300 ; 
       20 21 300 ; 
       21 3 300 ; 
       21 6 300 ; 
       21 9 300 ; 
       25 15 300 ; 
       26 14 300 ; 
       27 13 300 ; 
       28 12 300 ; 
       29 10 300 ; 
       30 11 300 ; 
       31 2 300 ; 
       31 5 300 ; 
       31 8 300 ; 
       7 35 300 ; 
       36 41 300 ; 
       37 42 300 ; 
       38 23 300 ; 
       39 22 300 ; 
       40 24 300 ; 
       41 25 300 ; 
       42 26 300 ; 
       43 27 300 ; 
       44 44 300 ; 
       45 28 300 ; 
       46 29 300 ; 
       47 30 300 ; 
       48 31 300 ; 
       49 32 300 ; 
       50 33 300 ; 
       51 40 300 ; 
       6 34 300 ; 
       8 36 300 ; 
       9 37 300 ; 
       10 38 300 ; 
       11 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 2 400 ; 
       16 3 400 ; 
       18 4 400 ; 
       20 5 400 ; 
       25 1 400 ; 
       28 0 400 ; 
       30 27 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       12 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       43 11 401 ; 
       45 26 401 ; 
       46 15 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 62.5 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 65 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 70 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 72.5 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 75 -20.38656 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       24 SCHEM 47.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -20.38656 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       13 SCHEM 26.25 -22.38656 0 MPRFLG 0 ; 
       14 SCHEM 38.75 -24.38656 0 MPRFLG 0 ; 
       15 SCHEM 45 -26.38656 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -26.38656 0 MPRFLG 0 ; 
       17 SCHEM 35 -26.38656 0 MPRFLG 0 ; 
       18 SCHEM 37.5 -26.38656 0 MPRFLG 0 ; 
       19 SCHEM 40 -26.38656 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -26.38656 0 MPRFLG 0 ; 
       21 SCHEM 50 -24.38656 0 MPRFLG 0 ; 
       52 SCHEM 55 -22.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 2.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 0 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 20 -26.38656 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -26.38656 0 MPRFLG 0 ; 
       27 SCHEM 10 -26.38656 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -26.38656 0 MPRFLG 0 ; 
       29 SCHEM 15 -26.38656 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -26.38656 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -24.38656 0 MPRFLG 0 ; 
       53 SCHEM 57.5 -22.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 60 -22.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -26.38656 0 MPRFLG 0 ; 
       35 SCHEM 13.75 -24.38656 0 MPRFLG 0 ; 
       36 SCHEM 22.5 -24.38656 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 25 -24.38656 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 45 -28.38656 0 MPRFLG 0 ; 
       39 SCHEM 32.5 -28.38656 0 MPRFLG 0 ; 
       40 SCHEM 35 -28.38656 0 MPRFLG 0 ; 
       41 SCHEM 37.5 -28.38656 0 MPRFLG 0 ; 
       42 SCHEM 40 -28.38656 0 MPRFLG 0 ; 
       43 SCHEM 42.5 -28.38656 0 MPRFLG 0 ; 
       44 SCHEM 27.5 -24.38656 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 20 -28.38656 0 MPRFLG 0 ; 
       46 SCHEM 7.5 -28.38656 0 MPRFLG 0 ; 
       47 SCHEM 10 -28.38656 0 MPRFLG 0 ; 
       48 SCHEM 12.5 -28.38656 0 MPRFLG 0 ; 
       49 SCHEM 15 -28.38656 0 MPRFLG 0 ; 
       50 SCHEM 17.5 -28.38656 0 MPRFLG 0 ; 
       51 SCHEM 30 -24.38656 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 50 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -26.38656 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -26.38656 0 MPRFLG 0 ; 
       9 SCHEM 0 -26.38656 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -26.38656 0 MPRFLG 0 ; 
       11 SCHEM 5 -26.38656 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 SRT 0.02074963 0.02074963 0.02074963 1.56 0 0.29 0.2521787 -0.08236739 -0.4068539 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 77.5 0.6481262 0 USR DISPLAY 1 2 SRT 0.03199993 0.03199993 0.03199993 0 0 0 0.3226624 -0.6331027 -0.411007 MPRFLG 0 ; 
       0 SCHEM 80 0.6481262 0 USR SRT 0.03199993 0.03199993 0.03199993 0 0.61 0 0.5170741 -0.6331027 -0.544676 MPRFLG 0 ; 
       1 SCHEM 82.5 0.6481262 0 USR SRT 0.02406395 0.02406395 0.02406395 0 3.43 0 0.4021048 -0.6746338 -0.812014 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -30.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 29 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -26.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -24.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 51.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -28.38656 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 19 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 14 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -28.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 54 -26.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 49 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -30.38656 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -22.38656 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
