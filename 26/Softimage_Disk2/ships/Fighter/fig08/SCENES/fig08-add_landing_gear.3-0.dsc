SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.22-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.37-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_landing_gear-back1.1-0 ; 
       add_landing_gear-base.1-0 ; 
       add_landing_gear-base1.1-0 ; 
       add_landing_gear-base2.1-0 ; 
       add_landing_gear-bottom.1-0 ; 
       add_landing_gear-caution1.1-0 ; 
       add_landing_gear-caution2.1-0 ; 
       add_landing_gear-front1.1-0 ; 
       add_landing_gear-guns1.1-0 ; 
       add_landing_gear-guns2.1-0 ; 
       add_landing_gear-mat23.1-0 ; 
       add_landing_gear-mat24.1-0 ; 
       add_landing_gear-mat25.1-0 ; 
       add_landing_gear-mat26.1-0 ; 
       add_landing_gear-mat27.1-0 ; 
       add_landing_gear-mat28.1-0 ; 
       add_landing_gear-mat29.1-0 ; 
       add_landing_gear-mat30.1-0 ; 
       add_landing_gear-mat31.1-0 ; 
       add_landing_gear-mat32.1-0 ; 
       add_landing_gear-mat33.1-0 ; 
       add_landing_gear-mat34.1-0 ; 
       add_landing_gear-mat39.1-0 ; 
       add_landing_gear-mat40.1-0 ; 
       add_landing_gear-mat41.1-0 ; 
       add_landing_gear-mat42.1-0 ; 
       add_landing_gear-mat43.1-0 ; 
       add_landing_gear-mat44.1-0 ; 
       add_landing_gear-mat45.1-0 ; 
       add_landing_gear-mat46.1-0 ; 
       add_landing_gear-mat47.1-0 ; 
       add_landing_gear-mat48.1-0 ; 
       add_landing_gear-mat49.1-0 ; 
       add_landing_gear-mat50.1-0 ; 
       add_landing_gear-mat51.1-0 ; 
       add_landing_gear-mat52.1-0 ; 
       add_landing_gear-mat53.1-0 ; 
       add_landing_gear-mat54.1-0 ; 
       add_landing_gear-mat55.1-0 ; 
       add_landing_gear-mat56.1-0 ; 
       add_landing_gear-nose_white-center.1-0.1-0 ; 
       add_landing_gear-nose_white-center.1-1.1-0 ; 
       add_landing_gear-port_red-left.1-0.1-0 ; 
       add_landing_gear-sides_and_bottom.1-0 ; 
       add_landing_gear-starbord_green-right.1-0.1-0 ; 
       add_landing_gear-top.1-0 ; 
       add_landing_gear-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       add_landing_gear-cube10.1-0 ROOT ; 
       add_landing_gear-cube9.1-0 ROOT ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.21-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_landing_gear.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       add_landing_gear-t2d24.1-0 ; 
       add_landing_gear-t2d27.1-0 ; 
       add_landing_gear-t2d28.1-0 ; 
       add_landing_gear-t2d29.1-0 ; 
       add_landing_gear-t2d31.1-0 ; 
       add_landing_gear-t2d33.1-0 ; 
       add_landing_gear-t2d39.1-0 ; 
       add_landing_gear-t2d40.1-0 ; 
       add_landing_gear-t2d41.1-0 ; 
       add_landing_gear-t2d42.1-0 ; 
       add_landing_gear-t2d43.1-0 ; 
       add_landing_gear-t2d44.1-0 ; 
       add_landing_gear-t2d45.1-0 ; 
       add_landing_gear-t2d46.1-0 ; 
       add_landing_gear-t2d47.1-0 ; 
       add_landing_gear-t2d48.1-0 ; 
       add_landing_gear-t2d49.1-0 ; 
       add_landing_gear-t2d50.1-0 ; 
       add_landing_gear-t2d51.1-0 ; 
       add_landing_gear-t2d52.1-0 ; 
       add_landing_gear-t2d53.1-0 ; 
       add_landing_gear-t2d54.1-0 ; 
       add_landing_gear-t2d55.1-0 ; 
       add_landing_gear-t2d56.1-0 ; 
       add_landing_gear-t2d57.1-0 ; 
       add_landing_gear-t2d58.1-0 ; 
       add_landing_gear-t2d6.1-0 ; 
       add_landing_gear-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 5 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 10 110 ; 
       49 9 110 ; 
       30 7 110 ; 
       31 6 110 ; 
       22 32 110 ; 
       23 32 110 ; 
       24 32 110 ; 
       25 32 110 ; 
       26 32 110 ; 
       27 32 110 ; 
       28 10 110 ; 
       50 9 110 ; 
       2 9 110 ; 
       4 18 110 ; 
       32 10 110 ; 
       33 10 110 ; 
       34 10 110 ; 
       35 12 110 ; 
       36 13 110 ; 
       37 14 110 ; 
       38 15 110 ; 
       39 16 110 ; 
       40 17 110 ; 
       41 10 110 ; 
       42 22 110 ; 
       43 23 110 ; 
       44 24 110 ; 
       45 25 110 ; 
       46 26 110 ; 
       47 27 110 ; 
       48 10 110 ; 
       20 4 110 ; 
       19 3 110 ; 
       29 8 110 ; 
       3 18 110 ; 
       5 18 110 ; 
       6 28 110 ; 
       7 28 110 ; 
       8 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 4 300 ; 
       10 45 300 ; 
       10 1 300 ; 
       10 7 300 ; 
       10 43 300 ; 
       10 0 300 ; 
       10 46 300 ; 
       12 16 300 ; 
       13 17 300 ; 
       14 18 300 ; 
       15 19 300 ; 
       16 20 300 ; 
       17 21 300 ; 
       18 3 300 ; 
       18 6 300 ; 
       18 9 300 ; 
       22 15 300 ; 
       23 14 300 ; 
       24 13 300 ; 
       25 12 300 ; 
       26 10 300 ; 
       27 11 300 ; 
       28 2 300 ; 
       28 5 300 ; 
       28 8 300 ; 
       4 35 300 ; 
       33 41 300 ; 
       34 42 300 ; 
       35 23 300 ; 
       36 22 300 ; 
       37 24 300 ; 
       38 25 300 ; 
       39 26 300 ; 
       40 27 300 ; 
       41 44 300 ; 
       42 28 300 ; 
       43 29 300 ; 
       44 30 300 ; 
       45 31 300 ; 
       46 32 300 ; 
       47 33 300 ; 
       48 40 300 ; 
       3 34 300 ; 
       5 36 300 ; 
       6 37 300 ; 
       7 38 300 ; 
       8 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       12 2 400 ; 
       13 3 400 ; 
       15 4 400 ; 
       17 5 400 ; 
       22 1 400 ; 
       25 0 400 ; 
       27 27 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       43 11 401 ; 
       45 26 401 ; 
       46 15 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 77.5 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       10 SCHEM 73.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 88.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 105 -10 0 MPRFLG 0 ; 
       13 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 78.75 -10 0 MPRFLG 0 ; 
       15 SCHEM 85 -10 0 MPRFLG 0 ; 
       16 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       17 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 120 -8 0 MPRFLG 0 ; 
       49 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -10 0 MPRFLG 0 ; 
       23 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       24 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       25 SCHEM 35 -10 0 MPRFLG 0 ; 
       26 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       27 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 MPRFLG 0 ; 
       50 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 116.25 -10 0 MPRFLG 0 ; 
       32 SCHEM 40 -8 0 MPRFLG 0 ; 
       33 SCHEM 60 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 62.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 70 -12 0 MPRFLG 0 ; 
       37 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       38 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 90 -12 0 MPRFLG 0 ; 
       40 SCHEM 95 -12 0 MPRFLG 0 ; 
       41 SCHEM 65 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       45 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       46 SCHEM 40 -12 0 MPRFLG 0 ; 
       47 SCHEM 45 -12 0 MPRFLG 0 ; 
       48 SCHEM 67.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 111.25 -10 0 MPRFLG 0 ; 
       6 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 11.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 10 -16 0 DISPLAY 0 0 SRT 0.03199993 0.03199993 0.03199993 0 0 0 0.3226624 -0.6331027 -0.411007 MPRFLG 0 ; 
       0 SCHEM 7.5 -16 0 DISPLAY 0 0 SRT 0.03199993 0.03199993 0.03199993 0 0.61 0 0.5170741 -0.6331027 -0.544676 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 145 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 156.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
