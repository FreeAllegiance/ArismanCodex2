SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       add_landing_gear-null1.1-0 ; 
       fig08-fig08_3.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.38-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 67     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_landing_gear-back1.1-0 ; 
       add_landing_gear-base.1-0 ; 
       add_landing_gear-base1.1-0 ; 
       add_landing_gear-base2.1-0 ; 
       add_landing_gear-bottom.1-0 ; 
       add_landing_gear-caution1.1-0 ; 
       add_landing_gear-caution2.1-0 ; 
       add_landing_gear-front1.1-0 ; 
       add_landing_gear-guns1.1-0 ; 
       add_landing_gear-guns2.1-0 ; 
       add_landing_gear-mat23.1-0 ; 
       add_landing_gear-mat24.1-0 ; 
       add_landing_gear-mat25.1-0 ; 
       add_landing_gear-mat26.1-0 ; 
       add_landing_gear-mat27.1-0 ; 
       add_landing_gear-mat28.1-0 ; 
       add_landing_gear-mat29.1-0 ; 
       add_landing_gear-mat30.1-0 ; 
       add_landing_gear-mat31.1-0 ; 
       add_landing_gear-mat32.1-0 ; 
       add_landing_gear-mat33.1-0 ; 
       add_landing_gear-mat34.1-0 ; 
       add_landing_gear-mat39.1-0 ; 
       add_landing_gear-mat40.1-0 ; 
       add_landing_gear-mat41.1-0 ; 
       add_landing_gear-mat42.1-0 ; 
       add_landing_gear-mat43.1-0 ; 
       add_landing_gear-mat44.1-0 ; 
       add_landing_gear-mat45.1-0 ; 
       add_landing_gear-mat46.1-0 ; 
       add_landing_gear-mat47.1-0 ; 
       add_landing_gear-mat48.1-0 ; 
       add_landing_gear-mat49.1-0 ; 
       add_landing_gear-mat50.1-0 ; 
       add_landing_gear-mat51.1-0 ; 
       add_landing_gear-mat52.1-0 ; 
       add_landing_gear-mat53.1-0 ; 
       add_landing_gear-mat54.1-0 ; 
       add_landing_gear-mat55.1-0 ; 
       add_landing_gear-mat56.1-0 ; 
       add_landing_gear-nose_white-center.1-0.1-0 ; 
       add_landing_gear-nose_white-center.1-1.1-0 ; 
       add_landing_gear-port_red-left.1-0.1-0 ; 
       add_landing_gear-sides_and_bottom.1-0 ; 
       add_landing_gear-starbord_green-right.1-0.1-0 ; 
       add_landing_gear-top.1-0 ; 
       add_landing_gear-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       add_landing_gear-cube10.2-0 ROOT ; 
       add_landing_gear-cube9.2-0 ROOT ; 
       add_landing_gear-LL1.2-0 ; 
       add_landing_gear-LLa.1-0 ; 
       add_landing_gear-llandgr.1-0 ; 
       add_landing_gear-LLl.1-0 ; 
       add_landing_gear-LLr.1-0 ; 
       add_landing_gear-null1.1-0 ROOT ; 
       add_landing_gear-rlandgr.1-0 ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.22-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_landing_gear.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_landing_gear-t2d24.1-0 ; 
       add_landing_gear-t2d27.1-0 ; 
       add_landing_gear-t2d28.1-0 ; 
       add_landing_gear-t2d29.1-0 ; 
       add_landing_gear-t2d31.1-0 ; 
       add_landing_gear-t2d33.1-0 ; 
       add_landing_gear-t2d39.1-0 ; 
       add_landing_gear-t2d40.1-0 ; 
       add_landing_gear-t2d41.1-0 ; 
       add_landing_gear-t2d42.1-0 ; 
       add_landing_gear-t2d43.1-0 ; 
       add_landing_gear-t2d44.1-0 ; 
       add_landing_gear-t2d45.1-0 ; 
       add_landing_gear-t2d46.1-0 ; 
       add_landing_gear-t2d47.1-0 ; 
       add_landing_gear-t2d48.1-0 ; 
       add_landing_gear-t2d49.1-0 ; 
       add_landing_gear-t2d50.1-0 ; 
       add_landing_gear-t2d51.1-0 ; 
       add_landing_gear-t2d52.1-0 ; 
       add_landing_gear-t2d53.1-0 ; 
       add_landing_gear-t2d54.1-0 ; 
       add_landing_gear-t2d55.1-0 ; 
       add_landing_gear-t2d56.1-0 ; 
       add_landing_gear-t2d57.1-0 ; 
       add_landing_gear-t2d58.1-0 ; 
       add_landing_gear-t2d6.1-0 ; 
       add_landing_gear-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 7 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       8 2 110 ; 
       9 16 110 ; 
       10 25 110 ; 
       11 25 110 ; 
       12 25 110 ; 
       13 35 110 ; 
       14 35 110 ; 
       15 35 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 17 110 ; 
       26 10 110 ; 
       27 11 110 ; 
       28 12 110 ; 
       29 39 110 ; 
       30 39 110 ; 
       31 39 110 ; 
       32 39 110 ; 
       33 39 110 ; 
       34 39 110 ; 
       35 17 110 ; 
       36 15 110 ; 
       37 14 110 ; 
       38 13 110 ; 
       39 17 110 ; 
       40 17 110 ; 
       41 17 110 ; 
       42 19 110 ; 
       43 20 110 ; 
       44 21 110 ; 
       45 22 110 ; 
       46 23 110 ; 
       47 24 110 ; 
       48 17 110 ; 
       49 29 110 ; 
       50 30 110 ; 
       51 31 110 ; 
       52 32 110 ; 
       53 33 110 ; 
       54 34 110 ; 
       55 17 110 ; 
       56 16 110 ; 
       57 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       10 54 300 ; 
       11 55 300 ; 
       12 56 300 ; 
       13 57 300 ; 
       14 58 300 ; 
       15 59 300 ; 
       17 24 300 ; 
       17 65 300 ; 
       17 21 300 ; 
       17 27 300 ; 
       17 63 300 ; 
       17 20 300 ; 
       17 66 300 ; 
       19 36 300 ; 
       20 37 300 ; 
       21 38 300 ; 
       22 39 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 23 300 ; 
       25 26 300 ; 
       25 29 300 ; 
       29 35 300 ; 
       30 34 300 ; 
       31 33 300 ; 
       32 32 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       35 22 300 ; 
       35 25 300 ; 
       35 28 300 ; 
       40 61 300 ; 
       41 62 300 ; 
       42 43 300 ; 
       43 42 300 ; 
       44 44 300 ; 
       45 45 300 ; 
       46 46 300 ; 
       47 47 300 ; 
       48 64 300 ; 
       49 48 300 ; 
       50 49 300 ; 
       51 50 300 ; 
       52 51 300 ; 
       53 52 300 ; 
       54 53 300 ; 
       55 60 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 5 400 ; 
       6 11 400 ; 
       19 15 400 ; 
       20 16 400 ; 
       22 17 400 ; 
       24 18 400 ; 
       29 14 400 ; 
       32 13 400 ; 
       34 40 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
       16 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       18 12 401 ; 
       20 26 401 ; 
       24 27 401 ; 
       25 29 401 ; 
       26 31 401 ; 
       27 25 401 ; 
       28 30 401 ; 
       29 32 401 ; 
       30 19 401 ; 
       33 20 401 ; 
       34 21 401 ; 
       38 22 401 ; 
       40 23 401 ; 
       54 33 401 ; 
       55 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       63 24 401 ; 
       65 39 401 ; 
       66 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 5 -14 0 SRT 1 1 1 0 0 0 0 -0.06239066 -0.02599612 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 MPRFLG 0 ; 
       3 SCHEM 10 -18 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -18 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -18 0 MPRFLG 0 ; 
       6 SCHEM 5 -18 0 MPRFLG 0 ; 
       8 SCHEM 0 -18 0 MPRFLG 0 ; 
       0 SCHEM 15 -14 0 DISPLAY 0 0 SRT 0.03199993 0.03199993 0.03199993 0 0.61 0 0.5170741 -0.6331027 -0.544676 MPRFLG 0 ; 
       1 SCHEM 12.5 -14 0 DISPLAY 0 0 SRT 0.03199993 0.03199993 0.03199993 0 0 0 0.3226624 -0.6331027 -0.411007 MPRFLG 0 ; 
       9 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 50 -10 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 0 -10 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 5 -10 0 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       17 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 MPRFLG 0 ; 
       20 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 35 -10 0 MPRFLG 0 ; 
       22 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 40 -10 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 50 -8 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       31 SCHEM 10 -10 0 MPRFLG 0 ; 
       32 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       33 SCHEM 15 -10 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       35 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 25 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 45 -12 0 MPRFLG 0 ; 
       43 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 35 -12 0 MPRFLG 0 ; 
       45 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       46 SCHEM 40 -12 0 MPRFLG 0 ; 
       47 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 20 -12 0 MPRFLG 0 ; 
       50 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 10 -12 0 MPRFLG 0 ; 
       52 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       53 SCHEM 15 -12 0 MPRFLG 0 ; 
       54 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       55 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -1 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -1 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 51.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.5 -16 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 16 90 90 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
