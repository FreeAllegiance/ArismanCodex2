SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.24-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.39-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_landing_gear-light1.3-0 ROOT ; 
       add_landing_gear-light2.3-0 ROOT ; 
       add_landing_gear-light3.3-0 ROOT ; 
       add_landing_gear-light4.3-0 ROOT ; 
       add_landing_gear-light5.3-0 ROOT ; 
       add_landing_gear-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 65     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.2-0 ; 
       add_landing_gear-back1.1-0 ; 
       add_landing_gear-base.1-0 ; 
       add_landing_gear-base1.1-0 ; 
       add_landing_gear-base2.1-0 ; 
       add_landing_gear-bottom.1-0 ; 
       add_landing_gear-caution1.1-0 ; 
       add_landing_gear-caution2.1-0 ; 
       add_landing_gear-front1.1-0 ; 
       add_landing_gear-guns1.1-0 ; 
       add_landing_gear-guns2.1-0 ; 
       add_landing_gear-mat23.1-0 ; 
       add_landing_gear-mat24.1-0 ; 
       add_landing_gear-mat25.1-0 ; 
       add_landing_gear-mat26.1-0 ; 
       add_landing_gear-mat27.1-0 ; 
       add_landing_gear-mat28.1-0 ; 
       add_landing_gear-mat29.1-0 ; 
       add_landing_gear-mat30.1-0 ; 
       add_landing_gear-mat31.1-0 ; 
       add_landing_gear-mat32.1-0 ; 
       add_landing_gear-mat33.1-0 ; 
       add_landing_gear-mat34.1-0 ; 
       add_landing_gear-mat39.1-0 ; 
       add_landing_gear-mat40.1-0 ; 
       add_landing_gear-mat41.1-0 ; 
       add_landing_gear-mat42.1-0 ; 
       add_landing_gear-mat43.1-0 ; 
       add_landing_gear-mat44.1-0 ; 
       add_landing_gear-mat45.1-0 ; 
       add_landing_gear-mat46.1-0 ; 
       add_landing_gear-mat47.1-0 ; 
       add_landing_gear-mat48.1-0 ; 
       add_landing_gear-mat49.1-0 ; 
       add_landing_gear-mat50.1-0 ; 
       add_landing_gear-mat51.1-0 ; 
       add_landing_gear-mat52.1-0 ; 
       add_landing_gear-mat53.1-0 ; 
       add_landing_gear-mat54.1-0 ; 
       add_landing_gear-mat55.1-0 ; 
       add_landing_gear-mat56.1-0 ; 
       add_landing_gear-nose_white-center.1-0.1-0 ; 
       add_landing_gear-nose_white-center.1-1.1-0 ; 
       add_landing_gear-port_red-left.1-0.1-0 ; 
       add_landing_gear-sides_and_bottom.1-0 ; 
       add_landing_gear-starbord_green-right.1-0.1-0 ; 
       add_landing_gear-top.1-0 ; 
       add_landing_gear-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.23-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LLa.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-add_landing_gear.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       add_landing_gear-t2d24.1-0 ; 
       add_landing_gear-t2d27.1-0 ; 
       add_landing_gear-t2d28.1-0 ; 
       add_landing_gear-t2d29.1-0 ; 
       add_landing_gear-t2d31.1-0 ; 
       add_landing_gear-t2d33.1-0 ; 
       add_landing_gear-t2d39.1-0 ; 
       add_landing_gear-t2d40.1-0 ; 
       add_landing_gear-t2d41.1-0 ; 
       add_landing_gear-t2d42.1-0 ; 
       add_landing_gear-t2d43.1-0 ; 
       add_landing_gear-t2d44.1-0 ; 
       add_landing_gear-t2d45.1-0 ; 
       add_landing_gear-t2d46.1-0 ; 
       add_landing_gear-t2d47.1-0 ; 
       add_landing_gear-t2d48.1-0 ; 
       add_landing_gear-t2d49.1-0 ; 
       add_landing_gear-t2d50.1-0 ; 
       add_landing_gear-t2d51.1-0 ; 
       add_landing_gear-t2d52.1-0 ; 
       add_landing_gear-t2d53.1-0 ; 
       add_landing_gear-t2d54.1-0 ; 
       add_landing_gear-t2d55.1-0 ; 
       add_landing_gear-t2d56.1-0 ; 
       add_landing_gear-t2d57.1-0 ; 
       add_landing_gear-t2d58.1-0 ; 
       add_landing_gear-t2d59.1-0 ; 
       add_landing_gear-t2d6.1-0 ; 
       add_landing_gear-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 7 110 ; 
       9 25 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       26 9 110 ; 
       0 7 110 ; 
       1 21 110 ; 
       2 21 110 ; 
       3 21 110 ; 
       4 33 110 ; 
       5 33 110 ; 
       6 33 110 ; 
       8 7 110 ; 
       14 8 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 8 110 ; 
       22 1 110 ; 
       23 2 110 ; 
       24 3 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 37 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       33 8 110 ; 
       34 6 110 ; 
       35 5 110 ; 
       36 4 110 ; 
       37 8 110 ; 
       38 8 110 ; 
       39 8 110 ; 
       40 15 110 ; 
       41 16 110 ; 
       42 17 110 ; 
       43 18 110 ; 
       44 19 110 ; 
       45 20 110 ; 
       46 8 110 ; 
       47 27 110 ; 
       48 28 110 ; 
       49 29 110 ; 
       50 30 110 ; 
       51 31 110 ; 
       52 32 110 ; 
       53 8 110 ; 
       54 7 110 ; 
       55 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       13 17 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       26 6 300 ; 
       1 52 300 ; 
       2 53 300 ; 
       3 54 300 ; 
       4 55 300 ; 
       5 56 300 ; 
       6 57 300 ; 
       8 22 300 ; 
       8 63 300 ; 
       8 19 300 ; 
       8 25 300 ; 
       8 61 300 ; 
       8 18 300 ; 
       8 64 300 ; 
       15 34 300 ; 
       16 35 300 ; 
       17 36 300 ; 
       18 37 300 ; 
       19 38 300 ; 
       20 39 300 ; 
       21 21 300 ; 
       21 24 300 ; 
       21 27 300 ; 
       27 33 300 ; 
       28 32 300 ; 
       29 31 300 ; 
       30 30 300 ; 
       31 28 300 ; 
       32 29 300 ; 
       33 20 300 ; 
       33 23 300 ; 
       33 26 300 ; 
       38 59 300 ; 
       39 60 300 ; 
       40 41 300 ; 
       41 40 300 ; 
       42 42 300 ; 
       43 43 300 ; 
       44 44 300 ; 
       45 45 300 ; 
       46 62 300 ; 
       47 46 300 ; 
       48 47 300 ; 
       49 48 300 ; 
       50 49 300 ; 
       51 50 300 ; 
       52 51 300 ; 
       53 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       12 5 400 ; 
       15 13 400 ; 
       16 14 400 ; 
       18 15 400 ; 
       20 16 400 ; 
       27 12 400 ; 
       30 11 400 ; 
       32 39 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 37 401 ; 
       18 24 401 ; 
       22 25 401 ; 
       23 27 401 ; 
       24 29 401 ; 
       25 23 401 ; 
       26 28 401 ; 
       27 30 401 ; 
       28 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       36 20 401 ; 
       38 21 401 ; 
       52 31 401 ; 
       53 32 401 ; 
       54 33 401 ; 
       55 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       61 22 401 ; 
       63 38 401 ; 
       64 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       25 SCHEM 5 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 MPRFLG 0 ; 
       10 SCHEM 10 -10 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 5 -10 0 MPRFLG 0 ; 
       26 SCHEM 0 -10 0 MPRFLG 0 ; 
       0 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 60 -10 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 36.25 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 45 -10 0 MPRFLG 0 ; 
       17 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 50 -10 0 MPRFLG 0 ; 
       19 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 55 -10 0 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 20 -10 0 MPRFLG 0 ; 
       29 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       30 SCHEM 25 -10 0 MPRFLG 0 ; 
       31 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 30 -10 0 MPRFLG 0 ; 
       33 SCHEM 15 -8 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       38 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       41 SCHEM 45 -12 0 MPRFLG 0 ; 
       42 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 50 -12 0 MPRFLG 0 ; 
       44 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       45 SCHEM 55 -12 0 MPRFLG 0 ; 
       46 SCHEM 40 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 20 -12 0 MPRFLG 0 ; 
       49 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       50 SCHEM 25 -12 0 MPRFLG 0 ; 
       51 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 30 -12 0 MPRFLG 0 ; 
       53 SCHEM 42.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -26.9215 -20.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM -26.9215 -22.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 64 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 16 90 90 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
