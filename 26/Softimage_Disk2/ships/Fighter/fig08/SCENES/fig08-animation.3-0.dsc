SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       animation-null3.1-0 ; 
       fig08-fig08_3.27-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.42-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       animation-light1.3-0 ROOT ; 
       animation-light2.3-0 ROOT ; 
       animation-light3.3-0 ROOT ; 
       animation-light4.3-0 ROOT ; 
       animation-light5.3-0 ROOT ; 
       animation-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 83     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.2-0 ; 
       animation-back1.1-0 ; 
       animation-base.1-0 ; 
       animation-base1.1-0 ; 
       animation-base2.1-0 ; 
       animation-bottom.1-0 ; 
       animation-caution1.1-0 ; 
       animation-caution2.1-0 ; 
       animation-front1.1-0 ; 
       animation-guns1.1-0 ; 
       animation-guns2.1-0 ; 
       animation-mat23.1-0 ; 
       animation-mat24.1-0 ; 
       animation-mat25.1-0 ; 
       animation-mat26.1-0 ; 
       animation-mat27.1-0 ; 
       animation-mat28.1-0 ; 
       animation-mat29.1-0 ; 
       animation-mat30.1-0 ; 
       animation-mat31.1-0 ; 
       animation-mat32.1-0 ; 
       animation-mat33.1-0 ; 
       animation-mat34.1-0 ; 
       animation-mat39.1-0 ; 
       animation-mat40.1-0 ; 
       animation-mat41.1-0 ; 
       animation-mat42.1-0 ; 
       animation-mat43.1-0 ; 
       animation-mat44.1-0 ; 
       animation-mat45.1-0 ; 
       animation-mat46.1-0 ; 
       animation-mat47.1-0 ; 
       animation-mat48.1-0 ; 
       animation-mat49.1-0 ; 
       animation-mat50.1-0 ; 
       animation-mat51.1-0 ; 
       animation-mat52.1-0 ; 
       animation-mat53.1-0 ; 
       animation-mat54.1-0 ; 
       animation-mat55.1-0 ; 
       animation-mat56.1-0 ; 
       animation-mat67.1-0 ; 
       animation-mat68.1-0 ; 
       animation-mat69.1-0 ; 
       animation-mat70.1-0 ; 
       animation-mat71.1-0 ; 
       animation-mat72.1-0 ; 
       animation-mat73.1-0 ; 
       animation-mat74.1-0 ; 
       animation-mat75.1-0 ; 
       animation-mat76.1-0 ; 
       animation-mat77.1-0 ; 
       animation-mat78.1-0 ; 
       animation-mat79.1-0 ; 
       animation-mat80.1-0 ; 
       animation-mat81.1-0 ; 
       animation-mat82.1-0 ; 
       animation-mat83.1-0 ; 
       animation-mat84.1-0 ; 
       animation-nose_white-center.1-0.1-0 ; 
       animation-nose_white-center.1-1.1-0 ; 
       animation-port_red-left.1-0.1-0 ; 
       animation-sides_and_bottom.1-0 ; 
       animation-starbord_green-right.1-0.1-0 ; 
       animation-top.1-0 ; 
       animation-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       animation-LL3.1-0 ; 
       animation-LLa2.1-0 ; 
       animation-llandgr2.1-0 ; 
       animation-LLl2.1-0 ; 
       animation-LLr2.1-0 ; 
       animation-null3.1-0 ROOT ; 
       animation-rlandgr2.1-0 ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.26-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LLa.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-animation.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       animation-t2d24.1-0 ; 
       animation-t2d27.1-0 ; 
       animation-t2d28.1-0 ; 
       animation-t2d29.1-0 ; 
       animation-t2d31.1-0 ; 
       animation-t2d33.1-0 ; 
       animation-t2d39.1-0 ; 
       animation-t2d40.1-0 ; 
       animation-t2d41.1-0 ; 
       animation-t2d42.1-0 ; 
       animation-t2d43.1-0 ; 
       animation-t2d44.1-0 ; 
       animation-t2d45.1-0 ; 
       animation-t2d46.1-0 ; 
       animation-t2d47.1-0 ; 
       animation-t2d48.1-0 ; 
       animation-t2d49.1-0 ; 
       animation-t2d50.1-0 ; 
       animation-t2d51.1-0 ; 
       animation-t2d52.1-0 ; 
       animation-t2d53.1-0 ; 
       animation-t2d54.1-0 ; 
       animation-t2d55.1-0 ; 
       animation-t2d56.1-0 ; 
       animation-t2d57.1-0 ; 
       animation-t2d58.1-0 ; 
       animation-t2d59.1-0 ; 
       animation-t2d6.1-0 ; 
       animation-t2d60.1-0 ; 
       animation-t2d61.1-0 ; 
       animation-t2d62.1-0 ; 
       animation-t2d63.1-0 ; 
       animation-t2d64.1-0 ; 
       animation-t2d65.1-0 ; 
       animation-t2d66.1-0 ; 
       animation-t2d67.1-0 ; 
       animation-t2d68.1-0 ; 
       animation-t2d69.1-0 ; 
       animation-t2d70.1-0 ; 
       animation-t2d71.1-0 ; 
       animation-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 14 110 ; 
       8 28 110 ; 
       9 28 110 ; 
       10 28 110 ; 
       11 40 110 ; 
       12 40 110 ; 
       13 40 110 ; 
       15 14 110 ; 
       16 32 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 15 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 15 110 ; 
       29 8 110 ; 
       30 9 110 ; 
       31 10 110 ; 
       32 14 110 ; 
       33 16 110 ; 
       34 44 110 ; 
       35 44 110 ; 
       36 44 110 ; 
       37 44 110 ; 
       38 44 110 ; 
       39 44 110 ; 
       40 15 110 ; 
       41 13 110 ; 
       42 12 110 ; 
       43 11 110 ; 
       44 15 110 ; 
       45 15 110 ; 
       46 15 110 ; 
       47 22 110 ; 
       48 23 110 ; 
       49 24 110 ; 
       50 25 110 ; 
       51 26 110 ; 
       52 27 110 ; 
       53 15 110 ; 
       54 34 110 ; 
       55 35 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 15 110 ; 
       61 14 110 ; 
       62 14 110 ; 
       0 5 110 ; 
       3 0 110 ; 
       6 0 110 ; 
       2 0 110 ; 
       1 0 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 52 300 ; 
       9 53 300 ; 
       10 54 300 ; 
       11 55 300 ; 
       12 56 300 ; 
       13 57 300 ; 
       15 22 300 ; 
       15 81 300 ; 
       15 19 300 ; 
       15 25 300 ; 
       15 79 300 ; 
       15 18 300 ; 
       15 82 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       16 12 300 ; 
       16 13 300 ; 
       17 14 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       18 0 300 ; 
       18 1 300 ; 
       18 2 300 ; 
       18 3 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       20 17 300 ; 
       22 34 300 ; 
       23 35 300 ; 
       24 36 300 ; 
       25 37 300 ; 
       26 38 300 ; 
       27 39 300 ; 
       28 21 300 ; 
       28 24 300 ; 
       28 27 300 ; 
       33 4 300 ; 
       33 5 300 ; 
       33 6 300 ; 
       34 33 300 ; 
       35 32 300 ; 
       36 31 300 ; 
       37 30 300 ; 
       38 28 300 ; 
       39 29 300 ; 
       40 20 300 ; 
       40 23 300 ; 
       40 26 300 ; 
       45 77 300 ; 
       46 78 300 ; 
       47 41 300 ; 
       48 40 300 ; 
       49 42 300 ; 
       50 43 300 ; 
       51 44 300 ; 
       52 45 300 ; 
       53 80 300 ; 
       54 46 300 ; 
       55 47 300 ; 
       56 48 300 ; 
       57 49 300 ; 
       58 50 300 ; 
       59 51 300 ; 
       60 76 300 ; 
       0 58 300 ; 
       0 59 300 ; 
       0 60 300 ; 
       0 61 300 ; 
       3 62 300 ; 
       3 63 300 ; 
       3 64 300 ; 
       6 65 300 ; 
       6 66 300 ; 
       6 67 300 ; 
       2 68 300 ; 
       2 69 300 ; 
       2 70 300 ; 
       2 71 300 ; 
       1 72 300 ; 
       1 73 300 ; 
       1 74 300 ; 
       4 75 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       19 5 400 ; 
       22 13 400 ; 
       23 14 400 ; 
       25 15 400 ; 
       27 16 400 ; 
       34 12 400 ; 
       37 11 400 ; 
       39 51 400 ; 
       3 42 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       14 1 15000 ; 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 37 401 ; 
       18 24 401 ; 
       22 25 401 ; 
       23 27 401 ; 
       24 29 401 ; 
       25 23 401 ; 
       26 28 401 ; 
       27 30 401 ; 
       28 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       36 20 401 ; 
       38 21 401 ; 
       52 31 401 ; 
       53 32 401 ; 
       54 33 401 ; 
       55 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       79 22 401 ; 
       81 38 401 ; 
       82 26 401 ; 
       59 39 401 ; 
       60 40 401 ; 
       61 41 401 ; 
       63 43 401 ; 
       66 44 401 ; 
       67 45 401 ; 
       70 46 401 ; 
       71 47 401 ; 
       73 48 401 ; 
       74 49 401 ; 
       75 50 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65 -10 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 60 -10 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 15 -10 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 36.25 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       15 SCHEM 38.75 -6 0 MPRFLG 0 ; 
       16 SCHEM 5 -8 0 MPRFLG 0 ; 
       17 SCHEM 10 -10 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 5 -10 0 MPRFLG 0 ; 
       21 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       22 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 45 -10 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 50 -10 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 55 -10 0 MPRFLG 0 ; 
       28 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -6 0 MPRFLG 0 ; 
       33 SCHEM 0 -10 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       35 SCHEM 20 -10 0 MPRFLG 0 ; 
       36 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 25 -10 0 MPRFLG 0 ; 
       38 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       39 SCHEM 30 -10 0 MPRFLG 0 ; 
       40 SCHEM 15 -8 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       45 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 45 -12 0 MPRFLG 0 ; 
       49 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       50 SCHEM 50 -12 0 MPRFLG 0 ; 
       51 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 55 -12 0 MPRFLG 0 ; 
       53 SCHEM 40 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       54 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       55 SCHEM 20 -12 0 MPRFLG 0 ; 
       56 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       57 SCHEM 25 -12 0 MPRFLG 0 ; 
       58 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       59 SCHEM 30 -12 0 MPRFLG 0 ; 
       60 SCHEM 42.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 80 0 0 SRT 1 1 1 0 0 0 0 -0.06239066 0.7841253 MPRFLG 0 ; 
       0 SCHEM 80 -2 0 MPRFLG 0 ; 
       3 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 75 -4 0 MPRFLG 0 ; 
       2 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 85 -4 0 MPRFLG 0 ; 
       4 SCHEM 80 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -26.9215 -20.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 51.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 48.0785 -14.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 64 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM -26.9215 -22.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 81.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 48.0785 -16.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 74 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 87.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 58 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
