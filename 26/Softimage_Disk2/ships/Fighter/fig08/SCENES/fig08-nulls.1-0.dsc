SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.26-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       nulls-back1.1-0 ; 
       nulls-base.1-0 ; 
       nulls-base1.1-0 ; 
       nulls-base2.1-0 ; 
       nulls-bottom.1-0 ; 
       nulls-caution1.1-0 ; 
       nulls-caution2.1-0 ; 
       nulls-front1.1-0 ; 
       nulls-guns1.1-0 ; 
       nulls-guns2.1-0 ; 
       nulls-mat23.1-0 ; 
       nulls-mat24.1-0 ; 
       nulls-mat25.1-0 ; 
       nulls-mat26.1-0 ; 
       nulls-mat27.1-0 ; 
       nulls-mat28.1-0 ; 
       nulls-mat29.1-0 ; 
       nulls-mat30.1-0 ; 
       nulls-mat31.1-0 ; 
       nulls-mat32.1-0 ; 
       nulls-mat33.1-0 ; 
       nulls-mat34.1-0 ; 
       nulls-mat39.1-0 ; 
       nulls-mat40.1-0 ; 
       nulls-mat41.1-0 ; 
       nulls-mat42.1-0 ; 
       nulls-mat43.1-0 ; 
       nulls-mat44.1-0 ; 
       nulls-mat45.1-0 ; 
       nulls-mat46.1-0 ; 
       nulls-mat47.1-0 ; 
       nulls-mat48.1-0 ; 
       nulls-mat49.1-0 ; 
       nulls-mat50.1-0 ; 
       nulls-nose_white-center.1-0.1-0 ; 
       nulls-nose_white-center.1-1.1-0 ; 
       nulls-port_red-left.1-0.1-0 ; 
       nulls-sides_and_bottom.1-0 ; 
       nulls-starbord_green-right.1-0.1-0 ; 
       nulls-top.1-0 ; 
       nulls-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       fig08-cockpt.1-0 ; 
       fig08-fig08_3.11-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       nulls-t2d24.1-0 ; 
       nulls-t2d27.1-0 ; 
       nulls-t2d28.1-0 ; 
       nulls-t2d29.1-0 ; 
       nulls-t2d31.1-0 ; 
       nulls-t2d33.1-0 ; 
       nulls-t2d39.1-0 ; 
       nulls-t2d40.1-0 ; 
       nulls-t2d41.1-0 ; 
       nulls-t2d42.1-0 ; 
       nulls-t2d43.1-0 ; 
       nulls-t2d44.1-0 ; 
       nulls-t2d45.1-0 ; 
       nulls-t2d46.1-0 ; 
       nulls-t2d47.1-0 ; 
       nulls-t2d48.1-0 ; 
       nulls-t2d49.1-0 ; 
       nulls-t2d50.1-0 ; 
       nulls-t2d51.1-0 ; 
       nulls-t2d52.1-0 ; 
       nulls-t2d6.1-0 ; 
       nulls-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 10 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 2 110 ; 
       41 1 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       14 24 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 2 110 ; 
       42 1 110 ; 
       0 1 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 4 110 ; 
       28 5 110 ; 
       29 6 110 ; 
       30 7 110 ; 
       31 8 110 ; 
       32 9 110 ; 
       33 2 110 ; 
       34 14 110 ; 
       35 15 110 ; 
       36 16 110 ; 
       37 17 110 ; 
       38 18 110 ; 
       39 19 110 ; 
       40 2 110 ; 
       12 10 110 ; 
       11 10 110 ; 
       21 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 4 300 ; 
       2 39 300 ; 
       2 1 300 ; 
       2 7 300 ; 
       2 37 300 ; 
       2 0 300 ; 
       2 40 300 ; 
       4 16 300 ; 
       5 17 300 ; 
       6 18 300 ; 
       7 19 300 ; 
       8 20 300 ; 
       9 21 300 ; 
       10 3 300 ; 
       10 6 300 ; 
       10 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
       25 35 300 ; 
       26 36 300 ; 
       27 23 300 ; 
       28 22 300 ; 
       29 24 300 ; 
       30 25 300 ; 
       31 26 300 ; 
       32 27 300 ; 
       33 38 300 ; 
       34 28 300 ; 
       35 29 300 ; 
       36 30 300 ; 
       37 31 300 ; 
       38 32 300 ; 
       39 33 300 ; 
       40 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 2 400 ; 
       5 3 400 ; 
       7 4 400 ; 
       9 5 400 ; 
       14 1 400 ; 
       17 0 400 ; 
       19 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       37 11 401 ; 
       39 20 401 ; 
       40 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       2 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 35 -6 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 15 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 MPRFLG 0 ; 
       42 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 25 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 35 -8 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 40 -8 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 45 -8 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 10 -8 0 MPRFLG 0 ; 
       36 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 15 -8 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 20 -8 0 MPRFLG 0 ; 
       40 SCHEM 32.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       21 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 67.75 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
