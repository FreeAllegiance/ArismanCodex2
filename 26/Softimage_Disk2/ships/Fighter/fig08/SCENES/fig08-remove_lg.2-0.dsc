SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.40-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.59-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       remove_lg-back1.1-0 ; 
       remove_lg-base.1-0 ; 
       remove_lg-base1.1-0 ; 
       remove_lg-base2.1-0 ; 
       remove_lg-bottom.1-0 ; 
       remove_lg-caution1.1-0 ; 
       remove_lg-caution2.1-0 ; 
       remove_lg-front1.1-0 ; 
       remove_lg-guns1.1-0 ; 
       remove_lg-guns2.1-0 ; 
       remove_lg-mat23.1-0 ; 
       remove_lg-mat24.1-0 ; 
       remove_lg-mat25.1-0 ; 
       remove_lg-mat26.1-0 ; 
       remove_lg-mat27.1-0 ; 
       remove_lg-mat28.1-0 ; 
       remove_lg-mat29.1-0 ; 
       remove_lg-mat30.1-0 ; 
       remove_lg-mat31.1-0 ; 
       remove_lg-mat32.1-0 ; 
       remove_lg-mat33.1-0 ; 
       remove_lg-mat34.1-0 ; 
       remove_lg-mat39.1-0 ; 
       remove_lg-mat40.1-0 ; 
       remove_lg-mat41.1-0 ; 
       remove_lg-mat42.1-0 ; 
       remove_lg-mat43.1-0 ; 
       remove_lg-mat44.1-0 ; 
       remove_lg-mat45.1-0 ; 
       remove_lg-mat46.1-0 ; 
       remove_lg-mat47.1-0 ; 
       remove_lg-mat48.1-0 ; 
       remove_lg-mat49.1-0 ; 
       remove_lg-mat50.1-0 ; 
       remove_lg-mat51.1-0 ; 
       remove_lg-mat52.1-0 ; 
       remove_lg-mat53.1-0 ; 
       remove_lg-mat54.1-0 ; 
       remove_lg-mat55.1-0 ; 
       remove_lg-mat56.1-0 ; 
       remove_lg-nose_white-center.1-0.1-0 ; 
       remove_lg-nose_white-center.1-1.1-0 ; 
       remove_lg-port_red-left.1-0.1-0 ; 
       remove_lg-sides_and_bottom.1-0 ; 
       remove_lg-starbord_green-right.1-0.1-0 ; 
       remove_lg-top.1-0 ; 
       remove_lg-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.41-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-smoke.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-remove_lg.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       remove_lg-t2d44.1-0 ; 
       remove_lg-t2d45.1-0 ; 
       remove_lg-t2d46.1-0 ; 
       remove_lg-t2d47.1-0 ; 
       remove_lg-t2d48.1-0 ; 
       remove_lg-t2d49.1-0 ; 
       remove_lg-t2d50.1-0 ; 
       remove_lg-t2d51.1-0 ; 
       remove_lg-t2d52.1-0 ; 
       remove_lg-t2d53.1-0 ; 
       remove_lg-t2d54.1-0 ; 
       remove_lg-t2d55.1-0 ; 
       remove_lg-t2d56.1-0 ; 
       remove_lg-t2d57.1-0 ; 
       remove_lg-t2d58.1-0 ; 
       remove_lg-t2d6.1-0 ; 
       remove_lg-t2d72.1-0 ; 
       remove_lg-t2d73.1-0 ; 
       remove_lg-t2d74.1-0 ; 
       remove_lg-t2d75.1-0 ; 
       remove_lg-t2d76.1-0 ; 
       remove_lg-t2d77.1-0 ; 
       remove_lg-t2d78.1-0 ; 
       remove_lg-t2d79.1-0 ; 
       remove_lg-t2d80.1-0 ; 
       remove_lg-t2d81.1-0 ; 
       remove_lg-t2d82.1-0 ; 
       remove_lg-t2d83.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       32 7 110 ; 
       0 7 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 27 110 ; 
       5 27 110 ; 
       6 27 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 8 110 ; 
       17 1 110 ; 
       18 2 110 ; 
       19 3 110 ; 
       20 8 110 ; 
       21 31 110 ; 
       22 31 110 ; 
       23 31 110 ; 
       24 31 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 8 110 ; 
       28 6 110 ; 
       29 5 110 ; 
       30 4 110 ; 
       31 8 110 ; 
       33 8 110 ; 
       34 8 110 ; 
       35 10 110 ; 
       36 11 110 ; 
       37 12 110 ; 
       38 13 110 ; 
       39 14 110 ; 
       40 15 110 ; 
       41 8 110 ; 
       42 21 110 ; 
       43 22 110 ; 
       44 23 110 ; 
       45 24 110 ; 
       46 25 110 ; 
       47 26 110 ; 
       48 8 110 ; 
       49 7 110 ; 
       50 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 34 300 ; 
       2 35 300 ; 
       3 36 300 ; 
       4 37 300 ; 
       5 38 300 ; 
       6 39 300 ; 
       8 4 300 ; 
       8 45 300 ; 
       8 1 300 ; 
       8 7 300 ; 
       8 43 300 ; 
       8 0 300 ; 
       8 46 300 ; 
       10 16 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       13 19 300 ; 
       14 20 300 ; 
       15 21 300 ; 
       16 3 300 ; 
       16 6 300 ; 
       16 9 300 ; 
       21 15 300 ; 
       22 14 300 ; 
       23 13 300 ; 
       24 12 300 ; 
       25 10 300 ; 
       26 11 300 ; 
       27 2 300 ; 
       27 5 300 ; 
       27 8 300 ; 
       33 41 300 ; 
       34 42 300 ; 
       35 23 300 ; 
       36 22 300 ; 
       37 24 300 ; 
       38 25 300 ; 
       39 26 300 ; 
       40 27 300 ; 
       41 44 300 ; 
       42 28 300 ; 
       43 29 300 ; 
       44 30 300 ; 
       45 31 300 ; 
       46 32 300 ; 
       47 33 300 ; 
       48 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 19 401 ; 
       13 18 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 23 401 ; 
       17 22 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       39 14 401 ; 
       43 0 401 ; 
       45 15 401 ; 
       46 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       32 SCHEM 145 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 142.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 106.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 101.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 96.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 73.75 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 76.25 -6 0 MPRFLG 0 ; 
       14 SCHEM 81.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 105 -4 0 MPRFLG 0 ; 
       17 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       24 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       25 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       26 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 55 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 90 -8 0 MPRFLG 0 ; 
       36 SCHEM 65 -8 0 MPRFLG 0 ; 
       37 SCHEM 70 -8 0 MPRFLG 0 ; 
       38 SCHEM 75 -8 0 MPRFLG 0 ; 
       39 SCHEM 80 -8 0 MPRFLG 0 ; 
       40 SCHEM 85 -8 0 MPRFLG 0 ; 
       41 SCHEM 60 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 50 -8 0 MPRFLG 0 ; 
       43 SCHEM 25 -8 0 MPRFLG 0 ; 
       44 SCHEM 30 -8 0 MPRFLG 0 ; 
       45 SCHEM 35 -8 0 MPRFLG 0 ; 
       46 SCHEM 40 -8 0 MPRFLG 0 ; 
       47 SCHEM 45 -8 0 MPRFLG 0 ; 
       48 SCHEM 62.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 140 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       16 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 146.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 68 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
