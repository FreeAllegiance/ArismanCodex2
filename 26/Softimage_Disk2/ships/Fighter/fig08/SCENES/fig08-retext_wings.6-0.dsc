SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.38-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.57-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 83     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.2-0 ; 
       retext_wings-back1.1-0 ; 
       retext_wings-base.1-0 ; 
       retext_wings-base1.1-0 ; 
       retext_wings-base2.1-0 ; 
       retext_wings-bottom.1-0 ; 
       retext_wings-caution1.1-0 ; 
       retext_wings-caution2.1-0 ; 
       retext_wings-front1.1-0 ; 
       retext_wings-guns1.1-0 ; 
       retext_wings-guns2.1-0 ; 
       retext_wings-mat23.1-0 ; 
       retext_wings-mat24.1-0 ; 
       retext_wings-mat25.1-0 ; 
       retext_wings-mat26.1-0 ; 
       retext_wings-mat27.1-0 ; 
       retext_wings-mat28.1-0 ; 
       retext_wings-mat29.1-0 ; 
       retext_wings-mat30.1-0 ; 
       retext_wings-mat31.1-0 ; 
       retext_wings-mat32.1-0 ; 
       retext_wings-mat33.1-0 ; 
       retext_wings-mat34.1-0 ; 
       retext_wings-mat39.1-0 ; 
       retext_wings-mat40.1-0 ; 
       retext_wings-mat41.1-0 ; 
       retext_wings-mat42.1-0 ; 
       retext_wings-mat43.1-0 ; 
       retext_wings-mat44.1-0 ; 
       retext_wings-mat45.1-0 ; 
       retext_wings-mat46.1-0 ; 
       retext_wings-mat47.1-0 ; 
       retext_wings-mat48.1-0 ; 
       retext_wings-mat49.1-0 ; 
       retext_wings-mat50.1-0 ; 
       retext_wings-mat51.1-0 ; 
       retext_wings-mat52.1-0 ; 
       retext_wings-mat53.1-0 ; 
       retext_wings-mat54.1-0 ; 
       retext_wings-mat55.1-0 ; 
       retext_wings-mat56.1-0 ; 
       retext_wings-mat67.1-0 ; 
       retext_wings-mat68.1-0 ; 
       retext_wings-mat69.1-0 ; 
       retext_wings-mat70.1-0 ; 
       retext_wings-mat71.1-0 ; 
       retext_wings-mat72.1-0 ; 
       retext_wings-mat73.1-0 ; 
       retext_wings-mat74.1-0 ; 
       retext_wings-mat75.1-0 ; 
       retext_wings-mat76.1-0 ; 
       retext_wings-mat77.1-0 ; 
       retext_wings-mat78.1-0 ; 
       retext_wings-mat79.1-0 ; 
       retext_wings-mat80.1-0 ; 
       retext_wings-mat81.1-0 ; 
       retext_wings-mat82.1-0 ; 
       retext_wings-mat83.1-0 ; 
       retext_wings-mat84.1-0 ; 
       retext_wings-nose_white-center.1-0.1-0 ; 
       retext_wings-nose_white-center.1-1.1-0 ; 
       retext_wings-port_red-left.1-0.1-0 ; 
       retext_wings-sides_and_bottom.1-0 ; 
       retext_wings-starbord_green-right.1-0.1-0 ; 
       retext_wings-top.1-0 ; 
       retext_wings-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 64     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.39-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-retext_wings.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       retext_wings-t2d44.1-0 ; 
       retext_wings-t2d45.1-0 ; 
       retext_wings-t2d46.1-0 ; 
       retext_wings-t2d47.1-0 ; 
       retext_wings-t2d48.1-0 ; 
       retext_wings-t2d49.1-0 ; 
       retext_wings-t2d50.1-0 ; 
       retext_wings-t2d51.1-0 ; 
       retext_wings-t2d52.1-0 ; 
       retext_wings-t2d53.1-0 ; 
       retext_wings-t2d54.1-0 ; 
       retext_wings-t2d55.1-0 ; 
       retext_wings-t2d56.1-0 ; 
       retext_wings-t2d57.1-0 ; 
       retext_wings-t2d58.1-0 ; 
       retext_wings-t2d59.1-0 ; 
       retext_wings-t2d6.1-0 ; 
       retext_wings-t2d60.1-0 ; 
       retext_wings-t2d61.1-0 ; 
       retext_wings-t2d62.1-0 ; 
       retext_wings-t2d63.1-0 ; 
       retext_wings-t2d64.1-0 ; 
       retext_wings-t2d65.1-0 ; 
       retext_wings-t2d66.1-0 ; 
       retext_wings-t2d67.1-0 ; 
       retext_wings-t2d68.1-0 ; 
       retext_wings-t2d69.1-0 ; 
       retext_wings-t2d70.1-0 ; 
       retext_wings-t2d71.1-0 ; 
       retext_wings-t2d72.2-0 ; 
       retext_wings-t2d73.2-0 ; 
       retext_wings-t2d74.2-0 ; 
       retext_wings-t2d75.2-0 ; 
       retext_wings-t2d76.2-0 ; 
       retext_wings-t2d77.2-0 ; 
       retext_wings-t2d78.2-0 ; 
       retext_wings-t2d79.2-0 ; 
       retext_wings-t2d80.2-0 ; 
       retext_wings-t2d81.2-0 ; 
       retext_wings-t2d82.2-0 ; 
       retext_wings-t2d83.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 41 110 ; 
       5 41 110 ; 
       6 41 110 ; 
       8 7 110 ; 
       9 31 110 ; 
       10 32 110 ; 
       11 9 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 9 110 ; 
       16 10 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 8 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 8 110 ; 
       27 1 110 ; 
       28 2 110 ; 
       29 3 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 9 110 ; 
       34 10 110 ; 
       35 45 110 ; 
       36 45 110 ; 
       37 45 110 ; 
       38 45 110 ; 
       39 45 110 ; 
       40 45 110 ; 
       41 8 110 ; 
       42 6 110 ; 
       43 5 110 ; 
       44 4 110 ; 
       45 8 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 20 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 23 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 8 110 ; 
       55 35 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 8 110 ; 
       62 7 110 ; 
       63 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 52 300 ; 
       2 53 300 ; 
       3 54 300 ; 
       4 55 300 ; 
       5 56 300 ; 
       6 57 300 ; 
       8 22 300 ; 
       8 81 300 ; 
       8 19 300 ; 
       8 25 300 ; 
       8 79 300 ; 
       8 18 300 ; 
       8 82 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 58 300 ; 
       10 59 300 ; 
       10 60 300 ; 
       10 61 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       12 72 300 ; 
       12 73 300 ; 
       12 74 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 68 300 ; 
       14 69 300 ; 
       14 70 300 ; 
       14 71 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       16 62 300 ; 
       16 63 300 ; 
       16 64 300 ; 
       17 17 300 ; 
       18 75 300 ; 
       20 34 300 ; 
       21 35 300 ; 
       22 36 300 ; 
       23 37 300 ; 
       24 38 300 ; 
       25 39 300 ; 
       26 21 300 ; 
       26 24 300 ; 
       26 27 300 ; 
       33 4 300 ; 
       33 5 300 ; 
       33 6 300 ; 
       34 65 300 ; 
       34 66 300 ; 
       34 67 300 ; 
       35 33 300 ; 
       36 32 300 ; 
       37 31 300 ; 
       38 30 300 ; 
       39 28 300 ; 
       40 29 300 ; 
       41 20 300 ; 
       41 23 300 ; 
       41 26 300 ; 
       46 77 300 ; 
       47 78 300 ; 
       48 41 300 ; 
       49 40 300 ; 
       50 42 300 ; 
       51 43 300 ; 
       52 44 300 ; 
       53 45 300 ; 
       54 80 300 ; 
       55 46 300 ; 
       56 47 300 ; 
       57 48 300 ; 
       58 49 300 ; 
       59 50 300 ; 
       60 51 300 ; 
       61 76 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 5 400 ; 
       16 31 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 26 401 ; 
       18 13 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       25 12 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 44 401 ; 
       29 45 401 ; 
       30 43 401 ; 
       31 42 401 ; 
       32 40 401 ; 
       33 41 401 ; 
       34 47 401 ; 
       35 46 401 ; 
       36 48 401 ; 
       37 49 401 ; 
       38 50 401 ; 
       39 51 401 ; 
       52 20 401 ; 
       53 21 401 ; 
       54 22 401 ; 
       55 23 401 ; 
       56 24 401 ; 
       57 25 401 ; 
       59 28 401 ; 
       60 29 401 ; 
       61 30 401 ; 
       63 32 401 ; 
       66 33 401 ; 
       67 34 401 ; 
       70 35 401 ; 
       71 36 401 ; 
       73 37 401 ; 
       74 38 401 ; 
       75 39 401 ; 
       79 11 401 ; 
       81 27 401 ; 
       82 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 235 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 151.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 146.25 -10 0 MPRFLG 0 ; 
       3 SCHEM 141.25 -10 0 MPRFLG 0 ; 
       4 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       5 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       6 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       7 SCHEM 117.5 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 113.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 185 -10 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 195 -12 0 MPRFLG 0 ; 
       13 SCHEM 11.25 -12 0 MPRFLG 0 ; 
       14 SCHEM 173.75 -12 0 MPRFLG 0 ; 
       15 SCHEM 23.75 -12 0 MPRFLG 0 ; 
       16 SCHEM 186.25 -12 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       18 SCHEM 180 -12 0 MPRFLG 0 ; 
       19 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 136.25 -10 0 MPRFLG 0 ; 
       21 SCHEM 111.25 -10 0 MPRFLG 0 ; 
       22 SCHEM 116.25 -10 0 MPRFLG 0 ; 
       23 SCHEM 121.25 -10 0 MPRFLG 0 ; 
       24 SCHEM 126.25 -10 0 MPRFLG 0 ; 
       25 SCHEM 131.25 -10 0 MPRFLG 0 ; 
       26 SCHEM 150 -8 0 MPRFLG 0 ; 
       27 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 210 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 185 -8 0 MPRFLG 0 ; 
       33 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 165 -12 0 MPRFLG 0 ; 
       35 SCHEM 96.25 -10 0 MPRFLG 0 ; 
       36 SCHEM 71.25 -10 0 MPRFLG 0 ; 
       37 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       38 SCHEM 81.25 -10 0 MPRFLG 0 ; 
       39 SCHEM 86.25 -10 0 MPRFLG 0 ; 
       40 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       41 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       46 SCHEM 100 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 102.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 135 -12 0 MPRFLG 0 ; 
       49 SCHEM 110 -12 0 MPRFLG 0 ; 
       50 SCHEM 115 -12 0 MPRFLG 0 ; 
       51 SCHEM 120 -12 0 MPRFLG 0 ; 
       52 SCHEM 125 -12 0 MPRFLG 0 ; 
       53 SCHEM 130 -12 0 MPRFLG 0 ; 
       54 SCHEM 105 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 95 -12 0 MPRFLG 0 ; 
       56 SCHEM 70 -12 0 MPRFLG 0 ; 
       57 SCHEM 75 -12 0 MPRFLG 0 ; 
       58 SCHEM 80 -12 0 MPRFLG 0 ; 
       59 SCHEM 85 -12 0 MPRFLG 0 ; 
       60 SCHEM 90 -12 0 MPRFLG 0 ; 
       61 SCHEM 107.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 232.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -14 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 222.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 227.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 155 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 217.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 157.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 127.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 110 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 135 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 115 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 120 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 125 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 152.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 147.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 142.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 207.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 200 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 202.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 205 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 190 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 182.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 185 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 167.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 162.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 165 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 177.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 170 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 172.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 175 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 197.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 192.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 195 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 179 -14 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 220 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 212.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 225 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 0 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 97.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 112.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 137.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 122.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 127.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 220 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 217.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 222.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 227.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 225 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 155 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 157.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 152.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 147.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 142.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 212.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 200 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 202.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 205 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 187.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 182.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 162.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 165 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 172.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 175 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 192.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 195 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 179 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 236.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 68 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
