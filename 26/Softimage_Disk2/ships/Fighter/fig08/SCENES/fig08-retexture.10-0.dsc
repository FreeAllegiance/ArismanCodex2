SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.12-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       retexture-light1.10-0 ROOT ; 
       retexture-light2.10-0 ROOT ; 
       retexture-light3.10-0 ROOT ; 
       retexture-light4.10-0 ROOT ; 
       retexture-light5.10-0 ROOT ; 
       retexture-light6.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       retexture-back1.4-0 ; 
       retexture-base.1-0 ; 
       retexture-bottom.3-0 ; 
       retexture-front1.4-0 ; 
       retexture-mat16.2-0 ; 
       retexture-mat17.2-0 ; 
       retexture-mat18.2-0 ; 
       retexture-mat19.2-0 ; 
       retexture-mat20.2-0 ; 
       retexture-mat21.2-0 ; 
       retexture-mat23.2-0 ; 
       retexture-mat24.2-0 ; 
       retexture-mat25.2-0 ; 
       retexture-mat26.2-0 ; 
       retexture-mat27.2-0 ; 
       retexture-mat28.2-0 ; 
       retexture-mat29.2-0 ; 
       retexture-mat30.2-0 ; 
       retexture-mat31.2-0 ; 
       retexture-mat32.2-0 ; 
       retexture-mat33.2-0 ; 
       retexture-mat34.2-0 ; 
       retexture-mat39.2-0 ; 
       retexture-mat40.2-0 ; 
       retexture-mat41.2-0 ; 
       retexture-mat42.2-0 ; 
       retexture-mat43.2-0 ; 
       retexture-mat44.2-0 ; 
       retexture-mat45.2-0 ; 
       retexture-mat46.2-0 ; 
       retexture-mat47.2-0 ; 
       retexture-mat48.2-0 ; 
       retexture-mat49.2-0 ; 
       retexture-mat50.2-0 ; 
       retexture-nose_white-center.1-0.2-0 ; 
       retexture-nose_white-center.1-1.2-0 ; 
       retexture-port_red-left.1-0.2-0 ; 
       retexture-sides1.4-0 ; 
       retexture-starbord_green-right.1-0.2-0 ; 
       retexture-top.4-0 ; 
       retexture-vents1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig08-fig08.12-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.8-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/kezari_LF ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-retexture.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       retexture-t2d13.2-0 ; 
       retexture-t2d14.2-0 ; 
       retexture-t2d15.2-0 ; 
       retexture-t2d16.2-0 ; 
       retexture-t2d17.2-0 ; 
       retexture-t2d18.2-0 ; 
       retexture-t2d24.2-0 ; 
       retexture-t2d27.2-0 ; 
       retexture-t2d28.2-0 ; 
       retexture-t2d29.2-0 ; 
       retexture-t2d31.2-0 ; 
       retexture-t2d33.2-0 ; 
       retexture-t2d39.2-0 ; 
       retexture-t2d40.2-0 ; 
       retexture-t2d41.2-0 ; 
       retexture-t2d42.2-0 ; 
       retexture-t2d43.2-0 ; 
       retexture-t2d44.7-0 ; 
       retexture-t2d45.7-0 ; 
       retexture-t2d46.7-0 ; 
       retexture-t2d47.5-0 ; 
       retexture-t2d48.2-0 ; 
       retexture-t2d6.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 1 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 3 110 ; 
       27 4 110 ; 
       28 5 110 ; 
       29 6 110 ; 
       30 7 110 ; 
       31 8 110 ; 
       32 1 110 ; 
       33 13 110 ; 
       34 14 110 ; 
       35 15 110 ; 
       36 16 110 ; 
       37 17 110 ; 
       38 18 110 ; 
       39 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       1 39 300 ; 
       1 1 300 ; 
       1 3 300 ; 
       1 37 300 ; 
       1 0 300 ; 
       1 40 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 6 300 ; 
       9 9 300 ; 
       13 15 300 ; 
       14 14 300 ; 
       15 13 300 ; 
       16 12 300 ; 
       17 10 300 ; 
       18 11 300 ; 
       19 4 300 ; 
       19 5 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       24 35 300 ; 
       25 36 300 ; 
       26 23 300 ; 
       27 22 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
       32 38 300 ; 
       33 28 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 32 300 ; 
       38 33 300 ; 
       39 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 8 400 ; 
       4 9 400 ; 
       6 10 400 ; 
       8 11 400 ; 
       9 2 400 ; 
       13 7 400 ; 
       16 6 400 ; 
       18 5 400 ; 
       19 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       2 20 401 ; 
       3 18 401 ; 
       5 1 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       18 15 401 ; 
       20 16 401 ; 
       37 17 401 ; 
       39 22 401 ; 
       40 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -15.60165 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.9785 -4 0 SRT 0.4374 0.4374 0.4374 0 -1.192093e-007 0 0 -0.2177 0 MPRFLG 0 ; 
       1 SCHEM 129.4041 -5.601653 0 USR MPRFLG 0 ; 
       2 SCHEM 101.25 -7.601653 0 MPRFLG 0 ; 
       3 SCHEM 117.5 -9.601653 0 MPRFLG 0 ; 
       4 SCHEM 85 -9.601653 0 MPRFLG 0 ; 
       5 SCHEM 91.25 -9.601653 0 MPRFLG 0 ; 
       6 SCHEM 97.5 -9.601653 0 MPRFLG 0 ; 
       7 SCHEM 103.75 -9.601653 0 MPRFLG 0 ; 
       8 SCHEM 110 -9.601653 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -7.601653 0 MPRFLG 0 ; 
       10 SCHEM 5 -9.601653 0 MPRFLG 0 ; 
       11 SCHEM 0 -9.601653 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -9.601653 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -9.601653 0 MPRFLG 0 ; 
       14 SCHEM 36.25 -9.601653 0 MPRFLG 0 ; 
       15 SCHEM 41.25 -9.601653 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -9.601653 0 MPRFLG 0 ; 
       17 SCHEM 53.75 -9.601653 0 MPRFLG 0 ; 
       18 SCHEM 60 -9.601653 0 MPRFLG 0 ; 
       19 SCHEM 23.75 -7.601653 0 MPRFLG 0 ; 
       20 SCHEM 20 -9.601653 0 MPRFLG 0 ; 
       21 SCHEM 15 -9.601653 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -9.601653 0 MPRFLG 0 ; 
       23 SCHEM 52.5 -7.601653 0 MPRFLG 0 ; 
       24 SCHEM 72.5 -7.601653 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 75 -7.601653 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 115 -11.60165 0 MPRFLG 0 ; 
       27 SCHEM 82.5 -11.60165 0 MPRFLG 0 ; 
       28 SCHEM 90 -11.60165 0 MPRFLG 0 ; 
       29 SCHEM 95 -11.60165 0 MPRFLG 0 ; 
       30 SCHEM 102.5 -11.60165 0 MPRFLG 0 ; 
       31 SCHEM 107.5 -11.60165 0 MPRFLG 0 ; 
       32 SCHEM 77.5 -7.601653 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 65 -11.60165 0 MPRFLG 0 ; 
       34 SCHEM 35 -11.60165 0 MPRFLG 0 ; 
       35 SCHEM 40 -11.60165 0 MPRFLG 0 ; 
       36 SCHEM 45 -11.60165 0 MPRFLG 0 ; 
       37 SCHEM 52.5 -11.60165 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -11.60165 0 MPRFLG 0 ; 
       39 SCHEM 80 -7.601653 0 WIRECOL 3 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 132.5 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 135 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 127.5 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 125 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 70 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 120 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 92.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 100 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 105 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 112.5 -11.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 82.5 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 115 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 90 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 102.5 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 107.5 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 35 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 45 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 57.5 -13.60165 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 80 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 75 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 130 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 77.5 -9.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 122.5 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 137.5 -7.601653 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 117.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 85 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 97.5 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 110 -11.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -13.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -13.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 37.5 -13.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 92.5 -13.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 105 -13.60165 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 130 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 127.5 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 132.5 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 135 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 122.5 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 137.5 -9.601653 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 139 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 81 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
