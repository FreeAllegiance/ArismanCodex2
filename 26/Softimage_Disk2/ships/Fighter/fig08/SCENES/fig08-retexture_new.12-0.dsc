SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.25-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       retexture_new-back1.1-0 ; 
       retexture_new-base.1-0 ; 
       retexture_new-base1.2-0 ; 
       retexture_new-base2.1-0 ; 
       retexture_new-bottom.1-0 ; 
       retexture_new-caution1.1-0 ; 
       retexture_new-caution2.1-0 ; 
       retexture_new-front1.1-0 ; 
       retexture_new-guns1.1-0 ; 
       retexture_new-guns2.1-0 ; 
       retexture_new-mat23.1-0 ; 
       retexture_new-mat24.1-0 ; 
       retexture_new-mat25.1-0 ; 
       retexture_new-mat26.1-0 ; 
       retexture_new-mat27.1-0 ; 
       retexture_new-mat28.1-0 ; 
       retexture_new-mat29.1-0 ; 
       retexture_new-mat30.1-0 ; 
       retexture_new-mat31.1-0 ; 
       retexture_new-mat32.1-0 ; 
       retexture_new-mat33.1-0 ; 
       retexture_new-mat34.1-0 ; 
       retexture_new-mat39.1-0 ; 
       retexture_new-mat40.1-0 ; 
       retexture_new-mat41.1-0 ; 
       retexture_new-mat42.1-0 ; 
       retexture_new-mat43.1-0 ; 
       retexture_new-mat44.1-0 ; 
       retexture_new-mat45.1-0 ; 
       retexture_new-mat46.1-0 ; 
       retexture_new-mat47.1-0 ; 
       retexture_new-mat48.1-0 ; 
       retexture_new-mat49.1-0 ; 
       retexture_new-mat50.1-0 ; 
       retexture_new-nose_white-center.1-0.1-0 ; 
       retexture_new-nose_white-center.1-1.1-0 ; 
       retexture_new-port_red-left.1-0.1-0 ; 
       retexture_new-sides_and_bottom.1-0 ; 
       retexture_new-starbord_green-right.1-0.1-0 ; 
       retexture_new-top.1-0 ; 
       retexture_new-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig08-fig08_3.10-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-retexture_new.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       retexture_new-t2d24.2-0 ; 
       retexture_new-t2d27.2-0 ; 
       retexture_new-t2d28.2-0 ; 
       retexture_new-t2d29.2-0 ; 
       retexture_new-t2d31.2-0 ; 
       retexture_new-t2d33.2-0 ; 
       retexture_new-t2d39.1-0 ; 
       retexture_new-t2d40.1-0 ; 
       retexture_new-t2d41.2-0 ; 
       retexture_new-t2d42.2-0 ; 
       retexture_new-t2d43.2-0 ; 
       retexture_new-t2d44.5-0 ; 
       retexture_new-t2d45.5-0 ; 
       retexture_new-t2d46.5-0 ; 
       retexture_new-t2d47.5-0 ; 
       retexture_new-t2d48.5-0 ; 
       retexture_new-t2d49.3-0 ; 
       retexture_new-t2d50.2-0 ; 
       retexture_new-t2d51.2-0 ; 
       retexture_new-t2d52.2-0 ; 
       retexture_new-t2d6.5-0 ; 
       retexture_new-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 9 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       13 23 110 ; 
       14 23 110 ; 
       15 23 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 3 110 ; 
       27 4 110 ; 
       28 5 110 ; 
       29 6 110 ; 
       30 7 110 ; 
       31 8 110 ; 
       32 1 110 ; 
       33 13 110 ; 
       34 14 110 ; 
       35 15 110 ; 
       36 16 110 ; 
       37 17 110 ; 
       38 18 110 ; 
       39 1 110 ; 
       11 9 110 ; 
       10 9 110 ; 
       20 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 39 300 ; 
       1 1 300 ; 
       1 7 300 ; 
       1 37 300 ; 
       1 0 300 ; 
       1 40 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 3 300 ; 
       9 6 300 ; 
       9 9 300 ; 
       13 15 300 ; 
       14 14 300 ; 
       15 13 300 ; 
       16 12 300 ; 
       17 10 300 ; 
       18 11 300 ; 
       19 2 300 ; 
       19 5 300 ; 
       19 8 300 ; 
       24 35 300 ; 
       25 36 300 ; 
       26 23 300 ; 
       27 22 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
       32 38 300 ; 
       33 28 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 32 300 ; 
       38 33 300 ; 
       39 34 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 2 400 ; 
       4 3 400 ; 
       6 4 400 ; 
       8 5 400 ; 
       13 1 400 ; 
       16 0 400 ; 
       18 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       37 11 401 ; 
       39 20 401 ; 
       40 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       0 SCHEM 26.25 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       1 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 45 -10 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 35 -10 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 40 -10 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 50 -8 0 MPRFLG 0 ; 
       21 SCHEM 0 -10 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 20 -10 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 10 -10 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 25 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 45 -12 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 35 -12 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 40 -12 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 20 -12 0 MPRFLG 0 ; 
       34 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       35 SCHEM 10 -12 0 MPRFLG 0 ; 
       36 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 15 -12 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 50 -10 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       21 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 41.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 81 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
