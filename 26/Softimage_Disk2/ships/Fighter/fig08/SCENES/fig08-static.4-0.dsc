SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.51-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       static-back1.2-0 ; 
       static-base.2-0 ; 
       static-base1.2-0 ; 
       static-base2.2-0 ; 
       static-bottom.2-0 ; 
       static-caution1.2-0 ; 
       static-caution2.2-0 ; 
       static-front1.2-0 ; 
       static-guns1.2-0 ; 
       static-guns2.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat26.2-0 ; 
       static-mat27.2-0 ; 
       static-mat28.2-0 ; 
       static-mat29.2-0 ; 
       static-mat30.2-0 ; 
       static-mat31.2-0 ; 
       static-mat32.2-0 ; 
       static-mat33.2-0 ; 
       static-mat34.2-0 ; 
       static-sides_and_bottom.2-0 ; 
       static-top.2-0 ; 
       static-vents1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       fig08-fig08_3.34-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-slrsal0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       static-t2d24.2-0 ; 
       static-t2d27.2-0 ; 
       static-t2d28.2-0 ; 
       static-t2d29.2-0 ; 
       static-t2d31.2-0 ; 
       static-t2d33.2-0 ; 
       static-t2d39.2-0 ; 
       static-t2d40.2-0 ; 
       static-t2d41.2-0 ; 
       static-t2d42.2-0 ; 
       static-t2d43.2-0 ; 
       static-t2d44.2-0 ; 
       static-t2d45.2-0 ; 
       static-t2d46.2-0 ; 
       static-t2d47.2-0 ; 
       static-t2d48.2-0 ; 
       static-t2d49.2-0 ; 
       static-t2d50.2-0 ; 
       static-t2d51.2-0 ; 
       static-t2d52.2-0 ; 
       static-t2d6.2-0 ; 
       static-z.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 1 110 ; 
       17 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 23 300 ; 
       1 1 300 ; 
       1 7 300 ; 
       1 22 300 ; 
       1 0 300 ; 
       1 24 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 3 300 ; 
       9 6 300 ; 
       9 9 300 ; 
       10 15 300 ; 
       11 14 300 ; 
       12 13 300 ; 
       13 12 300 ; 
       14 10 300 ; 
       15 11 300 ; 
       16 2 300 ; 
       16 5 300 ; 
       16 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 2 400 ; 
       4 3 400 ; 
       6 4 400 ; 
       8 5 400 ; 
       10 1 400 ; 
       13 0 400 ; 
       15 21 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       22 11 401 ; 
       23 20 401 ; 
       24 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       1 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 11.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
