SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.41-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.60-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       static-back1.3-0 ; 
       static-base.3-0 ; 
       static-base1.3-0 ; 
       static-base2.3-0 ; 
       static-bottom.3-0 ; 
       static-caution1.3-0 ; 
       static-caution2.3-0 ; 
       static-front1.3-0 ; 
       static-guns1.3-0 ; 
       static-guns2.3-0 ; 
       static-mat23.3-0 ; 
       static-mat24.3-0 ; 
       static-mat25.3-0 ; 
       static-mat26.3-0 ; 
       static-mat27.3-0 ; 
       static-mat28.3-0 ; 
       static-mat29.3-0 ; 
       static-mat30.3-0 ; 
       static-mat31.3-0 ; 
       static-mat32.3-0 ; 
       static-mat33.3-0 ; 
       static-mat34.3-0 ; 
       static-mat39.3-0 ; 
       static-mat40.3-0 ; 
       static-mat41.3-0 ; 
       static-mat42.3-0 ; 
       static-mat43.3-0 ; 
       static-mat44.3-0 ; 
       static-mat45.3-0 ; 
       static-mat46.3-0 ; 
       static-mat47.3-0 ; 
       static-mat48.3-0 ; 
       static-mat49.3-0 ; 
       static-mat50.3-0 ; 
       static-mat51.3-0 ; 
       static-mat52.3-0 ; 
       static-mat53.3-0 ; 
       static-mat54.3-0 ; 
       static-mat55.3-0 ; 
       static-mat56.3-0 ; 
       static-sides_and_bottom.3-0 ; 
       static-top.3-0 ; 
       static-vents1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.42-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig08-static.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       static-t2d44.3-0 ; 
       static-t2d45.3-0 ; 
       static-t2d46.3-0 ; 
       static-t2d47.3-0 ; 
       static-t2d48.3-0 ; 
       static-t2d49.3-0 ; 
       static-t2d50.3-0 ; 
       static-t2d51.3-0 ; 
       static-t2d52.3-0 ; 
       static-t2d53.3-0 ; 
       static-t2d54.3-0 ; 
       static-t2d55.3-0 ; 
       static-t2d56.3-0 ; 
       static-t2d57.3-0 ; 
       static-t2d58.3-0 ; 
       static-t2d6.3-0 ; 
       static-t2d72.1-0 ; 
       static-t2d73.1-0 ; 
       static-t2d74.1-0 ; 
       static-t2d75.1-0 ; 
       static-t2d76.1-0 ; 
       static-t2d77.1-0 ; 
       static-t2d78.1-0 ; 
       static-t2d79.1-0 ; 
       static-t2d80.1-0 ; 
       static-t2d81.1-0 ; 
       static-t2d82.1-0 ; 
       static-t2d83.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 22 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 7 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       26 11 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 14 110 ; 
       30 16 110 ; 
       31 17 110 ; 
       32 18 110 ; 
       33 19 110 ; 
       34 20 110 ; 
       35 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 34 300 ; 
       1 35 300 ; 
       2 36 300 ; 
       3 37 300 ; 
       4 38 300 ; 
       5 39 300 ; 
       7 4 300 ; 
       7 41 300 ; 
       7 1 300 ; 
       7 7 300 ; 
       7 40 300 ; 
       7 0 300 ; 
       7 42 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       11 18 300 ; 
       12 19 300 ; 
       13 20 300 ; 
       14 21 300 ; 
       15 3 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       16 15 300 ; 
       17 14 300 ; 
       18 13 300 ; 
       19 12 300 ; 
       20 10 300 ; 
       21 11 300 ; 
       22 2 300 ; 
       22 5 300 ; 
       22 8 300 ; 
       24 23 300 ; 
       25 22 300 ; 
       26 24 300 ; 
       27 25 300 ; 
       28 26 300 ; 
       29 27 300 ; 
       30 28 300 ; 
       31 29 300 ; 
       32 30 300 ; 
       33 31 300 ; 
       34 32 300 ; 
       35 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 19 401 ; 
       13 18 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 23 401 ; 
       17 22 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       39 14 401 ; 
       40 0 401 ; 
       41 15 401 ; 
       42 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 40 -10 0 MPRFLG 0 ; 
       2 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 0 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 21.25 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       8 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 35 -10 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 25 -10 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 30 -10 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 MPRFLG 0 ; 
       16 SCHEM 20 -10 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 15 -10 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 35 -12 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       26 SCHEM 25 -12 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 30 -12 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 20 -12 0 MPRFLG 0 ; 
       31 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       32 SCHEM 10 -12 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 15 -12 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       16 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 68 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
