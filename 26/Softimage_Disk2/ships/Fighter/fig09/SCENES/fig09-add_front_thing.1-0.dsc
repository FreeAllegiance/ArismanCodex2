SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_front_thing-fig08_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.11-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_front_thing-light1.1-0 ROOT ; 
       add_front_thing-light2.1-0 ROOT ; 
       add_front_thing-light3.1-0 ROOT ; 
       add_front_thing-light4.1-0 ROOT ; 
       add_front_thing-light5.1-0 ROOT ; 
       add_front_thing-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_front_thing-back1.1-0 ; 
       add_front_thing-base.1-0 ; 
       add_front_thing-base1.1-0 ; 
       add_front_thing-base2.1-0 ; 
       add_front_thing-bottom.1-0 ; 
       add_front_thing-caution1.1-0 ; 
       add_front_thing-caution2.1-0 ; 
       add_front_thing-front1.1-0 ; 
       add_front_thing-guns1.1-0 ; 
       add_front_thing-guns2.1-0 ; 
       add_front_thing-mat23.1-0 ; 
       add_front_thing-mat24.1-0 ; 
       add_front_thing-mat25.1-0 ; 
       add_front_thing-mat26.1-0 ; 
       add_front_thing-mat27.1-0 ; 
       add_front_thing-mat28.1-0 ; 
       add_front_thing-mat29.1-0 ; 
       add_front_thing-mat30.1-0 ; 
       add_front_thing-mat31.1-0 ; 
       add_front_thing-mat32.1-0 ; 
       add_front_thing-mat33.1-0 ; 
       add_front_thing-mat34.1-0 ; 
       add_front_thing-mat39.1-0 ; 
       add_front_thing-mat40.1-0 ; 
       add_front_thing-mat41.1-0 ; 
       add_front_thing-mat42.1-0 ; 
       add_front_thing-mat43.1-0 ; 
       add_front_thing-mat44.1-0 ; 
       add_front_thing-mat45.1-0 ; 
       add_front_thing-mat46.1-0 ; 
       add_front_thing-mat47.1-0 ; 
       add_front_thing-mat48.1-0 ; 
       add_front_thing-mat49.1-0 ; 
       add_front_thing-mat50.1-0 ; 
       add_front_thing-mat51.1-0 ; 
       add_front_thing-mat52.1-0 ; 
       add_front_thing-mat53.1-0 ; 
       add_front_thing-mat54.1-0 ; 
       add_front_thing-mat56.1-0 ; 
       add_front_thing-nose_white-center.1-0.1-0 ; 
       add_front_thing-nose_white-center.1-1.1-0 ; 
       add_front_thing-port_red-left.1-0.1-0 ; 
       add_front_thing-sides_and_bottom.1-0 ; 
       add_front_thing-starbord_green-right.1-0.1-0 ; 
       add_front_thing-top.1-0 ; 
       add_front_thing-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       add_front_thing-cockpt.1-0 ; 
       add_front_thing-cube1.1-0 ; 
       add_front_thing-cube4.1-0 ; 
       add_front_thing-cyl1.1-0 ; 
       add_front_thing-fig08_3.1-0 ROOT ; 
       add_front_thing-fuselg.1-0 ; 
       add_front_thing-lslrsal0.1-0 ; 
       add_front_thing-lslrsal1.1-0 ; 
       add_front_thing-lslrsal2.1-0 ; 
       add_front_thing-lslrsal3.1-0 ; 
       add_front_thing-lslrsal4.1-0 ; 
       add_front_thing-lslrsal5.1-0 ; 
       add_front_thing-lslrsal6.2-0 ; 
       add_front_thing-lwepbar.1-0 ; 
       add_front_thing-missemt.1-0 ; 
       add_front_thing-null1.2-0 ; 
       add_front_thing-rslrsal1.1-0 ; 
       add_front_thing-rslrsal2.1-0 ; 
       add_front_thing-rslrsal3.1-0 ; 
       add_front_thing-rslrsal4.1-0 ; 
       add_front_thing-rslrsal5.1-0 ; 
       add_front_thing-rslrsal6.1-0 ; 
       add_front_thing-rwepbar.2-0 ; 
       add_front_thing-slrsal0.1-0 ; 
       add_front_thing-SSa.1-0 ; 
       add_front_thing-SSal.1-0 ; 
       add_front_thing-SSal1.1-0 ; 
       add_front_thing-SSal2.1-0 ; 
       add_front_thing-SSal3.1-0 ; 
       add_front_thing-SSal4.1-0 ; 
       add_front_thing-SSal5.1-0 ; 
       add_front_thing-SSal6.1-0 ; 
       add_front_thing-SSar.1-0 ; 
       add_front_thing-SSar1.1-0 ; 
       add_front_thing-SSar2.1-0 ; 
       add_front_thing-SSar3.1-0 ; 
       add_front_thing-SSar4.1-0 ; 
       add_front_thing-SSar5.1-0 ; 
       add_front_thing-SSar6.1-0 ; 
       add_front_thing-SSf.1-0 ; 
       add_front_thing-tetra1.1-0 ; 
       add_front_thing-tetra2.1-0 ; 
       add_front_thing-thrust.1-0 ; 
       add_front_thing-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-add_front_thing.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_front_thing-t2d24.1-0 ; 
       add_front_thing-t2d27.1-0 ; 
       add_front_thing-t2d28.1-0 ; 
       add_front_thing-t2d29.1-0 ; 
       add_front_thing-t2d31.1-0 ; 
       add_front_thing-t2d33.1-0 ; 
       add_front_thing-t2d39.1-0 ; 
       add_front_thing-t2d40.1-0 ; 
       add_front_thing-t2d41.1-0 ; 
       add_front_thing-t2d42.1-0 ; 
       add_front_thing-t2d43.1-0 ; 
       add_front_thing-t2d44.1-0 ; 
       add_front_thing-t2d45.1-0 ; 
       add_front_thing-t2d46.1-0 ; 
       add_front_thing-t2d47.1-0 ; 
       add_front_thing-t2d48.1-0 ; 
       add_front_thing-t2d49.1-0 ; 
       add_front_thing-t2d50.1-0 ; 
       add_front_thing-t2d51.1-0 ; 
       add_front_thing-t2d52.1-0 ; 
       add_front_thing-t2d53.1-0 ; 
       add_front_thing-t2d54.1-0 ; 
       add_front_thing-t2d55.1-0 ; 
       add_front_thing-t2d56.1-0 ; 
       add_front_thing-t2d58.1-0 ; 
       add_front_thing-t2d6.1-0 ; 
       add_front_thing-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 15 110 ; 
       1 15 110 ; 
       41 2 110 ; 
       2 15 110 ; 
       40 1 110 ; 
       15 5 110 ; 
       0 4 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 5 110 ; 
       23 5 110 ; 
       24 5 110 ; 
       25 5 110 ; 
       26 7 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 5 110 ; 
       33 16 110 ; 
       34 17 110 ; 
       35 18 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 21 110 ; 
       39 5 110 ; 
       42 4 110 ; 
       43 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 34 300 ; 
       1 35 300 ; 
       41 38 300 ; 
       2 36 300 ; 
       40 37 300 ; 
       5 4 300 ; 
       5 44 300 ; 
       5 1 300 ; 
       5 7 300 ; 
       5 42 300 ; 
       5 0 300 ; 
       5 45 300 ; 
       7 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       10 19 300 ; 
       11 20 300 ; 
       12 21 300 ; 
       13 3 300 ; 
       13 6 300 ; 
       13 9 300 ; 
       16 15 300 ; 
       17 14 300 ; 
       18 13 300 ; 
       19 12 300 ; 
       20 10 300 ; 
       21 11 300 ; 
       22 2 300 ; 
       22 5 300 ; 
       22 8 300 ; 
       24 40 300 ; 
       25 41 300 ; 
       26 23 300 ; 
       27 22 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
       32 43 300 ; 
       33 28 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 32 300 ; 
       38 33 300 ; 
       39 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 2 400 ; 
       8 3 400 ; 
       10 4 400 ; 
       12 5 400 ; 
       16 1 400 ; 
       19 0 400 ; 
       21 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       42 11 401 ; 
       44 25 401 ; 
       45 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 145 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 147.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 150 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 152.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 155 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 157.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       1 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       41 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       40 SCHEM 10 -8 0 MPRFLG 0 ; 
       15 SCHEM 15 -4 0 MPRFLG 0 ; 
       0 SCHEM 142.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 72.5 0 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -8.459078e-008 MPRFLG 0 ; 
       5 SCHEM 68.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 105 -6 0 MPRFLG 0 ; 
       8 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 85 -6 0 MPRFLG 0 ; 
       11 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 112.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -6 0 MPRFLG 0 ; 
       17 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 35 -6 0 MPRFLG 0 ; 
       20 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 5 -4 0 MPRFLG 0 ; 
       23 SCHEM 40 -4 0 MPRFLG 0 ; 
       24 SCHEM 60 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 70 -8 0 MPRFLG 0 ; 
       28 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 90 -8 0 MPRFLG 0 ; 
       31 SCHEM 95 -8 0 MPRFLG 0 ; 
       32 SCHEM 65 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 40 -8 0 MPRFLG 0 ; 
       38 SCHEM 45 -8 0 MPRFLG 0 ; 
       39 SCHEM 67.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 140 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 127.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 144 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
