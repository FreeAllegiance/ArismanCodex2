SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_front_thing-fig08_3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.13-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_front_thing-light1.3-0 ROOT ; 
       add_front_thing-light2.3-0 ROOT ; 
       add_front_thing-light3.3-0 ROOT ; 
       add_front_thing-light4.3-0 ROOT ; 
       add_front_thing-light5.3-0 ROOT ; 
       add_front_thing-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_front_thing-back1.1-0 ; 
       add_front_thing-base.1-0 ; 
       add_front_thing-base1.1-0 ; 
       add_front_thing-base2.1-0 ; 
       add_front_thing-bottom.1-0 ; 
       add_front_thing-caution1.1-0 ; 
       add_front_thing-caution2.1-0 ; 
       add_front_thing-front1.1-0 ; 
       add_front_thing-guns1.1-0 ; 
       add_front_thing-guns2.1-0 ; 
       add_front_thing-mat23.1-0 ; 
       add_front_thing-mat24.1-0 ; 
       add_front_thing-mat25.1-0 ; 
       add_front_thing-mat26.1-0 ; 
       add_front_thing-mat27.1-0 ; 
       add_front_thing-mat28.1-0 ; 
       add_front_thing-mat29.1-0 ; 
       add_front_thing-mat30.1-0 ; 
       add_front_thing-mat31.1-0 ; 
       add_front_thing-mat32.1-0 ; 
       add_front_thing-mat33.1-0 ; 
       add_front_thing-mat34.1-0 ; 
       add_front_thing-mat39.1-0 ; 
       add_front_thing-mat40.1-0 ; 
       add_front_thing-mat41.1-0 ; 
       add_front_thing-mat42.1-0 ; 
       add_front_thing-mat43.1-0 ; 
       add_front_thing-mat44.1-0 ; 
       add_front_thing-mat45.1-0 ; 
       add_front_thing-mat46.1-0 ; 
       add_front_thing-mat47.1-0 ; 
       add_front_thing-mat48.1-0 ; 
       add_front_thing-mat49.1-0 ; 
       add_front_thing-mat50.1-0 ; 
       add_front_thing-mat51.1-0 ; 
       add_front_thing-mat52.1-0 ; 
       add_front_thing-mat53.1-0 ; 
       add_front_thing-mat54.1-0 ; 
       add_front_thing-mat56.1-0 ; 
       add_front_thing-nose_white-center.1-0.1-0 ; 
       add_front_thing-nose_white-center.1-1.1-0 ; 
       add_front_thing-port_red-left.1-0.1-0 ; 
       add_front_thing-sides_and_bottom.1-0 ; 
       add_front_thing-starbord_green-right.1-0.1-0 ; 
       add_front_thing-top.1-0 ; 
       add_front_thing-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       add_front_thing-cube1.1-0 ; 
       add_front_thing-cube4.1-0 ; 
       add_front_thing-cyl1.1-0 ; 
       add_front_thing-fig08_3.2-0 ROOT ; 
       add_front_thing-fuselg.1-0 ; 
       add_front_thing-lslrsal0.1-0 ; 
       add_front_thing-lslrsal1.1-0 ; 
       add_front_thing-lslrsal2.1-0 ; 
       add_front_thing-lslrsal3.1-0 ; 
       add_front_thing-lslrsal4.1-0 ; 
       add_front_thing-lslrsal5.1-0 ; 
       add_front_thing-lslrsal6.2-0 ; 
       add_front_thing-lwepbar.1-0 ; 
       add_front_thing-null1.2-0 ; 
       add_front_thing-rslrsal1.1-0 ; 
       add_front_thing-rslrsal2.1-0 ; 
       add_front_thing-rslrsal3.1-0 ; 
       add_front_thing-rslrsal4.1-0 ; 
       add_front_thing-rslrsal5.1-0 ; 
       add_front_thing-rslrsal6.1-0 ; 
       add_front_thing-rwepbar.2-0 ; 
       add_front_thing-slrsal0.1-0 ; 
       add_front_thing-smoke.1-0 ; 
       add_front_thing-SSa.1-0 ; 
       add_front_thing-SSal.1-0 ; 
       add_front_thing-SSal1.1-0 ; 
       add_front_thing-SSal2.1-0 ; 
       add_front_thing-SSal3.1-0 ; 
       add_front_thing-SSal4.1-0 ; 
       add_front_thing-SSal5.1-0 ; 
       add_front_thing-SSal6.1-0 ; 
       add_front_thing-SSar.1-0 ; 
       add_front_thing-SSar1.1-0 ; 
       add_front_thing-SSar2.1-0 ; 
       add_front_thing-SSar3.1-0 ; 
       add_front_thing-SSar4.1-0 ; 
       add_front_thing-SSar5.1-0 ; 
       add_front_thing-SSar6.1-0 ; 
       add_front_thing-SSf.1-0 ; 
       add_front_thing-tetra1.1-0 ; 
       add_front_thing-tetra2.1-0 ; 
       add_front_thing-thrust.1-0 ; 
       add_front_thing-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-add_front_thing.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_front_thing-t2d24.1-0 ; 
       add_front_thing-t2d27.1-0 ; 
       add_front_thing-t2d28.1-0 ; 
       add_front_thing-t2d29.1-0 ; 
       add_front_thing-t2d31.1-0 ; 
       add_front_thing-t2d33.1-0 ; 
       add_front_thing-t2d39.1-0 ; 
       add_front_thing-t2d40.1-0 ; 
       add_front_thing-t2d41.1-0 ; 
       add_front_thing-t2d42.1-0 ; 
       add_front_thing-t2d43.1-0 ; 
       add_front_thing-t2d44.1-0 ; 
       add_front_thing-t2d45.1-0 ; 
       add_front_thing-t2d46.1-0 ; 
       add_front_thing-t2d47.1-0 ; 
       add_front_thing-t2d48.1-0 ; 
       add_front_thing-t2d49.1-0 ; 
       add_front_thing-t2d50.1-0 ; 
       add_front_thing-t2d51.1-0 ; 
       add_front_thing-t2d52.1-0 ; 
       add_front_thing-t2d53.1-0 ; 
       add_front_thing-t2d54.1-0 ; 
       add_front_thing-t2d55.1-0 ; 
       add_front_thing-t2d56.1-0 ; 
       add_front_thing-t2d58.1-0 ; 
       add_front_thing-t2d6.1-0 ; 
       add_front_thing-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 13 110 ; 
       0 13 110 ; 
       40 1 110 ; 
       1 13 110 ; 
       39 0 110 ; 
       13 4 110 ; 
       22 3 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 6 110 ; 
       26 7 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 4 110 ; 
       32 14 110 ; 
       33 15 110 ; 
       34 16 110 ; 
       35 17 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 4 110 ; 
       41 3 110 ; 
       42 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 34 300 ; 
       0 35 300 ; 
       40 38 300 ; 
       1 36 300 ; 
       39 37 300 ; 
       4 4 300 ; 
       4 44 300 ; 
       4 1 300 ; 
       4 7 300 ; 
       4 42 300 ; 
       4 0 300 ; 
       4 45 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       12 3 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 23 300 ; 
       26 22 300 ; 
       27 24 300 ; 
       28 25 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       31 43 300 ; 
       32 28 300 ; 
       33 29 300 ; 
       34 30 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 39 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 2 400 ; 
       7 3 400 ; 
       9 4 400 ; 
       11 5 400 ; 
       14 1 400 ; 
       17 0 400 ; 
       19 26 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       42 11 401 ; 
       44 25 401 ; 
       45 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 -10 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 5 -8 0 MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 28.75 -4 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -8.459078e-008 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 MPRFLG 0 ; 
       5 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 35 -10 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 40 -10 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 45 -10 0 MPRFLG 0 ; 
       12 SCHEM 50 -8 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 10 -10 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 MPRFLG 0 ; 
       20 SCHEM 0 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       23 SCHEM 25 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       26 SCHEM 35 -12 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 40 -12 0 MPRFLG 0 ; 
       29 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 45 -12 0 MPRFLG 0 ; 
       31 SCHEM 30 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       33 SCHEM 10 -12 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       35 SCHEM 15 -12 0 MPRFLG 0 ; 
       36 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 20 -12 0 MPRFLG 0 ; 
       38 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
