SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_thrust-fig08_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.24-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_thrust-light1.2-0 ROOT ; 
       add_thrust-light2.2-0 ROOT ; 
       add_thrust-light3.2-0 ROOT ; 
       add_thrust-light4.2-0 ROOT ; 
       add_thrust-light5.2-0 ROOT ; 
       add_thrust-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       add_thrust-back1.1-0 ; 
       add_thrust-base.1-0 ; 
       add_thrust-base1.1-0 ; 
       add_thrust-base2.1-0 ; 
       add_thrust-bottom.1-0 ; 
       add_thrust-caution1.1-0 ; 
       add_thrust-caution2.1-0 ; 
       add_thrust-front1.1-0 ; 
       add_thrust-guns1.1-0 ; 
       add_thrust-guns2.1-0 ; 
       add_thrust-mat23.1-0 ; 
       add_thrust-mat24.1-0 ; 
       add_thrust-mat25.1-0 ; 
       add_thrust-mat26.1-0 ; 
       add_thrust-mat27.1-0 ; 
       add_thrust-mat28.1-0 ; 
       add_thrust-mat29.1-0 ; 
       add_thrust-mat30.1-0 ; 
       add_thrust-mat31.1-0 ; 
       add_thrust-mat32.1-0 ; 
       add_thrust-mat33.1-0 ; 
       add_thrust-mat34.1-0 ; 
       add_thrust-mat39.1-0 ; 
       add_thrust-mat40.1-0 ; 
       add_thrust-mat41.1-0 ; 
       add_thrust-mat42.1-0 ; 
       add_thrust-mat43.1-0 ; 
       add_thrust-mat44.1-0 ; 
       add_thrust-mat45.1-0 ; 
       add_thrust-mat46.1-0 ; 
       add_thrust-mat47.1-0 ; 
       add_thrust-mat48.1-0 ; 
       add_thrust-mat49.1-0 ; 
       add_thrust-mat50.1-0 ; 
       add_thrust-mat51.1-0 ; 
       add_thrust-mat52.1-0 ; 
       add_thrust-mat53.1-0 ; 
       add_thrust-mat54.1-0 ; 
       add_thrust-mat56.1-0 ; 
       add_thrust-nose_white-center.1-0.1-0 ; 
       add_thrust-nose_white-center.1-1.1-0 ; 
       add_thrust-port_red-left.1-0.1-0 ; 
       add_thrust-sides_and_bottom.1-0 ; 
       add_thrust-starbord_green-right.1-0.1-0 ; 
       add_thrust-top.1-0 ; 
       add_thrust-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 45     
       add_thrust-Bthrust.1-0 ; 
       add_thrust-cube1.1-0 ; 
       add_thrust-cube4.1-0 ; 
       add_thrust-cyl1.1-0 ; 
       add_thrust-fig08_3.1-0 ROOT ; 
       add_thrust-fuselg.1-0 ; 
       add_thrust-lslrsal0.1-0 ; 
       add_thrust-lslrsal1.1-0 ; 
       add_thrust-lslrsal2.1-0 ; 
       add_thrust-lslrsal3.1-0 ; 
       add_thrust-lslrsal4.1-0 ; 
       add_thrust-lslrsal5.1-0 ; 
       add_thrust-lslrsal6.2-0 ; 
       add_thrust-lwepbar.1-0 ; 
       add_thrust-null1.2-0 ; 
       add_thrust-rslrsal1.1-0 ; 
       add_thrust-rslrsal2.1-0 ; 
       add_thrust-rslrsal3.1-0 ; 
       add_thrust-rslrsal4.1-0 ; 
       add_thrust-rslrsal5.1-0 ; 
       add_thrust-rslrsal6.1-0 ; 
       add_thrust-rwepbar.2-0 ; 
       add_thrust-slrsal0.1-0 ; 
       add_thrust-smoke.1-0 ; 
       add_thrust-SSa.1-0 ; 
       add_thrust-SSal.1-0 ; 
       add_thrust-SSal1.1-0 ; 
       add_thrust-SSal2.1-0 ; 
       add_thrust-SSal3.1-0 ; 
       add_thrust-SSal4.1-0 ; 
       add_thrust-SSal5.1-0 ; 
       add_thrust-SSal6.1-0 ; 
       add_thrust-SSar.1-0 ; 
       add_thrust-SSar1.1-0 ; 
       add_thrust-SSar2.1-0 ; 
       add_thrust-SSar3.1-0 ; 
       add_thrust-SSar4.1-0 ; 
       add_thrust-SSar5.1-0 ; 
       add_thrust-SSar6.1-0 ; 
       add_thrust-SSf.1-0 ; 
       add_thrust-tetra1.1-0 ; 
       add_thrust-tetra2.1-0 ; 
       add_thrust-thrust.1-0 ; 
       add_thrust-trail.1-0 ; 
       add_thrust-tthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-add_thrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_thrust-t2d44.1-0 ; 
       add_thrust-t2d45.1-0 ; 
       add_thrust-t2d46.1-0 ; 
       add_thrust-t2d47.1-0 ; 
       add_thrust-t2d48.1-0 ; 
       add_thrust-t2d49.1-0 ; 
       add_thrust-t2d50.1-0 ; 
       add_thrust-t2d51.1-0 ; 
       add_thrust-t2d52.1-0 ; 
       add_thrust-t2d53.1-0 ; 
       add_thrust-t2d54.1-0 ; 
       add_thrust-t2d55.1-0 ; 
       add_thrust-t2d56.1-0 ; 
       add_thrust-t2d58.1-0 ; 
       add_thrust-t2d59.1-0 ; 
       add_thrust-t2d6.1-0 ; 
       add_thrust-t2d60.1-0 ; 
       add_thrust-t2d61.1-0 ; 
       add_thrust-t2d62.1-0 ; 
       add_thrust-t2d63.1-0 ; 
       add_thrust-t2d64.1-0 ; 
       add_thrust-t2d65.1-0 ; 
       add_thrust-t2d66.1-0 ; 
       add_thrust-t2d67.1-0 ; 
       add_thrust-t2d68.1-0 ; 
       add_thrust-t2d69.1-0 ; 
       add_thrust-t2d70.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 4 110 ; 
       24 5 110 ; 
       25 5 110 ; 
       26 7 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 5 110 ; 
       33 15 110 ; 
       34 16 110 ; 
       35 17 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 20 110 ; 
       39 5 110 ; 
       40 1 110 ; 
       41 2 110 ; 
       42 4 110 ; 
       43 4 110 ; 
       44 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 35 300 ; 
       2 36 300 ; 
       3 34 300 ; 
       5 4 300 ; 
       5 44 300 ; 
       5 1 300 ; 
       5 7 300 ; 
       5 42 300 ; 
       5 0 300 ; 
       5 45 300 ; 
       7 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       10 19 300 ; 
       11 20 300 ; 
       12 21 300 ; 
       13 3 300 ; 
       13 6 300 ; 
       13 9 300 ; 
       15 15 300 ; 
       16 14 300 ; 
       17 13 300 ; 
       18 12 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       21 2 300 ; 
       21 5 300 ; 
       21 8 300 ; 
       24 40 300 ; 
       25 41 300 ; 
       26 23 300 ; 
       27 22 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
       32 43 300 ; 
       33 28 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 32 300 ; 
       38 33 300 ; 
       39 39 300 ; 
       40 37 300 ; 
       41 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 17 401 ; 
       13 16 401 ; 
       14 14 401 ; 
       15 20 401 ; 
       16 26 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       42 0 401 ; 
       44 15 401 ; 
       45 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 65 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 33.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -0.06255432 MPRFLG 0 ; 
       5 SCHEM 27.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 43.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 45 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 18.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 27.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 30 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 50 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 37.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 40 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 42.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 45 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 47.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 32.5 -4 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 12.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 15 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 17.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 20 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 22.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 35 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 10 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 55 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 57.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 62.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
