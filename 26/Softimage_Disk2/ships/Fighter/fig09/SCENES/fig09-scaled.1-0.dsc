SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       scaled-fig08_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.21-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       scaled-light1.1-0 ROOT ; 
       scaled-light2.1-0 ROOT ; 
       scaled-light3.1-0 ROOT ; 
       scaled-light4.1-0 ROOT ; 
       scaled-light5.1-0 ROOT ; 
       scaled-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       scaled-back1.1-0 ; 
       scaled-base.1-0 ; 
       scaled-base1.1-0 ; 
       scaled-base2.1-0 ; 
       scaled-bottom.1-0 ; 
       scaled-caution1.1-0 ; 
       scaled-caution2.1-0 ; 
       scaled-front1.1-0 ; 
       scaled-guns1.1-0 ; 
       scaled-guns2.1-0 ; 
       scaled-mat23.1-0 ; 
       scaled-mat24.1-0 ; 
       scaled-mat25.1-0 ; 
       scaled-mat26.1-0 ; 
       scaled-mat27.1-0 ; 
       scaled-mat28.1-0 ; 
       scaled-mat29.1-0 ; 
       scaled-mat30.1-0 ; 
       scaled-mat31.1-0 ; 
       scaled-mat32.1-0 ; 
       scaled-mat33.1-0 ; 
       scaled-mat34.1-0 ; 
       scaled-mat39.1-0 ; 
       scaled-mat40.1-0 ; 
       scaled-mat41.1-0 ; 
       scaled-mat42.1-0 ; 
       scaled-mat43.1-0 ; 
       scaled-mat44.1-0 ; 
       scaled-mat45.1-0 ; 
       scaled-mat46.1-0 ; 
       scaled-mat47.1-0 ; 
       scaled-mat48.1-0 ; 
       scaled-mat49.1-0 ; 
       scaled-mat50.1-0 ; 
       scaled-mat51.1-0 ; 
       scaled-mat52.1-0 ; 
       scaled-mat53.1-0 ; 
       scaled-mat54.1-0 ; 
       scaled-mat56.1-0 ; 
       scaled-nose_white-center.1-0.1-0 ; 
       scaled-nose_white-center.1-1.1-0 ; 
       scaled-port_red-left.1-0.1-0 ; 
       scaled-sides_and_bottom.1-0 ; 
       scaled-starbord_green-right.1-0.1-0 ; 
       scaled-top.1-0 ; 
       scaled-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       scaled-cube1.1-0 ; 
       scaled-cube4.1-0 ; 
       scaled-cyl1.1-0 ; 
       scaled-fig08_3.1-0 ROOT ; 
       scaled-fuselg.1-0 ; 
       scaled-lslrsal0.1-0 ; 
       scaled-lslrsal1.1-0 ; 
       scaled-lslrsal2.1-0 ; 
       scaled-lslrsal3.1-0 ; 
       scaled-lslrsal4.1-0 ; 
       scaled-lslrsal5.1-0 ; 
       scaled-lslrsal6.2-0 ; 
       scaled-lwepbar.1-0 ; 
       scaled-null1.2-0 ; 
       scaled-rslrsal1.1-0 ; 
       scaled-rslrsal2.1-0 ; 
       scaled-rslrsal3.1-0 ; 
       scaled-rslrsal4.1-0 ; 
       scaled-rslrsal5.1-0 ; 
       scaled-rslrsal6.1-0 ; 
       scaled-rwepbar.2-0 ; 
       scaled-slrsal0.1-0 ; 
       scaled-smoke.1-0 ; 
       scaled-SSa.1-0 ; 
       scaled-SSal.1-0 ; 
       scaled-SSal1.1-0 ; 
       scaled-SSal2.1-0 ; 
       scaled-SSal3.1-0 ; 
       scaled-SSal4.1-0 ; 
       scaled-SSal5.1-0 ; 
       scaled-SSal6.1-0 ; 
       scaled-SSar.1-0 ; 
       scaled-SSar1.1-0 ; 
       scaled-SSar2.1-0 ; 
       scaled-SSar3.1-0 ; 
       scaled-SSar4.1-0 ; 
       scaled-SSar5.1-0 ; 
       scaled-SSar6.1-0 ; 
       scaled-SSf.1-0 ; 
       scaled-tetra1.1-0 ; 
       scaled-tetra2.1-0 ; 
       scaled-thrust.1-0 ; 
       scaled-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       scaled-t2d44.1-0 ; 
       scaled-t2d45.1-0 ; 
       scaled-t2d46.1-0 ; 
       scaled-t2d47.1-0 ; 
       scaled-t2d48.1-0 ; 
       scaled-t2d49.1-0 ; 
       scaled-t2d50.1-0 ; 
       scaled-t2d51.1-0 ; 
       scaled-t2d52.1-0 ; 
       scaled-t2d53.1-0 ; 
       scaled-t2d54.1-0 ; 
       scaled-t2d55.1-0 ; 
       scaled-t2d56.1-0 ; 
       scaled-t2d58.1-0 ; 
       scaled-t2d59.1-0 ; 
       scaled-t2d6.1-0 ; 
       scaled-t2d60.1-0 ; 
       scaled-t2d61.1-0 ; 
       scaled-t2d62.1-0 ; 
       scaled-t2d63.1-0 ; 
       scaled-t2d64.1-0 ; 
       scaled-t2d65.1-0 ; 
       scaled-t2d66.1-0 ; 
       scaled-t2d67.1-0 ; 
       scaled-t2d68.1-0 ; 
       scaled-t2d69.1-0 ; 
       scaled-t2d70.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 3 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 6 110 ; 
       26 7 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 4 110 ; 
       32 14 110 ; 
       33 15 110 ; 
       34 16 110 ; 
       35 17 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 4 110 ; 
       39 0 110 ; 
       40 1 110 ; 
       41 3 110 ; 
       42 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       1 36 300 ; 
       2 34 300 ; 
       4 4 300 ; 
       4 44 300 ; 
       4 1 300 ; 
       4 7 300 ; 
       4 42 300 ; 
       4 0 300 ; 
       4 45 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       12 3 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 23 300 ; 
       26 22 300 ; 
       27 24 300 ; 
       28 25 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       31 43 300 ; 
       32 28 300 ; 
       33 29 300 ; 
       34 30 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 39 300 ; 
       39 37 300 ; 
       40 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 17 401 ; 
       13 16 401 ; 
       14 14 401 ; 
       15 20 401 ; 
       16 26 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       42 0 401 ; 
       44 15 401 ; 
       45 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -0.06255432 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 45 -6 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 27.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 30 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 50 -8 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 40 -8 0 MPRFLG 0 ; 
       28 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 45 -8 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 32.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 25 -8 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 15 -8 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 20 -8 0 MPRFLG 0 ; 
       37 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 5 -8 0 MPRFLG 0 ; 
       40 SCHEM 10 -8 0 MPRFLG 0 ; 
       41 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
