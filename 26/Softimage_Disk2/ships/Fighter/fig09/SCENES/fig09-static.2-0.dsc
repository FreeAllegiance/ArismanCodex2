SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       static-fig08_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.19-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1.2-0 ROOT ; 
       static-light2.2-0 ROOT ; 
       static-light3.2-0 ROOT ; 
       static-light4.2-0 ROOT ; 
       static-light5.2-0 ROOT ; 
       static-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       static-back1.2-0 ; 
       static-base.2-0 ; 
       static-base1.2-0 ; 
       static-base2.2-0 ; 
       static-bottom.2-0 ; 
       static-caution1.2-0 ; 
       static-caution2.2-0 ; 
       static-front1.2-0 ; 
       static-guns1.2-0 ; 
       static-guns2.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat26.2-0 ; 
       static-mat27.2-0 ; 
       static-mat28.2-0 ; 
       static-mat29.2-0 ; 
       static-mat30.2-0 ; 
       static-mat31.2-0 ; 
       static-mat32.2-0 ; 
       static-mat33.2-0 ; 
       static-mat34.2-0 ; 
       static-mat39.2-0 ; 
       static-mat40.2-0 ; 
       static-mat41.2-0 ; 
       static-mat42.2-0 ; 
       static-mat43.2-0 ; 
       static-mat44.2-0 ; 
       static-mat45.2-0 ; 
       static-mat46.2-0 ; 
       static-mat47.2-0 ; 
       static-mat48.2-0 ; 
       static-mat49.2-0 ; 
       static-mat50.2-0 ; 
       static-mat51.2-0 ; 
       static-mat52.2-0 ; 
       static-mat53.2-0 ; 
       static-mat54.2-0 ; 
       static-mat56.2-0 ; 
       static-sides_and_bottom.2-0 ; 
       static-top.2-0 ; 
       static-vents1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       static-cube1.1-0 ; 
       static-cube4.1-0 ; 
       static-cyl1.1-0 ; 
       static-fig08_3.2-0 ROOT ; 
       static-fuselg.1-0 ; 
       static-lslrsal0.1-0 ; 
       static-lslrsal1.1-0 ; 
       static-lslrsal2.1-0 ; 
       static-lslrsal3.1-0 ; 
       static-lslrsal4.1-0 ; 
       static-lslrsal5.1-0 ; 
       static-lslrsal6.2-0 ; 
       static-lwepbar.1-0 ; 
       static-null1.2-0 ; 
       static-rslrsal1.1-0 ; 
       static-rslrsal2.1-0 ; 
       static-rslrsal3.1-0 ; 
       static-rslrsal4.1-0 ; 
       static-rslrsal5.1-0 ; 
       static-rslrsal6.1-0 ; 
       static-rwepbar.2-0 ; 
       static-slrsal0.1-0 ; 
       static-SSal1.1-0 ; 
       static-SSal2.1-0 ; 
       static-SSal3.1-0 ; 
       static-SSal4.1-0 ; 
       static-SSal5.1-0 ; 
       static-SSal6.1-0 ; 
       static-SSar1.1-0 ; 
       static-SSar2.1-0 ; 
       static-SSar3.1-0 ; 
       static-SSar4.1-0 ; 
       static-SSar5.1-0 ; 
       static-SSar6.1-0 ; 
       static-tetra1.1-0 ; 
       static-tetra2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       static-t2d44.2-0 ; 
       static-t2d45.2-0 ; 
       static-t2d46.2-0 ; 
       static-t2d47.2-0 ; 
       static-t2d48.2-0 ; 
       static-t2d49.2-0 ; 
       static-t2d50.2-0 ; 
       static-t2d51.2-0 ; 
       static-t2d52.2-0 ; 
       static-t2d53.2-0 ; 
       static-t2d54.2-0 ; 
       static-t2d55.2-0 ; 
       static-t2d56.2-0 ; 
       static-t2d58.2-0 ; 
       static-t2d59.1-0 ; 
       static-t2d6.2-0 ; 
       static-t2d60.1-0 ; 
       static-t2d61.1-0 ; 
       static-t2d62.1-0 ; 
       static-t2d63.1-0 ; 
       static-t2d64.1-0 ; 
       static-t2d65.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
       static-t2d68.1-0 ; 
       static-t2d69.1-0 ; 
       static-t2d70.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 6 110 ; 
       23 7 110 ; 
       24 8 110 ; 
       25 9 110 ; 
       26 10 110 ; 
       27 11 110 ; 
       28 14 110 ; 
       29 15 110 ; 
       30 16 110 ; 
       31 17 110 ; 
       32 18 110 ; 
       33 19 110 ; 
       34 0 110 ; 
       35 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       1 36 300 ; 
       2 34 300 ; 
       4 4 300 ; 
       4 40 300 ; 
       4 1 300 ; 
       4 7 300 ; 
       4 39 300 ; 
       4 0 300 ; 
       4 41 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       12 3 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
       22 23 300 ; 
       23 22 300 ; 
       24 24 300 ; 
       25 25 300 ; 
       26 26 300 ; 
       27 27 300 ; 
       28 28 300 ; 
       29 29 300 ; 
       30 30 300 ; 
       31 31 300 ; 
       32 32 300 ; 
       33 33 300 ; 
       34 37 300 ; 
       35 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 17 401 ; 
       13 16 401 ; 
       14 14 401 ; 
       15 20 401 ; 
       16 26 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       39 0 401 ; 
       40 15 401 ; 
       41 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -8.459078e-008 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 40 -6 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 35 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 40 -8 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 30 -8 0 MPRFLG 0 ; 
       25 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 35 -8 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 25 -8 0 MPRFLG 0 ; 
       29 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 15 -8 0 MPRFLG 0 ; 
       31 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 20 -8 0 MPRFLG 0 ; 
       33 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 5 -8 0 MPRFLG 0 ; 
       35 SCHEM 10 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
