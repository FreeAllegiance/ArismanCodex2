SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       static-fig08_3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.22-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       static-back1.3-0 ; 
       static-base.3-0 ; 
       static-base1.3-0 ; 
       static-base2.3-0 ; 
       static-bottom.3-0 ; 
       static-caution1.3-0 ; 
       static-caution2.3-0 ; 
       static-front1.3-0 ; 
       static-guns1.3-0 ; 
       static-guns2.3-0 ; 
       static-mat23.3-0 ; 
       static-mat24.3-0 ; 
       static-mat25.3-0 ; 
       static-mat26.3-0 ; 
       static-mat27.3-0 ; 
       static-mat28.3-0 ; 
       static-mat29.3-0 ; 
       static-mat30.3-0 ; 
       static-mat31.3-0 ; 
       static-mat32.3-0 ; 
       static-mat33.3-0 ; 
       static-mat34.3-0 ; 
       static-mat51.3-0 ; 
       static-mat52.3-0 ; 
       static-mat53.3-0 ; 
       static-sides_and_bottom.3-0 ; 
       static-top.3-0 ; 
       static-vents1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       static-cube1.1-0 ; 
       static-cube4.1-0 ; 
       static-cyl1.1-0 ; 
       static-fig08_3.3-0 ROOT ; 
       static-fuselg.1-0 ; 
       static-lslrsal0.1-0 ; 
       static-lslrsal1.1-0 ; 
       static-lslrsal2.1-0 ; 
       static-lslrsal3.1-0 ; 
       static-lslrsal4.1-0 ; 
       static-lslrsal5.1-0 ; 
       static-lslrsal6.2-0 ; 
       static-lwepbar.1-0 ; 
       static-null1.2-0 ; 
       static-rslrsal1.1-0 ; 
       static-rslrsal2.1-0 ; 
       static-rslrsal3.1-0 ; 
       static-rslrsal4.1-0 ; 
       static-rslrsal5.1-0 ; 
       static-rslrsal6.1-0 ; 
       static-rwepbar.2-0 ; 
       static-slrsal0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       static-t2d44.3-0 ; 
       static-t2d45.3-0 ; 
       static-t2d46.3-0 ; 
       static-t2d47.3-0 ; 
       static-t2d48.3-0 ; 
       static-t2d49.3-0 ; 
       static-t2d50.3-0 ; 
       static-t2d51.3-0 ; 
       static-t2d52.3-0 ; 
       static-t2d53.3-0 ; 
       static-t2d54.3-0 ; 
       static-t2d55.3-0 ; 
       static-t2d59.2-0 ; 
       static-t2d6.3-0 ; 
       static-t2d60.2-0 ; 
       static-t2d61.2-0 ; 
       static-t2d62.2-0 ; 
       static-t2d63.2-0 ; 
       static-t2d64.2-0 ; 
       static-t2d65.2-0 ; 
       static-t2d66.2-0 ; 
       static-t2d67.2-0 ; 
       static-t2d68.2-0 ; 
       static-t2d69.2-0 ; 
       static-t2d70.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 4 110 ; 
       21 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 23 300 ; 
       1 24 300 ; 
       2 22 300 ; 
       4 4 300 ; 
       4 26 300 ; 
       4 1 300 ; 
       4 7 300 ; 
       4 25 300 ; 
       4 0 300 ; 
       4 27 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       12 3 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 16 401 ; 
       11 17 401 ; 
       12 15 401 ; 
       13 14 401 ; 
       14 12 401 ; 
       15 18 401 ; 
       16 24 401 ; 
       17 19 401 ; 
       18 20 401 ; 
       19 21 401 ; 
       20 22 401 ; 
       21 23 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       24 11 401 ; 
       25 0 401 ; 
       26 13 401 ; 
       27 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -0.06255432 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 45 -6 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 18.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
