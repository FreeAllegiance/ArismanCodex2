SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.8-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.4-0 ROOT ; 
       text-light2.4-0 ROOT ; 
       text-light3.4-0 ROOT ; 
       text-light4.4-0 ROOT ; 
       text-light5.4-0 ROOT ; 
       text-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       text-back1.1-0 ; 
       text-base.1-0 ; 
       text-base1.1-0 ; 
       text-base2.1-0 ; 
       text-bottom.1-0 ; 
       text-caution1.1-0 ; 
       text-caution2.1-0 ; 
       text-front1.1-0 ; 
       text-guns1.1-0 ; 
       text-guns2.1-0 ; 
       text-mat23.1-0 ; 
       text-mat24.1-0 ; 
       text-mat25.1-0 ; 
       text-mat26.1-0 ; 
       text-mat27.1-0 ; 
       text-mat28.1-0 ; 
       text-mat29.1-0 ; 
       text-mat30.1-0 ; 
       text-mat31.1-0 ; 
       text-mat32.1-0 ; 
       text-mat33.1-0 ; 
       text-mat34.1-0 ; 
       text-mat39.1-0 ; 
       text-mat40.1-0 ; 
       text-mat41.1-0 ; 
       text-mat42.1-0 ; 
       text-mat43.1-0 ; 
       text-mat44.1-0 ; 
       text-mat45.1-0 ; 
       text-mat46.1-0 ; 
       text-mat47.1-0 ; 
       text-mat48.1-0 ; 
       text-mat49.1-0 ; 
       text-mat50.1-0 ; 
       text-mat51.2-0 ; 
       text-mat52.2-0 ; 
       text-mat53.2-0 ; 
       text-nose_white-center.1-0.1-0 ; 
       text-nose_white-center.1-1.1-0 ; 
       text-port_red-left.1-0.1-0 ; 
       text-sides_and_bottom.1-0 ; 
       text-starbord_green-right.1-0.1-0 ; 
       text-top.1-0 ; 
       text-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cyl1.1-0 ; 
       fig08-fig08_3.7-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-text.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       text-t2d24.1-0 ; 
       text-t2d27.1-0 ; 
       text-t2d28.1-0 ; 
       text-t2d29.1-0 ; 
       text-t2d31.1-0 ; 
       text-t2d33.1-0 ; 
       text-t2d39.1-0 ; 
       text-t2d40.1-0 ; 
       text-t2d41.1-0 ; 
       text-t2d42.1-0 ; 
       text-t2d43.1-0 ; 
       text-t2d44.2-0 ; 
       text-t2d45.2-0 ; 
       text-t2d46.2-0 ; 
       text-t2d47.2-0 ; 
       text-t2d48.2-0 ; 
       text-t2d49.1-0 ; 
       text-t2d50.1-0 ; 
       text-t2d51.1-0 ; 
       text-t2d52.1-0 ; 
       text-t2d53.2-0 ; 
       text-t2d54.2-0 ; 
       text-t2d55.2-0 ; 
       text-t2d6.2-0 ; 
       text-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       1 5 110 ; 
       2 5 110 ; 
       0 4 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 5 110 ; 
       24 5 110 ; 
       25 7 110 ; 
       26 8 110 ; 
       27 9 110 ; 
       28 10 110 ; 
       29 11 110 ; 
       30 12 110 ; 
       31 5 110 ; 
       32 15 110 ; 
       33 16 110 ; 
       34 17 110 ; 
       35 18 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 5 110 ; 
       39 4 110 ; 
       40 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 34 300 ; 
       1 35 300 ; 
       2 36 300 ; 
       5 4 300 ; 
       5 42 300 ; 
       5 1 300 ; 
       5 7 300 ; 
       5 40 300 ; 
       5 0 300 ; 
       5 43 300 ; 
       7 16 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       10 19 300 ; 
       11 20 300 ; 
       12 21 300 ; 
       13 3 300 ; 
       13 6 300 ; 
       13 9 300 ; 
       15 15 300 ; 
       16 14 300 ; 
       17 13 300 ; 
       18 12 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       21 2 300 ; 
       21 5 300 ; 
       21 8 300 ; 
       23 38 300 ; 
       24 39 300 ; 
       25 23 300 ; 
       26 22 300 ; 
       27 24 300 ; 
       28 25 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       31 41 300 ; 
       32 28 300 ; 
       33 29 300 ; 
       34 30 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 2 400 ; 
       8 3 400 ; 
       10 4 400 ; 
       12 5 400 ; 
       15 1 400 ; 
       18 0 400 ; 
       20 24 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       40 11 401 ; 
       42 23 401 ; 
       43 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 0 -16 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 MPRFLG 0 ; 
       2 SCHEM 135 0 0 MPRFLG 0 ; 
       0 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 63.75 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       5 SCHEM 60 -6 0 MPRFLG 0 ; 
       6 SCHEM 73.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 90 -10 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 70 -10 0 MPRFLG 0 ; 
       11 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 97.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -10 0 MPRFLG 0 ; 
       16 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       17 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       18 SCHEM 20 -10 0 MPRFLG 0 ; 
       19 SCHEM 26.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 25 -8 0 MPRFLG 0 ; 
       23 SCHEM 45 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       26 SCHEM 55 -12 0 MPRFLG 0 ; 
       27 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       28 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 75 -12 0 MPRFLG 0 ; 
       30 SCHEM 80 -12 0 MPRFLG 0 ; 
       31 SCHEM 50 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 25 -12 0 MPRFLG 0 ; 
       37 SCHEM 30 -12 0 MPRFLG 0 ; 
       38 SCHEM 52.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 0 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 130 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 262.5 16 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 0 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 132.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 265 16 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 129 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
