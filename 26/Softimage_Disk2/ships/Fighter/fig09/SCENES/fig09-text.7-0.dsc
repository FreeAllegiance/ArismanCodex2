SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       text-fig08_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.9-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text-light1.5-0 ROOT ; 
       text-light2.5-0 ROOT ; 
       text-light3.5-0 ROOT ; 
       text-light4.5-0 ROOT ; 
       text-light5.5-0 ROOT ; 
       text-light6.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       text-back1.1-0 ; 
       text-base.1-0 ; 
       text-base1.1-0 ; 
       text-base2.1-0 ; 
       text-bottom.1-0 ; 
       text-caution1.1-0 ; 
       text-caution2.1-0 ; 
       text-front1.1-0 ; 
       text-guns1.1-0 ; 
       text-guns2.1-0 ; 
       text-mat23.1-0 ; 
       text-mat24.1-0 ; 
       text-mat25.1-0 ; 
       text-mat26.1-0 ; 
       text-mat27.1-0 ; 
       text-mat28.1-0 ; 
       text-mat29.1-0 ; 
       text-mat30.1-0 ; 
       text-mat31.1-0 ; 
       text-mat32.1-0 ; 
       text-mat33.1-0 ; 
       text-mat34.1-0 ; 
       text-mat39.1-0 ; 
       text-mat40.1-0 ; 
       text-mat41.1-0 ; 
       text-mat42.1-0 ; 
       text-mat43.1-0 ; 
       text-mat44.1-0 ; 
       text-mat45.1-0 ; 
       text-mat46.1-0 ; 
       text-mat47.1-0 ; 
       text-mat48.1-0 ; 
       text-mat49.1-0 ; 
       text-mat50.1-0 ; 
       text-mat51.2-0 ; 
       text-mat52.2-0 ; 
       text-mat53.2-0 ; 
       text-nose_white-center.1-0.1-0 ; 
       text-nose_white-center.1-1.1-0 ; 
       text-port_red-left.1-0.1-0 ; 
       text-sides_and_bottom.1-0 ; 
       text-starbord_green-right.1-0.1-0 ; 
       text-top.1-0 ; 
       text-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       text-cockpt.1-0 ; 
       text-cube1.1-0 ; 
       text-cube4.1-0 ; 
       text-cube5.1-0 ; 
       text-cube6.1-0 ; 
       text-cyl1.1-0 ; 
       text-fig08_3.1-0 ROOT ; 
       text-fuselg.1-0 ; 
       text-lslrsal0.1-0 ; 
       text-lslrsal1.1-0 ; 
       text-lslrsal2.1-0 ; 
       text-lslrsal3.1-0 ; 
       text-lslrsal4.1-0 ; 
       text-lslrsal5.1-0 ; 
       text-lslrsal6.2-0 ; 
       text-lwepbar.1-0 ; 
       text-missemt.1-0 ; 
       text-null1.1-0 ROOT ; 
       text-rslrsal1.1-0 ; 
       text-rslrsal2.1-0 ; 
       text-rslrsal3.1-0 ; 
       text-rslrsal4.1-0 ; 
       text-rslrsal5.1-0 ; 
       text-rslrsal6.1-0 ; 
       text-rwepbar.2-0 ; 
       text-slrsal0.1-0 ; 
       text-SSa.1-0 ; 
       text-SSal.1-0 ; 
       text-SSal1.1-0 ; 
       text-SSal2.1-0 ; 
       text-SSal3.1-0 ; 
       text-SSal4.1-0 ; 
       text-SSal5.1-0 ; 
       text-SSal6.1-0 ; 
       text-SSar.1-0 ; 
       text-SSar1.1-0 ; 
       text-SSar2.1-0 ; 
       text-SSar3.1-0 ; 
       text-SSar4.1-0 ; 
       text-SSar5.1-0 ; 
       text-SSar6.1-0 ; 
       text-SSf.1-0 ; 
       text-tetra1.1-0 ; 
       text-tetra2.1-0 ; 
       text-thrust.1-0 ; 
       text-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-text.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       text-t2d24.1-0 ; 
       text-t2d27.1-0 ; 
       text-t2d28.1-0 ; 
       text-t2d29.1-0 ; 
       text-t2d31.1-0 ; 
       text-t2d33.1-0 ; 
       text-t2d39.1-0 ; 
       text-t2d40.1-0 ; 
       text-t2d41.1-0 ; 
       text-t2d42.1-0 ; 
       text-t2d43.1-0 ; 
       text-t2d44.2-0 ; 
       text-t2d45.2-0 ; 
       text-t2d46.2-0 ; 
       text-t2d47.2-0 ; 
       text-t2d48.2-0 ; 
       text-t2d49.1-0 ; 
       text-t2d50.1-0 ; 
       text-t2d51.1-0 ; 
       text-t2d52.1-0 ; 
       text-t2d53.2-0 ; 
       text-t2d54.2-0 ; 
       text-t2d55.2-0 ; 
       text-t2d6.2-0 ; 
       text-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 17 110 ; 
       1 17 110 ; 
       3 42 110 ; 
       43 2 110 ; 
       2 17 110 ; 
       42 1 110 ; 
       4 43 110 ; 
       0 6 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       18 25 110 ; 
       19 25 110 ; 
       20 25 110 ; 
       21 25 110 ; 
       22 25 110 ; 
       23 25 110 ; 
       24 7 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       27 7 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 13 110 ; 
       33 14 110 ; 
       34 7 110 ; 
       35 18 110 ; 
       36 19 110 ; 
       37 20 110 ; 
       38 21 110 ; 
       39 22 110 ; 
       40 23 110 ; 
       41 7 110 ; 
       44 6 110 ; 
       45 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 34 300 ; 
       1 35 300 ; 
       2 36 300 ; 
       7 4 300 ; 
       7 42 300 ; 
       7 1 300 ; 
       7 7 300 ; 
       7 40 300 ; 
       7 0 300 ; 
       7 43 300 ; 
       9 16 300 ; 
       10 17 300 ; 
       11 18 300 ; 
       12 19 300 ; 
       13 20 300 ; 
       14 21 300 ; 
       15 3 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       18 15 300 ; 
       19 14 300 ; 
       20 13 300 ; 
       21 12 300 ; 
       22 10 300 ; 
       23 11 300 ; 
       24 2 300 ; 
       24 5 300 ; 
       24 8 300 ; 
       26 38 300 ; 
       27 39 300 ; 
       28 23 300 ; 
       29 22 300 ; 
       30 24 300 ; 
       31 25 300 ; 
       32 26 300 ; 
       33 27 300 ; 
       34 41 300 ; 
       35 28 300 ; 
       36 29 300 ; 
       37 30 300 ; 
       38 31 300 ; 
       39 32 300 ; 
       40 33 300 ; 
       41 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 2 400 ; 
       10 3 400 ; 
       12 4 400 ; 
       14 5 400 ; 
       18 1 400 ; 
       21 0 400 ; 
       23 24 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       6 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       0 13 401 ; 
       4 14 401 ; 
       5 16 401 ; 
       6 18 401 ; 
       7 12 401 ; 
       8 17 401 ; 
       9 19 401 ; 
       10 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       18 9 401 ; 
       20 10 401 ; 
       40 11 401 ; 
       42 23 401 ; 
       43 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 5 -18 0 MPRFLG 0 ; 
       1 SCHEM 1.25 -18 0 MPRFLG 0 ; 
       3 SCHEM 0 -22 0 MPRFLG 0 ; 
       43 SCHEM 7.5 -20 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -18 0 MPRFLG 0 ; 
       42 SCHEM 0 -20 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -22 0 MPRFLG 0 ; 
       17 SCHEM 5 -16 0 SRT 0.672076 0.672076 0.672076 0 0 0 0 -0.1122814 0.6934136 MPRFLG 0 ; 
       0 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 63.75 -4 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -8.459078e-008 MPRFLG 0 ; 
       7 SCHEM 60 -6 0 MPRFLG 0 ; 
       8 SCHEM 73.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 90 -10 0 MPRFLG 0 ; 
       10 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 70 -10 0 MPRFLG 0 ; 
       13 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       14 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 40 -10 0 MPRFLG 0 ; 
       19 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       20 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       21 SCHEM 20 -10 0 MPRFLG 0 ; 
       22 SCHEM 26.25 -10 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 25 -8 0 MPRFLG 0 ; 
       26 SCHEM 45 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 55 -12 0 MPRFLG 0 ; 
       30 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       31 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       32 SCHEM 75 -12 0 MPRFLG 0 ; 
       33 SCHEM 80 -12 0 MPRFLG 0 ; 
       34 SCHEM 50 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       36 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 25 -12 0 MPRFLG 0 ; 
       40 SCHEM 30 -12 0 MPRFLG 0 ; 
       41 SCHEM 52.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 2.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 10 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 129 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
