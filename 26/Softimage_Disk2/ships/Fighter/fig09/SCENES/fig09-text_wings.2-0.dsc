SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       text_wings-fig08_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.16-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       text_wings-light1.2-0 ROOT ; 
       text_wings-light2.2-0 ROOT ; 
       text_wings-light3.2-0 ROOT ; 
       text_wings-light4.2-0 ROOT ; 
       text_wings-light5.2-0 ROOT ; 
       text_wings-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       text_wings-back1.1-0 ; 
       text_wings-base.1-0 ; 
       text_wings-base1.1-0 ; 
       text_wings-base2.1-0 ; 
       text_wings-bottom.1-0 ; 
       text_wings-caution1.1-0 ; 
       text_wings-caution2.1-0 ; 
       text_wings-front1.1-0 ; 
       text_wings-guns1.1-0 ; 
       text_wings-guns2.1-0 ; 
       text_wings-mat23.1-0 ; 
       text_wings-mat24.1-0 ; 
       text_wings-mat25.1-0 ; 
       text_wings-mat26.1-0 ; 
       text_wings-mat27.1-0 ; 
       text_wings-mat28.1-0 ; 
       text_wings-mat29.1-0 ; 
       text_wings-mat30.1-0 ; 
       text_wings-mat31.1-0 ; 
       text_wings-mat32.1-0 ; 
       text_wings-mat33.1-0 ; 
       text_wings-mat34.1-0 ; 
       text_wings-mat39.1-0 ; 
       text_wings-mat40.1-0 ; 
       text_wings-mat41.1-0 ; 
       text_wings-mat42.1-0 ; 
       text_wings-mat43.1-0 ; 
       text_wings-mat44.1-0 ; 
       text_wings-mat45.1-0 ; 
       text_wings-mat46.1-0 ; 
       text_wings-mat47.1-0 ; 
       text_wings-mat48.1-0 ; 
       text_wings-mat49.1-0 ; 
       text_wings-mat50.1-0 ; 
       text_wings-mat51.1-0 ; 
       text_wings-mat52.1-0 ; 
       text_wings-mat53.1-0 ; 
       text_wings-mat54.1-0 ; 
       text_wings-mat56.1-0 ; 
       text_wings-nose_white-center.1-0.1-0 ; 
       text_wings-nose_white-center.1-1.1-0 ; 
       text_wings-port_red-left.1-0.1-0 ; 
       text_wings-sides_and_bottom.1-0 ; 
       text_wings-starbord_green-right.1-0.1-0 ; 
       text_wings-top.1-0 ; 
       text_wings-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       text_wings-cube1.1-0 ; 
       text_wings-cube4.1-0 ; 
       text_wings-cyl1.1-0 ; 
       text_wings-fig08_3.2-0 ROOT ; 
       text_wings-fuselg.1-0 ; 
       text_wings-lslrsal0.1-0 ; 
       text_wings-lslrsal1.1-0 ; 
       text_wings-lslrsal2.1-0 ; 
       text_wings-lslrsal3.1-0 ; 
       text_wings-lslrsal4.1-0 ; 
       text_wings-lslrsal5.1-0 ; 
       text_wings-lslrsal6.2-0 ; 
       text_wings-lwepbar.1-0 ; 
       text_wings-null1.2-0 ; 
       text_wings-rslrsal1.1-0 ; 
       text_wings-rslrsal2.1-0 ; 
       text_wings-rslrsal3.1-0 ; 
       text_wings-rslrsal4.1-0 ; 
       text_wings-rslrsal5.1-0 ; 
       text_wings-rslrsal6.1-0 ; 
       text_wings-rwepbar.2-0 ; 
       text_wings-slrsal0.1-0 ; 
       text_wings-smoke.1-0 ; 
       text_wings-SSa.1-0 ; 
       text_wings-SSal.1-0 ; 
       text_wings-SSal1.1-0 ; 
       text_wings-SSal2.1-0 ; 
       text_wings-SSal3.1-0 ; 
       text_wings-SSal4.1-0 ; 
       text_wings-SSal5.1-0 ; 
       text_wings-SSal6.1-0 ; 
       text_wings-SSar.1-0 ; 
       text_wings-SSar1.1-0 ; 
       text_wings-SSar2.1-0 ; 
       text_wings-SSar3.1-0 ; 
       text_wings-SSar4.1-0 ; 
       text_wings-SSar5.1-0 ; 
       text_wings-SSar6.1-0 ; 
       text_wings-SSf.1-0 ; 
       text_wings-tetra1.1-0 ; 
       text_wings-tetra2.1-0 ; 
       text_wings-thrust.1-0 ; 
       text_wings-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-text_wings.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       text_wings-t2d44.1-0 ; 
       text_wings-t2d45.1-0 ; 
       text_wings-t2d46.1-0 ; 
       text_wings-t2d47.1-0 ; 
       text_wings-t2d48.1-0 ; 
       text_wings-t2d49.1-0 ; 
       text_wings-t2d50.1-0 ; 
       text_wings-t2d51.1-0 ; 
       text_wings-t2d52.1-0 ; 
       text_wings-t2d53.1-0 ; 
       text_wings-t2d54.1-0 ; 
       text_wings-t2d55.1-0 ; 
       text_wings-t2d56.1-0 ; 
       text_wings-t2d58.1-0 ; 
       text_wings-t2d59.1-0 ; 
       text_wings-t2d6.1-0 ; 
       text_wings-t2d60.1-0 ; 
       text_wings-t2d61.1-0 ; 
       text_wings-t2d62.1-0 ; 
       text_wings-t2d63.1-0 ; 
       text_wings-t2d64.1-0 ; 
       text_wings-t2d65.1-0 ; 
       text_wings-t2d66.1-0 ; 
       text_wings-t2d67.1-0 ; 
       text_wings-t2d68.1-0 ; 
       text_wings-t2d69.1-0 ; 
       text_wings-t2d70.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 3 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 6 110 ; 
       26 7 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 4 110 ; 
       32 14 110 ; 
       33 15 110 ; 
       34 16 110 ; 
       35 17 110 ; 
       36 18 110 ; 
       37 19 110 ; 
       38 4 110 ; 
       39 0 110 ; 
       40 1 110 ; 
       41 3 110 ; 
       42 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       1 36 300 ; 
       2 34 300 ; 
       4 4 300 ; 
       4 44 300 ; 
       4 1 300 ; 
       4 7 300 ; 
       4 42 300 ; 
       4 0 300 ; 
       4 45 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       12 3 300 ; 
       12 6 300 ; 
       12 9 300 ; 
       14 15 300 ; 
       15 14 300 ; 
       16 13 300 ; 
       17 12 300 ; 
       18 10 300 ; 
       19 11 300 ; 
       20 2 300 ; 
       20 5 300 ; 
       20 8 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 23 300 ; 
       26 22 300 ; 
       27 24 300 ; 
       28 25 300 ; 
       29 26 300 ; 
       30 27 300 ; 
       31 43 300 ; 
       32 28 300 ; 
       33 29 300 ; 
       34 30 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 39 300 ; 
       39 37 300 ; 
       40 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 18 401 ; 
       11 19 401 ; 
       12 17 401 ; 
       13 16 401 ; 
       14 14 401 ; 
       15 20 401 ; 
       16 26 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       34 9 401 ; 
       35 10 401 ; 
       36 11 401 ; 
       37 12 401 ; 
       38 13 401 ; 
       42 0 401 ; 
       44 15 401 ; 
       45 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 SRT 1 1 1 0 0 0 -8.43943e-009 -0.2177 -8.459078e-008 MPRFLG 0 ; 
       4 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 73.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 86.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 61.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 66.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 71.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 81.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       15 SCHEM 21.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 26.25 -10 0 MPRFLG 0 ; 
       17 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       18 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       19 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 120 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 85 -12 0 MPRFLG 0 ; 
       26 SCHEM 60 -12 0 MPRFLG 0 ; 
       27 SCHEM 65 -12 0 MPRFLG 0 ; 
       28 SCHEM 70 -12 0 MPRFLG 0 ; 
       29 SCHEM 75 -12 0 MPRFLG 0 ; 
       30 SCHEM 80 -12 0 MPRFLG 0 ; 
       31 SCHEM 55 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 45 -12 0 MPRFLG 0 ; 
       33 SCHEM 20 -12 0 MPRFLG 0 ; 
       34 SCHEM 25 -12 0 MPRFLG 0 ; 
       35 SCHEM 30 -12 0 MPRFLG 0 ; 
       36 SCHEM 35 -12 0 MPRFLG 0 ; 
       37 SCHEM 40 -12 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       40 SCHEM 15 -12 0 MPRFLG 0 ; 
       41 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       14 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 121.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
