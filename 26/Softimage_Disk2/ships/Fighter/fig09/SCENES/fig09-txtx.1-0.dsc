SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.2-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 83     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.1-0 ; 
       txtx-back1.1-0 ; 
       txtx-base.1-0 ; 
       txtx-base1.1-0 ; 
       txtx-base2.1-0 ; 
       txtx-bottom.1-0 ; 
       txtx-caution1.1-0 ; 
       txtx-caution2.1-0 ; 
       txtx-front1.1-0 ; 
       txtx-guns1.1-0 ; 
       txtx-guns2.1-0 ; 
       txtx-mat23.1-0 ; 
       txtx-mat24.1-0 ; 
       txtx-mat25.1-0 ; 
       txtx-mat26.1-0 ; 
       txtx-mat27.1-0 ; 
       txtx-mat28.1-0 ; 
       txtx-mat29.1-0 ; 
       txtx-mat30.1-0 ; 
       txtx-mat31.1-0 ; 
       txtx-mat32.1-0 ; 
       txtx-mat33.1-0 ; 
       txtx-mat34.1-0 ; 
       txtx-mat39.1-0 ; 
       txtx-mat40.1-0 ; 
       txtx-mat41.1-0 ; 
       txtx-mat42.1-0 ; 
       txtx-mat43.1-0 ; 
       txtx-mat44.1-0 ; 
       txtx-mat45.1-0 ; 
       txtx-mat46.1-0 ; 
       txtx-mat47.1-0 ; 
       txtx-mat48.1-0 ; 
       txtx-mat49.1-0 ; 
       txtx-mat50.1-0 ; 
       txtx-mat51.1-0 ; 
       txtx-mat52.1-0 ; 
       txtx-mat53.1-0 ; 
       txtx-mat54.1-0 ; 
       txtx-mat55.1-0 ; 
       txtx-mat56.1-0 ; 
       txtx-mat67.1-0 ; 
       txtx-mat68.1-0 ; 
       txtx-mat69.1-0 ; 
       txtx-mat70.1-0 ; 
       txtx-mat71.1-0 ; 
       txtx-mat72.1-0 ; 
       txtx-mat73.1-0 ; 
       txtx-mat74.1-0 ; 
       txtx-mat75.1-0 ; 
       txtx-mat76.1-0 ; 
       txtx-mat77.1-0 ; 
       txtx-mat78.1-0 ; 
       txtx-mat79.1-0 ; 
       txtx-mat80.1-0 ; 
       txtx-mat81.1-0 ; 
       txtx-mat82.1-0 ; 
       txtx-mat83.1-0 ; 
       txtx-mat84.1-0 ; 
       txtx-nose_white-center.1-0.1-0 ; 
       txtx-nose_white-center.1-1.1-0 ; 
       txtx-port_red-left.1-0.1-0 ; 
       txtx-sides_and_bottom.1-0 ; 
       txtx-starbord_green-right.1-0.1-0 ; 
       txtx-top.1-0 ; 
       txtx-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 64     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.2-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig09/PICTURES/fig09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig09-txtx.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 52     
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       txtx-t2d24.1-0 ; 
       txtx-t2d27.1-0 ; 
       txtx-t2d28.1-0 ; 
       txtx-t2d29.1-0 ; 
       txtx-t2d31.1-0 ; 
       txtx-t2d33.1-0 ; 
       txtx-t2d39.1-0 ; 
       txtx-t2d40.1-0 ; 
       txtx-t2d41.1-0 ; 
       txtx-t2d42.1-0 ; 
       txtx-t2d43.1-0 ; 
       txtx-t2d44.1-0 ; 
       txtx-t2d45.1-0 ; 
       txtx-t2d46.1-0 ; 
       txtx-t2d47.1-0 ; 
       txtx-t2d48.1-0 ; 
       txtx-t2d49.1-0 ; 
       txtx-t2d50.1-0 ; 
       txtx-t2d51.1-0 ; 
       txtx-t2d52.1-0 ; 
       txtx-t2d53.1-0 ; 
       txtx-t2d54.1-0 ; 
       txtx-t2d55.1-0 ; 
       txtx-t2d56.1-0 ; 
       txtx-t2d57.1-0 ; 
       txtx-t2d58.1-0 ; 
       txtx-t2d59.1-0 ; 
       txtx-t2d6.1-0 ; 
       txtx-t2d60.1-0 ; 
       txtx-t2d61.1-0 ; 
       txtx-t2d62.1-0 ; 
       txtx-t2d63.1-0 ; 
       txtx-t2d64.1-0 ; 
       txtx-t2d65.1-0 ; 
       txtx-t2d66.1-0 ; 
       txtx-t2d67.1-0 ; 
       txtx-t2d68.1-0 ; 
       txtx-t2d69.1-0 ; 
       txtx-t2d70.1-0 ; 
       txtx-t2d71.1-0 ; 
       txtx-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 41 110 ; 
       5 41 110 ; 
       6 41 110 ; 
       8 7 110 ; 
       9 31 110 ; 
       10 32 110 ; 
       11 9 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 9 110 ; 
       16 10 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 8 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 8 110 ; 
       27 1 110 ; 
       28 2 110 ; 
       29 3 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 9 110 ; 
       34 10 110 ; 
       35 45 110 ; 
       36 45 110 ; 
       37 45 110 ; 
       38 45 110 ; 
       39 45 110 ; 
       40 45 110 ; 
       41 8 110 ; 
       42 6 110 ; 
       43 5 110 ; 
       44 4 110 ; 
       45 8 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 20 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 23 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 8 110 ; 
       55 35 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 8 110 ; 
       62 7 110 ; 
       63 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 52 300 ; 
       2 53 300 ; 
       3 54 300 ; 
       4 55 300 ; 
       5 56 300 ; 
       6 57 300 ; 
       8 22 300 ; 
       8 81 300 ; 
       8 19 300 ; 
       8 25 300 ; 
       8 79 300 ; 
       8 18 300 ; 
       8 82 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 58 300 ; 
       10 59 300 ; 
       10 60 300 ; 
       10 61 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       12 72 300 ; 
       12 73 300 ; 
       12 74 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 68 300 ; 
       14 69 300 ; 
       14 70 300 ; 
       14 71 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       16 62 300 ; 
       16 63 300 ; 
       16 64 300 ; 
       17 17 300 ; 
       18 75 300 ; 
       20 34 300 ; 
       21 35 300 ; 
       22 36 300 ; 
       23 37 300 ; 
       24 38 300 ; 
       25 39 300 ; 
       26 21 300 ; 
       26 24 300 ; 
       26 27 300 ; 
       33 4 300 ; 
       33 5 300 ; 
       33 6 300 ; 
       34 65 300 ; 
       34 66 300 ; 
       34 67 300 ; 
       35 33 300 ; 
       36 32 300 ; 
       37 31 300 ; 
       38 30 300 ; 
       39 28 300 ; 
       40 29 300 ; 
       41 20 300 ; 
       41 23 300 ; 
       41 26 300 ; 
       46 77 300 ; 
       47 78 300 ; 
       48 41 300 ; 
       49 40 300 ; 
       50 42 300 ; 
       51 43 300 ; 
       52 44 300 ; 
       53 45 300 ; 
       54 80 300 ; 
       55 46 300 ; 
       56 47 300 ; 
       57 48 300 ; 
       58 49 300 ; 
       59 50 300 ; 
       60 51 300 ; 
       61 76 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 5 400 ; 
       16 42 400 ; 
       20 13 400 ; 
       21 14 400 ; 
       23 15 400 ; 
       25 16 400 ; 
       35 12 400 ; 
       38 11 400 ; 
       40 51 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 37 401 ; 
       18 24 401 ; 
       22 25 401 ; 
       23 27 401 ; 
       24 29 401 ; 
       25 23 401 ; 
       26 28 401 ; 
       27 30 401 ; 
       28 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       36 20 401 ; 
       38 21 401 ; 
       52 31 401 ; 
       53 32 401 ; 
       54 33 401 ; 
       55 34 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       59 39 401 ; 
       60 40 401 ; 
       61 41 401 ; 
       63 43 401 ; 
       66 44 401 ; 
       67 45 401 ; 
       70 46 401 ; 
       71 47 401 ; 
       73 48 401 ; 
       74 49 401 ; 
       75 50 401 ; 
       79 22 401 ; 
       81 38 401 ; 
       82 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 269.4215 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 185.6715 -6 0 MPRFLG 0 ; 
       2 SCHEM 180.6715 -6 0 MPRFLG 0 ; 
       3 SCHEM 175.6715 -6 0 MPRFLG 0 ; 
       4 SCHEM 65.67149 -6 0 MPRFLG 0 ; 
       5 SCHEM 70.67149 -6 0 MPRFLG 0 ; 
       6 SCHEM 75.67149 -6 0 MPRFLG 0 ; 
       7 SCHEM 143.1715 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 139.4215 -2 0 MPRFLG 0 ; 
       9 SCHEM 39.42149 -6 0 MPRFLG 0 ; 
       10 SCHEM 219.4215 -6 0 MPRFLG 0 ; 
       11 SCHEM 49.42149 -8 0 MPRFLG 0 ; 
       12 SCHEM 229.4215 -8 0 MPRFLG 0 ; 
       13 SCHEM 28.17149 -8 0 MPRFLG 0 ; 
       14 SCHEM 208.1715 -8 0 MPRFLG 0 ; 
       15 SCHEM 40.67149 -8 0 MPRFLG 0 ; 
       16 SCHEM 220.6715 -8 0 MPRFLG 0 ; 
       17 SCHEM 34.42149 -8 0 MPRFLG 0 ; 
       18 SCHEM 214.4215 -8 0 MPRFLG 0 ; 
       19 SCHEM 153.1715 -4 0 MPRFLG 0 ; 
       20 SCHEM 169.4215 -6 0 MPRFLG 0 ; 
       21 SCHEM 136.9215 -6 0 MPRFLG 0 ; 
       22 SCHEM 143.1715 -6 0 MPRFLG 0 ; 
       23 SCHEM 149.4215 -6 0 MPRFLG 0 ; 
       24 SCHEM 155.6715 -6 0 MPRFLG 0 ; 
       25 SCHEM 161.9215 -6 0 MPRFLG 0 ; 
       26 SCHEM 184.4215 -4 0 MPRFLG 0 ; 
       27 SCHEM 184.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 179.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 174.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 244.4215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39.42149 -4 0 MPRFLG 0 ; 
       32 SCHEM 219.4215 -4 0 MPRFLG 0 ; 
       33 SCHEM 19.42149 -8 0 MPRFLG 0 ; 
       34 SCHEM 199.4215 -8 0 MPRFLG 0 ; 
       35 SCHEM 119.4215 -6 0 MPRFLG 0 ; 
       36 SCHEM 88.17149 -6 0 MPRFLG 0 ; 
       37 SCHEM 93.17149 -6 0 MPRFLG 0 ; 
       38 SCHEM 99.42149 -6 0 MPRFLG 0 ; 
       39 SCHEM 105.6715 -6 0 MPRFLG 0 ; 
       40 SCHEM 111.9215 -6 0 MPRFLG 0 ; 
       41 SCHEM 74.42149 -4 0 MPRFLG 0 ; 
       42 SCHEM 74.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 69.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 64.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 104.4215 -4 0 MPRFLG 0 ; 
       46 SCHEM 124.4215 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 126.9215 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 166.9215 -8 0 MPRFLG 0 ; 
       49 SCHEM 134.4215 -8 0 MPRFLG 0 ; 
       50 SCHEM 141.9215 -8 0 MPRFLG 0 ; 
       51 SCHEM 146.9215 -8 0 MPRFLG 0 ; 
       52 SCHEM 154.4215 -8 0 MPRFLG 0 ; 
       53 SCHEM 159.4215 -8 0 MPRFLG 0 ; 
       54 SCHEM 129.4215 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 116.9215 -8 0 MPRFLG 0 ; 
       56 SCHEM 86.92149 -8 0 MPRFLG 0 ; 
       57 SCHEM 91.92149 -8 0 MPRFLG 0 ; 
       58 SCHEM 96.92149 -8 0 MPRFLG 0 ; 
       59 SCHEM 104.4215 -8 0 MPRFLG 0 ; 
       60 SCHEM 109.4215 -8 0 MPRFLG 0 ; 
       61 SCHEM 131.9215 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 264.4215 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 266.9215 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 61.92149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 56.92149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49.42149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -18.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 256.9215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 249.4215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 84.42149 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 194.4215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 261.9215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 79.42149 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 189.4215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 251.9215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 81.92149 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 191.9215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 106.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 114.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 94.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 89.42149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 121.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 171.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 139.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 144.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 151.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 156.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 164.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 134.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 166.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 141.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 146.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 154.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 159.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 116.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 86.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 91.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 96.92149 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 104.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 109.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 186.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 181.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 176.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 66.92149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 71.92149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 76.92149 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 241.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 234.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 236.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 239.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 224.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 216.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 219.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 201.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 196.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 199.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 211.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 204.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 206.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 209.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 231.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 226.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 229.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 182.5 -18.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 131.9215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 124.4215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 126.9215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 254.4215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 129.4215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 246.9215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 259.4215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.92149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29.42149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.92149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19.42149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.92149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.92149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 54.42149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.92149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.42149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.92149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 49.42149 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 99.42149 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 119.4215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 169.4215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 136.9215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 149.4215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 161.9215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 106.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 94.42149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 89.42149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 144.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 156.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 254.4215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 251.9215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 256.9215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 261.9215 -6 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 259.4215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 79.42149 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 81.92149 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 189.4215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 191.9215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 186.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 181.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 176.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 66.92149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 71.92149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 76.92149 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -20.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 246.9215 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 234.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 236.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 239.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 221.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 216.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 196.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 199.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 206.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 209.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 226.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 229.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 182.5 -20.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 111.9215 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 270.9215 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
