SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.2-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat66.1-0 ; 
       ee-base1.1-0 ; 
       ee-base2.1-0 ; 
       ee-caution1.1-0 ; 
       ee-caution2.1-0 ; 
       ee-guns1.1-0 ; 
       ee-guns2.1-0 ; 
       ee-mat51.1-0 ; 
       ee-mat52.1-0 ; 
       ee-mat53.1-0 ; 
       ee-mat54.1-0 ; 
       ee-mat55.1-0 ; 
       ee-mat56.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.1-0 ROOT ; 
       fig08-LL1.2-0 ; 
       fig08-LLa.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-lwepbar.1-0 ROOT ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rwepbar.1-0 ROOT ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig08/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ee-ee.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       ee-t2d49.1-0 ; 
       ee-t2d50.1-0 ; 
       ee-t2d51.1-0 ; 
       ee-t2d52.1-0 ; 
       ee-t2d53.1-0 ; 
       ee-t2d54.1-0 ; 
       ee-t2d55.1-0 ; 
       ee-t2d56.1-0 ; 
       ee-t2d57.1-0 ; 
       ee-t2d58.1-0 ; 
       ee-t2d59.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       3 13 110 ; 
       4 19 110 ; 
       5 19 110 ; 
       6 19 110 ; 
       8 17 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       14 1 110 ; 
       15 2 110 ; 
       16 3 110 ; 
       17 7 110 ; 
       18 8 110 ; 
       20 6 110 ; 
       21 5 110 ; 
       22 4 110 ; 
       23 7 110 ; 
       24 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 24 300 ; 
       2 25 300 ; 
       3 26 300 ; 
       4 27 300 ; 
       5 28 300 ; 
       6 29 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       10 0 300 ; 
       10 1 300 ; 
       10 2 300 ; 
       10 3 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       12 17 300 ; 
       13 19 300 ; 
       13 21 300 ; 
       13 23 300 ; 
       18 4 300 ; 
       18 5 300 ; 
       18 6 300 ; 
       19 18 300 ; 
       19 20 300 ; 
       19 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 5 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 21 401 ; 
       20 11 401 ; 
       21 13 401 ; 
       22 12 401 ; 
       23 14 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 32.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 32.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       20 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -24.4215 -16.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM -24.4215 -18.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 21.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 110 110 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
