SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.3-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       ee-light1.1-0 ROOT ; 
       ee-light2.1-0 ROOT ; 
       ee-light3.1-0 ROOT ; 
       ee-light4.1-0 ROOT ; 
       ee-light5.1-0 ROOT ; 
       ee-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat66.2-0 ; 
       ee-back1.1-0 ; 
       ee-base.1-0 ; 
       ee-base1.2-0 ; 
       ee-base2.2-0 ; 
       ee-bottom.1-0 ; 
       ee-caution1.2-0 ; 
       ee-caution2.2-0 ; 
       ee-front1.1-0 ; 
       ee-guns1.2-0 ; 
       ee-guns2.2-0 ; 
       ee-mat51.2-0 ; 
       ee-mat52.2-0 ; 
       ee-mat53.2-0 ; 
       ee-mat54.2-0 ; 
       ee-mat55.2-0 ; 
       ee-mat56.2-0 ; 
       ee-sides_and_bottom.1-0 ; 
       ee-top.1-0 ; 
       ee-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3_1.1-0 ROOT ; 
       fig08-fuselg.1-0 ROOT ; 
       fig08-LL1.2-0 ; 
       fig08-LLa.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-lwepbar.2-0 ROOT ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rwepbar_1.1-0 ROOT ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ee-ee.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       ee-t2d44.1-0 ; 
       ee-t2d45.1-0 ; 
       ee-t2d46.1-0 ; 
       ee-t2d47.1-0 ; 
       ee-t2d48.1-0 ; 
       ee-t2d49.2-0 ; 
       ee-t2d50.2-0 ; 
       ee-t2d51.2-0 ; 
       ee-t2d52.2-0 ; 
       ee-t2d53.2-0 ; 
       ee-t2d54.2-0 ; 
       ee-t2d55.2-0 ; 
       ee-t2d56.2-0 ; 
       ee-t2d57.2-0 ; 
       ee-t2d58.2-0 ; 
       ee-t2d59.2-0 ; 
       ee-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 20 110 ; 
       5 20 110 ; 
       6 20 110 ; 
       9 18 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       15 1 110 ; 
       16 2 110 ; 
       17 3 110 ; 
       18 7 110 ; 
       19 9 110 ; 
       21 6 110 ; 
       22 5 110 ; 
       23 4 110 ; 
       24 7 110 ; 
       25 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 28 300 ; 
       2 29 300 ; 
       3 30 300 ; 
       4 31 300 ; 
       5 32 300 ; 
       6 33 300 ; 
       8 22 300 ; 
       8 35 300 ; 
       8 19 300 ; 
       8 25 300 ; 
       8 34 300 ; 
       8 18 300 ; 
       8 36 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       12 9 300 ; 
       13 17 300 ; 
       14 21 300 ; 
       14 24 300 ; 
       14 27 300 ; 
       19 4 300 ; 
       19 5 300 ; 
       19 6 300 ; 
       20 20 300 ; 
       20 23 300 ; 
       20 26 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       12 5 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 26 401 ; 
       18 13 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       25 12 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 11 401 ; 
       35 27 401 ; 
       36 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 60 -10 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 36.25 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 38.75 -6 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 10 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 2.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 62.5 -8 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       15 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 DISPLAY 3 2 MPRFLG 0 ; 
       19 SCHEM 0 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 15 -8 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -26.9215 -20.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 64 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM -26.9215 -22.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 66.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 110 110 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
