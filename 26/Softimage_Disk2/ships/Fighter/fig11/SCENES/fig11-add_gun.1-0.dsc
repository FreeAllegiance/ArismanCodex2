SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.16-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.4-0 ROOT ; 
       new-light2.4-0 ROOT ; 
       new-light3.4-0 ROOT ; 
       new-light4.4-0 ROOT ; 
       new-light5.4-0 ROOT ; 
       new-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       add_gun-mat20.1-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat66.3-0 ; 
       medium_fighter_sPATL-mat38.2-0 ; 
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-mat67.1-0 ; 
       new-mat68.1-0 ; 
       new-mat69.1-0 ; 
       new-mat70.1-0 ; 
       new-mat71.1-0 ; 
       new-mat72.1-0 ; 
       new-mat73.1-0 ; 
       new-mat74.1-0 ; 
       new-mat75.1-0 ; 
       new-mat76.1-0 ; 
       new-mat77.1-0 ; 
       new-mat78.1-0 ; 
       new-mat79.1-0 ; 
       new-mat80.1-0 ; 
       new-mat81.1-0 ; 
       new-mat82.1-0 ; 
       new-mat83.1-0 ; 
       new-mat84.1-0 ; 
       new-nose_white-center.1-0.1-0 ; 
       new-nose_white-center.1-1.1-0 ; 
       new-port_red-left.1-0.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-starbord_green-right.1-0.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       fig07-tgun.2-0 ; 
       fig07-twepbas.1-0 ROOT ; 
       fig07-twepemt.1-0 ; 
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.5-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-hatchz.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
       fig08-wepatt.1-0 ; 
       fig08-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-add_gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d19_1.1-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.4-0 ; 
       add_gun-t2d26.4-0 ; 
       add_gun-t2d27.4-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       add_gun-t2d37.1-0 ; 
       medium_fighter_sPATL-t2d33.2-0 ; 
       medium_fighter_sPATL-t2d34.2-0 ; 
       medium_fighter_sPATL-t2d35.2-0 ; 
       medium_fighter_sPATL-t2d36.2-0 ; 
       new-t2d24.1-0 ; 
       new-t2d27.1-0 ; 
       new-t2d28.1-0 ; 
       new-t2d29.1-0 ; 
       new-t2d31.1-0 ; 
       new-t2d33.1-0 ; 
       new-t2d39.1-0 ; 
       new-t2d40.1-0 ; 
       new-t2d41.1-0 ; 
       new-t2d42.1-0 ; 
       new-t2d43.1-0 ; 
       new-t2d44.1-0 ; 
       new-t2d45.1-0 ; 
       new-t2d46.1-0 ; 
       new-t2d47.1-0 ; 
       new-t2d48.1-0 ; 
       new-t2d49.1-0 ; 
       new-t2d50.1-0 ; 
       new-t2d51.1-0 ; 
       new-t2d52.1-0 ; 
       new-t2d53.1-0 ; 
       new-t2d54.1-0 ; 
       new-t2d55.1-0 ; 
       new-t2d56.1-0 ; 
       new-t2d57.1-0 ; 
       new-t2d58.1-0 ; 
       new-t2d59.2-0 ; 
       new-t2d6.1-0 ; 
       new-t2d60.1-0 ; 
       new-t2d61.1-0 ; 
       new-t2d62.1-0 ; 
       new-t2d63.1-0 ; 
       new-t2d64.1-0 ; 
       new-t2d65.1-0 ; 
       new-t2d66.1-0 ; 
       new-t2d67.1-0 ; 
       new-t2d68.1-0 ; 
       new-t2d69.1-0 ; 
       new-t2d70.1-0 ; 
       new-t2d71.2-0 ; 
       new-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 10 110 ; 
       4 30 110 ; 
       5 30 110 ; 
       6 30 110 ; 
       7 45 110 ; 
       8 45 110 ; 
       9 45 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 35 110 ; 
       14 36 110 ; 
       15 13 110 ; 
       16 14 110 ; 
       17 13 110 ; 
       18 14 110 ; 
       19 13 110 ; 
       20 14 110 ; 
       21 13 110 ; 
       22 14 110 ; 
       23 11 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 23 110 ; 
       30 11 110 ; 
       31 4 110 ; 
       32 5 110 ; 
       33 6 110 ; 
       34 11 110 ; 
       35 11 110 ; 
       36 11 110 ; 
       37 13 110 ; 
       38 14 110 ; 
       39 49 110 ; 
       40 49 110 ; 
       41 49 110 ; 
       42 49 110 ; 
       43 49 110 ; 
       44 49 110 ; 
       45 11 110 ; 
       46 9 110 ; 
       47 8 110 ; 
       48 7 110 ; 
       49 11 110 ; 
       50 11 110 ; 
       51 11 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 26 110 ; 
       55 27 110 ; 
       56 28 110 ; 
       57 29 110 ; 
       58 11 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 41 110 ; 
       62 42 110 ; 
       63 43 110 ; 
       64 44 110 ; 
       65 11 110 ; 
       66 10 110 ; 
       67 10 110 ; 
       0 1 110 ; 
       2 0 110 ; 
       68 69 110 ; 
       69 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 58 300 ; 
       5 59 300 ; 
       6 60 300 ; 
       7 61 300 ; 
       8 62 300 ; 
       9 63 300 ; 
       11 28 300 ; 
       11 87 300 ; 
       11 25 300 ; 
       11 31 300 ; 
       11 85 300 ; 
       11 24 300 ; 
       11 88 300 ; 
       12 20 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       13 14 300 ; 
       14 64 300 ; 
       14 65 300 ; 
       14 66 300 ; 
       14 67 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       16 78 300 ; 
       16 79 300 ; 
       16 80 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       18 74 300 ; 
       18 75 300 ; 
       18 76 300 ; 
       18 77 300 ; 
       19 8 300 ; 
       19 9 300 ; 
       19 10 300 ; 
       20 68 300 ; 
       20 69 300 ; 
       20 70 300 ; 
       21 19 300 ; 
       22 81 300 ; 
       24 40 300 ; 
       25 41 300 ; 
       26 42 300 ; 
       27 43 300 ; 
       28 44 300 ; 
       29 45 300 ; 
       30 27 300 ; 
       30 30 300 ; 
       30 33 300 ; 
       37 5 300 ; 
       37 6 300 ; 
       37 7 300 ; 
       38 71 300 ; 
       38 72 300 ; 
       38 73 300 ; 
       39 39 300 ; 
       40 38 300 ; 
       41 37 300 ; 
       42 36 300 ; 
       43 34 300 ; 
       44 35 300 ; 
       45 26 300 ; 
       45 29 300 ; 
       45 32 300 ; 
       50 83 300 ; 
       51 84 300 ; 
       52 47 300 ; 
       53 46 300 ; 
       54 48 300 ; 
       55 49 300 ; 
       56 50 300 ; 
       57 51 300 ; 
       58 86 300 ; 
       59 52 300 ; 
       60 53 300 ; 
       61 54 300 ; 
       62 55 300 ; 
       63 56 300 ; 
       64 57 300 ; 
       65 82 300 ; 
       0 15 300 ; 
       1 0 300 ; 
       69 21 300 ; 
       69 22 300 ; 
       69 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       12 13 400 ; 
       19 6 400 ; 
       20 48 400 ; 
       24 19 400 ; 
       25 20 400 ; 
       27 21 400 ; 
       29 22 400 ; 
       39 18 400 ; 
       42 17 400 ; 
       44 57 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
       4 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       9 5 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       0 1 401 ; 
       19 43 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 30 401 ; 
       28 31 401 ; 
       29 33 401 ; 
       30 35 401 ; 
       15 12 401 ; 
       31 29 401 ; 
       32 34 401 ; 
       33 36 401 ; 
       34 23 401 ; 
       37 24 401 ; 
       38 25 401 ; 
       42 26 401 ; 
       44 27 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 39 401 ; 
       61 40 401 ; 
       62 41 401 ; 
       63 42 401 ; 
       65 45 401 ; 
       66 46 401 ; 
       67 47 401 ; 
       69 49 401 ; 
       72 50 401 ; 
       73 51 401 ; 
       76 52 401 ; 
       77 53 401 ; 
       79 54 401 ; 
       80 55 401 ; 
       81 56 401 ; 
       85 28 401 ; 
       87 44 401 ; 
       88 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 102 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 104.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 107 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 109.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 112 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 114.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 99.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77 -6 0 MPRFLG 0 ; 
       5 SCHEM 74.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 72 -6 0 MPRFLG 0 ; 
       7 SCHEM 24.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 27 -6 0 MPRFLG 0 ; 
       9 SCHEM 29.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 53.25 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       11 SCHEM 49.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 9.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 17 -6 0 MPRFLG 0 ; 
       14 SCHEM 84.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 22 -8 0 MPRFLG 0 ; 
       16 SCHEM 89.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 14.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 82 -8 0 MPRFLG 0 ; 
       19 SCHEM 19.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 87 -8 0 MPRFLG 0 ; 
       21 SCHEM 17 -8 0 MPRFLG 0 ; 
       22 SCHEM 84.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 63.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 69.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 57 -6 0 MPRFLG 0 ; 
       26 SCHEM 59.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 62 -6 0 MPRFLG 0 ; 
       28 SCHEM 64.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 67 -6 0 MPRFLG 0 ; 
       30 SCHEM 74.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 77 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 74.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 72 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 92 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 17 -4 0 MPRFLG 0 ; 
       36 SCHEM 84.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 12 -8 0 MPRFLG 0 ; 
       38 SCHEM 79.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 44.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 32 -6 0 MPRFLG 0 ; 
       41 SCHEM 34.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 37 -6 0 MPRFLG 0 ; 
       43 SCHEM 39.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 42 -6 0 MPRFLG 0 ; 
       45 SCHEM 27 -4 0 MPRFLG 0 ; 
       46 SCHEM 29.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 27 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 38.25 -4 0 MPRFLG 0 ; 
       50 SCHEM 47 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 49.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 69.5 -8 0 MPRFLG 0 ; 
       53 SCHEM 57 -8 0 MPRFLG 0 ; 
       54 SCHEM 59.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 62 -8 0 MPRFLG 0 ; 
       56 SCHEM 64.5 -8 0 MPRFLG 0 ; 
       57 SCHEM 67 -8 0 MPRFLG 0 ; 
       58 SCHEM 52 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 44.5 -8 0 MPRFLG 0 ; 
       60 SCHEM 32 -8 0 MPRFLG 0 ; 
       61 SCHEM 34.5 -8 0 MPRFLG 0 ; 
       62 SCHEM 37 -8 0 MPRFLG 0 ; 
       63 SCHEM 39.5 -8 0 MPRFLG 0 ; 
       64 SCHEM 42 -8 0 MPRFLG 0 ; 
       65 SCHEM 54.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 94.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 97 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.30262 -17.75366 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 23.55262 -15.75366 0 USR SRT 1 1 1 0.005999994 0 0 0 0.00881687 -1.469542 MPRFLG 0 ; 
       2 SCHEM 21.05262 -19.75366 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 7 -6 0 MPRFLG 0 ; 
       69 SCHEM 7 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 13.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 18.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 18.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 18.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 23.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 23.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 23.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 23.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 26.05262 -17.75366 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 8.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 8.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 8.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 8.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 78.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 78.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 23.55262 -19.75366 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 78.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 43.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 38.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 33.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 71 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 58.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 61 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 63.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 66 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 68.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 56 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 68.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 58.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 63.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 66 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 43.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 33.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 38.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 41 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 78.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 76 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 73.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 26 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 28.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 31 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 91 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 86 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 86 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 86 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 78.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 78.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 78.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 81 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 81 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 81 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 81 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 88.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 88.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 88.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 83.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 53.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 48.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 51 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 93.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 18.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 18.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 23.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 23.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 23.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.05262 -19.75366 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 8.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 8.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 8.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 8.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 38.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 71 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 58.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 23.55262 -21.75366 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 63.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 68.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 33.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 61 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 66 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 78.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 78.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 78.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 76 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 73.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 28.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 31 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 93.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 91 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 91 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 91 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 86 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 86 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 78.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 78.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 81 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 81 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 88.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 88.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 83.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 43.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 101 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
