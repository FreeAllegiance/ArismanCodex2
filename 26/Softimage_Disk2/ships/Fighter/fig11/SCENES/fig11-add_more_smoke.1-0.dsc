SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       remove_anim_edit_nulls-fig08_3.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.46-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       medium_fighter_sPATL-mat39.3-0 ; 
       medium_fighter_sPATL-mat40.3-0 ; 
       medium_fighter_sPATL-mat41.3-0 ; 
       new-back1.2-0 ; 
       new-base.2-0 ; 
       new-base1.2-0 ; 
       new-base2.2-0 ; 
       new-bottom.2-0 ; 
       new-caution1.2-0 ; 
       new-caution2.2-0 ; 
       new-front1.2-0 ; 
       new-guns1.2-0 ; 
       new-guns2.2-0 ; 
       new-mat23.2-0 ; 
       new-mat24.2-0 ; 
       new-mat25.2-0 ; 
       new-mat26.2-0 ; 
       new-mat27.2-0 ; 
       new-mat28.2-0 ; 
       new-mat29.2-0 ; 
       new-mat30.2-0 ; 
       new-mat31.2-0 ; 
       new-mat32.2-0 ; 
       new-mat33.2-0 ; 
       new-mat34.2-0 ; 
       new-mat39.2-0 ; 
       new-mat40.2-0 ; 
       new-mat41.2-0 ; 
       new-mat42.2-0 ; 
       new-mat43.2-0 ; 
       new-mat44.2-0 ; 
       new-mat45.2-0 ; 
       new-mat46.2-0 ; 
       new-mat47.2-0 ; 
       new-mat48.2-0 ; 
       new-mat49.2-0 ; 
       new-mat50.2-0 ; 
       new-mat51.2-0 ; 
       new-mat52.2-0 ; 
       new-mat53.2-0 ; 
       new-mat54.2-0 ; 
       new-mat55.2-0 ; 
       new-mat56.2-0 ; 
       new-nose_white-center.1-0.2-0 ; 
       new-nose_white-center.1-1.2-0 ; 
       new-port_red-left.1-0.2-0 ; 
       new-sides_and_bottom.2-0 ; 
       new-starbord_green-right.1-0.2-0 ; 
       new-top.2-0 ; 
       new-vents1.2-0 ; 
       remove_anim_edit_nulls-mat20.2-0 ; 
       remove_anim_edit_nulls-mat41.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       remove_anim_edit_nulls-cockpt.1-0 ; 
       remove_anim_edit_nulls-cube1.1-0 ; 
       remove_anim_edit_nulls-cube2.2-0 ; 
       remove_anim_edit_nulls-cube3.2-0 ; 
       remove_anim_edit_nulls-cube4.1-0 ; 
       remove_anim_edit_nulls-cube5.1-0 ; 
       remove_anim_edit_nulls-cube6.1-0 ; 
       remove_anim_edit_nulls-fig08_3.7-0 ROOT ; 
       remove_anim_edit_nulls-fuselg.1-0 ; 
       remove_anim_edit_nulls-lslrsal0.1-0 ; 
       remove_anim_edit_nulls-lslrsal1.1-0 ; 
       remove_anim_edit_nulls-lslrsal2.1-0 ; 
       remove_anim_edit_nulls-lslrsal3.1-0 ; 
       remove_anim_edit_nulls-lslrsal4.1-0 ; 
       remove_anim_edit_nulls-lslrsal5.1-0 ; 
       remove_anim_edit_nulls-lslrsal6.2-0 ; 
       remove_anim_edit_nulls-lwepbar.1-0 ; 
       remove_anim_edit_nulls-lwepemt1.1-0 ; 
       remove_anim_edit_nulls-lwepemt2.1-0 ; 
       remove_anim_edit_nulls-lwepemt3.1-0 ; 
       remove_anim_edit_nulls-missemt.1-0 ; 
       remove_anim_edit_nulls-rslrsal1.1-0 ; 
       remove_anim_edit_nulls-rslrsal2.1-0 ; 
       remove_anim_edit_nulls-rslrsal3.1-0 ; 
       remove_anim_edit_nulls-rslrsal4.1-0 ; 
       remove_anim_edit_nulls-rslrsal5.1-0 ; 
       remove_anim_edit_nulls-rslrsal6.1-0 ; 
       remove_anim_edit_nulls-rwepbar.2-0 ; 
       remove_anim_edit_nulls-rwepemt1.1-0 ; 
       remove_anim_edit_nulls-rwepemt2.1-0 ; 
       remove_anim_edit_nulls-rwepemt3.1-0 ; 
       remove_anim_edit_nulls-slrsal0.1-0 ; 
       remove_anim_edit_nulls-smoke.1-0 ; 
       remove_anim_edit_nulls-SSa.1-0 ; 
       remove_anim_edit_nulls-SSal.1-0 ; 
       remove_anim_edit_nulls-SSal1.1-0 ; 
       remove_anim_edit_nulls-SSal2.1-0 ; 
       remove_anim_edit_nulls-SSal3.1-0 ; 
       remove_anim_edit_nulls-SSal4.1-0 ; 
       remove_anim_edit_nulls-SSal5.1-0 ; 
       remove_anim_edit_nulls-SSal6.1-0 ; 
       remove_anim_edit_nulls-SSar.1-0 ; 
       remove_anim_edit_nulls-SSar1.1-0 ; 
       remove_anim_edit_nulls-SSar2.1-0 ; 
       remove_anim_edit_nulls-SSar3.1-0 ; 
       remove_anim_edit_nulls-SSar4.1-0 ; 
       remove_anim_edit_nulls-SSar5.1-0 ; 
       remove_anim_edit_nulls-SSar6.1-0 ; 
       remove_anim_edit_nulls-SSf.1-0 ; 
       remove_anim_edit_nulls-tgun.2-0 ; 
       remove_anim_edit_nulls-thrust.1-0 ; 
       remove_anim_edit_nulls-thrust1.1-0 ; 
       remove_anim_edit_nulls-thrust2.1-0 ; 
       remove_anim_edit_nulls-trail.1-0 ; 
       remove_anim_edit_nulls-twepbas.1-0 ; 
       remove_anim_edit_nulls-twepemt.1-0 ; 
       remove_anim_edit_nulls-wepatt.1-0 ; 
       remove_anim_edit_nulls-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-add_more_smoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       medium_fighter_sPATL-t2d34.5-0 ; 
       medium_fighter_sPATL-t2d35.5-0 ; 
       medium_fighter_sPATL-t2d36.5-0 ; 
       new-t2d24.4-0 ; 
       new-t2d27.4-0 ; 
       new-t2d28.4-0 ; 
       new-t2d29.4-0 ; 
       new-t2d31.4-0 ; 
       new-t2d33.4-0 ; 
       new-t2d39.4-0 ; 
       new-t2d40.4-0 ; 
       new-t2d41.4-0 ; 
       new-t2d42.4-0 ; 
       new-t2d43.4-0 ; 
       new-t2d44.4-0 ; 
       new-t2d45.4-0 ; 
       new-t2d46.4-0 ; 
       new-t2d47.4-0 ; 
       new-t2d48.4-0 ; 
       new-t2d49.4-0 ; 
       new-t2d50.4-0 ; 
       new-t2d51.4-0 ; 
       new-t2d52.4-0 ; 
       new-t2d53.4-0 ; 
       new-t2d54.4-0 ; 
       new-t2d55.4-0 ; 
       new-t2d56.4-0 ; 
       new-t2d57.4-0 ; 
       new-t2d58.4-0 ; 
       new-t2d6.4-0 ; 
       new-z.4-0 ; 
       remove_anim_edit_nulls-t2d19.2-0 ; 
       remove_anim_edit_nulls-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 27 110 ; 
       5 27 110 ; 
       6 27 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 8 110 ; 
       17 1 110 ; 
       18 2 110 ; 
       19 3 110 ; 
       20 7 110 ; 
       21 31 110 ; 
       22 31 110 ; 
       23 31 110 ; 
       24 31 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 8 110 ; 
       28 6 110 ; 
       29 5 110 ; 
       30 4 110 ; 
       31 8 110 ; 
       32 7 110 ; 
       33 8 110 ; 
       34 8 110 ; 
       35 10 110 ; 
       36 11 110 ; 
       37 12 110 ; 
       38 13 110 ; 
       39 14 110 ; 
       40 15 110 ; 
       41 8 110 ; 
       42 21 110 ; 
       43 22 110 ; 
       44 23 110 ; 
       45 24 110 ; 
       46 25 110 ; 
       47 26 110 ; 
       48 8 110 ; 
       49 54 110 ; 
       50 7 110 ; 
       53 7 110 ; 
       54 57 110 ; 
       55 49 110 ; 
       56 8 110 ; 
       57 56 110 ; 
       51 7 110 ; 
       52 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 37 300 ; 
       2 38 300 ; 
       3 39 300 ; 
       4 40 300 ; 
       5 41 300 ; 
       6 42 300 ; 
       8 7 300 ; 
       8 48 300 ; 
       8 4 300 ; 
       8 10 300 ; 
       8 46 300 ; 
       8 3 300 ; 
       8 49 300 ; 
       10 19 300 ; 
       11 20 300 ; 
       12 21 300 ; 
       13 22 300 ; 
       14 23 300 ; 
       15 24 300 ; 
       16 6 300 ; 
       16 9 300 ; 
       16 12 300 ; 
       21 18 300 ; 
       22 17 300 ; 
       23 16 300 ; 
       24 15 300 ; 
       25 13 300 ; 
       26 14 300 ; 
       27 5 300 ; 
       27 8 300 ; 
       27 11 300 ; 
       33 44 300 ; 
       34 45 300 ; 
       35 26 300 ; 
       36 25 300 ; 
       37 27 300 ; 
       38 28 300 ; 
       39 29 300 ; 
       40 30 300 ; 
       41 47 300 ; 
       42 31 300 ; 
       43 32 300 ; 
       44 33 300 ; 
       45 34 300 ; 
       46 35 300 ; 
       47 36 300 ; 
       48 43 300 ; 
       49 51 300 ; 
       54 50 300 ; 
       57 0 300 ; 
       57 1 300 ; 
       57 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 5 400 ; 
       11 6 400 ; 
       13 7 400 ; 
       15 8 400 ; 
       21 4 400 ; 
       24 3 400 ; 
       26 30 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 16 401 ; 
       7 17 401 ; 
       8 19 401 ; 
       9 21 401 ; 
       10 15 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       21 12 401 ; 
       23 13 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 14 401 ; 
       48 29 401 ; 
       49 18 401 ; 
       50 31 401 ; 
       51 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 MPRFLG 0 ; 
       9 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 50 -6 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 45 -6 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 55 -4 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25 -6 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 15 -6 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 20 -6 0 MPRFLG 0 ; 
       26 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       32 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 27.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 30 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 50 -8 0 MPRFLG 0 ; 
       36 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 40 -8 0 MPRFLG 0 ; 
       38 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 45 -8 0 MPRFLG 0 ; 
       40 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 32.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 25 -8 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       44 SCHEM 15 -8 0 MPRFLG 0 ; 
       45 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       46 SCHEM 20 -8 0 MPRFLG 0 ; 
       47 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       48 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       50 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       57 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 52.64839 -6.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 52.64839 -8.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 52.64839 -8.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 52.64839 -10.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 76.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
