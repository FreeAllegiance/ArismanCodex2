SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.17-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.5-0 ROOT ; 
       new-light2.5-0 ROOT ; 
       new-light3.5-0 ROOT ; 
       new-light4.5-0 ROOT ; 
       new-light5.5-0 ROOT ; 
       new-light6.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 89     
       animate_gun-mat20.1-0 ; 
       animate_gun-mat22.1-0 ; 
       animate_gun-mat23.1-0 ; 
       animate_gun-mat24.1-0 ; 
       animate_gun-mat25.1-0 ; 
       animate_gun-mat26.1-0 ; 
       animate_gun-mat27.1-0 ; 
       animate_gun-mat28.1-0 ; 
       animate_gun-mat29.1-0 ; 
       animate_gun-mat30.1-0 ; 
       animate_gun-mat31.1-0 ; 
       animate_gun-mat34.1-0 ; 
       animate_gun-mat35.1-0 ; 
       animate_gun-mat36.1-0 ; 
       animate_gun-mat37.1-0 ; 
       animate_gun-mat41.1-0 ; 
       animate_gun-mat42.1-0 ; 
       animate_gun-mat43.1-0 ; 
       animate_gun-mat44.1-0 ; 
       animate_gun-mat66.1-0 ; 
       medium_fighter_sPATL-mat38.2-0 ; 
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-mat67.1-0 ; 
       new-mat68.1-0 ; 
       new-mat69.1-0 ; 
       new-mat70.1-0 ; 
       new-mat71.1-0 ; 
       new-mat72.1-0 ; 
       new-mat73.1-0 ; 
       new-mat74.1-0 ; 
       new-mat75.1-0 ; 
       new-mat76.1-0 ; 
       new-mat77.1-0 ; 
       new-mat78.1-0 ; 
       new-mat79.1-0 ; 
       new-mat80.1-0 ; 
       new-mat81.1-0 ; 
       new-mat82.1-0 ; 
       new-mat83.1-0 ; 
       new-mat84.1-0 ; 
       new-nose_white-center.1-0.1-0 ; 
       new-nose_white-center.1-1.1-0 ; 
       new-port_red-left.1-0.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-starbord_green-right.1-0.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.6-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-hatchz.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-tgun.2-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
       fig08-twepbas.1-0 ; 
       fig08-twepemt.1-0 ; 
       fig08-wepatt.1-0 ; 
       fig08-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-animate_gun.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       animate_gun-t2d18.1-0 ; 
       animate_gun-t2d19.1-0 ; 
       animate_gun-t2d19_1.1-0 ; 
       animate_gun-t2d20.1-0 ; 
       animate_gun-t2d21.1-0 ; 
       animate_gun-t2d22.1-0 ; 
       animate_gun-t2d23.1-0 ; 
       animate_gun-t2d25.1-0 ; 
       animate_gun-t2d26.1-0 ; 
       animate_gun-t2d27.1-0 ; 
       animate_gun-t2d31.1-0 ; 
       animate_gun-t2d32.1-0 ; 
       animate_gun-t2d37.1-0 ; 
       medium_fighter_sPATL-t2d33.2-0 ; 
       medium_fighter_sPATL-t2d34.2-0 ; 
       medium_fighter_sPATL-t2d35.2-0 ; 
       medium_fighter_sPATL-t2d36.2-0 ; 
       new-t2d24.1-0 ; 
       new-t2d27.1-0 ; 
       new-t2d28.1-0 ; 
       new-t2d29.1-0 ; 
       new-t2d31.1-0 ; 
       new-t2d33.1-0 ; 
       new-t2d39.1-0 ; 
       new-t2d40.1-0 ; 
       new-t2d41.1-0 ; 
       new-t2d42.1-0 ; 
       new-t2d43.1-0 ; 
       new-t2d44.1-0 ; 
       new-t2d45.1-0 ; 
       new-t2d46.1-0 ; 
       new-t2d47.1-0 ; 
       new-t2d48.1-0 ; 
       new-t2d49.1-0 ; 
       new-t2d50.1-0 ; 
       new-t2d51.1-0 ; 
       new-t2d52.1-0 ; 
       new-t2d53.1-0 ; 
       new-t2d54.1-0 ; 
       new-t2d55.1-0 ; 
       new-t2d56.1-0 ; 
       new-t2d57.1-0 ; 
       new-t2d58.1-0 ; 
       new-t2d59.2-0 ; 
       new-t2d6.1-0 ; 
       new-t2d60.1-0 ; 
       new-t2d61.1-0 ; 
       new-t2d62.1-0 ; 
       new-t2d63.1-0 ; 
       new-t2d64.1-0 ; 
       new-t2d65.1-0 ; 
       new-t2d66.1-0 ; 
       new-t2d67.1-0 ; 
       new-t2d68.1-0 ; 
       new-t2d69.1-0 ; 
       new-t2d70.1-0 ; 
       new-t2d71.2-0 ; 
       new-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 27 110 ; 
       4 42 110 ; 
       5 42 110 ; 
       6 42 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 32 110 ; 
       11 33 110 ; 
       12 10 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 11 110 ; 
       16 10 110 ; 
       17 11 110 ; 
       18 10 110 ; 
       19 11 110 ; 
       20 8 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 8 110 ; 
       28 1 110 ; 
       29 2 110 ; 
       30 3 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 10 110 ; 
       35 11 110 ; 
       36 46 110 ; 
       37 46 110 ; 
       38 46 110 ; 
       39 46 110 ; 
       40 46 110 ; 
       41 46 110 ; 
       42 8 110 ; 
       43 6 110 ; 
       44 5 110 ; 
       45 4 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 8 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 23 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 26 110 ; 
       55 8 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 41 110 ; 
       62 8 110 ; 
       64 7 110 ; 
       65 7 110 ; 
       63 66 110 ; 
       66 69 110 ; 
       67 63 110 ; 
       68 69 110 ; 
       69 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 58 300 ; 
       2 59 300 ; 
       3 60 300 ; 
       4 61 300 ; 
       5 62 300 ; 
       6 63 300 ; 
       8 28 300 ; 
       8 87 300 ; 
       8 25 300 ; 
       8 31 300 ; 
       8 85 300 ; 
       8 24 300 ; 
       8 88 300 ; 
       9 20 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 64 300 ; 
       11 65 300 ; 
       11 66 300 ; 
       11 67 300 ; 
       12 16 300 ; 
       12 17 300 ; 
       12 18 300 ; 
       13 78 300 ; 
       13 79 300 ; 
       13 80 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       15 74 300 ; 
       15 75 300 ; 
       15 76 300 ; 
       15 77 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       17 68 300 ; 
       17 69 300 ; 
       17 70 300 ; 
       18 19 300 ; 
       19 81 300 ; 
       21 40 300 ; 
       22 41 300 ; 
       23 42 300 ; 
       24 43 300 ; 
       25 44 300 ; 
       26 45 300 ; 
       27 27 300 ; 
       27 30 300 ; 
       27 33 300 ; 
       34 5 300 ; 
       34 6 300 ; 
       34 7 300 ; 
       35 71 300 ; 
       35 72 300 ; 
       35 73 300 ; 
       36 39 300 ; 
       37 38 300 ; 
       38 37 300 ; 
       39 36 300 ; 
       40 34 300 ; 
       41 35 300 ; 
       42 26 300 ; 
       42 29 300 ; 
       42 32 300 ; 
       47 83 300 ; 
       48 84 300 ; 
       49 47 300 ; 
       50 46 300 ; 
       51 48 300 ; 
       52 49 300 ; 
       53 50 300 ; 
       54 51 300 ; 
       55 86 300 ; 
       56 52 300 ; 
       57 53 300 ; 
       58 54 300 ; 
       59 55 300 ; 
       60 56 300 ; 
       61 57 300 ; 
       62 82 300 ; 
       63 15 300 ; 
       66 0 300 ; 
       69 21 300 ; 
       69 22 300 ; 
       69 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 13 400 ; 
       16 6 400 ; 
       17 48 400 ; 
       21 19 400 ; 
       22 20 400 ; 
       24 21 400 ; 
       26 22 400 ; 
       36 18 400 ; 
       39 17 400 ; 
       41 57 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
       4 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       9 5 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       0 1 401 ; 
       19 43 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 30 401 ; 
       28 31 401 ; 
       29 33 401 ; 
       30 35 401 ; 
       15 12 401 ; 
       31 29 401 ; 
       32 34 401 ; 
       33 36 401 ; 
       34 23 401 ; 
       37 24 401 ; 
       38 25 401 ; 
       42 26 401 ; 
       44 27 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 39 401 ; 
       61 40 401 ; 
       62 41 401 ; 
       63 42 401 ; 
       65 45 401 ; 
       66 46 401 ; 
       67 47 401 ; 
       69 49 401 ; 
       72 50 401 ; 
       73 51 401 ; 
       76 52 401 ; 
       77 53 401 ; 
       79 54 401 ; 
       80 55 401 ; 
       81 56 401 ; 
       85 28 401 ; 
       87 44 401 ; 
       88 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 112.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -6 0 MPRFLG 0 ; 
       2 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 70 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 50 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 MPRFLG 0 ; 
       13 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 80 -8 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 85 -8 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 MPRFLG 0 ; 
       19 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 61.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 60 -6 0 MPRFLG 0 ; 
       25 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 65 -6 0 MPRFLG 0 ; 
       27 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 15 -4 0 MPRFLG 0 ; 
       33 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 10 -8 0 MPRFLG 0 ; 
       35 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       37 SCHEM 30 -6 0 MPRFLG 0 ; 
       38 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 35 -6 0 MPRFLG 0 ; 
       40 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 40 -6 0 MPRFLG 0 ; 
       42 SCHEM 25 -4 0 MPRFLG 0 ; 
       43 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       47 SCHEM 45 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       50 SCHEM 55 -8 0 MPRFLG 0 ; 
       51 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       52 SCHEM 60 -8 0 MPRFLG 0 ; 
       53 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       54 SCHEM 65 -8 0 MPRFLG 0 ; 
       55 SCHEM 50 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       57 SCHEM 30 -8 0 MPRFLG 0 ; 
       58 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       59 SCHEM 35 -8 0 MPRFLG 0 ; 
       60 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       61 SCHEM 40 -8 0 MPRFLG 0 ; 
       62 SCHEM 52.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 5 -8 0 MPRFLG 0 ; 
       66 SCHEM 5 -6 0 MPRFLG 0 ; 
       67 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       69 SCHEM 3.75 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 66.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 89 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 79 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 86.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 81.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 64 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 76.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 89 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 89 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 89 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 84 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 86.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 81.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 99 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
