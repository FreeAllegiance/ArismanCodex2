SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       remove_anim_edit_nulls-fig08_3_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.49-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       fix_stuff-light1.1-0 ROOT ; 
       fix_stuff-light2.1-0 ROOT ; 
       fix_stuff-light3.1-0 ROOT ; 
       fix_stuff-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       medium_fighter_sPATL-mat39.3-0 ; 
       medium_fighter_sPATL-mat40.3-0 ; 
       medium_fighter_sPATL-mat41.3-0 ; 
       new-back1.2-0 ; 
       new-base.2-0 ; 
       new-base1.2-0 ; 
       new-base2.2-0 ; 
       new-bottom.2-0 ; 
       new-caution1.2-0 ; 
       new-caution2.2-0 ; 
       new-front1.2-0 ; 
       new-guns1.2-0 ; 
       new-guns2.2-0 ; 
       new-mat23.2-0 ; 
       new-mat24.2-0 ; 
       new-mat25.2-0 ; 
       new-mat26.2-0 ; 
       new-mat27.2-0 ; 
       new-mat28.2-0 ; 
       new-mat29.2-0 ; 
       new-mat30.2-0 ; 
       new-mat32.2-0 ; 
       new-mat33.2-0 ; 
       new-mat34.2-0 ; 
       new-mat39.2-0 ; 
       new-mat40.2-0 ; 
       new-mat41.2-0 ; 
       new-mat42.2-0 ; 
       new-mat43.2-0 ; 
       new-mat44.2-0 ; 
       new-mat45.2-0 ; 
       new-mat46.2-0 ; 
       new-mat47.2-0 ; 
       new-mat48.2-0 ; 
       new-mat49.2-0 ; 
       new-mat50.2-0 ; 
       new-mat51.2-0 ; 
       new-mat52.2-0 ; 
       new-mat53.2-0 ; 
       new-mat54.2-0 ; 
       new-mat55.2-0 ; 
       new-mat56.2-0 ; 
       new-mat57.1-0 ; 
       new-nose_white-center.1-0.2-0 ; 
       new-nose_white-center.1-1.2-0 ; 
       new-port_red-left.1-0.2-0 ; 
       new-sides_and_bottom.2-0 ; 
       new-starbord_green-right.1-0.2-0 ; 
       new-top.2-0 ; 
       new-vents1.2-0 ; 
       remove_anim_edit_nulls-mat20.2-0 ; 
       remove_anim_edit_nulls-mat41.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       remove_anim_edit_nulls-cockpt.1-0 ; 
       remove_anim_edit_nulls-cube1.1-0 ; 
       remove_anim_edit_nulls-cube2.2-0 ; 
       remove_anim_edit_nulls-cube3.2-0 ; 
       remove_anim_edit_nulls-cube4.1-0 ; 
       remove_anim_edit_nulls-cube5.1-0 ; 
       remove_anim_edit_nulls-cube6.1-0 ; 
       remove_anim_edit_nulls-fig08_3_1.2-0 ROOT ; 
       remove_anim_edit_nulls-fuselg.1-0 ; 
       remove_anim_edit_nulls-lslrsal0.1-0 ; 
       remove_anim_edit_nulls-lslrsal1.1-0 ; 
       remove_anim_edit_nulls-lslrsal2.1-0 ; 
       remove_anim_edit_nulls-lslrsal3.1-0 ; 
       remove_anim_edit_nulls-lslrsal4.1-0 ; 
       remove_anim_edit_nulls-lslrsal5.1-0 ; 
       remove_anim_edit_nulls-lslrsal6.2-0 ; 
       remove_anim_edit_nulls-lwepbar.1-0 ; 
       remove_anim_edit_nulls-lwepemt1.1-0 ; 
       remove_anim_edit_nulls-lwepemt2.1-0 ; 
       remove_anim_edit_nulls-lwepemt3.1-0 ; 
       remove_anim_edit_nulls-missemt.1-0 ; 
       remove_anim_edit_nulls-rslrsal1.1-0 ; 
       remove_anim_edit_nulls-rslrsal2.1-0 ; 
       remove_anim_edit_nulls-rslrsal3.1-0 ; 
       remove_anim_edit_nulls-rslrsal4.1-0 ; 
       remove_anim_edit_nulls-rslrsal5.1-0 ; 
       remove_anim_edit_nulls-rslrsal6.1-0 ; 
       remove_anim_edit_nulls-rwepbar.2-0 ; 
       remove_anim_edit_nulls-rwepemt1.1-0 ; 
       remove_anim_edit_nulls-rwepemt2.1-0 ; 
       remove_anim_edit_nulls-rwepemt3.1-0 ; 
       remove_anim_edit_nulls-slrsal0.1-0 ; 
       remove_anim_edit_nulls-smoke.1-0 ; 
       remove_anim_edit_nulls-SSa.1-0 ; 
       remove_anim_edit_nulls-SSal.1-0 ; 
       remove_anim_edit_nulls-SSal1.1-0 ; 
       remove_anim_edit_nulls-SSal2.1-0 ; 
       remove_anim_edit_nulls-SSal3.1-0 ; 
       remove_anim_edit_nulls-SSal4.1-0 ; 
       remove_anim_edit_nulls-SSal5.1-0 ; 
       remove_anim_edit_nulls-SSal6.1-0 ; 
       remove_anim_edit_nulls-SSar.1-0 ; 
       remove_anim_edit_nulls-SSar1.1-0 ; 
       remove_anim_edit_nulls-SSar2.1-0 ; 
       remove_anim_edit_nulls-SSar3.1-0 ; 
       remove_anim_edit_nulls-SSar4.1-0 ; 
       remove_anim_edit_nulls-SSar5.1-0 ; 
       remove_anim_edit_nulls-SSar6.1-0 ; 
       remove_anim_edit_nulls-SSf.1-0 ; 
       remove_anim_edit_nulls-tgun.2-0 ; 
       remove_anim_edit_nulls-thrust.1-0 ; 
       remove_anim_edit_nulls-thrust1.1-0 ; 
       remove_anim_edit_nulls-thrust2.1-0 ; 
       remove_anim_edit_nulls-trail.1-0 ; 
       remove_anim_edit_nulls-twepbas.1-0 ; 
       remove_anim_edit_nulls-twepemt.1-0 ; 
       remove_anim_edit_nulls-wepatt.1-0 ; 
       remove_anim_edit_nulls-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-fix_stuff.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       fix_stuff-t2d59.1-0 ; 
       medium_fighter_sPATL-t2d34.5-0 ; 
       medium_fighter_sPATL-t2d35.5-0 ; 
       medium_fighter_sPATL-t2d36.5-0 ; 
       new-t2d24.4-0 ; 
       new-t2d27.4-0 ; 
       new-t2d28.4-0 ; 
       new-t2d29.5-0 ; 
       new-t2d31.4-0 ; 
       new-t2d33.4-0 ; 
       new-t2d39.4-0 ; 
       new-t2d40.4-0 ; 
       new-t2d41.4-0 ; 
       new-t2d43.4-0 ; 
       new-t2d44.4-0 ; 
       new-t2d45.4-0 ; 
       new-t2d46.4-0 ; 
       new-t2d47.4-0 ; 
       new-t2d48.4-0 ; 
       new-t2d49.4-0 ; 
       new-t2d50.4-0 ; 
       new-t2d51.4-0 ; 
       new-t2d52.4-0 ; 
       new-t2d53.4-0 ; 
       new-t2d54.4-0 ; 
       new-t2d55.4-0 ; 
       new-t2d56.4-0 ; 
       new-t2d57.4-0 ; 
       new-t2d58.4-0 ; 
       new-t2d6.4-0 ; 
       new-z.4-0 ; 
       remove_anim_edit_nulls-t2d19.2-0 ; 
       remove_anim_edit_nulls-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 27 110 ; 
       5 27 110 ; 
       6 27 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 9 110 ; 
       15 9 110 ; 
       16 8 110 ; 
       17 1 110 ; 
       18 2 110 ; 
       19 3 110 ; 
       20 7 110 ; 
       21 31 110 ; 
       22 31 110 ; 
       23 31 110 ; 
       24 31 110 ; 
       25 31 110 ; 
       26 31 110 ; 
       27 8 110 ; 
       28 6 110 ; 
       29 5 110 ; 
       30 4 110 ; 
       31 8 110 ; 
       32 7 110 ; 
       33 8 110 ; 
       34 8 110 ; 
       35 10 110 ; 
       36 11 110 ; 
       37 12 110 ; 
       38 13 110 ; 
       39 14 110 ; 
       40 15 110 ; 
       41 8 110 ; 
       42 21 110 ; 
       43 22 110 ; 
       44 23 110 ; 
       45 24 110 ; 
       46 25 110 ; 
       47 26 110 ; 
       48 8 110 ; 
       49 54 110 ; 
       50 7 110 ; 
       51 7 110 ; 
       52 7 110 ; 
       53 7 110 ; 
       54 57 110 ; 
       55 49 110 ; 
       56 8 110 ; 
       57 56 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 36 300 ; 
       2 37 300 ; 
       3 38 300 ; 
       4 39 300 ; 
       5 40 300 ; 
       6 41 300 ; 
       8 7 300 ; 
       8 48 300 ; 
       8 4 300 ; 
       8 10 300 ; 
       8 46 300 ; 
       8 3 300 ; 
       8 49 300 ; 
       10 19 300 ; 
       11 20 300 ; 
       12 42 300 ; 
       13 21 300 ; 
       14 22 300 ; 
       15 23 300 ; 
       16 6 300 ; 
       16 9 300 ; 
       16 12 300 ; 
       21 18 300 ; 
       22 17 300 ; 
       23 16 300 ; 
       24 15 300 ; 
       25 13 300 ; 
       26 14 300 ; 
       27 5 300 ; 
       27 8 300 ; 
       27 11 300 ; 
       33 44 300 ; 
       34 45 300 ; 
       35 25 300 ; 
       36 24 300 ; 
       37 26 300 ; 
       38 27 300 ; 
       39 28 300 ; 
       40 29 300 ; 
       41 47 300 ; 
       42 30 300 ; 
       43 31 300 ; 
       44 32 300 ; 
       45 33 300 ; 
       46 34 300 ; 
       47 35 300 ; 
       48 43 300 ; 
       49 51 300 ; 
       54 50 300 ; 
       57 0 300 ; 
       57 1 300 ; 
       57 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 6 400 ; 
       11 7 400 ; 
       12 0 400 ; 
       13 8 400 ; 
       15 9 400 ; 
       21 5 400 ; 
       24 4 400 ; 
       26 30 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 16 401 ; 
       7 17 401 ; 
       8 19 401 ; 
       9 21 401 ; 
       10 15 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       22 13 401 ; 
       36 23 401 ; 
       37 24 401 ; 
       38 25 401 ; 
       39 26 401 ; 
       40 27 401 ; 
       41 28 401 ; 
       46 14 401 ; 
       48 29 401 ; 
       49 18 401 ; 
       50 31 401 ; 
       51 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 190 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 192.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 195 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 177.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 141.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 136.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 131.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 93.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 85 -2 0 MPRFLG 0 ; 
       9 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 125 -6 0 MPRFLG 0 ; 
       11 SCHEM 90 -6 0 MPRFLG 0 ; 
       12 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 MPRFLG 0 ; 
       14 SCHEM 111.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 140 -4 0 MPRFLG 0 ; 
       17 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 170 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 65 -6 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 180 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 77.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 80 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 122.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 95 -8 0 MPRFLG 0 ; 
       38 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 110 -8 0 MPRFLG 0 ; 
       40 SCHEM 115 -8 0 MPRFLG 0 ; 
       41 SCHEM 82.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       42 SCHEM 70 -8 0 MPRFLG 0 ; 
       43 SCHEM 40 -8 0 MPRFLG 0 ; 
       44 SCHEM 45 -8 0 MPRFLG 0 ; 
       45 SCHEM 50 -8 0 MPRFLG 0 ; 
       46 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       48 SCHEM 85 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       50 SCHEM 172.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 182.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 185 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 175 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 5 -8 0 MPRFLG 0 ; 
       55 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       57 SCHEM 8.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 157.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 160 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 55.14839 -6.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 53.89839 -8.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 160 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 157.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 152.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 55.14839 -8.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 53.89839 -10.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 186.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
