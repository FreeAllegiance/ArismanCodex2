SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig11-fig11.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.1-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       medium_fighter_sPATL-inf_light1_2_2_2_2_2.1-0 ROOT ; 
       medium_fighter_sPATL-inf_light2_2_2_2_2_2.1-0 ROOT ; 
       medium_fighter_sPATL-inf_light3_2_2_2_2_2.1-0 ROOT ; 
       medium_fighter_sPATL-light5_1_2_4_1_2_2_2_2_2.1-0 ROOT ; 
       medium_fighter_sPATL-light6_3_2_4_1_2_2_2_2_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       medium_fighter_sPATL-mat10.1-0 ; 
       medium_fighter_sPATL-mat11.1-0 ; 
       medium_fighter_sPATL-mat14.1-0 ; 
       medium_fighter_sPATL-mat16.1-0 ; 
       medium_fighter_sPATL-mat18.1-0 ; 
       medium_fighter_sPATL-mat19.1-0 ; 
       medium_fighter_sPATL-mat20.1-0 ; 
       medium_fighter_sPATL-mat21.1-0 ; 
       medium_fighter_sPATL-mat22.1-0 ; 
       medium_fighter_sPATL-mat23.1-0 ; 
       medium_fighter_sPATL-mat24.1-0 ; 
       medium_fighter_sPATL-mat25.1-0 ; 
       medium_fighter_sPATL-mat26.1-0 ; 
       medium_fighter_sPATL-mat27.1-0 ; 
       medium_fighter_sPATL-mat28.1-0 ; 
       medium_fighter_sPATL-mat29.1-0 ; 
       medium_fighter_sPATL-mat30.1-0 ; 
       medium_fighter_sPATL-mat31.1-0 ; 
       medium_fighter_sPATL-mat32.1-0 ; 
       medium_fighter_sPATL-mat34.1-0 ; 
       medium_fighter_sPATL-mat35.1-0 ; 
       medium_fighter_sPATL-mat36.1-0 ; 
       medium_fighter_sPATL-mat37.1-0 ; 
       medium_fighter_sPATL-mat38.1-0 ; 
       medium_fighter_sPATL-mat39.1-0 ; 
       medium_fighter_sPATL-mat4_3.1-0 ; 
       medium_fighter_sPATL-mat4_4.1-0 ; 
       medium_fighter_sPATL-mat40.1-0 ; 
       medium_fighter_sPATL-mat41.1-0 ; 
       medium_fighter_sPATL-mat42.1-0 ; 
       medium_fighter_sPATL-mat43.1-0 ; 
       medium_fighter_sPATL-mat44.1-0 ; 
       medium_fighter_sPATL-mat45.1-0 ; 
       medium_fighter_sPATL-mat46.1-0 ; 
       medium_fighter_sPATL-mat47.1-0 ; 
       medium_fighter_sPATL-mat49.1-0 ; 
       medium_fighter_sPATL-mat5.1-0 ; 
       medium_fighter_sPATL-mat50.1-0 ; 
       medium_fighter_sPATL-mat51.1-0 ; 
       medium_fighter_sPATL-mat52.1-0 ; 
       medium_fighter_sPATL-mat53.1-0 ; 
       medium_fighter_sPATL-mat54.1-0 ; 
       medium_fighter_sPATL-mat8.1-0 ; 
       medium_fighter_sPATL-port_red-left.1-0.1-0 ; 
       medium_fighter_sPATL-starbord_green-right.1-0.1-0 ; 
       medium_fighter_sPATL-white-nose_white-center.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       fig11-fig11.1-0 ROOT ; 
       fig11-fuselg.1-0 ; 
       fig11-hatchz.2-0 ; 
       fig11-lwepbas.1-0 ; 
       fig11-lwepemt1.1-0 ; 
       fig11-lwepemt2.1-0 ; 
       fig11-lwepemt3.1-0 ; 
       fig11-lwingzz0.1-0 ; 
       fig11-lwingzz1.1-0 ; 
       fig11-lwingzz2.1-0 ; 
       fig11-lwingzz3.1-0 ; 
       fig11-lwingzz4.1-0 ; 
       fig11-lwingzz5.1-0 ; 
       fig11-lwingzz6.1-0 ; 
       fig11-rwepbas.1-0 ; 
       fig11-rwepemt1.1-0 ; 
       fig11-rwepemt2.1-0 ; 
       fig11-rwepemt3.1-0 ; 
       fig11-rwingzz0.1-0 ; 
       fig11-rwingzz1.1-0 ; 
       fig11-rwingzz2.1-0 ; 
       fig11-rwingzz3.1-0 ; 
       fig11-rwingzz4.1-0 ; 
       fig11-rwingzz5.1-0 ; 
       fig11-rwingzz6.1-0 ; 
       fig11-SSf.1-0 ; 
       fig11-SSl.1-0 ; 
       fig11-SSl1.1-0 ; 
       fig11-SSl2.1-0 ; 
       fig11-SSl3.1-0 ; 
       fig11-SSl4.1-0 ; 
       fig11-SSl5.1-0 ; 
       fig11-SSl6.1-0 ; 
       fig11-SSr.1-0 ; 
       fig11-SSr1.1-0 ; 
       fig11-SSr2.1-0 ; 
       fig11-SSr3.1-0 ; 
       fig11-SSr4.1-0 ; 
       fig11-SSr5.1-0 ; 
       fig11-SSr6.1-0 ; 
       fig11-wepatt.1-0 ; 
       fig11-wepmnt.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-med_fig_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       medium_fighter_sPATL-t2d11.1-0 ; 
       medium_fighter_sPATL-t2d13.1-0 ; 
       medium_fighter_sPATL-t2d14.1-0 ; 
       medium_fighter_sPATL-t2d15.1-0 ; 
       medium_fighter_sPATL-t2d16.1-0 ; 
       medium_fighter_sPATL-t2d19.1-0 ; 
       medium_fighter_sPATL-t2d2.1-0 ; 
       medium_fighter_sPATL-t2d21.1-0 ; 
       medium_fighter_sPATL-t2d23.1-0 ; 
       medium_fighter_sPATL-t2d27.1-0 ; 
       medium_fighter_sPATL-t2d29.1-0 ; 
       medium_fighter_sPATL-t2d30.1-0 ; 
       medium_fighter_sPATL-t2d31.1-0 ; 
       medium_fighter_sPATL-t2d32.1-0 ; 
       medium_fighter_sPATL-t2d33.1-0 ; 
       medium_fighter_sPATL-t2d34.1-0 ; 
       medium_fighter_sPATL-t2d35.1-0 ; 
       medium_fighter_sPATL-t2d36.1-0 ; 
       medium_fighter_sPATL-t2d4.1-0 ; 
       medium_fighter_sPATL-t2d5.1-0 ; 
       medium_fighter_sPATL-t2d9.1-0 ; 
       med_fig_F-t2d43.1-0 ; 
       med_fig_F-t2d44.1-0 ; 
       med_fig_F-t2d45.1-0 ; 
       med_fig_F-t2d46.1-0 ; 
       med_fig_F-t2d47.1-0 ; 
       med_fig_F-t2d48.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       25 1 110 ; 
       26 1 110 ; 
       27 8 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 13 110 ; 
       33 1 110 ; 
       34 19 110 ; 
       35 20 110 ; 
       36 21 110 ; 
       37 22 110 ; 
       38 23 110 ; 
       39 24 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 1 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 1 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 1 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       40 41 110 ; 
       41 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       25 45 300 ; 
       26 43 300 ; 
       27 41 300 ; 
       28 35 300 ; 
       29 37 300 ; 
       30 38 300 ; 
       31 39 300 ; 
       32 40 300 ; 
       33 44 300 ; 
       34 30 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 34 300 ; 
       39 29 300 ; 
       1 36 300 ; 
       1 42 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 18 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       2 23 300 ; 
       3 25 300 ; 
       3 2 300 ; 
       3 5 300 ; 
       8 12 300 ; 
       9 13 300 ; 
       10 14 300 ; 
       11 15 300 ; 
       12 16 300 ; 
       12 19 300 ; 
       13 17 300 ; 
       14 26 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       19 6 300 ; 
       20 7 300 ; 
       21 8 300 ; 
       22 9 300 ; 
       23 10 300 ; 
       24 11 300 ; 
       41 24 300 ; 
       41 27 300 ; 
       41 28 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 14 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       1 19 401 ; 
       2 20 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 23 401 ; 
       9 22 401 ; 
       10 5 401 ; 
       11 21 401 ; 
       12 7 401 ; 
       13 24 401 ; 
       14 8 401 ; 
       15 25 401 ; 
       17 26 401 ; 
       18 9 401 ; 
       19 10 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 15 401 ; 
       27 16 401 ; 
       28 17 401 ; 
       42 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 26 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 26 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 26 -4.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 26 -6.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 26 -8.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       25 SCHEM 13 -18.5 0 MPRFLG 0 ; 
       26 SCHEM 13 -16.5 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 20 -46.5 0 MPRFLG 0 ; 
       28 SCHEM 20 -50.5 0 MPRFLG 0 ; 
       29 SCHEM 20 -54.5 0 MPRFLG 0 ; 
       30 SCHEM 20 -58.5 0 MPRFLG 0 ; 
       31 SCHEM 20 -64.5 0 MPRFLG 0 ; 
       32 SCHEM 20 -68.5 0 MPRFLG 0 ; 
       33 SCHEM 13 -70.5 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 20 -22.5 0 MPRFLG 0 ; 
       35 SCHEM 20 -26.5 0 MPRFLG 0 ; 
       36 SCHEM 20 -30.5 0 MPRFLG 0 ; 
       37 SCHEM 20 -34.5 0 MPRFLG 0 ; 
       38 SCHEM 20 -38.5 0 MPRFLG 0 ; 
       39 SCHEM 20 -42.5 0 MPRFLG 0 ; 
       0 SCHEM 6 -53.5 0 SRT 1 1 1 -2.842171e-014 0 5.684342e-014 0 0.01075098 0.001670957 MPRFLG 0 ; 
       1 SCHEM 9.5 -53.5 0 MPRFLG 0 ; 
       2 SCHEM 13 -73.5 0 MPRFLG 0 ; 
       3 SCHEM 13 -101.5 0 MPRFLG 0 ; 
       4 SCHEM 16.5 -102.5 0 MPRFLG 0 ; 
       5 SCHEM 16.5 -104.5 0 MPRFLG 0 ; 
       6 SCHEM 16.5 -106.5 0 MPRFLG 0 ; 
       7 SCHEM 13 -56.5 0 MPRFLG 0 ; 
       8 SCHEM 16.5 -45.5 0 MPRFLG 0 ; 
       9 SCHEM 16.5 -49.5 0 MPRFLG 0 ; 
       10 SCHEM 16.5 -53.5 0 MPRFLG 0 ; 
       11 SCHEM 16.5 -57.5 0 MPRFLG 0 ; 
       12 SCHEM 16.5 -62.5 0 MPRFLG 0 ; 
       13 SCHEM 16.5 -67.5 0 MPRFLG 0 ; 
       14 SCHEM 13 -89.5 0 MPRFLG 0 ; 
       15 SCHEM 16.5 -90.5 0 MPRFLG 0 ; 
       16 SCHEM 16.5 -92.5 0 MPRFLG 0 ; 
       17 SCHEM 16.5 -94.5 0 MPRFLG 0 ; 
       18 SCHEM 13 -31.5 0 MPRFLG 0 ; 
       19 SCHEM 16.5 -21.5 0 MPRFLG 0 ; 
       20 SCHEM 16.5 -25.5 0 MPRFLG 0 ; 
       21 SCHEM 16.5 -29.5 0 MPRFLG 0 ; 
       22 SCHEM 16.5 -33.5 0 MPRFLG 0 ; 
       23 SCHEM 16.5 -37.5 0 MPRFLG 0 ; 
       24 SCHEM 16.5 -41.5 0 MPRFLG 0 ; 
       40 SCHEM 16.5 -82.5 0 MPRFLG 0 ; 
       41 SCHEM 13 -79.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 13 -2.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -98.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -86.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -84.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -96.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -20.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -24.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -28.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -32.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -36.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -40.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -44.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -48.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -52.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -56.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -62.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -66.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -60.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 13 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -74.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -80.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -100.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -88.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -78.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -76.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 23.5 -42.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 23.5 -22.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 23.5 -26.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 23.5 -30.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 23.5 -34.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 23.5 -38.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 23.5 -50.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 13 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 23.5 -54.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 23.5 -58.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 23.5 -64.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 23.5 -68.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 23.5 -46.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 13 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -70.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -18.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       21 SCHEM 23.5 -40.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 23.5 -32.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 23.5 -28.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 23.5 -48.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 23.5 -56.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 23.5 -66.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 20 -86.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -84.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -96.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 23.5 -20.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 23.5 -24.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 23.5 -36.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 23.5 -44.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 23.5 -52.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 23.5 -60.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -72.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -80.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -78.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20 -76.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -0.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 20 -98.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9.5 1.25 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 133 133 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
