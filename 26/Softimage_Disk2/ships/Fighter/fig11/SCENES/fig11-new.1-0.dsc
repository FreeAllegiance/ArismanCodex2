SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       fig08-fig08_3.2-0 ; 
       new-hatchz.1-0 ; 
       new-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.13-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.1-0 ROOT ; 
       new-light2.1-0 ROOT ; 
       new-light3.1-0 ROOT ; 
       new-light4.1-0 ROOT ; 
       new-light5.1-0 ROOT ; 
       new-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 87     
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat66.3-0 ; 
       medium_fighter_sPATL-mat38.2-0 ; 
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-mat67.1-0 ; 
       new-mat68.1-0 ; 
       new-mat69.1-0 ; 
       new-mat70.1-0 ; 
       new-mat71.1-0 ; 
       new-mat72.1-0 ; 
       new-mat73.1-0 ; 
       new-mat74.1-0 ; 
       new-mat75.1-0 ; 
       new-mat76.1-0 ; 
       new-mat77.1-0 ; 
       new-mat78.1-0 ; 
       new-mat79.1-0 ; 
       new-mat80.1-0 ; 
       new-mat81.1-0 ; 
       new-mat82.1-0 ; 
       new-mat83.1-0 ; 
       new-mat84.1-0 ; 
       new-nose_white-center.1-0.1-0 ; 
       new-nose_white-center.1-1.1-0 ; 
       new-port_red-left.1-0.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-starbord_green-right.1-0.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 67     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.2-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
       new-hatchz.1-0 ROOT ; 
       new-wepatt.1-0 ; 
       new-wepmnt.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig08 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-new.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.4-0 ; 
       add_gun-t2d26.4-0 ; 
       add_gun-t2d27.4-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       medium_fighter_sPATL-t2d33.2-0 ; 
       medium_fighter_sPATL-t2d34.2-0 ; 
       medium_fighter_sPATL-t2d35.2-0 ; 
       medium_fighter_sPATL-t2d36.2-0 ; 
       new-t2d24.1-0 ; 
       new-t2d27.1-0 ; 
       new-t2d28.1-0 ; 
       new-t2d29.1-0 ; 
       new-t2d31.1-0 ; 
       new-t2d33.1-0 ; 
       new-t2d39.1-0 ; 
       new-t2d40.1-0 ; 
       new-t2d41.1-0 ; 
       new-t2d42.1-0 ; 
       new-t2d43.1-0 ; 
       new-t2d44.1-0 ; 
       new-t2d45.1-0 ; 
       new-t2d46.1-0 ; 
       new-t2d47.1-0 ; 
       new-t2d48.1-0 ; 
       new-t2d49.1-0 ; 
       new-t2d50.1-0 ; 
       new-t2d51.1-0 ; 
       new-t2d52.1-0 ; 
       new-t2d53.1-0 ; 
       new-t2d54.1-0 ; 
       new-t2d55.1-0 ; 
       new-t2d56.1-0 ; 
       new-t2d57.1-0 ; 
       new-t2d58.1-0 ; 
       new-t2d59.1-0 ; 
       new-t2d6.1-0 ; 
       new-t2d60.1-0 ; 
       new-t2d61.1-0 ; 
       new-t2d62.1-0 ; 
       new-t2d63.1-0 ; 
       new-t2d64.1-0 ; 
       new-t2d65.1-0 ; 
       new-t2d66.1-0 ; 
       new-t2d67.1-0 ; 
       new-t2d68.1-0 ; 
       new-t2d69.1-0 ; 
       new-t2d70.1-0 ; 
       new-t2d71.1-0 ; 
       new-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 41 110 ; 
       5 41 110 ; 
       6 41 110 ; 
       8 7 110 ; 
       9 31 110 ; 
       10 32 110 ; 
       11 9 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 9 110 ; 
       16 10 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 8 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 19 110 ; 
       25 19 110 ; 
       26 8 110 ; 
       27 1 110 ; 
       28 2 110 ; 
       29 3 110 ; 
       30 8 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 9 110 ; 
       34 10 110 ; 
       35 45 110 ; 
       36 45 110 ; 
       37 45 110 ; 
       38 45 110 ; 
       39 45 110 ; 
       40 45 110 ; 
       41 8 110 ; 
       42 6 110 ; 
       43 5 110 ; 
       44 4 110 ; 
       45 8 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 20 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 23 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 8 110 ; 
       55 35 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 8 110 ; 
       62 7 110 ; 
       63 7 110 ; 
       65 66 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 56 300 ; 
       2 57 300 ; 
       3 58 300 ; 
       4 59 300 ; 
       5 60 300 ; 
       6 61 300 ; 
       8 26 300 ; 
       8 85 300 ; 
       8 23 300 ; 
       8 29 300 ; 
       8 83 300 ; 
       8 22 300 ; 
       8 86 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       10 62 300 ; 
       10 63 300 ; 
       10 64 300 ; 
       10 65 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       12 76 300 ; 
       12 77 300 ; 
       12 78 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 72 300 ; 
       14 73 300 ; 
       14 74 300 ; 
       14 75 300 ; 
       15 7 300 ; 
       15 8 300 ; 
       15 9 300 ; 
       16 66 300 ; 
       16 67 300 ; 
       16 68 300 ; 
       17 17 300 ; 
       18 79 300 ; 
       20 38 300 ; 
       21 39 300 ; 
       64 18 300 ; 
       22 40 300 ; 
       23 41 300 ; 
       24 42 300 ; 
       25 43 300 ; 
       26 25 300 ; 
       26 28 300 ; 
       26 31 300 ; 
       33 4 300 ; 
       33 5 300 ; 
       33 6 300 ; 
       34 69 300 ; 
       34 70 300 ; 
       34 71 300 ; 
       35 37 300 ; 
       36 36 300 ; 
       37 35 300 ; 
       38 34 300 ; 
       39 32 300 ; 
       40 33 300 ; 
       41 24 300 ; 
       41 27 300 ; 
       41 30 300 ; 
       46 81 300 ; 
       47 82 300 ; 
       48 45 300 ; 
       49 44 300 ; 
       50 46 300 ; 
       51 47 300 ; 
       52 48 300 ; 
       53 49 300 ; 
       54 84 300 ; 
       55 50 300 ; 
       56 51 300 ; 
       57 52 300 ; 
       58 53 300 ; 
       59 54 300 ; 
       60 55 300 ; 
       61 80 300 ; 
       66 19 300 ; 
       66 20 300 ; 
       66 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 5 400 ; 
       16 46 400 ; 
       20 17 400 ; 
       21 18 400 ; 
       64 11 400 ; 
       23 19 400 ; 
       25 20 400 ; 
       35 16 400 ; 
       38 15 400 ; 
       40 55 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
       64 1 15000 ; 
       66 2 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 41 401 ; 
       22 28 401 ; 
       26 29 401 ; 
       27 31 401 ; 
       28 33 401 ; 
       29 27 401 ; 
       30 32 401 ; 
       31 34 401 ; 
       32 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       40 24 401 ; 
       42 25 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 39 401 ; 
       61 40 401 ; 
       63 43 401 ; 
       64 44 401 ; 
       65 45 401 ; 
       67 47 401 ; 
       70 48 401 ; 
       71 49 401 ; 
       74 50 401 ; 
       75 51 401 ; 
       77 52 401 ; 
       78 53 401 ; 
       79 54 401 ; 
       83 26 401 ; 
       85 42 401 ; 
       86 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 25 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -26.98965 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 266.9215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 183.1715 -10 0 MPRFLG 0 ; 
       2 SCHEM 178.1715 -10 0 MPRFLG 0 ; 
       3 SCHEM 173.1715 -10 0 MPRFLG 0 ; 
       4 SCHEM 63.17151 -10 0 MPRFLG 0 ; 
       5 SCHEM 68.17151 -10 0 MPRFLG 0 ; 
       6 SCHEM 73.17151 -10 0 MPRFLG 0 ; 
       7 SCHEM 140.6715 -4 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 136.9215 -6 0 MPRFLG 0 ; 
       9 SCHEM 36.92151 -10 0 MPRFLG 0 ; 
       10 SCHEM 216.9215 -10 0 MPRFLG 0 ; 
       11 SCHEM 46.92151 -12 0 MPRFLG 0 ; 
       12 SCHEM 226.9215 -12 0 MPRFLG 0 ; 
       13 SCHEM 25.67151 -12 0 MPRFLG 0 ; 
       14 SCHEM 205.6715 -12 0 MPRFLG 0 ; 
       15 SCHEM 38.17151 -12 0 MPRFLG 0 ; 
       16 SCHEM 218.1715 -12 0 MPRFLG 0 ; 
       17 SCHEM 31.92151 -12 0 MPRFLG 0 ; 
       18 SCHEM 211.9215 -12 0 MPRFLG 0 ; 
       19 SCHEM 150.6715 -8 0 MPRFLG 0 ; 
       20 SCHEM 166.9215 -10 0 MPRFLG 0 ; 
       21 SCHEM 134.4215 -10 0 MPRFLG 0 ; 
       64 SCHEM 11.25 -26.98965 0 SRT 1 1 1 0.2913085 0 -4.591775e-041 -0.001175673 0.6634291 -0.9082873 MPRFLG 0 ; 
       22 SCHEM 140.6715 -10 0 MPRFLG 0 ; 
       23 SCHEM 146.9215 -10 0 MPRFLG 0 ; 
       24 SCHEM 153.1715 -10 0 MPRFLG 0 ; 
       25 SCHEM 159.4215 -10 0 MPRFLG 0 ; 
       26 SCHEM 181.9215 -8 0 MPRFLG 0 ; 
       27 SCHEM 181.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 176.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 171.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 241.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 36.92151 -8 0 MPRFLG 0 ; 
       32 SCHEM 216.9215 -8 0 MPRFLG 0 ; 
       33 SCHEM 16.92151 -12 0 MPRFLG 0 ; 
       34 SCHEM 196.9215 -12 0 MPRFLG 0 ; 
       35 SCHEM 116.9215 -10 0 MPRFLG 0 ; 
       36 SCHEM 85.67151 -10 0 MPRFLG 0 ; 
       37 SCHEM 90.67151 -10 0 MPRFLG 0 ; 
       38 SCHEM 96.92151 -10 0 MPRFLG 0 ; 
       39 SCHEM 103.1715 -10 0 MPRFLG 0 ; 
       40 SCHEM 109.4215 -10 0 MPRFLG 0 ; 
       41 SCHEM 71.92151 -8 0 MPRFLG 0 ; 
       42 SCHEM 71.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 66.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 61.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 101.9215 -8 0 MPRFLG 0 ; 
       46 SCHEM 121.9215 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 124.4215 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 164.4215 -12 0 MPRFLG 0 ; 
       49 SCHEM 131.9215 -12 0 MPRFLG 0 ; 
       50 SCHEM 139.4215 -12 0 MPRFLG 0 ; 
       51 SCHEM 144.4215 -12 0 MPRFLG 0 ; 
       52 SCHEM 151.9215 -12 0 MPRFLG 0 ; 
       53 SCHEM 156.9215 -12 0 MPRFLG 0 ; 
       54 SCHEM 126.9215 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       55 SCHEM 114.4215 -12 0 MPRFLG 0 ; 
       56 SCHEM 84.42151 -12 0 MPRFLG 0 ; 
       57 SCHEM 89.42151 -12 0 MPRFLG 0 ; 
       58 SCHEM 94.42151 -12 0 MPRFLG 0 ; 
       59 SCHEM 101.9215 -12 0 MPRFLG 0 ; 
       60 SCHEM 106.9215 -12 0 MPRFLG 0 ; 
       61 SCHEM 129.4215 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 261.9215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 264.4215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 0 -28.98965 0 MPRFLG 0 ; 
       66 SCHEM 3.75 -26.98965 0 SRT 1 1 1 0.3490658 0 0 0.0003913101 0.2757397 -1.58 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 46.92151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 0 -22.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 254.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 246.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 81.92151 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 191.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 259.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.92151 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 186.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 249.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 79.42151 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 189.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 104.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 111.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 99.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 91.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 86.92151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 119.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 169.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 136.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 141.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 149.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 154.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 161.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 131.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 164.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 139.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 144.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 151.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 156.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 114.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 84.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 89.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 94.42151 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -28.98965 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -28.98965 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -28.98965 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -28.98965 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 101.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 106.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 184.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 179.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 174.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 64.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 69.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 74.42151 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 239.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 231.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 234.4215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 236.9215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 221.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 214.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 216.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 199.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 194.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 196.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 209.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 201.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 204.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 206.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 229.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 224.4215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 226.9215 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 180 -22.98965 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 129.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 121.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 124.4215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 251.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 126.9215 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 244.4215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 256.9215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24.42151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.92151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14.42151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.92151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34.42151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39.42151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 51.92151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 54.42151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.92151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44.42151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 46.92151 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 96.92151 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 116.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 166.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 134.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 146.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 159.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 104.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 91.92151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 86.92151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 141.9215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 154.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 251.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 249.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 254.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 259.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 256.9215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 76.92151 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 79.42151 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 186.9215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 189.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 184.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 179.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 174.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10 -28.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -30.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -30.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 5 -30.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 64.42151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 69.42151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 74.42151 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 0 -24.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 244.4215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 231.9215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 234.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 236.9215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 219.4215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 214.4215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 194.4215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 196.9215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 204.4215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 206.9215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 224.4215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 226.9215 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 180 -24.98965 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 109.4215 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 268.4215 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 9 -28.98965 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -28.98965 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 62 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
