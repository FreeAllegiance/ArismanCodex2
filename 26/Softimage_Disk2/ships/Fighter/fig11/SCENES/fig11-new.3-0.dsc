SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig08-fig08_3.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.15-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.3-0 ROOT ; 
       new-light2.3-0 ROOT ; 
       new-light3.3-0 ROOT ; 
       new-light4.3-0 ROOT ; 
       new-light5.3-0 ROOT ; 
       new-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 87     
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat66.3-0 ; 
       medium_fighter_sPATL-mat38.2-0 ; 
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-mat67.1-0 ; 
       new-mat68.1-0 ; 
       new-mat69.1-0 ; 
       new-mat70.1-0 ; 
       new-mat71.1-0 ; 
       new-mat72.1-0 ; 
       new-mat73.1-0 ; 
       new-mat74.1-0 ; 
       new-mat75.1-0 ; 
       new-mat76.1-0 ; 
       new-mat77.1-0 ; 
       new-mat78.1-0 ; 
       new-mat79.1-0 ; 
       new-mat80.1-0 ; 
       new-mat81.1-0 ; 
       new-mat82.1-0 ; 
       new-mat83.1-0 ; 
       new-mat84.1-0 ; 
       new-nose_white-center.1-0.1-0 ; 
       new-nose_white-center.1-1.1-0 ; 
       new-port_red-left.1-0.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-starbord_green-right.1-0.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 67     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-cube2.2-0 ; 
       fig08-cube3.2-0 ; 
       fig08-cube4.1-0 ; 
       fig08-cube5.1-0 ; 
       fig08-cube6.1-0 ; 
       fig08-fig08_3.4-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-hatchz.1-0 ; 
       fig08-LL1.2-0 ; 
       fig08-LL3.1-0 ; 
       fig08-LLa.1-0 ; 
       fig08-LLa2.1-0 ; 
       fig08-llandgr.1-0 ; 
       fig08-llandgr2.1-0 ; 
       fig08-LLl.1-0 ; 
       fig08-LLl2.1-0 ; 
       fig08-LLr.1-0 ; 
       fig08-LLr2.1-0 ; 
       fig08-lslrsal0.1-0 ; 
       fig08-lslrsal1.1-0 ; 
       fig08-lslrsal2.1-0 ; 
       fig08-lslrsal3.1-0 ; 
       fig08-lslrsal4.1-0 ; 
       fig08-lslrsal5.1-0 ; 
       fig08-lslrsal6.2-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-lwepemt1.1-0 ; 
       fig08-lwepemt2.1-0 ; 
       fig08-lwepemt3.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-null1.1-0 ; 
       fig08-null3.1-0 ; 
       fig08-rlandgr.1-0 ; 
       fig08-rlandgr2.1-0 ; 
       fig08-rslrsal1.1-0 ; 
       fig08-rslrsal2.1-0 ; 
       fig08-rslrsal3.1-0 ; 
       fig08-rslrsal4.1-0 ; 
       fig08-rslrsal5.1-0 ; 
       fig08-rslrsal6.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-rwepemt1.1-0 ; 
       fig08-rwepemt2.1-0 ; 
       fig08-rwepemt3.1-0 ; 
       fig08-slrsal0.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSal1.1-0 ; 
       fig08-SSal2.1-0 ; 
       fig08-SSal3.1-0 ; 
       fig08-SSal4.1-0 ; 
       fig08-SSal5.1-0 ; 
       fig08-SSal6.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSar1.1-0 ; 
       fig08-SSar2.1-0 ; 
       fig08-SSar3.1-0 ; 
       fig08-SSar4.1-0 ; 
       fig08-SSar5.1-0 ; 
       fig08-SSar6.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
       fig08-wepatt.1-0 ; 
       fig08-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-new.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 56     
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.4-0 ; 
       add_gun-t2d26.4-0 ; 
       add_gun-t2d27.4-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       medium_fighter_sPATL-t2d33.2-0 ; 
       medium_fighter_sPATL-t2d34.2-0 ; 
       medium_fighter_sPATL-t2d35.2-0 ; 
       medium_fighter_sPATL-t2d36.2-0 ; 
       new-t2d24.1-0 ; 
       new-t2d27.1-0 ; 
       new-t2d28.1-0 ; 
       new-t2d29.1-0 ; 
       new-t2d31.1-0 ; 
       new-t2d33.1-0 ; 
       new-t2d39.1-0 ; 
       new-t2d40.1-0 ; 
       new-t2d41.1-0 ; 
       new-t2d42.1-0 ; 
       new-t2d43.1-0 ; 
       new-t2d44.1-0 ; 
       new-t2d45.1-0 ; 
       new-t2d46.1-0 ; 
       new-t2d47.1-0 ; 
       new-t2d48.1-0 ; 
       new-t2d49.1-0 ; 
       new-t2d50.1-0 ; 
       new-t2d51.1-0 ; 
       new-t2d52.1-0 ; 
       new-t2d53.1-0 ; 
       new-t2d54.1-0 ; 
       new-t2d55.1-0 ; 
       new-t2d56.1-0 ; 
       new-t2d57.1-0 ; 
       new-t2d58.1-0 ; 
       new-t2d59.2-0 ; 
       new-t2d6.1-0 ; 
       new-t2d60.1-0 ; 
       new-t2d61.1-0 ; 
       new-t2d62.1-0 ; 
       new-t2d63.1-0 ; 
       new-t2d64.1-0 ; 
       new-t2d65.1-0 ; 
       new-t2d66.1-0 ; 
       new-t2d67.1-0 ; 
       new-t2d68.1-0 ; 
       new-t2d69.1-0 ; 
       new-t2d70.1-0 ; 
       new-t2d71.2-0 ; 
       new-z.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 27 110 ; 
       4 42 110 ; 
       5 42 110 ; 
       6 42 110 ; 
       8 7 110 ; 
       10 32 110 ; 
       11 33 110 ; 
       12 10 110 ; 
       13 11 110 ; 
       14 10 110 ; 
       15 11 110 ; 
       16 10 110 ; 
       17 11 110 ; 
       18 10 110 ; 
       19 11 110 ; 
       20 8 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       9 8 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 8 110 ; 
       28 1 110 ; 
       29 2 110 ; 
       30 3 110 ; 
       31 8 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 10 110 ; 
       35 11 110 ; 
       36 46 110 ; 
       37 46 110 ; 
       38 46 110 ; 
       39 46 110 ; 
       40 46 110 ; 
       41 46 110 ; 
       42 8 110 ; 
       43 6 110 ; 
       44 5 110 ; 
       45 4 110 ; 
       46 8 110 ; 
       47 8 110 ; 
       48 8 110 ; 
       49 21 110 ; 
       50 22 110 ; 
       51 23 110 ; 
       52 24 110 ; 
       53 25 110 ; 
       54 26 110 ; 
       55 8 110 ; 
       56 36 110 ; 
       57 37 110 ; 
       58 38 110 ; 
       59 39 110 ; 
       60 40 110 ; 
       61 41 110 ; 
       62 8 110 ; 
       63 7 110 ; 
       64 7 110 ; 
       65 66 110 ; 
       66 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 56 300 ; 
       2 57 300 ; 
       3 58 300 ; 
       4 59 300 ; 
       5 60 300 ; 
       6 61 300 ; 
       8 26 300 ; 
       8 85 300 ; 
       8 23 300 ; 
       8 29 300 ; 
       8 83 300 ; 
       8 22 300 ; 
       8 86 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       11 62 300 ; 
       11 63 300 ; 
       11 64 300 ; 
       11 65 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 76 300 ; 
       13 77 300 ; 
       13 78 300 ; 
       14 0 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       15 72 300 ; 
       15 73 300 ; 
       15 74 300 ; 
       15 75 300 ; 
       16 7 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       17 66 300 ; 
       17 67 300 ; 
       17 68 300 ; 
       18 17 300 ; 
       19 79 300 ; 
       21 38 300 ; 
       22 39 300 ; 
       9 18 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 42 300 ; 
       26 43 300 ; 
       27 25 300 ; 
       27 28 300 ; 
       27 31 300 ; 
       34 4 300 ; 
       34 5 300 ; 
       34 6 300 ; 
       35 69 300 ; 
       35 70 300 ; 
       35 71 300 ; 
       36 37 300 ; 
       37 36 300 ; 
       38 35 300 ; 
       39 34 300 ; 
       40 32 300 ; 
       41 33 300 ; 
       42 24 300 ; 
       42 27 300 ; 
       42 30 300 ; 
       47 81 300 ; 
       48 82 300 ; 
       49 45 300 ; 
       50 44 300 ; 
       51 46 300 ; 
       52 47 300 ; 
       53 48 300 ; 
       54 49 300 ; 
       55 84 300 ; 
       56 50 300 ; 
       57 51 300 ; 
       58 52 300 ; 
       59 53 300 ; 
       60 54 300 ; 
       61 55 300 ; 
       62 80 300 ; 
       66 19 300 ; 
       66 20 300 ; 
       66 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       16 5 400 ; 
       17 46 400 ; 
       21 17 400 ; 
       22 18 400 ; 
       9 11 400 ; 
       24 19 400 ; 
       26 20 400 ; 
       36 16 400 ; 
       39 15 400 ; 
       41 55 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       7 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 41 401 ; 
       22 28 401 ; 
       26 29 401 ; 
       27 31 401 ; 
       28 33 401 ; 
       29 27 401 ; 
       30 32 401 ; 
       31 34 401 ; 
       32 21 401 ; 
       35 22 401 ; 
       36 23 401 ; 
       40 24 401 ; 
       42 25 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       56 35 401 ; 
       57 36 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 39 401 ; 
       61 40 401 ; 
       63 43 401 ; 
       64 44 401 ; 
       65 45 401 ; 
       67 47 401 ; 
       70 48 401 ; 
       71 49 401 ; 
       74 50 401 ; 
       75 51 401 ; 
       77 52 401 ; 
       78 53 401 ; 
       79 54 401 ; 
       83 26 401 ; 
       85 42 401 ; 
       86 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 70 -6 0 MPRFLG 0 ; 
       3 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 48.75 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       8 SCHEM 45 -2 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 80 -6 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 85 -8 0 MPRFLG 0 ; 
       14 SCHEM 10 -8 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 MPRFLG 0 ; 
       20 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 MPRFLG 0 ; 
       22 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 MPRFLG 0 ; 
       24 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 60 -6 0 MPRFLG 0 ; 
       26 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 70 -4 0 MPRFLG 0 ; 
       28 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 80 -4 0 MPRFLG 0 ; 
       34 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 75 -8 0 MPRFLG 0 ; 
       36 SCHEM 40 -6 0 MPRFLG 0 ; 
       37 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 30 -6 0 MPRFLG 0 ; 
       39 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 35 -6 0 MPRFLG 0 ; 
       41 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       43 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       47 SCHEM 42.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 45 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 65 -8 0 MPRFLG 0 ; 
       50 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       51 SCHEM 55 -8 0 MPRFLG 0 ; 
       52 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       53 SCHEM 60 -8 0 MPRFLG 0 ; 
       54 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       55 SCHEM 47.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       56 SCHEM 40 -8 0 MPRFLG 0 ; 
       57 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       58 SCHEM 30 -8 0 MPRFLG 0 ; 
       59 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       60 SCHEM 35 -8 0 MPRFLG 0 ; 
       61 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       62 SCHEM 50 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 2.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       66 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 64 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 86.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 81.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 84 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 79 -10 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 89 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 74 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 71.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 89 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 86.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 86.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 86.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 81.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 81.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 74 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 74 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 84 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 79 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 96.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
