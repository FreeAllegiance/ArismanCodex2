SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.50-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       STATIC-light1.1-0 ROOT ; 
       STATIC-light2.1-0 ROOT ; 
       STATIC-light3.1-0 ROOT ; 
       STATIC-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       medium_fighter_sPATL-mat39.3-0 ; 
       medium_fighter_sPATL-mat40.3-0 ; 
       medium_fighter_sPATL-mat41.3-0 ; 
       new-back1.2-0 ; 
       new-base.2-0 ; 
       new-base1.2-0 ; 
       new-base2.2-0 ; 
       new-bottom.2-0 ; 
       new-caution1.2-0 ; 
       new-caution2.2-0 ; 
       new-front1.2-0 ; 
       new-guns1.2-0 ; 
       new-guns2.2-0 ; 
       new-mat23.2-0 ; 
       new-mat24.2-0 ; 
       new-mat25.2-0 ; 
       new-mat26.2-0 ; 
       new-mat27.2-0 ; 
       new-mat28.2-0 ; 
       new-mat29.2-0 ; 
       new-mat30.2-0 ; 
       new-mat32.2-0 ; 
       new-mat33.2-0 ; 
       new-mat34.2-0 ; 
       new-mat57.1-0 ; 
       new-sides_and_bottom.2-0 ; 
       new-top.2-0 ; 
       new-vents1.2-0 ; 
       remove_anim_edit_nulls-mat20.2-0 ; 
       remove_anim_edit_nulls-mat41.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       remove_anim_edit_nulls-fig08_3_1.3-0 ROOT ; 
       remove_anim_edit_nulls-fuselg.1-0 ; 
       remove_anim_edit_nulls-lslrsal0.1-0 ; 
       remove_anim_edit_nulls-lslrsal1.1-0 ; 
       remove_anim_edit_nulls-lslrsal2.1-0 ; 
       remove_anim_edit_nulls-lslrsal3.1-0 ; 
       remove_anim_edit_nulls-lslrsal4.1-0 ; 
       remove_anim_edit_nulls-lslrsal5.1-0 ; 
       remove_anim_edit_nulls-lslrsal6.2-0 ; 
       remove_anim_edit_nulls-lwepbar.1-0 ; 
       remove_anim_edit_nulls-rslrsal1.1-0 ; 
       remove_anim_edit_nulls-rslrsal2.1-0 ; 
       remove_anim_edit_nulls-rslrsal3.1-0 ; 
       remove_anim_edit_nulls-rslrsal4.1-0 ; 
       remove_anim_edit_nulls-rslrsal5.1-0 ; 
       remove_anim_edit_nulls-rslrsal6.1-0 ; 
       remove_anim_edit_nulls-rwepbar.2-0 ; 
       remove_anim_edit_nulls-slrsal0.1-0 ; 
       remove_anim_edit_nulls-tgun.2-0 ; 
       remove_anim_edit_nulls-twepbas.1-0 ; 
       remove_anim_edit_nulls-wepatt.1-0 ; 
       remove_anim_edit_nulls-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       medium_fighter_sPATL-t2d34.5-0 ; 
       medium_fighter_sPATL-t2d35.5-0 ; 
       medium_fighter_sPATL-t2d36.5-0 ; 
       new-t2d24.4-0 ; 
       new-t2d27.4-0 ; 
       new-t2d28.4-0 ; 
       new-t2d29.5-0 ; 
       new-t2d31.4-0 ; 
       new-t2d33.4-0 ; 
       new-t2d39.4-0 ; 
       new-t2d40.4-0 ; 
       new-t2d41.4-0 ; 
       new-t2d43.4-0 ; 
       new-t2d44.4-0 ; 
       new-t2d45.4-0 ; 
       new-t2d46.4-0 ; 
       new-t2d47.4-0 ; 
       new-t2d48.4-0 ; 
       new-t2d49.4-0 ; 
       new-t2d50.4-0 ; 
       new-t2d51.4-0 ; 
       new-t2d52.4-0 ; 
       new-t2d6.4-0 ; 
       new-z.4-0 ; 
       remove_anim_edit_nulls-t2d19.2-0 ; 
       remove_anim_edit_nulls-t2d37.2-0 ; 
       STATIC-t2d59.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 19 110 ; 
       19 21 110 ; 
       20 1 110 ; 
       21 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 7 300 ; 
       1 26 300 ; 
       1 4 300 ; 
       1 10 300 ; 
       1 25 300 ; 
       1 3 300 ; 
       1 27 300 ; 
       3 19 300 ; 
       4 20 300 ; 
       5 24 300 ; 
       6 21 300 ; 
       7 22 300 ; 
       8 23 300 ; 
       9 6 300 ; 
       9 9 300 ; 
       9 12 300 ; 
       10 18 300 ; 
       11 17 300 ; 
       12 16 300 ; 
       13 15 300 ; 
       14 13 300 ; 
       15 14 300 ; 
       16 5 300 ; 
       16 8 300 ; 
       16 11 300 ; 
       18 29 300 ; 
       19 28 300 ; 
       21 0 300 ; 
       21 1 300 ; 
       21 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 5 400 ; 
       4 6 400 ; 
       5 26 400 ; 
       6 7 400 ; 
       8 8 400 ; 
       10 4 400 ; 
       13 3 400 ; 
       15 23 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 15 401 ; 
       7 16 401 ; 
       8 18 401 ; 
       9 20 401 ; 
       10 14 401 ; 
       11 19 401 ; 
       12 21 401 ; 
       13 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       22 12 401 ; 
       25 13 401 ; 
       26 22 401 ; 
       27 17 401 ; 
       28 24 401 ; 
       29 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 25 -10 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 30 -10 0 MPRFLG 0 ; 
       9 SCHEM 35 -8 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 5 -10 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 10 -10 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 15 -10 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 0 -14 0 MPRFLG 0 ; 
       19 SCHEM 0 -12 0 MPRFLG 0 ; 
       20 SCHEM 0 -8 0 MPRFLG 0 ; 
       21 SCHEM 0 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 50.14839 -10.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 50.14839 -12.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 50.14839 -12.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 50.14839 -14.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
