SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.37-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-nose_white-center.1-0.1-0 ; 
       new-nose_white-center.1-1.1-0 ; 
       new-port_red-left.1-0.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-starbord_green-right.1-0.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
       static-mat20.2-0 ; 
       static-mat41.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       static-chn2.1-0 ; 
       static-cockpt.1-0 ; 
       static-cube1.1-0 ; 
       static-cube2.2-0 ; 
       static-cube3.2-0 ; 
       static-cube4.1-0 ; 
       static-cube5.1-0 ; 
       static-cube6.1-0 ; 
       static-eff2.1-0 ; 
       static-fig08_3.2-0 ROOT ; 
       static-fuselg.1-0 ; 
       static-jnt2_1.1-0 ; 
       static-jnt2_2.1-0 ; 
       static-lslrsal0.1-0 ; 
       static-lslrsal1.1-0 ; 
       static-lslrsal2.1-0 ; 
       static-lslrsal3.1-0 ; 
       static-lslrsal4.1-0 ; 
       static-lslrsal5.1-0 ; 
       static-lslrsal6.2-0 ; 
       static-lwepbar.1-0 ; 
       static-lwepemt1.1-0 ; 
       static-lwepemt2.1-0 ; 
       static-lwepemt3.1-0 ; 
       static-missemt.1-0 ; 
       static-null4.1-0 ; 
       static-rslrsal1.1-0 ; 
       static-rslrsal2.1-0 ; 
       static-rslrsal3.1-0 ; 
       static-rslrsal4.1-0 ; 
       static-rslrsal5.1-0 ; 
       static-rslrsal6.1-0 ; 
       static-rwepbar.2-0 ; 
       static-rwepemt1.1-0 ; 
       static-rwepemt2.1-0 ; 
       static-rwepemt3.1-0 ; 
       static-slrsal0.1-0 ; 
       static-SSa.1-0 ; 
       static-SSal.1-0 ; 
       static-SSal1.1-0 ; 
       static-SSal2.1-0 ; 
       static-SSal3.1-0 ; 
       static-SSal4.1-0 ; 
       static-SSal5.1-0 ; 
       static-SSal6.1-0 ; 
       static-SSar.1-0 ; 
       static-SSar1.1-0 ; 
       static-SSar2.1-0 ; 
       static-SSar3.1-0 ; 
       static-SSar4.1-0 ; 
       static-SSar5.1-0 ; 
       static-SSar6.1-0 ; 
       static-SSf.1-0 ; 
       static-tgun.2-0 ; 
       static-thrust.1-0 ; 
       static-trail.1-0 ; 
       static-twepbas.1-0 ; 
       static-twepemt.1-0 ; 
       static-wepatt.1-0 ; 
       static-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       medium_fighter_sPATL-t2d34.3-0 ; 
       medium_fighter_sPATL-t2d35.3-0 ; 
       medium_fighter_sPATL-t2d36.3-0 ; 
       new-t2d24.2-0 ; 
       new-t2d27.2-0 ; 
       new-t2d28.2-0 ; 
       new-t2d29.2-0 ; 
       new-t2d31.2-0 ; 
       new-t2d33.2-0 ; 
       new-t2d39.2-0 ; 
       new-t2d40.2-0 ; 
       new-t2d41.2-0 ; 
       new-t2d42.2-0 ; 
       new-t2d43.2-0 ; 
       new-t2d44.2-0 ; 
       new-t2d45.2-0 ; 
       new-t2d46.2-0 ; 
       new-t2d47.2-0 ; 
       new-t2d48.2-0 ; 
       new-t2d49.2-0 ; 
       new-t2d50.2-0 ; 
       new-t2d51.2-0 ; 
       new-t2d52.2-0 ; 
       new-t2d53.2-0 ; 
       new-t2d54.2-0 ; 
       new-t2d55.2-0 ; 
       new-t2d56.2-0 ; 
       new-t2d57.2-0 ; 
       new-t2d58.2-0 ; 
       new-t2d6.2-0 ; 
       new-z.2-0 ; 
       static-t2d19.2-0 ; 
       static-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 58 110 ; 
       1 9 110 ; 
       2 20 110 ; 
       3 20 110 ; 
       4 20 110 ; 
       5 32 110 ; 
       6 32 110 ; 
       7 32 110 ; 
       8 12 110 ; 
       10 9 110 ; 
       11 0 110 ; 
       12 11 110 ; 
       13 10 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       18 13 110 ; 
       19 13 110 ; 
       20 10 110 ; 
       21 2 110 ; 
       22 3 110 ; 
       23 4 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 36 110 ; 
       27 36 110 ; 
       28 36 110 ; 
       29 36 110 ; 
       30 36 110 ; 
       31 36 110 ; 
       32 10 110 ; 
       33 7 110 ; 
       34 6 110 ; 
       35 5 110 ; 
       36 10 110 ; 
       37 10 110 ; 
       38 10 110 ; 
       39 14 110 ; 
       40 15 110 ; 
       41 16 110 ; 
       42 17 110 ; 
       43 18 110 ; 
       44 19 110 ; 
       45 10 110 ; 
       46 26 110 ; 
       47 27 110 ; 
       48 28 110 ; 
       49 29 110 ; 
       50 30 110 ; 
       51 31 110 ; 
       52 10 110 ; 
       53 56 110 ; 
       54 9 110 ; 
       55 9 110 ; 
       56 59 110 ; 
       57 53 110 ; 
       58 10 110 ; 
       59 58 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 37 300 ; 
       3 38 300 ; 
       4 39 300 ; 
       5 40 300 ; 
       6 41 300 ; 
       7 42 300 ; 
       10 7 300 ; 
       10 48 300 ; 
       10 4 300 ; 
       10 10 300 ; 
       10 46 300 ; 
       10 3 300 ; 
       10 49 300 ; 
       14 19 300 ; 
       15 20 300 ; 
       16 21 300 ; 
       17 22 300 ; 
       18 23 300 ; 
       19 24 300 ; 
       20 6 300 ; 
       20 9 300 ; 
       20 12 300 ; 
       26 18 300 ; 
       27 17 300 ; 
       28 16 300 ; 
       29 15 300 ; 
       30 13 300 ; 
       31 14 300 ; 
       32 5 300 ; 
       32 8 300 ; 
       32 11 300 ; 
       37 44 300 ; 
       38 45 300 ; 
       39 26 300 ; 
       40 25 300 ; 
       41 27 300 ; 
       42 28 300 ; 
       43 29 300 ; 
       44 30 300 ; 
       45 47 300 ; 
       46 31 300 ; 
       47 32 300 ; 
       48 33 300 ; 
       49 34 300 ; 
       50 35 300 ; 
       51 36 300 ; 
       52 43 300 ; 
       53 51 300 ; 
       56 50 300 ; 
       59 0 300 ; 
       59 1 300 ; 
       59 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 5 400 ; 
       15 6 400 ; 
       17 7 400 ; 
       19 8 400 ; 
       26 4 400 ; 
       29 3 400 ; 
       31 30 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 16 401 ; 
       7 17 401 ; 
       8 19 401 ; 
       9 21 401 ; 
       10 15 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       21 12 401 ; 
       23 13 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       46 14 401 ; 
       48 29 401 ; 
       49 18 401 ; 
       50 31 401 ; 
       51 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 1 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 97.5 -2 0 WIRECOL 1 3 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 57.5 -6 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 5 -12 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 50 0 0 WIRECOL 0 3 DISPLAY 1 2 SRT 1 1 1 2.970961e-007 -3.684032e-008 -6.945096e-009 -1.534618e-010 -0.2177 -2.903108e-007 MPRFLG 0 ; 
       10 SCHEM 46.25 -2 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 5 -10 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 46.25 -4 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 52.5 -6 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       16 SCHEM 42.5 -6 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       17 SCHEM 45 -6 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       18 SCHEM 47.5 -6 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 50 -6 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 57.5 -4 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       21 SCHEM 60 -8 0 WIRECOL 1 6 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 57.5 -8 0 WIRECOL 1 4 DISPLAY 1 2 MPRFLG 0 ; 
       23 SCHEM 55 -8 0 WIRECOL 1 3 DISPLAY 1 2 MPRFLG 0 ; 
       24 SCHEM 75 -4 0 WIRECOL 1 3 DISPLAY 1 2 MPRFLG 0 ; 
       25 SCHEM 77.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 27.5 -6 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       27 SCHEM 15 -6 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       29 SCHEM 20 -6 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       30 SCHEM 22.5 -6 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       32 SCHEM 10 -4 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       33 SCHEM 12.5 -8 0 WIRECOL 1 6 DISPLAY 1 2 MPRFLG 0 ; 
       34 SCHEM 10 -8 0 WIRECOL 1 5 DISPLAY 1 2 MPRFLG 0 ; 
       35 SCHEM 7.5 -8 0 WIRECOL 1 4 DISPLAY 1 2 MPRFLG 0 ; 
       36 SCHEM 21.25 -4 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       37 SCHEM 30 -4 0 WIRECOL 3 5 DISPLAY 1 2 MPRFLG 0 ; 
       38 SCHEM 32.5 -4 0 WIRECOL 4 3 DISPLAY 1 2 MPRFLG 0 ; 
       39 SCHEM 52.5 -8 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       40 SCHEM 40 -8 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       41 SCHEM 42.5 -8 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       42 SCHEM 45 -8 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       43 SCHEM 47.5 -8 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       44 SCHEM 50 -8 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       45 SCHEM 35 -4 0 WIRECOL 2 6 DISPLAY 1 2 MPRFLG 0 ; 
       46 SCHEM 27.5 -8 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       47 SCHEM 15 -8 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       48 SCHEM 17.5 -8 0 WIRECOL 0 3 DISPLAY 1 2 MPRFLG 0 ; 
       49 SCHEM 20 -8 0 WIRECOL 0 4 DISPLAY 1 2 MPRFLG 0 ; 
       50 SCHEM 22.5 -8 0 WIRECOL 0 5 DISPLAY 1 2 MPRFLG 0 ; 
       51 SCHEM 25 -8 0 WIRECOL 0 6 DISPLAY 1 2 MPRFLG 0 ; 
       52 SCHEM 37.5 -4 0 WIRECOL 3 4 DISPLAY 1 2 MPRFLG 0 ; 
       53 SCHEM 2.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       54 SCHEM 92.5 -2 0 WIRECOL 1 5 DISPLAY 1 2 MPRFLG 0 ; 
       55 SCHEM 95 -2 0 WIRECOL 1 6 DISPLAY 1 2 MPRFLG 0 ; 
       56 SCHEM 2.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       57 SCHEM 2.5 -12 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       58 SCHEM 3.75 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       59 SCHEM 2.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 91.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 52.64839 -6.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 52.64839 -8.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 52.64839 -8.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 52.64839 -10.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
