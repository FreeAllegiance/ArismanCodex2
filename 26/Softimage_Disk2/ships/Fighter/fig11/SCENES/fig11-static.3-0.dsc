SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.38-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       medium_fighter_sPATL-mat39.2-0 ; 
       medium_fighter_sPATL-mat40.2-0 ; 
       medium_fighter_sPATL-mat41.2-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat31.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
       static-mat20.2-0 ; 
       static-mat41.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       static-fig08_3.3-0 ROOT ; 
       static-fuselg.1-0 ; 
       static-lslrsal0.1-0 ; 
       static-lslrsal1.1-0 ; 
       static-lslrsal2.1-0 ; 
       static-lslrsal3.1-0 ; 
       static-lslrsal4.1-0 ; 
       static-lslrsal5.1-0 ; 
       static-lslrsal6.2-0 ; 
       static-lwepbar.1-0 ; 
       static-rslrsal1.1-0 ; 
       static-rslrsal2.1-0 ; 
       static-rslrsal3.1-0 ; 
       static-rslrsal4.1-0 ; 
       static-rslrsal5.1-0 ; 
       static-rslrsal6.1-0 ; 
       static-slrsal0.1-0 ; 
       static-tgun.2-0 ; 
       static-twepbas.1-0 ; 
       static-wepatt.1-0 ; 
       static-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       medium_fighter_sPATL-t2d34.3-0 ; 
       medium_fighter_sPATL-t2d35.3-0 ; 
       medium_fighter_sPATL-t2d36.3-0 ; 
       new-t2d24.2-0 ; 
       new-t2d27.2-0 ; 
       new-t2d28.2-0 ; 
       new-t2d29.2-0 ; 
       new-t2d31.2-0 ; 
       new-t2d33.2-0 ; 
       new-t2d39.2-0 ; 
       new-t2d40.2-0 ; 
       new-t2d41.2-0 ; 
       new-t2d42.2-0 ; 
       new-t2d43.2-0 ; 
       new-t2d44.2-0 ; 
       new-t2d45.2-0 ; 
       new-t2d46.2-0 ; 
       new-t2d47.2-0 ; 
       new-t2d48.2-0 ; 
       new-t2d51.2-0 ; 
       new-t2d52.2-0 ; 
       new-t2d6.2-0 ; 
       new-z.2-0 ; 
       static-t2d19.2-0 ; 
       static-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 1 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       16 1 110 ; 
       17 18 110 ; 
       18 20 110 ; 
       19 1 110 ; 
       20 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 6 300 ; 
       1 23 300 ; 
       1 4 300 ; 
       1 8 300 ; 
       1 22 300 ; 
       1 3 300 ; 
       1 24 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       6 19 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 5 300 ; 
       9 7 300 ; 
       9 9 300 ; 
       10 15 300 ; 
       11 14 300 ; 
       12 13 300 ; 
       13 12 300 ; 
       14 10 300 ; 
       15 11 300 ; 
       17 26 300 ; 
       18 25 300 ; 
       20 0 300 ; 
       20 1 300 ; 
       20 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 5 400 ; 
       4 6 400 ; 
       6 7 400 ; 
       8 8 400 ; 
       10 4 400 ; 
       13 3 400 ; 
       15 22 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 16 401 ; 
       6 17 401 ; 
       7 19 401 ; 
       8 15 401 ; 
       9 20 401 ; 
       10 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       18 12 401 ; 
       20 13 401 ; 
       22 14 401 ; 
       23 21 401 ; 
       24 18 401 ; 
       25 23 401 ; 
       26 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 WIRECOL 0 3 SRT 1 1 1 2.970961e-007 -3.684032e-008 -6.945096e-009 -1.534618e-010 -0.2177 -2.903108e-007 MPRFLG 0 ; 
       1 SCHEM 18.75 -2 0 WIRECOL 0 4 MPRFLG 0 ; 
       2 SCHEM 26.25 -4 0 WIRECOL 0 5 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 WIRECOL 0 6 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 0 6 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 WIRECOL 0 3 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 WIRECOL 0 4 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 WIRECOL 0 5 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 WIRECOL 0 3 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 WIRECOL 0 4 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 0 4 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 WIRECOL 0 5 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 0 6 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 WIRECOL 0 3 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 WIRECOL 0 4 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 WIRECOL 0 5 MPRFLG 0 ; 
       16 SCHEM 11.25 -4 0 WIRECOL 0 3 MPRFLG 0 ; 
       17 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 52.64839 -6.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.64839 -8.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 52.64839 -8.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.64839 -10.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
