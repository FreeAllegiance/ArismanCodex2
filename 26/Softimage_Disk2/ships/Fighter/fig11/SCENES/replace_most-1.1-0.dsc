SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig11-fig11.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.4-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 11     
       1-light1.1-0 ROOT ; 
       1-light2.1-0 ROOT ; 
       1-light3.1-0 ROOT ; 
       1-light4.1-0 ROOT ; 
       1-light5.1-0 ROOT ; 
       1-light6.1-0 ROOT ; 
       medium_fighter_sPATL-inf_light1_2_2_2_2_2.2-0 ROOT ; 
       medium_fighter_sPATL-inf_light2_2_2_2_2_2.2-0 ROOT ; 
       medium_fighter_sPATL-inf_light3_2_2_2_2_2.2-0 ROOT ; 
       medium_fighter_sPATL-light5_1_2_4_1_2_2_2_2_2.2-0 ROOT ; 
       medium_fighter_sPATL-light6_3_2_4_1_2_2_2_2_2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       1-back1.1-0 ; 
       1-base.1-0 ; 
       1-base1.1-0 ; 
       1-base2.1-0 ; 
       1-bottom.1-0 ; 
       1-caution1.1-0 ; 
       1-caution2.1-0 ; 
       1-front1.1-0 ; 
       1-guns1.1-0 ; 
       1-guns2.1-0 ; 
       1-mat51.1-0 ; 
       1-mat52.1-0 ; 
       1-mat53.1-0 ; 
       1-mat54.1-0 ; 
       1-mat55.1-0 ; 
       1-mat56.1-0 ; 
       1-sides_and_bottom.1-0 ; 
       1-top.1-0 ; 
       1-vents1.1-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat66.2-0 ; 
       medium_fighter_sPATL-mat10.1-0 ; 
       medium_fighter_sPATL-mat11.1-0 ; 
       medium_fighter_sPATL-mat20.1-0 ; 
       medium_fighter_sPATL-mat21.1-0 ; 
       medium_fighter_sPATL-mat22.1-0 ; 
       medium_fighter_sPATL-mat23.1-0 ; 
       medium_fighter_sPATL-mat24.1-0 ; 
       medium_fighter_sPATL-mat25.1-0 ; 
       medium_fighter_sPATL-mat26.1-0 ; 
       medium_fighter_sPATL-mat27.1-0 ; 
       medium_fighter_sPATL-mat28.1-0 ; 
       medium_fighter_sPATL-mat29.1-0 ; 
       medium_fighter_sPATL-mat30.1-0 ; 
       medium_fighter_sPATL-mat31.1-0 ; 
       medium_fighter_sPATL-mat32.1-0 ; 
       medium_fighter_sPATL-mat34.1-0 ; 
       medium_fighter_sPATL-mat35.1-0 ; 
       medium_fighter_sPATL-mat36.1-0 ; 
       medium_fighter_sPATL-mat37.1-0 ; 
       medium_fighter_sPATL-mat38.1-0 ; 
       medium_fighter_sPATL-mat39.1-0 ; 
       medium_fighter_sPATL-mat40.1-0 ; 
       medium_fighter_sPATL-mat41.1-0 ; 
       medium_fighter_sPATL-mat42.1-0 ; 
       medium_fighter_sPATL-mat43.1-0 ; 
       medium_fighter_sPATL-mat44.1-0 ; 
       medium_fighter_sPATL-mat45.1-0 ; 
       medium_fighter_sPATL-mat46.1-0 ; 
       medium_fighter_sPATL-mat47.1-0 ; 
       medium_fighter_sPATL-mat49.1-0 ; 
       medium_fighter_sPATL-mat5.1-0 ; 
       medium_fighter_sPATL-mat50.1-0 ; 
       medium_fighter_sPATL-mat51.1-0 ; 
       medium_fighter_sPATL-mat52.1-0 ; 
       medium_fighter_sPATL-mat53.1-0 ; 
       medium_fighter_sPATL-mat54.1-0 ; 
       medium_fighter_sPATL-mat8.1-0 ; 
       medium_fighter_sPATL-port_red-left.1-0.1-0 ; 
       medium_fighter_sPATL-starbord_green-right.1-0.1-0 ; 
       medium_fighter_sPATL-white-nose_white-center.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       fig11-cockpt.1-0 ; 
       fig11-cube1.1-0 ; 
       fig11-cube2.2-0 ; 
       fig11-cube3.2-0 ; 
       fig11-cube4.1-0 ; 
       fig11-cube5.1-0 ; 
       fig11-cube6.1-0 ; 
       fig11-fig08_3_1.1-0 ; 
       fig11-fig11.2-0 ROOT ; 
       fig11-fuselg.1-0 ROOT ; 
       fig11-fuselg_1.1-0 ; 
       fig11-hatchz.2-0 ; 
       fig11-LL1.2-0 ; 
       fig11-LLa.1-0 ; 
       fig11-llandgr.1-0 ; 
       fig11-LLl.1-0 ; 
       fig11-LLr.1-0 ; 
       fig11-lwepbar.2-0 ; 
       fig11-lwepemt1.1-0 ; 
       fig11-lwepemt2.1-0 ; 
       fig11-lwepemt3.1-0 ; 
       fig11-lwingzz0.1-0 ; 
       fig11-lwingzz1.1-0 ; 
       fig11-lwingzz2.1-0 ; 
       fig11-lwingzz3.1-0 ; 
       fig11-lwingzz4.1-0 ; 
       fig11-lwingzz5.1-0 ; 
       fig11-lwingzz6.1-0 ; 
       fig11-null1.1-0 ; 
       fig11-rlandgr.1-0 ; 
       fig11-rwepbar_1.1-0 ; 
       fig11-rwepemt1.1-0 ; 
       fig11-rwepemt2.1-0 ; 
       fig11-rwepemt3.1-0 ; 
       fig11-rwingzz0.1-0 ; 
       fig11-rwingzz1.1-0 ; 
       fig11-rwingzz2.1-0 ; 
       fig11-rwingzz3.1-0 ; 
       fig11-rwingzz4.1-0 ; 
       fig11-rwingzz5.1-0 ; 
       fig11-rwingzz6.1-0 ; 
       fig11-SSf.1-0 ; 
       fig11-SSl.1-0 ; 
       fig11-SSl1.1-0 ; 
       fig11-SSl2.1-0 ; 
       fig11-SSl3.1-0 ; 
       fig11-SSl4.1-0 ; 
       fig11-SSl5.1-0 ; 
       fig11-SSl6.1-0 ; 
       fig11-SSr.1-0 ; 
       fig11-SSr1.1-0 ; 
       fig11-SSr2.1-0 ; 
       fig11-SSr3.1-0 ; 
       fig11-SSr4.1-0 ; 
       fig11-SSr5.1-0 ; 
       fig11-SSr6.1-0 ; 
       fig11-thrust.1-0 ; 
       fig11-trail.1-0 ; 
       fig11-wepatt.1-0 ; 
       fig11-wepmnt.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig08 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       replace_most-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       1-t2d44.1-0 ; 
       1-t2d45.1-0 ; 
       1-t2d46.1-0 ; 
       1-t2d47.1-0 ; 
       1-t2d48.1-0 ; 
       1-t2d49.1-0 ; 
       1-t2d50.1-0 ; 
       1-t2d51.1-0 ; 
       1-t2d52.1-0 ; 
       1-t2d53.1-0 ; 
       1-t2d54.1-0 ; 
       1-t2d55.1-0 ; 
       1-t2d56.1-0 ; 
       1-t2d57.1-0 ; 
       1-t2d58.1-0 ; 
       1-t2d59.1-0 ; 
       1-t2d6.1-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       medium_fighter_sPATL-t2d15.1-0 ; 
       medium_fighter_sPATL-t2d16.1-0 ; 
       medium_fighter_sPATL-t2d19.1-0 ; 
       medium_fighter_sPATL-t2d2.1-0 ; 
       medium_fighter_sPATL-t2d21.1-0 ; 
       medium_fighter_sPATL-t2d23.1-0 ; 
       medium_fighter_sPATL-t2d27.1-0 ; 
       medium_fighter_sPATL-t2d29.1-0 ; 
       medium_fighter_sPATL-t2d30.1-0 ; 
       medium_fighter_sPATL-t2d31.1-0 ; 
       medium_fighter_sPATL-t2d32.1-0 ; 
       medium_fighter_sPATL-t2d33.1-0 ; 
       medium_fighter_sPATL-t2d34.1-0 ; 
       medium_fighter_sPATL-t2d35.1-0 ; 
       medium_fighter_sPATL-t2d36.1-0 ; 
       medium_fighter_sPATL-t2d4.1-0 ; 
       medium_fighter_sPATL-t2d5.1-0 ; 
       med_fig_F-t2d43.1-0 ; 
       med_fig_F-t2d44.1-0 ; 
       med_fig_F-t2d45.1-0 ; 
       med_fig_F-t2d46.1-0 ; 
       med_fig_F-t2d47.1-0 ; 
       med_fig_F-t2d48.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 10 110 ; 
       21 10 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       34 10 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 34 110 ; 
       38 34 110 ; 
       39 34 110 ; 
       40 34 110 ; 
       41 10 110 ; 
       42 10 110 ; 
       43 22 110 ; 
       44 23 110 ; 
       45 24 110 ; 
       46 25 110 ; 
       47 26 110 ; 
       48 27 110 ; 
       49 10 110 ; 
       50 35 110 ; 
       51 36 110 ; 
       52 37 110 ; 
       53 38 110 ; 
       54 39 110 ; 
       55 40 110 ; 
       58 59 110 ; 
       59 10 110 ; 
       0 7 110 ; 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 30 110 ; 
       5 30 110 ; 
       6 30 110 ; 
       7 10 110 ; 
       10 8 110 ; 
       12 28 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 10 110 ; 
       18 1 110 ; 
       19 2 110 ; 
       20 3 110 ; 
       28 7 110 ; 
       29 12 110 ; 
       30 10 110 ; 
       31 6 110 ; 
       32 5 110 ; 
       33 4 110 ; 
       56 7 110 ; 
       57 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 67 300 ; 
       9 73 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 51 300 ; 
       9 53 300 ; 
       9 54 300 ; 
       9 55 300 ; 
       11 56 300 ; 
       22 45 300 ; 
       23 46 300 ; 
       24 47 300 ; 
       25 48 300 ; 
       26 49 300 ; 
       26 52 300 ; 
       27 50 300 ; 
       35 39 300 ; 
       36 40 300 ; 
       37 41 300 ; 
       38 42 300 ; 
       39 43 300 ; 
       40 44 300 ; 
       41 76 300 ; 
       42 74 300 ; 
       43 72 300 ; 
       44 66 300 ; 
       45 68 300 ; 
       46 69 300 ; 
       47 70 300 ; 
       48 71 300 ; 
       49 75 300 ; 
       50 61 300 ; 
       51 62 300 ; 
       52 63 300 ; 
       53 64 300 ; 
       54 65 300 ; 
       55 60 300 ; 
       59 57 300 ; 
       59 58 300 ; 
       59 59 300 ; 
       1 10 300 ; 
       2 11 300 ; 
       3 12 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       6 15 300 ; 
       10 4 300 ; 
       10 17 300 ; 
       10 1 300 ; 
       10 7 300 ; 
       10 16 300 ; 
       10 0 300 ; 
       10 18 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       13 33 300 ; 
       13 34 300 ; 
       13 35 300 ; 
       14 19 300 ; 
       14 20 300 ; 
       14 21 300 ; 
       14 22 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       15 28 300 ; 
       16 36 300 ; 
       17 3 300 ; 
       17 6 300 ; 
       17 9 300 ; 
       29 23 300 ; 
       29 24 300 ; 
       29 25 300 ; 
       30 2 300 ; 
       30 5 300 ; 
       30 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       11 39 400 ; 
       15 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       21 17 401 ; 
       22 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       27 21 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 15 401 ; 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       37 43 401 ; 
       38 44 401 ; 
       39 28 401 ; 
       40 29 401 ; 
       41 47 401 ; 
       42 46 401 ; 
       43 30 401 ; 
       44 45 401 ; 
       45 32 401 ; 
       46 48 401 ; 
       47 33 401 ; 
       48 49 401 ; 
       50 50 401 ; 
       51 34 401 ; 
       52 35 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       55 38 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       57 40 401 ; 
       16 0 401 ; 
       58 41 401 ; 
       17 16 401 ; 
       18 4 401 ; 
       59 42 401 ; 
       73 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       6 SCHEM 20 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 25 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 30 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 40 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 45 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 100 -4 0 SRT 1 1 1 -2.842171e-014 0 5.684342e-014 0 0.01075098 0.001670957 MPRFLG 0 ; 
       9 SCHEM 8.75 -20 0 DISPLAY 0 0 SRT 1 1 1 -2.842171e-014 0 5.684342e-014 -8.215585e-006 0.01075098 0.001670957 MPRFLG 0 ; 
       11 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       21 SCHEM 50 -8 0 MPRFLG 0 ; 
       22 SCHEM 63.75 -10 0 MPRFLG 0 ; 
       23 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       24 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       25 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       34 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       35 SCHEM 93.75 -10 0 MPRFLG 0 ; 
       36 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       37 SCHEM 73.75 -10 0 MPRFLG 0 ; 
       38 SCHEM 78.75 -10 0 MPRFLG 0 ; 
       39 SCHEM 83.75 -10 0 MPRFLG 0 ; 
       40 SCHEM 88.75 -10 0 MPRFLG 0 ; 
       41 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       42 SCHEM 22.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       44 SCHEM 35 -12 0 MPRFLG 0 ; 
       45 SCHEM 40 -12 0 MPRFLG 0 ; 
       46 SCHEM 45 -12 0 MPRFLG 0 ; 
       47 SCHEM 50 -12 0 MPRFLG 0 ; 
       48 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       49 SCHEM 25 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 72.5 -12 0 MPRFLG 0 ; 
       53 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       54 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       55 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       58 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       59 SCHEM 101.25 -8 0 MPRFLG 0 ; 
       0 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 173.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 163.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 168.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       7 SCHEM 133.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 100 -6 0 MPRFLG 0 ; 
       12 SCHEM 137.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 147.5 -14 0 MPRFLG 0 ; 
       14 SCHEM 118.75 -14 0 MPRFLG 0 ; 
       15 SCHEM 128.75 -14 0 MPRFLG 0 ; 
       16 SCHEM 135 -14 0 MPRFLG 0 ; 
       17 SCHEM 172.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 162.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 140 -14 0 MPRFLG 0 ; 
       30 SCHEM 10 -8 0 MPRFLG 0 ; 
       31 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       19 SCHEM 122.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 115 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 117.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 120 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 142.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 137.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 140 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 132.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 125 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 127.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 160 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 152.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 155 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 157.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 150 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 145 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 147.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 135 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 182.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 180 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 7.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 10 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 12.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 15 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 175 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 165 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 170 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 17.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 0 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       17 SCHEM 117.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 120 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 137.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 140 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 125 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 130 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 152.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 155 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 157.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 145 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 147.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 0 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 7.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 195 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 200 -10 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 197.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 177.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 180 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 175 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 165 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 170 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 135 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 10 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 12.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 15 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 2.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 90 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 201.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 133 133 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
