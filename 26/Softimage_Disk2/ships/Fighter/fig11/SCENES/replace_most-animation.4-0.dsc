SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig11-fig11.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.9-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 7     
       animation-light1.4-0 ROOT ; 
       animation-light2.4-0 ROOT ; 
       animation-light3.4-0 ROOT ; 
       animation-light4.4-0 ROOT ; 
       animation-light5.4-0 ROOT ; 
       animation-light6.4-0 ROOT ; 
       medium_fighter_sPATL-light6_3_2_4_1_2_2_2_2_2.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 69     
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat66.2-0 ; 
       animation-back1.1-0 ; 
       animation-base.1-0 ; 
       animation-base1.1-0 ; 
       animation-base2.1-0 ; 
       animation-bottom.1-0 ; 
       animation-caution1.1-0 ; 
       animation-caution2.1-0 ; 
       animation-front1.1-0 ; 
       animation-guns1.1-0 ; 
       animation-guns2.1-0 ; 
       animation-mat51.1-0 ; 
       animation-mat52.1-0 ; 
       animation-mat53.1-0 ; 
       animation-mat54.1-0 ; 
       animation-mat55.1-0 ; 
       animation-mat56.1-0 ; 
       animation-sides_and_bottom.1-0 ; 
       animation-top.1-0 ; 
       animation-vents1.1-0 ; 
       medium_fighter_sPATL-mat20.1-0 ; 
       medium_fighter_sPATL-mat21.1-0 ; 
       medium_fighter_sPATL-mat22.1-0 ; 
       medium_fighter_sPATL-mat23.1-0 ; 
       medium_fighter_sPATL-mat24.1-0 ; 
       medium_fighter_sPATL-mat25.1-0 ; 
       medium_fighter_sPATL-mat26.1-0 ; 
       medium_fighter_sPATL-mat27.1-0 ; 
       medium_fighter_sPATL-mat28.1-0 ; 
       medium_fighter_sPATL-mat29.1-0 ; 
       medium_fighter_sPATL-mat30.1-0 ; 
       medium_fighter_sPATL-mat31.1-0 ; 
       medium_fighter_sPATL-mat34.1-0 ; 
       medium_fighter_sPATL-mat38.1-0 ; 
       medium_fighter_sPATL-mat39.1-0 ; 
       medium_fighter_sPATL-mat40.1-0 ; 
       medium_fighter_sPATL-mat41.1-0 ; 
       medium_fighter_sPATL-mat42.1-0 ; 
       medium_fighter_sPATL-mat43.1-0 ; 
       medium_fighter_sPATL-mat44.1-0 ; 
       medium_fighter_sPATL-mat45.1-0 ; 
       medium_fighter_sPATL-mat46.1-0 ; 
       medium_fighter_sPATL-mat47.1-0 ; 
       medium_fighter_sPATL-mat49.1-0 ; 
       medium_fighter_sPATL-mat50.1-0 ; 
       medium_fighter_sPATL-mat51.1-0 ; 
       medium_fighter_sPATL-mat52.1-0 ; 
       medium_fighter_sPATL-mat53.1-0 ; 
       medium_fighter_sPATL-mat54.1-0 ; 
       medium_fighter_sPATL-port_red-left.1-0.1-0 ; 
       medium_fighter_sPATL-starbord_green-right.1-0.1-0 ; 
       medium_fighter_sPATL-white-nose_white-center.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 59     
       fig11-cockpt.1-0 ; 
       fig11-cube1.1-0 ; 
       fig11-cube2.2-0 ; 
       fig11-cube3.2-0 ; 
       fig11-cube4.1-0 ; 
       fig11-cube5.1-0 ; 
       fig11-cube6.1-0 ; 
       fig11-fig08_3_1.1-0 ; 
       fig11-fig11.7-0 ROOT ; 
       fig11-fuselg_1.1-0 ; 
       fig11-hatchz.2-0 ; 
       fig11-LL1.2-0 ; 
       fig11-LLa.1-0 ; 
       fig11-llandgr.1-0 ; 
       fig11-LLl.1-0 ; 
       fig11-LLr.1-0 ; 
       fig11-lwepbar.2-0 ; 
       fig11-lwepemt1.1-0 ; 
       fig11-lwepemt2.1-0 ; 
       fig11-lwepemt3.1-0 ; 
       fig11-lwingzz0.1-0 ; 
       fig11-lwingzz1.1-0 ; 
       fig11-lwingzz2.1-0 ; 
       fig11-lwingzz3.1-0 ; 
       fig11-lwingzz4.1-0 ; 
       fig11-lwingzz5.1-0 ; 
       fig11-lwingzz6.1-0 ; 
       fig11-null1.1-0 ; 
       fig11-rlandgr.1-0 ; 
       fig11-rwepbar_1.1-0 ; 
       fig11-rwepemt1.1-0 ; 
       fig11-rwepemt2.1-0 ; 
       fig11-rwepemt3.1-0 ; 
       fig11-rwingzz0.1-0 ; 
       fig11-rwingzz1.1-0 ; 
       fig11-rwingzz2.1-0 ; 
       fig11-rwingzz3.1-0 ; 
       fig11-rwingzz4.1-0 ; 
       fig11-rwingzz5.1-0 ; 
       fig11-rwingzz6.1-0 ; 
       fig11-SSf.1-0 ; 
       fig11-SSl.1-0 ; 
       fig11-SSl1.1-0 ; 
       fig11-SSl2.1-0 ; 
       fig11-SSl3.1-0 ; 
       fig11-SSl4.1-0 ; 
       fig11-SSl5.1-0 ; 
       fig11-SSl6.1-0 ; 
       fig11-SSr.1-0 ; 
       fig11-SSr1.1-0 ; 
       fig11-SSr2.1-0 ; 
       fig11-SSr3.1-0 ; 
       fig11-SSr4.1-0 ; 
       fig11-SSr5.1-0 ; 
       fig11-SSr6.1-0 ; 
       fig11-thrust.1-0 ; 
       fig11-trail.1-0 ; 
       fig11-wepatt.1-0 ; 
       fig11-wepmnt.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       replace_most-animation.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       animation-t2d44.1-0 ; 
       animation-t2d45.1-0 ; 
       animation-t2d46.1-0 ; 
       animation-t2d47.1-0 ; 
       animation-t2d48.1-0 ; 
       animation-t2d49.1-0 ; 
       animation-t2d50.1-0 ; 
       animation-t2d51.1-0 ; 
       animation-t2d52.1-0 ; 
       animation-t2d53.1-0 ; 
       animation-t2d54.1-0 ; 
       animation-t2d55.1-0 ; 
       animation-t2d56.1-0 ; 
       animation-t2d57.1-0 ; 
       animation-t2d58.1-0 ; 
       animation-t2d59.1-0 ; 
       animation-t2d6.1-0 ; 
       medium_fighter_sPATL-t2d15.1-0 ; 
       medium_fighter_sPATL-t2d16.1-0 ; 
       medium_fighter_sPATL-t2d19.1-0 ; 
       medium_fighter_sPATL-t2d21.1-0 ; 
       medium_fighter_sPATL-t2d23.1-0 ; 
       medium_fighter_sPATL-t2d29.1-0 ; 
       medium_fighter_sPATL-t2d33.1-0 ; 
       medium_fighter_sPATL-t2d34.1-0 ; 
       medium_fighter_sPATL-t2d35.1-0 ; 
       medium_fighter_sPATL-t2d36.1-0 ; 
       med_fig_F-t2d43.1-0 ; 
       med_fig_F-t2d44.1-0 ; 
       med_fig_F-t2d45.1-0 ; 
       med_fig_F-t2d46.1-0 ; 
       med_fig_F-t2d47.1-0 ; 
       med_fig_F-t2d48.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 9 110 ; 
       20 9 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       33 9 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       36 33 110 ; 
       37 33 110 ; 
       38 33 110 ; 
       39 33 110 ; 
       40 9 110 ; 
       41 9 110 ; 
       42 21 110 ; 
       43 22 110 ; 
       44 23 110 ; 
       45 24 110 ; 
       46 25 110 ; 
       47 26 110 ; 
       48 9 110 ; 
       49 34 110 ; 
       50 35 110 ; 
       51 36 110 ; 
       52 37 110 ; 
       53 38 110 ; 
       54 39 110 ; 
       57 58 110 ; 
       58 9 110 ; 
       0 7 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 16 110 ; 
       4 29 110 ; 
       5 29 110 ; 
       6 29 110 ; 
       7 9 110 ; 
       9 8 110 ; 
       11 27 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 9 110 ; 
       17 1 110 ; 
       18 2 110 ; 
       19 3 110 ; 
       27 7 110 ; 
       28 11 110 ; 
       29 9 110 ; 
       30 6 110 ; 
       31 5 110 ; 
       32 4 110 ; 
       55 7 110 ; 
       56 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 50 300 ; 
       21 43 300 ; 
       22 44 300 ; 
       23 45 300 ; 
       24 46 300 ; 
       25 47 300 ; 
       25 49 300 ; 
       26 48 300 ; 
       34 37 300 ; 
       35 38 300 ; 
       36 39 300 ; 
       37 40 300 ; 
       38 41 300 ; 
       39 42 300 ; 
       40 68 300 ; 
       41 66 300 ; 
       42 65 300 ; 
       43 60 300 ; 
       44 61 300 ; 
       45 62 300 ; 
       46 63 300 ; 
       47 64 300 ; 
       48 67 300 ; 
       49 55 300 ; 
       50 56 300 ; 
       51 57 300 ; 
       52 58 300 ; 
       53 59 300 ; 
       54 54 300 ; 
       58 51 300 ; 
       58 52 300 ; 
       58 53 300 ; 
       1 28 300 ; 
       2 29 300 ; 
       3 30 300 ; 
       4 31 300 ; 
       5 32 300 ; 
       6 33 300 ; 
       9 22 300 ; 
       9 35 300 ; 
       9 19 300 ; 
       9 25 300 ; 
       9 34 300 ; 
       9 18 300 ; 
       9 36 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       15 17 300 ; 
       16 21 300 ; 
       16 24 300 ; 
       16 27 300 ; 
       28 4 300 ; 
       28 5 300 ; 
       28 6 300 ; 
       29 20 300 ; 
       29 23 300 ; 
       29 26 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 34 400 ; 
       14 5 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 26 401 ; 
       18 13 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       25 12 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 40 401 ; 
       40 39 401 ; 
       41 30 401 ; 
       42 38 401 ; 
       43 31 401 ; 
       44 41 401 ; 
       45 32 401 ; 
       46 42 401 ; 
       48 43 401 ; 
       49 33 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       51 35 401 ; 
       34 11 401 ; 
       52 36 401 ; 
       35 27 401 ; 
       36 15 401 ; 
       53 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 40 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 45 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 100 -4 0 SRT 1 1 1 -2.842171e-014 0 5.684342e-014 0 0.01075098 0.001670957 MPRFLG 0 ; 
       10 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 50 -8 0 MPRFLG 0 ; 
       21 SCHEM 63.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       22 SCHEM 36.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       23 SCHEM 41.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       24 SCHEM 46.25 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       25 SCHEM 52.5 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       26 SCHEM 58.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       33 SCHEM 81.25 -8 0 MPRFLG 0 ; 
       34 SCHEM 93.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       35 SCHEM 68.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       36 SCHEM 73.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       37 SCHEM 78.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       38 SCHEM 83.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       39 SCHEM 88.75 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       40 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 22.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 35 -12 0 MPRFLG 0 ; 
       44 SCHEM 40 -12 0 MPRFLG 0 ; 
       45 SCHEM 45 -12 0 MPRFLG 0 ; 
       46 SCHEM 50 -12 0 MPRFLG 0 ; 
       47 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 25 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       50 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 72.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       53 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       54 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       57 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       58 SCHEM 101.25 -8 0 MPRFLG 0 ; 
       0 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 173.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 163.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 168.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       7 SCHEM 133.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 100 -6 0 MPRFLG 0 ; 
       11 SCHEM 137.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 147.5 -14 0 MPRFLG 0 ; 
       13 SCHEM 118.75 -14 0 MPRFLG 0 ; 
       14 SCHEM 128.75 -14 0 MPRFLG 0 ; 
       15 SCHEM 135 -14 0 MPRFLG 0 ; 
       16 SCHEM 172.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 162.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 137.5 -10 0 MPRFLG 0 ; 
       28 SCHEM 140 -14 0 MPRFLG 0 ; 
       29 SCHEM 10 -8 0 MPRFLG 0 ; 
       30 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 122.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 115 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 117.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 120 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 142.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 137.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 140 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 132.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 125 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 160 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 152.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 155 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 157.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 150 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 145 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 135 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 182.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 177.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 190 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 180 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 175 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 165 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 170 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 117.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 120 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 137.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 140 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 130 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 152.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 155 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 157.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 145 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 147.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 192.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 190 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 195 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 200 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 197.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 177.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 180 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 175 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 165 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 170 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 135 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 90 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 201.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 128 123 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
