SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       animation-null2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.12-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 7     
       animation-light1.7-0 ROOT ; 
       animation-light2.7-0 ROOT ; 
       animation-light3.7-0 ROOT ; 
       animation-light4.7-0 ROOT ; 
       animation-light5.7-0 ROOT ; 
       animation-light6.7-0 ROOT ; 
       medium_fighter_sPATL-light6_3_2_4_1_2_2_2_2_2.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 69     
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat66.2-0 ; 
       animation-back1.1-0 ; 
       animation-base.1-0 ; 
       animation-base1.1-0 ; 
       animation-base2.1-0 ; 
       animation-bottom.1-0 ; 
       animation-caution1.1-0 ; 
       animation-caution2.1-0 ; 
       animation-front1.1-0 ; 
       animation-guns1.1-0 ; 
       animation-guns2.1-0 ; 
       animation-mat51.1-0 ; 
       animation-mat67.1-0 ; 
       animation-mat68.1-0 ; 
       animation-mat69.1-0 ; 
       animation-mat70.1-0 ; 
       animation-mat71.1-0 ; 
       animation-sides_and_bottom.1-0 ; 
       animation-top.1-0 ; 
       animation-vents1.1-0 ; 
       medium_fighter_sPATL-mat20.1-0 ; 
       medium_fighter_sPATL-mat21.1-0 ; 
       medium_fighter_sPATL-mat22.1-0 ; 
       medium_fighter_sPATL-mat23.1-0 ; 
       medium_fighter_sPATL-mat24.1-0 ; 
       medium_fighter_sPATL-mat25.1-0 ; 
       medium_fighter_sPATL-mat26.1-0 ; 
       medium_fighter_sPATL-mat27.1-0 ; 
       medium_fighter_sPATL-mat28.1-0 ; 
       medium_fighter_sPATL-mat29.1-0 ; 
       medium_fighter_sPATL-mat30.1-0 ; 
       medium_fighter_sPATL-mat31.1-0 ; 
       medium_fighter_sPATL-mat34.1-0 ; 
       medium_fighter_sPATL-mat38.1-0 ; 
       medium_fighter_sPATL-mat39.1-0 ; 
       medium_fighter_sPATL-mat40.1-0 ; 
       medium_fighter_sPATL-mat41.1-0 ; 
       medium_fighter_sPATL-mat42.1-0 ; 
       medium_fighter_sPATL-mat43.1-0 ; 
       medium_fighter_sPATL-mat44.1-0 ; 
       medium_fighter_sPATL-mat45.1-0 ; 
       medium_fighter_sPATL-mat46.1-0 ; 
       medium_fighter_sPATL-mat47.1-0 ; 
       medium_fighter_sPATL-mat49.1-0 ; 
       medium_fighter_sPATL-mat50.1-0 ; 
       medium_fighter_sPATL-mat51.1-0 ; 
       medium_fighter_sPATL-mat52.1-0 ; 
       medium_fighter_sPATL-mat53.1-0 ; 
       medium_fighter_sPATL-mat54.1-0 ; 
       medium_fighter_sPATL-port_red-left.1-0.1-0 ; 
       medium_fighter_sPATL-starbord_green-right.1-0.1-0 ; 
       medium_fighter_sPATL-white-nose_white-center.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       animation-cockpt.1-0 ; 
       animation-cube1.1-0 ; 
       animation-cube10.1-0 ; 
       animation-cube11.1-0 ; 
       animation-cube7.1-0 ; 
       animation-cube8.1-0 ; 
       animation-cube9.1-0 ; 
       animation-fig08_3_1.1-0 ; 
       animation-fig11.8-0 ; 
       animation-fuselg_1.1-0 ; 
       animation-hatchz.2-0 ; 
       animation-LL1.2-0 ; 
       animation-LLa.1-0 ; 
       animation-llandgr.1-0 ; 
       animation-LLl.1-0 ; 
       animation-LLr.1-0 ; 
       animation-lwepbar.2-0 ; 
       animation-lwepemt1.1-0 ; 
       animation-lwepemt2.1-0 ; 
       animation-lwepemt3.1-0 ; 
       animation-lwingzz0.1-0 ; 
       animation-lwingzz1.1-0 ; 
       animation-lwingzz2.1-0 ; 
       animation-lwingzz3.1-0 ; 
       animation-lwingzz4.1-0 ; 
       animation-lwingzz5.1-0 ; 
       animation-lwingzz6.1-0 ; 
       animation-null1.1-0 ; 
       animation-null2.1-0 ROOT ; 
       animation-rlandgr.1-0 ; 
       animation-rwepbar_1.1-0 ; 
       animation-rwepemt1.1-0 ; 
       animation-rwepemt2.1-0 ; 
       animation-rwepemt3.1-0 ; 
       animation-rwingzz0.1-0 ; 
       animation-rwingzz1.1-0 ; 
       animation-rwingzz2.1-0 ; 
       animation-rwingzz3.1-0 ; 
       animation-rwingzz4.1-0 ; 
       animation-rwingzz5.1-0 ; 
       animation-rwingzz6.1-0 ; 
       animation-SSf.1-0 ; 
       animation-SSl.1-0 ; 
       animation-SSl1.1-0 ; 
       animation-SSl2.1-0 ; 
       animation-SSl3.1-0 ; 
       animation-SSl4.1-0 ; 
       animation-SSl5.1-0 ; 
       animation-SSl6.1-0 ; 
       animation-SSr.1-0 ; 
       animation-SSr1.1-0 ; 
       animation-SSr2.1-0 ; 
       animation-SSr3.1-0 ; 
       animation-SSr4.1-0 ; 
       animation-SSr5.1-0 ; 
       animation-SSr6.1-0 ; 
       animation-thrust.1-0 ; 
       animation-trail.1-0 ; 
       animation-wepatt.1-0 ; 
       animation-wepmnt.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig11/PICTURES/fig11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       replace_most-animation.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_gun-t2d18.3-0 ; 
       add_gun-t2d19.3-0 ; 
       add_gun-t2d20.3-0 ; 
       add_gun-t2d21.3-0 ; 
       add_gun-t2d22.3-0 ; 
       add_gun-t2d23.3-0 ; 
       add_gun-t2d25.3-0 ; 
       add_gun-t2d26.3-0 ; 
       add_gun-t2d27.3-0 ; 
       add_gun-t2d31.3-0 ; 
       add_gun-t2d32.3-0 ; 
       animation-t2d44.1-0 ; 
       animation-t2d45.1-0 ; 
       animation-t2d46.1-0 ; 
       animation-t2d47.1-0 ; 
       animation-t2d48.1-0 ; 
       animation-t2d49.1-0 ; 
       animation-t2d50.1-0 ; 
       animation-t2d51.1-0 ; 
       animation-t2d52.1-0 ; 
       animation-t2d53.2-0 ; 
       animation-t2d59.1-0 ; 
       animation-t2d6.1-0 ; 
       animation-t2d60.1-0 ; 
       animation-t2d61.1-0 ; 
       animation-t2d64.1-0 ; 
       animation-t2d65.1-0 ; 
       animation-t2d66.1-0 ; 
       medium_fighter_sPATL-t2d15.1-0 ; 
       medium_fighter_sPATL-t2d16.1-0 ; 
       medium_fighter_sPATL-t2d19.1-0 ; 
       medium_fighter_sPATL-t2d21.1-0 ; 
       medium_fighter_sPATL-t2d23.1-0 ; 
       medium_fighter_sPATL-t2d29.1-0 ; 
       medium_fighter_sPATL-t2d33.1-0 ; 
       medium_fighter_sPATL-t2d34.1-0 ; 
       medium_fighter_sPATL-t2d35.1-0 ; 
       medium_fighter_sPATL-t2d36.1-0 ; 
       med_fig_F-t2d43.1-0 ; 
       med_fig_F-t2d44.1-0 ; 
       med_fig_F-t2d45.1-0 ; 
       med_fig_F-t2d46.1-0 ; 
       med_fig_F-t2d47.1-0 ; 
       med_fig_F-t2d48.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 28 110 ; 
       10 9 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 30 110 ; 
       20 9 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       31 6 110 ; 
       2 30 110 ; 
       32 2 110 ; 
       3 30 110 ; 
       34 9 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 34 110 ; 
       38 34 110 ; 
       39 34 110 ; 
       40 34 110 ; 
       41 9 110 ; 
       42 9 110 ; 
       43 21 110 ; 
       44 22 110 ; 
       45 23 110 ; 
       46 24 110 ; 
       47 25 110 ; 
       48 26 110 ; 
       49 9 110 ; 
       50 35 110 ; 
       51 36 110 ; 
       52 37 110 ; 
       53 38 110 ; 
       54 39 110 ; 
       55 40 110 ; 
       58 59 110 ; 
       59 9 110 ; 
       33 3 110 ; 
       0 7 110 ; 
       1 16 110 ; 
       7 9 110 ; 
       9 8 110 ; 
       11 27 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 9 110 ; 
       17 1 110 ; 
       18 4 110 ; 
       19 5 110 ; 
       27 7 110 ; 
       29 11 110 ; 
       30 9 110 ; 
       56 7 110 ; 
       57 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 50 300 ; 
       4 29 300 ; 
       5 30 300 ; 
       6 31 300 ; 
       21 43 300 ; 
       22 44 300 ; 
       23 45 300 ; 
       24 46 300 ; 
       25 47 300 ; 
       25 49 300 ; 
       26 48 300 ; 
       2 32 300 ; 
       3 33 300 ; 
       35 37 300 ; 
       36 38 300 ; 
       37 39 300 ; 
       38 40 300 ; 
       39 41 300 ; 
       40 42 300 ; 
       41 68 300 ; 
       42 66 300 ; 
       43 65 300 ; 
       44 60 300 ; 
       45 61 300 ; 
       46 62 300 ; 
       47 63 300 ; 
       48 64 300 ; 
       49 67 300 ; 
       50 55 300 ; 
       51 56 300 ; 
       52 57 300 ; 
       53 58 300 ; 
       54 59 300 ; 
       55 54 300 ; 
       59 51 300 ; 
       59 52 300 ; 
       59 53 300 ; 
       1 28 300 ; 
       9 22 300 ; 
       9 35 300 ; 
       9 19 300 ; 
       9 25 300 ; 
       9 34 300 ; 
       9 18 300 ; 
       9 36 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 0 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       15 17 300 ; 
       16 21 300 ; 
       16 24 300 ; 
       16 27 300 ; 
       29 4 300 ; 
       29 5 300 ; 
       29 6 300 ; 
       30 20 300 ; 
       30 23 300 ; 
       30 26 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 34 400 ; 
       14 5 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       28 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       8 4 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 21 401 ; 
       18 13 401 ; 
       22 14 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       25 12 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 40 401 ; 
       40 39 401 ; 
       41 30 401 ; 
       42 38 401 ; 
       43 31 401 ; 
       44 41 401 ; 
       45 32 401 ; 
       46 42 401 ; 
       48 43 401 ; 
       32 26 401 ; 
       49 33 401 ; 
       33 27 401 ; 
       28 20 401 ; 
       51 35 401 ; 
       34 11 401 ; 
       52 36 401 ; 
       35 22 401 ; 
       36 15 401 ; 
       53 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 10 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 15 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       28 SCHEM 37.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 75 -12 0 MPRFLG 0 ; 
       6 SCHEM 0 -12 0 MPRFLG 0 ; 
       20 SCHEM 23.75 -10 0 MPRFLG 0 ; 
       21 SCHEM 30 -12 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       25 SCHEM 25 -12 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       31 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       32 SCHEM 2.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -12 0 MPRFLG 0 ; 
       34 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       35 SCHEM 45 -12 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       37 SCHEM 35 -12 0 MPRFLG 0 ; 
       38 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       39 SCHEM 40 -12 0 MPRFLG 0 ; 
       40 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       41 SCHEM 15 -10 0 MPRFLG 0 ; 
       42 SCHEM 7.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 30 -14 0 MPRFLG 0 ; 
       44 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       45 SCHEM 20 -14 0 MPRFLG 0 ; 
       46 SCHEM 22.5 -14 0 MPRFLG 0 ; 
       47 SCHEM 25 -14 0 MPRFLG 0 ; 
       48 SCHEM 27.5 -14 0 MPRFLG 0 ; 
       49 SCHEM 10 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 45 -14 0 MPRFLG 0 ; 
       51 SCHEM 32.5 -14 0 MPRFLG 0 ; 
       52 SCHEM 35 -14 0 MPRFLG 0 ; 
       53 SCHEM 37.5 -14 0 MPRFLG 0 ; 
       54 SCHEM 40 -14 0 MPRFLG 0 ; 
       55 SCHEM 42.5 -14 0 MPRFLG 0 ; 
       58 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       59 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       33 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 70 -12 0 MPRFLG 0 ; 
       7 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -14 0 MPRFLG 0 ; 
       12 SCHEM 67.5 -16 0 MPRFLG 0 ; 
       13 SCHEM 57.5 -16 0 MPRFLG 0 ; 
       14 SCHEM 60 -16 0 MPRFLG 0 ; 
       15 SCHEM 62.5 -16 0 MPRFLG 0 ; 
       16 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 65 -16 0 MPRFLG 0 ; 
       30 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       56 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 64 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 69 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 69 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 69 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 74 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 76.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 71.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 76.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 49 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 41.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 44 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 31.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 34 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 36.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 39 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 24 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 29 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 56.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 64 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 64 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 69 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 69 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 69 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 66.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 74 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 76.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 46.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 76.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 76.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 71.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 76.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 49 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 44 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 39 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 36.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 29 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 76.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 128 128 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
