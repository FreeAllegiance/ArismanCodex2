SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.30-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.30-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.30-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       re_textur-light1.24-0 ROOT ; 
       re_textur-light2.24-0 ROOT ; 
       re_textur-light3.24-0 ROOT ; 
       re_textur-light4.24-0 ROOT ; 
       re_textur-light5.24-0 ROOT ; 
       re_textur-light6.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_gun-front1.1-0 ; 
       add_gun-mat91.1-0 ; 
       add_gun-mat92.1-0 ; 
       add_gun-sides1.1-0 ; 
       re_textur-default1_1.4-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.4-0 ; 
       re_textur-mat41_1.4-0 ; 
       re_textur-mat42_1.4-0 ; 
       re_textur-mat43_1.4-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat57_1.1-0 ; 
       re_textur-mat58_1.1-0 ; 
       re_textur-mat59_1.1-0 ; 
       re_textur-mat60_1.1-0 ; 
       re_textur-mat61_1.1-0 ; 
       re_textur-mat62_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat67_1.1-0 ; 
       re_textur-mat68_1.1-0 ; 
       re_textur-mat69_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.4-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.4-0 ; 
       re_textur-mat83_1.4-0 ; 
       re_textur-mat84_1.4-0 ; 
       re_textur-mat85_1.4-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig12-awepatt.1-0 ; 
       fig12-bwepemt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-cube1.1-0 ; 
       fig12-cyl1.1-0 ; 
       fig12-fig12.23-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-lwepemt1.1-0 ; 
       fig12-lwepemt2.1-0 ; 
       fig12-missemt.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-add_gun.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       add_gun-t2d68.1-0 ; 
       add_gun-t2d69.1-0 ; 
       add_gun-t2d70.1-0 ; 
       add_gun-t2d71.1-0 ; 
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.4-0 ; 
       re_textur-t2d25_1.4-0 ; 
       re_textur-t2d26_1.4-0 ; 
       re_textur-t2d27_1.4-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d38_1.1-0 ; 
       re_textur-t2d39_1.1-0 ; 
       re_textur-t2d40_1.1-0 ; 
       re_textur-t2d41_1.1-0 ; 
       re_textur-t2d42_1.1-0 ; 
       re_textur-t2d43_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d48_1.1-0 ; 
       re_textur-t2d49_1.1-0 ; 
       re_textur-t2d50_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.5-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.4-0 ; 
       re_textur-t2d65_1.4-0 ; 
       re_textur-t2d66_1.4-0 ; 
       re_textur-t2d67_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 46 110 ; 
       4 3 110 ; 
       0 46 110 ; 
       1 31 110 ; 
       2 7 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 47 110 ; 
       11 7 110 ; 
       12 47 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       15 21 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 21 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 5 110 ; 
       22 21 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 47 110 ; 
       26 47 110 ; 
       27 25 110 ; 
       28 26 110 ; 
       29 13 110 ; 
       30 47 110 ; 
       31 30 110 ; 
       32 6 110 ; 
       33 47 110 ; 
       34 47 110 ; 
       35 33 110 ; 
       36 34 110 ; 
       37 9 110 ; 
       38 8 110 ; 
       39 7 110 ; 
       40 6 110 ; 
       41 7 110 ; 
       42 6 110 ; 
       43 7 110 ; 
       44 7 110 ; 
       45 47 110 ; 
       46 45 110 ; 
       47 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 1 300 ; 
       4 2 300 ; 
       6 5 300 ; 
       7 4 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 47 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       12 45 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       14 9 300 ; 
       15 38 300 ; 
       16 39 300 ; 
       17 40 300 ; 
       18 28 300 ; 
       19 29 300 ; 
       20 30 300 ; 
       22 31 300 ; 
       23 32 300 ; 
       24 33 300 ; 
       25 37 300 ; 
       26 36 300 ; 
       30 46 300 ; 
       31 23 300 ; 
       31 24 300 ; 
       31 25 300 ; 
       31 26 300 ; 
       31 27 300 ; 
       31 41 300 ; 
       32 44 300 ; 
       33 13 300 ; 
       33 34 300 ; 
       34 35 300 ; 
       37 55 300 ; 
       38 54 300 ; 
       39 51 300 ; 
       40 52 300 ; 
       41 56 300 ; 
       42 53 300 ; 
       45 43 300 ; 
       46 42 300 ; 
       46 3 300 ; 
       46 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 4 401 ; 
       3 0 401 ; 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 9 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 36 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 40 401 ; 
       44 39 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       50 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 98.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 97.5 -12 0 MPRFLG 0 ; 
       0 SCHEM 95 -10 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 85 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 142.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 133.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 147.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 85 -6 0 MPRFLG 0 ; 
       13 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 20 -8 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 30 -6 0 MPRFLG 0 ; 
       23 SCHEM 30 -8 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       31 SCHEM 65 -8 0 MPRFLG 0 ; 
       32 SCHEM 5 -6 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       35 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 130 -8 0 MPRFLG 0 ; 
       38 SCHEM 120 -8 0 MPRFLG 0 ; 
       39 SCHEM 115 -6 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 0 -6 0 MPRFLG 0 ; 
       43 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       46 SCHEM 101.25 -8 0 MPRFLG 0 ; 
       47 SCHEM 73.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 172.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 177.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 152.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 155 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 167.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 170 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 171.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 108 16 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
