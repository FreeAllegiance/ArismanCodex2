SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.56-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.56-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.56-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.56-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.56-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       scaled-light1.2-0 ROOT ; 
       scaled-light2.2-0 ROOT ; 
       scaled-light3.2-0 ROOT ; 
       scaled-light4.2-0 ROOT ; 
       scaled-light5.2-0 ROOT ; 
       scaled-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       re_textur-default1_1.4-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.4-0 ; 
       re_textur-mat41_1.4-0 ; 
       re_textur-mat42_1.4-0 ; 
       re_textur-mat43_1.4-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.4-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.4-0 ; 
       re_textur-mat83_1.4-0 ; 
       re_textur-mat84_1.4-0 ; 
       re_textur-mat85_1.4-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
       scaled-front1.1-0 ; 
       scaled-mat91.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-sides1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       fig12-awepatt.1-0 ; 
       fig12-bwepemt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-cube1.1-0 ; 
       fig12-cyl1.1-0 ; 
       fig12-fig12.43-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-lthrust.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-lwepemt1.1-0 ; 
       fig12-lwepemt2.1-0 ; 
       fig12-missemt.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-Rthrust.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-smoke.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-add_more_thrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       re_textur-t2d1_1.3-0 ; 
       re_textur-t2d16_1.3-0 ; 
       re_textur-t2d17_1.3-0 ; 
       re_textur-t2d18_1.3-0 ; 
       re_textur-t2d19_1.3-0 ; 
       re_textur-t2d2_1.3-0 ; 
       re_textur-t2d20_1.3-0 ; 
       re_textur-t2d21_1.3-0 ; 
       re_textur-t2d22_1.6-0 ; 
       re_textur-t2d25_1.6-0 ; 
       re_textur-t2d26_1.6-0 ; 
       re_textur-t2d27_1.6-0 ; 
       re_textur-t2d29_1.3-0 ; 
       re_textur-t2d30_1.3-0 ; 
       re_textur-t2d31_1.3-0 ; 
       re_textur-t2d32_1.3-0 ; 
       re_textur-t2d33_1.3-0 ; 
       re_textur-t2d34_1.3-0 ; 
       re_textur-t2d35_1.3-0 ; 
       re_textur-t2d44_1.3-0 ; 
       re_textur-t2d45_1.3-0 ; 
       re_textur-t2d46_1.3-0 ; 
       re_textur-t2d47_1.3-0 ; 
       re_textur-t2d51_1.3-0 ; 
       re_textur-t2d52_1.3-0 ; 
       re_textur-t2d53_1.7-0 ; 
       re_textur-t2d54_1.3-0 ; 
       re_textur-t2d56_1.3-0 ; 
       re_textur-t2d58_1.3-0 ; 
       re_textur-t2d59_1.3-0 ; 
       re_textur-t2d62_1.6-0 ; 
       re_textur-t2d65_1.6-0 ; 
       re_textur-t2d66_1.6-0 ; 
       re_textur-t2d67_1.6-0 ; 
       scaled-t2d68.2-0 ; 
       scaled-t2d69.2-0 ; 
       scaled-t2d70.2-0 ; 
       scaled-t2d71.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 39 110 ; 
       1 22 110 ; 
       2 5 110 ; 
       3 39 110 ; 
       4 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 40 110 ; 
       11 7 110 ; 
       12 40 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       16 40 110 ; 
       17 40 110 ; 
       18 16 110 ; 
       19 17 110 ; 
       20 13 110 ; 
       21 40 110 ; 
       22 21 110 ; 
       23 6 110 ; 
       25 40 110 ; 
       26 40 110 ; 
       27 25 110 ; 
       28 26 110 ; 
       29 5 110 ; 
       30 9 110 ; 
       31 8 110 ; 
       32 7 110 ; 
       33 6 110 ; 
       34 7 110 ; 
       35 6 110 ; 
       36 5 110 ; 
       37 5 110 ; 
       38 40 110 ; 
       39 38 110 ; 
       40 5 110 ; 
       15 5 110 ; 
       24 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 45 300 ; 
       4 46 300 ; 
       6 1 300 ; 
       7 0 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       12 32 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       14 5 300 ; 
       16 27 300 ; 
       17 26 300 ; 
       21 33 300 ; 
       22 19 300 ; 
       22 20 300 ; 
       22 21 300 ; 
       22 22 300 ; 
       22 23 300 ; 
       22 28 300 ; 
       23 31 300 ; 
       25 9 300 ; 
       25 24 300 ; 
       26 25 300 ; 
       30 42 300 ; 
       31 41 300 ; 
       32 38 300 ; 
       33 39 300 ; 
       34 43 300 ; 
       35 40 300 ; 
       38 30 300 ; 
       39 29 300 ; 
       39 47 300 ; 
       39 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 23 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 27 401 ; 
       31 26 401 ; 
       32 28 401 ; 
       33 29 401 ; 
       34 30 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       44 35 401 ; 
       45 36 401 ; 
       46 37 401 ; 
       47 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 65 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 70 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 72.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 75 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 77.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -8 0 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 33.75 0 0 SRT 1 1 1 0 0 0 0 0 -0.09168417 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 MPRFLG 0 ; 
       10 SCHEM 35 -4 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 20 -4 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25 -4 0 MPRFLG 0 ; 
       22 SCHEM 25 -6 0 MPRFLG 0 ; 
       23 SCHEM 10 -4 0 MPRFLG 0 ; 
       25 SCHEM 15 -4 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45 -6 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 40 -4 0 MPRFLG 0 ; 
       35 SCHEM 5 -4 0 MPRFLG 0 ; 
       36 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       39 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       40 SCHEM 25 -2 0 MPRFLG 0 ; 
       15 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       24 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
