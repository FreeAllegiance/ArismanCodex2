SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.44-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.52-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.52-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.52-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.52-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.52-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_smoke-light1.3-0 ROOT ; 
       add_smoke-light2.3-0 ROOT ; 
       add_smoke-light3.3-0 ROOT ; 
       add_smoke-light4.3-0 ROOT ; 
       add_smoke-light5.3-0 ROOT ; 
       add_smoke-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       add_smoke-front1.1-0 ; 
       add_smoke-mat91.1-0 ; 
       add_smoke-mat92.1-0 ; 
       add_smoke-sides1.1-0 ; 
       re_textur-default1_1.4-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.4-0 ; 
       re_textur-mat41_1.4-0 ; 
       re_textur-mat42_1.4-0 ; 
       re_textur-mat43_1.4-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.4-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.4-0 ; 
       re_textur-mat83_1.4-0 ; 
       re_textur-mat84_1.4-0 ; 
       re_textur-mat85_1.4-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       fig12-awepatt.1-0 ; 
       fig12-bwepemt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-cube1.1-0 ; 
       fig12-cyl1.1-0 ; 
       fig12-fig12.39-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-lwepemt1.1-0 ; 
       fig12-lwepemt2.1-0 ; 
       fig12-missemt.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-smoke.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-add_smoke.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       add_smoke-t2d68.1-0 ; 
       add_smoke-t2d69.1-0 ; 
       add_smoke-t2d70.1-0 ; 
       add_smoke-t2d71.1-0 ; 
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.4-0 ; 
       re_textur-t2d25_1.4-0 ; 
       re_textur-t2d26_1.4-0 ; 
       re_textur-t2d27_1.4-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.5-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.4-0 ; 
       re_textur-t2d65_1.4-0 ; 
       re_textur-t2d66_1.4-0 ; 
       re_textur-t2d67_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 37 110 ; 
       1 21 110 ; 
       2 5 110 ; 
       3 37 110 ; 
       4 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 38 110 ; 
       11 7 110 ; 
       12 38 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       15 38 110 ; 
       16 38 110 ; 
       17 15 110 ; 
       18 16 110 ; 
       19 13 110 ; 
       20 38 110 ; 
       21 20 110 ; 
       22 6 110 ; 
       23 38 110 ; 
       24 38 110 ; 
       25 23 110 ; 
       26 24 110 ; 
       27 5 110 ; 
       28 9 110 ; 
       29 8 110 ; 
       30 7 110 ; 
       31 6 110 ; 
       32 7 110 ; 
       33 6 110 ; 
       34 5 110 ; 
       35 5 110 ; 
       36 38 110 ; 
       37 36 110 ; 
       38 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 1 300 ; 
       4 2 300 ; 
       6 5 300 ; 
       7 4 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       12 36 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       13 21 300 ; 
       13 22 300 ; 
       14 9 300 ; 
       15 31 300 ; 
       16 30 300 ; 
       20 37 300 ; 
       21 23 300 ; 
       21 24 300 ; 
       21 25 300 ; 
       21 26 300 ; 
       21 27 300 ; 
       21 32 300 ; 
       22 35 300 ; 
       23 13 300 ; 
       23 28 300 ; 
       24 29 300 ; 
       28 46 300 ; 
       29 45 300 ; 
       30 42 300 ; 
       31 43 300 ; 
       32 47 300 ; 
       33 44 300 ; 
       36 34 300 ; 
       37 33 300 ; 
       37 3 300 ; 
       37 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 0 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 9 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 27 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 28 401 ; 
       33 29 401 ; 
       34 31 401 ; 
       35 30 401 ; 
       36 32 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -8 0 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 31.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 MPRFLG 0 ; 
       10 SCHEM 35 -4 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 25 -4 0 MPRFLG 0 ; 
       21 SCHEM 25 -6 0 MPRFLG 0 ; 
       22 SCHEM 10 -4 0 MPRFLG 0 ; 
       23 SCHEM 15 -4 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -6 0 MPRFLG 0 ; 
       29 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 40 -4 0 MPRFLG 0 ; 
       33 SCHEM 5 -4 0 MPRFLG 0 ; 
       34 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       38 SCHEM 25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
