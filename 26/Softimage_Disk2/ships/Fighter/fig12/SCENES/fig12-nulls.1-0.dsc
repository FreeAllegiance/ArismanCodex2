SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.14-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       re_textur-light1.8-0 ROOT ; 
       re_textur-light2.8-0 ROOT ; 
       re_textur-light3.8-0 ROOT ; 
       re_textur-light4.8-0 ROOT ; 
       re_textur-light5.8-0 ROOT ; 
       re_textur-light6.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 59     
       re_textur-default1_1.3-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat11_1.1-0 ; 
       re_textur-mat12_1.1-0 ; 
       re_textur-mat13_1.1-0 ; 
       re_textur-mat14_1.1-0 ; 
       re_textur-mat15_1.1-0 ; 
       re_textur-mat16_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.3-0 ; 
       re_textur-mat41_1.3-0 ; 
       re_textur-mat42_1.3-0 ; 
       re_textur-mat43_1.3-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat57_1.1-0 ; 
       re_textur-mat58_1.1-0 ; 
       re_textur-mat59_1.1-0 ; 
       re_textur-mat60_1.1-0 ; 
       re_textur-mat61_1.1-0 ; 
       re_textur-mat62_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat67_1.1-0 ; 
       re_textur-mat68_1.1-0 ; 
       re_textur-mat69_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.1-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.3-0 ; 
       re_textur-mat83_1.3-0 ; 
       re_textur-mat84_1.3-0 ; 
       re_textur-mat85_1.3-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 49     
       fig12-awepatt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-fig12.11-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lbwepemt.1-0 ; 
       fig12-lengine.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-mengine.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rbwepemt.1-0 ; 
       fig12-rengine.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-rwepemt3.1-0 ; 
       fig12-rwepemt4.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 49     
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d10_1.1-0 ; 
       re_textur-t2d11_1.1-0 ; 
       re_textur-t2d12_1.1-0 ; 
       re_textur-t2d13_1.1-0 ; 
       re_textur-t2d14_1.1-0 ; 
       re_textur-t2d15_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.4-0 ; 
       re_textur-t2d25_1.4-0 ; 
       re_textur-t2d26_1.4-0 ; 
       re_textur-t2d27_1.4-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d38_1.1-0 ; 
       re_textur-t2d39_1.1-0 ; 
       re_textur-t2d40_1.1-0 ; 
       re_textur-t2d41_1.1-0 ; 
       re_textur-t2d42_1.1-0 ; 
       re_textur-t2d43_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d48_1.1-0 ; 
       re_textur-t2d49_1.1-0 ; 
       re_textur-t2d50_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.1-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.4-0 ; 
       re_textur-t2d65_1.4-0 ; 
       re_textur-t2d66_1.4-0 ; 
       re_textur-t2d67_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 47 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 48 110 ; 
       8 4 110 ; 
       9 48 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 4 110 ; 
       13 3 110 ; 
       14 20 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 20 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 2 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 48 110 ; 
       25 48 110 ; 
       26 4 110 ; 
       27 48 110 ; 
       28 27 110 ; 
       29 28 110 ; 
       30 4 110 ; 
       31 3 110 ; 
       32 48 110 ; 
       33 48 110 ; 
       34 32 110 ; 
       35 33 110 ; 
       36 25 110 ; 
       37 24 110 ; 
       38 6 110 ; 
       39 5 110 ; 
       40 4 110 ; 
       41 3 110 ; 
       42 4 110 ; 
       43 3 110 ; 
       45 4 110 ; 
       1 4 110 ; 
       44 4 110 ; 
       46 48 110 ; 
       47 46 110 ; 
       48 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 1 300 ; 
       4 0 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 49 300 ; 
       4 50 300 ; 
       4 51 300 ; 
       4 52 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       9 47 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 24 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       13 11 300 ; 
       14 40 300 ; 
       15 41 300 ; 
       16 42 300 ; 
       17 30 300 ; 
       18 31 300 ; 
       19 32 300 ; 
       21 33 300 ; 
       22 34 300 ; 
       23 35 300 ; 
       24 39 300 ; 
       25 38 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       27 48 300 ; 
       28 25 300 ; 
       28 26 300 ; 
       28 27 300 ; 
       28 28 300 ; 
       28 29 300 ; 
       28 43 300 ; 
       30 2 300 ; 
       30 3 300 ; 
       31 46 300 ; 
       32 15 300 ; 
       32 36 300 ; 
       33 37 300 ; 
       38 57 300 ; 
       39 56 300 ; 
       40 53 300 ; 
       41 54 300 ; 
       42 58 300 ; 
       43 55 300 ; 
       46 45 300 ; 
       47 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 38 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 42 401 ; 
       46 41 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       50 46 401 ; 
       51 47 401 ; 
       52 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 -10 0 MPRFLG 0 ; 
       2 SCHEM 35 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 70 -6 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 60 -6 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       17 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 15 -8 0 MPRFLG 0 ; 
       20 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 20 -8 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 30 -6 0 MPRFLG 0 ; 
       26 SCHEM 55 -6 0 MPRFLG 0 ; 
       27 SCHEM 35 -6 0 MPRFLG 0 ; 
       28 SCHEM 35 -8 0 MPRFLG 0 ; 
       29 SCHEM 35 -10 0 MPRFLG 0 ; 
       30 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 5 -6 0 MPRFLG 0 ; 
       32 SCHEM 25 -6 0 MPRFLG 0 ; 
       33 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 25 -8 0 MPRFLG 0 ; 
       35 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 30 -8 0 MPRFLG 0 ; 
       37 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 50 -8 0 MPRFLG 0 ; 
       40 SCHEM 45 -6 0 MPRFLG 0 ; 
       41 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       43 SCHEM 0 -6 0 MPRFLG 0 ; 
       45 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -6 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       44 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 40 -6 0 MPRFLG 0 ; 
       47 SCHEM 40 -8 0 MPRFLG 0 ; 
       48 SCHEM 33.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 71.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 108 108 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
