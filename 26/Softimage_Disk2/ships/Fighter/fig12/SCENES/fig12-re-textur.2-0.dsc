SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       re_textur-light1.2-0 ROOT ; 
       re_textur-light2.2-0 ROOT ; 
       re_textur-light3.2-0 ROOT ; 
       re_textur-light4.2-0 ROOT ; 
       re_textur-light5.2-0 ROOT ; 
       re_textur-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       re_textur-default1_1.2-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat11_1.1-0 ; 
       re_textur-mat12_1.1-0 ; 
       re_textur-mat13_1.1-0 ; 
       re_textur-mat14_1.1-0 ; 
       re_textur-mat15_1.1-0 ; 
       re_textur-mat16_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.2-0 ; 
       re_textur-mat39_1.2-0 ; 
       re_textur-mat41_1.2-0 ; 
       re_textur-mat42_1.2-0 ; 
       re_textur-mat43_1.2-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat57_1.1-0 ; 
       re_textur-mat58_1.1-0 ; 
       re_textur-mat59_1.1-0 ; 
       re_textur-mat60_1.1-0 ; 
       re_textur-mat61_1.1-0 ; 
       re_textur-mat62_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat67_1.1-0 ; 
       re_textur-mat68_1.1-0 ; 
       re_textur-mat69_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.1-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.2-0 ; 
       re_textur-mat83_1.2-0 ; 
       re_textur-mat84_1.2-0 ; 
       re_textur-mat85_1.2-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       fig12-awepatt.1-0 ; 
       fig12-fig12.7-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lbwepemt.1-0 ; 
       fig12-lengine.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-mengine.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rbwepemt.1-0 ; 
       fig12-rengine.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-rwepemt3.1-0 ; 
       fig12-rwepemt4.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-re-textur.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d10_1.1-0 ; 
       re_textur-t2d11_1.1-0 ; 
       re_textur-t2d12_1.1-0 ; 
       re_textur-t2d13_1.1-0 ; 
       re_textur-t2d14_1.1-0 ; 
       re_textur-t2d15_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.2-0 ; 
       re_textur-t2d23_1.2-0 ; 
       re_textur-t2d25_1.2-0 ; 
       re_textur-t2d26_1.2-0 ; 
       re_textur-t2d27_1.2-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d38_1.1-0 ; 
       re_textur-t2d39_1.1-0 ; 
       re_textur-t2d40_1.1-0 ; 
       re_textur-t2d41_1.1-0 ; 
       re_textur-t2d42_1.1-0 ; 
       re_textur-t2d43_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d48_1.1-0 ; 
       re_textur-t2d49_1.1-0 ; 
       re_textur-t2d50_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.1-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.2-0 ; 
       re_textur-t2d65_1.2-0 ; 
       re_textur-t2d66_1.2-0 ; 
       re_textur-t2d67_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 44 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 45 110 ; 
       7 3 110 ; 
       8 45 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 3 110 ; 
       12 2 110 ; 
       13 19 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 19 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 1 110 ; 
       20 19 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 45 110 ; 
       24 45 110 ; 
       25 3 110 ; 
       26 45 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 3 110 ; 
       30 2 110 ; 
       31 45 110 ; 
       32 45 110 ; 
       33 31 110 ; 
       34 32 110 ; 
       35 24 110 ; 
       36 23 110 ; 
       37 5 110 ; 
       38 4 110 ; 
       39 3 110 ; 
       40 2 110 ; 
       41 3 110 ; 
       42 2 110 ; 
       43 45 110 ; 
       44 43 110 ; 
       45 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 1 300 ; 
       3 0 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 50 300 ; 
       3 51 300 ; 
       3 52 300 ; 
       3 53 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       8 48 300 ; 
       9 21 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       12 11 300 ; 
       13 41 300 ; 
       14 42 300 ; 
       15 43 300 ; 
       16 31 300 ; 
       17 32 300 ; 
       18 33 300 ; 
       20 34 300 ; 
       21 35 300 ; 
       22 36 300 ; 
       23 40 300 ; 
       24 39 300 ; 
       25 4 300 ; 
       25 5 300 ; 
       26 49 300 ; 
       27 26 300 ; 
       27 27 300 ; 
       27 28 300 ; 
       27 29 300 ; 
       27 30 300 ; 
       27 44 300 ; 
       29 2 300 ; 
       29 3 300 ; 
       30 47 300 ; 
       31 15 300 ; 
       31 37 300 ; 
       32 38 300 ; 
       37 58 300 ; 
       38 57 300 ; 
       39 54 300 ; 
       40 55 300 ; 
       41 59 300 ; 
       42 56 300 ; 
       43 46 300 ; 
       44 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 39 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 32 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       43 38 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 43 401 ; 
       47 42 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       50 46 401 ; 
       51 47 401 ; 
       52 48 401 ; 
       53 49 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 190 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 192.5 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 195 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 197.5 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 200 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 202.5 0.9559417 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 95 -10 0 MPRFLG 0 ; 
       1 SCHEM 93.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 169.2544 0.9559417 0 USR MPRFLG 0 ; 
       4 SCHEM 113.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 133.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 145 -6 0 MPRFLG 0 ; 
       8 SCHEM 85 -6 0 MPRFLG 0 ; 
       9 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 141.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 20 -8 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 30 -6 0 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 126.25 -6 0 MPRFLG 0 ; 
       26 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       27 SCHEM 65 -8 0 MPRFLG 0 ; 
       28 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 121.25 -6 0 MPRFLG 0 ; 
       30 SCHEM 5 -6 0 MPRFLG 0 ; 
       31 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 130 -8 0 MPRFLG 0 ; 
       38 SCHEM 110 -8 0 MPRFLG 0 ; 
       39 SCHEM 105 -6 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 0 -6 0 MPRFLG 0 ; 
       43 SCHEM 97.5 -6 0 MPRFLG 0 ; 
       44 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       45 SCHEM 68.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 175 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 182.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 185 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 155 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 175 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 182.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 185 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 187.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 211.9795 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 351.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 351.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 351.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 108 29 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
