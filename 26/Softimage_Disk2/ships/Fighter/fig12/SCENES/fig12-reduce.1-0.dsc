SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       destroy-inf_light2_1_3_1_2_1_1.3-0 ROOT ; 
       destroy-inf_light4_1_3_1_2_1_1.3-0 ROOT ; 
       il_heavyfighter_sAT-spot1.1-0 ; 
       il_heavyfighter_sAT-spot1_int.3-0 ROOT ; 
       reduce-inf_light4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 81     
       reduce-default1_1.1-0 ; 
       reduce-default2_1.1-0 ; 
       reduce-mat1_1.1-0 ; 
       reduce-mat11_1.1-0 ; 
       reduce-mat12_1.1-0 ; 
       reduce-mat13_1.1-0 ; 
       reduce-mat14_1.1-0 ; 
       reduce-mat15_1.1-0 ; 
       reduce-mat16_1.1-0 ; 
       reduce-mat17_1.1-0 ; 
       reduce-mat18_1.1-0 ; 
       reduce-mat19_1.1-0 ; 
       reduce-mat2_1.1-0 ; 
       reduce-mat20_1.1-0 ; 
       reduce-mat21_1.1-0 ; 
       reduce-mat22_1.1-0 ; 
       reduce-mat26_1.1-0 ; 
       reduce-mat27_1.1-0 ; 
       reduce-mat28_1.1-0 ; 
       reduce-mat29_1.1-0 ; 
       reduce-mat30_1.1-0 ; 
       reduce-mat31_1.1-0 ; 
       reduce-mat32_1.1-0 ; 
       reduce-mat33_1.1-0 ; 
       reduce-mat34_1.1-0 ; 
       reduce-mat35_1.1-0 ; 
       reduce-mat36_1.1-0 ; 
       reduce-mat37_1.1-0 ; 
       reduce-mat38_1.1-0 ; 
       reduce-mat39_1.1-0 ; 
       reduce-mat40_1.1-0 ; 
       reduce-mat41_1.1-0 ; 
       reduce-mat42_1.1-0 ; 
       reduce-mat43_1.1-0 ; 
       reduce-mat44_1.1-0 ; 
       reduce-mat45_1.1-0 ; 
       reduce-mat46_1.1-0 ; 
       reduce-mat47_1.1-0 ; 
       reduce-mat48_1.1-0 ; 
       reduce-mat49_1.1-0 ; 
       reduce-mat50_1.1-0 ; 
       reduce-mat51_1.1-0 ; 
       reduce-mat52_1.1-0 ; 
       reduce-mat53_1.1-0 ; 
       reduce-mat54_1.1-0 ; 
       reduce-mat55_1.1-0 ; 
       reduce-mat56_1.1-0 ; 
       reduce-mat57_1.1-0 ; 
       reduce-mat58_1.1-0 ; 
       reduce-mat59_1.1-0 ; 
       reduce-mat60_1.1-0 ; 
       reduce-mat61_1.1-0 ; 
       reduce-mat62_1.1-0 ; 
       reduce-mat63_1.1-0 ; 
       reduce-mat64_1.1-0 ; 
       reduce-mat65_1.1-0 ; 
       reduce-mat66_1.1-0 ; 
       reduce-mat67_1.1-0 ; 
       reduce-mat68_1.1-0 ; 
       reduce-mat69_1.1-0 ; 
       reduce-mat70_1.1-0 ; 
       reduce-mat71_1.1-0 ; 
       reduce-mat72_1.1-0 ; 
       reduce-mat73_1.1-0 ; 
       reduce-mat74_1.1-0 ; 
       reduce-mat76_1.1-0 ; 
       reduce-mat77_1.1-0 ; 
       reduce-mat78_1.1-0 ; 
       reduce-mat79_1.1-0 ; 
       reduce-mat80_1.1-0 ; 
       reduce-mat81_1.1-0 ; 
       reduce-mat82_1.1-0 ; 
       reduce-mat83_1.1-0 ; 
       reduce-mat84_1.1-0 ; 
       reduce-mat85_1.1-0 ; 
       reduce-mat86.1-0 ; 
       reduce-mat87.1-0 ; 
       reduce-mat88.1-0 ; 
       reduce-mat89.1-0 ; 
       reduce-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       fig12-awepatt.1-0 ; 
       fig12-fig12.3-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lbwepemt.1-0 ; 
       fig12-lengine.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-mengine.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rbwepemt.1-0 ; 
       fig12-rengine.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-rwepemt3.1-0 ; 
       fig12-rwepemt4.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-reduce.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 58     
       reduce-t2d1_1.1-0 ; 
       reduce-t2d10_1.1-0 ; 
       reduce-t2d11_1.1-0 ; 
       reduce-t2d12_1.1-0 ; 
       reduce-t2d13_1.1-0 ; 
       reduce-t2d14_1.1-0 ; 
       reduce-t2d15_1.1-0 ; 
       reduce-t2d16_1.1-0 ; 
       reduce-t2d17_1.1-0 ; 
       reduce-t2d18_1.1-0 ; 
       reduce-t2d19_1.1-0 ; 
       reduce-t2d2_1.1-0 ; 
       reduce-t2d20_1.1-0 ; 
       reduce-t2d21_1.1-0 ; 
       reduce-t2d22_1.1-0 ; 
       reduce-t2d23_1.1-0 ; 
       reduce-t2d24_1.1-0 ; 
       reduce-t2d25_1.1-0 ; 
       reduce-t2d26_1.1-0 ; 
       reduce-t2d27_1.1-0 ; 
       reduce-t2d29_1.1-0 ; 
       reduce-t2d30_1.1-0 ; 
       reduce-t2d31_1.1-0 ; 
       reduce-t2d32_1.1-0 ; 
       reduce-t2d33_1.1-0 ; 
       reduce-t2d34_1.1-0 ; 
       reduce-t2d35_1.1-0 ; 
       reduce-t2d36_1.1-0 ; 
       reduce-t2d37_1.1-0 ; 
       reduce-t2d38_1.1-0 ; 
       reduce-t2d39_1.1-0 ; 
       reduce-t2d40_1.1-0 ; 
       reduce-t2d41_1.1-0 ; 
       reduce-t2d42_1.1-0 ; 
       reduce-t2d43_1.1-0 ; 
       reduce-t2d44_1.1-0 ; 
       reduce-t2d45_1.1-0 ; 
       reduce-t2d46_1.1-0 ; 
       reduce-t2d47_1.1-0 ; 
       reduce-t2d48_1.1-0 ; 
       reduce-t2d49_1.1-0 ; 
       reduce-t2d50_1.1-0 ; 
       reduce-t2d51_1.1-0 ; 
       reduce-t2d52_1.1-0 ; 
       reduce-t2d53_1.1-0 ; 
       reduce-t2d54_1.1-0 ; 
       reduce-t2d55_1.1-0 ; 
       reduce-t2d56_1.1-0 ; 
       reduce-t2d58_1.1-0 ; 
       reduce-t2d59_1.1-0 ; 
       reduce-t2d60_1.1-0 ; 
       reduce-t2d61_1.1-0 ; 
       reduce-t2d62_1.1-0 ; 
       reduce-t2d63_1.1-0 ; 
       reduce-t2d64_1.1-0 ; 
       reduce-t2d65_1.1-0 ; 
       reduce-t2d66_1.1-0 ; 
       reduce-t2d67_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 44 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 45 110 ; 
       7 3 110 ; 
       8 45 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 3 110 ; 
       12 2 110 ; 
       13 19 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 19 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 1 110 ; 
       20 19 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 45 110 ; 
       24 45 110 ; 
       25 3 110 ; 
       26 45 110 ; 
       27 26 110 ; 
       28 27 110 ; 
       29 3 110 ; 
       30 2 110 ; 
       31 45 110 ; 
       32 45 110 ; 
       33 31 110 ; 
       34 32 110 ; 
       35 24 110 ; 
       36 23 110 ; 
       37 5 110 ; 
       38 4 110 ; 
       39 3 110 ; 
       40 2 110 ; 
       41 3 110 ; 
       42 2 110 ; 
       43 45 110 ; 
       44 43 110 ; 
       45 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       3 0 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 1 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 64 300 ; 
       3 67 300 ; 
       3 68 300 ; 
       3 69 300 ; 
       3 70 300 ; 
       3 71 300 ; 
       3 72 300 ; 
       3 73 300 ; 
       3 74 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       8 65 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 39 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       12 12 300 ; 
       13 57 300 ; 
       14 58 300 ; 
       15 59 300 ; 
       16 47 300 ; 
       17 48 300 ; 
       18 49 300 ; 
       20 50 300 ; 
       21 51 300 ; 
       22 52 300 ; 
       23 56 300 ; 
       24 55 300 ; 
       25 5 300 ; 
       25 6 300 ; 
       26 66 300 ; 
       27 40 300 ; 
       27 41 300 ; 
       27 42 300 ; 
       27 43 300 ; 
       27 44 300 ; 
       27 60 300 ; 
       29 3 300 ; 
       29 4 300 ; 
       30 63 300 ; 
       31 16 300 ; 
       31 53 300 ; 
       32 54 300 ; 
       37 79 300 ; 
       38 78 300 ; 
       39 75 300 ; 
       40 76 300 ; 
       41 80 300 ; 
       42 77 300 ; 
       43 62 300 ; 
       44 61 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 11 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       28 14 401 ; 
       29 15 401 ; 
       30 16 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       39 23 401 ; 
       40 42 401 ; 
       41 24 401 ; 
       42 25 401 ; 
       43 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
       48 30 401 ; 
       49 31 401 ; 
       50 32 401 ; 
       51 33 401 ; 
       52 34 401 ; 
       53 35 401 ; 
       54 36 401 ; 
       55 37 401 ; 
       56 38 401 ; 
       57 39 401 ; 
       58 40 401 ; 
       59 41 401 ; 
       60 43 401 ; 
       61 44 401 ; 
       62 47 401 ; 
       63 45 401 ; 
       64 46 401 ; 
       65 48 401 ; 
       66 49 401 ; 
       67 50 401 ; 
       68 51 401 ; 
       69 52 401 ; 
       70 53 401 ; 
       71 54 401 ; 
       72 55 401 ; 
       73 56 401 ; 
       74 57 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -12 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -16 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 31.25 -2 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 53.75 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 50 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 60 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 11.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 10 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 15 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 16.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 21.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 20 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 30 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 55 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 35 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 35 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 35 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 30 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 32.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 57.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 50 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 45 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 47.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 0 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 40 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 40 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 33.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 51.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 51.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 54 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 59 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 59 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 59 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 59 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 51.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 51.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 51.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 39 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 39 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 39 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 39 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 39 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 19 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 16.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 24 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 21.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 19 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 29 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 31.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 34 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 9 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 36.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 41.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 41.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 4 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 39 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 36.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 64 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 44 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 1.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM -1 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 49 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 46.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 51.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 51.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 54 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 59 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 59 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 59 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 59 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 59 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 51.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 51.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 51.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 39 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 39 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 39 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 39 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 16.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 21.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 29 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 31.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 34 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 9 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 36.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 41.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 4 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 41.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 39 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 36.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 64 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 64 -4 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 351.5 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 351.5 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 351.5 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 108 79 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
