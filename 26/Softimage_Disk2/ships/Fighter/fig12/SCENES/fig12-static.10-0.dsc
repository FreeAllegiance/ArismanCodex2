SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.55-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.55-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.55-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.55-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.55-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       re_textur-default1_1.4-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat38_1.4-0 ; 
       re_textur-mat41_1.4-0 ; 
       re_textur-mat42_1.4-0 ; 
       re_textur-mat43_1.4-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.4-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.4-0 ; 
       re_textur-mat83_1.4-0 ; 
       re_textur-mat84_1.4-0 ; 
       re_textur-mat85_1.4-0 ; 
       static-front1.5-0 ; 
       static-mat91.5-0 ; 
       static-mat92.5-0 ; 
       static-sides1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       fig12-cube1.1-0 ; 
       fig12-cyl1.1-0 ; 
       fig12-fig12.42-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-static.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       re_textur-t2d1_1.2-0 ; 
       re_textur-t2d16_1.2-0 ; 
       re_textur-t2d17_1.2-0 ; 
       re_textur-t2d18_1.2-0 ; 
       re_textur-t2d19_1.2-0 ; 
       re_textur-t2d2_1.2-0 ; 
       re_textur-t2d20_1.2-0 ; 
       re_textur-t2d21_1.2-0 ; 
       re_textur-t2d22_1.5-0 ; 
       re_textur-t2d25_1.5-0 ; 
       re_textur-t2d26_1.5-0 ; 
       re_textur-t2d27_1.5-0 ; 
       re_textur-t2d29_1.2-0 ; 
       re_textur-t2d30_1.2-0 ; 
       re_textur-t2d31_1.2-0 ; 
       re_textur-t2d32_1.2-0 ; 
       re_textur-t2d33_1.2-0 ; 
       re_textur-t2d34_1.2-0 ; 
       re_textur-t2d35_1.2-0 ; 
       re_textur-t2d51_1.2-0 ; 
       re_textur-t2d52_1.2-0 ; 
       re_textur-t2d53_1.6-0 ; 
       re_textur-t2d54_1.2-0 ; 
       re_textur-t2d56_1.2-0 ; 
       re_textur-t2d58_1.2-0 ; 
       re_textur-t2d59_1.2-0 ; 
       re_textur-t2d62_1.5-0 ; 
       re_textur-t2d65_1.5-0 ; 
       re_textur-t2d66_1.5-0 ; 
       re_textur-t2d67_1.5-0 ; 
       static-t2d68.5-0 ; 
       static-t2d69.5-0 ; 
       static-t2d70.5-0 ; 
       static-t2d71.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 0 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 15 110 ; 
       8 7 110 ; 
       9 3 110 ; 
       10 15 110 ; 
       11 10 110 ; 
       12 3 110 ; 
       13 15 110 ; 
       14 13 110 ; 
       15 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 34 300 ; 
       1 35 300 ; 
       3 1 300 ; 
       4 0 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       7 27 300 ; 
       8 13 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       9 5 300 ; 
       10 28 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       12 26 300 ; 
       13 25 300 ; 
       14 24 300 ; 
       14 36 300 ; 
       14 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 30 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 19 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 23 401 ; 
       26 22 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 25 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -0.09168417 MPRFLG 0 ; 
       3 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 28.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 17.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       33 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       30 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
