SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.46-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.46-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.46-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.46-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.46-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1.5-0 ROOT ; 
       static-light2.5-0 ROOT ; 
       static-light3.5-0 ROOT ; 
       static-light4.5-0 ROOT ; 
       static-light5.5-0 ROOT ; 
       static-light6.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       re_textur-default1_1.4-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.4-0 ; 
       re_textur-mat41_1.4-0 ; 
       re_textur-mat42_1.4-0 ; 
       re_textur-mat43_1.4-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat57_1.1-0 ; 
       re_textur-mat58_1.1-0 ; 
       re_textur-mat59_1.1-0 ; 
       re_textur-mat60_1.1-0 ; 
       re_textur-mat61_1.1-0 ; 
       re_textur-mat62_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat67_1.1-0 ; 
       re_textur-mat68_1.1-0 ; 
       re_textur-mat69_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.4-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.4-0 ; 
       re_textur-mat83_1.4-0 ; 
       re_textur-mat84_1.4-0 ; 
       re_textur-mat85_1.4-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
       static-front1.4-0 ; 
       static-mat91.4-0 ; 
       static-mat92.4-0 ; 
       static-sides1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       fig12-awepatt.1-0 ; 
       fig12-bwepemt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-cube1.1-0 ; 
       fig12-cyl1.1-0 ; 
       fig12-fig12.34-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-lwepemt1.1-0 ; 
       fig12-lwepemt2.1-0 ; 
       fig12-missemt.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.4-0 ; 
       re_textur-t2d25_1.4-0 ; 
       re_textur-t2d26_1.4-0 ; 
       re_textur-t2d27_1.4-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d38_1.1-0 ; 
       re_textur-t2d39_1.1-0 ; 
       re_textur-t2d40_1.1-0 ; 
       re_textur-t2d41_1.1-0 ; 
       re_textur-t2d42_1.1-0 ; 
       re_textur-t2d43_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d48_1.1-0 ; 
       re_textur-t2d49_1.1-0 ; 
       re_textur-t2d50_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.5-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.4-0 ; 
       re_textur-t2d65_1.4-0 ; 
       re_textur-t2d66_1.4-0 ; 
       re_textur-t2d67_1.4-0 ; 
       static-t2d68.4-0 ; 
       static-t2d69.4-0 ; 
       static-t2d70.4-0 ; 
       static-t2d71.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 46 110 ; 
       1 31 110 ; 
       2 5 110 ; 
       3 46 110 ; 
       4 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 47 110 ; 
       11 7 110 ; 
       12 47 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       15 21 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 21 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 5 110 ; 
       22 21 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 47 110 ; 
       26 47 110 ; 
       27 25 110 ; 
       28 26 110 ; 
       29 13 110 ; 
       30 47 110 ; 
       31 30 110 ; 
       32 6 110 ; 
       33 47 110 ; 
       34 47 110 ; 
       35 33 110 ; 
       36 34 110 ; 
       37 9 110 ; 
       38 8 110 ; 
       39 7 110 ; 
       40 6 110 ; 
       41 7 110 ; 
       42 6 110 ; 
       43 5 110 ; 
       44 5 110 ; 
       45 47 110 ; 
       46 45 110 ; 
       47 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 54 300 ; 
       4 55 300 ; 
       6 1 300 ; 
       7 0 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       7 45 300 ; 
       7 46 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       12 41 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       13 17 300 ; 
       13 18 300 ; 
       14 5 300 ; 
       15 34 300 ; 
       16 35 300 ; 
       17 36 300 ; 
       18 24 300 ; 
       19 25 300 ; 
       20 26 300 ; 
       22 27 300 ; 
       23 28 300 ; 
       24 29 300 ; 
       25 33 300 ; 
       26 32 300 ; 
       30 42 300 ; 
       31 19 300 ; 
       31 20 300 ; 
       31 21 300 ; 
       31 22 300 ; 
       31 23 300 ; 
       31 37 300 ; 
       32 40 300 ; 
       33 9 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       37 51 300 ; 
       38 50 300 ; 
       39 47 300 ; 
       40 48 300 ; 
       41 52 300 ; 
       42 49 300 ; 
       45 39 300 ; 
       46 38 300 ; 
       46 56 300 ; 
       46 53 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 32 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       39 36 401 ; 
       40 35 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       53 44 401 ; 
       54 45 401 ; 
       55 46 401 ; 
       56 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -8 0 MPRFLG 0 ; 
       1 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 60 -4 0 MPRFLG 0 ; 
       10 SCHEM 50 -4 0 MPRFLG 0 ; 
       11 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 15 -6 0 MPRFLG 0 ; 
       18 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 20 -6 0 MPRFLG 0 ; 
       21 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 25 -6 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 35 -4 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -4 0 MPRFLG 0 ; 
       31 SCHEM 40 -6 0 MPRFLG 0 ; 
       32 SCHEM 10 -4 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 60 -6 0 MPRFLG 0 ; 
       38 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 55 -4 0 MPRFLG 0 ; 
       42 SCHEM 5 -4 0 MPRFLG 0 ; 
       43 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       46 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       47 SCHEM 40 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 71.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 49 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
