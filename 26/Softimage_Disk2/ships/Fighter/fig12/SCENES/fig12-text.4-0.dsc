SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       fig12-fig12.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.23-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.23-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       re_textur-light1.17-0 ROOT ; 
       re_textur-light2.17-0 ROOT ; 
       re_textur-light3.17-0 ROOT ; 
       re_textur-light4.17-0 ROOT ; 
       re_textur-light5.17-0 ROOT ; 
       re_textur-light6.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       re_textur-default1_1.3-0 ; 
       re_textur-mat1_1.1-0 ; 
       re_textur-mat17_1.1-0 ; 
       re_textur-mat18_1.1-0 ; 
       re_textur-mat19_1.1-0 ; 
       re_textur-mat2_1.1-0 ; 
       re_textur-mat20_1.1-0 ; 
       re_textur-mat21_1.1-0 ; 
       re_textur-mat22_1.1-0 ; 
       re_textur-mat26_1.1-0 ; 
       re_textur-mat38_1.3-0 ; 
       re_textur-mat41_1.3-0 ; 
       re_textur-mat42_1.3-0 ; 
       re_textur-mat43_1.3-0 ; 
       re_textur-mat45_1.1-0 ; 
       re_textur-mat46_1.1-0 ; 
       re_textur-mat47_1.1-0 ; 
       re_textur-mat48_1.1-0 ; 
       re_textur-mat49_1.1-0 ; 
       re_textur-mat50_1.1-0 ; 
       re_textur-mat51_1.1-0 ; 
       re_textur-mat52_1.1-0 ; 
       re_textur-mat53_1.1-0 ; 
       re_textur-mat54_1.1-0 ; 
       re_textur-mat57_1.1-0 ; 
       re_textur-mat58_1.1-0 ; 
       re_textur-mat59_1.1-0 ; 
       re_textur-mat60_1.1-0 ; 
       re_textur-mat61_1.1-0 ; 
       re_textur-mat62_1.1-0 ; 
       re_textur-mat63_1.1-0 ; 
       re_textur-mat64_1.1-0 ; 
       re_textur-mat65_1.1-0 ; 
       re_textur-mat66_1.1-0 ; 
       re_textur-mat67_1.1-0 ; 
       re_textur-mat68_1.1-0 ; 
       re_textur-mat69_1.1-0 ; 
       re_textur-mat70_1.1-0 ; 
       re_textur-mat71_1.3-0 ; 
       re_textur-mat72_1.1-0 ; 
       re_textur-mat73_1.1-0 ; 
       re_textur-mat76_1.1-0 ; 
       re_textur-mat77_1.1-0 ; 
       re_textur-mat80_1.3-0 ; 
       re_textur-mat83_1.3-0 ; 
       re_textur-mat84_1.3-0 ; 
       re_textur-mat85_1.3-0 ; 
       re_textur-mat86.1-0 ; 
       re_textur-mat87.1-0 ; 
       re_textur-mat88.1-0 ; 
       re_textur-mat89.1-0 ; 
       re_textur-mat90.1-0 ; 
       rix_fighter_sPAt-mat75.1-0 ; 
       text-front1.2-0 ; 
       text-sides1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 46     
       fig12-awepatt.1-0 ; 
       fig12-bwepemt.1-0 ; 
       fig12-cockpt.1-0 ; 
       fig12-fig12.18-0 ROOT ; 
       fig12-finzzz1.1-0 ; 
       fig12-fuselg1.1-0 ; 
       fig12-fuselg2.1-0 ; 
       fig12-fuselg3.1-0 ; 
       fig12-fweapon0.1-0 ; 
       fig12-fwepmnt.1-0 ; 
       fig12-lbweapon1.1-0 ; 
       fig12-lbweapon2.1-0 ; 
       fig12-lfinzzz.1-0 ; 
       fig12-LLf1.1-0 ; 
       fig12-LLf2.1-0 ; 
       fig12-LLf3.1-0 ; 
       fig12-LLl1.1-0 ; 
       fig12-LLl2.1-0 ; 
       fig12-LLl3.1-0 ; 
       fig12-LLlandgr0.1-0 ; 
       fig12-LLr1.1-0 ; 
       fig12-LLr2.1-0 ; 
       fig12-LLr3.1-0 ; 
       fig12-lweapon1.1-0 ; 
       fig12-lweapon2.1-0 ; 
       fig12-lwepemt1.1-0 ; 
       fig12-lwepemt2.1-0 ; 
       fig12-missemt.1-0 ; 
       fig12-rbweapon1.1-0 ; 
       fig12-rbweapon2.1-0 ; 
       fig12-rfinzzz.1-0 ; 
       fig12-rweapon1.1-0 ; 
       fig12-rweapon2.1-0 ; 
       fig12-rwepemt1.1-0 ; 
       fig12-rwepemt2.1-0 ; 
       fig12-SSfl.1-0 ; 
       fig12-SSfr.1-0 ; 
       fig12-SSl.1-0 ; 
       fig12-SSla.1-0 ; 
       fig12-SSr.1-0 ; 
       fig12-SSra.1-0 ; 
       fig12-thrust.1-0 ; 
       fig12-trail.1-0 ; 
       fig12-tweapon1.1-0 ; 
       fig12-tweapon2.1-0 ; 
       fig12-weapon0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig12/PICTURES/fig12 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig12-text.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       re_textur-t2d1_1.1-0 ; 
       re_textur-t2d16_1.1-0 ; 
       re_textur-t2d17_1.1-0 ; 
       re_textur-t2d18_1.1-0 ; 
       re_textur-t2d19_1.1-0 ; 
       re_textur-t2d2_1.1-0 ; 
       re_textur-t2d20_1.1-0 ; 
       re_textur-t2d21_1.1-0 ; 
       re_textur-t2d22_1.4-0 ; 
       re_textur-t2d25_1.4-0 ; 
       re_textur-t2d26_1.4-0 ; 
       re_textur-t2d27_1.4-0 ; 
       re_textur-t2d29_1.1-0 ; 
       re_textur-t2d30_1.1-0 ; 
       re_textur-t2d31_1.1-0 ; 
       re_textur-t2d32_1.1-0 ; 
       re_textur-t2d33_1.1-0 ; 
       re_textur-t2d34_1.1-0 ; 
       re_textur-t2d35_1.1-0 ; 
       re_textur-t2d38_1.1-0 ; 
       re_textur-t2d39_1.1-0 ; 
       re_textur-t2d40_1.1-0 ; 
       re_textur-t2d41_1.1-0 ; 
       re_textur-t2d42_1.1-0 ; 
       re_textur-t2d43_1.1-0 ; 
       re_textur-t2d44_1.1-0 ; 
       re_textur-t2d45_1.1-0 ; 
       re_textur-t2d46_1.1-0 ; 
       re_textur-t2d47_1.1-0 ; 
       re_textur-t2d48_1.1-0 ; 
       re_textur-t2d49_1.1-0 ; 
       re_textur-t2d50_1.1-0 ; 
       re_textur-t2d51_1.1-0 ; 
       re_textur-t2d52_1.1-0 ; 
       re_textur-t2d53_1.5-0 ; 
       re_textur-t2d54_1.1-0 ; 
       re_textur-t2d56_1.1-0 ; 
       re_textur-t2d58_1.1-0 ; 
       re_textur-t2d59_1.1-0 ; 
       re_textur-t2d62_1.4-0 ; 
       re_textur-t2d65_1.4-0 ; 
       re_textur-t2d66_1.4-0 ; 
       re_textur-t2d67_1.4-0 ; 
       text-t2d68.4-0 ; 
       text-t2d69.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 44 110 ; 
       1 29 110 ; 
       2 5 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 45 110 ; 
       9 5 110 ; 
       10 45 110 ; 
       11 10 110 ; 
       12 4 110 ; 
       13 19 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 19 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 3 110 ; 
       20 19 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 45 110 ; 
       24 45 110 ; 
       25 23 110 ; 
       26 24 110 ; 
       27 11 110 ; 
       28 45 110 ; 
       29 28 110 ; 
       30 4 110 ; 
       31 45 110 ; 
       32 45 110 ; 
       33 31 110 ; 
       34 32 110 ; 
       35 7 110 ; 
       36 6 110 ; 
       37 5 110 ; 
       38 4 110 ; 
       39 5 110 ; 
       40 4 110 ; 
       41 5 110 ; 
       42 5 110 ; 
       43 45 110 ; 
       44 43 110 ; 
       45 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 1 300 ; 
       5 0 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       5 46 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       10 41 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       12 5 300 ; 
       13 34 300 ; 
       14 35 300 ; 
       15 36 300 ; 
       16 24 300 ; 
       17 25 300 ; 
       18 26 300 ; 
       20 27 300 ; 
       21 28 300 ; 
       22 29 300 ; 
       23 33 300 ; 
       24 32 300 ; 
       28 42 300 ; 
       29 19 300 ; 
       29 20 300 ; 
       29 21 300 ; 
       29 22 300 ; 
       29 23 300 ; 
       29 37 300 ; 
       30 40 300 ; 
       31 9 300 ; 
       31 30 300 ; 
       32 31 300 ; 
       35 51 300 ; 
       36 50 300 ; 
       37 47 300 ; 
       38 48 300 ; 
       39 52 300 ; 
       40 49 300 ; 
       43 39 300 ; 
       44 38 300 ; 
       44 54 300 ; 
       44 53 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       54 43 401 ; 
       53 44 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 32 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 33 401 ; 
       38 34 401 ; 
       39 36 401 ; 
       40 35 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       46 42 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 95 -10 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 137.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 118.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 128.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 85 -6 0 MPRFLG 0 ; 
       11 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 20 -8 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 30 -6 0 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       24 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       29 SCHEM 65 -8 0 MPRFLG 0 ; 
       30 SCHEM 5 -6 0 MPRFLG 0 ; 
       31 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       33 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 125 -8 0 MPRFLG 0 ; 
       36 SCHEM 115 -8 0 MPRFLG 0 ; 
       37 SCHEM 110 -6 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       39 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       40 SCHEM 0 -6 0 MPRFLG 0 ; 
       41 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 100 -6 0 MPRFLG 0 ; 
       44 SCHEM 98.75 -8 0 MPRFLG 0 ; 
       45 SCHEM 71.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 152.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 166.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 344 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 15 108 107 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
