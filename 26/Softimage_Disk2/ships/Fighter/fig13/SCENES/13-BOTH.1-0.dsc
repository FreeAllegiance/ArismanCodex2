SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 33     
       done_cropping-fig13_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_31.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.80-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.80-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 54     
       BOTH-mat110.1-0 ; 
       BOTH-mat111.1-0 ; 
       BOTH-mat69.1-0 ; 
       done_cropping-mat10.2-0 ; 
       done_cropping-mat10_1.1-0 ; 
       done_cropping-mat100.2-0 ; 
       done_cropping-mat101.2-0 ; 
       done_cropping-mat105.2-0 ; 
       done_cropping-mat105_1.1-0 ; 
       done_cropping-mat106.2-0 ; 
       done_cropping-mat106_1.1-0 ; 
       done_cropping-mat107.2-0 ; 
       done_cropping-mat107_1.1-0 ; 
       done_cropping-mat12.2-0 ; 
       done_cropping-mat12_1.1-0 ; 
       done_cropping-mat13.2-0 ; 
       done_cropping-mat13_1.1-0 ; 
       done_cropping-mat14.2-0 ; 
       done_cropping-mat14_1.1-0 ; 
       done_cropping-mat15.2-0 ; 
       done_cropping-mat15_1.1-0 ; 
       done_cropping-mat23.2-0 ; 
       done_cropping-mat24.2-0 ; 
       done_cropping-mat25.2-0 ; 
       done_cropping-mat34.4-0 ; 
       done_cropping-mat34_1.1-0 ; 
       done_cropping-mat44.4-0 ; 
       done_cropping-mat44_1.1-0 ; 
       done_cropping-mat59.4-0 ; 
       done_cropping-mat59_1.1-0 ; 
       done_cropping-mat74.2-0 ; 
       done_cropping-mat74_1.1-0 ; 
       done_cropping-mat75.2-0 ; 
       done_cropping-mat75_1.1-0 ; 
       done_cropping-mat76.2-0 ; 
       done_cropping-mat76_1.1-0 ; 
       done_cropping-mat77.2-0 ; 
       done_cropping-mat77_1.1-0 ; 
       done_cropping-mat83.2-0 ; 
       done_cropping-mat84.2-0 ; 
       done_cropping-mat84_1.1-0 ; 
       done_cropping-mat88.2-0 ; 
       done_cropping-mat88_1.1-0 ; 
       done_cropping-mat90.2-0 ; 
       done_cropping-mat94.2-0 ; 
       done_cropping-mat94_1.1-0 ; 
       done_cropping-mat96.2-0 ; 
       done_cropping-mat96_1.1-0 ; 
       done_cropping-mat98.2-0 ; 
       fins-mat109.2-0 ; 
       fins-mat109_1.1-0 ; 
       fix_blinker-mat72.2-0 ; 
       fix_blinker-mat72_1.1-0 ; 
       STATIC-mat110.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       done_cropping-afuselg.3-0 ; 
       done_cropping-afuselg_1.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool10_1.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-bool11_1.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-ffuselg_1.1-0 ; 
       done_cropping-fig13.38-0 ROOT ; 
       done_cropping-fig13_1.1-0 ROOT ; 
       done_cropping-gear-null.1-0 ; 
       done_cropping-l-gun.1-0 ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine_1.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-lengine1_1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz_1.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-Lfinzzz1_1.1-0 ; 
       done_cropping-LL1.1-0 ; 
       done_cropping-LL2.1-0 ; 
       done_cropping-LR1.1-0 ; 
       done_cropping-LR2.1-0 ; 
       done_cropping-lsmoke.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-lwingzz_1.1-0 ; 
       done_cropping-missemt.1-0 ; 
       done_cropping-rsmoke.1-0 ; 
       done_cropping-rthrust.1-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-rwingzz_1.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSar1_1_3.1-0 ; 
       done_cropping-SSar1_1_8.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       13-BOTH.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 49     
       BOTH-t2d119.1-0 ; 
       BOTH-t2d120.1-0 ; 
       BOTH-t2d85.1-0 ; 
       done_cropping-t2d100.3-0 ; 
       done_cropping-t2d100_1.1-0 ; 
       done_cropping-t2d105.2-0 ; 
       done_cropping-t2d105_1.1-0 ; 
       done_cropping-t2d107.2-0 ; 
       done_cropping-t2d107_1.1-0 ; 
       done_cropping-t2d109.3-0 ; 
       done_cropping-t2d11.3-0 ; 
       done_cropping-t2d11_1.1-0 ; 
       done_cropping-t2d110.5-0 ; 
       done_cropping-t2d111.2-0 ; 
       done_cropping-t2d114.2-0 ; 
       done_cropping-t2d114_1.1-0 ; 
       done_cropping-t2d115.2-0 ; 
       done_cropping-t2d115_1.1-0 ; 
       done_cropping-t2d116.2-0 ; 
       done_cropping-t2d116_1.1-0 ; 
       done_cropping-t2d12.3-0 ; 
       done_cropping-t2d12_1.1-0 ; 
       done_cropping-t2d13.3-0 ; 
       done_cropping-t2d13_1.1-0 ; 
       done_cropping-t2d14.3-0 ; 
       done_cropping-t2d14_1.1-0 ; 
       done_cropping-t2d30.5-0 ; 
       done_cropping-t2d30_1.1-0 ; 
       done_cropping-t2d61.5-0 ; 
       done_cropping-t2d61_1.1-0 ; 
       done_cropping-t2d64.3-0 ; 
       done_cropping-t2d64_1.1-0 ; 
       done_cropping-t2d65.5-0 ; 
       done_cropping-t2d65_1.1-0 ; 
       done_cropping-t2d89.3-0 ; 
       done_cropping-t2d89_1.1-0 ; 
       done_cropping-t2d90.3-0 ; 
       done_cropping-t2d90_1.1-0 ; 
       done_cropping-t2d91.3-0 ; 
       done_cropping-t2d91_1.1-0 ; 
       done_cropping-t2d96.3-0 ; 
       done_cropping-t2d96_1.1-0 ; 
       done_cropping-z.1-0 ; 
       done_cropping-z_1.1-0 ; 
       fins-t2d118.2-0 ; 
       fins-t2d118_1.1-0 ; 
       fix_blinker-t2d88.4-0 ; 
       fix_blinker-t2d88_1.1-0 ; 
       STATIC-t2d119.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       2 32 110 ; 
       4 27 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       11 10 110 ; 
       12 7 110 ; 
       13 10 110 ; 
       15 10 110 ; 
       17 27 110 ; 
       19 32 110 ; 
       21 11 110 ; 
       22 21 110 ; 
       23 11 110 ; 
       24 23 110 ; 
       25 10 110 ; 
       26 13 110 ; 
       27 13 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 15 110 ; 
       32 15 110 ; 
       34 17 110 ; 
       35 19 110 ; 
       36 17 110 ; 
       37 19 110 ; 
       38 7 110 ; 
       39 10 110 ; 
       40 12 110 ; 
       1 8 110 ; 
       3 33 110 ; 
       5 28 110 ; 
       8 9 110 ; 
       14 9 110 ; 
       16 9 110 ; 
       18 28 110 ; 
       20 33 110 ; 
       28 14 110 ; 
       33 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 13 300 ; 
       0 15 300 ; 
       0 17 300 ; 
       0 19 300 ; 
       0 36 300 ; 
       2 44 300 ; 
       4 46 300 ; 
       7 24 300 ; 
       7 26 300 ; 
       7 28 300 ; 
       7 0 300 ; 
       12 2 300 ; 
       13 30 300 ; 
       13 32 300 ; 
       13 34 300 ; 
       15 7 300 ; 
       15 9 300 ; 
       15 11 300 ; 
       17 51 300 ; 
       19 49 300 ; 
       21 48 300 ; 
       22 5 300 ; 
       23 6 300 ; 
       24 1 300 ; 
       27 39 300 ; 
       32 41 300 ; 
       34 21 300 ; 
       35 22 300 ; 
       36 38 300 ; 
       37 43 300 ; 
       38 23 300 ; 
       1 4 300 ; 
       1 14 300 ; 
       1 16 300 ; 
       1 18 300 ; 
       1 20 300 ; 
       1 37 300 ; 
       3 45 300 ; 
       5 47 300 ; 
       8 25 300 ; 
       8 27 300 ; 
       8 29 300 ; 
       8 53 300 ; 
       14 31 300 ; 
       14 33 300 ; 
       14 35 300 ; 
       16 8 300 ; 
       16 10 300 ; 
       16 12 300 ; 
       18 52 300 ; 
       20 50 300 ; 
       28 40 300 ; 
       33 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 30 401 ; 
       5 12 401 ; 
       6 13 401 ; 
       7 14 401 ; 
       9 16 401 ; 
       11 18 401 ; 
       13 10 401 ; 
       15 20 401 ; 
       17 22 401 ; 
       19 24 401 ; 
       24 26 401 ; 
       26 28 401 ; 
       28 32 401 ; 
       30 34 401 ; 
       32 36 401 ; 
       34 38 401 ; 
       36 42 401 ; 
       39 40 401 ; 
       41 3 401 ; 
       44 5 401 ; 
       46 7 401 ; 
       48 9 401 ; 
       49 44 401 ; 
       51 46 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 31 401 ; 
       8 15 401 ; 
       10 17 401 ; 
       12 19 401 ; 
       14 11 401 ; 
       16 21 401 ; 
       18 23 401 ; 
       20 25 401 ; 
       25 27 401 ; 
       27 29 401 ; 
       29 33 401 ; 
       31 35 401 ; 
       33 37 401 ; 
       35 39 401 ; 
       37 43 401 ; 
       40 41 401 ; 
       42 4 401 ; 
       45 6 401 ; 
       47 8 401 ; 
       50 45 401 ; 
       52 47 401 ; 
       53 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 40 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 25.82489 5.774246 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 15 -4 0 MPRFLG 0 ; 
       24 SCHEM 14.86935 -7.022033 0 USR MPRFLG 0 ; 
       25 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20 -4 0 MPRFLG 0 ; 
       29 SCHEM 47.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 35 -8 0 MPRFLG 0 ; 
       36 SCHEM 20 -8 0 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 10 -4 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 57 -4 0 MPRFLG 0 ; 
       3 SCHEM 89.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 72 -6 0 MPRFLG 0 ; 
       8 SCHEM 57 -2 0 MPRFLG 0 ; 
       9 SCHEM 74.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 70.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 85.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 68.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 85.75 -6 0 MPRFLG 0 ; 
       28 SCHEM 69.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 87 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.36935 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13.86935 -9.022034 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 91 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 91 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 91 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 76 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 56 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 73.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 91 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 88.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 71 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 88.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 71 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 61 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.36935 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 13.86935 -11.02203 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 91 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 88.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 71 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 91 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 91 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 91 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 76 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 73.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 56 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 88.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 71 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 61 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 200.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 139.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 140.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 187.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 186.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 289.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 147.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 182.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 221.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 189.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 182.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 147.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 189.1147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 221.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 172.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 212.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
