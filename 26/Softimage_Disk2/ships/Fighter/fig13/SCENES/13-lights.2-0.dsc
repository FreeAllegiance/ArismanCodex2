SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.85-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.85-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       done_cropping-mat10.2-0 ; 
       done_cropping-mat105.2-0 ; 
       done_cropping-mat106.2-0 ; 
       done_cropping-mat107.2-0 ; 
       done_cropping-mat12.2-0 ; 
       done_cropping-mat13.2-0 ; 
       done_cropping-mat14.2-0 ; 
       done_cropping-mat15.2-0 ; 
       done_cropping-mat23.2-0 ; 
       done_cropping-mat24.2-0 ; 
       done_cropping-mat25.2-0 ; 
       done_cropping-mat34.5-0 ; 
       done_cropping-mat44.5-0 ; 
       done_cropping-mat59.5-0 ; 
       done_cropping-mat74.2-0 ; 
       done_cropping-mat75.2-0 ; 
       done_cropping-mat76.2-0 ; 
       done_cropping-mat77.2-0 ; 
       done_cropping-mat84.2-0 ; 
       done_cropping-mat88.2-0 ; 
       done_cropping-mat94.2-0 ; 
       done_cropping-mat96.2-0 ; 
       fins-mat109.2-0 ; 
       fix_blinker-mat72.2-0 ; 
       text-mat110.1-0 ; 
       text-mat69.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13_1.4-0 ROOT ; 
       done_cropping-l-gun.1-0 ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-lsmoke.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-missemt.1-0 ; 
       done_cropping-rsmoke.1-0 ; 
       done_cropping-rthrust.1-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       13-lights.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       done_cropping-t2d100.3-0 ; 
       done_cropping-t2d105.2-0 ; 
       done_cropping-t2d107.2-0 ; 
       done_cropping-t2d11.3-0 ; 
       done_cropping-t2d114.2-0 ; 
       done_cropping-t2d115.2-0 ; 
       done_cropping-t2d116.2-0 ; 
       done_cropping-t2d12.3-0 ; 
       done_cropping-t2d13.3-0 ; 
       done_cropping-t2d14.3-0 ; 
       done_cropping-t2d30.7-0 ; 
       done_cropping-t2d61.7-0 ; 
       done_cropping-t2d64.3-0 ; 
       done_cropping-t2d65.7-0 ; 
       done_cropping-t2d89.3-0 ; 
       done_cropping-t2d90.3-0 ; 
       done_cropping-t2d91.3-0 ; 
       done_cropping-t2d96.3-0 ; 
       done_cropping-z.1-0 ; 
       fins-t2d118.2-0 ; 
       fix_blinker-t2d88.4-0 ; 
       text-t2d119.2-0 ; 
       text-t2d85.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 17 110 ; 
       2 13 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 13 110 ; 
       10 17 110 ; 
       11 5 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 9 110 ; 
       19 10 110 ; 
       20 4 110 ; 
       21 5 110 ; 
       22 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 17 300 ; 
       1 20 300 ; 
       2 21 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 24 300 ; 
       6 25 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 23 300 ; 
       10 22 300 ; 
       13 18 300 ; 
       17 19 300 ; 
       18 8 300 ; 
       19 9 300 ; 
       20 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 3 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 18 401 ; 
       18 17 401 ; 
       19 0 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 10 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
