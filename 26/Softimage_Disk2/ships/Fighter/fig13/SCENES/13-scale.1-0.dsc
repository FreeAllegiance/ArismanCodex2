SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 17     
       done_cropping-fig13.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.32-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.32-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.75-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.75-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       done_cropping-mat10.2-0 ; 
       done_cropping-mat100.2-0 ; 
       done_cropping-mat101.2-0 ; 
       done_cropping-mat105.2-0 ; 
       done_cropping-mat106.2-0 ; 
       done_cropping-mat107.2-0 ; 
       done_cropping-mat12.2-0 ; 
       done_cropping-mat13.2-0 ; 
       done_cropping-mat14.2-0 ; 
       done_cropping-mat15.2-0 ; 
       done_cropping-mat23.2-0 ; 
       done_cropping-mat24.2-0 ; 
       done_cropping-mat25.2-0 ; 
       done_cropping-mat34.4-0 ; 
       done_cropping-mat44.4-0 ; 
       done_cropping-mat59.4-0 ; 
       done_cropping-mat74.2-0 ; 
       done_cropping-mat75.2-0 ; 
       done_cropping-mat76.2-0 ; 
       done_cropping-mat77.2-0 ; 
       done_cropping-mat83.2-0 ; 
       done_cropping-mat84.2-0 ; 
       done_cropping-mat88.2-0 ; 
       done_cropping-mat90.2-0 ; 
       done_cropping-mat94.2-0 ; 
       done_cropping-mat96.2-0 ; 
       done_cropping-mat98.2-0 ; 
       fins-mat109.2-0 ; 
       fix_blinker-mat72.2-0 ; 
       scale-mat110.1-0 ; 
       scale-mat111.1-0 ; 
       scale-mat69.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13.33-0 ROOT ; 
       done_cropping-gear-null.1-0 ; 
       done_cropping-l-gun.1-0 ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-LL1.1-0 ; 
       done_cropping-LL2.1-0 ; 
       done_cropping-LR1.1-0 ; 
       done_cropping-LR2.1-0 ; 
       done_cropping-lsmoke.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-rsmoke.1-0 ; 
       done_cropping-rthrust.1-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSar1_1_3.1-0 ; 
       done_cropping-SSar1_1_8.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       13-scale.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       done_cropping-t2d100.4-0 ; 
       done_cropping-t2d105.3-0 ; 
       done_cropping-t2d107.3-0 ; 
       done_cropping-t2d109.4-0 ; 
       done_cropping-t2d11.4-0 ; 
       done_cropping-t2d110.5-0 ; 
       done_cropping-t2d111.3-0 ; 
       done_cropping-t2d114.2-0 ; 
       done_cropping-t2d115.2-0 ; 
       done_cropping-t2d116.2-0 ; 
       done_cropping-t2d12.4-0 ; 
       done_cropping-t2d13.4-0 ; 
       done_cropping-t2d14.4-0 ; 
       done_cropping-t2d30.6-0 ; 
       done_cropping-t2d61.6-0 ; 
       done_cropping-t2d64.4-0 ; 
       done_cropping-t2d65.6-0 ; 
       done_cropping-t2d89.3-0 ; 
       done_cropping-t2d90.3-0 ; 
       done_cropping-t2d91.3-0 ; 
       done_cropping-t2d96.4-0 ; 
       done_cropping-z.2-0 ; 
       fins-t2d118.3-0 ; 
       fix_blinker-t2d88.5-0 ; 
       scale-t2d119.1-0 ; 
       scale-t2d120.1-0 ; 
       scale-t2d85.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 21 110 ; 
       2 18 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 18 110 ; 
       11 21 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 5 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 5 110 ; 
       20 9 110 ; 
       21 9 110 ; 
       22 10 110 ; 
       23 11 110 ; 
       24 10 110 ; 
       25 11 110 ; 
       26 4 110 ; 
       27 5 110 ; 
       28 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 19 300 ; 
       1 24 300 ; 
       2 25 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 29 300 ; 
       7 31 300 ; 
       8 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       10 28 300 ; 
       11 27 300 ; 
       12 26 300 ; 
       13 1 300 ; 
       14 2 300 ; 
       15 30 300 ; 
       18 21 300 ; 
       21 22 300 ; 
       22 10 300 ; 
       23 11 300 ; 
       24 20 300 ; 
       25 23 300 ; 
       26 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 8 401 ; 
       5 9 401 ; 
       6 4 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 16 401 ; 
       16 17 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 21 401 ; 
       21 20 401 ; 
       22 0 401 ; 
       24 1 401 ; 
       25 2 401 ; 
       26 3 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 40 -6 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 -0.1039299 MPRFLG 0 ; 
       6 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 14.86935 -7.022033 0 USR MPRFLG 0 ; 
       16 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 20 -8 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 10 -4 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.36935 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 13.86935 -9.022034 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.36935 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 13.86935 -11.02203 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
