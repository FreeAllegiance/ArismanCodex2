SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.25-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.68-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.68-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       done_cropping-mat10.2-0 ; 
       done_cropping-mat105.2-0 ; 
       done_cropping-mat106.2-0 ; 
       done_cropping-mat107.2-0 ; 
       done_cropping-mat12.2-0 ; 
       done_cropping-mat13.2-0 ; 
       done_cropping-mat14.2-0 ; 
       done_cropping-mat15.2-0 ; 
       done_cropping-mat34.4-0 ; 
       done_cropping-mat44.4-0 ; 
       done_cropping-mat59.4-0 ; 
       done_cropping-mat74.2-0 ; 
       done_cropping-mat75.2-0 ; 
       done_cropping-mat76.2-0 ; 
       done_cropping-mat77.2-0 ; 
       done_cropping-mat84.2-0 ; 
       done_cropping-mat88.2-0 ; 
       done_cropping-mat94.2-0 ; 
       done_cropping-mat96.2-0 ; 
       fins-mat109.2-0 ; 
       fix_blinker-mat72.2-0 ; 
       static-mat110.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13.28-0 ROOT ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-rwingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       13-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       done_cropping-t2d100.3-0 ; 
       done_cropping-t2d105.2-0 ; 
       done_cropping-t2d107.2-0 ; 
       done_cropping-t2d11.3-0 ; 
       done_cropping-t2d114.2-0 ; 
       done_cropping-t2d115.2-0 ; 
       done_cropping-t2d116.2-0 ; 
       done_cropping-t2d12.3-0 ; 
       done_cropping-t2d13.3-0 ; 
       done_cropping-t2d14.3-0 ; 
       done_cropping-t2d30.5-0 ; 
       done_cropping-t2d61.5-0 ; 
       done_cropping-t2d64.3-0 ; 
       done_cropping-t2d65.5-0 ; 
       done_cropping-t2d89.3-0 ; 
       done_cropping-t2d90.3-0 ; 
       done_cropping-t2d91.3-0 ; 
       done_cropping-t2d96.3-0 ; 
       done_cropping-z.1-0 ; 
       fins-t2d118.2-0 ; 
       fix_blinker-t2d88.4-0 ; 
       static-t2d119.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 10 110 ; 
       2 9 110 ; 
       3 4 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 9 110 ; 
       8 10 110 ; 
       9 5 110 ; 
       10 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 14 300 ; 
       1 17 300 ; 
       2 18 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       3 21 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       7 20 300 ; 
       8 19 300 ; 
       9 15 300 ; 
       10 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 3 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 11 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 16 401 ; 
       14 18 401 ; 
       15 17 401 ; 
       16 0 401 ; 
       17 1 401 ; 
       18 2 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 13.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
