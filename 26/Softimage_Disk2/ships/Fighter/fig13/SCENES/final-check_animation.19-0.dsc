SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_animation-fig13.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.28-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.28-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 41     
       check_animation-mat10.1-0 ; 
       check_animation-mat100.2-0 ; 
       check_animation-mat101.1-0 ; 
       check_animation-mat102.1-0 ; 
       check_animation-mat12.1-0 ; 
       check_animation-mat13.1-0 ; 
       check_animation-mat14.1-0 ; 
       check_animation-mat15.1-0 ; 
       check_animation-mat23.1-0 ; 
       check_animation-mat24.1-0 ; 
       check_animation-mat25.1-0 ; 
       check_animation-mat34.1-0 ; 
       check_animation-mat44.1-0 ; 
       check_animation-mat52_1.1-0 ; 
       check_animation-mat53_1.1-0 ; 
       check_animation-mat54_1.1-0 ; 
       check_animation-mat59.1-0 ; 
       check_animation-mat65_1.1-0 ; 
       check_animation-mat65_2.1-0 ; 
       check_animation-mat66_1.1-0 ; 
       check_animation-mat66_2.1-0 ; 
       check_animation-mat67_1.1-0 ; 
       check_animation-mat67_2.1-0 ; 
       check_animation-mat74.1-0 ; 
       check_animation-mat75.1-0 ; 
       check_animation-mat76.1-0 ; 
       check_animation-mat77.1-0 ; 
       check_animation-mat82.1-0 ; 
       check_animation-mat83.1-0 ; 
       check_animation-mat84.1-0 ; 
       check_animation-mat85.1-0 ; 
       check_animation-mat86.1-0 ; 
       check_animation-mat87.1-0 ; 
       check_animation-mat88.1-0 ; 
       check_animation-mat89.1-0 ; 
       check_animation-mat90.1-0 ; 
       check_animation-mat94.1-0 ; 
       check_animation-mat95.1-0 ; 
       check_animation-mat96.1-0 ; 
       check_animation-mat97.1-0 ; 
       check_animation-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       check_animation-afuselg.3-0 ; 
       check_animation-bool10.1-0 ; 
       check_animation-bool11.1-0 ; 
       check_animation-cockpt.1-0 ; 
       check_animation-ffuselg.1-0 ; 
       check_animation-fig13.19-0 ROOT ; 
       check_animation-gear-null.1-0 ; 
       check_animation-lengine.1-0 ; 
       check_animation-lfinzzz.1-0 ; 
       check_animation-LL1.1-0 ; 
       check_animation-LL2.1-0 ; 
       check_animation-LR1.1-0 ; 
       check_animation-LR2.1-0 ; 
       check_animation-lthrust.1-0 ; 
       check_animation-lwingzz.1-0 ; 
       check_animation-rengine.1-0 ; 
       check_animation-rfinzzz.1-0 ; 
       check_animation-rfinzzz1_10.1-0 ; 
       check_animation-rfinzzz1_11.1-0 ; 
       check_animation-rthrust.5-0 ; 
       check_animation-rwingzz.1-0 ; 
       check_animation-SSal.1-0 ; 
       check_animation-SSar.1-0 ; 
       check_animation-SSar1_1_3.1-0 ; 
       check_animation-SSar1_1_8.1-0 ; 
       check_animation-SSf.2-0 ; 
       check_animation-trail.1-0 ; 
       check_animation-wepbar.3-0 ; 
       check_animation-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-check_animation.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       check_animation-t2d100.1-0 ; 
       check_animation-t2d101.1-0 ; 
       check_animation-t2d105.1-0 ; 
       check_animation-t2d106.1-0 ; 
       check_animation-t2d107.1-0 ; 
       check_animation-t2d108.1-0 ; 
       check_animation-t2d109.2-0 ; 
       check_animation-t2d11.1-0 ; 
       check_animation-t2d110.4-0 ; 
       check_animation-t2d111.1-0 ; 
       check_animation-t2d112.1-0 ; 
       check_animation-t2d12.1-0 ; 
       check_animation-t2d13.1-0 ; 
       check_animation-t2d14.1-0 ; 
       check_animation-t2d30.1-0 ; 
       check_animation-t2d53_1.1-0 ; 
       check_animation-t2d54_1.1-0 ; 
       check_animation-t2d61.1-0 ; 
       check_animation-t2d64.1-0 ; 
       check_animation-t2d65.1-0 ; 
       check_animation-t2d79_1.1-0 ; 
       check_animation-t2d82_1.1-0 ; 
       check_animation-t2d82_2.1-0 ; 
       check_animation-t2d83_1.1-0 ; 
       check_animation-t2d83_2.1-0 ; 
       check_animation-t2d84_1.1-0 ; 
       check_animation-t2d84_2.1-0 ; 
       check_animation-t2d89.1-0 ; 
       check_animation-t2d90.1-0 ; 
       check_animation-t2d91.1-0 ; 
       check_animation-t2d92.1-0 ; 
       check_animation-t2d95.1-0 ; 
       check_animation-t2d96.1-0 ; 
       check_animation-t2d97.1-0 ; 
       check_animation-t2d98.1-0 ; 
       check_animation-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 20 110 ; 
       2 14 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 14 110 ; 
       9 6 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 5 110 ; 
       16 20 110 ; 
       17 16 110 ; 
       18 8 110 ; 
       19 15 110 ; 
       20 15 110 ; 
       21 8 110 ; 
       22 16 110 ; 
       23 8 110 ; 
       24 16 110 ; 
       25 4 110 ; 
       26 5 110 ; 
       27 4 110 ; 
       28 27 110 ; 
       10 9 110 ; 
       11 6 110 ; 
       12 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 26 300 ; 
       1 36 300 ; 
       2 38 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 16 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       8 27 300 ; 
       9 40 300 ; 
       13 18 300 ; 
       13 20 300 ; 
       13 22 300 ; 
       14 29 300 ; 
       15 30 300 ; 
       15 31 300 ; 
       15 32 300 ; 
       16 34 300 ; 
       17 37 300 ; 
       18 39 300 ; 
       19 17 300 ; 
       19 19 300 ; 
       19 21 300 ; 
       20 33 300 ; 
       21 8 300 ; 
       22 9 300 ; 
       23 28 300 ; 
       24 35 300 ; 
       25 10 300 ; 
       27 13 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       10 1 300 ; 
       11 2 300 ; 
       12 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       4 7 401 ; 
       5 11 401 ; 
       6 12 401 ; 
       7 13 401 ; 
       11 14 401 ; 
       12 17 401 ; 
       13 20 401 ; 
       14 15 401 ; 
       15 16 401 ; 
       16 19 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       22 26 401 ; 
       23 27 401 ; 
       24 28 401 ; 
       25 29 401 ; 
       26 30 401 ; 
       27 31 401 ; 
       29 32 401 ; 
       30 33 401 ; 
       31 34 401 ; 
       32 35 401 ; 
       33 0 401 ; 
       34 1 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 4 401 ; 
       39 5 401 ; 
       40 6 401 ; 
       2 9 401 ; 
       1 8 401 ; 
       3 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 40 -6 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 MPRFLG 0 ; 
       3 SCHEM 45 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 MPRFLG 0 ; 
       16 SCHEM 35 -6 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 20 -8 0 MPRFLG 0 ; 
       24 SCHEM 35 -8 0 MPRFLG 0 ; 
       25 SCHEM 10 -4 0 MPRFLG 0 ; 
       26 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 46.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 25 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
