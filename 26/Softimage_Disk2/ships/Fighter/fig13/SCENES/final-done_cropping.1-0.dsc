SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       done_cropping-fig13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.37-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       done_cropping-mat10.1-0 ; 
       done_cropping-mat100.1-0 ; 
       done_cropping-mat101.1-0 ; 
       done_cropping-mat102.1-0 ; 
       done_cropping-mat103.1-0 ; 
       done_cropping-mat104.1-0 ; 
       done_cropping-mat105.1-0 ; 
       done_cropping-mat106.1-0 ; 
       done_cropping-mat107.1-0 ; 
       done_cropping-mat12.1-0 ; 
       done_cropping-mat13.1-0 ; 
       done_cropping-mat14.1-0 ; 
       done_cropping-mat15.1-0 ; 
       done_cropping-mat23.1-0 ; 
       done_cropping-mat24.1-0 ; 
       done_cropping-mat25.1-0 ; 
       done_cropping-mat34.1-0 ; 
       done_cropping-mat44.1-0 ; 
       done_cropping-mat52_1.1-0 ; 
       done_cropping-mat53_1.1-0 ; 
       done_cropping-mat54_1.1-0 ; 
       done_cropping-mat59.1-0 ; 
       done_cropping-mat74.1-0 ; 
       done_cropping-mat75.1-0 ; 
       done_cropping-mat76.1-0 ; 
       done_cropping-mat77.1-0 ; 
       done_cropping-mat83.1-0 ; 
       done_cropping-mat84.1-0 ; 
       done_cropping-mat88.1-0 ; 
       done_cropping-mat90.1-0 ; 
       done_cropping-mat94.1-0 ; 
       done_cropping-mat96.1-0 ; 
       done_cropping-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13.1-0 ROOT ; 
       done_cropping-gear-null.1-0 ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-lfinzzz.1-0 ; 
       done_cropping-lfinzzz1.1-0 ; 
       done_cropping-LL1.1-0 ; 
       done_cropping-LL2.1-0 ; 
       done_cropping-LR1.1-0 ; 
       done_cropping-LR2.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-rthrust.5-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSar1_1_3.1-0 ; 
       done_cropping-SSar1_1_8.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepbar.3-0 ; 
       done_cropping-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-done_cropping.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       done_cropping-t2d100.1-0 ; 
       done_cropping-t2d105.1-0 ; 
       done_cropping-t2d107.1-0 ; 
       done_cropping-t2d109.1-0 ; 
       done_cropping-t2d11.1-0 ; 
       done_cropping-t2d110.1-0 ; 
       done_cropping-t2d111.1-0 ; 
       done_cropping-t2d112.1-0 ; 
       done_cropping-t2d113.1-0 ; 
       done_cropping-t2d114.1-0 ; 
       done_cropping-t2d115.1-0 ; 
       done_cropping-t2d116.1-0 ; 
       done_cropping-t2d12.1-0 ; 
       done_cropping-t2d13.1-0 ; 
       done_cropping-t2d14.1-0 ; 
       done_cropping-t2d30.1-0 ; 
       done_cropping-t2d53_1.1-0 ; 
       done_cropping-t2d54_1.1-0 ; 
       done_cropping-t2d61.1-0 ; 
       done_cropping-t2d64.1-0 ; 
       done_cropping-t2d65.1-0 ; 
       done_cropping-t2d79_1.1-0 ; 
       done_cropping-t2d89.1-0 ; 
       done_cropping-t2d90.1-0 ; 
       done_cropping-t2d91.1-0 ; 
       done_cropping-t2d92.1-0 ; 
       done_cropping-t2d96.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 18 110 ; 
       2 16 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       9 16 110 ; 
       10 18 110 ; 
       11 6 110 ; 
       12 11 110 ; 
       13 6 110 ; 
       14 13 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       8 5 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 9 110 ; 
       20 10 110 ; 
       21 9 110 ; 
       22 10 110 ; 
       23 4 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 25 300 ; 
       1 30 300 ; 
       2 31 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 21 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       9 5 300 ; 
       10 4 300 ; 
       11 32 300 ; 
       12 1 300 ; 
       13 2 300 ; 
       14 3 300 ; 
       16 27 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       18 28 300 ; 
       19 13 300 ; 
       20 14 300 ; 
       21 26 300 ; 
       22 29 300 ; 
       23 15 300 ; 
       25 18 300 ; 
       25 19 300 ; 
       25 20 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 8 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       9 4 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       16 15 401 ; 
       17 18 401 ; 
       18 21 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 20 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       27 26 401 ; 
       28 0 401 ; 
       30 1 401 ; 
       31 2 401 ; 
       32 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 90 -6 0 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 MPRFLG 0 ; 
       3 SCHEM 77.5 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 61.25 -2 0 MPRFLG 0 ; 
       9 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 85 -6 0 MPRFLG 0 ; 
       11 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 MPRFLG 0 ; 
       13 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 45 -6 0 MPRFLG 0 ; 
       15 SCHEM 65 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 56.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 90 -2 0 MPRFLG 0 ; 
       17 SCHEM 80 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 MPRFLG 0 ; 
       20 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 85 -8 0 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 MPRFLG 0 ; 
       24 SCHEM 75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 101.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
