SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       engine-fig13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.34-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.34-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       engine-mat10.1-0 ; 
       engine-mat100.1-0 ; 
       engine-mat101.1-0 ; 
       engine-mat102.1-0 ; 
       engine-mat103.1-0 ; 
       engine-mat104.1-0 ; 
       engine-mat12.1-0 ; 
       engine-mat13.1-0 ; 
       engine-mat14.1-0 ; 
       engine-mat15.1-0 ; 
       engine-mat23.1-0 ; 
       engine-mat24.1-0 ; 
       engine-mat25.1-0 ; 
       engine-mat34.1-0 ; 
       engine-mat44.1-0 ; 
       engine-mat52_1.1-0 ; 
       engine-mat53_1.1-0 ; 
       engine-mat54_1.1-0 ; 
       engine-mat59.1-0 ; 
       engine-mat74.1-0 ; 
       engine-mat75.1-0 ; 
       engine-mat76.1-0 ; 
       engine-mat77.1-0 ; 
       engine-mat83.1-0 ; 
       engine-mat84.1-0 ; 
       engine-mat85.1-0 ; 
       engine-mat86.1-0 ; 
       engine-mat87.1-0 ; 
       engine-mat88.1-0 ; 
       engine-mat90.1-0 ; 
       engine-mat94.1-0 ; 
       engine-mat96.1-0 ; 
       engine-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       engine-afuselg.3-0 ; 
       engine-bool10.1-0 ; 
       engine-bool11.1-0 ; 
       engine-cockpt.1-0 ; 
       engine-ffuselg.1-0 ; 
       engine-fig13.1-0 ROOT ; 
       engine-gear-null.1-0 ; 
       engine-lengine.1-0 ; 
       engine-lfinzzz.1-0 ; 
       engine-lfinzzz1.1-0 ; 
       engine-LL1.1-0 ; 
       engine-LL2.1-0 ; 
       engine-LR1.1-0 ; 
       engine-LR2.1-0 ; 
       engine-lthrust.1-0 ; 
       engine-lwingzz.1-0 ; 
       engine-rengine.1-0 ; 
       engine-rthrust.5-0 ; 
       engine-rwingzz.1-0 ; 
       engine-SSal.1-0 ; 
       engine-SSar.1-0 ; 
       engine-SSar1_1_3.1-0 ; 
       engine-SSar1_1_8.1-0 ; 
       engine-SSf.2-0 ; 
       engine-trail.1-0 ; 
       engine-wepbar.3-0 ; 
       engine-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/bom01a ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-engine.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       engine-t2d100.1-0 ; 
       engine-t2d105.1-0 ; 
       engine-t2d107.1-0 ; 
       engine-t2d109.1-0 ; 
       engine-t2d11.1-0 ; 
       engine-t2d110.1-0 ; 
       engine-t2d111.1-0 ; 
       engine-t2d112.1-0 ; 
       engine-t2d113.1-0 ; 
       engine-t2d12.1-0 ; 
       engine-t2d13.1-0 ; 
       engine-t2d14.1-0 ; 
       engine-t2d30.1-0 ; 
       engine-t2d53_1.1-0 ; 
       engine-t2d54_1.1-0 ; 
       engine-t2d61.1-0 ; 
       engine-t2d64.1-0 ; 
       engine-t2d65.1-0 ; 
       engine-t2d79_1.1-0 ; 
       engine-t2d89.1-0 ; 
       engine-t2d90.1-0 ; 
       engine-t2d91.1-0 ; 
       engine-t2d92.1-0 ; 
       engine-t2d96.1-0 ; 
       engine-t2d97.1-0 ; 
       engine-t2d98.1-0 ; 
       engine-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 18 110 ; 
       2 15 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 15 110 ; 
       9 18 110 ; 
       10 6 110 ; 
       11 10 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 8 110 ; 
       20 9 110 ; 
       21 8 110 ; 
       22 9 110 ; 
       23 4 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 22 300 ; 
       1 30 300 ; 
       2 31 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 18 300 ; 
       7 19 300 ; 
       7 20 300 ; 
       7 21 300 ; 
       8 5 300 ; 
       9 4 300 ; 
       10 32 300 ; 
       11 1 300 ; 
       12 2 300 ; 
       13 3 300 ; 
       15 24 300 ; 
       16 25 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       18 28 300 ; 
       19 10 300 ; 
       20 11 300 ; 
       21 23 300 ; 
       22 29 300 ; 
       23 12 300 ; 
       25 15 300 ; 
       25 16 300 ; 
       25 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 8 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 16 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       6 4 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       9 11 401 ; 
       13 12 401 ; 
       14 15 401 ; 
       15 18 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 17 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 0 401 ; 
       30 1 401 ; 
       31 2 401 ; 
       32 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 80 -8 0 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 MPRFLG 0 ; 
       3 SCHEM 95 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 56.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 75 -8 0 MPRFLG 0 ; 
       10 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -8 0 MPRFLG 0 ; 
       12 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 MPRFLG 0 ; 
       14 SCHEM 60.53601 -6.214407 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 51.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 80 -4 0 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 45 -10 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 75 -10 0 MPRFLG 0 ; 
       23 SCHEM 25 -6 0 MPRFLG 0 ; 
       24 SCHEM 92.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 68.03601 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 63.03601 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65.53601 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 68.03601 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 63.03601 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65.53601 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 96.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
