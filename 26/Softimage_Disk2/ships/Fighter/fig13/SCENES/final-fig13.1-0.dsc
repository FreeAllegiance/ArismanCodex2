SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 6     
       fig13-ffuselg.1-0 ; 
       fig13-gear-null.1-0 ; 
       fig13-lengine.1-0 ; 
       fig13-LLf2Shp.1-0 ; 
       fig13-LLlShp.1-0 ; 
       fig13-rengine.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       fig13-light1_1_1_1.1-0 ROOT ; 
       fig13-light2_1_1_1.1-0 ROOT ; 
       fig13-light3_1_1_1.1-0 ROOT ; 
       fig13-light4_1_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       fig13-mat10.1-0 ; 
       fig13-mat12.1-0 ; 
       fig13-mat13.1-0 ; 
       fig13-mat14.1-0 ; 
       fig13-mat15.1-0 ; 
       fig13-mat23.1-0 ; 
       fig13-mat24.1-0 ; 
       fig13-mat25.1-0 ; 
       fig13-mat34.1-0 ; 
       fig13-mat44.1-0 ; 
       fig13-mat52.1-0 ; 
       fig13-mat52_1.1-0 ; 
       fig13-mat53.1-0 ; 
       fig13-mat53_1.1-0 ; 
       fig13-mat54.1-0 ; 
       fig13-mat54_1.1-0 ; 
       fig13-mat59.1-0 ; 
       fig13-mat65_1.1-0 ; 
       fig13-mat65_2.1-0 ; 
       fig13-mat66_1.1-0 ; 
       fig13-mat66_2.1-0 ; 
       fig13-mat67_1.1-0 ; 
       fig13-mat67_2.1-0 ; 
       fig13-mat74.1-0 ; 
       fig13-mat75.1-0 ; 
       fig13-mat76.1-0 ; 
       fig13-mat77.1-0 ; 
       fig13-mat82.1-0 ; 
       fig13-mat83.1-0 ; 
       fig13-mat84.1-0 ; 
       fig13-mat85.1-0 ; 
       fig13-mat86.1-0 ; 
       fig13-mat87.1-0 ; 
       fig13-mat88.1-0 ; 
       fig13-mat89.1-0 ; 
       fig13-mat90.1-0 ; 
       fig13-mat91.1-0 ; 
       fig13-mat92.1-0 ; 
       fig13-mat93.1-0 ; 
       fig13-mat94.1-0 ; 
       fig13-mat95.1-0 ; 
       fig13-mat96.1-0 ; 
       fig13-mat97.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       fig13-afuselg.3-0 ; 
       fig13-bool10.1-0 ; 
       fig13-bool11.1-0 ; 
       fig13-ffuselg.1-0 ROOT ; 
       fig13-gear-null.1-0 ROOT ; 
       fig13-lengine.1-0 ROOT ; 
       fig13-lfinzzz.1-0 ; 
       fig13-LLf2.16-0 ; 
       fig13-LLl.5-0 ; 
       fig13-lthrust.1-0 ; 
       fig13-lwingzz.1-0 ; 
       fig13-path1_1.1-0 ROOT ; 
       fig13-path10.1-0 ; 
       fig13-path12.1-0 ; 
       fig13-path13.1-0 ; 
       fig13-path14.1-0 ; 
       fig13-path15.1-0 ; 
       fig13-path16.1-0 ; 
       fig13-path17.1-0 ; 
       fig13-path2_1.1-0 ROOT ; 
       fig13-path3_1.1-0 ROOT ; 
       fig13-path4.1-0 ; 
       fig13-path5.1-0 ; 
       fig13-path6.1-0 ; 
       fig13-path7_1.1-0 ROOT ; 
       fig13-path8.1-0 ; 
       fig13-path9.1-0 ; 
       fig13-rengine.1-0 ROOT ; 
       fig13-rfinzzz.1-0 ; 
       fig13-rfinzzz1_10.1-0 ; 
       fig13-rfinzzz1_11.1-0 ; 
       fig13-rthrust.5-0 ; 
       fig13-rwingzz.1-0 ; 
       fig13-SSal.1-0 ; 
       fig13-SSar.1-0 ; 
       fig13-SSar1_1_3.1-0 ; 
       fig13-SSar1_1_8.1-0 ; 
       fig13-SSf.2-0 ; 
       fig13-wepbar.3-0 ; 
       fig13-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Bomber/pigmy_scout/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-fig13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 2     
       fig13-LLf2Shp.1-0 ; 
       fig13-LLlShp.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       fig13-t2d100.1-0 ; 
       fig13-t2d101.1-0 ; 
       fig13-t2d102.1-0 ; 
       fig13-t2d103.1-0 ; 
       fig13-t2d104.1-0 ; 
       fig13-t2d105.1-0 ; 
       fig13-t2d106.1-0 ; 
       fig13-t2d107.1-0 ; 
       fig13-t2d108.1-0 ; 
       fig13-t2d11.1-0 ; 
       fig13-t2d12.1-0 ; 
       fig13-t2d13.1-0 ; 
       fig13-t2d14.1-0 ; 
       fig13-t2d30.1-0 ; 
       fig13-t2d53.1-0 ; 
       fig13-t2d53_1.1-0 ; 
       fig13-t2d54.1-0 ; 
       fig13-t2d54_1.1-0 ; 
       fig13-t2d61.1-0 ; 
       fig13-t2d64.1-0 ; 
       fig13-t2d65.1-0 ; 
       fig13-t2d79.1-0 ; 
       fig13-t2d79_1.1-0 ; 
       fig13-t2d82_1.1-0 ; 
       fig13-t2d82_2.1-0 ; 
       fig13-t2d83_1.1-0 ; 
       fig13-t2d83_2.1-0 ; 
       fig13-t2d84_1.1-0 ; 
       fig13-t2d84_2.1-0 ; 
       fig13-t2d89.1-0 ; 
       fig13-t2d90.1-0 ; 
       fig13-t2d91.1-0 ; 
       fig13-t2d92.1-0 ; 
       fig13-t2d95.1-0 ; 
       fig13-t2d96.1-0 ; 
       fig13-t2d97.1-0 ; 
       fig13-t2d98.1-0 ; 
       fig13-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 32 110 ; 
       1 21 112 ; 
       1 21 112 2 ; 
       2 10 110 ; 
       2 25 112 ; 
       2 25 112 2 ; 
       5 24 112 ; 
       5 24 112 2 ; 
       6 10 110 ; 
       6 26 112 ; 
       6 26 112 2 ; 
       9 5 110 ; 
       10 5 110 ; 
       10 25 112 ; 
       10 25 112 2 ; 
       12 6 110 ; 
       12 18 112 ; 
       12 18 112 2 ; 
       13 27 110 ; 
       14 32 110 ; 
       15 28 110 ; 
       16 5 110 ; 
       17 10 110 ; 
       18 6 110 ; 
       21 27 110 ; 
       21 13 112 ; 
       21 13 112 2 ; 
       22 32 110 ; 
       22 14 112 ; 
       22 14 112 2 ; 
       23 28 110 ; 
       23 15 112 ; 
       23 15 112 2 ; 
       25 5 110 ; 
       25 16 112 ; 
       25 16 112 2 ; 
       26 10 110 ; 
       26 17 112 ; 
       26 17 112 2 ; 
       27 20 112 ; 
       27 20 112 2 ; 
       28 32 110 ; 
       28 22 112 ; 
       28 22 112 2 ; 
       29 28 110 ; 
       29 22 112 ; 
       29 22 112 2 ; 
       30 6 110 ; 
       30 26 112 ; 
       30 26 112 2 ; 
       31 27 110 ; 
       32 27 110 ; 
       32 21 112 ; 
       32 21 112 2 ; 
       33 6 110 ; 
       34 28 110 ; 
       35 6 110 ; 
       35 12 112 ; 
       35 12 112 2 ; 
       36 28 110 ; 
       36 23 112 ; 
       36 23 112 2 ; 
       37 3 110 ; 
       38 3 110 ; 
       39 38 110 ; 
       7 4 110 ; 
       8 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       7 0 200 ; 
       8 1 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 26 300 ; 
       1 39 300 ; 
       2 41 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 16 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       6 27 300 ; 
       9 18 300 ; 
       9 20 300 ; 
       9 22 300 ; 
       10 29 300 ; 
       27 30 300 ; 
       27 31 300 ; 
       27 32 300 ; 
       28 34 300 ; 
       29 40 300 ; 
       30 42 300 ; 
       31 17 300 ; 
       31 19 300 ; 
       31 21 300 ; 
       32 33 300 ; 
       33 5 300 ; 
       34 6 300 ; 
       35 28 300 ; 
       36 35 300 ; 
       37 7 300 ; 
       38 11 300 ; 
       38 13 300 ; 
       38 15 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 38 300 ; 
       8 10 300 ; 
       8 12 300 ; 
       8 14 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
       5 2 15000 ; 
       27 5 15000 ; 
       4 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 11 401 ; 
       4 12 401 ; 
       8 13 401 ; 
       9 18 401 ; 
       10 21 401 ; 
       11 22 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 20 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       25 31 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       29 34 401 ; 
       30 35 401 ; 
       31 36 401 ; 
       32 37 401 ; 
       33 0 401 ; 
       34 1 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 4 401 ; 
       39 5 401 ; 
       40 6 401 ; 
       41 7 401 ; 
       42 8 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       1 4 15010 ; 
       0 3 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 17 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM -0.555552 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 1.944448 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 4.444448 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 6.944448 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -9.805553 -12 0 MPRFLG 0 ; 
       1 SCHEM 59.73766 -4 0 MPRFLG 0 ; 
       2 SCHEM 25.98765 -4 0 MPRFLG 0 ; 
       3 SCHEM -7.305553 -10 0 SRT 0.5938728 0.7354548 0.6542871 -0.6003256 -7.849725 0.5980313 9.918994e-005 0.5539897 -0.6834707 MPRFLG 0 ; 
       5 SCHEM 20.98766 0 0 USR SRT 1 1 1 0 0 0.4700001 0.2039009 0.5528831 -0.3446878 MPRFLG 0 ; 
       6 SCHEM 15.98767 -4 0 USR MPRFLG 0 ; 
       9 SCHEM 30.98766 -2 0 MPRFLG 0 ; 
       10 SCHEM 18.48766 -2 0 USR MPRFLG 0 ; 
       12 SCHEM 15.98767 -6 0 USR MPRFLG 0 ; 
       13 SCHEM 62.23766 -2 0 USR MPRFLG 0 ; 
       14 SCHEM 57.23766 -4 0 USR MPRFLG 0 ; 
       15 SCHEM 52.23766 -6 0 USR MPRFLG 0 ; 
       16 SCHEM 28.48766 -2 0 USR MPRFLG 0 ; 
       17 SCHEM 23.48765 -4 0 USR MPRFLG 0 ; 
       18 SCHEM 18.48766 -6 0 USR MPRFLG 0 ; 
       21 SCHEM 59.73766 -2 0 USR MPRFLG 0 ; 
       22 SCHEM 54.73766 -4 0 USR MPRFLG 0 ; 
       23 SCHEM 49.73766 -6 0 USR MPRFLG 0 ; 
       25 SCHEM 25.98766 -2 0 USR MPRFLG 0 ; 
       26 SCHEM 20.98766 -4 0 USR MPRFLG 0 ; 
       27 SCHEM 54.73766 0 0 USR SRT 1 1 1 0 3.141593 -0.4700001 -0.2168285 0.5528833 -0.3446878 MPRFLG 0 ; 
       28 SCHEM 49.73766 -4 0 USR MPRFLG 0 ; 
       29 SCHEM 54.73766 -6 0 MPRFLG 0 ; 
       30 SCHEM 20.98766 -6 0 MPRFLG 0 ; 
       31 SCHEM 42.23766 -2 0 MPRFLG 0 ; 
       32 SCHEM 52.23766 -2 0 USR MPRFLG 0 ; 
       33 SCHEM 10.98767 -6 0 MPRFLG 0 ; 
       34 SCHEM 44.73766 -6 0 MPRFLG 0 ; 
       35 SCHEM 13.48768 -6 0 USR MPRFLG 0 ; 
       36 SCHEM 47.23766 -6 0 USR MPRFLG 0 ; 
       37 SCHEM -4.805553 -12 0 MPRFLG 0 ; 
       38 SCHEM -7.305553 -12 0 MPRFLG 0 ; 
       39 SCHEM -7.305553 -14 0 MPRFLG 0 ; 
       4 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 37.23767 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       19 SCHEM 39.73766 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       20 SCHEM 0 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       24 SCHEM 2.5 0 0 USR SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9.98767 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 43.73766 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -5.805553 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -3.305553 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -3.305553 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -5.805553 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -5.805553 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -5.805553 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -3.305553 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.23766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 30.98766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.23766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 30.98766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.23766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 30.98766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 32.48766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 32.48766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 32.48766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM -10.80555 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.48766 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 12.48768 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.48765 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 63.73766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 63.73766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 63.73766 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 61.23766 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 56.23766 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.23766 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 58.73766 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 53.73766 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24.98765 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19.98766 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.23766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.23766 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 58.73766 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 53.73766 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24.98765 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19.98766 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -3.305553 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM -5.805553 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -5.805553 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM -3.305553 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -3.305553 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM -5.805553 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.23766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 30.98766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 41.23766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 30.98766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 41.23766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 30.98766 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 32.48766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 32.48766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 32.48766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM -10.80555 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 22.48766 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 27.48765 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 63.73766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 63.73766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 63.73766 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM -3.305553 -12 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 32.48766 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 63.73766 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 50 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
