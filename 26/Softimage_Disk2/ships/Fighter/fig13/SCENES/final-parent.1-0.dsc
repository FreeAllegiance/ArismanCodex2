SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       parent-fig13.1-0 ; 
       parent-LLf2Shp.1-0 ; 
       parent-LLlShp.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       parent-mat10.1-0 ; 
       parent-mat12.1-0 ; 
       parent-mat13.1-0 ; 
       parent-mat14.1-0 ; 
       parent-mat15.1-0 ; 
       parent-mat23.1-0 ; 
       parent-mat24.1-0 ; 
       parent-mat25.1-0 ; 
       parent-mat34.1-0 ; 
       parent-mat44.1-0 ; 
       parent-mat52.1-0 ; 
       parent-mat52_1.1-0 ; 
       parent-mat53.1-0 ; 
       parent-mat53_1.1-0 ; 
       parent-mat54.1-0 ; 
       parent-mat54_1.1-0 ; 
       parent-mat59.1-0 ; 
       parent-mat65_1.1-0 ; 
       parent-mat65_2.1-0 ; 
       parent-mat66_1.1-0 ; 
       parent-mat66_2.1-0 ; 
       parent-mat67_1.1-0 ; 
       parent-mat67_2.1-0 ; 
       parent-mat74.1-0 ; 
       parent-mat75.1-0 ; 
       parent-mat76.1-0 ; 
       parent-mat77.1-0 ; 
       parent-mat82.1-0 ; 
       parent-mat83.1-0 ; 
       parent-mat84.1-0 ; 
       parent-mat85.1-0 ; 
       parent-mat86.1-0 ; 
       parent-mat87.1-0 ; 
       parent-mat88.1-0 ; 
       parent-mat89.1-0 ; 
       parent-mat90.1-0 ; 
       parent-mat91.1-0 ; 
       parent-mat92.1-0 ; 
       parent-mat93.1-0 ; 
       parent-mat94.1-0 ; 
       parent-mat95.1-0 ; 
       parent-mat96.1-0 ; 
       parent-mat97.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       parent-afuselg.3-0 ; 
       parent-bool10.1-0 ; 
       parent-bool11.1-0 ; 
       parent-ffuselg.1-0 ; 
       parent-fig13.1-0 ROOT ; 
       parent-gear-null.1-0 ; 
       parent-lengine.1-0 ; 
       parent-lfinzzz.1-0 ; 
       parent-LLf2.16-0 ; 
       parent-LLl.5-0 ; 
       parent-lthrust.1-0 ; 
       parent-lwingzz.1-0 ; 
       parent-path1_1.1-0 ROOT ; 
       parent-path10.1-0 ; 
       parent-path12.1-0 ; 
       parent-path13.1-0 ; 
       parent-path14.1-0 ; 
       parent-path15.1-0 ; 
       parent-path16.1-0 ; 
       parent-path17.1-0 ; 
       parent-path2_1.1-0 ROOT ; 
       parent-path3_1.1-0 ROOT ; 
       parent-path4.1-0 ; 
       parent-path5.1-0 ; 
       parent-path6.1-0 ; 
       parent-path7_1.1-0 ROOT ; 
       parent-path8.1-0 ; 
       parent-path9.1-0 ; 
       parent-rengine.1-0 ; 
       parent-rfinzzz.1-0 ; 
       parent-rfinzzz1_10.1-0 ; 
       parent-rfinzzz1_11.1-0 ; 
       parent-rthrust.5-0 ; 
       parent-rwingzz.1-0 ; 
       parent-SSal.1-0 ; 
       parent-SSar.1-0 ; 
       parent-SSar1_1_3.1-0 ; 
       parent-SSar1_1_8.1-0 ; 
       parent-SSf.2-0 ; 
       parent-wepbar.3-0 ; 
       parent-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-parent.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 2     
       parent-LLf2Shp.1-0 ; 
       parent-LLlShp.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       parent-t2d100.1-0 ; 
       parent-t2d101.1-0 ; 
       parent-t2d102.1-0 ; 
       parent-t2d103.1-0 ; 
       parent-t2d104.1-0 ; 
       parent-t2d105.1-0 ; 
       parent-t2d106.1-0 ; 
       parent-t2d107.1-0 ; 
       parent-t2d108.1-0 ; 
       parent-t2d11.1-0 ; 
       parent-t2d12.1-0 ; 
       parent-t2d13.1-0 ; 
       parent-t2d14.1-0 ; 
       parent-t2d30.1-0 ; 
       parent-t2d53.1-0 ; 
       parent-t2d53_1.1-0 ; 
       parent-t2d54.1-0 ; 
       parent-t2d54_1.1-0 ; 
       parent-t2d61.1-0 ; 
       parent-t2d64.1-0 ; 
       parent-t2d65.1-0 ; 
       parent-t2d79.1-0 ; 
       parent-t2d79_1.1-0 ; 
       parent-t2d82_1.1-0 ; 
       parent-t2d82_2.1-0 ; 
       parent-t2d83_1.1-0 ; 
       parent-t2d83_2.1-0 ; 
       parent-t2d84_1.1-0 ; 
       parent-t2d84_2.1-0 ; 
       parent-t2d89.1-0 ; 
       parent-t2d90.1-0 ; 
       parent-t2d91.1-0 ; 
       parent-t2d92.1-0 ; 
       parent-t2d95.1-0 ; 
       parent-t2d96.1-0 ; 
       parent-t2d97.1-0 ; 
       parent-t2d98.1-0 ; 
       parent-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 33 110 ; 
       1 22 112 ; 
       1 22 112 2 ; 
       2 11 110 ; 
       2 26 112 ; 
       2 26 112 2 ; 
       3 4 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       6 25 112 ; 
       6 25 112 2 ; 
       7 11 110 ; 
       7 27 112 ; 
       7 27 112 2 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       11 26 112 ; 
       11 26 112 2 ; 
       13 7 110 ; 
       13 19 112 ; 
       13 19 112 2 ; 
       14 28 110 ; 
       15 33 110 ; 
       16 29 110 ; 
       17 6 110 ; 
       18 11 110 ; 
       19 7 110 ; 
       22 28 110 ; 
       22 14 112 ; 
       22 14 112 2 ; 
       23 33 110 ; 
       23 15 112 ; 
       23 15 112 2 ; 
       24 29 110 ; 
       24 16 112 ; 
       24 16 112 2 ; 
       26 6 110 ; 
       26 17 112 ; 
       26 17 112 2 ; 
       27 11 110 ; 
       27 18 112 ; 
       27 18 112 2 ; 
       28 4 110 ; 
       28 21 112 ; 
       28 21 112 2 ; 
       29 33 110 ; 
       29 23 112 ; 
       29 23 112 2 ; 
       30 29 110 ; 
       30 23 112 ; 
       30 23 112 2 ; 
       31 7 110 ; 
       31 27 112 ; 
       31 27 112 2 ; 
       32 28 110 ; 
       33 28 110 ; 
       33 22 112 ; 
       33 22 112 2 ; 
       34 7 110 ; 
       35 29 110 ; 
       36 7 110 ; 
       36 13 112 ; 
       36 13 112 2 ; 
       37 29 110 ; 
       37 24 112 ; 
       37 24 112 2 ; 
       38 3 110 ; 
       39 3 110 ; 
       40 39 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       8 0 200 ; 
       9 1 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 26 300 ; 
       1 39 300 ; 
       2 41 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 16 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       7 27 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       9 14 300 ; 
       10 18 300 ; 
       10 20 300 ; 
       10 22 300 ; 
       11 29 300 ; 
       28 30 300 ; 
       28 31 300 ; 
       28 32 300 ; 
       29 34 300 ; 
       30 40 300 ; 
       31 42 300 ; 
       32 17 300 ; 
       32 19 300 ; 
       32 21 300 ; 
       33 33 300 ; 
       34 5 300 ; 
       35 6 300 ; 
       36 28 300 ; 
       37 35 300 ; 
       38 7 300 ; 
       39 11 300 ; 
       39 13 300 ; 
       39 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 11 401 ; 
       4 12 401 ; 
       8 13 401 ; 
       9 18 401 ; 
       10 21 401 ; 
       11 22 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 20 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       25 31 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       29 34 401 ; 
       30 35 401 ; 
       31 36 401 ; 
       32 37 401 ; 
       33 0 401 ; 
       34 1 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 4 401 ; 
       39 5 401 ; 
       40 6 401 ; 
       41 7 401 ; 
       42 8 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 1 15010 ; 
       1 2 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 34.99999 -6 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 MPRFLG 0 ; 
       7 SCHEM 25.00001 -6 0 USR MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 40 -4 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 USR MPRFLG 0 ; 
       12 SCHEM 67.5 0 0 SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       13 SCHEM 25.00001 -8 0 USR MPRFLG 0 ; 
       14 SCHEM 65 -4 0 USR MPRFLG 0 ; 
       15 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       16 SCHEM 55 -8 0 USR MPRFLG 0 ; 
       17 SCHEM 37.5 -4 0 USR MPRFLG 0 ; 
       18 SCHEM 32.49999 -6 0 USR MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 USR MPRFLG 0 ; 
       20 SCHEM 70 0 0 SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       21 SCHEM 72.5 0 0 SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       22 SCHEM 62.5 -4 0 USR MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       24 SCHEM 52.5 -8 0 USR MPRFLG 0 ; 
       25 SCHEM 75 0 0 SRT 1 1 1 0 0 0 0 0 -0.2947708 MPRFLG 0 ; 
       26 SCHEM 35 -4 0 USR MPRFLG 0 ; 
       27 SCHEM 30 -6 0 USR MPRFLG 0 ; 
       28 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 52.5 -6 0 USR MPRFLG 0 ; 
       30 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 30 -8 0 MPRFLG 0 ; 
       32 SCHEM 45 -4 0 MPRFLG 0 ; 
       33 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       34 SCHEM 20.00001 -8 0 MPRFLG 0 ; 
       35 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 22.50002 -8 0 USR MPRFLG 0 ; 
       37 SCHEM 50 -8 0 USR MPRFLG 0 ; 
       38 SCHEM 10 -4 0 MPRFLG 0 ; 
       39 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       40 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19.00001 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.50002 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.49999 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 33.99999 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 33.99999 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.49999 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 14 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
