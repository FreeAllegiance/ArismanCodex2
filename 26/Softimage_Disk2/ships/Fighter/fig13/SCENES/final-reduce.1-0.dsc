SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       reduce-fig13.1-0 ; 
       reduce-LLf2Shp.1-0 ; 
       reduce-LLlShp.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       reduce-mat10.1-0 ; 
       reduce-mat12.1-0 ; 
       reduce-mat13.1-0 ; 
       reduce-mat14.1-0 ; 
       reduce-mat15.1-0 ; 
       reduce-mat23.1-0 ; 
       reduce-mat24.1-0 ; 
       reduce-mat25.1-0 ; 
       reduce-mat34.1-0 ; 
       reduce-mat44.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat52_1.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat53_1.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat54_1.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat65_1.1-0 ; 
       reduce-mat65_2.1-0 ; 
       reduce-mat66_1.1-0 ; 
       reduce-mat66_2.1-0 ; 
       reduce-mat67_1.1-0 ; 
       reduce-mat67_2.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat82.1-0 ; 
       reduce-mat83.1-0 ; 
       reduce-mat84.1-0 ; 
       reduce-mat85.1-0 ; 
       reduce-mat86.1-0 ; 
       reduce-mat87.1-0 ; 
       reduce-mat88.1-0 ; 
       reduce-mat89.1-0 ; 
       reduce-mat90.1-0 ; 
       reduce-mat91.1-0 ; 
       reduce-mat92.1-0 ; 
       reduce-mat93.1-0 ; 
       reduce-mat94.1-0 ; 
       reduce-mat95.1-0 ; 
       reduce-mat96.1-0 ; 
       reduce-mat97.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       reduce-afuselg.3-0 ; 
       reduce-bool10.1-0 ; 
       reduce-bool11.1-0 ; 
       reduce-ffuselg.1-0 ; 
       reduce-fig13.1-0 ROOT ; 
       reduce-gear-null.1-0 ; 
       reduce-lengine.1-0 ; 
       reduce-lfinzzz.1-0 ; 
       reduce-LLf2.16-0 ; 
       reduce-LLl.5-0 ; 
       reduce-lthrust.1-0 ; 
       reduce-lwingzz.1-0 ; 
       reduce-path10.1-0 ; 
       reduce-path12.1-0 ; 
       reduce-path13.1-0 ; 
       reduce-path14.1-0 ; 
       reduce-path15.1-0 ; 
       reduce-path16.1-0 ; 
       reduce-path17.1-0 ; 
       reduce-path4.1-0 ; 
       reduce-path5.1-0 ; 
       reduce-path6.1-0 ; 
       reduce-path8.1-0 ; 
       reduce-path9.1-0 ; 
       reduce-rengine.1-0 ; 
       reduce-rfinzzz.1-0 ; 
       reduce-rfinzzz1_10.1-0 ; 
       reduce-rfinzzz1_11.1-0 ; 
       reduce-rthrust.5-0 ; 
       reduce-rwingzz.1-0 ; 
       reduce-SSal.1-0 ; 
       reduce-SSar.1-0 ; 
       reduce-SSar1_1_3.1-0 ; 
       reduce-SSar1_1_8.1-0 ; 
       reduce-SSf.2-0 ; 
       reduce-wepbar.3-0 ; 
       reduce-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/bom01a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-reduce.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES NBELEM 2     
       reduce-LLf2Shp.1-0 ; 
       reduce-LLlShp.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       reduce-t2d100.1-0 ; 
       reduce-t2d101.1-0 ; 
       reduce-t2d102.1-0 ; 
       reduce-t2d103.1-0 ; 
       reduce-t2d104.1-0 ; 
       reduce-t2d105.1-0 ; 
       reduce-t2d106.1-0 ; 
       reduce-t2d107.1-0 ; 
       reduce-t2d108.1-0 ; 
       reduce-t2d11.1-0 ; 
       reduce-t2d12.1-0 ; 
       reduce-t2d13.1-0 ; 
       reduce-t2d14.1-0 ; 
       reduce-t2d30.1-0 ; 
       reduce-t2d53.1-0 ; 
       reduce-t2d53_1.1-0 ; 
       reduce-t2d54.1-0 ; 
       reduce-t2d54_1.1-0 ; 
       reduce-t2d61.1-0 ; 
       reduce-t2d64.1-0 ; 
       reduce-t2d65.1-0 ; 
       reduce-t2d79.1-0 ; 
       reduce-t2d79_1.1-0 ; 
       reduce-t2d82_1.1-0 ; 
       reduce-t2d82_2.1-0 ; 
       reduce-t2d83_1.1-0 ; 
       reduce-t2d83_2.1-0 ; 
       reduce-t2d84_1.1-0 ; 
       reduce-t2d84_2.1-0 ; 
       reduce-t2d89.1-0 ; 
       reduce-t2d90.1-0 ; 
       reduce-t2d91.1-0 ; 
       reduce-t2d92.1-0 ; 
       reduce-t2d95.1-0 ; 
       reduce-t2d96.1-0 ; 
       reduce-t2d97.1-0 ; 
       reduce-t2d98.1-0 ; 
       reduce-t2d99.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 29 110 ; 
       1 19 112 ; 
       1 19 112 2 ; 
       2 11 110 ; 
       2 22 112 ; 
       2 22 112 2 ; 
       3 4 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 11 110 ; 
       7 23 112 ; 
       7 23 112 2 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       11 22 112 ; 
       11 22 112 2 ; 
       12 7 110 ; 
       12 18 112 ; 
       12 18 112 2 ; 
       13 24 110 ; 
       14 29 110 ; 
       15 25 110 ; 
       16 6 110 ; 
       17 11 110 ; 
       18 7 110 ; 
       19 24 110 ; 
       19 13 112 ; 
       19 13 112 2 ; 
       20 29 110 ; 
       20 14 112 ; 
       20 14 112 2 ; 
       21 25 110 ; 
       21 15 112 ; 
       21 15 112 2 ; 
       22 6 110 ; 
       22 16 112 ; 
       22 16 112 2 ; 
       23 11 110 ; 
       23 17 112 ; 
       23 17 112 2 ; 
       24 4 110 ; 
       25 29 110 ; 
       25 20 112 ; 
       25 20 112 2 ; 
       26 25 110 ; 
       26 20 112 ; 
       26 20 112 2 ; 
       27 7 110 ; 
       27 23 112 ; 
       27 23 112 2 ; 
       28 24 110 ; 
       29 24 110 ; 
       29 19 112 ; 
       29 19 112 2 ; 
       30 7 110 ; 
       31 25 110 ; 
       32 7 110 ; 
       32 12 112 ; 
       32 12 112 2 ; 
       33 25 110 ; 
       33 21 112 ; 
       33 21 112 2 ; 
       34 3 110 ; 
       35 3 110 ; 
       36 35 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER SHAPES 
       8 0 200 ; 
       9 1 200 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 26 300 ; 
       1 39 300 ; 
       2 41 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 16 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       7 27 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       9 14 300 ; 
       10 18 300 ; 
       10 20 300 ; 
       10 22 300 ; 
       11 29 300 ; 
       24 30 300 ; 
       24 31 300 ; 
       24 32 300 ; 
       25 34 300 ; 
       26 40 300 ; 
       27 42 300 ; 
       28 17 300 ; 
       28 19 300 ; 
       28 21 300 ; 
       29 33 300 ; 
       30 5 300 ; 
       31 6 300 ; 
       32 28 300 ; 
       33 35 300 ; 
       34 7 300 ; 
       35 11 300 ; 
       35 13 300 ; 
       35 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 11 401 ; 
       4 12 401 ; 
       8 13 401 ; 
       9 18 401 ; 
       10 21 401 ; 
       11 22 401 ; 
       12 14 401 ; 
       13 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 20 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 28 401 ; 
       23 29 401 ; 
       24 30 401 ; 
       25 31 401 ; 
       26 32 401 ; 
       27 33 401 ; 
       29 34 401 ; 
       30 35 401 ; 
       31 36 401 ; 
       32 37 401 ; 
       33 0 401 ; 
       34 1 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 4 401 ; 
       39 5 401 ; 
       40 6 401 ; 
       41 7 401 ; 
       42 8 401 ; 
    EndOfCHAPTER 

    CHAPTER SHAPES CHAPTER ANIMATION 
       0 1 15010 ; 
       1 2 15010 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 34.99999 -6 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 MPRFLG 0 ; 
       7 SCHEM 25.00001 -6 0 USR MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 40 -4 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 USR MPRFLG 0 ; 
       12 SCHEM 25.00001 -8 0 USR MPRFLG 0 ; 
       13 SCHEM 65 -4 0 USR MPRFLG 0 ; 
       14 SCHEM 60 -6 0 USR MPRFLG 0 ; 
       15 SCHEM 55 -8 0 USR MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 USR MPRFLG 0 ; 
       17 SCHEM 32.49999 -6 0 USR MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 USR MPRFLG 0 ; 
       19 SCHEM 62.5 -4 0 USR MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 USR MPRFLG 0 ; 
       21 SCHEM 52.5 -8 0 USR MPRFLG 0 ; 
       22 SCHEM 35 -4 0 USR MPRFLG 0 ; 
       23 SCHEM 30 -6 0 USR MPRFLG 0 ; 
       24 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       25 SCHEM 52.5 -6 0 USR MPRFLG 0 ; 
       26 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 30 -8 0 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 MPRFLG 0 ; 
       29 SCHEM 55 -4 0 USR MPRFLG 0 ; 
       30 SCHEM 20.00001 -8 0 MPRFLG 0 ; 
       31 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 22.50002 -8 0 USR MPRFLG 0 ; 
       33 SCHEM 50 -8 0 USR MPRFLG 0 ; 
       34 SCHEM 10 -4 0 MPRFLG 0 ; 
       35 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19.00001 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 46.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.50002 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.49999 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 49 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 33.99999 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 59 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 33.99999 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 36.49999 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 71.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
