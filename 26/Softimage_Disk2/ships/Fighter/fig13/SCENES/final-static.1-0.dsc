SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.43-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.43-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       done_cropping-mat10.1-0 ; 
       done_cropping-mat105.1-0 ; 
       done_cropping-mat106.1-0 ; 
       done_cropping-mat107.1-0 ; 
       done_cropping-mat12.1-0 ; 
       done_cropping-mat13.1-0 ; 
       done_cropping-mat14.1-0 ; 
       done_cropping-mat15.1-0 ; 
       done_cropping-mat23.1-0 ; 
       done_cropping-mat24.1-0 ; 
       done_cropping-mat25.1-0 ; 
       done_cropping-mat34.1-0 ; 
       done_cropping-mat44.1-0 ; 
       done_cropping-mat52_1.1-0 ; 
       done_cropping-mat53_1.1-0 ; 
       done_cropping-mat54_1.1-0 ; 
       done_cropping-mat59.1-0 ; 
       done_cropping-mat74.1-0 ; 
       done_cropping-mat75.1-0 ; 
       done_cropping-mat76.1-0 ; 
       done_cropping-mat77.1-0 ; 
       done_cropping-mat83.1-0 ; 
       done_cropping-mat84.1-0 ; 
       done_cropping-mat88.1-0 ; 
       done_cropping-mat90.1-0 ; 
       done_cropping-mat94.1-0 ; 
       done_cropping-mat96.1-0 ; 
       fix_blinker-mat72.1-0 ; 
       static-mat109.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13.5-0 ROOT ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-rthrust.5-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSar1_1_3.1-0 ; 
       done_cropping-SSar1_1_8.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepbar.3-0 ; 
       done_cropping-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       done_cropping-t2d100.1-0 ; 
       done_cropping-t2d105.1-0 ; 
       done_cropping-t2d107.1-0 ; 
       done_cropping-t2d11.1-0 ; 
       done_cropping-t2d114.1-0 ; 
       done_cropping-t2d115.1-0 ; 
       done_cropping-t2d116.1-0 ; 
       done_cropping-t2d12.1-0 ; 
       done_cropping-t2d13.1-0 ; 
       done_cropping-t2d14.1-0 ; 
       done_cropping-t2d30.1-0 ; 
       done_cropping-t2d53_1.1-0 ; 
       done_cropping-t2d54_1.1-0 ; 
       done_cropping-t2d61.1-0 ; 
       done_cropping-t2d64.1-0 ; 
       done_cropping-t2d65.1-0 ; 
       done_cropping-t2d79_1.1-0 ; 
       done_cropping-t2d89.1-0 ; 
       done_cropping-t2d90.1-0 ; 
       done_cropping-t2d91.1-0 ; 
       done_cropping-t2d92.1-0 ; 
       done_cropping-t2d96.1-0 ; 
       fix_blinker-t2d88.3-0 ; 
       static-t2d118.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 13 110 ; 
       2 11 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 11 110 ; 
       9 13 110 ; 
       10 6 110 ; 
       11 6 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 8 110 ; 
       15 9 110 ; 
       16 8 110 ; 
       17 9 110 ; 
       18 4 110 ; 
       19 5 110 ; 
       20 4 110 ; 
       21 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 20 300 ; 
       1 25 300 ; 
       2 26 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       8 27 300 ; 
       9 28 300 ; 
       11 22 300 ; 
       13 23 300 ; 
       14 8 300 ; 
       15 9 300 ; 
       16 21 300 ; 
       17 24 300 ; 
       18 10 300 ; 
       20 13 300 ; 
       20 14 300 ; 
       20 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 14 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 3 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       11 10 401 ; 
       12 13 401 ; 
       13 16 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 15 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       22 21 401 ; 
       23 0 401 ; 
       25 1 401 ; 
       26 2 401 ; 
       28 23 401 ; 
       27 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 MPRFLG 0 ; 
       1 SCHEM 35.12369 -8 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 31.37369 -7.444136 0 USR MPRFLG 0 ; 
       10 SCHEM 20 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 30.12369 -9.444136 0 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 MPRFLG 0 ; 
       17 SCHEM 33.72784 -10.09842 0 USR MPRFLG 0 ; 
       18 SCHEM 5 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29.12369 -11.44414 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.62369 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 32.72784 -12.09842 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34.12369 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 35.22784 -9.444136 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 36.62369 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34.12369 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 35.22784 -11.44414 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
