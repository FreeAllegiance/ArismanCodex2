SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.49-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.49-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       done_cropping-mat10.1-0 ; 
       done_cropping-mat105.1-0 ; 
       done_cropping-mat106.1-0 ; 
       done_cropping-mat107.1-0 ; 
       done_cropping-mat12.1-0 ; 
       done_cropping-mat13.1-0 ; 
       done_cropping-mat14.1-0 ; 
       done_cropping-mat15.1-0 ; 
       done_cropping-mat23.1-0 ; 
       done_cropping-mat24.1-0 ; 
       done_cropping-mat25.1-0 ; 
       done_cropping-mat34.2-0 ; 
       done_cropping-mat44.2-0 ; 
       done_cropping-mat59.2-0 ; 
       done_cropping-mat74.1-0 ; 
       done_cropping-mat75.1-0 ; 
       done_cropping-mat76.1-0 ; 
       done_cropping-mat77.1-0 ; 
       done_cropping-mat83.1-0 ; 
       done_cropping-mat84.1-0 ; 
       done_cropping-mat88.1-0 ; 
       done_cropping-mat90.1-0 ; 
       done_cropping-mat94.1-0 ; 
       done_cropping-mat96.1-0 ; 
       fins-mat109.1-0 ; 
       fix_blinker-mat72.1-0 ; 
       static-mat69.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       done_cropping-afuselg.3-0 ; 
       done_cropping-bool10.1-0 ; 
       done_cropping-bool11.1-0 ; 
       done_cropping-cockpt.1-0 ; 
       done_cropping-ffuselg.1-0 ; 
       done_cropping-fig13.11-0 ROOT ; 
       done_cropping-l-gun.1-0 ; 
       done_cropping-lengine.1-0 ; 
       done_cropping-lengine1.1-0 ; 
       done_cropping-Lfinzzz.1-0 ; 
       done_cropping-Lfinzzz1.1-0 ; 
       done_cropping-lthrust.1-0 ; 
       done_cropping-lwingzz.1-0 ; 
       done_cropping-rthrust.5-0 ; 
       done_cropping-rwingzz.1-0 ; 
       done_cropping-SSal.1-0 ; 
       done_cropping-SSar.1-0 ; 
       done_cropping-SSar1_1_3.1-0 ; 
       done_cropping-SSar1_1_8.1-0 ; 
       done_cropping-SSf.2-0 ; 
       done_cropping-trail.1-0 ; 
       done_cropping-wepemt_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       done_cropping-t2d100.1-0 ; 
       done_cropping-t2d105.1-0 ; 
       done_cropping-t2d107.1-0 ; 
       done_cropping-t2d11.1-0 ; 
       done_cropping-t2d114.1-0 ; 
       done_cropping-t2d115.1-0 ; 
       done_cropping-t2d116.1-0 ; 
       done_cropping-t2d12.1-0 ; 
       done_cropping-t2d13.1-0 ; 
       done_cropping-t2d14.1-0 ; 
       done_cropping-t2d30.2-0 ; 
       done_cropping-t2d61.2-0 ; 
       done_cropping-t2d64.1-0 ; 
       done_cropping-t2d65.2-0 ; 
       done_cropping-t2d89.2-0 ; 
       done_cropping-t2d90.2-0 ; 
       done_cropping-t2d91.2-0 ; 
       done_cropping-t2d92.1-0 ; 
       done_cropping-t2d96.1-0 ; 
       fins-t2d118.1-0 ; 
       fix_blinker-t2d88.3-0 ; 
       static-t2d85.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 14 110 ; 
       2 12 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 4 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 12 110 ; 
       10 14 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 9 110 ; 
       16 10 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 4 110 ; 
       20 5 110 ; 
       21 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 17 300 ; 
       1 22 300 ; 
       2 23 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       6 26 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 25 300 ; 
       10 24 300 ; 
       12 19 300 ; 
       14 20 300 ; 
       15 8 300 ; 
       16 9 300 ; 
       17 18 300 ; 
       18 21 300 ; 
       19 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 4 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 3 401 ; 
       5 7 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       19 18 401 ; 
       20 0 401 ; 
       22 1 401 ; 
       23 2 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 78.87369 -8 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 65 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 43.75 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 50 -4 0 MPRFLG 0 ; 
       8 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 73.87369 -7.444136 0 USR MPRFLG 0 ; 
       11 SCHEM 52.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 75 -6 0 MPRFLG 0 ; 
       15 SCHEM 40 -10 0 MPRFLG 0 ; 
       16 SCHEM 71.37369 -9.444136 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 76.22784 -10.09842 0 USR MPRFLG 0 ; 
       19 SCHEM 20 -6 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 71.37369 -11.44414 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 81.37369 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 76.22784 -12.09842 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 78.87369 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 78.72784 -9.444136 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 81.37369 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 78.87369 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 78.72784 -11.44414 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 151.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 89.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 90.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 137.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 136.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 239.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 132.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 97.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 139.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 172.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 123.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 162.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
