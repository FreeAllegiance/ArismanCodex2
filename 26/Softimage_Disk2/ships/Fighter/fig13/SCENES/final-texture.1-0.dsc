SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       check_animation-fig13.23-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.32-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.32-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       check_animation-mat10.1-0 ; 
       check_animation-mat100.2-0 ; 
       check_animation-mat101.1-0 ; 
       check_animation-mat102.1-0 ; 
       check_animation-mat103.1-0 ; 
       check_animation-mat12.1-0 ; 
       check_animation-mat13.1-0 ; 
       check_animation-mat14.1-0 ; 
       check_animation-mat15.1-0 ; 
       check_animation-mat23.1-0 ; 
       check_animation-mat24.1-0 ; 
       check_animation-mat25.1-0 ; 
       check_animation-mat34.1-0 ; 
       check_animation-mat44.1-0 ; 
       check_animation-mat52_1.1-0 ; 
       check_animation-mat53_1.1-0 ; 
       check_animation-mat54_1.1-0 ; 
       check_animation-mat59.1-0 ; 
       check_animation-mat65_1.1-0 ; 
       check_animation-mat65_2.1-0 ; 
       check_animation-mat66_1.1-0 ; 
       check_animation-mat66_2.1-0 ; 
       check_animation-mat67_1.1-0 ; 
       check_animation-mat67_2.1-0 ; 
       check_animation-mat74.1-0 ; 
       check_animation-mat75.1-0 ; 
       check_animation-mat76.1-0 ; 
       check_animation-mat77.1-0 ; 
       check_animation-mat83.1-0 ; 
       check_animation-mat84.1-0 ; 
       check_animation-mat85.1-0 ; 
       check_animation-mat86.1-0 ; 
       check_animation-mat87.1-0 ; 
       check_animation-mat88.1-0 ; 
       check_animation-mat90.1-0 ; 
       check_animation-mat94.1-0 ; 
       check_animation-mat96.1-0 ; 
       check_animation-mat98.1-0 ; 
       fix_blinker-mat72.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       check_animation-afuselg.3-0 ; 
       check_animation-bool10.1-0 ; 
       check_animation-bool11.1-0 ; 
       check_animation-cockpt.1-0 ; 
       check_animation-ffuselg.1-0 ; 
       check_animation-fig13.22-0 ROOT ; 
       check_animation-gear-null.1-0 ; 
       check_animation-lengine.1-0 ; 
       check_animation-Lfinzzz.1-0 ; 
       check_animation-lfinzzz1.1-0 ; 
       check_animation-LL1.1-0 ; 
       check_animation-LL2.1-0 ; 
       check_animation-LR1.1-0 ; 
       check_animation-LR2.1-0 ; 
       check_animation-lthrust.1-0 ; 
       check_animation-lwingzz.1-0 ; 
       check_animation-rengine.1-0 ; 
       check_animation-rthrust.5-0 ; 
       check_animation-rwingzz.1-0 ; 
       check_animation-SSal.1-0 ; 
       check_animation-SSar.1-0 ; 
       check_animation-SSar1_1_3.1-0 ; 
       check_animation-SSar1_1_8.1-0 ; 
       check_animation-SSf.2-0 ; 
       check_animation-trail.1-0 ; 
       check_animation-wepbar.3-0 ; 
       check_animation-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/bom01a ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig13/PICTURES/fig13 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-texture.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       check_animation-t2d100.1-0 ; 
       check_animation-t2d105.1-0 ; 
       check_animation-t2d107.2-0 ; 
       check_animation-t2d109.3-0 ; 
       check_animation-t2d11.2-0 ; 
       check_animation-t2d110.5-0 ; 
       check_animation-t2d111.2-0 ; 
       check_animation-t2d112.2-0 ; 
       check_animation-t2d12.2-0 ; 
       check_animation-t2d13.2-0 ; 
       check_animation-t2d14.2-0 ; 
       check_animation-t2d30.2-0 ; 
       check_animation-t2d53_1.2-0 ; 
       check_animation-t2d54_1.2-0 ; 
       check_animation-t2d61.2-0 ; 
       check_animation-t2d64.2-0 ; 
       check_animation-t2d65.2-0 ; 
       check_animation-t2d79_1.2-0 ; 
       check_animation-t2d82_1.1-0 ; 
       check_animation-t2d82_2.2-0 ; 
       check_animation-t2d83_1.1-0 ; 
       check_animation-t2d83_2.2-0 ; 
       check_animation-t2d84_1.1-0 ; 
       check_animation-t2d84_2.2-0 ; 
       check_animation-t2d89.1-0 ; 
       check_animation-t2d90.1-0 ; 
       check_animation-t2d91.1-0 ; 
       check_animation-t2d92.2-0 ; 
       check_animation-t2d96.2-0 ; 
       check_animation-t2d97.1-0 ; 
       check_animation-t2d98.1-0 ; 
       check_animation-t2d99.1-0 ; 
       fix_blinker-t2d88.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 15 110 ; 
       0 4 110 ; 
       1 18 110 ; 
       2 15 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       9 18 110 ; 
       10 6 110 ; 
       11 10 110 ; 
       12 6 110 ; 
       13 12 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 5 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 8 110 ; 
       20 9 110 ; 
       21 8 110 ; 
       22 9 110 ; 
       23 4 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 38 300 ; 
       0 0 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 27 300 ; 
       1 35 300 ; 
       2 36 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 17 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       9 4 300 ; 
       10 37 300 ; 
       11 1 300 ; 
       12 2 300 ; 
       13 3 300 ; 
       14 19 300 ; 
       14 21 300 ; 
       14 23 300 ; 
       15 29 300 ; 
       16 30 300 ; 
       16 31 300 ; 
       16 32 300 ; 
       17 18 300 ; 
       17 20 300 ; 
       17 22 300 ; 
       18 33 300 ; 
       19 9 300 ; 
       20 10 300 ; 
       21 28 300 ; 
       22 34 300 ; 
       23 11 300 ; 
       25 14 300 ; 
       25 15 300 ; 
       25 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 15 401 ; 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       5 4 401 ; 
       6 8 401 ; 
       7 9 401 ; 
       8 10 401 ; 
       12 11 401 ; 
       13 14 401 ; 
       14 17 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 16 401 ; 
       18 18 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
       33 0 401 ; 
       35 1 401 ; 
       36 2 401 ; 
       37 3 401 ; 
       38 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 50.00563 -7.493902 0 USR MPRFLG 0 ; 
       0 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 55.00563 -8 0 MPRFLG 0 ; 
       3 SCHEM 102.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       4 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 51.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 82.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 36.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 35 -8 0 MPRFLG 0 ; 
       12 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 50 -6 0 MPRFLG 0 ; 
       16 SCHEM 85 -4 0 MPRFLG 0 ; 
       17 SCHEM 75 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 85 -6 0 MPRFLG 0 ; 
       19 SCHEM 47.50563 -9.493902 0 MPRFLG 0 ; 
       20 SCHEM 80 -10 0 MPRFLG 0 ; 
       21 SCHEM 50.00563 -9.493902 0 MPRFLG 0 ; 
       22 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 25 -6 0 MPRFLG 0 ; 
       24 SCHEM 100 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       26 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.50563 -11.4939 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 50.00563 -11.4939 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 57.50563 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 55.00563 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 52.50563 -9.493902 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 55.00563 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 40 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -10 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 57.50563 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 52.50563 -11.4939 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 104 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
