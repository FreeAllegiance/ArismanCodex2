SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       fig03-alarmour.1-0 ; 
       fig03-alarmour1.1-0 ; 
       fig03-fuselg.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       edit_nulls-light3.1-0 ROOT ; 
       edit_nulls-light4.1-0 ROOT ; 
       edit_nulls-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       edit_nulls-mat62.1-0 ; 
       edit_nulls-mat63.1-0 ; 
       edit_nulls-mat64.1-0 ; 
       edit_nulls-mat65.1-0 ; 
       edit_nulls-mat66.1-0 ; 
       edit_nulls-mat67.1-0 ; 
       edit_nulls-mat68.1-0 ; 
       edit_Strobes-bottom1.1-0 ; 
       edit_Strobes-front1.1-0 ; 
       edit_Strobes-global1.1-0 ; 
       edit_Strobes-green1.1-0 ; 
       edit_Strobes-mat1.1-0 ; 
       edit_Strobes-mat2.1-0 ; 
       edit_Strobes-mat3.1-0 ; 
       edit_Strobes-mat4.1-0 ; 
       edit_Strobes-OTHERS-rubber.1-1.1-0 ; 
       edit_Strobes-OTHERS-rubber.1-2.1-0 ; 
       edit_Strobes-OTHERS-rubber.1-3.1-0 ; 
       edit_Strobes-OTHERS-rubber.1-4.1-0 ; 
       edit_Strobes-OTHERS-rubber.1-5.1-0 ; 
       edit_Strobes-red1.1-0 ; 
       edit_Strobes-top1.1-0 ; 
       reduce_flap-mat59.1-0 ; 
       reduce_flap-mat60.1-0 ; 
       reduce_flap-mat61.1-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat34.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat53.1-0 ; 
       reduce_wing-mat6.1-0 ; 
       reduce_wing-mat7.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       fig03-alarmour.1-0 ROOT ; 
       fig03-alarmour1.1-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ROOT ; 
       fig03-SSb1.1-0 ; 
       fig03-SSb2.1-0 ; 
       fig03-SSb3.1-0 ; 
       fig03-SSb4.1-0 ; 
       fig03-SStl1.1-0 ; 
       fig03-SStl2.1-0 ; 
       fig03-SStl3.1-0 ; 
       fig03-SStr.1-0 ; 
       fig03-SStr1.1-0 ; 
       fig03-SStr2.1-0 ; 
       fig03-SStr3.1-0 ; 
       fig03-SSttl4.1-0 ; 
       fig15-face2.1-0 ; 
       fig15-face3.1-0 ; 
       fig15-ftfuselg.1-0 ; 
       fig15-fuselg.1-0 ; 
       fig15-missemt.1-0 ; 
       fig15-poly_arm.1-0 ; 
       fig15-poly_arm1.1-0 ; 
       fig15-root.1-0 ROOT ; 
       fig15-smoke.1-0 ; 
       fig15-ssb1.1-0 ; 
       fig15-ssb2.1-0 ; 
       fig15-ssb3.1-0 ; 
       fig15-ssb4.1-0 ; 
       fig15-thrust.1-0 ; 
       fig15-top_fin.1-0 ; 
       fig15-top_fin2.1-0 ; 
       fig15-trail.1-0 ; 
       fig15-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig15/PICTURES/fig03 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig15/PICTURES/fig15 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig15-edit_Strobes.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       edit_Strobes-t2d1.1-0 ; 
       edit_Strobes-t2d10.1-0 ; 
       edit_Strobes-t2d2.1-0 ; 
       edit_Strobes-t2d3.1-0 ; 
       edit_Strobes-t2d4.1-0 ; 
       edit_Strobes-t2d5.1-0 ; 
       edit_Strobes-t2d6.1-0 ; 
       edit_Strobes-t2d7.1-0 ; 
       edit_Strobes-t2d8.1-0 ; 
       edit_Strobes-t2d9.1-0 ; 
       reduce_flap-t2d47.1-0 ; 
       reduce_flap-t2d48.1-0 ; 
       reduce_flap-t2d49.1-0 ; 
       reduce_wing-t2d14.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d34.1-0 ; 
       reduce_wing-t2d6.1-0 ; 
       reduce_wing-t2d7.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       16 21 110 ; 
       17 22 110 ; 
       18 19 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 23 110 ; 
       30 19 110 ; 
       31 19 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 11 300 ; 
       17 12 300 ; 
       18 15 300 ; 
       19 9 300 ; 
       19 21 300 ; 
       19 7 300 ; 
       19 8 300 ; 
       21 17 300 ; 
       22 19 300 ; 
       25 20 300 ; 
       26 10 300 ; 
       27 13 300 ; 
       28 14 300 ; 
       30 16 300 ; 
       31 18 300 ; 
       0 33 300 ; 
       0 34 300 ; 
       0 27 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       2 25 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 26 300 ; 
       4 31 300 ; 
       5 30 300 ; 
       6 29 300 ; 
       7 28 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       10 5 300 ; 
       11 32 300 ; 
       12 0 300 ; 
       13 1 300 ; 
       14 2 300 ; 
       15 6 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
       1 1 15000 ; 
       3 2 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       21 0 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       15 4 401 ; 
       16 5 401 ; 
       17 6 401 ; 
       11 7 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       12 1 401 ; 
       22 10 401 ; 
       23 11 401 ; 
       24 12 401 ; 
       25 13 401 ; 
       26 14 401 ; 
       27 15 401 ; 
       33 16 401 ; 
       34 17 401 ; 
       35 18 401 ; 
       36 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 52.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 55 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 15 -2 0 MPRFLG 0 ; 
       20 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -4 0 MPRFLG 0 ; 
       31 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 62.5 -4 0 DISPLAY 1 2 SRT 1 1 1 0 3.141593 0.7504916 0 0.5594667 -0.3438337 MPRFLG 0 ; 
       1 SCHEM 75 -4 0 SRT 1 1 1 0 0 -0.7504916 0 0.5635141 -0.3438337 MPRFLG 0 ; 
       2 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 91.25 -2 0 SRT 1 1 1 0 0 0 -4.463202e-008 0.3656361 -1.330034e-005 MPRFLG 0 ; 
       4 SCHEM 90 -4 0 MPRFLG 0 ; 
       5 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 85 -4 0 MPRFLG 0 ; 
       7 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 60 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       10 SCHEM 65 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       11 SCHEM 75 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       12 SCHEM 80 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -6 0 WIRECOL 6 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       9 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 81.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 91.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 69 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       8 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 127.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 127.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
