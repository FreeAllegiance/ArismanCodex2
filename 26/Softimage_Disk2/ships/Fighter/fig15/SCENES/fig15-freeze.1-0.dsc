SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 7     
       fig15-root.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       edit_nulls-light3.3-0 ROOT ; 
       edit_nulls-light4.3-0 ROOT ; 
       edit_nulls-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       freeze-bottom1.1-0 ; 
       freeze-front1.1-0 ; 
       freeze-global1.1-0 ; 
       freeze-mat1.1-0 ; 
       freeze-mat2.1-0 ; 
       freeze-OTHERS-rubber.1-1.1-0 ; 
       freeze-OTHERS-rubber.1-2.1-0 ; 
       freeze-OTHERS-rubber.1-3.1-0 ; 
       freeze-OTHERS-rubber.1-4.1-0 ; 
       freeze-OTHERS-rubber.1-5.1-0 ; 
       freeze-top1.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       fig15-crap.1-0 ; 
       fig15-face2.1-0 ; 
       fig15-face3.1-0 ; 
       fig15-ftfuselg.1-0 ; 
       fig15-fuselg.1-0 ; 
       fig15-missemt.1-0 ; 
       fig15-poly_arm.1-0 ; 
       fig15-poly_arm1.1-0 ; 
       fig15-root.3-0 ROOT ; 
       fig15-smoke.1-0 ; 
       fig15-SSb1.1-0 ; 
       fig15-SSb2.1-0 ; 
       fig15-SSb3.1-0 ; 
       fig15-SSb4.1-0 ; 
       fig15-thrust.1-0 ; 
       fig15-top_fin.1-0 ; 
       fig15-top_fin2.1-0 ; 
       fig15-trail.1-0 ; 
       fig15-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig15/PICTURES/fig03 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig15/PICTURES/fig15 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig15-freeze.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       freeze-t2d1.1-0 ; 
       freeze-t2d10.1-0 ; 
       freeze-t2d2.1-0 ; 
       freeze-t2d3.1-0 ; 
       freeze-t2d4.1-0 ; 
       freeze-t2d5.1-0 ; 
       freeze-t2d6.1-0 ; 
       freeze-t2d7.1-0 ; 
       freeze-t2d8.1-0 ; 
       freeze-t2d9.1-0 ; 
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 6 110 ; 
       2 7 110 ; 
       3 4 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 8 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 8 110 ; 
       18 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 17 300 ; 
       0 11 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 5 300 ; 
       4 2 300 ; 
       4 10 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       6 7 300 ; 
       7 9 300 ; 
       10 15 300 ; 
       11 14 300 ; 
       12 13 300 ; 
       13 12 300 ; 
       15 6 300 ; 
       16 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
       3 7 401 ; 
       4 1 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 0 401 ; 
       11 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 0 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 16.25 -2 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
