SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.14-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       STATIC-bottom1.2-0 ; 
       STATIC-front1.2-0 ; 
       STATIC-global1.2-0 ; 
       STATIC-mat1.2-0 ; 
       STATIC-mat2.2-0 ; 
       STATIC-OTHERS-rubber.1-1.2-0 ; 
       STATIC-OTHERS-rubber.1-2.2-0 ; 
       STATIC-OTHERS-rubber.1-3.2-0 ; 
       STATIC-OTHERS-rubber.1-4.2-0 ; 
       STATIC-OTHERS-rubber.1-5.2-0 ; 
       STATIC-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig15-face2.1-0 ; 
       fig15-face3.1-0 ; 
       fig15-ftfuselg.1-0 ; 
       fig15-fuselg.1-0 ; 
       fig15-poly_arm.1-0 ; 
       fig15-poly_arm1.1-0 ; 
       fig15-root.11-0 ROOT ; 
       fig15-top_fin.1-0 ; 
       fig15-top_fin2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig15/PICTURES/fig15 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig15-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       STATIC-t2d1.2-0 ; 
       STATIC-t2d10.2-0 ; 
       STATIC-t2d2.2-0 ; 
       STATIC-t2d3.2-0 ; 
       STATIC-t2d4.2-0 ; 
       STATIC-t2d5.2-0 ; 
       STATIC-t2d6.2-0 ; 
       STATIC-t2d7.2-0 ; 
       STATIC-t2d8.2-0 ; 
       STATIC-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 5 110 ; 
       2 3 110 ; 
       3 6 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 4 300 ; 
       2 5 300 ; 
       3 2 300 ; 
       3 10 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       4 7 300 ; 
       5 9 300 ; 
       7 6 300 ; 
       8 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
       3 7 401 ; 
       4 1 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
