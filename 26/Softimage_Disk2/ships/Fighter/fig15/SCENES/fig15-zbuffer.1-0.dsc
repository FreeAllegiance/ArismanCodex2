SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 7     
       fig15-root.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.18-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.11-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       zbuffer-light3.1-0 ROOT ; 
       zbuffer-light4.1-0 ROOT ; 
       zbuffer-light5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat44.1-0 ; 
       reduce_wing-mat45.1-0 ; 
       reduce_wing-mat46.1-0 ; 
       reduce_wing-mat47.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
       zbuffer-bottom1.1-0 ; 
       zbuffer-front1.1-0 ; 
       zbuffer-global1.1-0 ; 
       zbuffer-mat1.1-0 ; 
       zbuffer-mat2.1-0 ; 
       zbuffer-OTHERS-rubber.1-1.1-0 ; 
       zbuffer-OTHERS-rubber.1-2.1-0 ; 
       zbuffer-OTHERS-rubber.1-3.1-0 ; 
       zbuffer-OTHERS-rubber.1-4.1-0 ; 
       zbuffer-OTHERS-rubber.1-5.1-0 ; 
       zbuffer-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       fig15-crap.1-0 ; 
       fig15-face2.1-0 ; 
       fig15-face3.1-0 ; 
       fig15-ftfuselg.1-0 ; 
       fig15-fuselg.1-0 ; 
       fig15-missemt.1-0 ; 
       fig15-poly_arm.1-0 ; 
       fig15-poly_arm1.1-0 ; 
       fig15-root.9-0 ROOT ; 
       fig15-smoke.1-0 ; 
       fig15-SSb1.1-0 ; 
       fig15-SSb2.1-0 ; 
       fig15-SSb3.1-0 ; 
       fig15-SSb4.1-0 ; 
       fig15-thrust.1-0 ; 
       fig15-top_fin.1-0 ; 
       fig15-top_fin2.1-0 ; 
       fig15-trail.1-0 ; 
       fig15-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig15/PICTURES/fig03 ; 
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig15/PICTURES/fig15 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig15-zbuffer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       reduce_wing-t2d33.1-0 ; 
       reduce_wing-t2d8.1-0 ; 
       reduce_wing-t2d9.1-0 ; 
       zbuffer-t2d1.1-0 ; 
       zbuffer-t2d10.1-0 ; 
       zbuffer-t2d2.1-0 ; 
       zbuffer-t2d3.1-0 ; 
       zbuffer-t2d4.1-0 ; 
       zbuffer-t2d5.1-0 ; 
       zbuffer-t2d6.1-0 ; 
       zbuffer-t2d7.1-0 ; 
       zbuffer-t2d8.1-0 ; 
       zbuffer-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 6 110 ; 
       2 7 110 ; 
       3 4 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       9 8 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 8 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 8 110 ; 
       18 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 6 300 ; 
       0 0 300 ; 
       1 10 300 ; 
       2 11 300 ; 
       3 12 300 ; 
       4 9 300 ; 
       4 17 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       6 14 300 ; 
       7 16 300 ; 
       10 4 300 ; 
       11 3 300 ; 
       12 2 300 ; 
       13 1 300 ; 
       15 13 300 ; 
       16 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 5 401 ; 
       8 6 401 ; 
       10 10 401 ; 
       11 4 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 3 401 ; 
       0 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       3 SCHEM 0 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -1 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -1 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 34 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 108.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 381 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
