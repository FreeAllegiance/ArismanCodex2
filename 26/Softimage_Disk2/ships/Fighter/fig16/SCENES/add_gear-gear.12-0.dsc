SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_grear-null1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.205-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       add_grear-back6.4-0 ; 
       add_grear-engine7.4-0 ; 
       add_grear-fiiped_decal1.4-0 ; 
       add_grear-inside5.2-0 ; 
       add_grear-inside6.2-0 ; 
       add_grear-mat1.2-0 ; 
       add_grear-mat2.2-0 ; 
       add_grear-mat3.2-0 ; 
       add_grear-outside6.2-0 ; 
       add_grear-outside7.2-0 ; 
       add_grear-sides5.4-0 ; 
       add_grear-tur3.2-0 ; 
       add_grear-wing_bottom7.2-0 ; 
       add_grear-wing_bottom8.2-0 ; 
       add_grear-wing_top8.2-0 ; 
       add_grear-wing_top9.2-0 ; 
       gear-gear1.1-0 ; 
       gear-mat52.3-0 ; 
       gear-mat53.3-0 ; 
       gear-mat54.3-0 ; 
       gear-mat55.2-0 ; 
       gear-mat56.2-0 ; 
       gear-mat57.2-0 ; 
       gear-mat58.2-0 ; 
       gear-mat59.1-0 ; 
       gear-mat60.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       add_grear-cyl1.1-0 ; 
       add_grear-cyl2.4-0 ; 
       add_grear-cyl3.1-0 ; 
       add_grear-LL0.1-0 ; 
       add_grear-LLf.1-0 ; 
       add_grear-LLf1.1-0 ; 
       add_grear-LLf2.1-0 ; 
       add_grear-map.7-0 ; 
       add_grear-null1.11-0 ROOT ; 
       add_grear-r-stablat2.1-0 ; 
       add_grear-r-stablat3.1-0 ; 
       add_grear-r-wing1.1-0 ; 
       add_grear-r-wing2.1-0 ; 
       add_grear-tur_pod1.1-0 ; 
       gear-cube2.1-0 ROOT ; 
       gear-cube3.1-0 ROOT ; 
       gear-cube4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       add_gear-gear.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_grear-engine_map5.2-0 ; 
       add_grear-outside5.2-0 ; 
       add_grear-outside6.2-0 ; 
       add_grear-t2d27.2-0 ; 
       add_grear-t2d28.2-0 ; 
       add_grear-t2d29.2-0 ; 
       add_grear-t2d30.2-0 ; 
       add_grear-t2d31.2-0 ; 
       add_grear-t2d32.2-0 ; 
       add_grear-t2d33.2-0 ; 
       add_grear-t2d34.2-0 ; 
       add_grear-t2d35.2-0 ; 
       add_grear-wing_bottom8.2-0 ; 
       add_grear-wing_bottom9.2-0 ; 
       add_grear-wing_top8.2-0 ; 
       add_grear-wing_top9.2-0 ; 
       gear-t2d82.3-0 ; 
       gear-t2d83.3-0 ; 
       gear-t2d84.2-0 ; 
       gear-t2d85.2-0 ; 
       gear-t2d86.2-0 ; 
       gear-t2d87.2-0 ; 
       gear-t2d88.2-0 ; 
       gear-t2d89.2-0 ; 
       gear-t2d90.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       7 8 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       5 3 110 ; 
       6 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       1 6 300 ; 
       2 7 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       7 10 300 ; 
       7 1 300 ; 
       7 0 300 ; 
       7 2 300 ; 
       7 16 300 ; 
       9 3 300 ; 
       9 8 300 ; 
       10 4 300 ; 
       10 9 300 ; 
       11 12 300 ; 
       11 14 300 ; 
       12 13 300 ; 
       12 15 300 ; 
       13 11 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 11 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 7 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       20 19 401 ; 
       17 16 401 ; 
       18 18 401 ; 
       19 17 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 50 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 33.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 33.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 41.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 46.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 28.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 36.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 51.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 110 0 0 SRT 0.1350399 0.1350399 0.1350399 0 0 0 1.98085 -1.416134 -1.134834 MPRFLG 0 ; 
       14 SCHEM 107.5 0 0 DISPLAY 1 2 SRT 0.04999994 0.04999994 0.04999994 0 0 0 0.05212764 -1.407007 4.291091 MPRFLG 0 ; 
       16 SCHEM 112.5 0 0 SRT 0.1350399 0.1350399 0.1350399 0 3.141593 0 -1.98085 -1.416134 -1.134834 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 66.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
