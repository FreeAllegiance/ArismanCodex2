SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       fig16-fuselg.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.244-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       add_grear-back6.5-0 ; 
       add_grear-engine7.5-0 ; 
       add_grear-fiiped_decal1.5-0 ; 
       add_grear-inside5.3-0 ; 
       add_grear-inside6.4-0 ; 
       add_grear-mat1.3-0 ; 
       add_grear-mat2.3-0 ; 
       add_grear-outside6.3-0 ; 
       add_grear-outside7.4-0 ; 
       add_grear-sides5.5-0 ; 
       add_grear-tur3.3-0 ; 
       add_grear-wing_bottom7.3-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
       edit_nulls-default3.1-0 ; 
       edit_nulls-default5.1-0 ; 
       edit_nulls-default6.1-0 ; 
       edit_nulls-mat55.1-0 ; 
       edit_nulls-mat56.1-0 ; 
       edit_nulls-mat62.1-0 ; 
       edit_nulls-mat63.1-0 ; 
       edit_nulls-mat64.1-0 ; 
       edit_nulls-mat65.1-0 ; 
       edit_nulls-mat66.1-0 ; 
       edit_nulls-mat67.1-0 ; 
       edit_nulls-mat68.1-0 ; 
       edit_nulls-mat69.1-0 ; 
       edit_nulls-mat70.1-0 ; 
       edit_nulls-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       fig16-cockpt.1-0 ; 
       fig16-fgun.1-0 ; 
       fig16-FL.1-0 ; 
       fig16-fuselg.10-0 ROOT ; 
       fig16-fwepemt.1-0 ; 
       fig16-LL0.1-0 ; 
       fig16-LLl.1-0 ; 
       fig16-LRl.1-0 ; 
       fig16-lsmoke.1-0 ; 
       fig16-lstablat.1-0 ; 
       fig16-lthrust.1-0 ; 
       fig16-lwing.1-0 ; 
       fig16-lwingpod.4-0 ; 
       fig16-missemt.1-0 ; 
       fig16-rsmoke.1-0 ; 
       fig16-rstablat.1-0 ; 
       fig16-rthrust.1-0 ; 
       fig16-rwing.1-0 ; 
       fig16-rwing_pod.1-0 ; 
       fig16-SSal.1-0 ; 
       fig16-SSar.1-0 ; 
       fig16-SSf.1-0 ; 
       fig16-SSl.1-0 ; 
       fig16-SSr.1-0 ; 
       fig16-trail.1-0 ; 
       fig16-tur_pod.1-0 ; 
       fig16-wepemt1.1-0 ; 
       fig16-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig16-edit_nulls.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_grear-engine_map5.5-0 ; 
       add_grear-outside5.1-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-t2d27.5-0 ; 
       add_grear-t2d28.5-0 ; 
       add_grear-t2d29.1-0 ; 
       add_grear-t2d30.1-0 ; 
       add_grear-t2d31.1-0 ; 
       add_grear-t2d32.1-0 ; 
       add_grear-t2d33.4-0 ; 
       add_grear-t2d35.5-0 ; 
       add_grear-wing_bottom8.1-0 ; 
       add_grear-wing_bottom9.1-0 ; 
       add_grear-wing_top8.1-0 ; 
       add_grear-wing_top9.1-0 ; 
       edit_nulls-t2d36.1-0 ; 
       edit_nulls-t2d37.1-0 ; 
       edit_nulls-t2d38.1-0 ; 
       edit_nulls-t2d39.1-0 ; 
       edit_nulls-t2d40.1-0 ; 
       edit_nulls-t2d41.1-0 ; 
       edit_nulls-t2d42.1-0 ; 
       edit_nulls-t2d43.1-0 ; 
       edit_nulls-t2d44.1-0 ; 
       edit_nulls-t2d45.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 25 110 ; 
       2 5 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 11 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 17 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 9 300 ; 
       3 1 300 ; 
       3 0 300 ; 
       3 2 300 ; 
       6 17 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       7 16 300 ; 
       7 20 300 ; 
       7 21 300 ; 
       9 4 300 ; 
       9 8 300 ; 
       11 12 300 ; 
       11 14 300 ; 
       12 6 300 ; 
       15 3 300 ; 
       15 7 300 ; 
       17 11 300 ; 
       17 13 300 ; 
       18 24 300 ; 
       19 28 300 ; 
       20 27 300 ; 
       21 29 300 ; 
       22 25 300 ; 
       23 26 300 ; 
       25 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 10 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 7 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 18 401 ; 
       17 21 401 ; 
       18 17 401 ; 
       19 16 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       4 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -2 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 59 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 59 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 89 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
