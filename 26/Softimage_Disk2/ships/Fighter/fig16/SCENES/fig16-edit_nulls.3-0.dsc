SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.246-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       add_grear-back6.5-0 ; 
       add_grear-engine7.5-0 ; 
       add_grear-fiiped_decal1.5-0 ; 
       add_grear-inside5.3-0 ; 
       add_grear-inside6.4-0 ; 
       add_grear-mat1.3-0 ; 
       add_grear-mat2.3-0 ; 
       add_grear-outside6.3-0 ; 
       add_grear-outside7.4-0 ; 
       add_grear-sides5.5-0 ; 
       add_grear-tur3.3-0 ; 
       add_grear-wing_bottom7.3-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
       edit_nulls-mat66.1-0 ; 
       edit_nulls-mat67.1-0 ; 
       edit_nulls-mat68.1-0 ; 
       edit_nulls-mat69.1-0 ; 
       edit_nulls-mat70.1-0 ; 
       edit_nulls-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       fig16-cockpt.1-0 ; 
       fig16-fgun.1-0 ; 
       fig16-fuselg.11-0 ROOT ; 
       fig16-fwepemt.1-0 ; 
       fig16-lsmoke.1-0 ; 
       fig16-lstablat.1-0 ; 
       fig16-lthrust.1-0 ; 
       fig16-lwing.1-0 ; 
       fig16-lwingpod.4-0 ; 
       fig16-missemt.1-0 ; 
       fig16-rsmoke.1-0 ; 
       fig16-rstablat.1-0 ; 
       fig16-rthrust.1-0 ; 
       fig16-rwing.1-0 ; 
       fig16-rwing_pod.1-0 ; 
       fig16-SSal.1-0 ; 
       fig16-SSar.1-0 ; 
       fig16-SSf.1-0 ; 
       fig16-SSl.1-0 ; 
       fig16-SSr.1-0 ; 
       fig16-trail.1-0 ; 
       fig16-tur_pod.1-0 ; 
       fig16-wepemt1.1-0 ; 
       fig16-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig16-edit_nulls.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       add_grear-engine_map5.6-0 ; 
       add_grear-outside5.3-0 ; 
       add_grear-outside6.3-0 ; 
       add_grear-t2d27.6-0 ; 
       add_grear-t2d28.6-0 ; 
       add_grear-t2d29.3-0 ; 
       add_grear-t2d30.3-0 ; 
       add_grear-t2d31.3-0 ; 
       add_grear-t2d32.3-0 ; 
       add_grear-t2d33.5-0 ; 
       add_grear-t2d35.6-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_bottom9.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
       edit_nulls-t2d45.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 21 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       2 9 300 ; 
       2 1 300 ; 
       2 0 300 ; 
       2 2 300 ; 
       5 4 300 ; 
       5 8 300 ; 
       7 12 300 ; 
       7 14 300 ; 
       8 6 300 ; 
       11 3 300 ; 
       11 7 300 ; 
       13 11 300 ; 
       13 13 300 ; 
       14 15 300 ; 
       15 19 300 ; 
       16 18 300 ; 
       17 20 300 ; 
       18 16 300 ; 
       19 17 300 ; 
       21 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 10 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 7 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       22 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 89 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
