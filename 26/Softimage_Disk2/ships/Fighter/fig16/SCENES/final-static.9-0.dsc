SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.243-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       add_grear-back6.5-0 ; 
       add_grear-engine7.5-0 ; 
       add_grear-fiiped_decal1.5-0 ; 
       add_grear-inside5.3-0 ; 
       add_grear-inside6.4-0 ; 
       add_grear-outside6.3-0 ; 
       add_grear-outside7.4-0 ; 
       add_grear-sides5.5-0 ; 
       add_grear-wing_bottom7.3-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       fig16-fuselg.9-0 ROOT ; 
       fig16-lstablat.1-0 ; 
       fig16-lwing.1-0 ; 
       fig16-rstablat.1-0 ; 
       fig16-rwing.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-static.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       add_grear-engine_map5.5-0 ; 
       add_grear-outside5.1-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-t2d27.5-0 ; 
       add_grear-t2d28.5-0 ; 
       add_grear-t2d29.1-0 ; 
       add_grear-t2d30.1-0 ; 
       add_grear-t2d35.5-0 ; 
       add_grear-wing_bottom8.1-0 ; 
       add_grear-wing_bottom9.1-0 ; 
       add_grear-wing_top8.1-0 ; 
       add_grear-wing_top9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 1 300 ; 
       0 0 300 ; 
       0 2 300 ; 
       1 4 300 ; 
       1 6 300 ; 
       2 9 300 ; 
       2 11 300 ; 
       3 3 300 ; 
       3 5 300 ; 
       4 8 300 ; 
       4 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 7 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 89 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
