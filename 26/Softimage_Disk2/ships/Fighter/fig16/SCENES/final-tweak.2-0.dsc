SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_grear-fig16.9-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.232-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       add_grear-back6.5-0 ; 
       add_grear-engine7.5-0 ; 
       add_grear-fiiped_decal1.5-0 ; 
       add_grear-inside5.3-0 ; 
       add_grear-inside6.4-0 ; 
       add_grear-mat1.3-0 ; 
       add_grear-mat2.3-0 ; 
       add_grear-outside6.3-0 ; 
       add_grear-outside7.4-0 ; 
       add_grear-sides5.5-0 ; 
       add_grear-tur3.3-0 ; 
       add_grear-wing_bottom7.3-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
       tweak-default3.1-0 ; 
       tweak-default5.1-0 ; 
       tweak-default6.1-0 ; 
       tweak-mat55.1-0 ; 
       tweak-mat56.1-0 ; 
       tweak-mat62.1-0 ; 
       tweak-mat63.1-0 ; 
       tweak-mat64.1-0 ; 
       tweak-mat65.1-0 ; 
       tweak-mat66.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       add_grear-cockpt.1-0 ; 
       add_grear-cyl1.1-0 ; 
       add_grear-cyl2.4-0 ; 
       add_grear-cyl4.1-0 ; 
       add_grear-fig16.10-0 ROOT ; 
       add_grear-fwepemt.1-0 ; 
       add_grear-LL0.1-0 ; 
       add_grear-LLl.1-0 ; 
       add_grear-LLl1.1-0 ; 
       add_grear-LLl2.1-0 ; 
       add_grear-lthrust.1-0 ; 
       add_grear-missemt.1-0 ; 
       add_grear-r-stablat2.1-0 ; 
       add_grear-r-stablat3.1-0 ; 
       add_grear-r-wing1.1-0 ; 
       add_grear-r-wing2.1-0 ; 
       add_grear-rthrust.1-0 ; 
       add_grear-trail.1-0 ; 
       add_grear-tur_pod1.1-0 ; 
       add_grear-wepemt1.1-0 ; 
       add_grear-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final-tweak.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_grear-engine_map5.5-0 ; 
       add_grear-outside5.1-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-t2d27.5-0 ; 
       add_grear-t2d28.5-0 ; 
       add_grear-t2d29.1-0 ; 
       add_grear-t2d30.1-0 ; 
       add_grear-t2d31.1-0 ; 
       add_grear-t2d32.1-0 ; 
       add_grear-t2d33.3-0 ; 
       add_grear-t2d35.5-0 ; 
       add_grear-wing_bottom8.1-0 ; 
       add_grear-wing_bottom9.1-0 ; 
       add_grear-wing_top8.1-0 ; 
       add_grear-wing_top9.1-0 ; 
       tweak-t2d36.1-0 ; 
       tweak-t2d37.1-0 ; 
       tweak-t2d38.1-0 ; 
       tweak-t2d39.1-0 ; 
       tweak-t2d40.1-0 ; 
       tweak-t2d41.1-0 ; 
       tweak-t2d42.1-0 ; 
       tweak-t2d43.1-0 ; 
       tweak-t2d44.1-0 ; 
       tweak-t2d45.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 18 110 ; 
       2 15 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       3 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       2 6 300 ; 
       4 9 300 ; 
       4 1 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       7 15 300 ; 
       7 18 300 ; 
       7 19 300 ; 
       8 16 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       9 17 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       12 3 300 ; 
       12 7 300 ; 
       13 4 300 ; 
       13 8 300 ; 
       14 11 300 ; 
       14 13 300 ; 
       15 12 300 ; 
       15 14 300 ; 
       18 10 300 ; 
       3 24 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 10 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       24 24 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 7 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 18 401 ; 
       17 21 401 ; 
       18 17 401 ; 
       19 16 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 22 401 ; 
       23 23 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       5 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 40 -4 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 55 -4 0 MPRFLG 0 ; 
       10 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       19 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
