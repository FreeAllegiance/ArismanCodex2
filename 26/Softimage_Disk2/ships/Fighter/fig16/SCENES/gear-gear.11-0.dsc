SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_grear-map.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.216-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       add_grear-back6.5-0 ; 
       add_grear-engine7.5-0 ; 
       add_grear-fiiped_decal1.5-0 ; 
       add_grear-inside5.1-0 ; 
       add_grear-inside6.3-0 ; 
       add_grear-mat1.3-0 ; 
       add_grear-mat2.3-0 ; 
       add_grear-mat3.3-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-outside7.3-0 ; 
       add_grear-sides5.5-0 ; 
       add_grear-tur3.3-0 ; 
       add_grear-wing_bottom7.3-0 ; 
       add_grear-wing_bottom8.3-0 ; 
       add_grear-wing_top8.3-0 ; 
       add_grear-wing_top9.3-0 ; 
       gear-default3.2-0 ; 
       gear-default4.3-0 ; 
       gear-mat52.4-0 ; 
       gear-mat53.4-0 ; 
       gear-mat54.4-0 ; 
       gear-mat55.3-0 ; 
       gear-mat56.3-0 ; 
       gear-mat57.4-0 ; 
       gear-mat58.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       add_grear-cockpt.1-0 ; 
       add_grear-cyl1.1-0 ; 
       add_grear-cyl2.4-0 ; 
       add_grear-cyl3.1-0 ; 
       add_grear-fwepemt.1-0 ; 
       add_grear-LL0.1-0 ; 
       add_grear-LLf.1-0 ; 
       add_grear-LLl.1-0 ; 
       add_grear-LLr.1-0 ; 
       add_grear-lthrust.1-0 ; 
       add_grear-map.7-0 ROOT ; 
       add_grear-missemt.1-0 ; 
       add_grear-r-stablat2.1-0 ; 
       add_grear-r-stablat3.1-0 ; 
       add_grear-r-wing1.1-0 ; 
       add_grear-r-wing2.1-0 ; 
       add_grear-rthrust.1-0 ; 
       add_grear-trail.1-0 ; 
       add_grear-tur_pod1.1-0 ; 
       add_grear-wepemt1.1-0 ; 
       add_grear-wepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/bom01a ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       gear-gear.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       add_grear-engine_map5.1-0 ; 
       add_grear-outside5.1-0 ; 
       add_grear-outside6.1-0 ; 
       add_grear-t2d27.1-0 ; 
       add_grear-t2d28.1-0 ; 
       add_grear-t2d29.1-0 ; 
       add_grear-t2d30.1-0 ; 
       add_grear-t2d31.1-0 ; 
       add_grear-t2d32.1-0 ; 
       add_grear-t2d33.1-0 ; 
       add_grear-t2d34.1-0 ; 
       add_grear-t2d35.1-0 ; 
       add_grear-wing_bottom8.1-0 ; 
       add_grear-wing_bottom9.1-0 ; 
       add_grear-wing_top8.1-0 ; 
       add_grear-wing_top9.1-0 ; 
       gear-t2d53.1-0 ; 
       gear-t2d54.1-0 ; 
       gear-t2d55.1-0 ; 
       gear-t2d56.1-0 ; 
       gear-t2d58.1-0 ; 
       gear-t2d59.1-0 ; 
       gear-t2d79.1-0 ; 
       gear-t2d80.1-0 ; 
       gear-t2d81.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       17 10 110 ; 
       0 10 110 ; 
       9 10 110 ; 
       16 10 110 ; 
       11 10 110 ; 
       4 10 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       1 18 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       5 10 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       18 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 5 300 ; 
       2 6 300 ; 
       3 7 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       7 16 300 ; 
       7 21 300 ; 
       7 22 300 ; 
       8 17 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       10 10 300 ; 
       10 1 300 ; 
       10 0 300 ; 
       10 2 300 ; 
       12 3 300 ; 
       12 8 300 ; 
       13 4 300 ; 
       13 9 300 ; 
       14 12 300 ; 
       14 14 300 ; 
       15 13 300 ; 
       15 15 300 ; 
       18 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       10 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 11 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 7 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 23 401 ; 
       17 24 401 ; 
       18 22 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       17 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 MPRFLG 0 ; 
       7 SCHEM 40 -4 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       12 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       18 SCHEM 31.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 90 90 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
