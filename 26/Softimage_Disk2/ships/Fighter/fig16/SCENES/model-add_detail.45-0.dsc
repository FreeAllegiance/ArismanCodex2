SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.64-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       add_detail-bottom1.3-0 ; 
       add_detail-default1.10-0 ; 
       add_detail-default3.1-0 ; 
       add_detail-default4.1-0 ; 
       add_detail-default5.3-0 ; 
       add_detail-default6.1-0 ; 
       add_detail-engines1.2-0 ; 
       add_detail-inside1.1-0 ; 
       add_detail-inside2.1-0 ; 
       add_detail-intakes1.1-0 ; 
       add_detail-outside1.2-0 ; 
       add_detail-outside2.1-0 ; 
       add_detail-Rear_sides1.4-0 ; 
       add_detail-rear_top1.5-0 ; 
       add_detail-tur1.1-0 ; 
       add_detail-upper_cockpit1.6-0 ; 
       add_detail-wing_bottom1.1-0 ; 
       add_detail-wing_bottom2.1-0 ; 
       add_detail-wing_top1.1-0 ; 
       add_detail-wing_top2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       add_detail-body1.37-0 ROOT ; 
       add_detail-l-wing.1-0 ; 
       add_detail-r-stablat.1-0 ; 
       add_detail-r-stablat1.1-0 ; 
       add_detail-r-wing.1-0 ; 
       add_detail-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-add_detail.45-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       add_detail-bottom1.8-0 ; 
       add_detail-engines1.6-0 ; 
       add_detail-inside1.2-0 ; 
       add_detail-inside2.2-0 ; 
       add_detail-intakes1.4-0 ; 
       add_detail-outside.2-0 ; 
       add_detail-outside1.2-0 ; 
       add_detail-rear_sides1.9-0 ; 
       add_detail-rear_top1.10-0 ; 
       add_detail-tur1.2-0 ; 
       add_detail-upper_cockpit1.12-0 ; 
       add_detail-wing_bottom1.2-0 ; 
       add_detail-wing_bottom2.2-0 ; 
       add_detail-wing_top1.3-0 ; 
       add_detail-wing_top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       3 0 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 15 300 ; 
       0 13 300 ; 
       0 12 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       0 9 300 ; 
       1 3 300 ; 
       1 19 300 ; 
       1 17 300 ; 
       3 5 300 ; 
       3 11 300 ; 
       3 8 300 ; 
       2 4 300 ; 
       2 10 300 ; 
       2 7 300 ; 
       4 2 300 ; 
       4 18 300 ; 
       4 16 300 ; 
       5 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       14 9 401 ; 
       15 10 401 ; 
       13 8 401 ; 
       12 7 401 ; 
       0 0 401 ; 
       6 1 401 ; 
       9 4 401 ; 
       18 13 401 ; 
       16 11 401 ; 
       19 14 401 ; 
       17 12 401 ; 
       10 5 401 ; 
       7 2 401 ; 
       11 6 401 ; 
       8 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       9 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
