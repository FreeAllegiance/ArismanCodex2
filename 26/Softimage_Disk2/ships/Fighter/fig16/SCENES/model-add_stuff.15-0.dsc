SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.182-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       add_stuff-back6.1-0 ; 
       add_stuff-engine7.1-0 ; 
       add_stuff-inside5.1-0 ; 
       add_stuff-inside6.1-0 ; 
       add_stuff-mat1.1-0 ; 
       add_stuff-mat2.1-0 ; 
       add_stuff-mat3.1-0 ; 
       add_stuff-outside6.1-0 ; 
       add_stuff-outside7.1-0 ; 
       add_stuff-sides5.1-0 ; 
       add_stuff-tur3.1-0 ; 
       add_stuff-wing_bottom7.1-0 ; 
       add_stuff-wing_bottom8.1-0 ; 
       add_stuff-wing_top8.1-0 ; 
       add_stuff-wing_top9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       add_stuff-cyl1.1-0 ; 
       add_stuff-cyl2.4-0 ; 
       add_stuff-cyl3.1-0 ; 
       add_stuff-map.7-0 ; 
       add_stuff-null1.3-0 ROOT ; 
       add_stuff-r-stablat2.1-0 ; 
       add_stuff-r-stablat3.1-0 ; 
       add_stuff-r-wing1.1-0 ; 
       add_stuff-r-wing2.1-0 ; 
       add_stuff-tur_pod1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-add_stuff.15-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       add_stuff-engine_map5.1-0 ; 
       add_stuff-outside5.1-0 ; 
       add_stuff-outside6.1-0 ; 
       add_stuff-t2d27.1-0 ; 
       add_stuff-t2d28.1-0 ; 
       add_stuff-t2d29.1-0 ; 
       add_stuff-t2d30.1-0 ; 
       add_stuff-t2d31.1-0 ; 
       add_stuff-t2d32.2-0 ; 
       add_stuff-t2d33.1-0 ; 
       add_stuff-t2d34.1-0 ; 
       add_stuff-wing_bottom8.1-0 ; 
       add_stuff-wing_bottom9.1-0 ; 
       add_stuff-wing_top8.1-0 ; 
       add_stuff-wing_top9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 4 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       9 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       3 9 300 ; 
       3 1 300 ; 
       3 0 300 ; 
       7 11 300 ; 
       7 13 300 ; 
       8 12 300 ; 
       8 14 300 ; 
       5 2 300 ; 
       5 7 300 ; 
       6 3 300 ; 
       6 8 300 ; 
       9 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 8 401 ; 
       5 9 401 ; 
       6 10 401 ; 
       9 3 401 ; 
       1 0 401 ; 
       0 4 401 ; 
       11 11 401 ; 
       13 13 401 ; 
       12 12 401 ; 
       14 14 401 ; 
       2 5 401 ; 
       7 1 401 ; 
       3 6 401 ; 
       8 2 401 ; 
       10 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
