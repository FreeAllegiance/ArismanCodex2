SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.90-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 7     
       after_rendermap-light1.3-0 ROOT ; 
       after_rendermap-light2.3-0 ROOT ; 
       after_rendermap-light3.3-0 ROOT ; 
       after_rendermap-light4.3-0 ROOT ; 
       after_rendermap-light5.3-0 ROOT ; 
       after_rendermap-light6.3-0 ROOT ; 
       after_rendermap-light7.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       after_rendermap-bottom1.1-0 ; 
       after_rendermap-bottom2.2-0 ; 
       after_rendermap-decal1.1-0 ; 
       after_rendermap-default.2-0 ; 
       after_rendermap-default1.1-0 ; 
       after_rendermap-default3.1-0 ; 
       after_rendermap-default4.1-0 ; 
       after_rendermap-default5.1-0 ; 
       after_rendermap-default6.1-0 ; 
       after_rendermap-engines1.1-0 ; 
       after_rendermap-inside1.1-0 ; 
       after_rendermap-inside2.1-0 ; 
       after_rendermap-intakes1.1-0 ; 
       after_rendermap-outside1.1-0 ; 
       after_rendermap-outside2.1-0 ; 
       after_rendermap-Rear_sides1.1-0 ; 
       after_rendermap-rear_top1.1-0 ; 
       after_rendermap-side1.2-0 ; 
       after_rendermap-top1.2-0 ; 
       after_rendermap-tur1.1-0 ; 
       after_rendermap-upper_cockpit1.1-0 ; 
       after_rendermap-wing_bottom1.1-0 ; 
       after_rendermap-wing_bottom2.1-0 ; 
       after_rendermap-wing_top1.1-0 ; 
       after_rendermap-wing_top2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       after_rendermap-body1.1-0 ROOT ; 
       after_rendermap-l-wing.1-0 ; 
       after_rendermap-r-stablat.1-0 ; 
       after_rendermap-r-stablat1.1-0 ; 
       after_rendermap-r-wing.1-0 ; 
       after_rendermap-tur_pod.1-0 ; 
       texture3-body1.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-after_rendermap.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 21     
       after_rendermap-bottom_map1.2-0 ; 
       after_rendermap-bottom1.1-0 ; 
       after_rendermap-decal1.1-0 ; 
       after_rendermap-engines1.1-0 ; 
       after_rendermap-inside1.1-0 ; 
       after_rendermap-inside2.1-0 ; 
       after_rendermap-intakes1.1-0 ; 
       after_rendermap-outside.1-0 ; 
       after_rendermap-outside1.1-0 ; 
       after_rendermap-rear_sides1.1-0 ; 
       after_rendermap-rear_top1.1-0 ; 
       after_rendermap-side.2-0 ; 
       after_rendermap-side_map1.2-0 ; 
       after_rendermap-top_map1.2-0 ; 
       after_rendermap-top1.2-0 ; 
       after_rendermap-tur1.1-0 ; 
       after_rendermap-upper_cockpit1.1-0 ; 
       after_rendermap-wing_bottom1.1-0 ; 
       after_rendermap-wing_bottom2.1-0 ; 
       after_rendermap-wing_top1.1-0 ; 
       after_rendermap-wing_top2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 20 300 ; 
       0 16 300 ; 
       0 15 300 ; 
       0 0 300 ; 
       0 9 300 ; 
       0 12 300 ; 
       0 2 300 ; 
       1 6 300 ; 
       1 24 300 ; 
       1 22 300 ; 
       2 7 300 ; 
       2 13 300 ; 
       2 10 300 ; 
       3 8 300 ; 
       3 14 300 ; 
       3 11 300 ; 
       4 5 300 ; 
       4 23 300 ; 
       4 21 300 ; 
       5 19 300 ; 
       6 3 300 ; 
       6 18 300 ; 
       6 1 300 ; 
       6 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 11 400 ; 
       6 14 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 0 401 ; 
       2 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 61.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
