SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.166-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       all_set_up-back6.1-0 ; 
       all_set_up-engine7.1-0 ; 
       all_set_up-inside5.1-0 ; 
       all_set_up-inside6.1-0 ; 
       all_set_up-outside6.1-0 ; 
       all_set_up-outside7.1-0 ; 
       all_set_up-sides5.1-0 ; 
       all_set_up-tur3.1-0 ; 
       all_set_up-wing_bottom7.1-0 ; 
       all_set_up-wing_bottom8.1-0 ; 
       all_set_up-wing_top8.1-0 ; 
       all_set_up-wing_top9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       texture18-map.3-0 ROOT ; 
       texture18-r-stablat2.1-0 ; 
       texture18-r-stablat3.1-0 ; 
       texture18-r-wing1.1-0 ; 
       texture18-r-wing2.1-0 ; 
       texture18-tur_pod1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-all_set_up.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       all_set_up-engine_map5.2-0 ; 
       all_set_up-outside5.1-0 ; 
       all_set_up-outside6.1-0 ; 
       all_set_up-t2d27.2-0 ; 
       all_set_up-t2d28.2-0 ; 
       all_set_up-t2d29.1-0 ; 
       all_set_up-t2d30.1-0 ; 
       all_set_up-t2d31.1-0 ; 
       all_set_up-wing_bottom8.1-0 ; 
       all_set_up-wing_bottom9.1-0 ; 
       all_set_up-wing_top8.1-0 ; 
       all_set_up-wing_top9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       0 1 300 ; 
       0 0 300 ; 
       3 8 300 ; 
       3 10 300 ; 
       4 9 300 ; 
       4 11 300 ; 
       1 2 300 ; 
       1 4 300 ; 
       2 3 300 ; 
       2 5 300 ; 
       5 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 3 401 ; 
       1 0 401 ; 
       0 4 401 ; 
       8 8 401 ; 
       10 10 401 ; 
       9 9 401 ; 
       11 11 401 ; 
       2 5 401 ; 
       4 1 401 ; 
       3 6 401 ; 
       5 2 401 ; 
       7 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
