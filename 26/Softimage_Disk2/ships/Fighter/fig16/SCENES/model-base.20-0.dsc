SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.20-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       base-body.7-0 ROOT ; 
       base-forward.6-0 ; 
       base-l_stablat.1-0 ; 
       base-l_wing.1-0 ; 
       base-r-stablat.1-0 ; 
       base-r-wing.1-0 ; 
       base-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-base.20-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 1 110 ; 
       1 0 110 ; 
       3 0 110 ; 
       5 0 110 ; 
       4 0 110 ; 
       2 0 110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 -6 0 MPRFLG 0 ; 
       1 SCHEM 0 -4 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
