SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.188-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       fool_with_geo-back6.2-0 ; 
       fool_with_geo-engine7.2-0 ; 
       fool_with_geo-fiiped_decal1.1-0 ; 
       fool_with_geo-inside5.1-0 ; 
       fool_with_geo-inside6.1-0 ; 
       fool_with_geo-mat1.1-0 ; 
       fool_with_geo-mat2.1-0 ; 
       fool_with_geo-mat3.1-0 ; 
       fool_with_geo-outside6.1-0 ; 
       fool_with_geo-outside7.1-0 ; 
       fool_with_geo-sides5.2-0 ; 
       fool_with_geo-tur3.1-0 ; 
       fool_with_geo-wing_bottom7.1-0 ; 
       fool_with_geo-wing_bottom8.1-0 ; 
       fool_with_geo-wing_top8.1-0 ; 
       fool_with_geo-wing_top9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       fool_with_geo-cyl1.1-0 ; 
       fool_with_geo-cyl2.4-0 ; 
       fool_with_geo-cyl3.1-0 ; 
       fool_with_geo-map.7-0 ; 
       fool_with_geo-null1.3-0 ROOT ; 
       fool_with_geo-r-stablat2.1-0 ; 
       fool_with_geo-r-stablat3.1-0 ; 
       fool_with_geo-r-wing1.1-0 ; 
       fool_with_geo-r-wing2.1-0 ; 
       fool_with_geo-tur_pod1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fool_with_geo.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       fool_with_geo-engine_map5.3-0 ; 
       fool_with_geo-outside5.1-0 ; 
       fool_with_geo-outside6.1-0 ; 
       fool_with_geo-t2d27.3-0 ; 
       fool_with_geo-t2d28.3-0 ; 
       fool_with_geo-t2d29.1-0 ; 
       fool_with_geo-t2d30.1-0 ; 
       fool_with_geo-t2d31.1-0 ; 
       fool_with_geo-t2d32.1-0 ; 
       fool_with_geo-t2d33.1-0 ; 
       fool_with_geo-t2d34.1-0 ; 
       fool_with_geo-t2d35.1-0 ; 
       fool_with_geo-wing_bottom8.1-0 ; 
       fool_with_geo-wing_bottom9.1-0 ; 
       fool_with_geo-wing_top8.1-0 ; 
       fool_with_geo-wing_top9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 4 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       9 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       1 6 300 ; 
       2 7 300 ; 
       3 10 300 ; 
       3 1 300 ; 
       3 0 300 ; 
       3 2 300 ; 
       7 12 300 ; 
       7 14 300 ; 
       8 13 300 ; 
       8 15 300 ; 
       5 3 300 ; 
       5 8 300 ; 
       6 4 300 ; 
       6 9 300 ; 
       9 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       2 11 401 ; 
       10 3 401 ; 
       1 0 401 ; 
       0 4 401 ; 
       12 12 401 ; 
       14 14 401 ; 
       13 13 401 ; 
       15 15 401 ; 
       3 5 401 ; 
       8 1 401 ; 
       4 6 401 ; 
       9 2 401 ; 
       11 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 42.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
