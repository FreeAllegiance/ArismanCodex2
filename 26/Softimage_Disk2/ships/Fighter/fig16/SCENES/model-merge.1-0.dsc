SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.22-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       merge-default1.1-0 ; 
       merge-default2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       merge-bmerge1.1-0 ROOT ; 
       merge-body.1-0 ROOT ; 
       merge-forward.6-0 ; 
       merge-l_stablat.1-0 ; 
       merge-l_wing.1-0 ; 
       merge-r-stablat.1-0 ; 
       merge-r-wing.1-0 ; 
       merge-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-merge.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 2 110 ; 
       2 1 110 ; 
       4 1 110 ; 
       6 1 110 ; 
       5 1 110 ; 
       3 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 5 -2 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 0 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 0 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
