SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.158-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 56     
       new-back1.10-0 ; 
       new-back2.14-0 ; 
       new-back3.2-0 ; 
       new-back4.1-0 ; 
       new-back5.1-0 ; 
       new-bottom1.3-0 ; 
       new-decal1.3-0 ; 
       new-default1.3-0 ; 
       new-default3.3-0 ; 
       new-default4.3-0 ; 
       new-default5.3-0 ; 
       new-default6.3-0 ; 
       new-engine1.9-0 ; 
       new-engine2.14-0 ; 
       new-engine3.5-0 ; 
       new-engine4.2-0 ; 
       new-engine5.2-0 ; 
       new-engine6.1-0 ; 
       new-engines1.3-0 ; 
       new-inside.3-0 ; 
       new-inside1.3-0 ; 
       new-inside2.3-0 ; 
       new-inside3.1-0 ; 
       new-inside4.1-0 ; 
       new-intakes1.3-0 ; 
       new-mat1.4-0 ; 
       new-other_side1.12-0 ; 
       new-outside1.3-0 ; 
       new-outside2.3-0 ; 
       new-outside3.3-0 ; 
       new-outside4.1-0 ; 
       new-outside5.1-0 ; 
       new-Rear_sides1.3-0 ; 
       new-rear_top1.3-0 ; 
       new-sides.11-0 ; 
       new-sides1.15-0 ; 
       new-sides2.2-0 ; 
       new-sides3.1-0 ; 
       new-sides4.1-0 ; 
       new-tur1.3-0 ; 
       new-tur2.1-0 ; 
       new-upper_cockpit1.3-0 ; 
       new-wing_bottom.4-0 ; 
       new-wing_bottom1.3-0 ; 
       new-wing_bottom2.3-0 ; 
       new-wing_bottom3.5-0 ; 
       new-wing_bottom4.3-0 ; 
       new-wing_bottom5.3-0 ; 
       new-wing_bottom6.1-0 ; 
       new-wing_top1.3-0 ; 
       new-wing_top2.3-0 ; 
       new-wing_top3.7-0 ; 
       new-wing_top4.4-0 ; 
       new-wing_top5.3-0 ; 
       new-wing_top6.1-0 ; 
       new-wing_top7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       new-l-wing.1-0 ; 
       new-orig.4-0 ROOT ; 
       new-r-stablat.1-0 ; 
       new-r-stablat1.1-0 ; 
       new-r-wing.1-0 ; 
       new-tur_pod.1-0 ; 
       texture14-map.10-0 ROOT ; 
       texture15-map.10-0 ROOT ; 
       texture15-r-wing1.1-0 ; 
       texture16-map.7-0 ROOT ; 
       texture16-r-stablat2.1-0 ; 
       texture16-r-wing1.1-0 ; 
       texture16-r-wing2.1-0 ; 
       texture17-map.4-0 ROOT ; 
       texture17-r-stablat2.1-0 ; 
       texture17-r-stablat3.1-0 ; 
       texture17-r-wing1.1-0 ; 
       texture17-r-wing2.1-0 ; 
       texture17-tur_pod1.1-0 ; 
       texture3-map.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 13     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/bottom-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/cockpit ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/engine ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/engine_map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16old ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/stripe ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/turmap ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing_no_step ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-new.55-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 54     
       new-bottom1.3-0 ; 
       new-decal1.3-0 ; 
       new-engines1.3-0 ; 
       new-engine_map1.3-0 ; 
       new-engine_map2.4-0 ; 
       new-engine_map3.1-0 ; 
       new-engine_map4.1-0 ; 
       new-engine1.3-0 ; 
       new-inside1.3-0 ; 
       new-inside2.3-0 ; 
       new-intakes1.3-0 ; 
       new-outside.3-0 ; 
       new-outside1.3-0 ; 
       new-outside2.2-0 ; 
       new-outside3.1-0 ; 
       new-outside4.1-0 ; 
       new-rear_sides1.3-0 ; 
       new-rear_top1.3-0 ; 
       new-side.15-0 ; 
       new-stripe1.4-0 ; 
       new-t2d10.11-0 ; 
       new-t2d12.15-0 ; 
       new-t2d13.4-0 ; 
       new-t2d14.2-0 ; 
       new-t2d15.4-0 ; 
       new-t2d16.3-0 ; 
       new-t2d17.1-0 ; 
       new-t2d18.1-0 ; 
       new-t2d19.2-0 ; 
       new-t2d20.1-0 ; 
       new-t2d21.1-0 ; 
       new-t2d22.1-0 ; 
       new-t2d24.1-0 ; 
       new-t2d25.1-0 ; 
       new-t2d26.2-0 ; 
       new-t2d8.14-0 ; 
       new-top1.15-0 ; 
       new-tur1.3-0 ; 
       new-tur2.3-0 ; 
       new-upper_cockpit1.3-0 ; 
       new-wing_bottom1.3-0 ; 
       new-wing_bottom2.3-0 ; 
       new-wing_bottom3.4-0 ; 
       new-wing_bottom4.2-0 ; 
       new-wing_bottom5.1-0 ; 
       new-wing_bottom6.1-0 ; 
       new-wing_bottom7.1-0 ; 
       new-wing_top1.3-0 ; 
       new-wing_top2.3-0 ; 
       new-wing_top3.5-0 ; 
       new-wing_top4.2-0 ; 
       new-wing_top5.1-0 ; 
       new-wing_top6.1-0 ; 
       new-wing_top7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 7 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       10 9 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       18 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 35 300 ; 
       6 14 300 ; 
       6 1 300 ; 
       6 13 300 ; 
       7 36 300 ; 
       7 15 300 ; 
       7 2 300 ; 
       8 42 300 ; 
       8 51 300 ; 
       9 37 300 ; 
       9 16 300 ; 
       9 3 300 ; 
       11 45 300 ; 
       11 52 300 ; 
       12 46 300 ; 
       12 53 300 ; 
       10 19 300 ; 
       10 29 300 ; 
       13 38 300 ; 
       13 17 300 ; 
       13 4 300 ; 
       16 47 300 ; 
       16 54 300 ; 
       17 48 300 ; 
       17 55 300 ; 
       0 9 300 ; 
       0 50 300 ; 
       0 44 300 ; 
       1 7 300 ; 
       1 41 300 ; 
       1 33 300 ; 
       1 32 300 ; 
       1 5 300 ; 
       1 18 300 ; 
       1 24 300 ; 
       1 6 300 ; 
       2 10 300 ; 
       2 27 300 ; 
       2 20 300 ; 
       3 11 300 ; 
       3 28 300 ; 
       3 21 300 ; 
       4 8 300 ; 
       4 49 300 ; 
       4 43 300 ; 
       5 39 300 ; 
       14 22 300 ; 
       14 30 300 ; 
       19 34 300 ; 
       19 26 300 ; 
       19 0 300 ; 
       19 12 300 ; 
       19 25 300 ; 
       15 23 300 ; 
       15 31 300 ; 
       18 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 7 400 ; 
       8 24 400 ; 
       8 25 400 ; 
       10 29 400 ; 
       19 18 400 ; 
       19 36 400 ; 
       19 20 400 ; 
       19 19 400 ; 
       18 34 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       35 21 401 ; 
       14 3 401 ; 
       6 1 401 ; 
       18 2 401 ; 
       20 8 401 ; 
       21 9 401 ; 
       24 10 401 ; 
       36 22 401 ; 
       15 4 401 ; 
       2 23 401 ; 
       27 11 401 ; 
       28 12 401 ; 
       32 16 401 ; 
       33 17 401 ; 
       34 35 401 ; 
       42 42 401 ; 
       51 49 401 ; 
       37 26 401 ; 
       39 37 401 ; 
       41 39 401 ; 
       43 40 401 ; 
       44 41 401 ; 
       16 5 401 ; 
       49 47 401 ; 
       50 48 401 ; 
       3 27 401 ; 
       45 43 401 ; 
       52 50 401 ; 
       46 44 401 ; 
       53 51 401 ; 
       19 28 401 ; 
       29 13 401 ; 
       38 30 401 ; 
       17 6 401 ; 
       4 31 401 ; 
       47 45 401 ; 
       54 52 401 ; 
       48 46 401 ; 
       55 53 401 ; 
       22 32 401 ; 
       30 14 401 ; 
       23 33 401 ; 
       31 15 401 ; 
       40 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 82.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       7 SCHEM 97.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       8 SCHEM 93.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 118.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       11 SCHEM 108.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 113.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 120 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 147.5 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       16 SCHEM 133.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 138.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 143.75 -2 0 MPRFLG 0 ; 
       19 SCHEM 65 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       15 SCHEM 148.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 153.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 125 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 127.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 157.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 160 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 162.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 145 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 147.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 150 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 70 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 60 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 105 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 125 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 127.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 130 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 122.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 157.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 160 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 162.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 140 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 147.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 145 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 150 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 152.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 155 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
