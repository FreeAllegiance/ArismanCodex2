SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.161-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       new-back1.10-0 ; 
       new-back2.14-0 ; 
       new-back3.2-0 ; 
       new-back4.1-0 ; 
       new-back5.1-0 ; 
       new-back6.1-0 ; 
       new-bottom1.3-0 ; 
       new-decal1.3-0 ; 
       new-default1.3-0 ; 
       new-default3.3-0 ; 
       new-default4.3-0 ; 
       new-default5.3-0 ; 
       new-default6.3-0 ; 
       new-engine1.9-0 ; 
       new-engine2.14-0 ; 
       new-engine3.5-0 ; 
       new-engine4.2-0 ; 
       new-engine5.2-0 ; 
       new-engine6.1-0 ; 
       new-engine7.1-0 ; 
       new-engines1.3-0 ; 
       new-inside.3-0 ; 
       new-inside1.3-0 ; 
       new-inside2.3-0 ; 
       new-inside3.1-0 ; 
       new-inside4.1-0 ; 
       new-inside5.1-0 ; 
       new-inside6.1-0 ; 
       new-intakes1.3-0 ; 
       new-mat1.4-0 ; 
       new-other_side1.12-0 ; 
       new-outside1.3-0 ; 
       new-outside2.3-0 ; 
       new-outside3.3-0 ; 
       new-outside4.1-0 ; 
       new-outside5.1-0 ; 
       new-outside6.1-0 ; 
       new-outside7.1-0 ; 
       new-Rear_sides1.3-0 ; 
       new-rear_top1.3-0 ; 
       new-sides.11-0 ; 
       new-sides1.15-0 ; 
       new-sides2.2-0 ; 
       new-sides3.1-0 ; 
       new-sides4.1-0 ; 
       new-sides5.1-0 ; 
       new-tur1.3-0 ; 
       new-tur2.3-0 ; 
       new-tur3.1-0 ; 
       new-upper_cockpit1.3-0 ; 
       new-wing_bottom.4-0 ; 
       new-wing_bottom1.3-0 ; 
       new-wing_bottom2.3-0 ; 
       new-wing_bottom3.5-0 ; 
       new-wing_bottom4.3-0 ; 
       new-wing_bottom5.3-0 ; 
       new-wing_bottom6.1-0 ; 
       new-wing_bottom7.1-0 ; 
       new-wing_bottom8.1-0 ; 
       new-wing_top1.3-0 ; 
       new-wing_top2.3-0 ; 
       new-wing_top3.7-0 ; 
       new-wing_top4.4-0 ; 
       new-wing_top5.3-0 ; 
       new-wing_top6.1-0 ; 
       new-wing_top7.1-0 ; 
       new-wing_top8.1-0 ; 
       new-wing_top9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       new-l-wing.1-0 ; 
       new-orig.4-0 ROOT ; 
       new-r-stablat.1-0 ; 
       new-r-stablat1.1-0 ; 
       new-r-wing.1-0 ; 
       new-tur_pod.1-0 ; 
       texture14-map.10-0 ROOT ; 
       texture15-map.10-0 ROOT ; 
       texture15-r-wing1.1-0 ; 
       texture16-map.7-0 ROOT ; 
       texture16-r-stablat2.1-0 ; 
       texture16-r-wing1.1-0 ; 
       texture16-r-wing2.1-0 ; 
       texture17-map.7-0 ROOT ; 
       texture17-r-stablat2.1-0 ; 
       texture17-r-stablat3.1-0 ; 
       texture17-r-wing1.1-0 ; 
       texture17-r-wing2.1-0 ; 
       texture17-tur_pod1.1-0 ; 
       texture18-map.1-0 ROOT ; 
       texture18-r-stablat2.1-0 ; 
       texture18-r-stablat3.1-0 ; 
       texture18-r-wing1.1-0 ; 
       texture18-r-wing2.1-0 ; 
       texture18-tur_pod1.1-0 ; 
       texture3-map.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 14     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/bottom-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/cockpit ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/engine ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/engine_map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16old ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/stripe ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/turmap ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/turrett ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing_no_step ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-new.58-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 66     
       new-bottom1.3-0 ; 
       new-decal1.3-0 ; 
       new-engines1.3-0 ; 
       new-engine_map1.3-0 ; 
       new-engine_map2.4-0 ; 
       new-engine_map3.1-0 ; 
       new-engine_map4.1-0 ; 
       new-engine_map5.1-0 ; 
       new-engine1.3-0 ; 
       new-inside1.3-0 ; 
       new-inside2.3-0 ; 
       new-intakes1.3-0 ; 
       new-outside.3-0 ; 
       new-outside1.3-0 ; 
       new-outside2.2-0 ; 
       new-outside3.1-0 ; 
       new-outside4.1-0 ; 
       new-outside5.1-0 ; 
       new-outside6.1-0 ; 
       new-rear_sides1.3-0 ; 
       new-rear_top1.3-0 ; 
       new-side.15-0 ; 
       new-stripe1.4-0 ; 
       new-t2d10.11-0 ; 
       new-t2d12.15-0 ; 
       new-t2d13.4-0 ; 
       new-t2d14.2-0 ; 
       new-t2d15.4-0 ; 
       new-t2d16.3-0 ; 
       new-t2d17.1-0 ; 
       new-t2d18.1-0 ; 
       new-t2d19.2-0 ; 
       new-t2d20.1-0 ; 
       new-t2d21.1-0 ; 
       new-t2d22.1-0 ; 
       new-t2d24.1-0 ; 
       new-t2d25.1-0 ; 
       new-t2d26.4-0 ; 
       new-t2d27.1-0 ; 
       new-t2d28.1-0 ; 
       new-t2d29.1-0 ; 
       new-t2d30.1-0 ; 
       new-t2d8.14-0 ; 
       new-top1.15-0 ; 
       new-tur1.3-0 ; 
       new-tur2.5-0 ; 
       new-tur3.1-0 ; 
       new-upper_cockpit1.3-0 ; 
       new-wing_bottom1.3-0 ; 
       new-wing_bottom2.3-0 ; 
       new-wing_bottom3.4-0 ; 
       new-wing_bottom4.2-0 ; 
       new-wing_bottom5.1-0 ; 
       new-wing_bottom6.1-0 ; 
       new-wing_bottom7.1-0 ; 
       new-wing_bottom8.1-0 ; 
       new-wing_bottom9.1-0 ; 
       new-wing_top1.3-0 ; 
       new-wing_top2.3-0 ; 
       new-wing_top3.5-0 ; 
       new-wing_top4.2-0 ; 
       new-wing_top5.1-0 ; 
       new-wing_top6.1-0 ; 
       new-wing_top7.1-0 ; 
       new-wing_top8.1-0 ; 
       new-wing_top9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 7 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       10 9 110 ; 
       16 13 110 ; 
       17 13 110 ; 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       18 13 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       24 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 41 300 ; 
       6 15 300 ; 
       6 1 300 ; 
       6 14 300 ; 
       7 42 300 ; 
       7 16 300 ; 
       7 2 300 ; 
       8 50 300 ; 
       8 61 300 ; 
       9 43 300 ; 
       9 17 300 ; 
       9 3 300 ; 
       11 53 300 ; 
       11 62 300 ; 
       12 54 300 ; 
       12 63 300 ; 
       10 21 300 ; 
       10 33 300 ; 
       13 44 300 ; 
       13 18 300 ; 
       13 4 300 ; 
       16 55 300 ; 
       16 64 300 ; 
       17 56 300 ; 
       17 65 300 ; 
       0 10 300 ; 
       0 60 300 ; 
       0 52 300 ; 
       1 8 300 ; 
       1 49 300 ; 
       1 39 300 ; 
       1 38 300 ; 
       1 6 300 ; 
       1 20 300 ; 
       1 28 300 ; 
       1 7 300 ; 
       2 11 300 ; 
       2 31 300 ; 
       2 22 300 ; 
       3 12 300 ; 
       3 32 300 ; 
       3 23 300 ; 
       4 9 300 ; 
       4 59 300 ; 
       4 51 300 ; 
       5 46 300 ; 
       14 24 300 ; 
       14 34 300 ; 
       25 40 300 ; 
       25 30 300 ; 
       25 0 300 ; 
       25 13 300 ; 
       25 29 300 ; 
       15 25 300 ; 
       15 35 300 ; 
       18 47 300 ; 
       19 45 300 ; 
       19 19 300 ; 
       19 5 300 ; 
       22 57 300 ; 
       22 66 300 ; 
       23 58 300 ; 
       23 67 300 ; 
       20 26 300 ; 
       20 36 300 ; 
       21 27 300 ; 
       21 37 300 ; 
       24 48 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 8 400 ; 
       8 27 400 ; 
       8 28 400 ; 
       10 32 400 ; 
       25 21 400 ; 
       25 43 400 ; 
       25 23 400 ; 
       25 22 400 ; 
       18 37 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       41 24 401 ; 
       15 3 401 ; 
       7 1 401 ; 
       20 2 401 ; 
       22 9 401 ; 
       23 10 401 ; 
       28 11 401 ; 
       42 25 401 ; 
       16 4 401 ; 
       2 26 401 ; 
       31 12 401 ; 
       32 13 401 ; 
       38 19 401 ; 
       39 20 401 ; 
       40 42 401 ; 
       50 50 401 ; 
       61 59 401 ; 
       43 29 401 ; 
       46 44 401 ; 
       49 47 401 ; 
       51 48 401 ; 
       52 49 401 ; 
       17 5 401 ; 
       59 57 401 ; 
       60 58 401 ; 
       3 30 401 ; 
       53 51 401 ; 
       62 60 401 ; 
       54 52 401 ; 
       63 61 401 ; 
       21 31 401 ; 
       33 14 401 ; 
       44 33 401 ; 
       18 6 401 ; 
       4 34 401 ; 
       55 53 401 ; 
       64 62 401 ; 
       56 54 401 ; 
       65 63 401 ; 
       24 35 401 ; 
       34 15 401 ; 
       25 36 401 ; 
       35 16 401 ; 
       47 45 401 ; 
       45 38 401 ; 
       19 7 401 ; 
       5 39 401 ; 
       57 55 401 ; 
       66 64 401 ; 
       58 56 401 ; 
       67 65 401 ; 
       26 40 401 ; 
       36 17 401 ; 
       27 41 401 ; 
       37 18 401 ; 
       48 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 82.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       7 SCHEM 97.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       8 SCHEM 93.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 118.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       11 SCHEM 108.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 113.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 120 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 147.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       16 SCHEM 133.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 138.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 143.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 65 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       15 SCHEM 148.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 153.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 178.75 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       22 SCHEM 166.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 171.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 176.25 -2 0 MPRFLG 0 ; 
       21 SCHEM 181.25 -2 0 MPRFLG 0 ; 
       24 SCHEM 185 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 125 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 127.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 157.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 160 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 162.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 145 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 147.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 150 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 187.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 190 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 192.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 165 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 167.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 170 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 180 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 182.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 185 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 75 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 70 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 60 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 105 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 125 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 127.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 130 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 122.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 157.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 160 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 162.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 140 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 147.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 145 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 150 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 152.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 155 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 187.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 190 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 192.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 165 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 167.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 170 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 180 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 182.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 185 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
