SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.103-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rendermap-light1.11-0 ROOT ; 
       rendermap-light2.11-0 ROOT ; 
       rendermap-light3.11-0 ROOT ; 
       rendermap-light4.11-0 ROOT ; 
       rendermap-light5.11-0 ROOT ; 
       rendermap-light7.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       rendermap-bottom1.2-0 ; 
       rendermap-bottom2.3-0 ; 
       rendermap-bottom3.3-0 ; 
       rendermap-decal1.2-0 ; 
       rendermap-default.3-0 ; 
       rendermap-default1.2-0 ; 
       rendermap-default10.1-0 ; 
       rendermap-default3.2-0 ; 
       rendermap-default4.2-0 ; 
       rendermap-default5.2-0 ; 
       rendermap-default6.2-0 ; 
       rendermap-default7.3-0 ; 
       rendermap-default8.1-0 ; 
       rendermap-default9.2-0 ; 
       rendermap-engines1.2-0 ; 
       rendermap-inside1.2-0 ; 
       rendermap-inside2.2-0 ; 
       rendermap-intakes1.2-0 ; 
       rendermap-other_side1.2-0 ; 
       rendermap-outside1.2-0 ; 
       rendermap-outside2.2-0 ; 
       rendermap-Rear_sides1.2-0 ; 
       rendermap-rear_top1.2-0 ; 
       rendermap-side1.3-0 ; 
       rendermap-side2.3-0 ; 
       rendermap-top1.3-0 ; 
       rendermap-top2.3-0 ; 
       rendermap-tur1.2-0 ; 
       rendermap-upper_cockpit1.2-0 ; 
       rendermap-wing_bottom1.2-0 ; 
       rendermap-wing_bottom2.2-0 ; 
       rendermap-wing_bottom3.1-0 ; 
       rendermap-wing_bottom4.2-0 ; 
       rendermap-wing_bottom5.1-0 ; 
       rendermap-wing_top1.2-0 ; 
       rendermap-wing_top2.2-0 ; 
       rendermap-wing_top3.1-0 ; 
       rendermap-wing_top4.2-0 ; 
       rendermap-wing_top5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       rendermap-l-wing.1-0 ; 
       rendermap-orig.2-0 ROOT ; 
       rendermap-r-stablat.1-0 ; 
       rendermap-r-stablat1.1-0 ; 
       rendermap-r-wing.1-0 ; 
       rendermap-tur_pod.1-0 ; 
       texture3-l-wing1.1-0 ; 
       texture3-map.4-0 ROOT ; 
       texture4-l-wing2.1-0 ; 
       texture4-l-wing3.1-0 ; 
       texture4-post_map.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/bottom-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16old ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing-map ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-rendermap.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       rendermap-bottom_map1.2-0 ; 
       rendermap-bottom_map2.5-0 ; 
       rendermap-bottom1.2-0 ; 
       rendermap-decal1.2-0 ; 
       rendermap-engines1.2-0 ; 
       rendermap-inside1.2-0 ; 
       rendermap-inside2.2-0 ; 
       rendermap-intakes1.2-0 ; 
       rendermap-outside.2-0 ; 
       rendermap-outside1.2-0 ; 
       rendermap-rear_sides1.2-0 ; 
       rendermap-rear_top1.2-0 ; 
       rendermap-side.2-0 ; 
       rendermap-side_map1.2-0 ; 
       rendermap-side_map2.5-0 ; 
       rendermap-t2d1.1-0 ; 
       rendermap-t2d2.1-0 ; 
       rendermap-t2d4.2-0 ; 
       rendermap-t2d5.1-0 ; 
       rendermap-t2d6.1-0 ; 
       rendermap-t2d7.1-0 ; 
       rendermap-top_map1.2-0 ; 
       rendermap-top_map2.5-0 ; 
       rendermap-top1.2-0 ; 
       rendermap-tur1.2-0 ; 
       rendermap-upper_cockpit1.2-0 ; 
       rendermap-wing_bottom1.2-0 ; 
       rendermap-wing_bottom2.2-0 ; 
       rendermap-wing_top1.2-0 ; 
       rendermap-wing_top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 7 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 12 300 ; 
       6 36 300 ; 
       6 31 300 ; 
       8 13 300 ; 
       8 37 300 ; 
       8 32 300 ; 
       9 6 300 ; 
       9 38 300 ; 
       9 33 300 ; 
       0 8 300 ; 
       0 35 300 ; 
       0 30 300 ; 
       1 5 300 ; 
       1 28 300 ; 
       1 22 300 ; 
       1 21 300 ; 
       1 0 300 ; 
       1 14 300 ; 
       1 17 300 ; 
       1 3 300 ; 
       2 9 300 ; 
       2 19 300 ; 
       2 15 300 ; 
       3 10 300 ; 
       3 20 300 ; 
       3 16 300 ; 
       4 7 300 ; 
       4 34 300 ; 
       4 29 300 ; 
       5 27 300 ; 
       7 4 300 ; 
       7 25 300 ; 
       7 1 300 ; 
       7 23 300 ; 
       7 18 300 ; 
       10 11 300 ; 
       10 26 300 ; 
       10 2 300 ; 
       10 24 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 15 400 ; 
       7 12 400 ; 
       7 23 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       19 8 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 11 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 16 401 ; 
       37 17 401 ; 
       32 18 401 ; 
       38 19 401 ; 
       33 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 112.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 115 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 117.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 120 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 58.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 85 -2 0 MPRFLG 0 ; 
       9 SCHEM 92.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       10 SCHEM 93.75 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 102.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 105 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 102.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 105 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 72.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 70 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
