SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.100-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rendermap-light1.8-0 ROOT ; 
       rendermap-light2.8-0 ROOT ; 
       rendermap-light3.8-0 ROOT ; 
       rendermap-light4.8-0 ROOT ; 
       rendermap-light5.8-0 ROOT ; 
       rendermap-light7.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       rendermap-bottom1.2-0 ; 
       rendermap-bottom2.3-0 ; 
       rendermap-bottom3.3-0 ; 
       rendermap-decal1.2-0 ; 
       rendermap-default.3-0 ; 
       rendermap-default1.2-0 ; 
       rendermap-default3.2-0 ; 
       rendermap-default4.2-0 ; 
       rendermap-default5.2-0 ; 
       rendermap-default6.2-0 ; 
       rendermap-default7.3-0 ; 
       rendermap-default8.1-0 ; 
       rendermap-default9.1-0 ; 
       rendermap-engines1.2-0 ; 
       rendermap-inside1.2-0 ; 
       rendermap-inside2.2-0 ; 
       rendermap-intakes1.2-0 ; 
       rendermap-other_side1.2-0 ; 
       rendermap-outside1.2-0 ; 
       rendermap-outside2.2-0 ; 
       rendermap-Rear_sides1.2-0 ; 
       rendermap-rear_top1.2-0 ; 
       rendermap-side1.3-0 ; 
       rendermap-side2.3-0 ; 
       rendermap-top1.3-0 ; 
       rendermap-top2.3-0 ; 
       rendermap-tur1.2-0 ; 
       rendermap-upper_cockpit1.2-0 ; 
       rendermap-wing_bottom1.2-0 ; 
       rendermap-wing_bottom2.2-0 ; 
       rendermap-wing_bottom3.1-0 ; 
       rendermap-wing_bottom4.1-0 ; 
       rendermap-wing_top1.2-0 ; 
       rendermap-wing_top2.2-0 ; 
       rendermap-wing_top3.1-0 ; 
       rendermap-wing_top4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       rendermap-l-wing.1-0 ; 
       rendermap-orig.2-0 ROOT ; 
       rendermap-r-stablat.1-0 ; 
       rendermap-r-stablat1.1-0 ; 
       rendermap-r-wing.1-0 ; 
       rendermap-tur_pod.1-0 ; 
       texture3-l-wing1.1-0 ; 
       texture3-map.4-0 ROOT ; 
       texture4-l-wing2.1-0 ; 
       texture4-post_map.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/bottom-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16old ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/side-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/top-map ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/wing-map ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-rendermap.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       rendermap-bottom_map1.2-0 ; 
       rendermap-bottom_map2.4-0 ; 
       rendermap-bottom1.2-0 ; 
       rendermap-decal1.2-0 ; 
       rendermap-engines1.2-0 ; 
       rendermap-inside1.2-0 ; 
       rendermap-inside2.2-0 ; 
       rendermap-intakes1.2-0 ; 
       rendermap-outside.2-0 ; 
       rendermap-outside1.2-0 ; 
       rendermap-rear_sides1.2-0 ; 
       rendermap-rear_top1.2-0 ; 
       rendermap-side.2-0 ; 
       rendermap-side_map1.2-0 ; 
       rendermap-side_map2.4-0 ; 
       rendermap-t2d1.1-0 ; 
       rendermap-t2d2.1-0 ; 
       rendermap-t2d3.1-0 ; 
       rendermap-t2d4.1-0 ; 
       rendermap-top_map1.2-0 ; 
       rendermap-top_map2.4-0 ; 
       rendermap-top1.2-0 ; 
       rendermap-tur1.2-0 ; 
       rendermap-upper_cockpit1.2-0 ; 
       rendermap-wing_bottom1.2-0 ; 
       rendermap-wing_bottom2.2-0 ; 
       rendermap-wing_top1.2-0 ; 
       rendermap-wing_top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 7 110 ; 
       8 9 110 ; 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 11 300 ; 
       6 34 300 ; 
       6 30 300 ; 
       8 12 300 ; 
       8 35 300 ; 
       8 31 300 ; 
       0 7 300 ; 
       0 33 300 ; 
       0 29 300 ; 
       1 5 300 ; 
       1 27 300 ; 
       1 21 300 ; 
       1 20 300 ; 
       1 0 300 ; 
       1 13 300 ; 
       1 16 300 ; 
       1 3 300 ; 
       2 8 300 ; 
       2 18 300 ; 
       2 14 300 ; 
       3 9 300 ; 
       3 19 300 ; 
       3 15 300 ; 
       4 6 300 ; 
       4 32 300 ; 
       4 28 300 ; 
       5 26 300 ; 
       7 4 300 ; 
       7 24 300 ; 
       7 1 300 ; 
       7 22 300 ; 
       7 17 300 ; 
       9 10 300 ; 
       9 25 300 ; 
       9 2 300 ; 
       9 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 15 400 ; 
       8 17 400 ; 
       7 12 400 ; 
       7 21 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 3 401 ; 
       13 4 401 ; 
       14 5 401 ; 
       15 6 401 ; 
       16 7 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 13 401 ; 
       23 14 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 16 401 ; 
       35 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 112.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 115 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 58.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       9 SCHEM 91.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 70 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 90 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
