SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.19-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       rendermapuv-light1.1-0 ROOT ; 
       rendermapuv-light4.1-0 ROOT ; 
       rendermapuv-light7.1-0 ROOT ; 
       rendermapuv-light8.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       rendermapuv-default1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       rendermapuv-body1.1-0 ROOT ; 
       rendermapuv-l_stablat.1-0 ; 
       rendermapuv-l_wing.1-0 ; 
       rendermapuv-r-stablat.1-0 ; 
       rendermapuv-r-wing.1-0 ; 
       rendermapuv-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/light ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-rendermapuv.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       rendermapuv-guide.1-0 ; 
       rendermapuv-rendermap1.1-0 ; 
       rendermapuv-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       0 0 401 ; 
       0 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
