SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.69-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       texture-bottom1.2-0 ; 
       texture-decal1.1-0 ; 
       texture-default1.2-0 ; 
       texture-default3.1-0 ; 
       texture-default4.1-0 ; 
       texture-default5.1-0 ; 
       texture-default6.1-0 ; 
       texture-engines1.2-0 ; 
       texture-inside1.1-0 ; 
       texture-inside2.1-0 ; 
       texture-intakes1.2-0 ; 
       texture-outside1.1-0 ; 
       texture-outside2.1-0 ; 
       texture-Rear_sides1.2-0 ; 
       texture-rear_top1.2-0 ; 
       texture-tur1.1-0 ; 
       texture-upper_cockpit1.2-0 ; 
       texture-wing_bottom1.1-0 ; 
       texture-wing_bottom2.1-0 ; 
       texture-wing_top1.1-0 ; 
       texture-wing_top2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       texture-body1.2-0 ROOT ; 
       texture-l-wing.1-0 ; 
       texture-r-stablat.1-0 ; 
       texture-r-stablat1.1-0 ; 
       texture-r-wing.1-0 ; 
       texture-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-texture.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       texture-bottom1.2-0 ; 
       texture-decal1.1-0 ; 
       texture-engines1.2-0 ; 
       texture-inside1.1-0 ; 
       texture-inside2.1-0 ; 
       texture-intakes1.2-0 ; 
       texture-outside.1-0 ; 
       texture-outside1.1-0 ; 
       texture-rear_sides1.2-0 ; 
       texture-rear_top1.2-0 ; 
       texture-tur1.1-0 ; 
       texture-upper_cockpit1.2-0 ; 
       texture-wing_bottom1.1-0 ; 
       texture-wing_bottom2.1-0 ; 
       texture-wing_top1.1-0 ; 
       texture-wing_top2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       3 0 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 16 300 ; 
       0 14 300 ; 
       0 13 300 ; 
       0 0 300 ; 
       0 7 300 ; 
       0 10 300 ; 
       0 1 300 ; 
       1 4 300 ; 
       1 20 300 ; 
       1 18 300 ; 
       3 6 300 ; 
       3 12 300 ; 
       3 9 300 ; 
       2 5 300 ; 
       2 11 300 ; 
       2 8 300 ; 
       4 3 300 ; 
       4 19 300 ; 
       4 17 300 ; 
       5 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       15 10 401 ; 
       16 11 401 ; 
       14 9 401 ; 
       13 8 401 ; 
       0 0 401 ; 
       7 2 401 ; 
       10 5 401 ; 
       19 14 401 ; 
       17 12 401 ; 
       20 15 401 ; 
       18 13 401 ; 
       11 6 401 ; 
       8 3 401 ; 
       12 7 401 ; 
       9 4 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       10 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
