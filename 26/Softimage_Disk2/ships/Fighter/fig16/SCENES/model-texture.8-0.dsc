SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.72-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       texture-bottom1.2-0 ; 
       texture-bottom2.1-0 ; 
       texture-decal1.1-0 ; 
       texture-decal2.1-0 ; 
       texture-default1.2-0 ; 
       texture-default3.1-0 ; 
       texture-default4.1-0 ; 
       texture-default5.1-0 ; 
       texture-default6.1-0 ; 
       texture-default7.1-0 ; 
       texture-engines1.2-0 ; 
       texture-engines2.1-0 ; 
       texture-inside1.1-0 ; 
       texture-inside2.1-0 ; 
       texture-intakes1.2-0 ; 
       texture-intakes2.1-0 ; 
       texture-outside1.1-0 ; 
       texture-outside2.1-0 ; 
       texture-Rear_sides1.2-0 ; 
       texture-Rear_sides2.1-0 ; 
       texture-rear_top1.2-0 ; 
       texture-rear_top2.1-0 ; 
       texture-tur1.1-0 ; 
       texture-upper_cockpit1.2-0 ; 
       texture-upper_cockpit2.1-0 ; 
       texture-wing_bottom1.1-0 ; 
       texture-wing_bottom2.1-0 ; 
       texture-wing_top1.1-0 ; 
       texture-wing_top2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       texture-body1.2-0 ROOT ; 
       texture-l-wing.1-0 ; 
       texture-r-stablat.1-0 ; 
       texture-r-stablat1.1-0 ; 
       texture-r-wing.1-0 ; 
       texture-tur_pod.1-0 ; 
       texture3-body1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-texture.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       texture-bottom1.2-0 ; 
       texture-bottom2.1-0 ; 
       texture-decal1.1-0 ; 
       texture-decal2.1-0 ; 
       texture-engines1.2-0 ; 
       texture-engines2.1-0 ; 
       texture-inside1.1-0 ; 
       texture-inside2.1-0 ; 
       texture-intakes1.2-0 ; 
       texture-intakes2.1-0 ; 
       texture-outside.1-0 ; 
       texture-outside1.1-0 ; 
       texture-rear_sides1.2-0 ; 
       texture-rear_sides2.1-0 ; 
       texture-rear_top1.2-0 ; 
       texture-rear_top2.1-0 ; 
       texture-tur1.1-0 ; 
       texture-upper_cockpit1.2-0 ; 
       texture-upper_cockpit2.1-0 ; 
       texture-wing_bottom1.1-0 ; 
       texture-wing_bottom2.1-0 ; 
       texture-wing_top1.1-0 ; 
       texture-wing_top2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       3 0 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 23 300 ; 
       0 20 300 ; 
       0 18 300 ; 
       0 0 300 ; 
       0 10 300 ; 
       0 14 300 ; 
       0 2 300 ; 
       1 6 300 ; 
       1 28 300 ; 
       1 26 300 ; 
       3 8 300 ; 
       3 17 300 ; 
       3 13 300 ; 
       2 7 300 ; 
       2 16 300 ; 
       2 12 300 ; 
       4 5 300 ; 
       4 27 300 ; 
       4 25 300 ; 
       5 22 300 ; 
       6 9 300 ; 
       6 24 300 ; 
       6 21 300 ; 
       6 19 300 ; 
       6 1 300 ; 
       6 11 300 ; 
       6 15 300 ; 
       6 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       22 16 401 ; 
       23 17 401 ; 
       20 14 401 ; 
       18 12 401 ; 
       0 0 401 ; 
       10 4 401 ; 
       14 8 401 ; 
       27 21 401 ; 
       25 19 401 ; 
       28 22 401 ; 
       26 20 401 ; 
       16 10 401 ; 
       12 6 401 ; 
       17 11 401 ; 
       13 7 401 ; 
       2 2 401 ; 
       24 18 401 ; 
       21 15 401 ; 
       19 13 401 ; 
       1 1 401 ; 
       11 5 401 ; 
       15 9 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 63.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       16 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 65 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 67.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 70 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
