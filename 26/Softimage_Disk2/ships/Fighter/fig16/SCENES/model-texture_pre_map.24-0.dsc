SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1701d_low-cam_int1.16-0 ROOT ; 
       1701d_low-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       texture_pre_map-light1.9-0 ROOT ; 
       texture_pre_map-light4.9-0 ROOT ; 
       texture_pre_map-light7.9-0 ROOT ; 
       texture_pre_map-light8.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       texture_pre_map-default1.10-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       texture_pre_map-body1.21-0 ROOT ; 
       texture_pre_map-l_stablat.1-0 ; 
       texture_pre_map-l_wing.1-0 ; 
       texture_pre_map-r-stablat.1-0 ; 
       texture_pre_map-r-wing.1-0 ; 
       texture_pre_map-tur_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/guide ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/light ; 
       D:/Pete_Data/Softimage/ships/Fighter/fig16/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-texture_pre_map.24-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       texture_pre_map-guide.3-0 ; 
       texture_pre_map-lights1.1-0 ; 
       texture_pre_map-rendermap1.18-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       0 0 401 ; 
       0 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.03510642 0.2263446 1.013678 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 0 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
