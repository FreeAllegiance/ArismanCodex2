SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig17-cam_int1.11-0 ROOT ; 
       fig17-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CLUSTERS NBELEM 4     
       fig17-cube1g1.11-0 ; 
       main4-cube1_17g1.11-0 ; 
       main4-cube1_6g1.11-0 ; 
       main4-cube20g1.11-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       edit_nulls-mat70.1-0 ; 
       fig17-mat1.1-0 ; 
       fig17-mat15.1-0 ; 
       fig17-mat2.1-0 ; 
       fig17-mat29.1-0 ; 
       fig17-mat3.1-0 ; 
       fig17-mat6.1-0 ; 
       fig17-mat71.1-0 ; 
       fig17-mat74.1-0 ; 
       fig17-mat75.1-0 ; 
       fig17-mat77.1-0 ; 
       fig17-mat78.1-0 ; 
       fig17-mat8.1-0 ; 
       fig17-mat80.1-0 ; 
       fig17-mat81.1-0 ; 
       fig17-mat82.1-0 ; 
       fig17-mat83.1-0 ; 
       fig17-mat84.1-0 ; 
       fig17-mat85.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       fig17-bwepemt.1-0 ; 
       fig17-canopy.3-0 ; 
       fig17-cockpt.1-0 ; 
       fig17-cube1.1-0 ; 
       fig17-cube1_17.1-0 ; 
       fig17-cube1_6.1-0 ; 
       fig17-cube10.1-0 ; 
       fig17-cube20.1-0 ; 
       fig17-cube21.1-0 ; 
       fig17-cyl13.1-0 ; 
       fig17-cyl14.1-0 ; 
       fig17-fuslg.1-0 ROOT ; 
       fig17-fwepemt.1-0 ; 
       fig17-lsmoke.1-0 ; 
       fig17-lthrust.1-0 ; 
       fig17-missemt.1-0 ; 
       fig17-rsmoke.1-0 ; 
       fig17-rthrust.1-0 ; 
       fig17-sphere10.1-0 ; 
       fig17-SS01.1-0 ; 
       fig17-SS02.1-0 ; 
       fig17-SS03.1-0 ; 
       fig17-SS04.1-0 ; 
       fig17-SS05.1-0 ; 
       fig17-SS06.1-0 ; 
       fig17-trail.1-0 ; 
       fig17-wing1.1-0 ; 
       fig17-wing4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig17/PICTURES/fig17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       add_wings-fig17.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       fig17-t2d1.4-0 ; 
       fig17-t2d14.4-0 ; 
       fig17-t2d2.4-0 ; 
       fig17-t2d22.4-0 ; 
       fig17-t2d23.4-0 ; 
       fig17-t2d47.4-0 ; 
       fig17-t2d48.4-0 ; 
       fig17-t2d49.4-0 ; 
       fig17-t2d5.4-0 ; 
       fig17-t2d50.4-0 ; 
       fig17-t2d51.2-0 ; 
       fig17-t2d52.2-0 ; 
       fig17-tex.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 3 110 ; 
       7 11 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       27 11 110 ; 
       26 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER CLUSTERS 
       3 0 700 ; 
       4 1 700 ; 
       5 2 700 ; 
       7 3 700 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       3 3 300 ; 
       4 16 300 ; 
       5 5 300 ; 
       6 12 300 ; 
       7 14 300 ; 
       8 15 300 ; 
       9 2 300 ; 
       10 8 300 ; 
       11 1 300 ; 
       18 6 300 ; 
       19 0 300 ; 
       20 7 300 ; 
       21 10 300 ; 
       22 9 300 ; 
       23 13 300 ; 
       24 11 300 ; 
       27 18 300 ; 
       26 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       3 0 400 ; 
       4 9 400 ; 
       5 2 400 ; 
       6 1 400 ; 
       7 6 400 ; 
       8 7 400 ; 
       9 4 400 ; 
       10 5 400 ; 
       11 12 400 ; 
       18 8 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       17 10 401 ; 
       18 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 15 -2 0 MPRFLG 0 ; 
       11 SCHEM 35 0 0 SRT 1 1 1 0 0 0 0 -0.1685402 0.4407974 MPRFLG 0 ; 
       12 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -2 0 MPRFLG 0 ; 
       19 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 30 -2 0 MPRFLG 0 ; 
       26 SCHEM 27.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 69 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 69 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
