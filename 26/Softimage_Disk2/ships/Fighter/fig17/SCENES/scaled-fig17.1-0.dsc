SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fig17-cam_int1.3-0 ROOT ; 
       fig17-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CLUSTERS NBELEM 4     
       fig17-cube1g1.3-0 ; 
       main4-cube1_17g1.3-0 ; 
       main4-cube1_6g1.3-0 ; 
       main4-cube20g1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 101     
       edit_nulls-mat70.1-0 ; 
       fig17-default1.1-0 ; 
       fig17-default1_1.1-0 ; 
       fig17-default1_1_1.1-0 ; 
       fig17-default1_3.1-0 ; 
       fig17-default10.1-0 ; 
       fig17-default10_1.1-0 ; 
       fig17-default10_1_1.1-0 ; 
       fig17-default10_3.1-0 ; 
       fig17-default11.1-0 ; 
       fig17-default11_1.1-0 ; 
       fig17-default11_1_1.1-0 ; 
       fig17-default11_3.1-0 ; 
       fig17-default12.1-0 ; 
       fig17-default12_1.1-0 ; 
       fig17-default12_1_1.1-0 ; 
       fig17-default12_3.1-0 ; 
       fig17-default13.1-0 ; 
       fig17-default13_1.1-0 ; 
       fig17-default13_1_1.1-0 ; 
       fig17-default13_3.1-0 ; 
       fig17-default14.1-0 ; 
       fig17-default14_1.1-0 ; 
       fig17-default14_1_1.1-0 ; 
       fig17-default14_3.1-0 ; 
       fig17-default15.1-0 ; 
       fig17-default15_1.1-0 ; 
       fig17-default15_1_1.1-0 ; 
       fig17-default15_3.1-0 ; 
       fig17-default16.1-0 ; 
       fig17-default16_1.1-0 ; 
       fig17-default16_1_1.1-0 ; 
       fig17-default16_3.1-0 ; 
       fig17-default17.1-0 ; 
       fig17-default17_1.1-0 ; 
       fig17-default17_1_1.1-0 ; 
       fig17-default17_3.1-0 ; 
       fig17-default18.1-0 ; 
       fig17-default18_1.1-0 ; 
       fig17-default18_1_1.1-0 ; 
       fig17-default18_3.1-0 ; 
       fig17-default19.1-0 ; 
       fig17-default19_1.1-0 ; 
       fig17-default19_1_1.1-0 ; 
       fig17-default19_3.1-0 ; 
       fig17-default2.1-0 ; 
       fig17-default2_1.1-0 ; 
       fig17-default2_1_1.1-0 ; 
       fig17-default2_3.1-0 ; 
       fig17-default20.1-0 ; 
       fig17-default20_1.1-0 ; 
       fig17-default20_1_1.1-0 ; 
       fig17-default20_3.1-0 ; 
       fig17-default21.1-0 ; 
       fig17-default21_1.1-0 ; 
       fig17-default21_1_1.1-0 ; 
       fig17-default21_3.1-0 ; 
       fig17-default3.1-0 ; 
       fig17-default3_1.1-0 ; 
       fig17-default3_1_1.1-0 ; 
       fig17-default3_3.1-0 ; 
       fig17-default4.1-0 ; 
       fig17-default4_1.1-0 ; 
       fig17-default4_1_1.1-0 ; 
       fig17-default4_3.1-0 ; 
       fig17-default5.1-0 ; 
       fig17-default5_1.1-0 ; 
       fig17-default5_1_1.1-0 ; 
       fig17-default5_3.1-0 ; 
       fig17-default6.1-0 ; 
       fig17-default6_1.1-0 ; 
       fig17-default6_1_1.1-0 ; 
       fig17-default6_3.1-0 ; 
       fig17-default7.1-0 ; 
       fig17-default7_1.1-0 ; 
       fig17-default7_1_1.1-0 ; 
       fig17-default7_3.1-0 ; 
       fig17-default8.1-0 ; 
       fig17-default8_1.1-0 ; 
       fig17-default8_1_1.1-0 ; 
       fig17-default8_3.1-0 ; 
       fig17-default9.1-0 ; 
       fig17-default9_1.1-0 ; 
       fig17-default9_1_1.1-0 ; 
       fig17-default9_3.1-0 ; 
       fig17-mat1.1-0 ; 
       fig17-mat15.1-0 ; 
       fig17-mat2.1-0 ; 
       fig17-mat29.1-0 ; 
       fig17-mat3.1-0 ; 
       fig17-mat6.1-0 ; 
       fig17-mat71.1-0 ; 
       fig17-mat74.1-0 ; 
       fig17-mat75.1-0 ; 
       fig17-mat77.1-0 ; 
       fig17-mat78.1-0 ; 
       fig17-mat8.1-0 ; 
       fig17-mat80.1-0 ; 
       fig17-mat81.1-0 ; 
       fig17-mat82.1-0 ; 
       fig17-mat83.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       main4-bwepemt.1-0 ; 
       main4-canopy.3-0 ; 
       main4-cockpt.1-0 ; 
       main4-cube1.1-0 ; 
       main4-cube1_17.1-0 ; 
       main4-cube1_6.1-0 ; 
       main4-cube10.1-0 ; 
       main4-cube20.1-0 ; 
       main4-cube21.1-0 ; 
       main4-cyl13.1-0 ; 
       main4-cyl14.1-0 ; 
       main4-fuslg.3-0 ROOT ; 
       main4-fwepemt.1-0 ; 
       main4-lsmoke.1-0 ; 
       main4-lthrust.1-0 ; 
       main4-missemt.1-0 ; 
       main4-rsmoke.1-0 ; 
       main4-rthrust.1-0 ; 
       main4-sphere10.1-0 ; 
       main4-SS01.1-0 ; 
       main4-SS02.1-0 ; 
       main4-SS03.1-0 ; 
       main4-SS04.1-0 ; 
       main4-SS05.1-0 ; 
       main4-SS06.1-0 ; 
       main4-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig17/PICTURES/fig17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       scaled-fig17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       fig17-t2d1.2-0 ; 
       fig17-t2d14.2-0 ; 
       fig17-t2d2.2-0 ; 
       fig17-t2d22.2-0 ; 
       fig17-t2d23.2-0 ; 
       fig17-t2d47.2-0 ; 
       fig17-t2d48.2-0 ; 
       fig17-t2d49.2-0 ; 
       fig17-t2d5.2-0 ; 
       fig17-t2d50.2-0 ; 
       fig17-tex.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 3 110 ; 
       7 11 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER CLUSTERS 
       3 0 700 ; 
       4 1 700 ; 
       5 2 700 ; 
       7 3 700 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 88 300 ; 
       3 87 300 ; 
       4 100 300 ; 
       5 89 300 ; 
       6 96 300 ; 
       7 98 300 ; 
       8 99 300 ; 
       9 86 300 ; 
       10 92 300 ; 
       11 85 300 ; 
       18 90 300 ; 
       19 0 300 ; 
       20 91 300 ; 
       21 94 300 ; 
       22 93 300 ; 
       23 97 300 ; 
       24 95 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       3 0 400 ; 
       4 9 400 ; 
       5 2 400 ; 
       6 1 400 ; 
       7 6 400 ; 
       8 7 400 ; 
       9 4 400 ; 
       10 5 400 ; 
       11 10 400 ; 
       18 8 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 33.69549 4.915833 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.32886 13.56472 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 29.02953 16.23403 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 43.78005 8.43354 0 USR MPRFLG 0 ; 
       4 SCHEM 19.74123 9.306456 0 USR MPRFLG 0 ; 
       5 SCHEM 43.76201 10.14393 0 USR MPRFLG 0 ; 
       6 SCHEM 43.93953 6.433541 0 USR MPRFLG 0 ; 
       7 SCHEM 20.35865 7.769012 0 USR MPRFLG 0 ; 
       8 SCHEM 20.51807 5.769012 0 USR MPRFLG 0 ; 
       9 SCHEM 27.60465 5.501436 0 USR MPRFLG 0 ; 
       10 SCHEM 29.9805 5.382464 0 USR MPRFLG 0 ; 
       11 SCHEM 28.5729 13.99374 0 USR WIRECOL 4 7 SRT 1 1 1 -3.834921e-007 1.82744e-016 -9.509074e-016 0 -0.1685402 0.4407974 MPRFLG 0 ; 
       12 SCHEM 33.58346 5.954167 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24.38608 17.59685 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20.53828 13.91263 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 28.97084 17.28288 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24.51636 18.37369 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20.67636 13.02481 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24.89886 5.01275 0 USR MPRFLG 0 ; 
       19 SCHEM 28.91445 19.95259 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 28.8899 19.25145 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 31.36215 20.02948 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 31.28586 19.21852 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 33.82228 20.03329 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 33.8246 19.14402 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 28.98198 18.20871 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 0 0 MPRFLG 0 ; 
       3 SCHEM 56.5 0 0 MPRFLG 0 ; 
       4 SCHEM 56.5 0 0 MPRFLG 0 ; 
       5 SCHEM 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 0 0 MPRFLG 0 ; 
       7 SCHEM 56.5 0 0 MPRFLG 0 ; 
       8 SCHEM 56.5 0 0 MPRFLG 0 ; 
       9 SCHEM 0 0 0 MPRFLG 0 ; 
       10 SCHEM 0 0 0 MPRFLG 0 ; 
       11 SCHEM 56.5 0 0 MPRFLG 0 ; 
       12 SCHEM 56.5 0 0 MPRFLG 0 ; 
       13 SCHEM 0 0 0 MPRFLG 0 ; 
       14 SCHEM 0 0 0 MPRFLG 0 ; 
       15 SCHEM 56.5 0 0 MPRFLG 0 ; 
       16 SCHEM 56.5 0 0 MPRFLG 0 ; 
       17 SCHEM 0 0 0 MPRFLG 0 ; 
       18 SCHEM 0 0 0 MPRFLG 0 ; 
       19 SCHEM 56.5 0 0 MPRFLG 0 ; 
       20 SCHEM 56.5 0 0 MPRFLG 0 ; 
       21 SCHEM 0 0 0 MPRFLG 0 ; 
       22 SCHEM 0 0 0 MPRFLG 0 ; 
       23 SCHEM 56.5 0 0 MPRFLG 0 ; 
       24 SCHEM 56.5 0 0 MPRFLG 0 ; 
       25 SCHEM 0 0 0 MPRFLG 0 ; 
       26 SCHEM 0 0 0 MPRFLG 0 ; 
       27 SCHEM 56.5 0 0 MPRFLG 0 ; 
       28 SCHEM 56.5 0 0 MPRFLG 0 ; 
       29 SCHEM 0 0 0 MPRFLG 0 ; 
       30 SCHEM 0 0 0 MPRFLG 0 ; 
       31 SCHEM 56.5 0 0 MPRFLG 0 ; 
       32 SCHEM 56.5 0 0 MPRFLG 0 ; 
       33 SCHEM 0 0 0 MPRFLG 0 ; 
       34 SCHEM 0 0 0 MPRFLG 0 ; 
       35 SCHEM 56.5 0 0 MPRFLG 0 ; 
       36 SCHEM 56.5 0 0 MPRFLG 0 ; 
       37 SCHEM 0 0 0 MPRFLG 0 ; 
       38 SCHEM 0 0 0 MPRFLG 0 ; 
       39 SCHEM 56.5 0 0 MPRFLG 0 ; 
       40 SCHEM 56.5 0 0 MPRFLG 0 ; 
       41 SCHEM 0 0 0 MPRFLG 0 ; 
       42 SCHEM 0 0 0 MPRFLG 0 ; 
       43 SCHEM 56.5 0 0 MPRFLG 0 ; 
       44 SCHEM 56.5 0 0 MPRFLG 0 ; 
       45 SCHEM 0 0 0 MPRFLG 0 ; 
       46 SCHEM 0 0 0 MPRFLG 0 ; 
       47 SCHEM 56.5 0 0 MPRFLG 0 ; 
       48 SCHEM 56.5 0 0 MPRFLG 0 ; 
       49 SCHEM 0 0 0 MPRFLG 0 ; 
       50 SCHEM 0 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 0 0 MPRFLG 0 ; 
       52 SCHEM 56.5 0 0 MPRFLG 0 ; 
       53 SCHEM 0 0 0 MPRFLG 0 ; 
       54 SCHEM 0 0 0 MPRFLG 0 ; 
       55 SCHEM 56.5 0 0 MPRFLG 0 ; 
       56 SCHEM 56.5 0 0 MPRFLG 0 ; 
       57 SCHEM 0 0 0 MPRFLG 0 ; 
       58 SCHEM 0 0 0 MPRFLG 0 ; 
       59 SCHEM 56.5 0 0 MPRFLG 0 ; 
       60 SCHEM 56.5 0 0 MPRFLG 0 ; 
       61 SCHEM 0 0 0 MPRFLG 0 ; 
       62 SCHEM 0 0 0 MPRFLG 0 ; 
       63 SCHEM 56.5 0 0 MPRFLG 0 ; 
       64 SCHEM 56.5 0 0 MPRFLG 0 ; 
       65 SCHEM 0 0 0 MPRFLG 0 ; 
       66 SCHEM 0 0 0 MPRFLG 0 ; 
       67 SCHEM 56.5 0 0 MPRFLG 0 ; 
       68 SCHEM 56.5 0 0 MPRFLG 0 ; 
       69 SCHEM 0 0 0 MPRFLG 0 ; 
       70 SCHEM 0 0 0 MPRFLG 0 ; 
       71 SCHEM 56.5 0 0 MPRFLG 0 ; 
       72 SCHEM 56.5 0 0 MPRFLG 0 ; 
       73 SCHEM 0 0 0 MPRFLG 0 ; 
       74 SCHEM 0 0 0 MPRFLG 0 ; 
       75 SCHEM 56.5 0 0 MPRFLG 0 ; 
       76 SCHEM 56.5 0 0 MPRFLG 0 ; 
       77 SCHEM 0 0 0 MPRFLG 0 ; 
       78 SCHEM 0 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 0 0 MPRFLG 0 ; 
       80 SCHEM 56.5 0 0 MPRFLG 0 ; 
       81 SCHEM 0 0 0 MPRFLG 0 ; 
       82 SCHEM 0 0 0 MPRFLG 0 ; 
       83 SCHEM 56.5 0 0 MPRFLG 0 ; 
       84 SCHEM 56.5 0 0 MPRFLG 0 ; 
       85 SCHEM 139 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 156.7651 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 404.1303 8.47596 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 537.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 542.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 1050.225 -2.4879 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 2069.126 -1.602436 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 2124.346 7.599751 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 2116.846 5.599751 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 2189.584 5.88936 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 99 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 91.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 106.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 111.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 71.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 406.6303 8.47596 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2126.846 7.599751 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2119.346 5.599751 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2192.084 5.88936 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 141.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
