SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fig17-cam_int1.6-0 ROOT ; 
       fig17-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CLUSTERS NBELEM 4     
       fig17-cube1g1.6-0 ; 
       main4-cube1_17g1.6-0 ; 
       main4-cube1_6g1.6-0 ; 
       main4-cube20g1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 95     
       fig17-default1.1-0 ; 
       fig17-default1_1.1-0 ; 
       fig17-default1_1_1.1-0 ; 
       fig17-default1_3.1-0 ; 
       fig17-default10.1-0 ; 
       fig17-default10_1.1-0 ; 
       fig17-default10_1_1.1-0 ; 
       fig17-default10_3.1-0 ; 
       fig17-default11.1-0 ; 
       fig17-default11_1.1-0 ; 
       fig17-default11_1_1.1-0 ; 
       fig17-default11_3.1-0 ; 
       fig17-default12.1-0 ; 
       fig17-default12_1.1-0 ; 
       fig17-default12_1_1.1-0 ; 
       fig17-default12_3.1-0 ; 
       fig17-default13.1-0 ; 
       fig17-default13_1.1-0 ; 
       fig17-default13_1_1.1-0 ; 
       fig17-default13_3.1-0 ; 
       fig17-default14.1-0 ; 
       fig17-default14_1.1-0 ; 
       fig17-default14_1_1.1-0 ; 
       fig17-default14_3.1-0 ; 
       fig17-default15.1-0 ; 
       fig17-default15_1.1-0 ; 
       fig17-default15_1_1.1-0 ; 
       fig17-default15_3.1-0 ; 
       fig17-default16.1-0 ; 
       fig17-default16_1.1-0 ; 
       fig17-default16_1_1.1-0 ; 
       fig17-default16_3.1-0 ; 
       fig17-default17.1-0 ; 
       fig17-default17_1.1-0 ; 
       fig17-default17_1_1.1-0 ; 
       fig17-default17_3.1-0 ; 
       fig17-default18.1-0 ; 
       fig17-default18_1.1-0 ; 
       fig17-default18_1_1.1-0 ; 
       fig17-default18_3.1-0 ; 
       fig17-default19.1-0 ; 
       fig17-default19_1.1-0 ; 
       fig17-default19_1_1.1-0 ; 
       fig17-default19_3.1-0 ; 
       fig17-default2.1-0 ; 
       fig17-default2_1.1-0 ; 
       fig17-default2_1_1.1-0 ; 
       fig17-default2_3.1-0 ; 
       fig17-default20.1-0 ; 
       fig17-default20_1.1-0 ; 
       fig17-default20_1_1.1-0 ; 
       fig17-default20_3.1-0 ; 
       fig17-default21.1-0 ; 
       fig17-default21_1.1-0 ; 
       fig17-default21_1_1.1-0 ; 
       fig17-default21_3.1-0 ; 
       fig17-default3.1-0 ; 
       fig17-default3_1.1-0 ; 
       fig17-default3_1_1.1-0 ; 
       fig17-default3_3.1-0 ; 
       fig17-default4.1-0 ; 
       fig17-default4_1.1-0 ; 
       fig17-default4_1_1.1-0 ; 
       fig17-default4_3.1-0 ; 
       fig17-default5.1-0 ; 
       fig17-default5_1.1-0 ; 
       fig17-default5_1_1.1-0 ; 
       fig17-default5_3.1-0 ; 
       fig17-default6.1-0 ; 
       fig17-default6_1.1-0 ; 
       fig17-default6_1_1.1-0 ; 
       fig17-default6_3.1-0 ; 
       fig17-default7.1-0 ; 
       fig17-default7_1.1-0 ; 
       fig17-default7_1_1.1-0 ; 
       fig17-default7_3.1-0 ; 
       fig17-default8.1-0 ; 
       fig17-default8_1.1-0 ; 
       fig17-default8_1_1.1-0 ; 
       fig17-default8_3.1-0 ; 
       fig17-default9.1-0 ; 
       fig17-default9_1.1-0 ; 
       fig17-default9_1_1.1-0 ; 
       fig17-default9_3.1-0 ; 
       fig17-mat1.1-0 ; 
       fig17-mat15.1-0 ; 
       fig17-mat2.1-0 ; 
       fig17-mat29.1-0 ; 
       fig17-mat3.1-0 ; 
       fig17-mat6.1-0 ; 
       fig17-mat74.1-0 ; 
       fig17-mat8.1-0 ; 
       fig17-mat81.1-0 ; 
       fig17-mat82.1-0 ; 
       fig17-mat83.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       main4-canopy.3-0 ; 
       main4-cube1.1-0 ; 
       main4-cube1_17.1-0 ; 
       main4-cube1_6.1-0 ; 
       main4-cube10.1-0 ; 
       main4-cube20.1-0 ; 
       main4-cube21.1-0 ; 
       main4-cyl13.1-0 ; 
       main4-cyl14.1-0 ; 
       main4-fuslg.6-0 ROOT ; 
       main4-sphere10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig17/PICTURES/fig17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       fig17-t2d1.3-0 ; 
       fig17-t2d14.3-0 ; 
       fig17-t2d2.3-0 ; 
       fig17-t2d22.3-0 ; 
       fig17-t2d23.3-0 ; 
       fig17-t2d47.3-0 ; 
       fig17-t2d48.3-0 ; 
       fig17-t2d49.3-0 ; 
       fig17-t2d5.3-0 ; 
       fig17-t2d50.3-0 ; 
       fig17-tex.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 1 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       10 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER CLUSTERS 
       1 0 700 ; 
       2 1 700 ; 
       3 2 700 ; 
       5 3 700 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 87 300 ; 
       1 86 300 ; 
       2 94 300 ; 
       3 88 300 ; 
       4 91 300 ; 
       5 92 300 ; 
       6 93 300 ; 
       7 85 300 ; 
       8 90 300 ; 
       9 84 300 ; 
       10 89 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       1 0 400 ; 
       2 9 400 ; 
       3 2 400 ; 
       4 1 400 ; 
       5 6 400 ; 
       6 7 400 ; 
       7 4 400 ; 
       8 5 400 ; 
       9 10 400 ; 
       10 8 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 MPRFLG 0 ; 
       9 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 -0.1685402 0.4407974 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
       2 SCHEM 56.5 0 0 MPRFLG 0 ; 
       3 SCHEM 56.5 0 0 MPRFLG 0 ; 
       4 SCHEM 0 0 0 MPRFLG 0 ; 
       5 SCHEM 0 0 0 MPRFLG 0 ; 
       6 SCHEM 56.5 0 0 MPRFLG 0 ; 
       7 SCHEM 56.5 0 0 MPRFLG 0 ; 
       8 SCHEM 0 0 0 MPRFLG 0 ; 
       9 SCHEM 0 0 0 MPRFLG 0 ; 
       10 SCHEM 56.5 0 0 MPRFLG 0 ; 
       11 SCHEM 56.5 0 0 MPRFLG 0 ; 
       12 SCHEM 0 0 0 MPRFLG 0 ; 
       13 SCHEM 0 0 0 MPRFLG 0 ; 
       14 SCHEM 56.5 0 0 MPRFLG 0 ; 
       15 SCHEM 56.5 0 0 MPRFLG 0 ; 
       16 SCHEM 0 0 0 MPRFLG 0 ; 
       17 SCHEM 0 0 0 MPRFLG 0 ; 
       18 SCHEM 56.5 0 0 MPRFLG 0 ; 
       19 SCHEM 56.5 0 0 MPRFLG 0 ; 
       20 SCHEM 0 0 0 MPRFLG 0 ; 
       21 SCHEM 0 0 0 MPRFLG 0 ; 
       22 SCHEM 56.5 0 0 MPRFLG 0 ; 
       23 SCHEM 56.5 0 0 MPRFLG 0 ; 
       24 SCHEM 0 0 0 MPRFLG 0 ; 
       25 SCHEM 0 0 0 MPRFLG 0 ; 
       26 SCHEM 56.5 0 0 MPRFLG 0 ; 
       27 SCHEM 56.5 0 0 MPRFLG 0 ; 
       28 SCHEM 0 0 0 MPRFLG 0 ; 
       29 SCHEM 0 0 0 MPRFLG 0 ; 
       30 SCHEM 56.5 0 0 MPRFLG 0 ; 
       31 SCHEM 56.5 0 0 MPRFLG 0 ; 
       32 SCHEM 0 0 0 MPRFLG 0 ; 
       33 SCHEM 0 0 0 MPRFLG 0 ; 
       34 SCHEM 56.5 0 0 MPRFLG 0 ; 
       35 SCHEM 56.5 0 0 MPRFLG 0 ; 
       36 SCHEM 0 0 0 MPRFLG 0 ; 
       37 SCHEM 0 0 0 MPRFLG 0 ; 
       38 SCHEM 56.5 0 0 MPRFLG 0 ; 
       39 SCHEM 56.5 0 0 MPRFLG 0 ; 
       40 SCHEM 0 0 0 MPRFLG 0 ; 
       41 SCHEM 0 0 0 MPRFLG 0 ; 
       42 SCHEM 56.5 0 0 MPRFLG 0 ; 
       43 SCHEM 56.5 0 0 MPRFLG 0 ; 
       44 SCHEM 0 0 0 MPRFLG 0 ; 
       45 SCHEM 0 0 0 MPRFLG 0 ; 
       46 SCHEM 56.5 0 0 MPRFLG 0 ; 
       47 SCHEM 56.5 0 0 MPRFLG 0 ; 
       48 SCHEM 0 0 0 MPRFLG 0 ; 
       49 SCHEM 0 0 0 MPRFLG 0 ; 
       50 SCHEM 56.5 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 0 0 MPRFLG 0 ; 
       52 SCHEM 0 0 0 MPRFLG 0 ; 
       53 SCHEM 0 0 0 MPRFLG 0 ; 
       54 SCHEM 56.5 0 0 MPRFLG 0 ; 
       55 SCHEM 56.5 0 0 MPRFLG 0 ; 
       56 SCHEM 0 0 0 MPRFLG 0 ; 
       57 SCHEM 0 0 0 MPRFLG 0 ; 
       58 SCHEM 56.5 0 0 MPRFLG 0 ; 
       59 SCHEM 56.5 0 0 MPRFLG 0 ; 
       60 SCHEM 0 0 0 MPRFLG 0 ; 
       61 SCHEM 0 0 0 MPRFLG 0 ; 
       62 SCHEM 56.5 0 0 MPRFLG 0 ; 
       63 SCHEM 56.5 0 0 MPRFLG 0 ; 
       64 SCHEM 0 0 0 MPRFLG 0 ; 
       65 SCHEM 0 0 0 MPRFLG 0 ; 
       66 SCHEM 56.5 0 0 MPRFLG 0 ; 
       67 SCHEM 56.5 0 0 MPRFLG 0 ; 
       68 SCHEM 0 0 0 MPRFLG 0 ; 
       69 SCHEM 0 0 0 MPRFLG 0 ; 
       70 SCHEM 56.5 0 0 MPRFLG 0 ; 
       71 SCHEM 56.5 0 0 MPRFLG 0 ; 
       72 SCHEM 0 0 0 MPRFLG 0 ; 
       73 SCHEM 0 0 0 MPRFLG 0 ; 
       74 SCHEM 56.5 0 0 MPRFLG 0 ; 
       75 SCHEM 56.5 0 0 MPRFLG 0 ; 
       76 SCHEM 0 0 0 MPRFLG 0 ; 
       77 SCHEM 0 0 0 MPRFLG 0 ; 
       78 SCHEM 56.5 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 0 0 MPRFLG 0 ; 
       80 SCHEM 0 0 0 MPRFLG 0 ; 
       81 SCHEM 0 0 0 MPRFLG 0 ; 
       82 SCHEM 56.5 0 0 MPRFLG 0 ; 
       83 SCHEM 56.5 0 0 MPRFLG 0 ; 
       84 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
