SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig17-cam_int1.14-0 ROOT ; 
       fig17-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CLUSTERS NBELEM 4     
       fig17-cube1g1.14-0 ; 
       main4-cube1_17g1.14-0 ; 
       main4-cube1_6g1.14-0 ; 
       main4-cube20g1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       fig17-mat1.2-0 ; 
       fig17-mat2.2-0 ; 
       fig17-mat29.2-0 ; 
       fig17-mat3.2-0 ; 
       fig17-mat6.2-0 ; 
       fig17-mat8.2-0 ; 
       fig17-mat81.2-0 ; 
       fig17-mat82.2-0 ; 
       fig17-mat83.2-0 ; 
       fig17-mat84.2-0 ; 
       fig17-mat85.4-0 ; 
       fig17-mat86.1-0 ; 
       fig17-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       fig17-canopy.3-0 ; 
       fig17-cube1.1-0 ; 
       fig17-cube1_17.1-0 ; 
       fig17-cube1_6.1-0 ; 
       fig17-cube10.1-0 ; 
       fig17-cube20.1-0 ; 
       fig17-cube21.1-0 ; 
       fig17-cyl13.1-0 ; 
       fig17-cyl14.1-0 ; 
       fig17-fuslg.3-0 ROOT ; 
       fig17-sphere10.1-0 ; 
       fig17-wing1.1-0 ; 
       fig17-wing4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig17/PICTURES/fig17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-fig17.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       fig17-t2d1.5-0 ; 
       fig17-t2d14.5-0 ; 
       fig17-t2d2.5-0 ; 
       fig17-t2d22.5-0 ; 
       fig17-t2d48.5-0 ; 
       fig17-t2d49.5-0 ; 
       fig17-t2d5.5-0 ; 
       fig17-t2d50.5-0 ; 
       fig17-t2d51.3-0 ; 
       fig17-t2d52.3-0 ; 
       fig17-t2d53.1-0 ; 
       fig17-t2d54.1-0 ; 
       fig17-tex.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 1 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       10 9 110 ; 
       12 9 110 ; 
       11 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER CLUSTERS 
       1 0 700 ; 
       2 1 700 ; 
       3 2 700 ; 
       5 3 700 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 1 300 ; 
       2 8 300 ; 
       3 3 300 ; 
       4 5 300 ; 
       5 6 300 ; 
       6 7 300 ; 
       7 11 300 ; 
       8 12 300 ; 
       9 0 300 ; 
       10 4 300 ; 
       12 10 300 ; 
       11 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       1 0 400 ; 
       2 7 400 ; 
       3 2 400 ; 
       4 1 400 ; 
       5 4 400 ; 
       6 5 400 ; 
       7 10 400 ; 
       8 11 400 ; 
       9 12 400 ; 
       10 6 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 8 401 ; 
       10 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 MPRFLG 0 ; 
       9 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 -0.1685402 0.4407974 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       11 SCHEM 26.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 MPRFLG 0 ; 
       0 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 29 -4 0 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
