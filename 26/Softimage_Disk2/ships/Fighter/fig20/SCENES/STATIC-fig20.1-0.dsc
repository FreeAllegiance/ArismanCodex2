SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.68-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       edit_nulls-mat70.1-0 ; 
       fig20-mat100.1-0 ; 
       fig20-mat71.1-0 ; 
       fig20-mat75.1-0 ; 
       fig20-mat77.1-0 ; 
       fig20-mat78.1-0 ; 
       fig20-mat80.1-0 ; 
       fig20-mat81.8-0 ; 
       fig20-mat82.4-0 ; 
       fig20-mat83.2-0 ; 
       fig20-mat85.2-0 ; 
       fig20-mat88.6-0 ; 
       fig20-mat89.5-0 ; 
       fig20-mat91.3-0 ; 
       fig20-mat92.2-0 ; 
       fig20-mat93.1-0 ; 
       fig20-mat94.1-0 ; 
       fig20-mat95.1-0 ; 
       fig20-mat96.1-0 ; 
       fig20-mat97.1-0 ; 
       fig20-mat98.1-0 ; 
       fig20-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       fig20-blthrust.1-0 ; 
       fig20-bottom_gun.1-0 ; 
       fig20-brthrust.1-0 ; 
       fig20-canopy_1.18-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-cockpt.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-gun.1-0 ; 
       fig20-gun1.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine3.1-0 ; 
       fig20-l-t-engine4.1-0 ; 
       fig20-l-t-engine5.1-0 ; 
       fig20-lsmoke.1-0 ; 
       fig20-lwepemt.1-0 ; 
       fig20-missemt.1-0 ; 
       fig20-rsmoke.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing5.1-0 ; 
       fig20-rwepemt.1-0 ; 
       fig20-SS01.1-0 ; 
       fig20-SS02.1-0 ; 
       fig20-SS03.1-0 ; 
       fig20-SS04.1-0 ; 
       fig20-SS05.1-0 ; 
       fig20-SS06.1-0 ; 
       fig20-tlthrust.1-0 ; 
       fig20-trail.1-0 ; 
       fig20-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig20/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig20.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       fig20-back.7-0 ; 
       fig20-Front.7-0 ; 
       fig20-Rear_front.7-0 ; 
       fig20-Side.6-0 ; 
       fig20-t2d1.6-0 ; 
       fig20-t2d10.2-0 ; 
       fig20-t2d11.1-0 ; 
       fig20-t2d12.1-0 ; 
       fig20-t2d13.2-0 ; 
       fig20-t2d3.6-0 ; 
       fig20-t2d4.12-0 ; 
       fig20-t2d5.11-0 ; 
       fig20-t2d6.9-0 ; 
       fig20-t2d7.6-0 ; 
       fig20-t2d8.4-0 ; 
       fig20-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 4 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       7 17 110 ; 
       8 18 110 ; 
       6 3 110 ; 
       18 4 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 4 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 10 300 ; 
       3 7 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       4 8 300 ; 
       4 19 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       6 1 300 ; 
       18 18 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 17 300 ; 
       17 9 300 ; 
       20 0 300 ; 
       21 2 300 ; 
       22 4 300 ; 
       23 3 300 ; 
       24 6 300 ; 
       25 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 9 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       18 14 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       19 15 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       1 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       4 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 MPRFLG 0 ; 
       10 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 70 -2 0 MPRFLG 0 ; 
       12 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 73 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 74.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 76 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
