SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.69-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       fig20-mat100.1-0 ; 
       fig20-mat81.8-0 ; 
       fig20-mat82.4-0 ; 
       fig20-mat83.2-0 ; 
       fig20-mat85.2-0 ; 
       fig20-mat88.6-0 ; 
       fig20-mat89.5-0 ; 
       fig20-mat91.3-0 ; 
       fig20-mat92.2-0 ; 
       fig20-mat93.1-0 ; 
       fig20-mat94.1-0 ; 
       fig20-mat95.1-0 ; 
       fig20-mat96.1-0 ; 
       fig20-mat97.1-0 ; 
       fig20-mat98.1-0 ; 
       fig20-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fig20-bottom_gun.1-0 ; 
       fig20-canopy_1.19-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-gun.1-0 ; 
       fig20-gun1.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine3.1-0 ; 
       fig20-l-t-engine4.1-0 ; 
       fig20-l-t-engine5.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig20/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig20.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       fig20-back.7-0 ; 
       fig20-Front.7-0 ; 
       fig20-Rear_front.7-0 ; 
       fig20-Side.6-0 ; 
       fig20-t2d1.6-0 ; 
       fig20-t2d10.2-0 ; 
       fig20-t2d11.1-0 ; 
       fig20-t2d12.1-0 ; 
       fig20-t2d13.2-0 ; 
       fig20-t2d3.6-0 ; 
       fig20-t2d4.12-0 ; 
       fig20-t2d5.11-0 ; 
       fig20-t2d6.9-0 ; 
       fig20-t2d7.6-0 ; 
       fig20-t2d8.4-0 ; 
       fig20-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       4 10 110 ; 
       5 11 110 ; 
       3 1 110 ; 
       11 2 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 1 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 2 300 ; 
       2 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       3 0 300 ; 
       11 12 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       8 10 300 ; 
       9 11 300 ; 
       10 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 9 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       12 14 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 10 401 ; 
       9 11 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       13 15 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       0 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 52.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 53.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
