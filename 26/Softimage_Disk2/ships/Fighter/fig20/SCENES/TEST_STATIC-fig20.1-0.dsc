SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.25-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.25-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.35-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       fig20-mat81.7-0 ; 
       fig20-mat82.3-0 ; 
       fig20-mat83.2-0 ; 
       fig20-mat85.1-0 ; 
       fig20-mat86.1-0 ; 
       fig20-mat87.5-0 ; 
       fig20-mat88.5-0 ; 
       fig20-mat89.4-0 ; 
       fig20-mat90.3-0 ; 
       fig20-mat91.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fig20-bottom_gun.1-0 ; 
       fig20-canopy.29-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-cyl13.1-0 ; 
       fig20-cyl16.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-l-b-engine.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine1.1-0 ; 
       fig20-r-b-engine.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig20/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEST_STATIC-fig20.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       fig20-t2d1.2-0 ; 
       fig20-t2d2.2-0 ; 
       fig20-t2d3.2-0 ; 
       fig20-t2d4.8-0 ; 
       fig20-t2d5.8-0 ; 
       fig20-t2d6.6-0 ; 
       fig20-t2d7.3-0 ; 
       fig20-t2d8.2-0 ; 
       fig20-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       3 10 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 2 110 ; 
       11 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 0 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       2 1 300 ; 
       10 2 300 ; 
       11 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 2 401 ; 
       2 0 401 ; 
       4 1 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 8 401 ; 
       9 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       2 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 23.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 55.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 60.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 55.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 57.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 62.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
