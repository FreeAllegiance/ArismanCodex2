SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.53-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       fig20-mat81.8-0 ; 
       fig20-mat82.3-0 ; 
       fig20-mat83.2-0 ; 
       fig20-mat85.1-0 ; 
       fig20-mat86.1-0 ; 
       fig20-mat88.6-0 ; 
       fig20-mat89.5-0 ; 
       fig20-mat91.3-0 ; 
       fig20-mat92.2-0 ; 
       fig20-mat93.1-0 ; 
       fig20-mat94.1-0 ; 
       fig20-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fig20-bottom_gun.1-0 ; 
       fig20-canopy_1.6-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-cyl13.1-0 ; 
       fig20-cyl16.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine3.1-0 ; 
       fig20-l-t-engine4.1-0 ; 
       fig20-l-t-engine5.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig20/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEST_STATIC-fig20.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       fig20-back.6-0 ; 
       fig20-Front.6-0 ; 
       fig20-Rear_front.6-0 ; 
       fig20-t2d1.3-0 ; 
       fig20-t2d2.3-0 ; 
       fig20-t2d3.3-0 ; 
       fig20-t2d4.11-0 ; 
       fig20-t2d5.10-0 ; 
       fig20-t2d6.8-0 ; 
       fig20-t2d7.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       3 10 110 ; 
       4 11 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 0 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 1 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       8 10 300 ; 
       10 2 300 ; 
       11 4 300 ; 
       9 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 5 401 ; 
       2 3 401 ; 
       4 4 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       2 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.14191 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 54.64191 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.14191 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 59.64191 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 52.14191 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54.64191 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 57.14191 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
