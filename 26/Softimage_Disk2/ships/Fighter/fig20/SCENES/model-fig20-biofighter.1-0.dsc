SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20_biofighter-cam_int1.1-0 ROOT ; 
       fig20_biofighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat100.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig20_biofighter-mat81.1-0 ; 
       fig20_biofighter-mat82.1-0 ; 
       fig20_biofighter-mat83.1-0 ; 
       fig20_biofighter-mat85.1-0 ; 
       fig20_biofighter-mat88.1-0 ; 
       fig20_biofighter-mat89.1-0 ; 
       fig20_biofighter-mat91.1-0 ; 
       fig20_biofighter-mat92.1-0 ; 
       fig20_biofighter-mat93.1-0 ; 
       fig20_biofighter-mat94.1-0 ; 
       fig20_biofighter-mat95.1-0 ; 
       fig20_biofighter-mat96.1-0 ; 
       fig20_biofighter-mat97.1-0 ; 
       fig20_biofighter-mat98.1-0 ; 
       fig20_biofighter-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       fig20_biofighter-blthrust.1-0 ; 
       fig20_biofighter-bottom_gun.1-0 ; 
       fig20_biofighter-brthrust.1-0 ; 
       fig20_biofighter-canopy_1.1-0 ROOT ; 
       fig20_biofighter-center-wing.1-0 ; 
       fig20_biofighter-cockpt.1-0 ; 
       fig20_biofighter-engine-nib.1-0 ; 
       fig20_biofighter-gun.1-0 ; 
       fig20_biofighter-gun1.1-0 ; 
       fig20_biofighter-l-t-engine.1-0 ; 
       fig20_biofighter-l-t-engine3.1-0 ; 
       fig20_biofighter-l-t-engine4.1-0 ; 
       fig20_biofighter-l-t-engine5.1-0 ; 
       fig20_biofighter-lsmoke.1-0 ; 
       fig20_biofighter-lwepemt.1-0 ; 
       fig20_biofighter-missemt.1-0 ; 
       fig20_biofighter-mwepemt.1-0 ; 
       fig20_biofighter-rsmoke.1-0 ; 
       fig20_biofighter-rt-wing1.1-0 ; 
       fig20_biofighter-rt-wing5.1-0 ; 
       fig20_biofighter-rwepemt.1-0 ; 
       fig20_biofighter-SS01.1-0 ; 
       fig20_biofighter-SS02.1-0 ; 
       fig20_biofighter-SS03.1-0 ; 
       fig20_biofighter-SS04.1-0 ; 
       fig20_biofighter-SS05.1-0 ; 
       fig20_biofighter-SS06.1-0 ; 
       fig20_biofighter-tlthrust.1-0 ; 
       fig20_biofighter-trail.1-0 ; 
       fig20_biofighter-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig20/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig20-biofighter.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       fig20_biofighter-back.1-0 ; 
       fig20_biofighter-Front.1-0 ; 
       fig20_biofighter-Rear_front.1-0 ; 
       fig20_biofighter-Side.1-0 ; 
       fig20_biofighter-t2d1.1-0 ; 
       fig20_biofighter-t2d10.1-0 ; 
       fig20_biofighter-t2d11.1-0 ; 
       fig20_biofighter-t2d12.1-0 ; 
       fig20_biofighter-t2d13.1-0 ; 
       fig20_biofighter-t2d3.1-0 ; 
       fig20_biofighter-t2d4.1-0 ; 
       fig20_biofighter-t2d5.1-0 ; 
       fig20_biofighter-t2d6.1-0 ; 
       fig20_biofighter-t2d7.1-0 ; 
       fig20_biofighter-t2d8.1-0 ; 
       fig20_biofighter-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 4 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 18 110 ; 
       8 19 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 3 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 10 300 ; 
       3 7 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       4 8 300 ; 
       4 19 300 ; 
       6 1 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       12 17 300 ; 
       18 9 300 ; 
       19 18 300 ; 
       21 0 300 ; 
       22 2 300 ; 
       23 4 300 ; 
       24 3 300 ; 
       25 6 300 ; 
       26 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 8 401 ; 
       7 0 401 ; 
       8 9 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 1 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 6 401 ; 
       21 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 49.80492 0 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       4 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 67.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 70 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 72.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 26.55225 -3.450914 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 36.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 43.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 55 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 52.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 60 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 57.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 62.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 65 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 12.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 78.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 73 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 74.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 76 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
