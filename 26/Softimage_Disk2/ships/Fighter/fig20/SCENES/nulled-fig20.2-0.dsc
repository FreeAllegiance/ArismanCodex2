SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.12-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 90     
       edit_nulls-mat70.1-0 ; 
       fig20-default1.1-0 ; 
       fig20-default1_1.1-0 ; 
       fig20-default1_1_1.1-0 ; 
       fig20-default1_3.1-0 ; 
       fig20-default10.1-0 ; 
       fig20-default10_1.1-0 ; 
       fig20-default10_1_1.1-0 ; 
       fig20-default10_3.1-0 ; 
       fig20-default11.1-0 ; 
       fig20-default11_1.1-0 ; 
       fig20-default11_1_1.1-0 ; 
       fig20-default11_3.1-0 ; 
       fig20-default12.1-0 ; 
       fig20-default12_1.1-0 ; 
       fig20-default12_1_1.1-0 ; 
       fig20-default12_3.1-0 ; 
       fig20-default13.1-0 ; 
       fig20-default13_1.1-0 ; 
       fig20-default13_1_1.1-0 ; 
       fig20-default13_3.1-0 ; 
       fig20-default14.1-0 ; 
       fig20-default14_1.1-0 ; 
       fig20-default14_1_1.1-0 ; 
       fig20-default14_3.1-0 ; 
       fig20-default15.1-0 ; 
       fig20-default15_1.1-0 ; 
       fig20-default15_1_1.1-0 ; 
       fig20-default15_3.1-0 ; 
       fig20-default16.1-0 ; 
       fig20-default16_1.1-0 ; 
       fig20-default16_1_1.1-0 ; 
       fig20-default16_3.1-0 ; 
       fig20-default17.1-0 ; 
       fig20-default17_1.1-0 ; 
       fig20-default17_1_1.1-0 ; 
       fig20-default17_3.1-0 ; 
       fig20-default18.1-0 ; 
       fig20-default18_1.1-0 ; 
       fig20-default18_1_1.1-0 ; 
       fig20-default18_3.1-0 ; 
       fig20-default19.1-0 ; 
       fig20-default19_1.1-0 ; 
       fig20-default19_1_1.1-0 ; 
       fig20-default19_3.1-0 ; 
       fig20-default2.1-0 ; 
       fig20-default2_1.1-0 ; 
       fig20-default2_1_1.1-0 ; 
       fig20-default2_3.1-0 ; 
       fig20-default20.1-0 ; 
       fig20-default20_1.1-0 ; 
       fig20-default20_1_1.1-0 ; 
       fig20-default20_3.1-0 ; 
       fig20-default21.1-0 ; 
       fig20-default21_1.1-0 ; 
       fig20-default21_1_1.1-0 ; 
       fig20-default21_3.1-0 ; 
       fig20-default3.1-0 ; 
       fig20-default3_1.1-0 ; 
       fig20-default3_1_1.1-0 ; 
       fig20-default3_3.1-0 ; 
       fig20-default4.1-0 ; 
       fig20-default4_1.1-0 ; 
       fig20-default4_1_1.1-0 ; 
       fig20-default4_3.1-0 ; 
       fig20-default5.1-0 ; 
       fig20-default5_1.1-0 ; 
       fig20-default5_1_1.1-0 ; 
       fig20-default5_3.1-0 ; 
       fig20-default6.1-0 ; 
       fig20-default6_1.1-0 ; 
       fig20-default6_1_1.1-0 ; 
       fig20-default6_3.1-0 ; 
       fig20-default7.1-0 ; 
       fig20-default7_1.1-0 ; 
       fig20-default7_1_1.1-0 ; 
       fig20-default7_3.1-0 ; 
       fig20-default8.1-0 ; 
       fig20-default8_1.1-0 ; 
       fig20-default8_1_1.1-0 ; 
       fig20-default8_3.1-0 ; 
       fig20-default9.1-0 ; 
       fig20-default9_1.1-0 ; 
       fig20-default9_1_1.1-0 ; 
       fig20-default9_3.1-0 ; 
       fig20-mat71.1-0 ; 
       fig20-mat75.1-0 ; 
       fig20-mat77.1-0 ; 
       fig20-mat78.1-0 ; 
       fig20-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       fig20-blthrust.1-0 ; 
       fig20-brthrust.1-0 ; 
       fig20-canopy.11-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-cockpt.1-0 ; 
       fig20-cyl13.1-0 ; 
       fig20-cyl15.1-0 ; 
       fig20-cyl8.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-l-b-engine.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine1.1-0 ; 
       fig20-lsmoke.1-0 ; 
       fig20-lwepemt.1-0 ; 
       fig20-missemt.1-0 ; 
       fig20-r-b-engine.1-0 ; 
       fig20-rsmoke.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing3.1-0 ; 
       fig20-rwepemt.1-0 ; 
       fig20-SS01.1-0 ; 
       fig20-SS02.1-0 ; 
       fig20-SS03.1-0 ; 
       fig20-SS04.1-0 ; 
       fig20-SS05.1-0 ; 
       fig20-SS06.1-0 ; 
       fig20-tlthrust.1-0 ; 
       fig20-trail.1-0 ; 
       fig20-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       nulled-fig20.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 2 110 ; 
       0 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       1 2 110 ; 
       5 17 110 ; 
       6 18 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       15 2 110 ; 
       19 2 110 ; 
       12 2 110 ; 
       28 2 110 ; 
       14 2 110 ; 
       16 2 110 ; 
       26 2 110 ; 
       20 2 110 ; 
       17 3 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       27 2 110 ; 
       18 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       20 0 300 ; 
       21 85 300 ; 
       22 87 300 ; 
       23 86 300 ; 
       24 89 300 ; 
       25 88 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2107.138 -26.94645 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2107.138 -28.94645 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 2121.845 -20.60957 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2118.556 -18.76091 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2122.184 -25.83596 0 USR SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       3 SCHEM 2125.934 -27.83596 0 MPRFLG 0 ; 
       4 SCHEM 2118.635 -20.88811 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2118.567 -19.52225 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2123.375 -32.0812 0 MPRFLG 0 ; 
       6 SCHEM 2128.551 -32.16059 0 MPRFLG 0 ; 
       7 SCHEM 2125.934 -29.83597 0 MPRFLG 0 ; 
       8 SCHEM 2130.934 -27.83596 0 MPRFLG 0 ; 
       9 SCHEM 2118.434 -27.83596 0 MPRFLG 0 ; 
       10 SCHEM 2113.434 -27.83596 0 MPRFLG 0 ; 
       11 SCHEM 2115.779 -28.24747 0 USR MPRFLG 0 ; 
       15 SCHEM 2120.934 -27.83596 0 MPRFLG 0 ; 
       19 SCHEM 2121.818 -19.99248 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2116.129 -20.84521 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 2116.218 -19.49457 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2121.895 -22.59706 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2116.137 -21.42102 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 2116.229 -18.75235 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2128.244 -21.98841 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 2123.375 -30.08121 0 USR MPRFLG 0 ; 
       21 SCHEM 2128.219 -22.68956 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 2130.691 -21.91153 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 2130.615 -22.72249 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 2133.152 -21.90772 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2133.154 -22.79699 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2111.835 -20.2327 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2128.551 -30.1606 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 0 0 MPRFLG 0 ; 
       3 SCHEM 56.5 0 0 MPRFLG 0 ; 
       4 SCHEM 56.5 0 0 MPRFLG 0 ; 
       5 SCHEM 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 0 0 MPRFLG 0 ; 
       7 SCHEM 56.5 0 0 MPRFLG 0 ; 
       8 SCHEM 56.5 0 0 MPRFLG 0 ; 
       9 SCHEM 0 0 0 MPRFLG 0 ; 
       10 SCHEM 0 0 0 MPRFLG 0 ; 
       11 SCHEM 56.5 0 0 MPRFLG 0 ; 
       12 SCHEM 56.5 0 0 MPRFLG 0 ; 
       13 SCHEM 0 0 0 MPRFLG 0 ; 
       14 SCHEM 0 0 0 MPRFLG 0 ; 
       15 SCHEM 56.5 0 0 MPRFLG 0 ; 
       16 SCHEM 56.5 0 0 MPRFLG 0 ; 
       17 SCHEM 0 0 0 MPRFLG 0 ; 
       18 SCHEM 0 0 0 MPRFLG 0 ; 
       19 SCHEM 56.5 0 0 MPRFLG 0 ; 
       20 SCHEM 56.5 0 0 MPRFLG 0 ; 
       21 SCHEM 0 0 0 MPRFLG 0 ; 
       22 SCHEM 0 0 0 MPRFLG 0 ; 
       23 SCHEM 56.5 0 0 MPRFLG 0 ; 
       24 SCHEM 56.5 0 0 MPRFLG 0 ; 
       25 SCHEM 0 0 0 MPRFLG 0 ; 
       26 SCHEM 0 0 0 MPRFLG 0 ; 
       27 SCHEM 56.5 0 0 MPRFLG 0 ; 
       28 SCHEM 56.5 0 0 MPRFLG 0 ; 
       29 SCHEM 0 0 0 MPRFLG 0 ; 
       30 SCHEM 0 0 0 MPRFLG 0 ; 
       31 SCHEM 56.5 0 0 MPRFLG 0 ; 
       32 SCHEM 56.5 0 0 MPRFLG 0 ; 
       33 SCHEM 0 0 0 MPRFLG 0 ; 
       34 SCHEM 0 0 0 MPRFLG 0 ; 
       35 SCHEM 56.5 0 0 MPRFLG 0 ; 
       36 SCHEM 56.5 0 0 MPRFLG 0 ; 
       37 SCHEM 0 0 0 MPRFLG 0 ; 
       38 SCHEM 0 0 0 MPRFLG 0 ; 
       39 SCHEM 56.5 0 0 MPRFLG 0 ; 
       40 SCHEM 56.5 0 0 MPRFLG 0 ; 
       41 SCHEM 0 0 0 MPRFLG 0 ; 
       42 SCHEM 0 0 0 MPRFLG 0 ; 
       43 SCHEM 56.5 0 0 MPRFLG 0 ; 
       44 SCHEM 56.5 0 0 MPRFLG 0 ; 
       45 SCHEM 0 0 0 MPRFLG 0 ; 
       46 SCHEM 0 0 0 MPRFLG 0 ; 
       47 SCHEM 56.5 0 0 MPRFLG 0 ; 
       48 SCHEM 56.5 0 0 MPRFLG 0 ; 
       49 SCHEM 0 0 0 MPRFLG 0 ; 
       50 SCHEM 0 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 0 0 MPRFLG 0 ; 
       52 SCHEM 56.5 0 0 MPRFLG 0 ; 
       53 SCHEM 0 0 0 MPRFLG 0 ; 
       54 SCHEM 0 0 0 MPRFLG 0 ; 
       55 SCHEM 56.5 0 0 MPRFLG 0 ; 
       56 SCHEM 56.5 0 0 MPRFLG 0 ; 
       57 SCHEM 0 0 0 MPRFLG 0 ; 
       58 SCHEM 0 0 0 MPRFLG 0 ; 
       59 SCHEM 56.5 0 0 MPRFLG 0 ; 
       60 SCHEM 56.5 0 0 MPRFLG 0 ; 
       61 SCHEM 0 0 0 MPRFLG 0 ; 
       62 SCHEM 0 0 0 MPRFLG 0 ; 
       63 SCHEM 56.5 0 0 MPRFLG 0 ; 
       64 SCHEM 56.5 0 0 MPRFLG 0 ; 
       65 SCHEM 0 0 0 MPRFLG 0 ; 
       66 SCHEM 0 0 0 MPRFLG 0 ; 
       67 SCHEM 56.5 0 0 MPRFLG 0 ; 
       68 SCHEM 56.5 0 0 MPRFLG 0 ; 
       69 SCHEM 0 0 0 MPRFLG 0 ; 
       70 SCHEM 0 0 0 MPRFLG 0 ; 
       71 SCHEM 56.5 0 0 MPRFLG 0 ; 
       72 SCHEM 56.5 0 0 MPRFLG 0 ; 
       73 SCHEM 0 0 0 MPRFLG 0 ; 
       74 SCHEM 0 0 0 MPRFLG 0 ; 
       75 SCHEM 56.5 0 0 MPRFLG 0 ; 
       76 SCHEM 56.5 0 0 MPRFLG 0 ; 
       77 SCHEM 0 0 0 MPRFLG 0 ; 
       78 SCHEM 0 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 0 0 MPRFLG 0 ; 
       80 SCHEM 56.5 0 0 MPRFLG 0 ; 
       81 SCHEM 0 0 0 MPRFLG 0 ; 
       82 SCHEM 0 0 0 MPRFLG 0 ; 
       83 SCHEM 56.5 0 0 MPRFLG 0 ; 
       84 SCHEM 56.5 0 0 MPRFLG 0 ; 
       85 SCHEM 156.7651 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 537.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 542.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 1050.225 -2.4879 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 2069.126 -1.602436 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
