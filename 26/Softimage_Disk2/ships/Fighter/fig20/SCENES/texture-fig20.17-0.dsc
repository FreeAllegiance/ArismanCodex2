SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.19-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fig20-cam_int1.29-0 ROOT ; 
       fig20-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       edit_nulls-mat70.1-0 ; 
       fig20-mat71.1-0 ; 
       fig20-mat75.1-0 ; 
       fig20-mat77.1-0 ; 
       fig20-mat78.1-0 ; 
       fig20-mat80.1-0 ; 
       fig20-mat81.5-0 ; 
       fig20-mat82.3-0 ; 
       fig20-mat83.2-0 ; 
       fig20-mat85.1-0 ; 
       fig20-mat86.1-0 ; 
       fig20-mat87.3-0 ; 
       fig20-mat88.3-0 ; 
       fig20-mat89.2-0 ; 
       fig20-mat90.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       fig20-blthrust.1-0 ; 
       fig20-bottom_gun.1-0 ; 
       fig20-brthrust.1-0 ; 
       fig20-canopy.24-0 ROOT ; 
       fig20-center-wing.1-0 ; 
       fig20-cockpt.1-0 ; 
       fig20-cyl13.1-0 ; 
       fig20-cyl16.1-0 ; 
       fig20-engine-nib.1-0 ; 
       fig20-l-b-engine.1-0 ; 
       fig20-l-t-engine.1-0 ; 
       fig20-l-t-engine1.1-0 ; 
       fig20-lsmoke.1-0 ; 
       fig20-lwepemt.1-0 ; 
       fig20-missemt.1-0 ; 
       fig20-r-b-engine.1-0 ; 
       fig20-rsmoke.1-0 ; 
       fig20-rt-wing1.1-0 ; 
       fig20-rt-wing4.1-0 ; 
       fig20-rwepemt.1-0 ; 
       fig20-SS01.1-0 ; 
       fig20-SS02.1-0 ; 
       fig20-SS03.1-0 ; 
       fig20-SS04.1-0 ; 
       fig20-SS05.1-0 ; 
       fig20-SS06.1-0 ; 
       fig20-tlthrust.1-0 ; 
       fig20-trail.1-0 ; 
       fig20-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig20/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig20.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       fig20-t2d1.1-0 ; 
       fig20-t2d2.1-0 ; 
       fig20-t2d3.1-0 ; 
       fig20-t2d4.4-0 ; 
       fig20-t2d5.4-0 ; 
       fig20-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 4 110 ; 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 17 110 ; 
       7 18 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 9 300 ; 
       3 6 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       4 7 300 ; 
       17 8 300 ; 
       18 10 300 ; 
       20 0 300 ; 
       21 1 300 ; 
       22 3 300 ; 
       23 2 300 ; 
       24 5 300 ; 
       25 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 2 401 ; 
       8 0 401 ; 
       10 1 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       4 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 50 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -2 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 77.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 80.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 82.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 75.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
