SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig21-cam_int1.33-0 ROOT ; 
       fig21-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       fig21-mat81.5-0 ; 
       fig21-mat82.5-0 ; 
       fig21-mat83.5-0 ; 
       fig21-mat84.4-0 ; 
       fig21-mat85.3-0 ; 
       fig21-mat86.3-0 ; 
       fig21-mat87.2-0 ; 
       fig21-mat88.1-0 ; 
       fig21-mat89.2-0 ; 
       fig21-mat90.2-0 ; 
       fig21-mat91.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       fig21-cyl10.1-0 ; 
       fig21-cyl11.1-0 ; 
       fig21-cyl5.1-0 ; 
       fig21-cyl9.1-0 ; 
       fig21-null1.1-0 ; 
       fig21-zz_base.31-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig21/PICTURES/fig21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-fig21.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       fig21-back.5-0 ; 
       fig21-Side.13-0 ; 
       fig21-t2d1.13-0 ; 
       fig21-t2d10.5-0 ; 
       fig21-t2d2.12-0 ; 
       fig21-t2d3.11-0 ; 
       fig21-t2d4.11-0 ; 
       fig21-t2d5.10-0 ; 
       fig21-t2d7.5-0 ; 
       fig21-t2d8.5-0 ; 
       fig21-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       1 10 300 ; 
       2 7 300 ; 
       3 8 300 ; 
       5 0 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
