SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig21-cam_int1.4-0 ROOT ; 
       fig21-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       fig21-light1.2-0 ROOT ; 
       fig21-light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig21-mat71.1-0 ; 
       fig21-mat75.1-0 ; 
       fig21-mat77.1-0 ; 
       fig21-mat78.1-0 ; 
       fig21-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       fig21-bthrust.1-0 ; 
       fig21-cockpt.1-0 ; 
       fig21-cyl1.1-0 ; 
       fig21-cyl10.1-0 ; 
       fig21-cyl2.1-0 ; 
       fig21-cyl3.1-0 ; 
       fig21-cyl5.1-0 ; 
       fig21-cyl6.1-0 ; 
       fig21-cyl8.1-0 ; 
       fig21-cyl9.1-0 ; 
       fig21-lsmoke.1-0 ; 
       fig21-lthrust.1-0 ; 
       fig21-lwepemt.1-0 ; 
       fig21-missemt.1-0 ROOT ; 
       fig21-rsmoke.1-0 ; 
       fig21-rthrust.1-0 ; 
       fig21-rwepemt.1-0 ; 
       fig21-SS01.1-0 ; 
       fig21-SS02.1-0 ; 
       fig21-SS03.1-0 ; 
       fig21-SS04.1-0 ; 
       fig21-SS05.1-0 ; 
       fig21-SS06.1-0 ; 
       fig21-trail.1-0 ; 
       fig21-tthrust.1-0 ; 
       fig21-zz_base.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base-fig21.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       0 25 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 25 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       15 25 110 ; 
       1 25 110 ; 
       10 25 110 ; 
       12 25 110 ; 
       14 25 110 ; 
       16 25 110 ; 
       17 25 110 ; 
       18 25 110 ; 
       19 25 110 ; 
       20 25 110 ; 
       21 25 110 ; 
       22 25 110 ; 
       11 25 110 ; 
       23 25 110 ; 
       24 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 0 300 ; 
       18 1 300 ; 
       19 3 300 ; 
       20 2 300 ; 
       21 5 300 ; 
       22 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 90.81196 4.687069 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 90.81196 2.68707 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 114.6414 4.687069 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 117.1414 4.687069 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 88.04446 -4 0 MPRFLG 0 ; 
       0 SCHEM 105.2087 1.939517 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 95.54446 -4 0 MPRFLG 0 ; 
       4 SCHEM 80.54446 -4 0 MPRFLG 0 ; 
       5 SCHEM 85.54446 -4 0 MPRFLG 0 ; 
       6 SCHEM 88.04446 -2 0 MPRFLG 0 ; 
       7 SCHEM 83.04446 -4 0 MPRFLG 0 ; 
       8 SCHEM 90.54446 -4 0 MPRFLG 0 ; 
       9 SCHEM 93.04446 -4 0 MPRFLG 0 ; 
       25 SCHEM 88.04446 0 0 DISPLAY 1 2 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 105.2501 1.131185 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 109.3916 2.958328 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 101.507 3.185696 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 112.1394 2.198006 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 102.1683 -0.2564488 0 USR WIRECOL 1 7 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 3.592577e-022 -2.144784 1.122335 MPRFLG 0 ; 
       14 SCHEM 101.5146 2.377364 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 112.1414 2.93577 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 101.9064 -2.655762 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 101.9554 -3.389557 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 104.2354 -2.685113 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 104.2597 -3.418909 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 106.7354 -2.685113 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 106.7111 -3.418909 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 105.2685 2.646807 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 98.60284 4.364514 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 105.2608 3.42146 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
