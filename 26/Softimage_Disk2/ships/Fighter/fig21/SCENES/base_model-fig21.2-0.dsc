SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig21-cam_int1.7-0 ROOT ; 
       fig21-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       fig21-light1.5-0 ROOT ; 
       fig21-light2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.2-0 ; 
       fig21-mat71.2-0 ; 
       fig21-mat75.2-0 ; 
       fig21-mat77.2-0 ; 
       fig21-mat78.2-0 ; 
       fig21-mat80.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       fig21-bthrust.1-0 ; 
       fig21-cockpt.1-0 ; 
       fig21-cyl1.1-0 ; 
       fig21-cyl2.1-0 ; 
       fig21-cyl3.1-0 ; 
       fig21-cyl5.1-0 ; 
       fig21-lsmoke.1-0 ; 
       fig21-lthrust.1-0 ; 
       fig21-lwepemt.1-0 ; 
       fig21-missemt.1-0 ; 
       fig21-rsmoke.1-0 ; 
       fig21-rthrust.1-0 ; 
       fig21-rwepemt.1-0 ; 
       fig21-SS01.1-0 ; 
       fig21-SS02.1-0 ; 
       fig21-SS03.1-0 ; 
       fig21-SS04.1-0 ; 
       fig21-SS05.1-0 ; 
       fig21-SS06.1-0 ; 
       fig21-trail.1-0 ; 
       fig21-tthrust.1-0 ; 
       fig21-zz_base.8-0 ROOT ; 
       fig22-zz_base.1-0 ROOT ; 
       fig23-zz_base.1-0 ROOT ; 
       fig24-zz_base.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base_model-fig21.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 21 110 ; 
       1 21 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 21 110 ; 
       6 21 110 ; 
       7 21 110 ; 
       8 21 110 ; 
       9 21 110 ; 
       10 21 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 21 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 0 300 ; 
       14 1 300 ; 
       15 3 300 ; 
       16 2 300 ; 
       17 5 300 ; 
       18 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 90.81196 4.687069 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 90.81196 2.68707 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 114.6414 4.687069 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 117.1414 4.687069 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 106.5504 12.00222 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 110.7333 13.02103 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 88.37914 10.62788 0 MPRFLG 0 ; 
       3 SCHEM 80.87914 10.62788 0 MPRFLG 0 ; 
       4 SCHEM 85.87914 10.62788 0 MPRFLG 0 ; 
       5 SCHEM 88.37914 12.62788 0 MPRFLG 0 ; 
       6 SCHEM 102.8487 13.2484 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 106.6102 12.70951 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 113.4811 12.26071 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 103.51 9.806257 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 102.8563 12.44007 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 106.5918 11.19389 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 113.4831 12.99848 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 103.2481 7.406944 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 103.2971 6.673149 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 105.5771 7.377594 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 105.6014 6.643797 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 108.0771 7.377594 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 108.0528 6.643797 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 99.94452 14.42722 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 106.6025 13.48417 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 88.37914 14.62788 0 USR SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 88.79047 -6.933918 0 USR DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 88.6619 -5.827157 0 USR DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 88.67057 -4.285443 0 USR DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
