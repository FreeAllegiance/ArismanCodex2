SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Rotoscope-cam_int1.36-0 ROOT ; 
       Rotoscope-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       Rotoscope-default10.1-0 ; 
       Rotoscope-default7.1-0 ; 
       Rotoscope-default8.1-0 ; 
       Rotoscope-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       Rotoscope-bmerge3.4-0 ROOT ; 
       Rotoscope-cube1.13-0 ; 
       Rotoscope-cube2.1-0 ; 
       Rotoscope-sphere1.10-0 ; 
       Rotoscope-sphere3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-Rotoscope.36-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       1 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
