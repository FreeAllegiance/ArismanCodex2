SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       back_flap-cam_int1.6-0 ROOT ; 
       back_flap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       back_flap-default10.1-0 ; 
       back_flap-default7.1-0 ; 
       back_flap-default8.1-0 ; 
       back_flap-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       back_flap-bmerge3.2-0 ROOT ; 
       back_flap-cube1.13-0 ; 
       back_flap-cube2.1-0 ; 
       back_flap-cube4.4-0 ROOT ; 
       back_flap-cube5.1-0 ROOT ; 
       back_flap-sphere1.10-0 ; 
       back_flap-sphere3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-back_flap.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       1 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 0 0 SRT 0.3259999 0.1010599 0.5464517 2.443461 0 0 0 -2.767058 -1.317647 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 SRT 0.3259999 0.1010599 0.5464517 0.6981317 0 0 0 2.767058 -1.317647 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
