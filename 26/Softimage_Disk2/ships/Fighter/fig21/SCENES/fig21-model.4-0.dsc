SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       model-cam_int1.4-0 ROOT ; 
       model-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       model-default7.2-0 ; 
       model-mat1.1-0 ; 
       model-mat2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       model-bmerge3.3-0 ROOT ; 
       model-cube1.13-0 ; 
       model-cube4.6-0 ; 
       model-cube6.2-0 ; 
       model-cube7.2-0 ; 
       model-cyl1.1-0 ; 
       model-cyl4.1-0 ; 
       model-cyl4_1.1-0 ; 
       model-cyl5.1-0 ; 
       model-null1.1-0 ; 
       model-null2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-model.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       model-t2d1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 0 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       9 1 300 ; 
       10 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
