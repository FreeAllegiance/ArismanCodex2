SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       redo_cock-cam_int1.2-0 ROOT ; 
       redo_cock-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       redo_cock-default10.1-0 ; 
       redo_cock-default7.1-0 ; 
       redo_cock-default8.1-0 ; 
       redo_cock-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       redo_cock-bmerge3.2-0 ROOT ; 
       redo_cock-cube1.13-0 ; 
       redo_cock-cube4.6-0 ; 
       redo_cock-cube6.2-0 ; 
       redo_cock-cube7.2-0 ; 
       redo_cock-cyl1.1-0 ; 
       redo_cock-cyl4.1-0 ; 
       redo_cock-cyl4_1.1-0 ; 
       redo_cock-cyl5.1-0 ; 
       redo_cock-null1.1-0 ROOT ; 
       redo_cock-null2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-redo_cock.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 10 110 ; 
       6 10 110 ; 
       8 10 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 12.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
