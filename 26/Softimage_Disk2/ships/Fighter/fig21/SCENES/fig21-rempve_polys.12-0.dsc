SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rempve_polys-cam_int1.12-0 ROOT ; 
       rempve_polys-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       rempve_polys-default10.1-0 ; 
       rempve_polys-default7.1-0 ; 
       rempve_polys-default8.1-0 ; 
       rempve_polys-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       rempve_polys-bmerge3.5-0 ROOT ; 
       rempve_polys-cube1.13-0 ; 
       rempve_polys-cube4.6-0 ROOT ; 
       rempve_polys-cube6.2-0 ROOT ; 
       rempve_polys-cube7.2-0 ROOT ; 
       rempve_polys-cyl1.1-0 ROOT ; 
       rempve_polys-cyl4.1-0 ROOT ; 
       rempve_polys-cyl5.1-0 ROOT ; 
       rempve_polys-sphere1.10-0 ; 
       rempve_polys-sphere3.2-0 ; 
       rempve_polys1-cyl4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-rempve_polys.12-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 25 0 0 SRT 0.8419999 0.8419999 0.8419999 0 -0.1745329 0 0.7481014 -0.0103159 -1.384436 MPRFLG 0 ; 
       10 SCHEM 27.5 0 0 SRT 0.431104 0.431104 0.5569864 0.1745329 0 0 0 0.7427468 -1.384436 MPRFLG 0 ; 
       7 SCHEM 30 0 0 SRT 0.431104 0.431104 0.5569864 -0.1745329 0 0 0 -0.7427468 -1.384436 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 SRT 0.3259999 0.1010599 0.5464517 0.6981317 0 0 0 2.767058 -1.317647 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 SRT 0.03094279 0.155832 0.1767062 0 -2.478368 0 -6.14147 0 -5.186587 MPRFLG 0 ; 
       4 SCHEM 20 0 0 SRT 0.3259999 0.1010599 0.5464517 2.443461 0 0 0 -2.767058 -1.317647 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 22.5 0 0 SRT 0.8419999 0.8419999 0.8419999 0 0.1745329 0 -0.7481014 -0.0103159 -1.384436 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
