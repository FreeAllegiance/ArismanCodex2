SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rempve_polys-cam_int1.14-0 ROOT ; 
       rempve_polys-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       rempve_polys-default10.1-0 ; 
       rempve_polys-default7.1-0 ; 
       rempve_polys-default8.1-0 ; 
       rempve_polys-default9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       rempve_polys-bmerge3.7-0 ROOT ; 
       rempve_polys-cube1.13-0 ; 
       rempve_polys-cube4.6-0 ; 
       rempve_polys-cube6.2-0 ; 
       rempve_polys-cube7.2-0 ; 
       rempve_polys-cyl1.1-0 ; 
       rempve_polys-cyl4.1-0 ; 
       rempve_polys-cyl4_1.1-0 ; 
       rempve_polys-cyl5.1-0 ; 
       rempve_polys-null1.1-0 ; 
       rempve_polys-null2.1-0 ; 
       rempve_polys-sphere1.10-0 ; 
       rempve_polys-sphere3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-rempve_polys.14-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 0 110 ; 
       10 0 110 ; 
       7 10 110 ; 
       6 10 110 ; 
       8 10 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       5 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       0 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
