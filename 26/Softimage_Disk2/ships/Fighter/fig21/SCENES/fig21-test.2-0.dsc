SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_sid_wings-cam_int1.8-0 ROOT ; 
       texture_sid_wings-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       test-mat7.1-0 ; 
       test-mat8.1-0 ; 
       texture_sid_wings-default7.2-0 ; 
       texture_sid_wings-mat1.2-0 ; 
       texture_sid_wings-mat2.2-0 ; 
       texture_sid_wings-mat3.2-0 ; 
       texture_sid_wings-mat4.4-0 ; 
       texture_sid_wings-mat6.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       texture_sid_wings-bmerge3.7-0 ROOT ; 
       texture_sid_wings-cube1.13-0 ; 
       texture_sid_wings-cube4.6-0 ; 
       texture_sid_wings-cube7.2-0 ; 
       texture_sid_wings-cube8.1-0 ; 
       texture_sid_wings-cyl1.1-0 ; 
       texture_sid_wings-cyl4.1-0 ; 
       texture_sid_wings-cyl4_1.1-0 ; 
       texture_sid_wings-cyl5.1-0 ; 
       texture_sid_wings-null1.1-0 ; 
       texture_sid_wings-null2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-test.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       test-t2d5.1-0 ; 
       test-t2d6.1-0 ; 
       texture_sid_wings-t2d1.4-0 ; 
       texture_sid_wings-t2d2.4-0 ; 
       texture_sid_wings-t2d3.5-0 ; 
       texture_sid_wings-t2d4.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       4 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       4 0 300 ; 
       4 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
