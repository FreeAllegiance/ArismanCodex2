SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_sid_wings-cam_int1.5-0 ROOT ; 
       texture_sid_wings-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       texture_sid_wings-default7.1-0 ; 
       texture_sid_wings-mat1.1-0 ; 
       texture_sid_wings-mat2.1-0 ; 
       texture_sid_wings-mat3.1-0 ; 
       texture_sid_wings-mat4.2-0 ; 
       texture_sid_wings-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       texture_sid_wings-bmerge3.4-0 ROOT ; 
       texture_sid_wings-cube1.13-0 ; 
       texture_sid_wings-cube4.6-0 ; 
       texture_sid_wings-cube7.2-0 ; 
       texture_sid_wings-cyl1.1-0 ; 
       texture_sid_wings-cyl4.1-0 ; 
       texture_sid_wings-cyl4_1.1-0 ; 
       texture_sid_wings-cyl5.1-0 ; 
       texture_sid_wings-null1.1-0 ; 
       texture_sid_wings-null2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig21-texture-sid_wings.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       texture_sid_wings-t2d1.2-0 ; 
       texture_sid_wings-t2d2.2-0 ; 
       texture_sid_wings-t2d3.2-0 ; 
       texture_sid_wings-t2d4.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 0 110 ; 
       9 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       8 1 300 ; 
       9 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 1.54376e-005 -1.192093e-007 2.067501 MPRFLG 0 ; 
       1 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 27.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
