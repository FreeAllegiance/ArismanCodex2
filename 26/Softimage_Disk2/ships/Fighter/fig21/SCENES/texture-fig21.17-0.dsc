SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig21-cam_int1.26-0 ROOT ; 
       fig21-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       edit_nulls-mat70.3-0 ; 
       fig21-mat71.3-0 ; 
       fig21-mat75.3-0 ; 
       fig21-mat77.3-0 ; 
       fig21-mat78.3-0 ; 
       fig21-mat80.3-0 ; 
       fig21-mat81.5-0 ; 
       fig21-mat82.5-0 ; 
       fig21-mat83.5-0 ; 
       fig21-mat84.4-0 ; 
       fig21-mat85.3-0 ; 
       fig21-mat86.3-0 ; 
       fig21-mat87.2-0 ; 
       fig21-mat88.1-0 ; 
       fig21-mat89.2-0 ; 
       fig21-mat90.2-0 ; 
       fig21-mat91.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       fig21-bthrust.1-0 ; 
       fig21-cockpt.1-0 ; 
       fig21-cyl10.1-0 ; 
       fig21-cyl11.1-0 ; 
       fig21-cyl5.1-0 ; 
       fig21-cyl9.1-0 ; 
       fig21-lsmoke.1-0 ; 
       fig21-lthrust.1-0 ; 
       fig21-lwepemt.1-0 ; 
       fig21-missemt.1-0 ; 
       fig21-null1.1-0 ; 
       fig21-rsmoke.1-0 ; 
       fig21-rthrust.1-0 ; 
       fig21-rwepemt.1-0 ; 
       fig21-SS01.1-0 ; 
       fig21-SS02.1-0 ; 
       fig21-SS03.1-0 ; 
       fig21-SS04.1-0 ; 
       fig21-SS05.1-0 ; 
       fig21-SS06.1-0 ; 
       fig21-trail.1-0 ; 
       fig21-tthrust.1-0 ; 
       fig21-zz_base.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig21.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       fig21-back.3-0 ; 
       fig21-Side.11-0 ; 
       fig21-t2d1.11-0 ; 
       fig21-t2d10.3-0 ; 
       fig21-t2d2.10-0 ; 
       fig21-t2d3.9-0 ; 
       fig21-t2d4.9-0 ; 
       fig21-t2d5.8-0 ; 
       fig21-t2d7.3-0 ; 
       fig21-t2d8.3-0 ; 
       fig21-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 22 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 22 110 ; 
       7 22 110 ; 
       8 22 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 22 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 15 300 ; 
       3 16 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       22 6 300 ; 
       22 7 300 ; 
       22 8 300 ; 
       22 9 300 ; 
       22 10 300 ; 
       22 11 300 ; 
       22 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50 -4 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 45 -4 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.25 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 67.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 70 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
