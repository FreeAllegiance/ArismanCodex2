SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_sid_wings-cam_int1.9-0 ROOT ; 
       texture_sid_wings-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       fig21-mat100.1-0 ; 
       fig21-mat103.1-0 ; 
       fig21-mat104.1-0 ; 
       fig21-mat105.1-0 ; 
       fig21-mat106.1-0 ; 
       fig21-mat107.1-0 ; 
       fig21-mat108.1-0 ; 
       fig21-mat92.1-0 ; 
       fig21-mat93.1-0 ; 
       fig21-mat94.1-0 ; 
       fig21-mat95.1-0 ; 
       fig21-mat96.1-0 ; 
       fig21-mat97.1-0 ; 
       fig21-mat98.1-0 ; 
       fig21-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig21-canopy_1.1-0 ROOT ; 
       fig21-cube10.1-0 ROOT ; 
       fig21-cube11.1-0 ROOT ; 
       fig21-cube14.1-0 ROOT ; 
       fig21-cube15.1-0 ROOT ; 
       fig21-cube16.1-0 ROOT ; 
       fig21-cube9.1-0 ROOT ; 
       fig21-sphere2.1-0 ROOT ; 
       fig21-sphere3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig16 ; 
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig21.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       fig21-back1.1-0 ; 
       fig21-Front1.1-0 ; 
       fig21-Rear_front1.1-0 ; 
       fig21-t2d10.1-0 ; 
       fig21-t2d11.1-0 ; 
       fig21-t2d12.1-0 ; 
       fig21-t2d15.1-0 ; 
       fig21-t2d16.1-0 ; 
       fig21-t2d17.1-0 ; 
       fig21-t2d18.1-0 ; 
       fig21-t2d19.1-0 ; 
       fig21-t2d20.1-0 ; 
       fig21-t2d7.1-0 ; 
       fig21-t2d8.1-0 ; 
       fig21-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       4 3 300 ; 
       4 4 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       2 14 300 ; 
       2 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 12 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       0 5 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       5 10 401 ; 
       6 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 19.16555 5.461839 0 USR SRT 1 1 1 0 3.141593 0 -8.98032 0 -12.20137 MPRFLG 0 ; 
       6 SCHEM 7.579201 5.841133 0 USR SRT 1 1 1 0 0 0 8.98032 0 -12.20137 MPRFLG 0 ; 
       1 SCHEM 2.493317 -0.834447 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 15.13884 0.1203182 -18.14366 MPRFLG 0 ; 
       2 SCHEM 10.15172 5.689416 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 5.059144 0 0 USR DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 14.92414 5.46184 0 USR SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 12.57586 5.916993 0 USR SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 4.9766 -0.8344476 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -2.471268 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 0.0287323 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.528732 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       12 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM -2.471268 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 0.0287323 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.528732 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
