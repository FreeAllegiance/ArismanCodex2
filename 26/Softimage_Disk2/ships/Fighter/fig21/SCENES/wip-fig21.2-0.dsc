SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_sid_wings-cam_int1.10-0 ROOT ; 
       texture_sid_wings-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       fig21-mat100.1-0 ; 
       fig21-mat103.1-0 ; 
       fig21-mat104.1-0 ; 
       fig21-mat105.1-0 ; 
       fig21-mat106.1-0 ; 
       fig21-mat107.1-0 ; 
       fig21-mat108.1-0 ; 
       fig21-mat109.1-0 ; 
       fig21-mat110.1-0 ; 
       fig21-mat111.1-0 ; 
       fig21-mat112.1-0 ; 
       fig21-mat92.1-0 ; 
       fig21-mat93.1-0 ; 
       fig21-mat94.1-0 ; 
       fig21-mat95.1-0 ; 
       fig21-mat96.1-0 ; 
       fig21-mat97.1-0 ; 
       fig21-mat98.1-0 ; 
       fig21-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       fig21-canopy_1.1-0 ROOT ; 
       fig21-cube10.1-0 ROOT ; 
       fig21-cube11.2-0 ROOT ; 
       fig21-cube14.1-0 ROOT ; 
       fig21-cube15.1-0 ROOT ; 
       fig21-cube16.1-0 ROOT ; 
       fig21-cube17.1-0 ROOT ; 
       fig21-cube18.1-0 ROOT ; 
       fig21-cube9.2-0 ROOT ; 
       fig21-sphere2.2-0 ROOT ; 
       fig21-sphere3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig16 ; 
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig21.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       fig21-back1.1-0 ; 
       fig21-Front1.1-0 ; 
       fig21-Rear_front1.1-0 ; 
       fig21-t2d10.1-0 ; 
       fig21-t2d11.1-0 ; 
       fig21-t2d12.1-0 ; 
       fig21-t2d15.1-0 ; 
       fig21-t2d16.1-0 ; 
       fig21-t2d17.1-0 ; 
       fig21-t2d18.1-0 ; 
       fig21-t2d19.1-0 ; 
       fig21-t2d20.1-0 ; 
       fig21-t2d21.1-0 ; 
       fig21-t2d22.1-0 ; 
       fig21-t2d23.1-0 ; 
       fig21-t2d24.1-0 ; 
       fig21-t2d7.1-0 ; 
       fig21-t2d8.1-0 ; 
       fig21-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       4 3 300 ; 
       4 4 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       8 14 300 ; 
       8 15 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       2 18 300 ; 
       2 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       0 5 401 ; 
       11 0 401 ; 
       12 1 401 ; 
       13 2 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       5 10 401 ; 
       6 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 19.16555 5.461839 0 USR DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 -8.98032 0 -12.20137 MPRFLG 0 ; 
       6 SCHEM 14.58488 9.697989 0 USR WIRECOL 4 7 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 21.66555 9.735513 0 USR WIRECOL 4 7 SRT 1 1 1 0 3.141593 0 -8.98032 0 -12.20137 MPRFLG 0 ; 
       8 SCHEM 7.579201 5.841133 0 USR WIRECOL 4 7 SRT 1 1 1 0 0 0 8.98032 0 -12.20137 MPRFLG 0 ; 
       1 SCHEM 2.493317 -0.834447 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 15.13884 0.1203182 -18.14366 MPRFLG 0 ; 
       2 SCHEM 12.17477 9.735513 0 USR WIRECOL 4 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 5.059144 0 0 USR DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 10.07459 8.541961 0 USR DISPLAY 1 2 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 12.57586 5.916993 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 4.9766 -0.8344476 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 20.51383 0.8525443 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20.51383 0.8525443 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 18.08635 1.89438 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 18.08635 1.89438 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -2.471268 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 0.0287323 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.528732 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       12 SCHEM 20.51383 -1.147456 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20.51383 -1.147456 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 18.08635 -0.1056204 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 18.08635 -0.1056204 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM -2.471268 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 0.0287323 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.528732 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
