SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       texture_sid_wings-cam_int1.14-0 ROOT ; 
       texture_sid_wings-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       fig21-mat100.1-0 ; 
       fig21-mat103.1-0 ; 
       fig21-mat104.1-0 ; 
       fig21-mat105.1-0 ; 
       fig21-mat106.1-0 ; 
       fig21-mat107.1-0 ; 
       fig21-mat108.1-0 ; 
       fig21-mat109.1-0 ; 
       fig21-mat110.1-0 ; 
       fig21-mat111.1-0 ; 
       fig21-mat112.1-0 ; 
       fig21-mat92.1-0 ; 
       fig21-mat93.1-0 ; 
       fig21-mat94.1-0 ; 
       fig21-mat95.1-0 ; 
       fig21-mat96.1-0 ; 
       fig21-mat97.1-0 ; 
       fig21-mat98.1-0 ; 
       fig21-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       fig21-canopy_1.3-0 ROOT ; 
       fig21-cube10.3-0 ROOT ; 
       fig21-cube11.4-0 ROOT ; 
       fig21-cube14.3-0 ROOT ; 
       fig21-cube15.3-0 ROOT ; 
       fig21-cube16.3-0 ROOT ; 
       fig21-cube17.3-0 ROOT ; 
       fig21-cube18.3-0 ROOT ; 
       fig21-cube19.1-0 ROOT ; 
       fig21-cube9.4-0 ROOT ; 
       fig21-cyl1.1-0 ; 
       fig21-cyl2.1-0 ; 
       fig21-cyl3.1-0 ; 
       fig21-cyl4.1-0 ; 
       fig21-cyl5.1-0 ; 
       fig21-cyl6.1-0 ; 
       fig21-cyl8.1-0 ; 
       fig21-cyl9.1-0 ; 
       fig21-sphere2.5-0 ROOT ; 
       fig21-sphere3.3-0 ROOT ; 
       fig22-sphere2.3-0 ROOT ; 
       fig23-sphere2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig16 ; 
       //softmachine/D/Pete_Data/Softimage/ships/Fighter/fig21/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip_reduce-fig21.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       fig21-back1.1-0 ; 
       fig21-Front1.1-0 ; 
       fig21-Rear_front1.1-0 ; 
       fig21-t2d10.1-0 ; 
       fig21-t2d11.1-0 ; 
       fig21-t2d12.1-0 ; 
       fig21-t2d15.1-0 ; 
       fig21-t2d16.1-0 ; 
       fig21-t2d17.1-0 ; 
       fig21-t2d18.1-0 ; 
       fig21-t2d19.1-0 ; 
       fig21-t2d20.1-0 ; 
       fig21-t2d21.1-0 ; 
       fig21-t2d22.1-0 ; 
       fig21-t2d23.1-0 ; 
       fig21-t2d24.1-0 ; 
       fig21-t2d7.1-0 ; 
       fig21-t2d8.1-0 ; 
       fig21-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 18 110 ; 
       15 18 110 ; 
       17 18 110 ; 
       16 18 110 ; 
       10 18 110 ; 
       11 18 110 ; 
       12 18 110 ; 
       13 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       2 18 300 ; 
       2 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       9 14 300 ; 
       9 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       5 10 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 13 401 ; 
       9 14 401 ; 
       10 15 401 ; 
       11 0 401 ; 
       12 1 401 ; 
       13 2 401 ; 
       14 16 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 3 401 ; 
       18 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 42.5 8.021199 0 DISPLAY 0 0 SRT 1.179031 1.179031 1.179031 0 0 0 0 5.90702 -17.48941 MPRFLG 0 ; 
       14 SCHEM 45 8.021199 0 MPRFLG 0 ; 
       15 SCHEM 47.5 8.021199 0 MPRFLG 0 ; 
       17 SCHEM 52.5 8.021199 0 MPRFLG 0 ; 
       16 SCHEM 50 8.021199 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 15.13884 0.1203182 -18.14366 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 32.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 -8.98032 0 -12.20137 MPRFLG 0 ; 
       5 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 30 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 35 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 -8.98032 0 -12.20137 MPRFLG 0 ; 
       9 SCHEM 12.5 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 8.98032 0 -12.20137 MPRFLG 0 ; 
       10 SCHEM 12.84505 6.021199 0 MPRFLG 0 ; 
       11 SCHEM 15.34505 6.021199 0 MPRFLG 0 ; 
       12 SCHEM 17.84505 6.021199 0 MPRFLG 0 ; 
       13 SCHEM 20.34505 6.021199 0 MPRFLG 0 ; 
       18 SCHEM 16.59505 8.021199 0 USR DISPLAY 1 2 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 40 0 0 DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 0.0287323 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.528732 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5.028732 -1.333758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 0.0287323 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.528732 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5.028732 -3.333757 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
