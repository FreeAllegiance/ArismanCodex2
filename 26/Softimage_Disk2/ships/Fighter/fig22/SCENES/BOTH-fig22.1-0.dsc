SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl101-cam_int1.24-0 ROOT ; 
       utl101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       edit_nulls-mat70_1.1-0 ; 
       fig22-mat71.1-0 ; 
       fig22-mat75.1-0 ; 
       fig22-mat77.1-0 ; 
       fig22-mat78.1-0 ; 
       fig22-mat80.1-0 ; 
       fig22-mat81_1.3-0 ; 
       fig22-mat81_1_1.1-0 ; 
       fig22-mat82_1.3-0 ; 
       fig22-mat82_1_1.1-0 ; 
       fig22-mat83_1.3-0 ; 
       fig22-mat83_1_1.1-0 ; 
       fig22-mat84_1.3-0 ; 
       fig22-mat84_1_1.1-0 ; 
       fig22-mat85_1.3-0 ; 
       fig22-mat85_1_1.1-0 ; 
       fig22-mat86_1.3-0 ; 
       fig22-mat86_1_1.1-0 ; 
       fig22-mat87_1.3-0 ; 
       fig22-mat87_1_1.1-0 ; 
       fig22-mat91_1.1-0 ; 
       fig22-mat91_1_1.1-0 ; 
       fig22-mat92.1-0 ; 
       fig22-mat92_1.1-0 ; 
       fig22-mat93.1-0 ; 
       fig22-mat93_1.1-0 ; 
       fig22-mat94.1-0 ; 
       fig22-mat94_1.1-0 ; 
       fig22-mat95.6-0 ; 
       fig22-mat95_1.1-0 ; 
       fig22-mat96.6-0 ; 
       fig22-mat96_1.1-0 ; 
       fig22-mat97.6-0 ; 
       fig22-mat97_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       utl101-bthrust.1-0 ; 
       utl101-cockpt.1-0 ; 
       utl101-cyl11_3.1-0 ; 
       utl101-cyl11_3_1.1-0 ; 
       utl101-cyl12.1-0 ; 
       utl101-cyl12_1.1-0 ; 
       utl101-cyl13.1-0 ; 
       utl101-cyl13_1.1-0 ; 
       utl101-cyl14.1-0 ; 
       utl101-cyl14_1.1-0 ; 
       utl101-lsmoke.1-0 ; 
       utl101-lthrust.1-0 ; 
       utl101-lwepemt.1-0 ; 
       utl101-missemt.1-0 ; 
       utl101-null1_3.1-0 ; 
       utl101-null1_3_1.1-0 ; 
       utl101-rsmoke.1-0 ; 
       utl101-rthrust.1-0 ; 
       utl101-rwepemt.1-0 ; 
       utl101-SS01.1-0 ; 
       utl101-SS02.1-0 ; 
       utl101-SS03.1-0 ; 
       utl101-SS04.1-0 ; 
       utl101-SS05.1-0 ; 
       utl101-SS06.1-0 ; 
       utl101-trail.1-0 ; 
       utl101-tthrust.1-0 ; 
       utl101-zz_base.20-0 ROOT ; 
       utl101-zz_base_2_3.19-0 ; 
       utl101-zz_base_2_3_1.19-0 ; 
       utl101-zz_base_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig22/PICTURES/fig22 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-fig22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       fig22-Side_1.3-0 ; 
       fig22-Side_1_1.1-0 ; 
       fig22-t2d1_1.3-0 ; 
       fig22-t2d1_1_1.1-0 ; 
       fig22-t2d10_1.1-0 ; 
       fig22-t2d10_1_1.1-0 ; 
       fig22-t2d11.1-0 ; 
       fig22-t2d11_1.1-0 ; 
       fig22-t2d12.1-0 ; 
       fig22-t2d12_1.1-0 ; 
       fig22-t2d13.1-0 ; 
       fig22-t2d13_1.1-0 ; 
       fig22-t2d14.5-0 ; 
       fig22-t2d14_1.1-0 ; 
       fig22-t2d15.5-0 ; 
       fig22-t2d15_1.1-0 ; 
       fig22-t2d16.5-0 ; 
       fig22-t2d16_1.1-0 ; 
       fig22-t2d2_1.3-0 ; 
       fig22-t2d2_1_1.1-0 ; 
       fig22-t2d3_1.3-0 ; 
       fig22-t2d3_1_1.1-0 ; 
       fig22-t2d4_1.3-0 ; 
       fig22-t2d4_1_1.1-0 ; 
       fig22-t2d5_1.3-0 ; 
       fig22-t2d5_1_1.1-0 ; 
       fig22-t2d6_1.3-0 ; 
       fig22-t2d6_1_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 30 110 ; 
       1 30 110 ; 
       2 14 110 ; 
       4 14 110 ; 
       6 14 110 ; 
       8 14 110 ; 
       10 30 110 ; 
       11 30 110 ; 
       12 30 110 ; 
       13 30 110 ; 
       14 28 110 ; 
       16 30 110 ; 
       17 30 110 ; 
       18 30 110 ; 
       19 30 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 30 110 ; 
       25 30 110 ; 
       26 30 110 ; 
       28 30 110 ; 
       3 15 110 ; 
       5 15 110 ; 
       7 15 110 ; 
       9 15 110 ; 
       15 29 110 ; 
       29 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 20 300 ; 
       4 22 300 ; 
       6 24 300 ; 
       8 26 300 ; 
       19 0 300 ; 
       20 1 300 ; 
       21 3 300 ; 
       22 2 300 ; 
       23 5 300 ; 
       24 4 300 ; 
       30 28 300 ; 
       30 30 300 ; 
       30 32 300 ; 
       28 6 300 ; 
       28 8 300 ; 
       28 10 300 ; 
       28 12 300 ; 
       28 14 300 ; 
       28 16 300 ; 
       28 18 300 ; 
       3 21 300 ; 
       5 23 300 ; 
       7 25 300 ; 
       9 27 300 ; 
       27 29 300 ; 
       27 31 300 ; 
       27 33 300 ; 
       29 7 300 ; 
       29 9 300 ; 
       29 11 300 ; 
       29 13 300 ; 
       29 15 300 ; 
       29 17 300 ; 
       29 19 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 27 401 ; 
       8 1 401 ; 
       10 3 401 ; 
       12 19 401 ; 
       14 21 401 ; 
       16 23 401 ; 
       18 25 401 ; 
       20 5 401 ; 
       22 7 401 ; 
       24 9 401 ; 
       26 11 401 ; 
       28 17 401 ; 
       30 13 401 ; 
       32 15 401 ; 
       7 26 401 ; 
       9 0 401 ; 
       11 2 401 ; 
       13 18 401 ; 
       15 20 401 ; 
       17 22 401 ; 
       19 24 401 ; 
       21 4 401 ; 
       23 6 401 ; 
       25 8 401 ; 
       27 10 401 ; 
       29 16 401 ; 
       31 12 401 ; 
       33 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 82 -6 0 MPRFLG 0 ; 
       5 SCHEM 84.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 87 -6 0 MPRFLG 0 ; 
       9 SCHEM 89.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 85.75 -4 0 MPRFLG 0 ; 
       27 SCHEM 98.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 94.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 77.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 92 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 94.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 97 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 99.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 102 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 104.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 107 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 82 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 84.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 87 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 89.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 109.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 112 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 114.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 94.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 97 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 82 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 84.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 89.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 112 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 114.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 109.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 99.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 102 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 104.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 107 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 92 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
