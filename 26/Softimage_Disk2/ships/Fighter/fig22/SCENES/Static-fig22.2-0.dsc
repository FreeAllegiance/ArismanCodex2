SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl101-cam_int1.30-0 ROOT ; 
       utl101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       fig22-mat81_1.3-0 ; 
       fig22-mat82_1.3-0 ; 
       fig22-mat83_1.3-0 ; 
       fig22-mat84_1.3-0 ; 
       fig22-mat85_1.3-0 ; 
       fig22-mat86_1.3-0 ; 
       fig22-mat87_1.3-0 ; 
       fig22-mat91_1.1-0 ; 
       fig22-mat92.1-0 ; 
       fig22-mat93.1-0 ; 
       fig22-mat94.1-0 ; 
       fig22-mat95.6-0 ; 
       fig22-mat96.6-0 ; 
       fig22-mat97.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       utl101-cyl11_3.1-0 ; 
       utl101-cyl12.1-0 ; 
       utl101-cyl13.1-0 ; 
       utl101-cyl14.1-0 ; 
       utl101-null1_3.1-0 ; 
       utl101-zz_base_2_3.19-0 ; 
       utl101-zz_base_3.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig22/PICTURES/fig22 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Static-fig22.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       fig22-Side_1_1.3-0 ; 
       fig22-t2d1_1_1.3-0 ; 
       fig22-t2d10_1_1.3-0 ; 
       fig22-t2d11_1.3-0 ; 
       fig22-t2d12_1.3-0 ; 
       fig22-t2d13_1.3-0 ; 
       fig22-t2d14_1.4-0 ; 
       fig22-t2d15_1.4-0 ; 
       fig22-t2d16_1.4-0 ; 
       fig22-t2d2_1_1.3-0 ; 
       fig22-t2d3_1_1.3-0 ; 
       fig22-t2d4_1_1.3-0 ; 
       fig22-t2d5_1_1.3-0 ; 
       fig22-t2d6_1_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 5 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       5 0 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 13 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 9 401 ; 
       4 10 401 ; 
       5 11 401 ; 
       6 12 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 8 401 ; 
       12 6 401 ; 
       13 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 18.75 0 0 SRT 0.9380001 0.9380001 0.9380001 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
