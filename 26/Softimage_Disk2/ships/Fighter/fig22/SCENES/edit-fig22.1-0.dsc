SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl101-cam_int1.25-0 ROOT ; 
       utl101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       edit_nulls-mat70_1.1-0 ; 
       fig22-mat71.1-0 ; 
       fig22-mat75.1-0 ; 
       fig22-mat77.1-0 ; 
       fig22-mat78.1-0 ; 
       fig22-mat80.1-0 ; 
       fig22-mat81_1.3-0 ; 
       fig22-mat82_1.3-0 ; 
       fig22-mat83_1.3-0 ; 
       fig22-mat84_1.3-0 ; 
       fig22-mat85_1.3-0 ; 
       fig22-mat86_1.3-0 ; 
       fig22-mat87_1.3-0 ; 
       fig22-mat91_1.1-0 ; 
       fig22-mat92.1-0 ; 
       fig22-mat93.1-0 ; 
       fig22-mat94.1-0 ; 
       fig22-mat95.6-0 ; 
       fig22-mat96.6-0 ; 
       fig22-mat97.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       utl101-bthrust.1-0 ; 
       utl101-cockpt.1-0 ; 
       utl101-cyl11_3.1-0 ; 
       utl101-cyl12.1-0 ; 
       utl101-cyl13.1-0 ; 
       utl101-cyl14.1-0 ; 
       utl101-fwepemt.1-0 ; 
       utl101-lsmoke.1-0 ; 
       utl101-lthrust.1-0 ; 
       utl101-lwepemt.1-0 ; 
       utl101-missemt.1-0 ; 
       utl101-null1_3.1-0 ; 
       utl101-rsmoke.1-0 ; 
       utl101-rthrust.1-0 ; 
       utl101-rwepemt.1-0 ; 
       utl101-SS01.1-0 ; 
       utl101-SS02.1-0 ; 
       utl101-SS03.1-0 ; 
       utl101-SS04.1-0 ; 
       utl101-SS05.1-0 ; 
       utl101-SS06.1-0 ; 
       utl101-trail.1-0 ; 
       utl101-tthrust.1-0 ; 
       utl101-zz_base_2_3.19-0 ; 
       utl101-zz_base_3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig22/PICTURES/fig22 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       edit-fig22.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       fig22-Side_1_1.2-0 ; 
       fig22-t2d1_1_1.2-0 ; 
       fig22-t2d10_1_1.2-0 ; 
       fig22-t2d11_1.2-0 ; 
       fig22-t2d12_1.2-0 ; 
       fig22-t2d13_1.2-0 ; 
       fig22-t2d14_1.2-0 ; 
       fig22-t2d15_1.2-0 ; 
       fig22-t2d16_1.2-0 ; 
       fig22-t2d2_1_1.2-0 ; 
       fig22-t2d3_1_1.2-0 ; 
       fig22-t2d4_1_1.2-0 ; 
       fig22-t2d5_1_1.2-0 ; 
       fig22-t2d6_1_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 24 110 ; 
       0 24 110 ; 
       1 24 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       7 24 110 ; 
       8 24 110 ; 
       9 24 110 ; 
       10 24 110 ; 
       11 23 110 ; 
       12 24 110 ; 
       13 24 110 ; 
       14 24 110 ; 
       15 24 110 ; 
       16 24 110 ; 
       17 24 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       21 24 110 ; 
       22 24 110 ; 
       23 24 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 13 300 ; 
       3 14 300 ; 
       4 15 300 ; 
       5 16 300 ; 
       15 0 300 ; 
       16 1 300 ; 
       17 3 300 ; 
       18 2 300 ; 
       19 5 300 ; 
       20 4 300 ; 
       23 6 300 ; 
       23 7 300 ; 
       23 8 300 ; 
       23 9 300 ; 
       23 10 300 ; 
       23 11 300 ; 
       23 12 300 ; 
       24 17 300 ; 
       24 18 300 ; 
       24 19 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 13 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 2 401 ; 
       14 3 401 ; 
       15 4 401 ; 
       16 5 401 ; 
       17 8 401 ; 
       18 6 401 ; 
       19 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 50 -6 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 55 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60 -2 0 MPRFLG 0 ; 
       24 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.42534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 58.92534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.42534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 63.92534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 66.42534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 68.92534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.42534 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.42534 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 48.92534 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.42534 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 53.92534 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 73.92534 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 76.42534 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 78.92534 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 58.92534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.42534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.42534 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 48.92534 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.42534 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 53.92534 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 76.42534 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 78.92534 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 73.92534 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 63.92534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 66.42534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 68.92534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 71.42534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.42534 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
