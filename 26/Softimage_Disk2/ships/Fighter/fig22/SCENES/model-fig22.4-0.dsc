SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl101-cam_int1.12-0 ROOT ; 
       utl101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       edit_nulls-mat70_1.1-0 ; 
       fig22-mat71.1-0 ; 
       fig22-mat75.1-0 ; 
       fig22-mat77.1-0 ; 
       fig22-mat78.1-0 ; 
       fig22-mat80.1-0 ; 
       fig22-mat81.1-0 ; 
       fig22-mat82.1-0 ; 
       fig22-mat83.1-0 ; 
       fig22-mat84.1-0 ; 
       fig22-mat85.1-0 ; 
       fig22-mat86.1-0 ; 
       fig22-mat87.1-0 ; 
       fig22-mat88.1-0 ; 
       fig22-mat89.1-0 ; 
       fig22-mat90.1-0 ; 
       fig22-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       utl101-bthrust_1.1-0 ; 
       utl101-cockpt_1.1-0 ; 
       utl101-cyl10.1-0 ; 
       utl101-cyl11.1-0 ; 
       utl101-cyl5.1-0 ; 
       utl101-cyl9.1-0 ; 
       utl101-lsmoke_1.1-0 ; 
       utl101-lthrust_1.1-0 ; 
       utl101-lwepemt_1.1-0 ; 
       utl101-missemt_1.1-0 ; 
       utl101-null1.1-0 ; 
       utl101-rsmoke_1.1-0 ; 
       utl101-rthrust_1.1-0 ; 
       utl101-rwepemt_1.1-0 ; 
       utl101-SS01_1.1-0 ; 
       utl101-SS02_1.1-0 ; 
       utl101-SS03_1.1-0 ; 
       utl101-SS04_1.1-0 ; 
       utl101-SS05_1.1-0 ; 
       utl101-SS06_1.1-0 ; 
       utl101-trail_1.1-0 ; 
       utl101-tthrust_1.1-0 ; 
       utl101-zz_base.10-0 ROOT ; 
       utl101-zz_base_2.19-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Fighter/fig22/PICTURES/fig22 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig22.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       fig22-Side.2-0 ; 
       fig22-t2d1.2-0 ; 
       fig22-t2d10.2-0 ; 
       fig22-t2d2.2-0 ; 
       fig22-t2d3.2-0 ; 
       fig22-t2d4.2-0 ; 
       fig22-t2d5.2-0 ; 
       fig22-t2d6.2-0 ; 
       fig22-t2d7.2-0 ; 
       fig22-t2d8.2-0 ; 
       fig22-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 22 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 22 110 ; 
       7 22 110 ; 
       8 22 110 ; 
       9 22 110 ; 
       10 23 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 22 110 ; 
       15 22 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       23 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 15 300 ; 
       3 16 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       23 6 300 ; 
       23 7 300 ; 
       23 8 300 ; 
       23 9 300 ; 
       23 10 300 ; 
       23 11 300 ; 
       23 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 7 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 10 401 ; 
       16 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.99999 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.21606 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.43648 -5.341234 0 MPRFLG 0 ; 
       3 SCHEM 39.93648 -5.341234 0 MPRFLG 0 ; 
       4 SCHEM 32.43648 -5.341234 0 MPRFLG 0 ; 
       5 SCHEM 34.93648 -5.341234 0 MPRFLG 0 ; 
       6 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 53.71606 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 36.18648 -3.341234 0 USR MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 56.21606 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.25 0 0 SRT 0.6037188 0.5397246 0.7872491 0 0 0 0 0 11.21029 MPRFLG 0 ; 
       23 SCHEM 36.21606 -1.872507 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.43648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44.93648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.43648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49.93648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.43648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54.93648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.43648 -3.872507 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.43648 -7.341234 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 34.93648 -7.341234 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.43648 -7.341234 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39.93648 -7.341234 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44.93648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 47.43648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39.93648 -9.341234 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 49.93648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.43648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54.93648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 57.43648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.43648 -5.872507 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.43648 -9.341234 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34.93648 -9.341234 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.43648 -9.341234 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
