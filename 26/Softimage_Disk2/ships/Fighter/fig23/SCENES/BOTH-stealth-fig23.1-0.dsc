SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       altstealth-cam_int1.14-0 ROOT ; 
       altstealth-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       altstealth-mat105.3-0 ; 
       altstealth-mat105_1.1-0 ; 
       altstealth-mat106.3-0 ; 
       altstealth-mat106_1.1-0 ; 
       altstealth-mat110.1-0 ; 
       altstealth-mat110_1.1-0 ; 
       altstealth-mat71.1-0 ; 
       altstealth-mat75.1-0 ; 
       altstealth-mat77.1-0 ; 
       altstealth-mat78.1-0 ; 
       altstealth-mat80.1-0 ; 
       altstealth-mat81.4-0 ; 
       altstealth-mat81_1.1-0 ; 
       altstealth-mat83_3.1-0 ; 
       altstealth-mat83_3_1.1-0 ; 
       altstealth-mat88.4-0 ; 
       altstealth-mat88_1.1-0 ; 
       altstealth-mat89.4-0 ; 
       altstealth-mat89_1.1-0 ; 
       altstealth-mat91.4-0 ; 
       altstealth-mat91_1.1-0 ; 
       edit_nulls-mat70_1.1-0 ; 
       stealth_fig23-mat111.1-0 ; 
       stealth_fig23-mat111_1.1-0 ; 
       stealth_fig23-mat112.1-0 ; 
       stealth_fig23-mat112_1.1-0 ; 
       stealth_fig23-mat113.1-0 ; 
       stealth_fig23-mat113_1.1-0 ; 
       stealth_fig23-mat114.1-0 ; 
       stealth_fig23-mat114_1.1-0 ; 
       stealth_fig23-mat83_4.1-0 ; 
       stealth_fig23-mat83_4_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       altstealth-blthrust.2-0 ; 
       altstealth-brthrust.2-0 ; 
       altstealth-canopy_1_1.14-0 ROOT ; 
       altstealth-canopy_1_1_1.1-0 ROOT ; 
       altstealth-center-wing1.1-0 ; 
       altstealth-center-wing1_1.1-0 ; 
       altstealth-cockpt.2-0 ; 
       altstealth-lsmoke.2-0 ; 
       altstealth-lwepemt.2-0 ; 
       altstealth-missemt.2-0 ; 
       altstealth-rsmoke.2-0 ; 
       altstealth-rt-wing1_2.1-0 ; 
       altstealth-rt-wing1_2_1.1-0 ; 
       altstealth-rt-wing1_3.1-0 ; 
       altstealth-rt-wing1_3_1.1-0 ; 
       altstealth-rt-wing12.1-0 ; 
       altstealth-rt-wing12_1.1-0 ; 
       altstealth-rt-wing16.1-0 ; 
       altstealth-rt-wing16_1.1-0 ; 
       altstealth-rt-wing17.1-0 ; 
       altstealth-rt-wing17_1.1-0 ; 
       altstealth-rt-wing18.1-0 ; 
       altstealth-rt-wing18_1.1-0 ; 
       altstealth-rwepemt.2-0 ; 
       altstealth-SS01.2-0 ; 
       altstealth-SS02.2-0 ; 
       altstealth-SS03.2-0 ; 
       altstealth-SS04.2-0 ; 
       altstealth-SS05.2-0 ; 
       altstealth-SS06.2-0 ; 
       altstealth-thrust.1-0 ; 
       altstealth-tlthrust.2-0 ; 
       altstealth-trail.2-0 ; 
       altstealth-trthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig23/PICTURES/fig23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-stealth-fig23.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       altstealth-back.6-0 ; 
       altstealth-back_1.1-0 ; 
       altstealth-bottom.3-0 ; 
       altstealth-bottom_1.1-0 ; 
       altstealth-Front.6-0 ; 
       altstealth-Front_1.1-0 ; 
       altstealth-Rear_front.6-0 ; 
       altstealth-Rear_front_1.1-0 ; 
       altstealth-t2d1_3.5-0 ; 
       altstealth-t2d1_3_1.1-0 ; 
       altstealth-t2d18.5-0 ; 
       altstealth-t2d18_1.1-0 ; 
       altstealth-t2d19.5-0 ; 
       altstealth-t2d19_1.1-0 ; 
       altstealth-t2d23.4-0 ; 
       altstealth-t2d23_1.1-0 ; 
       stealth_fig23-t2d1_4.1-0 ; 
       stealth_fig23-t2d1_4_1.1-0 ; 
       stealth_fig23-t2d24.3-0 ; 
       stealth_fig23-t2d24_1.1-0 ; 
       stealth_fig23-t2d25.1-0 ; 
       stealth_fig23-t2d25_1.1-0 ; 
       stealth_fig23-t2d26.1-0 ; 
       stealth_fig23-t2d26_1.1-0 ; 
       stealth_fig23-t2d27.1-0 ; 
       stealth_fig23-t2d27_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 2 110 ; 
       12 2 110 ; 
       14 2 110 ; 
       16 2 110 ; 
       18 2 110 ; 
       20 2 110 ; 
       0 3 110 ; 
       1 3 110 ; 
       4 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       13 3 110 ; 
       15 3 110 ; 
       17 3 110 ; 
       19 3 110 ; 
       21 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       31 3 110 ; 
       32 3 110 ; 
       33 3 110 ; 
       22 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 12 300 ; 
       2 16 300 ; 
       2 18 300 ; 
       2 20 300 ; 
       5 1 300 ; 
       5 3 300 ; 
       5 27 300 ; 
       12 5 300 ; 
       14 29 300 ; 
       16 14 300 ; 
       18 23 300 ; 
       20 25 300 ; 
       3 11 300 ; 
       3 15 300 ; 
       3 17 300 ; 
       3 19 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       4 26 300 ; 
       11 4 300 ; 
       13 28 300 ; 
       15 13 300 ; 
       17 22 300 ; 
       19 24 300 ; 
       21 30 300 ; 
       24 21 300 ; 
       25 6 300 ; 
       26 8 300 ; 
       27 7 300 ; 
       28 10 300 ; 
       29 9 300 ; 
       22 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       2 13 401 ; 
       4 15 401 ; 
       11 1 401 ; 
       13 9 401 ; 
       15 5 401 ; 
       17 7 401 ; 
       19 3 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       26 23 401 ; 
       28 25 401 ; 
       30 17 401 ; 
       1 10 401 ; 
       3 12 401 ; 
       5 14 401 ; 
       12 0 401 ; 
       14 8 401 ; 
       16 4 401 ; 
       18 6 401 ; 
       20 2 401 ; 
       23 18 401 ; 
       25 20 401 ; 
       27 22 401 ; 
       29 24 401 ; 
       31 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 81.5 0 0 SRT 1 1 1 0 0 0 0 0.05263241 0.6099114 MPRFLG 0 ; 
       5 SCHEM 76.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 79 -2 0 MPRFLG 0 ; 
       14 SCHEM 89 -2 0 MPRFLG 0 ; 
       16 SCHEM 74 -2 0 MPRFLG 0 ; 
       18 SCHEM 81.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 84 -2 0 MPRFLG 0 ; 
       0 SCHEM 14.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 49.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 4.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 39.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 59.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 69.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 12 -2 0 MPRFLG 0 ; 
       17 SCHEM 62 -2 0 MPRFLG 0 ; 
       19 SCHEM 64.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 67 -2 0 MPRFLG 0 ; 
       23 SCHEM 42 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 22 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 17 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 37 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 32 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 54.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 57 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 27 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 7 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 86.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 47 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 57 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 68.49998 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 71.05859 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 73.79697 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.63519 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 22 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 62 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 64.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 49.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 69.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 66 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 75.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 75.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 78 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 108 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 73 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 110.5586 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 113.297 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 116.1352 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 80.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 83 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 75.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 88 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 85.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 67.49998 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 75.63519 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 70.05859 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 72.79697 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 64.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 49.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 69.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 107 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 115.1352 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 109.5586 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 112.297 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 72 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 75.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 75.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 78 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 84.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 80.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 83 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 75.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 88 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
