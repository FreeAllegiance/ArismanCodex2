SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       altstealth-cam_int1.12-0 ROOT ; 
       altstealth-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       altstealth-mat105.3-0 ; 
       altstealth-mat106.3-0 ; 
       altstealth-mat110.1-0 ; 
       altstealth-mat81.4-0 ; 
       altstealth-mat83_3.1-0 ; 
       altstealth-mat88.4-0 ; 
       altstealth-mat89.4-0 ; 
       altstealth-mat91.4-0 ; 
       stealth_fig23-mat111.1-0 ; 
       stealth_fig23-mat112.1-0 ; 
       stealth_fig23-mat113.1-0 ; 
       stealth_fig23-mat114.1-0 ; 
       stealth_fig23-mat83_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       altstealth-canopy_1_1.12-0 ROOT ; 
       altstealth-center-wing1.1-0 ; 
       altstealth-rt-wing1_2.1-0 ; 
       altstealth-rt-wing1_3.1-0 ; 
       altstealth-rt-wing12.1-0 ; 
       altstealth-rt-wing16.1-0 ; 
       altstealth-rt-wing17.1-0 ; 
       altstealth-rt-wing18.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig23/PICTURES/fig23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-stealth-fig23.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       altstealth-back.6-0 ; 
       altstealth-bottom.3-0 ; 
       altstealth-Front.6-0 ; 
       altstealth-Rear_front.6-0 ; 
       altstealth-t2d1_3.5-0 ; 
       altstealth-t2d18.5-0 ; 
       altstealth-t2d19.5-0 ; 
       altstealth-t2d23.4-0 ; 
       stealth_fig23-t2d1_4.1-0 ; 
       stealth_fig23-t2d24.3-0 ; 
       stealth_fig23-t2d25.1-0 ; 
       stealth_fig23-t2d26.1-0 ; 
       stealth_fig23-t2d27.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 10 300 ; 
       2 2 300 ; 
       4 4 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       7 12 300 ; 
       3 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       2 7 401 ; 
       3 0 401 ; 
       11 12 401 ; 
       4 4 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 1 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       12 8 401 ; 
       10 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 36.49998 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.05859 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 41.79697 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44.63519 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35.49998 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 43.63519 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 38.05859 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40.79697 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 0.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
