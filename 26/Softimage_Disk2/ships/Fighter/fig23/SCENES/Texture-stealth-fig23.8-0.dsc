SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       altstealth-cam_int1.9-0 ROOT ; 
       altstealth-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       altstealth-mat105.2-0 ; 
       altstealth-mat106.2-0 ; 
       altstealth-mat110.1-0 ; 
       altstealth-mat71.1-0 ; 
       altstealth-mat75.1-0 ; 
       altstealth-mat77.1-0 ; 
       altstealth-mat78.1-0 ; 
       altstealth-mat80.1-0 ; 
       altstealth-mat81.4-0 ; 
       altstealth-mat83.1-0 ; 
       altstealth-mat83_3.1-0 ; 
       altstealth-mat88.4-0 ; 
       altstealth-mat89.4-0 ; 
       altstealth-mat91.4-0 ; 
       edit_nulls-mat70_1.1-0 ; 
       stealth_fig23-mat111.1-0 ; 
       stealth_fig23-mat112.1-0 ; 
       stealth_fig23-mat83_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       altstealth-blthrust.2-0 ; 
       altstealth-brthrust.2-0 ; 
       altstealth-canopy_1_1.9-0 ROOT ; 
       altstealth-center-wing1.1-0 ; 
       altstealth-cockpt.2-0 ; 
       altstealth-lsmoke.2-0 ; 
       altstealth-lwepemt.2-0 ; 
       altstealth-missemt.2-0 ; 
       altstealth-rsmoke.2-0 ; 
       altstealth-rt-wing1_1.1-0 ; 
       altstealth-rt-wing1_2.1-0 ; 
       altstealth-rt-wing12.1-0 ; 
       altstealth-rt-wing16.1-0 ; 
       altstealth-rt-wing17.1-0 ; 
       altstealth-rt-wing18.1-0 ; 
       altstealth-rwepemt.2-0 ; 
       altstealth-SS01.2-0 ; 
       altstealth-SS02.2-0 ; 
       altstealth-SS03.2-0 ; 
       altstealth-SS04.2-0 ; 
       altstealth-SS05.2-0 ; 
       altstealth-SS06.2-0 ; 
       altstealth-tlthrust.2-0 ; 
       altstealth-trail.2-0 ; 
       altstealth-trthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig23/PICTURES/fig23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-stealth-fig23.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       altstealth-back.6-0 ; 
       altstealth-bottom.3-0 ; 
       altstealth-Front.6-0 ; 
       altstealth-Rear_front.6-0 ; 
       altstealth-t2d1.3-0 ; 
       altstealth-t2d1_3.5-0 ; 
       altstealth-t2d18.4-0 ; 
       altstealth-t2d19.4-0 ; 
       altstealth-t2d23.3-0 ; 
       stealth_fig23-t2d1_4.1-0 ; 
       stealth_fig23-t2d24.3-0 ; 
       stealth_fig23-t2d25.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 8 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       9 9 300 ; 
       10 2 300 ; 
       11 10 300 ; 
       12 15 300 ; 
       13 16 300 ; 
       14 17 300 ; 
       16 14 300 ; 
       17 3 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 7 300 ; 
       21 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
       1 7 401 ; 
       2 8 401 ; 
       8 0 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 1 401 ; 
       15 10 401 ; 
       16 11 401 ; 
       17 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40.02666 0 0 USR SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       3 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 55 -2 0 MPRFLG 0 ; 
       11 SCHEM 10 -2 0 MPRFLG 0 ; 
       12 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 60 -2 0 MPRFLG 0 ; 
       14 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 50 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 66.52665 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 69.08525 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 71.82362 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 74.66186 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65.52665 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 73.66186 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 68.08525 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 70.82362 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 8 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
