SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       altstealth-cam_int1.1-0 ROOT ; 
       altstealth-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       altstealth-mat105.1-0 ; 
       altstealth-mat106.1-0 ; 
       altstealth-mat107.1-0 ; 
       altstealth-mat110.1-0 ; 
       altstealth-mat71.1-0 ; 
       altstealth-mat75.1-0 ; 
       altstealth-mat77.1-0 ; 
       altstealth-mat78.1-0 ; 
       altstealth-mat80.1-0 ; 
       altstealth-mat81.1-0 ; 
       altstealth-mat83.1-0 ; 
       altstealth-mat83_1.1-0 ; 
       altstealth-mat83_3.1-0 ; 
       altstealth-mat88.1-0 ; 
       altstealth-mat89.1-0 ; 
       altstealth-mat91.1-0 ; 
       edit_nulls-mat70_1.1-0 ; 
       stealth-mat111.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       altstealth-blthrust.2-0 ; 
       altstealth-brthrust.2-0 ; 
       altstealth-canopy_1_1.1-0 ROOT ; 
       altstealth-center-wing1.1-0 ; 
       altstealth-cockpt.2-0 ; 
       altstealth-lsmoke.2-0 ; 
       altstealth-lwepemt.2-0 ; 
       altstealth-missemt.2-0 ; 
       altstealth-rsmoke.2-0 ; 
       altstealth-rt-wing1.1-0 ; 
       altstealth-rt-wing1_1.1-0 ; 
       altstealth-rt-wing1_2.1-0 ; 
       altstealth-rt-wing12.1-0 ; 
       altstealth-rt-wing16.1-0 ; 
       altstealth-rt-wing8.1-0 ; 
       altstealth-rwepemt.2-0 ; 
       altstealth-SS01.2-0 ; 
       altstealth-SS02.2-0 ; 
       altstealth-SS03.2-0 ; 
       altstealth-SS04.2-0 ; 
       altstealth-SS05.2-0 ; 
       altstealth-SS06.2-0 ; 
       altstealth-tlthrust.2-0 ; 
       altstealth-trail.2-0 ; 
       altstealth-trthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig23/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-stealth.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       altstealth-back.1-0 ; 
       altstealth-Front.1-0 ; 
       altstealth-Rear_front.1-0 ; 
       altstealth-Side.1-0 ; 
       altstealth-t2d1.1-0 ; 
       altstealth-t2d1_1.1-0 ; 
       altstealth-t2d1_3.1-0 ; 
       altstealth-t2d18.1-0 ; 
       altstealth-t2d19.1-0 ; 
       altstealth-t2d20.1-0 ; 
       altstealth-t2d23.1-0 ; 
       stealth-t2d24.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 2 110 ; 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 17 300 ; 
       2 9 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       9 11 300 ; 
       10 10 300 ; 
       11 3 300 ; 
       12 12 300 ; 
       14 2 300 ; 
       16 16 300 ; 
       17 4 300 ; 
       18 6 300 ; 
       19 5 300 ; 
       20 8 300 ; 
       21 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       17 11 401 ; 
       0 7 401 ; 
       1 8 401 ; 
       2 9 401 ; 
       3 10 401 ; 
       9 0 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 1 401 ; 
       14 2 401 ; 
       15 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1158.418 28.46931 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1158.418 26.46931 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 1182.828 27.11607 0 MPRFLG 0 ; 
       0 SCHEM 1168.304 33.32911 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1170.711 33.42163 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1175.328 29.11607 0 USR SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       3 SCHEM 1172.828 27.11607 0 MPRFLG 0 ; 
       4 SCHEM 1163.897 33.85342 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1168.173 35.02541 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1174.264 34.37773 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1174.327 33.63754 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1170.673 35.02541 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1167.828 27.11607 0 MPRFLG 0 ; 
       10 SCHEM 1175.328 27.11607 0 MPRFLG 0 ; 
       11 SCHEM 1177.828 27.11607 0 MPRFLG 0 ; 
       12 SCHEM 1180.328 27.11607 0 MPRFLG 0 ; 
       14 SCHEM 1170.328 27.11607 0 MPRFLG 0 ; 
       15 SCHEM 1174.324 35.08709 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1168.55 38.08017 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 1168.363 37.27828 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 1173.488 38.23438 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 1173.363 37.27828 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 1180.831 38.23438 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 1180.863 37.27828 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 1168.364 34.19268 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1165.673 35.02541 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1170.799 34.22353 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       17 SCHEM 618.9916 16.90106 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 377.8074 -1.167717 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 380.3074 -1.167717 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 605.9562 5.782749 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 927.1212 -5.189892 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 150.3994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 155.3994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 157.8994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 162.8994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 160.3994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 171.8994 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 135.3994 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54.89941 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 871.777 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 173.3994 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 174.8994 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 176.3994 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 152.8994 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 170.8994 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 172.3994 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 173.8994 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 175.3994 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 135.3994 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54.89941 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 618.9916 14.90106 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 871.777 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 377.8074 -3.167717 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 380.3074 -3.167717 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 605.9562 3.782749 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 927.1212 -7.189892 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
