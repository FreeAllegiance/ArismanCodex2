SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       recovery_fig24-cam_int1.9-0 ROOT ; 
       recovery_fig24-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       recovery_fig24-mat100.2-0 ; 
       recovery_fig24-mat101.2-0 ; 
       recovery_fig24-mat102.2-0 ; 
       recovery_fig24-mat103.2-0 ; 
       recovery_fig24-mat104.2-0 ; 
       recovery_fig24-mat105.2-0 ; 
       recovery_fig24-mat106.1-0 ; 
       recovery_fig24-mat108.1-0 ; 
       recovery_fig24-mat109.1-0 ; 
       recovery_fig24-mat110.1-0 ; 
       recovery_fig24-mat88.1-0 ; 
       recovery_fig24-mat89.1-0 ; 
       recovery_fig24-mat90.1-0 ; 
       recovery_fig24-mat91.1-0 ; 
       recovery_fig24-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       recovery2-cyl10.1-0 ; 
       recovery2-cyl11.1-0 ; 
       recovery2-cyl5.1-0 ; 
       recovery2-cyl9.1-0 ; 
       recovery2-null1.1-0 ; 
       recovery2-sphere3.1-0 ; 
       recovery2-sphere5.1-0 ; 
       recovery2-sphere6.1-0 ; 
       recovery2-sphere7.1-0 ; 
       recovery2-zz_base_1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig24/PICTURES/fig24 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-recovery-fig24.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       recovery_fig24-back2.5-0 ; 
       recovery_fig24-Side2.5-0 ; 
       recovery_fig24-t2d10.2-0 ; 
       recovery_fig24-t2d16.5-0 ; 
       recovery_fig24-t2d17.5-0 ; 
       recovery_fig24-t2d18.5-0 ; 
       recovery_fig24-t2d19.5-0 ; 
       recovery_fig24-t2d20.5-0 ; 
       recovery_fig24-t2d21.1-0 ; 
       recovery_fig24-t2d22.1-0 ; 
       recovery_fig24-t2d23.1-0 ; 
       recovery_fig24-t2d24.1-0 ; 
       recovery_fig24-t2d7.2-0 ; 
       recovery_fig24-t2d8.2-0 ; 
       recovery_fig24-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 4 110 ; 
       8 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 12 300 ; 
       1 13 300 ; 
       2 10 300 ; 
       3 11 300 ; 
       5 6 300 ; 
       8 9 300 ; 
       9 14 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       6 7 300 ; 
       7 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 2 401 ; 
       14 0 401 ; 
       6 8 401 ; 
       8 10 401 ; 
       7 9 401 ; 
       9 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 60 -2 0 MPRFLG 0 ; 
       2 SCHEM 55 -2 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 45 -4 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 32.5 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 50 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 64 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 59 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 64 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
