SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       recovery-cam_int1.1-0 ROOT ; 
       recovery-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       edit_nulls-mat70.1-0 ; 
       recovery-mat100.1-0 ; 
       recovery-mat101.1-0 ; 
       recovery-mat102.1-0 ; 
       recovery-mat103.1-0 ; 
       recovery-mat104.1-0 ; 
       recovery-mat105.1-0 ; 
       recovery-mat71.1-0 ; 
       recovery-mat75.1-0 ; 
       recovery-mat77.1-0 ; 
       recovery-mat78.1-0 ; 
       recovery-mat80.1-0 ; 
       recovery-mat88.1-0 ; 
       recovery-mat89.1-0 ; 
       recovery-mat90.1-0 ; 
       recovery-mat91.1-0 ; 
       recovery-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       recovery2-bthrust.1-0 ; 
       recovery2-cockpt.1-0 ; 
       recovery2-cyl10.1-0 ; 
       recovery2-cyl11.1-0 ; 
       recovery2-cyl5.1-0 ; 
       recovery2-cyl9.1-0 ; 
       recovery2-lsmoke.1-0 ; 
       recovery2-lthrust.1-0 ; 
       recovery2-lwepemt.1-0 ; 
       recovery2-missemt.1-0 ; 
       recovery2-null1.1-0 ; 
       recovery2-rsmoke.1-0 ; 
       recovery2-rthrust.1-0 ; 
       recovery2-rwepemt.1-0 ; 
       recovery2-sphere1.1-0 ; 
       recovery2-sphere2.1-0 ; 
       recovery2-sphere3.1-0 ; 
       recovery2-sphere4.1-0 ; 
       recovery2-SS01.1-0 ; 
       recovery2-SS02.1-0 ; 
       recovery2-SS03.1-0 ; 
       recovery2-SS04.1-0 ; 
       recovery2-SS05.1-0 ; 
       recovery2-SS06.1-0 ; 
       recovery2-trail.1-0 ; 
       recovery2-tthrust.1-0 ; 
       recovery2-zz_base.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig24/PICTURES/fig21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-recovery.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       recovery-back2.1-0 ; 
       recovery-Side2.1-0 ; 
       recovery-t2d10.1-0 ; 
       recovery-t2d16.1-0 ; 
       recovery-t2d17.1-0 ; 
       recovery-t2d18.1-0 ; 
       recovery-t2d19.1-0 ; 
       recovery-t2d20.1-0 ; 
       recovery-t2d7.1-0 ; 
       recovery-t2d8.1-0 ; 
       recovery-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       21 26 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 26 110 ; 
       25 26 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 14 300 ; 
       3 15 300 ; 
       4 12 300 ; 
       5 13 300 ; 
       18 0 300 ; 
       19 7 300 ; 
       20 9 300 ; 
       21 8 300 ; 
       22 11 300 ; 
       23 10 300 ; 
       26 16 300 ; 
       26 1 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       26 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 2 401 ; 
       16 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 31.78894 -3.030184 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.73287 -0.6589372 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20.77306 -3.018219 0 USR MPRFLG 0 ; 
       3 SCHEM 23.23177 -2.89435 0 USR MPRFLG 0 ; 
       4 SCHEM 16.06382 -2.962977 0 MPRFLG 0 ; 
       5 SCHEM 18.56383 -2.962977 0 MPRFLG 0 ; 
       6 SCHEM 26.67727 -2.361048 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.62038 -3.033564 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 35.12887 1.213505 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 33.96167 0.2346996 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9.813818 -2.962977 0 MPRFLG 0 ; 
       11 SCHEM 29.05042 -2.392763 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29.05289 -2.948138 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34.97162 -0.7298562 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 3.157267 2.130449 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 3.197308 1.49086 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 5.476422 2.184135 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 5.368869 1.544546 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 7.856648 2.22613 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 7.826272 1.528195 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 27.68168 -1.298624 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.83833 -2.307505 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13.56382 -4.962976 0 MPRFLG 0 ; 
       15 SCHEM 6.06382 -4.962976 0 MPRFLG 0 ; 
       16 SCHEM 8.563818 -4.962976 0 MPRFLG 0 ; 
       17 SCHEM 11.06382 -4.962976 0 MPRFLG 0 ; 
       26 SCHEM 12.03528 8.228585 0 USR SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 99.02902 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 101.529 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 104.029 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 106.529 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 109.029 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 111.529 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 114.029 8.588675 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 99.02902 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 101.529 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 104.029 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 106.529 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 109.029 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 111.529 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 114.029 6.588675 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
