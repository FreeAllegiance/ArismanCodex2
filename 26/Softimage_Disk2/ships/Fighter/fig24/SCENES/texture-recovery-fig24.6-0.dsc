SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       recovery_fig24-cam_int1.6-0 ROOT ; 
       recovery_fig24-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       edit_nulls-mat70.3-0 ; 
       recovery_fig24-mat100.2-0 ; 
       recovery_fig24-mat101.2-0 ; 
       recovery_fig24-mat102.2-0 ; 
       recovery_fig24-mat103.2-0 ; 
       recovery_fig24-mat104.2-0 ; 
       recovery_fig24-mat105.2-0 ; 
       recovery_fig24-mat106.1-0 ; 
       recovery_fig24-mat108.1-0 ; 
       recovery_fig24-mat109.1-0 ; 
       recovery_fig24-mat110.1-0 ; 
       recovery_fig24-mat71.1-0 ; 
       recovery_fig24-mat75.1-0 ; 
       recovery_fig24-mat77.1-0 ; 
       recovery_fig24-mat78.1-0 ; 
       recovery_fig24-mat80.1-0 ; 
       recovery_fig24-mat88.1-0 ; 
       recovery_fig24-mat89.1-0 ; 
       recovery_fig24-mat90.1-0 ; 
       recovery_fig24-mat91.1-0 ; 
       recovery_fig24-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       recovery2-bthrust.1-0 ; 
       recovery2-cockpt.1-0 ; 
       recovery2-cyl10.1-0 ; 
       recovery2-cyl11.1-0 ; 
       recovery2-cyl5.1-0 ; 
       recovery2-cyl9.1-0 ; 
       recovery2-lsmoke.1-0 ; 
       recovery2-lthrust.1-0 ; 
       recovery2-lwepemt.1-0 ; 
       recovery2-missemt.1-0 ; 
       recovery2-null1.1-0 ; 
       recovery2-rsmoke.1-0 ; 
       recovery2-rthrust.1-0 ; 
       recovery2-rwepemt.1-0 ; 
       recovery2-sphere3.1-0 ; 
       recovery2-sphere5.1-0 ; 
       recovery2-sphere6.1-0 ; 
       recovery2-sphere7.1-0 ; 
       recovery2-SS01.1-0 ; 
       recovery2-SS02.1-0 ; 
       recovery2-SS03.1-0 ; 
       recovery2-SS04.1-0 ; 
       recovery2-SS05.1-0 ; 
       recovery2-SS06.1-0 ; 
       recovery2-trail.1-0 ; 
       recovery2-tthrust.1-0 ; 
       recovery2-zz_base_1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig24/PICTURES/fig24 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-recovery-fig24.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       recovery_fig24-back2.4-0 ; 
       recovery_fig24-Side2.4-0 ; 
       recovery_fig24-t2d10.2-0 ; 
       recovery_fig24-t2d16.4-0 ; 
       recovery_fig24-t2d17.4-0 ; 
       recovery_fig24-t2d18.4-0 ; 
       recovery_fig24-t2d19.4-0 ; 
       recovery_fig24-t2d20.4-0 ; 
       recovery_fig24-t2d21.1-0 ; 
       recovery_fig24-t2d22.1-0 ; 
       recovery_fig24-t2d23.1-0 ; 
       recovery_fig24-t2d24.1-0 ; 
       recovery_fig24-t2d7.2-0 ; 
       recovery_fig24-t2d8.2-0 ; 
       recovery_fig24-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       14 10 110 ; 
       17 10 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       21 26 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 26 110 ; 
       25 26 110 ; 
       15 10 110 ; 
       16 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 18 300 ; 
       3 19 300 ; 
       4 16 300 ; 
       5 17 300 ; 
       14 7 300 ; 
       17 10 300 ; 
       18 0 300 ; 
       19 11 300 ; 
       20 13 300 ; 
       21 12 300 ; 
       22 15 300 ; 
       23 14 300 ; 
       26 20 300 ; 
       26 1 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       26 5 300 ; 
       26 6 300 ; 
       15 8 300 ; 
       16 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 2 401 ; 
       20 0 401 ; 
       7 8 401 ; 
       9 10 401 ; 
       8 9 401 ; 
       10 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 60 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 48.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 45 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 52.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 10 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 15 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 35 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 41.25 0 0 DISPLAY 1 2 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 50 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 67.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 70 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 75 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 15 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 55 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 62.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 57.5 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 60 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 80 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 45 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 65 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 67.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 70 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 72.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 75 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 77.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 55 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 62.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 57.5 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
