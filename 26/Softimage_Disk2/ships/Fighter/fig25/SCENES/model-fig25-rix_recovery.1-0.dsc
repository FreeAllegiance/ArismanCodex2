SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.3-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       fig25_rix_recovery-face4.1-0 ; 
       fig25_rix_recovery-face6.1-0 ; 
       fig25_rix_recovery-fuselg1.3-0 ROOT ; 
       fig25_rix_recovery-fuselg5.1-0 ; 
       fig25_rix_recovery-fuselg7.1-0 ; 
       fig25_rix_recovery-lwingzz1.1-0 ; 
       fig25_rix_recovery-lwingzz2.1-0 ; 
       fig25_rix_recovery-lwingzz8.1-0 ; 
       fig25_rix_recovery-lwingzz9.1-0 ; 
       fig25_rix_recovery-poly_arm2.1-0 ; 
       fig25_rix_recovery-poly_arm4.1-0 ; 
       fig25_rix_recovery-SSl.1-0 ; 
       fig25_rix_recovery-SSl3.1-0 ; 
       fig25_rix_recovery-top_fin3.1-0 ; 
       fig25_rix_recovery-top_fin4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig25-rix_recovery.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       9 2 110 ; 
       11 6 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       10 2 110 ; 
       1 10 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       12 8 110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10.3118 -0.2649742 0 MPRFLG 0 ; 
       2 SCHEM 11.5618 3.735026 0 USR SRT 1 1 1 0 0 0 0 0.2432211 -0.8185986 MPRFLG 0 ; 
       3 SCHEM 12.8118 1.735026 0 MPRFLG 0 ; 
       4 SCHEM 15.3118 1.735026 0 MPRFLG 0 ; 
       5 SCHEM 2.811803 1.735026 0 MPRFLG 0 ; 
       6 SCHEM 2.811803 -0.2649742 0 MPRFLG 0 ; 
       9 SCHEM 10.3118 1.735026 0 MPRFLG 0 ; 
       11 SCHEM 2.811803 -2.264973 0 MPRFLG 0 ; 
       13 SCHEM 5.311803 1.735026 0 MPRFLG 0 ; 
       14 SCHEM 7.811802 1.735026 0 MPRFLG 0 ; 
       10 SCHEM 17.8118 1.735026 0 MPRFLG 0 ; 
       1 SCHEM 17.8118 -0.2649742 0 MPRFLG 0 ; 
       7 SCHEM 20.3118 1.735026 0 MPRFLG 0 ; 
       8 SCHEM 20.3118 -0.2649742 0 MPRFLG 0 ; 
       12 SCHEM 20.3118 -2.264973 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
