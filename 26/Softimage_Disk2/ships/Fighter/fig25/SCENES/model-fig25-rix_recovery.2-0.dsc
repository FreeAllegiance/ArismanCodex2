SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig25_rix_recovery-cam_int1.6-0 ROOT ; 
       fig25_rix_recovery-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       edit_nulls-mat70.1-0 ; 
       fig25_rix_recovery-mat1.3-0 ; 
       fig25_rix_recovery-mat10.1-0 ; 
       fig25_rix_recovery-mat11.1-0 ; 
       fig25_rix_recovery-mat12.1-0 ; 
       fig25_rix_recovery-mat13.2-0 ; 
       fig25_rix_recovery-mat14.1-0 ; 
       fig25_rix_recovery-mat17.1-0 ; 
       fig25_rix_recovery-mat18.1-0 ; 
       fig25_rix_recovery-mat19.1-0 ; 
       fig25_rix_recovery-mat2.3-0 ; 
       fig25_rix_recovery-mat20.1-0 ; 
       fig25_rix_recovery-mat21.1-0 ; 
       fig25_rix_recovery-mat22.1-0 ; 
       fig25_rix_recovery-mat23.1-0 ; 
       fig25_rix_recovery-mat24.1-0 ; 
       fig25_rix_recovery-mat25.1-0 ; 
       fig25_rix_recovery-mat26.1-0 ; 
       fig25_rix_recovery-mat27.1-0 ; 
       fig25_rix_recovery-mat3.1-0 ; 
       fig25_rix_recovery-mat7.1-0 ; 
       fig25_rix_recovery-mat71.1-0 ; 
       fig25_rix_recovery-mat75.1-0 ; 
       fig25_rix_recovery-mat77.1-0 ; 
       fig25_rix_recovery-mat78.1-0 ; 
       fig25_rix_recovery-mat8.1-0 ; 
       fig25_rix_recovery-mat80.1-0 ; 
       fig25_rix_recovery-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       fig25-cockpt.1-0 ; 
       fig25-face4.1-0 ; 
       fig25-face8.1-0 ; 
       fig25-fig25.2-0 ROOT ; 
       fig25-fuselg5.1-0 ; 
       fig25-fuselg8.1-0 ; 
       fig25-lsmoke.1-0 ; 
       fig25-lthrust.1-0 ; 
       fig25-lwepemt.1-0 ; 
       fig25-lwingzz1.1-0 ; 
       fig25-lwingzz2.1-0 ; 
       fig25-lwingzz5.1-0 ; 
       fig25-lwingzz6.1-0 ; 
       fig25-missemt.1-0 ; 
       fig25-poly_arm2.1-0 ; 
       fig25-poly_arm6.1-0 ; 
       fig25-rsmoke.1-0 ; 
       fig25-rthrust.1-0 ; 
       fig25-rwepemt.1-0 ; 
       fig25-SS01.1-0 ; 
       fig25-SS02.1-0 ; 
       fig25-SS03.1-0 ; 
       fig25-SS04.1-0 ; 
       fig25-SS05.1-0 ; 
       fig25-SS06.1-0 ; 
       fig25-top_fin3.1-0 ; 
       fig25-top_fin5.1-0 ; 
       fig25-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig25 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig25-rix_recovery.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       fig25_rix_recovery-t2d1.5-0 ; 
       fig25_rix_recovery-t2d12.3-0 ; 
       fig25_rix_recovery-t2d13.3-0 ; 
       fig25_rix_recovery-t2d14.3-0 ; 
       fig25_rix_recovery-t2d15.3-0 ; 
       fig25_rix_recovery-t2d16.3-0 ; 
       fig25_rix_recovery-t2d17.3-0 ; 
       fig25_rix_recovery-t2d18.3-0 ; 
       fig25_rix_recovery-t2d19.3-0 ; 
       fig25_rix_recovery-t2d2.3-0 ; 
       fig25_rix_recovery-t2d20.3-0 ; 
       fig25_rix_recovery-t2d21.3-0 ; 
       fig25_rix_recovery-t2d22.3-0 ; 
       fig25_rix_recovery-t2d6.3-0 ; 
       fig25_rix_recovery-t2d7.3-0 ; 
       fig25_rix_recovery-t2d8.4-0 ; 
       fig25_rix_recovery-t2d9.4-0 ; 
       fig25_rix_recovery-zt2d1.3-0 ; 
       fig25_rix_recovery-zt2d2.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 14 110 ; 
       2 15 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 9 110 ; 
       11 3 110 ; 
       12 11 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 3 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 19 300 ; 
       2 18 300 ; 
       3 1 300 ; 
       3 10 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       4 20 300 ; 
       4 25 300 ; 
       4 27 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       9 5 300 ; 
       9 12 300 ; 
       10 6 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       12 15 300 ; 
       14 16 300 ; 
       15 17 300 ; 
       19 0 300 ; 
       20 21 300 ; 
       21 23 300 ; 
       22 22 300 ; 
       23 26 300 ; 
       24 24 300 ; 
       25 9 300 ; 
       26 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 14 401 ; 
       4 18 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 0 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 9 401 ; 
       25 13 401 ; 
       27 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.71381 -7.808204 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.36355 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 24.86354 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 16.11355 3.686334 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 17.36354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 19.86354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 16.65821 -9.510314 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 16.60132 -10.18283 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 25.1098 -5.935764 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 14.86355 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 14.86355 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 22.36354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 22.36354 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 23.9426 -6.914569 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 12.36355 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 24.86354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 19.03136 -9.542028 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 19.03383 -10.0974 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 24.95255 -7.879122 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 8.835515 -7.764554 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 8.875556 -8.404141 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 11.15467 -7.710868 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 11.04712 -8.350455 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 13.5349 -7.668873 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 13.50452 -8.366807 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 7.363549 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 9.863548 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 17.66262 -8.44789 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40.86491 -0.1987019 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 50.62887 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 60.62887 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60.62887 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 43.72167 -2.842311 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 43.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 58.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 58.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 58.12887 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 48.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 59.09406 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 46.59406 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 22 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 37 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 43.48301 -4.565022 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60.62887 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60.62887 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 43.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 58.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 58.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 58.12887 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.49086 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 48.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 58.99087 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 53.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 50.62887 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 53.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
