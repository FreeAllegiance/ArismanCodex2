SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig25_rix_recovery-cam_int1.7-0 ROOT ; 
       fig25_rix_recovery-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       fig25_rix_recovery-mat1.3-0 ; 
       fig25_rix_recovery-mat10.1-0 ; 
       fig25_rix_recovery-mat11.1-0 ; 
       fig25_rix_recovery-mat12.1-0 ; 
       fig25_rix_recovery-mat13.2-0 ; 
       fig25_rix_recovery-mat14.1-0 ; 
       fig25_rix_recovery-mat17.1-0 ; 
       fig25_rix_recovery-mat18.1-0 ; 
       fig25_rix_recovery-mat19.1-0 ; 
       fig25_rix_recovery-mat2.3-0 ; 
       fig25_rix_recovery-mat20.1-0 ; 
       fig25_rix_recovery-mat21.1-0 ; 
       fig25_rix_recovery-mat22.1-0 ; 
       fig25_rix_recovery-mat23.1-0 ; 
       fig25_rix_recovery-mat24.1-0 ; 
       fig25_rix_recovery-mat25.1-0 ; 
       fig25_rix_recovery-mat26.1-0 ; 
       fig25_rix_recovery-mat27.1-0 ; 
       fig25_rix_recovery-mat3.1-0 ; 
       fig25_rix_recovery-mat7.1-0 ; 
       fig25_rix_recovery-mat8.1-0 ; 
       fig25_rix_recovery-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       fig25-face4.1-0 ; 
       fig25-face8.1-0 ; 
       fig25-fig25.3-0 ROOT ; 
       fig25-fuselg5.1-0 ; 
       fig25-fuselg8.1-0 ; 
       fig25-lwingzz1.1-0 ; 
       fig25-lwingzz2.1-0 ; 
       fig25-lwingzz5.1-0 ; 
       fig25-lwingzz6.1-0 ; 
       fig25-poly_arm2.1-0 ; 
       fig25-poly_arm6.1-0 ; 
       fig25-top_fin3.1-0 ; 
       fig25-top_fin5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig25 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-fig25-rix_recovery.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       fig25_rix_recovery-t2d1.5-0 ; 
       fig25_rix_recovery-t2d12.3-0 ; 
       fig25_rix_recovery-t2d13.3-0 ; 
       fig25_rix_recovery-t2d14.3-0 ; 
       fig25_rix_recovery-t2d15.3-0 ; 
       fig25_rix_recovery-t2d16.3-0 ; 
       fig25_rix_recovery-t2d17.3-0 ; 
       fig25_rix_recovery-t2d18.3-0 ; 
       fig25_rix_recovery-t2d19.3-0 ; 
       fig25_rix_recovery-t2d2.3-0 ; 
       fig25_rix_recovery-t2d20.3-0 ; 
       fig25_rix_recovery-t2d21.3-0 ; 
       fig25_rix_recovery-t2d22.3-0 ; 
       fig25_rix_recovery-t2d6.3-0 ; 
       fig25_rix_recovery-t2d7.3-0 ; 
       fig25_rix_recovery-t2d8.4-0 ; 
       fig25_rix_recovery-t2d9.4-0 ; 
       fig25_rix_recovery-zt2d1.3-0 ; 
       fig25_rix_recovery-zt2d2.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 10 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       1 17 300 ; 
       2 0 300 ; 
       2 9 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       5 11 300 ; 
       6 5 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       8 14 300 ; 
       9 15 300 ; 
       10 16 300 ; 
       11 8 300 ; 
       12 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 14 401 ; 
       3 18 401 ; 
       4 15 401 ; 
       5 16 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 0 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       18 9 401 ; 
       20 13 401 ; 
       21 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.36355 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 24.86354 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 16.11355 3.686334 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 17.36354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 19.86354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 14.86355 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 14.86355 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.36354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 22.36354 -0.3136656 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.36355 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 24.86354 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7.363549 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 9.863548 1.686334 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40.86491 -0.1987019 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 53.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50.62887 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60.62887 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 60.62887 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 43.72167 -2.842311 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 43.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 58.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 58.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 58.12887 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 48.12887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 59.09406 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 46.59406 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 50.62887 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 43.48301 -4.565022 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60.62887 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60.62887 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 43.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 58.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 58.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 58.12887 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.49086 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 48.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 58.99087 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 53.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 50.62887 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 50.62887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 53.12887 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
