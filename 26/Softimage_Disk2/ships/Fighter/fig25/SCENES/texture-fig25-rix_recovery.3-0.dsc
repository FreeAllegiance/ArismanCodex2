SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig25_rix_recovery-cam_int1.2-0 ROOT ; 
       fig25_rix_recovery-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       fig25_rix_recovery-mat1.2-0 ; 
       fig25_rix_recovery-mat10.1-0 ; 
       fig25_rix_recovery-mat11.1-0 ; 
       fig25_rix_recovery-mat12.1-0 ; 
       fig25_rix_recovery-mat13.1-0 ; 
       fig25_rix_recovery-mat14.1-0 ; 
       fig25_rix_recovery-mat15.1-0 ; 
       fig25_rix_recovery-mat16.1-0 ; 
       fig25_rix_recovery-mat2.2-0 ; 
       fig25_rix_recovery-mat3.1-0 ; 
       fig25_rix_recovery-mat4.1-0 ; 
       fig25_rix_recovery-mat5.1-0 ; 
       fig25_rix_recovery-mat6.1-0 ; 
       fig25_rix_recovery-mat7.1-0 ; 
       fig25_rix_recovery-mat8.1-0 ; 
       fig25_rix_recovery-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       aa_rix-face4.1-0 ; 
       aa_rix-face7.1-0 ; 
       aa_rix-fuselg5.1-0 ; 
       aa_rix-fuselg8.1-0 ; 
       aa_rix-lwingzz1.1-0 ; 
       aa_rix-lwingzz10.1-0 ; 
       aa_rix-lwingzz11.1-0 ; 
       aa_rix-lwingzz2.1-0 ; 
       aa_rix-poly_arm2.1-0 ; 
       aa_rix-poly_arm5.1-0 ; 
       aa_rix-recovery.3-0 ROOT ; 
       aa_rix-SSl.1-0 ; 
       aa_rix-SSl4.1-0 ; 
       aa_rix-top_fin3.1-0 ; 
       aa_rix-top_fin4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig25 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig25-rix_recovery.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       fig25_rix_recovery-t2d1.2-0 ; 
       fig25_rix_recovery-t2d10.1-0 ; 
       fig25_rix_recovery-t2d11.1-0 ; 
       fig25_rix_recovery-t2d2.1-0 ; 
       fig25_rix_recovery-t2d3.1-0 ; 
       fig25_rix_recovery-t2d4.1-0 ; 
       fig25_rix_recovery-t2d5.1-0 ; 
       fig25_rix_recovery-t2d6.1-0 ; 
       fig25_rix_recovery-t2d7.1-0 ; 
       fig25_rix_recovery-t2d8.1-0 ; 
       fig25_rix_recovery-t2d9.1-0 ; 
       fig25_rix_recovery-zt2d1.1-0 ; 
       fig25_rix_recovery-zt2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 10 110 ; 
       7 4 110 ; 
       11 7 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       8 10 110 ; 
       0 8 110 ; 
       2 10 110 ; 
       5 10 110 ; 
       3 10 110 ; 
       6 5 110 ; 
       9 10 110 ; 
       1 9 110 ; 
       12 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 0 300 ; 
       10 8 300 ; 
       4 4 300 ; 
       7 5 300 ; 
       8 10 300 ; 
       0 9 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       5 6 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       6 7 300 ; 
       9 11 300 ; 
       1 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 0 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 11 401 ; 
       2 8 401 ; 
       3 12 401 ; 
       4 9 401 ; 
       5 10 401 ; 
       6 1 401 ; 
       7 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 24.80204 11.99742 0 USR SRT 1 1 1 0 0 0 0 0.2432211 -0.8185986 MPRFLG 0 ; 
       4 SCHEM 29.83583 -10.4169 0 USR MPRFLG 0 ; 
       7 SCHEM 28.58583 -12.4169 0 MPRFLG 0 ; 
       11 SCHEM 27.33583 -14.4169 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 5 -2 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 37.33583 -2 0 MPRFLG 0 ; 
       5 SCHEM 52.5 4 0 USR MPRFLG 0 ; 
       3 SCHEM 44.83583 -2 0 MPRFLG 0 ; 
       6 SCHEM 51.25 2 0 MPRFLG 0 ; 
       9 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 50 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.63471 1.991419 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.54928 1.449375 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9.965193 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14.96519 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34.83583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 37.33583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39.83583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.33583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 44.83583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.33583 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.33583 -12.4169 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29.83583 -14.4169 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 52.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 37.45073 0.2870796 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9.861996 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14.862 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 37.33583 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39.83583 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 44.83583 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.33583 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 32.33583 -14.4169 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29.83583 -16.4169 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
