SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig25_rix_recovery-cam_int1.4-0 ROOT ; 
       fig25_rix_recovery-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       fig25_rix_recovery-mat1.3-0 ; 
       fig25_rix_recovery-mat10.1-0 ; 
       fig25_rix_recovery-mat11.1-0 ; 
       fig25_rix_recovery-mat12.1-0 ; 
       fig25_rix_recovery-mat13.2-0 ; 
       fig25_rix_recovery-mat14.1-0 ; 
       fig25_rix_recovery-mat15.2-0 ; 
       fig25_rix_recovery-mat16.2-0 ; 
       fig25_rix_recovery-mat17.1-0 ; 
       fig25_rix_recovery-mat18.1-0 ; 
       fig25_rix_recovery-mat19.1-0 ; 
       fig25_rix_recovery-mat2.3-0 ; 
       fig25_rix_recovery-mat20.1-0 ; 
       fig25_rix_recovery-mat21.1-0 ; 
       fig25_rix_recovery-mat22.1-0 ; 
       fig25_rix_recovery-mat23.1-0 ; 
       fig25_rix_recovery-mat24.1-0 ; 
       fig25_rix_recovery-mat25.1-0 ; 
       fig25_rix_recovery-mat26.1-0 ; 
       fig25_rix_recovery-mat27.1-0 ; 
       fig25_rix_recovery-mat3.1-0 ; 
       fig25_rix_recovery-mat5.1-0 ; 
       fig25_rix_recovery-mat6.1-0 ; 
       fig25_rix_recovery-mat7.1-0 ; 
       fig25_rix_recovery-mat8.1-0 ; 
       fig25_rix_recovery-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       aa_rix-face4.1-0 ; 
       aa_rix-face7.1-0 ; 
       aa_rix-face8.1-0 ; 
       aa_rix-fuselg5.1-0 ; 
       aa_rix-fuselg8.1-0 ; 
       aa_rix-lwingzz1.1-0 ; 
       aa_rix-lwingzz2.1-0 ; 
       aa_rix-lwingzz3.2-0 ROOT ; 
       aa_rix-lwingzz4.1-0 ; 
       aa_rix-lwingzz5.1-0 ; 
       aa_rix-lwingzz6.1-0 ; 
       aa_rix-poly_arm2.1-0 ; 
       aa_rix-poly_arm5.1-0 ROOT ; 
       aa_rix-poly_arm6.1-0 ; 
       aa_rix-recovery.5-0 ROOT ; 
       aa_rix-SSl.1-0 ; 
       aa_rix-SSl1.1-0 ; 
       aa_rix-SSl2.1-0 ; 
       aa_rix-top_fin3.1-0 ; 
       aa_rix-top_fin4.2-0 ROOT ; 
       aa_rix-top_fin5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig25 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig25-rix_recovery.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       fig25_rix_recovery-t2d1.3-0 ; 
       fig25_rix_recovery-t2d10.2-0 ; 
       fig25_rix_recovery-t2d11.2-0 ; 
       fig25_rix_recovery-t2d12.1-0 ; 
       fig25_rix_recovery-t2d13.1-0 ; 
       fig25_rix_recovery-t2d14.1-0 ; 
       fig25_rix_recovery-t2d15.1-0 ; 
       fig25_rix_recovery-t2d16.1-0 ; 
       fig25_rix_recovery-t2d17.1-0 ; 
       fig25_rix_recovery-t2d18.1-0 ; 
       fig25_rix_recovery-t2d19.1-0 ; 
       fig25_rix_recovery-t2d2.1-0 ; 
       fig25_rix_recovery-t2d20.1-0 ; 
       fig25_rix_recovery-t2d21.1-0 ; 
       fig25_rix_recovery-t2d22.1-0 ; 
       fig25_rix_recovery-t2d4.1-0 ; 
       fig25_rix_recovery-t2d5.1-0 ; 
       fig25_rix_recovery-t2d6.1-0 ; 
       fig25_rix_recovery-t2d7.1-0 ; 
       fig25_rix_recovery-t2d8.2-0 ; 
       fig25_rix_recovery-t2d9.2-0 ; 
       fig25_rix_recovery-zt2d1.1-0 ; 
       fig25_rix_recovery-zt2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 12 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 5 110 ; 
       8 7 110 ; 
       9 14 110 ; 
       10 9 110 ; 
       11 14 110 ; 
       15 6 110 ; 
       16 8 110 ; 
       17 10 110 ; 
       18 14 110 ; 
       20 14 110 ; 
       13 14 110 ; 
       2 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 20 300 ; 
       1 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       5 13 300 ; 
       6 5 300 ; 
       7 6 300 ; 
       8 7 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       10 16 300 ; 
       11 17 300 ; 
       12 21 300 ; 
       14 0 300 ; 
       14 11 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       18 10 300 ; 
       20 12 300 ; 
       13 18 300 ; 
       2 19 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 18 401 ; 
       3 22 401 ; 
       4 19 401 ; 
       5 20 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 0 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       20 11 401 ; 
       17 12 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       24 17 401 ; 
       25 21 401 ; 
       18 13 401 ; 
       19 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 -1.894602 0.2432216 -0.9174227 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 25 0 0 DISPLAY 0 0 SRT 1.473516 1.473516 1.550139 0 3.141593 0.3 0.07295833 0.405457 -1.27268 MPRFLG 0 ; 
       14 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0.2432211 -0.8185986 MPRFLG 0 ; 
       15 SCHEM 10 -6 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 27.5 0 0 DISPLAY 0 0 SRT 0.8785285 0.8785285 0.8785285 3.321592 0 0 2.504207e-015 -0.2360523 -0.8157877 MPRFLG 0 ; 
       20 SCHEM 5 -2 0 MPRFLG 0 ; 
       13 SCHEM 20 -2 0 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.736042 -0.1987019 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4.5928 -2.842311 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.465193 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9.190088 -4.077496 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19.96519 -5.578284 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4.354137 -4.565022 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 7.361996 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9.086891 -5.458786 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19.862 -6.959574 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
