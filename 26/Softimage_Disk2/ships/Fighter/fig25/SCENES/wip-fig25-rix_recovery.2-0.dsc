SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_5.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_8.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.2-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       fig25_rix_recovery-default2.1-0 ; 
       fig25_rix_recovery-default6.1-0 ; 
       fig25_rix_recovery-default8.1-0 ; 
       fig25_rix_recovery-mat113.1-0 ; 
       fig25_rix_recovery-mat149.1-0 ; 
       fig25_rix_recovery-mat150.1-0 ; 
       fig25_rix_recovery-mat151.1-0 ; 
       fig25_rix_recovery-mat152.1-0 ; 
       fig25_rix_recovery-mat153.1-0 ; 
       fig25_rix_recovery-mat154.1-0 ; 
       fig25_rix_recovery-mat155.1-0 ; 
       fig25_rix_recovery-mat156.1-0 ; 
       fig25_rix_recovery-mat157.1-0 ; 
       fig25_rix_recovery-mat167.1-0 ; 
       fig25_rix_recovery-mat168.1-0 ; 
       fig25_rix_recovery-mat169.1-0 ; 
       fig25_rix_recovery-mat170.1-0 ; 
       fig25_rix_recovery-mat171.1-0 ; 
       fig25_rix_recovery-mat172.1-0 ; 
       fig25_rix_recovery-mat173.1-0 ; 
       fig25_rix_recovery-mat174.1-0 ; 
       fig25_rix_recovery-mat175.1-0 ; 
       fig25_rix_recovery-mat176.1-0 ; 
       fig25_rix_recovery-mat177.1-0 ; 
       fig25_rix_recovery-mat178.1-0 ; 
       fig25_rix_recovery-mat179.1-0 ; 
       fig25_rix_recovery-mat180.1-0 ; 
       fig25_rix_recovery-mat181.1-0 ; 
       fig25_rix_recovery-mat182.1-0 ; 
       fig25_rix_recovery-mat183.1-0 ; 
       fig25_rix_recovery-mat74.1-0 ; 
       fig25_rix_recovery-mat90.1-0 ; 
       fig25_rix_recovery-mat91.1-0 ; 
       fig25_rix_recovery-mat92.1-0 ; 
       fig25_rix_recovery-mat93.1-0 ; 
       fig25_rix_recovery-mat94.1-0 ; 
       fig25_rix_recovery-mat95.1-0 ; 
       fig25_rix_recovery-mat96.1-0 ; 
       fig25_rix_recovery-mat97.1-0 ; 
       fig25_rix_recovery-mat98.1-0 ; 
       fig25_rix_recovery-OTHERS-rubber.1-10.1-0 ; 
       fig25_rix_recovery-OTHERS-rubber.1-6.1-0 ; 
       fig25_rix_recovery-OTHERS-rubber.1-7.1-0 ; 
       fig25_rix_recovery-OTHERS-rubber.1-8.1-0 ; 
       rix_fighter_sPt-mat54.1-0 ; 
       rix_fighter_sPt-mat55.1-0 ; 
       rix_fighter_sPt-mat56.1-0 ; 
       rix_fighter_sPt-mat57.1-0 ; 
       rix_fighter_sPt-mat58.1-0 ; 
       rix_fighter_sPt-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       fig25_rix_recovery-face4.1-0 ; 
       fig25_rix_recovery-face6.1-0 ; 
       fig25_rix_recovery-fuselg1.2-0 ROOT ; 
       fig25_rix_recovery-fuselg5.1-0 ; 
       fig25_rix_recovery-fuselg7.1-0 ; 
       fig25_rix_recovery-lwingzz1.1-0 ; 
       fig25_rix_recovery-lwingzz2.1-0 ; 
       fig25_rix_recovery-lwingzz8.1-0 ; 
       fig25_rix_recovery-lwingzz9.1-0 ; 
       fig25_rix_recovery-poly_arm2.1-0 ; 
       fig25_rix_recovery-poly_arm4.1-0 ; 
       fig25_rix_recovery-SSl.1-0 ; 
       fig25_rix_recovery-SSl3.1-0 ; 
       fig25_rix_recovery-top_fin3.1-0 ; 
       fig25_rix_recovery-top_fin4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig04 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig25/PICTURES/fig15 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig25-rix_recovery.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       fig25_rix_recovery-t2d132.1-0 ; 
       fig25_rix_recovery-t2d133.1-0 ; 
       fig25_rix_recovery-t2d134.1-0 ; 
       fig25_rix_recovery-t2d135.1-0 ; 
       fig25_rix_recovery-t2d136.1-0 ; 
       fig25_rix_recovery-t2d137.1-0 ; 
       fig25_rix_recovery-t2d138.1-0 ; 
       fig25_rix_recovery-t2d139.1-0 ; 
       fig25_rix_recovery-t2d148.1-0 ; 
       fig25_rix_recovery-t2d149.1-0 ; 
       fig25_rix_recovery-t2d150.1-0 ; 
       fig25_rix_recovery-t2d151.1-0 ; 
       fig25_rix_recovery-t2d152.1-0 ; 
       fig25_rix_recovery-t2d153.1-0 ; 
       fig25_rix_recovery-t2d154.1-0 ; 
       fig25_rix_recovery-t2d155.1-0 ; 
       fig25_rix_recovery-t2d156.1-0 ; 
       fig25_rix_recovery-t2d157.1-0 ; 
       fig25_rix_recovery-t2d158.1-0 ; 
       fig25_rix_recovery-t2d159.1-0 ; 
       fig25_rix_recovery-t2d160.1-0 ; 
       fig25_rix_recovery-t2d161.1-0 ; 
       fig25_rix_recovery-t2d162.1-0 ; 
       fig25_rix_recovery-t2d163.1-0 ; 
       fig25_rix_recovery-t2d164.1-0 ; 
       fig25_rix_recovery-t2d165.1-0 ; 
       fig25_rix_recovery-t2d72.1-0 ; 
       fig25_rix_recovery-t2d73.1-0 ; 
       fig25_rix_recovery-t2d74.1-0 ; 
       fig25_rix_recovery-t2d75.1-0 ; 
       fig25_rix_recovery-t2d76.1-0 ; 
       fig25_rix_recovery-t2d77.1-0 ; 
       fig25_rix_recovery-t2d78.1-0 ; 
       fig25_rix_recovery-t2d79.1-0 ; 
       fig25_rix_recovery-t2d94.1-0 ; 
       fig25_rix_recovery-t2d95.1-0 ; 
       fig25_rix_recovery-t2d96.1-0 ; 
       fig25_rix_recovery-t2d97.1-0 ; 
       rix_fighter_sPt-t2d49.1-0 ; 
       rix_fighter_sPt-t2d50.1-0 ; 
       rix_fighter_sPt-t2d51.1-0 ; 
       rix_fighter_sPt-t2d52.1-0 ; 
       rix_fighter_sPt-t2d53.1-0 ; 
       rix_fighter_sPt-t2d54.1-0 ; 
       rix_fighter_sPt-t2d55.1-0 ; 
       rix_fighter_sPt-t2d58.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 5 110 ; 
       9 2 110 ; 
       11 6 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       10 2 110 ; 
       1 10 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       12 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       2 31 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 36 300 ; 
       2 0 300 ; 
       2 37 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 1 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 2 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       5 46 300 ; 
       5 47 300 ; 
       5 48 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 49 300 ; 
       9 41 300 ; 
       11 30 300 ; 
       13 42 300 ; 
       14 43 300 ; 
       10 40 300 ; 
       1 22 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       8 28 300 ; 
       12 29 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 26 400 ; 
       3 0 400 ; 
       4 8 400 ; 
       5 45 400 ; 
       6 44 400 ; 
       7 18 400 ; 
       8 22 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 35 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 9 401 ; 
       15 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       31 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       41 34 401 ; 
       42 36 401 ; 
       43 37 401 ; 
       44 38 401 ; 
       45 39 401 ; 
       46 40 401 ; 
       47 41 401 ; 
       48 42 401 ; 
       49 43 401 ; 
       40 16 401 ; 
       22 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1.794487 0.3712731 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 SRT 1 1 1 0 0 0 0 0.2432211 -0.8185986 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 0 -4 0 MPRFLG 0 ; 
       6 SCHEM 0 -6 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 0 -8 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       18 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 180.5 2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 453 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 71 71 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
