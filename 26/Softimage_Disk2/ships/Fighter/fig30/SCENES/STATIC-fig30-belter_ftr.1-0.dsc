SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.56-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       fig30_belter_ftr-mat100.3-0 ; 
       fig30_belter_ftr-mat101.2-0 ; 
       fig30_belter_ftr-mat102.2-0 ; 
       fig30_belter_ftr-mat103.2-0 ; 
       fig30_belter_ftr-mat105.3-0 ; 
       fig30_belter_ftr-mat106.3-0 ; 
       fig30_belter_ftr-mat113.2-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat116.1-0 ; 
       fig30_belter_ftr-mat117.1-0 ; 
       fig30_belter_ftr-mat118.1-0 ; 
       fig30_belter_ftr-mat119.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat121.1-0 ; 
       fig30_belter_ftr-mat122.1-0 ; 
       fig30_belter_ftr-mat83.5-0 ; 
       fig30_belter_ftr-mat84.4-0 ; 
       fig30_belter_ftr-mat87.2-0 ; 
       fig30_belter_ftr-mat88.3-0 ; 
       fig30_belter_ftr-mat89.3-0 ; 
       fig30_belter_ftr-mat90.3-0 ; 
       fig30_belter_ftr-mat91.4-0 ; 
       fig30_belter_ftr-mat92.3-0 ; 
       fig30_belter_ftr-mat93.2-0 ; 
       fig30_belter_ftr-mat94.2-0 ; 
       fig30_belter_ftr-mat95.2-0 ; 
       fig30_belter_ftr-mat97.2-0 ; 
       fig30_belter_ftr-mat98.2-0 ; 
       fig30_belter_ftr-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       fig30_belter_ftr-cube1.1-0 ; 
       fig30_belter_ftr-cube10.1-0 ; 
       fig30_belter_ftr-cube11.1-0 ; 
       fig30_belter_ftr-cube14.1-0 ; 
       fig30_belter_ftr-cube18.1-0 ; 
       fig30_belter_ftr-cube20.1-0 ; 
       fig30_belter_ftr-cube21.1-0 ; 
       fig30_belter_ftr-cube22.1-0 ; 
       fig30_belter_ftr-cube23.1-0 ; 
       fig30_belter_ftr-cube3.1-0 ; 
       fig30_belter_ftr-cube6.1-0 ; 
       fig30_belter_ftr-cube7.1-0 ; 
       fig30_belter_ftr-cube8.1-0 ; 
       fig30_belter_ftr-cyl1.1-0 ; 
       fig30_belter_ftr-extru2.1-0 ; 
       fig30_belter_ftr-extru3.2-0 ; 
       fig30_belter_ftr-null2.34-0 ROOT ; 
       fig30_belter_ftr-sphere1.1-0 ; 
       fig30_belter_ftr-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig30/PICTURES/fig30 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig30-belter_ftr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       fig30_belter_ftr-t2d1.8-0 ; 
       fig30_belter_ftr-t2d10.3-0 ; 
       fig30_belter_ftr-t2d11.4-0 ; 
       fig30_belter_ftr-t2d12.3-0 ; 
       fig30_belter_ftr-t2d13.3-0 ; 
       fig30_belter_ftr-t2d15.2-0 ; 
       fig30_belter_ftr-t2d16.3-0 ; 
       fig30_belter_ftr-t2d17.2-0 ; 
       fig30_belter_ftr-t2d18.3-0 ; 
       fig30_belter_ftr-t2d19.2-0 ; 
       fig30_belter_ftr-t2d2.7-0 ; 
       fig30_belter_ftr-t2d20.2-0 ; 
       fig30_belter_ftr-t2d21.2-0 ; 
       fig30_belter_ftr-t2d23.3-0 ; 
       fig30_belter_ftr-t2d24.3-0 ; 
       fig30_belter_ftr-t2d32.4-0 ; 
       fig30_belter_ftr-t2d33.1-0 ; 
       fig30_belter_ftr-t2d34.1-0 ; 
       fig30_belter_ftr-t2d35.1-0 ; 
       fig30_belter_ftr-t2d36.1-0 ; 
       fig30_belter_ftr-t2d37.1-0 ; 
       fig30_belter_ftr-t2d38.1-0 ; 
       fig30_belter_ftr-t2d39.2-0 ; 
       fig30_belter_ftr-t2d40.1-0 ; 
       fig30_belter_ftr-t2d41.1-0 ; 
       fig30_belter_ftr-t2d5.2-0 ; 
       fig30_belter_ftr-t2d6.4-0 ; 
       fig30_belter_ftr-t2d7.4-0 ; 
       fig30_belter_ftr-t2d8.3-0 ; 
       fig30_belter_ftr-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 14 110 ; 
       4 1 110 ; 
       5 13 110 ; 
       6 0 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 13 110 ; 
       10 0 110 ; 
       11 15 110 ; 
       12 0 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 16 110 ; 
       17 15 110 ; 
       18 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 22 300 ; 
       0 0 300 ; 
       0 8 300 ; 
       1 23 300 ; 
       1 3 300 ; 
       2 24 300 ; 
       3 6 300 ; 
       4 25 300 ; 
       5 28 300 ; 
       6 29 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       7 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 12 300 ; 
       9 27 300 ; 
       10 21 300 ; 
       11 19 300 ; 
       12 26 300 ; 
       13 20 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       14 16 300 ; 
       14 17 300 ; 
       14 13 300 ; 
       15 18 300 ; 
       15 7 300 ; 
       17 14 300 ; 
       18 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 11 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 15 401 ; 
       7 16 401 ; 
       8 17 401 ; 
       9 18 401 ; 
       10 19 401 ; 
       11 20 401 ; 
       12 21 401 ; 
       13 22 401 ; 
       14 23 401 ; 
       15 24 401 ; 
       16 0 401 ; 
       17 10 401 ; 
       18 25 401 ; 
       19 26 401 ; 
       20 27 401 ; 
       21 28 401 ; 
       22 29 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 4 401 ; 
       27 5 401 ; 
       28 6 401 ; 
       29 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 51.70941 -4 0 MPRFLG 0 ; 
       1 SCHEM 44.20941 -6 0 MPRFLG 0 ; 
       2 SCHEM 40.45941 -8 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 42.95941 -8 0 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 MPRFLG 0 ; 
       6 SCHEM 55.45941 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 52.95941 -6 0 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 MPRFLG 0 ; 
       12 SCHEM 50.45941 -6 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       15 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 37.95941 -4 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 60.45941 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 47.95941 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.95941 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.95941 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 37.95941 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 65.45941 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 52.95941 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 57.95941 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 45.45941 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 40.45941 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.95941 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 50.45941 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 55.45941 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45.45941 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40.45941 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 42.95941 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50.45941 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 55.45941 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60.45941 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.95941 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 67.95941 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 62.95941 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 37.95941 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 65.45941 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 52.95941 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 57.95941 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
