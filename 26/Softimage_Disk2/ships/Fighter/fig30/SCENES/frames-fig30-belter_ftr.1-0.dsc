SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.59-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat100.3-0 ; 
       fig30_belter_ftr-mat101.2-0 ; 
       fig30_belter_ftr-mat102.2-0 ; 
       fig30_belter_ftr-mat103.2-0 ; 
       fig30_belter_ftr-mat105.3-0 ; 
       fig30_belter_ftr-mat106.3-0 ; 
       fig30_belter_ftr-mat107.2-0 ; 
       fig30_belter_ftr-mat108.2-0 ; 
       fig30_belter_ftr-mat109.2-0 ; 
       fig30_belter_ftr-mat110.2-0 ; 
       fig30_belter_ftr-mat111.2-0 ; 
       fig30_belter_ftr-mat112.2-0 ; 
       fig30_belter_ftr-mat113.2-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat116.1-0 ; 
       fig30_belter_ftr-mat117.1-0 ; 
       fig30_belter_ftr-mat118.1-0 ; 
       fig30_belter_ftr-mat119.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat121.1-0 ; 
       fig30_belter_ftr-mat122.1-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
       fig30_belter_ftr-mat83.5-0 ; 
       fig30_belter_ftr-mat84.4-0 ; 
       fig30_belter_ftr-mat87.2-0 ; 
       fig30_belter_ftr-mat88.3-0 ; 
       fig30_belter_ftr-mat89.3-0 ; 
       fig30_belter_ftr-mat90.3-0 ; 
       fig30_belter_ftr-mat91.4-0 ; 
       fig30_belter_ftr-mat92.3-0 ; 
       fig30_belter_ftr-mat93.2-0 ; 
       fig30_belter_ftr-mat94.2-0 ; 
       fig30_belter_ftr-mat95.2-0 ; 
       fig30_belter_ftr-mat97.2-0 ; 
       fig30_belter_ftr-mat98.2-0 ; 
       fig30_belter_ftr-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       fig30_belter_ftr-cockpt.1-0 ; 
       fig30_belter_ftr-cube1.1-0 ; 
       fig30_belter_ftr-cube10.1-0 ; 
       fig30_belter_ftr-cube11.1-0 ; 
       fig30_belter_ftr-cube14.1-0 ; 
       fig30_belter_ftr-cube18.1-0 ; 
       fig30_belter_ftr-cube20.1-0 ; 
       fig30_belter_ftr-cube21.1-0 ; 
       fig30_belter_ftr-cube22.1-0 ; 
       fig30_belter_ftr-cube23.1-0 ; 
       fig30_belter_ftr-cube3.1-0 ; 
       fig30_belter_ftr-cube6.1-0 ; 
       fig30_belter_ftr-cube7.1-0 ; 
       fig30_belter_ftr-cube8.1-0 ; 
       fig30_belter_ftr-cyl1.1-0 ; 
       fig30_belter_ftr-cyl10.1-0 ; 
       fig30_belter_ftr-cyl11.1-0 ; 
       fig30_belter_ftr-cyl3.1-0 ; 
       fig30_belter_ftr-cyl4.1-0 ; 
       fig30_belter_ftr-cyl5.1-0 ; 
       fig30_belter_ftr-cyl9.1-0 ; 
       fig30_belter_ftr-extru2.1-0 ; 
       fig30_belter_ftr-extru3.2-0 ; 
       fig30_belter_ftr-lsmoke.1-0 ; 
       fig30_belter_ftr-lthrust.1-0 ; 
       fig30_belter_ftr-lwepemt.1-0 ; 
       fig30_belter_ftr-missemt.1-0 ; 
       fig30_belter_ftr-null1.1-0 ; 
       fig30_belter_ftr-null2.36-0 ROOT ; 
       fig30_belter_ftr-rsmoke.1-0 ; 
       fig30_belter_ftr-rthrust.1-0 ; 
       fig30_belter_ftr-rwepemt.1-0 ; 
       fig30_belter_ftr-sphere1.1-0 ; 
       fig30_belter_ftr-sphere3.1-0 ; 
       fig30_belter_ftr-SS01.1-0 ; 
       fig30_belter_ftr-SS02.1-0 ; 
       fig30_belter_ftr-SS03.1-0 ; 
       fig30_belter_ftr-SS04.1-0 ; 
       fig30_belter_ftr-SS05.1-0 ; 
       fig30_belter_ftr-SS06.1-0 ; 
       fig30_belter_ftr-SS07.1-0 ; 
       fig30_belter_ftr-SS08.1-0 ; 
       fig30_belter_ftr-trail.1-0 ; 
       fig30_belter_ftr-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig30/PICTURES/fig30 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       frames-fig30-belter_ftr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       fig30_belter_ftr-t2d1.8-0 ; 
       fig30_belter_ftr-t2d10.3-0 ; 
       fig30_belter_ftr-t2d11.4-0 ; 
       fig30_belter_ftr-t2d12.3-0 ; 
       fig30_belter_ftr-t2d13.3-0 ; 
       fig30_belter_ftr-t2d15.2-0 ; 
       fig30_belter_ftr-t2d16.3-0 ; 
       fig30_belter_ftr-t2d17.2-0 ; 
       fig30_belter_ftr-t2d18.3-0 ; 
       fig30_belter_ftr-t2d19.2-0 ; 
       fig30_belter_ftr-t2d2.7-0 ; 
       fig30_belter_ftr-t2d20.2-0 ; 
       fig30_belter_ftr-t2d21.2-0 ; 
       fig30_belter_ftr-t2d23.3-0 ; 
       fig30_belter_ftr-t2d24.3-0 ; 
       fig30_belter_ftr-t2d25.2-0 ; 
       fig30_belter_ftr-t2d27.2-0 ; 
       fig30_belter_ftr-t2d28.2-0 ; 
       fig30_belter_ftr-t2d29.2-0 ; 
       fig30_belter_ftr-t2d30.2-0 ; 
       fig30_belter_ftr-t2d31.2-0 ; 
       fig30_belter_ftr-t2d32.4-0 ; 
       fig30_belter_ftr-t2d33.1-0 ; 
       fig30_belter_ftr-t2d34.1-0 ; 
       fig30_belter_ftr-t2d35.1-0 ; 
       fig30_belter_ftr-t2d36.1-0 ; 
       fig30_belter_ftr-t2d37.1-0 ; 
       fig30_belter_ftr-t2d38.1-0 ; 
       fig30_belter_ftr-t2d39.2-0 ; 
       fig30_belter_ftr-t2d40.1-0 ; 
       fig30_belter_ftr-t2d41.1-0 ; 
       fig30_belter_ftr-t2d5.2-0 ; 
       fig30_belter_ftr-t2d6.4-0 ; 
       fig30_belter_ftr-t2d7.4-0 ; 
       fig30_belter_ftr-t2d8.3-0 ; 
       fig30_belter_ftr-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 28 110 ; 
       1 22 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 21 110 ; 
       5 2 110 ; 
       6 14 110 ; 
       7 1 110 ; 
       8 21 110 ; 
       9 21 110 ; 
       10 14 110 ; 
       11 1 110 ; 
       12 22 110 ; 
       13 1 110 ; 
       14 22 110 ; 
       15 20 110 ; 
       16 20 110 ; 
       17 27 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 27 110 ; 
       21 22 110 ; 
       22 28 110 ; 
       23 28 110 ; 
       24 28 110 ; 
       25 28 110 ; 
       26 28 110 ; 
       27 22 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 28 110 ; 
       35 28 110 ; 
       36 28 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 28 110 ; 
       40 28 110 ; 
       41 28 110 ; 
       42 28 110 ; 
       43 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 36 300 ; 
       1 6 300 ; 
       1 20 300 ; 
       2 37 300 ; 
       2 9 300 ; 
       3 38 300 ; 
       4 18 300 ; 
       5 39 300 ; 
       6 42 300 ; 
       7 43 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       8 21 300 ; 
       9 22 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       10 41 300 ; 
       11 35 300 ; 
       12 33 300 ; 
       13 40 300 ; 
       14 34 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       15 16 300 ; 
       16 17 300 ; 
       17 13 300 ; 
       18 12 300 ; 
       19 14 300 ; 
       20 15 300 ; 
       21 30 300 ; 
       21 31 300 ; 
       21 25 300 ; 
       22 32 300 ; 
       22 19 300 ; 
       32 26 300 ; 
       33 27 300 ; 
       34 0 300 ; 
       35 1 300 ; 
       36 3 300 ; 
       37 2 300 ; 
       38 5 300 ; 
       39 4 300 ; 
       40 28 300 ; 
       41 29 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 8 401 ; 
       7 9 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 17 401 ; 
       14 16 401 ; 
       15 18 401 ; 
       16 19 401 ; 
       17 20 401 ; 
       18 21 401 ; 
       19 22 401 ; 
       20 23 401 ; 
       21 24 401 ; 
       22 25 401 ; 
       23 26 401 ; 
       24 27 401 ; 
       25 28 401 ; 
       26 29 401 ; 
       27 30 401 ; 
       30 0 401 ; 
       31 10 401 ; 
       32 31 401 ; 
       33 32 401 ; 
       34 33 401 ; 
       35 34 401 ; 
       36 35 401 ; 
       37 1 401 ; 
       38 2 401 ; 
       39 3 401 ; 
       40 4 401 ; 
       41 5 401 ; 
       42 6 401 ; 
       43 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 118.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 111.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 107.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 110 -8 0 MPRFLG 0 ; 
       6 SCHEM 75 -6 0 MPRFLG 0 ; 
       7 SCHEM 122.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 60 -6 0 MPRFLG 0 ; 
       10 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 120 -6 0 MPRFLG 0 ; 
       12 SCHEM 85 -4 0 MPRFLG 0 ; 
       13 SCHEM 117.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 100 -8 0 MPRFLG 0 ; 
       16 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 95 -6 0 MPRFLG 0 ; 
       18 SCHEM 95 -8 0 MPRFLG 0 ; 
       19 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 58.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 91.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       28 SCHEM 68.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 90 -4 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 40 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 45 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 130 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
