SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.1-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       bel_fighter-cockpt.1-0 ; 
       bel_fighter-cube1.1-0 ; 
       bel_fighter-cube10.1-0 ; 
       bel_fighter-cube11.1-0 ; 
       bel_fighter-cube13.1-0 ; 
       bel_fighter-cube2.1-0 ; 
       bel_fighter-cube3.1-0 ; 
       bel_fighter-cube4.1-0 ; 
       bel_fighter-cube5.1-0 ; 
       bel_fighter-cube6.1-0 ; 
       bel_fighter-cube7.1-0 ; 
       bel_fighter-cube8.1-0 ; 
       bel_fighter-cube9.1-0 ; 
       bel_fighter-cyl1.1-0 ; 
       bel_fighter-cyl3.1-0 ; 
       bel_fighter-cyl4.1-0 ; 
       bel_fighter-cyl5.1-0 ; 
       bel_fighter-cyl6.1-0 ; 
       bel_fighter-cyl7.1-0 ; 
       bel_fighter-cyl8.1-0 ; 
       bel_fighter-extru2.1-0 ; 
       bel_fighter-extru3.1-0 ROOT ; 
       bel_fighter-lsmoke.1-0 ; 
       bel_fighter-lthrust.1-0 ; 
       bel_fighter-lwepemt.1-0 ; 
       bel_fighter-missemt.1-0 ; 
       bel_fighter-null1.1-0 ; 
       bel_fighter-rsmoke.1-0 ; 
       bel_fighter-rthrust.1-0 ; 
       bel_fighter-rwepemt.1-0 ; 
       bel_fighter-sphere1.1-0 ; 
       bel_fighter-sphere2.1-0 ; 
       bel_fighter-spline1.1-0 ROOT ; 
       bel_fighter-SS01.1-0 ; 
       bel_fighter-SS02.1-0 ; 
       bel_fighter-SS03.1-0 ; 
       bel_fighter-SS04.1-0 ; 
       bel_fighter-SS05.1-0 ; 
       bel_fighter-SS06.1-0 ; 
       bel_fighter-SS07.1-0 ; 
       bel_fighter-SS08.1-0 ; 
       bel_fighter-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig30-belter_ftr.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 21 110 ; 
       28 21 110 ; 
       0 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       22 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       27 21 110 ; 
       29 21 110 ; 
       33 21 110 ; 
       34 21 110 ; 
       35 21 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       41 21 110 ; 
       1 21 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 21 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 20 110 ; 
       9 1 110 ; 
       10 21 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 21 110 ; 
       14 26 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 26 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 21 110 ; 
       26 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       39 6 300 ; 
       40 7 300 ; 
       33 0 300 ; 
       34 1 300 ; 
       35 3 300 ; 
       36 2 300 ; 
       37 5 300 ; 
       38 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 72.07513 3.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 72.07513 1.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 72.9009 -22.31113 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 77.5751 -22.2152 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 83.58306 -21.11666 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 97.67372 -19.76183 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 97.60789 -20.42364 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 72.97263 -21.51178 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 86.13657 -22.23332 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 83.63657 -22.23332 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 77.5829 -21.25599 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 81.13657 -22.23332 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 90.44331 -19.6374 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 90.50601 -20.40357 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 92.88061 -19.58456 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 92.83763 -20.42999 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 95.30134 -19.69024 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 95.34435 -20.40357 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 75.12785 -19.98207 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 101.3049 -6.486435 0 MPRFLG 0 ; 
       2 SCHEM 97.55489 -8.486437 0 MPRFLG 0 ; 
       3 SCHEM 96.30489 -10.48644 0 MPRFLG 0 ; 
       4 SCHEM 98.80489 -10.48644 0 MPRFLG 0 ; 
       5 SCHEM 78.80489 -6.486435 0 MPRFLG 0 ; 
       6 SCHEM 76.30489 -8.486437 0 MPRFLG 0 ; 
       7 SCHEM 73.80489 -8.486437 0 MPRFLG 0 ; 
       8 SCHEM 71.30489 -8.486437 0 MPRFLG 0 ; 
       9 SCHEM 106.3049 -8.486437 0 MPRFLG 0 ; 
       10 SCHEM 81.30489 -6.486435 0 MPRFLG 0 ; 
       11 SCHEM 101.3049 -8.486437 0 MPRFLG 0 ; 
       12 SCHEM 103.8049 -8.486437 0 MPRFLG 0 ; 
       13 SCHEM 75.05489 -6.486435 0 MPRFLG 0 ; 
       14 SCHEM 92.55489 -8.486437 0 MPRFLG 0 ; 
       15 SCHEM 93.80489 -10.48644 0 MPRFLG 0 ; 
       16 SCHEM 91.30489 -10.48644 0 MPRFLG 0 ; 
       17 SCHEM 87.55489 -8.486437 0 MPRFLG 0 ; 
       18 SCHEM 86.30489 -10.48644 0 MPRFLG 0 ; 
       19 SCHEM 88.80489 -10.48644 0 MPRFLG 0 ; 
       20 SCHEM 71.30489 -6.486435 0 MPRFLG 0 ; 
       21 SCHEM 90.05489 -4.486434 0 USR SRT 1 1 1 3.141593 0 0 0 0 -0.3758854 MPRFLG 0 ; 
       26 SCHEM 90.05489 -6.486435 0 MPRFLG 0 ; 
       30 SCHEM 83.80489 -6.486435 0 MPRFLG 0 ; 
       31 SCHEM 108.8049 -6.486435 0 MPRFLG 0 ; 
       32 SCHEM 72 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
