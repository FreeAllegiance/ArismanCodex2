SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.12-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 43     
       fig30_belter_ftr-cockpt.1-0 ; 
       fig30_belter_ftr-cube1.1-0 ; 
       fig30_belter_ftr-cube10.1-0 ; 
       fig30_belter_ftr-cube11.1-0 ; 
       fig30_belter_ftr-cube13.1-0 ; 
       fig30_belter_ftr-cube14.1-0 ; 
       fig30_belter_ftr-cube2.1-0 ; 
       fig30_belter_ftr-cube3.1-0 ; 
       fig30_belter_ftr-cube4.1-0 ; 
       fig30_belter_ftr-cube5.1-0 ; 
       fig30_belter_ftr-cube6.1-0 ; 
       fig30_belter_ftr-cube7.1-0 ; 
       fig30_belter_ftr-cube8.1-0 ; 
       fig30_belter_ftr-cube9.1-0 ; 
       fig30_belter_ftr-cyl1.1-0 ; 
       fig30_belter_ftr-cyl3.1-0 ; 
       fig30_belter_ftr-cyl4.1-0 ; 
       fig30_belter_ftr-cyl5.1-0 ; 
       fig30_belter_ftr-cyl6.1-0 ; 
       fig30_belter_ftr-cyl7.1-0 ; 
       fig30_belter_ftr-cyl8.1-0 ; 
       fig30_belter_ftr-extru2.1-0 ; 
       fig30_belter_ftr-extru3.2-0 ; 
       fig30_belter_ftr-lsmoke.1-0 ; 
       fig30_belter_ftr-lthrust.1-0 ; 
       fig30_belter_ftr-lwepemt.1-0 ; 
       fig30_belter_ftr-missemt.1-0 ; 
       fig30_belter_ftr-null1.1-0 ; 
       fig30_belter_ftr-null2.1-0 ROOT ; 
       fig30_belter_ftr-rsmoke.1-0 ; 
       fig30_belter_ftr-rthrust.1-0 ; 
       fig30_belter_ftr-rwepemt.1-0 ; 
       fig30_belter_ftr-sphere1.1-0 ; 
       fig30_belter_ftr-sphere2.1-0 ; 
       fig30_belter_ftr-SS01.1-0 ; 
       fig30_belter_ftr-SS02.1-0 ; 
       fig30_belter_ftr-SS03.1-0 ; 
       fig30_belter_ftr-SS04.1-0 ; 
       fig30_belter_ftr-SS05.1-0 ; 
       fig30_belter_ftr-SS06.1-0 ; 
       fig30_belter_ftr-SS07.1-0 ; 
       fig30_belter_ftr-SS08.1-0 ; 
       fig30_belter_ftr-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig30-belter_ftr.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 21 110 ; 
       0 28 110 ; 
       1 22 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       6 22 110 ; 
       7 14 110 ; 
       8 14 110 ; 
       9 21 110 ; 
       10 1 110 ; 
       11 22 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 22 110 ; 
       15 27 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 27 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 22 110 ; 
       22 28 110 ; 
       23 28 110 ; 
       24 28 110 ; 
       25 28 110 ; 
       26 28 110 ; 
       27 22 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 28 110 ; 
       35 28 110 ; 
       36 28 110 ; 
       37 28 110 ; 
       38 28 110 ; 
       39 28 110 ; 
       40 28 110 ; 
       41 28 110 ; 
       42 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       34 0 300 ; 
       35 1 300 ; 
       36 3 300 ; 
       37 2 300 ; 
       38 5 300 ; 
       39 4 300 ; 
       40 6 300 ; 
       41 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 72.07513 3.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 72.07513 1.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 72.93746 -11.57291 0 USR MPRFLG 0 ; 
       28 SCHEM 86.25154 2.456968 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 75.78072 -7.086786 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 105.1703 -10.7813 0 MPRFLG 0 ; 
       2 SCHEM 101.4202 -12.7813 0 MPRFLG 0 ; 
       3 SCHEM 100.1702 -14.7813 0 MPRFLG 0 ; 
       4 SCHEM 102.6702 -14.7813 0 MPRFLG 0 ; 
       6 SCHEM 82.67025 -10.7813 0 MPRFLG 0 ; 
       7 SCHEM 80.17025 -12.7813 0 MPRFLG 0 ; 
       8 SCHEM 77.67025 -12.7813 0 MPRFLG 0 ; 
       9 SCHEM 75.17025 -12.7813 0 MPRFLG 0 ; 
       10 SCHEM 110.1703 -12.7813 0 MPRFLG 0 ; 
       11 SCHEM 85.17025 -10.7813 0 MPRFLG 0 ; 
       12 SCHEM 105.1703 -12.7813 0 MPRFLG 0 ; 
       13 SCHEM 107.6703 -12.7813 0 MPRFLG 0 ; 
       14 SCHEM 78.92025 -10.7813 0 MPRFLG 0 ; 
       15 SCHEM 96.42025 -12.7813 0 MPRFLG 0 ; 
       16 SCHEM 97.67025 -14.7813 0 MPRFLG 0 ; 
       17 SCHEM 95.17025 -14.7813 0 MPRFLG 0 ; 
       18 SCHEM 91.42025 -12.7813 0 MPRFLG 0 ; 
       19 SCHEM 90.17025 -14.7813 0 MPRFLG 0 ; 
       20 SCHEM 92.67025 -14.7813 0 MPRFLG 0 ; 
       21 SCHEM 75.17025 -10.7813 0 MPRFLG 0 ; 
       22 SCHEM 93.92025 -8.781296 0 USR MPRFLG 0 ; 
       23 SCHEM 65.17029 -7.481907 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 65.09856 -8.281257 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 78.33423 -8.203446 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75.83423 -8.203446 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 93.92025 -10.7813 0 MPRFLG 0 ; 
       29 SCHEM 69.78056 -7.226116 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 69.77276 -8.185327 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 73.33423 -8.203446 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 87.67025 -10.7813 0 MPRFLG 0 ; 
       33 SCHEM 112.6703 -10.7813 0 MPRFLG 0 ; 
       34 SCHEM 82.64097 -5.607526 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 82.70367 -6.373696 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 85.07826 -5.554687 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 85.03529 -6.400117 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 87.49899 -5.660367 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 87.54201 -6.373696 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 89.87138 -5.731955 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 89.80554 -6.393765 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 67.32551 -5.952198 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
