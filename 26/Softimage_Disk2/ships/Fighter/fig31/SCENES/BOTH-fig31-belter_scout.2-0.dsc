SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.37-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       edit_nulls-mat70.4-0 ; 
       fig20_biofighter-mat71.4-0 ; 
       fig20_biofighter-mat75.4-0 ; 
       fig20_biofighter-mat77.4-0 ; 
       fig20_biofighter-mat78.4-0 ; 
       fig20_biofighter-mat80.4-0 ; 
       fig30_belter_ftr-mat81.3-0 ; 
       fig30_belter_ftr-mat82.3-0 ; 
       fig31_belter_scout-mat83.2-0 ; 
       fig31_belter_scout-mat83_1.1-0 ; 
       fig31_belter_scout-mat84.3-0 ; 
       fig31_belter_scout-mat84_1.1-0 ; 
       fig31_belter_scout-mat85.2-0 ; 
       fig31_belter_scout-mat85_1.1-0 ; 
       fig31_belter_scout-mat86.3-0 ; 
       fig31_belter_scout-mat86_1.1-0 ; 
       fig31_belter_scout-mat87.2-0 ; 
       fig31_belter_scout-mat87_1.1-0 ; 
       fig31_belter_scout-mat88.3-0 ; 
       fig31_belter_scout-mat88_1.1-0 ; 
       fig31_belter_scout-mat89.3-0 ; 
       fig31_belter_scout-mat89_1.1-0 ; 
       fig31_belter_scout-mat90.3-0 ; 
       fig31_belter_scout-mat90_1.1-0 ; 
       fig31_belter_scout-mat91.2-0 ; 
       fig31_belter_scout-mat91_1.1-0 ; 
       fig31_belter_scout-mat92.2-0 ; 
       fig31_belter_scout-mat92_1.1-0 ; 
       fig31_belter_scout-mat93.3-0 ; 
       fig31_belter_scout-mat93_1.1-0 ; 
       fig31_belter_scout-mat94.2-0 ; 
       fig31_belter_scout-mat94_1.1-0 ; 
       fig31_belter_scout-mat95.2-0 ; 
       fig31_belter_scout-mat95_1.1-0 ; 
       fig31_belter_scout-mat96.2-0 ; 
       fig31_belter_scout-mat97.2-0 ; 
       fig31_belter_scout-mat97_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       fig31_belter_scout-cockpt.1-0 ; 
       fig31_belter_scout-cube2.1-0 ; 
       fig31_belter_scout-cube2_1.1-0 ; 
       fig31_belter_scout-cube3.1-0 ; 
       fig31_belter_scout-cube3_1.1-0 ; 
       fig31_belter_scout-cube4.1-0 ; 
       fig31_belter_scout-cube4_1.1-0 ; 
       fig31_belter_scout-cyl1.1-0 ; 
       fig31_belter_scout-lsmoke.1-0 ; 
       fig31_belter_scout-lthrust.1-0 ; 
       fig31_belter_scout-missemt.1-0 ; 
       fig31_belter_scout-null1.26-0 ROOT ; 
       fig31_belter_scout-null1_1.2-0 ROOT ; 
       fig31_belter_scout-rsmoke.1-0 ; 
       fig31_belter_scout-rthrust.1-0 ; 
       fig31_belter_scout-scout.9-0 ; 
       fig31_belter_scout-scout_1.2-0 ; 
       fig31_belter_scout-scout_1_1.2-0 ; 
       fig31_belter_scout-scout_3.9-0 ; 
       fig31_belter_scout-scout2.1-0 ; 
       fig31_belter_scout-scout2_1.1-0 ; 
       fig31_belter_scout-SS01.1-0 ; 
       fig31_belter_scout-SS02.1-0 ; 
       fig31_belter_scout-SS03.1-0 ; 
       fig31_belter_scout-SS04.1-0 ; 
       fig31_belter_scout-SS05.1-0 ; 
       fig31_belter_scout-SS06.1-0 ; 
       fig31_belter_scout-SS07.1-0 ; 
       fig31_belter_scout-SS08.1-0 ; 
       fig31_belter_scout-trail.1-0 ; 
       fig31_belter_scout-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig31/PICTURES/fig31 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-fig31-belter_scout.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       fig31_belter_scout-t2d1.3-0 ; 
       fig31_belter_scout-t2d1_1.1-0 ; 
       fig31_belter_scout-t2d10.3-0 ; 
       fig31_belter_scout-t2d10_1.1-0 ; 
       fig31_belter_scout-t2d11.2-0 ; 
       fig31_belter_scout-t2d11_1.1-0 ; 
       fig31_belter_scout-t2d12.2-0 ; 
       fig31_belter_scout-t2d12_1.1-0 ; 
       fig31_belter_scout-t2d13.2-0 ; 
       fig31_belter_scout-t2d14.2-0 ; 
       fig31_belter_scout-t2d14_1.1-0 ; 
       fig31_belter_scout-t2d2.2-0 ; 
       fig31_belter_scout-t2d2_1.1-0 ; 
       fig31_belter_scout-t2d3.6-0 ; 
       fig31_belter_scout-t2d3_1.1-0 ; 
       fig31_belter_scout-t2d4.4-0 ; 
       fig31_belter_scout-t2d4_1.1-0 ; 
       fig31_belter_scout-t2d5.3-0 ; 
       fig31_belter_scout-t2d5_1.1-0 ; 
       fig31_belter_scout-t2d6.3-0 ; 
       fig31_belter_scout-t2d6_1.1-0 ; 
       fig31_belter_scout-t2d7.4-0 ; 
       fig31_belter_scout-t2d7_1.1-0 ; 
       fig31_belter_scout-t2d8.4-0 ; 
       fig31_belter_scout-t2d8_1.1-0 ; 
       fig31_belter_scout-t2d9.4-0 ; 
       fig31_belter_scout-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 15 110 ; 
       2 18 110 ; 
       3 15 110 ; 
       4 18 110 ; 
       5 15 110 ; 
       6 18 110 ; 
       7 5 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 15 110 ; 
       17 18 110 ; 
       18 12 110 ; 
       19 16 110 ; 
       20 17 110 ; 
       21 11 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 11 110 ; 
       30 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 22 300 ; 
       1 24 300 ; 
       1 26 300 ; 
       2 23 300 ; 
       2 25 300 ; 
       2 27 300 ; 
       3 28 300 ; 
       3 30 300 ; 
       4 29 300 ; 
       4 31 300 ; 
       5 32 300 ; 
       6 33 300 ; 
       7 34 300 ; 
       11 8 300 ; 
       12 9 300 ; 
       15 10 300 ; 
       15 12 300 ; 
       16 18 300 ; 
       16 20 300 ; 
       16 35 300 ; 
       17 19 300 ; 
       17 21 300 ; 
       17 36 300 ; 
       18 11 300 ; 
       18 13 300 ; 
       19 14 300 ; 
       19 16 300 ; 
       20 15 300 ; 
       20 17 300 ; 
       21 0 300 ; 
       22 1 300 ; 
       23 3 300 ; 
       24 2 300 ; 
       25 5 300 ; 
       26 4 300 ; 
       27 6 300 ; 
       28 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 0 401 ; 
       11 1 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 2 401 ; 
       29 3 401 ; 
       30 4 401 ; 
       31 5 401 ; 
       32 6 401 ; 
       33 7 401 ; 
       34 8 401 ; 
       35 9 401 ; 
       36 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 107 -4 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 113.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 117 -4 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 108.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       17 SCHEM 97 -4 0 MPRFLG 0 ; 
       18 SCHEM 107 -2 0 MPRFLG 0 ; 
       19 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 93.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 124.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 119.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 122 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 92 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 94.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 97 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 99.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 104.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 107 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 109.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 112 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 114.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 117 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 80 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 85 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 102 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 119.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 114.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 77.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 117 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 82.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 87.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 102 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 122 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 92 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 94.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 97 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 99.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 104.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 107 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 109.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
