SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.35-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       fig31_belter_scout-mat83.2-0 ; 
       fig31_belter_scout-mat84.3-0 ; 
       fig31_belter_scout-mat85.2-0 ; 
       fig31_belter_scout-mat86.3-0 ; 
       fig31_belter_scout-mat87.2-0 ; 
       fig31_belter_scout-mat88.3-0 ; 
       fig31_belter_scout-mat89.3-0 ; 
       fig31_belter_scout-mat90.3-0 ; 
       fig31_belter_scout-mat91.2-0 ; 
       fig31_belter_scout-mat92.2-0 ; 
       fig31_belter_scout-mat93.3-0 ; 
       fig31_belter_scout-mat94.2-0 ; 
       fig31_belter_scout-mat95.2-0 ; 
       fig31_belter_scout-mat97.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       fig31_belter_scout-cube2.1-0 ; 
       fig31_belter_scout-cube3.1-0 ; 
       fig31_belter_scout-cube4.1-0 ; 
       fig31_belter_scout-null1.24-0 ROOT ; 
       fig31_belter_scout-scout.9-0 ; 
       fig31_belter_scout-scout_1.2-0 ; 
       fig31_belter_scout-scout2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig31/PICTURES/fig31 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig31-belter_scout.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       fig31_belter_scout-t2d1.3-0 ; 
       fig31_belter_scout-t2d10.3-0 ; 
       fig31_belter_scout-t2d11.2-0 ; 
       fig31_belter_scout-t2d12.2-0 ; 
       fig31_belter_scout-t2d14.2-0 ; 
       fig31_belter_scout-t2d2.2-0 ; 
       fig31_belter_scout-t2d3.6-0 ; 
       fig31_belter_scout-t2d4.4-0 ; 
       fig31_belter_scout-t2d5.3-0 ; 
       fig31_belter_scout-t2d6.3-0 ; 
       fig31_belter_scout-t2d7.4-0 ; 
       fig31_belter_scout-t2d8.4-0 ; 
       fig31_belter_scout-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       3 0 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 13 300 ; 
       6 3 300 ; 
       6 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 5 401 ; 
       3 6 401 ; 
       4 7 401 ; 
       5 8 401 ; 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 1 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 3.75 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
