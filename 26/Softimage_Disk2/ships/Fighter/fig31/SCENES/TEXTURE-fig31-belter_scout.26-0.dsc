SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.31-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       fig30_belter_ftr-mat82.2-0 ; 
       fig31_belter_scout-mat83.1-0 ; 
       fig31_belter_scout-mat84.2-0 ; 
       fig31_belter_scout-mat85.1-0 ; 
       fig31_belter_scout-mat86.2-0 ; 
       fig31_belter_scout-mat87.1-0 ; 
       fig31_belter_scout-mat88.2-0 ; 
       fig31_belter_scout-mat89.2-0 ; 
       fig31_belter_scout-mat90.2-0 ; 
       fig31_belter_scout-mat91.1-0 ; 
       fig31_belter_scout-mat92.1-0 ; 
       fig31_belter_scout-mat93.2-0 ; 
       fig31_belter_scout-mat94.1-0 ; 
       fig31_belter_scout-mat95.1-0 ; 
       fig31_belter_scout-mat96.1-0 ; 
       fig31_belter_scout-mat97.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       fig31_belter_scout-cockpt.1-0 ; 
       fig31_belter_scout-cube2.1-0 ; 
       fig31_belter_scout-cube3.1-0 ; 
       fig31_belter_scout-cube4.1-0 ; 
       fig31_belter_scout-cyl1.1-0 ; 
       fig31_belter_scout-lsmoke.1-0 ; 
       fig31_belter_scout-lthrust.1-0 ; 
       fig31_belter_scout-missemt.1-0 ; 
       fig31_belter_scout-null1.22-0 ROOT ; 
       fig31_belter_scout-rsmoke.1-0 ; 
       fig31_belter_scout-rthrust.1-0 ; 
       fig31_belter_scout-scout.9-0 ; 
       fig31_belter_scout-scout_1.2-0 ; 
       fig31_belter_scout-scout2.1-0 ; 
       fig31_belter_scout-SS01.1-0 ; 
       fig31_belter_scout-SS02.1-0 ; 
       fig31_belter_scout-SS03.1-0 ; 
       fig31_belter_scout-SS04.1-0 ; 
       fig31_belter_scout-SS05.1-0 ; 
       fig31_belter_scout-SS06.1-0 ; 
       fig31_belter_scout-SS07.1-0 ; 
       fig31_belter_scout-SS08.1-0 ; 
       fig31_belter_scout-trail.1-0 ; 
       fig31_belter_scout-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig31/PICTURES/fig31 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-fig31-belter_scout.26-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       fig31_belter_scout-t2d1.2-0 ; 
       fig31_belter_scout-t2d10.2-0 ; 
       fig31_belter_scout-t2d11.1-0 ; 
       fig31_belter_scout-t2d12.1-0 ; 
       fig31_belter_scout-t2d13.1-0 ; 
       fig31_belter_scout-t2d14.1-0 ; 
       fig31_belter_scout-t2d2.1-0 ; 
       fig31_belter_scout-t2d3.5-0 ; 
       fig31_belter_scout-t2d4.3-0 ; 
       fig31_belter_scout-t2d5.2-0 ; 
       fig31_belter_scout-t2d6.2-0 ; 
       fig31_belter_scout-t2d7.3-0 ; 
       fig31_belter_scout-t2d8.3-0 ; 
       fig31_belter_scout-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 3 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 15 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 20 300 ; 
       4 21 300 ; 
       8 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       12 13 300 ; 
       12 14 300 ; 
       12 22 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 6 300 ; 
       21 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       10 6 401 ; 
       11 7 401 ; 
       12 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 55 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 80 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 85 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 77.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 82.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
