SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       fig31_belter_scout-cockpt.1-0 ; 
       fig31_belter_scout-cube2.1-0 ; 
       fig31_belter_scout-cube3.1-0 ; 
       fig31_belter_scout-cube4.1-0 ; 
       fig31_belter_scout-cyl1.1-0 ; 
       fig31_belter_scout-lsmoke.1-0 ; 
       fig31_belter_scout-lthrust.1-0 ; 
       fig31_belter_scout-missemt.1-0 ; 
       fig31_belter_scout-null1.1-0 ROOT ; 
       fig31_belter_scout-rsmoke.1-0 ; 
       fig31_belter_scout-rthrust.1-0 ; 
       fig31_belter_scout-scout.9-0 ; 
       fig31_belter_scout-scout_1.2-0 ; 
       fig31_belter_scout-scout2.1-0 ; 
       fig31_belter_scout-SS01.1-0 ; 
       fig31_belter_scout-SS02.1-0 ; 
       fig31_belter_scout-SS03.1-0 ; 
       fig31_belter_scout-SS04.1-0 ; 
       fig31_belter_scout-SS05.1-0 ; 
       fig31_belter_scout-SS06.1-0 ; 
       fig31_belter_scout-SS07.1-0 ; 
       fig31_belter_scout-SS08.1-0 ; 
       fig31_belter_scout-trail.1-0 ; 
       fig31_belter_scout-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig31-belter_scout.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 8 110 ; 
       0 8 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 3 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 6 300 ; 
       21 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0.7201361 1.890922 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0.7201361 -0.1090781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 10.83888 -11.36155 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 8.285367 -10.24489 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.211409 1.339421 0 MPRFLG 0 ; 
       2 SCHEM 9.711406 1.339421 0 MPRFLG 0 ; 
       3 SCHEM 12.21141 1.339421 0 MPRFLG 0 ; 
       4 SCHEM 12.21141 -0.660579 0 MPRFLG 0 ; 
       5 SCHEM -2.325064 -10.64001 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -2.396796 -11.43936 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 8.33888 -11.36155 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.285204 -10.38422 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.277407 -11.34343 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 8.461407 3.339421 0 USR MPRFLG 0 ; 
       12 SCHEM 4.711409 1.339421 0 MPRFLG 0 ; 
       13 SCHEM 4.711409 -0.660579 0 MPRFLG 0 ; 
       14 SCHEM 15.14562 -8.765629 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 15.20832 -9.531798 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 17.58291 -8.71279 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 17.53993 -9.558219 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 20.00363 -8.81847 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 20.04664 -9.531798 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 22.37601 -8.890058 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 22.31017 -9.551867 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM -0.1698455 -9.1103 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15.77137 4.740189 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 74.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 79.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 82 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 87 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 87.39953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 89.83682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
