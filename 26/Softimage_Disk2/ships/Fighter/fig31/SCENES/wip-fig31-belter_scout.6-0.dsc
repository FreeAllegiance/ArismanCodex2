SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig31_belter_scout-cam_int1.5-0 ROOT ; 
       fig31_belter_scout-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       fig31_belter_scout-light1.3-0 ROOT ; 
       fig31_belter_scout-light2.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       fig31_belter_scout-cube1.1-0 ROOT ; 
       fig31_belter_scout1-cube2.1-0 ; 
       fig31_belter_scout1-cube3.1-0 ; 
       fig31_belter_scout1-cube4.1-0 ; 
       fig31_belter_scout1-scout.5-0 ROOT ; 
       fig31_belter_scout1-scout_1.2-0 ; 
       fig31_belter_scout1-scout2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig31-belter_scout.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       6 5 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 8.75 0 0 SRT 9.029158 1.232 1.232 0 0 0 0 3.534815 12.51325 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
