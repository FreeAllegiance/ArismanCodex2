SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       fig31_belter_scout1-bwepemt.1-0 ; 
       fig31_belter_scout1-cockpt.1-0 ; 
       fig31_belter_scout1-cube2.1-0 ; 
       fig31_belter_scout1-cube3.1-0 ; 
       fig31_belter_scout1-cube4.1-0 ; 
       fig31_belter_scout1-cyl1.1-0 ; 
       fig31_belter_scout1-lsmoke.1-0 ; 
       fig31_belter_scout1-lthrust.1-0 ; 
       fig31_belter_scout1-missemt.1-0 ; 
       fig31_belter_scout1-rsmoke.1-0 ; 
       fig31_belter_scout1-rthrust.1-0 ; 
       fig31_belter_scout1-scout.7-0 ROOT ; 
       fig31_belter_scout1-scout_1.2-0 ; 
       fig31_belter_scout1-scout2.1-0 ; 
       fig31_belter_scout1-SS01.1-0 ; 
       fig31_belter_scout1-SS02.1-0 ; 
       fig31_belter_scout1-SS03.1-0 ; 
       fig31_belter_scout1-SS04.1-0 ; 
       fig31_belter_scout1-SS05.1-0 ; 
       fig31_belter_scout1-SS06.1-0 ; 
       fig31_belter_scout1-SS07.1-0 ; 
       fig31_belter_scout1-SS08.1-0 ; 
       fig31_belter_scout1-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig31-belter_scout.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 4 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       1 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       0 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 11 110 ; 
       18 11 110 ; 
       19 11 110 ; 
       20 11 110 ; 
       21 11 110 ; 
       22 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 6 300 ; 
       21 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0.7201361 1.890922 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0.7201361 -0.1090781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 MPRFLG 0 ; 
       11 SCHEM 8.75 0 0 SRT 9.029158 1.232 1.232 0 0 0 0 11.98661 3.850006 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.285367 -10.24489 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -2.325064 -10.64001 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -2.396796 -11.43936 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10.83888 -11.36155 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.33888 -11.36155 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.285204 -10.38422 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.277407 -11.34343 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15.14562 -8.765629 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 15.20832 -9.531798 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 17.58291 -8.71279 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 17.53993 -9.558219 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 20.00363 -8.81847 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 20.04664 -9.531798 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 22.37601 -8.890058 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 22.31017 -9.551867 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM -0.1698455 -9.1103 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 74.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 79.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 82 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 87 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 87.39953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 89.83682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
