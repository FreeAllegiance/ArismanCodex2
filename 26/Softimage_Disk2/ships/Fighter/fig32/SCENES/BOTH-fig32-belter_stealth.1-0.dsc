SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.78-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
       fig32_belter_stealth-mat101.1-0 ; 
       fig32_belter_stealth-mat101_1.1-0 ; 
       fig32_belter_stealth-mat102.1-0 ; 
       fig32_belter_stealth-mat102_1.1-0 ; 
       fig32_belter_stealth-mat103.1-0 ; 
       fig32_belter_stealth-mat103_1.1-0 ; 
       fig32_belter_stealth-mat104.1-0 ; 
       fig32_belter_stealth-mat104_1.1-0 ; 
       fig32_belter_stealth-mat105.1-0 ; 
       fig32_belter_stealth-mat105_1.1-0 ; 
       fig32_belter_stealth-mat106.1-0 ; 
       fig32_belter_stealth-mat106_1.1-0 ; 
       fig32_belter_stealth-mat107.1-0 ; 
       fig32_belter_stealth-mat107_1.1-0 ; 
       fig32_belter_stealth-mat108.1-0 ; 
       fig32_belter_stealth-mat108_1.1-0 ; 
       fig32_belter_stealth-mat109.1-0 ; 
       fig32_belter_stealth-mat109_1.1-0 ; 
       fig32_belter_stealth-mat110.1-0 ; 
       fig32_belter_stealth-mat110_1.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat111_1.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat114.1-0 ; 
       fig32_belter_stealth-mat114_1.1-0 ; 
       fig32_belter_stealth-mat115.1-0 ; 
       fig32_belter_stealth-mat115_1.1-0 ; 
       fig32_belter_stealth-mat116.1-0 ; 
       fig32_belter_stealth-mat116_1.1-0 ; 
       fig32_belter_stealth-mat117.1-0 ; 
       fig32_belter_stealth-mat117_1.1-0 ; 
       fig32_belter_stealth-mat83.4-0 ; 
       fig32_belter_stealth-mat83_1.1-0 ; 
       fig32_belter_stealth-mat85.3-0 ; 
       fig32_belter_stealth-mat85_1.1-0 ; 
       fig32_belter_stealth-mat87.1-0 ; 
       fig32_belter_stealth-mat87_1.1-0 ; 
       fig32_belter_stealth-mat88.1-0 ; 
       fig32_belter_stealth-mat88_1.1-0 ; 
       fig32_belter_stealth-mat89.3-0 ; 
       fig32_belter_stealth-mat89_1.1-0 ; 
       fig32_belter_stealth-mat90.5-0 ; 
       fig32_belter_stealth-mat90_1.1-0 ; 
       fig32_belter_stealth-mat91.2-0 ; 
       fig32_belter_stealth-mat91_1.1-0 ; 
       fig32_belter_stealth-mat92.3-0 ; 
       fig32_belter_stealth-mat92_1.1-0 ; 
       fig32_belter_stealth-mat93.2-0 ; 
       fig32_belter_stealth-mat93_1.1-0 ; 
       fig32_belter_stealth-mat94.2-0 ; 
       fig32_belter_stealth-mat94_1.1-0 ; 
       fig32_belter_stealth-mat95.3-0 ; 
       fig32_belter_stealth-mat95_1.1-0 ; 
       fig32_belter_stealth-mat96.2-0 ; 
       fig32_belter_stealth-mat96_1.1-0 ; 
       fig32_belter_stealth-mat97.1-0 ; 
       fig32_belter_stealth-mat97_1.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig32_belter_stealth-mat98_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       fig32_belter_stealth-cockpt.1-0 ; 
       fig32_belter_stealth-cube1.1-0 ; 
       fig32_belter_stealth-cube1_1.1-0 ; 
       fig32_belter_stealth-cube14.1-0 ; 
       fig32_belter_stealth-cube14_1.1-0 ; 
       fig32_belter_stealth-cube15.1-0 ; 
       fig32_belter_stealth-cube15_1.1-0 ; 
       fig32_belter_stealth-cube16.1-0 ; 
       fig32_belter_stealth-cube16_1.1-0 ; 
       fig32_belter_stealth-cube17.1-0 ; 
       fig32_belter_stealth-cube17_1.1-0 ; 
       fig32_belter_stealth-cube18.1-0 ; 
       fig32_belter_stealth-cube18_1.1-0 ; 
       fig32_belter_stealth-cube20.1-0 ; 
       fig32_belter_stealth-cube20_1.1-0 ; 
       fig32_belter_stealth-cube4.1-0 ; 
       fig32_belter_stealth-cube4_1.1-0 ; 
       fig32_belter_stealth-cube7_1.1-0 ; 
       fig32_belter_stealth-cube7_1_1.1-0 ; 
       fig32_belter_stealth-cube7_3.1-0 ; 
       fig32_belter_stealth-cube7_3_1.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cube9_1.1-0 ; 
       fig32_belter_stealth-cyl1.1-0 ; 
       fig32_belter_stealth-cyl1_1.1-0 ; 
       fig32_belter_stealth-cyl1_1_1.1-0 ; 
       fig32_belter_stealth-cyl11.1-0 ; 
       fig32_belter_stealth-cyl11_1.1-0 ; 
       fig32_belter_stealth-cyl14.1-0 ; 
       fig32_belter_stealth-extru2.1-0 ; 
       fig32_belter_stealth-extru2_1.1-0 ; 
       fig32_belter_stealth-extru3.1-0 ; 
       fig32_belter_stealth-extru3_1.1-0 ; 
       fig32_belter_stealth-lsmoke.1-0 ; 
       fig32_belter_stealth-lthrust.1-0 ; 
       fig32_belter_stealth-lwepemt.1-0 ; 
       fig32_belter_stealth-missemt.1-0 ; 
       fig32_belter_stealth-null1.54-0 ROOT ; 
       fig32_belter_stealth-null1_1.1-0 ROOT ; 
       fig32_belter_stealth-rsmoke.1-0 ; 
       fig32_belter_stealth-rthrust.1-0 ; 
       fig32_belter_stealth-rwepemt.1-0 ; 
       fig32_belter_stealth-sphere2.1-0 ; 
       fig32_belter_stealth-sphere2_1.1-0 ; 
       fig32_belter_stealth-sphere3.1-0 ; 
       fig32_belter_stealth-sphere3_1.1-0 ; 
       fig32_belter_stealth-SS01.1-0 ; 
       fig32_belter_stealth-SS02.1-0 ; 
       fig32_belter_stealth-SS03.1-0 ; 
       fig32_belter_stealth-SS04.1-0 ; 
       fig32_belter_stealth-SS05.1-0 ; 
       fig32_belter_stealth-SS06.1-0 ; 
       fig32_belter_stealth-SS07.1-0 ; 
       fig32_belter_stealth-SS08.1-0 ; 
       fig32_belter_stealth-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig32/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-fig32-belter_stealth.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 60     
       fig32_belter_stealth-t2d1.6-0 ; 
       fig32_belter_stealth-t2d1_1.1-0 ; 
       fig32_belter_stealth-t2d10.5-0 ; 
       fig32_belter_stealth-t2d10_1.1-0 ; 
       fig32_belter_stealth-t2d11.4-0 ; 
       fig32_belter_stealth-t2d11_1.1-0 ; 
       fig32_belter_stealth-t2d12.3-0 ; 
       fig32_belter_stealth-t2d12_1.1-0 ; 
       fig32_belter_stealth-t2d13.8-0 ; 
       fig32_belter_stealth-t2d13_1.1-0 ; 
       fig32_belter_stealth-t2d14.5-0 ; 
       fig32_belter_stealth-t2d14_1.1-0 ; 
       fig32_belter_stealth-t2d15.4-0 ; 
       fig32_belter_stealth-t2d15_1.1-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig32_belter_stealth-t2d16_1.1-0 ; 
       fig32_belter_stealth-t2d19.3-0 ; 
       fig32_belter_stealth-t2d19_1.1-0 ; 
       fig32_belter_stealth-t2d20.1-0 ; 
       fig32_belter_stealth-t2d20_1.1-0 ; 
       fig32_belter_stealth-t2d21.1-0 ; 
       fig32_belter_stealth-t2d21_1.1-0 ; 
       fig32_belter_stealth-t2d22.2-0 ; 
       fig32_belter_stealth-t2d22_1.1-0 ; 
       fig32_belter_stealth-t2d23.2-0 ; 
       fig32_belter_stealth-t2d23_1.1-0 ; 
       fig32_belter_stealth-t2d24.1-0 ; 
       fig32_belter_stealth-t2d24_1.1-0 ; 
       fig32_belter_stealth-t2d25.1-0 ; 
       fig32_belter_stealth-t2d25_1.1-0 ; 
       fig32_belter_stealth-t2d26.1-0 ; 
       fig32_belter_stealth-t2d26_1.1-0 ; 
       fig32_belter_stealth-t2d27.1-0 ; 
       fig32_belter_stealth-t2d27_1.1-0 ; 
       fig32_belter_stealth-t2d28.1-0 ; 
       fig32_belter_stealth-t2d28_1.1-0 ; 
       fig32_belter_stealth-t2d29.1-0 ; 
       fig32_belter_stealth-t2d29_1.1-0 ; 
       fig32_belter_stealth-t2d3.4-0 ; 
       fig32_belter_stealth-t2d3_1.1-0 ; 
       fig32_belter_stealth-t2d30.2-0 ; 
       fig32_belter_stealth-t2d31.1-0 ; 
       fig32_belter_stealth-t2d32.1-0 ; 
       fig32_belter_stealth-t2d32_1.1-0 ; 
       fig32_belter_stealth-t2d33.1-0 ; 
       fig32_belter_stealth-t2d33_1.1-0 ; 
       fig32_belter_stealth-t2d34.1-0 ; 
       fig32_belter_stealth-t2d34_1.1-0 ; 
       fig32_belter_stealth-t2d35.1-0 ; 
       fig32_belter_stealth-t2d35_1.1-0 ; 
       fig32_belter_stealth-t2d5.3-0 ; 
       fig32_belter_stealth-t2d5_1.1-0 ; 
       fig32_belter_stealth-t2d6.3-0 ; 
       fig32_belter_stealth-t2d6_1.1-0 ; 
       fig32_belter_stealth-t2d7.5-0 ; 
       fig32_belter_stealth-t2d7_1.1-0 ; 
       fig32_belter_stealth-t2d8.3-0 ; 
       fig32_belter_stealth-t2d8_1.1-0 ; 
       fig32_belter_stealth-t2d9.6-0 ; 
       fig32_belter_stealth-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 37 110 ; 
       1 31 110 ; 
       3 1 110 ; 
       5 21 110 ; 
       7 21 110 ; 
       9 29 110 ; 
       11 29 110 ; 
       13 29 110 ; 
       15 29 110 ; 
       17 1 110 ; 
       19 1 110 ; 
       21 31 110 ; 
       23 15 110 ; 
       24 21 110 ; 
       26 1 110 ; 
       28 15 110 ; 
       29 37 110 ; 
       31 29 110 ; 
       33 37 110 ; 
       34 37 110 ; 
       35 37 110 ; 
       36 37 110 ; 
       39 37 110 ; 
       40 37 110 ; 
       41 37 110 ; 
       42 1 110 ; 
       44 1 110 ; 
       46 37 110 ; 
       47 37 110 ; 
       48 37 110 ; 
       49 37 110 ; 
       50 37 110 ; 
       51 37 110 ; 
       52 37 110 ; 
       53 37 110 ; 
       54 37 110 ; 
       2 32 110 ; 
       4 2 110 ; 
       6 22 110 ; 
       8 22 110 ; 
       10 30 110 ; 
       12 30 110 ; 
       14 30 110 ; 
       16 30 110 ; 
       18 2 110 ; 
       20 2 110 ; 
       22 32 110 ; 
       25 22 110 ; 
       27 2 110 ; 
       30 38 110 ; 
       32 30 110 ; 
       43 2 110 ; 
       45 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 50 300 ; 
       1 54 300 ; 
       1 56 300 ; 
       1 14 300 ; 
       3 26 300 ; 
       5 34 300 ; 
       7 36 300 ; 
       9 38 300 ; 
       11 44 300 ; 
       11 46 300 ; 
       13 10 300 ; 
       13 12 300 ; 
       15 28 300 ; 
       17 22 300 ; 
       19 24 300 ; 
       21 60 300 ; 
       21 62 300 ; 
       21 64 300 ; 
       23 30 300 ; 
       24 58 300 ; 
       24 66 300 ; 
       26 32 300 ; 
       28 31 300 ; 
       29 40 300 ; 
       29 42 300 ; 
       29 8 300 ; 
       31 48 300 ; 
       31 52 300 ; 
       31 16 300 ; 
       42 18 300 ; 
       44 20 300 ; 
       46 0 300 ; 
       47 1 300 ; 
       48 3 300 ; 
       49 2 300 ; 
       50 5 300 ; 
       51 4 300 ; 
       52 6 300 ; 
       53 7 300 ; 
       2 51 300 ; 
       2 55 300 ; 
       2 57 300 ; 
       2 15 300 ; 
       4 27 300 ; 
       6 35 300 ; 
       8 37 300 ; 
       10 39 300 ; 
       12 45 300 ; 
       12 47 300 ; 
       14 11 300 ; 
       14 13 300 ; 
       16 29 300 ; 
       18 23 300 ; 
       20 25 300 ; 
       22 61 300 ; 
       22 63 300 ; 
       22 65 300 ; 
       25 59 300 ; 
       25 67 300 ; 
       27 33 300 ; 
       30 41 300 ; 
       30 43 300 ; 
       30 9 300 ; 
       32 49 300 ; 
       32 53 300 ; 
       32 17 300 ; 
       43 19 300 ; 
       45 21 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 16 401 ; 
       10 18 401 ; 
       12 20 401 ; 
       14 22 401 ; 
       16 24 401 ; 
       18 26 401 ; 
       20 28 401 ; 
       22 30 401 ; 
       24 32 401 ; 
       26 34 401 ; 
       28 36 401 ; 
       30 40 401 ; 
       31 41 401 ; 
       32 44 401 ; 
       34 46 401 ; 
       36 48 401 ; 
       38 42 401 ; 
       40 0 401 ; 
       42 38 401 ; 
       44 50 401 ; 
       46 52 401 ; 
       48 54 401 ; 
       50 58 401 ; 
       52 56 401 ; 
       54 2 401 ; 
       56 4 401 ; 
       58 6 401 ; 
       60 8 401 ; 
       62 10 401 ; 
       64 12 401 ; 
       66 14 401 ; 
       9 17 401 ; 
       11 19 401 ; 
       13 21 401 ; 
       15 23 401 ; 
       17 25 401 ; 
       19 27 401 ; 
       21 29 401 ; 
       23 31 401 ; 
       25 33 401 ; 
       27 35 401 ; 
       29 37 401 ; 
       33 45 401 ; 
       35 47 401 ; 
       37 49 401 ; 
       39 43 401 ; 
       41 1 401 ; 
       43 39 401 ; 
       45 51 401 ; 
       47 53 401 ; 
       49 55 401 ; 
       51 59 401 ; 
       53 57 401 ; 
       55 3 401 ; 
       57 5 401 ; 
       59 7 401 ; 
       61 9 401 ; 
       63 11 401 ; 
       65 13 401 ; 
       67 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 55 -8 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       13 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 MPRFLG 0 ; 
       21 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 100 -6 0 MPRFLG 0 ; 
       24 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       26 SCHEM 75 -8 0 MPRFLG 0 ; 
       28 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       33 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       39 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 70 -8 0 MPRFLG 0 ; 
       44 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       46 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 158.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 157 -8 0 MPRFLG 0 ; 
       6 SCHEM 134.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 137 -8 0 MPRFLG 0 ; 
       10 SCHEM 182 -4 0 MPRFLG 0 ; 
       12 SCHEM 125.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 185.75 -4 0 MPRFLG 0 ; 
       16 SCHEM 179.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 152 -8 0 MPRFLG 0 ; 
       20 SCHEM 159.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 137 -6 0 MPRFLG 0 ; 
       25 SCHEM 130.75 -8 0 MPRFLG 0 ; 
       27 SCHEM 154.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 159.5 -2 0 MPRFLG 0 ; 
       32 SCHEM 153.25 -4 0 MPRFLG 0 ; 
       38 SCHEM 159.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       43 SCHEM 149.5 -8 0 MPRFLG 0 ; 
       45 SCHEM 147 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 194.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 184.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 187 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 169.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 177 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 149.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 147 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 152 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 159.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 157 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 179.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 154.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 134.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 137 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 182 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 189.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 192 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 124.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 127 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 172 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 162 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 174.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 164.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 167 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 129.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 139.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 142 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 144.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 132 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 189.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 164.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 167 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 129.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 139.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 142 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 144.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 132 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 194.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 184.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 187 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 169.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 177 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 149.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 147 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 152 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 159.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 157 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 179.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 192 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 182 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 154.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 134.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 137 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 124.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 127 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 172 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 174.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 162 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
