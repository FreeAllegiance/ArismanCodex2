SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.79-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       fig32_belter_stealth-mat101_1.1-0 ; 
       fig32_belter_stealth-mat102_1.1-0 ; 
       fig32_belter_stealth-mat103_1.1-0 ; 
       fig32_belter_stealth-mat104_1.1-0 ; 
       fig32_belter_stealth-mat105_1.1-0 ; 
       fig32_belter_stealth-mat108_1.1-0 ; 
       fig32_belter_stealth-mat109_1.1-0 ; 
       fig32_belter_stealth-mat110_1.1-0 ; 
       fig32_belter_stealth-mat111_1.1-0 ; 
       fig32_belter_stealth-mat83_1.1-0 ; 
       fig32_belter_stealth-mat85_1.1-0 ; 
       fig32_belter_stealth-mat87_1.1-0 ; 
       fig32_belter_stealth-mat88_1.1-0 ; 
       fig32_belter_stealth-mat89_1.1-0 ; 
       fig32_belter_stealth-mat90_1.1-0 ; 
       fig32_belter_stealth-mat91_1.1-0 ; 
       fig32_belter_stealth-mat92_1.1-0 ; 
       fig32_belter_stealth-mat93_1.1-0 ; 
       fig32_belter_stealth-mat94_1.1-0 ; 
       fig32_belter_stealth-mat95_1.1-0 ; 
       fig32_belter_stealth-mat96_1.1-0 ; 
       fig32_belter_stealth-mat97_1.1-0 ; 
       fig32_belter_stealth-mat98_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       fig32_belter_stealth-cube1_1.1-0 ; 
       fig32_belter_stealth-cube14_1.1-0 ; 
       fig32_belter_stealth-cube18_1.1-0 ; 
       fig32_belter_stealth-cube20_1.1-0 ; 
       fig32_belter_stealth-cube4_1.1-0 ; 
       fig32_belter_stealth-cube7_1_1.1-0 ; 
       fig32_belter_stealth-cube7_3_1.1-0 ; 
       fig32_belter_stealth-cube9_1.1-0 ; 
       fig32_belter_stealth-cyl1_1_1.1-0 ; 
       fig32_belter_stealth-extru2_1.1-0 ; 
       fig32_belter_stealth-extru3_1.1-0 ; 
       fig32_belter_stealth-null1_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig32/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-fig32-belter_stealth.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 23     
       fig32_belter_stealth-t2d1_1.1-0 ; 
       fig32_belter_stealth-t2d10_1.1-0 ; 
       fig32_belter_stealth-t2d11_1.1-0 ; 
       fig32_belter_stealth-t2d12_1.1-0 ; 
       fig32_belter_stealth-t2d13_1.1-0 ; 
       fig32_belter_stealth-t2d14_1.1-0 ; 
       fig32_belter_stealth-t2d15_1.1-0 ; 
       fig32_belter_stealth-t2d16_1.1-0 ; 
       fig32_belter_stealth-t2d19_1.1-0 ; 
       fig32_belter_stealth-t2d20_1.1-0 ; 
       fig32_belter_stealth-t2d21_1.1-0 ; 
       fig32_belter_stealth-t2d22_1.1-0 ; 
       fig32_belter_stealth-t2d23_1.1-0 ; 
       fig32_belter_stealth-t2d26_1.1-0 ; 
       fig32_belter_stealth-t2d27_1.1-0 ; 
       fig32_belter_stealth-t2d28_1.1-0 ; 
       fig32_belter_stealth-t2d29_1.1-0 ; 
       fig32_belter_stealth-t2d3_1.1-0 ; 
       fig32_belter_stealth-t2d5_1.1-0 ; 
       fig32_belter_stealth-t2d6_1.1-0 ; 
       fig32_belter_stealth-t2d7_1.1-0 ; 
       fig32_belter_stealth-t2d8_1.1-0 ; 
       fig32_belter_stealth-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 0 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 3 300 ; 
       1 7 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 8 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       7 19 300 ; 
       7 20 300 ; 
       7 21 300 ; 
       8 18 300 ; 
       8 22 300 ; 
       9 9 300 ; 
       9 10 300 ; 
       9 0 300 ; 
       10 13 300 ; 
       10 15 300 ; 
       10 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       2 10 401 ; 
       3 11 401 ; 
       4 12 401 ; 
       5 13 401 ; 
       6 14 401 ; 
       7 15 401 ; 
       8 16 401 ; 
       9 0 401 ; 
       10 17 401 ; 
       11 18 401 ; 
       12 19 401 ; 
       13 20 401 ; 
       14 22 401 ; 
       15 21 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 3 401 ; 
       19 4 401 ; 
       20 5 401 ; 
       21 6 401 ; 
       22 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
