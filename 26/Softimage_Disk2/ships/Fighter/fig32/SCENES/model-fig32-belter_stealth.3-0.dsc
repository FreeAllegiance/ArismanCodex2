SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       fig32_belter_stealth-cockpt.1-0 ; 
       fig32_belter_stealth-cube1.1-0 ; 
       fig32_belter_stealth-cube14.1-0 ; 
       fig32_belter_stealth-cube15.1-0 ; 
       fig32_belter_stealth-cube16.1-0 ; 
       fig32_belter_stealth-cube4.1-0 ; 
       fig32_belter_stealth-cube7.1-0 ; 
       fig32_belter_stealth-cube7_1.1-0 ; 
       fig32_belter_stealth-cube7_2.1-0 ; 
       fig32_belter_stealth-cube8.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cyl1.1-0 ; 
       fig32_belter_stealth-cyl1_1.1-0 ; 
       fig32_belter_stealth-cyl11.1-0 ; 
       fig32_belter_stealth-cyl12.1-0 ; 
       fig32_belter_stealth-extru2.1-0 ; 
       fig32_belter_stealth-extru3.1-0 ; 
       fig32_belter_stealth-lsmoke.1-0 ; 
       fig32_belter_stealth-lthrust.1-0 ; 
       fig32_belter_stealth-lwepemt.1-0 ; 
       fig32_belter_stealth-missemt.1-0 ; 
       fig32_belter_stealth-null1.3-0 ROOT ; 
       fig32_belter_stealth-rsmoke.1-0 ; 
       fig32_belter_stealth-rthrust.1-0 ; 
       fig32_belter_stealth-rwepemt.1-0 ; 
       fig32_belter_stealth-sphere1.1-0 ; 
       fig32_belter_stealth-sphere2.1-0 ; 
       fig32_belter_stealth-SS01.1-0 ; 
       fig32_belter_stealth-SS02.1-0 ; 
       fig32_belter_stealth-SS03.1-0 ; 
       fig32_belter_stealth-SS04.1-0 ; 
       fig32_belter_stealth-SS05.1-0 ; 
       fig32_belter_stealth-SS06.1-0 ; 
       fig32_belter_stealth-SS07.1-0 ; 
       fig32_belter_stealth-SS08.1-0 ; 
       fig32_belter_stealth-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig32-belter_stealth.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 21 110 ; 
       1 16 110 ; 
       2 1 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 15 110 ; 
       6 15 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 15 110 ; 
       10 16 110 ; 
       11 5 110 ; 
       12 10 110 ; 
       13 1 110 ; 
       14 5 110 ; 
       15 21 110 ; 
       16 15 110 ; 
       17 21 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
       32 21 110 ; 
       33 21 110 ; 
       34 21 110 ; 
       35 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       27 0 300 ; 
       28 1 300 ; 
       29 3 300 ; 
       30 2 300 ; 
       31 5 300 ; 
       32 4 300 ; 
       33 6 300 ; 
       34 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 213.396 18.6328 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 239.9264 20.83182 0 MPRFLG 0 ; 
       2 SCHEM 246.1764 18.83182 0 MPRFLG 0 ; 
       3 SCHEM 228.6764 18.83182 0 MPRFLG 0 ; 
       4 SCHEM 231.1764 18.83182 0 MPRFLG 0 ; 
       5 SCHEM 249.9264 22.83182 0 MPRFLG 0 ; 
       6 SCHEM 221.1764 22.83182 0 MPRFLG 0 ; 
       7 SCHEM 238.6764 18.83182 0 MPRFLG 0 ; 
       8 SCHEM 241.1764 18.83182 0 MPRFLG 0 ; 
       9 SCHEM 223.6764 22.83182 0 MPRFLG 0 ; 
       10 SCHEM 228.6764 20.83182 0 MPRFLG 0 ; 
       11 SCHEM 248.6764 20.83182 0 MPRFLG 0 ; 
       12 SCHEM 226.1764 18.83182 0 MPRFLG 0 ; 
       13 SCHEM 243.6764 18.83182 0 MPRFLG 0 ; 
       14 SCHEM 251.1764 20.83182 0 MPRFLG 0 ; 
       15 SCHEM 236.1764 24.83182 0 USR MPRFLG 0 ; 
       16 SCHEM 236.1764 22.83182 0 MPRFLG 0 ; 
       17 SCHEM 202.7855 18.23767 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 202.7138 17.43832 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 215.9495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 213.4495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 209.1263 31.62712 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 207.3958 18.49347 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 207.388 17.53425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 210.9495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 233.6764 18.83182 0 MPRFLG 0 ; 
       26 SCHEM 236.1764 18.83182 0 MPRFLG 0 ; 
       27 SCHEM 200.1659 27.98838 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 200.2286 27.22221 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 202.6032 28.04122 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 202.5602 27.19579 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 205.024 27.93554 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 205.067 27.22221 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 207.3963 27.86395 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 207.3305 27.20214 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 204.9408 19.76738 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
