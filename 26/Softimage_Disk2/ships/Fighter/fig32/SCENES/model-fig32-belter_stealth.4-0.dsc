SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.6-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       fig32_belter_stealth-cockpt.1-0 ; 
       fig32_belter_stealth-cube1.1-0 ; 
       fig32_belter_stealth-cube14.1-0 ; 
       fig32_belter_stealth-cube15.1-0 ; 
       fig32_belter_stealth-cube16.1-0 ; 
       fig32_belter_stealth-cube17.1-0 ; 
       fig32_belter_stealth-cube4.1-0 ; 
       fig32_belter_stealth-cube7.1-0 ; 
       fig32_belter_stealth-cube7_1.1-0 ; 
       fig32_belter_stealth-cube7_2.1-0 ; 
       fig32_belter_stealth-cube8.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cyl1.1-0 ; 
       fig32_belter_stealth-cyl1_1.1-0 ; 
       fig32_belter_stealth-cyl11.1-0 ; 
       fig32_belter_stealth-cyl12.1-0 ; 
       fig32_belter_stealth-extru2.1-0 ; 
       fig32_belter_stealth-extru3.1-0 ; 
       fig32_belter_stealth-lsmoke.1-0 ; 
       fig32_belter_stealth-lthrust.1-0 ; 
       fig32_belter_stealth-lwepemt.1-0 ; 
       fig32_belter_stealth-missemt.1-0 ; 
       fig32_belter_stealth-null1.4-0 ROOT ; 
       fig32_belter_stealth-rsmoke.1-0 ; 
       fig32_belter_stealth-rthrust.1-0 ; 
       fig32_belter_stealth-rwepemt.1-0 ; 
       fig32_belter_stealth-sphere1.1-0 ; 
       fig32_belter_stealth-sphere2.1-0 ; 
       fig32_belter_stealth-SS01.1-0 ; 
       fig32_belter_stealth-SS02.1-0 ; 
       fig32_belter_stealth-SS03.1-0 ; 
       fig32_belter_stealth-SS04.1-0 ; 
       fig32_belter_stealth-SS05.1-0 ; 
       fig32_belter_stealth-SS06.1-0 ; 
       fig32_belter_stealth-SS07.1-0 ; 
       fig32_belter_stealth-SS08.1-0 ; 
       fig32_belter_stealth-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig32-belter_stealth.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 16 110 ; 
       0 22 110 ; 
       1 17 110 ; 
       2 1 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 16 110 ; 
       11 17 110 ; 
       12 6 110 ; 
       13 11 110 ; 
       14 1 110 ; 
       15 6 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 22 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 22 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 22 110 ; 
       35 22 110 ; 
       36 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       28 0 300 ; 
       29 1 300 ; 
       30 3 300 ; 
       31 2 300 ; 
       32 5 300 ; 
       33 4 300 ; 
       34 6 300 ; 
       35 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 252.4264 22.83182 0 MPRFLG 0 ; 
       0 SCHEM 213.396 18.6328 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 238.6764 20.83182 0 MPRFLG 0 ; 
       2 SCHEM 244.9264 18.83182 0 MPRFLG 0 ; 
       3 SCHEM 227.4264 18.83182 0 MPRFLG 0 ; 
       4 SCHEM 229.9264 18.83182 0 MPRFLG 0 ; 
       6 SCHEM 248.6764 22.83182 0 MPRFLG 0 ; 
       7 SCHEM 219.9264 22.83182 0 MPRFLG 0 ; 
       8 SCHEM 237.4264 18.83182 0 MPRFLG 0 ; 
       9 SCHEM 239.9264 18.83182 0 MPRFLG 0 ; 
       10 SCHEM 222.4264 22.83182 0 MPRFLG 0 ; 
       11 SCHEM 227.4264 20.83182 0 MPRFLG 0 ; 
       12 SCHEM 247.4264 20.83182 0 MPRFLG 0 ; 
       13 SCHEM 224.9264 18.83182 0 MPRFLG 0 ; 
       14 SCHEM 242.4264 18.83182 0 MPRFLG 0 ; 
       15 SCHEM 249.9264 20.83182 0 MPRFLG 0 ; 
       16 SCHEM 236.1764 24.83182 0 USR MPRFLG 0 ; 
       17 SCHEM 234.9264 22.83182 0 MPRFLG 0 ; 
       18 SCHEM 202.7855 18.23767 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 202.7138 17.43832 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 215.9495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 213.4495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 209.1263 31.62712 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 207.3958 18.49347 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 207.388 17.53425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 210.9495 17.51613 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 232.4264 18.83182 0 MPRFLG 0 ; 
       27 SCHEM 234.9264 18.83182 0 MPRFLG 0 ; 
       28 SCHEM 200.1659 27.98838 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 200.2286 27.22221 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 202.6032 28.04122 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 202.5602 27.19579 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 205.024 27.93554 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 205.067 27.22221 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 207.3963 27.86395 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 207.3305 27.20214 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 204.9408 19.76738 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
