SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.75-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
       fig32_belter_stealth-mat101.1-0 ; 
       fig32_belter_stealth-mat102.1-0 ; 
       fig32_belter_stealth-mat103.1-0 ; 
       fig32_belter_stealth-mat104.1-0 ; 
       fig32_belter_stealth-mat105.1-0 ; 
       fig32_belter_stealth-mat106.1-0 ; 
       fig32_belter_stealth-mat107.1-0 ; 
       fig32_belter_stealth-mat108.1-0 ; 
       fig32_belter_stealth-mat109.1-0 ; 
       fig32_belter_stealth-mat110.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat114.1-0 ; 
       fig32_belter_stealth-mat115.1-0 ; 
       fig32_belter_stealth-mat116.1-0 ; 
       fig32_belter_stealth-mat117.1-0 ; 
       fig32_belter_stealth-mat83.4-0 ; 
       fig32_belter_stealth-mat85.3-0 ; 
       fig32_belter_stealth-mat87.1-0 ; 
       fig32_belter_stealth-mat88.1-0 ; 
       fig32_belter_stealth-mat89.3-0 ; 
       fig32_belter_stealth-mat90.5-0 ; 
       fig32_belter_stealth-mat91.2-0 ; 
       fig32_belter_stealth-mat92.3-0 ; 
       fig32_belter_stealth-mat93.2-0 ; 
       fig32_belter_stealth-mat94.2-0 ; 
       fig32_belter_stealth-mat95.3-0 ; 
       fig32_belter_stealth-mat96.2-0 ; 
       fig32_belter_stealth-mat97.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       fig32_belter_stealth-cockpt.1-0 ; 
       fig32_belter_stealth-cube1.1-0 ; 
       fig32_belter_stealth-cube14.1-0 ; 
       fig32_belter_stealth-cube15.1-0 ; 
       fig32_belter_stealth-cube16.1-0 ; 
       fig32_belter_stealth-cube17.1-0 ; 
       fig32_belter_stealth-cube18.1-0 ; 
       fig32_belter_stealth-cube20.1-0 ; 
       fig32_belter_stealth-cube4.1-0 ; 
       fig32_belter_stealth-cube7_1.1-0 ; 
       fig32_belter_stealth-cube7_3.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cyl1.1-0 ; 
       fig32_belter_stealth-cyl1_1.1-0 ; 
       fig32_belter_stealth-cyl11.1-0 ; 
       fig32_belter_stealth-cyl14.1-0 ; 
       fig32_belter_stealth-extru2.1-0 ; 
       fig32_belter_stealth-extru3.1-0 ; 
       fig32_belter_stealth-lsmoke.1-0 ; 
       fig32_belter_stealth-lthrust.1-0 ; 
       fig32_belter_stealth-lwepemt.1-0 ; 
       fig32_belter_stealth-missemt.1-0 ; 
       fig32_belter_stealth-null1.52-0 ROOT ; 
       fig32_belter_stealth-rsmoke.1-0 ; 
       fig32_belter_stealth-rthrust.1-0 ; 
       fig32_belter_stealth-rwepemt.1-0 ; 
       fig32_belter_stealth-sphere2.1-0 ; 
       fig32_belter_stealth-sphere3.1-0 ; 
       fig32_belter_stealth-SS01.1-0 ; 
       fig32_belter_stealth-SS02.1-0 ; 
       fig32_belter_stealth-SS03.1-0 ; 
       fig32_belter_stealth-SS04.1-0 ; 
       fig32_belter_stealth-SS05.1-0 ; 
       fig32_belter_stealth-SS06.1-0 ; 
       fig32_belter_stealth-SS07.1-0 ; 
       fig32_belter_stealth-SS08.1-0 ; 
       fig32_belter_stealth-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig32/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig32-belter_stealth.69-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       fig32_belter_stealth-t2d1.6-0 ; 
       fig32_belter_stealth-t2d10.5-0 ; 
       fig32_belter_stealth-t2d11.4-0 ; 
       fig32_belter_stealth-t2d12.3-0 ; 
       fig32_belter_stealth-t2d13.8-0 ; 
       fig32_belter_stealth-t2d14.5-0 ; 
       fig32_belter_stealth-t2d15.4-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig32_belter_stealth-t2d19.3-0 ; 
       fig32_belter_stealth-t2d20.1-0 ; 
       fig32_belter_stealth-t2d21.1-0 ; 
       fig32_belter_stealth-t2d22.2-0 ; 
       fig32_belter_stealth-t2d23.2-0 ; 
       fig32_belter_stealth-t2d24.1-0 ; 
       fig32_belter_stealth-t2d25.1-0 ; 
       fig32_belter_stealth-t2d26.1-0 ; 
       fig32_belter_stealth-t2d27.1-0 ; 
       fig32_belter_stealth-t2d28.1-0 ; 
       fig32_belter_stealth-t2d29.1-0 ; 
       fig32_belter_stealth-t2d3.4-0 ; 
       fig32_belter_stealth-t2d30.2-0 ; 
       fig32_belter_stealth-t2d31.1-0 ; 
       fig32_belter_stealth-t2d32.1-0 ; 
       fig32_belter_stealth-t2d33.1-0 ; 
       fig32_belter_stealth-t2d34.1-0 ; 
       fig32_belter_stealth-t2d35.1-0 ; 
       fig32_belter_stealth-t2d5.3-0 ; 
       fig32_belter_stealth-t2d6.3-0 ; 
       fig32_belter_stealth-t2d7.5-0 ; 
       fig32_belter_stealth-t2d8.3-0 ; 
       fig32_belter_stealth-t2d9.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 17 110 ; 
       2 1 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 17 110 ; 
       12 8 110 ; 
       13 11 110 ; 
       14 1 110 ; 
       15 8 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 22 110 ; 
       29 22 110 ; 
       30 22 110 ; 
       31 22 110 ; 
       32 22 110 ; 
       33 22 110 ; 
       34 22 110 ; 
       35 22 110 ; 
       36 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 30 300 ; 
       1 32 300 ; 
       1 33 300 ; 
       1 11 300 ; 
       2 17 300 ; 
       3 22 300 ; 
       4 23 300 ; 
       5 24 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       8 18 300 ; 
       9 15 300 ; 
       10 16 300 ; 
       11 35 300 ; 
       11 36 300 ; 
       11 37 300 ; 
       12 19 300 ; 
       13 34 300 ; 
       13 38 300 ; 
       14 21 300 ; 
       15 20 300 ; 
       16 25 300 ; 
       16 26 300 ; 
       16 8 300 ; 
       17 29 300 ; 
       17 31 300 ; 
       17 12 300 ; 
       26 13 300 ; 
       27 14 300 ; 
       28 0 300 ; 
       29 1 300 ; 
       30 3 300 ; 
       31 2 300 ; 
       32 5 300 ; 
       33 4 300 ; 
       34 6 300 ; 
       35 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 20 401 ; 
       20 21 401 ; 
       21 23 401 ; 
       22 24 401 ; 
       23 25 401 ; 
       24 22 401 ; 
       25 0 401 ; 
       26 19 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 30 401 ; 
       31 29 401 ; 
       32 1 401 ; 
       33 2 401 ; 
       34 3 401 ; 
       35 4 401 ; 
       36 5 401 ; 
       37 6 401 ; 
       38 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 78.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 77.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 55 -8 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 80 -8 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 100 -6 0 MPRFLG 0 ; 
       13 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 75 -8 0 MPRFLG 0 ; 
       15 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 73.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 6.975916e-009 1.211629e-008 MPRFLG 0 ; 
       23 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 70 -8 0 MPRFLG 0 ; 
       27 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
