SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.1-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       fig31_belter_scout1-cube4.1-0 ROOT ; 
       fig31_belter_scout1-cyl1.1-0 ; 
       fig32_belter_stealth-cube5.1-0 ; 
       fig32_belter_stealth-cube6.1-0 ; 
       fig32_belter_stealth-cube7.1-0 ; 
       fig32_belter_stealth-cube8.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cyl3.1-0 ; 
       fig32_belter_stealth-extru1.1-0 ROOT ; 
       fig32_belter_stealth-spline1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig32-belter_stealth.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 3 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 3 110 ; 
       7 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 80.718 14.5967 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 80.718 12.5967 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 106.4512 9.624887 0 USR SRT 0.2356028 0.4076175 0.4076175 0.65 0 3.141593 0 -4.637105 14.59307 MPRFLG 0 ; 
       1 SCHEM 106.4512 7.624889 0 USR MPRFLG 0 ; 
       9 SCHEM 92.33682 14.12467 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 90.06841 12.31267 0 USR SRT 0.7355042 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 86.31841 8.312675 0 MPRFLG 0 ; 
       3 SCHEM 87.56841 10.31267 0 MPRFLG 0 ; 
       4 SCHEM 91.31841 10.31267 0 MPRFLG 0 ; 
       5 SCHEM 93.81841 10.31267 0 MPRFLG 0 ; 
       6 SCHEM 88.81841 8.312675 0 MPRFLG 0 ; 
       7 SCHEM 88.81841 6.312675 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
