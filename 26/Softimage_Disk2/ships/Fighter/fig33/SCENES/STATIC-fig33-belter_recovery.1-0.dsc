SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.69-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       fig33_belter_recovery-mat100.1-0 ; 
       fig33_belter_recovery-mat101.1-0 ; 
       fig33_belter_recovery-mat102.2-0 ; 
       fig33_belter_recovery-mat103.1-0 ; 
       fig33_belter_recovery-mat104.2-0 ; 
       fig33_belter_recovery-mat105.1-0 ; 
       fig33_belter_recovery-mat106.1-0 ; 
       fig33_belter_recovery-mat107.1-0 ; 
       fig33_belter_recovery-mat108.1-0 ; 
       fig33_belter_recovery-mat109.1-0 ; 
       fig33_belter_recovery-mat110.1-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.1-0 ; 
       fig33_belter_recovery-mat113.1-0 ; 
       fig33_belter_recovery-mat114.1-0 ; 
       fig33_belter_recovery-mat115.1-0 ; 
       fig33_belter_recovery-mat116.1-0 ; 
       fig33_belter_recovery-mat117.1-0 ; 
       fig33_belter_recovery-mat118.1-0 ; 
       fig33_belter_recovery-mat83.3-0 ; 
       fig33_belter_recovery-mat84.3-0 ; 
       fig33_belter_recovery-mat85.2-0 ; 
       fig33_belter_recovery-mat86.2-0 ; 
       fig33_belter_recovery-mat87.2-0 ; 
       fig33_belter_recovery-mat88.2-0 ; 
       fig33_belter_recovery-mat89.2-0 ; 
       fig33_belter_recovery-mat90.1-0 ; 
       fig33_belter_recovery-mat91.2-0 ; 
       fig33_belter_recovery-mat92.1-0 ; 
       fig33_belter_recovery-mat93.3-0 ; 
       fig33_belter_recovery-mat94.2-0 ; 
       fig33_belter_recovery-mat95.1-0 ; 
       fig33_belter_recovery-mat96.2-0 ; 
       fig33_belter_recovery-mat97.2-0 ; 
       fig33_belter_recovery-mat98.1-0 ; 
       fig33_belter_recovery-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube17.1-0 ; 
       fig33_belter_recovery-cube18.1-0 ; 
       fig33_belter_recovery-cube19.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.3-0 ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl1_9.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-null1.51-0 ROOT ; 
       fig33_belter_recovery-sphere5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig33/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-fig33-belter_recovery.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       fig33_belter_recovery-t2d1.3-0 ; 
       fig33_belter_recovery-t2d10.2-0 ; 
       fig33_belter_recovery-t2d11.6-0 ; 
       fig33_belter_recovery-t2d12.5-0 ; 
       fig33_belter_recovery-t2d13.2-0 ; 
       fig33_belter_recovery-t2d14.3-0 ; 
       fig33_belter_recovery-t2d15.3-0 ; 
       fig33_belter_recovery-t2d16.1-0 ; 
       fig33_belter_recovery-t2d17.2-0 ; 
       fig33_belter_recovery-t2d18.2-0 ; 
       fig33_belter_recovery-t2d19.2-0 ; 
       fig33_belter_recovery-t2d2.4-0 ; 
       fig33_belter_recovery-t2d20.4-0 ; 
       fig33_belter_recovery-t2d21.2-0 ; 
       fig33_belter_recovery-t2d22.2-0 ; 
       fig33_belter_recovery-t2d23.1-0 ; 
       fig33_belter_recovery-t2d24.1-0 ; 
       fig33_belter_recovery-t2d25.1-0 ; 
       fig33_belter_recovery-t2d26.1-0 ; 
       fig33_belter_recovery-t2d27.1-0 ; 
       fig33_belter_recovery-t2d28.1-0 ; 
       fig33_belter_recovery-t2d29.4-0 ; 
       fig33_belter_recovery-t2d3.3-0 ; 
       fig33_belter_recovery-t2d30.1-0 ; 
       fig33_belter_recovery-t2d31.1-0 ; 
       fig33_belter_recovery-t2d32.1-0 ; 
       fig33_belter_recovery-t2d33.1-0 ; 
       fig33_belter_recovery-t2d34.1-0 ; 
       fig33_belter_recovery-t2d35.1-0 ; 
       fig33_belter_recovery-t2d36.2-0 ; 
       fig33_belter_recovery-t2d4.2-0 ; 
       fig33_belter_recovery-t2d5.3-0 ; 
       fig33_belter_recovery-t2d6.2-0 ; 
       fig33_belter_recovery-t2d7.2-0 ; 
       fig33_belter_recovery-t2d8.2-0 ; 
       fig33_belter_recovery-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 18 110 ; 
       9 10 110 ; 
       10 20 110 ; 
       11 10 110 ; 
       12 18 110 ; 
       13 16 110 ; 
       14 21 110 ; 
       15 16 110 ; 
       16 19 110 ; 
       17 10 110 ; 
       18 9 110 ; 
       19 10 110 ; 
       21 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 35 300 ; 
       1 0 300 ; 
       2 1 300 ; 
       3 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       6 17 300 ; 
       7 18 300 ; 
       8 10 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       10 19 300 ; 
       10 21 300 ; 
       10 23 300 ; 
       10 26 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       12 9 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       14 32 300 ; 
       14 8 300 ; 
       15 6 300 ; 
       15 7 300 ; 
       16 2 300 ; 
       16 3 300 ; 
       17 20 300 ; 
       17 22 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       18 29 300 ; 
       18 30 300 ; 
       18 16 300 ; 
       19 33 300 ; 
       19 34 300 ; 
       21 31 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 10 401 ; 
       2 12 401 ; 
       3 13 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 17 401 ; 
       8 18 401 ; 
       9 19 401 ; 
       10 20 401 ; 
       11 21 401 ; 
       12 23 401 ; 
       13 24 401 ; 
       14 25 401 ; 
       15 26 401 ; 
       16 27 401 ; 
       17 28 401 ; 
       18 29 401 ; 
       19 11 401 ; 
       20 0 401 ; 
       21 22 401 ; 
       22 30 401 ; 
       23 31 401 ; 
       24 32 401 ; 
       25 33 401 ; 
       26 34 401 ; 
       27 35 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       31 4 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
       35 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -6 0 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 45 -8 0 MPRFLG 0 ; 
       3 SCHEM 70 -4 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 75 -4 0 MPRFLG 0 ; 
       6 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 80 -4 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 28.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 MPRFLG 0 ; 
       13 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 56.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       17 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 MPRFLG 0 ; 
       19 SCHEM 55 -4 0 MPRFLG 0 ; 
       20 SCHEM 46.25 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 20 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
