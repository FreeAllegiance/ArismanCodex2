SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.4-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.3-0 ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube8.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.1-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig33-belter_recovery.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       34 25 110 ; 
       35 25 110 ; 
       37 25 110 ; 
       37 38 111 ; 
       3 20 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       9 10 110 ; 
       10 25 110 ; 
       11 9 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 27 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 9 110 ; 
       20 10 110 ; 
       27 9 110 ; 
       1 25 110 ; 
       2 25 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       21 25 110 ; 
       22 25 110 ; 
       23 25 110 ; 
       24 25 110 ; 
       24 26 111 ; 
       24 23 111 ; 
       0 25 110 ; 
       0 1 111 ; 
       26 25 110 ; 
       28 25 110 ; 
       29 25 110 ; 
       30 25 110 ; 
       31 25 110 ; 
       32 25 110 ; 
       33 25 110 ; 
       36 25 110 ; 
       38 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       34 6 300 ; 
       35 7 300 ; 
       28 0 300 ; 
       29 1 300 ; 
       30 3 300 ; 
       31 2 300 ; 
       32 5 300 ; 
       33 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       34 SCHEM 12.45372 -1.769249 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 12.40374 -2.49561 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 26.84687 -0.2979131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.3517 9.293587 0 USR SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 21.5111 -6.531021 0 MPRFLG 0 ; 
       4 SCHEM 20.2611 -8.531019 0 MPRFLG 0 ; 
       5 SCHEM 22.7611 -8.531019 0 MPRFLG 0 ; 
       9 SCHEM 11.51111 -4.531019 0 MPRFLG 0 ; 
       10 SCHEM 16.51111 -2.531023 0 USR MPRFLG 0 ; 
       11 SCHEM 5.261109 -6.531021 0 MPRFLG 0 ; 
       12 SCHEM 15.26112 -8.531019 0 MPRFLG 0 ; 
       13 SCHEM 17.76111 -8.531019 0 MPRFLG 0 ; 
       14 SCHEM 7.761118 -8.531019 0 MPRFLG 0 ; 
       15 SCHEM 10.26111 -8.531019 0 MPRFLG 0 ; 
       16 SCHEM 12.76111 -8.531019 0 MPRFLG 0 ; 
       17 SCHEM 9.011117 -6.531021 0 MPRFLG 0 ; 
       18 SCHEM 2.761109 -4.531019 0 MPRFLG 0 ; 
       19 SCHEM 16.51111 -6.531021 0 MPRFLG 0 ; 
       20 SCHEM 21.5111 -4.531019 0 MPRFLG 0 ; 
       27 SCHEM 12.76111 -6.531021 0 MPRFLG 0 ; 
       1 SCHEM 24.67723 -1.010641 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20.58495 2.664433 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 25.26108 -4.531019 0 MPRFLG 0 ; 
       7 SCHEM 27.76107 -4.531019 0 MPRFLG 0 ; 
       8 SCHEM 30.26108 -4.531019 0 MPRFLG 0 ; 
       21 SCHEM 19.56557 -0.3415046 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19.50867 -1.01402 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 28.01717 3.233047 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.84997 2.254242 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 26.87655 -0.9366774 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 27.85992 1.289688 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5.350509 -1.832631 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 5.390552 -2.47222 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 7.669669 -1.778944 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 7.562115 -2.418534 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 10.0499 -1.736948 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 10.01952 -2.434884 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 20.56998 3.473445 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24.72662 -0.2879605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64.77222 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.3026 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
