SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.6-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
       fig33_belter_recovery-mat81.1-0 ; 
       fig33_belter_recovery-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube17.1-0 ; 
       fig33_belter_recovery-cube18.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.3-0 ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube8.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.3-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig33-belter_recovery.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 12 110 ; 
       10 12 110 ; 
       0 27 110 ; 
       0 1 111 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 22 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       8 12 110 ; 
       11 12 110 ; 
       12 27 110 ; 
       13 12 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 19 110 ; 
       17 19 110 ; 
       18 29 110 ; 
       19 22 110 ; 
       20 12 110 ; 
       21 11 110 ; 
       22 12 110 ; 
       23 27 110 ; 
       24 27 110 ; 
       25 27 110 ; 
       26 27 110 ; 
       26 28 111 ; 
       26 25 111 ; 
       28 27 110 ; 
       29 11 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 27 110 ; 
       34 27 110 ; 
       35 27 110 ; 
       36 27 110 ; 
       37 27 110 ; 
       38 27 110 ; 
       39 27 110 ; 
       39 40 111 ; 
       40 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       30 0 300 ; 
       31 1 300 ; 
       32 3 300 ; 
       33 2 300 ; 
       34 5 300 ; 
       35 4 300 ; 
       36 6 300 ; 
       37 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 30.09487 -5.17657 0 MPRFLG 0 ; 
       10 SCHEM 32.59488 -5.17657 0 MPRFLG 0 ; 
       0 SCHEM 26.87655 -0.9366774 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24.67723 -1.010641 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20.58495 2.664433 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13.84488 -7.17657 0 MPRFLG 0 ; 
       4 SCHEM 12.59488 -9.176573 0 MPRFLG 0 ; 
       5 SCHEM 15.09487 -9.176573 0 MPRFLG 0 ; 
       6 SCHEM 22.59487 -5.17657 0 MPRFLG 0 ; 
       7 SCHEM 25.09487 -5.17657 0 MPRFLG 0 ; 
       8 SCHEM 27.59487 -5.17657 0 MPRFLG 0 ; 
       11 SCHEM 7.594876 -5.17657 0 MPRFLG 0 ; 
       12 SCHEM 16.34487 -3.17657 0 USR MPRFLG 0 ; 
       13 SCHEM 2.594876 -5.17657 0 MPRFLG 0 ; 
       14 SCHEM 7.594876 -9.176573 0 MPRFLG 0 ; 
       15 SCHEM 10.09488 -9.176573 0 MPRFLG 0 ; 
       16 SCHEM 17.59487 -9.176573 0 MPRFLG 0 ; 
       17 SCHEM 20.09487 -9.176573 0 MPRFLG 0 ; 
       18 SCHEM 5.094875 -9.176573 0 MPRFLG 0 ; 
       19 SCHEM 18.84487 -7.17657 0 MPRFLG 0 ; 
       20 SCHEM 0.09487663 -5.17657 0 MPRFLG 0 ; 
       21 SCHEM 8.844875 -7.17657 0 MPRFLG 0 ; 
       22 SCHEM 16.34487 -5.17657 0 MPRFLG 0 ; 
       23 SCHEM 19.56557 -0.3415046 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19.50867 -1.01402 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 28.01717 3.233047 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.84997 2.254242 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.3517 9.293587 0 USR SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 27.85992 1.289688 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5.094875 -7.17657 0 MPRFLG 0 ; 
       30 SCHEM 5.350509 -1.832631 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 5.390552 -2.47222 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 7.669669 -1.778944 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 7.562115 -2.418534 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 10.0499 -1.736948 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 10.01952 -2.434884 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 12.45372 -1.769249 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 12.40374 -2.49561 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 20.56998 3.473445 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.84687 -0.2979131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24.72662 -0.2879605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 64.77222 12.94726 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.3026 13.6452 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
