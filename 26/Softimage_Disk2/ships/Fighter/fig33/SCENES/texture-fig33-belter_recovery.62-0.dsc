SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.68-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       edit_nulls-mat70.2-0 ; 
       fig33_belter_recovery-mat100.1-0 ; 
       fig33_belter_recovery-mat101.1-0 ; 
       fig33_belter_recovery-mat102.2-0 ; 
       fig33_belter_recovery-mat103.1-0 ; 
       fig33_belter_recovery-mat104.2-0 ; 
       fig33_belter_recovery-mat105.1-0 ; 
       fig33_belter_recovery-mat106.1-0 ; 
       fig33_belter_recovery-mat107.1-0 ; 
       fig33_belter_recovery-mat108.1-0 ; 
       fig33_belter_recovery-mat109.1-0 ; 
       fig33_belter_recovery-mat110.1-0 ; 
       fig33_belter_recovery-mat111.2-0 ; 
       fig33_belter_recovery-mat112.1-0 ; 
       fig33_belter_recovery-mat113.1-0 ; 
       fig33_belter_recovery-mat114.1-0 ; 
       fig33_belter_recovery-mat115.1-0 ; 
       fig33_belter_recovery-mat116.1-0 ; 
       fig33_belter_recovery-mat117.1-0 ; 
       fig33_belter_recovery-mat118.1-0 ; 
       fig33_belter_recovery-mat71.2-0 ; 
       fig33_belter_recovery-mat75.2-0 ; 
       fig33_belter_recovery-mat77.2-0 ; 
       fig33_belter_recovery-mat78.2-0 ; 
       fig33_belter_recovery-mat80.2-0 ; 
       fig33_belter_recovery-mat81.2-0 ; 
       fig33_belter_recovery-mat82.2-0 ; 
       fig33_belter_recovery-mat83.3-0 ; 
       fig33_belter_recovery-mat84.3-0 ; 
       fig33_belter_recovery-mat85.2-0 ; 
       fig33_belter_recovery-mat86.2-0 ; 
       fig33_belter_recovery-mat87.2-0 ; 
       fig33_belter_recovery-mat88.2-0 ; 
       fig33_belter_recovery-mat89.2-0 ; 
       fig33_belter_recovery-mat90.1-0 ; 
       fig33_belter_recovery-mat91.2-0 ; 
       fig33_belter_recovery-mat92.1-0 ; 
       fig33_belter_recovery-mat93.3-0 ; 
       fig33_belter_recovery-mat94.2-0 ; 
       fig33_belter_recovery-mat95.1-0 ; 
       fig33_belter_recovery-mat96.2-0 ; 
       fig33_belter_recovery-mat97.2-0 ; 
       fig33_belter_recovery-mat98.1-0 ; 
       fig33_belter_recovery-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       fig33_belter_recovery-bsmoke.1-0 ; 
       fig33_belter_recovery-bthrust.1-0 ; 
       fig33_belter_recovery-cockpt.1-0 ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube17.1-0 ; 
       fig33_belter_recovery-cube18.1-0 ; 
       fig33_belter_recovery-cube19.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.3-0 ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl1_9.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-lsmoke.1-0 ; 
       fig33_belter_recovery-lthrust.1-0 ; 
       fig33_belter_recovery-lwepemt.1-0 ; 
       fig33_belter_recovery-missemt.1-0 ; 
       fig33_belter_recovery-null1.50-0 ROOT ; 
       fig33_belter_recovery-rwepemt.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ; 
       fig33_belter_recovery-SS01.1-0 ; 
       fig33_belter_recovery-SS02.1-0 ; 
       fig33_belter_recovery-SS03.1-0 ; 
       fig33_belter_recovery-SS04.1-0 ; 
       fig33_belter_recovery-SS05.1-0 ; 
       fig33_belter_recovery-SS06.1-0 ; 
       fig33_belter_recovery-SS07.1-0 ; 
       fig33_belter_recovery-SS08.1-0 ; 
       fig33_belter_recovery-trail.1-0 ; 
       fig33_belter_recovery-tsmoke.1-0 ; 
       fig33_belter_recovery-tthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig33/PICTURES/fig33 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-fig33-belter_recovery.62-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       fig33_belter_recovery-t2d1.3-0 ; 
       fig33_belter_recovery-t2d10.2-0 ; 
       fig33_belter_recovery-t2d11.6-0 ; 
       fig33_belter_recovery-t2d12.5-0 ; 
       fig33_belter_recovery-t2d13.2-0 ; 
       fig33_belter_recovery-t2d14.3-0 ; 
       fig33_belter_recovery-t2d15.3-0 ; 
       fig33_belter_recovery-t2d16.1-0 ; 
       fig33_belter_recovery-t2d17.2-0 ; 
       fig33_belter_recovery-t2d18.2-0 ; 
       fig33_belter_recovery-t2d19.2-0 ; 
       fig33_belter_recovery-t2d2.4-0 ; 
       fig33_belter_recovery-t2d20.4-0 ; 
       fig33_belter_recovery-t2d21.2-0 ; 
       fig33_belter_recovery-t2d22.2-0 ; 
       fig33_belter_recovery-t2d23.1-0 ; 
       fig33_belter_recovery-t2d24.1-0 ; 
       fig33_belter_recovery-t2d25.1-0 ; 
       fig33_belter_recovery-t2d26.1-0 ; 
       fig33_belter_recovery-t2d27.1-0 ; 
       fig33_belter_recovery-t2d28.1-0 ; 
       fig33_belter_recovery-t2d29.4-0 ; 
       fig33_belter_recovery-t2d3.3-0 ; 
       fig33_belter_recovery-t2d30.1-0 ; 
       fig33_belter_recovery-t2d31.1-0 ; 
       fig33_belter_recovery-t2d32.1-0 ; 
       fig33_belter_recovery-t2d33.1-0 ; 
       fig33_belter_recovery-t2d34.1-0 ; 
       fig33_belter_recovery-t2d35.1-0 ; 
       fig33_belter_recovery-t2d36.2-0 ; 
       fig33_belter_recovery-t2d4.2-0 ; 
       fig33_belter_recovery-t2d5.3-0 ; 
       fig33_belter_recovery-t2d6.2-0 ; 
       fig33_belter_recovery-t2d7.2-0 ; 
       fig33_belter_recovery-t2d8.2-0 ; 
       fig33_belter_recovery-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 27 110 ; 
       0 1 111 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 22 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 21 110 ; 
       12 13 110 ; 
       13 27 110 ; 
       14 13 110 ; 
       15 21 110 ; 
       16 19 110 ; 
       17 29 110 ; 
       18 19 110 ; 
       19 22 110 ; 
       20 13 110 ; 
       21 12 110 ; 
       22 13 110 ; 
       23 27 110 ; 
       24 27 110 ; 
       25 27 110 ; 
       26 27 110 ; 
       26 28 111 ; 
       26 25 111 ; 
       28 27 110 ; 
       29 12 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 27 110 ; 
       34 27 110 ; 
       35 27 110 ; 
       36 27 110 ; 
       37 27 110 ; 
       38 27 110 ; 
       39 27 110 ; 
       39 40 111 ; 
       40 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 43 300 ; 
       4 1 300 ; 
       5 2 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       8 16 300 ; 
       9 18 300 ; 
       10 19 300 ; 
       11 11 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       13 27 300 ; 
       13 29 300 ; 
       13 31 300 ; 
       13 34 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       15 10 300 ; 
       16 5 300 ; 
       16 6 300 ; 
       17 40 300 ; 
       17 9 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       19 3 300 ; 
       19 4 300 ; 
       20 28 300 ; 
       20 30 300 ; 
       20 32 300 ; 
       20 33 300 ; 
       21 37 300 ; 
       21 38 300 ; 
       21 17 300 ; 
       22 41 300 ; 
       22 42 300 ; 
       29 39 300 ; 
       30 0 300 ; 
       31 20 300 ; 
       32 22 300 ; 
       33 21 300 ; 
       34 24 300 ; 
       35 23 300 ; 
       36 25 300 ; 
       37 26 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 10 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 15 401 ; 
       7 16 401 ; 
       8 17 401 ; 
       9 18 401 ; 
       10 19 401 ; 
       11 20 401 ; 
       12 21 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       27 11 401 ; 
       28 0 401 ; 
       29 22 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 1 401 ; 
       37 2 401 ; 
       38 3 401 ; 
       39 4 401 ; 
       40 5 401 ; 
       41 6 401 ; 
       42 7 401 ; 
       43 8 401 ; 
       16 26 401 ; 
       17 27 401 ; 
       18 28 401 ; 
       19 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 127.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 117.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 115 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -6 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 60 -8 0 MPRFLG 0 ; 
       6 SCHEM 85 -4 0 MPRFLG 0 ; 
       7 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 90 -4 0 MPRFLG 0 ; 
       9 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 95 -4 0 MPRFLG 0 ; 
       11 SCHEM 45 -8 0 MPRFLG 0 ; 
       12 SCHEM 45 -4 0 MPRFLG 0 ; 
       13 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 66.25 -8 0 MPRFLG 0 ; 
       17 SCHEM 36.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 71.25 -8 0 MPRFLG 0 ; 
       19 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       20 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       21 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       22 SCHEM 70 -4 0 MPRFLG 0 ; 
       23 SCHEM 110 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 107.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 132.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 125 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 67.5 0 0 SRT 0.7559999 0.7559999 0.7559999 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 130 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 112.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 122.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 120 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 135 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 140 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 145 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 150 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 155 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 160 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 167.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 170 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 137.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 142.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 147.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 152.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 157.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 165 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 172.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 175 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
