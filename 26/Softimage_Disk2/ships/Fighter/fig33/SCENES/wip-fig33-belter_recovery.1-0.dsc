SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.1-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       cap300_belter_destroyer-cube4.1-0 ROOT ; 
       cap300_belter_destroyer-cyl1.1-0 ; 
       cap300_belter_destroyer-cyl1_1.1-0 ; 
       cap300_belter_destroyer-cyl1_2.1-0 ; 
       cap300_belter_destroyer-cyl1_3.1-0 ; 
       cap300_belter_destroyer-cyl1_4.1-0 ; 
       cap300_belter_destroyer-cyl12.1-0 ; 
       cap300_belter_destroyer-cyl2.1-0 ROOT ; 
       cap300_belter_destroyer-cyl3.1-0 ROOT ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere4.1-0 ; 
       fig33_belter_recovery-cube1.1-0 ROOT ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube5.1-0 ROOT ; 
       fig33_belter_recovery-cube6.1-0 ROOT ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_7.1-0 ROOT ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-cyl14.1-0 ROOT ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru4.1-0 ROOT ; 
       fig33_belter_recovery-sphere5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig33-belter_recovery.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 15 110 ; 
       16 13 110 ; 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       21 13 110 ; 
       23 15 110 ; 
       9 8 110 ; 
       17 21 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       18 21 110 ; 
       25 13 110 ; 
       20 25 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 21.25 -2 0 SRT 0.3007 0.3007 0.3007 0 -6.283185 0 -6.151315 2.119331 3.033037 MPRFLG 0 ; 
       13 SCHEM 25 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 32.5 5.81466 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 3.163644 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 8.163643 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10.66364 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 13.16364 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 3.75 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 -3.936946 1.593639 1.006804 MPRFLG 0 ; 
       8 SCHEM 8.163643 5.81466 0 USR DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 1.502842 -1.313398 -2.440639 MPRFLG 0 ; 
       21 SCHEM 40 5.81466 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 21.25 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 37.5 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 1.570796 0 -0.5761687 -1.170648 -1.324085 MPRFLG 0 ; 
       9 SCHEM 5.663643 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 35 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 3.141593 0 -1.769007 -1.011235 -4.266492 MPRFLG 0 ; 
       17 SCHEM 40 3.814659 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 42.5 3.814659 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 27.5 0 0 WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 30 0 0 DISPLAY 3 2 SRT 1.553148 0.777728 0.7999999 0 0 0 -0.3671674 -2.621368 3.669191 MPRFLG 0 ; 
       19 SCHEM 45 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 3.141593 0 -1.769007 -1.665495 -4.266492 MPRFLG 0 ; 
       25 SCHEM 47.5 5.81466 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 50 5.81466 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 21.25 -4 0 SRT 0.3007 0.3007 0.3007 0.65 1.748456e-007 3.141593 -5.633607 2.642406 2.106103 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
