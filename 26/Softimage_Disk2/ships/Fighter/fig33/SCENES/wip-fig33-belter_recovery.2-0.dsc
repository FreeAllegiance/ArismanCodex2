SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.2-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       cap300_belter_destroyer-cube4.2-0 ROOT ; 
       cap300_belter_destroyer-cyl1.1-0 ; 
       cap300_belter_destroyer-cyl1_1.1-0 ; 
       cap300_belter_destroyer-cyl1_2.1-0 ; 
       cap300_belter_destroyer-cyl1_3.1-0 ; 
       cap300_belter_destroyer-cyl1_4.1-0 ; 
       cap300_belter_destroyer-cyl12.1-0 ; 
       cap300_belter_destroyer-cyl2.2-0 ROOT ; 
       cap300_belter_destroyer-cyl3.2-0 ROOT ; 
       cap300_belter_destroyer-extru3.1-0 ; 
       cap300_belter_destroyer-sphere3.1-0 ; 
       cap300_belter_destroyer-sphere4.1-0 ; 
       fig33_belter_recovery-cube1.2-0 ROOT ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube5.2-0 ROOT ; 
       fig33_belter_recovery-cube6.2-0 ROOT ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube8.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_7.2-0 ROOT ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-cyl14.2-0 ROOT ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru4.2-0 ROOT ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig33-belter_recovery.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 8 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       9 8 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       16 18 110 ; 
       19 16 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       25 32 110 ; 
       26 16 110 ; 
       28 18 110 ; 
       32 16 110 ; 
       30 16 110 ; 
       20 30 110 ; 
       21 30 110 ; 
       31 18 110 ; 
       13 31 110 ; 
       14 13 110 ; 
       15 13 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35.4963 -8.070372 0 USR SRT 0.3007 0.3007 0.3007 0.65 1.748456e-007 3.141593 -5.633607 2.642406 2.106103 MPRFLG 0 ; 
       1 SCHEM 3.163644 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 8.163643 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 10.66364 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 13.16364 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 34.2463 -10.07037 0 MPRFLG 0 ; 
       6 SCHEM 36.7463 -10.07037 0 MPRFLG 0 ; 
       7 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 -3.936946 1.593639 1.006804 MPRFLG 0 ; 
       8 SCHEM 8.163643 5.81466 0 USR DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 0 0 1.502842 -1.313398 -2.440639 MPRFLG 0 ; 
       9 SCHEM 5.663643 3.814659 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 21.25 -2 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 -6.283185 0 -6.151315 2.119331 3.033037 MPRFLG 0 ; 
       16 SCHEM 14.37282 -9.59854 0 MPRFLG 0 ; 
       17 SCHEM 27.5 0 0 WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 15.62282 -7.59854 0 USR SRT 1.37609 0.777728 0.7999999 0 0 0 -0.3671674 -2.621368 3.669191 MPRFLG 0 ; 
       19 SCHEM 8.122816 -11.59854 0 MPRFLG 0 ; 
       22 SCHEM 10.62282 -13.59854 0 MPRFLG 0 ; 
       23 SCHEM 13.12282 -13.59854 0 MPRFLG 0 ; 
       24 SCHEM 45 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 3.141593 0 -1.769007 -1.665495 -4.266492 MPRFLG 0 ; 
       25 SCHEM 15.62282 -13.59854 0 MPRFLG 0 ; 
       26 SCHEM 11.87282 -11.59854 0 MPRFLG 0 ; 
       27 SCHEM 35 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 3.141593 0 -1.769007 -1.011235 -4.266492 MPRFLG 0 ; 
       28 SCHEM 5.622816 -9.59854 0 MPRFLG 0 ; 
       29 SCHEM 37.5 3.814659 0 DISPLAY 0 0 SRT 0.3007 0.3007 0.3007 0 1.570796 0 -0.5761687 -1.170648 -1.324085 MPRFLG 0 ; 
       32 SCHEM 15.62282 -11.59854 0 MPRFLG 0 ; 
       30 SCHEM 19.37282 -11.59854 0 MPRFLG 0 ; 
       20 SCHEM 18.12282 -13.59854 0 MPRFLG 0 ; 
       21 SCHEM 20.62282 -13.59854 0 MPRFLG 0 ; 
       31 SCHEM 24.37282 -9.59854 0 MPRFLG 0 ; 
       13 SCHEM 24.37282 -11.59854 0 MPRFLG 0 ; 
       14 SCHEM 23.12282 -13.59854 0 MPRFLG 0 ; 
       15 SCHEM 25.62282 -13.59854 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
