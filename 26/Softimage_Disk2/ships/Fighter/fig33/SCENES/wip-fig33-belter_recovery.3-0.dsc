SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cap300_belter_destroyer-cam_int1.3-0 ROOT ; 
       cap300_belter_destroyer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig33_belter_recovery-mat71.1-0 ; 
       fig33_belter_recovery-mat75.1-0 ; 
       fig33_belter_recovery-mat77.1-0 ; 
       fig33_belter_recovery-mat78.1-0 ; 
       fig33_belter_recovery-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       cap300_belter_destroyer-cube4.3-0 ROOT ; 
       cap300_belter_destroyer-cyl1_4.1-0 ; 
       cap300_belter_destroyer-cyl12.1-0 ; 
       fig33_belter_recovery-cube10.1-0 ; 
       fig33_belter_recovery-cube11.1-0 ; 
       fig33_belter_recovery-cube12.1-0 ; 
       fig33_belter_recovery-cube13.1-0 ; 
       fig33_belter_recovery-cube14.1-0 ; 
       fig33_belter_recovery-cube15.1-0 ; 
       fig33_belter_recovery-cube2.1-0 ; 
       fig33_belter_recovery-cube6.3-0 ROOT ; 
       fig33_belter_recovery-cube7.1-0 ; 
       fig33_belter_recovery-cube8.1-0 ; 
       fig33_belter_recovery-cube9.1-0 ; 
       fig33_belter_recovery-cyl1_5.1-0 ; 
       fig33_belter_recovery-cyl1_6.1-0 ; 
       fig33_belter_recovery-cyl1_8.1-0 ; 
       fig33_belter_recovery-cyl13.1-0 ; 
       fig33_belter_recovery-extru1.1-0 ; 
       fig33_belter_recovery-extru5.1-0 ; 
       fig33_belter_recovery-extru6.1-0 ; 
       fig33_belter_recovery-sphere5.1-0 ; 
       recovery2-bthrust.1-0 ROOT ; 
       recovery2-cockpt.1-0 ROOT ; 
       recovery2-lsmoke.1-0 ROOT ; 
       recovery2-lthrust.1-0 ROOT ; 
       recovery2-lwepemt.1-0 ROOT ; 
       recovery2-missemt.1-0 ROOT ; 
       recovery2-rsmoke.1-0 ROOT ; 
       recovery2-rthrust.1-0 ROOT ; 
       recovery2-rwepemt.1-0 ROOT ; 
       recovery2-SS01.1-0 ROOT ; 
       recovery2-SS02.1-0 ROOT ; 
       recovery2-SS03.1-0 ROOT ; 
       recovery2-SS04.1-0 ROOT ; 
       recovery2-SS05.1-0 ROOT ; 
       recovery2-SS06.1-0 ROOT ; 
       recovery2-trail.1-0 ROOT ; 
       recovery2-tthrust.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig33-belter_recovery.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 20 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       9 10 110 ; 
       11 9 110 ; 
       12 19 110 ; 
       13 19 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 21 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 9 110 ; 
       20 10 110 ; 
       21 9 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       31 0 300 ; 
       32 1 300 ; 
       33 3 300 ; 
       34 2 300 ; 
       35 5 300 ; 
       36 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.62517 -8.070372 0 USR DISPLAY 0 0 SRT 0.3881577 0.3881577 0.3881577 0.65 1.748456e-007 3.141593 -0.3262624 -3.444836 4.170756 MPRFLG 0 ; 
       1 SCHEM 71.37517 -10.07037 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 73.87517 -10.07037 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 58.83659 -10.20001 0 MPRFLG 0 ; 
       4 SCHEM 57.58659 -12.20001 0 MPRFLG 0 ; 
       5 SCHEM 60.08659 -12.20001 0 MPRFLG 0 ; 
       9 SCHEM 48.83659 -8.200006 0 MPRFLG 0 ; 
       10 SCHEM 53.83659 -6.200009 0 USR SRT 1.37609 0.777728 0.7999999 0 0 0 -0.3671674 -2.621368 3.669191 MPRFLG 0 ; 
       11 SCHEM 42.58659 -10.20001 0 MPRFLG 0 ; 
       12 SCHEM 52.58659 -12.20001 0 MPRFLG 0 ; 
       13 SCHEM 55.08659 -12.20001 0 MPRFLG 0 ; 
       14 SCHEM 45.08659 -12.20001 0 MPRFLG 0 ; 
       15 SCHEM 47.58659 -12.20001 0 MPRFLG 0 ; 
       16 SCHEM 50.08659 -12.20001 0 MPRFLG 0 ; 
       17 SCHEM 46.33659 -10.20001 0 MPRFLG 0 ; 
       18 SCHEM 40.08659 -8.200006 0 MPRFLG 0 ; 
       19 SCHEM 53.83659 -10.20001 0 MPRFLG 0 ; 
       20 SCHEM 58.83659 -8.200006 0 MPRFLG 0 ; 
       21 SCHEM 50.08659 -10.20001 0 MPRFLG 0 ; 
       22 SCHEM 73.11537 -22.20358 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 -3.034166e-008 -2.662327 -13.04863 MPRFLG 0 ; 
       23 SCHEM 69.0593 -19.83233 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 -6.428331e-022 -0.04985064 8.920129 MPRFLG 0 ; 
       6 SCHEM 62.58659 -8.200006 0 MPRFLG 0 ; 
       7 SCHEM 65.08659 -8.200006 0 MPRFLG 0 ; 
       8 SCHEM 67.58659 -8.200006 0 MPRFLG 0 ; 
       24 SCHEM 68.0037 -21.53444 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 2.6633 1.247829e-006 -13.04863 MPRFLG 0 ; 
       25 SCHEM 67.94681 -22.20695 0 USR WIRECOL 1 7 SRT 1 1 1 3.141593 -5.911489e-024 -3.579809e-023 2.6633 1.247829e-006 -13.04863 MPRFLG 0 ; 
       26 SCHEM 76.4553 -17.95989 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 8.595625 -0.03049457 -8.536576 MPRFLG 0 ; 
       27 SCHEM 75.2881 -18.93869 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 3.592577e-022 -2.798723 5.192977 MPRFLG 0 ; 
       28 SCHEM 70.37685 -21.56615 0 USR WIRECOL 1 7 SRT 1 1 1 8.428471e-008 3.141593 -5.614126e-008 -2.6633 6.538269e-007 -13.04863 MPRFLG 0 ; 
       29 SCHEM 70.37932 -22.12153 0 USR WIRECOL 1 7 SRT 1 1 1 8.428471e-008 3.141593 -5.614126e-008 -2.6633 6.538269e-007 -13.04863 MPRFLG 0 ; 
       30 SCHEM 76.29805 -19.90325 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 -8.595625 -0.03049457 -8.536576 MPRFLG 0 ; 
       31 SCHEM 44.4837 -17.04295 0 USR WIRECOL 4 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 20.28126 -0.3672264 -22.70805 MPRFLG 0 ; 
       32 SCHEM 44.52374 -17.68254 0 USR WIRECOL 4 7 SRT 1 1 1 8.428471e-008 3.141593 3.579809e-023 -20.28126 -0.3672264 -22.70805 MPRFLG 0 ; 
       33 SCHEM 46.80285 -16.98926 0 USR WIRECOL 4 7 SRT 1 1 1 8.428471e-008 3.141593 -5.614126e-008 6.87765 8.732319 -12.58549 MPRFLG 0 ; 
       34 SCHEM 46.6953 -17.62885 0 USR WIRECOL 4 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 -6.87765 8.732319 -12.58549 MPRFLG 0 ; 
       35 SCHEM 49.18308 -16.94726 0 USR WIRECOL 4 7 SRT 1 1 1 -8.428471e-008 -3.258414e-007 -3.141593 6.87765 -8.732319 -12.58549 MPRFLG 0 ; 
       36 SCHEM 49.1527 -17.6452 0 USR WIRECOL 4 7 SRT 1 1 1 -3.141593 -3.258414e-007 -1.509958e-007 -6.87765 -8.732319 -12.58549 MPRFLG 0 ; 
       37 SCHEM 69.00811 -20.47202 0 USR WIRECOL 1 7 SRT 1 1 1 -8.428471e-008 -5.911489e-024 -3.579809e-023 -1.402849e-021 -0.02065563 -9.485557 MPRFLG 0 ; 
       38 SCHEM 73.16476 -21.4809 0 USR WIRECOL 1 7 SRT 1 1 1 8.428471e-008 3.141593 -5.614126e-008 -2.705746e-008 2.662328 -13.04863 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
