SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.24-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       fig30_belter_ftr-mat100.1-0 ; 
       fig30_belter_ftr-mat101.1-0 ; 
       fig30_belter_ftr-mat102.1-0 ; 
       fig30_belter_ftr-mat103.2-0 ; 
       fig30_belter_ftr-mat113.1-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat83.1-0 ; 
       fig30_belter_ftr-mat84.1-0 ; 
       fig30_belter_ftr-mat87.1-0 ; 
       fig30_belter_ftr-mat88.1-0 ; 
       fig30_belter_ftr-mat89.1-0 ; 
       fig30_belter_ftr-mat90.1-0 ; 
       fig30_belter_ftr-mat91.1-0 ; 
       fig30_belter_ftr-mat92.2-0 ; 
       fig30_belter_ftr-mat98.1-0 ; 
       fig30_belter_ftr-mat99.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig34_interceptor-mat123.1-0 ; 
       fig34_interceptor-mat124.1-0 ; 
       fig34_interceptor-mat127.1-0 ; 
       fig34_interceptor-mat128.1-0 ; 
       fig34_interceptor-mat129.1-0 ; 
       fig34_interceptor-mat130.1-0 ; 
       fig34_interceptor-mat131.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       fig34_interceptor-cube1.1-0 ; 
       fig34_interceptor-cube10.1-0 ; 
       fig34_interceptor-cube14.1-0 ; 
       fig34_interceptor-cube20.1-0 ; 
       fig34_interceptor-cube21.1-0 ; 
       fig34_interceptor-cube24.1-0 ; 
       fig34_interceptor-cube27.1-0 ; 
       fig34_interceptor-cube28.1-0 ; 
       fig34_interceptor-cube30.1-0 ; 
       fig34_interceptor-cube4.1-0 ; 
       fig34_interceptor-cube6.1-0 ; 
       fig34_interceptor-cube7.1-0 ; 
       fig34_interceptor-cyl1.1-0 ; 
       fig34_interceptor-cyl1_1.1-0 ; 
       fig34_interceptor-extru2.1-0 ; 
       fig34_interceptor-extru3.2-0 ; 
       fig34_interceptor-null4_1_1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig34/PICTURES/fig34 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Static-fig34-interceptor.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       fig30_belter_ftr-t2d1_1.1-0 ; 
       fig30_belter_ftr-t2d10_1.2-0 ; 
       fig30_belter_ftr-t2d16_1.1-0 ; 
       fig30_belter_ftr-t2d17_1.1-0 ; 
       fig30_belter_ftr-t2d18_1.1-0 ; 
       fig30_belter_ftr-t2d19.9-0 ; 
       fig30_belter_ftr-t2d2_1.1-0 ; 
       fig30_belter_ftr-t2d20.9-0 ; 
       fig30_belter_ftr-t2d21_1.2-0 ; 
       fig30_belter_ftr-t2d32_1.1-0 ; 
       fig30_belter_ftr-t2d33_1.3-0 ; 
       fig30_belter_ftr-t2d34_1.1-0 ; 
       fig30_belter_ftr-t2d39_1.1-0 ; 
       fig30_belter_ftr-t2d5_1.3-0 ; 
       fig30_belter_ftr-t2d6_1.2-0 ; 
       fig30_belter_ftr-t2d7.9-0 ; 
       fig30_belter_ftr-t2d8_1.1-0 ; 
       fig30_belter_ftr-t2d9_1.1-0 ; 
       fig32_belter_stealth-t2d12_1.1-0 ; 
       fig32_belter_stealth-t2d16_1.1-0 ; 
       fig32_belter_stealth-t2d29_1.2-0 ; 
       fig34_interceptor-t2d42_1.1-0 ; 
       fig34_interceptor-t2d43_1.1-0 ; 
       fig34_interceptor-t2d46_1.1-0 ; 
       fig34_interceptor-t2d47_1.1-0 ; 
       fig34_interceptor-t2d48.1-0 ; 
       fig34_interceptor-t2d49.1-0 ; 
       fig34_interceptor-t2d50.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 0 110 ; 
       2 14 110 ; 
       3 15 110 ; 
       4 0 110 ; 
       5 14 110 ; 
       6 15 110 ; 
       7 0 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 0 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 5 110 ; 
       14 16 110 ; 
       15 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 0 300 ; 
       0 6 300 ; 
       1 15 300 ; 
       1 3 300 ; 
       1 27 300 ; 
       2 4 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       6 23 300 ; 
       7 24 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       9 18 300 ; 
       10 13 300 ; 
       11 11 300 ; 
       12 12 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       14 7 300 ; 
       15 10 300 ; 
       15 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 5 401 ; 
       2 7 401 ; 
       3 8 401 ; 
       4 9 401 ; 
       5 10 401 ; 
       6 11 401 ; 
       7 12 401 ; 
       8 0 401 ; 
       9 6 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 16 401 ; 
       14 17 401 ; 
       15 1 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 20 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 20 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 27.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 16.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 13.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 16.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
