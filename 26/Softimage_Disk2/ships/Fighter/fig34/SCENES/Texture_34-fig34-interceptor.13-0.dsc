SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.21-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       edit_nulls-mat70_1.1-0 ; 
       fig20_biofighter-mat71_1.1-0 ; 
       fig20_biofighter-mat75_1.1-0 ; 
       fig20_biofighter-mat77_1.1-0 ; 
       fig20_biofighter-mat78_1.1-0 ; 
       fig20_biofighter-mat80_1.1-0 ; 
       fig30_belter_ftr-mat100.1-0 ; 
       fig30_belter_ftr-mat101.1-0 ; 
       fig30_belter_ftr-mat102.1-0 ; 
       fig30_belter_ftr-mat103.2-0 ; 
       fig30_belter_ftr-mat113.1-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat81_1.1-0 ; 
       fig30_belter_ftr-mat82_1.1-0 ; 
       fig30_belter_ftr-mat83.1-0 ; 
       fig30_belter_ftr-mat84.1-0 ; 
       fig30_belter_ftr-mat87.1-0 ; 
       fig30_belter_ftr-mat88.1-0 ; 
       fig30_belter_ftr-mat89.1-0 ; 
       fig30_belter_ftr-mat90.1-0 ; 
       fig30_belter_ftr-mat91.1-0 ; 
       fig30_belter_ftr-mat92.2-0 ; 
       fig30_belter_ftr-mat98.1-0 ; 
       fig30_belter_ftr-mat99.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig34_interceptor-mat123.1-0 ; 
       fig34_interceptor-mat124.1-0 ; 
       fig34_interceptor-mat127.1-0 ; 
       fig34_interceptor-mat128.1-0 ; 
       fig34_interceptor-mat129.1-0 ; 
       fig34_interceptor-mat130.1-0 ; 
       fig34_interceptor-mat131.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       fig34_interceptor-cockpt.1-0 ; 
       fig34_interceptor-cube1.1-0 ; 
       fig34_interceptor-cube10.1-0 ; 
       fig34_interceptor-cube14.1-0 ; 
       fig34_interceptor-cube20.1-0 ; 
       fig34_interceptor-cube21.1-0 ; 
       fig34_interceptor-cube24.1-0 ; 
       fig34_interceptor-cube27.1-0 ; 
       fig34_interceptor-cube28.1-0 ; 
       fig34_interceptor-cube30.1-0 ; 
       fig34_interceptor-cube4.1-0 ; 
       fig34_interceptor-cube6.1-0 ; 
       fig34_interceptor-cube7.1-0 ; 
       fig34_interceptor-cyl1.1-0 ; 
       fig34_interceptor-cyl1_1.1-0 ; 
       fig34_interceptor-cyl1_2.1-0 ; 
       fig34_interceptor-cyl14.1-0 ; 
       fig34_interceptor-extru2.1-0 ; 
       fig34_interceptor-extru3.2-0 ; 
       fig34_interceptor-fwepemt.1-0 ; 
       fig34_interceptor-lthrust.1-0 ; 
       fig34_interceptor-lwepemt.1-0 ; 
       fig34_interceptor-missemt.1-0 ; 
       fig34_interceptor-msmoke.1-0 ; 
       fig34_interceptor-mthrust.1-0 ; 
       fig34_interceptor-null4_1_1.2-0 ROOT ; 
       fig34_interceptor-rsmoke.1-0 ; 
       fig34_interceptor-rthrust.1-0 ; 
       fig34_interceptor-rwepemt.1-0 ; 
       fig34_interceptor-SS01.1-0 ; 
       fig34_interceptor-SS02.1-0 ; 
       fig34_interceptor-SS03.1-0 ; 
       fig34_interceptor-SS04.1-0 ; 
       fig34_interceptor-SS05.1-0 ; 
       fig34_interceptor-SS06.1-0 ; 
       fig34_interceptor-SS07.1-0 ; 
       fig34_interceptor-SS08.1-0 ; 
       fig34_interceptor-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Fighter/fig34/PICTURES/fig34 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture_34-fig34-interceptor.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       fig30_belter_ftr-t2d1_1.1-0 ; 
       fig30_belter_ftr-t2d10_1.2-0 ; 
       fig30_belter_ftr-t2d16_1.1-0 ; 
       fig30_belter_ftr-t2d17_1.1-0 ; 
       fig30_belter_ftr-t2d18_1.1-0 ; 
       fig30_belter_ftr-t2d19.9-0 ; 
       fig30_belter_ftr-t2d2_1.1-0 ; 
       fig30_belter_ftr-t2d20.9-0 ; 
       fig30_belter_ftr-t2d21_1.2-0 ; 
       fig30_belter_ftr-t2d32_1.1-0 ; 
       fig30_belter_ftr-t2d33_1.2-0 ; 
       fig30_belter_ftr-t2d34_1.1-0 ; 
       fig30_belter_ftr-t2d39_1.1-0 ; 
       fig30_belter_ftr-t2d5_1.2-0 ; 
       fig30_belter_ftr-t2d6_1.2-0 ; 
       fig30_belter_ftr-t2d7.9-0 ; 
       fig30_belter_ftr-t2d8_1.1-0 ; 
       fig30_belter_ftr-t2d9_1.1-0 ; 
       fig32_belter_stealth-t2d12_1.1-0 ; 
       fig32_belter_stealth-t2d16_1.1-0 ; 
       fig32_belter_stealth-t2d29_1.2-0 ; 
       fig32_belter_stealth-t2d30_1.2-0 ; 
       fig32_belter_stealth-t2d31_1.2-0 ; 
       fig34_interceptor-t2d42_1.1-0 ; 
       fig34_interceptor-t2d43_1.1-0 ; 
       fig34_interceptor-t2d46_1.1-0 ; 
       fig34_interceptor-t2d47_1.1-0 ; 
       fig34_interceptor-t2d48.1-0 ; 
       fig34_interceptor-t2d49.1-0 ; 
       fig34_interceptor-t2d50.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 17 110 ; 
       2 1 110 ; 
       3 17 110 ; 
       4 18 110 ; 
       5 1 110 ; 
       6 17 110 ; 
       7 18 110 ; 
       8 1 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 1 110 ; 
       12 18 110 ; 
       13 18 110 ; 
       14 6 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 25 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 17 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       32 17 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 22 300 ; 
       1 6 300 ; 
       1 12 300 ; 
       2 23 300 ; 
       2 9 300 ; 
       2 37 300 ; 
       3 10 300 ; 
       4 24 300 ; 
       5 25 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       7 33 300 ; 
       8 34 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       10 26 300 ; 
       11 21 300 ; 
       12 19 300 ; 
       13 20 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       14 29 300 ; 
       14 30 300 ; 
       15 27 300 ; 
       16 28 300 ; 
       17 16 300 ; 
       17 17 300 ; 
       17 13 300 ; 
       18 18 300 ; 
       18 11 300 ; 
       29 0 300 ; 
       30 1 300 ; 
       31 3 300 ; 
       32 2 300 ; 
       33 5 300 ; 
       34 4 300 ; 
       35 14 300 ; 
       36 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 4 401 ; 
       7 5 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       16 0 401 ; 
       17 6 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       22 17 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 18 401 ; 
       30 19 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 80 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 91.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 87.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 67.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 92.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 70 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 95 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 97.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 38.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 90 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 65 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 72.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 37.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 40 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 61.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 68.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 100 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 58.80019 -16.57235 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 85 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 82.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 60.62906 -17.28458 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 66.02067 -16.62515 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 61.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 61.30019 -16.57235 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 63.80019 -16.57235 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 62.5 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 45 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 42.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 55 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 52.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 60 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 57.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 77.5 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 75 -4 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 35 -4 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
