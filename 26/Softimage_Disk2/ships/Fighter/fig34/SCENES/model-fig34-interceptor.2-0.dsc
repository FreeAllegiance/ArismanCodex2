SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       edit_nulls-mat70_1.1-0 ; 
       fig20_biofighter-mat71_1.1-0 ; 
       fig20_biofighter-mat75_1.1-0 ; 
       fig20_biofighter-mat77_1.1-0 ; 
       fig20_biofighter-mat78_1.1-0 ; 
       fig20_biofighter-mat80_1.1-0 ; 
       fig30_belter_ftr-mat100.1-0 ; 
       fig30_belter_ftr-mat101.1-0 ; 
       fig30_belter_ftr-mat102.1-0 ; 
       fig30_belter_ftr-mat103.1-0 ; 
       fig30_belter_ftr-mat113.1-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat81_1.1-0 ; 
       fig30_belter_ftr-mat82_1.1-0 ; 
       fig30_belter_ftr-mat83.1-0 ; 
       fig30_belter_ftr-mat84.1-0 ; 
       fig30_belter_ftr-mat87.1-0 ; 
       fig30_belter_ftr-mat88.1-0 ; 
       fig30_belter_ftr-mat89.1-0 ; 
       fig30_belter_ftr-mat90.1-0 ; 
       fig30_belter_ftr-mat91.1-0 ; 
       fig30_belter_ftr-mat92.1-0 ; 
       fig30_belter_ftr-mat98.1-0 ; 
       fig30_belter_ftr-mat99.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig34_interceptor-mat123.1-0 ; 
       fig34_interceptor-mat124.1-0 ; 
       fig34_interceptor-mat125.1-0 ; 
       fig34_interceptor-mat126.1-0 ; 
       fig34_interceptor-mat127.1-0 ; 
       fig34_interceptor-mat128.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       fig34_interceptor-cockpt.1-0 ; 
       fig34_interceptor-cube1.1-0 ; 
       fig34_interceptor-cube10.1-0 ; 
       fig34_interceptor-cube14.1-0 ; 
       fig34_interceptor-cube20.1-0 ; 
       fig34_interceptor-cube21.1-0 ; 
       fig34_interceptor-cube24.1-0 ; 
       fig34_interceptor-cube25.1-0 ; 
       fig34_interceptor-cube27.1-0 ; 
       fig34_interceptor-cube28.1-0 ; 
       fig34_interceptor-cube4.1-0 ; 
       fig34_interceptor-cube6.1-0 ; 
       fig34_interceptor-cube7.1-0 ; 
       fig34_interceptor-cyl1.1-0 ; 
       fig34_interceptor-cyl1_1.1-0 ; 
       fig34_interceptor-cyl1_2.1-0 ; 
       fig34_interceptor-cyl14.1-0 ; 
       fig34_interceptor-extru2.1-0 ; 
       fig34_interceptor-extru3.2-0 ; 
       fig34_interceptor-lthrust.1-0 ; 
       fig34_interceptor-lwepemt.1-0 ; 
       fig34_interceptor-missemt.1-0 ; 
       fig34_interceptor-msmoke.1-0 ; 
       fig34_interceptor-mthrust.1-0 ; 
       fig34_interceptor-null4.4-0 ROOT ; 
       fig34_interceptor-rsmoke.1-0 ; 
       fig34_interceptor-rthrust.1-0 ; 
       fig34_interceptor-rwepemt.1-0 ; 
       fig34_interceptor-SS01.1-0 ; 
       fig34_interceptor-SS02.1-0 ; 
       fig34_interceptor-SS03.1-0 ; 
       fig34_interceptor-SS04.1-0 ; 
       fig34_interceptor-SS05.1-0 ; 
       fig34_interceptor-SS06.1-0 ; 
       fig34_interceptor-SS07.1-0 ; 
       fig34_interceptor-SS08.1-0 ; 
       fig34_interceptor-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig34/PICTURES/fig30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig34/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-fig34-interceptor.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       fig30_belter_ftr-t2d1.2-0 ; 
       fig30_belter_ftr-t2d10.2-0 ; 
       fig30_belter_ftr-t2d16.2-0 ; 
       fig30_belter_ftr-t2d17.2-0 ; 
       fig30_belter_ftr-t2d18.2-0 ; 
       fig30_belter_ftr-t2d19.2-0 ; 
       fig30_belter_ftr-t2d2.2-0 ; 
       fig30_belter_ftr-t2d20.2-0 ; 
       fig30_belter_ftr-t2d21.2-0 ; 
       fig30_belter_ftr-t2d32.2-0 ; 
       fig30_belter_ftr-t2d33.2-0 ; 
       fig30_belter_ftr-t2d34.2-0 ; 
       fig30_belter_ftr-t2d39.2-0 ; 
       fig30_belter_ftr-t2d5.2-0 ; 
       fig30_belter_ftr-t2d6.2-0 ; 
       fig30_belter_ftr-t2d7.2-0 ; 
       fig30_belter_ftr-t2d8.2-0 ; 
       fig30_belter_ftr-t2d9.2-0 ; 
       fig32_belter_stealth-t2d12.2-0 ; 
       fig32_belter_stealth-t2d16.2-0 ; 
       fig32_belter_stealth-t2d29.2-0 ; 
       fig32_belter_stealth-t2d30.2-0 ; 
       fig32_belter_stealth-t2d31.2-0 ; 
       fig34_interceptor-t2d42.2-0 ; 
       fig34_interceptor-t2d43.2-0 ; 
       fig34_interceptor-t2d44.2-0 ; 
       fig34_interceptor-t2d45.2-0 ; 
       fig34_interceptor-t2d46.2-0 ; 
       fig34_interceptor-t2d47.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       25 17 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 17 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       32 17 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       1 17 110 ; 
       2 1 110 ; 
       3 17 110 ; 
       4 18 110 ; 
       5 1 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 18 110 ; 
       9 1 110 ; 
       10 17 110 ; 
       11 1 110 ; 
       12 18 110 ; 
       13 7 110 ; 
       14 6 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 24 110 ; 
       18 17 110 ; 
       22 17 110 ; 
       23 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       28 0 300 ; 
       29 1 300 ; 
       30 3 300 ; 
       31 2 300 ; 
       32 5 300 ; 
       33 4 300 ; 
       34 14 300 ; 
       35 15 300 ; 
       1 22 300 ; 
       1 6 300 ; 
       1 12 300 ; 
       2 23 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       4 24 300 ; 
       5 25 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       8 35 300 ; 
       9 36 300 ; 
       10 26 300 ; 
       11 21 300 ; 
       12 19 300 ; 
       13 20 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       14 29 300 ; 
       14 30 300 ; 
       15 27 300 ; 
       16 28 300 ; 
       17 16 300 ; 
       17 17 300 ; 
       17 13 300 ; 
       18 18 300 ; 
       18 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 4 401 ; 
       7 5 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       16 0 401 ; 
       17 6 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       22 17 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 18 401 ; 
       30 19 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 24.70509 -5.719952 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 13.44746 -5.966581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.03008 -6.63912 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24.77127 -6.639121 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 15.90719 -6.933778 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 15.94813 -6.010435 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 22.53406 -6.661047 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 17.58532 -10.13112 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 17.49795 -10.83278 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 19.91057 -10.13112 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 19.91024 -10.87663 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 22.41057 -10.13112 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 22.27934 -10.87663 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 24.70292 -10.10314 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 24.69358 -10.94268 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 16.02908 -3.981126 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.2607 0.9684206 0 USR MPRFLG 0 ; 
       2 SCHEM 28.5107 -1.03158 0 USR MPRFLG 0 ; 
       3 SCHEM 8.510706 0.9684206 0 USR MPRFLG 0 ; 
       4 SCHEM 23.5107 -1.03158 0 USR MPRFLG 0 ; 
       5 SCHEM 33.5107 -1.03158 0 USR MPRFLG 0 ; 
       6 SCHEM 11.01071 0.9684206 0 USR MPRFLG 0 ; 
       7 SCHEM 13.51071 0.9684206 0 USR MPRFLG 0 ; 
       8 SCHEM 26.0107 -1.03158 0 USR MPRFLG 0 ; 
       9 SCHEM 36.0107 -1.03158 0 USR MPRFLG 0 ; 
       10 SCHEM 17.2607 0.9684206 0 USR MPRFLG 0 ; 
       11 SCHEM 31.0107 -1.03158 0 USR MPRFLG 0 ; 
       12 SCHEM 21.0107 -1.03158 0 USR MPRFLG 0 ; 
       13 SCHEM 13.51071 -1.03158 0 USR MPRFLG 0 ; 
       14 SCHEM 11.01071 -1.03158 0 USR MPRFLG 0 ; 
       15 SCHEM 16.0107 -1.03158 0 USR MPRFLG 0 ; 
       16 SCHEM 18.5107 -1.03158 0 USR MPRFLG 0 ; 
       17 SCHEM 22.2607 2.968421 0 USR MPRFLG 0 ; 
       18 SCHEM 23.5107 0.9684206 0 USR MPRFLG 0 ; 
       24 SCHEM 22.2607 4.968421 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 18.38171 -6.962002 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 18.36916 -6.136815 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 125.4594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 132.9594 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 127.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 130.4594 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 117.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 122.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 110.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 120.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 239.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 234.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 237.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 184.9594 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 187.4594 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 258.7094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 261.2094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 271.2094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 273.7094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 259.9594 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 276.2094 42.74044 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 110.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 120.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125.4594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 112.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132.9594 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 127.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 130.4594 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 117.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 122.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 184.9594 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 187.4594 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 239.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 234.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 237.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 258.7094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 261.2094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 271.2094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 273.7094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 259.9594 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 276.2094 40.74044 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
