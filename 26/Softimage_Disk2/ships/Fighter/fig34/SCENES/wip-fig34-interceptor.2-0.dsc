SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       edit_nulls-mat70_1.1-0 ; 
       fig20_biofighter-mat71_1.1-0 ; 
       fig20_biofighter-mat75_1.1-0 ; 
       fig20_biofighter-mat77_1.1-0 ; 
       fig20_biofighter-mat78_1.1-0 ; 
       fig20_biofighter-mat80_1.1-0 ; 
       fig30_belter_ftr-mat100.1-0 ; 
       fig30_belter_ftr-mat101.1-0 ; 
       fig30_belter_ftr-mat102.1-0 ; 
       fig30_belter_ftr-mat103.1-0 ; 
       fig30_belter_ftr-mat113.1-0 ; 
       fig30_belter_ftr-mat114.1-0 ; 
       fig30_belter_ftr-mat115.1-0 ; 
       fig30_belter_ftr-mat120.1-0 ; 
       fig30_belter_ftr-mat81_1.1-0 ; 
       fig30_belter_ftr-mat82_1.1-0 ; 
       fig30_belter_ftr-mat83.1-0 ; 
       fig30_belter_ftr-mat84.1-0 ; 
       fig30_belter_ftr-mat87.1-0 ; 
       fig30_belter_ftr-mat88.1-0 ; 
       fig30_belter_ftr-mat89.1-0 ; 
       fig30_belter_ftr-mat90.1-0 ; 
       fig30_belter_ftr-mat91.1-0 ; 
       fig30_belter_ftr-mat92.1-0 ; 
       fig30_belter_ftr-mat98.1-0 ; 
       fig30_belter_ftr-mat99.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       fig32_belter_stealth-mat94.1-0 ; 
       fig32_belter_stealth-mat98.1-0 ; 
       fig34_interceptor-mat123.1-0 ; 
       fig34_interceptor-mat124.1-0 ; 
       fig34_interceptor-mat125.1-0 ; 
       fig34_interceptor-mat126.1-0 ; 
       fig34_interceptor-mat127.1-0 ; 
       fig34_interceptor-mat128.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       fig30_belter_ftr-cockpt.1-0 ROOT ; 
       fig30_belter_ftr-lsmoke.1-0 ROOT ; 
       fig30_belter_ftr-lthrust.1-0 ROOT ; 
       fig30_belter_ftr-lwepemt.1-0 ROOT ; 
       fig30_belter_ftr-missemt.1-0 ROOT ; 
       fig30_belter_ftr-rsmoke.1-0 ROOT ; 
       fig30_belter_ftr-rthrust.1-0 ROOT ; 
       fig30_belter_ftr-rwepemt.1-0 ROOT ; 
       fig30_belter_ftr-SS01.1-0 ROOT ; 
       fig30_belter_ftr-SS02.1-0 ROOT ; 
       fig30_belter_ftr-SS03.1-0 ROOT ; 
       fig30_belter_ftr-SS04.1-0 ROOT ; 
       fig30_belter_ftr-SS05.1-0 ROOT ; 
       fig30_belter_ftr-SS06.1-0 ROOT ; 
       fig30_belter_ftr-SS07.1-0 ROOT ; 
       fig30_belter_ftr-SS08.1-0 ROOT ; 
       fig30_belter_ftr-trail.1-0 ROOT ; 
       fig32_belter_stealth-missemt.1-0 ROOT ; 
       fig34_interceptor-cube1.1-0 ; 
       fig34_interceptor-cube10.1-0 ; 
       fig34_interceptor-cube14.1-0 ; 
       fig34_interceptor-cube20.1-0 ; 
       fig34_interceptor-cube21.1-0 ; 
       fig34_interceptor-cube24.1-0 ; 
       fig34_interceptor-cube25.1-0 ; 
       fig34_interceptor-cube27.1-0 ; 
       fig34_interceptor-cube28.1-0 ; 
       fig34_interceptor-cube4.1-0 ; 
       fig34_interceptor-cube6.1-0 ; 
       fig34_interceptor-cube7.1-0 ; 
       fig34_interceptor-cyl1.1-0 ; 
       fig34_interceptor-cyl1_1.1-0 ; 
       fig34_interceptor-cyl1_2.1-0 ; 
       fig34_interceptor-cyl14.1-0 ; 
       fig34_interceptor-extru2.1-0 ; 
       fig34_interceptor-extru3.2-0 ; 
       fig34_interceptor-null4.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig34/PICTURES/fig30 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Fighter/fig34/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-fig34-interceptor.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       fig30_belter_ftr-t2d1.1-0 ; 
       fig30_belter_ftr-t2d10.1-0 ; 
       fig30_belter_ftr-t2d16.1-0 ; 
       fig30_belter_ftr-t2d17.1-0 ; 
       fig30_belter_ftr-t2d18.1-0 ; 
       fig30_belter_ftr-t2d19.1-0 ; 
       fig30_belter_ftr-t2d2.1-0 ; 
       fig30_belter_ftr-t2d20.1-0 ; 
       fig30_belter_ftr-t2d21.1-0 ; 
       fig30_belter_ftr-t2d32.1-0 ; 
       fig30_belter_ftr-t2d33.1-0 ; 
       fig30_belter_ftr-t2d34.1-0 ; 
       fig30_belter_ftr-t2d39.1-0 ; 
       fig30_belter_ftr-t2d5.1-0 ; 
       fig30_belter_ftr-t2d6.1-0 ; 
       fig30_belter_ftr-t2d7.1-0 ; 
       fig30_belter_ftr-t2d8.1-0 ; 
       fig30_belter_ftr-t2d9.1-0 ; 
       fig32_belter_stealth-t2d12.1-0 ; 
       fig32_belter_stealth-t2d16.1-0 ; 
       fig32_belter_stealth-t2d29.1-0 ; 
       fig32_belter_stealth-t2d30.1-0 ; 
       fig32_belter_stealth-t2d31.1-0 ; 
       fig34_interceptor-t2d42.1-0 ; 
       fig34_interceptor-t2d43.1-0 ; 
       fig34_interceptor-t2d44.1-0 ; 
       fig34_interceptor-t2d45.1-0 ; 
       fig34_interceptor-t2d46.1-0 ; 
       fig34_interceptor-t2d47.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 34 110 ; 
       24 34 110 ; 
       25 35 110 ; 
       26 18 110 ; 
       27 34 110 ; 
       32 27 110 ; 
       31 23 110 ; 
       33 27 110 ; 
       18 34 110 ; 
       19 18 110 ; 
       20 34 110 ; 
       21 35 110 ; 
       22 18 110 ; 
       28 18 110 ; 
       29 35 110 ; 
       30 24 110 ; 
       34 36 110 ; 
       35 34 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       23 31 300 ; 
       23 32 300 ; 
       24 33 300 ; 
       24 34 300 ; 
       25 35 300 ; 
       26 36 300 ; 
       27 26 300 ; 
       32 27 300 ; 
       31 29 300 ; 
       31 30 300 ; 
       33 28 300 ; 
       18 22 300 ; 
       18 6 300 ; 
       18 12 300 ; 
       19 23 300 ; 
       19 9 300 ; 
       20 10 300 ; 
       21 24 300 ; 
       22 25 300 ; 
       28 21 300 ; 
       29 19 300 ; 
       30 20 300 ; 
       30 7 300 ; 
       30 8 300 ; 
       34 16 300 ; 
       34 17 300 ; 
       34 13 300 ; 
       35 18 300 ; 
       35 11 300 ; 
       8 0 300 ; 
       9 1 300 ; 
       10 3 300 ; 
       11 2 300 ; 
       12 5 300 ; 
       13 4 300 ; 
       14 14 300 ; 
       15 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 4 401 ; 
       7 5 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       16 0 401 ; 
       17 6 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       22 17 401 ; 
       23 1 401 ; 
       24 2 401 ; 
       25 3 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 18 401 ; 
       30 19 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 13.45732 0.08842927 0 MPRFLG 0 ; 
       24 SCHEM 24.70732 0.08842927 0 MPRFLG 0 ; 
       36 SCHEM 42.20732 4.088429 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 44.70732 -1.911571 0 MPRFLG 0 ; 
       26 SCHEM 62.20732 -1.911571 0 MPRFLG 0 ; 
       27 SCHEM 34.70733 0.08842927 0 MPRFLG 0 ; 
       32 SCHEM 32.20732 -1.911571 0 MPRFLG 0 ; 
       31 SCHEM 10.95732 -1.911571 0 MPRFLG 0 ; 
       33 SCHEM 34.70733 -1.911571 0 MPRFLG 0 ; 
       0 SCHEM 27.22675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -1.787128e-022 0.3111044 2.493818 MPRFLG 0 ; 
       18 SCHEM 60.95732 0.08842927 0 MPRFLG 0 ; 
       19 SCHEM 53.45732 -1.911571 0 MPRFLG 0 ; 
       20 SCHEM 7.207316 0.08842927 0 MPRFLG 0 ; 
       21 SCHEM 42.20732 -1.911571 0 MPRFLG 0 ; 
       22 SCHEM 59.70732 -1.911571 0 MPRFLG 0 ; 
       28 SCHEM 57.20732 -1.911571 0 MPRFLG 0 ; 
       29 SCHEM 39.70732 -1.911571 0 MPRFLG 0 ; 
       30 SCHEM 22.20732 -1.911571 0 MPRFLG 0 ; 
       34 SCHEM 42.20732 2.088429 0 MPRFLG 0 ; 
       35 SCHEM 44.70732 0.08842927 0 MPRFLG 0 ; 
       1 SCHEM 14.72675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.881877 8.272682e-014 -3.235957 MPRFLG 0 ; 
       2 SCHEM 12.22675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.881877 8.272682e-014 -3.235957 MPRFLG 0 ; 
       3 SCHEM 32.22675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.7786173 -0.6693835 2.729695 MPRFLG 0 ; 
       4 SCHEM 29.72674 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 9.987657e-023 -0.501887 2.676266 MPRFLG 0 ; 
       5 SCHEM 22.22675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.667996 1.465971e-013 -3.221422 MPRFLG 0 ; 
       6 SCHEM 19.72675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.667996 1.465971e-013 -3.221422 MPRFLG 0 ; 
       7 SCHEM 24.72675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.7786174 -0.6693835 2.729695 MPRFLG 0 ; 
       8 SCHEM 34.72675 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.4004749 0.1942236 3.193892 MPRFLG 0 ; 
       9 SCHEM 37.22675 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.400804 0.1942236 3.193892 MPRFLG 0 ; 
       10 SCHEM 42.22675 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 2.671272 0.000347077 -0.9917474 MPRFLG 0 ; 
       11 SCHEM 39.72675 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -2.47545 0.000347077 -0.9917474 MPRFLG 0 ; 
       12 SCHEM 44.72675 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 0.6240765 0.8088385 0.08614995 MPRFLG 0 ; 
       13 SCHEM 47.22674 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.6240727 0.8088385 0.08614995 MPRFLG 0 ; 
       14 SCHEM 52.22674 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.5418239 -0.7611478 0.1765151 MPRFLG 0 ; 
       15 SCHEM 49.72674 -9.073005 0 USR WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.5393358 -0.7611526 0.1765151 MPRFLG 0 ; 
       16 SCHEM 17.22675 -9.073005 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 2.82499e-015 -1.753179 MPRFLG 0 ; 
       17 SCHEM 66.71315 -13.08362 0 USR WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 9.987657e-023 -0.7243716 2.23437 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 125.4594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 132.9594 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 127.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 130.4594 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 117.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 122.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 110.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 120.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 239.9594 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 234.9594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 237.4594 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 184.9594 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 187.4594 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 258.7094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 261.2094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 271.2094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 273.7094 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 259.9594 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 276.2094 42.74044 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 110.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 120.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 125.4594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 112.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132.9594 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 127.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 130.4594 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 117.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 122.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 184.9594 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 187.4594 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 239.9594 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 234.9594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 237.4594 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 258.7094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 261.2094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 271.2094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 273.7094 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 259.9594 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 276.2094 40.74044 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
