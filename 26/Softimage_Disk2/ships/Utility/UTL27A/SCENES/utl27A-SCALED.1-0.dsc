SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.26-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       SCALED-light1.1-0 ROOT ; 
       SCALED-light2.1-0 ROOT ; 
       SCALED-light3.1-0 ROOT ; 
       SCALED-light4.1-0 ROOT ; 
       SCALED-light5.1-0 ROOT ; 
       SCALED-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       drop_ship-default1.3-0 ; 
       drop_ship-default2.3-0 ; 
       drop_ship-mat1.3-0 ; 
       drop_ship-mat10.3-0 ; 
       drop_ship-mat102.3-0 ; 
       drop_ship-mat104.3-0 ; 
       drop_ship-mat105.3-0 ; 
       drop_ship-mat11.3-0 ; 
       drop_ship-mat12.3-0 ; 
       drop_ship-mat13.3-0 ; 
       drop_ship-mat14.3-0 ; 
       drop_ship-mat15.3-0 ; 
       drop_ship-mat2.3-0 ; 
       drop_ship-mat28.3-0 ; 
       drop_ship-mat29.3-0 ; 
       drop_ship-mat3.3-0 ; 
       drop_ship-mat30.3-0 ; 
       drop_ship-mat31.3-0 ; 
       drop_ship-mat4.3-0 ; 
       drop_ship-mat41.3-0 ; 
       drop_ship-mat42.3-0 ; 
       drop_ship-mat43.3-0 ; 
       drop_ship-mat44.3-0 ; 
       drop_ship-mat45.3-0 ; 
       drop_ship-mat46.3-0 ; 
       drop_ship-mat47.3-0 ; 
       drop_ship-mat49.3-0 ; 
       drop_ship-mat50.3-0 ; 
       drop_ship-mat51.3-0 ; 
       drop_ship-mat52.3-0 ; 
       drop_ship-mat53.3-0 ; 
       drop_ship-mat54.3-0 ; 
       drop_ship-mat55.3-0 ; 
       drop_ship-mat56.3-0 ; 
       drop_ship-mat7.3-0 ; 
       drop_ship-mat9.3-0 ; 
       SCALED-back4.1-0 ; 
       SCALED-botto1.1-0 ; 
       SCALED-front3.1-0 ; 
       SCALED-mat118.1-0 ; 
       SCALED-mat119.1-0 ; 
       SCALED-mat120.1-0 ; 
       SCALED-nose_white-center.1-1.1-0 ; 
       SCALED-nose_white-center.1-4.1-0 ; 
       SCALED-port_red-left.1-0.1-0 ; 
       SCALED-side1.1-0 ; 
       SCALED-side2.1-0 ; 
       SCALED-starbord_green-right.1-0.1-0 ; 
       SCALED-top3.1-0 ; 
       SCALED-top4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lsmoke.1-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rsmoke.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27_1.1-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27A/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27A-SCALED.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       drop_ship-t2d1.4-0 ; 
       drop_ship-t2d10.4-0 ; 
       drop_ship-t2d11.4-0 ; 
       drop_ship-t2d12.4-0 ; 
       drop_ship-t2d2.4-0 ; 
       drop_ship-t2d24.4-0 ; 
       drop_ship-t2d37.4-0 ; 
       drop_ship-t2d38.4-0 ; 
       drop_ship-t2d39.4-0 ; 
       drop_ship-t2d40.4-0 ; 
       drop_ship-t2d41.4-0 ; 
       drop_ship-t2d42.4-0 ; 
       drop_ship-t2d43.4-0 ; 
       drop_ship-t2d44.4-0 ; 
       drop_ship-t2d45.4-0 ; 
       drop_ship-t2d46.4-0 ; 
       drop_ship-t2d47.4-0 ; 
       drop_ship-t2d48.4-0 ; 
       drop_ship-t2d49.4-0 ; 
       drop_ship-t2d5.4-0 ; 
       drop_ship-t2d50.4-0 ; 
       drop_ship-t2d7.4-0 ; 
       drop_ship-t2d8.4-0 ; 
       drop_ship-t2d9.4-0 ; 
       drop_ship-t2d93.4-0 ; 
       drop_ship-t2d95.4-0 ; 
       drop_ship-t2d96.4-0 ; 
       SCALED-t2d118.1-0 ; 
       SCALED-t2d119.1-0 ; 
       SCALED-t2d120.1-0 ; 
       SCALED-t2d121.1-0 ; 
       SCALED-t2d122.1-0 ; 
       SCALED-t2d123.1-0 ; 
       SCALED-t2d124.1-0 ; 
       SCALED-t2d125.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 22 110 ; 
       3 22 110 ; 
       4 9 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 8 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 22 110 ; 
       11 16 110 ; 
       12 13 110 ; 
       13 15 110 ; 
       14 13 110 ; 
       15 3 110 ; 
       16 0 110 ; 
       17 3 110 ; 
       18 4 110 ; 
       19 3 110 ; 
       20 11 110 ; 
       21 22 110 ; 
       23 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       0 45 300 ; 
       0 48 300 ; 
       0 38 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       1 41 300 ; 
       1 46 300 ; 
       1 49 300 ; 
       3 12 300 ; 
       3 15 300 ; 
       3 18 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 40 300 ; 
       4 14 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       7 0 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 3 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       9 28 300 ; 
       11 17 300 ; 
       11 29 300 ; 
       11 30 300 ; 
       11 31 300 ; 
       11 6 300 ; 
       13 1 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       15 19 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       16 16 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       17 42 300 ; 
       18 44 300 ; 
       19 43 300 ; 
       20 47 300 ; 
       22 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       36 31 401 ; 
       37 27 401 ; 
       38 30 401 ; 
       40 32 401 ; 
       45 28 401 ; 
       46 33 401 ; 
       48 29 401 ; 
       49 34 401 ; 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -14 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       14 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -16 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 15 -16 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.25 -4 0 SRT 1 1 1 0 0 0 0 0 0.8267195 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       36 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 14 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
