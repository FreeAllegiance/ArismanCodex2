SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.19-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       drop_ship-default1.2-0 ; 
       drop_ship-default2.2-0 ; 
       drop_ship-mat1.2-0 ; 
       drop_ship-mat10.2-0 ; 
       drop_ship-mat102.2-0 ; 
       drop_ship-mat104.2-0 ; 
       drop_ship-mat105.2-0 ; 
       drop_ship-mat11.2-0 ; 
       drop_ship-mat12.2-0 ; 
       drop_ship-mat13.2-0 ; 
       drop_ship-mat14.2-0 ; 
       drop_ship-mat15.2-0 ; 
       drop_ship-mat2.2-0 ; 
       drop_ship-mat28.2-0 ; 
       drop_ship-mat29.2-0 ; 
       drop_ship-mat3.2-0 ; 
       drop_ship-mat30.2-0 ; 
       drop_ship-mat31.2-0 ; 
       drop_ship-mat4.2-0 ; 
       drop_ship-mat41.2-0 ; 
       drop_ship-mat42.2-0 ; 
       drop_ship-mat43.2-0 ; 
       drop_ship-mat44.2-0 ; 
       drop_ship-mat45.2-0 ; 
       drop_ship-mat46.2-0 ; 
       drop_ship-mat47.2-0 ; 
       drop_ship-mat49.2-0 ; 
       drop_ship-mat50.2-0 ; 
       drop_ship-mat51.2-0 ; 
       drop_ship-mat52.2-0 ; 
       drop_ship-mat53.2-0 ; 
       drop_ship-mat54.2-0 ; 
       drop_ship-mat55.2-0 ; 
       drop_ship-mat56.2-0 ; 
       drop_ship-mat7.2-0 ; 
       drop_ship-mat9.2-0 ; 
       static-back4.1-0 ; 
       static-botto1.1-0 ; 
       static-front3.1-0 ; 
       static-mat118.1-0 ; 
       static-mat119.1-0 ; 
       static-mat120.1-0 ; 
       static-nose_white-center.1-1.1-0 ; 
       static-nose_white-center.1-4.1-0 ; 
       static-port_red-left.1-0.1-0 ; 
       static-side1.1-0 ; 
       static-side2.1-0 ; 
       static-starbord_green-right.1-0.1-0 ; 
       static-top3.1-0 ; 
       static-top4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrust1.1-0 ; 
       utl27-lthrust2.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthrust1.1-0 ; 
       utl27-rthrust2.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27.16-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27A/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       drop_ship-t2d1.2-0 ; 
       drop_ship-t2d10.2-0 ; 
       drop_ship-t2d11.2-0 ; 
       drop_ship-t2d12.2-0 ; 
       drop_ship-t2d2.2-0 ; 
       drop_ship-t2d24.2-0 ; 
       drop_ship-t2d37.2-0 ; 
       drop_ship-t2d38.2-0 ; 
       drop_ship-t2d39.2-0 ; 
       drop_ship-t2d40.2-0 ; 
       drop_ship-t2d41.2-0 ; 
       drop_ship-t2d42.2-0 ; 
       drop_ship-t2d43.2-0 ; 
       drop_ship-t2d44.2-0 ; 
       drop_ship-t2d45.2-0 ; 
       drop_ship-t2d46.2-0 ; 
       drop_ship-t2d47.2-0 ; 
       drop_ship-t2d48.2-0 ; 
       drop_ship-t2d49.2-0 ; 
       drop_ship-t2d5.2-0 ; 
       drop_ship-t2d50.2-0 ; 
       drop_ship-t2d7.2-0 ; 
       drop_ship-t2d8.2-0 ; 
       drop_ship-t2d9.2-0 ; 
       drop_ship-t2d93.2-0 ; 
       drop_ship-t2d95.2-0 ; 
       drop_ship-t2d96.2-0 ; 
       static-t2d118.1-0 ; 
       static-t2d119.1-0 ; 
       static-t2d120.1-0 ; 
       static-t2d121.1-0 ; 
       static-t2d122.1-0 ; 
       static-t2d123.1-0 ; 
       static-t2d124.1-0 ; 
       static-t2d125.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 20 110 ; 
       3 20 110 ; 
       4 8 110 ; 
       5 7 110 ; 
       6 3 110 ; 
       7 6 110 ; 
       8 0 110 ; 
       9 20 110 ; 
       10 14 110 ; 
       11 13 110 ; 
       12 3 110 ; 
       13 12 110 ; 
       14 0 110 ; 
       15 3 110 ; 
       16 4 110 ; 
       17 3 110 ; 
       18 10 110 ; 
       19 20 110 ; 
       21 20 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       0 45 300 ; 
       0 48 300 ; 
       0 38 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       1 41 300 ; 
       1 46 300 ; 
       1 49 300 ; 
       3 12 300 ; 
       3 15 300 ; 
       3 18 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 40 300 ; 
       4 14 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       6 3 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 0 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 13 300 ; 
       8 25 300 ; 
       8 28 300 ; 
       10 17 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 6 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 1 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       14 16 300 ; 
       14 26 300 ; 
       14 27 300 ; 
       15 42 300 ; 
       16 44 300 ; 
       17 43 300 ; 
       18 47 300 ; 
       20 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       36 31 401 ; 
       37 27 401 ; 
       38 30 401 ; 
       40 32 401 ; 
       45 28 401 ; 
       46 33 401 ; 
       48 29 401 ; 
       49 34 401 ; 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -10 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 15 -8 0 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 10 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       36 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
