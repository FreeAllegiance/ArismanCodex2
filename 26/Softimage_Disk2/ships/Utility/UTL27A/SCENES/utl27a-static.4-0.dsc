SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.27-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       SCALED-light1_1.1-0 ROOT ; 
       SCALED-light2_1.1-0 ROOT ; 
       SCALED-light3_1.1-0 ROOT ; 
       SCALED-light4_1.1-0 ROOT ; 
       SCALED-light5_1.1-0 ROOT ; 
       SCALED-light6_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       drop_ship-default1.4-0 ; 
       drop_ship-default2.4-0 ; 
       drop_ship-mat1.4-0 ; 
       drop_ship-mat10.4-0 ; 
       drop_ship-mat102.4-0 ; 
       drop_ship-mat104.4-0 ; 
       drop_ship-mat105.4-0 ; 
       drop_ship-mat11.4-0 ; 
       drop_ship-mat12.4-0 ; 
       drop_ship-mat13.4-0 ; 
       drop_ship-mat14.4-0 ; 
       drop_ship-mat15.4-0 ; 
       drop_ship-mat2.4-0 ; 
       drop_ship-mat28.4-0 ; 
       drop_ship-mat29.4-0 ; 
       drop_ship-mat3.4-0 ; 
       drop_ship-mat30.4-0 ; 
       drop_ship-mat31.4-0 ; 
       drop_ship-mat4.4-0 ; 
       drop_ship-mat41.4-0 ; 
       drop_ship-mat42.4-0 ; 
       drop_ship-mat43.4-0 ; 
       drop_ship-mat44.4-0 ; 
       drop_ship-mat45.4-0 ; 
       drop_ship-mat46.4-0 ; 
       drop_ship-mat47.4-0 ; 
       drop_ship-mat49.4-0 ; 
       drop_ship-mat50.4-0 ; 
       drop_ship-mat51.4-0 ; 
       drop_ship-mat52.4-0 ; 
       drop_ship-mat53.4-0 ; 
       drop_ship-mat54.4-0 ; 
       drop_ship-mat55.4-0 ; 
       drop_ship-mat56.4-0 ; 
       drop_ship-mat7.4-0 ; 
       drop_ship-mat9.4-0 ; 
       SCALED-back4.2-0 ; 
       SCALED-botto1.2-0 ; 
       SCALED-front3.2-0 ; 
       SCALED-mat118.2-0 ; 
       SCALED-mat119.2-0 ; 
       SCALED-mat120.2-0 ; 
       SCALED-side1.2-0 ; 
       SCALED-side2.2-0 ; 
       SCALED-top3.2-0 ; 
       SCALED-top4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-utl27_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27A/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       drop_ship-t2d1.5-0 ; 
       drop_ship-t2d10.5-0 ; 
       drop_ship-t2d11.5-0 ; 
       drop_ship-t2d12.5-0 ; 
       drop_ship-t2d2.5-0 ; 
       drop_ship-t2d24.5-0 ; 
       drop_ship-t2d37.5-0 ; 
       drop_ship-t2d38.5-0 ; 
       drop_ship-t2d39.5-0 ; 
       drop_ship-t2d40.5-0 ; 
       drop_ship-t2d41.5-0 ; 
       drop_ship-t2d42.5-0 ; 
       drop_ship-t2d43.5-0 ; 
       drop_ship-t2d44.5-0 ; 
       drop_ship-t2d45.5-0 ; 
       drop_ship-t2d46.5-0 ; 
       drop_ship-t2d47.5-0 ; 
       drop_ship-t2d48.5-0 ; 
       drop_ship-t2d49.5-0 ; 
       drop_ship-t2d5.5-0 ; 
       drop_ship-t2d50.5-0 ; 
       drop_ship-t2d7.5-0 ; 
       drop_ship-t2d8.5-0 ; 
       drop_ship-t2d9.5-0 ; 
       drop_ship-t2d93.5-0 ; 
       drop_ship-t2d95.5-0 ; 
       drop_ship-t2d96.5-0 ; 
       SCALED-t2d118.2-0 ; 
       SCALED-t2d119.2-0 ; 
       SCALED-t2d120.2-0 ; 
       SCALED-t2d121.2-0 ; 
       SCALED-t2d122.2-0 ; 
       SCALED-t2d123.2-0 ; 
       SCALED-t2d124.2-0 ; 
       SCALED-t2d125.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 11 110 ; 
       3 6 110 ; 
       4 5 110 ; 
       5 2 110 ; 
       6 0 110 ; 
       7 10 110 ; 
       8 9 110 ; 
       9 2 110 ; 
       10 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       0 42 300 ; 
       0 44 300 ; 
       0 38 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       1 41 300 ; 
       1 43 300 ; 
       1 45 300 ; 
       2 12 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 40 300 ; 
       3 14 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 0 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       5 3 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       6 13 300 ; 
       6 25 300 ; 
       6 28 300 ; 
       7 17 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 6 300 ; 
       8 1 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       9 19 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       10 16 300 ; 
       10 26 300 ; 
       10 27 300 ; 
       11 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 31 401 ; 
       37 27 401 ; 
       38 30 401 ; 
       40 32 401 ; 
       42 28 401 ; 
       43 33 401 ; 
       44 29 401 ; 
       45 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0.8267195 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
