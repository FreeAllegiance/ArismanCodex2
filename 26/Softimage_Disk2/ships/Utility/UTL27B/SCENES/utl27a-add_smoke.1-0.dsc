SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.13-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_smoke-light1.1-0 ROOT ; 
       add_smoke-light2.1-0 ROOT ; 
       add_smoke-light3.1-0 ROOT ; 
       add_smoke-light4.1-0 ROOT ; 
       add_smoke-light5.1-0 ROOT ; 
       add_smoke-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       add_smoke-back2.1-0 ; 
       add_smoke-back3.1-0 ; 
       add_smoke-back4.1-0 ; 
       add_smoke-back6.1-0 ; 
       add_smoke-back7.1-0 ; 
       add_smoke-back8.1-0 ; 
       add_smoke-back9.1-0 ; 
       add_smoke-botto1.1-0 ; 
       add_smoke-front2.1-0 ; 
       add_smoke-front3.1-0 ; 
       add_smoke-front5.1-0 ; 
       add_smoke-front6.1-0 ; 
       add_smoke-mat117.1-0 ; 
       add_smoke-mat118.1-0 ; 
       add_smoke-mat119.1-0 ; 
       add_smoke-mat120.1-0 ; 
       add_smoke-mat121.1-0 ; 
       add_smoke-mat122.1-0 ; 
       add_smoke-nose_white-center.1-1.1-0 ; 
       add_smoke-nose_white-center.1-4.1-0 ; 
       add_smoke-port_red-left.1-0.1-0 ; 
       add_smoke-side1.1-0 ; 
       add_smoke-side2.1-0 ; 
       add_smoke-sides2.1-0 ; 
       add_smoke-sides3.1-0 ; 
       add_smoke-sides4.1-0 ; 
       add_smoke-starbord_green-right.1-0.1-0 ; 
       add_smoke-top2.1-0 ; 
       add_smoke-top3.1-0 ; 
       add_smoke-top4.1-0 ; 
       add_smoke-top5.1-0 ; 
       add_smoke-top6.1-0 ; 
       drop_ship-default1.1-0 ; 
       drop_ship-default2.1-0 ; 
       drop_ship-mat1.1-0 ; 
       drop_ship-mat10.1-0 ; 
       drop_ship-mat102.1-0 ; 
       drop_ship-mat104.1-0 ; 
       drop_ship-mat105.1-0 ; 
       drop_ship-mat11.1-0 ; 
       drop_ship-mat12.1-0 ; 
       drop_ship-mat13.1-0 ; 
       drop_ship-mat14.1-0 ; 
       drop_ship-mat15.1-0 ; 
       drop_ship-mat2.1-0 ; 
       drop_ship-mat28.1-0 ; 
       drop_ship-mat29.1-0 ; 
       drop_ship-mat3.1-0 ; 
       drop_ship-mat30.1-0 ; 
       drop_ship-mat31.1-0 ; 
       drop_ship-mat4.1-0 ; 
       drop_ship-mat41.1-0 ; 
       drop_ship-mat42.1-0 ; 
       drop_ship-mat43.1-0 ; 
       drop_ship-mat44.1-0 ; 
       drop_ship-mat45.1-0 ; 
       drop_ship-mat46.1-0 ; 
       drop_ship-mat47.1-0 ; 
       drop_ship-mat49.1-0 ; 
       drop_ship-mat50.1-0 ; 
       drop_ship-mat51.1-0 ; 
       drop_ship-mat52.1-0 ; 
       drop_ship-mat53.1-0 ; 
       drop_ship-mat54.1-0 ; 
       drop_ship-mat55.1-0 ; 
       drop_ship-mat56.1-0 ; 
       drop_ship-mat7.1-0 ; 
       drop_ship-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.3-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lsmoke.1-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rsmoke.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27_1.3-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-add_smoke.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       add_smoke-t2d112.1-0 ; 
       add_smoke-t2d113.1-0 ; 
       add_smoke-t2d114.1-0 ; 
       add_smoke-t2d115.1-0 ; 
       add_smoke-t2d116.1-0 ; 
       add_smoke-t2d117.1-0 ; 
       add_smoke-t2d118.1-0 ; 
       add_smoke-t2d119.1-0 ; 
       add_smoke-t2d120.1-0 ; 
       add_smoke-t2d121.1-0 ; 
       add_smoke-t2d122.1-0 ; 
       add_smoke-t2d123.1-0 ; 
       add_smoke-t2d124.1-0 ; 
       add_smoke-t2d125.1-0 ; 
       add_smoke-t2d129.1-0 ; 
       add_smoke-t2d130.1-0 ; 
       add_smoke-t2d131.1-0 ; 
       add_smoke-t2d132.1-0 ; 
       add_smoke-t2d133.1-0 ; 
       add_smoke-t2d134.1-0 ; 
       add_smoke-t2d135.1-0 ; 
       add_smoke-t2d136.1-0 ; 
       add_smoke-t2d137.1-0 ; 
       add_smoke-t2d138.1-0 ; 
       add_smoke-t2d139.1-0 ; 
       add_smoke-t2d140.1-0 ; 
       drop_ship-t2d1.2-0 ; 
       drop_ship-t2d10.2-0 ; 
       drop_ship-t2d11.2-0 ; 
       drop_ship-t2d12.2-0 ; 
       drop_ship-t2d2.2-0 ; 
       drop_ship-t2d24.2-0 ; 
       drop_ship-t2d37.2-0 ; 
       drop_ship-t2d38.2-0 ; 
       drop_ship-t2d39.2-0 ; 
       drop_ship-t2d40.2-0 ; 
       drop_ship-t2d41.2-0 ; 
       drop_ship-t2d42.2-0 ; 
       drop_ship-t2d43.2-0 ; 
       drop_ship-t2d44.2-0 ; 
       drop_ship-t2d45.2-0 ; 
       drop_ship-t2d46.2-0 ; 
       drop_ship-t2d47.2-0 ; 
       drop_ship-t2d48.2-0 ; 
       drop_ship-t2d49.2-0 ; 
       drop_ship-t2d5.2-0 ; 
       drop_ship-t2d50.2-0 ; 
       drop_ship-t2d7.2-0 ; 
       drop_ship-t2d8.2-0 ; 
       drop_ship-t2d9.2-0 ; 
       drop_ship-t2d93.2-0 ; 
       drop_ship-t2d95.2-0 ; 
       drop_ship-t2d96.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 25 110 ; 
       3 25 110 ; 
       4 25 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 12 110 ; 
       9 10 110 ; 
       11 3 110 ; 
       10 11 110 ; 
       12 0 110 ; 
       13 25 110 ; 
       14 19 110 ; 
       17 16 110 ; 
       18 3 110 ; 
       16 18 110 ; 
       19 0 110 ; 
       20 3 110 ; 
       21 7 110 ; 
       22 3 110 ; 
       23 14 110 ; 
       24 25 110 ; 
       26 25 110 ; 
       8 10 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 21 300 ; 
       0 28 300 ; 
       0 9 300 ; 
       0 2 300 ; 
       0 7 300 ; 
       1 15 300 ; 
       1 22 300 ; 
       1 29 300 ; 
       3 44 300 ; 
       3 47 300 ; 
       3 50 300 ; 
       3 66 300 ; 
       3 67 300 ; 
       3 14 300 ; 
       4 16 300 ; 
       4 30 300 ; 
       4 24 300 ; 
       4 3 300 ; 
       4 10 300 ; 
       4 4 300 ; 
       5 17 300 ; 
       5 31 300 ; 
       5 25 300 ; 
       5 5 300 ; 
       5 11 300 ; 
       5 6 300 ; 
       6 12 300 ; 
       6 27 300 ; 
       6 23 300 ; 
       6 0 300 ; 
       6 8 300 ; 
       6 1 300 ; 
       7 46 300 ; 
       7 64 300 ; 
       7 65 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       11 35 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       10 32 300 ; 
       10 41 300 ; 
       10 42 300 ; 
       10 43 300 ; 
       12 45 300 ; 
       12 57 300 ; 
       12 60 300 ; 
       14 49 300 ; 
       14 61 300 ; 
       14 62 300 ; 
       14 63 300 ; 
       14 38 300 ; 
       18 51 300 ; 
       18 52 300 ; 
       18 53 300 ; 
       16 33 300 ; 
       16 54 300 ; 
       16 55 300 ; 
       16 56 300 ; 
       19 48 300 ; 
       19 58 300 ; 
       19 59 300 ; 
       20 18 300 ; 
       21 20 300 ; 
       22 19 300 ; 
       23 26 300 ; 
       25 34 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       36 50 401 ; 
       37 51 401 ; 
       38 52 401 ; 
       39 48 401 ; 
       40 49 401 ; 
       41 27 401 ; 
       42 28 401 ; 
       43 29 401 ; 
       45 31 401 ; 
       47 26 401 ; 
       50 30 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 35 401 ; 
       56 36 401 ; 
       57 39 401 ; 
       58 37 401 ; 
       59 38 401 ; 
       60 40 401 ; 
       61 42 401 ; 
       62 41 401 ; 
       63 43 401 ; 
       64 44 401 ; 
       65 46 401 ; 
       66 45 401 ; 
       67 47 401 ; 
       0 3 401 ; 
       1 5 401 ; 
       2 10 401 ; 
       3 17 401 ; 
       4 19 401 ; 
       5 23 401 ; 
       6 25 401 ; 
       7 6 401 ; 
       8 4 401 ; 
       9 9 401 ; 
       10 18 401 ; 
       11 24 401 ; 
       12 0 401 ; 
       14 11 401 ; 
       16 14 401 ; 
       17 20 401 ; 
       21 7 401 ; 
       22 12 401 ; 
       23 2 401 ; 
       24 16 401 ; 
       25 22 401 ; 
       27 1 401 ; 
       28 8 401 ; 
       29 13 401 ; 
       30 15 401 ; 
       31 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       9 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       10 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -14 0 MPRFLG 0 ; 
       17 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       19 SCHEM 15 -12 0 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -16 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 15 -16 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       32 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       26 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
