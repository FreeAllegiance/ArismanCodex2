SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.15-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_smoke-light1.3-0 ROOT ; 
       add_smoke-light2.3-0 ROOT ; 
       add_smoke-light3.3-0 ROOT ; 
       add_smoke-light4.3-0 ROOT ; 
       add_smoke-light5.3-0 ROOT ; 
       add_smoke-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       add_smoke-back4.1-0 ; 
       add_smoke-botto1.1-0 ; 
       add_smoke-front3.1-0 ; 
       add_smoke-mat118.1-0 ; 
       add_smoke-mat119.1-0 ; 
       add_smoke-mat120.1-0 ; 
       add_smoke-nose_white-center.1-1.1-0 ; 
       add_smoke-nose_white-center.1-4.1-0 ; 
       add_smoke-port_red-left.1-0.1-0 ; 
       add_smoke-side1.1-0 ; 
       add_smoke-side2.1-0 ; 
       add_smoke-starbord_green-right.1-0.1-0 ; 
       add_smoke-top3.1-0 ; 
       add_smoke-top4.1-0 ; 
       drop_ship-default1.1-0 ; 
       drop_ship-default2.1-0 ; 
       drop_ship-mat1.1-0 ; 
       drop_ship-mat10.1-0 ; 
       drop_ship-mat102.1-0 ; 
       drop_ship-mat104.1-0 ; 
       drop_ship-mat105.1-0 ; 
       drop_ship-mat11.1-0 ; 
       drop_ship-mat12.1-0 ; 
       drop_ship-mat13.1-0 ; 
       drop_ship-mat14.1-0 ; 
       drop_ship-mat15.1-0 ; 
       drop_ship-mat2.1-0 ; 
       drop_ship-mat28.1-0 ; 
       drop_ship-mat29.1-0 ; 
       drop_ship-mat3.1-0 ; 
       drop_ship-mat30.1-0 ; 
       drop_ship-mat31.1-0 ; 
       drop_ship-mat4.1-0 ; 
       drop_ship-mat41.1-0 ; 
       drop_ship-mat42.1-0 ; 
       drop_ship-mat43.1-0 ; 
       drop_ship-mat44.1-0 ; 
       drop_ship-mat45.1-0 ; 
       drop_ship-mat46.1-0 ; 
       drop_ship-mat47.1-0 ; 
       drop_ship-mat49.1-0 ; 
       drop_ship-mat50.1-0 ; 
       drop_ship-mat51.1-0 ; 
       drop_ship-mat52.1-0 ; 
       drop_ship-mat53.1-0 ; 
       drop_ship-mat54.1-0 ; 
       drop_ship-mat55.1-0 ; 
       drop_ship-mat56.1-0 ; 
       drop_ship-mat7.1-0 ; 
       drop_ship-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lsmoke.1-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rsmoke.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27_1.5-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-add_smoke.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       add_smoke-t2d118.2-0 ; 
       add_smoke-t2d119.2-0 ; 
       add_smoke-t2d120.2-0 ; 
       add_smoke-t2d121.2-0 ; 
       add_smoke-t2d122.2-0 ; 
       add_smoke-t2d123.2-0 ; 
       add_smoke-t2d124.2-0 ; 
       add_smoke-t2d125.2-0 ; 
       drop_ship-t2d1.3-0 ; 
       drop_ship-t2d10.3-0 ; 
       drop_ship-t2d11.3-0 ; 
       drop_ship-t2d12.3-0 ; 
       drop_ship-t2d2.3-0 ; 
       drop_ship-t2d24.3-0 ; 
       drop_ship-t2d37.3-0 ; 
       drop_ship-t2d38.3-0 ; 
       drop_ship-t2d39.3-0 ; 
       drop_ship-t2d40.3-0 ; 
       drop_ship-t2d41.3-0 ; 
       drop_ship-t2d42.3-0 ; 
       drop_ship-t2d43.3-0 ; 
       drop_ship-t2d44.3-0 ; 
       drop_ship-t2d45.3-0 ; 
       drop_ship-t2d46.3-0 ; 
       drop_ship-t2d47.3-0 ; 
       drop_ship-t2d48.3-0 ; 
       drop_ship-t2d49.3-0 ; 
       drop_ship-t2d5.3-0 ; 
       drop_ship-t2d50.3-0 ; 
       drop_ship-t2d7.3-0 ; 
       drop_ship-t2d8.3-0 ; 
       drop_ship-t2d9.3-0 ; 
       drop_ship-t2d93.3-0 ; 
       drop_ship-t2d95.3-0 ; 
       drop_ship-t2d96.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 22 110 ; 
       3 22 110 ; 
       4 9 110 ; 
       6 7 110 ; 
       8 3 110 ; 
       7 8 110 ; 
       9 0 110 ; 
       10 22 110 ; 
       11 16 110 ; 
       14 13 110 ; 
       15 3 110 ; 
       13 15 110 ; 
       16 0 110 ; 
       17 3 110 ; 
       18 4 110 ; 
       19 3 110 ; 
       20 11 110 ; 
       21 22 110 ; 
       23 22 110 ; 
       5 7 110 ; 
       12 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       0 9 300 ; 
       0 12 300 ; 
       0 2 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       1 5 300 ; 
       1 10 300 ; 
       1 13 300 ; 
       3 26 300 ; 
       3 29 300 ; 
       3 32 300 ; 
       3 48 300 ; 
       3 49 300 ; 
       3 4 300 ; 
       4 28 300 ; 
       4 46 300 ; 
       4 47 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       8 17 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       7 14 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       9 27 300 ; 
       9 39 300 ; 
       9 42 300 ; 
       11 31 300 ; 
       11 43 300 ; 
       11 44 300 ; 
       11 45 300 ; 
       11 20 300 ; 
       15 33 300 ; 
       15 34 300 ; 
       15 35 300 ; 
       13 15 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       16 30 300 ; 
       16 40 300 ; 
       16 41 300 ; 
       17 6 300 ; 
       18 8 300 ; 
       19 7 300 ; 
       20 11 300 ; 
       22 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       18 32 401 ; 
       19 33 401 ; 
       20 34 401 ; 
       21 30 401 ; 
       22 31 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
       27 13 401 ; 
       29 8 401 ; 
       32 12 401 ; 
       34 14 401 ; 
       35 15 401 ; 
       36 16 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 21 401 ; 
       40 19 401 ; 
       41 20 401 ; 
       42 22 401 ; 
       43 24 401 ; 
       44 23 401 ; 
       45 25 401 ; 
       46 26 401 ; 
       47 28 401 ; 
       48 27 401 ; 
       49 29 401 ; 
       0 4 401 ; 
       1 0 401 ; 
       2 3 401 ; 
       4 5 401 ; 
       9 1 401 ; 
       10 6 401 ; 
       12 2 401 ; 
       13 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       6 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -14 0 MPRFLG 0 ; 
       14 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 15 -12 0 MPRFLG 0 ; 
       17 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -16 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 15 -16 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       14 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
