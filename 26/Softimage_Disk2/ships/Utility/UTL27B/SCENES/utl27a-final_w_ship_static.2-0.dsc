SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl27-utl27.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.9-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       final_w_ship_static-light1.2-0 ROOT ; 
       final_w_ship_static-light2.2-0 ROOT ; 
       final_w_ship_static-light3.2-0 ROOT ; 
       final_w_ship_static-light4.2-0 ROOT ; 
       final_w_ship_static-light5.2-0 ROOT ; 
       final_w_ship_static-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       drop_ship-default1.1-0 ; 
       drop_ship-default2.1-0 ; 
       drop_ship-mat1.1-0 ; 
       drop_ship-mat10.1-0 ; 
       drop_ship-mat102.1-0 ; 
       drop_ship-mat104.1-0 ; 
       drop_ship-mat105.1-0 ; 
       drop_ship-mat11.1-0 ; 
       drop_ship-mat12.1-0 ; 
       drop_ship-mat13.1-0 ; 
       drop_ship-mat14.1-0 ; 
       drop_ship-mat15.1-0 ; 
       drop_ship-mat2.1-0 ; 
       drop_ship-mat28.1-0 ; 
       drop_ship-mat29.1-0 ; 
       drop_ship-mat3.1-0 ; 
       drop_ship-mat30.1-0 ; 
       drop_ship-mat31.1-0 ; 
       drop_ship-mat4.1-0 ; 
       drop_ship-mat41.1-0 ; 
       drop_ship-mat42.1-0 ; 
       drop_ship-mat43.1-0 ; 
       drop_ship-mat44.1-0 ; 
       drop_ship-mat45.1-0 ; 
       drop_ship-mat46.1-0 ; 
       drop_ship-mat47.1-0 ; 
       drop_ship-mat49.1-0 ; 
       drop_ship-mat50.1-0 ; 
       drop_ship-mat51.1-0 ; 
       drop_ship-mat52.1-0 ; 
       drop_ship-mat53.1-0 ; 
       drop_ship-mat54.1-0 ; 
       drop_ship-mat55.1-0 ; 
       drop_ship-mat56.1-0 ; 
       drop_ship-mat7.1-0 ; 
       drop_ship-mat9.1-0 ; 
       final_w_ship_static-back2.1-0 ; 
       final_w_ship_static-back3.1-0 ; 
       final_w_ship_static-back4.1-0 ; 
       final_w_ship_static-back6.1-0 ; 
       final_w_ship_static-back7.1-0 ; 
       final_w_ship_static-back8.1-0 ; 
       final_w_ship_static-back9.1-0 ; 
       final_w_ship_static-botto1.1-0 ; 
       final_w_ship_static-front2.1-0 ; 
       final_w_ship_static-front3.1-0 ; 
       final_w_ship_static-front5.1-0 ; 
       final_w_ship_static-front6.1-0 ; 
       final_w_ship_static-mat117.1-0 ; 
       final_w_ship_static-mat118.1-0 ; 
       final_w_ship_static-mat119.1-0 ; 
       final_w_ship_static-mat120.1-0 ; 
       final_w_ship_static-mat121.1-0 ; 
       final_w_ship_static-mat122.1-0 ; 
       final_w_ship_static-nose_white-center.1-1.1-0 ; 
       final_w_ship_static-nose_white-center.1-4.1-0 ; 
       final_w_ship_static-port_red-left.1-0.1-0 ; 
       final_w_ship_static-side1.1-0 ; 
       final_w_ship_static-side2.1-0 ; 
       final_w_ship_static-sides2.1-0 ; 
       final_w_ship_static-sides3.1-0 ; 
       final_w_ship_static-sides4.1-0 ; 
       final_w_ship_static-starbord_green-right.1-0.1-0 ; 
       final_w_ship_static-top2.1-0 ; 
       final_w_ship_static-top3.1-0 ; 
       final_w_ship_static-top4.1-0 ; 
       final_w_ship_static-top5.1-0 ; 
       final_w_ship_static-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.3-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrust1.1-0 ; 
       utl27-lthrust2.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthrust1.1-0 ; 
       utl27-rthrust2.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27.3-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-final_w_ship_static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.1-0 ; 
       drop_ship-t2d10.1-0 ; 
       drop_ship-t2d11.1-0 ; 
       drop_ship-t2d12.1-0 ; 
       drop_ship-t2d2.1-0 ; 
       drop_ship-t2d24.1-0 ; 
       drop_ship-t2d37.1-0 ; 
       drop_ship-t2d38.1-0 ; 
       drop_ship-t2d39.1-0 ; 
       drop_ship-t2d40.1-0 ; 
       drop_ship-t2d41.1-0 ; 
       drop_ship-t2d42.1-0 ; 
       drop_ship-t2d43.1-0 ; 
       drop_ship-t2d44.1-0 ; 
       drop_ship-t2d45.1-0 ; 
       drop_ship-t2d46.1-0 ; 
       drop_ship-t2d47.1-0 ; 
       drop_ship-t2d48.1-0 ; 
       drop_ship-t2d49.1-0 ; 
       drop_ship-t2d5.1-0 ; 
       drop_ship-t2d50.1-0 ; 
       drop_ship-t2d7.1-0 ; 
       drop_ship-t2d8.1-0 ; 
       drop_ship-t2d9.1-0 ; 
       drop_ship-t2d93.1-0 ; 
       drop_ship-t2d95.1-0 ; 
       drop_ship-t2d96.1-0 ; 
       final_w_ship_static-t2d112.1-0 ; 
       final_w_ship_static-t2d113.1-0 ; 
       final_w_ship_static-t2d114.1-0 ; 
       final_w_ship_static-t2d115.1-0 ; 
       final_w_ship_static-t2d116.1-0 ; 
       final_w_ship_static-t2d117.1-0 ; 
       final_w_ship_static-t2d118.1-0 ; 
       final_w_ship_static-t2d119.1-0 ; 
       final_w_ship_static-t2d120.1-0 ; 
       final_w_ship_static-t2d121.1-0 ; 
       final_w_ship_static-t2d122.1-0 ; 
       final_w_ship_static-t2d123.1-0 ; 
       final_w_ship_static-t2d124.1-0 ; 
       final_w_ship_static-t2d125.1-0 ; 
       final_w_ship_static-t2d129.1-0 ; 
       final_w_ship_static-t2d130.1-0 ; 
       final_w_ship_static-t2d131.1-0 ; 
       final_w_ship_static-t2d132.1-0 ; 
       final_w_ship_static-t2d133.1-0 ; 
       final_w_ship_static-t2d134.1-0 ; 
       final_w_ship_static-t2d135.1-0 ; 
       final_w_ship_static-t2d136.1-0 ; 
       final_w_ship_static-t2d137.1-0 ; 
       final_w_ship_static-t2d138.1-0 ; 
       final_w_ship_static-t2d139.1-0 ; 
       final_w_ship_static-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 23 110 ; 
       3 23 110 ; 
       7 11 110 ; 
       8 10 110 ; 
       9 3 110 ; 
       10 9 110 ; 
       11 0 110 ; 
       12 23 110 ; 
       13 17 110 ; 
       14 16 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 0 110 ; 
       18 3 110 ; 
       19 7 110 ; 
       20 3 110 ; 
       21 13 110 ; 
       22 23 110 ; 
       24 23 110 ; 
       4 23 110 ; 
       5 4 110 ; 
       6 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 57 300 ; 
       0 64 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 58 300 ; 
       1 65 300 ; 
       3 12 300 ; 
       3 15 300 ; 
       3 18 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 50 300 ; 
       7 14 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       9 3 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       10 0 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       11 13 300 ; 
       11 25 300 ; 
       11 28 300 ; 
       13 17 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       13 31 300 ; 
       13 6 300 ; 
       15 19 300 ; 
       15 20 300 ; 
       15 21 300 ; 
       16 1 300 ; 
       16 22 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       17 16 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 54 300 ; 
       19 56 300 ; 
       20 55 300 ; 
       21 62 300 ; 
       23 2 300 ; 
       4 52 300 ; 
       4 66 300 ; 
       4 60 300 ; 
       4 39 300 ; 
       4 46 300 ; 
       4 40 300 ; 
       5 53 300 ; 
       5 67 300 ; 
       5 61 300 ; 
       5 41 300 ; 
       5 47 300 ; 
       5 42 300 ; 
       6 48 300 ; 
       6 63 300 ; 
       6 59 300 ; 
       6 36 300 ; 
       6 44 300 ; 
       6 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       23 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       38 37 401 ; 
       43 33 401 ; 
       45 36 401 ; 
       50 38 401 ; 
       57 34 401 ; 
       58 39 401 ; 
       64 35 401 ; 
       65 40 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       44 31 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       59 29 401 ; 
       60 43 401 ; 
       61 49 401 ; 
       63 28 401 ; 
       66 42 401 ; 
       67 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -10 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 25 -8 0 MPRFLG 0 ; 
       12 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -4 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 17.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 25 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 20 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 40 -4 0 MPRFLG 0 ; 
       6 SCHEM 37.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 41.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
