SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.21-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       SCALED-light1.2-0 ROOT ; 
       SCALED-light2.2-0 ROOT ; 
       SCALED-light3.2-0 ROOT ; 
       SCALED-light4.2-0 ROOT ; 
       SCALED-light5.2-0 ROOT ; 
       SCALED-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       drop_ship-default1.1-0 ; 
       drop_ship-default2.1-0 ; 
       drop_ship-mat1.1-0 ; 
       drop_ship-mat10.1-0 ; 
       drop_ship-mat102.1-0 ; 
       drop_ship-mat104.1-0 ; 
       drop_ship-mat105.1-0 ; 
       drop_ship-mat11.1-0 ; 
       drop_ship-mat12.1-0 ; 
       drop_ship-mat13.1-0 ; 
       drop_ship-mat14.1-0 ; 
       drop_ship-mat15.1-0 ; 
       drop_ship-mat2.1-0 ; 
       drop_ship-mat28.1-0 ; 
       drop_ship-mat29.1-0 ; 
       drop_ship-mat3.1-0 ; 
       drop_ship-mat30.1-0 ; 
       drop_ship-mat31.1-0 ; 
       drop_ship-mat4.1-0 ; 
       drop_ship-mat41.1-0 ; 
       drop_ship-mat42.1-0 ; 
       drop_ship-mat43.1-0 ; 
       drop_ship-mat44.1-0 ; 
       drop_ship-mat45.1-0 ; 
       drop_ship-mat46.1-0 ; 
       drop_ship-mat47.1-0 ; 
       drop_ship-mat49.1-0 ; 
       drop_ship-mat50.1-0 ; 
       drop_ship-mat51.1-0 ; 
       drop_ship-mat52.1-0 ; 
       drop_ship-mat53.1-0 ; 
       drop_ship-mat54.1-0 ; 
       drop_ship-mat55.1-0 ; 
       drop_ship-mat56.1-0 ; 
       drop_ship-mat7.1-0 ; 
       drop_ship-mat9.1-0 ; 
       SCALED-back2.1-0 ; 
       SCALED-back3.1-0 ; 
       SCALED-back4.1-0 ; 
       SCALED-back6.1-0 ; 
       SCALED-back7.1-0 ; 
       SCALED-back8.1-0 ; 
       SCALED-back9.1-0 ; 
       SCALED-botto1.1-0 ; 
       SCALED-front2.1-0 ; 
       SCALED-front3.1-0 ; 
       SCALED-front5.1-0 ; 
       SCALED-front6.1-0 ; 
       SCALED-mat117.1-0 ; 
       SCALED-mat118.1-0 ; 
       SCALED-mat119.1-0 ; 
       SCALED-mat120.1-0 ; 
       SCALED-mat121.1-0 ; 
       SCALED-mat122.1-0 ; 
       SCALED-nose_white-center.1-1.1-0 ; 
       SCALED-nose_white-center.1-4.1-0 ; 
       SCALED-port_red-left.1-0.1-0 ; 
       SCALED-side1.1-0 ; 
       SCALED-side2.1-0 ; 
       SCALED-sides2.1-0 ; 
       SCALED-sides3.1-0 ; 
       SCALED-sides4.1-0 ; 
       SCALED-starbord_green-right.1-0.1-0 ; 
       SCALED-top2.1-0 ; 
       SCALED-top3.1-0 ; 
       SCALED-top4.1-0 ; 
       SCALED-top5.1-0 ; 
       SCALED-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-cockpt.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.3-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lsmoke.1-0 ; 
       utl27-lthrust.1-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-missemt.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rsmoke.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthrust.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-trail.1-0 ; 
       utl27-utl27_1.9-0 ROOT ; 
       utl27-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27b-SCALED.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.5-0 ; 
       drop_ship-t2d10.5-0 ; 
       drop_ship-t2d11.5-0 ; 
       drop_ship-t2d12.5-0 ; 
       drop_ship-t2d2.5-0 ; 
       drop_ship-t2d24.5-0 ; 
       drop_ship-t2d37.5-0 ; 
       drop_ship-t2d38.5-0 ; 
       drop_ship-t2d39.5-0 ; 
       drop_ship-t2d40.5-0 ; 
       drop_ship-t2d41.5-0 ; 
       drop_ship-t2d42.5-0 ; 
       drop_ship-t2d43.5-0 ; 
       drop_ship-t2d44.5-0 ; 
       drop_ship-t2d45.5-0 ; 
       drop_ship-t2d46.5-0 ; 
       drop_ship-t2d47.5-0 ; 
       drop_ship-t2d48.5-0 ; 
       drop_ship-t2d49.5-0 ; 
       drop_ship-t2d5.5-0 ; 
       drop_ship-t2d50.5-0 ; 
       drop_ship-t2d7.5-0 ; 
       drop_ship-t2d8.5-0 ; 
       drop_ship-t2d9.5-0 ; 
       drop_ship-t2d93.5-0 ; 
       drop_ship-t2d95.5-0 ; 
       drop_ship-t2d96.5-0 ; 
       SCALED-t2d112.1-0 ; 
       SCALED-t2d113.1-0 ; 
       SCALED-t2d114.1-0 ; 
       SCALED-t2d115.1-0 ; 
       SCALED-t2d116.1-0 ; 
       SCALED-t2d117.1-0 ; 
       SCALED-t2d118.1-0 ; 
       SCALED-t2d119.1-0 ; 
       SCALED-t2d120.1-0 ; 
       SCALED-t2d121.1-0 ; 
       SCALED-t2d122.1-0 ; 
       SCALED-t2d123.1-0 ; 
       SCALED-t2d124.1-0 ; 
       SCALED-t2d125.1-0 ; 
       SCALED-t2d129.1-0 ; 
       SCALED-t2d130.1-0 ; 
       SCALED-t2d131.1-0 ; 
       SCALED-t2d132.1-0 ; 
       SCALED-t2d133.1-0 ; 
       SCALED-t2d134.1-0 ; 
       SCALED-t2d135.1-0 ; 
       SCALED-t2d136.1-0 ; 
       SCALED-t2d137.1-0 ; 
       SCALED-t2d138.1-0 ; 
       SCALED-t2d139.1-0 ; 
       SCALED-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 3 110 ; 
       2 25 110 ; 
       3 25 110 ; 
       4 25 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 12 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       10 11 110 ; 
       11 3 110 ; 
       12 0 110 ; 
       13 25 110 ; 
       14 19 110 ; 
       15 16 110 ; 
       16 18 110 ; 
       17 16 110 ; 
       18 3 110 ; 
       19 0 110 ; 
       20 3 110 ; 
       21 7 110 ; 
       22 3 110 ; 
       23 14 110 ; 
       24 25 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 57 300 ; 
       0 64 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 58 300 ; 
       1 65 300 ; 
       3 12 300 ; 
       3 15 300 ; 
       3 18 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 50 300 ; 
       4 52 300 ; 
       4 66 300 ; 
       4 60 300 ; 
       4 39 300 ; 
       4 46 300 ; 
       4 40 300 ; 
       5 53 300 ; 
       5 67 300 ; 
       5 61 300 ; 
       5 41 300 ; 
       5 47 300 ; 
       5 42 300 ; 
       6 48 300 ; 
       6 63 300 ; 
       6 59 300 ; 
       6 36 300 ; 
       6 44 300 ; 
       6 37 300 ; 
       7 14 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       10 0 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       11 3 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       12 13 300 ; 
       12 25 300 ; 
       12 28 300 ; 
       14 17 300 ; 
       14 29 300 ; 
       14 30 300 ; 
       14 31 300 ; 
       14 6 300 ; 
       16 1 300 ; 
       16 22 300 ; 
       16 23 300 ; 
       16 24 300 ; 
       18 19 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       19 16 300 ; 
       19 26 300 ; 
       19 27 300 ; 
       20 54 300 ; 
       21 56 300 ; 
       22 55 300 ; 
       23 62 300 ; 
       25 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       57 34 401 ; 
       58 39 401 ; 
       59 29 401 ; 
       60 43 401 ; 
       61 49 401 ; 
       63 28 401 ; 
       64 35 401 ; 
       65 40 401 ; 
       66 42 401 ; 
       67 48 401 ; 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 31.25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -14 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       11 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -14 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       17 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       19 SCHEM 15 -12 0 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -16 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 15 -16 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.25 -4 0 SRT 1 1 1 0 0 0 0 0 0.8267195 MPRFLG 0 ; 
       26 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       36 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 14 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       27 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
