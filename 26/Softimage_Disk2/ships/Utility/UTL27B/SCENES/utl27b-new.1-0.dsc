SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.1-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       new-light1.1-0 ROOT ; 
       new-light2.1-0 ROOT ; 
       new-light3.1-0 ROOT ; 
       new-light4.1-0 ROOT ; 
       new-light5.1-0 ROOT ; 
       new-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       new-back2.1-0 ; 
       new-back3.1-0 ; 
       new-back6.1-0 ; 
       new-back7.1-0 ; 
       new-back8.1-0 ; 
       new-back9.1-0 ; 
       new-front2.1-0 ; 
       new-front5.1-0 ; 
       new-front6.1-0 ; 
       new-mat117.1-0 ; 
       new-mat121.1-0 ; 
       new-mat122.1-0 ; 
       new-sides2.1-0 ; 
       new-sides3.1-0 ; 
       new-sides4.1-0 ; 
       new-top2.1-0 ; 
       new-top5.1-0 ; 
       new-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       new2-fuselg3.1-0 ROOT ; 
       new2-fuselg3_1.1-0 ; 
       new2-fuselg3_2.6-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27b-new.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       new-t2d112.1-0 ; 
       new-t2d113.1-0 ; 
       new-t2d114.1-0 ; 
       new-t2d115.1-0 ; 
       new-t2d116.1-0 ; 
       new-t2d117.1-0 ; 
       new-t2d129.1-0 ; 
       new-t2d130.1-0 ; 
       new-t2d131.1-0 ; 
       new-t2d132.1-0 ; 
       new-t2d133.1-0 ; 
       new-t2d134.1-0 ; 
       new-t2d135.1-0 ; 
       new-t2d136.1-0 ; 
       new-t2d137.1-0 ; 
       new-t2d138.1-0 ; 
       new-t2d139.1-0 ; 
       new-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 9 300 ; 
       2 15 300 ; 
       2 12 300 ; 
       2 0 300 ; 
       2 6 300 ; 
       2 1 300 ; 
       0 10 300 ; 
       0 16 300 ; 
       0 13 300 ; 
       0 2 300 ; 
       0 7 300 ; 
       0 3 300 ; 
       1 11 300 ; 
       1 17 300 ; 
       1 14 300 ; 
       1 4 300 ; 
       1 8 300 ; 
       1 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       1 5 401 ; 
       6 4 401 ; 
       9 0 401 ; 
       12 2 401 ; 
       15 1 401 ; 
       10 6 401 ; 
       16 7 401 ; 
       13 8 401 ; 
       2 9 401 ; 
       7 10 401 ; 
       3 11 401 ; 
       11 12 401 ; 
       17 13 401 ; 
       14 14 401 ; 
       4 15 401 ; 
       8 16 401 ; 
       5 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 6.25 -22 0 MPRFLG 0 ; 
       0 SCHEM 21.25 -20 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 21.25 -22 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 0 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 37.5 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 40 -22 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 15 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 0 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 35 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -24 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 15 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 25 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
