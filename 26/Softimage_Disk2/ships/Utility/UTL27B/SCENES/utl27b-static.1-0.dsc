SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.22-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       SCALED-light1_1.1-0 ROOT ; 
       SCALED-light2_1.1-0 ROOT ; 
       SCALED-light3_1.1-0 ROOT ; 
       SCALED-light4_1.1-0 ROOT ; 
       SCALED-light5_1.1-0 ROOT ; 
       SCALED-light6_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       drop_ship-default1.2-0 ; 
       drop_ship-default2.2-0 ; 
       drop_ship-mat1.2-0 ; 
       drop_ship-mat10.2-0 ; 
       drop_ship-mat102.2-0 ; 
       drop_ship-mat104.2-0 ; 
       drop_ship-mat105.2-0 ; 
       drop_ship-mat11.2-0 ; 
       drop_ship-mat12.2-0 ; 
       drop_ship-mat13.2-0 ; 
       drop_ship-mat14.2-0 ; 
       drop_ship-mat15.2-0 ; 
       drop_ship-mat2.2-0 ; 
       drop_ship-mat28.2-0 ; 
       drop_ship-mat29.2-0 ; 
       drop_ship-mat3.2-0 ; 
       drop_ship-mat30.2-0 ; 
       drop_ship-mat31.2-0 ; 
       drop_ship-mat4.2-0 ; 
       drop_ship-mat41.2-0 ; 
       drop_ship-mat42.2-0 ; 
       drop_ship-mat43.2-0 ; 
       drop_ship-mat44.2-0 ; 
       drop_ship-mat45.2-0 ; 
       drop_ship-mat46.2-0 ; 
       drop_ship-mat47.2-0 ; 
       drop_ship-mat49.2-0 ; 
       drop_ship-mat50.2-0 ; 
       drop_ship-mat51.2-0 ; 
       drop_ship-mat52.2-0 ; 
       drop_ship-mat53.2-0 ; 
       drop_ship-mat54.2-0 ; 
       drop_ship-mat55.2-0 ; 
       drop_ship-mat56.2-0 ; 
       drop_ship-mat7.2-0 ; 
       drop_ship-mat9.2-0 ; 
       SCALED-back2.2-0 ; 
       SCALED-back3.2-0 ; 
       SCALED-back4.2-0 ; 
       SCALED-back6.2-0 ; 
       SCALED-back7.2-0 ; 
       SCALED-back8.2-0 ; 
       SCALED-back9.2-0 ; 
       SCALED-botto1.2-0 ; 
       SCALED-front2.2-0 ; 
       SCALED-front3.2-0 ; 
       SCALED-front5.2-0 ; 
       SCALED-front6.2-0 ; 
       SCALED-mat117.2-0 ; 
       SCALED-mat118.2-0 ; 
       SCALED-mat119.2-0 ; 
       SCALED-mat120.2-0 ; 
       SCALED-mat121.2-0 ; 
       SCALED-mat122.2-0 ; 
       SCALED-side1.2-0 ; 
       SCALED-side2.2-0 ; 
       SCALED-sides2.2-0 ; 
       SCALED-sides3.2-0 ; 
       SCALED-sides4.2-0 ; 
       SCALED-top2.2-0 ; 
       SCALED-top3.2-0 ; 
       SCALED-top4.2-0 ; 
       SCALED-top5.2-0 ; 
       SCALED-top6.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.3-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrw.1-0 ; 
       utl27-lthw.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthre.1-0 ; 
       utl27-rthwe.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-utl27_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27b-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.6-0 ; 
       drop_ship-t2d10.6-0 ; 
       drop_ship-t2d11.6-0 ; 
       drop_ship-t2d12.6-0 ; 
       drop_ship-t2d2.6-0 ; 
       drop_ship-t2d24.6-0 ; 
       drop_ship-t2d37.6-0 ; 
       drop_ship-t2d38.6-0 ; 
       drop_ship-t2d39.6-0 ; 
       drop_ship-t2d40.6-0 ; 
       drop_ship-t2d41.6-0 ; 
       drop_ship-t2d42.6-0 ; 
       drop_ship-t2d43.6-0 ; 
       drop_ship-t2d44.6-0 ; 
       drop_ship-t2d45.6-0 ; 
       drop_ship-t2d46.6-0 ; 
       drop_ship-t2d47.6-0 ; 
       drop_ship-t2d48.6-0 ; 
       drop_ship-t2d49.6-0 ; 
       drop_ship-t2d5.6-0 ; 
       drop_ship-t2d50.6-0 ; 
       drop_ship-t2d7.6-0 ; 
       drop_ship-t2d8.6-0 ; 
       drop_ship-t2d9.6-0 ; 
       drop_ship-t2d93.6-0 ; 
       drop_ship-t2d95.6-0 ; 
       drop_ship-t2d96.6-0 ; 
       SCALED-t2d112.2-0 ; 
       SCALED-t2d113.2-0 ; 
       SCALED-t2d114.2-0 ; 
       SCALED-t2d115.2-0 ; 
       SCALED-t2d116.2-0 ; 
       SCALED-t2d117.2-0 ; 
       SCALED-t2d118.2-0 ; 
       SCALED-t2d119.2-0 ; 
       SCALED-t2d120.2-0 ; 
       SCALED-t2d121.2-0 ; 
       SCALED-t2d122.2-0 ; 
       SCALED-t2d123.2-0 ; 
       SCALED-t2d124.2-0 ; 
       SCALED-t2d125.2-0 ; 
       SCALED-t2d129.2-0 ; 
       SCALED-t2d130.2-0 ; 
       SCALED-t2d131.2-0 ; 
       SCALED-t2d132.2-0 ; 
       SCALED-t2d133.2-0 ; 
       SCALED-t2d134.2-0 ; 
       SCALED-t2d135.2-0 ; 
       SCALED-t2d136.2-0 ; 
       SCALED-t2d137.2-0 ; 
       SCALED-t2d138.2-0 ; 
       SCALED-t2d139.2-0 ; 
       SCALED-t2d140.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 8 110 ; 
       8 2 110 ; 
       9 0 110 ; 
       10 13 110 ; 
       11 12 110 ; 
       12 2 110 ; 
       13 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 54 300 ; 
       0 60 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 55 300 ; 
       1 61 300 ; 
       2 12 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 50 300 ; 
       3 52 300 ; 
       3 62 300 ; 
       3 57 300 ; 
       3 39 300 ; 
       3 46 300 ; 
       3 40 300 ; 
       4 53 300 ; 
       4 63 300 ; 
       4 58 300 ; 
       4 41 300 ; 
       4 47 300 ; 
       4 42 300 ; 
       5 48 300 ; 
       5 59 300 ; 
       5 56 300 ; 
       5 36 300 ; 
       5 44 300 ; 
       5 37 300 ; 
       6 14 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 0 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 3 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       9 28 300 ; 
       10 17 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 6 300 ; 
       11 1 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 24 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 16 300 ; 
       13 26 300 ; 
       13 27 300 ; 
       14 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       54 34 401 ; 
       55 39 401 ; 
       56 29 401 ; 
       57 43 401 ; 
       58 49 401 ; 
       59 28 401 ; 
       60 35 401 ; 
       61 40 401 ; 
       62 42 401 ; 
       63 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -10 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0.8267195 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 60 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
