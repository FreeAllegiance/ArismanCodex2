SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.11-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       drop_ship-default1.1-0 ; 
       drop_ship-default2.1-0 ; 
       drop_ship-mat1.1-0 ; 
       drop_ship-mat10.1-0 ; 
       drop_ship-mat102.1-0 ; 
       drop_ship-mat104.1-0 ; 
       drop_ship-mat105.1-0 ; 
       drop_ship-mat11.1-0 ; 
       drop_ship-mat12.1-0 ; 
       drop_ship-mat13.1-0 ; 
       drop_ship-mat14.1-0 ; 
       drop_ship-mat15.1-0 ; 
       drop_ship-mat2.1-0 ; 
       drop_ship-mat28.1-0 ; 
       drop_ship-mat29.1-0 ; 
       drop_ship-mat3.1-0 ; 
       drop_ship-mat30.1-0 ; 
       drop_ship-mat31.1-0 ; 
       drop_ship-mat4.1-0 ; 
       drop_ship-mat41.1-0 ; 
       drop_ship-mat42.1-0 ; 
       drop_ship-mat43.1-0 ; 
       drop_ship-mat44.1-0 ; 
       drop_ship-mat45.1-0 ; 
       drop_ship-mat46.1-0 ; 
       drop_ship-mat47.1-0 ; 
       drop_ship-mat49.1-0 ; 
       drop_ship-mat50.1-0 ; 
       drop_ship-mat51.1-0 ; 
       drop_ship-mat52.1-0 ; 
       drop_ship-mat53.1-0 ; 
       drop_ship-mat54.1-0 ; 
       drop_ship-mat55.1-0 ; 
       drop_ship-mat56.1-0 ; 
       drop_ship-mat7.1-0 ; 
       drop_ship-mat9.1-0 ; 
       static-back2.1-0 ; 
       static-back3.1-0 ; 
       static-back4.1-0 ; 
       static-back6.1-0 ; 
       static-back7.1-0 ; 
       static-back8.1-0 ; 
       static-back9.1-0 ; 
       static-botto1.1-0 ; 
       static-front2.1-0 ; 
       static-front3.1-0 ; 
       static-front5.1-0 ; 
       static-front6.1-0 ; 
       static-mat117.1-0 ; 
       static-mat118.1-0 ; 
       static-mat119.1-0 ; 
       static-mat120.1-0 ; 
       static-mat121.1-0 ; 
       static-mat122.1-0 ; 
       static-side1.1-0 ; 
       static-side2.1-0 ; 
       static-sides2.1-0 ; 
       static-sides3.1-0 ; 
       static-sides4.1-0 ; 
       static-top2.1-0 ; 
       static-top3.1-0 ; 
       static-top4.1-0 ; 
       static-top5.1-0 ; 
       static-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.3-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrust1.1-0 ; 
       utl27-lthrust2.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthrust1.1-0 ; 
       utl27-rthrust2.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-utl27_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/UTL27B/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27b-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.1-0 ; 
       drop_ship-t2d10.1-0 ; 
       drop_ship-t2d11.1-0 ; 
       drop_ship-t2d12.1-0 ; 
       drop_ship-t2d2.1-0 ; 
       drop_ship-t2d24.1-0 ; 
       drop_ship-t2d37.1-0 ; 
       drop_ship-t2d38.1-0 ; 
       drop_ship-t2d39.1-0 ; 
       drop_ship-t2d40.1-0 ; 
       drop_ship-t2d41.1-0 ; 
       drop_ship-t2d42.1-0 ; 
       drop_ship-t2d43.1-0 ; 
       drop_ship-t2d44.1-0 ; 
       drop_ship-t2d45.1-0 ; 
       drop_ship-t2d46.1-0 ; 
       drop_ship-t2d47.1-0 ; 
       drop_ship-t2d48.1-0 ; 
       drop_ship-t2d49.1-0 ; 
       drop_ship-t2d5.1-0 ; 
       drop_ship-t2d50.1-0 ; 
       drop_ship-t2d7.1-0 ; 
       drop_ship-t2d8.1-0 ; 
       drop_ship-t2d9.1-0 ; 
       drop_ship-t2d93.1-0 ; 
       drop_ship-t2d95.1-0 ; 
       drop_ship-t2d96.1-0 ; 
       static-t2d112.1-0 ; 
       static-t2d113.1-0 ; 
       static-t2d114.1-0 ; 
       static-t2d115.1-0 ; 
       static-t2d116.1-0 ; 
       static-t2d117.1-0 ; 
       static-t2d118.1-0 ; 
       static-t2d119.1-0 ; 
       static-t2d120.1-0 ; 
       static-t2d121.1-0 ; 
       static-t2d122.1-0 ; 
       static-t2d123.1-0 ; 
       static-t2d124.1-0 ; 
       static-t2d125.1-0 ; 
       static-t2d129.1-0 ; 
       static-t2d130.1-0 ; 
       static-t2d131.1-0 ; 
       static-t2d132.1-0 ; 
       static-t2d133.1-0 ; 
       static-t2d134.1-0 ; 
       static-t2d135.1-0 ; 
       static-t2d136.1-0 ; 
       static-t2d137.1-0 ; 
       static-t2d138.1-0 ; 
       static-t2d139.1-0 ; 
       static-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 0 110 ; 
       10 13 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 54 300 ; 
       0 60 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 55 300 ; 
       1 61 300 ; 
       2 12 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 50 300 ; 
       3 52 300 ; 
       3 62 300 ; 
       3 57 300 ; 
       3 39 300 ; 
       3 46 300 ; 
       3 40 300 ; 
       4 53 300 ; 
       4 63 300 ; 
       4 58 300 ; 
       4 41 300 ; 
       4 47 300 ; 
       4 42 300 ; 
       5 48 300 ; 
       5 59 300 ; 
       5 56 300 ; 
       5 36 300 ; 
       5 44 300 ; 
       5 37 300 ; 
       6 14 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 3 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       8 0 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       9 28 300 ; 
       10 17 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 6 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       12 1 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       13 16 300 ; 
       13 26 300 ; 
       13 27 300 ; 
       14 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       54 34 401 ; 
       55 39 401 ; 
       56 29 401 ; 
       57 43 401 ; 
       58 49 401 ; 
       59 28 401 ; 
       60 35 401 ; 
       61 40 401 ; 
       62 42 401 ; 
       63 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -10 0 MPRFLG 0 ; 
       1 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -14 0 MPRFLG 0 ; 
       7 SCHEM 0 -8 0 MPRFLG 0 ; 
       8 SCHEM 0 -10 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 10 -14 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 10 -12 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
