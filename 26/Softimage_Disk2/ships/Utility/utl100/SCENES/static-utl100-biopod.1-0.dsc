SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20_biofighter-cam_int1.6-0 ROOT ; 
       fig20_biofighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       fig20_biofighter-mat81.1-0 ; 
       fig20_biofighter-mat88.1-0 ; 
       fig20_biofighter-mat89.1-0 ; 
       fig20_biofighter-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       root-canopy_1.3-0 ; 
       root-root.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl100/PICTURES/utl100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-utl100-biopod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       fig20_biofighter-back.3-0 ; 
       fig20_biofighter-Front.3-0 ; 
       fig20_biofighter-Rear_front.3-0 ; 
       fig20_biofighter-Side.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 14.72042 2.27411 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.6813 -2.232675 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25.19862 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.87009 -2.047707 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 30.75736 -1.893567 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.74152 -3.997071 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25.25885 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.93032 -3.812103 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30.81758 -3.657963 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
