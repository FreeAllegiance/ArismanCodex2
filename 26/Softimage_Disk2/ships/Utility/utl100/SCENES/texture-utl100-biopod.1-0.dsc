SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20_biofighter-cam_int1.4-0 ROOT ; 
       fig20_biofighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig20_biofighter-mat81.1-0 ; 
       fig20_biofighter-mat88.1-0 ; 
       fig20_biofighter-mat89.1-0 ; 
       fig20_biofighter-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       root-canopy_1.3-0 ; 
       root-cockpt.1-0 ; 
       root-lthrust.1-0 ; 
       root-root.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl100/PICTURES/utl100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl100-biopod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       fig20_biofighter-back.3-0 ; 
       fig20_biofighter-Front.3-0 ; 
       fig20_biofighter-Rear_front.3-0 ; 
       fig20_biofighter-Side.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       5 0 300 ; 
       6 1 300 ; 
       7 3 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 14.72042 2.27411 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.6813 -2.232675 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25.19862 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.87009 -2.047707 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30.75736 -1.893567 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 22.74152 -3.997071 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25.25885 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.93032 -3.812103 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 30.81758 -3.657963 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
