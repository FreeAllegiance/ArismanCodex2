SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.22-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       Utl101_Miner-back1.1-0 ; 
       Utl101_Miner-back2.2-0 ; 
       Utl101_Miner-back4.1-0 ; 
       Utl101_Miner-back5.1-0 ; 
       Utl101_Miner-bottom1.2-0 ; 
       Utl101_Miner-mat100.1-0 ; 
       Utl101_Miner-mat101.1-0 ; 
       Utl101_Miner-mat102.1-0 ; 
       Utl101_Miner-mat87.2-0 ; 
       Utl101_Miner-mat89.1-0 ; 
       Utl101_Miner-mat90.1-0 ; 
       Utl101_Miner-mat91.2-0 ; 
       Utl101_Miner-mat92.2-0 ; 
       Utl101_Miner-mat93.2-0 ; 
       Utl101_Miner-mat94.2-0 ; 
       Utl101_Miner-mat95.2-0 ; 
       Utl101_Miner-mat96.2-0 ; 
       Utl101_Miner-mat97.2-0 ; 
       Utl101_Miner-mat98.2-0 ; 
       Utl101_Miner-mat99.1-0 ; 
       Utl101_Miner-Side.2-0 ; 
       Utl101_Miner-side1.2-0 ; 
       Utl101_Miner-top1.2-0 ; 
       Utl101_Miner-wing1.1-0 ; 
       Utl101_Miner-wing2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl14.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru29.1-0 ; 
       rev_miner-extru30.1-0 ; 
       rev_miner-extru31.1-0 ; 
       rev_miner-extru32.1-0 ; 
       rev_miner-extru4.18-0 ROOT ; 
       rev_miner-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl101/PICTURES/utl101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-Utl101-Miner.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       Utl101_Miner-back1.3-0 ; 
       Utl101_Miner-side.3-0 ; 
       Utl101_Miner-side_1.4-0 ; 
       Utl101_Miner-t2d1.3-0 ; 
       Utl101_Miner-t2d14.1-0 ; 
       Utl101_Miner-t2d15.2-0 ; 
       Utl101_Miner-t2d16.2-0 ; 
       Utl101_Miner-t2d17.1-0 ; 
       Utl101_Miner-t2d18.1-0 ; 
       Utl101_Miner-t2d19.1-0 ; 
       Utl101_Miner-t2d2.2-0 ; 
       Utl101_Miner-t2d20.1-0 ; 
       Utl101_Miner-t2d21.1-0 ; 
       Utl101_Miner-t2d22.1-0 ; 
       Utl101_Miner-t2d23.1-0 ; 
       Utl101_Miner-t2d24.2-0 ; 
       Utl101_Miner-t2d25.2-0 ; 
       Utl101_Miner-t2d26.2-0 ; 
       Utl101_Miner-t2d27.1-0 ; 
       Utl101_Miner-t2d4.1-0 ; 
       Utl101_Miner-t2d5.3-0 ; 
       Utl101_Miner-t2d7.3-0 ; 
       Utl101_Miner-top.3-0 ; 
       Utl101_Miner-top1.3-0 ; 
       Utl101_Miner-top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 14 110 ; 
       11 6 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       2 14 110 ; 
       0 4 110 ; 
       1 14 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 14 110 ; 
       7 14 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 13 300 ; 
       6 3 300 ; 
       6 14 300 ; 
       11 24 300 ; 
       12 15 300 ; 
       13 16 300 ; 
       2 18 300 ; 
       0 5 300 ; 
       1 17 300 ; 
       3 19 300 ; 
       4 6 300 ; 
       5 11 300 ; 
       5 1 300 ; 
       5 12 300 ; 
       7 8 300 ; 
       7 21 300 ; 
       7 2 300 ; 
       8 23 300 ; 
       9 9 300 ; 
       10 10 300 ; 
       14 20 300 ; 
       14 22 300 ; 
       14 4 300 ; 
       14 0 300 ; 
       15 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 20 401 ; 
       13 7 401 ; 
       4 4 401 ; 
       8 5 401 ; 
       9 10 401 ; 
       10 19 401 ; 
       11 21 401 ; 
       12 23 401 ; 
       3 8 401 ; 
       14 24 401 ; 
       24 9 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       20 1 401 ; 
       21 2 401 ; 
       22 22 401 ; 
       23 3 401 ; 
       18 14 401 ; 
       2 6 401 ; 
       19 15 401 ; 
       5 16 401 ; 
       6 17 401 ; 
       7 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 0 0 SRT 1.525208 1 1 0 0 0 0 0.09783381 0 MPRFLG 0 ; 
       15 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
