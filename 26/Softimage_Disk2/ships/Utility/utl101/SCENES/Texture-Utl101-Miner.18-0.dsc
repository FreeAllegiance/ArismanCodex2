SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.20-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       edit_nulls-mat70.4-0 ; 
       rev_miner-mat77.3-0 ; 
       rev_miner-mat80.3-0 ; 
       rev_miner-mat82.3-0 ; 
       rev_miner-mat84.3-0 ; 
       rev_miner-mat86.3-0 ; 
       Utl101_Miner-back1.1-0 ; 
       Utl101_Miner-back2.2-0 ; 
       Utl101_Miner-back4.1-0 ; 
       Utl101_Miner-back5.1-0 ; 
       Utl101_Miner-bottom1.2-0 ; 
       Utl101_Miner-mat100.1-0 ; 
       Utl101_Miner-mat101.1-0 ; 
       Utl101_Miner-mat102.1-0 ; 
       Utl101_Miner-mat87.2-0 ; 
       Utl101_Miner-mat89.1-0 ; 
       Utl101_Miner-mat90.1-0 ; 
       Utl101_Miner-mat91.2-0 ; 
       Utl101_Miner-mat92.2-0 ; 
       Utl101_Miner-mat93.2-0 ; 
       Utl101_Miner-mat94.2-0 ; 
       Utl101_Miner-mat95.2-0 ; 
       Utl101_Miner-mat96.2-0 ; 
       Utl101_Miner-mat97.2-0 ; 
       Utl101_Miner-mat98.2-0 ; 
       Utl101_Miner-mat99.1-0 ; 
       Utl101_Miner-Side.2-0 ; 
       Utl101_Miner-side1.2-0 ; 
       Utl101_Miner-top1.2-0 ; 
       Utl101_Miner-wing1.1-0 ; 
       Utl101_Miner-wing2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       rev_miner-cockpt.3-0 ; 
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru29.1-0 ; 
       rev_miner-extru30.1-0 ; 
       rev_miner-extru31.1-0 ; 
       rev_miner-extru32.1-0 ; 
       rev_miner-extru4.16-0 ROOT ; 
       rev_miner-lsmoke.3-0 ; 
       rev_miner-lthrust.3-0 ; 
       rev_miner-rsmoke.3-0 ; 
       rev_miner-rthrust.3-0 ; 
       rev_miner-sphere1.1-0 ; 
       rev_miner-SS01.3-0 ; 
       rev_miner-SS02.3-0 ; 
       rev_miner-SS03.3-0 ; 
       rev_miner-SS04.3-0 ; 
       rev_miner-SS05.3-0 ; 
       rev_miner-SS06.3-0 ; 
       rev_miner-trail.3-0 ; 
       Utl101_Miner-cyl14.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl101/PICTURES/utl101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-Utl101-Miner.18-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       Utl101_Miner-back1.3-0 ; 
       Utl101_Miner-side.3-0 ; 
       Utl101_Miner-side_1.4-0 ; 
       Utl101_Miner-t2d1.3-0 ; 
       Utl101_Miner-t2d14.1-0 ; 
       Utl101_Miner-t2d15.2-0 ; 
       Utl101_Miner-t2d16.2-0 ; 
       Utl101_Miner-t2d17.1-0 ; 
       Utl101_Miner-t2d18.1-0 ; 
       Utl101_Miner-t2d19.1-0 ; 
       Utl101_Miner-t2d2.2-0 ; 
       Utl101_Miner-t2d20.1-0 ; 
       Utl101_Miner-t2d21.1-0 ; 
       Utl101_Miner-t2d22.1-0 ; 
       Utl101_Miner-t2d23.1-0 ; 
       Utl101_Miner-t2d24.1-0 ; 
       Utl101_Miner-t2d25.1-0 ; 
       Utl101_Miner-t2d26.1-0 ; 
       Utl101_Miner-t2d4.1-0 ; 
       Utl101_Miner-t2d5.3-0 ; 
       Utl101_Miner-t2d7.3-0 ; 
       Utl101_Miner-top.3-0 ; 
       Utl101_Miner-top1.3-0 ; 
       Utl101_Miner-top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 14 110 ; 
       11 6 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       0 14 110 ; 
       1 4 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 14 110 ; 
       7 14 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 4 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 14 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 19 300 ; 
       6 9 300 ; 
       6 20 300 ; 
       11 30 300 ; 
       12 21 300 ; 
       13 22 300 ; 
       27 24 300 ; 
       1 11 300 ; 
       2 23 300 ; 
       3 25 300 ; 
       4 12 300 ; 
       5 17 300 ; 
       5 7 300 ; 
       5 18 300 ; 
       7 14 300 ; 
       7 27 300 ; 
       7 8 300 ; 
       8 29 300 ; 
       9 15 300 ; 
       10 16 300 ; 
       14 26 300 ; 
       14 28 300 ; 
       14 10 300 ; 
       14 6 300 ; 
       19 13 300 ; 
       20 0 300 ; 
       21 3 300 ; 
       22 1 300 ; 
       23 4 300 ; 
       24 2 300 ; 
       25 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 19 401 ; 
       19 7 401 ; 
       10 4 401 ; 
       14 5 401 ; 
       15 10 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 22 401 ; 
       9 8 401 ; 
       20 23 401 ; 
       30 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       26 1 401 ; 
       27 2 401 ; 
       28 21 401 ; 
       29 3 401 ; 
       24 14 401 ; 
       8 6 401 ; 
       25 15 401 ; 
       11 16 401 ; 
       12 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 70 -4 0 MPRFLG 0 ; 
       12 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 70 -6 0 MPRFLG 0 ; 
       27 SCHEM 92.5 0 0 SRT 0.1383151 0.01493802 0.1383151 -1.570796 3.141593 0 0.1571502 0.711882 -1.963307 MPRFLG 0 ; 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 65 -2 0 MPRFLG 0 ; 
       3 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 35 -4 0 MPRFLG 0 ; 
       5 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 60 -2 0 MPRFLG 0 ; 
       8 SCHEM 45 -4 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 45 -6 0 MPRFLG 0 ; 
       14 SCHEM 46.25 0 0 SRT 1.525208 1 1 0 0 0 0 0.09783381 0 MPRFLG 0 ; 
       15 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 35 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 87.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 90 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 85 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
