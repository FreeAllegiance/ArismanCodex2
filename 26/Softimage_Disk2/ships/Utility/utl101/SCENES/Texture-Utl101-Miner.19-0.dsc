SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.21-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       edit_nulls-mat70.4-0 ; 
       rev_miner-mat77.3-0 ; 
       rev_miner-mat80.3-0 ; 
       rev_miner-mat82.3-0 ; 
       rev_miner-mat84.3-0 ; 
       rev_miner-mat86.3-0 ; 
       Utl101_Miner-back1.1-0 ; 
       Utl101_Miner-back2.2-0 ; 
       Utl101_Miner-back4.1-0 ; 
       Utl101_Miner-back5.1-0 ; 
       Utl101_Miner-bottom1.2-0 ; 
       Utl101_Miner-mat100.1-0 ; 
       Utl101_Miner-mat101.1-0 ; 
       Utl101_Miner-mat102.1-0 ; 
       Utl101_Miner-mat87.2-0 ; 
       Utl101_Miner-mat89.1-0 ; 
       Utl101_Miner-mat90.1-0 ; 
       Utl101_Miner-mat91.2-0 ; 
       Utl101_Miner-mat92.2-0 ; 
       Utl101_Miner-mat93.2-0 ; 
       Utl101_Miner-mat94.2-0 ; 
       Utl101_Miner-mat95.2-0 ; 
       Utl101_Miner-mat96.2-0 ; 
       Utl101_Miner-mat97.2-0 ; 
       Utl101_Miner-mat98.2-0 ; 
       Utl101_Miner-mat99.1-0 ; 
       Utl101_Miner-Side.2-0 ; 
       Utl101_Miner-side1.2-0 ; 
       Utl101_Miner-top1.2-0 ; 
       Utl101_Miner-wing1.1-0 ; 
       Utl101_Miner-wing2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       rev_miner-cockpt.3-0 ; 
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl14.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru29.1-0 ; 
       rev_miner-extru30.1-0 ; 
       rev_miner-extru31.1-0 ; 
       rev_miner-extru32.1-0 ; 
       rev_miner-extru4.17-0 ROOT ; 
       rev_miner-lsmoke.3-0 ; 
       rev_miner-lthrust.3-0 ; 
       rev_miner-rsmoke.3-0 ; 
       rev_miner-rthrust.3-0 ; 
       rev_miner-sphere1.1-0 ; 
       rev_miner-SS01.3-0 ; 
       rev_miner-SS02.3-0 ; 
       rev_miner-SS03.3-0 ; 
       rev_miner-SS04.3-0 ; 
       rev_miner-SS05.3-0 ; 
       rev_miner-SS06.3-0 ; 
       rev_miner-trail.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl101/PICTURES/utl101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-Utl101-Miner.19-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       Utl101_Miner-back1.3-0 ; 
       Utl101_Miner-side.3-0 ; 
       Utl101_Miner-side_1.4-0 ; 
       Utl101_Miner-t2d1.3-0 ; 
       Utl101_Miner-t2d14.1-0 ; 
       Utl101_Miner-t2d15.2-0 ; 
       Utl101_Miner-t2d16.2-0 ; 
       Utl101_Miner-t2d17.1-0 ; 
       Utl101_Miner-t2d18.1-0 ; 
       Utl101_Miner-t2d19.1-0 ; 
       Utl101_Miner-t2d2.2-0 ; 
       Utl101_Miner-t2d20.1-0 ; 
       Utl101_Miner-t2d21.1-0 ; 
       Utl101_Miner-t2d22.1-0 ; 
       Utl101_Miner-t2d23.1-0 ; 
       Utl101_Miner-t2d24.2-0 ; 
       Utl101_Miner-t2d25.2-0 ; 
       Utl101_Miner-t2d26.2-0 ; 
       Utl101_Miner-t2d27.1-0 ; 
       Utl101_Miner-t2d4.1-0 ; 
       Utl101_Miner-t2d5.3-0 ; 
       Utl101_Miner-t2d7.3-0 ; 
       Utl101_Miner-top.3-0 ; 
       Utl101_Miner-top1.3-0 ; 
       Utl101_Miner-top2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 15 110 ; 
       12 7 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       3 15 110 ; 
       0 15 110 ; 
       1 5 110 ; 
       2 15 110 ; 
       4 15 110 ; 
       5 4 110 ; 
       6 15 110 ; 
       8 15 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       16 15 110 ; 
       17 15 110 ; 
       18 15 110 ; 
       19 15 110 ; 
       20 5 110 ; 
       21 15 110 ; 
       22 15 110 ; 
       23 15 110 ; 
       24 15 110 ; 
       25 15 110 ; 
       26 15 110 ; 
       27 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 19 300 ; 
       7 9 300 ; 
       7 20 300 ; 
       12 30 300 ; 
       13 21 300 ; 
       14 22 300 ; 
       3 24 300 ; 
       1 11 300 ; 
       2 23 300 ; 
       4 25 300 ; 
       5 12 300 ; 
       6 17 300 ; 
       6 7 300 ; 
       6 18 300 ; 
       8 14 300 ; 
       8 27 300 ; 
       8 8 300 ; 
       9 29 300 ; 
       10 15 300 ; 
       11 16 300 ; 
       15 26 300 ; 
       15 28 300 ; 
       15 10 300 ; 
       15 6 300 ; 
       20 13 300 ; 
       21 0 300 ; 
       22 3 300 ; 
       23 1 300 ; 
       24 4 300 ; 
       25 2 300 ; 
       26 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 20 401 ; 
       19 7 401 ; 
       10 4 401 ; 
       14 5 401 ; 
       15 10 401 ; 
       16 19 401 ; 
       17 21 401 ; 
       18 23 401 ; 
       9 8 401 ; 
       20 24 401 ; 
       30 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       26 1 401 ; 
       27 2 401 ; 
       28 22 401 ; 
       29 3 401 ; 
       24 14 401 ; 
       8 6 401 ; 
       25 15 401 ; 
       11 16 401 ; 
       12 17 401 ; 
       13 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 MPRFLG 0 ; 
       3 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 45 -2 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 38.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 40 -6 0 MPRFLG 0 ; 
       15 SCHEM 27.5 0 0 SRT 1.525208 1 1 0 0 0 0 0.09783381 0 MPRFLG 0 ; 
       16 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 35 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 2.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
