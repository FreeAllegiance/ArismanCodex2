SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl101-cam_int1.2-0 ROOT ; 
       utl101-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.2-0 ; 
       utl101-mat71.1-0 ; 
       utl101-mat75.1-0 ; 
       utl101-mat77.1-0 ; 
       utl101-mat78.1-0 ; 
       utl101-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       good-zz_base.1-0 ROOT ; 
       utl101-bthrust.1-0 ROOT ; 
       utl101-cockpt.1-0 ROOT ; 
       utl101-cyl1.1-0 ; 
       utl101-cyl3.1-0 ; 
       utl101-cyl6.1-0 ; 
       utl101-cyl7.1-0 ; 
       utl101-lsmoke.1-0 ROOT ; 
       utl101-lthrust.1-0 ROOT ; 
       utl101-lwepemt.1-0 ROOT ; 
       utl101-missemt.1-0 ROOT ; 
       utl101-rsmoke.1-0 ROOT ; 
       utl101-rthrust.1-0 ROOT ; 
       utl101-rwepemt.1-0 ROOT ; 
       utl101-SS01.1-0 ROOT ; 
       utl101-SS02.1-0 ROOT ; 
       utl101-SS03.1-0 ROOT ; 
       utl101-SS04.1-0 ROOT ; 
       utl101-SS05.1-0 ROOT ; 
       utl101-SS06.1-0 ROOT ; 
       utl101-trail.1-0 ROOT ; 
       utl101-tthrust.1-0 ROOT ; 
       utl101-zz_base.1-0 ; 
       utl101-zz_base_1.2-0 ROOT ; 
       utl101-zz_base1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       base_model-utl101.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 23 110 ; 
       6 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       22 23 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 90.81196 4.687069 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 90.81196 2.68707 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 100.1153 16.37322 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -3.034166e-008 -2.662327 -13.04863 MPRFLG 0 ; 
       2 SCHEM 104.2982 17.39203 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -6.428331e-022 0.5764374 3.909827 MPRFLG 0 ; 
       3 SCHEM 87.12914 12.62788 0 MPRFLG 0 ; 
       6 SCHEM 89.62914 10.62788 0 MPRFLG 0 ; 
       4 SCHEM 84.62914 10.62788 0 MPRFLG 0 ; 
       7 SCHEM 96.41361 17.6194 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 2.6633 -1.462093e-007 -13.04863 MPRFLG 0 ; 
       8 SCHEM 100.1751 17.08051 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 2.6633 -1.462093e-007 -13.04863 MPRFLG 0 ; 
       9 SCHEM 107.046 16.63171 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 8.595625 -0.03049529 -8.536576 MPRFLG 0 ; 
       10 SCHEM 97.0749 14.17726 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 3.592577e-022 -1.815619 10.60005 MPRFLG 0 ; 
       11 SCHEM 96.42121 16.81107 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 -2.358896e-021 -2.6633 0 -13.04863 MPRFLG 0 ; 
       12 SCHEM 100.1567 15.56489 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 -2.358896e-021 -2.6633 0 -13.04863 MPRFLG 0 ; 
       13 SCHEM 107.048 17.36948 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -8.595625 -0.03049529 -8.536576 MPRFLG 0 ; 
       14 SCHEM 96.81301 11.77794 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 21.7829 -0.3056388 -17.25257 MPRFLG 0 ; 
       15 SCHEM 96.86201 11.04415 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -21.7829 -0.3056388 -17.25257 MPRFLG 0 ; 
       16 SCHEM 99.14201 11.74859 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 -2.358896e-021 6.87765 8.732319 -12.58549 MPRFLG 0 ; 
       17 SCHEM 99.1663 11.0148 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -6.87765 8.732319 -12.58549 MPRFLG 0 ; 
       18 SCHEM 101.642 11.74859 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 -2.358896e-021 7.572831 -0.02419401 -10.78291 MPRFLG 0 ; 
       19 SCHEM 101.6177 11.0148 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -7.572831 -0.02419401 -10.78291 MPRFLG 0 ; 
       20 SCHEM 93.50944 18.79822 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 -5.911491e-024 -3.579809e-023 -1.402849e-021 -0.02065635 -12.82181 MPRFLG 0 ; 
       21 SCHEM 100.1674 17.85517 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 3.141593 -2.358896e-021 -2.705746e-008 2.662327 -13.04863 MPRFLG 0 ; 
       23 SCHEM 88.37914 14.62788 0 USR SRT 1.142 1.020948 2.130414 0 0 0 0 0 4.582694 MPRFLG 0 ; 
       24 SCHEM 82.95935 15.18976 0 USR DISPLAY 0 0 SRT 0.8527099 0.7623227 1.111934 0 0 0 0 0 9.50353 MPRFLG 0 ; 
       5 SCHEM 87.12914 10.62788 0 MPRFLG 0 ; 
       22 SCHEM 92.12914 12.62788 0 MPRFLG 0 ; 
       0 SCHEM 83.00645 14.54779 0 USR WIRECOL 6 7 DISPLAY 0 0 SRT 1 0.8939999 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
