SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.2-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.2-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 90     
       edit_nulls-mat70.3-0 ; 
       rev_miner-default1.2-0 ; 
       rev_miner-default1_1.2-0 ; 
       rev_miner-default1_1_1.2-0 ; 
       rev_miner-default1_3.2-0 ; 
       rev_miner-default10.2-0 ; 
       rev_miner-default10_1.2-0 ; 
       rev_miner-default10_1_1.2-0 ; 
       rev_miner-default10_3.2-0 ; 
       rev_miner-default11.2-0 ; 
       rev_miner-default11_1.2-0 ; 
       rev_miner-default11_1_1.2-0 ; 
       rev_miner-default11_3.2-0 ; 
       rev_miner-default12.2-0 ; 
       rev_miner-default12_1.2-0 ; 
       rev_miner-default12_1_1.2-0 ; 
       rev_miner-default12_3.2-0 ; 
       rev_miner-default13.2-0 ; 
       rev_miner-default13_1.2-0 ; 
       rev_miner-default13_1_1.2-0 ; 
       rev_miner-default13_3.2-0 ; 
       rev_miner-default14.2-0 ; 
       rev_miner-default14_1.2-0 ; 
       rev_miner-default14_1_1.2-0 ; 
       rev_miner-default14_3.2-0 ; 
       rev_miner-default15.2-0 ; 
       rev_miner-default15_1.2-0 ; 
       rev_miner-default15_1_1.2-0 ; 
       rev_miner-default15_3.2-0 ; 
       rev_miner-default16.2-0 ; 
       rev_miner-default16_1.2-0 ; 
       rev_miner-default16_1_1.2-0 ; 
       rev_miner-default16_3.2-0 ; 
       rev_miner-default17.2-0 ; 
       rev_miner-default17_1.2-0 ; 
       rev_miner-default17_1_1.2-0 ; 
       rev_miner-default17_3.2-0 ; 
       rev_miner-default18.2-0 ; 
       rev_miner-default18_1.2-0 ; 
       rev_miner-default18_1_1.2-0 ; 
       rev_miner-default18_3.2-0 ; 
       rev_miner-default19.2-0 ; 
       rev_miner-default19_1.2-0 ; 
       rev_miner-default19_1_1.2-0 ; 
       rev_miner-default19_3.2-0 ; 
       rev_miner-default2.2-0 ; 
       rev_miner-default2_1.2-0 ; 
       rev_miner-default2_1_1.2-0 ; 
       rev_miner-default2_3.2-0 ; 
       rev_miner-default20.2-0 ; 
       rev_miner-default20_1.2-0 ; 
       rev_miner-default20_1_1.2-0 ; 
       rev_miner-default20_3.2-0 ; 
       rev_miner-default21.2-0 ; 
       rev_miner-default21_1.2-0 ; 
       rev_miner-default21_1_1.2-0 ; 
       rev_miner-default21_3.2-0 ; 
       rev_miner-default3.2-0 ; 
       rev_miner-default3_1.2-0 ; 
       rev_miner-default3_1_1.2-0 ; 
       rev_miner-default3_3.2-0 ; 
       rev_miner-default4.2-0 ; 
       rev_miner-default4_1.2-0 ; 
       rev_miner-default4_1_1.2-0 ; 
       rev_miner-default4_3.2-0 ; 
       rev_miner-default5.2-0 ; 
       rev_miner-default5_1.2-0 ; 
       rev_miner-default5_1_1.2-0 ; 
       rev_miner-default5_3.2-0 ; 
       rev_miner-default6.2-0 ; 
       rev_miner-default6_1.2-0 ; 
       rev_miner-default6_1_1.2-0 ; 
       rev_miner-default6_3.2-0 ; 
       rev_miner-default7.2-0 ; 
       rev_miner-default7_1.2-0 ; 
       rev_miner-default7_1_1.2-0 ; 
       rev_miner-default7_3.2-0 ; 
       rev_miner-default8.2-0 ; 
       rev_miner-default8_1.2-0 ; 
       rev_miner-default8_1_1.2-0 ; 
       rev_miner-default8_3.2-0 ; 
       rev_miner-default9.2-0 ; 
       rev_miner-default9_1.2-0 ; 
       rev_miner-default9_1_1.2-0 ; 
       rev_miner-default9_3.2-0 ; 
       rev_miner-mat77.2-0 ; 
       rev_miner-mat80.2-0 ; 
       rev_miner-mat82.2-0 ; 
       rev_miner-mat84.2-0 ; 
       rev_miner-mat86.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       rev_miner-cockpt.3-0 ; 
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl10.1-0 ROOT ; 
       rev_miner-cyl12.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-cyl9.1-0 ROOT ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru19.1-0 ; 
       rev_miner-extru22.1-0 ; 
       rev_miner-extru25.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru28.1-0 ; 
       rev_miner-extru4.2-0 ROOT ; 
       rev_miner-lsmoke.3-0 ; 
       rev_miner-lthrust.3-0 ; 
       rev_miner-rsmoke.3-0 ; 
       rev_miner-rthrust.3-0 ; 
       rev_miner-sphere1.1-0 ; 
       rev_miner-SS01.3-0 ; 
       rev_miner-SS02.3-0 ; 
       rev_miner-SS03.3-0 ; 
       rev_miner-SS04.3-0 ; 
       rev_miner-SS05.3-0 ; 
       rev_miner-SS06.3-0 ; 
       rev_miner-trail.3-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       miner-utl101.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 17 110 ; 
       4 17 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       0 17 110 ; 
       1 6 110 ; 
       5 17 110 ; 
       6 5 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 8 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       22 6 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 17 110 ; 
       26 17 110 ; 
       27 17 110 ; 
       28 17 110 ; 
       29 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       23 0 300 ; 
       24 87 300 ; 
       25 85 300 ; 
       26 88 300 ; 
       27 86 300 ; 
       28 89 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2065.219 9.933495 0 USR MPRFLG 0 ; 
       1 SCHEM 2065.219 7.933495 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 2095.764 31.68521 0 MPRFLG 0 ; 
       4 SCHEM 2093.264 31.68521 0 MPRFLG 0 ; 
       18 SCHEM 2091.216 42.00329 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2093.487 41.27283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2098.14 41.97251 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2075.764 27.68521 0 MPRFLG 0 ; 
       2 SCHEM 2101.043 28.16934 0 USR SRT 0.9999999 1.102893 1 -1.152886 0.5235988 -0.8366936 0.2039648 1.049178 -0.2293439 MPRFLG 0 ; 
       5 SCHEM 2077.014 31.68521 0 MPRFLG 0 ; 
       6 SCHEM 2077.014 29.68521 0 MPRFLG 0 ; 
       7 SCHEM 2098.689 27.9496 0 USR SRT 0.9999999 1.102893 1 1.152886 2.617994 0.8366936 -0.2039648 1.049178 -0.2293439 MPRFLG 0 ; 
       8 SCHEM 2082.014 31.68521 0 MPRFLG 0 ; 
       9 SCHEM 2089.514 31.68521 0 MPRFLG 0 ; 
       10 SCHEM 2085.764 31.68521 0 MPRFLG 0 ; 
       11 SCHEM 2089.514 29.68521 0 MPRFLG 0 ; 
       12 SCHEM 2088.264 27.68521 0 MPRFLG 0 ; 
       13 SCHEM 2090.764 27.68521 0 MPRFLG 0 ; 
       14 SCHEM 2082.014 29.68521 0 MPRFLG 0 ; 
       15 SCHEM 2083.264 27.68521 0 MPRFLG 0 ; 
       16 SCHEM 2080.764 27.68521 0 MPRFLG 0 ; 
       17 SCHEM 2085.764 33.68521 0 USR SRT 1.525208 1 1 0 0 0 0 0.09783381 0 MPRFLG 0 ; 
       22 SCHEM 2078.264 27.68521 0 MPRFLG 0 ; 
       23 SCHEM 2083.677 41.92725 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 2083.583 41.15337 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2086.125 42.00414 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 2086.216 41.22577 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2088.585 42.00795 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 2088.714 41.2925 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 2095.821 41.95034 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2091.122 41.35944 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 2093.418 42.03233 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 0 0 MPRFLG 0 ; 
       3 SCHEM 56.5 0 0 MPRFLG 0 ; 
       4 SCHEM 56.5 0 0 MPRFLG 0 ; 
       5 SCHEM 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 0 0 MPRFLG 0 ; 
       7 SCHEM 56.5 0 0 MPRFLG 0 ; 
       8 SCHEM 56.5 0 0 MPRFLG 0 ; 
       9 SCHEM 0 0 0 MPRFLG 0 ; 
       10 SCHEM 0 0 0 MPRFLG 0 ; 
       11 SCHEM 56.5 0 0 MPRFLG 0 ; 
       12 SCHEM 56.5 0 0 MPRFLG 0 ; 
       13 SCHEM 0 0 0 MPRFLG 0 ; 
       14 SCHEM 0 0 0 MPRFLG 0 ; 
       15 SCHEM 56.5 0 0 MPRFLG 0 ; 
       16 SCHEM 56.5 0 0 MPRFLG 0 ; 
       17 SCHEM 0 0 0 MPRFLG 0 ; 
       18 SCHEM 0 0 0 MPRFLG 0 ; 
       19 SCHEM 56.5 0 0 MPRFLG 0 ; 
       20 SCHEM 56.5 0 0 MPRFLG 0 ; 
       21 SCHEM 0 0 0 MPRFLG 0 ; 
       22 SCHEM 0 0 0 MPRFLG 0 ; 
       23 SCHEM 56.5 0 0 MPRFLG 0 ; 
       24 SCHEM 56.5 0 0 MPRFLG 0 ; 
       25 SCHEM 0 0 0 MPRFLG 0 ; 
       26 SCHEM 0 0 0 MPRFLG 0 ; 
       27 SCHEM 56.5 0 0 MPRFLG 0 ; 
       28 SCHEM 56.5 0 0 MPRFLG 0 ; 
       29 SCHEM 0 0 0 MPRFLG 0 ; 
       30 SCHEM 0 0 0 MPRFLG 0 ; 
       31 SCHEM 56.5 0 0 MPRFLG 0 ; 
       32 SCHEM 56.5 0 0 MPRFLG 0 ; 
       33 SCHEM 0 0 0 MPRFLG 0 ; 
       34 SCHEM 0 0 0 MPRFLG 0 ; 
       35 SCHEM 56.5 0 0 MPRFLG 0 ; 
       36 SCHEM 56.5 0 0 MPRFLG 0 ; 
       37 SCHEM 0 0 0 MPRFLG 0 ; 
       38 SCHEM 0 0 0 MPRFLG 0 ; 
       39 SCHEM 56.5 0 0 MPRFLG 0 ; 
       40 SCHEM 56.5 0 0 MPRFLG 0 ; 
       41 SCHEM 0 0 0 MPRFLG 0 ; 
       42 SCHEM 0 0 0 MPRFLG 0 ; 
       43 SCHEM 56.5 0 0 MPRFLG 0 ; 
       44 SCHEM 56.5 0 0 MPRFLG 0 ; 
       45 SCHEM 0 0 0 MPRFLG 0 ; 
       46 SCHEM 0 0 0 MPRFLG 0 ; 
       47 SCHEM 56.5 0 0 MPRFLG 0 ; 
       48 SCHEM 56.5 0 0 MPRFLG 0 ; 
       49 SCHEM 0 0 0 MPRFLG 0 ; 
       50 SCHEM 0 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 0 0 MPRFLG 0 ; 
       52 SCHEM 56.5 0 0 MPRFLG 0 ; 
       53 SCHEM 0 0 0 MPRFLG 0 ; 
       54 SCHEM 0 0 0 MPRFLG 0 ; 
       55 SCHEM 56.5 0 0 MPRFLG 0 ; 
       56 SCHEM 56.5 0 0 MPRFLG 0 ; 
       57 SCHEM 0 0 0 MPRFLG 0 ; 
       58 SCHEM 0 0 0 MPRFLG 0 ; 
       59 SCHEM 56.5 0 0 MPRFLG 0 ; 
       60 SCHEM 56.5 0 0 MPRFLG 0 ; 
       61 SCHEM 0 0 0 MPRFLG 0 ; 
       62 SCHEM 0 0 0 MPRFLG 0 ; 
       63 SCHEM 56.5 0 0 MPRFLG 0 ; 
       64 SCHEM 56.5 0 0 MPRFLG 0 ; 
       65 SCHEM 0 0 0 MPRFLG 0 ; 
       66 SCHEM 0 0 0 MPRFLG 0 ; 
       67 SCHEM 56.5 0 0 MPRFLG 0 ; 
       68 SCHEM 56.5 0 0 MPRFLG 0 ; 
       69 SCHEM 0 0 0 MPRFLG 0 ; 
       70 SCHEM 0 0 0 MPRFLG 0 ; 
       71 SCHEM 56.5 0 0 MPRFLG 0 ; 
       72 SCHEM 56.5 0 0 MPRFLG 0 ; 
       73 SCHEM 0 0 0 MPRFLG 0 ; 
       74 SCHEM 0 0 0 MPRFLG 0 ; 
       75 SCHEM 56.5 0 0 MPRFLG 0 ; 
       76 SCHEM 56.5 0 0 MPRFLG 0 ; 
       77 SCHEM 0 0 0 MPRFLG 0 ; 
       78 SCHEM 0 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 0 0 MPRFLG 0 ; 
       80 SCHEM 56.5 0 0 MPRFLG 0 ; 
       81 SCHEM 0 0 0 MPRFLG 0 ; 
       82 SCHEM 0 0 0 MPRFLG 0 ; 
       83 SCHEM 56.5 0 0 MPRFLG 0 ; 
       84 SCHEM 56.5 0 0 MPRFLG 0 ; 
       85 SCHEM 542.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 2069.126 -1.602436 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 87.35864 4.106678 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 557.4133 4.730927 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 2082.073 6.423546 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
