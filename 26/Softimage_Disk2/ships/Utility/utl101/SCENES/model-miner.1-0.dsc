SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.1-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 90     
       edit_nulls-mat70.1-0 ; 
       rev_miner-default1.1-0 ; 
       rev_miner-default1_1.1-0 ; 
       rev_miner-default1_1_1.1-0 ; 
       rev_miner-default1_3.1-0 ; 
       rev_miner-default10.1-0 ; 
       rev_miner-default10_1.1-0 ; 
       rev_miner-default10_1_1.1-0 ; 
       rev_miner-default10_3.1-0 ; 
       rev_miner-default11.1-0 ; 
       rev_miner-default11_1.1-0 ; 
       rev_miner-default11_1_1.1-0 ; 
       rev_miner-default11_3.1-0 ; 
       rev_miner-default12.1-0 ; 
       rev_miner-default12_1.1-0 ; 
       rev_miner-default12_1_1.1-0 ; 
       rev_miner-default12_3.1-0 ; 
       rev_miner-default13.1-0 ; 
       rev_miner-default13_1.1-0 ; 
       rev_miner-default13_1_1.1-0 ; 
       rev_miner-default13_3.1-0 ; 
       rev_miner-default14.1-0 ; 
       rev_miner-default14_1.1-0 ; 
       rev_miner-default14_1_1.1-0 ; 
       rev_miner-default14_3.1-0 ; 
       rev_miner-default15.1-0 ; 
       rev_miner-default15_1.1-0 ; 
       rev_miner-default15_1_1.1-0 ; 
       rev_miner-default15_3.1-0 ; 
       rev_miner-default16.1-0 ; 
       rev_miner-default16_1.1-0 ; 
       rev_miner-default16_1_1.1-0 ; 
       rev_miner-default16_3.1-0 ; 
       rev_miner-default17.1-0 ; 
       rev_miner-default17_1.1-0 ; 
       rev_miner-default17_1_1.1-0 ; 
       rev_miner-default17_3.1-0 ; 
       rev_miner-default18.1-0 ; 
       rev_miner-default18_1.1-0 ; 
       rev_miner-default18_1_1.1-0 ; 
       rev_miner-default18_3.1-0 ; 
       rev_miner-default19.1-0 ; 
       rev_miner-default19_1.1-0 ; 
       rev_miner-default19_1_1.1-0 ; 
       rev_miner-default19_3.1-0 ; 
       rev_miner-default2.1-0 ; 
       rev_miner-default2_1.1-0 ; 
       rev_miner-default2_1_1.1-0 ; 
       rev_miner-default2_3.1-0 ; 
       rev_miner-default20.1-0 ; 
       rev_miner-default20_1.1-0 ; 
       rev_miner-default20_1_1.1-0 ; 
       rev_miner-default20_3.1-0 ; 
       rev_miner-default21.1-0 ; 
       rev_miner-default21_1.1-0 ; 
       rev_miner-default21_1_1.1-0 ; 
       rev_miner-default21_3.1-0 ; 
       rev_miner-default3.1-0 ; 
       rev_miner-default3_1.1-0 ; 
       rev_miner-default3_1_1.1-0 ; 
       rev_miner-default3_3.1-0 ; 
       rev_miner-default4.1-0 ; 
       rev_miner-default4_1.1-0 ; 
       rev_miner-default4_1_1.1-0 ; 
       rev_miner-default4_3.1-0 ; 
       rev_miner-default5.1-0 ; 
       rev_miner-default5_1.1-0 ; 
       rev_miner-default5_1_1.1-0 ; 
       rev_miner-default5_3.1-0 ; 
       rev_miner-default6.1-0 ; 
       rev_miner-default6_1.1-0 ; 
       rev_miner-default6_1_1.1-0 ; 
       rev_miner-default6_3.1-0 ; 
       rev_miner-default7.1-0 ; 
       rev_miner-default7_1.1-0 ; 
       rev_miner-default7_1_1.1-0 ; 
       rev_miner-default7_3.1-0 ; 
       rev_miner-default8.1-0 ; 
       rev_miner-default8_1.1-0 ; 
       rev_miner-default8_1_1.1-0 ; 
       rev_miner-default8_3.1-0 ; 
       rev_miner-default9.1-0 ; 
       rev_miner-default9_1.1-0 ; 
       rev_miner-default9_1_1.1-0 ; 
       rev_miner-default9_3.1-0 ; 
       rev_miner-mat77.1-0 ; 
       rev_miner-mat80.1-0 ; 
       rev_miner-mat82.1-0 ; 
       rev_miner-mat84.1-0 ; 
       rev_miner-mat86.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       rev_miner-bsmoke.1-0 ; 
       rev_miner-bthrust.1-0 ; 
       rev_miner-cockpt.1-0 ; 
       rev_miner-cube1.2-0 ; 
       rev_miner-cube2.1-0 ROOT ; 
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl1.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl3.1-0 ; 
       rev_miner-cyl4.1-0 ; 
       rev_miner-cyl5.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-cyl7.1-0 ; 
       rev_miner-cyl8.1-0 ; 
       rev_miner-extru1.2-0 ; 
       rev_miner-extru1_1.4-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru10.2-0 ; 
       rev_miner-extru11.1-0 ROOT ; 
       rev_miner-extru12.1-0 ; 
       rev_miner-extru12_1.1-0 ROOT ; 
       rev_miner-extru14.1-0 ROOT ; 
       rev_miner-extru15.1-0 ROOT ; 
       rev_miner-extru16.1-0 ROOT ; 
       rev_miner-extru17.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru2.1-0 ROOT ; 
       rev_miner-extru4.1-0 ROOT ; 
       rev_miner-extru8.3-0 ; 
       rev_miner-extru9.2-0 ; 
       rev_miner-SS01.1-0 ; 
       rev_miner-SS02.1-0 ; 
       rev_miner-SS03.1-0 ; 
       rev_miner-SS04.1-0 ; 
       rev_miner-SS05.1-0 ; 
       rev_miner-SS06.1-0 ; 
       rev_miner-trail.1-0 ; 
       rev_miner-tsmoke.1-0 ; 
       rev_miner-tthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-miner.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 27 110 ; 
       1 27 110 ; 
       2 27 110 ; 
       3 27 110 ; 
       5 13 110 ; 
       6 7 110 ; 
       7 27 110 ; 
       8 27 110 ; 
       9 29 110 ; 
       10 29 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 11 110 ; 
       14 29 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 25 110 ; 
       19 15 110 ; 
       24 16 110 ; 
       25 28 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 27 110 ; 
       34 27 110 ; 
       35 27 110 ; 
       36 27 110 ; 
       37 27 110 ; 
       38 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       30 0 300 ; 
       31 87 300 ; 
       32 85 300 ; 
       33 88 300 ; 
       34 86 300 ; 
       35 89 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2060.853 21.62132 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2060.853 19.62132 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2091.216 42.00329 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2093.487 41.27283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2098.14 41.97251 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2078.878 34.83047 0 MPRFLG 0 ; 
       4 SCHEM 2068.623 16.14256 0 USR DISPLAY 0 0 SRT 0.1550399 0.1550399 0.15504 0 0 0 2.220583 -0.07394264 -6.230625 MPRFLG 0 ; 
       5 SCHEM 2070.106 29.38144 0 USR MPRFLG 0 ; 
       6 SCHEM 2067.628 32.83047 0 MPRFLG 0 ; 
       7 SCHEM 2070.128 34.83047 0 MPRFLG 0 ; 
       8 SCHEM 2075.128 34.83047 0 MPRFLG 0 ; 
       9 SCHEM 2082.628 32.83047 0 MPRFLG 0 ; 
       10 SCHEM 2085.128 32.83047 0 MPRFLG 0 ; 
       11 SCHEM 2070.128 32.83047 0 MPRFLG 0 ; 
       12 SCHEM 2072.628 32.83047 0 MPRFLG 0 ; 
       13 SCHEM 2070.128 30.83047 0 MPRFLG 0 ; 
       14 SCHEM 2087.628 32.83047 0 MPRFLG 0 ; 
       15 SCHEM 2077.628 32.83047 0 MPRFLG 0 ; 
       16 SCHEM 2080.128 32.83047 0 MPRFLG 0 ; 
       17 SCHEM 2090.128 32.83047 0 MPRFLG 0 ; 
       18 SCHEM 2083.314 22.76757 0 USR DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0.8312001 0.005184758 0.05184752 MPRFLG 0 ; 
       19 SCHEM 2077.351 31.42285 0 USR MPRFLG 0 ; 
       20 SCHEM 2075.068 21.90853 0 USR DISPLAY 0 0 SRT 0.03711954 0.4899998 0.4899998 0 0 0 0.777816 0.09399581 0.7058469 MPRFLG 0 ; 
       21 SCHEM 2075.665 16.65194 0 USR DISPLAY 0 0 SRT 1.149 1.03404 1.03404 0 0 0 0.1817833 0.06824299 0.06809049 MPRFLG 0 ; 
       22 SCHEM 2073.165 16.65194 0 USR DISPLAY 0 0 SRT 1.149 1.03404 1.03404 0 3.141593 0 -0.1817833 0.06824299 0.06809049 MPRFLG 0 ; 
       23 SCHEM 2086.097 22.88621 0 USR DISPLAY 0 0 SRT 1 1 1 0 2.384186e-007 0 -0.8312002 0.005184758 0.05184753 MPRFLG 0 ; 
       24 SCHEM 2080.067 31.42285 0 USR MPRFLG 0 ; 
       25 SCHEM 2091.151 34.24717 0 USR MPRFLG 0 ; 
       26 SCHEM 2074.746 15.7325 0 USR DISPLAY 0 0 SRT 1.149 1.03404 1.03404 0 0 0 0 -0.7158328 0.01629514 MPRFLG 0 ; 
       27 SCHEM 2080.128 36.83047 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 2090.795 35.56033 0 USR MPRFLG 0 ; 
       29 SCHEM 2085.128 34.83047 0 MPRFLG 0 ; 
       30 SCHEM 2083.677 41.92725 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 2083.583 41.15337 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 2086.125 42.00414 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 2086.216 41.22577 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 2088.585 42.00795 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 2088.714 41.2925 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 2095.821 41.95034 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 2091.122 41.35944 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 2093.418 42.03233 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
       2 SCHEM 0 0 0 MPRFLG 0 ; 
       3 SCHEM 56.5 0 0 MPRFLG 0 ; 
       4 SCHEM 56.5 0 0 MPRFLG 0 ; 
       5 SCHEM 0 0 0 MPRFLG 0 ; 
       6 SCHEM 0 0 0 MPRFLG 0 ; 
       7 SCHEM 56.5 0 0 MPRFLG 0 ; 
       8 SCHEM 56.5 0 0 MPRFLG 0 ; 
       9 SCHEM 0 0 0 MPRFLG 0 ; 
       10 SCHEM 0 0 0 MPRFLG 0 ; 
       11 SCHEM 56.5 0 0 MPRFLG 0 ; 
       12 SCHEM 56.5 0 0 MPRFLG 0 ; 
       13 SCHEM 0 0 0 MPRFLG 0 ; 
       14 SCHEM 0 0 0 MPRFLG 0 ; 
       15 SCHEM 56.5 0 0 MPRFLG 0 ; 
       16 SCHEM 56.5 0 0 MPRFLG 0 ; 
       17 SCHEM 0 0 0 MPRFLG 0 ; 
       18 SCHEM 0 0 0 MPRFLG 0 ; 
       19 SCHEM 56.5 0 0 MPRFLG 0 ; 
       20 SCHEM 56.5 0 0 MPRFLG 0 ; 
       21 SCHEM 0 0 0 MPRFLG 0 ; 
       22 SCHEM 0 0 0 MPRFLG 0 ; 
       23 SCHEM 56.5 0 0 MPRFLG 0 ; 
       24 SCHEM 56.5 0 0 MPRFLG 0 ; 
       25 SCHEM 0 0 0 MPRFLG 0 ; 
       26 SCHEM 0 0 0 MPRFLG 0 ; 
       27 SCHEM 56.5 0 0 MPRFLG 0 ; 
       28 SCHEM 56.5 0 0 MPRFLG 0 ; 
       29 SCHEM 0 0 0 MPRFLG 0 ; 
       30 SCHEM 0 0 0 MPRFLG 0 ; 
       31 SCHEM 56.5 0 0 MPRFLG 0 ; 
       32 SCHEM 56.5 0 0 MPRFLG 0 ; 
       33 SCHEM 0 0 0 MPRFLG 0 ; 
       34 SCHEM 0 0 0 MPRFLG 0 ; 
       35 SCHEM 56.5 0 0 MPRFLG 0 ; 
       36 SCHEM 56.5 0 0 MPRFLG 0 ; 
       37 SCHEM 0 0 0 MPRFLG 0 ; 
       38 SCHEM 0 0 0 MPRFLG 0 ; 
       39 SCHEM 56.5 0 0 MPRFLG 0 ; 
       40 SCHEM 56.5 0 0 MPRFLG 0 ; 
       41 SCHEM 0 0 0 MPRFLG 0 ; 
       42 SCHEM 0 0 0 MPRFLG 0 ; 
       43 SCHEM 56.5 0 0 MPRFLG 0 ; 
       44 SCHEM 56.5 0 0 MPRFLG 0 ; 
       45 SCHEM 0 0 0 MPRFLG 0 ; 
       46 SCHEM 0 0 0 MPRFLG 0 ; 
       47 SCHEM 56.5 0 0 MPRFLG 0 ; 
       48 SCHEM 56.5 0 0 MPRFLG 0 ; 
       49 SCHEM 0 0 0 MPRFLG 0 ; 
       50 SCHEM 0 0 0 MPRFLG 0 ; 
       51 SCHEM 56.5 0 0 MPRFLG 0 ; 
       52 SCHEM 56.5 0 0 MPRFLG 0 ; 
       53 SCHEM 0 0 0 MPRFLG 0 ; 
       54 SCHEM 0 0 0 MPRFLG 0 ; 
       55 SCHEM 56.5 0 0 MPRFLG 0 ; 
       56 SCHEM 56.5 0 0 MPRFLG 0 ; 
       57 SCHEM 0 0 0 MPRFLG 0 ; 
       58 SCHEM 0 0 0 MPRFLG 0 ; 
       59 SCHEM 56.5 0 0 MPRFLG 0 ; 
       60 SCHEM 56.5 0 0 MPRFLG 0 ; 
       61 SCHEM 0 0 0 MPRFLG 0 ; 
       62 SCHEM 0 0 0 MPRFLG 0 ; 
       63 SCHEM 56.5 0 0 MPRFLG 0 ; 
       64 SCHEM 56.5 0 0 MPRFLG 0 ; 
       65 SCHEM 0 0 0 MPRFLG 0 ; 
       66 SCHEM 0 0 0 MPRFLG 0 ; 
       67 SCHEM 56.5 0 0 MPRFLG 0 ; 
       68 SCHEM 56.5 0 0 MPRFLG 0 ; 
       69 SCHEM 0 0 0 MPRFLG 0 ; 
       70 SCHEM 0 0 0 MPRFLG 0 ; 
       71 SCHEM 56.5 0 0 MPRFLG 0 ; 
       72 SCHEM 56.5 0 0 MPRFLG 0 ; 
       73 SCHEM 0 0 0 MPRFLG 0 ; 
       74 SCHEM 0 0 0 MPRFLG 0 ; 
       75 SCHEM 56.5 0 0 MPRFLG 0 ; 
       76 SCHEM 56.5 0 0 MPRFLG 0 ; 
       77 SCHEM 0 0 0 MPRFLG 0 ; 
       78 SCHEM 0 0 0 MPRFLG 0 ; 
       79 SCHEM 56.5 0 0 MPRFLG 0 ; 
       80 SCHEM 56.5 0 0 MPRFLG 0 ; 
       81 SCHEM 0 0 0 MPRFLG 0 ; 
       82 SCHEM 0 0 0 MPRFLG 0 ; 
       83 SCHEM 56.5 0 0 MPRFLG 0 ; 
       84 SCHEM 56.5 0 0 MPRFLG 0 ; 
       85 SCHEM 542.0056 -3.298862 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 2069.126 -1.602436 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 87.35864 4.106678 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 557.4133 4.730927 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 2082.073 6.423546 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 238.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 176.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 177.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 224.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 223.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 326.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 184.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 226.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 259.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 210.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 249.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
