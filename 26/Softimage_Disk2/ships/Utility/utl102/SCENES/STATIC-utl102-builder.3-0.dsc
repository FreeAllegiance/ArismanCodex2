SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.14-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       Utl101_Miner-back1.1-0 ; 
       Utl101_Miner-back2.1-0 ; 
       Utl101_Miner-back5.1-0 ; 
       Utl101_Miner-bottom1.1-0 ; 
       Utl101_Miner-mat100.1-0 ; 
       Utl101_Miner-mat101.1-0 ; 
       Utl101_Miner-mat102.1-0 ; 
       Utl101_Miner-mat89.1-0 ; 
       Utl101_Miner-mat90.1-0 ; 
       Utl101_Miner-mat91.1-0 ; 
       Utl101_Miner-mat92.1-0 ; 
       Utl101_Miner-mat93.1-0 ; 
       Utl101_Miner-mat94.1-0 ; 
       Utl101_Miner-mat95.1-0 ; 
       Utl101_Miner-mat96.1-0 ; 
       Utl101_Miner-mat97.1-0 ; 
       Utl101_Miner-mat98.1-0 ; 
       Utl101_Miner-mat99.1-0 ; 
       Utl101_Miner-Side.1-0 ; 
       Utl101_Miner-top1.1-0 ; 
       Utl101_Miner-wing1.1-0 ; 
       Utl101_Miner-wing2.1-0 ; 
       utl102_builder-mat103.2-0 ; 
       utl102_builder-mat104.1-0 ; 
       utl102_builder-mat105.3-0 ; 
       utl102_builder-mat106.1-0 ; 
       utl102_builder-mat107.1-0 ; 
       utl102_builder-mat108.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl14.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru29.1-0 ; 
       rev_miner-extru29_2.1-0 ; 
       rev_miner-extru29_3.1-0 ; 
       rev_miner-extru30.1-0 ; 
       rev_miner-extru31.1-0 ; 
       rev_miner-extru32.1-0 ; 
       rev_miner-extru34.1-0 ; 
       rev_miner-extru4_1.10-0 ROOT ; 
       rev_miner-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl102/PICTURES/utl102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl102-builder.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       Utl101_Miner-back1.4-0 ; 
       Utl101_Miner-qs.3-0 ; 
       Utl101_Miner-side.4-0 ; 
       Utl101_Miner-t2d1.4-0 ; 
       Utl101_Miner-t2d14.4-0 ; 
       Utl101_Miner-t2d17.4-0 ; 
       Utl101_Miner-t2d18.4-0 ; 
       Utl101_Miner-t2d19.4-0 ; 
       Utl101_Miner-t2d2.4-0 ; 
       Utl101_Miner-t2d20.4-0 ; 
       Utl101_Miner-t2d21.4-0 ; 
       Utl101_Miner-t2d22.4-0 ; 
       Utl101_Miner-t2d23.4-0 ; 
       Utl101_Miner-t2d24.4-0 ; 
       Utl101_Miner-t2d25.4-0 ; 
       Utl101_Miner-t2d26.4-0 ; 
       Utl101_Miner-t2d27.4-0 ; 
       Utl101_Miner-t2d4.4-0 ; 
       Utl101_Miner-t2d5.4-0 ; 
       Utl101_Miner-top.4-0 ; 
       Utl101_Miner-top1.4-0 ; 
       Utl101_Miner-top2.4-0 ; 
       utl102_builder-t2d28.4-0 ; 
       utl102_builder-t2d29.4-0 ; 
       utl102_builder-t2d30.3-0 ; 
       utl102_builder-t2d31.3-0 ; 
       utl102_builder-t2d32.3-0 ; 
       utl102_builder-t2d33.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 3 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 7 110 ; 
       12 16 110 ; 
       13 6 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 11 110 ; 
       18 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 15 300 ; 
       2 16 300 ; 
       3 17 300 ; 
       4 5 300 ; 
       5 9 300 ; 
       5 1 300 ; 
       5 10 300 ; 
       6 11 300 ; 
       6 2 300 ; 
       6 12 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       8 20 300 ; 
       9 7 300 ; 
       10 8 300 ; 
       11 24 300 ; 
       12 25 300 ; 
       12 27 300 ; 
       13 21 300 ; 
       14 13 300 ; 
       15 14 300 ; 
       16 26 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 3 300 ; 
       17 0 300 ; 
       18 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 18 401 ; 
       2 6 401 ; 
       3 4 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 8 401 ; 
       8 17 401 ; 
       9 1 401 ; 
       10 20 401 ; 
       11 5 401 ; 
       12 21 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 2 401 ; 
       19 19 401 ; 
       20 3 401 ; 
       21 7 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 10 -6 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0.09783381 0 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
