SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.17-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       Utl101_Miner-back1_1.1-0 ; 
       Utl101_Miner-back2_1.1-0 ; 
       Utl101_Miner-back5_1.1-0 ; 
       Utl101_Miner-bottom1_1.1-0 ; 
       Utl101_Miner-mat100_1.1-0 ; 
       Utl101_Miner-mat101_1.1-0 ; 
       Utl101_Miner-mat102_1.1-0 ; 
       Utl101_Miner-mat89_1.1-0 ; 
       Utl101_Miner-mat90_1.1-0 ; 
       Utl101_Miner-mat91_1.1-0 ; 
       Utl101_Miner-mat92_1.1-0 ; 
       Utl101_Miner-mat93_1.1-0 ; 
       Utl101_Miner-mat94_1.1-0 ; 
       Utl101_Miner-mat95_1.1-0 ; 
       Utl101_Miner-mat96_1.1-0 ; 
       Utl101_Miner-mat97_1.1-0 ; 
       Utl101_Miner-mat98_1.1-0 ; 
       Utl101_Miner-mat99_1.1-0 ; 
       Utl101_Miner-Side_1.1-0 ; 
       Utl101_Miner-top1_1.1-0 ; 
       Utl101_Miner-wing1_1.1-0 ; 
       Utl101_Miner-wing2_1.1-0 ; 
       utl102_builder-mat103_1.1-0 ; 
       utl102_builder-mat104_1.1-0 ; 
       utl102_builder-mat105_1.1-0 ; 
       utl102_builder-mat106_1.1-0 ; 
       utl102_builder-mat107_1.1-0 ; 
       utl102_builder-mat108_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       rev_miner-cube7_1.1-0 ; 
       rev_miner-cyl13_1.1-0 ; 
       rev_miner-cyl14_1.1-0 ; 
       rev_miner-cyl2_1.1-0 ; 
       rev_miner-cyl6_1.1-0 ; 
       rev_miner-extru1_1_1.1-0 ; 
       rev_miner-extru1_2_1.1-0 ; 
       rev_miner-extru18_1.1-0 ; 
       rev_miner-extru26_1.1-0 ; 
       rev_miner-extru27_1.1-0 ; 
       rev_miner-extru29_2_1.1-0 ; 
       rev_miner-extru29_3_1.1-0 ; 
       rev_miner-extru29_5.1-0 ; 
       rev_miner-extru30_1.1-0 ; 
       rev_miner-extru31_1.1-0 ; 
       rev_miner-extru32_1.1-0 ; 
       rev_miner-extru34_1.1-0 ; 
       rev_miner-extru4_1.12-0 ROOT ; 
       rev_miner-sphere1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl102/PICTURES/utl102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl102-builder.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       Utl101_Miner-back1.6-0 ; 
       Utl101_Miner-qs_1.1-0 ; 
       Utl101_Miner-side.6-0 ; 
       Utl101_Miner-t2d1_1.1-0 ; 
       Utl101_Miner-t2d14.6-0 ; 
       Utl101_Miner-t2d17_1.1-0 ; 
       Utl101_Miner-t2d18_1.1-0 ; 
       Utl101_Miner-t2d19_1.1-0 ; 
       Utl101_Miner-t2d2_1.1-0 ; 
       Utl101_Miner-t2d20_1.1-0 ; 
       Utl101_Miner-t2d21_1.1-0 ; 
       Utl101_Miner-t2d22_1.1-0 ; 
       Utl101_Miner-t2d23_1.1-0 ; 
       Utl101_Miner-t2d24_1.1-0 ; 
       Utl101_Miner-t2d25_1.1-0 ; 
       Utl101_Miner-t2d26_1.1-0 ; 
       Utl101_Miner-t2d27_1.1-0 ; 
       Utl101_Miner-t2d4_1.1-0 ; 
       Utl101_Miner-t2d5_1.1-0 ; 
       Utl101_Miner-top.6-0 ; 
       Utl101_Miner-top1_1.1-0 ; 
       Utl101_Miner-top2_1.1-0 ; 
       utl102_builder-t2d28_1.1-0 ; 
       utl102_builder-t2d29_1.1-0 ; 
       utl102_builder-t2d30_1.1-0 ; 
       utl102_builder-t2d31_1.1-0 ; 
       utl102_builder-t2d32_1.1-0 ; 
       utl102_builder-t2d33_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 3 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       12 8 110 ; 
       10 7 110 ; 
       11 16 110 ; 
       13 6 110 ; 
       14 13 110 ; 
       15 13 110 ; 
       16 10 110 ; 
       18 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 15 300 ; 
       2 16 300 ; 
       3 17 300 ; 
       4 5 300 ; 
       5 9 300 ; 
       5 1 300 ; 
       5 10 300 ; 
       6 11 300 ; 
       6 2 300 ; 
       6 12 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       8 20 300 ; 
       9 7 300 ; 
       12 8 300 ; 
       10 24 300 ; 
       11 25 300 ; 
       11 27 300 ; 
       13 21 300 ; 
       14 13 300 ; 
       15 14 300 ; 
       16 26 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 3 300 ; 
       17 0 300 ; 
       18 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 18 401 ; 
       2 6 401 ; 
       3 4 401 ; 
       4 14 401 ; 
       5 15 401 ; 
       6 16 401 ; 
       7 8 401 ; 
       8 17 401 ; 
       9 1 401 ; 
       10 20 401 ; 
       11 5 401 ; 
       12 21 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 2 401 ; 
       19 19 401 ; 
       20 3 401 ; 
       21 7 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 3.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 8.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 16.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 16.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 22.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 12.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 2.8622 MPRFLG 0 ; 
       18 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
