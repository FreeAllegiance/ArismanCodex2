SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rev_miner-cam_int1.16-0 ROOT ; 
       rev_miner-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       edit_nulls-mat70.1-0 ; 
       rev_miner-mat77.1-0 ; 
       rev_miner-mat80.1-0 ; 
       rev_miner-mat82.1-0 ; 
       rev_miner-mat84.1-0 ; 
       rev_miner-mat86.1-0 ; 
       Utl101_Miner-back1.1-0 ; 
       Utl101_Miner-back2.1-0 ; 
       Utl101_Miner-back5.1-0 ; 
       Utl101_Miner-bottom1.1-0 ; 
       Utl101_Miner-mat100.1-0 ; 
       Utl101_Miner-mat101.1-0 ; 
       Utl101_Miner-mat102.1-0 ; 
       Utl101_Miner-mat89.1-0 ; 
       Utl101_Miner-mat90.1-0 ; 
       Utl101_Miner-mat91.1-0 ; 
       Utl101_Miner-mat92.1-0 ; 
       Utl101_Miner-mat93.1-0 ; 
       Utl101_Miner-mat94.1-0 ; 
       Utl101_Miner-mat95.1-0 ; 
       Utl101_Miner-mat96.1-0 ; 
       Utl101_Miner-mat97.1-0 ; 
       Utl101_Miner-mat98.1-0 ; 
       Utl101_Miner-mat99.1-0 ; 
       Utl101_Miner-Side.1-0 ; 
       Utl101_Miner-top1.1-0 ; 
       Utl101_Miner-wing1.1-0 ; 
       Utl101_Miner-wing2.1-0 ; 
       utl102_builder-mat103.2-0 ; 
       utl102_builder-mat104.1-0 ; 
       utl102_builder-mat105.3-0 ; 
       utl102_builder-mat106.1-0 ; 
       utl102_builder-mat107.1-0 ; 
       utl102_builder-mat108.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       rev_miner-cockpt.3-0 ; 
       rev_miner-cube7.1-0 ; 
       rev_miner-cyl13.1-0 ; 
       rev_miner-cyl14.1-0 ; 
       rev_miner-cyl2.1-0 ; 
       rev_miner-cyl6.1-0 ; 
       rev_miner-extru1_1.1-0 ; 
       rev_miner-extru1_2.1-0 ; 
       rev_miner-extru18.1-0 ; 
       rev_miner-extru26.1-0 ; 
       rev_miner-extru27.1-0 ; 
       rev_miner-extru29.1-0 ; 
       rev_miner-extru29_2.1-0 ; 
       rev_miner-extru29_3.1-0 ; 
       rev_miner-extru30.1-0 ; 
       rev_miner-extru31.1-0 ; 
       rev_miner-extru32.1-0 ; 
       rev_miner-extru34.1-0 ; 
       rev_miner-extru4_1.11-0 ROOT ; 
       rev_miner-lsmoke.3-0 ; 
       rev_miner-lthrust.3-0 ; 
       rev_miner-rsmoke.3-0 ; 
       rev_miner-rthrust.3-0 ; 
       rev_miner-sphere1.1-0 ; 
       rev_miner-SS01.3-0 ; 
       rev_miner-SS02.3-0 ; 
       rev_miner-SS03.3-0 ; 
       rev_miner-SS04.3-0 ; 
       rev_miner-SS05.3-0 ; 
       rev_miner-SS06.3-0 ; 
       rev_miner-trail.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl102/PICTURES/utl102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Shorten-utl102-builder.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       Utl101_Miner-back1.5-0 ; 
       Utl101_Miner-qs.3-0 ; 
       Utl101_Miner-side.5-0 ; 
       Utl101_Miner-t2d1.4-0 ; 
       Utl101_Miner-t2d14.5-0 ; 
       Utl101_Miner-t2d17.4-0 ; 
       Utl101_Miner-t2d18.4-0 ; 
       Utl101_Miner-t2d19.4-0 ; 
       Utl101_Miner-t2d2.4-0 ; 
       Utl101_Miner-t2d20.4-0 ; 
       Utl101_Miner-t2d21.4-0 ; 
       Utl101_Miner-t2d22.4-0 ; 
       Utl101_Miner-t2d23.4-0 ; 
       Utl101_Miner-t2d24.4-0 ; 
       Utl101_Miner-t2d25.4-0 ; 
       Utl101_Miner-t2d26.4-0 ; 
       Utl101_Miner-t2d27.4-0 ; 
       Utl101_Miner-t2d4.4-0 ; 
       Utl101_Miner-t2d5.4-0 ; 
       Utl101_Miner-top.5-0 ; 
       Utl101_Miner-top1.4-0 ; 
       Utl101_Miner-top2.4-0 ; 
       utl102_builder-t2d28.4-0 ; 
       utl102_builder-t2d29.4-0 ; 
       utl102_builder-t2d30.3-0 ; 
       utl102_builder-t2d31.3-0 ; 
       utl102_builder-t2d32.3-0 ; 
       utl102_builder-t2d33.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 5 110 ; 
       2 18 110 ; 
       3 18 110 ; 
       4 18 110 ; 
       5 4 110 ; 
       6 18 110 ; 
       7 18 110 ; 
       8 18 110 ; 
       9 6 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 8 110 ; 
       13 17 110 ; 
       14 7 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 12 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 5 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
       29 18 110 ; 
       30 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 10 300 ; 
       2 21 300 ; 
       3 22 300 ; 
       4 23 300 ; 
       5 11 300 ; 
       6 15 300 ; 
       6 7 300 ; 
       6 16 300 ; 
       7 17 300 ; 
       7 8 300 ; 
       7 18 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       9 26 300 ; 
       10 13 300 ; 
       11 14 300 ; 
       12 30 300 ; 
       13 31 300 ; 
       13 33 300 ; 
       14 27 300 ; 
       15 19 300 ; 
       16 20 300 ; 
       17 32 300 ; 
       18 24 300 ; 
       18 25 300 ; 
       18 9 300 ; 
       18 6 300 ; 
       23 12 300 ; 
       24 0 300 ; 
       25 3 300 ; 
       26 1 300 ; 
       27 4 300 ; 
       28 2 300 ; 
       29 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 18 401 ; 
       8 6 401 ; 
       9 4 401 ; 
       10 14 401 ; 
       11 15 401 ; 
       12 16 401 ; 
       13 8 401 ; 
       14 17 401 ; 
       15 1 401 ; 
       16 20 401 ; 
       17 5 401 ; 
       18 21 401 ; 
       19 9 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 2 401 ; 
       25 19 401 ; 
       26 3 401 ; 
       27 7 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 90 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 45 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 18.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 36.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 53.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 51.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 48.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 50 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 51.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 80 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 85 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 77.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 82.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 65 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 62.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 67.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 70 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 72.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 75 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 87.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 97.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 95 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 100 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 97.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 100 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 95 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 92.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
