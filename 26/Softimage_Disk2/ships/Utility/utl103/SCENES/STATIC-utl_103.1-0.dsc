SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.40-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       utl_103-mat81.1-0 ; 
       utl_103-mat81_1.1-0 ; 
       utl_103-mat82.1-0 ; 
       utl_103-mat82_1.1-0 ; 
       utl_103-mat83.2-0 ; 
       utl_103-mat84.1-0 ; 
       utl_103-mat87.2-0 ; 
       utl_103-mat88.8-0 ; 
       utl_103-mat88_1.1-0 ; 
       utl_103-mat89.6-0 ; 
       utl_103-mat90.5-0 ; 
       utl_103-mat91.4-0 ; 
       utl_103-mat92.3-0 ; 
       utl_103-mat93.2-0 ; 
       utl_103-mat94.1-0 ; 
       utl_103-mat95.1-0 ; 
       utl_103-mat96.1-0 ; 
       utl_103-mat97.1-0 ; 
       utl_103-mat98.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       root-canopy_1_1.32-0 ; 
       root-canopy_1_1_1.21-0 ; 
       root-cube1.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl3_1.1-0 ; 
       root-cyl4.1-0 ; 
       root-cyl5.1-0 ; 
       root-extru2_1.1-0 ; 
       root-null1_1.1-0 ; 
       root-root.2-0 ROOT ; 
       root-rt-wing1.1-0 ; 
       root-rt-wing3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl103/PICTURES/utl103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl_103.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       utl_103-t2d1.3-0 ; 
       utl_103-t2d1_1.3-0 ; 
       utl_103-t2d10.10-0 ; 
       utl_103-t2d11.8-0 ; 
       utl_103-t2d12.8-0 ; 
       utl_103-t2d12_1.3-0 ; 
       utl_103-t2d13.4-0 ; 
       utl_103-t2d14.3-0 ; 
       utl_103-t2d15.3-0 ; 
       utl_103-t2d16.3-0 ; 
       utl_103-t2d17.3-0 ; 
       utl_103-t2d2.3-0 ; 
       utl_103-t2d3.3-0 ; 
       utl_103-t2d4.3-0 ; 
       utl_103-t2d6.6-0 ; 
       utl_103-t2d7.6-0 ; 
       utl_103-t2d8.13-0 ; 
       utl_103-t2d9.12-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 9 110 ; 
       7 9 110 ; 
       8 1 110 ; 
       9 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       1 1 300 ; 
       1 3 300 ; 
       1 6 300 ; 
       2 14 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 15 300 ; 
       5 16 300 ; 
       6 17 300 ; 
       7 18 300 ; 
       8 7 300 ; 
       11 0 300 ; 
       12 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 15 401 ; 
       2 11 401 ; 
       3 1 401 ; 
       4 13 401 ; 
       5 12 401 ; 
       6 14 401 ; 
       7 4 401 ; 
       9 16 401 ; 
       10 17 401 ; 
       11 2 401 ; 
       12 3 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -2.328327 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 2.5 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 5 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -6.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -6.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 17.5 -6.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 20 -6.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 2.5 -6.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.25 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 22.5 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 25 -4.328327 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 13.07133 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -4.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -8.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -8.328327 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -6.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -6.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -10.32833 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -6.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -10.32833 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -10.32833 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -10.32833 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19 -10.32833 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -6.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -6.328327 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
