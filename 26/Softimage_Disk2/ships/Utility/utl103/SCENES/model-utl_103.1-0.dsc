SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.1-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       apc-mat100.1-0 ; 
       apc-mat101.1-0 ; 
       apc-mat102.1-0 ; 
       apc-mat103.1-0 ; 
       apc-mat104.1-0 ; 
       apc-mat105.1-0 ; 
       apc-mat106.1-0 ; 
       apc-mat71.1-0 ; 
       apc-mat75.1-0 ; 
       apc-mat77.1-0 ; 
       apc-mat78.1-0 ; 
       apc-mat80.1-0 ; 
       apc-mat83.1-0 ; 
       apc-mat94.1-0 ; 
       edit_nulls-mat70.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       TEST1-blthrust.4-0 ; 
       TEST1-brthrust.4-0 ; 
       TEST1-canopy_1_1.1-0 ROOT ; 
       TEST1-cockpt.4-0 ; 
       TEST1-cube1.1-0 ; 
       TEST1-cube3.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl2.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-extru1.1-0 ; 
       TEST1-extru2.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine6.1-0 ; 
       TEST1-lsmoke.4-0 ; 
       TEST1-lwepemt.4-0 ; 
       TEST1-missemt.4-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-rsmoke.4-0 ; 
       TEST1-rt-wing1.1-0 ; 
       TEST1-rt-wing2.1-0 ; 
       TEST1-rwepemt.4-0 ; 
       TEST1-SS01.4-0 ; 
       TEST1-SS02.4-0 ; 
       TEST1-SS03.4-0 ; 
       TEST1-SS04.4-0 ; 
       TEST1-SS05.4-0 ; 
       TEST1-SS06.4-0 ; 
       TEST1-tlthrust.4-0 ; 
       TEST1-trail.4-0 ; 
       TEST1-trthrust.4-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl103/PICTURES/fig16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl_103.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       apc-back1.1-0 ; 
       apc-Front1.1-0 ; 
       apc-Rear_front1.1-0 ; 
       apc-Side1.1-0 ; 
       apc-t2d1.1-0 ; 
       apc-t2d13.1-0 ; 
       apc-t2d14.1-0 ; 
       apc-t2d15.1-0 ; 
       apc-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       18 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       17 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       22 14 300 ; 
       23 7 300 ; 
       24 9 300 ; 
       25 8 300 ; 
       26 11 300 ; 
       27 10 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       8 0 300 ; 
       12 13 300 ; 
       13 6 300 ; 
       19 12 300 ; 
       20 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 6 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 7 401 ; 
       12 4 401 ; 
       13 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.4482 -3.895634 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.05643 -3.931713 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.85242 -3.153095 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13.51286 -2.272305 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5.274897 -3.246193 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.332802 -2.58839 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.04893 -2.236233 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.397532 -3.244808 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 18.44878 -2.253793 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 18.46606 -2.967022 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 20.70074 -2.232823 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 20.71803 -3.008983 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 22.99096 -2.190861 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 23.05759 -3.008983 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 13.43732 -3.174156 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14.76456 -1.190087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.05984 -3.102009 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.39872 2.734477 0 USR SRT 1 1 1 0 0 0 0 0.3104329 0.5966939 MPRFLG 0 ; 
       4 SCHEM 22.64872 0.7344769 0 MPRFLG 0 ; 
       5 SCHEM 25.14872 0.7344769 0 MPRFLG 0 ; 
       6 SCHEM 26.96979 -5.333393 0 MPRFLG 0 ; 
       7 SCHEM 29.46979 -5.333393 0 MPRFLG 0 ; 
       8 SCHEM 12.64872 0.7344769 0 MPRFLG 0 ; 
       9 SCHEM 20.14872 0.7344769 0 MPRFLG 0 ; 
       10 SCHEM 17.64872 0.7344769 0 MPRFLG 0 ; 
       11 SCHEM 7.64872 0.7344769 0 MPRFLG 0 ; 
       12 SCHEM 24.46979 -5.333393 0 MPRFLG 0 ; 
       13 SCHEM 21.96979 -5.333393 0 MPRFLG 0 ; 
       17 SCHEM 25.71979 -3.333393 0 USR MPRFLG 0 ; 
       19 SCHEM 15.14872 0.7344769 0 MPRFLG 0 ; 
       20 SCHEM 10.14872 0.7344769 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 87.95054 2.283315 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 113.1997 3.59587 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 114.6996 3.59587 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 116.1996 3.59587 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 117.6996 3.59587 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 121.6326 14.20495 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 84.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 89.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 91.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 96.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 94.47044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 69.47044 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 101.9704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 86.97044 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 112.1997 1.59587 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 113.6997 1.59587 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 115.1996 1.59587 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 116.6996 1.59587 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 69.47044 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 64.47044 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 87.95054 0.2833147 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 121.6326 12.20495 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 101.9704 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
