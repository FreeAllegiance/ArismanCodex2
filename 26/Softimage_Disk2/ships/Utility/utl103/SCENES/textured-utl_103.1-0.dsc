SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.39-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       apc-mat71.1-0 ; 
       apc-mat75.1-0 ; 
       apc-mat77.1-0 ; 
       apc-mat78.1-0 ; 
       apc-mat80.1-0 ; 
       edit_nulls-mat70.1-0 ; 
       utl_103-mat81.1-0 ; 
       utl_103-mat81_1.1-0 ; 
       utl_103-mat82.1-0 ; 
       utl_103-mat82_1.1-0 ; 
       utl_103-mat83.2-0 ; 
       utl_103-mat84.1-0 ; 
       utl_103-mat87.2-0 ; 
       utl_103-mat88.8-0 ; 
       utl_103-mat88_1.1-0 ; 
       utl_103-mat89.6-0 ; 
       utl_103-mat90.5-0 ; 
       utl_103-mat91.4-0 ; 
       utl_103-mat92.3-0 ; 
       utl_103-mat93.2-0 ; 
       utl_103-mat94.1-0 ; 
       utl_103-mat95.1-0 ; 
       utl_103-mat96.1-0 ; 
       utl_103-mat97.1-0 ; 
       utl_103-mat98.1-0 ; 
       utl_103-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       root-blthrust.4-0 ; 
       root-brthrust.4-0 ; 
       root-canopy_1_1.32-0 ; 
       root-canopy_1_1_1.21-0 ; 
       root-cockpt.4-0 ; 
       root-cube1.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl3_1.1-0 ; 
       root-cyl4.1-0 ; 
       root-cyl5.1-0 ; 
       root-engine-nib_1.1-0 ; 
       root-extru2_1.1-0 ; 
       root-lsmoke.4-0 ; 
       root-lwepemt.4-0 ; 
       root-missemt.4-0 ; 
       root-null1_1.1-0 ; 
       root-root.1-0 ROOT ; 
       root-rsmoke.4-0 ; 
       root-rt-wing1.1-0 ; 
       root-rt-wing3.1-0 ; 
       root-rwepemt.4-0 ; 
       root-SS01.4-0 ; 
       root-SS02.4-0 ; 
       root-SS03.4-0 ; 
       root-SS04.4-0 ; 
       root-SS05.4-0 ; 
       root-SS06.4-0 ; 
       root-tlthrust.4-0 ; 
       root-trail.4-0 ; 
       root-trthrust.4-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl103/PICTURES/utl103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       textured-utl_103.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       utl_103-t2d1.2-0 ; 
       utl_103-t2d1_1.2-0 ; 
       utl_103-t2d10.9-0 ; 
       utl_103-t2d11.7-0 ; 
       utl_103-t2d12.7-0 ; 
       utl_103-t2d12_1.2-0 ; 
       utl_103-t2d13.3-0 ; 
       utl_103-t2d14.2-0 ; 
       utl_103-t2d15.2-0 ; 
       utl_103-t2d16.2-0 ; 
       utl_103-t2d17.2-0 ; 
       utl_103-t2d18.2-0 ; 
       utl_103-t2d2.2-0 ; 
       utl_103-t2d3.2-0 ; 
       utl_103-t2d4.2-0 ; 
       utl_103-t2d6.5-0 ; 
       utl_103-t2d7.5-0 ; 
       utl_103-t2d8.12-0 ; 
       utl_103-t2d9.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 17 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 14 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 7 300 ; 
       3 9 300 ; 
       3 12 300 ; 
       5 20 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       7 21 300 ; 
       8 22 300 ; 
       9 23 300 ; 
       10 24 300 ; 
       11 25 300 ; 
       12 13 300 ; 
       19 6 300 ; 
       20 8 300 ; 
       22 5 300 ; 
       23 0 300 ; 
       24 2 300 ; 
       25 1 300 ; 
       26 4 300 ; 
       27 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 16 401 ; 
       8 12 401 ; 
       9 1 401 ; 
       10 14 401 ; 
       11 13 401 ; 
       12 15 401 ; 
       13 4 401 ; 
       15 17 401 ; 
       16 18 401 ; 
       17 2 401 ; 
       18 3 401 ; 
       19 5 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 50.11937 2.764889 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 55 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 58.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 65 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 67.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 70 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 72.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 62.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 68.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 27.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 77.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 30 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 32.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 37.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 40 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 42.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 12.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 47.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 50 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 52.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 49.4407 5.093216 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 80 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 85 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 87.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 90 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 85 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 87.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 90 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 80 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 82.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
