SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       scaled-cam_int1.1-0 ROOT ; 
       scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 26     
       scaled-mat100.1-0 ; 
       scaled-mat101.1-0 ; 
       scaled-mat102.1-0 ; 
       scaled-mat103.1-0 ; 
       scaled-mat104.1-0 ; 
       scaled-mat105.1-0 ; 
       scaled-mat106.1-0 ; 
       scaled-mat107.1-0 ; 
       scaled-mat108.1-0 ; 
       scaled-mat109.1-0 ; 
       scaled-mat110.1-0 ; 
       scaled-mat111.1-0 ; 
       scaled-mat81_1.1-0 ; 
       scaled-mat82_1.1-0 ; 
       scaled-mat88_1.1-0 ; 
       scaled-mat89.1-0 ; 
       scaled-mat90.1-0 ; 
       scaled-mat91.1-0 ; 
       scaled-mat92.1-0 ; 
       scaled-mat93.1-0 ; 
       scaled-mat94.1-0 ; 
       scaled-mat95.1-0 ; 
       scaled-mat96.1-0 ; 
       scaled-mat97.1-0 ; 
       scaled-mat98.1-0 ; 
       scaled-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       root-blthrust.4-0 ; 
       root-brthrust.4-0 ; 
       root-canopy_1_1.32-0 ; 
       root-canopy_1_1_1.21-0 ; 
       root-cockpt.4-0 ; 
       root-cube1.1-0 ; 
       root-cube3_1.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl3_1.1-0 ; 
       root-cyl4.1-0 ; 
       root-cyl5.1-0 ; 
       root-engine-nib_1.1-0 ; 
       root-extru2_1.1-0 ; 
       root-lsmoke.4-0 ; 
       root-lwepemt.4-0 ; 
       root-missemt.4-0 ; 
       root-null1_1.1-0 ; 
       root-root.3-0 ROOT ; 
       root-rsmoke.4-0 ; 
       root-rt-wing1.1-0 ; 
       root-rt-wing3.1-0 ; 
       root-rwepemt.4-0 ; 
       root-SS01.4-0 ; 
       root-SS02.4-0 ; 
       root-SS03.4-0 ; 
       root-SS04.4-0 ; 
       root-SS05.4-0 ; 
       root-SS06.4-0 ; 
       root-tlthrust.4-0 ; 
       root-trail.4-0 ; 
       root-trthrust.4-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl103/PICTURES/utl103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl103b-scaled.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 19     
       scaled-t2d1.1-0 ; 
       scaled-t2d1_1.1-0 ; 
       scaled-t2d12_1.1-0 ; 
       scaled-t2d13.1-0 ; 
       scaled-t2d14.1-0 ; 
       scaled-t2d15.1-0 ; 
       scaled-t2d16.1-0 ; 
       scaled-t2d17.1-0 ; 
       scaled-t2d18.1-0 ; 
       scaled-t2d19.1-0 ; 
       scaled-t2d2.1-0 ; 
       scaled-t2d20.1-0 ; 
       scaled-t2d21.1-0 ; 
       scaled-t2d22.1-0 ; 
       scaled-t2d23.1-0 ; 
       scaled-t2d24.1-0 ; 
       scaled-t2d25.1-0 ; 
       scaled-t2d3.1-0 ; 
       scaled-t2d4.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 17 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 2 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 14 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 10 300 ; 
       5 22 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       7 25 300 ; 
       8 0 300 ; 
       9 2 300 ; 
       10 1 300 ; 
       11 21 300 ; 
       12 11 300 ; 
       19 20 300 ; 
       20 9 300 ; 
       22 8 300 ; 
       23 7 300 ; 
       24 6 300 ; 
       25 5 300 ; 
       26 4 300 ; 
       27 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       15 0 401 ; 
       16 10 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 5 401 ; 
       23 6 401 ; 
       24 7 401 ; 
       25 8 401 ; 
       0 9 401 ; 
       1 11 401 ; 
       2 12 401 ; 
       9 13 401 ; 
       12 14 401 ; 
       13 1 401 ; 
       10 15 401 ; 
       11 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 114.6286 -21.84722 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 51.82224 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 79.32223 -9.272365 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 46.82224 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 54.32223 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 129.2515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 131.7515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 136.7515 -13.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 139.2515 -13.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 141.7515 -13.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 144.2515 -13.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 134.2515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 46.82224 -13.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 116.9846 -21.9027 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 56.82224 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 59.32224 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 140.5015 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 29.20285 -12.03726 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 61.82223 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 146.7515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 149.2515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 64.32223 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 66.82223 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 69.32223 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 117.1286 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 119.4846 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 124.2515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 49.32224 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 119.6025 -22.00637 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 126.7515 -11.27237 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 121.7515 -21.95454 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       14 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 270.0486 0.764889 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 265.0486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 252.5486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 247.5486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 250.0486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 250.0486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 255.0486 -3.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 257.5486 -3.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 262.5486 -3.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 260.0486 -3.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 167.6194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 242.5486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 237.7817 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 235.4258 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 187.6194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 185.1194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 267.5486 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 167.6194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 167.6194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 167.6194 -1.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 165.1194 -3.235111 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 270.0486 -1.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 270.0486 -1.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 270.0486 -1.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 270.0486 -1.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 270.0486 -1.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 265.0486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 252.5486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 247.5486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 250.0486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 250.0486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 255.0486 -5.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 257.5486 -5.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 262.5486 -5.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 260.0486 -5.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 267.5486 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 167.6194 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 167.6194 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 167.6194 -3.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 165.1194 -5.235111 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
