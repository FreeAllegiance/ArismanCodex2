SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.1-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       ord-extru1.1-0 ROOT ; 
       ord-extru10.1-0 ; 
       ord-extru11.1-0 ; 
       ord-extru12.1-0 ; 
       ord-extru13.1-0 ; 
       ord-extru14.1-0 ; 
       ord-extru15.1-0 ; 
       ord-extru2.1-0 ; 
       ord-extru3.1-0 ; 
       ord-null1.1-0 ; 
       ord-null10.1-0 ROOT ; 
       ord-null6.1-0 ; 
       ord-null7.1-0 ; 
       ord-null8.1-0 ; 
       ord-null9.1-0 ; 
       ord-revol2.1-0 ; 
       ord-revol3.1-0 ROOT ; 
       ord-revol4.1-0 ROOT ; 
       ord-spline2.1-0 ROOT ; 
       ord-spline3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-ord.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 10 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       9 11 110 ; 
       11 15 110 ; 
       12 11 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       13 11 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       14 11 110 ; 
       5 14 110 ; 
       6 14 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 10.95009 -7.805401 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 25.74902 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 22.81487 -4.947511 0 USR DISPLAY 0 0 SRT 0.692 0.692 0.258808 0 -0.512 0 1.129042 -0.03522289 -6.690796 MPRFLG 0 ; 
       7 SCHEM 21.19651 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 23.69652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 22.44652 -11.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 36.19651 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 24.94652 -9.5826 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 32.44651 -11.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 31.19652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 33.69651 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 17.44652 -11.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 16.19652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 18.69652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 27.44652 -11.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 26.19652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 28.69652 -13.5826 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
