SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20_biofighter-cam_int1.1-0 ROOT ; 
       fig20_biofighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig20_biofighter-mat81.1-0 ; 
       fig20_biofighter-mat88.1-0 ; 
       fig20_biofighter-mat89.1-0 ; 
       fig20_biofighter-mat91.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig20_biofighter-canopy_1.1-0 ROOT ; 
       fig20_biofighter-cockpt.1-0 ; 
       fig20_biofighter-lthrust.1-0 ; 
       fig20_biofighter-rthrust.1-0 ; 
       fig20_biofighter-SS01.1-0 ; 
       fig20_biofighter-SS02.1-0 ; 
       fig20_biofighter-SS05.1-0 ; 
       fig20_biofighter-SS06.1-0 ; 
       fig20_biofighter-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl104-biopod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       fig20_biofighter-back.1-0 ; 
       fig20_biofighter-Front.1-0 ; 
       fig20_biofighter-Rear_front.1-0 ; 
       fig20_biofighter-Side.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       3 0 110 ; 
       1 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       4 0 300 ; 
       5 1 300 ; 
       6 3 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 23.26703 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.76703 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.26703 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.76703 -2.078535 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 23.32726 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24.82726 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 26.32726 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.82726 -3.842931 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
