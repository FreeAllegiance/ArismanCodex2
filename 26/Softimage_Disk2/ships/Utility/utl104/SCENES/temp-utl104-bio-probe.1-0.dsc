SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       utl104_bio_probe-mat101.2-0 ; 
       utl104_bio_probe-mat102.2-0 ; 
       utl104_bio_probe-mat103.2-0 ; 
       utl104_bio_probe-mat104.2-0 ; 
       utl104_bio_probe-mat105.2-0 ; 
       utl104_bio_probe-mat106.2-0 ; 
       utl104_bio_probe-mat107.2-0 ; 
       utl104_bio_probe-mat108.2-0 ; 
       utl104_bio_probe-mat109.2-0 ; 
       utl104_bio_probe-mat110.2-0 ; 
       utl104_bio_probe-mat152.1-0 ; 
       utl104_bio_probe-mat153.1-0 ; 
       utl104_bio_probe-mat154.1-0 ; 
       utl104_bio_probe-mat155.1-0 ; 
       utl104_bio_probe-mat156.1-0 ; 
       utl104_bio_probe-mat157.1-0 ; 
       utl104_bio_probe-mat158.1-0 ; 
       utl104_bio_probe-mat159.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       fig20_biofighter1-canopy_1.2-0 ROOT ; 
       utl104_bio_probe-bottom_gun1.1-0 ; 
       utl104_bio_probe-center-wing1.2-0 ROOT ; 
       utl104_bio_probe-extru56.1-0 ; 
       utl104_bio_probe-rt-wing10.1-0 ROOT ; 
       utl104_bio_probe-rt-wing11.1-0 ROOT ; 
       utl104_bio_probe-rt-wing6.2-0 ROOT ; 
       utl104_bio_probe-rt-wing7.2-0 ROOT ; 
       utl104_bio_probe-rt-wing8.2-0 ROOT ; 
       utl104_bio_probe-rt-wing9.1-0 ROOT ; 
       utl104_bio_probe-sphere17.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/cap101 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       temp-utl104-bio-probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       utl104_bio_probe-back1.2-0 ; 
       utl104_bio_probe-Front1.2-0 ; 
       utl104_bio_probe-Rear_front1.2-0 ; 
       utl104_bio_probe-Side1.2-0 ; 
       utl104_bio_probe-t2d14.2-0 ; 
       utl104_bio_probe-t2d15.2-0 ; 
       utl104_bio_probe-t2d16.2-0 ; 
       utl104_bio_probe-t2d17.2-0 ; 
       utl104_bio_probe-t2d18.2-0 ; 
       utl104_bio_probe-t2d19.2-0 ; 
       utl104_bio_probe-t2d64.1-0 ; 
       utl104_bio_probe-t2d65.1-0 ; 
       utl104_bio_probe-t2d66.1-0 ; 
       utl104_bio_probe-t2d67.1-0 ; 
       utl104_bio_probe-t2d68.1-0 ; 
       utl104_bio_probe-t2d69.1-0 ; 
       utl104_bio_probe-t2d70.1-0 ; 
       utl104_bio_probe-t2d71.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 10 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 5 300 ; 
       2 6 300 ; 
       7 8 300 ; 
       8 9 300 ; 
       9 15 300 ; 
       4 16 300 ; 
       5 17 300 ; 
       6 7 300 ; 
       10 10 300 ; 
       10 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 8 401 ; 
       9 9 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 -0.02 1.744019e-009 3.141593 0.0164412 0.5009421 3.420575 MPRFLG 0 ; 
       7 SCHEM 10.69331 6.586405 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 -3.959524e-008 0.4956735 1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       8 SCHEM 1.695985 -7.741914 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.12361 0.3587686 0.9336911 MPRFLG 0 ; 
       9 SCHEM 12.93574 6.701956 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       4 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1.144 1 -0.7921187 -0.367415 -0.3405962 0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       5 SCHEM 22.5 6.701956 0 DISPLAY 0 0 SRT 1 1.144 0.9999999 0.7921185 -2.774178 0.3405962 -0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       6 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 1.213072e-007 -3.959524e-008 -0.4956735 0.9297128 0.2199657 1.023064 MPRFLG 0 ; 
       10 SCHEM 15 0 0 DISPLAY 0 0 SRT 2.454875 2.454875 2.454875 0 0 0 0 -1.207581 0.7964329 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 4.701956 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 35.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 38.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 2.701956 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 34.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 38.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
