SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       biodestroyer_cap100-mat136.1-0 ; 
       biodestroyer_cap100-mat137.1-0 ; 
       biodestroyer_cap100-mat138.1-0 ; 
       biodestroyer_cap100-mat139.1-0 ; 
       biodestroyer_cap100-mat141.1-0 ; 
       biodestroyer_cap100-mat142.1-0 ; 
       biodestroyer_cap100-mat143.1-0 ; 
       biodestroyer_cap100-mat144.1-0 ; 
       biodestroyer_cap100-mat145.1-0 ; 
       biodestroyer_cap100-mat146.1-0 ; 
       biodestroyer_cap100-mat147.1-0 ; 
       biodestroyer_cap100-mat148.1-0 ; 
       utl104_bio_probe-mat101.3-0 ; 
       utl104_bio_probe-mat102.3-0 ; 
       utl104_bio_probe-mat103.3-0 ; 
       utl104_bio_probe-mat104.3-0 ; 
       utl104_bio_probe-mat105.3-0 ; 
       utl104_bio_probe-mat106.3-0 ; 
       utl104_bio_probe-mat107.3-0 ; 
       utl104_bio_probe-mat108.3-0 ; 
       utl104_bio_probe-mat109.3-0 ; 
       utl104_bio_probe-mat110.3-0 ; 
       utl104_bio_probe-mat152.2-0 ; 
       utl104_bio_probe-mat153.2-0 ; 
       utl104_bio_probe-mat154.2-0 ; 
       utl104_bio_probe-mat155.2-0 ; 
       utl104_bio_probe-mat156.2-0 ; 
       utl104_bio_probe-mat157.2-0 ; 
       utl104_bio_probe-mat158.2-0 ; 
       utl104_bio_probe-mat159.2-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl104_bio_probe-mat163.1-0 ; 
       utl104_bio_probe-mat164.1-0 ; 
       utl104_bio_probe-mat165.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       destroyer-extru42.1-0 ROOT ; 
       destroyer-extru43.1-0 ROOT ; 
       destroyer-extru44.1-0 ROOT ; 
       destroyer-octa12.1-0 ; 
       destroyer-octa13.1-0 ; 
       destroyer1-cube1.1-0 ROOT ; 
       fig20_biofighter1-canopy_1.3-0 ROOT ; 
       utl104_bio_probe-bottom_gun1.1-0 ; 
       utl104_bio_probe-center-wing1.3-0 ROOT ; 
       utl104_bio_probe-extru56.1-0 ; 
       utl104_bio_probe-extru57.1-0 ROOT ; 
       utl104_bio_probe-rt-wing10.2-0 ROOT ; 
       utl104_bio_probe-rt-wing11.2-0 ROOT ; 
       utl104_bio_probe-rt-wing6.3-0 ROOT ; 
       utl104_bio_probe-rt-wing7.3-0 ROOT ; 
       utl104_bio_probe-rt-wing8.3-0 ROOT ; 
       utl104_bio_probe-rt-wing9.2-0 ROOT ; 
       utl104_bio_probe-sphere17.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/cap100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/cap101 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       temp-utl104-bio-probe.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       biodestroyer_cap100-t2d52.1-0 ; 
       biodestroyer_cap100-t2d53.1-0 ; 
       biodestroyer_cap100-t2d54.1-0 ; 
       biodestroyer_cap100-t2d55.1-0 ; 
       biodestroyer_cap100-t2d57.1-0 ; 
       biodestroyer_cap100-t2d58.1-0 ; 
       biodestroyer_cap100-t2d59.1-0 ; 
       biodestroyer_cap100-t2d60.1-0 ; 
       biodestroyer_cap100-t2d61.1-0 ; 
       biodestroyer_cap100-t2d62.1-0 ; 
       biodestroyer_cap100-t2d63.1-0 ; 
       utl104_bio_probe-back1.3-0 ; 
       utl104_bio_probe-Front1.3-0 ; 
       utl104_bio_probe-Rear_front1.3-0 ; 
       utl104_bio_probe-Side1.3-0 ; 
       utl104_bio_probe-t2d14.3-0 ; 
       utl104_bio_probe-t2d15.3-0 ; 
       utl104_bio_probe-t2d16.3-0 ; 
       utl104_bio_probe-t2d17.3-0 ; 
       utl104_bio_probe-t2d18.3-0 ; 
       utl104_bio_probe-t2d19.3-0 ; 
       utl104_bio_probe-t2d64.2-0 ; 
       utl104_bio_probe-t2d65.2-0 ; 
       utl104_bio_probe-t2d66.2-0 ; 
       utl104_bio_probe-t2d67.2-0 ; 
       utl104_bio_probe-t2d68.2-0 ; 
       utl104_bio_probe-t2d69.2-0 ; 
       utl104_bio_probe-t2d70.2-0 ; 
       utl104_bio_probe-t2d71.2-0 ; 
       utl104_bio_probe-t2d72.1-0 ; 
       utl104_bio_probe-t2d73.1-0 ; 
       utl104_bio_probe-t2d74.1-0 ; 
       utl104_bio_probe-t2d75.1-0 ; 
       utl104_bio_probe-t2d76.1-0 ; 
       utl104_bio_probe-t2d77.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       9 17 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 17 300 ; 
       8 18 300 ; 
       14 20 300 ; 
       15 21 300 ; 
       16 27 300 ; 
       11 28 300 ; 
       12 29 300 ; 
       10 33 300 ; 
       10 34 300 ; 
       10 35 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       13 19 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       17 22 300 ; 
       17 23 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       5 30 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       7 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 19 401 ; 
       21 20 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       33 32 401 ; 
       34 33 401 ; 
       35 34 401 ; 
       0 0 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 -0.02 1.744019e-009 3.141593 0.0164412 0.5009421 3.420575 MPRFLG 0 ; 
       14 SCHEM 10.69331 6.586405 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 -3.959524e-008 0.4956735 1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       15 SCHEM 1.695985 -7.741914 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.12361 0.3587686 0.9336911 MPRFLG 0 ; 
       16 SCHEM 12.93574 6.701956 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       11 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1.144 1 -0.7921187 -0.367415 -0.3405962 0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       12 SCHEM 22.5 6.701956 0 DISPLAY 0 0 SRT 1 1.144 0.9999999 0.7921185 -2.774178 0.3405962 -0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       10 SCHEM 16.21442 -24.68284 0 USR SRT 1 0.4779999 1 0 3.141593 0 -0.3758436 6.47406e-009 1.19971 MPRFLG 0 ; 
       0 SCHEM 19.58354 -27.3221 0 USR SRT 1 0.4779999 1 0 1.509958e-007 0 0.3758436 6.47406e-009 1.19971 MPRFLG 0 ; 
       1 SCHEM 10.18932 -26.6011 0 USR DISPLAY 0 0 SRT 1.528 1.193447 1.528 3.141593 0 0 6.135582e-009 -0.1541755 1.430882 MPRFLG 0 ; 
       2 SCHEM 5.189316 -28.6011 0 USR SRT 1.42 1.600829 1.42 3.511592 0 0 1.650567e-008 -0.2093544 1.329646 MPRFLG 0 ; 
       13 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 1.213072e-007 -3.959524e-008 -0.4956735 0.9297128 0.2199657 1.023064 MPRFLG 0 ; 
       3 SCHEM 13.33354 -29.3221 0 USR MPRFLG 0 ; 
       4 SCHEM 18.33354 -29.3221 0 USR MPRFLG 0 ; 
       17 SCHEM 15 0 0 DISPLAY 0 0 SRT 2.454875 2.454875 2.454875 0 0 0 0 -1.207581 0.7964329 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       5 SCHEM 13.71443 -24.68284 0 USR SRT 1 0.4779999 1 0 0 0 0 0 0.9333884 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       20 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 4.701956 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 185.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 188.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 190.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 167 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 35.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 38.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 169.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 172 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 159.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 164.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 134.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 139.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 142 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 129.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 132 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 250.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 253.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 255.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       19 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 2.701956 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 185.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 188.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 190.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 37.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 38.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 167 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 169.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 172 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 159.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 164.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 137 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 139.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 142 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 127 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 129.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 250.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 253.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 255.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
