SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       biodestroyer_cap100-mat136.1-0 ; 
       biodestroyer_cap100-mat137.1-0 ; 
       biodestroyer_cap100-mat138.1-0 ; 
       biodestroyer_cap100-mat139.1-0 ; 
       biodestroyer_cap100-mat141.1-0 ; 
       biodestroyer_cap100-mat142.1-0 ; 
       biodestroyer_cap100-mat143.1-0 ; 
       biodestroyer_cap100-mat144.1-0 ; 
       biodestroyer_cap100-mat145.1-0 ; 
       biodestroyer_cap100-mat146.1-0 ; 
       biodestroyer_cap100-mat147.1-0 ; 
       biodestroyer_cap100-mat148.1-0 ; 
       utl104_bio_probe-mat101.3-0 ; 
       utl104_bio_probe-mat102.3-0 ; 
       utl104_bio_probe-mat103.3-0 ; 
       utl104_bio_probe-mat104.3-0 ; 
       utl104_bio_probe-mat105.3-0 ; 
       utl104_bio_probe-mat106.3-0 ; 
       utl104_bio_probe-mat107.3-0 ; 
       utl104_bio_probe-mat108.3-0 ; 
       utl104_bio_probe-mat109.3-0 ; 
       utl104_bio_probe-mat110.3-0 ; 
       utl104_bio_probe-mat152.2-0 ; 
       utl104_bio_probe-mat153.2-0 ; 
       utl104_bio_probe-mat154.2-0 ; 
       utl104_bio_probe-mat155.2-0 ; 
       utl104_bio_probe-mat156.2-0 ; 
       utl104_bio_probe-mat157.2-0 ; 
       utl104_bio_probe-mat158.2-0 ; 
       utl104_bio_probe-mat159.2-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl104_bio_probe-mat163.1-0 ; 
       utl104_bio_probe-mat164.1-0 ; 
       utl104_bio_probe-mat165.1-0 ; 
       utl104_bio_probe-mat166.1-0 ; 
       utl104_bio_probe-mat167.1-0 ; 
       utl104_bio_probe-mat168.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       destroyer-extru43.1-0 ROOT ; 
       destroyer1-bottom_gun1.1-0 ; 
       destroyer1-center-wing1.1-0 ROOT ; 
       destroyer1-cube1.2-0 ROOT ; 
       destroyer1-extru42.1-0 ; 
       destroyer1-extru44.1-0 ; 
       destroyer1-extru57.1-0 ROOT ; 
       destroyer1-extru58.1-0 ; 
       destroyer1-octa12.1-0 ROOT ; 
       destroyer1-octa13.1-0 ROOT ; 
       fig20_biofighter1-canopy_1.3-0 ROOT ; 
       utl104_bio_probe-extru56.1-0 ; 
       utl104_bio_probe-rt-wing10.2-0 ROOT ; 
       utl104_bio_probe-rt-wing11.2-0 ROOT ; 
       utl104_bio_probe-rt-wing6.3-0 ROOT ; 
       utl104_bio_probe-rt-wing7.3-0 ROOT ; 
       utl104_bio_probe-rt-wing8.3-0 ROOT ; 
       utl104_bio_probe-rt-wing9.2-0 ROOT ; 
       utl104_bio_probe-sphere17.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/cap100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/cap101 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       temp-utl104-bio-probe.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       biodestroyer_cap100-t2d52.1-0 ; 
       biodestroyer_cap100-t2d53.1-0 ; 
       biodestroyer_cap100-t2d54.1-0 ; 
       biodestroyer_cap100-t2d55.1-0 ; 
       biodestroyer_cap100-t2d57.1-0 ; 
       biodestroyer_cap100-t2d58.1-0 ; 
       biodestroyer_cap100-t2d59.1-0 ; 
       biodestroyer_cap100-t2d60.1-0 ; 
       biodestroyer_cap100-t2d61.1-0 ; 
       biodestroyer_cap100-t2d62.1-0 ; 
       biodestroyer_cap100-t2d63.1-0 ; 
       utl104_bio_probe-back1.3-0 ; 
       utl104_bio_probe-Front1.3-0 ; 
       utl104_bio_probe-Rear_front1.3-0 ; 
       utl104_bio_probe-Side1.3-0 ; 
       utl104_bio_probe-t2d14.3-0 ; 
       utl104_bio_probe-t2d15.3-0 ; 
       utl104_bio_probe-t2d16.3-0 ; 
       utl104_bio_probe-t2d17.3-0 ; 
       utl104_bio_probe-t2d18.3-0 ; 
       utl104_bio_probe-t2d19.3-0 ; 
       utl104_bio_probe-t2d64.2-0 ; 
       utl104_bio_probe-t2d65.2-0 ; 
       utl104_bio_probe-t2d66.2-0 ; 
       utl104_bio_probe-t2d67.2-0 ; 
       utl104_bio_probe-t2d68.2-0 ; 
       utl104_bio_probe-t2d69.2-0 ; 
       utl104_bio_probe-t2d70.2-0 ; 
       utl104_bio_probe-t2d71.2-0 ; 
       utl104_bio_probe-t2d72.1-0 ; 
       utl104_bio_probe-t2d73.1-0 ; 
       utl104_bio_probe-t2d74.1-0 ; 
       utl104_bio_probe-t2d75.1-0 ; 
       utl104_bio_probe-t2d76.1-0 ; 
       utl104_bio_probe-t2d77.1-0 ; 
       utl104_bio_probe-t2d78.1-0 ; 
       utl104_bio_probe-t2d79.1-0 ; 
       utl104_bio_probe-t2d80.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       11 18 110 ; 
       1 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 17 300 ; 
       2 18 300 ; 
       15 20 300 ; 
       16 21 300 ; 
       17 27 300 ; 
       12 28 300 ; 
       13 29 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 38 300 ; 
       4 0 300 ; 
       4 1 300 ; 
       4 2 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       14 19 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       11 24 300 ; 
       11 25 300 ; 
       11 26 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       1 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       20 19 401 ; 
       21 20 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       33 32 401 ; 
       34 33 401 ; 
       35 34 401 ; 
       36 35 401 ; 
       37 36 401 ; 
       38 37 401 ; 
       0 0 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0.2790482 10.97195 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0.2790482 8.971948 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 1.266173 -7.259898 0 USR DISPLAY 0 0 SRT 1 0.9999999 1 -0.02 1.744019e-009 3.141593 0.0164412 0.500942 3.420575 MPRFLG 0 ; 
       15 SCHEM 14.97313 7.01576 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 -3.959524e-008 0.4956735 1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       16 SCHEM 0.4074787 -0.3030105 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.12361 0.3587686 0.9336911 MPRFLG 0 ; 
       17 SCHEM 17.21556 7.131311 0 USR DISPLAY 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.195663 0.3884434 0.8813081 MPRFLG 0 ; 
       12 SCHEM 24.27983 0.4293551 0 USR DISPLAY 0 0 SRT 1 1.144 1 -0.7921187 -0.367415 -0.3405962 0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       13 SCHEM 26.77983 7.131311 0 USR DISPLAY 0 0 SRT 1 1.144 0.9999999 0.7921185 -2.774178 0.3405962 -0.8547294 0.3083091 -0.03386676 MPRFLG 0 ; 
       6 SCHEM 16.26617 -7.259898 0 USR DISPLAY 0 0 SRT 1 0.4779999 1 0 3.141593 0 -0.3758436 6.47406e-009 1.19971 MPRFLG 0 ; 
       7 SCHEM 13.2576 9.605335 0 MPRFLG 0 ; 
       4 SCHEM 10.7576 9.605335 0 MPRFLG 0 ; 
       0 SCHEM 11.91426 -15.03508 0 USR DISPLAY 0 0 SRT 1.528 1.193447 1.528 3.141593 0 0 6.135582e-009 -0.1541755 1.430882 MPRFLG 0 ; 
       5 SCHEM 8.257603 9.605335 0 MPRFLG 0 ; 
       14 SCHEM 11.77983 0.4293551 0 USR DISPLAY 0 0 SRT 1 1 1 1.213072e-007 -3.959524e-008 -0.4956735 0.9297128 0.2199657 1.023064 MPRFLG 0 ; 
       8 SCHEM 21.26617 -9.259897 0 USR DISPLAY 0 0 SRT 1 0.9999999 1 0 1.509958e-007 0 0.2282582 1.654322e-009 3.141537 MPRFLG 0 ; 
       9 SCHEM 23.76617 -9.259897 0 USR DISPLAY 0 0 SRT 1 0.9999999 1 0 1.509958e-007 0 0.2282582 -1.65432e-009 3.141537 MPRFLG 0 ; 
       18 SCHEM 19.27983 0.4293551 0 USR DISPLAY 0 0 SRT 2.454875 2.454875 2.454875 0 0 0 0 -1.207581 0.7964329 MPRFLG 0 ; 
       11 SCHEM 19.27983 -1.570644 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 6.779826 0.4293551 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       3 SCHEM 9.507603 11.60534 0 USR SRT 1 0.4779999 1 0 0 0 0 0 0.9333884 MPRFLG 0 ; 
       1 SCHEM 5.757602 9.605335 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       20 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 4.701956 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 185.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 188.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 190.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 400.7088 21.97929 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 403.2088 21.97929 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 405.7088 21.97929 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 167 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 35.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 38.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 169.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 172 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 159.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 164.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 134.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 137 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 139.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 142 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 129.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 132 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 250.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 253.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 255.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       19 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 2.701956 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 185.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 188.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 190.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 400.7088 19.97929 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 403.2088 19.97929 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 405.7088 19.97929 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 37.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 38.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 167 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 169.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 172 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 159.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 164.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 137 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 139.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 142 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 127 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 129.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 250.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 253.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 255.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
