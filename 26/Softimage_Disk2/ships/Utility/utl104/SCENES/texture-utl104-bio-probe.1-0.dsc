SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.5-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       biodestroyer_cap100-mat136.2-0 ; 
       biodestroyer_cap100-mat137.2-0 ; 
       biodestroyer_cap100-mat138.2-0 ; 
       biodestroyer_cap100-mat146.2-0 ; 
       biodestroyer_cap100-mat147.2-0 ; 
       biodestroyer_cap100-mat148.2-0 ; 
       utl104_bio_probe-mat105.4-0 ; 
       utl104_bio_probe-mat168_1.1-0 ; 
       utl104_bio_probe-mat169.2-0 ; 
       utl104_bio_probe-mat170.2-0 ; 
       utl104_bio_probe-mat174.1-0 ; 
       utl104_bio_probe-mat175.1-0 ; 
       utl104_bio_probe-mat176.1-0 ; 
       utl104_bio_probe-mat177.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       utl104_bio_probe-bottom_gun1.1-0 ; 
       utl104_bio_probe-cube3.1-0 ROOT ; 
       utl104_bio_probe-extru42.1-0 ; 
       utl104_bio_probe-extru44.1-0 ; 
       utl104_bio_probe-extru60.1-0 ; 
       utl104_bio_probe-extru62.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/utl104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl104-bio-probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       biodestroyer_cap100-t2d52.2-0 ; 
       biodestroyer_cap100-t2d53.2-0 ; 
       biodestroyer_cap100-t2d54.2-0 ; 
       biodestroyer_cap100-t2d61.2-0 ; 
       biodestroyer_cap100-t2d62.2-0 ; 
       biodestroyer_cap100-t2d63.2-0 ; 
       utl104_bio_probe-t2d14.4-0 ; 
       utl104_bio_probe-t2d80_1.1-0 ; 
       utl104_bio_probe-t2d81.2-0 ; 
       utl104_bio_probe-t2d82.2-0 ; 
       utl104_bio_probe-t2d86.1-0 ; 
       utl104_bio_probe-t2d87.1-0 ; 
       utl104_bio_probe-t2d88.1-0 ; 
       utl104_bio_probe-t2d89.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000002 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.69618 3.992718 0 MPRFLG 0 ; 
       1 SCHEM 22.69618 5.992718 0 USR SRT 1.203106 1.613309 1.761253 0 0 0 0 -0.001293125 0.4167402 MPRFLG 0 ; 
       2 SCHEM 22.69618 3.992718 0 MPRFLG 0 ; 
       3 SCHEM 20.19618 3.992718 0 MPRFLG 0 ; 
       4 SCHEM 25.19618 3.992718 0 MPRFLG 0 ; 
       5 SCHEM 27.69618 3.992718 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.7542 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 44.25421 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.75421 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34.2542 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.7542 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.2542 4.704131 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 79.30772 -51.68075 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.97991 18.5668 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 142.386 64.4128 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 144.886 64.4128 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 147.386 64.4128 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.7542 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 44.25421 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.75421 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34.2542 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.7542 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39.2542 2.704131 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 79.30772 -53.68075 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50.47991 18.5668 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 142.386 62.4128 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 144.886 62.4128 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 147.386 62.4128 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
