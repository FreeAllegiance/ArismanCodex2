SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.6-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       biodestroyer_cap100-mat136.3-0 ; 
       biodestroyer_cap100-mat137.3-0 ; 
       biodestroyer_cap100-mat138.3-0 ; 
       biodestroyer_cap100-mat146.3-0 ; 
       biodestroyer_cap100-mat147.3-0 ; 
       biodestroyer_cap100-mat148.3-0 ; 
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       utl104_bio_probe-mat105_1.1-0 ; 
       utl104_bio_probe-mat168_1.2-0 ; 
       utl104_bio_probe-mat169.3-0 ; 
       utl104_bio_probe-mat170.3-0 ; 
       utl104_bio_probe-mat174.2-0 ; 
       utl104_bio_probe-mat175.2-0 ; 
       utl104_bio_probe-mat176.2-0 ; 
       utl104_bio_probe-mat177.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       root-bottom_gun1.1-0 ; 
       root-cube3.1-0 ; 
       root-extru42.1-0 ; 
       root-extru44.1-0 ; 
       root-extru60.1-0 ; 
       root-extru62.1-0 ; 
       root-lthrust.1-0 ; 
       root-root.1-0 ROOT ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-trail.1-0 ; 
       root-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/utl104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl104-bioprobe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       biodestroyer_cap100-t2d52.3-0 ; 
       biodestroyer_cap100-t2d53.3-0 ; 
       biodestroyer_cap100-t2d54.3-0 ; 
       biodestroyer_cap100-t2d61.3-0 ; 
       biodestroyer_cap100-t2d62.3-0 ; 
       biodestroyer_cap100-t2d63.3-0 ; 
       utl104_bio_probe-t2d14.5-0 ; 
       utl104_bio_probe-t2d80_1.2-0 ; 
       utl104_bio_probe-t2d81.3-0 ; 
       utl104_bio_probe-t2d82.3-0 ; 
       utl104_bio_probe-t2d86.2-0 ; 
       utl104_bio_probe-t2d87.2-0 ; 
       utl104_bio_probe-t2d88.2-0 ; 
       utl104_bio_probe-t2d89.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 7 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 7 110 ; 
       16 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       8 6 300 ; 
       9 7 300 ; 
       10 9 300 ; 
       11 8 300 ; 
       12 11 300 ; 
       13 10 300 ; 
       14 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 14.39924 13.67999 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.39992 1.223269 0 USR MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 MPRFLG 0 ; 
       6 SCHEM 20.23676 -5.470472 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25.23674 -5.470472 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.736745 -5.470472 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 2.721158 -6.353136 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 10.23674 -5.470472 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 10.25235 -6.397268 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 12.73674 -5.470472 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 12.67703 -6.397268 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 6.482794 -5.779404 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 22.73674 -5.470472 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 23 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 24.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 57.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 59.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 59.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 61.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 64.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 13.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 3 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 0.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 18 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 22 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 23.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
