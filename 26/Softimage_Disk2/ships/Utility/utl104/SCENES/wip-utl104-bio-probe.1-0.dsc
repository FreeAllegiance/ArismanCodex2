SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig20_biofighter-cam_int1.2-0 ROOT ; 
       fig20_biofighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       utl104_bio_probe-mat101.1-0 ; 
       utl104_bio_probe-mat102.1-0 ; 
       utl104_bio_probe-mat103.1-0 ; 
       utl104_bio_probe-mat104.1-0 ; 
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat106.1-0 ; 
       utl104_bio_probe-mat107.1-0 ; 
       utl104_bio_probe-mat108.1-0 ; 
       utl104_bio_probe-mat109.1-0 ; 
       utl104_bio_probe-mat110.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       fig20_biofighter1-canopy_1.1-0 ROOT ; 
       utl104_bio_probe-bottom_gun1.1-0 ; 
       utl104_bio_probe-center-wing1.1-0 ROOT ; 
       utl104_bio_probe-rt-wing6.1-0 ROOT ; 
       utl104_bio_probe-rt-wing7.1-0 ROOT ; 
       utl104_bio_probe-rt-wing8.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl104/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-utl104-bio-probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl104_bio_probe-back1.1-0 ; 
       utl104_bio_probe-Front1.1-0 ; 
       utl104_bio_probe-Rear_front1.1-0 ; 
       utl104_bio_probe-Side1.1-0 ; 
       utl104_bio_probe-t2d14.1-0 ; 
       utl104_bio_probe-t2d15.1-0 ; 
       utl104_bio_probe-t2d16.1-0 ; 
       utl104_bio_probe-t2d17.1-0 ; 
       utl104_bio_probe-t2d18.1-0 ; 
       utl104_bio_probe-t2d19.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 5 300 ; 
       2 6 300 ; 
       4 8 300 ; 
       5 9 300 ; 
       3 7 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       1 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 8 401 ; 
       9 9 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 0 0 SRT 1 1 1 -0.02 1.744019e-009 3.141593 0.0164412 0.5009421 3.420575 MPRFLG 0 ; 
       4 SCHEM 10 0 0 SRT 1 1 1 3.141593 -3.959524e-008 0.4956735 1.098091 0.01840723 0.9874021 MPRFLG 0 ; 
       5 SCHEM 12.5 0 0 SRT 1 1 1 1.509958e-007 -3.959524e-008 2.645919 -1.098091 0.01840723 0.9874021 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 1.213072e-007 -3.959524e-008 -0.4956735 0.9297128 0.2199657 1.023064 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0.3104329 0.8828766 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 9 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 35.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 36.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 38.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 9 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 34.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 37.25 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 38.75 -4 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
