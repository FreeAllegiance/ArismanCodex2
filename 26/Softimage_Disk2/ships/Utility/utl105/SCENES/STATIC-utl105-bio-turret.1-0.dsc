SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.12-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl105_bio_turret-mat164.2-0 ; 
       utl105_bio_turret-mat165.1-0 ; 
       utl105_bio_turret-mat166.2-0 ; 
       utl105_bio_turret-mat169.1-0 ; 
       utl105_bio_turret-mat170.1-0 ; 
       utl105_bio_turret-mat171.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       root-bottom_gun1.1-0 ; 
       root-cube1.1-0 ; 
       root-cyl2.1-0 ; 
       root-extru58.1-0 ; 
       root-extru59.1-0 ; 
       root-extru60.1-0 ; 
       root-root_1.2-0 ROOT ; 
       root-sphere17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/utl105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl105-bio-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl104_bio_probe-t2d14.6-0 ; 
       utl104_bio_probe-t2d72.5-0 ; 
       utl104_bio_probe-t2d73.5-0 ; 
       utl104_bio_probe-t2d74.5-0 ; 
       utl105_bio_turret-t2d76.4-0 ; 
       utl105_bio_turret-t2d77.4-0 ; 
       utl105_bio_turret-t2d78.5-0 ; 
       utl105_bio_turret-t2d81.3-0 ; 
       utl105_bio_turret-t2d82.2-0 ; 
       utl105_bio_turret-t2d83.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       7 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       2 5 300 ; 
       3 6 300 ; 
       4 8 300 ; 
       5 9 300 ; 
       7 4 300 ; 
       7 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.77282 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 36.77282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 39.27282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 34.27282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 57.8875 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 55.3875 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 39.86247 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 46.82687 -2.037286 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35.77282 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.57189 -6.761698 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 33.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 30.77282 -7.438924 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.8875 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54.3875 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35.77282 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.56904 -8.324231 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 33.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30.77282 -9.438924 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.8875 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54.3875 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
