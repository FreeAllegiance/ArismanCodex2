SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat152.1-0 ; 
       utl104_bio_probe-mat153.1-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl104_bio_turret-mat166.1-0 ; 
       utl104_bio_turret-mat167.1-0 ; 
       utl104_bio_turret-mat168.1-0 ; 
       utl104_bio_turret-mat169.1-0 ; 
       utl104_bio_turret-mat170.1-0 ; 
       utl104_bio_turret-mat171.1-0 ; 
       utl104_bio_turret-mat172.1-0 ; 
       utl104_bio_turret-mat173.1-0 ; 
       utl104_bio_turret-mat174.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       root-bottom_gun1.1-0 ; 
       root-cube1.1-0 ; 
       root-cyl2.1-0 ; 
       root-extru58.1-0 ; 
       root-extru59.1-0 ; 
       root-extru60.1-0 ; 
       root-root.1-0 ROOT ; 
       root-sphere17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/cap100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/cap101 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl104-bio-turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       utl104_bio_probe-t2d14.2-0 ; 
       utl104_bio_probe-t2d64.2-0 ; 
       utl104_bio_probe-t2d65.2-0 ; 
       utl104_bio_probe-t2d72.2-0 ; 
       utl104_bio_probe-t2d73.2-0 ; 
       utl104_bio_probe-t2d74.2-0 ; 
       utl104_bio_turret-t2d78.2-0 ; 
       utl104_bio_turret-t2d79.2-0 ; 
       utl104_bio_turret-t2d80.2-0 ; 
       utl104_bio_turret-t2d81.2-0 ; 
       utl104_bio_turret-t2d82.2-0 ; 
       utl104_bio_turret-t2d83.2-0 ; 
       utl104_bio_turret-t2d84.2-0 ; 
       utl104_bio_turret-t2d85.2-0 ; 
       utl104_bio_turret-t2d86.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       2 6 110 ; 
       1 6 110 ; 
       0 1 110 ; 
       7 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       0 0 300 ; 
       7 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 225.9265 -1.582805 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 225.9265 -3.582805 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 238.217 -3.440048 0 MPRFLG 0 ; 
       6 SCHEM 241.967 -1.440048 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 243.217 -3.440048 0 MPRFLG 0 ; 
       5 SCHEM 240.717 -3.440048 0 MPRFLG 0 ; 
       2 SCHEM 248.217 -3.440048 0 MPRFLG 0 ; 
       1 SCHEM 245.717 -3.440048 0 MPRFLG 0 ; 
       0 SCHEM 245.717 -5.440049 0 MPRFLG 0 ; 
       7 SCHEM 235.717 -3.440048 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 250.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 253.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 255.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 273.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 268.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 270.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 265.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 260.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 263.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 250.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 253.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 255.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 273.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 268.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 270.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 265.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 260.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 263.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
