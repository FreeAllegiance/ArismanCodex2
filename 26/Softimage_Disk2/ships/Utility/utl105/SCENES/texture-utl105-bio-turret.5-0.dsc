SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.9-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl105_bio_turret-mat164.2-0 ; 
       utl105_bio_turret-mat165.1-0 ; 
       utl105_bio_turret-mat166.2-0 ; 
       utl105_bio_turret-mat167.2-0 ; 
       utl105_bio_turret-mat168.1-0 ; 
       utl105_bio_turret-mat169.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       root-bottom_gun1.1-0 ; 
       root-cube1.1-0 ; 
       root-cyl2.1-0 ; 
       root-extru58.1-0 ; 
       root-extru61.1-0 ; 
       root-extru62.1-0 ; 
       root-root.6-0 ROOT ; 
       root-sphere17.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/utl105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl105-bio-turret.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl104_bio_probe-t2d14.5-0 ; 
       utl104_bio_probe-t2d72.4-0 ; 
       utl104_bio_probe-t2d73.4-0 ; 
       utl104_bio_probe-t2d74.4-0 ; 
       utl105_bio_turret-t2d76.3-0 ; 
       utl105_bio_turret-t2d77.3-0 ; 
       utl105_bio_turret-t2d78.3-0 ; 
       utl105_bio_turret-t2d79.3-0 ; 
       utl105_bio_turret-t2d80.2-0 ; 
       utl105_bio_turret-t2d81.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       7 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       4 7 300 ; 
       5 8 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       2 5 300 ; 
       3 6 300 ; 
       7 4 300 ; 
       7 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       6 6 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10.58965 0 0 DISPLAY 1 2 SRT 1 1 1 -6.283185 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 17.55405 -2.037286 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.29907 -6.761698 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -7.438924 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.296217 -8.324231 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -9.438924 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
