SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.11-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl105_bio_turret-mat164.2-0 ; 
       utl105_bio_turret-mat165.1-0 ; 
       utl105_bio_turret-mat166.2-0 ; 
       utl105_bio_turret-mat169.1-0 ; 
       utl105_bio_turret-mat170.1-0 ; 
       utl105_bio_turret-mat171.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       root-bottom_gun1.1-0 ; 
       root-cube1.1-0 ; 
       root-cyl2.1-0 ; 
       root-extru58.1-0 ; 
       root-extru59.1-0 ; 
       root-extru60.1-0 ; 
       root-lthrust.1-0 ; 
       root-root_1.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere17.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/utl105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl105-bio-turret.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl104_bio_probe-t2d14.6-0 ; 
       utl104_bio_probe-t2d72.5-0 ; 
       utl104_bio_probe-t2d73.5-0 ; 
       utl104_bio_probe-t2d74.5-0 ; 
       utl105_bio_turret-t2d76.4-0 ; 
       utl105_bio_turret-t2d77.4-0 ; 
       utl105_bio_turret-t2d78.5-0 ; 
       utl105_bio_turret-t2d81.3-0 ; 
       utl105_bio_turret-t2d82.2-0 ; 
       utl105_bio_turret-t2d83.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       8 7 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       2 12 300 ; 
       3 13 300 ; 
       4 15 300 ; 
       5 16 300 ; 
       10 11 300 ; 
       10 14 300 ; 
       11 0 300 ; 
       12 1 300 ; 
       13 3 300 ; 
       14 2 300 ; 
       15 5 300 ; 
       16 4 300 ; 
       17 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.77282 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 36.77282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 39.27282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 34.27282 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 57.8875 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 55.3875 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 47.88751 -6.759284 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 39.86247 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 52.8875 -6.759284 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 49.57815 -4.53856 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 46.82687 -2.037286 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 30.3875 -6.759284 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 30.37191 -7.641948 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 37.8875 -6.759284 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 37.9031 -7.686081 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 40.3875 -6.759284 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 40.32778 -7.686081 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 34.13355 -7.068217 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 50.3875 -6.759284 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35.77282 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.57189 -6.761698 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 38.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 33.27282 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30.77282 -7.438924 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 56.8875 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 54.3875 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 35.77282 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.56904 -8.324231 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 38.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 33.27282 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30.77282 -9.438924 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.8875 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54.3875 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
