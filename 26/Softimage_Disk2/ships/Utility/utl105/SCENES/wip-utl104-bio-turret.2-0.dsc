SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.2-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       utl104_bio_probe-mat105.1-0 ; 
       utl104_bio_probe-mat106.1-0 ; 
       utl104_bio_probe-mat107.1-0 ; 
       utl104_bio_probe-mat152.1-0 ; 
       utl104_bio_probe-mat153.1-0 ; 
       utl104_bio_probe-mat154.1-0 ; 
       utl104_bio_probe-mat155.1-0 ; 
       utl104_bio_probe-mat156.1-0 ; 
       utl104_bio_probe-mat160.1-0 ; 
       utl104_bio_probe-mat161.1-0 ; 
       utl104_bio_probe-mat162.1-0 ; 
       utl104_bio_turret-mat166.1-0 ; 
       utl104_bio_turret-mat167.1-0 ; 
       utl104_bio_turret-mat168.1-0 ; 
       utl104_bio_turret-mat169.1-0 ; 
       utl104_bio_turret-mat170.1-0 ; 
       utl104_bio_turret-mat171.1-0 ; 
       utl104_bio_turret-mat172.1-0 ; 
       utl104_bio_turret-mat173.1-0 ; 
       utl104_bio_turret-mat174.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       destroyer1-center-wing1.1-0 ROOT ; 
       utl104_bio_probe-extru56.1-0 ; 
       utl104_bio_probe-sphere17.1-0 ROOT ; 
       utl104_bio_turret-bottom_gun1.1-0 ; 
       utl104_bio_turret-cube1.1-0 ; 
       utl104_bio_turret-cyl2.1-0 ROOT ; 
       utl104_bio_turret-extru58.1-0 ; 
       utl104_bio_turret-extru59.1-0 ; 
       utl104_bio_turret-extru60.1-0 ; 
       utl104_bio_turret-null1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/cap100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/cap101 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl105/PICTURES/fig20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-utl104-bio-turret.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       utl104_bio_probe-t2d14.1-0 ; 
       utl104_bio_probe-t2d15.1-0 ; 
       utl104_bio_probe-t2d16.1-0 ; 
       utl104_bio_probe-t2d64.1-0 ; 
       utl104_bio_probe-t2d65.1-0 ; 
       utl104_bio_probe-t2d66.1-0 ; 
       utl104_bio_probe-t2d67.1-0 ; 
       utl104_bio_probe-t2d68.1-0 ; 
       utl104_bio_probe-t2d72.1-0 ; 
       utl104_bio_probe-t2d73.1-0 ; 
       utl104_bio_probe-t2d74.1-0 ; 
       utl104_bio_turret-t2d78.1-0 ; 
       utl104_bio_turret-t2d79.1-0 ; 
       utl104_bio_turret-t2d80.1-0 ; 
       utl104_bio_turret-t2d81.1-0 ; 
       utl104_bio_turret-t2d82.1-0 ; 
       utl104_bio_turret-t2d83.1-0 ; 
       utl104_bio_turret-t2d84.1-0 ; 
       utl104_bio_turret-t2d85.1-0 ; 
       utl104_bio_turret-t2d86.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 9 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       4 9 110 ; 
       3 4 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       3 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 3 300 ; 
       2 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 18 401 ; 
       19 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 225.9265 -1.582805 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 225.9265 -3.582805 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 232.9044 2.762156 0 MPRFLG 0 ; 
       9 SCHEM 245.4044 4.762156 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 247.9044 2.762156 0 MPRFLG 0 ; 
       8 SCHEM 240.4044 2.762156 0 MPRFLG 0 ; 
       5 SCHEM 275.75 7.153368 0 SRT 0.6769777 0.06498984 0.6769777 0 0 0 0 -0.8816311 0 MPRFLG 0 ; 
       4 SCHEM 256.6544 2.762156 0 MPRFLG 0 ; 
       3 SCHEM 252.9044 0.7621571 0 MPRFLG 0 ; 
       0 SCHEM 246.0769 -1.456035 0 USR DISPLAY 0 0 SRT 0.602388 0.602388 0.602388 -0.02 1.744019e-009 3.141593 0.009903982 0.4078301 1.77534 MPRFLG 0 ; 
       1 SCHEM 240.9265 -3.582805 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 240.9265 -1.582805 0 USR DISPLAY 0 0 SRT 2.454875 2.454875 2.454875 0 0 0 0 -1.207581 0.7964329 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 250.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 253.25 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 255.75 7.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 257.25 5.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 273.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 268.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 270.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 265.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 260.75 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 263.25 3.153368 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 250.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 253.25 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 255.75 5.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 257.25 3.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 273.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 268.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 270.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 265.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 260.75 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 263.25 1.153368 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
