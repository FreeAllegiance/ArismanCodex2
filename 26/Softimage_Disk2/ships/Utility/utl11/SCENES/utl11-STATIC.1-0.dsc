SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.14-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       cargo-light7_3_1_1.13-0 ROOT ; 
       cargo-light7_3_1_1_1.13-0 ROOT ; 
       cargo-light8_3_1_1.13-0 ROOT ; 
       cargo-light8_3_1_1_1.13-0 ROOT ; 
       cargo-light9_3_1_1.13-0 ROOT ; 
       cargo-light9_3_1_1_1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       cargoclose_F-mat1.3-0 ; 
       cargoclose_F-mat2.3-0 ; 
       cargoclose_F-mat3.3-0 ; 
       cargoclose_F-mat4.3-0 ; 
       cargoclose_F-mat5.3-0 ; 
       cargoclose_F-mat6.3-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat24.1-0 ; 
       STATIC-mat25.1-0 ; 
       STATIC-mat26.1-0 ; 
       STATIC-mat27.1-0 ; 
       STATIC-mat28.1-0 ; 
       STATIC-mat29.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat30.1-0 ; 
       STATIC-mat33.1-0 ; 
       STATIC-mat34.1-0 ; 
       STATIC-mat35.1-0 ; 
       STATIC-mat36.1-0 ; 
       STATIC-mat37.1-0 ; 
       STATIC-mat38.1-0 ; 
       STATIC-mat39.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat40.1-0 ; 
       STATIC-mat47.1-0 ; 
       STATIC-mat48.1-0 ; 
       STATIC-mat49.1-0 ; 
       STATIC-mat50.1-0 ; 
       STATIC-mat51.1-0 ; 
       STATIC-mat52.1-0 ; 
       STATIC-mat53.1-0 ; 
       STATIC-mat54.1-0 ; 
       STATIC-mat55.1-0 ; 
       STATIC-mat56.1-0 ; 
       STATIC-mat57.1-0 ; 
       STATIC-mat58.1-0 ; 
       STATIC-mat59.1-0 ; 
       STATIC-mat60.1-0 ; 
       STATIC-mat61.1-0 ; 
       STATIC-mat62.1-0 ; 
       STATIC-mat63.1-0 ; 
       STATIC-mat64.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       utl11-acrgatt1.1-0 ; 
       utl11-acrgatt2.1-0 ; 
       utl11-acrgatt3.1-0 ; 
       utl11-bcrgatt.1-0 ; 
       utl11-bcrgatt1.1-0 ; 
       utl11-bcrgatt2.1-0 ; 
       utl11-blthrust.2-0 ; 
       utl11-brthrust.5-0 ; 
       utl11-bthrust0.1-0 ; 
       utl11-fcrgatt.1-0 ; 
       utl11-fcrgatt1.1-0 ; 
       utl11-fcrgatt2.1-0 ; 
       utl11-fuselg.3-0 ; 
       utl11-fuselg1.3-0 ; 
       utl11-fuselg2.1-0 ; 
       utl11-fuselg3.1-0 ; 
       utl11-lbtractr1.1-0 ; 
       utl11-lbtractr2.1-0 ; 
       utl11-lstrake0.1-0 ; 
       utl11-lstrake1.1-0 ; 
       utl11-lstrake2.1-0 ; 
       utl11-lstrake3.1-0 ; 
       utl11-ltthrust.2-0 ; 
       utl11-rbtractr1.1-0 ; 
       utl11-rbtractr2.1-0 ; 
       utl11-rstrake0.1-0 ; 
       utl11-rstrake1.1-0 ; 
       utl11-rstrake2.1-0 ; 
       utl11-rstrake3.1-0 ; 
       utl11-rtthrust.2-0 ; 
       utl11-rttractr1.1-0 ; 
       utl11-rttractr2.1-0 ; 
       utl11-tractr.1-0 ; 
       utl11-utl11.11-0 ROOT ; 
       utl11-utl15a.2-0 ; 
       utl11-utl15a1.1-0 ; 
       utl11-utl15a2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl11/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl11-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       cargoclose_F-t2d1.3-0 ; 
       cargoclose_F-t2d2.3-0 ; 
       cargoclose_F-t2d3.3-0 ; 
       cargoclose_F-t2d4.3-0 ; 
       cargoclose_F-t2d6.3-0 ; 
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d23.1-0 ; 
       STATIC-t2d24.1-0 ; 
       STATIC-t2d25.1-0 ; 
       STATIC-t2d26.1-0 ; 
       STATIC-t2d27.1-0 ; 
       STATIC-t2d28.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d31.1-0 ; 
       STATIC-t2d32.1-0 ; 
       STATIC-t2d33.1-0 ; 
       STATIC-t2d34.1-0 ; 
       STATIC-t2d35.1-0 ; 
       STATIC-t2d36.1-0 ; 
       STATIC-t2d37.1-0 ; 
       STATIC-t2d39.1-0 ; 
       STATIC-t2d40.1-0 ; 
       STATIC-t2d41.1-0 ; 
       STATIC-t2d42.1-0 ; 
       STATIC-t2d43.1-0 ; 
       STATIC-t2d44.1-0 ; 
       STATIC-t2d45.1-0 ; 
       STATIC-t2d46.1-0 ; 
       STATIC-t2d47.1-0 ; 
       STATIC-t2d48.1-0 ; 
       STATIC-t2d49.1-0 ; 
       STATIC-t2d50.1-0 ; 
       STATIC-t2d51.1-0 ; 
       STATIC-t2d52.1-0 ; 
       STATIC-t2d53.1-0 ; 
       STATIC-t2d54.1-0 ; 
       STATIC-t2d55.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 14 110 ; 
       2 15 110 ; 
       3 12 110 ; 
       4 14 110 ; 
       5 15 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 13 110 ; 
       9 12 110 ; 
       10 14 110 ; 
       11 15 110 ; 
       12 34 110 ; 
       13 33 110 ; 
       14 35 110 ; 
       15 36 110 ; 
       16 13 110 ; 
       17 16 110 ; 
       18 13 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 13 110 ; 
       23 13 110 ; 
       24 23 110 ; 
       25 13 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 30 110 ; 
       32 17 110 ; 
       34 13 110 ; 
       35 13 110 ; 
       36 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 40 300 ; 
       6 41 300 ; 
       7 42 300 ; 
       7 43 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       8 39 300 ; 
       12 0 300 ; 
       12 1 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       12 4 300 ; 
       12 5 300 ; 
       13 6 300 ; 
       13 27 300 ; 
       13 36 300 ; 
       13 56 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       14 44 300 ; 
       14 45 300 ; 
       14 46 300 ; 
       14 47 300 ; 
       14 48 300 ; 
       14 49 300 ; 
       15 50 300 ; 
       15 51 300 ; 
       15 52 300 ; 
       15 53 300 ; 
       15 54 300 ; 
       15 55 300 ; 
       16 18 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       17 21 300 ; 
       19 34 300 ; 
       20 33 300 ; 
       21 35 300 ; 
       22 15 300 ; 
       22 16 300 ; 
       22 17 300 ; 
       23 22 300 ; 
       23 23 300 ; 
       23 24 300 ; 
       24 25 300 ; 
       26 31 300 ; 
       27 30 300 ; 
       28 32 300 ; 
       29 12 300 ; 
       29 13 300 ; 
       29 14 300 ; 
       30 26 300 ; 
       31 28 300 ; 
       32 29 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 40 400 ; 
       13 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 52 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 16 401 ; 
       28 25 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 26 401 ; 
       38 34 401 ; 
       39 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       45 41 401 ; 
       46 42 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       51 46 401 ; 
       52 47 401 ; 
       53 48 401 ; 
       54 49 401 ; 
       55 50 401 ; 
       56 51 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -12 0 MPRFLG 0 ; 
       1 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       2 SCHEM 60 -12 0 MPRFLG 0 ; 
       3 SCHEM 40 -12 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       5 SCHEM 55 -12 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 MPRFLG 0 ; 
       8 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       10 SCHEM 50 -12 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 30 -6 0 MPRFLG 0 ; 
       14 SCHEM 50 -10 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 15 -10 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 5 -10 0 MPRFLG 0 ; 
       20 SCHEM 0 -10 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 30 -10 0 MPRFLG 0 ; 
       27 SCHEM 25 -10 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 MPRFLG 0 ; 
       31 SCHEM 35 -10 0 MPRFLG 0 ; 
       32 SCHEM 15 -12 0 MPRFLG 0 ; 
       33 SCHEM 30 -4 0 SRT 0.484 0.484 0.484 0 0 0 0 0 -1.077347 MPRFLG 0 ; 
       34 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 50 -8 0 MPRFLG 0 ; 
       36 SCHEM 57.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 46.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 61.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 34 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 61.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
