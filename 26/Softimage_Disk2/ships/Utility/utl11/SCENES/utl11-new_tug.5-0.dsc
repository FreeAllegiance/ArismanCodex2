SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl11-utl11.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.5-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       cargo-light7_3_1_1.6-0 ROOT ; 
       cargo-light7_3_1_1_1.6-0 ROOT ; 
       cargo-light8_3_1_1.6-0 ROOT ; 
       cargo-light8_3_1_1_1.6-0 ROOT ; 
       cargo-light9_3_1_1.6-0 ROOT ; 
       cargo-light9_3_1_1_1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       cargoclose_F-mat1.3-0 ; 
       cargoclose_F-mat2.3-0 ; 
       cargoclose_F-mat3.3-0 ; 
       cargoclose_F-mat4.3-0 ; 
       cargoclose_F-mat5.3-0 ; 
       cargoclose_F-mat6.3-0 ; 
       new_tug-mat1.2-0 ; 
       new_tug-mat10.2-0 ; 
       new_tug-mat11.2-0 ; 
       new_tug-mat12.2-0 ; 
       new_tug-mat13.2-0 ; 
       new_tug-mat14.2-0 ; 
       new_tug-mat15.2-0 ; 
       new_tug-mat16.2-0 ; 
       new_tug-mat17.2-0 ; 
       new_tug-mat18.2-0 ; 
       new_tug-mat19.2-0 ; 
       new_tug-mat20.2-0 ; 
       new_tug-mat21.2-0 ; 
       new_tug-mat22.2-0 ; 
       new_tug-mat23.2-0 ; 
       new_tug-mat24.2-0 ; 
       new_tug-mat25.2-0 ; 
       new_tug-mat26.2-0 ; 
       new_tug-mat27.2-0 ; 
       new_tug-mat28.2-0 ; 
       new_tug-mat29.2-0 ; 
       new_tug-mat3.2-0 ; 
       new_tug-mat30.2-0 ; 
       new_tug-mat31.2-0 ; 
       new_tug-mat32.2-0 ; 
       new_tug-mat33.2-0 ; 
       new_tug-mat34.2-0 ; 
       new_tug-mat35.2-0 ; 
       new_tug-mat36.2-0 ; 
       new_tug-mat37.2-0 ; 
       new_tug-mat38.2-0 ; 
       new_tug-mat39.2-0 ; 
       new_tug-mat4.2-0 ; 
       new_tug-mat40.2-0 ; 
       new_tug-mat47.2-0 ; 
       new_tug-mat48.2-0 ; 
       new_tug-mat49.2-0 ; 
       new_tug-mat50.2-0 ; 
       new_tug-mat51.2-0 ; 
       new_tug-mat52.2-0 ; 
       new_tug-mat53.1-0 ; 
       new_tug-mat54.1-0 ; 
       new_tug-mat55.1-0 ; 
       new_tug-mat56.1-0 ; 
       new_tug-mat57.1-0 ; 
       new_tug-mat58.1-0 ; 
       new_tug-mat59.1-0 ; 
       new_tug-mat60.1-0 ; 
       new_tug-mat61.1-0 ; 
       new_tug-mat62.1-0 ; 
       new_tug-mat63.1-0 ; 
       new_tug-mat64.1-0 ; 
       new_tug-mat9.2-0 ; 
       new_tug-nose_white-center.1-0.2-0 ; 
       new_tug-nose_white-center.1-1.2-0 ; 
       new_tug-nose_white-center.1-2.2-0 ; 
       new_tug-nose_white-center.1-3.2-0 ; 
       new_tug-nose_white-center.1-4.2-0 ; 
       new_tug-port_red-left.1-0.2-0 ; 
       new_tug-starbord_green-right.1-0.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       new_tug-tetra5.1-0 ROOT ; 
       new_tug-tetra6.1-0 ROOT ; 
       utl11-acrgatt.1-0 ; 
       utl11-acrgatt1.1-0 ; 
       utl11-acrgatt2.1-0 ; 
       utl11-acrgatt3.1-0 ; 
       utl11-bcrgatt.1-0 ; 
       utl11-bcrgatt1.1-0 ; 
       utl11-bcrgatt2.1-0 ; 
       utl11-blthrust.2-0 ; 
       utl11-brthrust.5-0 ; 
       utl11-bthrust0.1-0 ; 
       utl11-cockpt.1-0 ; 
       utl11-fcrgatt.1-0 ; 
       utl11-fcrgatt_1.1-0 ; 
       utl11-fcrgatt1.1-0 ; 
       utl11-fcrgatt2.1-0 ; 
       utl11-fuselg.3-0 ; 
       utl11-fuselg1.3-0 ; 
       utl11-fuselg2.1-0 ; 
       utl11-fuselg3.1-0 ; 
       utl11-lbtractr1.1-0 ; 
       utl11-lbtractr2.1-0 ; 
       utl11-lstrake0.1-0 ; 
       utl11-lstrake1.1-0 ; 
       utl11-lstrake2.1-0 ; 
       utl11-lstrake3.1-0 ; 
       utl11-lthrust.1-0 ; 
       utl11-ltthrust.2-0 ; 
       utl11-lttractr1.1-0 ; 
       utl11-lttractr2.1-0 ; 
       utl11-rbtractr1.1-0 ; 
       utl11-rbtractr2.1-0 ; 
       utl11-rstrake0.1-0 ; 
       utl11-rstrake1.1-0 ; 
       utl11-rstrake2.1-0 ; 
       utl11-rstrake3.1-0 ; 
       utl11-rthrust.1-0 ; 
       utl11-rtthrust.2-0 ; 
       utl11-rttractr1.1-0 ; 
       utl11-rttractr2.1-0 ; 
       utl11-SSa1.1-0 ; 
       utl11-SSa2.1-0 ; 
       utl11-SSa3.1-0 ; 
       utl11-SSa4.1-0 ; 
       utl11-SSf.1-0 ; 
       utl11-SSl.1-0 ; 
       utl11-SSr.1-0 ; 
       utl11-tractr.1-0 ; 
       utl11-trail.1-0 ; 
       utl11-turatt.1-0 ; 
       utl11-utl11.5-0 ROOT ; 
       utl11-utl15a.2-0 ; 
       utl11-utl15a1.1-0 ; 
       utl11-utl15a2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl11/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl11-new_tug.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 55     
       cargoclose_F-t2d1.3-0 ; 
       cargoclose_F-t2d2.3-0 ; 
       cargoclose_F-t2d3.3-0 ; 
       cargoclose_F-t2d4.3-0 ; 
       cargoclose_F-t2d6.3-0 ; 
       new_tug-t2d1.2-0 ; 
       new_tug-t2d10.2-0 ; 
       new_tug-t2d11.2-0 ; 
       new_tug-t2d12.2-0 ; 
       new_tug-t2d13.2-0 ; 
       new_tug-t2d14.2-0 ; 
       new_tug-t2d15.2-0 ; 
       new_tug-t2d16.2-0 ; 
       new_tug-t2d17.2-0 ; 
       new_tug-t2d18.2-0 ; 
       new_tug-t2d19.2-0 ; 
       new_tug-t2d2.2-0 ; 
       new_tug-t2d20.2-0 ; 
       new_tug-t2d21.2-0 ; 
       new_tug-t2d22.2-0 ; 
       new_tug-t2d23.2-0 ; 
       new_tug-t2d24.2-0 ; 
       new_tug-t2d25.2-0 ; 
       new_tug-t2d26.2-0 ; 
       new_tug-t2d27.2-0 ; 
       new_tug-t2d28.2-0 ; 
       new_tug-t2d29.2-0 ; 
       new_tug-t2d3.2-0 ; 
       new_tug-t2d30.2-0 ; 
       new_tug-t2d31.2-0 ; 
       new_tug-t2d32.2-0 ; 
       new_tug-t2d33.2-0 ; 
       new_tug-t2d34.2-0 ; 
       new_tug-t2d35.2-0 ; 
       new_tug-t2d36.2-0 ; 
       new_tug-t2d37.2-0 ; 
       new_tug-t2d39.2-0 ; 
       new_tug-t2d40.2-0 ; 
       new_tug-t2d41.2-0 ; 
       new_tug-t2d42.2-0 ; 
       new_tug-t2d43.2-0 ; 
       new_tug-t2d44.2-0 ; 
       new_tug-t2d45.2-0 ; 
       new_tug-t2d46.1-0 ; 
       new_tug-t2d47.1-0 ; 
       new_tug-t2d48.1-0 ; 
       new_tug-t2d49.1-0 ; 
       new_tug-t2d50.1-0 ; 
       new_tug-t2d51.1-0 ; 
       new_tug-t2d52.1-0 ; 
       new_tug-t2d53.1-0 ; 
       new_tug-t2d54.1-0 ; 
       new_tug-t2d55.1-0 ; 
       new_tug-t2d8.2-0 ; 
       new_tug-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 18 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 18 110 ; 
       14 48 110 ; 
       18 51 110 ; 
       21 18 110 ; 
       22 21 110 ; 
       23 18 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       28 18 110 ; 
       29 18 110 ; 
       30 29 110 ; 
       31 18 110 ; 
       32 31 110 ; 
       33 18 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       36 33 110 ; 
       38 18 110 ; 
       39 18 110 ; 
       40 39 110 ; 
       41 18 110 ; 
       42 18 110 ; 
       43 18 110 ; 
       44 18 110 ; 
       45 18 110 ; 
       46 28 110 ; 
       47 38 110 ; 
       48 22 110 ; 
       50 18 110 ; 
       3 17 110 ; 
       6 17 110 ; 
       13 17 110 ; 
       17 52 110 ; 
       52 18 110 ; 
       53 18 110 ; 
       19 53 110 ; 
       15 19 110 ; 
       4 19 110 ; 
       7 19 110 ; 
       54 18 110 ; 
       20 54 110 ; 
       16 20 110 ; 
       5 20 110 ; 
       8 20 110 ; 
       27 18 110 ; 
       37 18 110 ; 
       49 20 110 ; 
       12 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 42 300 ; 
       9 43 300 ; 
       10 44 300 ; 
       10 45 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       18 6 300 ; 
       18 27 300 ; 
       18 38 300 ; 
       18 58 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       18 10 300 ; 
       18 11 300 ; 
       21 18 300 ; 
       21 19 300 ; 
       21 20 300 ; 
       22 21 300 ; 
       24 36 300 ; 
       25 35 300 ; 
       26 37 300 ; 
       28 15 300 ; 
       28 16 300 ; 
       28 17 300 ; 
       29 29 300 ; 
       30 30 300 ; 
       31 22 300 ; 
       31 23 300 ; 
       31 24 300 ; 
       32 25 300 ; 
       34 33 300 ; 
       35 32 300 ; 
       36 34 300 ; 
       38 12 300 ; 
       38 13 300 ; 
       38 14 300 ; 
       39 26 300 ; 
       40 28 300 ; 
       41 59 300 ; 
       42 62 300 ; 
       43 61 300 ; 
       44 63 300 ; 
       45 60 300 ; 
       46 64 300 ; 
       47 65 300 ; 
       48 31 300 ; 
       17 0 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       17 5 300 ; 
       19 46 300 ; 
       19 47 300 ; 
       19 48 300 ; 
       19 49 300 ; 
       19 50 300 ; 
       19 51 300 ; 
       20 52 300 ; 
       20 53 300 ; 
       20 54 300 ; 
       20 55 300 ; 
       20 56 300 ; 
       20 57 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 42 400 ; 
       18 10 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       51 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 5 401 ; 
       7 54 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 16 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 27 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       58 53 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       50 46 401 ; 
       51 47 401 ; 
       53 48 401 ; 
       54 49 401 ; 
       55 50 401 ; 
       56 51 401 ; 
       57 52 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 77.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 80 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 82.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 85 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 87.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 90 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 20 -4 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 5 -4 0 MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 5 -6 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 35 -4 0 MPRFLG 0 ; 
       30 SCHEM 35 -6 0 MPRFLG 0 ; 
       31 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 45 -6 0 MPRFLG 0 ; 
       35 SCHEM 40 -6 0 MPRFLG 0 ; 
       36 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       38 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 50 -4 0 MPRFLG 0 ; 
       40 SCHEM 50 -6 0 MPRFLG 0 ; 
       41 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 10 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 15 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 17.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 32.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       50 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       51 SCHEM 38.75 0 0 SRT 1 0.8611202 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 60 -8 0 MPRFLG 0 ; 
       6 SCHEM 55 -8 0 MPRFLG 0 ; 
       13 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       52 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       53 SCHEM 65 -4 0 MPRFLG 0 ; 
       19 SCHEM 65 -6 0 MPRFLG 0 ; 
       15 SCHEM 65 -8 0 MPRFLG 0 ; 
       4 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       54 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 75 -8 0 MPRFLG 0 ; 
       8 SCHEM 70 -8 0 MPRFLG 0 ; 
       27 SCHEM 92.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 95 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 97.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 100 0.3514161 0 USR WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 102.5 0 0 WIRECOL 1 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 105 0 0 WIRECOL 1 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 76.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 76.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 76.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 76.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 76.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
