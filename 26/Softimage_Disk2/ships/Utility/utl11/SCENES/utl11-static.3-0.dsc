SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.10-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       cargo-light7_3_1_1.11-0 ROOT ; 
       cargo-light7_3_1_1_1.11-0 ROOT ; 
       cargo-light8_3_1_1.11-0 ROOT ; 
       cargo-light8_3_1_1_1.11-0 ROOT ; 
       cargo-light9_3_1_1.11-0 ROOT ; 
       cargo-light9_3_1_1_1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       cargoclose_F-mat1.3-0 ; 
       cargoclose_F-mat2.3-0 ; 
       cargoclose_F-mat3.3-0 ; 
       cargoclose_F-mat4.3-0 ; 
       cargoclose_F-mat5.3-0 ; 
       cargoclose_F-mat6.3-0 ; 
       static-mat1.2-0 ; 
       static-mat10.2-0 ; 
       static-mat11.2-0 ; 
       static-mat12.2-0 ; 
       static-mat13.2-0 ; 
       static-mat14.2-0 ; 
       static-mat15.2-0 ; 
       static-mat16.2-0 ; 
       static-mat17.2-0 ; 
       static-mat18.2-0 ; 
       static-mat19.2-0 ; 
       static-mat20.2-0 ; 
       static-mat21.2-0 ; 
       static-mat22.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat26.2-0 ; 
       static-mat27.2-0 ; 
       static-mat28.2-0 ; 
       static-mat29.2-0 ; 
       static-mat3.2-0 ; 
       static-mat30.2-0 ; 
       static-mat31.2-0 ; 
       static-mat32.2-0 ; 
       static-mat33.2-0 ; 
       static-mat34.2-0 ; 
       static-mat35.2-0 ; 
       static-mat36.2-0 ; 
       static-mat37.2-0 ; 
       static-mat38.2-0 ; 
       static-mat39.2-0 ; 
       static-mat4.2-0 ; 
       static-mat40.2-0 ; 
       static-mat47.2-0 ; 
       static-mat48.2-0 ; 
       static-mat49.2-0 ; 
       static-mat50.2-0 ; 
       static-mat51.2-0 ; 
       static-mat52.2-0 ; 
       static-mat53.2-0 ; 
       static-mat54.2-0 ; 
       static-mat55.2-0 ; 
       static-mat56.2-0 ; 
       static-mat57.2-0 ; 
       static-mat58.2-0 ; 
       static-mat59.2-0 ; 
       static-mat60.2-0 ; 
       static-mat61.2-0 ; 
       static-mat62.2-0 ; 
       static-mat63.2-0 ; 
       static-mat64.2-0 ; 
       static-mat9.2-0 ; 
       static-nose_white-center.1-0.2-0 ; 
       static-nose_white-center.1-1.2-0 ; 
       static-nose_white-center.1-2.2-0 ; 
       static-nose_white-center.1-3.2-0 ; 
       static-nose_white-center.1-4.2-0 ; 
       static-port_red-left.1-0.2-0 ; 
       static-starbord_green-right.1-0.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       utl11-acrgatt.1-0 ; 
       utl11-acrgatt1.1-0 ; 
       utl11-acrgatt2.1-0 ; 
       utl11-acrgatt3.1-0 ; 
       utl11-bcrgatt.1-0 ; 
       utl11-bcrgatt1.1-0 ; 
       utl11-bcrgatt2.1-0 ; 
       utl11-blthrust.2-0 ; 
       utl11-brthrust.5-0 ; 
       utl11-bthrust0.1-0 ; 
       utl11-cockpt.1-0 ; 
       utl11-fcrgatt.1-0 ; 
       utl11-fcrgatt_1.1-0 ; 
       utl11-fcrgatt1.1-0 ; 
       utl11-fcrgatt2.1-0 ; 
       utl11-fuselg.3-0 ; 
       utl11-fuselg1.3-0 ; 
       utl11-fuselg2.1-0 ; 
       utl11-fuselg3.1-0 ; 
       utl11-lbtractr1.1-0 ; 
       utl11-lbtractr2.1-0 ; 
       utl11-lstrake0.1-0 ; 
       utl11-lstrake1.1-0 ; 
       utl11-lstrake2.1-0 ; 
       utl11-lstrake3.1-0 ; 
       utl11-lthrust.1-0 ; 
       utl11-ltthrust.2-0 ; 
       utl11-lttractr1.1-0 ; 
       utl11-lttractr2.1-0 ; 
       utl11-rbtractr1.1-0 ; 
       utl11-rbtractr2.1-0 ; 
       utl11-rstrake0.1-0 ; 
       utl11-rstrake1.1-0 ; 
       utl11-rstrake2.1-0 ; 
       utl11-rstrake3.1-0 ; 
       utl11-rthrust.1-0 ; 
       utl11-rtthrust.2-0 ; 
       utl11-rttractr1.1-0 ; 
       utl11-rttractr2.1-0 ; 
       utl11-SSa1.1-0 ; 
       utl11-SSa2.1-0 ; 
       utl11-SSa3.1-0 ; 
       utl11-SSa4.1-0 ; 
       utl11-SSf.1-0 ; 
       utl11-SSl.1-0 ; 
       utl11-SSr.1-0 ; 
       utl11-tractr.1-0 ; 
       utl11-trail.1-0 ; 
       utl11-turatt.1-0 ; 
       utl11-utl11.8-0 ROOT ; 
       utl11-utl15a.2-0 ; 
       utl11-utl15a1.1-0 ; 
       utl11-utl15a2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl11/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl11-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 55     
       cargoclose_F-t2d1.3-0 ; 
       cargoclose_F-t2d2.3-0 ; 
       cargoclose_F-t2d3.3-0 ; 
       cargoclose_F-t2d4.3-0 ; 
       cargoclose_F-t2d6.3-0 ; 
       static-t2d1.2-0 ; 
       static-t2d10.2-0 ; 
       static-t2d11.2-0 ; 
       static-t2d12.2-0 ; 
       static-t2d13.2-0 ; 
       static-t2d14.2-0 ; 
       static-t2d15.2-0 ; 
       static-t2d16.2-0 ; 
       static-t2d17.2-0 ; 
       static-t2d18.2-0 ; 
       static-t2d19.2-0 ; 
       static-t2d2.2-0 ; 
       static-t2d20.2-0 ; 
       static-t2d21.2-0 ; 
       static-t2d22.2-0 ; 
       static-t2d23.2-0 ; 
       static-t2d24.2-0 ; 
       static-t2d25.2-0 ; 
       static-t2d26.2-0 ; 
       static-t2d27.2-0 ; 
       static-t2d28.2-0 ; 
       static-t2d29.2-0 ; 
       static-t2d3.2-0 ; 
       static-t2d30.2-0 ; 
       static-t2d31.2-0 ; 
       static-t2d32.2-0 ; 
       static-t2d33.2-0 ; 
       static-t2d34.2-0 ; 
       static-t2d35.2-0 ; 
       static-t2d36.2-0 ; 
       static-t2d37.2-0 ; 
       static-t2d39.2-0 ; 
       static-t2d40.2-0 ; 
       static-t2d41.2-0 ; 
       static-t2d42.2-0 ; 
       static-t2d43.2-0 ; 
       static-t2d44.2-0 ; 
       static-t2d45.2-0 ; 
       static-t2d46.2-0 ; 
       static-t2d47.2-0 ; 
       static-t2d48.2-0 ; 
       static-t2d49.2-0 ; 
       static-t2d50.2-0 ; 
       static-t2d51.2-0 ; 
       static-t2d52.2-0 ; 
       static-t2d53.2-0 ; 
       static-t2d54.2-0 ; 
       static-t2d55.2-0 ; 
       static-t2d8.2-0 ; 
       static-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 15 110 ; 
       2 17 110 ; 
       3 18 110 ; 
       4 15 110 ; 
       5 17 110 ; 
       6 18 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 15 110 ; 
       12 46 110 ; 
       13 17 110 ; 
       14 18 110 ; 
       15 50 110 ; 
       16 49 110 ; 
       17 51 110 ; 
       18 52 110 ; 
       19 16 110 ; 
       20 19 110 ; 
       21 16 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 27 110 ; 
       29 16 110 ; 
       30 29 110 ; 
       31 16 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 37 110 ; 
       39 16 110 ; 
       40 16 110 ; 
       41 16 110 ; 
       42 16 110 ; 
       43 16 110 ; 
       44 26 110 ; 
       45 36 110 ; 
       46 20 110 ; 
       47 18 110 ; 
       48 16 110 ; 
       50 16 110 ; 
       51 16 110 ; 
       52 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 42 300 ; 
       7 43 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 39 300 ; 
       9 40 300 ; 
       9 41 300 ; 
       15 0 300 ; 
       15 1 300 ; 
       15 2 300 ; 
       15 3 300 ; 
       15 4 300 ; 
       15 5 300 ; 
       16 6 300 ; 
       16 27 300 ; 
       16 38 300 ; 
       16 58 300 ; 
       16 7 300 ; 
       16 8 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       16 11 300 ; 
       17 46 300 ; 
       17 47 300 ; 
       17 48 300 ; 
       17 49 300 ; 
       17 50 300 ; 
       17 51 300 ; 
       18 52 300 ; 
       18 53 300 ; 
       18 54 300 ; 
       18 55 300 ; 
       18 56 300 ; 
       18 57 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       20 21 300 ; 
       22 36 300 ; 
       23 35 300 ; 
       24 37 300 ; 
       26 15 300 ; 
       26 16 300 ; 
       26 17 300 ; 
       27 29 300 ; 
       28 30 300 ; 
       29 22 300 ; 
       29 23 300 ; 
       29 24 300 ; 
       30 25 300 ; 
       32 33 300 ; 
       33 32 300 ; 
       34 34 300 ; 
       36 12 300 ; 
       36 13 300 ; 
       36 14 300 ; 
       37 26 300 ; 
       38 28 300 ; 
       39 59 300 ; 
       40 62 300 ; 
       41 61 300 ; 
       42 63 300 ; 
       43 60 300 ; 
       44 64 300 ; 
       45 65 300 ; 
       46 31 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 42 400 ; 
       16 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 54 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 16 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 27 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
       47 43 401 ; 
       48 44 401 ; 
       49 45 401 ; 
       50 46 401 ; 
       51 47 401 ; 
       53 48 401 ; 
       54 49 401 ; 
       55 50 401 ; 
       56 51 401 ; 
       57 52 401 ; 
       58 53 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 87.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 90 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 92.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 95 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -4 0 MPRFLG 0 ; 
       1 SCHEM 60 -8 0 MPRFLG 0 ; 
       2 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 75 -8 0 MPRFLG 0 ; 
       4 SCHEM 55 -8 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 70 -8 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       10 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 65 -8 0 MPRFLG 0 ; 
       14 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 MPRFLG 0 ; 
       18 SCHEM 73.75 -6 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 5 -4 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 5 -6 0 MPRFLG 0 ; 
       25 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 35 -4 0 MPRFLG 0 ; 
       28 SCHEM 35 -6 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 45 -6 0 MPRFLG 0 ; 
       33 SCHEM 40 -6 0 MPRFLG 0 ; 
       34 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 50 -4 0 MPRFLG 0 ; 
       38 SCHEM 50 -6 0 MPRFLG 0 ; 
       39 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 10 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 12.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 15 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 17.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 32.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 47.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       47 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       49 SCHEM 43.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       50 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       51 SCHEM 65 -4 0 MPRFLG 0 ; 
       52 SCHEM 73.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 79 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 61.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 86.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 69 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 79 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 79 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 79 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 79 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 79 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
