SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.11-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       cargoclose_F-mat1.3-0 ; 
       cargoclose_F-mat2.3-0 ; 
       cargoclose_F-mat3.3-0 ; 
       cargoclose_F-mat4.3-0 ; 
       cargoclose_F-mat5.3-0 ; 
       cargoclose_F-mat6.3-0 ; 
       static-mat1.2-0 ; 
       static-mat10.2-0 ; 
       static-mat11.2-0 ; 
       static-mat12.2-0 ; 
       static-mat13.2-0 ; 
       static-mat14.2-0 ; 
       static-mat18.2-0 ; 
       static-mat19.2-0 ; 
       static-mat20.2-0 ; 
       static-mat21.2-0 ; 
       static-mat22.2-0 ; 
       static-mat23.2-0 ; 
       static-mat24.2-0 ; 
       static-mat25.2-0 ; 
       static-mat26.2-0 ; 
       static-mat27.2-0 ; 
       static-mat28.2-0 ; 
       static-mat3.2-0 ; 
       static-mat33.2-0 ; 
       static-mat4.2-0 ; 
       static-mat40.2-0 ; 
       static-mat47.2-0 ; 
       static-mat48.2-0 ; 
       static-mat49.2-0 ; 
       static-mat50.2-0 ; 
       static-mat51.2-0 ; 
       static-mat52.2-0 ; 
       static-mat53.2-0 ; 
       static-mat54.2-0 ; 
       static-mat55.2-0 ; 
       static-mat56.2-0 ; 
       static-mat57.2-0 ; 
       static-mat58.2-0 ; 
       static-mat59.2-0 ; 
       static-mat60.2-0 ; 
       static-mat61.2-0 ; 
       static-mat62.2-0 ; 
       static-mat63.2-0 ; 
       static-mat64.2-0 ; 
       static-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       utl11-blthrust.2-0 ; 
       utl11-brthrust.5-0 ; 
       utl11-bthrust0.1-0 ; 
       utl11-fuselg.3-0 ; 
       utl11-fuselg1.3-0 ; 
       utl11-fuselg2.1-0 ; 
       utl11-fuselg3.1-0 ; 
       utl11-lbtractr1.1-0 ; 
       utl11-lbtractr2.1-0 ; 
       utl11-ltthrust.2-0 ; 
       utl11-rbtractr1.1-0 ; 
       utl11-rbtractr2.1-0 ; 
       utl11-tractr.1-0 ; 
       utl11-utl11.9-0 ROOT ; 
       utl11-utl15a.2-0 ; 
       utl11-utl15a1.1-0 ; 
       utl11-utl15a2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl11/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl11-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       cargoclose_F-t2d1.3-0 ; 
       cargoclose_F-t2d2.3-0 ; 
       cargoclose_F-t2d3.3-0 ; 
       cargoclose_F-t2d4.3-0 ; 
       cargoclose_F-t2d6.3-0 ; 
       static-t2d1.2-0 ; 
       static-t2d10.2-0 ; 
       static-t2d11.2-0 ; 
       static-t2d12.2-0 ; 
       static-t2d13.2-0 ; 
       static-t2d14.2-0 ; 
       static-t2d17.2-0 ; 
       static-t2d18.2-0 ; 
       static-t2d19.2-0 ; 
       static-t2d2.2-0 ; 
       static-t2d20.2-0 ; 
       static-t2d21.2-0 ; 
       static-t2d22.2-0 ; 
       static-t2d23.2-0 ; 
       static-t2d24.2-0 ; 
       static-t2d25.2-0 ; 
       static-t2d26.2-0 ; 
       static-t2d3.2-0 ; 
       static-t2d31.2-0 ; 
       static-t2d39.2-0 ; 
       static-t2d40.2-0 ; 
       static-t2d41.2-0 ; 
       static-t2d42.2-0 ; 
       static-t2d43.2-0 ; 
       static-t2d44.2-0 ; 
       static-t2d45.2-0 ; 
       static-t2d46.2-0 ; 
       static-t2d47.2-0 ; 
       static-t2d48.2-0 ; 
       static-t2d49.2-0 ; 
       static-t2d50.2-0 ; 
       static-t2d51.2-0 ; 
       static-t2d52.2-0 ; 
       static-t2d53.2-0 ; 
       static-t2d54.2-0 ; 
       static-t2d55.2-0 ; 
       static-t2d8.2-0 ; 
       static-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 4 110 ; 
       3 14 110 ; 
       4 13 110 ; 
       5 15 110 ; 
       6 16 110 ; 
       7 4 110 ; 
       8 7 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       12 8 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 29 300 ; 
       0 30 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       4 23 300 ; 
       4 25 300 ; 
       4 45 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       5 38 300 ; 
       6 39 300 ; 
       6 40 300 ; 
       6 41 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       8 18 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       11 22 300 ; 
       12 24 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 30 400 ; 
       4 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 42 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 14 401 ; 
       24 23 401 ; 
       25 22 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       40 36 401 ; 
       41 37 401 ; 
       42 38 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       45 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -4 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
