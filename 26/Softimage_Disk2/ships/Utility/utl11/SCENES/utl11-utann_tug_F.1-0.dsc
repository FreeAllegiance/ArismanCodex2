SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl11-utl11.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utann_tug_F-cam_int1.1-0 ROOT ; 
       utann_tug_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       cargo-light7_3_1_1.1-0 ROOT ; 
       cargo-light7_3_1_1_1.1-0 ROOT ; 
       cargo-light8_3_1_1.1-0 ROOT ; 
       cargo-light8_3_1_1_1.1-0 ROOT ; 
       cargo-light9_3_1_1.1-0 ROOT ; 
       cargo-light9_3_1_1_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       utann_tug_F-mat1.1-0 ; 
       utann_tug_F-mat10.1-0 ; 
       utann_tug_F-mat11.1-0 ; 
       utann_tug_F-mat12.1-0 ; 
       utann_tug_F-mat13.1-0 ; 
       utann_tug_F-mat14.1-0 ; 
       utann_tug_F-mat15.1-0 ; 
       utann_tug_F-mat16.1-0 ; 
       utann_tug_F-mat17.1-0 ; 
       utann_tug_F-mat18.1-0 ; 
       utann_tug_F-mat19.1-0 ; 
       utann_tug_F-mat20.1-0 ; 
       utann_tug_F-mat21.1-0 ; 
       utann_tug_F-mat22.1-0 ; 
       utann_tug_F-mat23.1-0 ; 
       utann_tug_F-mat24.1-0 ; 
       utann_tug_F-mat25.1-0 ; 
       utann_tug_F-mat26.1-0 ; 
       utann_tug_F-mat27.1-0 ; 
       utann_tug_F-mat28.1-0 ; 
       utann_tug_F-mat29.1-0 ; 
       utann_tug_F-mat3.1-0 ; 
       utann_tug_F-mat30.1-0 ; 
       utann_tug_F-mat31.1-0 ; 
       utann_tug_F-mat32.1-0 ; 
       utann_tug_F-mat33.1-0 ; 
       utann_tug_F-mat34.1-0 ; 
       utann_tug_F-mat35.1-0 ; 
       utann_tug_F-mat36.1-0 ; 
       utann_tug_F-mat37.1-0 ; 
       utann_tug_F-mat38.1-0 ; 
       utann_tug_F-mat39.1-0 ; 
       utann_tug_F-mat4.1-0 ; 
       utann_tug_F-mat40.1-0 ; 
       utann_tug_F-mat47.1-0 ; 
       utann_tug_F-mat48.1-0 ; 
       utann_tug_F-mat49.1-0 ; 
       utann_tug_F-mat50.1-0 ; 
       utann_tug_F-mat51.1-0 ; 
       utann_tug_F-mat52.1-0 ; 
       utann_tug_F-mat9.1-0 ; 
       utann_tug_F-nose_white-center.1-0.1-0 ; 
       utann_tug_F-nose_white-center.1-1.1-0 ; 
       utann_tug_F-nose_white-center.1-2.1-0 ; 
       utann_tug_F-nose_white-center.1-3.1-0 ; 
       utann_tug_F-nose_white-center.1-4.1-0 ; 
       utann_tug_F-port_red-left.1-0.1-0 ; 
       utann_tug_F-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       utl11-acrgatt.1-0 ; 
       utl11-blthrust.2-0 ; 
       utl11-brthrust.5-0 ; 
       utl11-bthrust0.1-0 ; 
       utl11-fcrgatt.1-0 ; 
       utl11-fuselg1.3-0 ; 
       utl11-lbtractr1.1-0 ; 
       utl11-lbtractr2.1-0 ; 
       utl11-lstrake0.1-0 ; 
       utl11-lstrake1.1-0 ; 
       utl11-lstrake2.1-0 ; 
       utl11-lstrake3.1-0 ; 
       utl11-ltthrust.2-0 ; 
       utl11-lttractr1.1-0 ; 
       utl11-lttractr2.1-0 ; 
       utl11-rbtractr1.1-0 ; 
       utl11-rbtractr2.1-0 ; 
       utl11-rstrake0.1-0 ; 
       utl11-rstrake1.1-0 ; 
       utl11-rstrake2.1-0 ; 
       utl11-rstrake3.1-0 ; 
       utl11-rtthrust.2-0 ; 
       utl11-rttractr1.1-0 ; 
       utl11-rttractr2.1-0 ; 
       utl11-SSa1.1-0 ; 
       utl11-SSa2.1-0 ; 
       utl11-SSa3.1-0 ; 
       utl11-SSa4.1-0 ; 
       utl11-SSf.1-0 ; 
       utl11-SSl.1-0 ; 
       utl11-SSr.1-0 ; 
       utl11-tractr.1-0 ; 
       utl11-turatt.1-0 ; 
       utl11-utl11.1-0 ROOT ; 
       utl11-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl11/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl11-utann_tug_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       utann_tug_F-t2d1.1-0 ; 
       utann_tug_F-t2d10.1-0 ; 
       utann_tug_F-t2d11.1-0 ; 
       utann_tug_F-t2d12.1-0 ; 
       utann_tug_F-t2d13.1-0 ; 
       utann_tug_F-t2d14.1-0 ; 
       utann_tug_F-t2d15.1-0 ; 
       utann_tug_F-t2d16.1-0 ; 
       utann_tug_F-t2d17.1-0 ; 
       utann_tug_F-t2d18.1-0 ; 
       utann_tug_F-t2d19.1-0 ; 
       utann_tug_F-t2d2.1-0 ; 
       utann_tug_F-t2d20.1-0 ; 
       utann_tug_F-t2d21.1-0 ; 
       utann_tug_F-t2d22.1-0 ; 
       utann_tug_F-t2d23.1-0 ; 
       utann_tug_F-t2d24.1-0 ; 
       utann_tug_F-t2d25.1-0 ; 
       utann_tug_F-t2d26.1-0 ; 
       utann_tug_F-t2d27.1-0 ; 
       utann_tug_F-t2d28.1-0 ; 
       utann_tug_F-t2d29.1-0 ; 
       utann_tug_F-t2d3.1-0 ; 
       utann_tug_F-t2d30.1-0 ; 
       utann_tug_F-t2d31.1-0 ; 
       utann_tug_F-t2d32.1-0 ; 
       utann_tug_F-t2d33.1-0 ; 
       utann_tug_F-t2d34.1-0 ; 
       utann_tug_F-t2d35.1-0 ; 
       utann_tug_F-t2d36.1-0 ; 
       utann_tug_F-t2d37.1-0 ; 
       utann_tug_F-t2d39.1-0 ; 
       utann_tug_F-t2d40.1-0 ; 
       utann_tug_F-t2d41.1-0 ; 
       utann_tug_F-t2d42.1-0 ; 
       utann_tug_F-t2d43.1-0 ; 
       utann_tug_F-t2d44.1-0 ; 
       utann_tug_F-t2d45.1-0 ; 
       utann_tug_F-t2d8.1-0 ; 
       utann_tug_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       24 5 110 ; 
       25 5 110 ; 
       26 5 110 ; 
       27 5 110 ; 
       28 5 110 ; 
       29 12 110 ; 
       30 21 110 ; 
       0 5 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 5 110 ; 
       4 31 110 ; 
       5 33 110 ; 
       6 5 110 ; 
       7 6 110 ; 
       8 5 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 13 110 ; 
       15 5 110 ; 
       16 15 110 ; 
       17 5 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 22 110 ; 
       31 7 110 ; 
       32 5 110 ; 
       34 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       24 41 300 ; 
       25 44 300 ; 
       26 43 300 ; 
       27 45 300 ; 
       28 42 300 ; 
       29 46 300 ; 
       30 47 300 ; 
       1 36 300 ; 
       1 37 300 ; 
       2 38 300 ; 
       2 39 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       5 0 300 ; 
       5 21 300 ; 
       5 32 300 ; 
       5 40 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       9 30 300 ; 
       10 29 300 ; 
       11 31 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       13 23 300 ; 
       14 24 300 ; 
       15 16 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       16 19 300 ; 
       18 27 300 ; 
       19 26 300 ; 
       20 28 300 ; 
       21 6 300 ; 
       21 7 300 ; 
       21 8 300 ; 
       22 20 300 ; 
       23 22 300 ; 
       31 25 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 37 400 ; 
       5 5 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       33 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 39 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 11 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 22 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 36 401 ; 
       40 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM -4.44967 -2.054789 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM -4.44967 -4.054789 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM -4.44967 -6.054789 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM -4.44967 -8.054789 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM -4.44967 -10.05479 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM -4.44967 -12.05479 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       24 SCHEM 13 -36.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 13 -38.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 13 -34.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 13 -40.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 13 -32.5 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 16.5 -2.5 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 16.5 -22.5 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 13 -42.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 16.5 -30.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 16.5 -28.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 13 -29.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 23.5 -6.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 9.5 -21.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 13 -6.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 16.5 -6.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 13 -10.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.5 -8.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 16.5 -10.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 16.5 -12.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 13 -2.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 13 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 16.5 -4.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 13 -0.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 16.5 -0.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 13 -16.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 16.5 -14.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 16.5 -16.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 16.5 -18.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 13 -22.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 13 -24.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 16.5 -24.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 20 -6.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 13 -20.5 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 6 -21.5 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 13 -26.5 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -20.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -0.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -22.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -24.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 20 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 23.5 -4.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20 -16.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20 -14.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20 -18.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20 -10.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20 -8.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 20 -12.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -26.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 20 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 20 -30.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 20 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 20 -28.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 13 1.25 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -36.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -32.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -34.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -38.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -40.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 20 -2.75 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 20 -22.75 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 13 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -20.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 23.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 23.5 -0.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 20 -22.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 23.5 -24.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 20 -2.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 23.5 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 27 -4.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 23.5 -16.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 23.5 -14.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 23.5 -18.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 23.5 -10.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 23.5 -8.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 23.5 -12.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 20 -26.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 20 -26.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 23.5 -30.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 23.5 -30.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 23.5 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 23.5 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 20 -28.75 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 1.25 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9.5 1.25 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
