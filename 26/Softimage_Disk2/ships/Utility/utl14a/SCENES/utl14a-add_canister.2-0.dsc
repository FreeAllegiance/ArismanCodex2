SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl14a-utl14a.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.3-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       cargo_scaled-inf_light1.2-0 ROOT ; 
       cargo_scaled-inf_light2.2-0 ROOT ; 
       plan_surv_vesR6-light3_2_1_2_1.3-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_2_1.3-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_2_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       add_canister-mat1.1-0 ; 
       add_canister-mat10.1-0 ; 
       add_canister-mat11.1-0 ; 
       add_canister-mat2.1-0 ; 
       add_canister-mat215.1-0 ; 
       add_canister-mat216.1-0 ; 
       add_canister-mat217.1-0 ; 
       add_canister-mat218.1-0 ; 
       add_canister-mat219.1-0 ; 
       add_canister-mat220.1-0 ; 
       add_canister-mat221.1-0 ; 
       add_canister-mat222.1-0 ; 
       add_canister-mat223.1-0 ; 
       add_canister-mat224.1-0 ; 
       add_canister-mat225.1-0 ; 
       add_canister-mat226.1-0 ; 
       add_canister-mat227.1-0 ; 
       add_canister-mat26.1-0 ; 
       add_canister-mat27.1-0 ; 
       add_canister-mat28.1-0 ; 
       add_canister-mat29.1-0 ; 
       add_canister-mat3.1-0 ; 
       add_canister-mat30.1-0 ; 
       add_canister-mat31.1-0 ; 
       add_canister-mat32.1-0 ; 
       add_canister-mat33.1-0 ; 
       add_canister-mat34.1-0 ; 
       add_canister-mat35.1-0 ; 
       add_canister-mat36.1-0 ; 
       add_canister-mat37.1-0 ; 
       add_canister-mat38.1-0 ; 
       add_canister-mat4.1-0 ; 
       add_canister-mat5.1-0 ; 
       add_canister-mat6.1-0 ; 
       add_canister-mat7.1-0 ; 
       add_canister-mat78.1-0 ; 
       add_canister-mat79.1-0 ; 
       add_canister-mat8.1-0 ; 
       add_canister-mat80.1-0 ; 
       add_canister-mat81.1-0 ; 
       add_canister-mat82.1-0 ; 
       add_canister-mat83.1-0 ; 
       add_canister-mat9.1-0 ; 
       add_canister-mat94.1-0 ; 
       add_canister-mat95.1-0 ; 
       add_canister-port_red-left.1-1.1-0 ; 
       add_canister-SS-strobe_yellow.1-0.1-0 ; 
       add_canister-starbord_green-right.1-1.1-0 ; 
       add_canister-strobe_yellow.1-0 ; 
       cargoclose_sPTL-mat1.1-0 ; 
       cargoclose_sPTL-mat2.1-0 ; 
       cargoclose_sPTL-mat3.1-0 ; 
       cargoclose_sPTL-mat4.1-0 ; 
       cargoclose_sPTL-mat5.1-0 ; 
       cargoclose_sPTL-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       utl14a-crgatt.1-0 ; 
       utl14a-fuselg.1-0 ; 
       utl14a-generic_container_close.2-0 ; 
       utl14a-generic_container_close1.1-0 ; 
       utl14a-generic_container_close2.1-0 ; 
       utl14a-lengine0.1-0 ; 
       utl14a-lengine2.1-0 ; 
       utl14a-lenging1.1-0 ; 
       utl14a-rengine0.1-0 ; 
       utl14a-rengine1.1-0 ; 
       utl14a-rengine2.1-0 ; 
       utl14a-SS0.2-0 ; 
       utl14a-SSa.1-0 ; 
       utl14a-SSf.1-0 ; 
       utl14a-SSl.1-0 ; 
       utl14a-SSr.1-0 ; 
       utl14a-tractr0.1-0 ; 
       utl14a-tractr1.1-0 ; 
       utl14a-turatt.1-0 ; 
       utl14a-utl14a.3-0 ROOT ; 
       utl14a-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14a/PICTURES/utl14a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14a-add_canister.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       add_canister-t2d1.1-0 ; 
       add_canister-t2d10.1-0 ; 
       add_canister-t2d2.1-0 ; 
       add_canister-t2d203.1-0 ; 
       add_canister-t2d204.1-0 ; 
       add_canister-t2d205.1-0 ; 
       add_canister-t2d206.1-0 ; 
       add_canister-t2d207.1-0 ; 
       add_canister-t2d208.1-0 ; 
       add_canister-t2d209.1-0 ; 
       add_canister-t2d210.1-0 ; 
       add_canister-t2d211.1-0 ; 
       add_canister-t2d212.1-0 ; 
       add_canister-t2d213.1-0 ; 
       add_canister-t2d24.1-0 ; 
       add_canister-t2d25.1-0 ; 
       add_canister-t2d26.1-0 ; 
       add_canister-t2d27.1-0 ; 
       add_canister-t2d28.1-0 ; 
       add_canister-t2d29.1-0 ; 
       add_canister-t2d3.1-0 ; 
       add_canister-t2d30.1-0 ; 
       add_canister-t2d31.1-0 ; 
       add_canister-t2d32.1-0 ; 
       add_canister-t2d33.1-0 ; 
       add_canister-t2d34.1-0 ; 
       add_canister-t2d35.1-0 ; 
       add_canister-t2d4.1-0 ; 
       add_canister-t2d5.1-0 ; 
       add_canister-t2d6.1-0 ; 
       add_canister-t2d7.1-0 ; 
       add_canister-t2d75.1-0 ; 
       add_canister-t2d76.1-0 ; 
       add_canister-t2d77.1-0 ; 
       add_canister-t2d78.1-0 ; 
       add_canister-t2d79.1-0 ; 
       add_canister-t2d8.1-0 ; 
       add_canister-t2d80.1-0 ; 
       add_canister-t2d9.1-0 ; 
       add_canister-t2d90.1-0 ; 
       add_canister-t2d91.1-0 ; 
       cargoclose_sPTL-t2d1.2-0 ; 
       cargoclose_sPTL-t2d2.2-0 ; 
       cargoclose_sPTL-t2d3.2-0 ; 
       cargoclose_sPTL-t2d4.2-0 ; 
       cargoclose_sPTL-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 19 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 1 110 ; 
       17 16 110 ; 
       18 1 110 ; 
       20 1 110 ; 
       2 17 110 ; 
       3 2 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 3 300 ; 
       1 21 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       1 37 300 ; 
       1 42 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       1 38 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       1 41 300 ; 
       1 4 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       10 24 300 ; 
       10 25 300 ; 
       10 26 300 ; 
       12 46 300 ; 
       13 48 300 ; 
       14 45 300 ; 
       15 47 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 22 300 ; 
       17 23 300 ; 
       2 49 300 ; 
       2 50 300 ; 
       2 51 300 ; 
       2 52 300 ; 
       2 53 300 ; 
       2 54 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       19 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 2 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 26 401 ; 
       31 20 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       37 30 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 37 401 ; 
       42 36 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       50 41 401 ; 
       51 42 401 ; 
       52 43 401 ; 
       53 44 401 ; 
       54 45 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 10 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -12 0 MPRFLG 0 ; 
       1 SCHEM 56.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       7 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 40 -10 0 MPRFLG 0 ; 
       11 SCHEM 63.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 67.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 60 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 65 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 20 -8 0 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 MPRFLG 0 ; 
       18 SCHEM 0 -8 0 MPRFLG 0 ; 
       19 SCHEM 56.25 -4 0 SRT 1 1 1 0 0 0 0 -6.631132 108.8117 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -12 0 MPRFLG 0 ; 
       3 SCHEM 117.5 0 0 MPRFLG 0 ; 
       4 SCHEM 147.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 123.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 111.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 113.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 116.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 118.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 121.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 153.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 141.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 143.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 146.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 148.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 151.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 10 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 15 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 111.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 17.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 113.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 116.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 118.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 121.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 141.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 143.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 146.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 148.75 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 151.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 114 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
