SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl14a-utl14a.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.10-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       cargo_scaled-inf_light1.9-0 ROOT ; 
       cargo_scaled-inf_light2.9-0 ROOT ; 
       plan_surv_vesR6-light3_2_1_2_1.10-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_2_1.10-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_2_1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       add_frame-mat1.1-0 ; 
       add_frame-mat10.1-0 ; 
       add_frame-mat11.1-0 ; 
       add_frame-mat2.1-0 ; 
       add_frame-mat215.1-0 ; 
       add_frame-mat216.1-0 ; 
       add_frame-mat217.1-0 ; 
       add_frame-mat218.1-0 ; 
       add_frame-mat219.1-0 ; 
       add_frame-mat220.1-0 ; 
       add_frame-mat221.1-0 ; 
       add_frame-mat222.1-0 ; 
       add_frame-mat223.1-0 ; 
       add_frame-mat224.1-0 ; 
       add_frame-mat225.1-0 ; 
       add_frame-mat226.1-0 ; 
       add_frame-mat227.1-0 ; 
       add_frame-mat26.1-0 ; 
       add_frame-mat27.1-0 ; 
       add_frame-mat28.1-0 ; 
       add_frame-mat29.1-0 ; 
       add_frame-mat3.1-0 ; 
       add_frame-mat30.1-0 ; 
       add_frame-mat31.1-0 ; 
       add_frame-mat32.1-0 ; 
       add_frame-mat33.1-0 ; 
       add_frame-mat34.1-0 ; 
       add_frame-mat35.1-0 ; 
       add_frame-mat36.1-0 ; 
       add_frame-mat37.1-0 ; 
       add_frame-mat38.1-0 ; 
       add_frame-mat4.1-0 ; 
       add_frame-mat5.1-0 ; 
       add_frame-mat6.1-0 ; 
       add_frame-mat7.1-0 ; 
       add_frame-mat78.1-0 ; 
       add_frame-mat79.1-0 ; 
       add_frame-mat8.1-0 ; 
       add_frame-mat80.1-0 ; 
       add_frame-mat81.1-0 ; 
       add_frame-mat82.1-0 ; 
       add_frame-mat83.1-0 ; 
       add_frame-mat9.1-0 ; 
       add_frame-mat94.1-0 ; 
       add_frame-mat95.1-0 ; 
       add_frame-port_red-left.1-1.1-0 ; 
       add_frame-SS-strobe_yellow.1-0.1-0 ; 
       add_frame-starbord_green-right.1-1.1-0 ; 
       add_frame-strobe_yellow.1-0 ; 
       cargoclose_sPTL-mat1.1-0 ; 
       cargoclose_sPTL-mat2.1-0 ; 
       cargoclose_sPTL-mat3.1-0 ; 
       cargoclose_sPTL-mat4.1-0 ; 
       cargoclose_sPTL-mat5.1-0 ; 
       cargoclose_sPTL-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       utl14a-crgatt.1-0 ; 
       utl14a-fuselg.1-0 ; 
       utl14a-generic_container_close.2-0 ; 
       utl14a-generic_container_close1.1-0 ; 
       utl14a-generic_container_close2.1-0 ; 
       utl14a-lengine0.1-0 ; 
       utl14a-lengine2.1-0 ; 
       utl14a-lenging1.1-0 ; 
       utl14a-lsmoke.1-0 ; 
       utl14a-lthrust1.1-0 ; 
       utl14a-lthrust2.1-0 ; 
       utl14a-rengine0.1-0 ; 
       utl14a-rengine1.1-0 ; 
       utl14a-rengine2.1-0 ; 
       utl14a-rsmoke.1-0 ; 
       utl14a-rthrust1.1-0 ; 
       utl14a-rthrust2.1-0 ; 
       utl14a-SS0.2-0 ; 
       utl14a-SSa.1-0 ; 
       utl14a-SSf.1-0 ; 
       utl14a-SSl.1-0 ; 
       utl14a-SSr.1-0 ; 
       utl14a-tractr0.1-0 ; 
       utl14a-tractr1.1-0 ; 
       utl14a-trail.1-0 ; 
       utl14a-turatt.1-0 ; 
       utl14a-utl14a.7-0 ROOT ; 
       utl14a-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14a/PICTURES/utl14a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14a-add_frame.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       add_frame-t2d1.1-0 ; 
       add_frame-t2d10.1-0 ; 
       add_frame-t2d2.1-0 ; 
       add_frame-t2d203.1-0 ; 
       add_frame-t2d204.1-0 ; 
       add_frame-t2d205.1-0 ; 
       add_frame-t2d206.1-0 ; 
       add_frame-t2d207.1-0 ; 
       add_frame-t2d208.1-0 ; 
       add_frame-t2d209.1-0 ; 
       add_frame-t2d210.1-0 ; 
       add_frame-t2d211.1-0 ; 
       add_frame-t2d212.1-0 ; 
       add_frame-t2d213.1-0 ; 
       add_frame-t2d24.1-0 ; 
       add_frame-t2d25.1-0 ; 
       add_frame-t2d26.1-0 ; 
       add_frame-t2d27.1-0 ; 
       add_frame-t2d28.1-0 ; 
       add_frame-t2d29.1-0 ; 
       add_frame-t2d3.1-0 ; 
       add_frame-t2d30.1-0 ; 
       add_frame-t2d31.1-0 ; 
       add_frame-t2d32.1-0 ; 
       add_frame-t2d33.1-0 ; 
       add_frame-t2d34.1-0 ; 
       add_frame-t2d35.1-0 ; 
       add_frame-t2d4.1-0 ; 
       add_frame-t2d5.1-0 ; 
       add_frame-t2d6.1-0 ; 
       add_frame-t2d7.1-0 ; 
       add_frame-t2d75.1-0 ; 
       add_frame-t2d76.1-0 ; 
       add_frame-t2d77.1-0 ; 
       add_frame-t2d78.1-0 ; 
       add_frame-t2d79.1-0 ; 
       add_frame-t2d8.1-0 ; 
       add_frame-t2d80.1-0 ; 
       add_frame-t2d9.1-0 ; 
       add_frame-t2d90.1-0 ; 
       add_frame-t2d91.1-0 ; 
       cargoclose_sPTL-t2d1.2-0 ; 
       cargoclose_sPTL-t2d2.2-0 ; 
       cargoclose_sPTL-t2d3.2-0 ; 
       cargoclose_sPTL-t2d4.2-0 ; 
       cargoclose_sPTL-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 23 110 ; 
       1 26 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       17 1 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 1 110 ; 
       23 22 110 ; 
       25 1 110 ; 
       27 1 110 ; 
       2 23 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       24 26 110 ; 
       15 26 110 ; 
       16 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       8 26 110 ; 
       14 26 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 3 300 ; 
       1 21 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       1 37 300 ; 
       1 42 300 ; 
       1 2 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       1 38 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       1 41 300 ; 
       1 4 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       13 24 300 ; 
       13 25 300 ; 
       13 26 300 ; 
       18 46 300 ; 
       19 48 300 ; 
       20 45 300 ; 
       21 47 300 ; 
       23 17 300 ; 
       23 18 300 ; 
       23 19 300 ; 
       23 20 300 ; 
       23 22 300 ; 
       23 23 300 ; 
       2 49 300 ; 
       2 50 300 ; 
       2 51 300 ; 
       2 52 300 ; 
       2 53 300 ; 
       2 54 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       26 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 38 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 2 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 23 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 26 401 ; 
       31 20 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 31 401 ; 
       36 32 401 ; 
       37 30 401 ; 
       38 33 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 37 401 ; 
       42 36 401 ; 
       43 39 401 ; 
       44 40 401 ; 
       50 41 401 ; 
       51 42 401 ; 
       52 43 401 ; 
       53 44 401 ; 
       54 45 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 25 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 27.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       23 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       25 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 MPRFLG 0 ; 
       4 SCHEM 10 -12 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 2.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
