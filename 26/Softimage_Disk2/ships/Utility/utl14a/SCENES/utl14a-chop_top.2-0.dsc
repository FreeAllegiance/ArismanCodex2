SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl14a-utl14a.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.8-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       cargo_scaled-inf_light1.7-0 ROOT ; 
       cargo_scaled-inf_light2.7-0 ROOT ; 
       plan_surv_vesR6-light3_2_1_2_1.8-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_2_1.8-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_2_1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       cargoclose_sPTL-mat1.1-0 ; 
       cargoclose_sPTL-mat2.1-0 ; 
       cargoclose_sPTL-mat3.1-0 ; 
       cargoclose_sPTL-mat4.1-0 ; 
       cargoclose_sPTL-mat5.1-0 ; 
       cargoclose_sPTL-mat6.1-0 ; 
       chop_top-mat1.1-0 ; 
       chop_top-mat10.1-0 ; 
       chop_top-mat11.1-0 ; 
       chop_top-mat2.1-0 ; 
       chop_top-mat215.1-0 ; 
       chop_top-mat216.1-0 ; 
       chop_top-mat217.1-0 ; 
       chop_top-mat218.1-0 ; 
       chop_top-mat219.1-0 ; 
       chop_top-mat220.1-0 ; 
       chop_top-mat221.1-0 ; 
       chop_top-mat222.1-0 ; 
       chop_top-mat223.1-0 ; 
       chop_top-mat224.1-0 ; 
       chop_top-mat225.1-0 ; 
       chop_top-mat226.1-0 ; 
       chop_top-mat227.1-0 ; 
       chop_top-mat26.1-0 ; 
       chop_top-mat27.1-0 ; 
       chop_top-mat28.1-0 ; 
       chop_top-mat29.1-0 ; 
       chop_top-mat3.1-0 ; 
       chop_top-mat30.1-0 ; 
       chop_top-mat31.1-0 ; 
       chop_top-mat32.1-0 ; 
       chop_top-mat33.1-0 ; 
       chop_top-mat34.1-0 ; 
       chop_top-mat35.1-0 ; 
       chop_top-mat36.1-0 ; 
       chop_top-mat37.1-0 ; 
       chop_top-mat38.1-0 ; 
       chop_top-mat4.1-0 ; 
       chop_top-mat5.1-0 ; 
       chop_top-mat6.1-0 ; 
       chop_top-mat7.1-0 ; 
       chop_top-mat78.1-0 ; 
       chop_top-mat79.1-0 ; 
       chop_top-mat8.1-0 ; 
       chop_top-mat80.1-0 ; 
       chop_top-mat81.1-0 ; 
       chop_top-mat82.1-0 ; 
       chop_top-mat83.1-0 ; 
       chop_top-mat9.1-0 ; 
       chop_top-mat94.1-0 ; 
       chop_top-mat95.1-0 ; 
       chop_top-port_red-left.1-1.1-0 ; 
       chop_top-SS-strobe_yellow.1-0.1-0 ; 
       chop_top-starbord_green-right.1-1.1-0 ; 
       chop_top-strobe_yellow.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       utl14a-crgatt.1-0 ; 
       utl14a-fuselg.1-0 ; 
       utl14a-generic_container_close.2-0 ; 
       utl14a-generic_container_close1.1-0 ; 
       utl14a-generic_container_close2.1-0 ; 
       utl14a-lengine0.1-0 ; 
       utl14a-lengine2.1-0 ; 
       utl14a-lenging1.1-0 ; 
       utl14a-rengine0.1-0 ; 
       utl14a-rengine1.1-0 ; 
       utl14a-rengine2.1-0 ; 
       utl14a-SS0.2-0 ; 
       utl14a-SSa.1-0 ; 
       utl14a-SSf.1-0 ; 
       utl14a-SSl.1-0 ; 
       utl14a-SSr.1-0 ; 
       utl14a-tractr0.1-0 ; 
       utl14a-tractr1.1-0 ; 
       utl14a-turatt.1-0 ; 
       utl14a-utl14a.5-0 ROOT ; 
       utl14a-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14a/PICTURES/utl14a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14a-chop_top.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       cargoclose_sPTL-t2d1.2-0 ; 
       cargoclose_sPTL-t2d2.2-0 ; 
       cargoclose_sPTL-t2d3.2-0 ; 
       cargoclose_sPTL-t2d4.2-0 ; 
       cargoclose_sPTL-t2d6.2-0 ; 
       chop_top-t2d1.2-0 ; 
       chop_top-t2d10.2-0 ; 
       chop_top-t2d2.2-0 ; 
       chop_top-t2d203.2-0 ; 
       chop_top-t2d204.1-0 ; 
       chop_top-t2d205.1-0 ; 
       chop_top-t2d206.1-0 ; 
       chop_top-t2d207.1-0 ; 
       chop_top-t2d208.1-0 ; 
       chop_top-t2d209.1-0 ; 
       chop_top-t2d210.1-0 ; 
       chop_top-t2d211.1-0 ; 
       chop_top-t2d212.1-0 ; 
       chop_top-t2d213.1-0 ; 
       chop_top-t2d24.1-0 ; 
       chop_top-t2d25.1-0 ; 
       chop_top-t2d26.1-0 ; 
       chop_top-t2d27.1-0 ; 
       chop_top-t2d28.1-0 ; 
       chop_top-t2d29.1-0 ; 
       chop_top-t2d3.2-0 ; 
       chop_top-t2d30.1-0 ; 
       chop_top-t2d31.1-0 ; 
       chop_top-t2d32.1-0 ; 
       chop_top-t2d33.1-0 ; 
       chop_top-t2d34.1-0 ; 
       chop_top-t2d35.1-0 ; 
       chop_top-t2d4.2-0 ; 
       chop_top-t2d5.2-0 ; 
       chop_top-t2d6.2-0 ; 
       chop_top-t2d7.2-0 ; 
       chop_top-t2d75.2-0 ; 
       chop_top-t2d76.2-0 ; 
       chop_top-t2d77.2-0 ; 
       chop_top-t2d78.2-0 ; 
       chop_top-t2d79.2-0 ; 
       chop_top-t2d8.2-0 ; 
       chop_top-t2d80.2-0 ; 
       chop_top-t2d9.1-0 ; 
       chop_top-t2d90.1-0 ; 
       chop_top-t2d91.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 19 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 1 110 ; 
       17 16 110 ; 
       18 1 110 ; 
       20 1 110 ; 
       2 17 110 ; 
       3 2 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 6 300 ; 
       1 9 300 ; 
       1 27 300 ; 
       1 37 300 ; 
       1 38 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       1 43 300 ; 
       1 48 300 ; 
       1 8 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 10 300 ; 
       6 49 300 ; 
       6 50 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 32 300 ; 
       12 52 300 ; 
       13 54 300 ; 
       14 51 300 ; 
       15 53 300 ; 
       17 23 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       19 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 43 401 ; 
       8 6 401 ; 
       9 5 401 ; 
       10 8 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 7 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 25 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       40 34 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       43 35 401 ; 
       44 38 401 ; 
       45 39 401 ; 
       46 40 401 ; 
       47 42 401 ; 
       48 41 401 ; 
       49 44 401 ; 
       50 45 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 10 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -12 0 MPRFLG 0 ; 
       1 SCHEM 71.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 86.25 -10 0 MPRFLG 0 ; 
       7 SCHEM 81.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 70 -10 0 MPRFLG 0 ; 
       11 SCHEM 93.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 97.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 90 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 95 -10 0 WIRECOL 2 7 MPRFLG 0 ; 
       16 SCHEM 35 -8 0 MPRFLG 0 ; 
       17 SCHEM 35 -10 0 MPRFLG 0 ; 
       18 SCHEM 0 -8 0 MPRFLG 0 ; 
       19 SCHEM 71.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       3 SCHEM 21.25 -14 0 MPRFLG 0 ; 
       4 SCHEM 13.75 -16 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 77.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 132.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 97.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 27.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 15 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 77.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 132.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 40 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -18 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 7.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 10 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 15 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -20 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 144 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
