SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl14a-utl14a.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       plan_surv_vesR6-light3_2_1_2_1.1-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_2_1.1-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       pre_cargo_F-mat1.1-0 ; 
       pre_cargo_F-mat10.1-0 ; 
       pre_cargo_F-mat11.1-0 ; 
       pre_cargo_F-mat2.1-0 ; 
       pre_cargo_F-mat215.1-0 ; 
       pre_cargo_F-mat26.1-0 ; 
       pre_cargo_F-mat27.1-0 ; 
       pre_cargo_F-mat28.1-0 ; 
       pre_cargo_F-mat29.1-0 ; 
       pre_cargo_F-mat3.1-0 ; 
       pre_cargo_F-mat30.1-0 ; 
       pre_cargo_F-mat31.1-0 ; 
       pre_cargo_F-mat32.1-0 ; 
       pre_cargo_F-mat33.1-0 ; 
       pre_cargo_F-mat34.1-0 ; 
       pre_cargo_F-mat35.1-0 ; 
       pre_cargo_F-mat36.1-0 ; 
       pre_cargo_F-mat37.1-0 ; 
       pre_cargo_F-mat38.1-0 ; 
       pre_cargo_F-mat4.1-0 ; 
       pre_cargo_F-mat5.1-0 ; 
       pre_cargo_F-mat6.1-0 ; 
       pre_cargo_F-mat7.1-0 ; 
       pre_cargo_F-mat78.1-0 ; 
       pre_cargo_F-mat79.1-0 ; 
       pre_cargo_F-mat8.1-0 ; 
       pre_cargo_F-mat80.1-0 ; 
       pre_cargo_F-mat81.1-0 ; 
       pre_cargo_F-mat82.1-0 ; 
       pre_cargo_F-mat83.1-0 ; 
       pre_cargo_F-mat9.1-0 ; 
       pre_cargo_F-mat94.1-0 ; 
       pre_cargo_F-mat95.1-0 ; 
       pre_cargo_F-port_red-left.1-1.1-0 ; 
       pre_cargo_F-SS-strobe_yellow.1-0.1-0 ; 
       pre_cargo_F-starbord_green-right.1-1.1-0 ; 
       pre_cargo_F-strobe_yellow.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl14a-crgatt.1-0 ; 
       utl14a-fuselg.1-0 ; 
       utl14a-lengine0.1-0 ; 
       utl14a-lengine2.1-0 ; 
       utl14a-lenging1.1-0 ; 
       utl14a-rengine0.1-0 ; 
       utl14a-rengine1.1-0 ; 
       utl14a-rengine2.1-0 ; 
       utl14a-SS0.2-0 ; 
       utl14a-SSa.1-0 ; 
       utl14a-SSf.1-0 ; 
       utl14a-SSl.1-0 ; 
       utl14a-SSr.1-0 ; 
       utl14a-tractr0.1-0 ; 
       utl14a-tractr1.1-0 ; 
       utl14a-turatt.1-0 ; 
       utl14a-utl14a.1-0 ROOT ; 
       utl14a-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14a/PICTURES/utl14a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14a-pre_cargo_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       pre_cargo_F-t2d1.1-0 ; 
       pre_cargo_F-t2d10.1-0 ; 
       pre_cargo_F-t2d2.1-0 ; 
       pre_cargo_F-t2d203.1-0 ; 
       pre_cargo_F-t2d24.1-0 ; 
       pre_cargo_F-t2d25.1-0 ; 
       pre_cargo_F-t2d26.1-0 ; 
       pre_cargo_F-t2d27.1-0 ; 
       pre_cargo_F-t2d28.1-0 ; 
       pre_cargo_F-t2d29.1-0 ; 
       pre_cargo_F-t2d3.1-0 ; 
       pre_cargo_F-t2d30.1-0 ; 
       pre_cargo_F-t2d31.1-0 ; 
       pre_cargo_F-t2d32.1-0 ; 
       pre_cargo_F-t2d33.1-0 ; 
       pre_cargo_F-t2d34.1-0 ; 
       pre_cargo_F-t2d35.1-0 ; 
       pre_cargo_F-t2d4.1-0 ; 
       pre_cargo_F-t2d5.1-0 ; 
       pre_cargo_F-t2d6.1-0 ; 
       pre_cargo_F-t2d7.1-0 ; 
       pre_cargo_F-t2d75.1-0 ; 
       pre_cargo_F-t2d76.1-0 ; 
       pre_cargo_F-t2d77.1-0 ; 
       pre_cargo_F-t2d78.1-0 ; 
       pre_cargo_F-t2d79.1-0 ; 
       pre_cargo_F-t2d8.1-0 ; 
       pre_cargo_F-t2d80.1-0 ; 
       pre_cargo_F-t2d9.1-0 ; 
       pre_cargo_F-t2d90.1-0 ; 
       pre_cargo_F-t2d91.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       0 14 110 ; 
       1 16 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       13 1 110 ; 
       14 13 110 ; 
       15 1 110 ; 
       17 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 34 300 ; 
       10 36 300 ; 
       11 33 300 ; 
       12 35 300 ; 
       1 0 300 ; 
       1 3 300 ; 
       1 9 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 25 300 ; 
       1 30 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 23 300 ; 
       1 24 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       1 4 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       14 5 300 ; 
       14 6 300 ; 
       14 7 300 ; 
       14 8 300 ; 
       14 10 300 ; 
       14 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       16 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 28 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 2 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 10 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 20 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 27 401 ; 
       30 26 401 ; 
       31 29 401 ; 
       32 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -7.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 7.992793 -11.28998 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 11.49279 -8.28998 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 11.49279 -14.28998 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 11.49279 -10.28998 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 11.49279 -12.28998 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 14.96428 -6.289971 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 4.464282 -6.261463 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.964283 0.7385345 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 11.46428 1.738535 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 11.46428 -0.2614651 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7.964283 -3.289972 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 11.46428 -4.261464 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 11.46428 -2.261465 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 7.964283 -6.261463 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 11.46428 -6.289971 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 7.964282 -15.26146 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 0.9642802 -6.261463 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -6.631132 8.918533 MPRFLG 0 ; 
       17 SCHEM 7.964282 -13.26146 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       34 SCHEM 14.99279 -8.53998 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14.96428 -4.539971 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14.96428 -2.511465 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14.96428 -2.511465 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14.96428 -2.511465 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14.96428 -4.511464 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14.96428 -4.511464 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14.96428 -0.5114651 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14.96428 -0.5114651 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 7.964282 3.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14.96428 1.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14.96428 1.488535 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14.99279 -10.53998 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14.99279 -12.53998 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14.99279 -14.53998 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 18.46428 -4.539971 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 18.46428 -2.511465 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 18.46428 -2.511465 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 18.46428 -4.511464 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 18.46428 -4.511464 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 18.46428 -0.5114651 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 18.46428 -0.5114651 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.46428 3.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 18.46428 1.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 18.46428 1.488535 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 4.46428 5.488537 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
