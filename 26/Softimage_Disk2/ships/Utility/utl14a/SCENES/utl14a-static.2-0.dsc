SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.13-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       cargo_scaled-inf_light1.12-0 ROOT ; 
       cargo_scaled-inf_light2.12-0 ROOT ; 
       plan_surv_vesR6-light3_2_1_2_1.13-0 ROOT ; 
       pre_cargo_ship7-light1_2_1_2_1.13-0 ROOT ; 
       pre_cargo_ship7-light2_2_1_2_1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       cargoclose_sPTL-mat1.1-0 ; 
       cargoclose_sPTL-mat2.1-0 ; 
       cargoclose_sPTL-mat3.1-0 ; 
       cargoclose_sPTL-mat4.1-0 ; 
       cargoclose_sPTL-mat5.1-0 ; 
       cargoclose_sPTL-mat6.1-0 ; 
       static-mat1.1-0 ; 
       static-mat10.1-0 ; 
       static-mat11.1-0 ; 
       static-mat2.1-0 ; 
       static-mat215.1-0 ; 
       static-mat216.1-0 ; 
       static-mat217.1-0 ; 
       static-mat218.1-0 ; 
       static-mat219.1-0 ; 
       static-mat220.1-0 ; 
       static-mat221.1-0 ; 
       static-mat222.1-0 ; 
       static-mat223.1-0 ; 
       static-mat224.1-0 ; 
       static-mat225.1-0 ; 
       static-mat226.1-0 ; 
       static-mat227.1-0 ; 
       static-mat26.1-0 ; 
       static-mat27.1-0 ; 
       static-mat28.1-0 ; 
       static-mat29.1-0 ; 
       static-mat3.1-0 ; 
       static-mat30.1-0 ; 
       static-mat31.1-0 ; 
       static-mat32.1-0 ; 
       static-mat33.1-0 ; 
       static-mat34.1-0 ; 
       static-mat35.1-0 ; 
       static-mat36.1-0 ; 
       static-mat37.1-0 ; 
       static-mat38.1-0 ; 
       static-mat4.1-0 ; 
       static-mat5.1-0 ; 
       static-mat6.1-0 ; 
       static-mat7.1-0 ; 
       static-mat78.1-0 ; 
       static-mat79.1-0 ; 
       static-mat8.1-0 ; 
       static-mat80.1-0 ; 
       static-mat81.1-0 ; 
       static-mat82.1-0 ; 
       static-mat83.1-0 ; 
       static-mat9.1-0 ; 
       static-mat94.1-0 ; 
       static-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       utl14a-crgatt.1-0 ; 
       utl14a-fuselg.1-0 ; 
       utl14a-generic_container_close.2-0 ; 
       utl14a-generic_container_close1.1-0 ; 
       utl14a-generic_container_close2.1-0 ; 
       utl14a-lengine0.1-0 ; 
       utl14a-lengine2.1-0 ; 
       utl14a-lenging1.1-0 ; 
       utl14a-rengine0.1-0 ; 
       utl14a-rengine1.1-0 ; 
       utl14a-rengine2.1-0 ; 
       utl14a-tractr0.1-0 ; 
       utl14a-tractr1.1-0 ; 
       utl14a-utl14a.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14a/PICTURES/utl14a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14a-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       cargoclose_sPTL-t2d1.2-0 ; 
       cargoclose_sPTL-t2d2.2-0 ; 
       cargoclose_sPTL-t2d3.2-0 ; 
       cargoclose_sPTL-t2d4.2-0 ; 
       cargoclose_sPTL-t2d6.2-0 ; 
       static-t2d1.1-0 ; 
       static-t2d10.1-0 ; 
       static-t2d2.1-0 ; 
       static-t2d203.1-0 ; 
       static-t2d204.1-0 ; 
       static-t2d205.1-0 ; 
       static-t2d206.1-0 ; 
       static-t2d207.1-0 ; 
       static-t2d208.1-0 ; 
       static-t2d209.1-0 ; 
       static-t2d210.1-0 ; 
       static-t2d211.1-0 ; 
       static-t2d212.1-0 ; 
       static-t2d213.1-0 ; 
       static-t2d24.1-0 ; 
       static-t2d25.1-0 ; 
       static-t2d26.1-0 ; 
       static-t2d27.1-0 ; 
       static-t2d28.1-0 ; 
       static-t2d29.1-0 ; 
       static-t2d3.1-0 ; 
       static-t2d30.1-0 ; 
       static-t2d31.1-0 ; 
       static-t2d32.1-0 ; 
       static-t2d33.1-0 ; 
       static-t2d34.1-0 ; 
       static-t2d35.1-0 ; 
       static-t2d4.1-0 ; 
       static-t2d5.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d7.1-0 ; 
       static-t2d75.1-0 ; 
       static-t2d76.1-0 ; 
       static-t2d77.1-0 ; 
       static-t2d78.1-0 ; 
       static-t2d79.1-0 ; 
       static-t2d8.1-0 ; 
       static-t2d80.1-0 ; 
       static-t2d9.1-0 ; 
       static-t2d90.1-0 ; 
       static-t2d91.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 13 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 1 110 ; 
       12 11 110 ; 
       2 12 110 ; 
       3 2 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 6 300 ; 
       1 9 300 ; 
       1 27 300 ; 
       1 37 300 ; 
       1 38 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       1 43 300 ; 
       1 48 300 ; 
       1 8 300 ; 
       1 41 300 ; 
       1 42 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 10 300 ; 
       6 49 300 ; 
       6 50 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 32 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 26 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 43 401 ; 
       8 6 401 ; 
       9 5 401 ; 
       10 8 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 21 401 ; 
       26 22 401 ; 
       27 7 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 25 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       40 34 401 ; 
       41 36 401 ; 
       42 37 401 ; 
       43 35 401 ; 
       44 38 401 ; 
       45 39 401 ; 
       46 40 401 ; 
       47 42 401 ; 
       48 41 401 ; 
       49 44 401 ; 
       50 45 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 17 401 ; 
       22 18 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       13 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 MPRFLG 0 ; 
       4 SCHEM 5 -12 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -24 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 2.5 -26 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
