SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl14b-utl14b.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.2-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       plan_surv_vesR6-light3_2_1.2-0 ROOT ; 
       pre_cargo_ship7-light1_2_1.2-0 ROOT ; 
       pre_cargo_ship7-light2_2_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       pre_cargo_F-mat12.2-0 ; 
       pre_cargo_F-mat13.2-0 ; 
       pre_cargo_F-mat14.2-0 ; 
       pre_cargo_F-mat15.2-0 ; 
       pre_cargo_F-mat16.2-0 ; 
       pre_cargo_F-mat17.2-0 ; 
       pre_cargo_F-mat18.2-0 ; 
       pre_cargo_F-mat19.2-0 ; 
       pre_cargo_F-mat20.2-0 ; 
       pre_cargo_F-mat21.2-0 ; 
       pre_cargo_F-mat216.2-0 ; 
       pre_cargo_F-mat22.2-0 ; 
       pre_cargo_F-mat23.2-0 ; 
       pre_cargo_F-mat24.2-0 ; 
       pre_cargo_F-mat25.2-0 ; 
       pre_cargo_F-SS-strobe_blue.1-0.2-0 ; 
       pre_cargo_F-SS-strobe_blue.1-1.2-0 ; 
       pre_cargo_F-SS-strobe_blue.1-2.2-0 ; 
       pre_cargo_F-strobe_blue.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       utl14b-engine1.3-0 ; 
       utl14b-engine2.1-0 ; 
       utl14b-engine3.1-0 ; 
       utl14b-engine4.1-0 ; 
       utl14b-engine5.1-0 ; 
       utl14b-engine6.1-0 ; 
       utl14b-fuselg.1-0 ; 
       utl14b-lturatt.1-0 ; 
       utl14b-rturatt.1-0 ; 
       utl14b-SSa2.1-0 ; 
       utl14b-SSa3.1-0 ; 
       utl14b-SSa4.1-0 ; 
       utl14b-SSa5.1-0 ; 
       utl14b-utl14b.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl14b/PICTURES/utl14b ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl14b-reduce_poly_to_210.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       pre_cargo_F-gt2d1.2-0 ; 
       pre_cargo_F-t2d11.2-0 ; 
       pre_cargo_F-t2d12.2-0 ; 
       pre_cargo_F-t2d13.2-0 ; 
       pre_cargo_F-t2d14.2-0 ; 
       pre_cargo_F-t2d15.2-0 ; 
       pre_cargo_F-t2d16.2-0 ; 
       pre_cargo_F-t2d17.2-0 ; 
       pre_cargo_F-t2d18.2-0 ; 
       pre_cargo_F-t2d19.2-0 ; 
       pre_cargo_F-t2d20.2-0 ; 
       pre_cargo_F-t2d204.2-0 ; 
       pre_cargo_F-t2d21.2-0 ; 
       pre_cargo_F-t2d22.2-0 ; 
       pre_cargo_F-t2d23.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 13 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 3 110 ; 
       10 4 110 ; 
       11 1 110 ; 
       12 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 13 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 2 300 ; 
       2 12 300 ; 
       3 1 300 ; 
       3 11 300 ; 
       4 5 300 ; 
       4 8 300 ; 
       5 3 300 ; 
       5 14 300 ; 
       6 0 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       9 15 300 ; 
       10 18 300 ; 
       11 17 300 ; 
       12 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       13 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
       7 8 401 ; 
       8 9 401 ; 
       9 10 401 ; 
       10 11 401 ; 
       11 0 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 13 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 13 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 13 -4.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -1.397531 -12.54882 0 MPRFLG 0 ; 
       1 SCHEM -1.397531 -10.54882 0 MPRFLG 0 ; 
       2 SCHEM -1.397531 -8.54882 0 MPRFLG 0 ; 
       3 SCHEM -1.397531 -6.54882 0 MPRFLG 0 ; 
       4 SCHEM -1.397531 -4.54882 0 MPRFLG 0 ; 
       5 SCHEM -1.397531 -2.54882 0 MPRFLG 0 ; 
       6 SCHEM -4.897531 -9.54882 0 MPRFLG 0 ; 
       7 SCHEM -1.397531 -16.54882 0 MPRFLG 0 ; 
       8 SCHEM -1.397531 -14.54882 0 MPRFLG 0 ; 
       9 SCHEM 2.102469 -6.54882 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 2.102469 -4.54882 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 2.102469 -10.54882 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 2.102469 -8.54882 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM -8.397531 -9.54882 0 USR SRT 1 1 1 0 -3.141593 0 0 -0.003570795 68.37733 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -1.397531 -0.7988195 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.102469 -4.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.102469 -6.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.102469 -2.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.102469 -12.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.102469 -2.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.102469 -8.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.102469 -8.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.102469 -2.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -1.397531 -0.7988195 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -1.397531 -0.7988195 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 2.102469 -4.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 2.102469 -6.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.102469 -12.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.102469 -2.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5.602469 -6.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5.602469 -8.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5.602469 -10.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5.602469 -4.79882 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5.602469 -4.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.102469 -0.7988195 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5.602469 -4.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5.602469 -6.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5.602469 -2.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5.602469 -12.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5.602469 -2.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5.602469 -8.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5.602469 -8.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5.602469 -2.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.102469 -0.7988195 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 2.102469 -0.7988195 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 5.602469 -6.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 5.602469 -12.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 5.602469 -2.79882 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM -4.897531 -0.7988195 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 19 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
