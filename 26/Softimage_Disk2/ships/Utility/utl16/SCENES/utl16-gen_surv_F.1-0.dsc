SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl16-utl16.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       strobes-cam_int1.1-0 ROOT ; 
       strobes-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       plan_surv_vesR6-light3.1-0 ROOT ; 
       pre_cargo_ship7-light1.1-0 ROOT ; 
       pre_cargo_ship7-light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       gen_surv_F-mat216.1-0 ; 
       gen_surv_F-nose_white-center.1-2.1-0 ; 
       pre_cargo_RPT-mat1.1-0 ; 
       pre_cargo_RPT-mat10.1-0 ; 
       pre_cargo_RPT-mat11.1-0 ; 
       pre_cargo_RPT-mat2.1-0 ; 
       pre_cargo_RPT-mat215.1-0 ; 
       pre_cargo_RPT-mat26.1-0 ; 
       pre_cargo_RPT-mat27.1-0 ; 
       pre_cargo_RPT-mat28.1-0 ; 
       pre_cargo_RPT-mat29.1-0 ; 
       pre_cargo_RPT-mat3.1-0 ; 
       pre_cargo_RPT-mat30.1-0 ; 
       pre_cargo_RPT-mat31.1-0 ; 
       pre_cargo_RPT-mat32.1-0 ; 
       pre_cargo_RPT-mat33.1-0 ; 
       pre_cargo_RPT-mat34.1-0 ; 
       pre_cargo_RPT-mat35.1-0 ; 
       pre_cargo_RPT-mat36.1-0 ; 
       pre_cargo_RPT-mat37.1-0 ; 
       pre_cargo_RPT-mat38.1-0 ; 
       pre_cargo_RPT-mat4.1-0 ; 
       pre_cargo_RPT-mat5.1-0 ; 
       pre_cargo_RPT-mat6.1-0 ; 
       pre_cargo_RPT-mat7.1-0 ; 
       pre_cargo_RPT-mat78.1-0 ; 
       pre_cargo_RPT-mat79.1-0 ; 
       pre_cargo_RPT-mat8.1-0 ; 
       pre_cargo_RPT-mat80.1-0 ; 
       pre_cargo_RPT-mat81.1-0 ; 
       pre_cargo_RPT-mat82.1-0 ; 
       pre_cargo_RPT-mat83.1-0 ; 
       pre_cargo_RPT-mat9.1-0 ; 
       pre_cargo_RPT-mat94.1-0 ; 
       pre_cargo_RPT-mat95.1-0 ; 
       strobes-nose_white-center.1-0.1-0 ; 
       strobes-nose_white-center.1-1.1-0 ; 
       strobes-port_red-left.1-0.1-0 ; 
       strobes-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       utl16-crgatt.1-0 ; 
       utl16-engine1.1-0 ; 
       utl16-fuselg.1-0 ; 
       utl16-lengine0.1-0 ; 
       utl16-lengine1.1-0 ; 
       utl16-lengine2.1-0 ; 
       utl16-rengine0.1-0 ; 
       utl16-rengine2.1-0 ; 
       utl16-SSa.1-0 ; 
       utl16-SSf.1-0 ; 
       utl16-SSl.1-0 ; 
       utl16-SSm.1-0 ; 
       utl16-SSr.1-0 ; 
       utl16-tractr.1-0 ; 
       utl16-turatt.1-0 ; 
       utl16-utl16.1-0 ROOT ; 
       utl16-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl14a ; 
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl16-gen_surv_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       gen_surv_F-t2d204.1-0 ; 
       pre_cargo_RPT-t2d1.1-0 ; 
       pre_cargo_RPT-t2d10.1-0 ; 
       pre_cargo_RPT-t2d2.1-0 ; 
       pre_cargo_RPT-t2d203.1-0 ; 
       pre_cargo_RPT-t2d24.1-0 ; 
       pre_cargo_RPT-t2d25.1-0 ; 
       pre_cargo_RPT-t2d26.1-0 ; 
       pre_cargo_RPT-t2d27.1-0 ; 
       pre_cargo_RPT-t2d28.1-0 ; 
       pre_cargo_RPT-t2d29.1-0 ; 
       pre_cargo_RPT-t2d3.1-0 ; 
       pre_cargo_RPT-t2d30.1-0 ; 
       pre_cargo_RPT-t2d31.1-0 ; 
       pre_cargo_RPT-t2d32.1-0 ; 
       pre_cargo_RPT-t2d33.1-0 ; 
       pre_cargo_RPT-t2d34.1-0 ; 
       pre_cargo_RPT-t2d35.1-0 ; 
       pre_cargo_RPT-t2d4.1-0 ; 
       pre_cargo_RPT-t2d5.1-0 ; 
       pre_cargo_RPT-t2d6.1-0 ; 
       pre_cargo_RPT-t2d7.1-0 ; 
       pre_cargo_RPT-t2d75.1-0 ; 
       pre_cargo_RPT-t2d76.1-0 ; 
       pre_cargo_RPT-t2d77.1-0 ; 
       pre_cargo_RPT-t2d78.1-0 ; 
       pre_cargo_RPT-t2d79.1-0 ; 
       pre_cargo_RPT-t2d8.1-0 ; 
       pre_cargo_RPT-t2d80.1-0 ; 
       pre_cargo_RPT-t2d9.1-0 ; 
       pre_cargo_RPT-t2d90.1-0 ; 
       pre_cargo_RPT-t2d91.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       0 13 110 ; 
       1 6 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       16 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 1 300 ; 
       9 36 300 ; 
       10 37 300 ; 
       11 35 300 ; 
       12 38 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       2 2 300 ; 
       2 5 300 ; 
       2 11 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 27 300 ; 
       2 32 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 6 300 ; 
       2 0 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       7 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       13 12 300 ; 
       13 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       15 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       3 29 401 ; 
       4 2 401 ; 
       5 1 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 3 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 11 401 ; 
       22 18 401 ; 
       23 19 401 ; 
       24 20 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 21 401 ; 
       28 24 401 ; 
       29 25 401 ; 
       30 26 401 ; 
       31 28 401 ; 
       32 27 401 ; 
       33 30 401 ; 
       34 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5.997327 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 8.497326 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 10.99733 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 6.944209 -13.08829 0 USR WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 6.562959 -8.98889 0 USR WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 6.653089 -4.988903 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 6.635587 -10.98889 0 USR WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 6.653089 -6.988894 0 USR WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 10.34911 -18.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10.34911 -22.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 3.349113 -16.12529 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 6.849113 -24.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 10.34911 -25.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 10.34911 -23.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 6.849113 -21.12529 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10.34911 -20.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 6.849113 -18.12529 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 6.849113 -16.12529 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM -0.1508868 -16.22183 0 USR DISPLAY 3 2 SRT 6 6 6 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 6.849113 -27.12529 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10.44421 -13.33829 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13.84911 -22.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13.84911 -22.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 13.84911 -25.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 13.84911 -25.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 13.84911 -23.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 13.84911 -23.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10.13559 -11.23889 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10.06296 -9.23889 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10.15309 -5.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 10.15309 -7.238894 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.34911 -20.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.34911 -20.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.34911 -22.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.34911 -22.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.34911 -25.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.34911 -25.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 17.34911 -23.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 17.34911 -23.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.349113 -3.37529 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
