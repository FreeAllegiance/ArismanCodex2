SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl16-utl16.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       strobes-cam_int1.4-0 ROOT ; 
       strobes-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       reduce_poly_to_354-light1.1-0 ROOT ; 
       reduce_poly_to_354-light2.1-0 ROOT ; 
       reduce_poly_to_354-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       pre_cargo_RPT-mat1.2-0 ; 
       pre_cargo_RPT-mat10.2-0 ; 
       pre_cargo_RPT-mat11.2-0 ; 
       pre_cargo_RPT-mat2.2-0 ; 
       pre_cargo_RPT-mat215.2-0 ; 
       pre_cargo_RPT-mat26.2-0 ; 
       pre_cargo_RPT-mat27.2-0 ; 
       pre_cargo_RPT-mat28.2-0 ; 
       pre_cargo_RPT-mat29.2-0 ; 
       pre_cargo_RPT-mat3.2-0 ; 
       pre_cargo_RPT-mat31.2-0 ; 
       pre_cargo_RPT-mat33.2-0 ; 
       pre_cargo_RPT-mat34.2-0 ; 
       pre_cargo_RPT-mat36.2-0 ; 
       pre_cargo_RPT-mat38.2-0 ; 
       pre_cargo_RPT-mat4.2-0 ; 
       pre_cargo_RPT-mat5.2-0 ; 
       pre_cargo_RPT-mat6.2-0 ; 
       pre_cargo_RPT-mat7.2-0 ; 
       pre_cargo_RPT-mat78.2-0 ; 
       pre_cargo_RPT-mat79.2-0 ; 
       pre_cargo_RPT-mat8.2-0 ; 
       pre_cargo_RPT-mat80.2-0 ; 
       pre_cargo_RPT-mat81.2-0 ; 
       pre_cargo_RPT-mat82.2-0 ; 
       pre_cargo_RPT-mat83.2-0 ; 
       pre_cargo_RPT-mat9.2-0 ; 
       reduce_poly_to_354-nose_white-center.1-2.1-0 ; 
       strobes-port_red-left.1-0.1-0 ; 
       strobes-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       utl16-crgatt.1-0 ; 
       utl16-engine1.1-0 ; 
       utl16-fuselg.1-0 ; 
       utl16-lengine0.1-0 ; 
       utl16-lengine1.1-0 ; 
       utl16-lengine2.1-0 ; 
       utl16-rengine0.1-0 ; 
       utl16-rengine2.1-0 ; 
       utl16-SSa.1-0 ; 
       utl16-SSf.1-0 ; 
       utl16-SSl.1-0 ; 
       utl16-SSm.1-0 ; 
       utl16-SSr.1-0 ; 
       utl16-tractr.1-0 ; 
       utl16-turatt.1-0 ; 
       utl16-utl16.4-0 ROOT ; 
       utl16-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl14a ; 
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl16-reduce_poly_to_354.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       pre_cargo_RPT-t2d1.1-0 ; 
       pre_cargo_RPT-t2d10.1-0 ; 
       pre_cargo_RPT-t2d2.1-0 ; 
       pre_cargo_RPT-t2d203.1-0 ; 
       pre_cargo_RPT-t2d24.1-0 ; 
       pre_cargo_RPT-t2d25.1-0 ; 
       pre_cargo_RPT-t2d26.1-0 ; 
       pre_cargo_RPT-t2d27.1-0 ; 
       pre_cargo_RPT-t2d28.1-0 ; 
       pre_cargo_RPT-t2d29.1-0 ; 
       pre_cargo_RPT-t2d3.1-0 ; 
       pre_cargo_RPT-t2d30.1-0 ; 
       pre_cargo_RPT-t2d31.1-0 ; 
       pre_cargo_RPT-t2d32.1-0 ; 
       pre_cargo_RPT-t2d33.1-0 ; 
       pre_cargo_RPT-t2d34.1-0 ; 
       pre_cargo_RPT-t2d35.1-0 ; 
       pre_cargo_RPT-t2d4.1-0 ; 
       pre_cargo_RPT-t2d5.1-0 ; 
       pre_cargo_RPT-t2d6.1-0 ; 
       pre_cargo_RPT-t2d7.1-0 ; 
       pre_cargo_RPT-t2d75.1-0 ; 
       pre_cargo_RPT-t2d76.1-0 ; 
       pre_cargo_RPT-t2d77.1-0 ; 
       pre_cargo_RPT-t2d78.1-0 ; 
       pre_cargo_RPT-t2d79.1-0 ; 
       pre_cargo_RPT-t2d8.1-0 ; 
       pre_cargo_RPT-t2d80.1-0 ; 
       pre_cargo_RPT-t2d9.1-0 ; 
       pre_cargo_RPT-t2d90.1-0 ; 
       pre_cargo_RPT-t2d91.1-0 ; 
       reduce_poly_to_354-t2d204.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 6 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       16 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 11 300 ; 
       1 13 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 9 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 21 300 ; 
       2 26 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 4 300 ; 
       4 11 300 ; 
       4 14 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       7 0 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       8 27 300 ; 
       9 27 300 ; 
       10 28 300 ; 
       11 27 300 ; 
       12 29 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 10 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       15 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 28 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 2 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 14 401 ; 
       14 16 401 ; 
       15 10 401 ; 
       16 17 401 ; 
       17 18 401 ; 
       18 19 401 ; 
       19 21 401 ; 
       20 22 401 ; 
       21 20 401 ; 
       22 23 401 ; 
       23 24 401 ; 
       24 25 401 ; 
       25 27 401 ; 
       26 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 112.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 115 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 35 -6 0 MPRFLG 0 ; 
       1 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 55 0 0 SRT 6 6 6 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 55 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60.10602 -4.141359 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 95 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 107.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 4 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
