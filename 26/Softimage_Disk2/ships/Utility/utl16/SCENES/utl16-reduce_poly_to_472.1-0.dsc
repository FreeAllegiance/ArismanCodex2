SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl16-utl16.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       strobes-cam_int1.2-0 ROOT ; 
       strobes-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       pre_cargo_RPT-mat1.1-0 ; 
       pre_cargo_RPT-mat10.1-0 ; 
       pre_cargo_RPT-mat11.1-0 ; 
       pre_cargo_RPT-mat2.1-0 ; 
       pre_cargo_RPT-mat215.1-0 ; 
       pre_cargo_RPT-mat26.1-0 ; 
       pre_cargo_RPT-mat27.1-0 ; 
       pre_cargo_RPT-mat28.1-0 ; 
       pre_cargo_RPT-mat29.1-0 ; 
       pre_cargo_RPT-mat3.1-0 ; 
       pre_cargo_RPT-mat30.1-0 ; 
       pre_cargo_RPT-mat31.1-0 ; 
       pre_cargo_RPT-mat32.1-0 ; 
       pre_cargo_RPT-mat33.1-0 ; 
       pre_cargo_RPT-mat34.1-0 ; 
       pre_cargo_RPT-mat35.1-0 ; 
       pre_cargo_RPT-mat36.1-0 ; 
       pre_cargo_RPT-mat37.1-0 ; 
       pre_cargo_RPT-mat38.1-0 ; 
       pre_cargo_RPT-mat4.1-0 ; 
       pre_cargo_RPT-mat5.1-0 ; 
       pre_cargo_RPT-mat6.1-0 ; 
       pre_cargo_RPT-mat7.1-0 ; 
       pre_cargo_RPT-mat78.1-0 ; 
       pre_cargo_RPT-mat79.1-0 ; 
       pre_cargo_RPT-mat8.1-0 ; 
       pre_cargo_RPT-mat80.1-0 ; 
       pre_cargo_RPT-mat81.1-0 ; 
       pre_cargo_RPT-mat82.1-0 ; 
       pre_cargo_RPT-mat83.1-0 ; 
       pre_cargo_RPT-mat9.1-0 ; 
       pre_cargo_RPT-mat94.1-0 ; 
       pre_cargo_RPT-mat95.1-0 ; 
       reduce_poly_to_472-mat216.1-0 ; 
       reduce_poly_to_472-nose_white-center.1-2.1-0 ; 
       strobes-nose_white-center.1-0.1-0 ; 
       strobes-nose_white-center.1-1.1-0 ; 
       strobes-port_red-left.1-0.1-0 ; 
       strobes-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       utl16-crgatt.1-0 ; 
       utl16-engine1.1-0 ; 
       utl16-fuselg.1-0 ; 
       utl16-lengine0.1-0 ; 
       utl16-lengine1.1-0 ; 
       utl16-lengine2.1-0 ; 
       utl16-rengine0.1-0 ; 
       utl16-rengine2.1-0 ; 
       utl16-SSa.1-0 ; 
       utl16-SSf.1-0 ; 
       utl16-SSl.1-0 ; 
       utl16-SSm.1-0 ; 
       utl16-SSr.1-0 ; 
       utl16-tractr.1-0 ; 
       utl16-turatt.1-0 ; 
       utl16-utl16.2-0 ROOT ; 
       utl16-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl14a ; 
       D:/Pete_Data/Softimage/ships/Utility/utl16/PICTURES/utl16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl16-reduce_poly_to_472.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       pre_cargo_RPT-t2d1.1-0 ; 
       pre_cargo_RPT-t2d10.1-0 ; 
       pre_cargo_RPT-t2d2.1-0 ; 
       pre_cargo_RPT-t2d203.1-0 ; 
       pre_cargo_RPT-t2d24.1-0 ; 
       pre_cargo_RPT-t2d25.1-0 ; 
       pre_cargo_RPT-t2d26.1-0 ; 
       pre_cargo_RPT-t2d27.1-0 ; 
       pre_cargo_RPT-t2d28.1-0 ; 
       pre_cargo_RPT-t2d29.1-0 ; 
       pre_cargo_RPT-t2d3.1-0 ; 
       pre_cargo_RPT-t2d30.1-0 ; 
       pre_cargo_RPT-t2d31.1-0 ; 
       pre_cargo_RPT-t2d32.1-0 ; 
       pre_cargo_RPT-t2d33.1-0 ; 
       pre_cargo_RPT-t2d34.1-0 ; 
       pre_cargo_RPT-t2d35.1-0 ; 
       pre_cargo_RPT-t2d4.1-0 ; 
       pre_cargo_RPT-t2d5.1-0 ; 
       pre_cargo_RPT-t2d6.1-0 ; 
       pre_cargo_RPT-t2d7.1-0 ; 
       pre_cargo_RPT-t2d75.1-0 ; 
       pre_cargo_RPT-t2d76.1-0 ; 
       pre_cargo_RPT-t2d77.1-0 ; 
       pre_cargo_RPT-t2d78.1-0 ; 
       pre_cargo_RPT-t2d79.1-0 ; 
       pre_cargo_RPT-t2d8.1-0 ; 
       pre_cargo_RPT-t2d80.1-0 ; 
       pre_cargo_RPT-t2d9.1-0 ; 
       pre_cargo_RPT-t2d90.1-0 ; 
       pre_cargo_RPT-t2d91.1-0 ; 
       reduce_poly_to_472-t2d204.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 6 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       16 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 15 300 ; 
       1 16 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 9 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 25 300 ; 
       2 30 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 4 300 ; 
       2 33 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       7 12 300 ; 
       7 13 300 ; 
       7 14 300 ; 
       8 34 300 ; 
       9 36 300 ; 
       10 37 300 ; 
       11 35 300 ; 
       12 38 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
       13 10 300 ; 
       13 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       15 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       33 31 401 ; 
       1 28 401 ; 
       2 1 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 2 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       15 13 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       18 16 401 ; 
       19 10 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 20 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 27 401 ; 
       30 26 401 ; 
       31 29 401 ; 
       32 30 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10.34911 -18.12529 0 MPRFLG 0 ; 
       1 SCHEM 10.34911 -22.12529 0 MPRFLG 0 ; 
       2 SCHEM 3.349113 -16.12529 0 USR MPRFLG 0 ; 
       3 SCHEM 6.849113 -24.12529 0 MPRFLG 0 ; 
       4 SCHEM 10.34911 -25.12529 0 MPRFLG 0 ; 
       5 SCHEM 10.34911 -23.12529 0 MPRFLG 0 ; 
       6 SCHEM 6.849113 -21.12529 0 USR MPRFLG 0 ; 
       7 SCHEM 10.34911 -20.12529 0 MPRFLG 0 ; 
       8 SCHEM 6.944209 -13.08829 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       9 SCHEM 6.562959 -8.98889 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       10 SCHEM 6.653089 -4.988903 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 6.635587 -10.98889 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 6.653089 -6.988894 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       13 SCHEM 6.849113 -18.12529 0 USR MPRFLG 0 ; 
       14 SCHEM 6.849113 -16.12529 0 USR MPRFLG 0 ; 
       15 SCHEM -0.1508868 -16.22183 0 USR SRT 6 6 6 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 6.849113 -27.12529 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       33 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 10.44421 -13.33829 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10.34911 -16.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13.84911 -20.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13.84911 -22.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13.84911 -22.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 13.84911 -25.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 13.84911 -25.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.849113 -3.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 13.84911 -23.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 13.84911 -23.37529 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10.13559 -11.23889 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10.06296 -9.23889 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10.15309 -5.238903 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 10.15309 -7.238894 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       31 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 13.84911 -16.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.34911 -20.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.34911 -20.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.34911 -22.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.34911 -22.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 17.34911 -25.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.34911 -25.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 10.34911 -3.238903 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 17.34911 -23.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 17.34911 -23.37529 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.349113 -3.37529 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
