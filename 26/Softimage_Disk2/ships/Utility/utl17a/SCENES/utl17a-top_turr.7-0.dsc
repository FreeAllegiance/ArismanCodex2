SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl17-utl17a.19-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       top_turr-cam_int1.7-0 ROOT ; 
       top_turr-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       top_turr-inf_light2_1.7-0 ROOT ; 
       top_turr-inf_light2_1_2.7-0 ROOT ; 
       top_turr-light1_1.7-0 ROOT ; 
       top_turr-light1_1_2.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 101     
       top_turr-mat168.3-0 ; 
       top_turr-mat169.3-0 ; 
       top_turr-mat171.3-0 ; 
       top_turr-mat173.3-0 ; 
       top_turr-mat176.3-0 ; 
       top_turr-mat177.1-0 ; 
       top_turr-mat178.1-0 ; 
       top_turr-mat179.1-0 ; 
       top_turr-mat180.1-0 ; 
       top_turr-mat181.1-0 ; 
       top_turr-mat182.1-0 ; 
       top_turr-mat183.1-0 ; 
       top_turr-mat184.1-0 ; 
       top_turr-mat185.1-0 ; 
       top_turr-mat186.1-0 ; 
       top_turr-mat187.1-0 ; 
       top_turr-mat188.1-0 ; 
       top_turr-mat189.1-0 ; 
       top_turr-mat190.1-0 ; 
       top_turr-mat191.1-0 ; 
       top_turr-mat192.1-0 ; 
       top_turr-mat193.1-0 ; 
       top_turr-mat194.1-0 ; 
       top_turr-mat195.1-0 ; 
       top_turr-mat202.1-0 ; 
       top_turr-mat203.1-0 ; 
       top_turr-mat204.1-0 ; 
       top_turr-mat205.1-0 ; 
       top_turr-mat206.1-0 ; 
       top_turr-mat207.1-0 ; 
       top_turr-mat208.1-0 ; 
       top_turr-mat209.1-0 ; 
       top_turr-mat210.1-0 ; 
       top_turr-mat211.1-0 ; 
       top_turr-mat212.1-0 ; 
       top_turr-mat213.1-0 ; 
       top_turr-mat214.1-0 ; 
       top_turr-mat215.1-0 ; 
       top_turr-mat216.1-0 ; 
       top_turr-mat217.1-0 ; 
       top_turr-mat218.1-0 ; 
       top_turr-mat219.1-0 ; 
       top_turr-mat220.1-0 ; 
       top_turr-mat221.1-0 ; 
       top_turr-mat222.1-0 ; 
       top_turr-mat223.1-0 ; 
       top_turr-mat224.1-0 ; 
       top_turr-mat225.1-0 ; 
       top_turr-mat226.1-0 ; 
       top_turr-mat227.1-0 ; 
       top_turr-mat228.1-0 ; 
       top_turr-mat229.1-0 ; 
       top_turr-mat230.1-0 ; 
       top_turr-mat231.1-0 ; 
       top_turr-mat232.1-0 ; 
       top_turr-mat233.1-0 ; 
       top_turr-mat234.1-0 ; 
       top_turr-mat235.1-0 ; 
       top_turr-mat236.1-0 ; 
       top_turr-mat237.1-0 ; 
       top_turr-mat238.1-0 ; 
       top_turr-mat239.1-0 ; 
       top_turr-mat240.1-0 ; 
       top_turr-mat241.1-0 ; 
       top_turr-mat242.1-0 ; 
       top_turr-mat243.1-0 ; 
       top_turr-mat244.1-0 ; 
       top_turr-mat245.1-0 ; 
       top_turr-mat246.1-0 ; 
       top_turr-mat247.1-0 ; 
       top_turr-mat248.1-0 ; 
       top_turr-mat249.3-0 ; 
       top_turr-mat250.1-0 ; 
       top_turr-mat251.1-0 ; 
       top_turr-mat252.1-0 ; 
       top_turr-mat253.1-0 ; 
       top_turr-mat254.1-0 ; 
       top_turr-mat255.1-0 ; 
       top_turr-mat256.2-0 ; 
       top_turr-nose_white-center.1-1.1-0 ; 
       top_turr-nose_white-center.1-10.1-0 ; 
       top_turr-nose_white-center.1-11.1-0 ; 
       top_turr-nose_white-center.1-12.1-0 ; 
       top_turr-nose_white-center.1-13.1-0 ; 
       top_turr-nose_white-center.1-15.1-0 ; 
       top_turr-nose_white-center.1-16.1-0 ; 
       top_turr-nose_white-center.1-17.1-0 ; 
       top_turr-nose_white-center.1-18.1-0 ; 
       top_turr-nose_white-center.1-19.1-0 ; 
       top_turr-nose_white-center.1-2.1-0 ; 
       top_turr-nose_white-center.1-20.1-0 ; 
       top_turr-nose_white-center.1-21.1-0 ; 
       top_turr-nose_white-center.1-3.1-0 ; 
       top_turr-nose_white-center.1-4.1-0 ; 
       top_turr-nose_white-center.1-5.1-0 ; 
       top_turr-nose_white-center.1-6.1-0 ; 
       top_turr-nose_white-center.1-8.1-0 ; 
       top_turr-nose_white-center.1-9.1-0 ; 
       top_turr-port_red-left.1-0.1-0 ; 
       top_turr-port_red-left.1-1.1-0 ; 
       top_turr-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 53     
       top_turr-circle1.3-0 ROOT ; 
       utl17-antenn.1-0 ; 
       utl17-bcontwr.1-0 ; 
       utl17-bslrsal.1-0 ; 
       utl17-contwrt.1-0 ; 
       utl17-cyl1.1-0 ; 
       utl17-fuselg1.2-0 ; 
       utl17-fuselg2.1-0 ; 
       utl17-lslrsal0.2-0 ; 
       utl17-lturatt1.1-0 ; 
       utl17-lturatt2.1-0 ; 
       utl17-lturatt3.1-0 ; 
       utl17-lturatt4.1-0 ; 
       utl17-lwingzz.1-0 ; 
       utl17-mturatt.1-0 ; 
       utl17-rturatt1.1-0 ; 
       utl17-rturatt2.1-0 ; 
       utl17-rturatt3.1-0 ; 
       utl17-rturatt4.1-0 ; 
       utl17-rwingzz.1-0 ; 
       utl17-slrsal1.1-0 ; 
       utl17-slrsal2.1-0 ; 
       utl17-slrsal3.1-0 ; 
       utl17-slrsal4.1-0 ; 
       utl17-slrsal5.1-0 ; 
       utl17-slrsal6.1-0 ; 
       utl17-slrsal7.1-0 ; 
       utl17-SSa.1-0 ; 
       utl17-SSa10.1-0 ; 
       utl17-SSc0.1-0 ; 
       utl17-SSc1.1-0 ; 
       utl17-SSc2.1-0 ; 
       utl17-SSc3.1-0 ; 
       utl17-SSc4.1-0 ; 
       utl17-SSc5.1-0 ; 
       utl17-SSd0.1-0 ; 
       utl17-SSd1.1-0 ; 
       utl17-SSd3.1-0 ; 
       utl17-SSd4.1-0 ; 
       utl17-SSd5.1-0 ; 
       utl17-SSl.1-0 ; 
       utl17-SSl1.5-0 ; 
       utl17-SSr.1-0 ; 
       utl17-SSs1.1-0 ; 
       utl17-SSs2.1-0 ; 
       utl17-SSs3.1-0 ; 
       utl17-SSs4.1-0 ; 
       utl17-SSs5.1-0 ; 
       utl17-SSs7.3-0 ; 
       utl17-SSs8.1-0 ; 
       utl17-SSs9.1-0 ; 
       utl17-utl17a.14-0 ROOT ; 
       utl17-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl17a/PICTURES/utl17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl17a-top_turr.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 69     
       top_turr-t2d149.7-0 ; 
       top_turr-t2d151.7-0 ; 
       top_turr-t2d154.7-0 ; 
       top_turr-t2d155.1-0 ; 
       top_turr-t2d156.1-0 ; 
       top_turr-t2d157.1-0 ; 
       top_turr-t2d158.1-0 ; 
       top_turr-t2d159.1-0 ; 
       top_turr-t2d160.1-0 ; 
       top_turr-t2d161.1-0 ; 
       top_turr-t2d162.1-0 ; 
       top_turr-t2d163.1-0 ; 
       top_turr-t2d164.1-0 ; 
       top_turr-t2d165.1-0 ; 
       top_turr-t2d166.1-0 ; 
       top_turr-t2d167.1-0 ; 
       top_turr-t2d168.1-0 ; 
       top_turr-t2d169.1-0 ; 
       top_turr-t2d174.1-0 ; 
       top_turr-t2d175.1-0 ; 
       top_turr-t2d176.1-0 ; 
       top_turr-t2d177.1-0 ; 
       top_turr-t2d178.1-0 ; 
       top_turr-t2d179.1-0 ; 
       top_turr-t2d180.1-0 ; 
       top_turr-t2d181.1-0 ; 
       top_turr-t2d182.1-0 ; 
       top_turr-t2d183.1-0 ; 
       top_turr-t2d184.1-0 ; 
       top_turr-t2d185.1-0 ; 
       top_turr-t2d186.1-0 ; 
       top_turr-t2d187.1-0 ; 
       top_turr-t2d188.1-0 ; 
       top_turr-t2d189.1-0 ; 
       top_turr-t2d190.1-0 ; 
       top_turr-t2d191.1-0 ; 
       top_turr-t2d192.1-0 ; 
       top_turr-t2d193.1-0 ; 
       top_turr-t2d194.1-0 ; 
       top_turr-t2d195.1-0 ; 
       top_turr-t2d196.1-0 ; 
       top_turr-t2d197.1-0 ; 
       top_turr-t2d198.1-0 ; 
       top_turr-t2d199.1-0 ; 
       top_turr-t2d200.1-0 ; 
       top_turr-t2d201.1-0 ; 
       top_turr-t2d202.1-0 ; 
       top_turr-t2d203.1-0 ; 
       top_turr-t2d204.1-0 ; 
       top_turr-t2d205.1-0 ; 
       top_turr-t2d206.1-0 ; 
       top_turr-t2d207.1-0 ; 
       top_turr-t2d208.1-0 ; 
       top_turr-t2d209.1-0 ; 
       top_turr-t2d210.1-0 ; 
       top_turr-t2d211.1-0 ; 
       top_turr-t2d212.1-0 ; 
       top_turr-t2d213.1-0 ; 
       top_turr-t2d214.7-0 ; 
       top_turr-t2d215.1-0 ; 
       top_turr-t2d216.1-0 ; 
       top_turr-t2d217.1-0 ; 
       top_turr-t2d218.1-0 ; 
       top_turr-t2d219.1-0 ; 
       top_turr-t2d220.1-0 ; 
       top_turr-t2d221.5-0 ; 
       top_turr-t2d222.5-0 ; 
       top_turr-t2d223.5-0 ; 
       top_turr-zzt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 7 110 ; 
       6 51 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 7 110 ; 
       10 7 110 ; 
       11 7 110 ; 
       12 13 110 ; 
       13 7 110 ; 
       14 6 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 19 110 ; 
       19 7 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 2 110 ; 
       28 35 110 ; 
       29 7 110 ; 
       30 29 110 ; 
       31 29 110 ; 
       32 29 110 ; 
       33 29 110 ; 
       34 29 110 ; 
       35 7 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 35 110 ; 
       39 35 110 ; 
       40 13 110 ; 
       41 1 110 ; 
       42 19 110 ; 
       43 20 110 ; 
       44 21 110 ; 
       45 22 110 ; 
       46 23 110 ; 
       47 24 110 ; 
       48 25 110 ; 
       49 26 110 ; 
       50 26 110 ; 
       52 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 32 300 ; 
       1 33 300 ; 
       1 34 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       2 70 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       3 38 300 ; 
       3 39 300 ; 
       3 40 300 ; 
       3 69 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       5 72 300 ; 
       5 73 300 ; 
       5 74 300 ; 
       5 75 300 ; 
       5 76 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       6 4 300 ; 
       6 71 300 ; 
       6 78 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 77 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       13 14 300 ; 
       13 15 300 ; 
       13 16 300 ; 
       19 17 300 ; 
       19 18 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       20 65 300 ; 
       20 66 300 ; 
       20 67 300 ; 
       20 68 300 ; 
       21 41 300 ; 
       21 42 300 ; 
       21 43 300 ; 
       22 44 300 ; 
       22 45 300 ; 
       22 46 300 ; 
       23 47 300 ; 
       23 48 300 ; 
       23 49 300 ; 
       23 50 300 ; 
       24 51 300 ; 
       24 52 300 ; 
       24 53 300 ; 
       24 54 300 ; 
       25 55 300 ; 
       25 56 300 ; 
       25 57 300 ; 
       25 58 300 ; 
       26 59 300 ; 
       26 60 300 ; 
       26 61 300 ; 
       26 62 300 ; 
       26 63 300 ; 
       26 64 300 ; 
       27 79 300 ; 
       28 90 300 ; 
       30 81 300 ; 
       31 84 300 ; 
       32 85 300 ; 
       33 86 300 ; 
       34 83 300 ; 
       36 82 300 ; 
       37 88 300 ; 
       38 87 300 ; 
       39 91 300 ; 
       40 98 300 ; 
       41 99 300 ; 
       42 100 300 ; 
       43 89 300 ; 
       44 92 300 ; 
       45 93 300 ; 
       46 94 300 ; 
       47 95 300 ; 
       48 96 300 ; 
       49 97 300 ; 
       50 80 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       51 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 65 401 ; 
       1 67 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       23 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 68 401 ; 
       40 30 401 ; 
       41 31 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 34 401 ; 
       45 35 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 38 401 ; 
       49 39 401 ; 
       50 40 401 ; 
       51 41 401 ; 
       52 42 401 ; 
       53 43 401 ; 
       54 44 401 ; 
       56 45 401 ; 
       57 46 401 ; 
       58 47 401 ; 
       60 48 401 ; 
       62 49 401 ; 
       63 50 401 ; 
       64 51 401 ; 
       65 52 401 ; 
       66 53 401 ; 
       67 54 401 ; 
       68 55 401 ; 
       69 56 401 ; 
       70 57 401 ; 
       71 58 401 ; 
       72 63 401 ; 
       73 59 401 ; 
       74 60 401 ; 
       75 61 401 ; 
       76 62 401 ; 
       77 64 401 ; 
       78 66 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 0 0 SRT 0.171072 0.171072 0.171072 1.570796 0 0 -0.5992453 2.071687 11.44266 MPRFLG 0 ; 
       1 SCHEM 228.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 238.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 137.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 76.25 -8 0 MPRFLG 0 ; 
       8 SCHEM 176.25 -8 0 MPRFLG 0 ; 
       9 SCHEM 107.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 110 -10 0 MPRFLG 0 ; 
       11 SCHEM 40 -10 0 MPRFLG 0 ; 
       12 SCHEM 45 -12 0 MPRFLG 0 ; 
       13 SCHEM 50 -10 0 MPRFLG 0 ; 
       14 SCHEM 257.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 60 -10 0 MPRFLG 0 ; 
       16 SCHEM 35 -10 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 82.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 215 -10 0 MPRFLG 0 ; 
       21 SCHEM 136.25 -10 0 MPRFLG 0 ; 
       22 SCHEM 146.25 -10 0 MPRFLG 0 ; 
       23 SCHEM 157.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 170 -10 0 MPRFLG 0 ; 
       25 SCHEM 182.5 -10 0 MPRFLG 0 ; 
       26 SCHEM 198.75 -10 0 MPRFLG 0 ; 
       27 SCHEM 222.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 102.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 92.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 95 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 97.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 100 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 42.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 225 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 75 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       43 SCHEM 210 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 132.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 142.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 152.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 165 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 177.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 192.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 190 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 137.5 -4 0 SRT 1 1 1 0 0 0 -0.6727697 0.1343076 -0.0006141663 MPRFLG 0 ; 
       52 SCHEM 105 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 272.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 260 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 262.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 265 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 267.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 85 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 245 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 237.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 240 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 242.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 235 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 247.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 250 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 252.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 232.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 227.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 230 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 140 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 137.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 150 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 145 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 147.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 162.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 155 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 160 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 175 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 167.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 170 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 172.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 187.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 180 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 182.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 185 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 207.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 195 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 197.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 200 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 202.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 205 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 220 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 212.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 215 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 217.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 255 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 270 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 275 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 222.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 190 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 210 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 100 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 132.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 142.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 152.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 165 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 177.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 192.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 225 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       67 SCHEM 260 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 272.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 262.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 265 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 267.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 117.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 85 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 237.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 240 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 242.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 235 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 247.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 250 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 252.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 227.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 230 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 140 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 135 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 137.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 150 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 145 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 147.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 162.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 155 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 157.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 160 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 175 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 167.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 170 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 172.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 180 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 182.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 185 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 195 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 200 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 202.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 205 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 220 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 212.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 215 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 217.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 255 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 270 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 27.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 275 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 276.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 29 2 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
