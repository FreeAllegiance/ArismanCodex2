SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       clean_up_texture-utl18a.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.25-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       clean_up_texture-light5.2-0 ROOT ; 
       clean_up_texture-light6.2-0 ROOT ; 
       pass_cont_F-light1.25-0 ROOT ; 
       pass_cont_F-light2.25-0 ROOT ; 
       pass_cont_F-light3.25-0 ROOT ; 
       pass_cont_F-light4.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       clean_up_texture-mat69.1-0 ; 
       clean_up_texture-mat70.1-0 ; 
       clean_up_texture-mat71.1-0 ; 
       clean_up_texture-starbord_green-right.1-1.1-0 ; 
       clean_up_texture-starbord_green-right.1-2.1-0 ; 
       clean_up_texture-starbord_green-right.1-3.1-0 ; 
       clean_up_texture-starbord_green-right.1-4.1-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.8-0 ; 
       gen_skycrane_tL-mat23.8-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.8-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.8-0 ; 
       gen_skycrane_tL-mat52.8-0 ; 
       gen_skycrane_tL-mat53.8-0 ; 
       gen_skycrane_tL-mat55.8-0 ; 
       gen_skycrane_tL-mat56.8-0 ; 
       gen_skycrane_tL-mat58.6-0 ; 
       gen_skycrane_tL-mat59.6-0 ; 
       gen_skycrane_tL-mat60.8-0 ; 
       gen_skycrane_tL-mat61.8-0 ; 
       gen_skycrane_tL-mat62.7-0 ; 
       gen_skycrane_tL-mat63.7-0 ; 
       gen_skycrane_tL-mat64.8-0 ; 
       gen_skycrane_tL-mat65.8-0 ; 
       gen_skycrane_tL-mat66.8-0 ; 
       gen_skycrane_tL-mat67.8-0 ; 
       gen_skycrane_tL-mat68.8-0 ; 
       gen_skycrane_tL-mat9.8-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       clean_up_texture-antenn1.1-0 ; 
       clean_up_texture-antenn2.1-0 ; 
       clean_up_texture-engine0.1-0 ; 
       clean_up_texture-engine1.1-0 ; 
       clean_up_texture-engine2.1-0 ; 
       clean_up_texture-engine3.1-0 ; 
       clean_up_texture-fuselg.1-0 ; 
       clean_up_texture-lwepemt.1-0 ; 
       clean_up_texture-lwingzz.1-0 ; 
       clean_up_texture-rwepemt.1-0 ; 
       clean_up_texture-rwingzz.1-0 ; 
       clean_up_texture-smoke.1-0 ; 
       clean_up_texture-SSa1.1-0 ; 
       clean_up_texture-SSa2.1-0 ; 
       clean_up_texture-SSb1.1-0 ; 
       clean_up_texture-SSb2.1-0 ; 
       clean_up_texture-SSb3.1-0 ; 
       clean_up_texture-SSb4.1-0 ; 
       clean_up_texture-SSf.1-0 ; 
       clean_up_texture-SSl.1-0 ; 
       clean_up_texture-SSr.1-0 ; 
       clean_up_texture-thrust1.1-0 ; 
       clean_up_texture-thrust2.1-0 ; 
       clean_up_texture-thrust3.1-0 ; 
       clean_up_texture-trail.1-0 ; 
       clean_up_texture-turwepemt.1-0 ; 
       clean_up_texture-utl18a.2-0 ROOT ; 
       clean_up_texture-wepemt.1-0 ; 
       clean_up_texture-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-clean_up_texture.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       clean_up_texture-t2d64.2-0 ; 
       clean_up_texture-t2d65.2-0 ; 
       clean_up_texture-t2d66.2-0 ; 
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.6-0 ; 
       gen_skycrane_tL-t2d12.6-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.14-0 ; 
       gen_skycrane_tL-t2d34.14-0 ; 
       gen_skycrane_tL-t2d38.6-0 ; 
       gen_skycrane_tL-t2d39.6-0 ; 
       gen_skycrane_tL-t2d40.6-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.6-0 ; 
       gen_skycrane_tL-t2d44.6-0 ; 
       gen_skycrane_tL-t2d45.6-0 ; 
       gen_skycrane_tL-t2d46.14-0 ; 
       gen_skycrane_tL-t2d47.14-0 ; 
       gen_skycrane_tL-t2d48.14-0 ; 
       gen_skycrane_tL-t2d50.14-0 ; 
       gen_skycrane_tL-t2d51.14-0 ; 
       gen_skycrane_tL-t2d53.12-0 ; 
       gen_skycrane_tL-t2d54.12-0 ; 
       gen_skycrane_tL-t2d55.14-0 ; 
       gen_skycrane_tL-t2d56.14-0 ; 
       gen_skycrane_tL-t2d57.13-0 ; 
       gen_skycrane_tL-t2d58.13-0 ; 
       gen_skycrane_tL-t2d59.14-0 ; 
       gen_skycrane_tL-t2d6.14-0 ; 
       gen_skycrane_tL-t2d60.14-0 ; 
       gen_skycrane_tL-t2d61.14-0 ; 
       gen_skycrane_tL-t2d62.14-0 ; 
       gen_skycrane_tL-t2d63.14-0 ; 
       gen_skycrane_tL-t2d8.6-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 28 110 ; 
       9 26 110 ; 
       10 28 110 ; 
       11 26 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       20 6 110 ; 
       21 26 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 26 110 ; 
       25 26 110 ; 
       27 26 110 ; 
       28 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 46 300 ; 
       0 50 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 48 300 ; 
       1 49 300 ; 
       2 15 300 ; 
       3 9 300 ; 
       3 18 300 ; 
       3 23 300 ; 
       4 8 300 ; 
       4 16 300 ; 
       4 22 300 ; 
       5 7 300 ; 
       5 17 300 ; 
       5 21 300 ; 
       6 10 300 ; 
       6 40 300 ; 
       6 11 300 ; 
       6 13 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       6 39 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       8 14 300 ; 
       8 20 300 ; 
       10 12 300 ; 
       10 19 300 ; 
       12 42 300 ; 
       13 43 300 ; 
       14 4 300 ; 
       15 3 300 ; 
       16 5 300 ; 
       17 6 300 ; 
       18 41 300 ; 
       19 44 300 ; 
       20 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       26 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       7 34 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 7 401 ; 
       12 3 401 ; 
       13 8 401 ; 
       14 6 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       40 29 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -20.16581 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 60 -10 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 20 -10 0 MPRFLG 0 ; 
       4 SCHEM 5 -10 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 33.75 -10 0 MPRFLG 0 ; 
       11 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 42.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 45 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 0 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       18 SCHEM 50 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 55 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 140 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 142.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 73.75 -4 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       27 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 36.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 108.9618 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 103.8215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 110.1321 -16.00087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 112.749 -16.16581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 103.9618 -15.40961 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.3215 -15.3592 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 111.4618 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 112.6321 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 115.249 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 106.4618 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 62.81995 -10.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 65.31995 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 108.9618 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 103.8215 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 110.1321 -18.00087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 112.749 -18.16581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 103.9618 -17.40961 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 101.3215 -17.3592 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 111.4618 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 112.6321 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 115.249 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 106.4618 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 65.31995 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 149 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
