SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 2     
       edit_polys-utl18a.3-0 ; 
       utl26-utl26.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.4-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       edit_polys-light5.2-0 ROOT ; 
       edit_polys-light6.2-0 ROOT ; 
       pass_cont_F-light1.4-0 ROOT ; 
       pass_cont_F-light2.4-0 ROOT ; 
       pass_cont_F-light3.4-0 ROOT ; 
       pass_cont_F-light4.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 63     
       edit_polys-starbord_green-right.1-1.2-0 ; 
       edit_polys-starbord_green-right.1-2.2-0 ; 
       edit_polys-starbord_green-right.1-3.2-0 ; 
       edit_polys-starbord_green-right.1-4.2-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.3-0 ; 
       gen_skycrane_tL-mat23.3-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.3-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.3-0 ; 
       gen_skycrane_tL-mat52.3-0 ; 
       gen_skycrane_tL-mat53.3-0 ; 
       gen_skycrane_tL-mat55.3-0 ; 
       gen_skycrane_tL-mat56.3-0 ; 
       gen_skycrane_tL-mat58.3-0 ; 
       gen_skycrane_tL-mat59.3-0 ; 
       gen_skycrane_tL-mat60.3-0 ; 
       gen_skycrane_tL-mat61.3-0 ; 
       gen_skycrane_tL-mat62.3-0 ; 
       gen_skycrane_tL-mat63.3-0 ; 
       gen_skycrane_tL-mat64.3-0 ; 
       gen_skycrane_tL-mat65.3-0 ; 
       gen_skycrane_tL-mat66.3-0 ; 
       gen_skycrane_tL-mat67.3-0 ; 
       gen_skycrane_tL-mat68.3-0 ; 
       gen_skycrane_tL-mat9.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.2-0 ; 
       pass_cont_F-mat36.2-0 ; 
       pass_cont_F-mat37.2-0 ; 
       pass_cont_F-mat38.2-0 ; 
       pass_cont_F-mat39.2-0 ; 
       pass_cont_F-mat40.2-0 ; 
       pass_cont_F-mat41.2-0 ; 
       pass_cont_F-mat42.2-0 ; 
       pass_cont_F-mat43.2-0 ; 
       pass_cont_F-mat44.2-0 ; 
       pass_cont_F-mat45.2-0 ; 
       pass_cont_F-mat46.2-0 ; 
       pass_cont_F-mat47.2-0 ; 
       pass_cont_F-mat48.2-0 ; 
       pass_cont_F-mat49.2-0 ; 
       pass_cont_F-mat50.2-0 ; 
       pass_cont_F-mat51.2-0 ; 
       pass_cont_F-mat52.2-0 ; 
       pass_cont_F-mat53.2-0 ; 
       pass_cont_F-mat54.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       edit_polys-crgatt.1-0 ; 
       edit_polys-engine0.1-0 ; 
       edit_polys-engine1.1-0 ; 
       edit_polys-engine2.1-0 ; 
       edit_polys-engine3.1-0 ; 
       edit_polys-fuselg.1-0 ; 
       edit_polys-lwepmnt.1-0 ; 
       edit_polys-lwingzz.1-0 ; 
       edit_polys-rwepmnt.1-0 ; 
       edit_polys-rwingzz.1-0 ; 
       edit_polys-SSa1.1-0 ; 
       edit_polys-SSa2.1-0 ; 
       edit_polys-SSb1.1-0 ; 
       edit_polys-SSb2.1-0 ; 
       edit_polys-SSb3.1-0 ; 
       edit_polys-SSb4.1-0 ; 
       edit_polys-SSf.1-0 ; 
       edit_polys-SSl.1-0 ; 
       edit_polys-SSr.1-0 ; 
       edit_polys-turatt.1-0 ; 
       edit_polys-utl18a.3-0 ROOT ; 
       edit_polys-wepemt.1-0 ; 
       edit_polys-wingzz0.1-0 ; 
       utl26-antenn1.1-0 ; 
       utl26-antenn2.1-0 ; 
       utl26-fuselg.1-0 ; 
       utl26-turratt.1-0 ; 
       utl26-utl26.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl26 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-edit_polys.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.3-0 ; 
       gen_skycrane_tL-t2d12.3-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.3-0 ; 
       gen_skycrane_tL-t2d34.3-0 ; 
       gen_skycrane_tL-t2d38.3-0 ; 
       gen_skycrane_tL-t2d39.3-0 ; 
       gen_skycrane_tL-t2d40.3-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.3-0 ; 
       gen_skycrane_tL-t2d44.3-0 ; 
       gen_skycrane_tL-t2d45.3-0 ; 
       gen_skycrane_tL-t2d46.3-0 ; 
       gen_skycrane_tL-t2d47.3-0 ; 
       gen_skycrane_tL-t2d48.3-0 ; 
       gen_skycrane_tL-t2d50.3-0 ; 
       gen_skycrane_tL-t2d51.3-0 ; 
       gen_skycrane_tL-t2d53.3-0 ; 
       gen_skycrane_tL-t2d54.3-0 ; 
       gen_skycrane_tL-t2d55.3-0 ; 
       gen_skycrane_tL-t2d56.3-0 ; 
       gen_skycrane_tL-t2d57.3-0 ; 
       gen_skycrane_tL-t2d58.3-0 ; 
       gen_skycrane_tL-t2d59.3-0 ; 
       gen_skycrane_tL-t2d6.3-0 ; 
       gen_skycrane_tL-t2d60.3-0 ; 
       gen_skycrane_tL-t2d61.3-0 ; 
       gen_skycrane_tL-t2d62.3-0 ; 
       gen_skycrane_tL-t2d63.3-0 ; 
       gen_skycrane_tL-t2d8.3-0 ; 
       pass_cont_F-t2d33.2-0 ; 
       pass_cont_F-t2d34.2-0 ; 
       pass_cont_F-t2d35.2-0 ; 
       pass_cont_F-t2d36.2-0 ; 
       pass_cont_F-t2d37.2-0 ; 
       pass_cont_F-t2d38.2-0 ; 
       pass_cont_F-t2d39.2-0 ; 
       pass_cont_F-t2d40.2-0 ; 
       pass_cont_F-t2d41.2-0 ; 
       pass_cont_F-t2d42.2-0 ; 
       pass_cont_F-t2d43.2-0 ; 
       pass_cont_F-t2d44.2-0 ; 
       pass_cont_F-t2d45.2-0 ; 
       pass_cont_F-t2d46.2-0 ; 
       pass_cont_F-t2d47.2-0 ; 
       pass_cont_F-t2d48.2-0 ; 
       pass_cont_F-t2d49.2-0 ; 
       pass_cont_F-t2d50.2-0 ; 
       pass_cont_F-t2d51.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 20 110 ; 
       6 5 110 ; 
       7 22 110 ; 
       8 5 110 ; 
       9 22 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 5 110 ; 
       17 5 110 ; 
       18 5 110 ; 
       19 5 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 25 110 ; 
       24 23 110 ; 
       25 27 110 ; 
       26 25 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 12 300 ; 
       2 6 300 ; 
       2 15 300 ; 
       2 20 300 ; 
       3 5 300 ; 
       3 13 300 ; 
       3 19 300 ; 
       4 4 300 ; 
       4 14 300 ; 
       4 18 300 ; 
       5 7 300 ; 
       5 37 300 ; 
       5 8 300 ; 
       5 10 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       5 29 300 ; 
       5 30 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       7 11 300 ; 
       7 17 300 ; 
       9 9 300 ; 
       9 16 300 ; 
       10 39 300 ; 
       11 40 300 ; 
       12 1 300 ; 
       13 0 300 ; 
       14 2 300 ; 
       15 3 300 ; 
       16 38 300 ; 
       17 41 300 ; 
       18 42 300 ; 
       23 43 300 ; 
       23 59 300 ; 
       23 60 300 ; 
       23 61 300 ; 
       23 62 300 ; 
       24 43 300 ; 
       24 51 300 ; 
       24 52 300 ; 
       24 53 300 ; 
       24 54 300 ; 
       24 55 300 ; 
       24 56 300 ; 
       25 43 300 ; 
       25 44 300 ; 
       25 45 300 ; 
       25 46 300 ; 
       25 47 300 ; 
       25 48 300 ; 
       25 49 300 ; 
       25 50 300 ; 
       25 57 300 ; 
       25 58 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       20 0 15000 ; 
       27 1 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 31 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 4 401 ; 
       9 0 401 ; 
       10 5 401 ; 
       11 3 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 26 401 ; 
       44 32 401 ; 
       45 33 401 ; 
       46 34 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       51 39 401 ; 
       52 40 401 ; 
       53 41 401 ; 
       54 42 401 ; 
       55 43 401 ; 
       56 44 401 ; 
       57 45 401 ; 
       58 46 401 ; 
       59 47 401 ; 
       60 48 401 ; 
       61 49 401 ; 
       62 50 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 55 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 57.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 60 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 62.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 65 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 30 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 35 -4 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 40 -4 0 WIRECOL 6 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 45 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 47.5 -4 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 15 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       21 SCHEM 20 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 28.75 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 50 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 50 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 51.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 51.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0.01007271 -1.189506 9.992488 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 31.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 39 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 4 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 26.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 49 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 41.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 21.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 24 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 46.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 51.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 49 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 54 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 51.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 51.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 51.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 51.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 29 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 29 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 49 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 49 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 54 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 51.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 51.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 51.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 51.5 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 54 -2 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
