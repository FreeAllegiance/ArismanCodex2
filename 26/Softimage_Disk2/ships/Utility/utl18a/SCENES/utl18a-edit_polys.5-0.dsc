SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       edit_polys-utl18a.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.6-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       edit_polys-light5.4-0 ROOT ; 
       edit_polys-light6.4-0 ROOT ; 
       pass_cont_F-light1.6-0 ROOT ; 
       pass_cont_F-light2.6-0 ROOT ; 
       pass_cont_F-light3.6-0 ROOT ; 
       pass_cont_F-light4.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       edit_polys-starbord_green-right.1-1.2-0 ; 
       edit_polys-starbord_green-right.1-2.2-0 ; 
       edit_polys-starbord_green-right.1-3.2-0 ; 
       edit_polys-starbord_green-right.1-4.2-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.3-0 ; 
       gen_skycrane_tL-mat23.3-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.3-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.3-0 ; 
       gen_skycrane_tL-mat52.3-0 ; 
       gen_skycrane_tL-mat53.3-0 ; 
       gen_skycrane_tL-mat55.3-0 ; 
       gen_skycrane_tL-mat56.3-0 ; 
       gen_skycrane_tL-mat58.3-0 ; 
       gen_skycrane_tL-mat59.3-0 ; 
       gen_skycrane_tL-mat60.3-0 ; 
       gen_skycrane_tL-mat61.3-0 ; 
       gen_skycrane_tL-mat62.3-0 ; 
       gen_skycrane_tL-mat63.3-0 ; 
       gen_skycrane_tL-mat64.3-0 ; 
       gen_skycrane_tL-mat65.3-0 ; 
       gen_skycrane_tL-mat66.3-0 ; 
       gen_skycrane_tL-mat67.3-0 ; 
       gen_skycrane_tL-mat68.3-0 ; 
       gen_skycrane_tL-mat9.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       edit_polys-antenn1.1-0 ; 
       edit_polys-antenn2.1-0 ; 
       edit_polys-crgatt.1-0 ; 
       edit_polys-engine0.1-0 ; 
       edit_polys-engine1.1-0 ; 
       edit_polys-engine2.1-0 ; 
       edit_polys-engine3.1-0 ; 
       edit_polys-fuselg.1-0 ; 
       edit_polys-lwepmnt.1-0 ; 
       edit_polys-lwingzz.1-0 ; 
       edit_polys-rwepmnt.1-0 ; 
       edit_polys-rwingzz.1-0 ; 
       edit_polys-SSa1.1-0 ; 
       edit_polys-SSa2.1-0 ; 
       edit_polys-SSb1.1-0 ; 
       edit_polys-SSb2.1-0 ; 
       edit_polys-SSb3.1-0 ; 
       edit_polys-SSb4.1-0 ; 
       edit_polys-SSf.1-0 ; 
       edit_polys-SSl.1-0 ; 
       edit_polys-SSr.1-0 ; 
       edit_polys-turatt.1-0 ; 
       edit_polys-utl18a.5-0 ROOT ; 
       edit_polys-wepemt.1-0 ; 
       edit_polys-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-edit_polys.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.4-0 ; 
       gen_skycrane_tL-t2d12.4-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.3-0 ; 
       gen_skycrane_tL-t2d34.3-0 ; 
       gen_skycrane_tL-t2d38.4-0 ; 
       gen_skycrane_tL-t2d39.4-0 ; 
       gen_skycrane_tL-t2d40.4-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.4-0 ; 
       gen_skycrane_tL-t2d44.4-0 ; 
       gen_skycrane_tL-t2d45.4-0 ; 
       gen_skycrane_tL-t2d46.3-0 ; 
       gen_skycrane_tL-t2d47.3-0 ; 
       gen_skycrane_tL-t2d48.3-0 ; 
       gen_skycrane_tL-t2d50.3-0 ; 
       gen_skycrane_tL-t2d51.3-0 ; 
       gen_skycrane_tL-t2d53.3-0 ; 
       gen_skycrane_tL-t2d54.3-0 ; 
       gen_skycrane_tL-t2d55.3-0 ; 
       gen_skycrane_tL-t2d56.3-0 ; 
       gen_skycrane_tL-t2d57.3-0 ; 
       gen_skycrane_tL-t2d58.3-0 ; 
       gen_skycrane_tL-t2d59.3-0 ; 
       gen_skycrane_tL-t2d6.3-0 ; 
       gen_skycrane_tL-t2d60.3-0 ; 
       gen_skycrane_tL-t2d61.3-0 ; 
       gen_skycrane_tL-t2d62.3-0 ; 
       gen_skycrane_tL-t2d63.3-0 ; 
       gen_skycrane_tL-t2d8.4-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 7 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 22 110 ; 
       8 7 110 ; 
       9 24 110 ; 
       10 7 110 ; 
       11 24 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 7 110 ; 
       21 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
       0 7 110 ; 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 12 300 ; 
       4 6 300 ; 
       4 15 300 ; 
       4 20 300 ; 
       5 5 300 ; 
       5 13 300 ; 
       5 19 300 ; 
       6 4 300 ; 
       6 14 300 ; 
       6 18 300 ; 
       7 7 300 ; 
       7 37 300 ; 
       7 8 300 ; 
       7 10 300 ; 
       7 21 300 ; 
       7 22 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       9 11 300 ; 
       9 17 300 ; 
       11 9 300 ; 
       11 16 300 ; 
       12 39 300 ; 
       13 40 300 ; 
       14 1 300 ; 
       15 0 300 ; 
       16 2 300 ; 
       17 3 300 ; 
       18 38 300 ; 
       19 41 300 ; 
       20 42 300 ; 
       0 43 300 ; 
       0 47 300 ; 
       1 43 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       1 46 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       22 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 31 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 4 401 ; 
       9 0 401 ; 
       10 5 401 ; 
       11 3 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 26 401 ; 
       44 32 401 ; 
       45 33 401 ; 
       46 34 401 ; 
       47 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 10 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 65 -6 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 0 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 60 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 65 -4 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       0 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 75.31995 -10.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 77.81995 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 77.81995 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 131.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
