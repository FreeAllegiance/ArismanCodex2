SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       edit_text_color-utl18a.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.17-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       edit_text_color-light5.1-0 ROOT ; 
       edit_text_color-light6.1-0 ROOT ; 
       pass_cont_F-light1.17-0 ROOT ; 
       pass_cont_F-light2.17-0 ROOT ; 
       pass_cont_F-light3.17-0 ROOT ; 
       pass_cont_F-light4.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       edit_text_color-mat69.1-0 ; 
       edit_text_color-mat70.1-0 ; 
       edit_text_color-mat71.1-0 ; 
       edit_text_color-starbord_green-right.1-1.1-0 ; 
       edit_text_color-starbord_green-right.1-2.1-0 ; 
       edit_text_color-starbord_green-right.1-3.1-0 ; 
       edit_text_color-starbord_green-right.1-4.1-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.8-0 ; 
       gen_skycrane_tL-mat23.8-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.8-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.8-0 ; 
       gen_skycrane_tL-mat52.8-0 ; 
       gen_skycrane_tL-mat53.8-0 ; 
       gen_skycrane_tL-mat55.8-0 ; 
       gen_skycrane_tL-mat56.8-0 ; 
       gen_skycrane_tL-mat58.6-0 ; 
       gen_skycrane_tL-mat59.6-0 ; 
       gen_skycrane_tL-mat60.8-0 ; 
       gen_skycrane_tL-mat61.8-0 ; 
       gen_skycrane_tL-mat62.7-0 ; 
       gen_skycrane_tL-mat63.7-0 ; 
       gen_skycrane_tL-mat64.8-0 ; 
       gen_skycrane_tL-mat65.8-0 ; 
       gen_skycrane_tL-mat66.8-0 ; 
       gen_skycrane_tL-mat67.8-0 ; 
       gen_skycrane_tL-mat68.8-0 ; 
       gen_skycrane_tL-mat9.8-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       edit_text_color-antenn1.1-0 ; 
       edit_text_color-antenn2.1-0 ; 
       edit_text_color-crgatt.1-0 ; 
       edit_text_color-engine0.1-0 ; 
       edit_text_color-engine1.1-0 ; 
       edit_text_color-engine2.1-0 ; 
       edit_text_color-engine3.1-0 ; 
       edit_text_color-fuselg.1-0 ; 
       edit_text_color-lwepmnt.1-0 ; 
       edit_text_color-lwingzz.1-0 ; 
       edit_text_color-rwepmnt.1-0 ; 
       edit_text_color-rwingzz.1-0 ; 
       edit_text_color-SSa1.1-0 ; 
       edit_text_color-SSa2.1-0 ; 
       edit_text_color-SSb1.1-0 ; 
       edit_text_color-SSb2.1-0 ; 
       edit_text_color-SSb3.1-0 ; 
       edit_text_color-SSb4.1-0 ; 
       edit_text_color-SSf.1-0 ; 
       edit_text_color-SSl.1-0 ; 
       edit_text_color-SSr.1-0 ; 
       edit_text_color-turatt.1-0 ; 
       edit_text_color-utl18a.1-0 ROOT ; 
       edit_text_color-wepemt.1-0 ; 
       edit_text_color-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-edit_text_color.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       edit_text_color-t2d64.1-0 ; 
       edit_text_color-t2d65.1-0 ; 
       edit_text_color-t2d66.1-0 ; 
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.6-0 ; 
       gen_skycrane_tL-t2d12.6-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.11-0 ; 
       gen_skycrane_tL-t2d34.11-0 ; 
       gen_skycrane_tL-t2d38.6-0 ; 
       gen_skycrane_tL-t2d39.6-0 ; 
       gen_skycrane_tL-t2d40.6-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.6-0 ; 
       gen_skycrane_tL-t2d44.6-0 ; 
       gen_skycrane_tL-t2d45.6-0 ; 
       gen_skycrane_tL-t2d46.11-0 ; 
       gen_skycrane_tL-t2d47.11-0 ; 
       gen_skycrane_tL-t2d48.11-0 ; 
       gen_skycrane_tL-t2d50.11-0 ; 
       gen_skycrane_tL-t2d51.11-0 ; 
       gen_skycrane_tL-t2d53.9-0 ; 
       gen_skycrane_tL-t2d54.9-0 ; 
       gen_skycrane_tL-t2d55.11-0 ; 
       gen_skycrane_tL-t2d56.11-0 ; 
       gen_skycrane_tL-t2d57.10-0 ; 
       gen_skycrane_tL-t2d58.10-0 ; 
       gen_skycrane_tL-t2d59.11-0 ; 
       gen_skycrane_tL-t2d6.11-0 ; 
       gen_skycrane_tL-t2d60.11-0 ; 
       gen_skycrane_tL-t2d61.11-0 ; 
       gen_skycrane_tL-t2d62.11-0 ; 
       gen_skycrane_tL-t2d63.11-0 ; 
       gen_skycrane_tL-t2d8.6-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 0 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 22 110 ; 
       8 7 110 ; 
       9 24 110 ; 
       10 7 110 ; 
       11 24 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 7 110 ; 
       21 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 46 300 ; 
       0 50 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 48 300 ; 
       1 49 300 ; 
       3 15 300 ; 
       4 9 300 ; 
       4 18 300 ; 
       4 23 300 ; 
       5 8 300 ; 
       5 16 300 ; 
       5 22 300 ; 
       6 7 300 ; 
       6 17 300 ; 
       6 21 300 ; 
       7 10 300 ; 
       7 40 300 ; 
       7 11 300 ; 
       7 13 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       9 14 300 ; 
       9 20 300 ; 
       11 12 300 ; 
       11 19 300 ; 
       12 42 300 ; 
       13 43 300 ; 
       14 4 300 ; 
       15 3 300 ; 
       16 5 300 ; 
       17 6 300 ; 
       18 41 300 ; 
       19 44 300 ; 
       20 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       22 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 34 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 7 401 ; 
       12 3 401 ; 
       13 8 401 ; 
       14 6 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       40 29 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 68.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 0 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 60 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 68.75 -4 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 46.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 110.0715 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 116.3821 -16.00087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 118.999 -16.16581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 110.2118 -15.40961 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 107.5715 -15.3592 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 112.7118 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 115.2118 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 118.8821 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 121.499 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 123.999 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 126.499 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 128.999 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 75.31995 -10.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 77.81995 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 131.499 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 133.999 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 136.499 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 116.3821 -18.00087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 118.999 -18.16581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 110.2118 -17.40961 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 107.5715 -17.3592 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 112.7118 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 115.2118 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 118.8821 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 121.499 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 123.999 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 126.499 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 128.999 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 77.81995 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 131.499 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 133.999 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 136.499 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 139 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
