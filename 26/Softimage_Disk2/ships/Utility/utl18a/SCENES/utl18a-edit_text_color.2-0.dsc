SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       edit_text_color-utl18a.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.18-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       edit_text_color-light5.2-0 ROOT ; 
       edit_text_color-light6.2-0 ROOT ; 
       pass_cont_F-light1.18-0 ROOT ; 
       pass_cont_F-light2.18-0 ROOT ; 
       pass_cont_F-light3.18-0 ROOT ; 
       pass_cont_F-light4.18-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       edit_text_color-mat69.1-0 ; 
       edit_text_color-mat70.1-0 ; 
       edit_text_color-mat71.1-0 ; 
       edit_text_color-starbord_green-right.1-1.1-0 ; 
       edit_text_color-starbord_green-right.1-2.1-0 ; 
       edit_text_color-starbord_green-right.1-3.1-0 ; 
       edit_text_color-starbord_green-right.1-4.1-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.8-0 ; 
       gen_skycrane_tL-mat23.8-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.8-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.8-0 ; 
       gen_skycrane_tL-mat52.8-0 ; 
       gen_skycrane_tL-mat53.8-0 ; 
       gen_skycrane_tL-mat55.8-0 ; 
       gen_skycrane_tL-mat56.8-0 ; 
       gen_skycrane_tL-mat58.6-0 ; 
       gen_skycrane_tL-mat59.6-0 ; 
       gen_skycrane_tL-mat60.8-0 ; 
       gen_skycrane_tL-mat61.8-0 ; 
       gen_skycrane_tL-mat62.7-0 ; 
       gen_skycrane_tL-mat63.7-0 ; 
       gen_skycrane_tL-mat64.8-0 ; 
       gen_skycrane_tL-mat65.8-0 ; 
       gen_skycrane_tL-mat66.8-0 ; 
       gen_skycrane_tL-mat67.8-0 ; 
       gen_skycrane_tL-mat68.8-0 ; 
       gen_skycrane_tL-mat9.8-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       edit_text_color-antenn1.1-0 ; 
       edit_text_color-antenn2.1-0 ; 
       edit_text_color-engine0.1-0 ; 
       edit_text_color-engine1.1-0 ; 
       edit_text_color-engine2.1-0 ; 
       edit_text_color-engine3.1-0 ; 
       edit_text_color-fuselg.1-0 ; 
       edit_text_color-lwepemt.1-0 ; 
       edit_text_color-lwingzz.1-0 ; 
       edit_text_color-rwepemt.1-0 ; 
       edit_text_color-rwingzz.1-0 ; 
       edit_text_color-SSa1.1-0 ; 
       edit_text_color-SSa2.1-0 ; 
       edit_text_color-SSb1.1-0 ; 
       edit_text_color-SSb2.1-0 ; 
       edit_text_color-SSb3.1-0 ; 
       edit_text_color-SSb4.1-0 ; 
       edit_text_color-SSf.1-0 ; 
       edit_text_color-SSl.1-0 ; 
       edit_text_color-SSr.1-0 ; 
       edit_text_color-turwepemt.1-0 ; 
       edit_text_color-utl18a.2-0 ROOT ; 
       edit_text_color-wepemt.1-0 ; 
       edit_text_color-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-edit_text_color.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       edit_text_color-t2d64.1-0 ; 
       edit_text_color-t2d65.1-0 ; 
       edit_text_color-t2d66.1-0 ; 
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.6-0 ; 
       gen_skycrane_tL-t2d12.6-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.11-0 ; 
       gen_skycrane_tL-t2d34.11-0 ; 
       gen_skycrane_tL-t2d38.6-0 ; 
       gen_skycrane_tL-t2d39.6-0 ; 
       gen_skycrane_tL-t2d40.6-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.6-0 ; 
       gen_skycrane_tL-t2d44.6-0 ; 
       gen_skycrane_tL-t2d45.6-0 ; 
       gen_skycrane_tL-t2d46.11-0 ; 
       gen_skycrane_tL-t2d47.11-0 ; 
       gen_skycrane_tL-t2d48.11-0 ; 
       gen_skycrane_tL-t2d50.11-0 ; 
       gen_skycrane_tL-t2d51.11-0 ; 
       gen_skycrane_tL-t2d53.9-0 ; 
       gen_skycrane_tL-t2d54.9-0 ; 
       gen_skycrane_tL-t2d55.11-0 ; 
       gen_skycrane_tL-t2d56.11-0 ; 
       gen_skycrane_tL-t2d57.10-0 ; 
       gen_skycrane_tL-t2d58.10-0 ; 
       gen_skycrane_tL-t2d59.11-0 ; 
       gen_skycrane_tL-t2d6.11-0 ; 
       gen_skycrane_tL-t2d60.11-0 ; 
       gen_skycrane_tL-t2d61.11-0 ; 
       gen_skycrane_tL-t2d62.11-0 ; 
       gen_skycrane_tL-t2d63.11-0 ; 
       gen_skycrane_tL-t2d8.6-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 21 110 ; 
       8 23 110 ; 
       10 23 110 ; 
       11 6 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 6 110 ; 
       19 6 110 ; 
       23 6 110 ; 
       7 21 110 ; 
       20 21 110 ; 
       9 21 110 ; 
       22 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 46 300 ; 
       0 50 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 48 300 ; 
       1 49 300 ; 
       2 15 300 ; 
       3 9 300 ; 
       3 18 300 ; 
       3 23 300 ; 
       4 8 300 ; 
       4 16 300 ; 
       4 22 300 ; 
       5 7 300 ; 
       5 17 300 ; 
       5 21 300 ; 
       6 10 300 ; 
       6 40 300 ; 
       6 11 300 ; 
       6 13 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       6 39 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       8 14 300 ; 
       8 20 300 ; 
       10 12 300 ; 
       10 19 300 ; 
       11 42 300 ; 
       12 43 300 ; 
       13 4 300 ; 
       14 3 300 ; 
       15 5 300 ; 
       16 6 300 ; 
       17 41 300 ; 
       18 44 300 ; 
       19 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       21 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 34 401 ; 
       8 4 401 ; 
       9 5 401 ; 
       11 7 401 ; 
       12 3 401 ; 
       13 8 401 ; 
       14 6 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       39 33 401 ; 
       40 29 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       50 38 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       14 SCHEM 25 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -4 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 35 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       23 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       20 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 67.63206 -12.00087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 70.24896 -12.16581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61.4618 -11.40961 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 58.8215 -11.3592 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 37.81995 -6.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 67.63206 -14.00087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 70.24896 -14.16581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 61.4618 -13.40961 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 58.8215 -13.3592 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 49 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
