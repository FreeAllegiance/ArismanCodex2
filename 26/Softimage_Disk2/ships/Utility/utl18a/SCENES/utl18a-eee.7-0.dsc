SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       eee-utl18a.7-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.14-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       eee-light5.7-0 ROOT ; 
       eee-light6.7-0 ROOT ; 
       pass_cont_F-light1.14-0 ROOT ; 
       pass_cont_F-light2.14-0 ROOT ; 
       pass_cont_F-light3.14-0 ROOT ; 
       pass_cont_F-light4.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       eee-mat69.4-0 ; 
       eee-mat70.2-0 ; 
       eee-starbord_green-right.1-1.3-0 ; 
       eee-starbord_green-right.1-2.3-0 ; 
       eee-starbord_green-right.1-3.3-0 ; 
       eee-starbord_green-right.1-4.3-0 ; 
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.7-0 ; 
       gen_skycrane_tL-mat23.7-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.7-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.7-0 ; 
       gen_skycrane_tL-mat52.7-0 ; 
       gen_skycrane_tL-mat53.7-0 ; 
       gen_skycrane_tL-mat55.7-0 ; 
       gen_skycrane_tL-mat56.7-0 ; 
       gen_skycrane_tL-mat58.5-0 ; 
       gen_skycrane_tL-mat59.5-0 ; 
       gen_skycrane_tL-mat60.7-0 ; 
       gen_skycrane_tL-mat61.7-0 ; 
       gen_skycrane_tL-mat62.6-0 ; 
       gen_skycrane_tL-mat63.6-0 ; 
       gen_skycrane_tL-mat64.7-0 ; 
       gen_skycrane_tL-mat65.7-0 ; 
       gen_skycrane_tL-mat66.7-0 ; 
       gen_skycrane_tL-mat67.7-0 ; 
       gen_skycrane_tL-mat68.7-0 ; 
       gen_skycrane_tL-mat9.7-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.3-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.3-0 ; 
       gen_skycrane_tL-port_red-left.1-0.3-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.3-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       eee-antenn1.1-0 ; 
       eee-antenn2.1-0 ; 
       eee-crgatt.1-0 ; 
       eee-engine0.1-0 ; 
       eee-engine1.1-0 ; 
       eee-engine2.1-0 ; 
       eee-engine3.1-0 ; 
       eee-fuselg.1-0 ; 
       eee-lwepmnt.1-0 ; 
       eee-lwingzz.1-0 ; 
       eee-rwepmnt.1-0 ; 
       eee-rwingzz.1-0 ; 
       eee-SSa1.1-0 ; 
       eee-SSa2.1-0 ; 
       eee-SSb1.1-0 ; 
       eee-SSb2.1-0 ; 
       eee-SSb3.1-0 ; 
       eee-SSb4.1-0 ; 
       eee-SSf.1-0 ; 
       eee-SSl.1-0 ; 
       eee-SSr.1-0 ; 
       eee-turatt.1-0 ; 
       eee-utl18a.6-0 ROOT ; 
       eee-wepemt.1-0 ; 
       eee-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-eee.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       eee-t2d64.5-0 ; 
       eee-t2d65.3-0 ; 
       gen_skycrane_tL-t2d1.3-0 ; 
       gen_skycrane_tL-t2d10.3-0 ; 
       gen_skycrane_tL-t2d12.3-0 ; 
       gen_skycrane_tL-t2d2.3-0 ; 
       gen_skycrane_tL-t2d20.9-0 ; 
       gen_skycrane_tL-t2d34.9-0 ; 
       gen_skycrane_tL-t2d38.3-0 ; 
       gen_skycrane_tL-t2d39.3-0 ; 
       gen_skycrane_tL-t2d40.3-0 ; 
       gen_skycrane_tL-t2d41.3-0 ; 
       gen_skycrane_tL-t2d42.3-0 ; 
       gen_skycrane_tL-t2d43.3-0 ; 
       gen_skycrane_tL-t2d44.3-0 ; 
       gen_skycrane_tL-t2d45.3-0 ; 
       gen_skycrane_tL-t2d46.9-0 ; 
       gen_skycrane_tL-t2d47.9-0 ; 
       gen_skycrane_tL-t2d48.9-0 ; 
       gen_skycrane_tL-t2d50.9-0 ; 
       gen_skycrane_tL-t2d51.9-0 ; 
       gen_skycrane_tL-t2d53.7-0 ; 
       gen_skycrane_tL-t2d54.7-0 ; 
       gen_skycrane_tL-t2d55.9-0 ; 
       gen_skycrane_tL-t2d56.9-0 ; 
       gen_skycrane_tL-t2d57.8-0 ; 
       gen_skycrane_tL-t2d58.8-0 ; 
       gen_skycrane_tL-t2d59.9-0 ; 
       gen_skycrane_tL-t2d6.9-0 ; 
       gen_skycrane_tL-t2d60.9-0 ; 
       gen_skycrane_tL-t2d61.9-0 ; 
       gen_skycrane_tL-t2d62.9-0 ; 
       gen_skycrane_tL-t2d63.9-0 ; 
       gen_skycrane_tL-t2d8.3-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 0 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 22 110 ; 
       8 7 110 ; 
       9 24 110 ; 
       10 7 110 ; 
       11 24 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 7 110 ; 
       21 7 110 ; 
       23 7 110 ; 
       24 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 45 300 ; 
       0 49 300 ; 
       1 45 300 ; 
       1 46 300 ; 
       1 47 300 ; 
       1 48 300 ; 
       3 14 300 ; 
       4 8 300 ; 
       4 17 300 ; 
       4 22 300 ; 
       5 7 300 ; 
       5 15 300 ; 
       5 21 300 ; 
       6 6 300 ; 
       6 16 300 ; 
       6 20 300 ; 
       7 9 300 ; 
       7 39 300 ; 
       7 10 300 ; 
       7 12 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 35 300 ; 
       7 36 300 ; 
       7 37 300 ; 
       7 38 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       9 13 300 ; 
       9 19 300 ; 
       11 11 300 ; 
       11 18 300 ; 
       12 41 300 ; 
       13 42 300 ; 
       14 3 300 ; 
       15 2 300 ; 
       16 4 300 ; 
       17 5 300 ; 
       18 40 300 ; 
       19 43 300 ; 
       20 44 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       22 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 33 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       10 6 401 ; 
       11 2 401 ; 
       12 7 401 ; 
       13 5 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       38 32 401 ; 
       39 28 401 ; 
       46 34 401 ; 
       47 35 401 ; 
       48 36 401 ; 
       49 37 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -19.40961 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 75 -8 0 MPRFLG 0 ; 
       1 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 20 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 40 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 52.5 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       16 SCHEM 0 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       17 SCHEM 60 -8 0 WIRECOL 6 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 30 -8 0 MPRFLG 0 ; 
       22 SCHEM 67.5 -4 0 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 46.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 108.8215 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 115.1321 -16.00087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 117.749 -16.16581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 108.9618 -15.40961 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 106.3215 -15.3592 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 111.4618 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 113.9618 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 117.6321 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 120.249 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 122.749 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 125.249 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 127.749 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 75.31995 -10.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 70 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 72.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 77.81995 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 130.249 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 132.749 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 115.1321 -18.00087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 117.749 -18.16581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 108.9618 -17.40961 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 106.3215 -17.3592 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 111.4618 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 113.9618 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 117.6321 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 120.249 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 122.749 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 125.249 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 127.749 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 70 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 75 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 77.81995 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 130.249 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 132.749 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 136.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
