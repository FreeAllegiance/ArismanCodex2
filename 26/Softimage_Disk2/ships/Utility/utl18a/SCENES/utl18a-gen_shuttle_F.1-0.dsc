SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       gen_shuttle_F-utl18a.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       cargo_scaled-inf_light1.1-0 ROOT ; 
       cargo_scaled-inf_light1_1_1.1-0 ROOT ; 
       cargo_scaled-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       gen_shuttle_F-starbord_green-right.1-1.1-0 ; 
       gen_shuttle_F-starbord_green-right.1-2.1-0 ; 
       gen_shuttle_F-starbord_green-right.1-3.1-0 ; 
       gen_shuttle_F-starbord_green-right.1-4.1-0 ; 
       gen_skycrane_tL-mat11.1-0 ; 
       gen_skycrane_tL-mat13.1-0 ; 
       gen_skycrane_tL-mat15.1-0 ; 
       gen_skycrane_tL-mat2.1-0 ; 
       gen_skycrane_tL-mat23.1-0 ; 
       gen_skycrane_tL-mat3.1-0 ; 
       gen_skycrane_tL-mat37.1-0 ; 
       gen_skycrane_tL-mat4.1-0 ; 
       gen_skycrane_tL-mat42.1-0 ; 
       gen_skycrane_tL-mat43.1-0 ; 
       gen_skycrane_tL-mat44.1-0 ; 
       gen_skycrane_tL-mat45.1-0 ; 
       gen_skycrane_tL-mat46.1-0 ; 
       gen_skycrane_tL-mat47.1-0 ; 
       gen_skycrane_tL-mat48.1-0 ; 
       gen_skycrane_tL-mat49.1-0 ; 
       gen_skycrane_tL-mat50.1-0 ; 
       gen_skycrane_tL-mat51.1-0 ; 
       gen_skycrane_tL-mat52.1-0 ; 
       gen_skycrane_tL-mat53.1-0 ; 
       gen_skycrane_tL-mat55.1-0 ; 
       gen_skycrane_tL-mat56.1-0 ; 
       gen_skycrane_tL-mat58.1-0 ; 
       gen_skycrane_tL-mat59.1-0 ; 
       gen_skycrane_tL-mat60.1-0 ; 
       gen_skycrane_tL-mat61.1-0 ; 
       gen_skycrane_tL-mat62.1-0 ; 
       gen_skycrane_tL-mat63.1-0 ; 
       gen_skycrane_tL-mat64.1-0 ; 
       gen_skycrane_tL-mat65.1-0 ; 
       gen_skycrane_tL-mat66.1-0 ; 
       gen_skycrane_tL-mat67.1-0 ; 
       gen_skycrane_tL-mat68.1-0 ; 
       gen_skycrane_tL-mat9.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.1-0 ; 
       gen_skycrane_tL-port_red-left.1-0.1-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       gen_shuttle_F-crgatt.1-0 ; 
       gen_shuttle_F-engine0.1-0 ; 
       gen_shuttle_F-engine1.1-0 ; 
       gen_shuttle_F-engine2.1-0 ; 
       gen_shuttle_F-engine3.1-0 ; 
       gen_shuttle_F-fuselg.1-0 ; 
       gen_shuttle_F-lwepmnt.1-0 ; 
       gen_shuttle_F-lwingzz.1-0 ; 
       gen_shuttle_F-rwepmnt.1-0 ; 
       gen_shuttle_F-rwingzz.1-0 ; 
       gen_shuttle_F-SSa1.1-0 ; 
       gen_shuttle_F-SSa2.1-0 ; 
       gen_shuttle_F-SSb1.1-0 ; 
       gen_shuttle_F-SSb2.1-0 ; 
       gen_shuttle_F-SSb3.1-0 ; 
       gen_shuttle_F-SSb4.1-0 ; 
       gen_shuttle_F-SSf.1-0 ; 
       gen_shuttle_F-SSl.1-0 ; 
       gen_shuttle_F-SSr.1-0 ; 
       gen_shuttle_F-turatt.1-0 ; 
       gen_shuttle_F-utl18a.1-0 ROOT ; 
       gen_shuttle_F-wepemt.1-0 ; 
       gen_shuttle_F-wingzz0.1-0 ; 
       passenger_container-DECAL-box.1-0 ; 
       passenger_container-DECAL-box1.1-0 ; 
       passenger_container-DECAL-box2.1-0 ; 
       passenger_container-DECAL-box3.1-0 ; 
       passenger_container-DECAL-box4.1-0 ; 
       passenger_container-DECAL-box5.1-0 ; 
       passenger_container-STENCIL-utl26.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-gen_shuttle_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       gen_skycrane_tL-t2d1.1-0 ; 
       gen_skycrane_tL-t2d10.1-0 ; 
       gen_skycrane_tL-t2d12.1-0 ; 
       gen_skycrane_tL-t2d2.1-0 ; 
       gen_skycrane_tL-t2d20.1-0 ; 
       gen_skycrane_tL-t2d34.1-0 ; 
       gen_skycrane_tL-t2d38.1-0 ; 
       gen_skycrane_tL-t2d39.1-0 ; 
       gen_skycrane_tL-t2d40.1-0 ; 
       gen_skycrane_tL-t2d41.1-0 ; 
       gen_skycrane_tL-t2d42.1-0 ; 
       gen_skycrane_tL-t2d43.1-0 ; 
       gen_skycrane_tL-t2d44.1-0 ; 
       gen_skycrane_tL-t2d45.1-0 ; 
       gen_skycrane_tL-t2d46.1-0 ; 
       gen_skycrane_tL-t2d47.1-0 ; 
       gen_skycrane_tL-t2d48.1-0 ; 
       gen_skycrane_tL-t2d50.1-0 ; 
       gen_skycrane_tL-t2d51.1-0 ; 
       gen_skycrane_tL-t2d53.1-0 ; 
       gen_skycrane_tL-t2d54.1-0 ; 
       gen_skycrane_tL-t2d55.1-0 ; 
       gen_skycrane_tL-t2d56.1-0 ; 
       gen_skycrane_tL-t2d57.1-0 ; 
       gen_skycrane_tL-t2d58.1-0 ; 
       gen_skycrane_tL-t2d59.1-0 ; 
       gen_skycrane_tL-t2d6.1-0 ; 
       gen_skycrane_tL-t2d60.1-0 ; 
       gen_skycrane_tL-t2d61.1-0 ; 
       gen_skycrane_tL-t2d62.1-0 ; 
       gen_skycrane_tL-t2d63.1-0 ; 
       gen_skycrane_tL-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 5 110 ; 
       17 5 110 ; 
       18 5 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 20 110 ; 
       6 5 110 ; 
       7 22 110 ; 
       8 5 110 ; 
       9 22 110 ; 
       19 5 110 ; 
       21 5 110 ; 
       22 5 110 ; 
       23 29 110 ; 
       23 23 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       23 23 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       24 29 110 ; 
       24 24 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       24 24 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       25 29 110 ; 
       25 25 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       25 25 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       26 29 110 ; 
       26 26 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       26 26 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       27 29 110 ; 
       27 27 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       27 27 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       28 29 110 ; 
       28 28 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       28 28 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 39 300 ; 
       11 40 300 ; 
       12 1 300 ; 
       13 0 300 ; 
       14 2 300 ; 
       15 3 300 ; 
       16 38 300 ; 
       17 41 300 ; 
       18 42 300 ; 
       1 12 300 ; 
       2 6 300 ; 
       2 15 300 ; 
       2 20 300 ; 
       3 5 300 ; 
       3 13 300 ; 
       3 19 300 ; 
       4 4 300 ; 
       4 14 300 ; 
       4 18 300 ; 
       5 7 300 ; 
       5 37 300 ; 
       5 8 300 ; 
       5 10 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       5 29 300 ; 
       5 30 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       7 11 300 ; 
       7 17 300 ; 
       9 9 300 ; 
       9 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       20 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 31 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       8 4 401 ; 
       9 0 401 ; 
       10 5 401 ; 
       11 3 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 22 401 ; 
       30 23 401 ; 
       31 24 401 ; 
       32 25 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM -0.6145977 -10.09124 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM -0.6784612 -11.22917 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 13 16.42169 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 10.5 -15.57831 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 10.5 -7.578308 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 10.5 -31.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10.5 -33.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 10.5 -35.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 10.5 -37.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 10.5 -5.578308 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 10.5 -1.578308 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 10.5 -3.578308 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 10.5 -23.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10.5 -11.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 14 -9.578308 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 14 -11.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 14 -13.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7 -19.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10.5 -21.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 14 -19.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 10.5 -25.57831 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 14 -17.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 10.5 -29.57831 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 3.5 -19.57831 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       21 SCHEM 10.5 -27.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 10.5 -18.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 16.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 16.5 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 16.5 -6.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 16.5 -4.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 16.5 1.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 16.5 -8.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 13 -3.5 0 DISPLAY 0 0 SRT 1.999152 1.999152 1.999152 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -33.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -31.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -35.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -37.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -17.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -19.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -7.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -17.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -19.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -5.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -15.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -7.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -1.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -3.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21 -17.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21 -19.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21 -17.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21 -19.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 7 0.1716919 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
