SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       reduce_poly_to_598-utl18a.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.3-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       gen_skycrane_tL-mat11.1-0 ; 
       gen_skycrane_tL-mat13.1-0 ; 
       gen_skycrane_tL-mat15.1-0 ; 
       gen_skycrane_tL-mat2.1-0 ; 
       gen_skycrane_tL-mat23.1-0 ; 
       gen_skycrane_tL-mat3.1-0 ; 
       gen_skycrane_tL-mat37.1-0 ; 
       gen_skycrane_tL-mat4.1-0 ; 
       gen_skycrane_tL-mat42.1-0 ; 
       gen_skycrane_tL-mat43.1-0 ; 
       gen_skycrane_tL-mat44.1-0 ; 
       gen_skycrane_tL-mat45.1-0 ; 
       gen_skycrane_tL-mat46.1-0 ; 
       gen_skycrane_tL-mat47.1-0 ; 
       gen_skycrane_tL-mat48.1-0 ; 
       gen_skycrane_tL-mat49.1-0 ; 
       gen_skycrane_tL-mat50.1-0 ; 
       gen_skycrane_tL-mat51.1-0 ; 
       gen_skycrane_tL-mat52.1-0 ; 
       gen_skycrane_tL-mat53.1-0 ; 
       gen_skycrane_tL-mat55.1-0 ; 
       gen_skycrane_tL-mat56.1-0 ; 
       gen_skycrane_tL-mat58.1-0 ; 
       gen_skycrane_tL-mat59.1-0 ; 
       gen_skycrane_tL-mat60.1-0 ; 
       gen_skycrane_tL-mat61.1-0 ; 
       gen_skycrane_tL-mat62.1-0 ; 
       gen_skycrane_tL-mat63.1-0 ; 
       gen_skycrane_tL-mat64.1-0 ; 
       gen_skycrane_tL-mat65.1-0 ; 
       gen_skycrane_tL-mat66.1-0 ; 
       gen_skycrane_tL-mat67.1-0 ; 
       gen_skycrane_tL-mat68.1-0 ; 
       gen_skycrane_tL-mat9.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-1.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-2.1-0 ; 
       gen_skycrane_tL-nose_white-center.1-3.1-0 ; 
       gen_skycrane_tL-port_red-left.1-0.1-0 ; 
       gen_skycrane_tL-starbord_green-right.1-0.1-0 ; 
       reduce_poly_to_598-starbord_green-right.1-1.1-0 ; 
       reduce_poly_to_598-starbord_green-right.1-2.1-0 ; 
       reduce_poly_to_598-starbord_green-right.1-3.1-0 ; 
       reduce_poly_to_598-starbord_green-right.1-4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       passenger_container-DECAL-box.1-0 ; 
       passenger_container-DECAL-box1.1-0 ; 
       passenger_container-DECAL-box2.1-0 ; 
       passenger_container-DECAL-box3.1-0 ; 
       passenger_container-DECAL-box4.1-0 ; 
       passenger_container-DECAL-box5.1-0 ; 
       passenger_container-STENCIL-utl26.3-0 ROOT ; 
       reduce_poly_to_598-crgatt.1-0 ; 
       reduce_poly_to_598-engine0.1-0 ; 
       reduce_poly_to_598-engine1.1-0 ; 
       reduce_poly_to_598-engine2.1-0 ; 
       reduce_poly_to_598-engine3.1-0 ; 
       reduce_poly_to_598-fuselg.1-0 ; 
       reduce_poly_to_598-lwepmnt.1-0 ; 
       reduce_poly_to_598-lwingzz.1-0 ; 
       reduce_poly_to_598-rwepmnt.1-0 ; 
       reduce_poly_to_598-rwingzz.1-0 ; 
       reduce_poly_to_598-SSa1.1-0 ; 
       reduce_poly_to_598-SSa2.1-0 ; 
       reduce_poly_to_598-SSb1.1-0 ; 
       reduce_poly_to_598-SSb2.1-0 ; 
       reduce_poly_to_598-SSb3.1-0 ; 
       reduce_poly_to_598-SSb4.1-0 ; 
       reduce_poly_to_598-SSf.1-0 ; 
       reduce_poly_to_598-SSl.1-0 ; 
       reduce_poly_to_598-SSr.1-0 ; 
       reduce_poly_to_598-turatt.1-0 ; 
       reduce_poly_to_598-utl18a.2-0 ROOT ; 
       reduce_poly_to_598-wepemt.1-0 ; 
       reduce_poly_to_598-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-reduce_poly_to_598.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       gen_skycrane_tL-t2d1.1-0 ; 
       gen_skycrane_tL-t2d10.1-0 ; 
       gen_skycrane_tL-t2d12.1-0 ; 
       gen_skycrane_tL-t2d2.1-0 ; 
       gen_skycrane_tL-t2d20.1-0 ; 
       gen_skycrane_tL-t2d34.1-0 ; 
       gen_skycrane_tL-t2d38.1-0 ; 
       gen_skycrane_tL-t2d39.1-0 ; 
       gen_skycrane_tL-t2d40.1-0 ; 
       gen_skycrane_tL-t2d41.1-0 ; 
       gen_skycrane_tL-t2d42.1-0 ; 
       gen_skycrane_tL-t2d43.1-0 ; 
       gen_skycrane_tL-t2d44.1-0 ; 
       gen_skycrane_tL-t2d45.1-0 ; 
       gen_skycrane_tL-t2d46.1-0 ; 
       gen_skycrane_tL-t2d47.1-0 ; 
       gen_skycrane_tL-t2d48.1-0 ; 
       gen_skycrane_tL-t2d50.1-0 ; 
       gen_skycrane_tL-t2d51.1-0 ; 
       gen_skycrane_tL-t2d53.1-0 ; 
       gen_skycrane_tL-t2d54.1-0 ; 
       gen_skycrane_tL-t2d55.1-0 ; 
       gen_skycrane_tL-t2d56.1-0 ; 
       gen_skycrane_tL-t2d57.1-0 ; 
       gen_skycrane_tL-t2d58.1-0 ; 
       gen_skycrane_tL-t2d59.1-0 ; 
       gen_skycrane_tL-t2d6.1-0 ; 
       gen_skycrane_tL-t2d60.1-0 ; 
       gen_skycrane_tL-t2d61.1-0 ; 
       gen_skycrane_tL-t2d62.1-0 ; 
       gen_skycrane_tL-t2d63.1-0 ; 
       gen_skycrane_tL-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 12 110 ; 
       8 12 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 27 110 ; 
       13 12 110 ; 
       14 29 110 ; 
       15 12 110 ; 
       16 29 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       28 12 110 ; 
       29 12 110 ; 
       0 6 110 ; 
       0 0 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       0 0 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       1 6 110 ; 
       1 1 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       1 1 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       2 6 110 ; 
       2 2 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       2 2 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       3 6 110 ; 
       3 3 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       3 3 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       4 6 110 ; 
       4 4 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       4 4 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       5 6 110 ; 
       5 5 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       5 5 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 8 300 ; 
       9 2 300 ; 
       9 11 300 ; 
       9 16 300 ; 
       10 1 300 ; 
       10 9 300 ; 
       10 15 300 ; 
       11 0 300 ; 
       11 10 300 ; 
       11 14 300 ; 
       12 3 300 ; 
       12 33 300 ; 
       12 4 300 ; 
       12 6 300 ; 
       12 17 300 ; 
       12 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 25 300 ; 
       12 26 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       12 30 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       14 7 300 ; 
       14 13 300 ; 
       16 5 300 ; 
       16 12 300 ; 
       17 35 300 ; 
       18 36 300 ; 
       19 40 300 ; 
       20 39 300 ; 
       21 41 300 ; 
       22 42 300 ; 
       23 34 300 ; 
       24 37 300 ; 
       25 38 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       27 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 31 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 4 401 ; 
       5 0 401 ; 
       6 5 401 ; 
       7 3 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 10.5 -23.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 10.5 -11.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 14 -9.578308 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 14 -11.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 14 -13.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7 -19.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10.5 -21.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 14 -19.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 10.5 -25.57831 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 14 -17.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 10.5 -15.57831 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 10.5 -7.578308 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 10.5 -31.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 10.5 -33.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 10.5 -35.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 10.5 -37.57831 0 WIRECOL 6 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 10.5 -5.578308 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 10.5 -1.578308 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 10.5 -3.578308 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 10.5 -29.57831 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 3.5 -19.57831 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 1.670618 MPRFLG 0 ; 
       28 SCHEM 10.5 -27.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 10.5 -18.57831 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 16.5 1.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 16.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 16.5 -4.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 16.5 -2.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 16.5 3.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 16.5 -6.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 13 -1.5 0 DISPLAY 0 0 SRT 1.999152 1.999152 1.999152 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       39 SCHEM 14 -33.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -31.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 14 -35.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -37.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -17.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -19.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -7.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -17.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -19.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -13.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -11.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -9.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 10.5 0.1716919 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -5.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -15.82831 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -7.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -1.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -3.828308 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21 -17.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21 -19.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21 -17.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21 -19.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21 -11.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21 -9.828308 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 0.1716919 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21 -13.82831 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 7 0.1716919 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
