SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       pass_cont_F-cam_int1.28-0 ROOT ; 
       pass_cont_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       pass_cont_F-light1.28-0 ROOT ; 
       pass_cont_F-light2.28-0 ROOT ; 
       pass_cont_F-light3.28-0 ROOT ; 
       pass_cont_F-light4.28-0 ROOT ; 
       STATIC-light5.1-0 ROOT ; 
       STATIC-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       gen_skycrane_tL-mat11.3-0 ; 
       gen_skycrane_tL-mat13.3-0 ; 
       gen_skycrane_tL-mat15.3-0 ; 
       gen_skycrane_tL-mat2.8-0 ; 
       gen_skycrane_tL-mat23.8-0 ; 
       gen_skycrane_tL-mat3.3-0 ; 
       gen_skycrane_tL-mat37.8-0 ; 
       gen_skycrane_tL-mat4.3-0 ; 
       gen_skycrane_tL-mat42.3-0 ; 
       gen_skycrane_tL-mat43.3-0 ; 
       gen_skycrane_tL-mat44.3-0 ; 
       gen_skycrane_tL-mat45.3-0 ; 
       gen_skycrane_tL-mat46.3-0 ; 
       gen_skycrane_tL-mat47.3-0 ; 
       gen_skycrane_tL-mat48.3-0 ; 
       gen_skycrane_tL-mat49.3-0 ; 
       gen_skycrane_tL-mat50.3-0 ; 
       gen_skycrane_tL-mat51.8-0 ; 
       gen_skycrane_tL-mat52.8-0 ; 
       gen_skycrane_tL-mat53.8-0 ; 
       gen_skycrane_tL-mat55.8-0 ; 
       gen_skycrane_tL-mat56.8-0 ; 
       gen_skycrane_tL-mat58.6-0 ; 
       gen_skycrane_tL-mat59.6-0 ; 
       gen_skycrane_tL-mat60.8-0 ; 
       gen_skycrane_tL-mat61.8-0 ; 
       gen_skycrane_tL-mat62.7-0 ; 
       gen_skycrane_tL-mat63.7-0 ; 
       gen_skycrane_tL-mat64.8-0 ; 
       gen_skycrane_tL-mat65.8-0 ; 
       gen_skycrane_tL-mat66.8-0 ; 
       gen_skycrane_tL-mat67.8-0 ; 
       gen_skycrane_tL-mat68.8-0 ; 
       gen_skycrane_tL-mat9.8-0 ; 
       pass_cont_F-mat35.3-0 ; 
       pass_cont_F-mat43.3-0 ; 
       pass_cont_F-mat44.3-0 ; 
       pass_cont_F-mat45.3-0 ; 
       pass_cont_F-mat53.3-0 ; 
       STATIC-mat69.1-0 ; 
       STATIC-mat70.1-0 ; 
       STATIC-mat71.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       STATIC-antenn1.1-0 ; 
       STATIC-antenn2.1-0 ; 
       STATIC-engine0.1-0 ; 
       STATIC-engine1.1-0 ; 
       STATIC-engine2.1-0 ; 
       STATIC-engine3.1-0 ; 
       STATIC-fuselg.1-0 ; 
       STATIC-lwingzz.1-0 ; 
       STATIC-rwingzz.1-0 ; 
       STATIC-utl18a.1-0 ROOT ; 
       STATIC-wingzz0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl18a/PICTURES/utl18 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl18a-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       gen_skycrane_tL-t2d1.5-0 ; 
       gen_skycrane_tL-t2d10.7-0 ; 
       gen_skycrane_tL-t2d12.7-0 ; 
       gen_skycrane_tL-t2d2.5-0 ; 
       gen_skycrane_tL-t2d20.15-0 ; 
       gen_skycrane_tL-t2d34.15-0 ; 
       gen_skycrane_tL-t2d38.7-0 ; 
       gen_skycrane_tL-t2d39.7-0 ; 
       gen_skycrane_tL-t2d40.7-0 ; 
       gen_skycrane_tL-t2d41.5-0 ; 
       gen_skycrane_tL-t2d42.5-0 ; 
       gen_skycrane_tL-t2d43.7-0 ; 
       gen_skycrane_tL-t2d44.7-0 ; 
       gen_skycrane_tL-t2d45.7-0 ; 
       gen_skycrane_tL-t2d46.15-0 ; 
       gen_skycrane_tL-t2d47.15-0 ; 
       gen_skycrane_tL-t2d48.15-0 ; 
       gen_skycrane_tL-t2d50.15-0 ; 
       gen_skycrane_tL-t2d51.15-0 ; 
       gen_skycrane_tL-t2d53.13-0 ; 
       gen_skycrane_tL-t2d54.13-0 ; 
       gen_skycrane_tL-t2d55.15-0 ; 
       gen_skycrane_tL-t2d56.15-0 ; 
       gen_skycrane_tL-t2d57.14-0 ; 
       gen_skycrane_tL-t2d58.14-0 ; 
       gen_skycrane_tL-t2d59.15-0 ; 
       gen_skycrane_tL-t2d6.15-0 ; 
       gen_skycrane_tL-t2d60.15-0 ; 
       gen_skycrane_tL-t2d61.15-0 ; 
       gen_skycrane_tL-t2d62.15-0 ; 
       gen_skycrane_tL-t2d63.15-0 ; 
       gen_skycrane_tL-t2d8.7-0 ; 
       pass_cont_F-t2d40.3-0 ; 
       pass_cont_F-t2d41.3-0 ; 
       pass_cont_F-t2d42.3-0 ; 
       pass_cont_F-t2d50.5-0 ; 
       STATIC-t2d64.1-0 ; 
       STATIC-t2d65.1-0 ; 
       STATIC-t2d66.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 0 110 ; 
       2 6 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 9 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       10 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 34 300 ; 
       0 38 300 ; 
       1 34 300 ; 
       1 35 300 ; 
       1 36 300 ; 
       1 37 300 ; 
       2 8 300 ; 
       3 2 300 ; 
       3 11 300 ; 
       3 16 300 ; 
       4 1 300 ; 
       4 9 300 ; 
       4 15 300 ; 
       5 0 300 ; 
       5 10 300 ; 
       5 14 300 ; 
       6 3 300 ; 
       6 33 300 ; 
       6 4 300 ; 
       6 6 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       6 39 300 ; 
       6 40 300 ; 
       6 41 300 ; 
       7 7 300 ; 
       7 13 300 ; 
       8 5 300 ; 
       8 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       39 36 401 ; 
       40 37 401 ; 
       41 38 401 ; 
       0 31 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       4 4 401 ; 
       5 0 401 ; 
       6 5 401 ; 
       7 3 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       31 29 401 ; 
       32 30 401 ; 
       33 26 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       4 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0.5599859 MPRFLG 0 ; 
       10 SCHEM 11.25 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       39 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.3821 -12.00087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 58.999 -12.16581 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50.2118 -11.40961 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 47.5715 -11.3592 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 15.31995 -6.72872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       36 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 56.3821 -14.00087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 58.999 -14.16581 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 50.2118 -13.40961 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 47.5715 -13.3592 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 25 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
