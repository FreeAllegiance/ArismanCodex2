SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.38-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat25.2-0 ; 
       forklift_t-mat26.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       STATIC-mat104.1-0 ; 
       STATIC-mat108.1-0 ; 
       STATIC-mat109.1-0 ; 
       STATIC-mat110.1-0 ; 
       STATIC-mat111.1-0 ; 
       STATIC-mat44.1-0 ; 
       STATIC-mat45.1-0 ; 
       STATIC-mat46.1-0 ; 
       STATIC-mat47.1-0 ; 
       STATIC-mat48.1-0 ; 
       STATIC-mat49.1-0 ; 
       STATIC-mat53.1-0 ; 
       STATIC-mat66.1-0 ; 
       STATIC-mat75.1-0 ; 
       utl12_wtext-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights11.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-utl19_1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       forklift_t-t2d1.6-0 ; 
       forklift_t-t2d11.4-0 ; 
       forklift_t-t2d19.4-0 ; 
       forklift_t-t2d2.6-0 ; 
       forklift_t-t2d20.4-0 ; 
       forklift_t-t2d22.4-0 ; 
       forklift_t-t2d23.4-0 ; 
       forklift_t-t2d24.3-0 ; 
       forklift_t-t2d25.3-0 ; 
       forklift_t-t2d3.6-0 ; 
       forklift_T-t2d34.6-0 ; 
       forklift_T-t2d35.6-0 ; 
       forklift_t-t2d4.6-0 ; 
       forklift_t-t2d6.6-0 ; 
       forklift_t-t2d8.6-0 ; 
       gen_forklift_T-t2d36.3-0 ; 
       gen_forklift_T-t2d37.3-0 ; 
       STATIC-t2d100.1-0 ; 
       STATIC-t2d101.1-0 ; 
       STATIC-t2d102.1-0 ; 
       STATIC-t2d103.1-0 ; 
       STATIC-t2d38.1-0 ; 
       STATIC-t2d39.1-0 ; 
       STATIC-t2d40.1-0 ; 
       STATIC-t2d41.1-0 ; 
       STATIC-t2d42.1-0 ; 
       STATIC-t2d43.1-0 ; 
       STATIC-t2d92.1-0 ; 
       STATIC-t2d95.1-0 ; 
       STATIC-t2d96.1-0 ; 
       STATIC-t2d97.1-0 ; 
       STATIC-t2d98.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 2 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 8 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 2 110 ; 
       14 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 33 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       1 23 300 ; 
       2 0 300 ; 
       2 2 300 ; 
       2 10 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 11 300 ; 
       2 13 300 ; 
       2 19 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       4 20 300 ; 
       5 30 300 ; 
       6 32 300 ; 
       7 31 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       10 8 300 ; 
       11 17 300 ; 
       11 18 300 ; 
       11 16 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       13 9 300 ; 
       14 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 12 401 ; 
       13 11 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 16 401 ; 
       18 15 401 ; 
       19 27 401 ; 
       20 29 401 ; 
       21 18 401 ; 
       22 19 401 ; 
       23 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 31 401 ; 
       31 28 401 ; 
       32 30 401 ; 
       33 17 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -6 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
