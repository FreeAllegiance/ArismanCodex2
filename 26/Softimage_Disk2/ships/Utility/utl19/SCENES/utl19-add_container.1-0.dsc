SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 11     
       add_container-mat63Mat.1-0 ; 
       add_container-mat64Mat.1-0 ; 
       add_container-mat65Mat.1-0 ; 
       add_container-mat70Mat.1-0 ; 
       add_container-mat71Mat.1-0 ; 
       add_container-mat72Mat.1-0 ; 
       add_container-mat78Mat.1-0 ; 
       add_container-mat79Mat.1-0 ; 
       add_container-mat80Mat.1-0 ; 
       add_container-mat81Mat.1-0 ; 
       utl19-utl19_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.1-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       forklift_t-light1_2_1_1.4-0 ROOT ; 
       forklift_t-light2_2_1_1.4-0 ROOT ; 
       tanker_F-light1.1-0 ROOT ; 
       tanker_F-light2.1-0 ROOT ; 
       tanker_F-light3.1-0 ROOT ; 
       tanker_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 97     
       add_container-blue-cop_light.1-0.1-0 ; 
       add_container-blue-cop_light.1-1.1-0 ; 
       add_container-blue-cop_light.1-2.1-0 ; 
       add_container-blue-cop_light.1-3.1-0 ; 
       add_container-blue-cop_light.1-4.1-0 ; 
       add_container-blue-cop_light.1-5.1-0 ; 
       add_container-blue-cop_light.1-6.1-0 ; 
       add_container-cop_light.1-0 ; 
       add_container-cop_light1.1-0 ; 
       add_container-fork-amb_on.1-0.1-0 ; 
       add_container-mat101.1-0 ; 
       add_container-mat102.1-0 ; 
       add_container-mat103.1-0 ; 
       add_container-mat104.1-0 ; 
       add_container-mat105.1-0 ; 
       add_container-mat106.1-0 ; 
       add_container-mat107.1-0 ; 
       add_container-mat44.1-0 ; 
       add_container-mat45.1-0 ; 
       add_container-mat46.1-0 ; 
       add_container-mat47.1-0 ; 
       add_container-mat48.1-0 ; 
       add_container-mat49.1-0 ; 
       add_container-mat53.1-0 ; 
       add_container-mat55.1-0 ; 
       add_container-mat63.1-0 ; 
       add_container-mat64.1-0 ; 
       add_container-mat65.1-0 ; 
       add_container-mat66.1-0 ; 
       add_container-mat67.1-0 ; 
       add_container-mat68.1-0 ; 
       add_container-mat69.1-0 ; 
       add_container-mat70.1-0 ; 
       add_container-mat71.1-0 ; 
       add_container-mat72.1-0 ; 
       add_container-mat75.1-0 ; 
       add_container-mat77.1-0 ; 
       add_container-mat78.1-0 ; 
       add_container-mat79.1-0 ; 
       add_container-mat80.1-0 ; 
       add_container-mat81.1-0 ; 
       add_container-mat82.1-0 ; 
       add_container-mat83.1-0 ; 
       add_container-mat84.1-0 ; 
       add_container-mat89.1-0 ; 
       add_container-mat91.1-0 ; 
       add_container-mat92.1-0 ; 
       add_container-mat96.1-0 ; 
       add_container-nose_white-center.1-0.1-0 ; 
       add_container-nose_white-center.1-1.1-0 ; 
       add_container-orange-cop_light.1-0.1-0 ; 
       add_container-orange-cop_light.1-1.1-0 ; 
       add_container-orange-cop_light.1-2.1-0 ; 
       add_container-orange-cop_light.1-3.1-0 ; 
       add_container-orange-cop_light.1-4.1-0 ; 
       add_container-orange-cop_light.1-5.1-0 ; 
       add_container-orange-cop_light.1-6.1-0 ; 
       add_container-port_red-left.1-0.1-0 ; 
       add_container-starbord_green-right.1-0.1-0 ; 
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat25.2-0 ; 
       forklift_t-mat26.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat29.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_t-mat30.2-0 ; 
       forklift_t-mat31.2-0 ; 
       forklift_t-mat32.2-0 ; 
       forklift_t-mat33.2-0 ; 
       forklift_t-mat34.2-0 ; 
       forklift_t-mat35.2-0 ; 
       forklift_t-mat36.2-0 ; 
       forklift_t-mat37.2-0 ; 
       forklift_t-mat38.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       tanker_F-mat9.1-0 ; 
       utl12_wtext-mat1.1-0 ; 
       utl12_wtext-mat2.1-0 ; 
       utl12_wtext-mat3.1-0 ; 
       utl12_wtext-mat4.1-0 ; 
       utl12_wtext-mat5.1-0 ; 
       utl12_wtext-mat6.1-0 ; 
       utl12_wtext-mat7.1-0 ; 
       utl12_wtext-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       utl19-acrgatt.1-0 ; 
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lights0.1-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights2.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lights6.1-0 ; 
       utl19-lights7.1-0 ; 
       utl19-lights8.1-0 ; 
       utl19-lights9.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-SSa.1-0 ; 
       utl19-SSf.1-0 ; 
       utl19-SSglow1.1-0 ; 
       utl19-SSglow2.1-0 ; 
       utl19-SSglow3.1-0 ; 
       utl19-SSglow4.1-0 ; 
       utl19-SSl.1-0 ; 
       utl19-SSr.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-utl19_1.4-0 ROOT ; 
       utl19-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl12 ; 
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-add_container.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 79     
       add_container-t2d38.1-0 ; 
       add_container-t2d39.1-0 ; 
       add_container-t2d40.1-0 ; 
       add_container-t2d41.1-0 ; 
       add_container-t2d42.1-0 ; 
       add_container-t2d43.1-0 ; 
       add_container-t2d47.1-0 ; 
       add_container-t2d53.1-0 ; 
       add_container-t2d54.1-0 ; 
       add_container-t2d55.1-0 ; 
       add_container-t2d56.1-0 ; 
       add_container-t2d57.1-0 ; 
       add_container-t2d58.1-0 ; 
       add_container-t2d59.1-0 ; 
       add_container-t2d60.1-0 ; 
       add_container-t2d61.1-0 ; 
       add_container-t2d65.1-0 ; 
       add_container-t2d66.1-0 ; 
       add_container-t2d67.1-0 ; 
       add_container-t2d68.1-0 ; 
       add_container-t2d69.1-0 ; 
       add_container-t2d70.1-0 ; 
       add_container-t2d71.1-0 ; 
       add_container-t2d72.1-0 ; 
       add_container-t2d73.1-0 ; 
       add_container-t2d74.1-0 ; 
       add_container-t2d75.1-0 ; 
       add_container-t2d76.1-0 ; 
       add_container-t2d77.1-0 ; 
       add_container-t2d78.1-0 ; 
       add_container-t2d79.1-0 ; 
       add_container-t2d80.1-0 ; 
       add_container-t2d81.1-0 ; 
       add_container-t2d82.1-0 ; 
       add_container-t2d83.1-0 ; 
       add_container-t2d84.1-0 ; 
       add_container-t2d85.1-0 ; 
       add_container-t2d86.1-0 ; 
       add_container-t2d87.1-0 ; 
       add_container-t2d88.1-0 ; 
       add_container-t2d89.1-0 ; 
       add_container-t2d90.1-0 ; 
       add_container-t2d91.1-0 ; 
       add_container-t2d92.1-0 ; 
       add_container-t2d93.1-0 ; 
       add_container-t2d94.1-0 ; 
       forklift_t-t2d1.3-0 ; 
       forklift_t-t2d11.2-0 ; 
       forklift_t-t2d19.2-0 ; 
       forklift_t-t2d2.3-0 ; 
       forklift_t-t2d20.2-0 ; 
       forklift_t-t2d22.2-0 ; 
       forklift_t-t2d23.2-0 ; 
       forklift_t-t2d24.2-0 ; 
       forklift_t-t2d25.2-0 ; 
       forklift_t-t2d26.2-0 ; 
       forklift_t-t2d27.2-0 ; 
       forklift_t-t2d28.2-0 ; 
       forklift_t-t2d29.2-0 ; 
       forklift_t-t2d3.3-0 ; 
       forklift_t-t2d30.2-0 ; 
       forklift_t-t2d31.2-0 ; 
       forklift_t-t2d32.2-0 ; 
       forklift_t-t2d33.2-0 ; 
       forklift_T-t2d34.3-0 ; 
       forklift_T-t2d35.3-0 ; 
       forklift_t-t2d4.3-0 ; 
       forklift_t-t2d6.3-0 ; 
       forklift_t-t2d8.3-0 ; 
       gen_forklift_T-t2d36.2-0 ; 
       gen_forklift_T-t2d37.2-0 ; 
       tanker_F-t2d8.1-0 ; 
       tanker_F-t2d9.1-0 ; 
       utl12_wtext-t2d1.1-0 ; 
       utl12_wtext-t2d2.1-0 ; 
       utl12_wtext-t2d4.1-0 ; 
       utl12_wtext-t2d5.1-0 ; 
       utl12_wtext-t2d6.1-0 ; 
       utl12_wtext-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 28 110 ; 
       3 29 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 3 110 ; 
       15 14 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 17 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 6 110 ; 
       22 7 110 ; 
       23 8 110 ; 
       24 9 110 ; 
       25 15 110 ; 
       26 18 110 ; 
       27 3 110 ; 
       28 27 110 ; 
       30 3 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 59 300 ; 
       3 61 300 ; 
       3 70 300 ; 
       3 81 300 ; 
       3 83 300 ; 
       3 84 300 ; 
       3 80 300 ; 
       3 82 300 ; 
       3 13 300 ; 
       5 65 300 ; 
       5 66 300 ; 
       6 9 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 14 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       7 23 300 ; 
       7 24 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       8 35 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       9 28 300 ; 
       9 29 300 ; 
       9 30 300 ; 
       9 31 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       10 69 300 ; 
       10 71 300 ; 
       11 74 300 ; 
       11 75 300 ; 
       11 76 300 ; 
       12 72 300 ; 
       12 73 300 ; 
       13 77 300 ; 
       13 78 300 ; 
       13 79 300 ; 
       14 62 300 ; 
       14 63 300 ; 
       14 64 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       16 67 300 ; 
       17 86 300 ; 
       17 87 300 ; 
       17 85 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       18 22 300 ; 
       19 48 300 ; 
       20 49 300 ; 
       21 7 300 ; 
       21 41 300 ; 
       21 42 300 ; 
       21 43 300 ; 
       21 56 300 ; 
       21 51 300 ; 
       21 52 300 ; 
       22 8 300 ; 
       22 44 300 ; 
       22 1 300 ; 
       22 45 300 ; 
       22 46 300 ; 
       22 6 300 ; 
       22 2 300 ; 
       23 0 300 ; 
       23 47 300 ; 
       23 3 300 ; 
       23 5 300 ; 
       23 4 300 ; 
       24 50 300 ; 
       24 10 300 ; 
       24 11 300 ; 
       24 12 300 ; 
       24 55 300 ; 
       24 53 300 ; 
       24 54 300 ; 
       25 57 300 ; 
       26 58 300 ; 
       27 68 300 ; 
       28 60 300 ; 
       2 89 300 ; 
       2 90 300 ; 
       2 91 300 ; 
       2 92 300 ; 
       2 93 300 ; 
       2 94 300 ; 
       2 95 300 ; 
       2 96 300 ; 
       2 88 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 10 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       59 46 401 ; 
       60 47 401 ; 
       61 49 401 ; 
       63 48 401 ; 
       64 50 401 ; 
       65 51 401 ; 
       66 52 401 ; 
       67 53 401 ; 
       68 54 401 ; 
       69 55 401 ; 
       70 59 401 ; 
       71 56 401 ; 
       72 57 401 ; 
       73 58 401 ; 
       75 60 401 ; 
       76 61 401 ; 
       78 62 401 ; 
       79 63 401 ; 
       80 64 401 ; 
       81 66 401 ; 
       82 65 401 ; 
       83 67 401 ; 
       84 68 401 ; 
       85 70 401 ; 
       87 69 401 ; 
       1 28 401 ; 
       2 32 401 ; 
       3 34 401 ; 
       4 36 401 ; 
       5 35 401 ; 
       6 31 401 ; 
       10 37 401 ; 
       11 38 401 ; 
       12 39 401 ; 
       13 43 401 ; 
       15 44 401 ; 
       16 45 401 ; 
       17 0 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 5 401 ; 
       24 6 401 ; 
       25 7 401 ; 
       26 8 401 ; 
       27 9 401 ; 
       29 10 401 ; 
       30 11 401 ; 
       31 12 401 ; 
       32 13 401 ; 
       33 14 401 ; 
       34 15 401 ; 
       36 16 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 19 401 ; 
       40 20 401 ; 
       41 21 401 ; 
       42 22 401 ; 
       43 23 401 ; 
       44 27 401 ; 
       45 29 401 ; 
       46 30 401 ; 
       47 33 401 ; 
       51 25 401 ; 
       52 26 401 ; 
       53 41 401 ; 
       54 42 401 ; 
       55 40 401 ; 
       56 24 401 ; 
       88 71 401 ; 
       90 73 401 ; 
       91 74 401 ; 
       92 72 401 ; 
       93 75 401 ; 
       94 76 401 ; 
       95 77 401 ; 
       96 78 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER ANIMATION 
       25 0 15003 ; 
       26 1 15003 ; 
       27 2 15003 ; 
       32 3 15003 ; 
       33 4 15003 ; 
       34 5 15003 ; 
       37 6 15003 ; 
       38 7 15003 ; 
       39 8 15003 ; 
       40 9 15003 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 40 -8 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 30 -4 0 MPRFLG 0 ; 
       17 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 25 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 20 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 2.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 40 -4 0 MPRFLG 0 ; 
       28 SCHEM 40 -6 0 MPRFLG 0 ; 
       29 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       59 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       46 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       72 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       73 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       74 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       75 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       76 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       77 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       78 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 26.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 24 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 19 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 19 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 44 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
