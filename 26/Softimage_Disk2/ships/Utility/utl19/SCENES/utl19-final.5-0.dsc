SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl19-utl19_1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.11-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 74     
       final-blue-cop_light.1-0.1-0 ; 
       final-blue-cop_light.1-1.1-0 ; 
       final-blue-cop_light.1-2.1-0 ; 
       final-blue-cop_light.1-3.1-0 ; 
       final-blue-cop_light.1-4.1-0 ; 
       final-blue-cop_light.1-5.1-0 ; 
       final-blue-cop_light.1-6.1-0 ; 
       final-cop_light.1-0 ; 
       final-cop_light1.1-0 ; 
       final-mat101.1-0 ; 
       final-mat102.1-0 ; 
       final-mat103.1-0 ; 
       final-mat104.1-0 ; 
       final-mat108.1-0 ; 
       final-mat109.1-0 ; 
       final-mat110.1-0 ; 
       final-mat111.1-0 ; 
       final-mat44.1-0 ; 
       final-mat45.1-0 ; 
       final-mat46.1-0 ; 
       final-mat47.1-0 ; 
       final-mat48.1-0 ; 
       final-mat49.1-0 ; 
       final-mat53.1-0 ; 
       final-mat66.1-0 ; 
       final-mat75.1-0 ; 
       final-mat82.1-0 ; 
       final-mat83.1-0 ; 
       final-mat84.1-0 ; 
       final-mat89.1-0 ; 
       final-mat91.1-0 ; 
       final-mat92.1-0 ; 
       final-mat96.1-0 ; 
       final-nose_white-center.1-0.1-0 ; 
       final-nose_white-center.1-1.1-0 ; 
       final-orange-cop_light.1-0.1-0 ; 
       final-orange-cop_light.1-1.1-0 ; 
       final-orange-cop_light.1-2.1-0 ; 
       final-orange-cop_light.1-3.1-0 ; 
       final-orange-cop_light.1-4.1-0 ; 
       final-orange-cop_light.1-5.1-0 ; 
       final-orange-cop_light.1-6.1-0 ; 
       final-port_red-left.1-0.1-0 ; 
       final-starbord_green-right.1-0.1-0 ; 
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat25.2-0 ; 
       forklift_t-mat26.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat29.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_t-mat30.2-0 ; 
       forklift_t-mat31.2-0 ; 
       forklift_t-mat32.2-0 ; 
       forklift_t-mat33.2-0 ; 
       forklift_t-mat34.2-0 ; 
       forklift_t-mat35.2-0 ; 
       forklift_t-mat36.2-0 ; 
       forklift_t-mat37.2-0 ; 
       forklift_t-mat38.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       utl12_wtext-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       utl19-acrgatt.1-0 ; 
       utl19-cockpt.1-0 ; 
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lights0.1-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights11.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lights6.1-0 ; 
       utl19-lights7.1-0 ; 
       utl19-lights8.1-0 ; 
       utl19-lights9.1-0 ; 
       utl19-lthrust.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-SSa.1-0 ; 
       utl19-SSf.1-0 ; 
       utl19-SSglow1.1-0 ; 
       utl19-SSglow2.1-0 ; 
       utl19-SSglow3.1-0 ; 
       utl19-SSglow4.1-0 ; 
       utl19-SSl.1-0 ; 
       utl19-SSr.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-trail.1-0 ; 
       utl19-ttrail.1-0 ; 
       utl19-utl19_1.11-0 ROOT ; 
       utl19-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-final.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 62     
       final-t2d100.2-0 ; 
       final-t2d101.2-0 ; 
       final-t2d102.2-0 ; 
       final-t2d103.2-0 ; 
       final-t2d38.1-0 ; 
       final-t2d39.1-0 ; 
       final-t2d40.1-0 ; 
       final-t2d41.1-0 ; 
       final-t2d42.1-0 ; 
       final-t2d43.1-0 ; 
       final-t2d70.1-0 ; 
       final-t2d71.1-0 ; 
       final-t2d72.1-0 ; 
       final-t2d73.1-0 ; 
       final-t2d74.1-0 ; 
       final-t2d75.1-0 ; 
       final-t2d76.1-0 ; 
       final-t2d77.1-0 ; 
       final-t2d78.1-0 ; 
       final-t2d79.1-0 ; 
       final-t2d80.1-0 ; 
       final-t2d81.1-0 ; 
       final-t2d82.1-0 ; 
       final-t2d83.1-0 ; 
       final-t2d84.1-0 ; 
       final-t2d85.1-0 ; 
       final-t2d86.1-0 ; 
       final-t2d87.1-0 ; 
       final-t2d88.1-0 ; 
       final-t2d89.1-0 ; 
       final-t2d90.1-0 ; 
       final-t2d91.1-0 ; 
       final-t2d92.1-0 ; 
       final-t2d95.1-0 ; 
       final-t2d96.1-0 ; 
       final-t2d97.1-0 ; 
       final-t2d98.1-0 ; 
       forklift_t-t2d1.4-0 ; 
       forklift_t-t2d11.3-0 ; 
       forklift_t-t2d19.2-0 ; 
       forklift_t-t2d2.4-0 ; 
       forklift_t-t2d20.2-0 ; 
       forklift_t-t2d22.3-0 ; 
       forklift_t-t2d23.3-0 ; 
       forklift_t-t2d24.2-0 ; 
       forklift_t-t2d25.2-0 ; 
       forklift_t-t2d26.2-0 ; 
       forklift_t-t2d27.2-0 ; 
       forklift_t-t2d28.2-0 ; 
       forklift_t-t2d29.2-0 ; 
       forklift_t-t2d3.4-0 ; 
       forklift_t-t2d30.2-0 ; 
       forklift_t-t2d31.2-0 ; 
       forklift_t-t2d32.2-0 ; 
       forklift_t-t2d33.2-0 ; 
       forklift_T-t2d34.4-0 ; 
       forklift_T-t2d35.4-0 ; 
       forklift_t-t2d4.4-0 ; 
       forklift_t-t2d6.4-0 ; 
       forklift_t-t2d8.4-0 ; 
       gen_forklift_T-t2d36.2-0 ; 
       gen_forklift_T-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 31 110 ; 
       4 34 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       32 4 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 4 110 ; 
       20 4 110 ; 
       21 20 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 7 110 ; 
       25 8 110 ; 
       26 9 110 ; 
       27 10 110 ; 
       28 17 110 ; 
       29 21 110 ; 
       30 4 110 ; 
       31 30 110 ; 
       35 4 110 ; 
       1 4 110 ; 
       3 4 110 ; 
       7 6 110 ; 
       15 4 110 ; 
       19 4 110 ; 
       33 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 44 300 ; 
       4 46 300 ; 
       4 55 300 ; 
       4 66 300 ; 
       4 68 300 ; 
       4 69 300 ; 
       4 65 300 ; 
       4 67 300 ; 
       4 12 300 ; 
       6 50 300 ; 
       6 51 300 ; 
       8 23 300 ; 
       9 25 300 ; 
       10 24 300 ; 
       11 54 300 ; 
       11 56 300 ; 
       12 59 300 ; 
       12 60 300 ; 
       12 61 300 ; 
       13 57 300 ; 
       13 58 300 ; 
       14 62 300 ; 
       14 63 300 ; 
       14 64 300 ; 
       16 47 300 ; 
       16 48 300 ; 
       16 49 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       18 52 300 ; 
       20 71 300 ; 
       20 72 300 ; 
       20 70 300 ; 
       21 20 300 ; 
       21 21 300 ; 
       21 22 300 ; 
       22 33 300 ; 
       23 34 300 ; 
       24 7 300 ; 
       24 26 300 ; 
       24 27 300 ; 
       24 28 300 ; 
       24 41 300 ; 
       24 36 300 ; 
       24 37 300 ; 
       25 8 300 ; 
       25 29 300 ; 
       25 1 300 ; 
       25 30 300 ; 
       25 31 300 ; 
       25 6 300 ; 
       25 2 300 ; 
       26 0 300 ; 
       26 32 300 ; 
       26 3 300 ; 
       26 5 300 ; 
       26 4 300 ; 
       27 35 300 ; 
       27 9 300 ; 
       27 10 300 ; 
       27 11 300 ; 
       27 40 300 ; 
       27 38 300 ; 
       27 39 300 ; 
       28 42 300 ; 
       29 43 300 ; 
       30 53 300 ; 
       31 45 300 ; 
       3 73 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       7 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       34 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       44 37 401 ; 
       45 38 401 ; 
       46 40 401 ; 
       48 39 401 ; 
       49 41 401 ; 
       50 42 401 ; 
       51 43 401 ; 
       52 44 401 ; 
       53 45 401 ; 
       54 46 401 ; 
       55 50 401 ; 
       56 47 401 ; 
       57 48 401 ; 
       58 49 401 ; 
       60 51 401 ; 
       61 52 401 ; 
       63 53 401 ; 
       64 54 401 ; 
       65 55 401 ; 
       66 57 401 ; 
       67 56 401 ; 
       68 58 401 ; 
       69 59 401 ; 
       70 61 401 ; 
       72 60 401 ; 
       1 17 401 ; 
       2 21 401 ; 
       3 23 401 ; 
       4 25 401 ; 
       5 24 401 ; 
       6 20 401 ; 
       14 1 401 ; 
       9 26 401 ; 
       10 27 401 ; 
       11 28 401 ; 
       12 32 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 5 401 ; 
       19 6 401 ; 
       20 7 401 ; 
       21 8 401 ; 
       22 9 401 ; 
       23 36 401 ; 
       24 33 401 ; 
       13 34 401 ; 
       25 35 401 ; 
       26 10 401 ; 
       27 11 401 ; 
       28 12 401 ; 
       29 16 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 22 401 ; 
       36 14 401 ; 
       37 15 401 ; 
       38 30 401 ; 
       39 31 401 ; 
       40 29 401 ; 
       41 13 401 ; 
       73 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 40 -8 0 MPRFLG 0 ; 
       4 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       32 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -6 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 15 -6 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 30 -4 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 25 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 40 -4 0 MPRFLG 0 ; 
       31 SCHEM 40 -6 0 MPRFLG 0 ; 
       34 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 25 -6 0 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       44 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       37 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
