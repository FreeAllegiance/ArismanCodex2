SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl19-utl19_1.17-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.17-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       fix_strobes-mat104.1-0 ; 
       fix_strobes-mat108.1-0 ; 
       fix_strobes-mat109.1-0 ; 
       fix_strobes-mat110.1-0 ; 
       fix_strobes-mat111.1-0 ; 
       fix_strobes-mat44.1-0 ; 
       fix_strobes-mat45.1-0 ; 
       fix_strobes-mat46.1-0 ; 
       fix_strobes-mat47.1-0 ; 
       fix_strobes-mat48.1-0 ; 
       fix_strobes-mat49.1-0 ; 
       fix_strobes-mat53.1-0 ; 
       fix_strobes-mat66.1-0 ; 
       fix_strobes-mat75.1-0 ; 
       fix_strobes-nose_white-center.1-0.1-0 ; 
       fix_strobes-nose_white-center.1-1.1-0 ; 
       fix_strobes-port_red-left.1-0.1-0 ; 
       fix_strobes-port_red-left.1-1.1-0 ; 
       fix_strobes-port_red-left.1-2.1-0 ; 
       fix_strobes-port_red-left.1-3.1-0 ; 
       fix_strobes-port_red-left.1-4.1-0 ; 
       fix_strobes-starbord_green-right.1-0.1-0 ; 
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat25.2-0 ; 
       forklift_t-mat26.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat29.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_t-mat30.2-0 ; 
       forklift_t-mat31.2-0 ; 
       forklift_t-mat32.2-0 ; 
       forklift_t-mat33.2-0 ; 
       forklift_t-mat34.2-0 ; 
       forklift_t-mat35.2-0 ; 
       forklift_t-mat36.2-0 ; 
       forklift_t-mat37.2-0 ; 
       forklift_t-mat38.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       utl12_wtext-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       utl19-acrgatt.1-0 ; 
       utl19-cockpt.1-0 ; 
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lights0.1-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights11.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lights6.1-0 ; 
       utl19-lights7.1-0 ; 
       utl19-lights8.1-0 ; 
       utl19-lights9.1-0 ; 
       utl19-lthrust.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-SSa.1-0 ; 
       utl19-SSf.1-0 ; 
       utl19-SSl.1-0 ; 
       utl19-SSr.1-0 ; 
       utl19-SSt1_1.1-0 ; 
       utl19-SSt2.1-0 ; 
       utl19-SSt3.1-0 ; 
       utl19-SSt4.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-trail.1-0 ; 
       utl19-ttrail.1-0 ; 
       utl19-utl19_1.16-0 ROOT ; 
       utl19-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-fix_strobes.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       fix_strobes-t2d100.1-0 ; 
       fix_strobes-t2d101.1-0 ; 
       fix_strobes-t2d102.1-0 ; 
       fix_strobes-t2d103.1-0 ; 
       fix_strobes-t2d38.1-0 ; 
       fix_strobes-t2d39.1-0 ; 
       fix_strobes-t2d40.1-0 ; 
       fix_strobes-t2d41.1-0 ; 
       fix_strobes-t2d42.1-0 ; 
       fix_strobes-t2d43.1-0 ; 
       fix_strobes-t2d92.1-0 ; 
       fix_strobes-t2d95.1-0 ; 
       fix_strobes-t2d96.1-0 ; 
       fix_strobes-t2d97.1-0 ; 
       fix_strobes-t2d98.1-0 ; 
       forklift_t-t2d1.5-0 ; 
       forklift_t-t2d11.3-0 ; 
       forklift_t-t2d19.3-0 ; 
       forklift_t-t2d2.5-0 ; 
       forklift_t-t2d20.3-0 ; 
       forklift_t-t2d22.3-0 ; 
       forklift_t-t2d23.3-0 ; 
       forklift_t-t2d24.2-0 ; 
       forklift_t-t2d25.2-0 ; 
       forklift_t-t2d26.2-0 ; 
       forklift_t-t2d27.2-0 ; 
       forklift_t-t2d28.2-0 ; 
       forklift_t-t2d29.2-0 ; 
       forklift_t-t2d3.5-0 ; 
       forklift_t-t2d30.2-0 ; 
       forklift_t-t2d31.2-0 ; 
       forklift_t-t2d32.2-0 ; 
       forklift_t-t2d33.2-0 ; 
       forklift_T-t2d34.5-0 ; 
       forklift_T-t2d35.5-0 ; 
       forklift_t-t2d4.5-0 ; 
       forklift_t-t2d6.5-0 ; 
       forklift_t-t2d8.5-0 ; 
       gen_forklift_T-t2d36.2-0 ; 
       gen_forklift_T-t2d37.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 31 110 ; 
       3 4 110 ; 
       4 34 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 20 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 17 110 ; 
       25 21 110 ; 
       30 4 110 ; 
       31 30 110 ; 
       32 4 110 ; 
       33 4 110 ; 
       35 4 110 ; 
       26 8 110 ; 
       29 7 110 ; 
       28 10 110 ; 
       27 9 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 51 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       4 22 300 ; 
       4 24 300 ; 
       4 33 300 ; 
       4 44 300 ; 
       4 46 300 ; 
       4 47 300 ; 
       4 43 300 ; 
       4 45 300 ; 
       4 0 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       7 1 300 ; 
       8 11 300 ; 
       9 13 300 ; 
       10 12 300 ; 
       11 32 300 ; 
       11 34 300 ; 
       12 37 300 ; 
       12 38 300 ; 
       12 39 300 ; 
       13 35 300 ; 
       13 36 300 ; 
       14 40 300 ; 
       14 41 300 ; 
       14 42 300 ; 
       16 25 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       17 5 300 ; 
       17 6 300 ; 
       17 7 300 ; 
       18 30 300 ; 
       20 49 300 ; 
       20 50 300 ; 
       20 48 300 ; 
       21 8 300 ; 
       21 9 300 ; 
       21 10 300 ; 
       22 14 300 ; 
       23 15 300 ; 
       24 16 300 ; 
       25 21 300 ; 
       30 31 300 ; 
       31 23 300 ; 
       26 17 300 ; 
       29 18 300 ; 
       28 19 300 ; 
       27 20 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       34 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 12 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 14 401 ; 
       12 11 401 ; 
       13 13 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 18 401 ; 
       26 17 401 ; 
       27 19 401 ; 
       28 20 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       33 28 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       41 31 401 ; 
       42 32 401 ; 
       43 33 401 ; 
       44 35 401 ; 
       45 34 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 39 401 ; 
       50 38 401 ; 
       51 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 30 -8 0 MPRFLG 0 ; 
       1 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 20 -10 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 5 -10 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 10 -10 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 25 -8 0 MPRFLG 0 ; 
       17 SCHEM 25 -10 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 0 -8 0 MPRFLG 0 ; 
       21 SCHEM 0 -10 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 25 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 0 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       30 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.25 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       35 SCHEM 40 -8 0 MPRFLG 0 ; 
       26 SCHEM 15 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 20 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 17.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM -1 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 39 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 54 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
