SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 11     
       gen_forklift_F-mat63Mat.1-0 ; 
       gen_forklift_F-mat64Mat.1-0 ; 
       gen_forklift_F-mat65Mat.1-0 ; 
       gen_forklift_F-mat70Mat.1-0 ; 
       gen_forklift_F-mat71Mat.1-0 ; 
       gen_forklift_F-mat72Mat.1-0 ; 
       gen_forklift_F-mat78Mat.1-0 ; 
       gen_forklift_F-mat79Mat.1-0 ; 
       gen_forklift_F-mat80Mat.1-0 ; 
       gen_forklift_F-mat81Mat.1-0 ; 
       utl19-utl19.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       gen_forklift_F-cam_int1.1-0 ROOT ; 
       gen_forklift_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       forklift_t-light1_2_1.1-0 ROOT ; 
       forklift_t-light2_2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 88     
       forklift_t-mat1.1-0 ; 
       forklift_t-mat12.1-0 ; 
       forklift_t-mat2.1-0 ; 
       forklift_t-mat21.1-0 ; 
       forklift_t-mat22.1-0 ; 
       forklift_t-mat23.1-0 ; 
       forklift_t-mat25.1-0 ; 
       forklift_t-mat26.1-0 ; 
       forklift_t-mat27.1-0 ; 
       forklift_t-mat28.1-0 ; 
       forklift_t-mat29.1-0 ; 
       forklift_t-mat3.1-0 ; 
       forklift_t-mat30.1-0 ; 
       forklift_t-mat31.1-0 ; 
       forklift_t-mat32.1-0 ; 
       forklift_t-mat33.1-0 ; 
       forklift_t-mat34.1-0 ; 
       forklift_t-mat35.1-0 ; 
       forklift_t-mat36.1-0 ; 
       forklift_t-mat37.1-0 ; 
       forklift_t-mat38.1-0 ; 
       forklift_T-mat39.1-0 ; 
       forklift_t-mat4.1-0 ; 
       forklift_T-mat40.1-0 ; 
       forklift_t-mat6.1-0 ; 
       forklift_t-mat8.1-0 ; 
       gen_forklift_F-blue-cop_light.1-0.1-0 ; 
       gen_forklift_F-blue-cop_light.1-1.1-0 ; 
       gen_forklift_F-blue-cop_light.1-2.1-0 ; 
       gen_forklift_F-blue-cop_light.1-3.1-0 ; 
       gen_forklift_F-blue-cop_light.1-4.1-0 ; 
       gen_forklift_F-blue-cop_light.1-5.1-0 ; 
       gen_forklift_F-blue-cop_light.1-6.1-0 ; 
       gen_forklift_F-cop_light.1-0 ; 
       gen_forklift_F-cop_light1.1-0 ; 
       gen_forklift_F-fork-amb_on.1-0.1-0 ; 
       gen_forklift_F-mat101.1-0 ; 
       gen_forklift_F-mat102.1-0 ; 
       gen_forklift_F-mat103.1-0 ; 
       gen_forklift_F-mat104.1-0 ; 
       gen_forklift_F-mat105.1-0 ; 
       gen_forklift_F-mat106.1-0 ; 
       gen_forklift_F-mat107.1-0 ; 
       gen_forklift_F-mat44.1-0 ; 
       gen_forklift_F-mat45.1-0 ; 
       gen_forklift_F-mat46.1-0 ; 
       gen_forklift_F-mat47.1-0 ; 
       gen_forklift_F-mat48.1-0 ; 
       gen_forklift_F-mat49.1-0 ; 
       gen_forklift_F-mat53.1-0 ; 
       gen_forklift_F-mat55.1-0 ; 
       gen_forklift_F-mat63.1-0 ; 
       gen_forklift_F-mat64.1-0 ; 
       gen_forklift_F-mat65.1-0 ; 
       gen_forklift_F-mat66.1-0 ; 
       gen_forklift_F-mat67.1-0 ; 
       gen_forklift_F-mat68.1-0 ; 
       gen_forklift_F-mat69.1-0 ; 
       gen_forklift_F-mat70.1-0 ; 
       gen_forklift_F-mat71.1-0 ; 
       gen_forklift_F-mat72.1-0 ; 
       gen_forklift_F-mat75.1-0 ; 
       gen_forklift_F-mat77.1-0 ; 
       gen_forklift_F-mat78.1-0 ; 
       gen_forklift_F-mat79.1-0 ; 
       gen_forklift_F-mat80.1-0 ; 
       gen_forklift_F-mat81.1-0 ; 
       gen_forklift_F-mat82.1-0 ; 
       gen_forklift_F-mat83.1-0 ; 
       gen_forklift_F-mat84.1-0 ; 
       gen_forklift_F-mat89.1-0 ; 
       gen_forklift_F-mat91.1-0 ; 
       gen_forklift_F-mat92.1-0 ; 
       gen_forklift_F-mat96.1-0 ; 
       gen_forklift_F-nose_white-center.1-0.1-0 ; 
       gen_forklift_F-nose_white-center.1-1.1-0 ; 
       gen_forklift_F-orange-cop_light.1-0.1-0 ; 
       gen_forklift_F-orange-cop_light.1-1.1-0 ; 
       gen_forklift_F-orange-cop_light.1-2.1-0 ; 
       gen_forklift_F-orange-cop_light.1-3.1-0 ; 
       gen_forklift_F-orange-cop_light.1-4.1-0 ; 
       gen_forklift_F-orange-cop_light.1-5.1-0 ; 
       gen_forklift_F-orange-cop_light.1-6.1-0 ; 
       gen_forklift_F-port_red-left.1-0.1-0 ; 
       gen_forklift_F-starbord_green-right.1-0.1-0 ; 
       gen_forklift_T-.4.1-0 ; 
       gen_forklift_T-mat41.1-0 ; 
       gen_forklift_T-mat42.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       utl19-acrgatt.1-0 ; 
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.3-0 ; 
       utl19-lights0.1-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights2.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lights6.1-0 ; 
       utl19-lights7.1-0 ; 
       utl19-lights8.1-0 ; 
       utl19-lights9.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-SSa.1-0 ; 
       utl19-SSf.1-0 ; 
       utl19-SSglow1.1-0 ; 
       utl19-SSglow2.1-0 ; 
       utl19-SSglow3.1-0 ; 
       utl19-SSglow4.1-0 ; 
       utl19-SSl.1-0 ; 
       utl19-SSr.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-utl19.1-0 ROOT ; 
       utl19-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-gen_forklift_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 71     
       forklift_t-t2d1.1-0 ; 
       forklift_t-t2d11.1-0 ; 
       forklift_t-t2d19.1-0 ; 
       forklift_t-t2d2.1-0 ; 
       forklift_t-t2d20.1-0 ; 
       forklift_t-t2d22.1-0 ; 
       forklift_t-t2d23.1-0 ; 
       forklift_t-t2d24.1-0 ; 
       forklift_t-t2d25.1-0 ; 
       forklift_t-t2d26.1-0 ; 
       forklift_t-t2d27.1-0 ; 
       forklift_t-t2d28.1-0 ; 
       forklift_t-t2d29.1-0 ; 
       forklift_t-t2d3.1-0 ; 
       forklift_t-t2d30.1-0 ; 
       forklift_t-t2d31.1-0 ; 
       forklift_t-t2d32.1-0 ; 
       forklift_t-t2d33.1-0 ; 
       forklift_T-t2d34.1-0 ; 
       forklift_T-t2d35.1-0 ; 
       forklift_t-t2d4.1-0 ; 
       forklift_t-t2d6.1-0 ; 
       forklift_t-t2d8.1-0 ; 
       gen_forklift_F-t2d38.1-0 ; 
       gen_forklift_F-t2d39.1-0 ; 
       gen_forklift_F-t2d40.1-0 ; 
       gen_forklift_F-t2d41.1-0 ; 
       gen_forklift_F-t2d42.1-0 ; 
       gen_forklift_F-t2d43.1-0 ; 
       gen_forklift_F-t2d47.1-0 ; 
       gen_forklift_F-t2d53.1-0 ; 
       gen_forklift_F-t2d54.1-0 ; 
       gen_forklift_F-t2d55.1-0 ; 
       gen_forklift_F-t2d56.1-0 ; 
       gen_forklift_F-t2d57.1-0 ; 
       gen_forklift_F-t2d58.1-0 ; 
       gen_forklift_F-t2d59.1-0 ; 
       gen_forklift_F-t2d60.1-0 ; 
       gen_forklift_F-t2d61.1-0 ; 
       gen_forklift_F-t2d65.1-0 ; 
       gen_forklift_F-t2d66.1-0 ; 
       gen_forklift_F-t2d67.1-0 ; 
       gen_forklift_F-t2d68.1-0 ; 
       gen_forklift_F-t2d69.1-0 ; 
       gen_forklift_F-t2d70.1-0 ; 
       gen_forklift_F-t2d71.1-0 ; 
       gen_forklift_F-t2d72.1-0 ; 
       gen_forklift_F-t2d73.1-0 ; 
       gen_forklift_F-t2d74.1-0 ; 
       gen_forklift_F-t2d75.1-0 ; 
       gen_forklift_F-t2d76.1-0 ; 
       gen_forklift_F-t2d77.1-0 ; 
       gen_forklift_F-t2d78.1-0 ; 
       gen_forklift_F-t2d79.1-0 ; 
       gen_forklift_F-t2d80.1-0 ; 
       gen_forklift_F-t2d81.1-0 ; 
       gen_forklift_F-t2d82.1-0 ; 
       gen_forklift_F-t2d83.1-0 ; 
       gen_forklift_F-t2d84.1-0 ; 
       gen_forklift_F-t2d85.1-0 ; 
       gen_forklift_F-t2d86.1-0 ; 
       gen_forklift_F-t2d87.1-0 ; 
       gen_forklift_F-t2d88.1-0 ; 
       gen_forklift_F-t2d89.1-0 ; 
       gen_forklift_F-t2d90.1-0 ; 
       gen_forklift_F-t2d91.1-0 ; 
       gen_forklift_F-t2d92.1-0 ; 
       gen_forklift_F-t2d93.1-0 ; 
       gen_forklift_F-t2d94.1-0 ; 
       gen_forklift_T-t2d36.1-0 ; 
       gen_forklift_T-t2d37.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       18 2 110 ; 
       19 2 110 ; 
       20 5 110 ; 
       21 6 110 ; 
       22 7 110 ; 
       23 8 110 ; 
       24 14 110 ; 
       25 17 110 ; 
       0 2 110 ; 
       1 27 110 ; 
       2 28 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 16 110 ; 
       26 2 110 ; 
       27 26 110 ; 
       29 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       18 74 300 ; 
       19 75 300 ; 
       20 33 300 ; 
       20 67 300 ; 
       20 68 300 ; 
       20 69 300 ; 
       20 82 300 ; 
       20 77 300 ; 
       20 78 300 ; 
       21 34 300 ; 
       21 70 300 ; 
       21 27 300 ; 
       21 71 300 ; 
       21 72 300 ; 
       21 32 300 ; 
       21 28 300 ; 
       22 26 300 ; 
       22 73 300 ; 
       22 29 300 ; 
       22 31 300 ; 
       22 30 300 ; 
       23 76 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       23 38 300 ; 
       23 81 300 ; 
       23 79 300 ; 
       23 80 300 ; 
       24 83 300 ; 
       25 84 300 ; 
       2 0 300 ; 
       2 2 300 ; 
       2 11 300 ; 
       2 22 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       2 21 300 ; 
       2 23 300 ; 
       2 39 300 ; 
       4 6 300 ; 
       4 7 300 ; 
       5 35 300 ; 
       5 51 300 ; 
       5 52 300 ; 
       5 53 300 ; 
       5 40 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       6 49 300 ; 
       6 50 300 ; 
       6 65 300 ; 
       6 66 300 ; 
       7 61 300 ; 
       7 62 300 ; 
       7 63 300 ; 
       7 64 300 ; 
       8 54 300 ; 
       8 55 300 ; 
       8 56 300 ; 
       8 57 300 ; 
       8 58 300 ; 
       8 59 300 ; 
       8 60 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       12 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       13 3 300 ; 
       13 4 300 ; 
       13 5 300 ; 
       14 43 300 ; 
       14 44 300 ; 
       14 45 300 ; 
       15 8 300 ; 
       16 86 300 ; 
       16 87 300 ; 
       16 85 300 ; 
       17 46 300 ; 
       17 47 300 ; 
       17 48 300 ; 
       26 9 300 ; 
       27 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       28 10 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       21 18 401 ; 
       23 19 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 13 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       22 20 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       27 51 401 ; 
       28 55 401 ; 
       29 57 401 ; 
       30 59 401 ; 
       31 58 401 ; 
       32 54 401 ; 
       36 60 401 ; 
       37 61 401 ; 
       38 62 401 ; 
       39 66 401 ; 
       41 67 401 ; 
       42 68 401 ; 
       43 23 401 ; 
       44 24 401 ; 
       45 25 401 ; 
       46 26 401 ; 
       47 27 401 ; 
       48 28 401 ; 
       50 29 401 ; 
       51 30 401 ; 
       52 31 401 ; 
       53 32 401 ; 
       55 33 401 ; 
       56 34 401 ; 
       57 35 401 ; 
       58 36 401 ; 
       59 37 401 ; 
       60 38 401 ; 
       62 39 401 ; 
       63 40 401 ; 
       64 41 401 ; 
       65 42 401 ; 
       66 43 401 ; 
       67 44 401 ; 
       68 45 401 ; 
       69 46 401 ; 
       70 50 401 ; 
       71 52 401 ; 
       72 53 401 ; 
       73 56 401 ; 
       77 48 401 ; 
       78 49 401 ; 
       79 64 401 ; 
       80 65 401 ; 
       81 63 401 ; 
       82 47 401 ; 
       85 70 401 ; 
       87 69 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER ANIMATION 
       51 0 15003 ; 
       52 1 15003 ; 
       53 2 15003 ; 
       58 3 15003 ; 
       59 4 15003 ; 
       60 5 15003 ; 
       63 6 15003 ; 
       64 7 15003 ; 
       65 8 15003 ; 
       66 9 15003 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 23.5973 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 23.5973 -2.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       18 SCHEM 13 -78.2823 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 13 -82.2823 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 20 -70.2823 0 WIRECOL 7 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 20 -72.2823 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 20 -74.2823 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 20 -76.2823 0 WIRECOL 7 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 20 -68.2823 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 20 -88.2823 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 13 -80.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 20 -86.2823 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 9.5 -74.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 13 -63.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 13 -73.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 16.5 -70.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 16.5 -72.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 16.5 -74.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 16.5 -76.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.5 -60.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 16.5 -62.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 16.5 -64.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 16.5 -66.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 13 -68.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 16.5 -68.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 13 -84.2823 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 13 -88.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 16.5 -88.2823 0 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 13 -86.30543 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 16.5 -86.2823 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 6 -74.2823 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 13 -90.2823 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19.91047 -90.3886 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.78391 -88.6471 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.69773 -89.5664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -60.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 20 -60.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -64.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -64.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -62.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -62.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -62.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24.09542 -73.2596 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 23.96096 -81.0064 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24.30568 -89.0639 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24.21033 -77.1447 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24.12415 -81.5755 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24.66995 -79.4319 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24.42058 -87.0065 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24.02914 -68.2686 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24.07586 -76.633 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 20 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24.22583 -82.3042 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24.31201 -83.9021 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24.0822 -86.0169 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 13 -58.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 20 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 20 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 20 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 20 -66.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 19.82493 -91.5676 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 20.68673 -88.8207 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 20.31328 -87.0218 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 20.66204 -62.40321 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 20.81301 -66.61371 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 20.59147 -61.75148 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 20.50529 -63.66529 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.10855 -66.15367 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 20.80227 -65.43804 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 20 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 20 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 20 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 20.55347 -73.4424 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 20 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 20 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 20.74149 -63.05848 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 20 -72.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 20 -72.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 20 -72.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 20.7195 -72.7479 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 20.74822 -75.2363 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 24.54622 -70.4122 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 24.66112 -72.4122 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 24.3164 -74.4409 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 24.04714 -78.8915 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 24.30568 -83.1788 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 24.39186 -84.7192 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 23.5 -74.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 16.87009 -82.5896 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 16.75518 -86.6184 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 22.61714 -79.9308 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 24.02914 -78.3548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 24.14404 -80.4409 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 24.57055 -90.4192 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 24.31201 -92.1606 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 24.628 -88.1606 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 24.14404 -76.0388 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 23.5 -68.5323 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 24.1664 -93.108 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 16.64028 -83.4185 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 17.01372 -87.5909 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 16.78391 -85.5909 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       18 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.11777 -90.446 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -68.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -68.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 18.99121 -88.9057 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20.0541 -89.5951 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 23.5 -60.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 23.5 -60.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 23.5 -64.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 23.5 -64.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 23.5 -62.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 23.5 -62.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 23.5 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 23.5 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 23.5 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 23.5 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 23.5 -66.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 23.09512 -91.4527 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 23.00894 -89.1942 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 23.35366 -87.2804 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24.31301 -66.61371 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24.09147 -61.75148 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 24.00529 -63.66529 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24.60855 -66.15367 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 23.35637 -74.2738 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 23.5 -74.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 22.98292 -74.2451 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 23.99602 -73.6723 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 23.47127 -74.2737 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 23.47127 -74.3886 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 23.5 -72.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 23.5 -72.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 22.69566 -72.44611 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 24.30568 -72.8628 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 24.04714 -74.8341 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 28.04622 -70.4122 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 28.16112 -72.4122 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 27.8164 -74.4409 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 27.64404 -76.0388 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 27.52914 -78.3548 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 27.64404 -80.4409 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 27.54714 -78.8915 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 27.46096 -81.0064 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 27.80568 -83.1788 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 27.89186 -84.7192 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 27.92058 -87.0065 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 27.80568 -89.0639 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 27 -74.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 27.71033 -77.1447 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 28.16995 -79.4319 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 27.62415 -81.5755 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 27.72583 -82.3042 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 27.81201 -83.9021 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 27.5822 -86.0169 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 28.128 -88.1606 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 28.07055 -90.4192 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 27.81201 -92.1606 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 16.5 -58.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 23.5 -68.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 23.5 -68.5323 0 WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 20.169 -85.8495 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 20.22646 -83.1026 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 24.09147 -61.75148 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 24.00529 -63.66529 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 24.60855 -66.15367 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 24.05347 -73.4424 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 23.5 -74.5323 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 23.5 -74.5323 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 23.5 -72.5323 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 23.5 -72.5323 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 24.2195 -72.7479 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 24.24822 -75.2363 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 9.5 -57.5323 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 33 33 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
