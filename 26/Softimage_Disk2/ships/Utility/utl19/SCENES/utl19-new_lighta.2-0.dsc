SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl19-utl19_1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.3-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       forklift_t-light1_2_1_1.6-0 ROOT ; 
       forklift_t-light2_2_1_1.6-0 ROOT ; 
       tanker_F-light1.3-0 ROOT ; 
       tanker_F-light2.3-0 ROOT ; 
       tanker_F-light3.3-0 ROOT ; 
       tanker_F-light4.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 71     
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat25.2-0 ; 
       forklift_t-mat26.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat29.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_t-mat30.2-0 ; 
       forklift_t-mat31.2-0 ; 
       forklift_t-mat32.2-0 ; 
       forklift_t-mat33.2-0 ; 
       forklift_t-mat34.2-0 ; 
       forklift_t-mat35.2-0 ; 
       forklift_t-mat36.2-0 ; 
       forklift_t-mat37.2-0 ; 
       forklift_t-mat38.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       new_lighta-blue-cop_light.1-0.1-0 ; 
       new_lighta-blue-cop_light.1-1.1-0 ; 
       new_lighta-blue-cop_light.1-2.1-0 ; 
       new_lighta-blue-cop_light.1-3.1-0 ; 
       new_lighta-blue-cop_light.1-4.1-0 ; 
       new_lighta-blue-cop_light.1-5.1-0 ; 
       new_lighta-blue-cop_light.1-6.1-0 ; 
       new_lighta-cop_light.1-0 ; 
       new_lighta-cop_light1.1-0 ; 
       new_lighta-mat101.1-0 ; 
       new_lighta-mat102.1-0 ; 
       new_lighta-mat103.1-0 ; 
       new_lighta-mat104.1-0 ; 
       new_lighta-mat108.1-0 ; 
       new_lighta-mat44.1-0 ; 
       new_lighta-mat45.1-0 ; 
       new_lighta-mat46.1-0 ; 
       new_lighta-mat47.1-0 ; 
       new_lighta-mat48.1-0 ; 
       new_lighta-mat49.1-0 ; 
       new_lighta-mat53.1-0 ; 
       new_lighta-mat66.1-0 ; 
       new_lighta-mat75.1-0 ; 
       new_lighta-mat82.1-0 ; 
       new_lighta-mat83.1-0 ; 
       new_lighta-mat84.1-0 ; 
       new_lighta-mat89.1-0 ; 
       new_lighta-mat91.1-0 ; 
       new_lighta-mat92.1-0 ; 
       new_lighta-mat96.1-0 ; 
       new_lighta-nose_white-center.1-0.1-0 ; 
       new_lighta-nose_white-center.1-1.1-0 ; 
       new_lighta-orange-cop_light.1-0.1-0 ; 
       new_lighta-orange-cop_light.1-1.1-0 ; 
       new_lighta-orange-cop_light.1-2.1-0 ; 
       new_lighta-orange-cop_light.1-3.1-0 ; 
       new_lighta-orange-cop_light.1-4.1-0 ; 
       new_lighta-orange-cop_light.1-5.1-0 ; 
       new_lighta-orange-cop_light.1-6.1-0 ; 
       new_lighta-port_red-left.1-0.1-0 ; 
       new_lighta-starbord_green-right.1-0.1-0 ; 
       utl12_wtext-mat1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       utl19-acrgatt.1-0 ; 
       utl19-fcrgatt.1-0 ; 
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lights0.1-0 ; 
       utl19-lights1.1-0 ; 
       utl19-lights11.1-0 ; 
       utl19-lights3.1-0 ; 
       utl19-lights4.1-0 ; 
       utl19-lights5.1-0 ; 
       utl19-lights6.1-0 ; 
       utl19-lights7.1-0 ; 
       utl19-lights8.1-0 ; 
       utl19-lights9.1-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-SSa.1-0 ; 
       utl19-SSf.1-0 ; 
       utl19-SSglow1.1-0 ; 
       utl19-SSglow2.1-0 ; 
       utl19-SSglow3.1-0 ; 
       utl19-SSglow4.1-0 ; 
       utl19-SSl.1-0 ; 
       utl19-SSr.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-utl19_1.6-0 ROOT ; 
       utl19-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-new_lighta.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       forklift_t-t2d1.4-0 ; 
       forklift_t-t2d11.3-0 ; 
       forklift_t-t2d19.2-0 ; 
       forklift_t-t2d2.4-0 ; 
       forklift_t-t2d20.2-0 ; 
       forklift_t-t2d22.3-0 ; 
       forklift_t-t2d23.3-0 ; 
       forklift_t-t2d24.2-0 ; 
       forklift_t-t2d25.2-0 ; 
       forklift_t-t2d26.2-0 ; 
       forklift_t-t2d27.2-0 ; 
       forklift_t-t2d28.2-0 ; 
       forklift_t-t2d29.2-0 ; 
       forklift_t-t2d3.4-0 ; 
       forklift_t-t2d30.2-0 ; 
       forklift_t-t2d31.2-0 ; 
       forklift_t-t2d32.2-0 ; 
       forklift_t-t2d33.2-0 ; 
       forklift_T-t2d34.4-0 ; 
       forklift_T-t2d35.4-0 ; 
       forklift_t-t2d4.4-0 ; 
       forklift_t-t2d6.4-0 ; 
       forklift_t-t2d8.4-0 ; 
       gen_forklift_T-t2d36.2-0 ; 
       gen_forklift_T-t2d37.2-0 ; 
       new_lighta-t2d100.1-0 ; 
       new_lighta-t2d38.1-0 ; 
       new_lighta-t2d39.1-0 ; 
       new_lighta-t2d40.1-0 ; 
       new_lighta-t2d41.1-0 ; 
       new_lighta-t2d42.1-0 ; 
       new_lighta-t2d43.1-0 ; 
       new_lighta-t2d70.1-0 ; 
       new_lighta-t2d71.1-0 ; 
       new_lighta-t2d72.1-0 ; 
       new_lighta-t2d73.1-0 ; 
       new_lighta-t2d74.1-0 ; 
       new_lighta-t2d75.1-0 ; 
       new_lighta-t2d76.1-0 ; 
       new_lighta-t2d77.1-0 ; 
       new_lighta-t2d78.1-0 ; 
       new_lighta-t2d79.1-0 ; 
       new_lighta-t2d80.1-0 ; 
       new_lighta-t2d81.1-0 ; 
       new_lighta-t2d82.1-0 ; 
       new_lighta-t2d83.1-0 ; 
       new_lighta-t2d84.1-0 ; 
       new_lighta-t2d85.1-0 ; 
       new_lighta-t2d86.1-0 ; 
       new_lighta-t2d87.1-0 ; 
       new_lighta-t2d88.1-0 ; 
       new_lighta-t2d89.1-0 ; 
       new_lighta-t2d90.1-0 ; 
       new_lighta-t2d91.1-0 ; 
       new_lighta-t2d92.1-0 ; 
       new_lighta-t2d95.1-0 ; 
       new_lighta-t2d96.1-0 ; 
       new_lighta-t2d97.1-0 ; 
       new_lighta-t2d98.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 28 110 ; 
       3 29 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 3 110 ; 
       15 14 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 17 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       21 6 110 ; 
       22 7 110 ; 
       23 8 110 ; 
       24 9 110 ; 
       25 15 110 ; 
       26 18 110 ; 
       27 3 110 ; 
       28 27 110 ; 
       30 3 110 ; 
       2 3 110 ; 
       6 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       3 2 300 ; 
       3 11 300 ; 
       3 22 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 21 300 ; 
       3 23 300 ; 
       3 41 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       7 49 300 ; 
       8 51 300 ; 
       9 50 300 ; 
       10 10 300 ; 
       10 12 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       12 13 300 ; 
       12 14 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       13 20 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       14 5 300 ; 
       15 43 300 ; 
       15 44 300 ; 
       15 45 300 ; 
       16 8 300 ; 
       17 27 300 ; 
       17 28 300 ; 
       17 26 300 ; 
       18 46 300 ; 
       18 47 300 ; 
       18 48 300 ; 
       19 59 300 ; 
       20 60 300 ; 
       21 36 300 ; 
       21 52 300 ; 
       21 53 300 ; 
       21 54 300 ; 
       21 67 300 ; 
       21 62 300 ; 
       21 63 300 ; 
       22 37 300 ; 
       22 55 300 ; 
       22 30 300 ; 
       22 56 300 ; 
       22 57 300 ; 
       22 35 300 ; 
       22 31 300 ; 
       23 29 300 ; 
       23 58 300 ; 
       23 32 300 ; 
       23 34 300 ; 
       23 33 300 ; 
       24 61 300 ; 
       24 38 300 ; 
       24 39 300 ; 
       24 40 300 ; 
       24 66 300 ; 
       24 64 300 ; 
       24 65 300 ; 
       25 68 300 ; 
       26 69 300 ; 
       27 9 300 ; 
       28 1 300 ; 
       2 70 300 ; 
       6 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 13 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 14 401 ; 
       17 15 401 ; 
       19 16 401 ; 
       20 17 401 ; 
       21 18 401 ; 
       22 20 401 ; 
       23 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 24 401 ; 
       28 23 401 ; 
       30 39 401 ; 
       31 43 401 ; 
       32 45 401 ; 
       33 47 401 ; 
       34 46 401 ; 
       35 42 401 ; 
       38 48 401 ; 
       39 49 401 ; 
       40 50 401 ; 
       41 54 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       48 31 401 ; 
       49 58 401 ; 
       50 55 401 ; 
       42 56 401 ; 
       51 57 401 ; 
       52 32 401 ; 
       53 33 401 ; 
       54 34 401 ; 
       55 38 401 ; 
       56 40 401 ; 
       57 41 401 ; 
       58 44 401 ; 
       62 36 401 ; 
       63 37 401 ; 
       64 52 401 ; 
       65 53 401 ; 
       66 51 401 ; 
       67 35 401 ; 
       70 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -20 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -28 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 147.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 155 -12 0 MPRFLG 0 ; 
       3 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 86.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       8 SCHEM 73.75 -10 0 MPRFLG 0 ; 
       9 SCHEM 91.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       11 SCHEM 25 -10 0 MPRFLG 0 ; 
       12 SCHEM 31.25 -10 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 135 -8 0 MPRFLG 0 ; 
       15 SCHEM 131.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 145 -8 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 3.75 -10 0 MPRFLG 0 ; 
       19 SCHEM 150 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 152.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 110 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       22 SCHEM 55 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 90 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       25 SCHEM 127.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 0 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 157.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 156.25 -10 0 MPRFLG 0 ; 
       29 SCHEM 93.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 162.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 18.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 111.25 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 142.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 137.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 140 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 145 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 35 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 180 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 72.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 87.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 135 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 130 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 132.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 100 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 80 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 105 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 107.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 47.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 150 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 152.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 112.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 115 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 92.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 90 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 110 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 127.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 0 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 157.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 137.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 167.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 140 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 145 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 30 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 25 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 35 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 180 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 182.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 135 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 130 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 132.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 80 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 100 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 102.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 105 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 107.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 110 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 112.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 115 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 47.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 50 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 52.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 55 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 57.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 60 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 67.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 70 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 72.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 75 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 82.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 85 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 87.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 90 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 92.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 95 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 185 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 187.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
