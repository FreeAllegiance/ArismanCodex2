SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.36-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       forklift_t-mat1.2-0 ; 
       forklift_t-mat12.2-0 ; 
       forklift_t-mat2.2-0 ; 
       forklift_t-mat21.2-0 ; 
       forklift_t-mat22.2-0 ; 
       forklift_t-mat23.2-0 ; 
       forklift_t-mat27.2-0 ; 
       forklift_t-mat28.2-0 ; 
       forklift_t-mat3.2-0 ; 
       forklift_T-mat39.2-0 ; 
       forklift_t-mat4.2-0 ; 
       forklift_T-mat40.2-0 ; 
       forklift_t-mat6.2-0 ; 
       forklift_t-mat8.2-0 ; 
       gen_forklift_T-.4.2-0 ; 
       gen_forklift_T-mat41.2-0 ; 
       gen_forklift_T-mat42.2-0 ; 
       static-mat104.4-0 ; 
       static-mat109.4-0 ; 
       static-mat110.4-0 ; 
       static-mat111.4-0 ; 
       static-mat44.4-0 ; 
       static-mat45.4-0 ; 
       static-mat46.4-0 ; 
       static-mat47.4-0 ; 
       static-mat48.4-0 ; 
       static-mat49.4-0 ; 
       utl12_wtext-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       utl19-fuselg.2-0 ; 
       utl19-fuselg_1.3-0 ; 
       utl19-lthrust1.1-0 ; 
       utl19-lthrust2.1-0 ; 
       utl19-ltractr.1-0 ; 
       utl19-rthrust1.1-0 ; 
       utl19-rthrust2.1-0 ; 
       utl19-tractr1.1-0 ; 
       utl19-tractr2.1-0 ; 
       utl19-utl19_1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl19-static.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       forklift_t-t2d1.5-0 ; 
       forklift_t-t2d11.3-0 ; 
       forklift_t-t2d19.3-0 ; 
       forklift_t-t2d2.5-0 ; 
       forklift_t-t2d20.3-0 ; 
       forklift_t-t2d24.2-0 ; 
       forklift_t-t2d25.2-0 ; 
       forklift_t-t2d3.5-0 ; 
       forklift_T-t2d34.5-0 ; 
       forklift_T-t2d35.5-0 ; 
       forklift_t-t2d4.5-0 ; 
       forklift_t-t2d6.5-0 ; 
       forklift_t-t2d8.5-0 ; 
       gen_forklift_T-t2d36.2-0 ; 
       gen_forklift_T-t2d37.2-0 ; 
       static-t2d100.4-0 ; 
       static-t2d101.4-0 ; 
       static-t2d102.4-0 ; 
       static-t2d103.4-0 ; 
       static-t2d38.4-0 ; 
       static-t2d39.4-0 ; 
       static-t2d40.4-0 ; 
       static-t2d41.4-0 ; 
       static-t2d42.4-0 ; 
       static-t2d43.4-0 ; 
       static-t2d92.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 9 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 5 110 ; 
       7 1 110 ; 
       8 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 27 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       0 20 300 ; 
       1 0 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       1 10 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 9 300 ; 
       1 11 300 ; 
       1 17 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 6 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 14 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       7 7 300 ; 
       8 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 9 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 14 401 ; 
       16 13 401 ; 
       17 25 401 ; 
       18 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
