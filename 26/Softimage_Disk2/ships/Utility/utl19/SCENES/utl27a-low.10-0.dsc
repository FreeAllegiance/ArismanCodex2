SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.28-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       low-light1.5-0 ROOT ; 
       low-light2.5-0 ROOT ; 
       low-light3.5-0 ROOT ; 
       low-light4.4-0 ROOT ; 
       low-light5.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       low-dsfdsf.1-0 ; 
       low-mat1.4-0 ; 
       low-mat10.3-0 ; 
       low-mat12.2-0 ; 
       low-mat3.2-0 ; 
       low-mat4.3-0 ; 
       low-mat5.3-0 ; 
       low-mat6.3-0 ; 
       low-mat7.3-0 ; 
       low-mat8.3-0 ; 
       low-werer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       low-bmerge5.1-0 ; 
       low-bmerge8.1-0 ; 
       low-bmerge9.1-0 ; 
       low-cube1.1-0 ; 
       low-cube2.1-0 ; 
       low-obj1.10-0 ROOT ; 
       low-tetra1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-low.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       low-t2d1.2-0 ; 
       low-t2d10.1-0 ; 
       low-t2d11.1-0 ; 
       low-t2d12.1-0 ; 
       low-t2d13.1-0 ; 
       low-t2d2.2-0 ; 
       low-t2d3.2-0 ; 
       low-t2d4.2-0 ; 
       low-t2d6.2-0 ; 
       low-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       3 5 110 ; 
       6 5 110 ; 
       0 5 110 ; 
       2 5 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       3 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       0 9 300 ; 
       2 2 300 ; 
       4 0 300 ; 
       4 3 300 ; 
       4 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       4 8 401 ; 
       5 0 401 ; 
       6 7 401 ; 
       7 6 401 ; 
       8 5 401 ; 
       9 4 401 ; 
       0 9 401 ; 
       3 1 401 ; 
       10 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -16 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 12.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       0 SCHEM 15 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
