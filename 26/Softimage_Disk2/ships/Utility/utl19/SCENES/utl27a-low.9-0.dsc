SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       utl12_wtext-cam_int1.27-0 ROOT ; 
       utl12_wtext-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       low-light1.4-0 ROOT ; 
       low-light2.4-0 ROOT ; 
       low-light3.4-0 ROOT ; 
       low-light4.3-0 ROOT ; 
       low-light5.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       low-dsfdsf.1-0 ; 
       low-mat1.4-0 ; 
       low-mat10.3-0 ; 
       low-mat12.2-0 ; 
       low-mat3.2-0 ; 
       low-mat4.3-0 ; 
       low-mat5.3-0 ; 
       low-mat6.3-0 ; 
       low-mat7.3-0 ; 
       low-mat8.3-0 ; 
       low-werer.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       low-bmerge5.1-0 ; 
       low-bmerge8.1-0 ; 
       low-bmerge9.1-0 ; 
       low-cube1.1-0 ; 
       low-cube2.1-0 ; 
       low-obj1.9-0 ROOT ; 
       low-tetra1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl19/PICTURES/utl19 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27a-low.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       low-t2d1.2-0 ; 
       low-t2d10.1-0 ; 
       low-t2d11.1-0 ; 
       low-t2d12.1-0 ; 
       low-t2d2.2-0 ; 
       low-t2d3.2-0 ; 
       low-t2d4.2-0 ; 
       low-t2d6.2-0 ; 
       low-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       3 5 110 ; 
       6 5 110 ; 
       0 5 110 ; 
       2 5 110 ; 
       4 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       3 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       0 9 300 ; 
       2 2 300 ; 
       4 0 300 ; 
       4 3 300 ; 
       4 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       4 7 401 ; 
       5 0 401 ; 
       6 6 401 ; 
       7 5 401 ; 
       8 4 401 ; 
       0 8 401 ; 
       3 1 401 ; 
       10 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 MPRFLG 0 ; 
       4 SCHEM 25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
