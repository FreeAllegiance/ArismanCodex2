SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.31-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       utl200_belter_miner-mat83.2-0 ; 
       utl200_belter_miner-mat85.1-0 ; 
       utl200_belter_miner-mat86.2-0 ; 
       utl200_belter_miner-mat87.1-0 ; 
       utl200_belter_miner-mat88.2-0 ; 
       utl200_belter_miner-mat89.1-0 ; 
       utl200_belter_miner-mat90.1-0 ; 
       utl200_belter_miner-mat91.1-0 ; 
       utl200_belter_miner-mat92.1-0 ; 
       utl200_belter_miner-mat96.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       utl200_belter_miner-cube1.1-0 ; 
       utl200_belter_miner-cube2.1-0 ; 
       utl200_belter_miner-cube2_1.1-0 ; 
       utl200_belter_miner-cube4.1-0 ; 
       utl200_belter_miner-cube5.1-0 ; 
       utl200_belter_miner-cyl1.1-0 ; 
       utl200_belter_miner-cyl3.1-0 ; 
       utl200_belter_miner-extru1.5-0 ; 
       utl200_belter_miner-null1.28-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl200/PICTURES/utl200 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl200-belter_miner.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl200_belter_miner-t2d10.1-0 ; 
       utl200_belter_miner-t2d11.2-0 ; 
       utl200_belter_miner-t2d15.2-0 ; 
       utl200_belter_miner-t2d3.2-0 ; 
       utl200_belter_miner-t2d4.4-0 ; 
       utl200_belter_miner-t2d5.2-0 ; 
       utl200_belter_miner-t2d6.5-0 ; 
       utl200_belter_miner-t2d7.4-0 ; 
       utl200_belter_miner-t2d8.2-0 ; 
       utl200_belter_miner-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 2 110 ; 
       2 3 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 0 300 ; 
       2 1 300 ; 
       3 8 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 2 300 ; 
       7 9 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 3 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 7 401 ; 
       6 9 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 1.834863 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
