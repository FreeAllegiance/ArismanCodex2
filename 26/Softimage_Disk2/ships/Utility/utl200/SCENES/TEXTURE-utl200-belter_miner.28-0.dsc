SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.28-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
       utl200_belter_miner-mat83.2-0 ; 
       utl200_belter_miner-mat85.1-0 ; 
       utl200_belter_miner-mat86.2-0 ; 
       utl200_belter_miner-mat87.1-0 ; 
       utl200_belter_miner-mat88.2-0 ; 
       utl200_belter_miner-mat89.1-0 ; 
       utl200_belter_miner-mat90.1-0 ; 
       utl200_belter_miner-mat91.1-0 ; 
       utl200_belter_miner-mat92.1-0 ; 
       utl200_belter_miner-mat93.1-0 ; 
       utl200_belter_miner-mat94.1-0 ; 
       utl200_belter_miner-mat95.1-0 ; 
       utl200_belter_miner-mat96.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl200_belter_miner-bsmoke.2-0 ; 
       utl200_belter_miner-bthrust.2-0 ; 
       utl200_belter_miner-cockpt.2-0 ; 
       utl200_belter_miner-cube1.1-0 ; 
       utl200_belter_miner-cube2.1-0 ; 
       utl200_belter_miner-cube2_1.1-0 ; 
       utl200_belter_miner-cube3.1-0 ; 
       utl200_belter_miner-cube4.1-0 ; 
       utl200_belter_miner-cube5.1-0 ; 
       utl200_belter_miner-cube7.1-0 ; 
       utl200_belter_miner-cube8.1-0 ; 
       utl200_belter_miner-cyl1.1-0 ; 
       utl200_belter_miner-cyl3.1-0 ; 
       utl200_belter_miner-extru1.5-0 ; 
       utl200_belter_miner-lwepemt.2-0 ; 
       utl200_belter_miner-missemt.2-0 ; 
       utl200_belter_miner-null1.26-0 ROOT ; 
       utl200_belter_miner-rwepemt.2-0 ; 
       utl200_belter_miner-SS01.2-0 ; 
       utl200_belter_miner-SS02.2-0 ; 
       utl200_belter_miner-SS03.2-0 ; 
       utl200_belter_miner-SS04.2-0 ; 
       utl200_belter_miner-SS05.2-0 ; 
       utl200_belter_miner-SS06.2-0 ; 
       utl200_belter_miner-SS07.2-0 ; 
       utl200_belter_miner-SS08.2-0 ; 
       utl200_belter_miner-trail.2-0 ; 
       utl200_belter_miner-tsmoke.2-0 ; 
       utl200_belter_miner-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl200/PICTURES/utl200 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-utl200-belter_miner.27-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 13     
       utl200_belter_miner-t2d10.1-0 ; 
       utl200_belter_miner-t2d11.2-0 ; 
       utl200_belter_miner-t2d12.1-0 ; 
       utl200_belter_miner-t2d13.1-0 ; 
       utl200_belter_miner-t2d14.2-0 ; 
       utl200_belter_miner-t2d15.2-0 ; 
       utl200_belter_miner-t2d3.2-0 ; 
       utl200_belter_miner-t2d4.4-0 ; 
       utl200_belter_miner-t2d5.2-0 ; 
       utl200_belter_miner-t2d6.5-0 ; 
       utl200_belter_miner-t2d7.4-0 ; 
       utl200_belter_miner-t2d8.2-0 ; 
       utl200_belter_miner-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 16 110 ; 
       1 16 110 ; 
       2 16 110 ; 
       3 13 110 ; 
       4 5 110 ; 
       5 7 110 ; 
       6 13 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 13 110 ; 
       10 13 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 11 300 ; 
       4 8 300 ; 
       5 9 300 ; 
       6 17 300 ; 
       7 16 300 ; 
       9 19 300 ; 
       10 18 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       13 10 300 ; 
       13 20 300 ; 
       18 0 300 ; 
       19 1 300 ; 
       20 3 300 ; 
       21 2 300 ; 
       22 5 300 ; 
       23 4 300 ; 
       24 6 300 ; 
       25 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 11 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 12 401 ; 
       15 0 401 ; 
       16 1 401 ; 
       17 2 401 ; 
       18 3 401 ; 
       19 4 401 ; 
       20 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -6 0 MPRFLG 0 ; 
       8 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 31.06357 -4 0 MPRFLG 0 ; 
       10 SCHEM 28.56357 -6.227705 0 USR MPRFLG 0 ; 
       11 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       12 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 1.834863 MPRFLG 0 ; 
       17 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 60 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 65 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 70 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 33.56357 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 28.56357 -8.227705 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 31.06357 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.06357 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 28.56357 -10.22771 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 31.06357 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.06357 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 33.56357 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
