SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.1-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl200_belter_miner-bsmoke.2-0 ; 
       utl200_belter_miner-bthrust.2-0 ; 
       utl200_belter_miner-cockpt.2-0 ; 
       utl200_belter_miner-cube1.1-0 ; 
       utl200_belter_miner-cube2.1-0 ; 
       utl200_belter_miner-cube2_1.1-0 ; 
       utl200_belter_miner-cube3.1-0 ; 
       utl200_belter_miner-cube4.1-0 ; 
       utl200_belter_miner-cube5.1-0 ; 
       utl200_belter_miner-cube6.1-0 ; 
       utl200_belter_miner-cube7.1-0 ; 
       utl200_belter_miner-cyl1.1-0 ; 
       utl200_belter_miner-cyl2.1-0 ; 
       utl200_belter_miner-extru1.5-0 ; 
       utl200_belter_miner-lwepemt.2-0 ; 
       utl200_belter_miner-missemt.2-0 ; 
       utl200_belter_miner-null1.1-0 ROOT ; 
       utl200_belter_miner-rwepemt.2-0 ; 
       utl200_belter_miner-SS01.2-0 ; 
       utl200_belter_miner-SS02.2-0 ; 
       utl200_belter_miner-SS03.2-0 ; 
       utl200_belter_miner-SS04.2-0 ; 
       utl200_belter_miner-SS05.2-0 ; 
       utl200_belter_miner-SS06.2-0 ; 
       utl200_belter_miner-SS07.2-0 ; 
       utl200_belter_miner-SS08.2-0 ; 
       utl200_belter_miner-trail.2-0 ; 
       utl200_belter_miner-tsmoke.2-0 ; 
       utl200_belter_miner-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl200-belter_miner.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       0 16 110 ; 
       1 16 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       3 13 110 ; 
       4 5 110 ; 
       5 7 110 ; 
       6 13 110 ; 
       7 3 110 ; 
       11 8 110 ; 
       13 16 110 ; 
       12 8 110 ; 
       8 3 110 ; 
       9 13 110 ; 
       10 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       18 0 300 ; 
       19 1 300 ; 
       20 3 300 ; 
       21 2 300 ; 
       22 5 300 ; 
       23 4 300 ; 
       24 6 300 ; 
       25 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 72.07513 3.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 72.07513 1.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 97.1136 -10.32174 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 86.50317 -10.71686 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 86.43144 -11.51621 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 99.66711 -11.4384 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 97.16711 -11.4384 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 91.11344 -10.46107 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 91.10564 -11.42028 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 94.66711 -11.4384 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 90.72713 9.248376 0 USR SRT 1 1 1 0 0 0 0 0 1.834863 MPRFLG 0 ; 
       18 SCHEM 103.9739 -8.842482 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 104.0366 -9.608652 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 106.4111 -8.789643 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 106.3682 -9.635073 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 108.8319 -8.895323 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 108.8749 -9.608652 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 111.4447 -8.846703 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 111.1384 -9.628721 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 88.65839 -9.187153 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 78.45679 3.751997 0 MPRFLG 0 ; 
       4 SCHEM 75.95679 -2.248004 0 MPRFLG 0 ; 
       5 SCHEM 75.95679 -0.2480032 0 MPRFLG 0 ; 
       6 SCHEM 83.45679 3.751997 0 MPRFLG 0 ; 
       7 SCHEM 75.95679 1.751996 0 MPRFLG 0 ; 
       11 SCHEM 78.45679 -0.2480032 0 MPRFLG 0 ; 
       13 SCHEM 82.20679 5.751998 0 USR MPRFLG 0 ; 
       12 SCHEM 80.95679 -0.2480032 0 MPRFLG 0 ; 
       8 SCHEM 79.70679 1.751996 0 MPRFLG 0 ; 
       9 SCHEM 85.95679 3.751997 0 MPRFLG 0 ; 
       10 SCHEM 88.45679 3.751997 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
