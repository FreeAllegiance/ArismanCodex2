SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.36-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       utl201_belter_builder-mat100.1-0 ; 
       utl201_belter_builder-mat101.1-0 ; 
       utl201_belter_builder-mat102.1-0 ; 
       utl201_belter_builder-mat103.1-0 ; 
       utl201_belter_builder-mat104.2-0 ; 
       utl201_belter_builder-mat105.2-0 ; 
       utl201_belter_builder-mat106.2-0 ; 
       utl201_belter_builder-mat107.2-0 ; 
       utl201_belter_builder-mat108.1-0 ; 
       utl201_belter_builder-mat109.1-0 ; 
       utl201_belter_builder-mat110.1-0 ; 
       utl201_belter_builder-mat111.1-0 ; 
       utl201_belter_builder-mat112.1-0 ; 
       utl201_belter_builder-mat81.3-0 ; 
       utl201_belter_builder-mat82.2-0 ; 
       utl201_belter_builder-mat83.1-0 ; 
       utl201_belter_builder-mat84.3-0 ; 
       utl201_belter_builder-mat85.2-0 ; 
       utl201_belter_builder-mat86.2-0 ; 
       utl201_belter_builder-mat87.2-0 ; 
       utl201_belter_builder-mat88.2-0 ; 
       utl201_belter_builder-mat89.2-0 ; 
       utl201_belter_builder-mat90.1-0 ; 
       utl201_belter_builder-mat91.3-0 ; 
       utl201_belter_builder-mat92.1-0 ; 
       utl201_belter_builder-mat93.1-0 ; 
       utl201_belter_builder-mat94.1-0 ; 
       utl201_belter_builder-mat98.1-0 ; 
       utl201_belter_builder-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       utl201_belter_builder-cube17.1-0 ; 
       utl201_belter_builder-cube22.1-0 ; 
       utl201_belter_builder-cube23.1-0 ; 
       utl201_belter_builder-cube24.1-0 ; 
       utl201_belter_builder-cube26.1-0 ; 
       utl201_belter_builder-cube27.1-0 ; 
       utl201_belter_builder-cube30.1-0 ; 
       utl201_belter_builder-cube32.1-0 ; 
       utl201_belter_builder-cube33.1-0 ; 
       utl201_belter_builder-cube34.5-0 ; 
       utl201_belter_builder-cyl1_1.1-0 ; 
       utl201_belter_builder-extru4.3-0 ; 
       utl201_belter_builder-null1.29-0 ROOT ; 
       utl201_belter_builder-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl201/PICTURES/utl201 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl201-belter_builder.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl201_belter_builder-t2d1.3-0 ; 
       utl201_belter_builder-t2d10.2-0 ; 
       utl201_belter_builder-t2d11.3-0 ; 
       utl201_belter_builder-t2d12.2-0 ; 
       utl201_belter_builder-t2d13.2-0 ; 
       utl201_belter_builder-t2d14.2-0 ; 
       utl201_belter_builder-t2d18.1-0 ; 
       utl201_belter_builder-t2d19.1-0 ; 
       utl201_belter_builder-t2d2.2-0 ; 
       utl201_belter_builder-t2d20.1-0 ; 
       utl201_belter_builder-t2d21.1-0 ; 
       utl201_belter_builder-t2d22.1-0 ; 
       utl201_belter_builder-t2d23.1-0 ; 
       utl201_belter_builder-t2d24.3-0 ; 
       utl201_belter_builder-t2d25.3-0 ; 
       utl201_belter_builder-t2d26.3-0 ; 
       utl201_belter_builder-t2d27.2-0 ; 
       utl201_belter_builder-t2d28.1-0 ; 
       utl201_belter_builder-t2d29.1-0 ; 
       utl201_belter_builder-t2d3.1-0 ; 
       utl201_belter_builder-t2d30.1-0 ; 
       utl201_belter_builder-t2d31.2-0 ; 
       utl201_belter_builder-t2d4.4-0 ; 
       utl201_belter_builder-t2d5.3-0 ; 
       utl201_belter_builder-t2d6.3-0 ; 
       utl201_belter_builder-t2d7.3-0 ; 
       utl201_belter_builder-t2d8.3-0 ; 
       utl201_belter_builder-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 0 110 ; 
       2 4 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 2 110 ; 
       8 6 110 ; 
       9 12 110 ; 
       10 13 110 ; 
       11 12 110 ; 
       13 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 23 300 ; 
       11 7 300 ; 
       11 11 300 ; 
       13 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 10 401 ; 
       2 11 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 15 401 ; 
       7 16 401 ; 
       8 17 401 ; 
       10 18 401 ; 
       11 20 401 ; 
       12 21 401 ; 
       13 0 401 ; 
       14 8 401 ; 
       15 19 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 1 401 ; 
       23 2 401 ; 
       24 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       28 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 46.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 35 -14 0 MPRFLG 0 ; 
       6 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 38.75 -12 0 MPRFLG 0 ; 
       9 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 28.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0.8198837 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 50 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 50 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
