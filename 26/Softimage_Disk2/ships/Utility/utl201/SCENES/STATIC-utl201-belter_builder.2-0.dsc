SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.38-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       utl201_belter_builder-mat100_1.1-0 ; 
       utl201_belter_builder-mat101_1.1-0 ; 
       utl201_belter_builder-mat102_1.1-0 ; 
       utl201_belter_builder-mat103_1.1-0 ; 
       utl201_belter_builder-mat104_1.1-0 ; 
       utl201_belter_builder-mat105_1.1-0 ; 
       utl201_belter_builder-mat106_1.1-0 ; 
       utl201_belter_builder-mat107_1.1-0 ; 
       utl201_belter_builder-mat108_1.1-0 ; 
       utl201_belter_builder-mat109_1.1-0 ; 
       utl201_belter_builder-mat110_1.1-0 ; 
       utl201_belter_builder-mat111_1.1-0 ; 
       utl201_belter_builder-mat112_1.1-0 ; 
       utl201_belter_builder-mat81_1.1-0 ; 
       utl201_belter_builder-mat82_1.1-0 ; 
       utl201_belter_builder-mat83_1.1-0 ; 
       utl201_belter_builder-mat84_1.1-0 ; 
       utl201_belter_builder-mat85_1.1-0 ; 
       utl201_belter_builder-mat86_1.1-0 ; 
       utl201_belter_builder-mat87_1.1-0 ; 
       utl201_belter_builder-mat88_1.1-0 ; 
       utl201_belter_builder-mat89_1.1-0 ; 
       utl201_belter_builder-mat90_1.1-0 ; 
       utl201_belter_builder-mat91_1.1-0 ; 
       utl201_belter_builder-mat92_1.1-0 ; 
       utl201_belter_builder-mat93_1.1-0 ; 
       utl201_belter_builder-mat94_1.1-0 ; 
       utl201_belter_builder-mat98_1.1-0 ; 
       utl201_belter_builder-mat99_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       utl201_belter_builder-cube17_1.1-0 ; 
       utl201_belter_builder-cube22_1.1-0 ; 
       utl201_belter_builder-cube23_1.1-0 ; 
       utl201_belter_builder-cube24_1.1-0 ; 
       utl201_belter_builder-cube26_1.1-0 ; 
       utl201_belter_builder-cube27_1.1-0 ; 
       utl201_belter_builder-cube30_1.1-0 ; 
       utl201_belter_builder-cube32_1.1-0 ; 
       utl201_belter_builder-cube33_1.1-0 ; 
       utl201_belter_builder-cube34_1.5-0 ; 
       utl201_belter_builder-cyl1_1_1.1-0 ; 
       utl201_belter_builder-extru4_1.3-0 ; 
       utl201_belter_builder-null1.31-0 ROOT ; 
       utl201_belter_builder-sphere3_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl201/PICTURES/utl201 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl201-belter_builder.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl201_belter_builder-t2d1.3-0 ; 
       utl201_belter_builder-t2d10.2-0 ; 
       utl201_belter_builder-t2d11.3-0 ; 
       utl201_belter_builder-t2d12.2-0 ; 
       utl201_belter_builder-t2d13.2-0 ; 
       utl201_belter_builder-t2d14.2-0 ; 
       utl201_belter_builder-t2d18.1-0 ; 
       utl201_belter_builder-t2d19.1-0 ; 
       utl201_belter_builder-t2d2.2-0 ; 
       utl201_belter_builder-t2d20.1-0 ; 
       utl201_belter_builder-t2d21.1-0 ; 
       utl201_belter_builder-t2d22.1-0 ; 
       utl201_belter_builder-t2d23.1-0 ; 
       utl201_belter_builder-t2d24.3-0 ; 
       utl201_belter_builder-t2d25.3-0 ; 
       utl201_belter_builder-t2d26.3-0 ; 
       utl201_belter_builder-t2d27.2-0 ; 
       utl201_belter_builder-t2d28.1-0 ; 
       utl201_belter_builder-t2d29.1-0 ; 
       utl201_belter_builder-t2d3.1-0 ; 
       utl201_belter_builder-t2d30.1-0 ; 
       utl201_belter_builder-t2d31.2-0 ; 
       utl201_belter_builder-t2d4.4-0 ; 
       utl201_belter_builder-t2d5.3-0 ; 
       utl201_belter_builder-t2d6.3-0 ; 
       utl201_belter_builder-t2d7.3-0 ; 
       utl201_belter_builder-t2d8.3-0 ; 
       utl201_belter_builder-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 0 110 ; 
       2 4 110 ; 
       3 0 110 ; 
       4 11 110 ; 
       5 8 110 ; 
       6 7 110 ; 
       7 2 110 ; 
       8 6 110 ; 
       9 12 110 ; 
       10 13 110 ; 
       11 12 110 ; 
       13 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 10 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       7 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 4 300 ; 
       9 5 300 ; 
       9 6 300 ; 
       9 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
       11 23 300 ; 
       11 7 300 ; 
       11 11 300 ; 
       13 15 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 10 401 ; 
       2 11 401 ; 
       3 12 401 ; 
       4 13 401 ; 
       5 14 401 ; 
       6 15 401 ; 
       7 16 401 ; 
       8 17 401 ; 
       10 18 401 ; 
       11 20 401 ; 
       12 21 401 ; 
       13 0 401 ; 
       14 8 401 ; 
       15 19 401 ; 
       16 22 401 ; 
       17 23 401 ; 
       18 24 401 ; 
       19 25 401 ; 
       20 26 401 ; 
       21 27 401 ; 
       22 1 401 ; 
       23 2 401 ; 
       24 3 401 ; 
       25 4 401 ; 
       26 5 401 ; 
       27 6 401 ; 
       28 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 128.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 119.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 157 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 127 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 160.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 149.5 -14 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 153.25 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 157 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 153.25 -12 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 185.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 143.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 148.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 153.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 144.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 164.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 152 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 154.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 157 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 182 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 184.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 187 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 124.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 127 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 129.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 179.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 189.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 142 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 144.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 147 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 132 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 134.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 137 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 139.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 117 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 119.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 122 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 174.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 167 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 169.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 172 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 159.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 162 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 142 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 174.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 167 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 169.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 172 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 159.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 162 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 144.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 164.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 152 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 154.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 157 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 182 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 184.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 187 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 177 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 124.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 129.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 147 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 179.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 189.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 132 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 134.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 137 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 139.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 117 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 119.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
