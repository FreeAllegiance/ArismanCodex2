SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.4-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       utl201_belter_builder-bthrust.2-0 ; 
       utl201_belter_builder-cockpt.2-0 ; 
       utl201_belter_builder-cube17.1-0 ; 
       utl201_belter_builder-cube20.1-0 ; 
       utl201_belter_builder-cube21.1-0 ; 
       utl201_belter_builder-cube22.1-0 ; 
       utl201_belter_builder-cube23.1-0 ; 
       utl201_belter_builder-cube24.1-0 ; 
       utl201_belter_builder-cyl1_1.1-0 ; 
       utl201_belter_builder-extru4.3-0 ; 
       utl201_belter_builder-lsmoke.2-0 ; 
       utl201_belter_builder-lthrust.1-0 ; 
       utl201_belter_builder-lwepemt.2-0 ; 
       utl201_belter_builder-missemt.2-0 ; 
       utl201_belter_builder-null1.1-0 ROOT ; 
       utl201_belter_builder-rwepemt.2-0 ; 
       utl201_belter_builder-sphere3.1-0 ; 
       utl201_belter_builder-SS01.2-0 ; 
       utl201_belter_builder-SS02.2-0 ; 
       utl201_belter_builder-SS03.2-0 ; 
       utl201_belter_builder-SS04.2-0 ; 
       utl201_belter_builder-SS05.2-0 ; 
       utl201_belter_builder-SS06.2-0 ; 
       utl201_belter_builder-trail.2-0 ; 
       utl201_belter_builder-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl201-belter_builder.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 14 110 ; 
       11 14 110 ; 
       24 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       10 14 110 ; 
       10 11 111 ; 
       0 14 110 ; 
       0 10 111 ; 
       15 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       21 14 110 ; 
       22 14 110 ; 
       23 14 110 ; 
       2 9 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 2 110 ; 
       8 16 110 ; 
       9 14 110 ; 
       16 9 110 ; 
       6 9 110 ; 
       7 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       17 0 300 ; 
       18 1 300 ; 
       19 3 300 ; 
       20 2 300 ; 
       21 5 300 ; 
       22 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 207.5331 11.58191 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 199.2348 8.589569 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 201.9055 26.25416 0 USR SRT 1 1 1 0 0 0 0 0 0.8198837 MPRFLG 0 ; 
       24 SCHEM 196.8509 10.38743 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 210.0866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 207.5866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 199.1698 7.807058 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 201.5251 10.48336 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 205.0866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 214.8083 11.47355 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 214.871 10.70738 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 217.2456 11.52639 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 217.2026 10.68096 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 219.6663 11.42071 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 219.7093 10.70738 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 199.0779 12.71649 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 207.5107 21.6729 0 MPRFLG 0 ; 
       3 SCHEM 213.7607 19.6729 0 MPRFLG 0 ; 
       4 SCHEM 216.2607 19.6729 0 MPRFLG 0 ; 
       5 SCHEM 206.2607 19.6729 0 MPRFLG 0 ; 
       8 SCHEM 211.2607 19.6729 0 MPRFLG 0 ; 
       9 SCHEM 211.2607 23.6729 0 USR MPRFLG 0 ; 
       16 SCHEM 211.2607 21.6729 0 MPRFLG 0 ; 
       6 SCHEM 215.0107 21.6729 0 MPRFLG 0 ; 
       7 SCHEM 208.7607 19.6729 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
