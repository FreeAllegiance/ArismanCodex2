SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.6-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl201_belter_builder-bthrust.2-0 ; 
       utl201_belter_builder-cockpt.2-0 ; 
       utl201_belter_builder-cube17.1-0 ; 
       utl201_belter_builder-cube22.1-0 ; 
       utl201_belter_builder-cube23.1-0 ; 
       utl201_belter_builder-cube24.1-0 ; 
       utl201_belter_builder-cube25.1-0 ; 
       utl201_belter_builder-cube26.1-0 ; 
       utl201_belter_builder-cube27.1-0 ; 
       utl201_belter_builder-cube28.1-0 ; 
       utl201_belter_builder-cube29.1-0 ; 
       utl201_belter_builder-cube30.1-0 ; 
       utl201_belter_builder-cyl1_1.1-0 ; 
       utl201_belter_builder-extru4.3-0 ; 
       utl201_belter_builder-lsmoke.2-0 ; 
       utl201_belter_builder-lthrust.1-0 ; 
       utl201_belter_builder-lwepemt.2-0 ; 
       utl201_belter_builder-missemt.2-0 ; 
       utl201_belter_builder-null1.3-0 ROOT ; 
       utl201_belter_builder-rwepemt.2-0 ; 
       utl201_belter_builder-sphere3.1-0 ; 
       utl201_belter_builder-SS01.2-0 ; 
       utl201_belter_builder-SS02.2-0 ; 
       utl201_belter_builder-SS03.2-0 ; 
       utl201_belter_builder-SS04.2-0 ; 
       utl201_belter_builder-SS05.2-0 ; 
       utl201_belter_builder-SS06.2-0 ; 
       utl201_belter_builder-trail.2-0 ; 
       utl201_belter_builder-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl201-belter_builder.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 4 110 ; 
       0 18 110 ; 
       1 18 110 ; 
       2 13 110 ; 
       3 2 110 ; 
       4 13 110 ; 
       5 2 110 ; 
       12 20 110 ; 
       13 18 110 ; 
       14 18 110 ; 
       14 15 111 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 18 110 ; 
       19 18 110 ; 
       20 13 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 0 300 ; 
       22 1 300 ; 
       23 3 300 ; 
       24 2 300 ; 
       25 5 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 230.2086 16.90918 0 MPRFLG 0 ; 
       7 SCHEM 232.7086 16.90918 0 MPRFLG 0 ; 
       8 SCHEM 236.4586 16.90918 0 MPRFLG 0 ; 
       9 SCHEM 235.2086 14.90918 0 MPRFLG 0 ; 
       10 SCHEM 237.7086 14.90918 0 MPRFLG 0 ; 
       11 SCHEM 240.2086 16.90918 0 MPRFLG 0 ; 
       0 SCHEM 196.412 9.855995 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 207.5331 11.58191 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 223.9586 18.90918 0 MPRFLG 0 ; 
       3 SCHEM 222.7086 16.90918 0 MPRFLG 0 ; 
       4 SCHEM 235.2086 18.90918 0 MPRFLG 0 ; 
       5 SCHEM 225.2086 16.90918 0 MPRFLG 0 ; 
       12 SCHEM 227.7086 16.90918 0 MPRFLG 0 ; 
       13 SCHEM 231.4586 20.90918 0 USR MPRFLG 0 ; 
       14 SCHEM 199.1698 7.807058 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 199.2348 8.589569 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 210.0866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 207.5866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 201.9055 26.25416 0 USR SRT 1 1 1 0 0 0 0 0 0.8198837 MPRFLG 0 ; 
       19 SCHEM 205.0866 10.46524 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 227.7086 18.90918 0 MPRFLG 0 ; 
       21 SCHEM 214.8083 11.47355 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 214.871 10.70738 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 217.2456 11.52639 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 217.2026 10.68096 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 219.6663 11.42071 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 219.7093 10.70738 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 199.0779 12.71649 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 196.8509 10.38743 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
