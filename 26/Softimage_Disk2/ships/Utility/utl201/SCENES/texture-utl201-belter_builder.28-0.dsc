SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.34-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       utl201_belter_builder-mat100.1-0 ; 
       utl201_belter_builder-mat101.1-0 ; 
       utl201_belter_builder-mat102.1-0 ; 
       utl201_belter_builder-mat103.1-0 ; 
       utl201_belter_builder-mat104.2-0 ; 
       utl201_belter_builder-mat105.2-0 ; 
       utl201_belter_builder-mat106.2-0 ; 
       utl201_belter_builder-mat107.2-0 ; 
       utl201_belter_builder-mat108.1-0 ; 
       utl201_belter_builder-mat109.1-0 ; 
       utl201_belter_builder-mat110.1-0 ; 
       utl201_belter_builder-mat111.1-0 ; 
       utl201_belter_builder-mat112.1-0 ; 
       utl201_belter_builder-mat81.3-0 ; 
       utl201_belter_builder-mat82.2-0 ; 
       utl201_belter_builder-mat83.1-0 ; 
       utl201_belter_builder-mat84.3-0 ; 
       utl201_belter_builder-mat85.2-0 ; 
       utl201_belter_builder-mat86.2-0 ; 
       utl201_belter_builder-mat87.2-0 ; 
       utl201_belter_builder-mat88.2-0 ; 
       utl201_belter_builder-mat89.2-0 ; 
       utl201_belter_builder-mat90.1-0 ; 
       utl201_belter_builder-mat91.3-0 ; 
       utl201_belter_builder-mat92.1-0 ; 
       utl201_belter_builder-mat93.1-0 ; 
       utl201_belter_builder-mat94.1-0 ; 
       utl201_belter_builder-mat98.1-0 ; 
       utl201_belter_builder-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl201_belter_builder-bthrust.2-0 ; 
       utl201_belter_builder-cockpt.2-0 ; 
       utl201_belter_builder-cube17.1-0 ; 
       utl201_belter_builder-cube22.1-0 ; 
       utl201_belter_builder-cube23.1-0 ; 
       utl201_belter_builder-cube24.1-0 ; 
       utl201_belter_builder-cube26.1-0 ; 
       utl201_belter_builder-cube27.1-0 ; 
       utl201_belter_builder-cube30.1-0 ; 
       utl201_belter_builder-cube32.1-0 ; 
       utl201_belter_builder-cube33.1-0 ; 
       utl201_belter_builder-cube34.5-0 ; 
       utl201_belter_builder-cyl1_1.1-0 ; 
       utl201_belter_builder-extru4.3-0 ; 
       utl201_belter_builder-lsmoke.2-0 ; 
       utl201_belter_builder-lthrust.1-0 ; 
       utl201_belter_builder-lwepemt.2-0 ; 
       utl201_belter_builder-missemt.2-0 ; 
       utl201_belter_builder-null1.27-0 ROOT ; 
       utl201_belter_builder-rwepemt.2-0 ; 
       utl201_belter_builder-sphere3.1-0 ; 
       utl201_belter_builder-SS01.2-0 ; 
       utl201_belter_builder-SS02.2-0 ; 
       utl201_belter_builder-SS03.2-0 ; 
       utl201_belter_builder-SS04.2-0 ; 
       utl201_belter_builder-SS05.2-0 ; 
       utl201_belter_builder-SS06.2-0 ; 
       utl201_belter_builder-trail.2-0 ; 
       utl201_belter_builder-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl201/PICTURES/utl201 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl201-belter_builder.28-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl201_belter_builder-t2d1.3-0 ; 
       utl201_belter_builder-t2d10.2-0 ; 
       utl201_belter_builder-t2d11.3-0 ; 
       utl201_belter_builder-t2d12.2-0 ; 
       utl201_belter_builder-t2d13.2-0 ; 
       utl201_belter_builder-t2d14.2-0 ; 
       utl201_belter_builder-t2d18.1-0 ; 
       utl201_belter_builder-t2d19.1-0 ; 
       utl201_belter_builder-t2d2.2-0 ; 
       utl201_belter_builder-t2d20.1-0 ; 
       utl201_belter_builder-t2d21.1-0 ; 
       utl201_belter_builder-t2d22.1-0 ; 
       utl201_belter_builder-t2d23.1-0 ; 
       utl201_belter_builder-t2d24.3-0 ; 
       utl201_belter_builder-t2d25.3-0 ; 
       utl201_belter_builder-t2d26.3-0 ; 
       utl201_belter_builder-t2d27.2-0 ; 
       utl201_belter_builder-t2d28.1-0 ; 
       utl201_belter_builder-t2d29.1-0 ; 
       utl201_belter_builder-t2d3.1-0 ; 
       utl201_belter_builder-t2d30.1-0 ; 
       utl201_belter_builder-t2d31.2-0 ; 
       utl201_belter_builder-t2d4.4-0 ; 
       utl201_belter_builder-t2d5.3-0 ; 
       utl201_belter_builder-t2d6.3-0 ; 
       utl201_belter_builder-t2d7.3-0 ; 
       utl201_belter_builder-t2d8.3-0 ; 
       utl201_belter_builder-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 18 110 ; 
       2 13 110 ; 
       3 2 110 ; 
       4 6 110 ; 
       5 2 110 ; 
       6 13 110 ; 
       7 10 110 ; 
       8 9 110 ; 
       9 4 110 ; 
       10 8 110 ; 
       11 18 110 ; 
       12 20 110 ; 
       13 18 110 ; 
       14 18 110 ; 
       14 15 111 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 18 110 ; 
       19 18 110 ; 
       20 13 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       28 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       6 30 300 ; 
       6 31 300 ; 
       6 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       9 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 18 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       13 29 300 ; 
       13 13 300 ; 
       13 17 300 ; 
       20 21 300 ; 
       21 0 300 ; 
       22 1 300 ; 
       23 3 300 ; 
       24 2 300 ; 
       25 5 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 9 401 ; 
       7 10 401 ; 
       8 11 401 ; 
       9 12 401 ; 
       10 13 401 ; 
       11 14 401 ; 
       12 15 401 ; 
       13 16 401 ; 
       14 17 401 ; 
       16 18 401 ; 
       17 20 401 ; 
       18 21 401 ; 
       19 0 401 ; 
       20 8 401 ; 
       21 19 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 1 401 ; 
       29 2 401 ; 
       30 3 401 ; 
       31 4 401 ; 
       32 5 401 ; 
       33 6 401 ; 
       34 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 80 -6 0 MPRFLG 0 ; 
       5 SCHEM 50 -6 0 MPRFLG 0 ; 
       6 SCHEM 83.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 72.5 -14 0 MPRFLG 0 ; 
       8 SCHEM 76.25 -10 0 MPRFLG 0 ; 
       9 SCHEM 80 -8 0 MPRFLG 0 ; 
       10 SCHEM 76.25 -12 0 MPRFLG 0 ; 
       11 SCHEM 108.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 66.25 -6 0 MPRFLG 0 ; 
       13 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 57.5 0 0 SRT 1 1 1 0 0 0 0 0 0.8198837 MPRFLG 0 ; 
       19 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 75 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 77.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 100 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 97.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 97.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 75 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 77.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 80 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 100 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 70 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
