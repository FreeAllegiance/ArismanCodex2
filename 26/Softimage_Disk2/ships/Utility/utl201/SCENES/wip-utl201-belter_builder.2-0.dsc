SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig30_belter_ftr-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 42     
       fig32_belter_stealth-cockpt.1-0 ROOT ; 
       fig32_belter_stealth-cube1.1-0 ; 
       fig32_belter_stealth-cube14.1-0 ; 
       fig32_belter_stealth-cube15.1-0 ; 
       fig32_belter_stealth-cube16.1-0 ; 
       fig32_belter_stealth-cube4.1-0 ; 
       fig32_belter_stealth-cube7.1-0 ; 
       fig32_belter_stealth-cube7_1.1-0 ; 
       fig32_belter_stealth-cube7_2.1-0 ; 
       fig32_belter_stealth-cube8.1-0 ; 
       fig32_belter_stealth-cube9.1-0 ; 
       fig32_belter_stealth-cyl1.1-0 ; 
       fig32_belter_stealth-cyl11.1-0 ; 
       fig32_belter_stealth-cyl12.1-0 ; 
       fig32_belter_stealth-extru2.1-0 ; 
       fig32_belter_stealth-extru3.1-0 ; 
       fig32_belter_stealth-lsmoke.1-0 ROOT ; 
       fig32_belter_stealth-lthrust.1-0 ROOT ; 
       fig32_belter_stealth-lwepemt.1-0 ROOT ; 
       fig32_belter_stealth-missemt.1-0 ROOT ; 
       fig32_belter_stealth-null1.1-0 ROOT ; 
       fig32_belter_stealth-rsmoke.1-0 ROOT ; 
       fig32_belter_stealth-rthrust.1-0 ROOT ; 
       fig32_belter_stealth-rwepemt.1-0 ROOT ; 
       fig32_belter_stealth-sphere1.1-0 ; 
       fig32_belter_stealth-sphere2.1-0 ; 
       fig32_belter_stealth-SS01.1-0 ROOT ; 
       fig32_belter_stealth-SS02.1-0 ROOT ; 
       fig32_belter_stealth-SS03.1-0 ROOT ; 
       fig32_belter_stealth-SS04.1-0 ROOT ; 
       fig32_belter_stealth-SS05.1-0 ROOT ; 
       fig32_belter_stealth-SS06.1-0 ROOT ; 
       fig32_belter_stealth-SS07.1-0 ROOT ; 
       fig32_belter_stealth-SS08.1-0 ROOT ; 
       fig32_belter_stealth-trail.1-0 ROOT ; 
       utl201_belter_builder-cube17.1-0 ; 
       utl201_belter_builder-cube20.1-0 ; 
       utl201_belter_builder-cube21.1-0 ; 
       utl201_belter_builder-cube22.1-0 ROOT ; 
       utl201_belter_builder-cyl1_1.1-0 ; 
       utl201_belter_builder-extru4.2-0 ROOT ; 
       utl201_belter_builder-spline1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-utl201-belter_builder.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 15 110 ; 
       3 10 110 ; 
       4 10 110 ; 
       5 14 110 ; 
       11 5 110 ; 
       35 40 110 ; 
       37 40 110 ; 
       7 1 110 ; 
       36 40 110 ; 
       39 40 110 ; 
       6 14 110 ; 
       9 14 110 ; 
       10 15 110 ; 
       15 14 110 ; 
       14 20 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       2 1 110 ; 
       8 1 110 ; 
       13 5 110 ; 
       12 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       26 0 300 ; 
       27 1 300 ; 
       28 3 300 ; 
       29 2 300 ; 
       30 5 300 ; 
       31 4 300 ; 
       32 6 300 ; 
       33 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 220.7767 4.834068 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -1.787128e-022 0.3111044 2.493818 MPRFLG 0 ; 
       1 SCHEM 234.3817 14.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 223.1317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 225.6317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 244.3817 16.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 243.1317 14.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 211.2121 20.55605 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 246.0459 31.62712 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 248.5459 31.62712 0 SRT 0.06358217 0.06358217 0.06358217 0 0 0 0.8936656 0 0.6775011 MPRFLG 0 ; 
       35 SCHEM 251.0459 31.62712 0 DISPLAY 1 2 MPRFLG 0 ; 
       38 SCHEM 261.0459 36.56842 0 SRT 1 1 1 -1.570796 0 0 -0.4713476 0.5567521 2.791839 MPRFLG 0 ; 
       37 SCHEM 258.5459 36.56842 0 MPRFLG 0 ; 
       7 SCHEM 233.1317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 256.0459 36.56842 0 MPRFLG 0 ; 
       39 SCHEM 219.8295 36.56842 0 USR MPRFLG 0 ; 
       6 SCHEM 215.6317 16.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 218.1317 16.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 223.1317 14.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 230.6317 16.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 230.6317 18.82177 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 210.1663 4.438947 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.608333 8.272682e-014 -1.474056 MPRFLG 0 ; 
       17 SCHEM 210.0945 3.639596 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.608333 8.272682e-014 -1.474056 MPRFLG 0 ; 
       18 SCHEM 223.3302 3.717407 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.3974945 -0.9572577 2.493687 MPRFLG 0 ; 
       19 SCHEM 220.8302 3.717407 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 9.987657e-023 -0.8429355 2.23437 MPRFLG 0 ; 
       21 SCHEM 214.7765 4.694738 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.46694 0.2629859 -3.663743 MPRFLG 0 ; 
       22 SCHEM 214.7687 3.735526 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.46694 0.2629859 -3.663743 MPRFLG 0 ; 
       23 SCHEM 218.3302 3.717407 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.3932599 -0.9572577 2.489453 MPRFLG 0 ; 
       24 SCHEM 228.1317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 230.6317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 207.5467 14.18966 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.6483051 0.1942236 2.907908 MPRFLG 0 ; 
       27 SCHEM 207.6094 13.42349 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.648315 0.1942236 2.907908 MPRFLG 0 ; 
       28 SCHEM 209.984 14.2425 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 2.48182 0.000347077 -0.08912998 MPRFLG 0 ; 
       29 SCHEM 209.941 13.39707 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -2.207304 0.000347077 0.01605607 MPRFLG 0 ; 
       30 SCHEM 212.4047 14.13682 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 0.5930589 0.9367372 0.0912881 MPRFLG 0 ; 
       31 SCHEM 212.4477 13.42349 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.5896088 0.9367372 0.0912881 MPRFLG 0 ; 
       32 SCHEM 214.7771 14.06523 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.8400454 -0.4396002 -2.362017 MPRFLG 0 ; 
       33 SCHEM 214.7112 13.40342 0 USR WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.8428973 -0.439605 -2.362017 MPRFLG 0 ; 
       34 SCHEM 212.3215 5.968656 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 2.82499e-015 -2.227597 MPRFLG 0 ; 
       2 SCHEM 240.6317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 235.6317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 245.6317 14.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 238.1317 12.82177 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 67.89953 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 70.33682 14.12467 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
