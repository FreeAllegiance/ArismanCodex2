SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.54-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       utl202_belter_apc-mat100.1-0 ; 
       utl202_belter_apc-mat101.1-0 ; 
       utl202_belter_apc-mat102.2-0 ; 
       utl202_belter_apc-mat103.3-0 ; 
       utl202_belter_apc-mat104.2-0 ; 
       utl202_belter_apc-mat105.1-0 ; 
       utl202_belter_apc-mat106.1-0 ; 
       utl202_belter_apc-mat107.1-0 ; 
       utl202_belter_apc-mat108.1-0 ; 
       utl202_belter_apc-mat81.2-0 ; 
       utl202_belter_apc-mat82.2-0 ; 
       utl202_belter_apc-mat83.3-0 ; 
       utl202_belter_apc-mat84.3-0 ; 
       utl202_belter_apc-mat85.2-0 ; 
       utl202_belter_apc-mat86.4-0 ; 
       utl202_belter_apc-mat87.3-0 ; 
       utl202_belter_apc-mat88.2-0 ; 
       utl202_belter_apc-mat89.2-0 ; 
       utl202_belter_apc-mat90.2-0 ; 
       utl202_belter_apc-mat91.3-0 ; 
       utl202_belter_apc-mat92.3-0 ; 
       utl202_belter_apc-mat93.2-0 ; 
       utl202_belter_apc-mat94.2-0 ; 
       utl202_belter_apc-mat95.2-0 ; 
       utl202_belter_apc-mat96.2-0 ; 
       utl202_belter_apc-mat97.2-0 ; 
       utl202_belter_apc-mat98.2-0 ; 
       utl202_belter_apc-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl202_belter_apc-cube21.1-0 ; 
       utl202_belter_apc-cube26.1-0 ; 
       utl202_belter_apc-cube27.1-0 ; 
       utl202_belter_apc-cube31.1-0 ; 
       utl202_belter_apc-cube32.1-0 ; 
       utl202_belter_apc-cube33.1-0 ; 
       utl202_belter_apc-cube35.1-0 ; 
       utl202_belter_apc-cube36.1-0 ; 
       utl202_belter_apc-cube38.1-0 ; 
       utl202_belter_apc-cyl1_1.1-0 ; 
       utl202_belter_apc-extru10.1-0 ; 
       utl202_belter_apc-extru11.1-0 ; 
       utl202_belter_apc-extru12.1-0 ; 
       utl202_belter_apc-extru5.4-0 ; 
       utl202_belter_apc-extru6.1-0 ; 
       utl202_belter_apc-extru7.1-0 ; 
       utl202_belter_apc-extru8.1-0 ; 
       utl202_belter_apc-null1.42-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl202/PICTURES/utl202 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl202-belter_apc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl202_belter_apc-t2d1.2-0 ; 
       utl202_belter_apc-t2d10.4-0 ; 
       utl202_belter_apc-t2d11.4-0 ; 
       utl202_belter_apc-t2d12.4-0 ; 
       utl202_belter_apc-t2d13.3-0 ; 
       utl202_belter_apc-t2d14.2-0 ; 
       utl202_belter_apc-t2d15.2-0 ; 
       utl202_belter_apc-t2d16.2-0 ; 
       utl202_belter_apc-t2d17.2-0 ; 
       utl202_belter_apc-t2d18.2-0 ; 
       utl202_belter_apc-t2d19.2-0 ; 
       utl202_belter_apc-t2d2.2-0 ; 
       utl202_belter_apc-t2d20.2-0 ; 
       utl202_belter_apc-t2d21.2-0 ; 
       utl202_belter_apc-t2d22.4-0 ; 
       utl202_belter_apc-t2d23.5-0 ; 
       utl202_belter_apc-t2d24.4-0 ; 
       utl202_belter_apc-t2d25.3-0 ; 
       utl202_belter_apc-t2d26.1-0 ; 
       utl202_belter_apc-t2d27.1-0 ; 
       utl202_belter_apc-t2d28.1-0 ; 
       utl202_belter_apc-t2d3.4-0 ; 
       utl202_belter_apc-t2d4.4-0 ; 
       utl202_belter_apc-t2d5.3-0 ; 
       utl202_belter_apc-t2d6.6-0 ; 
       utl202_belter_apc-t2d7.6-0 ; 
       utl202_belter_apc-t2d8.2-0 ; 
       utl202_belter_apc-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 0 110 ; 
       2 13 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 0 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 2 110 ; 
       11 13 110 ; 
       12 2 110 ; 
       13 17 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 14 300 ; 
       2 2 300 ; 
       3 25 300 ; 
       4 24 300 ; 
       5 7 300 ; 
       6 26 300 ; 
       7 27 300 ; 
       8 8 300 ; 
       9 19 300 ; 
       9 6 300 ; 
       10 22 300 ; 
       11 16 300 ; 
       12 23 300 ; 
       13 15 300 ; 
       13 17 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       16 20 300 ; 
       16 21 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 13 401 ; 
       2 14 401 ; 
       3 15 401 ; 
       4 16 401 ; 
       5 17 401 ; 
       6 18 401 ; 
       7 19 401 ; 
       8 20 401 ; 
       9 0 401 ; 
       10 11 401 ; 
       11 21 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 26 401 ; 
       17 27 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 5 401 ; 
       23 6 401 ; 
       24 7 401 ; 
       25 8 401 ; 
       26 9 401 ; 
       27 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 MPRFLG 0 ; 
       4 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 55 -6 0 MPRFLG 0 ; 
       7 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 50 -6 0 MPRFLG 0 ; 
       11 SCHEM 65 -4 0 MPRFLG 0 ; 
       12 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 36.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       16 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       17 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
