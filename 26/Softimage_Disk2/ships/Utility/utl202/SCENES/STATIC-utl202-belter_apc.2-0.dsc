SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.56-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 28     
       utl202_belter_apc-mat100_1.1-0 ; 
       utl202_belter_apc-mat101_1.1-0 ; 
       utl202_belter_apc-mat102_1.1-0 ; 
       utl202_belter_apc-mat103_1.1-0 ; 
       utl202_belter_apc-mat104_1.1-0 ; 
       utl202_belter_apc-mat105_1.1-0 ; 
       utl202_belter_apc-mat106_1.1-0 ; 
       utl202_belter_apc-mat107_1.1-0 ; 
       utl202_belter_apc-mat108_1.1-0 ; 
       utl202_belter_apc-mat81_1.1-0 ; 
       utl202_belter_apc-mat82_1.1-0 ; 
       utl202_belter_apc-mat83_1.1-0 ; 
       utl202_belter_apc-mat84_1.1-0 ; 
       utl202_belter_apc-mat85_1.1-0 ; 
       utl202_belter_apc-mat86_1.1-0 ; 
       utl202_belter_apc-mat87_1.1-0 ; 
       utl202_belter_apc-mat88_1.1-0 ; 
       utl202_belter_apc-mat89_1.1-0 ; 
       utl202_belter_apc-mat90_1.1-0 ; 
       utl202_belter_apc-mat91_1.1-0 ; 
       utl202_belter_apc-mat92_1.1-0 ; 
       utl202_belter_apc-mat93_1.1-0 ; 
       utl202_belter_apc-mat94_1.1-0 ; 
       utl202_belter_apc-mat95_1.1-0 ; 
       utl202_belter_apc-mat96_1.1-0 ; 
       utl202_belter_apc-mat97_1.1-0 ; 
       utl202_belter_apc-mat98_1.1-0 ; 
       utl202_belter_apc-mat99_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       utl202_belter_apc-cube21_1.1-0 ; 
       utl202_belter_apc-cube26_1.1-0 ; 
       utl202_belter_apc-cube27_1.1-0 ; 
       utl202_belter_apc-cube31_1.1-0 ; 
       utl202_belter_apc-cube32_1.1-0 ; 
       utl202_belter_apc-cube33_1.1-0 ; 
       utl202_belter_apc-cube35_1.1-0 ; 
       utl202_belter_apc-cube36_1.1-0 ; 
       utl202_belter_apc-cube38_1.1-0 ; 
       utl202_belter_apc-cyl1_1_1.1-0 ; 
       utl202_belter_apc-extru10_1.1-0 ; 
       utl202_belter_apc-extru11_1.1-0 ; 
       utl202_belter_apc-extru12_1.1-0 ; 
       utl202_belter_apc-extru5_1.4-0 ; 
       utl202_belter_apc-extru6_1.1-0 ; 
       utl202_belter_apc-extru7_1.1-0 ; 
       utl202_belter_apc-extru8_1.1-0 ; 
       utl202_belter_apc-null1.44-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl202/PICTURES/utl202 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-utl202-belter_apc.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl202_belter_apc-t2d1_1.1-0 ; 
       utl202_belter_apc-t2d10_1.1-0 ; 
       utl202_belter_apc-t2d11_1.1-0 ; 
       utl202_belter_apc-t2d12_1.1-0 ; 
       utl202_belter_apc-t2d13_1.1-0 ; 
       utl202_belter_apc-t2d14_1.1-0 ; 
       utl202_belter_apc-t2d15_1.1-0 ; 
       utl202_belter_apc-t2d16_1.1-0 ; 
       utl202_belter_apc-t2d17_1.1-0 ; 
       utl202_belter_apc-t2d18_1.1-0 ; 
       utl202_belter_apc-t2d19_1.1-0 ; 
       utl202_belter_apc-t2d2_1.1-0 ; 
       utl202_belter_apc-t2d20_1.1-0 ; 
       utl202_belter_apc-t2d21_1.1-0 ; 
       utl202_belter_apc-t2d22_1.1-0 ; 
       utl202_belter_apc-t2d23_1.1-0 ; 
       utl202_belter_apc-t2d24_1.1-0 ; 
       utl202_belter_apc-t2d25_1.1-0 ; 
       utl202_belter_apc-t2d26_1.1-0 ; 
       utl202_belter_apc-t2d27_1.1-0 ; 
       utl202_belter_apc-t2d28_1.1-0 ; 
       utl202_belter_apc-t2d3_1.1-0 ; 
       utl202_belter_apc-t2d4_1.1-0 ; 
       utl202_belter_apc-t2d5_1.1-0 ; 
       utl202_belter_apc-t2d6_1.1-0 ; 
       utl202_belter_apc-t2d7_1.1-0 ; 
       utl202_belter_apc-t2d8_1.1-0 ; 
       utl202_belter_apc-t2d9_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 0 110 ; 
       2 13 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 0 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 2 110 ; 
       11 13 110 ; 
       12 2 110 ; 
       13 17 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 18 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       2 14 300 ; 
       2 2 300 ; 
       3 25 300 ; 
       4 24 300 ; 
       5 7 300 ; 
       6 26 300 ; 
       7 27 300 ; 
       8 8 300 ; 
       9 19 300 ; 
       9 6 300 ; 
       10 22 300 ; 
       11 16 300 ; 
       12 23 300 ; 
       13 15 300 ; 
       13 17 300 ; 
       14 11 300 ; 
       14 12 300 ; 
       14 13 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       16 20 300 ; 
       16 21 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 12 401 ; 
       1 13 401 ; 
       2 14 401 ; 
       3 15 401 ; 
       4 16 401 ; 
       5 17 401 ; 
       6 18 401 ; 
       7 19 401 ; 
       8 20 401 ; 
       9 0 401 ; 
       10 11 401 ; 
       11 21 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 26 401 ; 
       17 27 401 ; 
       18 1 401 ; 
       19 2 401 ; 
       20 3 401 ; 
       21 4 401 ; 
       22 5 401 ; 
       23 6 401 ; 
       24 7 401 ; 
       25 8 401 ; 
       26 9 401 ; 
       27 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 125.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 124.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 157 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 154.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 157 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 129.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 164.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 167 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 132 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 118.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 159.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 174.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 162 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 145.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 147 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 143.25 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 113.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 145.75 0 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 137 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 139.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 172 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 122 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 124.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 127 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 119.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 129.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 132 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 142 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 144.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 147 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 149.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 152 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 169.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 174.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 179.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 134.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 117 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 112 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 114.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 159.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 162 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 157 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 154.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 164.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 167 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 142 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 134.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 117 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 112 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 114.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 159.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 162 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 157 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 154.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 164.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 167 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 144.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 137 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 139.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 172 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 122 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 124.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 127 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 119.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 129.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 132 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 147 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 149.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 152 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 169.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 177 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 174.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 179.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
