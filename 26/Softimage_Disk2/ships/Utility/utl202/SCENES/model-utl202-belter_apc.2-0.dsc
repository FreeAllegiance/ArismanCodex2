SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       utl202_belter_apc-cockpt.2-0 ; 
       utl202_belter_apc-cube21.1-0 ; 
       utl202_belter_apc-cube26.1-0 ; 
       utl202_belter_apc-cube27.1-0 ; 
       utl202_belter_apc-cube29.1-0 ; 
       utl202_belter_apc-cube30.1-0 ; 
       utl202_belter_apc-cube31.1-0 ; 
       utl202_belter_apc-cube32.1-0 ; 
       utl202_belter_apc-cube33.1-0 ; 
       utl202_belter_apc-cube34.1-0 ; 
       utl202_belter_apc-cyl1_1.1-0 ; 
       utl202_belter_apc-extru5.4-0 ; 
       utl202_belter_apc-extru6.1-0 ; 
       utl202_belter_apc-extru7.1-0 ; 
       utl202_belter_apc-extru8.1-0 ; 
       utl202_belter_apc-lthrust.2-0 ; 
       utl202_belter_apc-lwepemt.2-0 ; 
       utl202_belter_apc-missemt.2-0 ; 
       utl202_belter_apc-null1.1-0 ROOT ; 
       utl202_belter_apc-rthrust.2-0 ; 
       utl202_belter_apc-rwepemt.2-0 ; 
       utl202_belter_apc-SS01.2-0 ; 
       utl202_belter_apc-SS02.2-0 ; 
       utl202_belter_apc-SS03.2-0 ; 
       utl202_belter_apc-SS04.2-0 ; 
       utl202_belter_apc-SS05.2-0 ; 
       utl202_belter_apc-SS06.2-0 ; 
       utl202_belter_apc-trail.2-0 ; 
       utl202_belter_apc-tsmoke.4-0 ; 
       utl202_belter_apc-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl202-belter_apc.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       19 18 110 ; 
       0 18 110 ; 
       28 18 110 ; 
       28 29 111 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 18 110 ; 
       24 18 110 ; 
       25 18 110 ; 
       26 18 110 ; 
       27 18 110 ; 
       29 18 110 ; 
       1 11 110 ; 
       2 1 110 ; 
       3 11 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 18 110 ; 
       12 3 110 ; 
       13 12 110 ; 
       14 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 0 300 ; 
       22 1 300 ; 
       23 3 300 ; 
       24 2 300 ; 
       25 5 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 193.7255 21.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 193.7255 19.32343 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       18 SCHEM 211.3316 20.3006 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 202.9487 1.462407 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 209.5803 4.327671 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 198.8308 3.814519 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 200.797 1.439259 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 212.1338 3.211009 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 209.6338 3.211009 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 207.1338 3.211009 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 216.8555 4.219316 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 216.9182 3.453143 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 219.2928 4.272156 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 219.2498 3.426723 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 221.7135 4.166475 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 221.7565 3.453143 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 201.1251 5.462259 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 198.8981 3.133197 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 210.9008 12.88847 0 MPRFLG 0 ; 
       2 SCHEM 209.6508 10.88847 0 MPRFLG 0 ; 
       3 SCHEM 223.4008 12.88847 0 MPRFLG 0 ; 
       4 SCHEM 222.1508 10.88847 0 MPRFLG 0 ; 
       5 SCHEM 224.6508 10.88847 0 MPRFLG 0 ; 
       6 SCHEM 227.1508 10.88847 0 MPRFLG 0 ; 
       7 SCHEM 229.6508 10.88847 0 MPRFLG 0 ; 
       8 SCHEM 212.1508 10.88847 0 MPRFLG 0 ; 
       9 SCHEM 214.6508 10.88847 0 MPRFLG 0 ; 
       10 SCHEM 207.1508 10.88847 0 MPRFLG 0 ; 
       11 SCHEM 218.4008 14.88847 0 USR MPRFLG 0 ; 
       12 SCHEM 218.4008 10.88847 0 MPRFLG 0 ; 
       13 SCHEM 217.1508 8.888468 0 MPRFLG 0 ; 
       14 SCHEM 219.6508 8.888468 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
