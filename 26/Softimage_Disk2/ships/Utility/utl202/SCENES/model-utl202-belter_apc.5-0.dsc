SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.9-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       utl202_belter_apc-cockpt.2-0 ; 
       utl202_belter_apc-cube21.1-0 ; 
       utl202_belter_apc-cube26.1-0 ; 
       utl202_belter_apc-cube27.1-0 ; 
       utl202_belter_apc-cube29.1-0 ; 
       utl202_belter_apc-cube30.1-0 ; 
       utl202_belter_apc-cube31.1-0 ; 
       utl202_belter_apc-cube32.1-0 ; 
       utl202_belter_apc-cube33.1-0 ; 
       utl202_belter_apc-cube34.1-0 ; 
       utl202_belter_apc-cyl1_1.1-0 ; 
       utl202_belter_apc-extru10.1-0 ; 
       utl202_belter_apc-extru11.1-0 ; 
       utl202_belter_apc-extru12.1-0 ; 
       utl202_belter_apc-extru5.4-0 ; 
       utl202_belter_apc-extru6.1-0 ; 
       utl202_belter_apc-extru7.1-0 ; 
       utl202_belter_apc-extru8.1-0 ; 
       utl202_belter_apc-lthrust.2-0 ; 
       utl202_belter_apc-lwepemt.2-0 ; 
       utl202_belter_apc-missemt.2-0 ; 
       utl202_belter_apc-null1.5-0 ROOT ; 
       utl202_belter_apc-rthrust.2-0 ; 
       utl202_belter_apc-rwepemt.2-0 ; 
       utl202_belter_apc-SS01.2-0 ; 
       utl202_belter_apc-SS02.2-0 ; 
       utl202_belter_apc-SS03.2-0 ; 
       utl202_belter_apc-SS04.2-0 ; 
       utl202_belter_apc-SS05.2-0 ; 
       utl202_belter_apc-SS06.2-0 ; 
       utl202_belter_apc-trail.2-0 ; 
       utl202_belter_apc-tsmoke.4-0 ; 
       utl202_belter_apc-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl202-belter_apc.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 21 110 ; 
       1 14 110 ; 
       2 1 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 3 110 ; 
       12 14 110 ; 
       13 3 110 ; 
       14 21 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 1 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
       31 32 111 ; 
       32 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       24 0 300 ; 
       25 1 300 ; 
       26 3 300 ; 
       27 2 300 ; 
       28 5 300 ; 
       29 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 MPRFLG 0 ; 
       2 SCHEM 45 -6 0 MPRFLG 0 ; 
       3 SCHEM 60 -4 0 MPRFLG 0 ; 
       4 SCHEM 55 -6 0 MPRFLG 0 ; 
       5 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 60 -6 0 MPRFLG 0 ; 
       7 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 50 -6 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 65 -6 0 MPRFLG 0 ; 
       12 SCHEM 70 -4 0 MPRFLG 0 ; 
       13 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 55 -2 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 40 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 36.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
