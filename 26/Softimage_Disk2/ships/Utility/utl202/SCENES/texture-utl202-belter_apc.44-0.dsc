SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.53-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 34     
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
       utl202_belter_apc-mat100.1-0 ; 
       utl202_belter_apc-mat101.1-0 ; 
       utl202_belter_apc-mat102.2-0 ; 
       utl202_belter_apc-mat103.3-0 ; 
       utl202_belter_apc-mat104.2-0 ; 
       utl202_belter_apc-mat105.1-0 ; 
       utl202_belter_apc-mat106.1-0 ; 
       utl202_belter_apc-mat107.1-0 ; 
       utl202_belter_apc-mat108.1-0 ; 
       utl202_belter_apc-mat81.2-0 ; 
       utl202_belter_apc-mat82.2-0 ; 
       utl202_belter_apc-mat83.3-0 ; 
       utl202_belter_apc-mat84.3-0 ; 
       utl202_belter_apc-mat85.2-0 ; 
       utl202_belter_apc-mat86.4-0 ; 
       utl202_belter_apc-mat87.3-0 ; 
       utl202_belter_apc-mat88.2-0 ; 
       utl202_belter_apc-mat89.2-0 ; 
       utl202_belter_apc-mat90.2-0 ; 
       utl202_belter_apc-mat91.3-0 ; 
       utl202_belter_apc-mat92.3-0 ; 
       utl202_belter_apc-mat93.2-0 ; 
       utl202_belter_apc-mat94.2-0 ; 
       utl202_belter_apc-mat95.2-0 ; 
       utl202_belter_apc-mat96.2-0 ; 
       utl202_belter_apc-mat97.2-0 ; 
       utl202_belter_apc-mat98.2-0 ; 
       utl202_belter_apc-mat99.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       utl202_belter_apc-cockpt.2-0 ; 
       utl202_belter_apc-cube21.1-0 ; 
       utl202_belter_apc-cube26.1-0 ; 
       utl202_belter_apc-cube27.1-0 ; 
       utl202_belter_apc-cube31.1-0 ; 
       utl202_belter_apc-cube32.1-0 ; 
       utl202_belter_apc-cube33.1-0 ; 
       utl202_belter_apc-cube35.1-0 ; 
       utl202_belter_apc-cube36.1-0 ; 
       utl202_belter_apc-cube38.1-0 ; 
       utl202_belter_apc-cyl1_1.1-0 ; 
       utl202_belter_apc-extru10.1-0 ; 
       utl202_belter_apc-extru11.1-0 ; 
       utl202_belter_apc-extru12.1-0 ; 
       utl202_belter_apc-extru5.4-0 ; 
       utl202_belter_apc-extru6.1-0 ; 
       utl202_belter_apc-extru7.1-0 ; 
       utl202_belter_apc-extru8.1-0 ; 
       utl202_belter_apc-lthrust.2-0 ; 
       utl202_belter_apc-lwepemt.2-0 ; 
       utl202_belter_apc-missemt.2-0 ; 
       utl202_belter_apc-null1.41-0 ROOT ; 
       utl202_belter_apc-rthrust.2-0 ; 
       utl202_belter_apc-rwepemt.2-0 ; 
       utl202_belter_apc-SS01.2-0 ; 
       utl202_belter_apc-SS02.2-0 ; 
       utl202_belter_apc-SS03.2-0 ; 
       utl202_belter_apc-SS04.2-0 ; 
       utl202_belter_apc-SS05.2-0 ; 
       utl202_belter_apc-SS06.2-0 ; 
       utl202_belter_apc-trail.2-0 ; 
       utl202_belter_apc-tsmoke.4-0 ; 
       utl202_belter_apc-tthrust.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl202/PICTURES/utl202 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl202-belter_apc.44-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 28     
       utl202_belter_apc-t2d1.2-0 ; 
       utl202_belter_apc-t2d10.4-0 ; 
       utl202_belter_apc-t2d11.4-0 ; 
       utl202_belter_apc-t2d12.4-0 ; 
       utl202_belter_apc-t2d13.3-0 ; 
       utl202_belter_apc-t2d14.2-0 ; 
       utl202_belter_apc-t2d15.2-0 ; 
       utl202_belter_apc-t2d16.2-0 ; 
       utl202_belter_apc-t2d17.2-0 ; 
       utl202_belter_apc-t2d18.2-0 ; 
       utl202_belter_apc-t2d19.2-0 ; 
       utl202_belter_apc-t2d2.2-0 ; 
       utl202_belter_apc-t2d20.2-0 ; 
       utl202_belter_apc-t2d21.2-0 ; 
       utl202_belter_apc-t2d22.4-0 ; 
       utl202_belter_apc-t2d23.5-0 ; 
       utl202_belter_apc-t2d24.4-0 ; 
       utl202_belter_apc-t2d25.3-0 ; 
       utl202_belter_apc-t2d26.1-0 ; 
       utl202_belter_apc-t2d27.1-0 ; 
       utl202_belter_apc-t2d28.1-0 ; 
       utl202_belter_apc-t2d3.4-0 ; 
       utl202_belter_apc-t2d4.4-0 ; 
       utl202_belter_apc-t2d5.3-0 ; 
       utl202_belter_apc-t2d6.6-0 ; 
       utl202_belter_apc-t2d7.6-0 ; 
       utl202_belter_apc-t2d8.2-0 ; 
       utl202_belter_apc-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 21 110 ; 
       1 14 110 ; 
       2 1 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       10 1 110 ; 
       11 3 110 ; 
       12 14 110 ; 
       13 3 110 ; 
       14 21 110 ; 
       15 3 110 ; 
       16 15 110 ; 
       17 1 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 21 110 ; 
       25 21 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
       31 32 111 ; 
       32 21 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 24 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       3 20 300 ; 
       3 8 300 ; 
       4 31 300 ; 
       5 30 300 ; 
       6 13 300 ; 
       7 32 300 ; 
       8 33 300 ; 
       10 25 300 ; 
       10 12 300 ; 
       11 28 300 ; 
       12 22 300 ; 
       13 29 300 ; 
       14 21 300 ; 
       14 23 300 ; 
       15 17 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       16 15 300 ; 
       16 16 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       24 0 300 ; 
       25 1 300 ; 
       26 3 300 ; 
       27 2 300 ; 
       28 5 300 ; 
       29 4 300 ; 
       9 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 12 401 ; 
       7 13 401 ; 
       8 14 401 ; 
       9 15 401 ; 
       10 16 401 ; 
       11 17 401 ; 
       15 0 401 ; 
       16 11 401 ; 
       17 21 401 ; 
       18 22 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       22 26 401 ; 
       23 27 401 ; 
       24 1 401 ; 
       25 2 401 ; 
       26 3 401 ; 
       27 4 401 ; 
       28 5 401 ; 
       29 6 401 ; 
       30 7 401 ; 
       31 8 401 ; 
       32 9 401 ; 
       33 10 401 ; 
       12 18 401 ; 
       13 19 401 ; 
       14 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 85 -4 0 MPRFLG 0 ; 
       4 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 85 -6 0 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 95 -6 0 MPRFLG 0 ; 
       10 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 90 -6 0 MPRFLG 0 ; 
       14 SCHEM 73.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 75 -6 0 MPRFLG 0 ; 
       16 SCHEM 71.25 -8 0 MPRFLG 0 ; 
       17 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 30 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 35 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 37.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
