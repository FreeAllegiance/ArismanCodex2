SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.6-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       utl203_belter_pod-mat123.3-0 ; 
       utl203_belter_pod-mat123_1.1-0 ; 
       utl203_belter_pod-mat124.3-0 ; 
       utl203_belter_pod-mat124_1.1-0 ; 
       utl203_belter_pod-mat125.3-0 ; 
       utl203_belter_pod-mat125_1.1-0 ; 
       utl203_belter_pod-mat126.2-0 ; 
       utl203_belter_pod-mat126_1.1-0 ; 
       utl203_belter_pod-mat127.1-0 ; 
       utl203_belter_pod-mat127_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       root-cockpt.1-0 ; 
       root-extru4.2-0 ; 
       root-extru4_2.2-0 ; 
       root-lthrust.1-0 ; 
       root-root.4-0 ROOT ; 
       root-root_3.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl203/PICTURES/utl203 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       both-utl203-belter_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       utl203_belter_pod-t2d42.3-0 ; 
       utl203_belter_pod-t2d42_1.1-0 ; 
       utl203_belter_pod-t2d43.3-0 ; 
       utl203_belter_pod-t2d43_1.1-0 ; 
       utl203_belter_pod-t2d44.3-0 ; 
       utl203_belter_pod-t2d44_1.1-0 ; 
       utl203_belter_pod-t2d45.2-0 ; 
       utl203_belter_pod-t2d45_1.1-0 ; 
       utl203_belter_pod-t2d46.1-0 ; 
       utl203_belter_pod-t2d46_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 5 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       2 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 6 300 ; 
       1 8 300 ; 
       1 10 300 ; 
       1 12 300 ; 
       8 0 300 ; 
       9 1 300 ; 
       10 3 300 ; 
       11 2 300 ; 
       2 5 300 ; 
       2 7 300 ; 
       2 9 300 ; 
       2 11 300 ; 
       2 13 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       6 2 401 ; 
       8 4 401 ; 
       10 6 401 ; 
       12 8 401 ; 
       5 1 401 ; 
       7 3 401 ; 
       9 5 401 ; 
       11 7 401 ; 
       13 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 19.5 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 26.62818 -0.5908026 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 23.56534 3.877388 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -0.3727745 MPRFLG 0 ; 
       6 SCHEM 31.62818 -0.5908026 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 34.14506 -0.5908026 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 16.05577 -1.986662 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 16.26042 -2.899586 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 23.55577 -1.986662 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 23.32504 -3.134337 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 29.12818 -0.5908026 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 46.56699 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 50.63233 3.877388 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 -0.3727745 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 37.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 19.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24.56699 -1.942583 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 46.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.63398 -1.942583 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54.13398 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24.90112 -4.153109 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.06699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.56699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 44.06699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 46.56699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.96811 -4.153109 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54.13398 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
