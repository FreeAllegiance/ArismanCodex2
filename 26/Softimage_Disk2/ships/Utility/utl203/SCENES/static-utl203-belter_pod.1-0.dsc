SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.4-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       utl203_belter_pod-mat123.3-0 ; 
       utl203_belter_pod-mat124.3-0 ; 
       utl203_belter_pod-mat125.3-0 ; 
       utl203_belter_pod-mat126.2-0 ; 
       utl203_belter_pod-mat127.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       root-extru4.2-0 ; 
       root-root.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl203/PICTURES/utl203 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-utl203-belter_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       utl203_belter_pod-t2d42.3-0 ; 
       utl203_belter_pod-t2d43.3-0 ; 
       utl203_belter_pod-t2d44.3-0 ; 
       utl203_belter_pod-t2d45.2-0 ; 
       utl203_belter_pod-t2d46.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 11.56534 3.877388 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.56699 -1.942583 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.90112 -4.153109 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15.06699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
