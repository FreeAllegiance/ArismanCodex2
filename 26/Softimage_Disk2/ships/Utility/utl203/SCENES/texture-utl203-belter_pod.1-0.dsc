SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.3-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       utl203_belter_pod-mat123.3-0 ; 
       utl203_belter_pod-mat124.3-0 ; 
       utl203_belter_pod-mat125.3-0 ; 
       utl203_belter_pod-mat126.2-0 ; 
       utl203_belter_pod-mat127.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       root-extru4.2-0 ; 
       root-lthrust.1-0 ; 
       root-root.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl203/PICTURES/utl203 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl203-belter_pod.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       utl203_belter_pod-t2d42.3-0 ; 
       utl203_belter_pod-t2d43.3-0 ; 
       utl203_belter_pod-t2d44.3-0 ; 
       utl203_belter_pod-t2d45.2-0 ; 
       utl203_belter_pod-t2d46.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       0 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       5 0 300 ; 
       6 1 300 ; 
       7 3 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 11.56534 3.877388 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 14.62818 -0.5908026 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19.62818 -0.5908026 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.14506 -0.5908026 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 MPRFLG 0 ; 
       5 SCHEM 4.055767 -1.986662 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 4.260416 -2.899586 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       7 SCHEM 11.55577 -1.986662 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       8 SCHEM 11.32504 -3.134337 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 17.12818 -0.5908026 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.56699 -1.942583 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 17.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.56699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25.06699 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.90112 -4.153109 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15.06699 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
