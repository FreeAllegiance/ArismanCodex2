SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig32_belter_stealth-mat111.2-0 ; 
       fig32_belter_stealth-mat112.2-0 ; 
       utl204_belter_probe-mat118.2-0 ; 
       utl204_belter_probe-mat119.2-0 ; 
       utl204_belter_probe-mat120.2-0 ; 
       utl204_belter_probe-mat121.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube27.1-0 ; 
       utl204_belter_probe-cube28.1-0 ; 
       utl204_belter_probe-cube29.1-0 ; 
       utl204_belter_probe-cube30.1-0 ; 
       utl204_belter_probe-cube4.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-lthrust.1-0 ; 
       utl204_belter_probe-root.1-0 ROOT ; 
       utl204_belter_probe-rthrust.1-0 ; 
       utl204_belter_probe-rwepemt.1-0 ; 
       utl204_belter_probe-SS01.1-0 ; 
       utl204_belter_probe-SS02.1-0 ; 
       utl204_belter_probe-SS03.1-0 ; 
       utl204_belter_probe-SS04.1-0 ; 
       utl204_belter_probe-SS05.1-0 ; 
       utl204_belter_probe-SS06.1-0 ; 
       utl204_belter_probe-SS07.1-0 ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-utl204-belter_probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       fig32_belter_stealth-t2d29.2-0 ; 
       fig32_belter_stealth-t2d30.2-0 ; 
       utl204_belter_probe-t2d36.2-0 ; 
       utl204_belter_probe-t2d37.2-0 ; 
       utl204_belter_probe-t2d38.2-0 ; 
       utl204_belter_probe-t2d39.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       1 2 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       18 0 110 ; 
       7 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       19 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       11 0 300 ; 
       12 1 300 ; 
       13 3 300 ; 
       14 2 300 ; 
       15 5 300 ; 
       16 4 300 ; 
       17 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 72.07513 3.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 72.07513 1.499569 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 81.29301 6.977524 0 USR DISPLAY 3 2 MPRFLG 0 ; 
       1 SCHEM 82.54301 2.977522 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 82.54301 4.977523 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 85.04301 4.977523 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 85.04301 2.977522 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 77.54301 4.977523 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 77.54301 2.977522 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 81.29301 8.977524 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 80.04301 4.977523 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 74.07697 -2.559729 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 78.75116 -2.463799 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 82.31263 -2.481919 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 91.61936 0.1140029 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 91.68206 -0.6521669 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 94.05666 0.1668422 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 94.01369 -0.6785875 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 96.47739 0.06116171 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 96.52041 -0.6521669 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 98.84978 -0.0104268 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 76.30391 -0.2306684 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 71.94177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 69.44177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 74.44177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 76.94177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 81.94177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 79.44177 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 82.3413 13.3585 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
