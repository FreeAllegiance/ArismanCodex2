SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       fig32_belter_stealth-mat111.2-0 ; 
       fig32_belter_stealth-mat112.2-0 ; 
       utl204_belter_probe-mat126.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
       utl204_belter_probe-mat132.1-0 ; 
       utl204_belter_probe-mat133.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube34.1-0 ; 
       utl204_belter_probe-cube35.1-0 ; 
       utl204_belter_probe-cube4.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-root.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/utl204 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-utl204-belter_probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       fig32_belter_stealth-t2d29.3-0 ; 
       fig32_belter_stealth-t2d30.3-0 ; 
       utl204_belter_probe-t2d44.1-0 ; 
       utl204_belter_probe-t2d48.1-0 ; 
       utl204_belter_probe-t2d49.2-0 ; 
       utl204_belter_probe-t2d50.1-0 ; 
       utl204_belter_probe-t2d51.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 3 300 ; 
       2 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       5 0 300 ; 
       6 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 24.05409 3.837628 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 37.8041 1.837629 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 36.5541 -0.1623713 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 42.8041 1.837629 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 41.5541 -0.1623713 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 30.30409 1.837629 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 29.05409 -0.1623713 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 24.05409 5.837628 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 25.29813 -6.423333 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.79813 -7.320166 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34.70537 -4.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.81907 -8.665483 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 37.31907 -9.483519 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 44.81907 -6.000002 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.31907 -6.818038 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25.29813 -8.42333 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.79813 -9.320164 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34.70537 -6.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39.81907 -10.66548 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 37.31907 -11.48352 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 44.81907 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 42.31907 -8.818038 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
