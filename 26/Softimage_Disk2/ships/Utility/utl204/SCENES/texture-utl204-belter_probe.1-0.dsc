SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.3-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig32_belter_stealth-mat111.2-0 ; 
       fig32_belter_stealth-mat112.2-0 ; 
       utl204_belter_probe-mat122.1-0 ; 
       utl204_belter_probe-mat123.1-0 ; 
       utl204_belter_probe-mat124.1-0 ; 
       utl204_belter_probe-mat125.1-0 ; 
       utl204_belter_probe-mat126.1-0 ; 
       utl204_belter_probe-mat127.1-0 ; 
       utl204_belter_probe-mat128.1-0 ; 
       utl204_belter_probe-mat129.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube29.1-0 ; 
       utl204_belter_probe-cube30.1-0 ; 
       utl204_belter_probe-cube31.1-0 ROOT ; 
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube4.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-lthrust.1-0 ; 
       utl204_belter_probe-root.2-0 ROOT ; 
       utl204_belter_probe-rthrust.1-0 ; 
       utl204_belter_probe-rwepemt.1-0 ; 
       utl204_belter_probe-SS01.1-0 ; 
       utl204_belter_probe-SS02.1-0 ; 
       utl204_belter_probe-SS03.1-0 ; 
       utl204_belter_probe-SS04.1-0 ; 
       utl204_belter_probe-SS05.1-0 ; 
       utl204_belter_probe-SS06.1-0 ; 
       utl204_belter_probe-SS07.1-0 ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/fig32 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/utl204 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl204-belter_probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       fig32_belter_stealth-t2d29.3-0 ; 
       fig32_belter_stealth-t2d30.3-0 ; 
       utl204_belter_probe-t2d40.1-0 ; 
       utl204_belter_probe-t2d41.1-0 ; 
       utl204_belter_probe-t2d42.1-0 ; 
       utl204_belter_probe-t2d43.1-0 ; 
       utl204_belter_probe-t2d44.1-0 ; 
       utl204_belter_probe-t2d45.1-0 ; 
       utl204_belter_probe-t2d46.1-0 ; 
       utl204_belter_probe-t2d47.1-0 ; 
       utl204_belter_probe-t2d48.1-0 ; 
       utl204_belter_probe-t2d49.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       19 0 110 ; 
       8 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       20 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 9 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       0 13 300 ; 
       1 16 300 ; 
       2 15 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       4 17 300 ; 
       5 18 300 ; 
       19 14 300 ; 
       12 0 300 ; 
       13 1 300 ; 
       14 3 300 ; 
       15 2 300 ; 
       16 5 300 ; 
       17 4 300 ; 
       18 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       17 10 401 ; 
       18 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 42.85457 11.43 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0.2286792 1.388299 1.584067 MPRFLG 0 ; 
       0 SCHEM 25.80271 9.429995 0 USR MPRFLG 0 ; 
       1 SCHEM 29.55271 7.429996 0 USR MPRFLG 0 ; 
       2 SCHEM 29.53301 6.61196 0 USR MPRFLG 0 ; 
       6 SCHEM 22.05272 7.429996 0 USR MPRFLG 0 ; 
       7 SCHEM 21.44921 5.946612 0 USR MPRFLG 0 ; 
       4 SCHEM 82.40643 11.43 0 USR DISPLAY 3 2 MPRFLG 0 ; 
       5 SCHEM 82.38673 10.61196 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 25.80271 11.43 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 24.55271 7.429996 0 USR MPRFLG 0 ; 
       8 SCHEM 2.137143 -11.11392 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.811338 -11.01799 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10.37281 -11.03611 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19.67954 -8.440188 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 19.74224 -9.206356 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 22.11684 -8.387349 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 22.07387 -9.232777 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 24.53757 -8.49303 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 24.58058 -9.206356 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 26.90996 -8.564618 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 4.364088 -8.784859 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 24.93527 5.006664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.43527 4.10983 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20.0424 -10.86352 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34.34251 7.177504 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 62.40643 11.43 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 69.90643 11.43 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 77.40643 11.43 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.10509 -11.62969 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.43673 -11.65611 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.4797 -10.81068 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.94344 -11.62969 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.90043 -10.91636 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.27282 -10.98795 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49.90643 10.25276 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 52.40643 10.25276 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54.90643 10.25276 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.40643 10.25276 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 130.2601 15.43 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 122.7601 15.43 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24.93527 3.006665 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.43527 2.109831 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 49.90643 8.252756 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 52.40643 8.252756 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 59.90643 11.43 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 67.40643 11.43 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 74.90643 11.43 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 79.90643 11.43 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 54.90643 8.252756 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 57.40643 8.252756 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 132.7601 15.43 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 127.7601 15.43 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
