SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.4-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig32_belter_stealth-mat111.2-0 ; 
       fig32_belter_stealth-mat112.2-0 ; 
       utl204_belter_probe-mat126.1-0 ; 
       utl204_belter_probe-mat127.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
       utl204_belter_probe-mat132.1-0 ; 
       utl204_belter_probe-mat133.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube34.1-0 ; 
       utl204_belter_probe-cube35.1-0 ; 
       utl204_belter_probe-cube4.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-lthrust.1-0 ; 
       utl204_belter_probe-root.3-0 ROOT ; 
       utl204_belter_probe-rthrust.1-0 ; 
       utl204_belter_probe-rwepemt.1-0 ; 
       utl204_belter_probe-SS01.1-0 ; 
       utl204_belter_probe-SS02.1-0 ; 
       utl204_belter_probe-SS03.1-0 ; 
       utl204_belter_probe-SS04.1-0 ; 
       utl204_belter_probe-SS05.1-0 ; 
       utl204_belter_probe-SS06.1-0 ; 
       utl204_belter_probe-SS07.1-0 ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/utl204 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl204-belter_probe.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       fig32_belter_stealth-t2d29.3-0 ; 
       fig32_belter_stealth-t2d30.3-0 ; 
       utl204_belter_probe-t2d44.1-0 ; 
       utl204_belter_probe-t2d45.1-0 ; 
       utl204_belter_probe-t2d48.1-0 ; 
       utl204_belter_probe-t2d49.2-0 ; 
       utl204_belter_probe-t2d50.1-0 ; 
       utl204_belter_probe-t2d51.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 8 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       18 0 110 ; 
       7 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       19 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       3 13 300 ; 
       4 14 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       18 10 300 ; 
       11 0 300 ; 
       12 1 300 ; 
       13 3 300 ; 
       14 2 300 ; 
       15 5 300 ; 
       16 4 300 ; 
       17 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       11 4 401 ; 
       12 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 24.05409 3.837628 0 MPRFLG 0 ; 
       3 SCHEM 42.8041 1.837629 0 MPRFLG 0 ; 
       4 SCHEM 41.5541 -0.1623713 0 MPRFLG 0 ; 
       5 SCHEM 30.30409 1.837629 0 MPRFLG 0 ; 
       6 SCHEM 29.05409 -0.1623713 0 MPRFLG 0 ; 
       1 SCHEM 37.8041 1.837629 0 MPRFLG 0 ; 
       2 SCHEM 36.5541 -0.1623713 0 MPRFLG 0 ; 
       8 SCHEM 24.05409 5.837628 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 34.0541 1.837629 0 MPRFLG 0 ; 
       7 SCHEM 19.05409 1.837629 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24.05409 1.837629 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.55409 1.837629 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.554087 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 4.054086 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 9.054082 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 6.554085 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 11.55408 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 14.05408 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 16.55409 1.837629 0 WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 21.55409 1.837629 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 25.29813 -6.423333 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.79813 -7.320166 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34.70537 -4.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24.91557 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44.81907 -6.000002 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.31907 -6.818038 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39.81907 -8.665483 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.31907 -9.483519 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25.29813 -8.42333 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.79813 -9.320164 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34.70537 -6.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24.91557 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44.81907 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.31907 -8.818038 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39.81907 -10.66548 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.31907 -11.48352 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
