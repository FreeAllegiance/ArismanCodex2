SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.1-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 8     
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       fig32_belter_stealth-mat113.1-0 ; 
       utl204_belter_probe-mat118.1-0 ; 
       utl204_belter_probe-mat119.1-0 ; 
       utl204_belter_probe-mat120.1-0 ; 
       utl204_belter_probe-mat121.1-0 ; 
       utl204_belter_probe-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       utl204_belter_probe-cube25.1-0 ROOT ; 
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube27.1-0 ; 
       utl204_belter_probe-cube28.1-0 ; 
       utl204_belter_probe-cube29.1-0 ; 
       utl204_belter_probe-cube30.1-0 ; 
       utl204_belter_probe-cube4.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-cyl14.1-0 ; 
       utl204_belter_probe-null1.1-0 ROOT ; 
       utl204_belter_probe-root.1-0 ; 
       utl204_belter_probe-tetra1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/fig31 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl204/PICTURES/fig32 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-utl204-belter_probe.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       fig32_belter_stealth-t2d29.1-0 ; 
       fig32_belter_stealth-t2d30.1-0 ; 
       fig32_belter_stealth-t2d31.1-0 ; 
       utl204_belter_probe-t2d16.1-0 ; 
       utl204_belter_probe-t2d36.1-0 ; 
       utl204_belter_probe-t2d37.1-0 ; 
       utl204_belter_probe-t2d38.1-0 ; 
       utl204_belter_probe-t2d39.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 10 110 ; 
       11 1 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 4 110 ; 
       10 9 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       0 7 300 ; 
       6 0 300 ; 
       7 1 300 ; 
       8 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       7 3 401 ; 
       3 4 401 ; 
       4 5 401 ; 
       5 6 401 ; 
       6 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 9.884058 0.3903644 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 9.884058 -1.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 12.38406 -3.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 12.38406 -1.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 14.88406 -1.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 14.88406 -3.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 9.519697 1.760286 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 17.38406 1.760286 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -5.317307 8.19919 MPRFLG 0 ; 
       6 SCHEM 6.134058 -1.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 4.884058 -3.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 7.384058 -3.609636 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
