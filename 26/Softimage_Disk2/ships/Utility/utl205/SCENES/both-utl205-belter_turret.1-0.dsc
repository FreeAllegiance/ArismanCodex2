SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.4-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
       fig30_belter_ftr-mat81.3-0 ; 
       utl204_belter_probe-mat127.2-0 ; 
       utl204_belter_probe-mat130.2-0 ; 
       utl204_belter_probe-mat130_1.1-0 ; 
       utl204_belter_probe-mat131.2-0 ; 
       utl204_belter_probe-mat131_1.1-0 ; 
       utl205_belter_turret-mat134.2-0 ; 
       utl205_belter_turret-mat134_1.1-0 ; 
       utl205_belter_turret-mat136.3-0 ; 
       utl205_belter_turret-mat136_1.1-0 ; 
       utl205_belter_turret-mat137.2-0 ; 
       utl205_belter_turret-mat142.2-0 ; 
       utl205_belter_turret-mat142_1.1-0 ; 
       utl205_belter_turret-mat143.2-0 ; 
       utl205_belter_turret-mat143_1.1-0 ; 
       utl205_belter_turret-mat144.3-0 ; 
       utl205_belter_turret-mat144_1.1-0 ; 
       utl205_belter_turret-mat145.2-0 ; 
       utl205_belter_turret-mat145_1.1-0 ; 
       utl205_belter_turret-mat146.2-0 ; 
       utl205_belter_turret-mat146_1.1-0 ; 
       utl205_belter_turret-mat147.2-0 ; 
       utl205_belter_turret-mat147_1.1-0 ; 
       utl205_belter_turret-mat148.1-0 ; 
       utl205_belter_turret-mat149.1-0 ; 
       utl205_belter_turret-mat149_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       root-cube32.1-0 ; 
       root-cube32_1.1-0 ; 
       root-cube33.1-0 ; 
       root-cube33_1.1-0 ; 
       root-cube37.1-0 ; 
       root-cube37_1.1-0 ; 
       root-cube38.1-0 ; 
       root-cube38_1.1-0 ; 
       root-cube43.1-0 ; 
       root-cube43_1.1-0 ; 
       root-cube44.1-0 ; 
       root-cube44_1.1-0 ; 
       root-cube45.1-0 ; 
       root-cube45_1.1-0 ; 
       root-cube46.1-0 ; 
       root-cube46_1.1-0 ; 
       root-cyl2.1-0 ; 
       root-cyl2_1.1-0 ; 
       root-lthrust.1-0 ; 
       root-root.4-0 ROOT ; 
       root-root_2.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere1.1-0 ; 
       root-sphere1_1.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl205 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       both-utl205-belter_turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       utl204_belter_probe-t2d45.4-0 ; 
       utl204_belter_probe-t2d48.4-0 ; 
       utl204_belter_probe-t2d48_1.1-0 ; 
       utl204_belter_probe-t2d49.4-0 ; 
       utl204_belter_probe-t2d49_1.1-0 ; 
       utl205_belter_turret-t2d52.4-0 ; 
       utl205_belter_turret-t2d52_1.1-0 ; 
       utl205_belter_turret-t2d54.4-0 ; 
       utl205_belter_turret-t2d54_1.1-0 ; 
       utl205_belter_turret-t2d55.4-0 ; 
       utl205_belter_turret-t2d60.4-0 ; 
       utl205_belter_turret-t2d60_1.1-0 ; 
       utl205_belter_turret-t2d61.4-0 ; 
       utl205_belter_turret-t2d61_1.1-0 ; 
       utl205_belter_turret-t2d62.4-0 ; 
       utl205_belter_turret-t2d62_1.1-0 ; 
       utl205_belter_turret-t2d63.3-0 ; 
       utl205_belter_turret-t2d63_1.1-0 ; 
       utl205_belter_turret-t2d64.3-0 ; 
       utl205_belter_turret-t2d64_1.1-0 ; 
       utl205_belter_turret-t2d65.3-0 ; 
       utl205_belter_turret-t2d65_1.1-0 ; 
       utl205_belter_turret-t2d66.1-0 ; 
       utl205_belter_turret-t2d66_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 0 110 ; 
       4 6 110 ; 
       6 19 110 ; 
       8 4 110 ; 
       10 8 110 ; 
       12 14 110 ; 
       14 6 110 ; 
       16 23 110 ; 
       18 6 110 ; 
       21 6 110 ; 
       22 6 110 ; 
       23 6 110 ; 
       25 6 110 ; 
       26 6 110 ; 
       27 6 110 ; 
       28 6 110 ; 
       29 6 110 ; 
       30 6 110 ; 
       31 6 110 ; 
       32 6 110 ; 
       33 4 110 ; 
       34 4 110 ; 
       35 6 110 ; 
       1 5 110 ; 
       3 1 110 ; 
       5 7 110 ; 
       7 20 110 ; 
       9 5 110 ; 
       11 9 110 ; 
       13 15 110 ; 
       15 7 110 ; 
       17 24 110 ; 
       24 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       2 10 300 ; 
       4 14 300 ; 
       6 25 300 ; 
       8 17 300 ; 
       10 19 300 ; 
       12 21 300 ; 
       12 30 300 ; 
       14 27 300 ; 
       16 12 300 ; 
       23 23 300 ; 
       25 0 300 ; 
       26 1 300 ; 
       27 3 300 ; 
       28 2 300 ; 
       29 5 300 ; 
       30 4 300 ; 
       31 6 300 ; 
       32 29 300 ; 
       33 7 300 ; 
       34 16 300 ; 
       1 9 300 ; 
       3 11 300 ; 
       5 15 300 ; 
       7 26 300 ; 
       9 18 300 ; 
       11 20 300 ; 
       13 22 300 ; 
       13 31 300 ; 
       15 28 300 ; 
       17 13 300 ; 
       24 24 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       10 3 401 ; 
       12 5 401 ; 
       14 7 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       19 12 401 ; 
       21 14 401 ; 
       23 16 401 ; 
       25 18 401 ; 
       27 20 401 ; 
       30 22 401 ; 
       9 2 401 ; 
       11 4 401 ; 
       13 6 401 ; 
       15 8 401 ; 
       18 11 401 ; 
       20 13 401 ; 
       22 15 401 ; 
       24 17 401 ; 
       26 19 401 ; 
       28 21 401 ; 
       31 23 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 10 -8 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       14 SCHEM 15 -4 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 17.25401 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 -0.2821491 MPRFLG 0 ; 
       21 SCHEM 22.254 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 18.94465 -8.454759 0 USR MPRFLG 0 ; 
       23 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       25 SCHEM -0.2459987 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM -0.2615894 -11.55815 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 7.254003 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 7.269601 -11.60228 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 9.754002 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 9.694283 -11.60228 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 3.500051 -10.98442 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 3.470023 -11.76686 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 5 -6 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       35 SCHEM 19.754 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 31.754 -6 0 MPRFLG 0 ; 
       3 SCHEM 31.754 -8 0 MPRFLG 0 ; 
       5 SCHEM 33.004 -4 0 MPRFLG 0 ; 
       7 SCHEM 33.004 -2 0 MPRFLG 0 ; 
       9 SCHEM 34.254 -6 0 MPRFLG 0 ; 
       11 SCHEM 34.254 -8 0 MPRFLG 0 ; 
       13 SCHEM 39.254 -6 0 MPRFLG 0 ; 
       15 SCHEM 39.254 -4 0 MPRFLG 0 ; 
       17 SCHEM 26.754 -6 0 MPRFLG 0 ; 
       20 SCHEM 33.004 0 0 SRT 1 1 1 0 0 0 0 0 -0.2821491 MPRFLG 0 ; 
       24 SCHEM 26.754 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.749916 -35.01283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 34.90718 -35.94515 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.69993 -7.473351 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 90.07137 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24.19993 1.757927 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 65.52676 -11.0102 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24.754 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 33.254 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 30.754 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 31.00392 -35.01283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 59.16118 -35.94515 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 35.754 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 33.254 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 45.95393 -7.473351 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 28.254 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 114.3254 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 48.45393 1.757927 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 49.008 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.749916 -37.01282 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 34.90718 -37.94515 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.69993 -9.473351 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 90.07137 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24.19993 -0.2420731 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 27.254 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 33.254 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 30.754 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 31.00392 -37.01282 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 59.16118 -37.94515 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 35.754 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 33.254 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 45.95393 -9.473351 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 28.254 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 114.3254 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 48.45393 -0.2420731 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.508 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
