SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.3-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       utl204_belter_probe-mat130.2-0 ; 
       utl204_belter_probe-mat131.2-0 ; 
       utl205_belter_turret-mat134.2-0 ; 
       utl205_belter_turret-mat136.3-0 ; 
       utl205_belter_turret-mat142.2-0 ; 
       utl205_belter_turret-mat143.2-0 ; 
       utl205_belter_turret-mat144.3-0 ; 
       utl205_belter_turret-mat145.2-0 ; 
       utl205_belter_turret-mat146.2-0 ; 
       utl205_belter_turret-mat147.2-0 ; 
       utl205_belter_turret-mat149.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       root-cube32.1-0 ; 
       root-cube33.1-0 ; 
       root-cube37.1-0 ; 
       root-cube38.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube46.1-0 ; 
       root-cyl2.1-0 ; 
       root-root.3-0 ROOT ; 
       root-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl205 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-utl205-belter_turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       utl204_belter_probe-t2d48.4-0 ; 
       utl204_belter_probe-t2d49.4-0 ; 
       utl205_belter_turret-t2d52.4-0 ; 
       utl205_belter_turret-t2d54.4-0 ; 
       utl205_belter_turret-t2d60.4-0 ; 
       utl205_belter_turret-t2d61.4-0 ; 
       utl205_belter_turret-t2d62.4-0 ; 
       utl205_belter_turret-t2d63.3-0 ; 
       utl205_belter_turret-t2d64.3-0 ; 
       utl205_belter_turret-t2d65.3-0 ; 
       utl205_belter_turret-t2d66.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 0 110 ; 
       2 3 110 ; 
       3 9 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       10 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       2 3 300 ; 
       3 8 300 ; 
       4 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       6 10 300 ; 
       7 9 300 ; 
       8 2 300 ; 
       10 7 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 8.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 8.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.749916 -35.01283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34.90718 -35.94515 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.69993 -7.473351 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 90.07137 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 24.19993 1.757927 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24.754 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.749916 -37.01282 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34.90718 -37.94515 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.69993 -9.473351 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90.07137 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24.19993 -0.2420731 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.254 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
