SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.5-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       edit_nulls-mat70.2-0 ; 
       fig20_biofighter-mat71.2-0 ; 
       fig20_biofighter-mat75.2-0 ; 
       fig20_biofighter-mat77.2-0 ; 
       fig20_biofighter-mat78.2-0 ; 
       fig20_biofighter-mat80.2-0 ; 
       fig30_belter_ftr-mat81.2-0 ; 
       utl204_belter_probe-mat127.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
       utl205_belter_turret-mat134.1-0 ; 
       utl205_belter_turret-mat136.2-0 ; 
       utl205_belter_turret-mat137.1-0 ; 
       utl205_belter_turret-mat142.1-0 ; 
       utl205_belter_turret-mat143.1-0 ; 
       utl205_belter_turret-mat144.1-0 ; 
       utl205_belter_turret-mat145.1-0 ; 
       utl205_belter_turret-mat146.1-0 ; 
       utl205_belter_turret-mat147.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube37.1-0 ; 
       utl204_belter_probe-cube38.1-0 ; 
       utl204_belter_probe-cube43.1-0 ; 
       utl204_belter_probe-cube44.1-0 ; 
       utl204_belter_probe-cube45.1-0 ; 
       utl204_belter_probe-cube46.1-0 ; 
       utl204_belter_probe-cyl2.1-0 ; 
       utl204_belter_probe-lthrust.2-0 ROOT ; 
       utl204_belter_probe-root_1.1-0 ROOT ; 
       utl204_belter_probe-rthrust.2-0 ROOT ; 
       utl204_belter_probe-rwepemt.2-0 ROOT ; 
       utl204_belter_probe-sphere1.1-0 ; 
       utl204_belter_probe-SS01.2-0 ROOT ; 
       utl204_belter_probe-SS02.2-0 ROOT ; 
       utl204_belter_probe-SS03.2-0 ROOT ; 
       utl204_belter_probe-SS04.2-0 ROOT ; 
       utl204_belter_probe-SS05.2-0 ROOT ; 
       utl204_belter_probe-SS06.2-0 ROOT ; 
       utl204_belter_probe-SS07.2-0 ROOT ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-tetra2.1-0 ; 
       utl204_belter_probe-trail.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl205 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl205-belter_turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       utl204_belter_probe-t2d45.2-0 ; 
       utl204_belter_probe-t2d48.2-0 ; 
       utl204_belter_probe-t2d49.2-0 ; 
       utl205_belter_turret-t2d52.2-0 ; 
       utl205_belter_turret-t2d54.2-0 ; 
       utl205_belter_turret-t2d55.2-0 ; 
       utl205_belter_turret-t2d60.2-0 ; 
       utl205_belter_turret-t2d61.2-0 ; 
       utl205_belter_turret-t2d62.1-0 ; 
       utl205_belter_turret-t2d63.1-0 ; 
       utl205_belter_turret-t2d64.1-0 ; 
       utl205_belter_turret-t2d65.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 0 110 ; 
       2 3 110 ; 
       3 10 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 3 110 ; 
       8 13 110 ; 
       13 3 110 ; 
       21 2 110 ; 
       22 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       1 9 300 ; 
       2 11 300 ; 
       3 17 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       6 15 300 ; 
       7 18 300 ; 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 6 300 ; 
       8 10 300 ; 
       13 16 300 ; 
       21 7 300 ; 
       22 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 74.83601 27.69266 0 MPRFLG 0 ; 
       1 SCHEM 73.58601 25.69265 0 MPRFLG 0 ; 
       2 SCHEM 78.58602 29.69266 0 MPRFLG 0 ; 
       3 SCHEM 78.58602 31.69266 0 MPRFLG 0 ; 
       4 SCHEM 79.83602 27.69266 0 MPRFLG 0 ; 
       5 SCHEM 78.58602 25.69265 0 MPRFLG 0 ; 
       6 SCHEM 92.53595 28.21931 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 158.4074 37.45059 0 USR MPRFLG 0 ; 
       9 SCHEM 19.05409 1.837629 0 WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 1.881877 1.469767 -3.235957 MPRFLG 0 ; 
       11 SCHEM 24.05409 1.837629 0 WIRECOL 1 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 -1.667996 1.469767 -3.221422 MPRFLG 0 ; 
       12 SCHEM 26.55409 1.837629 0 WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 0.0003840923 2.878532 MPRFLG 0 ; 
       14 SCHEM 1.554087 1.837629 0 WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0.8117672 1.948354 2.124539 MPRFLG 0 ; 
       15 SCHEM 4.054086 1.837629 0 WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.8118 1.948354 2.124539 MPRFLG 0 ; 
       16 SCHEM 9.054082 1.837629 0 WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 0.6275632 2.459638 -0.5431502 MPRFLG 0 ; 
       17 SCHEM 6.554085 1.837629 0 WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.6276 2.459638 -0.5431502 MPRFLG 0 ; 
       18 SCHEM 11.55408 1.837629 0 WIRECOL 4 7 SRT 1 1 1 6.3573e-008 3.141593 2.071472e-014 0.813318 0.715209 -0.870683 MPRFLG 0 ; 
       19 SCHEM 14.05408 1.837629 0 WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 -0.8157324 0.7159611 -0.870683 MPRFLG 0 ; 
       20 SCHEM 16.55409 1.837629 0 WIRECOL 4 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 5.560538 -0.2376819 MPRFLG 0 ; 
       23 SCHEM 21.55409 1.837629 0 WIRECOL 1 7 SRT 1 1 1 -6.3573e-008 -5.911491e-024 -3.579809e-023 0 1.469767 -1.753179 MPRFLG 0 ; 
       8 SCHEM 68.58601 27.69266 0 MPRFLG 0 ; 
       10 SCHEM 78.58602 33.69266 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 68.58601 29.69266 0 MPRFLG 0 ; 
       21 SCHEM 71.08601 27.69266 0 MPRFLG 0 ; 
       22 SCHEM 83.58602 27.69266 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 136.1068 14.85539 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 141.1068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 148.6068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 158.6068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 54.18839 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 69.09189 -8.665483 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.59189 -9.483519 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.83593 -1.320167 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 104.7432 -2.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 87.45335 -2.000001 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 133.6068 -2.000003 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 131.1068 -2.818039 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54.18839 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 69.09189 -10.66548 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 66.59189 -11.48352 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.83593 -3.320165 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 104.7432 -4.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.45335 -4.000001 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 133.6068 -4.000003 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 131.1068 -4.818039 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 138.6068 14.85539 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 143.6068 33.69266 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 151.1068 33.69266 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 166.1068 37.45059 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
