SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.6-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       utl204_belter_probe-mat127.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
       utl205_belter_turret-mat134.1-0 ; 
       utl205_belter_turret-mat136.2-0 ; 
       utl205_belter_turret-mat137.1-0 ; 
       utl205_belter_turret-mat142.1-0 ; 
       utl205_belter_turret-mat143.1-0 ; 
       utl205_belter_turret-mat144.1-0 ; 
       utl205_belter_turret-mat145.1-0 ; 
       utl205_belter_turret-mat146.1-0 ; 
       utl205_belter_turret-mat147.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube37.1-0 ; 
       utl204_belter_probe-cube38.1-0 ; 
       utl204_belter_probe-cube43.1-0 ; 
       utl204_belter_probe-cube44.1-0 ; 
       utl204_belter_probe-cube45.1-0 ; 
       utl204_belter_probe-cube46.1-0 ; 
       utl204_belter_probe-cyl2.1-0 ; 
       utl204_belter_probe-root_1.2-0 ROOT ; 
       utl204_belter_probe-sphere1.1-0 ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-tetra2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl205 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl205-belter_turret.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       utl204_belter_probe-t2d45.3-0 ; 
       utl204_belter_probe-t2d48.3-0 ; 
       utl204_belter_probe-t2d49.3-0 ; 
       utl205_belter_turret-t2d52.3-0 ; 
       utl205_belter_turret-t2d54.3-0 ; 
       utl205_belter_turret-t2d55.3-0 ; 
       utl205_belter_turret-t2d60.3-0 ; 
       utl205_belter_turret-t2d61.3-0 ; 
       utl205_belter_turret-t2d62.2-0 ; 
       utl205_belter_turret-t2d63.2-0 ; 
       utl205_belter_turret-t2d64.2-0 ; 
       utl205_belter_turret-t2d65.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 0 110 ; 
       2 3 110 ; 
       3 9 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 3 110 ; 
       8 10 110 ; 
       10 3 110 ; 
       11 2 110 ; 
       12 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 2 300 ; 
       2 4 300 ; 
       3 10 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       7 11 300 ; 
       8 3 300 ; 
       10 9 300 ; 
       11 0 300 ; 
       12 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 74.83601 27.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 73.58601 25.69265 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 78.58602 29.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 78.58602 31.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 79.83602 27.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 78.58602 25.69265 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 92.53595 28.21931 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 158.4074 37.45059 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 68.58601 27.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 78.58602 33.69266 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 68.58601 29.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 71.08601 27.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 83.58602 27.69266 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       8 SCHEM 136.1068 14.85539 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 141.1068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 148.6068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 158.6068 33.69266 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 54.18839 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 69.09189 -8.665483 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 66.59189 -9.483519 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.83593 -1.320167 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 104.7432 -2.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 87.45335 -2.000001 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 133.6068 -2.000003 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 131.1068 -2.818039 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 54.18839 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 69.09189 -10.66548 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 66.59189 -11.48352 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 72.83593 -3.320165 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 104.7432 -4.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.45335 -4.000001 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 133.6068 -4.000003 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 131.1068 -4.818039 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 138.6068 14.85539 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 143.6068 33.69266 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 151.1068 33.69266 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 166.1068 37.45059 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
