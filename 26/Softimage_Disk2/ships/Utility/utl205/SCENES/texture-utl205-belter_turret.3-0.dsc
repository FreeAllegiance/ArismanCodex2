SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.1-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       edit_nulls-mat70.3-0 ; 
       fig20_biofighter-mat71.3-0 ; 
       fig20_biofighter-mat75.3-0 ; 
       fig20_biofighter-mat77.3-0 ; 
       fig20_biofighter-mat78.3-0 ; 
       fig20_biofighter-mat80.3-0 ; 
       fig30_belter_ftr-mat81.3-0 ; 
       utl204_belter_probe-mat127.2-0 ; 
       utl204_belter_probe-mat130.2-0 ; 
       utl204_belter_probe-mat131.2-0 ; 
       utl205_belter_turret-mat134.2-0 ; 
       utl205_belter_turret-mat136.3-0 ; 
       utl205_belter_turret-mat137.2-0 ; 
       utl205_belter_turret-mat142.2-0 ; 
       utl205_belter_turret-mat143.2-0 ; 
       utl205_belter_turret-mat144.2-0 ; 
       utl205_belter_turret-mat145.2-0 ; 
       utl205_belter_turret-mat146.2-0 ; 
       utl205_belter_turret-mat147.2-0 ; 
       utl205_belter_turret-mat148.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 25     
       root-cube32.1-0 ; 
       root-cube33.1-0 ; 
       root-cube37.1-0 ; 
       root-cube38.1-0 ; 
       root-cube43.1-0 ; 
       root-cube44.1-0 ; 
       root-cube45.1-0 ; 
       root-cube46.1-0 ; 
       root-cyl2.1-0 ; 
       root-lthrust.1-0 ; 
       root-root.1-0 ROOT ; 
       root-rthrust.1-0 ; 
       root-rwepemt.1-0 ; 
       root-sphere1.1-0 ; 
       root-SS01.1-0 ; 
       root-SS02.1-0 ; 
       root-SS03.1-0 ; 
       root-SS04.1-0 ; 
       root-SS05.1-0 ; 
       root-SS06.1-0 ; 
       root-SS07.1-0 ; 
       root-SS08.1-0 ; 
       root-tetra1.1-0 ; 
       root-tetra2.1-0 ; 
       root-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl205 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-utl205-belter_turret.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       utl204_belter_probe-t2d45.4-0 ; 
       utl204_belter_probe-t2d48.4-0 ; 
       utl204_belter_probe-t2d49.4-0 ; 
       utl205_belter_turret-t2d52.4-0 ; 
       utl205_belter_turret-t2d54.4-0 ; 
       utl205_belter_turret-t2d55.4-0 ; 
       utl205_belter_turret-t2d60.4-0 ; 
       utl205_belter_turret-t2d61.4-0 ; 
       utl205_belter_turret-t2d62.3-0 ; 
       utl205_belter_turret-t2d63.3-0 ; 
       utl205_belter_turret-t2d64.3-0 ; 
       utl205_belter_turret-t2d65.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 0 110 ; 
       2 3 110 ; 
       3 10 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 3 110 ; 
       8 13 110 ; 
       13 3 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       21 3 110 ; 
       9 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 3 110 ; 
       18 3 110 ; 
       19 3 110 ; 
       20 3 110 ; 
       24 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 8 300 ; 
       1 9 300 ; 
       2 11 300 ; 
       3 17 300 ; 
       4 13 300 ; 
       5 14 300 ; 
       6 15 300 ; 
       7 18 300 ; 
       8 10 300 ; 
       13 16 300 ; 
       22 7 300 ; 
       23 12 300 ; 
       21 19 300 ; 
       14 0 300 ; 
       15 1 300 ; 
       16 3 300 ; 
       17 2 300 ; 
       18 5 300 ; 
       19 4 300 ; 
       20 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -8 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 5 -6 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 3.470023 -11.76686 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 17.25401 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.254 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 18.94465 -8.454759 0 USR MPRFLG 0 ; 
       14 SCHEM -0.2459987 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM -0.2615894 -11.55815 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 7.254003 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 7.269601 -11.60228 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       18 SCHEM 9.754002 -10.67548 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       19 SCHEM 9.694283 -11.60228 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       20 SCHEM 3.500051 -10.98442 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 19.754 -10.67548 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.749916 -35.01283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34.90718 -35.94515 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.69993 -7.473351 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 90.07137 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24.19993 1.757927 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 37.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 65.52676 -11.0102 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.749916 -37.01282 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34.90718 -37.94515 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.69993 -9.473351 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 90.07137 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24.19993 -0.2420731 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
