SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bel_fighter-cam_int1.2-0 ROOT ; 
       bel_fighter-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 22     
       edit_nulls-mat70.1-0 ; 
       fig20_biofighter-mat71.1-0 ; 
       fig20_biofighter-mat75.1-0 ; 
       fig20_biofighter-mat77.1-0 ; 
       fig20_biofighter-mat78.1-0 ; 
       fig20_biofighter-mat80.1-0 ; 
       fig30_belter_ftr-mat81.1-0 ; 
       fig32_belter_stealth-mat111.1-0 ; 
       fig32_belter_stealth-mat112.1-0 ; 
       utl204_belter_probe-mat126.1-0 ; 
       utl204_belter_probe-mat127.1-0 ; 
       utl204_belter_probe-mat130.1-0 ; 
       utl204_belter_probe-mat131.1-0 ; 
       utl204_belter_probe-mat132.1-0 ; 
       utl204_belter_probe-mat133.1-0 ; 
       utl205_belter_turret-mat134.1-0 ; 
       utl205_belter_turret-mat136.1-0 ; 
       utl205_belter_turret-mat137.1-0 ; 
       utl205_belter_turret-mat138.1-0 ; 
       utl205_belter_turret-mat139.1-0 ; 
       utl205_belter_turret-mat142.1-0 ; 
       utl205_belter_turret-mat143.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       utl204_belter_probe-cube26.1-0 ; 
       utl204_belter_probe-cube32.1-0 ; 
       utl204_belter_probe-cube33.1-0 ; 
       utl204_belter_probe-cube34.1-0 ; 
       utl204_belter_probe-cube35.1-0 ; 
       utl204_belter_probe-cube37.1-0 ; 
       utl204_belter_probe-cube38.1-0 ; 
       utl204_belter_probe-cube39.1-0 ; 
       utl204_belter_probe-cube4.1-0 ROOT ; 
       utl204_belter_probe-cube40.1-0 ; 
       utl204_belter_probe-cube43.1-0 ; 
       utl204_belter_probe-cube44.1-0 ; 
       utl204_belter_probe-cyl1.1-0 ; 
       utl204_belter_probe-cyl2.1-0 ; 
       utl204_belter_probe-lthrust.1-0 ROOT ; 
       utl204_belter_probe-root.2-0 ROOT ; 
       utl204_belter_probe-rthrust.1-0 ROOT ; 
       utl204_belter_probe-rwepemt.1-0 ROOT ; 
       utl204_belter_probe-sphere1.1-0 ; 
       utl204_belter_probe-SS01.1-0 ROOT ; 
       utl204_belter_probe-SS02.1-0 ROOT ; 
       utl204_belter_probe-SS03.1-0 ROOT ; 
       utl204_belter_probe-SS04.1-0 ROOT ; 
       utl204_belter_probe-SS05.1-0 ROOT ; 
       utl204_belter_probe-SS06.1-0 ROOT ; 
       utl204_belter_probe-SS07.1-0 ROOT ; 
       utl204_belter_probe-tetra1.1-0 ; 
       utl204_belter_probe-tetra2.1-0 ; 
       utl204_belter_probe-trail.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl205/PICTURES/utl204 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-utl205-belter_turret.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       fig32_belter_stealth-t2d29.1-0 ; 
       fig32_belter_stealth-t2d30.1-0 ; 
       utl204_belter_probe-t2d44.1-0 ; 
       utl204_belter_probe-t2d45.1-0 ; 
       utl204_belter_probe-t2d48.1-0 ; 
       utl204_belter_probe-t2d49.1-0 ; 
       utl204_belter_probe-t2d50.1-0 ; 
       utl204_belter_probe-t2d51.1-0 ; 
       utl205_belter_turret-t2d52.1-0 ; 
       utl205_belter_turret-t2d54.1-0 ; 
       utl205_belter_turret-t2d55.1-0 ; 
       utl205_belter_turret-t2d56.1-0 ; 
       utl205_belter_turret-t2d57.1-0 ; 
       utl205_belter_turret-t2d60.1-0 ; 
       utl205_belter_turret-t2d61.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       18 6 110 ; 
       0 6 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       13 18 110 ; 
       27 5 110 ; 
       12 8 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       26 5 110 ; 
       5 6 110 ; 
       6 15 110 ; 
       7 0 110 ; 
       9 7 110 ; 
       10 5 110 ; 
       11 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 9 300 ; 
       3 13 300 ; 
       4 14 300 ; 
       13 15 300 ; 
       27 17 300 ; 
       8 7 300 ; 
       12 8 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       26 10 300 ; 
       5 16 300 ; 
       7 18 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 21 300 ; 
       19 0 300 ; 
       20 1 300 ; 
       21 3 300 ; 
       22 2 300 ; 
       23 5 300 ; 
       24 4 300 ; 
       25 6 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       8 1 401 ; 
       9 2 401 ; 
       10 3 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 10 401 ; 
       16 9 401 ; 
       18 11 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       18 SCHEM 16.47153 11.63724 0 MPRFLG 0 ; 
       0 SCHEM 8.971526 11.63724 0 MPRFLG 0 ; 
       3 SCHEM 5.221525 9.637241 0 MPRFLG 0 ; 
       4 SCHEM 3.971525 7.63724 0 MPRFLG 0 ; 
       13 SCHEM 16.47153 9.637241 0 MPRFLG 0 ; 
       27 SCHEM 31.47153 9.637241 0 MPRFLG 0 ; 
       8 SCHEM 28.67632 28.45653 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 5.115178e-008 0.5859135 1.484781 MPRFLG 0 ; 
       12 SCHEM 27.42632 26.45653 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 22.72153 9.637241 0 MPRFLG 0 ; 
       2 SCHEM 21.47153 7.63724 0 MPRFLG 0 ; 
       15 SCHEM 18.97153 15.63724 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 18.97153 9.637241 0 MPRFLG 0 ; 
       5 SCHEM 26.47153 11.63724 0 MPRFLG 0 ; 
       6 SCHEM 18.97153 13.63724 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 10.22153 9.637241 0 MPRFLG 0 ; 
       9 SCHEM 8.971526 7.63724 0 MPRFLG 0 ; 
       10 SCHEM 27.72153 9.637241 0 MPRFLG 0 ; 
       11 SCHEM 26.47153 7.63724 0 MPRFLG 0 ; 
       14 SCHEM 19.05409 1.837629 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 1.881879 1.469767 -3.235958 MPRFLG 0 ; 
       16 SCHEM 24.05409 1.837629 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 0 -1.667993 1.469767 -3.221423 MPRFLG 0 ; 
       17 SCHEM 26.55409 1.837629 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 7.674608e-008 0.0003840792 2.878532 MPRFLG 0 ; 
       19 SCHEM 1.554087 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 0.8117673 1.948354 2.124539 MPRFLG 0 ; 
       20 SCHEM 4.054086 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 -0.8117971 1.948354 2.124539 MPRFLG 0 ; 
       21 SCHEM 9.054082 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 0 0.6275634 2.459638 -0.5431499 MPRFLG 0 ; 
       22 SCHEM 6.554085 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 -0.6276 2.459638 -0.5431499 MPRFLG 0 ; 
       23 SCHEM 11.55408 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 6.3573e-008 3.141593 0 0.8133183 0.7152089 -0.8706827 MPRFLG 0 ; 
       24 SCHEM 14.05408 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 -0.8157294 0.7159613 -0.8706827 MPRFLG 0 ; 
       25 SCHEM 16.55409 1.837629 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 7.21226e-008 5.560538 -0.2376819 MPRFLG 0 ; 
       28 SCHEM 21.55409 1.837629 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 -6.3573e-008 -5.911491e-024 0 1.25173e-008 1.469767 -1.753178 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       7 SCHEM 25.29813 -6.423333 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.79813 -7.320166 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20.04239 -21.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34.70537 -4.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24.91557 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 44.81907 -6.000002 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.31907 -6.818038 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.10509 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.43673 -22.66277 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.4797 -21.81735 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24.94344 -22.63635 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24.90043 -21.92303 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.27282 -21.99462 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 43.56311 -1.320167 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 58.18054 -2.000001 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 75.47035 -2.252492 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 79.33405 -2.000003 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39.81907 -8.665483 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 37.31907 -9.483519 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 76.83405 -2.818039 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 104.334 -2.000003 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 101.834 -2.818039 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25.29813 -8.42333 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.79813 -9.320164 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 43.56311 -3.320165 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 58.18054 -4.000001 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34.70537 -6.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24.91557 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 44.81907 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 42.31907 -8.818038 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 75.47035 -4.252492 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 79.33405 -4.000003 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39.81907 -10.66548 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 37.31907 -11.48352 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 76.83405 -4.818039 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 104.334 -4.000003 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 101.834 -4.818039 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
