SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       STATIC-light1.1-0 ROOT ; 
       STATIC-light2.1-0 ROOT ; 
       STATIC-light3.1-0 ROOT ; 
       STATIC-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       STATIC-mat32.1-0 ; 
       STATIC-mat33.1-0 ; 
       STATIC-mat34.1-0 ; 
       STATIC-mat35.1-0 ; 
       STATIC-mat36.1-0 ; 
       STATIC-q.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       utl22-fuselg.2-0 ; 
       utl22-utl22.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl22/PICTURES/fig01_crgopods ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl22-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -2 0 WIRECOL 6 7 MPRFLG 0 ; 
       1 SCHEM 11.25 0 0 SRT 1.28 1.28 1.28 0 0 0 0 0 -0.01182033 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 21 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
