SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.41-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       drop_ship-default1.4-0 ; 
       drop_ship-default2.4-0 ; 
       drop_ship-mat1.4-0 ; 
       drop_ship-mat10.4-0 ; 
       drop_ship-mat102.4-0 ; 
       drop_ship-mat104.4-0 ; 
       drop_ship-mat105.4-0 ; 
       drop_ship-mat11.4-0 ; 
       drop_ship-mat12.4-0 ; 
       drop_ship-mat13.4-0 ; 
       drop_ship-mat14.4-0 ; 
       drop_ship-mat15.4-0 ; 
       drop_ship-mat2.5-0 ; 
       drop_ship-mat28.4-0 ; 
       drop_ship-mat29.4-0 ; 
       drop_ship-mat3.5-0 ; 
       drop_ship-mat30.4-0 ; 
       drop_ship-mat31.4-0 ; 
       drop_ship-mat4.5-0 ; 
       drop_ship-mat41.4-0 ; 
       drop_ship-mat42.4-0 ; 
       drop_ship-mat43.4-0 ; 
       drop_ship-mat44.4-0 ; 
       drop_ship-mat45.4-0 ; 
       drop_ship-mat46.4-0 ; 
       drop_ship-mat47.4-0 ; 
       drop_ship-mat49.4-0 ; 
       drop_ship-mat50.4-0 ; 
       drop_ship-mat51.4-0 ; 
       drop_ship-mat52.4-0 ; 
       drop_ship-mat53.4-0 ; 
       drop_ship-mat54.4-0 ; 
       drop_ship-mat55.4-0 ; 
       drop_ship-mat56.4-0 ; 
       drop_ship-mat7.5-0 ; 
       drop_ship-mat9.5-0 ; 
       static-back2.1-0 ; 
       static-back3.1-0 ; 
       static-back4.1-0 ; 
       static-back6.1-0 ; 
       static-back7.1-0 ; 
       static-back8.1-0 ; 
       static-back9.1-0 ; 
       static-botto1.1-0 ; 
       static-front2.1-0 ; 
       static-front3.1-0 ; 
       static-front5.1-0 ; 
       static-front6.1-0 ; 
       static-mat117.1-0 ; 
       static-mat118.1-0 ; 
       static-mat119.1-0 ; 
       static-mat120.1-0 ; 
       static-mat121.1-0 ; 
       static-mat122.1-0 ; 
       static-side1.1-0 ; 
       static-side2.1-0 ; 
       static-sides2.1-0 ; 
       static-sides3.1-0 ; 
       static-sides4.1-0 ; 
       static-top2.1-0 ; 
       static-top3.1-0 ; 
       static-top4.1-0 ; 
       static-top5.1-0 ; 
       static-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       root-bmerge1.1-0 ; 
       root-bmerge2.1-0 ; 
       root-fuselg1.4-0 ; 
       root-fuselg3.5-0 ; 
       root-fuselg3_1.1-0 ; 
       root-fuselg3_2.6-0 ; 
       root-lfinzzz.2-0 ; 
       root-lthrust1.1-0 ; 
       root-lthrust2.1-0 ; 
       root-lwingzz.1-0 ; 
       root-rfinzzz.1-0 ; 
       root-root.2-0 ROOT ; 
       root-rthrust1.1-0 ; 
       root-rthrust2.1-0 ; 
       root-rwingzz.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl27/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.9-0 ; 
       drop_ship-t2d10.6-0 ; 
       drop_ship-t2d11.6-0 ; 
       drop_ship-t2d12.6-0 ; 
       drop_ship-t2d2.9-0 ; 
       drop_ship-t2d24.6-0 ; 
       drop_ship-t2d37.6-0 ; 
       drop_ship-t2d38.6-0 ; 
       drop_ship-t2d39.6-0 ; 
       drop_ship-t2d40.6-0 ; 
       drop_ship-t2d41.6-0 ; 
       drop_ship-t2d42.6-0 ; 
       drop_ship-t2d43.6-0 ; 
       drop_ship-t2d44.6-0 ; 
       drop_ship-t2d45.6-0 ; 
       drop_ship-t2d46.6-0 ; 
       drop_ship-t2d47.6-0 ; 
       drop_ship-t2d48.6-0 ; 
       drop_ship-t2d49.6-0 ; 
       drop_ship-t2d5.9-0 ; 
       drop_ship-t2d50.6-0 ; 
       drop_ship-t2d7.9-0 ; 
       drop_ship-t2d8.6-0 ; 
       drop_ship-t2d9.6-0 ; 
       drop_ship-t2d93.6-0 ; 
       drop_ship-t2d95.6-0 ; 
       drop_ship-t2d96.6-0 ; 
       static-t2d112.1-0 ; 
       static-t2d113.1-0 ; 
       static-t2d114.1-0 ; 
       static-t2d115.1-0 ; 
       static-t2d116.1-0 ; 
       static-t2d117.1-0 ; 
       static-t2d118.1-0 ; 
       static-t2d119.1-0 ; 
       static-t2d120.1-0 ; 
       static-t2d121.1-0 ; 
       static-t2d122.1-0 ; 
       static-t2d123.1-0 ; 
       static-t2d124.1-0 ; 
       static-t2d125.1-0 ; 
       static-t2d129.1-0 ; 
       static-t2d130.1-0 ; 
       static-t2d131.1-0 ; 
       static-t2d132.1-0 ; 
       static-t2d133.1-0 ; 
       static-t2d134.1-0 ; 
       static-t2d135.1-0 ; 
       static-t2d136.1-0 ; 
       static-t2d137.1-0 ; 
       static-t2d138.1-0 ; 
       static-t2d139.1-0 ; 
       static-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       2 11 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 9 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 0 110 ; 
       10 14 110 ; 
       12 2 110 ; 
       13 12 110 ; 
       14 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 54 300 ; 
       0 60 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 55 300 ; 
       1 61 300 ; 
       2 12 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 50 300 ; 
       3 52 300 ; 
       3 62 300 ; 
       3 57 300 ; 
       3 39 300 ; 
       3 46 300 ; 
       3 40 300 ; 
       4 53 300 ; 
       4 63 300 ; 
       4 58 300 ; 
       4 41 300 ; 
       4 47 300 ; 
       4 42 300 ; 
       5 48 300 ; 
       5 59 300 ; 
       5 56 300 ; 
       5 36 300 ; 
       5 44 300 ; 
       5 37 300 ; 
       6 14 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 3 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       8 0 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       9 28 300 ; 
       10 17 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 6 300 ; 
       12 19 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 1 300 ; 
       13 22 300 ; 
       13 23 300 ; 
       13 24 300 ; 
       14 16 300 ; 
       14 26 300 ; 
       14 27 300 ; 
       11 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       54 34 401 ; 
       55 39 401 ; 
       56 29 401 ; 
       57 43 401 ; 
       58 49 401 ; 
       59 28 401 ; 
       60 35 401 ; 
       61 40 401 ; 
       62 42 401 ; 
       63 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 13.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 9.974319 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 3.75 -4.088701 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 5 -6.088701 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 2.5 -6.088701 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 15 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 15 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 12.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 9.974319 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.72432 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
