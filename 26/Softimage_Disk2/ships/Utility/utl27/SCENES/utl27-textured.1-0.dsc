SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl27-utl27.39-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.39-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       drop_ship-default1.4-0 ; 
       drop_ship-default2.4-0 ; 
       drop_ship-mat1.4-0 ; 
       drop_ship-mat10.4-0 ; 
       drop_ship-mat102.4-0 ; 
       drop_ship-mat104.4-0 ; 
       drop_ship-mat105.4-0 ; 
       drop_ship-mat11.4-0 ; 
       drop_ship-mat12.4-0 ; 
       drop_ship-mat13.4-0 ; 
       drop_ship-mat14.4-0 ; 
       drop_ship-mat15.4-0 ; 
       drop_ship-mat2.5-0 ; 
       drop_ship-mat28.4-0 ; 
       drop_ship-mat29.4-0 ; 
       drop_ship-mat3.5-0 ; 
       drop_ship-mat30.4-0 ; 
       drop_ship-mat31.4-0 ; 
       drop_ship-mat4.5-0 ; 
       drop_ship-mat41.4-0 ; 
       drop_ship-mat42.4-0 ; 
       drop_ship-mat43.4-0 ; 
       drop_ship-mat44.4-0 ; 
       drop_ship-mat45.4-0 ; 
       drop_ship-mat46.4-0 ; 
       drop_ship-mat47.4-0 ; 
       drop_ship-mat49.4-0 ; 
       drop_ship-mat50.4-0 ; 
       drop_ship-mat51.4-0 ; 
       drop_ship-mat52.4-0 ; 
       drop_ship-mat53.4-0 ; 
       drop_ship-mat54.4-0 ; 
       drop_ship-mat55.4-0 ; 
       drop_ship-mat56.4-0 ; 
       drop_ship-mat7.5-0 ; 
       drop_ship-mat9.5-0 ; 
       textured-back2.1-0 ; 
       textured-back3.1-0 ; 
       textured-back4.1-0 ; 
       textured-back6.1-0 ; 
       textured-back7.1-0 ; 
       textured-back8.1-0 ; 
       textured-back9.1-0 ; 
       textured-botto1.1-0 ; 
       textured-front2.1-0 ; 
       textured-front3.1-0 ; 
       textured-front5.1-0 ; 
       textured-front6.1-0 ; 
       textured-mat117.1-0 ; 
       textured-mat118.1-0 ; 
       textured-mat119.1-0 ; 
       textured-mat120.1-0 ; 
       textured-mat121.1-0 ; 
       textured-mat122.1-0 ; 
       textured-nose_white-center.1-1.1-0 ; 
       textured-nose_white-center.1-4.1-0 ; 
       textured-port_red-left.1-0.1-0 ; 
       textured-side1.1-0 ; 
       textured-side2.1-0 ; 
       textured-sides2.1-0 ; 
       textured-sides3.1-0 ; 
       textured-sides4.1-0 ; 
       textured-starbord_green-right.1-0.1-0 ; 
       textured-top2.1-0 ; 
       textured-top3.1-0 ; 
       textured-top4.1-0 ; 
       textured-top5.1-0 ; 
       textured-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       utl27-bmerge1.1-0 ; 
       utl27-bmerge2.1-0 ; 
       utl27-fuselg1.4-0 ; 
       utl27-fuselg3.5-0 ; 
       utl27-fuselg3_1.1-0 ; 
       utl27-fuselg3_2.6-0 ; 
       utl27-lfinzzz.2-0 ; 
       utl27-lthrust1.1-0 ; 
       utl27-lthrust2.1-0 ; 
       utl27-lwingzz.1-0 ; 
       utl27-rfinzzz.1-0 ; 
       utl27-rthrust1.1-0 ; 
       utl27-rthrust2.1-0 ; 
       utl27-rwingzz.1-0 ; 
       utl27-SSf.1-0 ; 
       utl27-SSl.1-0 ; 
       utl27-SSm.1-0 ; 
       utl27-SSr.1-0 ; 
       utl27-utl27.25-0 ROOT ; 
       utl27-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl27/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27-textured.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.8-0 ; 
       drop_ship-t2d10.5-0 ; 
       drop_ship-t2d11.5-0 ; 
       drop_ship-t2d12.5-0 ; 
       drop_ship-t2d2.8-0 ; 
       drop_ship-t2d24.5-0 ; 
       drop_ship-t2d37.5-0 ; 
       drop_ship-t2d38.5-0 ; 
       drop_ship-t2d39.5-0 ; 
       drop_ship-t2d40.5-0 ; 
       drop_ship-t2d41.5-0 ; 
       drop_ship-t2d42.5-0 ; 
       drop_ship-t2d43.5-0 ; 
       drop_ship-t2d44.5-0 ; 
       drop_ship-t2d45.5-0 ; 
       drop_ship-t2d46.5-0 ; 
       drop_ship-t2d47.5-0 ; 
       drop_ship-t2d48.5-0 ; 
       drop_ship-t2d49.5-0 ; 
       drop_ship-t2d5.8-0 ; 
       drop_ship-t2d50.5-0 ; 
       drop_ship-t2d7.8-0 ; 
       drop_ship-t2d8.5-0 ; 
       drop_ship-t2d9.5-0 ; 
       drop_ship-t2d93.5-0 ; 
       drop_ship-t2d95.5-0 ; 
       drop_ship-t2d96.5-0 ; 
       textured-t2d112.1-0 ; 
       textured-t2d113.1-0 ; 
       textured-t2d114.1-0 ; 
       textured-t2d115.1-0 ; 
       textured-t2d116.1-0 ; 
       textured-t2d117.1-0 ; 
       textured-t2d118.1-0 ; 
       textured-t2d119.1-0 ; 
       textured-t2d120.1-0 ; 
       textured-t2d121.1-0 ; 
       textured-t2d122.1-0 ; 
       textured-t2d123.1-0 ; 
       textured-t2d124.1-0 ; 
       textured-t2d125.1-0 ; 
       textured-t2d129.1-0 ; 
       textured-t2d130.1-0 ; 
       textured-t2d131.1-0 ; 
       textured-t2d132.1-0 ; 
       textured-t2d133.1-0 ; 
       textured-t2d134.1-0 ; 
       textured-t2d135.1-0 ; 
       textured-t2d136.1-0 ; 
       textured-t2d137.1-0 ; 
       textured-t2d138.1-0 ; 
       textured-t2d139.1-0 ; 
       textured-t2d140.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       0 1 110 ; 
       1 2 110 ; 
       2 18 110 ; 
       6 9 110 ; 
       7 2 110 ; 
       8 7 110 ; 
       9 0 110 ; 
       10 13 110 ; 
       11 2 110 ; 
       12 11 110 ; 
       13 0 110 ; 
       14 2 110 ; 
       15 6 110 ; 
       16 2 110 ; 
       17 10 110 ; 
       19 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 52 300 ; 
       3 66 300 ; 
       3 60 300 ; 
       3 39 300 ; 
       3 46 300 ; 
       3 40 300 ; 
       4 53 300 ; 
       4 67 300 ; 
       4 61 300 ; 
       4 41 300 ; 
       4 47 300 ; 
       4 42 300 ; 
       5 48 300 ; 
       5 63 300 ; 
       5 59 300 ; 
       5 36 300 ; 
       5 44 300 ; 
       5 37 300 ; 
       0 49 300 ; 
       0 57 300 ; 
       0 64 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 58 300 ; 
       1 65 300 ; 
       2 12 300 ; 
       2 15 300 ; 
       2 18 300 ; 
       2 34 300 ; 
       2 35 300 ; 
       2 50 300 ; 
       6 14 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       7 3 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       8 0 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       9 13 300 ; 
       9 25 300 ; 
       9 28 300 ; 
       10 17 300 ; 
       10 29 300 ; 
       10 30 300 ; 
       10 31 300 ; 
       10 6 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       12 1 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       13 16 300 ; 
       13 26 300 ; 
       13 27 300 ; 
       14 54 300 ; 
       15 56 300 ; 
       16 55 300 ; 
       17 62 300 ; 
       18 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       18 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       57 34 401 ; 
       58 39 401 ; 
       59 29 401 ; 
       60 43 401 ; 
       61 49 401 ; 
       63 28 401 ; 
       64 35 401 ; 
       65 40 401 ; 
       66 42 401 ; 
       67 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 8.775683 -4.088701 0 USR MPRFLG 0 ; 
       4 SCHEM 10.02568 -6.088699 0 MPRFLG 0 ; 
       5 SCHEM 7.525683 -6.088699 0 MPRFLG 0 ; 
       0 SCHEM 26.25 -6 0 MPRFLG 0 ; 
       1 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 25 -10 0 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 25 -8 0 MPRFLG 0 ; 
       14 SCHEM 17.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -12 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 20 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 25 -12 0 WIRECOL 2 7 MPRFLG 0 ; 
       18 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 29 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
