SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       root-root.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       drop_ship-cam_int1.40-0 ROOT ; 
       drop_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       drop_ship-default1.4-0 ; 
       drop_ship-default2.4-0 ; 
       drop_ship-mat1.4-0 ; 
       drop_ship-mat10.4-0 ; 
       drop_ship-mat102.4-0 ; 
       drop_ship-mat104.4-0 ; 
       drop_ship-mat105.4-0 ; 
       drop_ship-mat11.4-0 ; 
       drop_ship-mat12.4-0 ; 
       drop_ship-mat13.4-0 ; 
       drop_ship-mat14.4-0 ; 
       drop_ship-mat15.4-0 ; 
       drop_ship-mat2.5-0 ; 
       drop_ship-mat28.4-0 ; 
       drop_ship-mat29.4-0 ; 
       drop_ship-mat3.5-0 ; 
       drop_ship-mat30.4-0 ; 
       drop_ship-mat31.4-0 ; 
       drop_ship-mat4.5-0 ; 
       drop_ship-mat41.4-0 ; 
       drop_ship-mat42.4-0 ; 
       drop_ship-mat43.4-0 ; 
       drop_ship-mat44.4-0 ; 
       drop_ship-mat45.4-0 ; 
       drop_ship-mat46.4-0 ; 
       drop_ship-mat47.4-0 ; 
       drop_ship-mat49.4-0 ; 
       drop_ship-mat50.4-0 ; 
       drop_ship-mat51.4-0 ; 
       drop_ship-mat52.4-0 ; 
       drop_ship-mat53.4-0 ; 
       drop_ship-mat54.4-0 ; 
       drop_ship-mat55.4-0 ; 
       drop_ship-mat56.4-0 ; 
       drop_ship-mat7.5-0 ; 
       drop_ship-mat9.5-0 ; 
       textured-back2.1-0 ; 
       textured-back3.1-0 ; 
       textured-back4.1-0 ; 
       textured-back6.1-0 ; 
       textured-back7.1-0 ; 
       textured-back8.1-0 ; 
       textured-back9.1-0 ; 
       textured-botto1.1-0 ; 
       textured-front2.1-0 ; 
       textured-front3.1-0 ; 
       textured-front5.1-0 ; 
       textured-front6.1-0 ; 
       textured-mat117.1-0 ; 
       textured-mat118.1-0 ; 
       textured-mat119.1-0 ; 
       textured-mat120.1-0 ; 
       textured-mat121.1-0 ; 
       textured-mat122.1-0 ; 
       textured-nose_white-center.1-1.1-0 ; 
       textured-nose_white-center.1-4.1-0 ; 
       textured-port_red-left.1-0.1-0 ; 
       textured-side1.1-0 ; 
       textured-side2.1-0 ; 
       textured-sides2.1-0 ; 
       textured-sides3.1-0 ; 
       textured-sides4.1-0 ; 
       textured-starbord_green-right.1-0.1-0 ; 
       textured-top2.1-0 ; 
       textured-top3.1-0 ; 
       textured-top4.1-0 ; 
       textured-top5.1-0 ; 
       textured-top6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       root-bmerge1.1-0 ; 
       root-bmerge2.1-0 ; 
       root-cockpt.1-0 ; 
       root-fuselg1.4-0 ; 
       root-fuselg3.5-0 ; 
       root-fuselg3_1.1-0 ; 
       root-fuselg3_2.6-0 ; 
       root-lfinzzz.2-0 ; 
       root-lsmoke.1-0 ; 
       root-lthrust.1-0 ; 
       root-lthrust1.1-0 ; 
       root-lthrust2.1-0 ; 
       root-lwingzz.1-0 ; 
       root-rfinzzz.1-0 ; 
       root-root.1-0 ROOT ; 
       root-rsmoke.1-0 ; 
       root-rthrust.1-0 ; 
       root-rthrust1.1-0 ; 
       root-rthrust2.1-0 ; 
       root-rwingzz.1-0 ; 
       root-SSf.1-0 ; 
       root-SSl.1-0 ; 
       root-SSm.1-0 ; 
       root-SSr.1-0 ; 
       root-trail.1-0 ; 
       root-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl27/PICTURES/utl27 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl27-textured.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 53     
       drop_ship-t2d1.9-0 ; 
       drop_ship-t2d10.6-0 ; 
       drop_ship-t2d11.6-0 ; 
       drop_ship-t2d12.6-0 ; 
       drop_ship-t2d2.9-0 ; 
       drop_ship-t2d24.6-0 ; 
       drop_ship-t2d37.6-0 ; 
       drop_ship-t2d38.6-0 ; 
       drop_ship-t2d39.6-0 ; 
       drop_ship-t2d40.6-0 ; 
       drop_ship-t2d41.6-0 ; 
       drop_ship-t2d42.6-0 ; 
       drop_ship-t2d43.6-0 ; 
       drop_ship-t2d44.6-0 ; 
       drop_ship-t2d45.6-0 ; 
       drop_ship-t2d46.6-0 ; 
       drop_ship-t2d47.6-0 ; 
       drop_ship-t2d48.6-0 ; 
       drop_ship-t2d49.6-0 ; 
       drop_ship-t2d5.9-0 ; 
       drop_ship-t2d50.6-0 ; 
       drop_ship-t2d7.9-0 ; 
       drop_ship-t2d8.6-0 ; 
       drop_ship-t2d9.6-0 ; 
       drop_ship-t2d93.6-0 ; 
       drop_ship-t2d95.6-0 ; 
       drop_ship-t2d96.6-0 ; 
       textured-t2d112.2-0 ; 
       textured-t2d113.2-0 ; 
       textured-t2d114.2-0 ; 
       textured-t2d115.2-0 ; 
       textured-t2d116.2-0 ; 
       textured-t2d117.2-0 ; 
       textured-t2d118.2-0 ; 
       textured-t2d119.2-0 ; 
       textured-t2d120.2-0 ; 
       textured-t2d121.2-0 ; 
       textured-t2d122.2-0 ; 
       textured-t2d123.2-0 ; 
       textured-t2d124.2-0 ; 
       textured-t2d125.2-0 ; 
       textured-t2d129.2-0 ; 
       textured-t2d130.2-0 ; 
       textured-t2d131.2-0 ; 
       textured-t2d132.2-0 ; 
       textured-t2d133.2-0 ; 
       textured-t2d134.2-0 ; 
       textured-t2d135.2-0 ; 
       textured-t2d136.2-0 ; 
       textured-t2d137.2-0 ; 
       textured-t2d138.2-0 ; 
       textured-t2d139.2-0 ; 
       textured-t2d140.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 3 110 ; 
       0 1 110 ; 
       1 3 110 ; 
       3 14 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 12 110 ; 
       10 3 110 ; 
       11 10 110 ; 
       12 0 110 ; 
       13 19 110 ; 
       17 3 110 ; 
       18 17 110 ; 
       19 0 110 ; 
       20 3 110 ; 
       21 7 110 ; 
       22 3 110 ; 
       23 13 110 ; 
       25 3 110 ; 
       15 3 110 ; 
       9 3 110 ; 
       24 3 110 ; 
       8 3 110 ; 
       8 9 111 ; 
       16 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 49 300 ; 
       0 57 300 ; 
       0 64 300 ; 
       0 45 300 ; 
       0 38 300 ; 
       0 43 300 ; 
       1 51 300 ; 
       1 58 300 ; 
       1 65 300 ; 
       3 12 300 ; 
       3 15 300 ; 
       3 18 300 ; 
       3 34 300 ; 
       3 35 300 ; 
       3 50 300 ; 
       4 52 300 ; 
       4 66 300 ; 
       4 60 300 ; 
       4 39 300 ; 
       4 46 300 ; 
       4 40 300 ; 
       5 53 300 ; 
       5 67 300 ; 
       5 61 300 ; 
       5 41 300 ; 
       5 47 300 ; 
       5 42 300 ; 
       6 48 300 ; 
       6 63 300 ; 
       6 59 300 ; 
       6 36 300 ; 
       6 44 300 ; 
       6 37 300 ; 
       7 14 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       10 3 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       11 0 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       12 13 300 ; 
       12 25 300 ; 
       12 28 300 ; 
       13 17 300 ; 
       13 29 300 ; 
       13 30 300 ; 
       13 31 300 ; 
       13 6 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 21 300 ; 
       18 1 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       19 16 300 ; 
       19 26 300 ; 
       19 27 300 ; 
       20 54 300 ; 
       21 56 300 ; 
       22 55 300 ; 
       23 62 300 ; 
       14 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       14 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 24 401 ; 
       5 25 401 ; 
       6 26 401 ; 
       7 22 401 ; 
       8 23 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       13 5 401 ; 
       15 0 401 ; 
       18 4 401 ; 
       20 6 401 ; 
       21 7 401 ; 
       22 8 401 ; 
       23 9 401 ; 
       24 10 401 ; 
       25 13 401 ; 
       26 11 401 ; 
       27 12 401 ; 
       28 14 401 ; 
       29 16 401 ; 
       30 15 401 ; 
       31 17 401 ; 
       32 18 401 ; 
       33 20 401 ; 
       34 19 401 ; 
       35 21 401 ; 
       36 30 401 ; 
       37 32 401 ; 
       38 37 401 ; 
       39 44 401 ; 
       40 46 401 ; 
       41 50 401 ; 
       42 52 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 36 401 ; 
       46 45 401 ; 
       47 51 401 ; 
       48 27 401 ; 
       50 38 401 ; 
       52 41 401 ; 
       53 47 401 ; 
       57 34 401 ; 
       58 39 401 ; 
       59 29 401 ; 
       60 43 401 ; 
       61 49 401 ; 
       63 28 401 ; 
       64 35 401 ; 
       65 40 401 ; 
       66 42 401 ; 
       67 48 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 14.92021 -7.696743 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 21.25 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 21.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 9.974319 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 3.75 -4.088701 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 5 -6.088701 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 2.5 -6.088701 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 22.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 7.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 22.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 12.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 22.5 -12 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 15 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 9.974319 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 17.71936 -9.61284 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 12.75106 -8.95653 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 14.90473 -6.60393 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 12.56029 -9.782995 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 17.48367 -8.95653 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.47432 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 1.5 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 6.5 -6.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 4 -8.088701 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -8.088701 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 4 -10.0887 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 21.47432 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
