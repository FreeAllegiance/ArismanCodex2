SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.100-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.100-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.100-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.70-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       utl90-cube1.1-0 ; 
       utl90-face2.1-0 ; 
       utl90-face3.1-0 ; 
       utl90-ftfuselg.1-0 ; 
       utl90-fuselg.1-0 ; 
       utl90-fuselg3.1-0 ; 
       utl90-missemt.1-0 ; 
       utl90-poly_arm.1-0 ; 
       utl90-poly_arm1.1-0 ; 
       utl90-smoke.1-0 ; 
       utl90-ss01.1-0 ; 
       utl90-ss02.1-0 ; 
       utl90-ss03.1-0 ; 
       utl90-ss04.1-0 ; 
       utl90-thrust.1-0 ; 
       utl90-top_fin.1-0 ; 
       utl90-top_fin2.1-0 ; 
       utl90-trail.1-0 ; 
       utl90-utl90.5-0 ROOT ; 
       utl90-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-SCALE.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 7 110 ; 
       2 8 110 ; 
       3 4 110 ; 
       4 18 110 ; 
       5 0 110 ; 
       6 18 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 18 110 ; 
       10 18 110 ; 
       11 18 110 ; 
       12 18 110 ; 
       13 18 110 ; 
       14 18 110 ; 
       15 5 110 ; 
       16 5 110 ; 
       17 18 110 ; 
       19 18 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       11 SCHEM 30 -2 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 32.5 -2 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       13 SCHEM 35 -2 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 15 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 10 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       16 SCHEM 12.5 -8 0 DISPLAY 1 2 MPRFLG 0 ; 
       17 SCHEM 20 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       18 SCHEM 18.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0.1512331 -3.144287 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 731 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 731 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 731 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
