SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       add_arms-circle1.1-0 ROOT ; 
       add_arms-circle2.1-0 ROOT ; 
       add_arms-circle3.1-0 ROOT ; 
       add_arms-circle4.1-0 ROOT ; 
       add_arms-circle5.1-0 ROOT ; 
       add_arms-circle6.1-0 ROOT ; 
       add_arms-circle7.1-0 ROOT ; 
       add_arms-spline2.1-0 ROOT ; 
       add_arms-spline3.1-0 ROOT ; 
       add_arms-top_fin1.1-0 ROOT ; 
       fig03-fig03_1.4-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-add_arms.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       reduce_wing-t2d14.2-0 ; 
       reduce_wing-t2d33.2-0 ; 
       reduce_wing-t2d8.2-0 ; 
       reduce_wing-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 12 110 ; 
       12 10 110 ; 
       13 12 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 0 300 ; 
       12 2 300 ; 
       12 3 300 ; 
       12 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 6.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 6.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 -3.72529e-009 -1.192005 MPRFLG 0 ; 
       0 SCHEM 15 0 0 SRT 1.004 1 1 0 0 0 0 -0.1383425 1.388816 MPRFLG 0 ; 
       7 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 SRT 0.57267 0.6677421 0.559248 0 0 0 0 -0.1259647 1.877737 MPRFLG 0 ; 
       2 SCHEM 20 0 0 SRT 0.3077179 0.41238 0.41238 0 0 0 0 -0.1166814 2.357376 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 SRT 0.24 0.5606455 1 0 0 0 0 -0.1187719 2.44402 MPRFLG 0 ; 
       4 SCHEM 25 0 0 SRT 0.3775799 0.9273677 1 0 0.268 0 0.03559755 -0.1155948 2.585457 MPRFLG 0 ; 
       5 SCHEM 27.5 0 0 SRT 0.378 0.5837799 1 0 -0.09200001 0 0.05729881 -0.1330748 2.770958 MPRFLG 0 ; 
       6 SCHEM 30 0 0 SRT 0.1239999 0.178 1 0 -0.6040001 0 -0.008356428 -0.1341614 2.889976 MPRFLG 0 ; 
       8 SCHEM 32.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
