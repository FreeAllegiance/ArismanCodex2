SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.10-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.10-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       add_arms-circle1.1-0 ; 
       add_arms-circle2.1-0 ; 
       add_arms-circle3.1-0 ; 
       add_arms-circle4.1-0 ; 
       add_arms-circle5.1-0 ; 
       add_arms-circle6.1-0 ; 
       add_arms-circle7.1-0 ; 
       add_arms-null1.1-0 ROOT ; 
       add_arms-nurbs_arm.2-0 ROOT ; 
       add_arms-spline2.1-0 ; 
       add_arms-spline3.2-0 ROOT ; 
       fig03-fig03_1.4-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-add_arms.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       reduce_wing-t2d14.2-0 ; 
       reduce_wing-t2d33.2-0 ; 
       reduce_wing-t2d8.2-0 ; 
       reduce_wing-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 13 110 ; 
       13 11 110 ; 
       14 13 110 ; 
       0 7 110 ; 
       9 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       12 0 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       13 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 30 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0.3276896 0 0.3901067 MPRFLG 0 ; 
       7 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 9 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
