SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.69-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.69-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.69-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.39-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       fig03-face2.1-0 ; 
       fig03-face3.1-0 ; 
       fig03-fig03_1.47-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-poly_arm.1-0 ; 
       fig03-poly_arm1.1-0 ; 
       fig03-smoke.1-0 ; 
       fig03-ss1.1-0 ; 
       fig03-ss2.1-0 ; 
       fig03-ss3.1-0 ; 
       fig03-ss4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin2.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-add_frames.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       13 2 110 ; 
       8 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       5 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       0 6 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       1 7 110 ; 
       3 4 110 ; 
       4 2 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       13 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       0 SCHEM 10 -10 0 MPRFLG 0 ; 
       11 SCHEM 45 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       1 SCHEM 5 -10 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -4 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       3 SCHEM 0 -8 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 193.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 193.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 193.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
