SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.85-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.85-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.85-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.55-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       fig03-cube1.1-0 ; 
       fig03-face2.1-0 ; 
       fig03-face3.1-0 ; 
       fig03-fig03_1.60-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-fuselg3.1-0 ; 
       fig03-missemt.1-0 ; 
       fig03-poly_arm.1-0 ; 
       fig03-poly_arm1.1-0 ; 
       fig03-smoke.1-0 ; 
       fig03-ss1.1-0 ; 
       fig03-ss2.1-0 ; 
       fig03-ss3.1-0 ; 
       fig03-ss4.1-0 ; 
       fig03-thrust.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin2.1-0 ; 
       fig03-trail.1-0 ; 
       fig03-wepemt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-create_miner.14-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 8 110 ; 
       2 9 110 ; 
       4 5 110 ; 
       5 3 110 ; 
       7 3 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 6 110 ; 
       17 6 110 ; 
       18 3 110 ; 
       19 3 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -4 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 MPRFLG 0 ; 
       5 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       9 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 60 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 65 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -12 0 MPRFLG 0 ; 
       17 SCHEM 15 -12 0 MPRFLG 0 ; 
       18 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 18.75 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 256 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 256 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 256 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
