SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.46-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.46-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.46-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.17-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig03-deformedn_nurbs_arm.1-0 ; 
       fig03-deformedn_nurbs_arm1.1-0 ; 
       fig03-face2.1-0 ; 
       fig03-face3.1-0 ; 
       fig03-fig03_1.30-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-paint_texture.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       3 1 110 ; 
       5 6 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -6.975543 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -6.975543 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -8.975542 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -8.975542 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       5 SCHEM 0 -6.975543 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4.975543 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 2.5 -6.975543 0 MPRFLG 0 ; 
       8 SCHEM 5 -6.975543 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 101 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 101 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 101 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
