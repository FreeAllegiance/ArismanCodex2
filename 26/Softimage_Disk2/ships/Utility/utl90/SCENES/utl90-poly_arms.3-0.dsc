SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.14-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.14-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       fig03-deformedn_nurbs_arm.1-0 ; 
       fig03-deformedn_nurbs_arm1.1-0 ; 
       fig03-fig03_1.7-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-poly_arms.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       reduce_wing-t2d14.2-0 ; 
       reduce_wing-t2d33.2-0 ; 
       reduce_wing-t2d8.2-0 ; 
       reduce_wing-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 4 110 ; 
       4 2 110 ; 
       5 4 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       6 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
