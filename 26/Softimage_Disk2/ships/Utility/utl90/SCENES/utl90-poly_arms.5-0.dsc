SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.16-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.16-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       poly_arms-mat34.1-0 ; 
       poly_arms-mat35.1-0 ; 
       poly_arms-mat36.1-0 ; 
       reduce_wing-mat14.1-0 ; 
       reduce_wing-mat33.1-0 ; 
       reduce_wing-mat8.1-0 ; 
       reduce_wing-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig03-deformedn_nurbs_arm.1-0 ; 
       fig03-deformedn_nurbs_arm1.1-0 ; 
       fig03-face2.1-0 ; 
       fig03-fig03_1.8-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin1.1-0 ; 
       poly_arms-face4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-poly_arms.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       reduce_wing-t2d14.2-0 ; 
       reduce_wing-t2d33.2-0 ; 
       reduce_wing-t2d8.2-0 ; 
       reduce_wing-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 5 110 ; 
       5 3 110 ; 
       6 5 110 ; 
       2 0 110 ; 
       0 5 110 ; 
       1 5 110 ; 
       7 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 3 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 4 300 ; 
       2 0 300 ; 
       8 2 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 5 -4 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       4 SCHEM 0 -8 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 12.5 0 0 SRT 1 0.9999999 1 0.04443791 -2.832134 -0.06303427 -0.5637786 -0.2494468 3.064035 MPRFLG 0 ; 
       0 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
