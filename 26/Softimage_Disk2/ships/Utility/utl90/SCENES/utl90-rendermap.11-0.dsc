SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.29-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.29-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.29-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.29-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.29-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       rendermap-spot1.1-0 ; 
       rendermap-spot1_int1.9-0 ROOT ; 
       rendermap-spot2.1-0 ; 
       rendermap-spot2_int.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       reduce_wing-OTHERS-rubber.1-0.4-0 ; 
       reduce_wing-OTHERS-rubber.1-3.2-0 ; 
       rendermap-mat34.2-0 ; 
       rendermap-mat36.2-0 ; 
       rendermap-mat37.2-0 ; 
       rendermap-mat38.1-0 ; 
       rendermap-OTHERS-rubber.1-1.2-0 ; 
       rendermap-OTHERS-rubber.1-2.2-0 ; 
       rendermap-OTHERS-rubber.1-4.2-0 ; 
       rendermap-OTHERS-rubber.1-5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 19     
       fig03-deformedn_nurbs_arm.1-0 ; 
       fig03-deformedn_nurbs_arm1.1-0 ; 
       fig03-face2.1-0 ; 
       fig03-face3.1-0 ; 
       fig03-fig03_1.20-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin1.1-0 ; 
       rendermap-circle1.1-0 ; 
       rendermap-circle2.1-0 ; 
       rendermap-circle4.1-0 ; 
       rendermap-circle5.1-0 ; 
       rendermap-circle6.1-0 ; 
       rendermap-circle7.1-0 ; 
       rendermap-circle8.1-0 ; 
       rendermap-circle9.1-0 ; 
       rendermap-null1.3-0 ROOT ; 
       rendermap-skin1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/rendermap ; 
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/spines ; 
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/top ; 
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-rendermap.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       reduce_wing-t2d14.4-0 ; 
       rendermap-t2d15.5-0 ; 
       rendermap-t2d16.4-0 ; 
       rendermap-t2d17.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 0 110 ; 
       3 1 110 ; 
       5 6 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 17 110 ; 
       16 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       1 6 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       5 1 300 ; 
       6 0 300 ; 
       6 5 300 ; 
       7 8 300 ; 
       8 9 300 ; 
       18 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 2 400 ; 
       18 1 400 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       5 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 -10.97554 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 -8.975542 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -10.97554 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -8.975542 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 13.75 -2.975543 0 MPRFLG 0 ; 
       1 SCHEM 18.75 -2.975543 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4.975543 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -4.975543 0 MPRFLG 0 ; 
       4 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       5 SCHEM 5 -2.975543 0 MPRFLG 0 ; 
       6 SCHEM 16.25 -0.9755425 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 7.5 -2.975543 0 MPRFLG 0 ; 
       8 SCHEM 10 -2.975543 0 MPRFLG 0 ; 
       9 SCHEM 10 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 5 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 15 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 0 -10.97554 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 8.75 -8.975542 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 21.25 -8.975542 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -4.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -6.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -10.97554 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -4.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -4.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -4.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -2.975543 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -6.975543 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -10.97554 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -2.975543 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4.975543 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 56 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
