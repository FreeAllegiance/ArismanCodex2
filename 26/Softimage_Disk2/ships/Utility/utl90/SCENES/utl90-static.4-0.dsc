SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.98-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.98-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.98-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.68-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       utl90-cube1.1-0 ; 
       utl90-face2.1-0 ; 
       utl90-face3.1-0 ; 
       utl90-ftfuselg.1-0 ; 
       utl90-fuselg.1-0 ; 
       utl90-fuselg3.1-0 ; 
       utl90-poly_arm.1-0 ; 
       utl90-poly_arm1.1-0 ; 
       utl90-top_fin.1-0 ; 
       utl90-top_fin2.1-0 ; 
       utl90-utl90.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-static.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 6 110 ; 
       2 7 110 ; 
       3 4 110 ; 
       4 10 110 ; 
       5 0 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 5 110 ; 
       9 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 5 -4 0 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0.1512331 -3.050295 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 636 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 636 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 636 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
