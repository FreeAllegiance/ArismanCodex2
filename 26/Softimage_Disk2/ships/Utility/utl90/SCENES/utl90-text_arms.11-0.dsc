SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.60-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.60-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.60-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.30-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       fig03-face2.1-0 ; 
       fig03-fig03_1.42-0 ROOT ; 
       fig03-ftfuselg.1-0 ; 
       fig03-fuselg.1-0 ; 
       fig03-poly_arm.1-0 ; 
       fig03-top_fin.1-0 ; 
       fig03-top_fin2.1-0 ; 
       text_arms-face3.1-0 ; 
       text_arms-poly_arm1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl90/PICTURES/utl90 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl90-text_arms.11-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       2 3 110 ; 
       3 1 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       7 8 110 ; 
       6 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 3.75 0 0 SRT 1 1 1 0 3.141593 0 -0.008041859 -0.0370917 0.1713636 MPRFLG 0 ; 
       0 SCHEM 15 -4.975543 0 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 -0.3303924 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2.975543 0 MPRFLG 0 ; 
       3 SCHEM 17.5 -0.975543 0 USR MPRFLG 0 ; 
       4 SCHEM 16.25 -2.975543 0 MPRFLG 0 ; 
       5 SCHEM 10 -2.975543 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 30 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 141 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 141 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 141 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
