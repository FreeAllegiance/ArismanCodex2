SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.8-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       CW_Miner-back1.1-0 ; 
       CW_Miner-base.1-0 ; 
       CW_Miner-base1.1-0 ; 
       CW_Miner-base2.1-0 ; 
       CW_Miner-bottom.1-0 ; 
       CW_Miner-caution1.1-0 ; 
       CW_Miner-caution2.1-0 ; 
       CW_Miner-front1.1-0 ; 
       CW_Miner-guns1.1-0 ; 
       CW_Miner-guns2.1-0 ; 
       CW_Miner-nose_white-center.1-0.1-0 ; 
       CW_Miner-nose_white-center.1-1.1-0 ; 
       CW_Miner-port_red-left.1-0.1-0 ; 
       CW_Miner-sides_and_bottom.1-0 ; 
       CW_Miner-starbord_green-right.1-0.1-0 ; 
       CW_Miner-top.1-0 ; 
       CW_Miner-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       fig08-cockpt.1-0 ; 
       fig08-fig08_3.8-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-smoke.1-0 ; 
       fig08-SSa.1-0 ; 
       fig08-SSal.1-0 ; 
       fig08-SSar.1-0 ; 
       fig08-SSf.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl91/PICTURES/utl91 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl91-CW_Miner.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       CW_Miner-t2d44.2-0 ; 
       CW_Miner-t2d45.2-0 ; 
       CW_Miner-t2d46.2-0 ; 
       CW_Miner-t2d47.2-0 ; 
       CW_Miner-t2d48.2-0 ; 
       CW_Miner-t2d49.2-0 ; 
       CW_Miner-t2d50.2-0 ; 
       CW_Miner-t2d51.2-0 ; 
       CW_Miner-t2d52.2-0 ; 
       CW_Miner-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       6 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 4 300 ; 
       2 15 300 ; 
       2 1 300 ; 
       2 7 300 ; 
       2 13 300 ; 
       2 0 300 ; 
       2 16 300 ; 
       3 3 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       5 2 300 ; 
       5 5 300 ; 
       5 8 300 ; 
       7 11 300 ; 
       8 12 300 ; 
       9 14 300 ; 
       10 10 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       13 0 401 ; 
       15 9 401 ; 
       16 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       2 SCHEM 23.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 37.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
