SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.55-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       CW_Miner_text-light1.43-0 ROOT ; 
       CW_Miner_text-light2.43-0 ROOT ; 
       CW_Miner_text-light3.43-0 ROOT ; 
       CW_Miner_text-light4.43-0 ROOT ; 
       CW_Miner_text-light5.43-0 ROOT ; 
       CW_Miner_text-light6.43-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 27     
       CW_Miner_text-back1.11-0 ; 
       CW_Miner_text-base.11-0 ; 
       CW_Miner_text-base1.3-0 ; 
       CW_Miner_text-base2.3-0 ; 
       CW_Miner_text-bottom.11-0 ; 
       CW_Miner_text-caution1.3-0 ; 
       CW_Miner_text-caution2.3-0 ; 
       CW_Miner_text-front1.11-0 ; 
       CW_Miner_text-guns1.3-0 ; 
       CW_Miner_text-guns2.3-0 ; 
       CW_Miner_text-mat10.7-0 ; 
       CW_Miner_text-mat12.6-0 ; 
       CW_Miner_text-mat14.6-0 ; 
       CW_Miner_text-mat16.2-0 ; 
       CW_Miner_text-mat17.1-0 ; 
       CW_Miner_text-mat6.8-0 ; 
       CW_Miner_text-mat7.8-0 ; 
       CW_Miner_text-mat8.8-0 ; 
       CW_Miner_text-mat9.7-0 ; 
       CW_Miner_text-nose_white-center.1-0.3-0 ; 
       CW_Miner_text-nose_white-center.1-1.3-0 ; 
       CW_Miner_text-pods1.1-0 ; 
       CW_Miner_text-port_red-left.1-0.3-0 ; 
       CW_Miner_text-sides_and_bottom.11-0 ; 
       CW_Miner_text-starbord_green-right.1-0.3-0 ; 
       CW_Miner_text-top.11-0 ; 
       CW_Miner_text-vents1.11-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       fig08-cockpt.1-0 ; 
       fig08-cube1.1-0 ; 
       fig08-fig08_3.48-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-missemt.1-0 ; 
       fig08-rwepbar.2-0 ; 
       fig08-smoke.1-0 ; 
       fig08-SS1.1-0 ; 
       fig08-SS2.1-0 ; 
       fig08-SS3.1-0 ; 
       fig08-SS4.1-0 ; 
       fig08-thrust.1-0 ; 
       fig08-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl91/PICTURES/utl91 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl91-CW_Miner_text.44-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       CW_Miner_text-t2d44.28-0 ; 
       CW_Miner_text-t2d45.28-0 ; 
       CW_Miner_text-t2d46.28-0 ; 
       CW_Miner_text-t2d47.28-0 ; 
       CW_Miner_text-t2d48.28-0 ; 
       CW_Miner_text-t2d49.3-0 ; 
       CW_Miner_text-t2d50.3-0 ; 
       CW_Miner_text-t2d51.3-0 ; 
       CW_Miner_text-t2d52.3-0 ; 
       CW_Miner_text-t2d53.26-0 ; 
       CW_Miner_text-t2d54.26-0 ; 
       CW_Miner_text-t2d55.26-0 ; 
       CW_Miner_text-t2d56.26-0 ; 
       CW_Miner_text-t2d57.25-0 ; 
       CW_Miner_text-t2d59.25-0 ; 
       CW_Miner_text-t2d6.28-0 ; 
       CW_Miner_text-t2d61.24-0 ; 
       CW_Miner_text-t2d65.13-0 ; 
       CW_Miner_text-t2d66.3-0 ; 
       CW_Miner_text-t2d67.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 3 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
       7 2 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 2 110 ; 
       13 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 14 300 ; 
       3 4 300 ; 
       3 25 300 ; 
       3 1 300 ; 
       3 7 300 ; 
       3 23 300 ; 
       3 0 300 ; 
       3 26 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 10 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 21 300 ; 
       4 3 300 ; 
       4 6 300 ; 
       4 9 300 ; 
       6 2 300 ; 
       6 5 300 ; 
       6 8 300 ; 
       8 20 300 ; 
       9 22 300 ; 
       10 24 300 ; 
       11 19 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 14 401 ; 
       12 13 401 ; 
       13 17 401 ; 
       14 18 401 ; 
       15 16 401 ; 
       16 12 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       21 19 401 ; 
       23 0 401 ; 
       25 15 401 ; 
       26 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       3 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 WIRECOL 3 7 DISPLAY 1 2 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
