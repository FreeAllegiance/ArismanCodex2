SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.51-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       static-back1.1-0 ; 
       static-base.1-0 ; 
       static-base1.1-0 ; 
       static-base2.1-0 ; 
       static-bottom.1-0 ; 
       static-caution1.1-0 ; 
       static-caution2.1-0 ; 
       static-front1.1-0 ; 
       static-guns1.1-0 ; 
       static-guns2.1-0 ; 
       static-mat10.1-0 ; 
       static-mat12.1-0 ; 
       static-mat14.1-0 ; 
       static-mat16.1-0 ; 
       static-mat17.1-0 ; 
       static-mat6.1-0 ; 
       static-mat7.1-0 ; 
       static-mat8.1-0 ; 
       static-mat9.1-0 ; 
       static-pods1.1-0 ; 
       static-sides_and_bottom.1-0 ; 
       static-top.1-0 ; 
       static-vents1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       fig08-cube1.1-0 ; 
       fig08-fig08_3.45-0 ROOT ; 
       fig08-fuselg.1-0 ; 
       fig08-lwepbar.1-0 ; 
       fig08-rwepbar.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl91/PICTURES/utl91 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl91-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       static-t2d44.1-0 ; 
       static-t2d45.1-0 ; 
       static-t2d46.1-0 ; 
       static-t2d47.1-0 ; 
       static-t2d48.1-0 ; 
       static-t2d49.1-0 ; 
       static-t2d50.1-0 ; 
       static-t2d51.1-0 ; 
       static-t2d52.1-0 ; 
       static-t2d53.1-0 ; 
       static-t2d54.1-0 ; 
       static-t2d55.1-0 ; 
       static-t2d56.1-0 ; 
       static-t2d57.1-0 ; 
       static-t2d59.1-0 ; 
       static-t2d6.1-0 ; 
       static-t2d61.1-0 ; 
       static-t2d65.1-0 ; 
       static-t2d66.1-0 ; 
       static-t2d67.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       2 4 300 ; 
       2 21 300 ; 
       2 1 300 ; 
       2 7 300 ; 
       2 20 300 ; 
       2 0 300 ; 
       2 22 300 ; 
       2 15 300 ; 
       2 16 300 ; 
       2 17 300 ; 
       2 18 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 19 300 ; 
       3 3 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       4 2 300 ; 
       4 5 300 ; 
       4 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       4 3 401 ; 
       5 5 401 ; 
       6 7 401 ; 
       7 1 401 ; 
       8 6 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 14 401 ; 
       12 13 401 ; 
       13 17 401 ; 
       14 18 401 ; 
       15 16 401 ; 
       16 12 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 19 401 ; 
       20 0 401 ; 
       21 15 401 ; 
       22 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 -0.2177 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
