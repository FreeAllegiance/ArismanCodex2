SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.25-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       add_smoke-mat1.1-0 ; 
       add_smoke-mat10.1-0 ; 
       add_smoke-mat2.1-0 ; 
       add_smoke-mat3.1-0 ; 
       add_smoke-mat4.1-0 ; 
       add_smoke-mat46.1-0 ; 
       add_smoke-mat5.1-0 ; 
       add_smoke-mat57.1-0 ; 
       add_smoke-mat58.1-0 ; 
       add_smoke-mat6.1-0 ; 
       add_smoke-mat63.1-0 ; 
       add_smoke-mat64.1-0 ; 
       add_smoke-mat66.1-0 ; 
       add_smoke-mat7.1-0 ; 
       add_smoke-mat8.1-0 ; 
       add_smoke-mat9.1-0 ; 
       Center-mat83.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       bom04-afinzzz.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.20-0 ROOT ; 
       bom04-fuselg1.2-0 ; 
       bom04-sphere2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/Utility/utl92/PICTURES/utl92 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl92-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       add_smoke-t2d1.4-0 ; 
       add_smoke-t2d2.4-0 ; 
       add_smoke-t2d4.5-0 ; 
       add_smoke-t2d45.5-0 ; 
       add_smoke-t2d5.5-0 ; 
       add_smoke-t2d57.4-0 ; 
       add_smoke-t2d58.4-0 ; 
       add_smoke-t2d6.5-0 ; 
       add_smoke-t2d63.5-0 ; 
       add_smoke-t2d64.5-0 ; 
       add_smoke-t2d65.5-0 ; 
       add_smoke-t2d7.5-0 ; 
       add_smoke-t2d8.5-0 ; 
       add_smoke-t2d9.5-0 ; 
       Center-t2d66.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       3 2 110 ; 
       4 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       3 4 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 1 300 ; 
       3 5 300 ; 
       3 12 300 ; 
       4 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 13 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 3 401 ; 
       6 2 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 4 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 7 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 21.25 0 0 SRT 1 1 1 0 0 0 0 0 1.377536 MPRFLG 0 ; 
       3 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
