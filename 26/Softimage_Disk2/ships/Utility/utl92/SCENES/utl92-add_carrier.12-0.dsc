SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       bom04-fuselg0.12-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.13-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       add_carrier-mat83.2-0 ; 
       add_smoke-mat1.1-0 ; 
       add_smoke-mat10.1-0 ; 
       add_smoke-mat2.1-0 ; 
       add_smoke-mat3.1-0 ; 
       add_smoke-mat4.1-0 ; 
       add_smoke-mat46.1-0 ; 
       add_smoke-mat5.1-0 ; 
       add_smoke-mat57.1-0 ; 
       add_smoke-mat58.1-0 ; 
       add_smoke-mat6.1-0 ; 
       add_smoke-mat63.1-0 ; 
       add_smoke-mat64.1-0 ; 
       add_smoke-mat66.1-0 ; 
       add_smoke-mat7.1-0 ; 
       add_smoke-mat8.1-0 ; 
       add_smoke-mat9.1-0 ; 
       rix_fig_F-mat79.1-0 ; 
       rix_fig_F-mat80.1-0 ; 
       rix_fig_F-mat81.1-0 ; 
       rix_fig_F-mat82.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       bom04-afinzzz.1-0 ; 
       bom04-cockpt.1-0 ; 
       bom04-ffuselg.1-0 ; 
       bom04-fuselg0.10-0 ROOT ; 
       bom04-fuselg1.2-0 ; 
       bom04-missemt.1-0 ; 
       bom04-smoke.1-0 ; 
       bom04-sphere2.1-0 ; 
       bom04-SSb1.1-0 ; 
       bom04-SSb2.1-0 ; 
       bom04-SSb3.1-0 ; 
       bom04-SSb4.1-0 ; 
       bom04-thrust.1-0 ; 
       bom04-trail.1-0 ; 
       bom04-turwepemt1.1-0 ; 
       bom04-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl92/PICTURES/utl92 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl92-add_carrier.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       add_carrier-t2d66.3-0 ; 
       add_smoke-t2d1.4-0 ; 
       add_smoke-t2d2.4-0 ; 
       add_smoke-t2d4.5-0 ; 
       add_smoke-t2d45.5-0 ; 
       add_smoke-t2d5.5-0 ; 
       add_smoke-t2d57.4-0 ; 
       add_smoke-t2d58.4-0 ; 
       add_smoke-t2d6.5-0 ; 
       add_smoke-t2d63.5-0 ; 
       add_smoke-t2d64.5-0 ; 
       add_smoke-t2d65.5-0 ; 
       add_smoke-t2d7.5-0 ; 
       add_smoke-t2d8.5-0 ; 
       add_smoke-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       7 4 110 ; 
       1 3 110 ; 
       2 4 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 4 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       7 0 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       4 5 300 ; 
       4 7 300 ; 
       4 10 300 ; 
       4 14 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 2 300 ; 
       4 6 300 ; 
       4 13 300 ; 
       8 17 300 ; 
       9 18 300 ; 
       10 19 300 ; 
       11 20 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 14 401 ; 
       0 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       6 4 401 ; 
       7 3 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 5 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 11 401 ; 
       14 8 401 ; 
       15 12 401 ; 
       16 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 -4 0 MPRFLG 0 ; 
       7 SCHEM 40 -4 0 MPRFLG 0 ; 
       1 SCHEM 65 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 38.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 72.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 70 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 74 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
