SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.10-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 56     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
       add_middle-2.1-0 ; 
       add_middle-default8.1-0 ; 
       add_middle-mat116.1-0 ; 
       add_middle-mat117.1-0 ; 
       add_middle-mat118.1-0 ; 
       nulls-0.1-0 ; 
       nulls-1.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat112.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       add_middle-fuselg1.4-0 ROOT ; 
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.6-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/bom05 ; 
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-add_middle.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d35.2-0 ; 
       add_gun-t2d38.2-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.1-0 ; 
       add_gun-t2d50.1-0 ; 
       add_gun-t2d9.1-0 ; 
       add_middle-t2d114.1-0 ; 
       add_middle-t2d115.1-0 ; 
       add_middle-t2d116.1-0 ; 
       add_middle-t2d117.1-0 ; 
       nulls-t2d105.1-0 ; 
       nulls-t2d106.1-0 ; 
       nulls-t2d107.1-0 ; 
       nulls-t2d108.1-0 ; 
       nulls-t2d109.1-0 ; 
       nulls-t2d110.1-0 ; 
       nulls-t2d111.1-0 ; 
       nulls-t2d112.2-0 ; 
       nulls-t2d113.2-0 ; 
       nulls-t2d95.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 24 110 ; 
       2 4 110 ; 
       4 14 110 ; 
       5 24 110 ; 
       6 4 110 ; 
       7 1 110 ; 
       8 14 110 ; 
       9 15 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 3 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 14 110 ; 
       18 12 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 14 110 ; 
       25 12 110 ; 
       26 19 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 4 110 ; 
       32 14 110 ; 
       33 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 42 300 ; 
       0 43 300 ; 
       0 44 300 ; 
       0 45 300 ; 
       0 41 300 ; 
       1 39 300 ; 
       1 52 300 ; 
       5 55 300 ; 
       5 46 300 ; 
       9 48 300 ; 
       9 51 300 ; 
       10 49 300 ; 
       11 50 300 ; 
       13 53 300 ; 
       14 0 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       14 54 300 ; 
       14 47 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       24 38 300 ; 
       25 2 300 ; 
       25 3 300 ; 
       25 4 300 ; 
       25 5 300 ; 
       26 14 300 ; 
       26 15 300 ; 
       26 16 300 ; 
       27 34 300 ; 
       28 33 300 ; 
       29 1 300 ; 
       30 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       36 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       46 35 401 ; 
       47 38 401 ; 
       48 30 401 ; 
       49 31 401 ; 
       50 32 401 ; 
       51 33 401 ; 
       52 34 401 ; 
       53 36 401 ; 
       54 37 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       41 29 401 ; 
       55 39 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 57.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 3.141593 0 0 0.1759995 -1.616466 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 MPRFLG 0 ; 
       14 SCHEM 28.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       16 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 10 -8 0 MPRFLG 0 ; 
       22 SCHEM 15 -8 0 MPRFLG 0 ; 
       23 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 40 -6 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 25 -4 0 MPRFLG 0 ; 
       28 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 30 -4 0 MPRFLG 0 ; 
       30 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 85.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 85.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 85.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 85.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 85.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 85.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 85.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 85.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 85.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
