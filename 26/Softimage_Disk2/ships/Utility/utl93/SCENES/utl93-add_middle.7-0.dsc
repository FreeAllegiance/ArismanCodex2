SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       add_middle-bom05.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
       add_middle-default8.2-0 ; 
       nulls-0.1-0 ; 
       nulls-1.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat112.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       add_middle-blgun.1-0 ; 
       add_middle-blthrust.1-0 ; 
       add_middle-bom05.2-0 ROOT ; 
       add_middle-boosters.1-0 ; 
       add_middle-brgun.1-0 ; 
       add_middle-brthrust.1-0 ; 
       add_middle-bwepemt.1-0 ; 
       add_middle-cockpt.1-0 ; 
       add_middle-cyl4.1-0 ; 
       add_middle-drop_tank.5-0 ; 
       add_middle-face2.1-0 ; 
       add_middle-face3.1-0 ; 
       add_middle-finzzz0.1-0 ; 
       add_middle-finzzz1.2-0 ; 
       add_middle-fuselg.2-0 ; 
       add_middle-fwepatt.1-0 ; 
       add_middle-fwepemt.1-0 ; 
       add_middle-landgr1.1-0 ; 
       add_middle-lfinzzz.1-0 ; 
       add_middle-LL1.2-0 ; 
       add_middle-LLa.1-0 ; 
       add_middle-llandgr.1-0 ; 
       add_middle-LLl.1-0 ; 
       add_middle-LLr.1-0 ; 
       add_middle-pod.1-0 ; 
       add_middle-rfinzzz.1-0 ; 
       add_middle-rlandgr.1-0 ; 
       add_middle-SSa.2-0 ; 
       add_middle-SSf.2-0 ; 
       add_middle-SSl.2-0 ; 
       add_middle-SSr.2-0 ; 
       add_middle-tlthrust.1-0 ; 
       add_middle-trail.1-0 ; 
       add_middle-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/bom05 ; 
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-add_middle.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d35.2-0 ; 
       add_gun-t2d38.2-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.1-0 ; 
       add_gun-t2d50.1-0 ; 
       add_gun-t2d9.1-0 ; 
       add_middle-t2d114.3-0 ; 
       nulls-t2d105.1-0 ; 
       nulls-t2d106.1-0 ; 
       nulls-t2d107.1-0 ; 
       nulls-t2d108.1-0 ; 
       nulls-t2d109.1-0 ; 
       nulls-t2d110.1-0 ; 
       nulls-t2d111.1-0 ; 
       nulls-t2d112.2-0 ; 
       nulls-t2d113.2-0 ; 
       nulls-t2d95.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 2 110 ; 
       0 24 110 ; 
       1 3 110 ; 
       3 14 110 ; 
       4 24 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 14 110 ; 
       8 15 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 14 110 ; 
       18 12 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 14 110 ; 
       25 12 110 ; 
       26 19 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 3 110 ; 
       32 14 110 ; 
       33 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       9 41 300 ; 
       0 39 300 ; 
       0 48 300 ; 
       4 51 300 ; 
       4 42 300 ; 
       8 44 300 ; 
       8 47 300 ; 
       10 45 300 ; 
       11 46 300 ; 
       13 49 300 ; 
       14 0 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       14 50 300 ; 
       14 43 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       24 38 300 ; 
       25 2 300 ; 
       25 3 300 ; 
       25 4 300 ; 
       25 5 300 ; 
       26 14 300 ; 
       26 15 300 ; 
       26 16 300 ; 
       27 34 300 ; 
       28 33 300 ; 
       29 1 300 ; 
       30 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       41 26 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       36 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       42 32 401 ; 
       43 35 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       48 31 401 ; 
       49 33 401 ; 
       50 34 401 ; 
       51 36 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 155 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 135 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 78.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 133.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 127.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 115 -4 0 MPRFLG 0 ; 
       13 SCHEM 105 -6 0 MPRFLG 0 ; 
       14 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       18 SCHEM 111.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       21 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       22 SCHEM 48.75 -8 0 MPRFLG 0 ; 
       23 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       24 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 121.25 -6 0 MPRFLG 0 ; 
       26 SCHEM 20 -8 0 MPRFLG 0 ; 
       27 SCHEM 95 -4 0 MPRFLG 0 ; 
       28 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       29 SCHEM 100 -4 0 MPRFLG 0 ; 
       30 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 130 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 152.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 55 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 155 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 145 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 150 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 147.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 145 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 35 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 147.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 150 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 155 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 156.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
