SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.31-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.41-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       add_refuel-2.1-0 ; 
       add_refuel-back1.1-0 ; 
       add_refuel-default8.1-0 ; 
       add_refuel-front1.1-0 ; 
       add_refuel-mat118.1-0 ; 
       add_refuel-mat119.1-0 ; 
       add_refuel-mat120.1-0 ; 
       add_refuel-mat121.1-0 ; 
       add_refuel-mat122.1-0 ; 
       add_refuel-mat123.1-0 ; 
       add_refuel-sides.1-0 ; 
       add_refuel-top1.1-0 ; 
       nulls-mat114.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 23     
       add_refuel-cyl3.1-0 ROOT ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.21-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-add_refuel.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d9.4-0 ; 
       add_refuel-t2d114.1-0 ; 
       add_refuel-t2d115.1-0 ; 
       add_refuel-t2d116.1-0 ; 
       add_refuel-t2d117.1-0 ; 
       add_refuel-t2d121.1-0 ; 
       add_refuel-t2d122.1-0 ; 
       add_refuel-t2d123.1-0 ; 
       add_refuel-t2d125.1-0 ; 
       add_refuel-t2d126.1-0 ; 
       add_refuel-t2d127.1-0 ; 
       add_refuel-t2d128.1-0 ; 
       add_refuel-t2d129.1-0 ; 
       nulls-t2d111.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       3 9 110 ; 
       4 3 110 ; 
       5 9 110 ; 
       6 2 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       9 2 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 7 110 ; 
       15 10 110 ; 
       16 9 110 ; 
       17 9 110 ; 
       18 9 110 ; 
       19 9 110 ; 
       20 3 110 ; 
       21 9 110 ; 
       22 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 33 300 ; 
       6 26 300 ; 
       6 24 300 ; 
       6 34 300 ; 
       6 32 300 ; 
       8 35 300 ; 
       9 25 300 ; 
       9 27 300 ; 
       9 28 300 ; 
       9 29 300 ; 
       9 23 300 ; 
       9 30 300 ; 
       9 31 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       16 21 300 ; 
       17 20 300 ; 
       18 0 300 ; 
       19 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 12 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       35 25 401 ; 
       23 20 401 ; 
       24 15 401 ; 
       25 17 401 ; 
       26 14 401 ; 
       27 18 401 ; 
       28 19 401 ; 
       29 23 401 ; 
       30 21 401 ; 
       31 22 401 ; 
       32 24 401 ; 
       33 13 401 ; 
       34 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 68.75 -8 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 107.5 0 0 DISPLAY 1 2 SRT 0.06814493 0.06814493 0.06814493 0 0 1.06 0.5201219 -0.2164767 1.807849 MPRFLG 0 ; 
       5 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 100 -6 0 MPRFLG 0 ; 
       7 SCHEM 50 -8 0 MPRFLG 0 ; 
       8 SCHEM 40 -10 0 MPRFLG 0 ; 
       9 SCHEM 46.25 -6 0 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 MPRFLG 0 ; 
       11 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       12 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       13 SCHEM 0 -8 0 MPRFLG 0 ; 
       14 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       15 SCHEM 5 -10 0 MPRFLG 0 ; 
       16 SCHEM 30 -8 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 35 -8 0 MPRFLG 0 ; 
       19 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 57.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 45 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 27.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 37.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 40 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 99.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 104.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 95.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 96.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 98.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 102.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 57.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 45 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 40 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 103.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 94.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 95.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 98.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 100.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 101.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 98.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 106.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
