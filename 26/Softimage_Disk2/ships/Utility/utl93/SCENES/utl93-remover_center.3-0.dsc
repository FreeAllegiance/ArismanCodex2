SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       add_gun-default7.1-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.1-0 ; 
       add_gun-mat10.1-0 ; 
       add_gun-mat11.1-0 ; 
       add_gun-mat12.1-0 ; 
       add_gun-mat13.1-0 ; 
       add_gun-mat18.1-0 ; 
       add_gun-mat19.1-0 ; 
       add_gun-mat20.1-0 ; 
       add_gun-mat21.1-0 ; 
       add_gun-mat22.1-0 ; 
       add_gun-mat23.1-0 ; 
       add_gun-mat24.1-0 ; 
       add_gun-mat25.1-0 ; 
       add_gun-mat26.1-0 ; 
       add_gun-mat27.1-0 ; 
       add_gun-mat28.1-0 ; 
       add_gun-mat29.1-0 ; 
       add_gun-mat30.1-0 ; 
       add_gun-mat31.1-0 ; 
       add_gun-mat34.1-0 ; 
       add_gun-mat35.1-0 ; 
       add_gun-mat36.1-0 ; 
       add_gun-mat37.1-0 ; 
       add_gun-mat38.1-0 ; 
       add_gun-mat39.1-0 ; 
       add_gun-mat40.1-0 ; 
       add_gun-mat41.1-0 ; 
       add_gun-mat42.1-0 ; 
       add_gun-mat43.1-0 ; 
       add_gun-mat44.1-0 ; 
       add_gun-mat47.1-0 ; 
       add_gun-mat50.1-0 ; 
       add_gun-mat62.1-0 ; 
       add_gun-mat63.1-0 ; 
       add_gun-mat66.1-0 ; 
       add_gun-mat67.1-0 ; 
       add_gun-mat68.1-0 ; 
       add_gun-mat69.1-0 ; 
       add_gun-mat71.1-0 ; 
       add_gun-starbord_green-right.1-0.1-0 ; 
       nulls-0.1-0 ; 
       nulls-1.1-0 ; 
       nulls-mat109.1-0 ; 
       nulls-mat110.1-0 ; 
       nulls-mat111.1-0 ; 
       nulls-mat112.1-0 ; 
       nulls-mat113.1-0 ; 
       nulls-mat114.1-0 ; 
       nulls-mat115.1-0 ; 
       nulls-mat95.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.5-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg.2-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/bom05 ; 
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-remover_center.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_gun-t2d10.1-0 ; 
       add_gun-t2d11.1-0 ; 
       add_gun-t2d15.1-0 ; 
       add_gun-t2d16.1-0 ; 
       add_gun-t2d17.1-0 ; 
       add_gun-t2d18.1-0 ; 
       add_gun-t2d19.1-0 ; 
       add_gun-t2d20.1-0 ; 
       add_gun-t2d21.1-0 ; 
       add_gun-t2d22.1-0 ; 
       add_gun-t2d23.1-0 ; 
       add_gun-t2d25.1-0 ; 
       add_gun-t2d26.1-0 ; 
       add_gun-t2d27.1-0 ; 
       add_gun-t2d28.1-0 ; 
       add_gun-t2d29.1-0 ; 
       add_gun-t2d30.1-0 ; 
       add_gun-t2d31.1-0 ; 
       add_gun-t2d32.1-0 ; 
       add_gun-t2d35.2-0 ; 
       add_gun-t2d38.2-0 ; 
       add_gun-t2d47.1-0 ; 
       add_gun-t2d48.1-0 ; 
       add_gun-t2d49.1-0 ; 
       add_gun-t2d50.1-0 ; 
       add_gun-t2d9.1-0 ; 
       nulls-t2d105.1-0 ; 
       nulls-t2d106.1-0 ; 
       nulls-t2d107.1-0 ; 
       nulls-t2d108.1-0 ; 
       nulls-t2d109.1-0 ; 
       nulls-t2d110.1-0 ; 
       nulls-t2d111.1-0 ; 
       nulls-t2d112.2-0 ; 
       nulls-t2d113.2-0 ; 
       nulls-t2d95.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 23 110 ; 
       1 3 110 ; 
       3 13 110 ; 
       4 23 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 13 110 ; 
       8 14 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 13 110 ; 
       12 11 110 ; 
       13 2 110 ; 
       14 13 110 ; 
       15 9 110 ; 
       16 13 110 ; 
       17 11 110 ; 
       18 16 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 18 110 ; 
       22 18 110 ; 
       23 13 110 ; 
       24 11 110 ; 
       25 18 110 ; 
       26 13 110 ; 
       27 13 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 3 110 ; 
       31 13 110 ; 
       32 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       0 47 300 ; 
       4 50 300 ; 
       4 41 300 ; 
       8 43 300 ; 
       8 46 300 ; 
       9 44 300 ; 
       10 45 300 ; 
       12 48 300 ; 
       13 0 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       13 49 300 ; 
       13 42 300 ; 
       16 24 300 ; 
       16 25 300 ; 
       16 26 300 ; 
       16 27 300 ; 
       17 6 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       17 9 300 ; 
       18 20 300 ; 
       18 21 300 ; 
       18 22 300 ; 
       18 23 300 ; 
       19 28 300 ; 
       19 29 300 ; 
       19 30 300 ; 
       20 10 300 ; 
       20 11 300 ; 
       20 12 300 ; 
       20 13 300 ; 
       21 17 300 ; 
       21 18 300 ; 
       21 19 300 ; 
       22 35 300 ; 
       22 36 300 ; 
       22 37 300 ; 
       23 38 300 ; 
       24 2 300 ; 
       24 3 300 ; 
       24 4 300 ; 
       24 5 300 ; 
       25 14 300 ; 
       25 15 300 ; 
       25 16 300 ; 
       26 34 300 ; 
       27 33 300 ; 
       28 1 300 ; 
       29 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       21 10 400 ; 
       22 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       36 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       41 31 401 ; 
       42 34 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 20 -8 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 MPRFLG 0 ; 
       13 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 10 -8 0 MPRFLG 0 ; 
       21 SCHEM 15 -8 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 40 -6 0 MPRFLG 0 ; 
       25 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 25 -4 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       28 SCHEM 30 -4 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
