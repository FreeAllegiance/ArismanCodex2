SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       retext-bom05.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       add_gun-default7.3-0 ; 
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.2-0 ; 
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat29.2-0 ; 
       add_gun-mat30.2-0 ; 
       add_gun-mat31.2-0 ; 
       add_gun-mat34.2-0 ; 
       add_gun-mat35.2-0 ; 
       add_gun-mat36.2-0 ; 
       add_gun-mat37.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       add_gun-mat42.2-0 ; 
       add_gun-mat43.2-0 ; 
       add_gun-mat44.2-0 ; 
       add_gun-mat47.3-0 ; 
       add_gun-mat50.3-0 ; 
       add_gun-mat62.2-0 ; 
       add_gun-mat63.2-0 ; 
       add_gun-mat66.2-0 ; 
       add_gun-mat67.2-0 ; 
       add_gun-mat68.2-0 ; 
       add_gun-mat69.2-0 ; 
       add_gun-mat71.2-0 ; 
       add_gun-starbord_green-right.1-0.2-0 ; 
       nulls-0.2-0 ; 
       nulls-1.3-0 ; 
       nulls-mat109.2-0 ; 
       nulls-mat110.2-0 ; 
       nulls-mat111.2-0 ; 
       nulls-mat112.2-0 ; 
       nulls-mat113.2-0 ; 
       nulls-mat114.2-0 ; 
       nulls-mat115.3-0 ; 
       nulls-mat95.2-0 ; 
       retext-back1.2-0 ; 
       retext-front1.2-0 ; 
       retext-mat116.1-0 ; 
       retext-mat117.1-0 ; 
       retext-sides.2-0 ; 
       retext-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       retext-blgun.1-0 ; 
       retext-blthrust.1-0 ; 
       retext-bom05.7-0 ROOT ; 
       retext-boosters.1-0 ; 
       retext-brgun.1-0 ; 
       retext-brthrust.1-0 ; 
       retext-bwepemt.1-0 ; 
       retext-cockpt.1-0 ; 
       retext-cyl4.1-0 ; 
       retext-drop_tank.5-0 ; 
       retext-face2.1-0 ; 
       retext-face3.1-0 ; 
       retext-finzzz0.1-0 ; 
       retext-finzzz1.2-0 ; 
       retext-fuselg.2-0 ; 
       retext-fwepatt.1-0 ; 
       retext-fwepemt.1-0 ; 
       retext-landgr1.1-0 ; 
       retext-lfinzzz.1-0 ; 
       retext-LL1.2-0 ; 
       retext-LLa.1-0 ; 
       retext-llandgr.1-0 ; 
       retext-LLl.1-0 ; 
       retext-LLr.1-0 ; 
       retext-pod.1-0 ; 
       retext-rfinzzz.1-0 ; 
       retext-rlandgr.1-0 ; 
       retext-SSa.2-0 ; 
       retext-SSf.2-0 ; 
       retext-SSl.2-0 ; 
       retext-SSr.2-0 ; 
       retext-tlthrust.1-0 ; 
       retext-trail.1-0 ; 
       retext-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-retext.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 43     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d22.2-0 ; 
       add_gun-t2d23.2-0 ; 
       add_gun-t2d25.2-0 ; 
       add_gun-t2d26.2-0 ; 
       add_gun-t2d27.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d31.2-0 ; 
       add_gun-t2d32.2-0 ; 
       add_gun-t2d35.5-0 ; 
       add_gun-t2d38.5-0 ; 
       add_gun-t2d47.2-0 ; 
       add_gun-t2d48.2-0 ; 
       add_gun-t2d49.2-0 ; 
       add_gun-t2d50.2-0 ; 
       add_gun-t2d9.2-0 ; 
       nulls-t2d105.2-0 ; 
       nulls-t2d106.2-0 ; 
       nulls-t2d107.2-0 ; 
       nulls-t2d108.2-0 ; 
       nulls-t2d109.2-0 ; 
       nulls-t2d110.2-0 ; 
       nulls-t2d111.2-0 ; 
       nulls-t2d112.5-0 ; 
       nulls-t2d113.5-0 ; 
       nulls-t2d95.2-0 ; 
       retext-t2d114.4-0 ; 
       retext-t2d115.3-0 ; 
       retext-t2d116.3-0 ; 
       retext-t2d117.3-0 ; 
       retext-t2d118.1-0 ; 
       retext-t2d119.1-0 ; 
       retext-t2d120.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 3 110 ; 
       3 14 110 ; 
       4 24 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 14 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 14 110 ; 
       18 12 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 14 110 ; 
       25 12 110 ; 
       26 19 110 ; 
       27 14 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 3 110 ; 
       32 14 110 ; 
       33 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 39 300 ; 
       0 47 300 ; 
       4 50 300 ; 
       4 41 300 ; 
       8 43 300 ; 
       8 46 300 ; 
       9 55 300 ; 
       9 52 300 ; 
       9 51 300 ; 
       9 56 300 ; 
       10 44 300 ; 
       11 45 300 ; 
       13 48 300 ; 
       14 0 300 ; 
       14 31 300 ; 
       14 32 300 ; 
       14 49 300 ; 
       14 42 300 ; 
       14 53 300 ; 
       14 54 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       18 9 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       19 23 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       20 30 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       21 13 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       22 19 300 ; 
       23 35 300 ; 
       23 36 300 ; 
       23 37 300 ; 
       24 38 300 ; 
       25 2 300 ; 
       25 3 300 ; 
       25 4 300 ; 
       25 5 300 ; 
       26 14 300 ; 
       26 15 300 ; 
       26 16 300 ; 
       27 34 300 ; 
       28 33 300 ; 
       29 1 300 ; 
       30 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 21 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 42 401 ; 
       3 25 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       18 9 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 16 401 ; 
       29 17 401 ; 
       30 18 401 ; 
       31 19 401 ; 
       32 20 401 ; 
       36 22 401 ; 
       38 23 401 ; 
       39 24 401 ; 
       41 31 401 ; 
       42 34 401 ; 
       43 26 401 ; 
       44 27 401 ; 
       45 28 401 ; 
       46 29 401 ; 
       47 30 401 ; 
       48 32 401 ; 
       49 33 401 ; 
       50 35 401 ; 
       51 38 401 ; 
       52 37 401 ; 
       55 36 401 ; 
       56 39 401 ; 
       53 40 401 ; 
       54 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       1 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 131.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -10 0 MPRFLG 0 ; 
       5 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 85 -10 0 MPRFLG 0 ; 
       9 SCHEM 161.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 81.25 -12 0 MPRFLG 0 ; 
       11 SCHEM 85 -12 0 MPRFLG 0 ; 
       12 SCHEM 112.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 85 -8 0 MPRFLG 0 ; 
       16 SCHEM 80 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 46.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 108.75 -10 0 MPRFLG 0 ; 
       19 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 55 -12 0 MPRFLG 0 ; 
       21 SCHEM 26.25 -12 0 MPRFLG 0 ; 
       22 SCHEM 46.25 -12 0 MPRFLG 0 ; 
       23 SCHEM 36.25 -12 0 MPRFLG 0 ; 
       24 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       25 SCHEM 118.75 -10 0 MPRFLG 0 ; 
       26 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       27 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 95 -8 0 MPRFLG 0 ; 
       29 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 100 -8 0 MPRFLG 0 ; 
       31 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 122.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 115 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 117.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 120 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 112.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 105 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 107.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 110 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 30 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 45 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 67.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 62.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 65 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 55 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 32.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 35 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 147.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 90 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 82.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 85 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 87.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 2.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 102.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 145 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 157.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 117.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 120 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 105 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 107.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 110 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 17.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 65 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 55 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 37.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 115 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 90 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 85 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 87.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 102.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 145 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 147.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 157.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 162.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 166.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
