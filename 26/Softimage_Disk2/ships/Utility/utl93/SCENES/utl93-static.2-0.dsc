SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.45-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       nulls-mat114.3-0 ; 
       static-2.1-0 ; 
       static-back1.1-0 ; 
       static-default8.1-0 ; 
       static-front1.1-0 ; 
       static-mat118.1-0 ; 
       static-mat119.1-0 ; 
       static-mat120.1-0 ; 
       static-mat121.1-0 ; 
       static-mat122.1-0 ; 
       static-mat123.1-0 ; 
       static-sides.1-0 ; 
       static-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       nulls-bom05.24-0 ROOT ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d9.4-0 ; 
       nulls-t2d111.4-0 ; 
       static-t2d114.1-0 ; 
       static-t2d115.1-0 ; 
       static-t2d116.1-0 ; 
       static-t2d117.1-0 ; 
       static-t2d121.1-0 ; 
       static-t2d122.1-0 ; 
       static-t2d123.1-0 ; 
       static-t2d125.1-0 ; 
       static-t2d126.1-0 ; 
       static-t2d127.1-0 ; 
       static-t2d128.1-0 ; 
       static-t2d129.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 5 110 ; 
       8 2 110 ; 
       9 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 30 300 ; 
       1 23 300 ; 
       1 21 300 ; 
       1 31 300 ; 
       1 29 300 ; 
       3 19 300 ; 
       4 22 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 20 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 12 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       20 21 401 ; 
       21 16 401 ; 
       22 18 401 ; 
       23 15 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 24 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 25 401 ; 
       30 14 401 ; 
       31 17 401 ; 
       19 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       5 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 59.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 60.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 63.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 65.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 65.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 58.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 61.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 62.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 64.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 60.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
