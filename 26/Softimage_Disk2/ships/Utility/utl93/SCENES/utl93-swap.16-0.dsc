SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.27-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.37-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 58     
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-mat66.3-0 ; 
       add_gun-mat67.3-0 ; 
       add_gun-mat68.3-0 ; 
       add_gun-mat69.3-0 ; 
       add_gun-mat71.3-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       nulls-0.3-0 ; 
       nulls-mat109.3-0 ; 
       nulls-mat110.3-0 ; 
       nulls-mat111.3-0 ; 
       nulls-mat112.3-0 ; 
       nulls-mat113.3-0 ; 
       nulls-mat114.3-0 ; 
       nulls-mat95.3-0 ; 
       swap-2.1-0 ; 
       swap-back1.2-0 ; 
       swap-default8.1-0 ; 
       swap-front1.2-0 ; 
       swap-mat118.1-0 ; 
       swap-mat119.1-0 ; 
       swap-mat120.1-0 ; 
       swap-mat121.1-0 ; 
       swap-mat122.1-0 ; 
       swap-mat123.1-0 ; 
       swap-sides.2-0 ; 
       swap-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 35     
       nulls-blgun.1-0 ; 
       nulls-blthrust.1-0 ; 
       nulls-bom05.19-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brgun.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-bwepemt.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl4.1-0 ; 
       nulls-drop_tank.5-0 ; 
       nulls-face2.1-0 ; 
       nulls-face3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-fwepatt.1-0 ; 
       nulls-fwepemt.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-pod.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-swap.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.5-0 ; 
       add_gun-t2d26.5-0 ; 
       add_gun-t2d27.5-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       add_gun-t2d47.4-0 ; 
       add_gun-t2d48.4-0 ; 
       add_gun-t2d49.4-0 ; 
       add_gun-t2d50.4-0 ; 
       add_gun-t2d9.4-0 ; 
       nulls-t2d105.4-0 ; 
       nulls-t2d106.4-0 ; 
       nulls-t2d107.4-0 ; 
       nulls-t2d108.4-0 ; 
       nulls-t2d109.4-0 ; 
       nulls-t2d110.4-0 ; 
       nulls-t2d111.4-0 ; 
       nulls-t2d95.4-0 ; 
       swap-t2d114.5-0 ; 
       swap-t2d115.5-0 ; 
       swap-t2d116.5-0 ; 
       swap-t2d117.5-0 ; 
       swap-t2d121.8-0 ; 
       swap-t2d122.8-0 ; 
       swap-t2d123.8-0 ; 
       swap-t2d125.8-0 ; 
       swap-t2d126.8-0 ; 
       swap-t2d127.8-0 ; 
       swap-t2d128.2-0 ; 
       swap-t2d129.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 25 110 ; 
       1 3 110 ; 
       3 14 110 ; 
       4 25 110 ; 
       5 3 110 ; 
       6 0 110 ; 
       7 14 110 ; 
       8 15 110 ; 
       9 2 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 14 110 ; 
       13 12 110 ; 
       14 2 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 14 110 ; 
       18 12 110 ; 
       19 17 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 14 110 ; 
       25 14 110 ; 
       26 12 110 ; 
       27 19 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 14 110 ; 
       32 3 110 ; 
       33 14 110 ; 
       34 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 36 300 ; 
       0 43 300 ; 
       4 45 300 ; 
       4 38 300 ; 
       8 39 300 ; 
       8 42 300 ; 
       9 56 300 ; 
       9 49 300 ; 
       9 47 300 ; 
       9 57 300 ; 
       9 55 300 ; 
       10 40 300 ; 
       11 41 300 ; 
       13 44 300 ; 
       14 48 300 ; 
       14 50 300 ; 
       14 51 300 ; 
       14 52 300 ; 
       14 46 300 ; 
       14 53 300 ; 
       14 54 300 ; 
       17 23 300 ; 
       17 24 300 ; 
       17 25 300 ; 
       17 26 300 ; 
       18 5 300 ; 
       18 6 300 ; 
       18 7 300 ; 
       18 8 300 ; 
       19 19 300 ; 
       19 20 300 ; 
       19 21 300 ; 
       19 22 300 ; 
       20 27 300 ; 
       20 28 300 ; 
       20 29 300 ; 
       21 9 300 ; 
       21 10 300 ; 
       21 11 300 ; 
       21 12 300 ; 
       22 16 300 ; 
       22 17 300 ; 
       22 18 300 ; 
       23 32 300 ; 
       23 33 300 ; 
       23 34 300 ; 
       25 35 300 ; 
       26 1 300 ; 
       26 2 300 ; 
       26 3 300 ; 
       26 4 300 ; 
       27 13 300 ; 
       27 14 300 ; 
       27 15 300 ; 
       28 31 300 ; 
       29 30 300 ; 
       30 0 300 ; 
       31 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       22 10 400 ; 
       23 19 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       2 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 23 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       28 17 401 ; 
       29 18 401 ; 
       33 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       38 29 401 ; 
       39 24 401 ; 
       40 25 401 ; 
       41 26 401 ; 
       42 27 401 ; 
       43 28 401 ; 
       44 30 401 ; 
       45 31 401 ; 
       46 39 401 ; 
       47 34 401 ; 
       48 36 401 ; 
       49 33 401 ; 
       50 37 401 ; 
       51 38 401 ; 
       52 42 401 ; 
       53 40 401 ; 
       54 41 401 ; 
       55 43 401 ; 
       56 32 401 ; 
       57 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -60.0085 0 MPRFLG 0 ; 
       1 SCHEM 135 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.9883 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 133.75 -58.0085 0 MPRFLG 0 ; 
       4 SCHEM 8.75 -60.0085 0 MPRFLG 0 ; 
       5 SCHEM 137.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 0 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 127.5 -58.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -60.0085 0 MPRFLG 0 ; 
       9 SCHEM 163.75 -6 0 MPRFLG 0 ; 
       10 SCHEM 83.75 -62.0085 0 MPRFLG 0 ; 
       11 SCHEM 87.5 -62.0085 0 MPRFLG 0 ; 
       12 SCHEM 115 -58.0085 0 MPRFLG 0 ; 
       13 SCHEM 105 -60.0085 0 MPRFLG 0 ; 
       14 SCHEM 78.75 -56.0085 0 USR MPRFLG 0 ; 
       15 SCHEM 87.5 -58.0085 0 MPRFLG 0 ; 
       16 SCHEM 82.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 48.75 -58.0085 0 MPRFLG 0 ; 
       18 SCHEM 111.25 -60.0085 0 MPRFLG 0 ; 
       19 SCHEM 43.75 -60.0085 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -62.0085 0 MPRFLG 0 ; 
       21 SCHEM 28.75 -62.0085 0 MPRFLG 0 ; 
       22 SCHEM 48.75 -62.0085 0 MPRFLG 0 ; 
       23 SCHEM 38.75 -62.0085 0 MPRFLG 0 ; 
       24 SCHEM 15 -58.0085 0 MPRFLG 0 ; 
       25 SCHEM 6.25 -58.0085 0 MPRFLG 0 ; 
       26 SCHEM 121.25 -60.0085 0 MPRFLG 0 ; 
       27 SCHEM 20 -62.0085 0 MPRFLG 0 ; 
       28 SCHEM 95 -58.0085 0 MPRFLG 0 ; 
       29 SCHEM 97.5 -58.0085 0 MPRFLG 0 ; 
       30 SCHEM 100 -58.0085 0 MPRFLG 0 ; 
       31 SCHEM 102.5 -58.0085 0 MPRFLG 0 ; 
       32 SCHEM 132.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 140 -58.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 130 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 100 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 125 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 117.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 120 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 122.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 115 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 107.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 110 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 112.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 25 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 30 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 22.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 52.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 45 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 47.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 70 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 80 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 75 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 60 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 55 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 97.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 95 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 35 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 37.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 12.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 102.5 -60.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 7.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 92.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 85 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 87.5 -64.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 90 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 2.5 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 105 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 10 -62.0085 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 80.4048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 162.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 87.9048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 160 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 72.9048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 75.4048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 77.9048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 82.9048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 85.4048 -62.38425 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 170 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 167.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 165 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 120 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 122.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 107.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 110 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 112.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 30 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 45 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 65 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 67.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 72.5 -62.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 75 -62.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 77.5 -62.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 55 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 57.5 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 40 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 35 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -62.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 117.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 92.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 85 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -66.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 90 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 2.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 105 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 10 -64.0085 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 167.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 160 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 162.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 165 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 87.9048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 72.9048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 75.4048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 80.4048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 82.9048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 85.4048 -64.38425 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 77.9048 -64.38425 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 172.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 169 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
