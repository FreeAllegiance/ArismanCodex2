SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.29-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.39-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 49     
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat29.3-0 ; 
       add_gun-mat30.3-0 ; 
       add_gun-mat31.3-0 ; 
       add_gun-mat34.3-0 ; 
       add_gun-mat35.3-0 ; 
       add_gun-mat36.3-0 ; 
       add_gun-mat37.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat42.3-0 ; 
       add_gun-mat43.3-0 ; 
       add_gun-mat44.3-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-mat66.3-0 ; 
       add_gun-mat67.3-0 ; 
       add_gun-mat68.3-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       nulls-mat114.3-0 ; 
       swap-2.1-0 ; 
       swap-back1.2-0 ; 
       swap-default8.1-0 ; 
       swap-front1.2-0 ; 
       swap-mat118.1-0 ; 
       swap-mat119.1-0 ; 
       swap-mat120.1-0 ; 
       swap-mat121.1-0 ; 
       swap-mat122.1-0 ; 
       swap-mat123.1-0 ; 
       swap-sides.2-0 ; 
       swap-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       nulls-blthrust.1-0 ; 
       nulls-bom05.20-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-LL1.2-0 ; 
       nulls-LLa.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-LLl.1-0 ; 
       nulls-LLr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93-swap.18-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d22.4-0 ; 
       add_gun-t2d23.4-0 ; 
       add_gun-t2d25.5-0 ; 
       add_gun-t2d26.5-0 ; 
       add_gun-t2d27.5-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d31.4-0 ; 
       add_gun-t2d32.4-0 ; 
       add_gun-t2d47.4-0 ; 
       add_gun-t2d48.4-0 ; 
       add_gun-t2d9.4-0 ; 
       nulls-t2d111.4-0 ; 
       swap-t2d114.5-0 ; 
       swap-t2d115.5-0 ; 
       swap-t2d116.5-0 ; 
       swap-t2d117.5-0 ; 
       swap-t2d121.8-0 ; 
       swap-t2d122.8-0 ; 
       swap-t2d123.8-0 ; 
       swap-t2d125.8-0 ; 
       swap-t2d126.8-0 ; 
       swap-t2d127.8-0 ; 
       swap-t2d128.2-0 ; 
       swap-t2d129.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 8 110 ; 
       3 2 110 ; 
       4 8 110 ; 
       5 1 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 6 110 ; 
       11 9 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 8 110 ; 
       17 6 110 ; 
       18 11 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 2 110 ; 
       24 8 110 ; 
       25 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 47 300 ; 
       5 40 300 ; 
       5 38 300 ; 
       5 48 300 ; 
       5 46 300 ; 
       7 36 300 ; 
       8 39 300 ; 
       8 41 300 ; 
       8 42 300 ; 
       8 43 300 ; 
       8 37 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       9 23 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       11 19 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       12 27 300 ; 
       12 28 300 ; 
       12 29 300 ; 
       13 9 300 ; 
       13 10 300 ; 
       13 11 300 ; 
       13 12 300 ; 
       14 16 300 ; 
       14 17 300 ; 
       14 18 300 ; 
       15 32 300 ; 
       15 33 300 ; 
       15 34 300 ; 
       17 1 300 ; 
       17 2 300 ; 
       17 3 300 ; 
       17 4 300 ; 
       18 13 300 ; 
       18 14 300 ; 
       18 15 300 ; 
       19 31 300 ; 
       20 30 300 ; 
       21 0 300 ; 
       22 35 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       14 10 400 ; 
       15 19 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 21 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       28 17 401 ; 
       29 18 401 ; 
       33 20 401 ; 
       36 22 401 ; 
       37 30 401 ; 
       38 25 401 ; 
       39 27 401 ; 
       40 24 401 ; 
       41 28 401 ; 
       42 29 401 ; 
       43 33 401 ; 
       44 31 401 ; 
       45 32 401 ; 
       46 34 401 ; 
       47 23 401 ; 
       48 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 73.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 108.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 140 -2 0 MPRFLG 0 ; 
       6 SCHEM 90 -4 0 MPRFLG 0 ; 
       7 SCHEM 80 -6 0 MPRFLG 0 ; 
       8 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       11 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 MPRFLG 0 ; 
       13 SCHEM 16.25 -8 0 MPRFLG 0 ; 
       14 SCHEM 36.25 -8 0 MPRFLG 0 ; 
       15 SCHEM 26.25 -8 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       17 SCHEM 96.25 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 70 -4 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 75 -4 0 MPRFLG 0 ; 
       22 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 40 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 35 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 47.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 42.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 45 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 30 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 121 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 137.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 125.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 116.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 118 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 119.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 122.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 124 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 145 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 142.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 140 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 65 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 45 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 142.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 137.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 140 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 124.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 115.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 117 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 120 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 121.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 123 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 119.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 145 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 146.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
