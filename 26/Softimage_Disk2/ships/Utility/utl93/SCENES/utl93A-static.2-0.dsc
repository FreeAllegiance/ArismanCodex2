SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.53-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 33     
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       nulls-mat114.3-0 ; 
       static-2.3-0 ; 
       static-back1.2-0 ; 
       static-default8.3-0 ; 
       static-front1.2-0 ; 
       static-mat118.3-0 ; 
       static-mat119.3-0 ; 
       static-mat120.3-0 ; 
       static-mat121.3-0 ; 
       static-mat122.3-0 ; 
       static-mat123.2-0 ; 
       static-mat124.2-0 ; 
       static-sides.2-0 ; 
       static-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       nulls-bom05.30-0 ROOT ; 
       nulls-cyl3.1-0 ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93A-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d9.4-0 ; 
       nulls-t2d111.4-0 ; 
       static-t2d114.2-0 ; 
       static-t2d115.2-0 ; 
       static-t2d116.2-0 ; 
       static-t2d117.2-0 ; 
       static-t2d121.3-0 ; 
       static-t2d122.3-0 ; 
       static-t2d123.3-0 ; 
       static-t2d125.3-0 ; 
       static-t2d126.3-0 ; 
       static-t2d127.3-0 ; 
       static-t2d128.3-0 ; 
       static-t2d129.2-0 ; 
       static-t2d130.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 5 110 ; 
       4 3 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 6 110 ; 
       9 3 110 ; 
       10 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 30 300 ; 
       2 31 300 ; 
       2 23 300 ; 
       2 21 300 ; 
       2 32 300 ; 
       2 29 300 ; 
       4 19 300 ; 
       5 22 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 20 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       6 15 300 ; 
       6 16 300 ; 
       6 17 300 ; 
       6 18 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 11 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 12 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       20 21 401 ; 
       21 16 401 ; 
       22 18 401 ; 
       23 15 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 24 401 ; 
       27 22 401 ; 
       28 23 401 ; 
       29 25 401 ; 
       30 26 401 ; 
       31 14 401 ; 
       32 17 401 ; 
       19 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 5 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 MPRFLG 0 ; 
       6 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 10 -10 0 MPRFLG 0 ; 
       10 SCHEM 0 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -1 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 35.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 38.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 41.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 42.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 43.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 35.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 38.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 40.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 38.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
