SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05_2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.57-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.4-0 ; 
       add_gun-mat10.4-0 ; 
       add_gun-mat11.4-0 ; 
       add_gun-mat12.4-0 ; 
       add_gun-mat13.4-0 ; 
       add_gun-mat18.4-0 ; 
       add_gun-mat19.4-0 ; 
       add_gun-mat20.4-0 ; 
       add_gun-mat21.4-0 ; 
       add_gun-mat22.4-0 ; 
       add_gun-mat23.4-0 ; 
       add_gun-mat24.4-0 ; 
       add_gun-mat25.4-0 ; 
       add_gun-mat26.4-0 ; 
       add_gun-mat27.4-0 ; 
       add_gun-mat28.4-0 ; 
       add_gun-mat38.4-0 ; 
       add_gun-mat39.4-0 ; 
       add_gun-mat40.4-0 ; 
       add_gun-mat41.4-0 ; 
       add_gun-mat62.4-0 ; 
       add_gun-mat63.4-0 ; 
       add_gun-starbord_green-right.1-0.4-0 ; 
       edit_cargo-2.2-0 ; 
       edit_cargo-default8.2-0 ; 
       edit_cargo-mat118.2-0 ; 
       edit_cargo-mat119.2-0 ; 
       edit_cargo-mat120.2-0 ; 
       edit_cargo-mat121.2-0 ; 
       edit_cargo-mat122.2-0 ; 
       edit_cargo-mat124.2-0 ; 
       nulls-mat114.4-0 ; 
       scale-back1.1-0 ; 
       scale-front1.1-0 ; 
       scale-mat123.1-0 ; 
       scale-sides.1-0 ; 
       scale-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       nulls-blthrust.1-0 ; 
       nulls-bom05_2.2-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl3.1-0 ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-smoke1.1-0 ; 
       nulls-smoke2.1-0 ; 
       nulls-smoke3.1-0 ; 
       nulls-smoke4.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93B-scale.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 27     
       add_gun-t2d10.5-0 ; 
       add_gun-t2d11.5-0 ; 
       add_gun-t2d15.5-0 ; 
       add_gun-t2d16.5-0 ; 
       add_gun-t2d17.5-0 ; 
       add_gun-t2d18.5-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d20.5-0 ; 
       add_gun-t2d21.5-0 ; 
       add_gun-t2d28.5-0 ; 
       add_gun-t2d29.5-0 ; 
       add_gun-t2d30.5-0 ; 
       add_gun-t2d9.5-0 ; 
       edit_cargo-t2d121.3-0 ; 
       edit_cargo-t2d122.3-0 ; 
       edit_cargo-t2d123.3-0 ; 
       edit_cargo-t2d125.3-0 ; 
       edit_cargo-t2d126.3-0 ; 
       edit_cargo-t2d127.3-0 ; 
       edit_cargo-t2d128.3-0 ; 
       edit_cargo-t2d130.2-0 ; 
       nulls-t2d111.5-0 ; 
       scale-t2d114.1-0 ; 
       scale-t2d115.1-0 ; 
       scale-t2d116.1-0 ; 
       scale-t2d117.1-0 ; 
       scale-t2d129.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       6 1 110 ; 
       2 9 110 ; 
       3 2 110 ; 
       4 9 110 ; 
       5 1 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       9 1 110 ; 
       10 9 110 ; 
       11 7 110 ; 
       12 10 110 ; 
       13 9 110 ; 
       14 7 110 ; 
       15 10 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 9 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 9 110 ; 
       24 2 110 ; 
       25 9 110 ; 
       26 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 35 300 ; 
       6 33 300 ; 
       6 32 300 ; 
       6 36 300 ; 
       6 34 300 ; 
       5 30 300 ; 
       8 31 300 ; 
       9 24 300 ; 
       9 25 300 ; 
       9 26 300 ; 
       9 27 300 ; 
       9 23 300 ; 
       9 28 300 ; 
       9 29 300 ; 
       10 16 300 ; 
       10 17 300 ; 
       10 18 300 ; 
       10 19 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
       14 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       14 4 300 ; 
       15 13 300 ; 
       15 14 300 ; 
       15 15 300 ; 
       20 21 300 ; 
       21 20 300 ; 
       22 0 300 ; 
       23 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 12 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       32 24 401 ; 
       33 23 401 ; 
       23 16 401 ; 
       24 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       34 26 401 ; 
       27 19 401 ; 
       35 22 401 ; 
       36 25 401 ; 
       28 17 401 ; 
       29 18 401 ; 
       30 20 401 ; 
       31 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 -0.6346477 MPRFLG 0 ; 
       2 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 MPRFLG 0 ; 
       7 SCHEM 25 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 MPRFLG 0 ; 
       13 SCHEM 5 -4 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 15 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 20 -4 0 MPRFLG 0 ; 
       24 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 58.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 63 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 54 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 55.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 57 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 60 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 61.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 62 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 53 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 54.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 57.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 59 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 60.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 57 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
