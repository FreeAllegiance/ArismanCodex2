SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       nulls-bom05.38-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.50-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_gun-dureth_bomber_sPT-port_red-left.1-0.1-0.3-0 ; 
       add_gun-mat10.3-0 ; 
       add_gun-mat11.3-0 ; 
       add_gun-mat12.3-0 ; 
       add_gun-mat13.3-0 ; 
       add_gun-mat18.3-0 ; 
       add_gun-mat19.3-0 ; 
       add_gun-mat20.3-0 ; 
       add_gun-mat21.3-0 ; 
       add_gun-mat22.3-0 ; 
       add_gun-mat23.3-0 ; 
       add_gun-mat24.3-0 ; 
       add_gun-mat25.3-0 ; 
       add_gun-mat26.3-0 ; 
       add_gun-mat27.3-0 ; 
       add_gun-mat28.3-0 ; 
       add_gun-mat38.3-0 ; 
       add_gun-mat39.3-0 ; 
       add_gun-mat40.3-0 ; 
       add_gun-mat41.3-0 ; 
       add_gun-mat62.3-0 ; 
       add_gun-mat63.3-0 ; 
       add_gun-starbord_green-right.1-0.3-0 ; 
       edit_cargo-2.1-0 ; 
       edit_cargo-default8.1-0 ; 
       edit_cargo-mat118.1-0 ; 
       edit_cargo-mat119.1-0 ; 
       edit_cargo-mat120.1-0 ; 
       edit_cargo-mat121.1-0 ; 
       edit_cargo-mat122.1-0 ; 
       edit_cargo-mat124.1-0 ; 
       nulls-mat114.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       nulls-blthrust.1-0 ; 
       nulls-bom05.28-0 ROOT ; 
       nulls-boosters.1-0 ; 
       nulls-brthrust.1-0 ; 
       nulls-cockpt.1-0 ; 
       nulls-cyl3.1-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls-SSa.2-0 ; 
       nulls-SSf.2-0 ; 
       nulls-SSl.2-0 ; 
       nulls-SSr.2-0 ; 
       nulls-tlthrust.1-0 ; 
       nulls-trail.1-0 ; 
       nulls-trthrust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93B-edit_cargo.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 22     
       add_gun-t2d10.4-0 ; 
       add_gun-t2d11.4-0 ; 
       add_gun-t2d15.4-0 ; 
       add_gun-t2d16.4-0 ; 
       add_gun-t2d17.4-0 ; 
       add_gun-t2d18.4-0 ; 
       add_gun-t2d19.4-0 ; 
       add_gun-t2d20.4-0 ; 
       add_gun-t2d21.4-0 ; 
       add_gun-t2d28.4-0 ; 
       add_gun-t2d29.4-0 ; 
       add_gun-t2d30.4-0 ; 
       add_gun-t2d9.4-0 ; 
       edit_cargo-t2d121.2-0 ; 
       edit_cargo-t2d122.2-0 ; 
       edit_cargo-t2d123.2-0 ; 
       edit_cargo-t2d125.2-0 ; 
       edit_cargo-t2d126.2-0 ; 
       edit_cargo-t2d127.2-0 ; 
       edit_cargo-t2d128.2-0 ; 
       edit_cargo-t2d130.1-0 ; 
       nulls-t2d111.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       2 8 110 ; 
       3 2 110 ; 
       4 8 110 ; 
       5 1 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 6 110 ; 
       11 9 110 ; 
       12 8 110 ; 
       13 6 110 ; 
       14 9 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 2 110 ; 
       20 8 110 ; 
       21 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 30 300 ; 
       7 31 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       8 23 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
       9 19 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       13 1 300 ; 
       13 2 300 ; 
       13 3 300 ; 
       13 4 300 ; 
       14 13 300 ; 
       14 14 300 ; 
       14 15 300 ; 
       15 21 300 ; 
       16 20 300 ; 
       17 0 300 ; 
       18 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 12 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       23 16 401 ; 
       24 13 401 ; 
       25 14 401 ; 
       26 15 401 ; 
       27 19 401 ; 
       28 17 401 ; 
       29 18 401 ; 
       30 20 401 ; 
       31 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 71.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 110 -2 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       12 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 58.75 -6 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 35 -4 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 40 -4 0 MPRFLG 0 ; 
       19 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 83.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 88 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 80.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 85 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 86.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 87 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 78 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 84 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 85.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 82 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 111.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
