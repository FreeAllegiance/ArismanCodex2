SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.59-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_gun-mat10.4-0 ; 
       add_gun-mat11.4-0 ; 
       add_gun-mat12.4-0 ; 
       add_gun-mat13.4-0 ; 
       add_gun-mat18.4-0 ; 
       add_gun-mat19.4-0 ; 
       add_gun-mat20.4-0 ; 
       add_gun-mat21.4-0 ; 
       add_gun-mat22.4-0 ; 
       add_gun-mat23.4-0 ; 
       add_gun-mat24.4-0 ; 
       add_gun-mat25.4-0 ; 
       add_gun-mat26.4-0 ; 
       add_gun-mat27.4-0 ; 
       add_gun-mat28.4-0 ; 
       add_gun-mat38.4-0 ; 
       add_gun-mat39.4-0 ; 
       add_gun-mat40.4-0 ; 
       add_gun-mat41.4-0 ; 
       edit_cargo-2.2-0 ; 
       edit_cargo-default8.2-0 ; 
       edit_cargo-mat118.2-0 ; 
       edit_cargo-mat119.2-0 ; 
       edit_cargo-mat120.2-0 ; 
       edit_cargo-mat121.2-0 ; 
       edit_cargo-mat122.2-0 ; 
       nulls-mat114.4-0 ; 
       scale-back1.1-0 ; 
       scale-front1.1-0 ; 
       scale-mat123.1-0 ; 
       scale-sides.1-0 ; 
       scale-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       nulls-bom05_2.4-0 ROOT ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl93/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl93B-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_gun-t2d10.5-0 ; 
       add_gun-t2d11.5-0 ; 
       add_gun-t2d15.5-0 ; 
       add_gun-t2d16.5-0 ; 
       add_gun-t2d17.5-0 ; 
       add_gun-t2d18.5-0 ; 
       add_gun-t2d19.5-0 ; 
       add_gun-t2d20.5-0 ; 
       add_gun-t2d21.5-0 ; 
       add_gun-t2d28.5-0 ; 
       add_gun-t2d29.5-0 ; 
       add_gun-t2d30.5-0 ; 
       add_gun-t2d9.5-0 ; 
       edit_cargo-t2d121.3-0 ; 
       edit_cargo-t2d122.3-0 ; 
       edit_cargo-t2d123.3-0 ; 
       edit_cargo-t2d125.3-0 ; 
       edit_cargo-t2d126.3-0 ; 
       edit_cargo-t2d127.3-0 ; 
       edit_cargo-t2d128.3-0 ; 
       nulls-t2d111.5-0 ; 
       scale-t2d114.1-0 ; 
       scale-t2d115.1-0 ; 
       scale-t2d116.1-0 ; 
       scale-t2d117.1-0 ; 
       scale-t2d129.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 5 110 ; 
       8 4 110 ; 
       9 2 110 ; 
       10 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 30 300 ; 
       1 28 300 ; 
       1 27 300 ; 
       1 31 300 ; 
       1 29 300 ; 
       3 26 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 19 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 12 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 16 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 19 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 20 401 ; 
       27 23 401 ; 
       28 22 401 ; 
       29 25 401 ; 
       30 21 401 ; 
       31 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 -0.6346477 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 46 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 41.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 43 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 47.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 49 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 40.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 42 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 45 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 46.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 48 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
