SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.1-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       acs02_and_acs_64-mat1.1-0 ; 
       acs02_and_acs_64-mat2.1-0 ; 
       acs02_and_acs_64-mat3.1-0 ; 
       acs02_and_acs_64-mat4.1-0 ; 
       acs02_and_acs_64-mat5.1-0 ; 
       acs02_and_acs_64-mat6.1-0 ; 
       acs02_and_acs_64-mat7.1-0 ; 
       mdRIXtur_F-mat53_1.1-0 ; 
       mdRIXtur_F-mat54.1-0 ; 
       mdRIXtur_F-mat55.1-0 ; 
       mdRIXtur_F-mat56.1-0 ; 
       mdRIXtur_F-mat57.1-0 ; 
       mdRIXtur_F-mat58.1-0 ; 
       mdRIXtur_F-mat59.1-0 ; 
       mdRIXtur_F-mat60.1-0 ; 
       mdRIXtur_F-mat61.1-0 ; 
       mdRIXtur_F-mat62.1-0 ; 
       mdRIXtur_F-mat63.1-0 ; 
       mdRIXtur_F-mat64.1-0 ; 
       mdRIXtur_F-mat65.1-0 ; 
       mdRIXtur_F-mat66.1-0 ; 
       mdRIXtur_F-mat67.1-0 ; 
       mdRIXtur_F-mat68.1-0 ; 
       mdRIXtur_F-mat69.1-0 ; 
       mdRIXtur_F-mat70.1-0 ; 
       mdRIXtur_F-mat71.1-0 ; 
       mdRIXtur_F-mat72.1-0 ; 
       mdRIXtur_F-mat73.1-0 ; 
       mdRIXtur_F-mat74.1-0 ; 
       mdRIXtur_F-mat75.1-0 ; 
       mdRIXtur_F-mat76.1-0 ; 
       mdRIXtur_F-mat77.1-0 ; 
       mdRIXtur_F-mat78.1-0 ; 
       mdRIXtur_F-mat79.1-0 ; 
       mdRIXtur_F-mat80.1-0 ; 
       mdRIXtur_F-mat81.1-0 ; 
       mdRIXtur_F-mat82.1-0 ; 
       mdRIXtur_F-mat83.1-0 ; 
       mdRIXtur_F-mat84.1-0 ; 
       mdRIXtur_F-mat85.1-0 ; 
       mdRIXtur_F-mat86.1-0 ; 
       mdRIXtur_F-mat87.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       acs02-acs02.1-0 ROOT ; 
       acs02-fuselg.1-0 ; 
       acs02-lwepbas.1-0 ; 
       acs02-lwepmnt.1-0 ; 
       acs02-rwepbas.1-0 ; 
       acs02-rwepmnt.1-0 ; 
       acs63-acs63.1-0 ROOT ; 
       acs63-antenn1.1-0 ; 
       acs63-antenn2.1-0 ; 
       acs63-antenn3.1-0 ; 
       acs63-lwepbas.1-0 ; 
       acs63-lwepmnt.1-0 ; 
       acs63-lwingzz.1-0 ; 
       acs63-rwepbas.1-0 ; 
       acs63-rwepmnt.1-0 ; 
       acs63-rwingzz.1-0 ; 
       acs63-turret1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/ships/Utility/utl94/PICTURES/acs02 ; 
       D:/Pete_Data/Softimage/ships/Utility/utl94/PICTURES/acs65 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl94-acs02_and_acs_64.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 37     
       acs02_and_acs_64-t2d1.1-0 ; 
       acs02_and_acs_64-t2d2.1-0 ; 
       acs02_and_acs_64-t2d3.1-0 ; 
       acs02_and_acs_64-t2d4.1-0 ; 
       acs02_and_acs_64-t2d5.1-0 ; 
       acs02_and_acs_64-t2d6.1-0 ; 
       mdRIXtur_F-t2d1.1-0 ; 
       mdRIXtur_F-t2d10.1-0 ; 
       mdRIXtur_F-t2d11.1-0 ; 
       mdRIXtur_F-t2d12.1-0 ; 
       mdRIXtur_F-t2d13.1-0 ; 
       mdRIXtur_F-t2d14.1-0 ; 
       mdRIXtur_F-t2d15.1-0 ; 
       mdRIXtur_F-t2d16.1-0 ; 
       mdRIXtur_F-t2d17.1-0 ; 
       mdRIXtur_F-t2d18.1-0 ; 
       mdRIXtur_F-t2d19.1-0 ; 
       mdRIXtur_F-t2d2.1-0 ; 
       mdRIXtur_F-t2d20.1-0 ; 
       mdRIXtur_F-t2d21.1-0 ; 
       mdRIXtur_F-t2d22.1-0 ; 
       mdRIXtur_F-t2d23.1-0 ; 
       mdRIXtur_F-t2d24.1-0 ; 
       mdRIXtur_F-t2d25.1-0 ; 
       mdRIXtur_F-t2d26.1-0 ; 
       mdRIXtur_F-t2d27.1-0 ; 
       mdRIXtur_F-t2d28.1-0 ; 
       mdRIXtur_F-t2d29.1-0 ; 
       mdRIXtur_F-t2d3.1-0 ; 
       mdRIXtur_F-t2d30.1-0 ; 
       mdRIXtur_F-t2d31.1-0 ; 
       mdRIXtur_F-t2d4.1-0 ; 
       mdRIXtur_F-t2d5.1-0 ; 
       mdRIXtur_F-t2d6.1-0 ; 
       mdRIXtur_F-t2d7.1-0 ; 
       mdRIXtur_F-t2d8.1-0 ; 
       mdRIXtur_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 4 110 ; 
       7 12 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 16 110 ; 
       11 10 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 13 110 ; 
       15 16 110 ; 
       16 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       2 0 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       4 0 300 ; 
       4 2 300 ; 
       4 6 300 ; 
       6 8 300 ; 
       7 7 300 ; 
       7 28 300 ; 
       7 29 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 39 300 ; 
       8 7 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       8 35 300 ; 
       9 7 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       9 40 300 ; 
       10 7 300 ; 
       10 14 300 ; 
       10 15 300 ; 
       10 16 300 ; 
       12 7 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       12 22 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       13 7 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       13 17 300 ; 
       15 7 300 ; 
       15 18 300 ; 
       15 19 300 ; 
       15 25 300 ; 
       15 26 300 ; 
       15 27 300 ; 
       15 41 300 ; 
       16 7 300 ; 
       16 9 300 ; 
       16 10 300 ; 
       16 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       9 6 401 ; 
       10 17 401 ; 
       11 28 401 ; 
       12 31 401 ; 
       13 32 401 ; 
       14 35 401 ; 
       15 36 401 ; 
       16 34 401 ; 
       17 33 401 ; 
       18 8 401 ; 
       19 9 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 12 401 ; 
       26 10 401 ; 
       27 7 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 18 401 ; 
       33 19 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 29 401 ; 
       37 30 401 ; 
       39 26 401 ; 
       40 27 401 ; 
       41 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.08056 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 16.08056 -2 0 MPRFLG 0 ; 
       2 SCHEM 14.83056 -4 0 MPRFLG 0 ; 
       3 SCHEM 14.83056 -6 0 USR MPRFLG 0 ; 
       4 SCHEM 17.33056 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.33056 -6 0 USR MPRFLG 0 ; 
       6 SCHEM 8.75 0 0 SRT 1.05 1.05 1.05 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 12.33056 -4 0 USR MPRFLG 0 ; 
       11 SCHEM 12.33056 -6 0 USR MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 9.830559 -4 0 USR MPRFLG 0 ; 
       14 SCHEM 9.830559 -6 0 USR MPRFLG 0 ; 
       15 SCHEM 5 -4 0 MPRFLG 0 ; 
       16 SCHEM 8.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 18.83056 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 18.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 18.83056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 18.83056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13.83056 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 13.83056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 13.83056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 13.83056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.33056 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 18.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 18.83056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 18.83056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 13.83056 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 11.33056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 13.83056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 13.83056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 13.83056 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 0 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
