SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4.43-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.43-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.43-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.43-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.37-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.8-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.13-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       STATIC-bottom1.1-0 ; 
       STATIC-cockpit_area1.1-0 ; 
       STATIC-default1.1-0 ; 
       STATIC-default2.1-0 ; 
       STATIC-front1.1-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat2.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat5.1-0 ; 
       STATIC-mat6.1-0 ; 
       STATIC-mat7.1-0 ; 
       STATIC-nose1.1-0 ; 
       STATIC-OTHERS-rubber.1-1.1-0 ; 
       STATIC-OTHERS-rubber.1-2.1-0 ; 
       STATIC-side1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       STATIC-bmerge1.1-0 ROOT ; 
       STATIC-cargo_tank.2-0 ; 
       STATIC-cargo_tank1.1-0 ; 
       STATIC-cone1.1-0 ; 
       STATIC-face2.1-0 ; 
       STATIC-face4.1-0 ; 
       STATIC-poly_arm.1-0 ; 
       STATIC-poly_arm2.1-0 ; 
       STATIC-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl95/PICTURES/utl95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d4.1-0 ; 
       STATIC-t2d5.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 6 110 ; 
       2 1 110 ; 
       6 0 110 ; 
       1 0 110 ; 
       8 0 110 ; 
       3 0 110 ; 
       7 0 110 ; 
       5 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 0 300 ; 
       0 4 300 ; 
       0 11 300 ; 
       0 1 300 ; 
       4 6 300 ; 
       2 3 300 ; 
       2 10 300 ; 
       6 12 300 ; 
       1 2 300 ; 
       1 9 300 ; 
       8 5 300 ; 
       3 7 300 ; 
       7 13 300 ; 
       5 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       14 0 401 ; 
       0 9 401 ; 
       4 1 401 ; 
       11 13 401 ; 
       12 11 401 ; 
       6 12 401 ; 
       13 3 401 ; 
       8 4 401 ; 
       1 14 401 ; 
       7 2 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       9 6 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0.0001688004 -0.405798 -2.882868 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       14 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
