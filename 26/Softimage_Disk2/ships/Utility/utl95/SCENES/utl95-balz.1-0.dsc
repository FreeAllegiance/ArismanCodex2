SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.3-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.2-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       balz-ball10.1-0 ; 
       balz-ball11.1-0 ; 
       balz-ball12.1-0 ; 
       balz-ball13.1-0 ; 
       balz-ball14.1-0 ; 
       balz-ball3.1-0 ; 
       balz-ball4.1-0 ; 
       balz-ball6.1-0 ; 
       balz-ball7.1-0 ; 
       balz-ball8.1-0 ; 
       balz-ball9.1-0 ; 
       balz-meta2.1-0 ROOT ; 
       balz-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-balz.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 11 110 ; 
       7 11 110 ; 
       9 11 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       10 11 110 ; 
       0 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 37 0 0 MPRFLG 0 ; 
       7 SCHEM 34.5 0 0 MPRFLG 0 ; 
       9 SCHEM 39.5 0 0 MPRFLG 0 ; 
       11 SCHEM 27 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 29.5 0 0 MPRFLG 0 ; 
       6 SCHEM 32 0 0 MPRFLG 0 ; 
       10 SCHEM 42 0 0 MPRFLG 0 ; 
       0 SCHEM 44.5 0 0 MPRFLG 0 ; 
       1 SCHEM 47 0 0 MPRFLG 0 ; 
       2 SCHEM 49.5 0 0 MPRFLG 0 ; 
       3 SCHEM 52 0 0 MPRFLG 0 ; 
       4 SCHEM 54.5 0 0 DISPLAY 1 2 MPRFLG 0 ; 
       12 SCHEM 24.5 0 0 SRT 1 1 1 0 0 0 0 -1.877526 -15.42253 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
