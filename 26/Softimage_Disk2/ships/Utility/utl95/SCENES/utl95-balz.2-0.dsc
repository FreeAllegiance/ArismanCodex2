SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 3     
       utann_heavy_fighter_land-utann_hvy_fighter_4.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.4-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.3-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       balz-ball10.1-0 ; 
       balz-ball11.1-0 ; 
       balz-ball12.1-0 ; 
       balz-ball13.1-0 ; 
       balz-ball14.1-0 ; 
       balz-ball15.1-0 ; 
       balz-ball16.1-0 ; 
       balz-ball17.1-0 ; 
       balz-ball3.1-0 ; 
       balz-ball4.1-0 ; 
       balz-ball6.1-0 ; 
       balz-ball7.1-0 ; 
       balz-ball8.1-0 ; 
       balz-ball9.1-0 ; 
       balz-meta2.2-0 ROOT ; 
       balz-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-balz.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 14 110 ; 
       10 14 110 ; 
       12 14 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       13 14 110 ; 
       0 14 110 ; 
       1 14 110 ; 
       2 14 110 ; 
       3 14 110 ; 
       4 14 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 14 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 37 0 0 MPRFLG 0 ; 
       10 SCHEM 34.5 0 0 MPRFLG 0 ; 
       12 SCHEM 39.5 0 0 MPRFLG 0 ; 
       14 SCHEM 27 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 29.5 0 0 MPRFLG 0 ; 
       9 SCHEM 32 0 0 MPRFLG 0 ; 
       13 SCHEM 42 0 0 MPRFLG 0 ; 
       0 SCHEM 44.5 0 0 MPRFLG 0 ; 
       1 SCHEM 47 0 0 MPRFLG 0 ; 
       2 SCHEM 49.5 0 0 MPRFLG 0 ; 
       3 SCHEM 52 0 0 MPRFLG 0 ; 
       4 SCHEM 54.5 0 0 MPRFLG 0 ; 
       5 SCHEM 57 0 0 MPRFLG 0 ; 
       6 SCHEM 59.5 0 0 MPRFLG 0 ; 
       7 SCHEM 62 0 0 MPRFLG 0 ; 
       15 SCHEM 24.5 0 0 SRT 1 1 1 0 0 0 0 -1.877526 -15.42253 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 640.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
