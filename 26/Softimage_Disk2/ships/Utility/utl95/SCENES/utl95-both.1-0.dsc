SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 18     
       utann_heavy_fighter_land-utann_hvy_fighter_4.47-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.47-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.47-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.47-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_5.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_11.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5_1.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_17.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.1-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.12-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.17-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       both-bottom1.1-0 ; 
       both-cockpit_area1.1-0 ; 
       both-default1.1-0 ; 
       both-default2.1-0 ; 
       both-front1.1-0 ; 
       both-mat1.1-0 ; 
       both-mat10.1-0 ; 
       both-mat11.1-0 ; 
       both-mat12.1-0 ; 
       both-mat13.1-0 ; 
       both-mat14.1-0 ; 
       both-mat15.1-0 ; 
       both-mat2.1-0 ; 
       both-mat4.1-0 ; 
       both-mat5.1-0 ; 
       both-mat6.1-0 ; 
       both-mat7.1-0 ; 
       both-mat8.1-0 ; 
       both-mat9.1-0 ; 
       both-nose1.1-0 ; 
       both-OTHERS-rubber.1-1.1-0 ; 
       both-OTHERS-rubber.1-2.1-0 ; 
       both-side1.1-0 ; 
       STATIC-bottom1.2-0 ; 
       STATIC-cockpit_area1.2-0 ; 
       STATIC-default1.2-0 ; 
       STATIC-default2.2-0 ; 
       STATIC-front1.2-0 ; 
       STATIC-mat1.2-0 ; 
       STATIC-mat2.2-0 ; 
       STATIC-mat4.2-0 ; 
       STATIC-mat5.2-0 ; 
       STATIC-mat6.2-0 ; 
       STATIC-mat7.2-0 ; 
       STATIC-nose1.2-0 ; 
       STATIC-OTHERS-rubber.1-1.2-0 ; 
       STATIC-OTHERS-rubber.1-2.2-0 ; 
       STATIC-side1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       both-bmerge1.1-0 ROOT ; 
       both-cargo_tank.2-0 ; 
       both-cargo_tank1.1-0 ; 
       both-cone1.1-0 ; 
       both-face2.1-0 ; 
       both-face4.1-0 ; 
       both-lthrust.1-0 ; 
       both-poly_arm.1-0 ; 
       both-poly_arm2.1-0 ; 
       both-rthrust.1-0 ; 
       both-sphere1.1-0 ; 
       both-ss01.1-0 ; 
       both-ss2.1-0 ; 
       both-ss3.1-0 ; 
       both-ss4.1-0 ; 
       both-ss5.1-0 ; 
       both-trail.1-0 ; 
       STATIC-bmerge1.3-0 ROOT ; 
       STATIC-cargo_tank.2-0 ; 
       STATIC-cargo_tank1.1-0 ; 
       STATIC-cone1.1-0 ; 
       STATIC-face2.1-0 ; 
       STATIC-face4.1-0 ; 
       STATIC-poly_arm.1-0 ; 
       STATIC-poly_arm2.1-0 ; 
       STATIC-sphere1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/ships/Utility/utl95/PICTURES/utl95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-both.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       both-t2d1.1-0 ; 
       both-t2d10.1-0 ; 
       both-t2d11.1-0 ; 
       both-t2d12.1-0 ; 
       both-t2d13.1-0 ; 
       both-t2d14.1-0 ; 
       both-t2d15.1-0 ; 
       both-t2d16.1-0 ; 
       both-t2d17.1-0 ; 
       both-t2d2.1-0 ; 
       both-t2d3.1-0 ; 
       both-t2d4.1-0 ; 
       both-t2d5.1-0 ; 
       both-t2d8.1-0 ; 
       both-t2d9.1-0 ; 
       STATIC-t2d1.3-0 ; 
       STATIC-t2d10.3-0 ; 
       STATIC-t2d11.2-0 ; 
       STATIC-t2d12.2-0 ; 
       STATIC-t2d13.2-0 ; 
       STATIC-t2d14.2-0 ; 
       STATIC-t2d15.2-0 ; 
       STATIC-t2d16.2-0 ; 
       STATIC-t2d17.2-0 ; 
       STATIC-t2d2.3-0 ; 
       STATIC-t2d3.2-0 ; 
       STATIC-t2d4.2-0 ; 
       STATIC-t2d5.2-0 ; 
       STATIC-t2d8.3-0 ; 
       STATIC-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 7 110 ; 
       5 8 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 17 110 ; 
       21 23 110 ; 
       22 24 110 ; 
       23 17 110 ; 
       24 17 110 ; 
       25 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 22 300 ; 
       0 0 300 ; 
       0 4 300 ; 
       0 19 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       1 15 300 ; 
       2 3 300 ; 
       2 16 300 ; 
       3 13 300 ; 
       4 12 300 ; 
       5 14 300 ; 
       6 9 300 ; 
       7 20 300 ; 
       8 21 300 ; 
       9 10 300 ; 
       10 5 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       13 6 300 ; 
       14 7 300 ; 
       15 8 300 ; 
       16 11 300 ; 
       17 37 300 ; 
       17 23 300 ; 
       17 27 300 ; 
       17 34 300 ; 
       17 24 300 ; 
       18 25 300 ; 
       18 32 300 ; 
       19 26 300 ; 
       19 33 300 ; 
       20 30 300 ; 
       21 29 300 ; 
       22 31 300 ; 
       23 35 300 ; 
       24 36 300 ; 
       25 28 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 10 400 ; 
       25 25 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 14 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       4 1 401 ; 
       12 12 401 ; 
       13 2 401 ; 
       14 4 401 ; 
       15 6 401 ; 
       16 8 401 ; 
       19 13 401 ; 
       20 11 401 ; 
       21 3 401 ; 
       22 0 401 ; 
       23 24 401 ; 
       24 29 401 ; 
       25 20 401 ; 
       26 22 401 ; 
       27 16 401 ; 
       29 27 401 ; 
       30 17 401 ; 
       31 19 401 ; 
       32 21 401 ; 
       33 23 401 ; 
       34 28 401 ; 
       35 26 401 ; 
       36 18 401 ; 
       37 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       11 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 37 -2 0 MPRFLG 0 ; 
       19 SCHEM 37 -4 0 MPRFLG 0 ; 
       20 SCHEM 44.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 42 -4 0 MPRFLG 0 ; 
       22 SCHEM 47 -4 0 MPRFLG 0 ; 
       23 SCHEM 42 -2 0 MPRFLG 0 ; 
       24 SCHEM 47 -2 0 MPRFLG 0 ; 
       25 SCHEM 39.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 68.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 68.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 36 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 68.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 43.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 46 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 38.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 68.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 43.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 48.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 68.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 68.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 68.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 43.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 48.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 46 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 38.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 36 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 36 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 68.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 38.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 43.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 41 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 68.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 68.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 715 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 742.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 715 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 715 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 742.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 698.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 742.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 698.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 698.75 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
