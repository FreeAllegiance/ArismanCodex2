SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4.40-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.40-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.40-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.40-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.34-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.34-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.5-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.5-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.10-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 20     
       new-bottom1.1-0 ; 
       new-cockpit_area1.1-0 ; 
       new-default1.3-0 ; 
       new-default2.3-0 ; 
       new-front1.1-0 ; 
       new-mat1.1-0 ; 
       new-mat10.1-0 ; 
       new-mat11.1-0 ; 
       new-mat12.1-0 ; 
       new-mat2.1-0 ; 
       new-mat4.1-0 ; 
       new-mat5.1-0 ; 
       new-mat6.1-0 ; 
       new-mat7.1-0 ; 
       new-mat8.1-0 ; 
       new-mat9.1-0 ; 
       new-nose1.1-0 ; 
       new-OTHERS-rubber.1-1.1-0 ; 
       new-OTHERS-rubber.1-2.1-0 ; 
       new-side1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       new-bmerge1.20-0 ROOT ; 
       new-cargo_tank.2-0 ; 
       new-cargo_tank1.1-0 ; 
       new-cone1.1-0 ; 
       new-face2.1-0 ; 
       new-face4.1-0 ; 
       new-poly_arm.1-0 ; 
       new-poly_arm2.1-0 ; 
       new-sphere1.1-0 ; 
       new-ss01.1-0 ; 
       new-ss2.1-0 ; 
       new-ss3.1-0 ; 
       new-ss4.1-0 ; 
       new-ss5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl95/PICTURES/utl95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-new.25-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       new-t2d1.1-0 ; 
       new-t2d10.1-0 ; 
       new-t2d11.1-0 ; 
       new-t2d12.1-0 ; 
       new-t2d13.1-0 ; 
       new-t2d14.2-0 ; 
       new-t2d15.1-0 ; 
       new-t2d16.1-0 ; 
       new-t2d17.1-0 ; 
       new-t2d2.1-0 ; 
       new-t2d3.1-0 ; 
       new-t2d4.1-0 ; 
       new-t2d5.1-0 ; 
       new-t2d8.1-0 ; 
       new-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 6 110 ; 
       2 1 110 ; 
       6 0 110 ; 
       1 0 110 ; 
       8 0 110 ; 
       3 0 110 ; 
       7 0 110 ; 
       5 7 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 19 300 ; 
       0 0 300 ; 
       0 4 300 ; 
       0 16 300 ; 
       0 1 300 ; 
       4 9 300 ; 
       2 3 300 ; 
       2 13 300 ; 
       6 17 300 ; 
       1 2 300 ; 
       1 12 300 ; 
       8 5 300 ; 
       3 10 300 ; 
       7 18 300 ; 
       5 11 300 ; 
       9 14 300 ; 
       10 15 300 ; 
       11 6 300 ; 
       12 7 300 ; 
       13 8 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       8 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       19 0 401 ; 
       0 9 401 ; 
       4 1 401 ; 
       16 13 401 ; 
       17 11 401 ; 
       9 12 401 ; 
       18 3 401 ; 
       11 4 401 ; 
       1 14 401 ; 
       10 2 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       12 6 401 ; 
       13 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0.0001688004 -0.405798 -2.882868 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 3.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 18.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 13.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 26.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 30 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 32.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 37.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       19 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 42.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
