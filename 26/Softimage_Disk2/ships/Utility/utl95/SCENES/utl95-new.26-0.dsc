SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 9     
       utann_heavy_fighter_land-utann_hvy_fighter_4.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.41-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.35-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_2.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.35-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_5.6-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.6-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.11-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       new-bottom1.1-0 ; 
       new-cockpit_area1.1-0 ; 
       new-default1.3-0 ; 
       new-default2.3-0 ; 
       new-front1.1-0 ; 
       new-mat1.1-0 ; 
       new-mat10.1-0 ; 
       new-mat11.1-0 ; 
       new-mat12.1-0 ; 
       new-mat13.1-0 ; 
       new-mat14.1-0 ; 
       new-mat15.1-0 ; 
       new-mat2.1-0 ; 
       new-mat4.1-0 ; 
       new-mat5.1-0 ; 
       new-mat6.1-0 ; 
       new-mat7.1-0 ; 
       new-mat8.1-0 ; 
       new-mat9.1-0 ; 
       new-nose1.1-0 ; 
       new-OTHERS-rubber.1-1.1-0 ; 
       new-OTHERS-rubber.1-2.1-0 ; 
       new-side1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       new-bmerge1.21-0 ROOT ; 
       new-cargo_tank.2-0 ; 
       new-cargo_tank1.1-0 ; 
       new-cone1.1-0 ; 
       new-face2.1-0 ; 
       new-face4.1-0 ; 
       new-lthrust.1-0 ; 
       new-poly_arm.1-0 ; 
       new-poly_arm2.1-0 ; 
       new-rthrust.1-0 ; 
       new-sphere1.1-0 ; 
       new-ss01.1-0 ; 
       new-ss2.1-0 ; 
       new-ss3.1-0 ; 
       new-ss4.1-0 ; 
       new-ss5.1-0 ; 
       new-trail.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/ships/Utility/utl95/PICTURES/utl95 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl95-new.26-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       new-t2d1.1-0 ; 
       new-t2d10.1-0 ; 
       new-t2d11.1-0 ; 
       new-t2d12.1-0 ; 
       new-t2d13.1-0 ; 
       new-t2d14.2-0 ; 
       new-t2d15.1-0 ; 
       new-t2d16.1-0 ; 
       new-t2d17.1-0 ; 
       new-t2d2.1-0 ; 
       new-t2d3.1-0 ; 
       new-t2d4.1-0 ; 
       new-t2d5.1-0 ; 
       new-t2d8.1-0 ; 
       new-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 7 110 ; 
       2 1 110 ; 
       7 0 110 ; 
       1 0 110 ; 
       10 0 110 ; 
       3 0 110 ; 
       8 0 110 ; 
       5 8 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       6 0 110 ; 
       15 0 110 ; 
       9 0 110 ; 
       16 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 22 300 ; 
       0 0 300 ; 
       0 4 300 ; 
       0 19 300 ; 
       0 1 300 ; 
       4 12 300 ; 
       2 3 300 ; 
       2 16 300 ; 
       7 20 300 ; 
       1 2 300 ; 
       1 15 300 ; 
       10 5 300 ; 
       3 13 300 ; 
       8 21 300 ; 
       5 14 300 ; 
       11 17 300 ; 
       12 18 300 ; 
       13 6 300 ; 
       14 7 300 ; 
       6 9 300 ; 
       15 8 300 ; 
       9 10 300 ; 
       16 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       10 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       22 0 401 ; 
       0 9 401 ; 
       4 1 401 ; 
       19 13 401 ; 
       20 11 401 ; 
       12 12 401 ; 
       21 3 401 ; 
       14 4 401 ; 
       1 14 401 ; 
       13 2 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       15 6 401 ; 
       16 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0.0001688004 -0.405798 -2.882868 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 20 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       14 SCHEM 22.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       22 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       1 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       0 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 680.5 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 708 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 664.25 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
