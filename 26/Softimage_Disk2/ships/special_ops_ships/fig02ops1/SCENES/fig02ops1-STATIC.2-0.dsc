SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.2-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
       ops_fig02-mat46.2-0 ; 
       ops_fig02-mat47.2-0 ; 
       ops_fig02-mat63.2-0 ; 
       ops_fig02-mat64.2-0 ; 
       ops_fig02-mat65.2-0 ; 
       ops_fig02-mat66.2-0 ; 
       ops_fig02-mat69.2-0 ; 
       ops_fig02-mat70.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       ops_fig02-afuselg.1-0 ; 
       ops_fig02-ffuselg.1-0 ; 
       ops_fig02-fig02.10-0 ROOT ; 
       ops_fig02-finzzz0.1-0 ; 
       ops_fig02-finzzz1.1-0 ; 
       ops_fig02-fwepmnt.1-0 ; 
       ops_fig02-l-gun.1-0 ; 
       ops_fig02-lbooster.1-0 ; 
       ops_fig02-lfinzzz.1-0 ; 
       ops_fig02-lwepatt.1-0 ; 
       ops_fig02-lwingzz0.1-0 ; 
       ops_fig02-lwingzz1.1-0 ; 
       ops_fig02-lwingzz2.1-0 ; 
       ops_fig02-lwingzz3.1-0 ; 
       ops_fig02-lwingzz4.1-0 ; 
       ops_fig02-r-gun.1-0 ; 
       ops_fig02-rbooster.1-0 ; 
       ops_fig02-rfinzzz.1-0 ; 
       ops_fig02-rwepatt.1-0 ; 
       ops_fig02-rwingzz0.1-0 ; 
       ops_fig02-rwingzz1.1-0 ; 
       ops_fig02-rwingzz2.1-0 ; 
       ops_fig02-rwingzz3.1-0 ; 
       ops_fig02-rwingzz4.1-0 ; 
       ops_fig02-SSal.1-0 ; 
       ops_fig02-SSar.1-0 ; 
       ops_fig02-SSf.1-0 ; 
       ops_fig02-SSr.1-0 ; 
       ops_fig02-SSr1.1-0 ; 
       ops_fig02-thrust0.1-0 ; 
       ops_fig02-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig02ops1/PICTURES/fig02ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02ops1-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       destroy-2d1_1.5-0 ; 
       destroy-t2d1_1.5-0 ; 
       destroy-t2d10_1.5-0 ; 
       destroy-t2d11_1.5-0 ; 
       destroy-t2d12_1.5-0 ; 
       destroy-t2d13_1.4-0 ; 
       destroy-t2d14_1.4-0 ; 
       destroy-t2d15_1.5-0 ; 
       destroy-t2d2_1.5-0 ; 
       destroy-t2d3_1.5-0 ; 
       destroy-t2d30_1.4-0 ; 
       destroy-t2d31_1.5-0 ; 
       destroy-t2d32_1.5-0 ; 
       destroy-t2d33_1.4-0 ; 
       destroy-t2d34_1.4-0 ; 
       destroy-t2d35_1.4-0 ; 
       destroy-t2d36_1.4-0 ; 
       destroy-t2d37_1.4-0 ; 
       destroy-t2d38_1.4-0 ; 
       destroy-t2d39_1.4-0 ; 
       destroy-t2d4_1.5-0 ; 
       destroy-t2d40_1.4-0 ; 
       destroy-t2d5_1.5-0 ; 
       destroy-t2d6_1.5-0 ; 
       destroy-t2d7_1.5-0 ; 
       destroy-t2d8_1.5-0 ; 
       destroy-t2d9_1_1.5-0 ; 
       ops_fig02-t2d42.4-0 ; 
       ops_fig02-t2d43.5-0 ; 
       ops_fig02-t2d44.4-0 ; 
       ops_fig02-t2d45.4-0 ; 
       ops_fig02-t2d85.4-0 ; 
       ops_fig02-t2d86.4-0 ; 
       ops_fig02-zt2d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 29 110 ; 
       8 3 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 10 110 ; 
       12 11 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 1 110 ; 
       16 29 110 ; 
       17 3 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 19 110 ; 
       21 20 110 ; 
       22 21 110 ; 
       23 21 110 ; 
       24 8 110 ; 
       25 17 110 ; 
       26 1 110 ; 
       27 21 110 ; 
       28 12 110 ; 
       29 2 110 ; 
       30 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       4 20 300 ; 
       6 38 300 ; 
       7 3 300 ; 
       7 22 300 ; 
       7 32 300 ; 
       8 5 300 ; 
       8 35 300 ; 
       12 23 300 ; 
       12 24 300 ; 
       12 26 300 ; 
       13 28 300 ; 
       14 27 300 ; 
       15 39 300 ; 
       16 4 300 ; 
       16 21 300 ; 
       16 33 300 ; 
       17 6 300 ; 
       17 36 300 ; 
       21 0 300 ; 
       21 25 300 ; 
       22 1 300 ; 
       23 2 300 ; 
       24 31 300 ; 
       25 30 300 ; 
       26 29 300 ; 
       27 34 300 ; 
       28 37 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 26 400 ; 
       1 1 400 ; 
       4 7 400 ; 
       7 10 400 ; 
       8 5 400 ; 
       13 15 400 ; 
       14 14 400 ; 
       16 11 400 ; 
       17 6 400 ; 
       22 16 400 ; 
       23 17 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 0 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 18 401 ; 
       25 21 401 ; 
       26 29 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 33 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 35 -4 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 30 -2 0 MPRFLG 0 ; 
       11 SCHEM 30 -4 0 MPRFLG 0 ; 
       12 SCHEM 30 -6 0 MPRFLG 0 ; 
       13 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 30 -8 0 MPRFLG 0 ; 
       15 SCHEM 50 -4 0 MPRFLG 0 ; 
       16 SCHEM 10 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 5 -2 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 20 -8 0 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 15 -6 0 MPRFLG 0 ; 
       25 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 40 -4 0 MPRFLG 0 ; 
       27 SCHEM 25 -8 0 MPRFLG 0 ; 
       28 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 35.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 50.25 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 54 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 29 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 54 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 41.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 35.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 50.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
