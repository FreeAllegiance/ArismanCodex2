SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig02ops1-cam_int1.1-0 ROOT ; 
       fig02ops1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
       ops_fig02-mat46.2-0 ; 
       ops_fig02-mat47.2-0 ; 
       ops_fig02-mat63.2-0 ; 
       ops_fig02-mat64.2-0 ; 
       ops_fig02-mat65.2-0 ; 
       ops_fig02-mat66.2-0 ; 
       ops_fig02-mat69.2-0 ; 
       ops_fig02-mat70.2-0 ; 
       ops_fig02-mat71.2-0 ; 
       ops_fig02-mat72.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       ops_fig02-afuselg.1-0 ; 
       ops_fig02-cockpt.1-0 ; 
       ops_fig02-ffuselg.1-0 ; 
       ops_fig02-fgun.1-0 ; 
       ops_fig02-fig02.9-0 ROOT ; 
       ops_fig02-finzzz0.1-0 ; 
       ops_fig02-finzzz1.1-0 ; 
       ops_fig02-fwepemt.1-0 ; 
       ops_fig02-fwepmnt.1-0 ; 
       ops_fig02-l-gun.1-0 ; 
       ops_fig02-lbooster.1-0 ; 
       ops_fig02-lfinzzz.1-0 ; 
       ops_fig02-lsmoke.1-0 ; 
       ops_fig02-lthrust.1-0 ; 
       ops_fig02-lwepatt.1-0 ; 
       ops_fig02-lwepemt.1-0 ; 
       ops_fig02-lwingzz0.1-0 ; 
       ops_fig02-lwingzz1.1-0 ; 
       ops_fig02-lwingzz2.1-0 ; 
       ops_fig02-lwingzz3.1-0 ; 
       ops_fig02-lwingzz4.1-0 ; 
       ops_fig02-missemt.1-0 ; 
       ops_fig02-r-gun.1-0 ; 
       ops_fig02-rbooster.1-0 ; 
       ops_fig02-rfinzzz.1-0 ; 
       ops_fig02-rsmoke.1-0 ; 
       ops_fig02-rthrust.1-0 ; 
       ops_fig02-rwepatt.1-0 ; 
       ops_fig02-rwepemt.1-0 ; 
       ops_fig02-rwingzz0.1-0 ; 
       ops_fig02-rwingzz1.1-0 ; 
       ops_fig02-rwingzz2.1-0 ; 
       ops_fig02-rwingzz3.1-0 ; 
       ops_fig02-rwingzz4.1-0 ; 
       ops_fig02-SSal.1-0 ; 
       ops_fig02-SSar.1-0 ; 
       ops_fig02-SSf.1-0 ; 
       ops_fig02-SSr.1-0 ; 
       ops_fig02-SSr1.1-0 ; 
       ops_fig02-tgun.1-0 ; 
       ops_fig02-thrust0.1-0 ; 
       ops_fig02-trail.1-0 ; 
       ops_fig02-twepemt.1-0 ; 
       ops_fig02-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig02ops1/PICTURES/fig02ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig02ops1-fig02ops1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       destroy-2d1_1.5-0 ; 
       destroy-t2d1_1.5-0 ; 
       destroy-t2d10_1.5-0 ; 
       destroy-t2d11_1.5-0 ; 
       destroy-t2d12_1.5-0 ; 
       destroy-t2d13_1.4-0 ; 
       destroy-t2d14_1.4-0 ; 
       destroy-t2d15_1.5-0 ; 
       destroy-t2d2_1.5-0 ; 
       destroy-t2d3_1.5-0 ; 
       destroy-t2d30_1.4-0 ; 
       destroy-t2d31_1.5-0 ; 
       destroy-t2d32_1.5-0 ; 
       destroy-t2d33_1.4-0 ; 
       destroy-t2d34_1.4-0 ; 
       destroy-t2d35_1.4-0 ; 
       destroy-t2d36_1.4-0 ; 
       destroy-t2d37_1.4-0 ; 
       destroy-t2d38_1.4-0 ; 
       destroy-t2d39_1.4-0 ; 
       destroy-t2d4_1.5-0 ; 
       destroy-t2d40_1.4-0 ; 
       destroy-t2d5_1.5-0 ; 
       destroy-t2d6_1.5-0 ; 
       destroy-t2d7_1.5-0 ; 
       destroy-t2d8_1.5-0 ; 
       destroy-t2d9_1_1.5-0 ; 
       ops_fig02-t2d42.4-0 ; 
       ops_fig02-t2d43.5-0 ; 
       ops_fig02-t2d44.4-0 ; 
       ops_fig02-t2d45.4-0 ; 
       ops_fig02-t2d85.4-0 ; 
       ops_fig02-t2d86.4-0 ; 
       ops_fig02-t2d87.4-0 ; 
       ops_fig02-t2d88.4-0 ; 
       ops_fig02-zt2d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 40 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 10 110 ; 
       14 4 110 ; 
       15 9 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 4 110 ; 
       22 2 110 ; 
       23 40 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 23 110 ; 
       27 4 110 ; 
       28 22 110 ; 
       29 4 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 11 110 ; 
       35 24 110 ; 
       36 2 110 ; 
       37 31 110 ; 
       38 18 110 ; 
       39 2 110 ; 
       40 4 110 ; 
       41 4 110 ; 
       42 39 110 ; 
       43 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       3 41 300 ; 
       6 20 300 ; 
       9 38 300 ; 
       10 3 300 ; 
       10 22 300 ; 
       10 32 300 ; 
       11 5 300 ; 
       11 35 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 26 300 ; 
       19 28 300 ; 
       20 27 300 ; 
       22 39 300 ; 
       23 4 300 ; 
       23 21 300 ; 
       23 33 300 ; 
       24 6 300 ; 
       24 36 300 ; 
       31 0 300 ; 
       31 25 300 ; 
       32 1 300 ; 
       33 2 300 ; 
       34 31 300 ; 
       35 30 300 ; 
       36 29 300 ; 
       37 34 300 ; 
       38 37 300 ; 
       39 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 26 400 ; 
       2 1 400 ; 
       6 7 400 ; 
       10 10 400 ; 
       11 5 400 ; 
       19 15 400 ; 
       20 14 400 ; 
       23 11 400 ; 
       24 6 400 ; 
       32 16 400 ; 
       33 17 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 0 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 18 401 ; 
       25 21 401 ; 
       26 29 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 35 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 118.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 169.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 133.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 140.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 90.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 48.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 38.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 139.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 107 -4 0 MPRFLG 0 ; 
       9 SCHEM 103.25 -4 0 MPRFLG 0 ; 
       10 SCHEM 17 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 45.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 174.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 12 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 7 -2 0 MPRFLG 0 ; 
       15 SCHEM 102 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 88.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 88.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 88.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 80.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 85.75 -8 0 MPRFLG 0 ; 
       21 SCHEM 99.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 135.75 -4 0 MPRFLG 0 ; 
       23 SCHEM 29.5 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       24 SCHEM 55.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 172 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 134.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 69.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 69.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 63.25 -8 0 MPRFLG 0 ; 
       33 SCHEM 68.25 -8 0 MPRFLG 0 ; 
       34 SCHEM 42 -6 0 MPRFLG 0 ; 
       35 SCHEM 52 -6 0 MPRFLG 0 ; 
       36 SCHEM 109.5 -4 0 MPRFLG 0 ; 
       37 SCHEM 72 -8 0 MPRFLG 0 ; 
       38 SCHEM 89.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 128.25 -4 0 MPRFLG 0 ; 
       40 SCHEM 23.25 -2 0 MPRFLG 0 ; 
       41 SCHEM 167 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 127 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 132 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 77 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 69.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 49.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 59.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 124.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 154.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 147 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 149.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 152 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 144.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 157 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 159.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 162 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 112 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 114.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 117 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 119.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 27 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 97 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 74.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 94.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 87 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 109.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 52 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 42 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 17 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 29.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 72 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 54.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 89.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 103.5 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 136 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 129.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 142 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 119.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 164.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 112 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 114.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 117 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 47 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 57 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 37 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 147 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 149.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 19.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 32 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 27 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 84.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 67 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 92 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 77 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 152 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 74.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 144.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 157 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 159.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 162 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 122 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 17 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 29.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 94.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 54.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 103.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 136 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 129.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 142 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 44.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
