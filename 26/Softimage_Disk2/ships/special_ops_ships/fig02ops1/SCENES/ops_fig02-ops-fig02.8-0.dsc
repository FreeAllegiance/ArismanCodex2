SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ops_fig02-fig02.8-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.8-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       destroy-default5_1_1.2-0 ; 
       destroy-mat10_1.2-0 ; 
       destroy-mat11_1.2-0 ; 
       destroy-mat12_1.2-0 ; 
       destroy-mat13_1.2-0 ; 
       destroy-mat14_1.2-0 ; 
       destroy-mat15_1.2-0 ; 
       destroy-mat16_1.2-0 ; 
       destroy-mat24_1.2-0 ; 
       destroy-mat25_1.2-0 ; 
       destroy-mat26_1.2-0 ; 
       destroy-mat27_1.2-0 ; 
       destroy-mat28_1.2-0 ; 
       destroy-mat29_1.2-0 ; 
       destroy-mat30_1.2-0 ; 
       destroy-mat31_1.2-0 ; 
       destroy-mat32_1.2-0 ; 
       destroy-mat33_1.2-0 ; 
       destroy-mat34_1.2-0 ; 
       destroy-mat35_1.2-0 ; 
       destroy-mat36_1.2-0 ; 
       destroy-mat38_1.2-0 ; 
       destroy-mat39_1.2-0 ; 
       destroy-mat40_1.2-0 ; 
       destroy-mat41_1.2-0 ; 
       destroy-mat42_1.2-0 ; 
       destroy-mat43_1.2-0 ; 
       destroy-mat5_1.2-0 ; 
       destroy-mat6_1.2-0 ; 
       done_lite-.5.2-0 ; 
       done_lite-mat11.2-0 ; 
       done_lite-mat44.2-0 ; 
       ops_fig02-mat46.2-0 ; 
       ops_fig02-mat47.2-0 ; 
       ops_fig02-mat63.2-0 ; 
       ops_fig02-mat64.2-0 ; 
       ops_fig02-mat65.2-0 ; 
       ops_fig02-mat66.2-0 ; 
       ops_fig02-mat69.2-0 ; 
       ops_fig02-mat70.2-0 ; 
       ops_fig02-mat71.2-0 ; 
       ops_fig02-mat72.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       ops_fig02-afuselg.1-0 ; 
       ops_fig02-cockpt.1-0 ; 
       ops_fig02-ffuselg.1-0 ; 
       ops_fig02-fgun.1-0 ; 
       ops_fig02-fig02.7-0 ROOT ; 
       ops_fig02-finzzz0.1-0 ; 
       ops_fig02-finzzz1.1-0 ; 
       ops_fig02-fwepemt.1-0 ; 
       ops_fig02-fwepmnt.1-0 ; 
       ops_fig02-l-gun.1-0 ; 
       ops_fig02-lbooster.1-0 ; 
       ops_fig02-lfinzzz.1-0 ; 
       ops_fig02-lsmoke.1-0 ; 
       ops_fig02-lthrust.1-0 ; 
       ops_fig02-lwepatt.1-0 ; 
       ops_fig02-lwepemt.1-0 ; 
       ops_fig02-lwingzz0.1-0 ; 
       ops_fig02-lwingzz1.1-0 ; 
       ops_fig02-lwingzz2.1-0 ; 
       ops_fig02-lwingzz3.1-0 ; 
       ops_fig02-lwingzz4.1-0 ; 
       ops_fig02-missemt.1-0 ; 
       ops_fig02-r-gun.1-0 ; 
       ops_fig02-rbooster.1-0 ; 
       ops_fig02-rfinzzz.1-0 ; 
       ops_fig02-rsmoke.1-0 ; 
       ops_fig02-rthrust.1-0 ; 
       ops_fig02-rwepatt.1-0 ; 
       ops_fig02-rwepemt.1-0 ; 
       ops_fig02-rwingzz0.1-0 ; 
       ops_fig02-rwingzz1.1-0 ; 
       ops_fig02-rwingzz2.1-0 ; 
       ops_fig02-rwingzz3.1-0 ; 
       ops_fig02-rwingzz4.1-0 ; 
       ops_fig02-SSal.1-0 ; 
       ops_fig02-SSar.1-0 ; 
       ops_fig02-SSf.1-0 ; 
       ops_fig02-SSr.1-0 ; 
       ops_fig02-SSr1.1-0 ; 
       ops_fig02-tgun.1-0 ; 
       ops_fig02-thrust0.1-0 ; 
       ops_fig02-trail.1-0 ; 
       ops_fig02-twepemt.1-0 ; 
       ops_fig02-twepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/ops_fig02/PICTURES/fig02ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ops_fig02-ops-fig02.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       destroy-2d1_1.4-0 ; 
       destroy-t2d1_1.4-0 ; 
       destroy-t2d10_1.4-0 ; 
       destroy-t2d11_1.4-0 ; 
       destroy-t2d12_1.4-0 ; 
       destroy-t2d13_1.3-0 ; 
       destroy-t2d14_1.3-0 ; 
       destroy-t2d15_1.4-0 ; 
       destroy-t2d2_1.4-0 ; 
       destroy-t2d3_1.4-0 ; 
       destroy-t2d30_1.3-0 ; 
       destroy-t2d31_1.4-0 ; 
       destroy-t2d32_1.4-0 ; 
       destroy-t2d33_1.3-0 ; 
       destroy-t2d34_1.3-0 ; 
       destroy-t2d35_1.3-0 ; 
       destroy-t2d36_1.3-0 ; 
       destroy-t2d37_1.3-0 ; 
       destroy-t2d38_1.3-0 ; 
       destroy-t2d39_1.3-0 ; 
       destroy-t2d4_1.4-0 ; 
       destroy-t2d40_1.3-0 ; 
       destroy-t2d5_1.4-0 ; 
       destroy-t2d6_1.4-0 ; 
       destroy-t2d7_1.4-0 ; 
       destroy-t2d8_1.4-0 ; 
       destroy-t2d9_1_1.4-0 ; 
       ops_fig02-t2d42.3-0 ; 
       ops_fig02-t2d43.4-0 ; 
       ops_fig02-t2d44.3-0 ; 
       ops_fig02-t2d45.3-0 ; 
       ops_fig02-t2d85.3-0 ; 
       ops_fig02-t2d86.3-0 ; 
       ops_fig02-t2d87.3-0 ; 
       ops_fig02-t2d88.3-0 ; 
       ops_fig02-zt2d1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 3 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 40 110 ; 
       11 5 110 ; 
       12 4 110 ; 
       13 10 110 ; 
       14 4 110 ; 
       15 9 110 ; 
       16 4 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 18 110 ; 
       20 18 110 ; 
       21 4 110 ; 
       22 2 110 ; 
       23 40 110 ; 
       24 5 110 ; 
       25 4 110 ; 
       26 23 110 ; 
       27 4 110 ; 
       28 22 110 ; 
       29 4 110 ; 
       30 29 110 ; 
       31 30 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 11 110 ; 
       35 24 110 ; 
       36 2 110 ; 
       37 31 110 ; 
       38 18 110 ; 
       39 2 110 ; 
       40 4 110 ; 
       41 4 110 ; 
       42 39 110 ; 
       43 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 7 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       3 41 300 ; 
       6 20 300 ; 
       9 38 300 ; 
       10 3 300 ; 
       10 22 300 ; 
       10 32 300 ; 
       11 5 300 ; 
       11 35 300 ; 
       18 23 300 ; 
       18 24 300 ; 
       18 26 300 ; 
       19 28 300 ; 
       20 27 300 ; 
       22 39 300 ; 
       23 4 300 ; 
       23 21 300 ; 
       23 33 300 ; 
       24 6 300 ; 
       24 36 300 ; 
       31 0 300 ; 
       31 25 300 ; 
       32 1 300 ; 
       33 2 300 ; 
       34 31 300 ; 
       35 30 300 ; 
       36 29 300 ; 
       37 34 300 ; 
       38 37 300 ; 
       39 40 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 26 400 ; 
       2 1 400 ; 
       6 7 400 ; 
       10 10 400 ; 
       11 5 400 ; 
       19 15 400 ; 
       20 14 400 ; 
       23 11 400 ; 
       24 6 400 ; 
       32 16 400 ; 
       33 17 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 19 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 20 401 ; 
       12 22 401 ; 
       13 23 401 ; 
       14 24 401 ; 
       15 25 401 ; 
       16 2 401 ; 
       17 3 401 ; 
       18 4 401 ; 
       19 0 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       24 18 401 ; 
       25 21 401 ; 
       26 29 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       35 35 401 ; 
       36 30 401 ; 
       38 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       41 34 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 140 -4 0 MPRFLG 0 ; 
       1 SCHEM 191.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 155 -2 0 MPRFLG 0 ; 
       3 SCHEM 162.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 88.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 46.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 161.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 128.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 125 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       12 SCHEM 196.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5 -2 0 MPRFLG 0 ; 
       15 SCHEM 123.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       18 SCHEM 86.25 -6 0 MPRFLG 0 ; 
       19 SCHEM 78.75 -8 0 MPRFLG 0 ; 
       20 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       21 SCHEM 121.25 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 157.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 27.5 -4 0 WIRECOL 8 7 MPRFLG 0 ; 
       24 SCHEM 53.75 -4 0 MPRFLG 0 ; 
       25 SCHEM 193.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 156.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       32 SCHEM 61.25 -8 0 MPRFLG 0 ; 
       33 SCHEM 66.25 -8 0 MPRFLG 0 ; 
       34 SCHEM 40 -6 0 MPRFLG 0 ; 
       35 SCHEM 50 -6 0 MPRFLG 0 ; 
       36 SCHEM 131.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 70 -8 0 MPRFLG 0 ; 
       38 SCHEM 87.5 -8 0 MPRFLG 0 ; 
       39 SCHEM 150 -4 0 MPRFLG 0 ; 
       40 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       41 SCHEM 188.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 148.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 153.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 146.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 176.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 168.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 171.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 173.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 166.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 178.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 181.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 183.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 133.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 136.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 138.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 141.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 131.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 128.5697 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 161.0697 -6.03605 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 151.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 163.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 141.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 186.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 133.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 136.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 138.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 168.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 171.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 173.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 166.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 178.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 181.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 183.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 143.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 128.5697 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 161.0697 -8.03605 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 151.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 163.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 42.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 197.75 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
