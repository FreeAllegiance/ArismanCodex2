SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       STATIC-cam_int1.2-0 ROOT ; 
       STATIC-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       STATIC-light1.2-0 ROOT ; 
       STATIC-light2.2-0 ROOT ; 
       STATIC-light3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       done_lite-mat7.2-0 ; 
       durethma_fighterbike_sa-mat48.2-0 ; 
       fix_dot-mat69.2-0 ; 
       reduce-mat49.2-0 ; 
       reduce-mat50.2-0 ; 
       reduce-mat51.2-0 ; 
       reduce-mat52.2-0 ; 
       reduce-mat53.2-0 ; 
       reduce-mat54.2-0 ; 
       reduce-mat55.2-0 ; 
       reduce-mat56.2-0 ; 
       reduce-mat57.2-0 ; 
       reduce-mat58.2-0 ; 
       reduce-mat59.2-0 ; 
       reduce-mat60.2-0 ; 
       reduce-mat61.2-0 ; 
       reduce-mat62.2-0 ; 
       reduce-mat63.2-0 ; 
       reduce-mat64.2-0 ; 
       reduce-mat65.2-0 ; 
       reduce-mat66.2-0 ; 
       reduce-mat67.2-0 ; 
       reduce-mat68.2-0 ; 
       reduce-mat69.2-0 ; 
       reduce-mat70.2-0 ; 
       reduce-mat71.2-0 ; 
       reduce-mat72.2-0 ; 
       reduce-mat73.2-0 ; 
       reduce-mat74.2-0 ; 
       reduce-mat75.2-0 ; 
       reduce-mat76.2-0 ; 
       reduce-mat77.2-0 ; 
       reduce-mat78.2-0 ; 
       reduce-mat79.2-0 ; 
       reduce-mat80.2-0 ; 
       reduce-mat81.2-0 ; 
       reduce-mat82.2-0 ; 
       reduce-mat83.2-0 ; 
       reduce-mat84.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 28     
       fig05-fig05_1.9-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-l-gun.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig05ops1/PICTURES/fig05ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05ops1-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       fix_dot-fig05ops1_34.4-0 ; 
       reduce-fig05ops1_34.2-0 ; 
       reduce-fig05ops1_67.4-0 ; 
       reduce-fig05ops1_68.4-0 ; 
       reduce-fig05ops1_69.4-0 ; 
       reduce-fig05ops1_70.3-0 ; 
       reduce-fig05ops1_71.3-0 ; 
       reduce-fig05ops1_72.3-0 ; 
       reduce-fig05ops1_73.3-0 ; 
       reduce-fig05ops1_74.4-0 ; 
       reduce-fig05ops1_75.4-0 ; 
       reduce-fig05ops1_76.3-0 ; 
       reduce-fig05ops1_77.4-0 ; 
       reduce-fig05ops1_78.4-0 ; 
       reduce-fig05ops1_79.4-0 ; 
       reduce-fig05ops1_80.4-0 ; 
       reduce-fig05ops1_81.4-0 ; 
       reduce-fig05ops1_82.4-0 ; 
       reduce-fig05ops1_83.4-0 ; 
       reduce-fig05ops1_84.4-0 ; 
       reduce-fig05ops1_85.4-0 ; 
       reduce-fig05ops1_86.4-0 ; 
       reduce-fig05ops1_87.3-0 ; 
       reduce-fig05ops1_88.4-0 ; 
       reduce-fig05ops1_89.3-0 ; 
       reduce-fig05ops1_90.3-0 ; 
       reduce-fig05ops1_91.4-0 ; 
       reduce-fig05ops1_92.4-0 ; 
       reduce-fig05ops1_93.4-0 ; 
       reduce-fig05ops1_94.3-0 ; 
       reduce-fig05ops1_95.3-0 ; 
       reduce-fig05ops1_96.3-0 ; 
       reduce-fig05ops1_97.3-0 ; 
       reduce-fig05ops1_98.3-0 ; 
       reduce-fig05ops1_99.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 27 110 ; 
       5 0 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 0 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 11 110 ; 
       15 14 110 ; 
       16 0 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 18 110 ; 
       22 21 110 ; 
       23 3 110 ; 
       24 3 110 ; 
       25 15 110 ; 
       26 22 110 ; 
       27 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       3 24 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       4 2 300 ; 
       6 6 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       9 16 300 ; 
       10 15 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 33 300 ; 
       11 34 300 ; 
       15 4 300 ; 
       16 18 300 ; 
       17 17 300 ; 
       18 12 300 ; 
       18 13 300 ; 
       18 14 300 ; 
       18 35 300 ; 
       18 36 300 ; 
       22 5 300 ; 
       23 38 300 ; 
       24 37 300 ; 
       25 0 300 ; 
       26 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       4 11 401 ; 
       5 22 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 32 401 ; 
       10 33 401 ; 
       11 34 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 28 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 16 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       12 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       13 SCHEM 25 -12 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       16 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 5 -12 0 MPRFLG 0 ; 
       20 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 30 -4 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 20 -12 0 MPRFLG 0 ; 
       26 SCHEM 10 -12 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 11.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
