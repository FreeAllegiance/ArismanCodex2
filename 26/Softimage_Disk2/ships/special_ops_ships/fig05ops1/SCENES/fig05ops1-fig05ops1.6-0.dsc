SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fig05ops1-cam_int1.5-0 ROOT ; 
       fig05ops1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       done_lite-mat7.1-0 ; 
       durethma_fighterbike_sa-mat48.1-0 ; 
       fix_dot-mat69.1-0 ; 
       reduce-mat49.1-0 ; 
       reduce-mat50.1-0 ; 
       reduce-mat51.1-0 ; 
       reduce-mat52.1-0 ; 
       reduce-mat53.1-0 ; 
       reduce-mat54.1-0 ; 
       reduce-mat55.1-0 ; 
       reduce-mat56.1-0 ; 
       reduce-mat57.1-0 ; 
       reduce-mat58.1-0 ; 
       reduce-mat59.1-0 ; 
       reduce-mat60.1-0 ; 
       reduce-mat61.1-0 ; 
       reduce-mat62.1-0 ; 
       reduce-mat63.1-0 ; 
       reduce-mat64.1-0 ; 
       reduce-mat65.1-0 ; 
       reduce-mat66.1-0 ; 
       reduce-mat67.1-0 ; 
       reduce-mat68.1-0 ; 
       reduce-mat69.1-0 ; 
       reduce-mat70.1-0 ; 
       reduce-mat71.1-0 ; 
       reduce-mat72.1-0 ; 
       reduce-mat73.1-0 ; 
       reduce-mat74.1-0 ; 
       reduce-mat75.1-0 ; 
       reduce-mat76.1-0 ; 
       reduce-mat77.1-0 ; 
       reduce-mat78.1-0 ; 
       reduce-mat79.1-0 ; 
       reduce-mat80.1-0 ; 
       reduce-mat81.1-0 ; 
       reduce-mat82.1-0 ; 
       reduce-mat83.1-0 ; 
       reduce-mat84.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       fig05-cockpt.2-0 ; 
       fig05-fig05_1.6-0 ROOT ; 
       fig05-finzz0.1-0 ; 
       fig05-finzz1.1-0 ; 
       fig05-fuselg.1-0 ; 
       fig05-l-gun.1-0 ; 
       fig05-landgr0.1-0 ; 
       fig05-landgr1.1-0 ; 
       fig05-landgr2.1-0 ; 
       fig05-landgr3.1-0 ; 
       fig05-lsmoke.1-0 ; 
       fig05-lthrust.2-0 ; 
       fig05-lthruster1.1-0 ; 
       fig05-lthruster2.1-0 ; 
       fig05-lthruster3.1-0 ; 
       fig05-lwepatt1.1-0 ; 
       fig05-lwepatt2.1-0 ; 
       fig05-lwing0.1-0 ; 
       fig05-lwing1.1-0 ; 
       fig05-missemt.1-0 ; 
       fig05-rsmoke.1-0 ; 
       fig05-rthrust.2-0 ; 
       fig05-rthruster1.1-0 ; 
       fig05-rthruster2.1-0 ; 
       fig05-rthruster3.1-0 ; 
       fig05-rwepatt1.1-0 ; 
       fig05-rwepatt2.1-0 ; 
       fig05-rwing0.1-0 ; 
       fig05-rwing1.1-0 ; 
       fig05-SSa.1-0 ; 
       fig05-SSf.1-0 ; 
       fig05-SSl.1-0 ; 
       fig05-SSr.1-0 ; 
       fig05-trail.2-0 ; 
       fig05-trthrust.1-0 ; 
       fig05-tthrust.2-0 ; 
       fig05-wepemt.2-0 ; 
       fig05-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig05ops1/PICTURES/fig05 ; 
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig05ops1/PICTURES/fig05ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig05ops1-fig05ops1.6-0 ; 
    EndOfCHAPTER 

    CHAPTER SPREADSHEET_SETUP NBELEM 1     
       fig05ops1-Textures_2D.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       fix_dot-fig05ops1_34.2-0 ; 
       reduce-fig05ops1_34.1-0 ; 
       reduce-fig05ops1_67.3-0 ; 
       reduce-fig05ops1_68.3-0 ; 
       reduce-fig05ops1_69.3-0 ; 
       reduce-fig05ops1_70.2-0 ; 
       reduce-fig05ops1_71.2-0 ; 
       reduce-fig05ops1_72.2-0 ; 
       reduce-fig05ops1_73.2-0 ; 
       reduce-fig05ops1_74.3-0 ; 
       reduce-fig05ops1_75.3-0 ; 
       reduce-fig05ops1_76.2-0 ; 
       reduce-fig05ops1_77.3-0 ; 
       reduce-fig05ops1_78.3-0 ; 
       reduce-fig05ops1_79.3-0 ; 
       reduce-fig05ops1_80.3-0 ; 
       reduce-fig05ops1_81.3-0 ; 
       reduce-fig05ops1_82.3-0 ; 
       reduce-fig05ops1_83.3-0 ; 
       reduce-fig05ops1_84.3-0 ; 
       reduce-fig05ops1_85.3-0 ; 
       reduce-fig05ops1_86.3-0 ; 
       reduce-fig05ops1_87.2-0 ; 
       reduce-fig05ops1_88.3-0 ; 
       reduce-fig05ops1_89.2-0 ; 
       reduce-fig05ops1_90.2-0 ; 
       reduce-fig05ops1_91.3-0 ; 
       reduce-fig05ops1_92.3-0 ; 
       reduce-fig05ops1_93.3-0 ; 
       reduce-fig05ops1_94.2-0 ; 
       reduce-fig05ops1_95.2-0 ; 
       reduce-fig05ops1_96.2-0 ; 
       reduce-fig05ops1_97.2-0 ; 
       reduce-fig05ops1_98.2-0 ; 
       reduce-fig05ops1_99.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 1 110 ; 
       5 37 110 ; 
       6 1 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 18 110 ; 
       16 18 110 ; 
       17 14 110 ; 
       18 17 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 22 110 ; 
       24 23 110 ; 
       25 28 110 ; 
       26 28 110 ; 
       27 24 110 ; 
       28 27 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 18 110 ; 
       32 28 110 ; 
       33 1 110 ; 
       34 1 110 ; 
       35 1 110 ; 
       36 5 110 ; 
       37 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 3 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       5 2 300 ; 
       7 6 300 ; 
       8 7 300 ; 
       9 8 300 ; 
       12 16 300 ; 
       13 15 300 ; 
       14 9 300 ; 
       14 10 300 ; 
       14 11 300 ; 
       14 33 300 ; 
       14 34 300 ; 
       18 4 300 ; 
       22 18 300 ; 
       23 17 300 ; 
       24 12 300 ; 
       24 13 300 ; 
       24 14 300 ; 
       24 35 300 ; 
       24 36 300 ; 
       28 5 300 ; 
       29 38 300 ; 
       30 37 300 ; 
       31 0 300 ; 
       32 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 0 401 ; 
       3 1 401 ; 
       4 11 401 ; 
       5 22 401 ; 
       6 29 401 ; 
       7 30 401 ; 
       8 31 401 ; 
       9 32 401 ; 
       10 33 401 ; 
       11 34 401 ; 
       12 2 401 ; 
       13 3 401 ; 
       14 4 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 28 401 ; 
       20 9 401 ; 
       21 10 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 17 401 ; 
       27 16 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       32 23 401 ; 
       33 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 139.7705 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 68.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 116.0205 -2 0 MPRFLG 0 ; 
       5 SCHEM 101.0205 -6 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 35 -4 0 MPRFLG 0 ; 
       9 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 92.27052 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 149.7705 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 WIRECOL 8 7 MPRFLG 0 ; 
       15 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 45 -12 0 MPRFLG 0 ; 
       17 SCHEM 43.75 -8 0 WIRECOL 8 7 MPRFLG 0 ; 
       18 SCHEM 43.75 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       19 SCHEM 70.30596 -9.947351 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 89.77052 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 144.7705 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 15 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 5 -12 0 MPRFLG 0 ; 
       26 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       27 SCHEM 8.75 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 8.75 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 97.27052 -4 0 MPRFLG 0 ; 
       30 SCHEM 94.77052 -4 0 MPRFLG 0 ; 
       31 SCHEM 40 -12 0 MPRFLG 0 ; 
       32 SCHEM 10 -12 0 MPRFLG 0 ; 
       33 SCHEM 142.2705 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 87.27052 -3.92279 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 147.2705 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 99.77052 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 101.0205 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 40 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 102.2705 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 122.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 107.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 109.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 112.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 114.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 117.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 119.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 104.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 124.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 127.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 129.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 132.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 134.7705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 137.2705 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 94.77052 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 97.27052 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 102.2705 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 107.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 109.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 112.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 114.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 117.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 119.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 124.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 104.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 127.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 129.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 132.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 134.7705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 12.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 137.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 122.2705 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 35 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 37.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 76 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
