SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       static-cam_int1.4-0 ROOT ; 
       static-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       fig07ops1-black1.1-0 ; 
       fig07ops1-blue_pure1.1-0 ; 
       fig07ops1-blue_pure2.1-0 ; 
       fig07ops1-default1.1-0 ; 
       fig07ops1-glass1.1-0 ; 
       fig07ops1-left.1-0 ; 
       fig07ops1-mat1.1-0 ; 
       fig07ops1-mat10.1-0 ; 
       fig07ops1-mat11.1-0 ; 
       fig07ops1-mat12.1-0 ; 
       fig07ops1-mat13.1-0 ; 
       fig07ops1-mat14.1-0 ; 
       fig07ops1-mat15.1-0 ; 
       fig07ops1-mat16.1-0 ; 
       fig07ops1-mat17.1-0 ; 
       fig07ops1-mat18.1-0 ; 
       fig07ops1-mat19.1-0 ; 
       fig07ops1-mat2.1-0 ; 
       fig07ops1-mat20.1-0 ; 
       fig07ops1-mat22.1-0 ; 
       fig07ops1-mat23.1-0 ; 
       fig07ops1-mat24.1-0 ; 
       fig07ops1-mat25.1-0 ; 
       fig07ops1-mat3.1-0 ; 
       fig07ops1-mat35.1-0 ; 
       fig07ops1-mat36.1-0 ; 
       fig07ops1-mat37.1-0 ; 
       fig07ops1-mat38.1-0 ; 
       fig07ops1-mat4.1-0 ; 
       fig07ops1-mat41.1-0 ; 
       fig07ops1-mat42.1-0 ; 
       fig07ops1-mat43.1-0 ; 
       fig07ops1-mat44.1-0 ; 
       fig07ops1-mat45.1-0 ; 
       fig07ops1-mat5.1-0 ; 
       fig07ops1-mat6.1-0 ; 
       fig07ops1-mat7.1-0 ; 
       fig07ops1-mat8.1-0 ; 
       fig07ops1-mat9.1-0 ; 
       fig07ops1-right.1-0 ; 
       fig07ops1-yellow1.1-0 ; 
       fig07ops1-yellow2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 30     
       fig07-fig07.3-0 ROOT ; 
       fig07-finzzz.1-0 ; 
       fig07-fuselg1.1-0 ; 
       fig07-fuselgf.1-0 ; 
       fig07-landgrar2.1-0 ; 
       fig07-landgrLal2.1-0 ; 
       fig07-lfuselg.1-0 ; 
       fig07-lgun1.1-0 ; 
       fig07-lgun2.1-0 ; 
       fig07-LLal1.1-0 ; 
       fig07-LLam.1-0 ; 
       fig07-LLar1.1-0 ; 
       fig07-lwepatt1.1-0 ; 
       fig07-lwepatt2.1-0 ; 
       fig07-lwepmnt1.1-0 ; 
       fig07-lwepmnt2.1-0 ; 
       fig07-rfuselg.1-0 ; 
       fig07-rgun1.1-0 ; 
       fig07-rgun2.5-0 ; 
       fig07-rwepatt1.1-0 ; 
       fig07-rwepatt2.1-0 ; 
       fig07-rwepmnt1.1-0 ; 
       fig07-rwepmnt2.1-0 ; 
       fig07-SSl.1-0 ; 
       fig07-SSr.1-0 ; 
       fig07-tfuselg1.1-0 ; 
       fig07-tfuselg2.1-0 ; 
       fig07-tgun.2-0 ; 
       fig07-twepbas.1-0 ; 
       fig07-wepatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig07ops1/PICTURES/fig07ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig07ops1-fig07ops1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       fig07ops1-t2d1.2-0 ; 
       fig07ops1-t2d10.2-0 ; 
       fig07ops1-t2d11.2-0 ; 
       fig07ops1-t2d12.2-0 ; 
       fig07ops1-t2d13.2-0 ; 
       fig07ops1-t2d14.2-0 ; 
       fig07ops1-t2d15.2-0 ; 
       fig07ops1-t2d16.2-0 ; 
       fig07ops1-t2d17.2-0 ; 
       fig07ops1-t2d18.2-0 ; 
       fig07ops1-t2d19.2-0 ; 
       fig07ops1-t2d2.2-0 ; 
       fig07ops1-t2d21.2-0 ; 
       fig07ops1-t2d22.2-0 ; 
       fig07ops1-t2d23.2-0 ; 
       fig07ops1-t2d24.2-0 ; 
       fig07ops1-t2d3.2-0 ; 
       fig07ops1-t2d33.2-0 ; 
       fig07ops1-t2d34.2-0 ; 
       fig07ops1-t2d35.2-0 ; 
       fig07ops1-t2d36.2-0 ; 
       fig07ops1-t2d37.2-0 ; 
       fig07ops1-t2d38.2-0 ; 
       fig07ops1-t2d39.2-0 ; 
       fig07ops1-t2d4.2-0 ; 
       fig07ops1-t2d40.2-0 ; 
       fig07ops1-t2d41.2-0 ; 
       fig07ops1-t2d5.2-0 ; 
       fig07ops1-t2d6.2-0 ; 
       fig07ops1-t2d7.2-0 ; 
       fig07ops1-t2d8.2-0 ; 
       fig07ops1-t2d9.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 11 110 ; 
       5 9 110 ; 
       6 2 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 10 110 ; 
       10 2 110 ; 
       11 10 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 6 110 ; 
       16 2 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 16 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 25 110 ; 
       27 28 110 ; 
       28 26 110 ; 
       29 28 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 38 300 ; 
       2 41 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       3 4 300 ; 
       3 13 300 ; 
       4 7 300 ; 
       5 24 300 ; 
       6 2 300 ; 
       6 6 300 ; 
       6 17 300 ; 
       6 23 300 ; 
       6 28 300 ; 
       7 33 300 ; 
       8 32 300 ; 
       9 25 300 ; 
       10 8 300 ; 
       11 26 300 ; 
       16 1 300 ; 
       16 34 300 ; 
       16 35 300 ; 
       16 36 300 ; 
       16 37 300 ; 
       17 31 300 ; 
       18 30 300 ; 
       23 5 300 ; 
       24 39 300 ; 
       25 40 300 ; 
       25 16 300 ; 
       26 0 300 ; 
       26 27 300 ; 
       27 29 300 ; 
       28 18 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       10 3 401 ; 
       11 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 11 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 24 401 ; 
       29 21 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 25 401 ; 
       33 26 401 ; 
       34 27 401 ; 
       35 28 401 ; 
       36 29 401 ; 
       37 30 401 ; 
       38 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 35 -8 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -4 0 MPRFLG 0 ; 
       7 SCHEM 50 -6 0 MPRFLG 0 ; 
       8 SCHEM 45 -6 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 40 -6 0 MPRFLG 0 ; 
       15 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       17 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 15 -6 0 MPRFLG 0 ; 
       20 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 10 -6 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 25 -4 0 MPRFLG 0 ; 
       24 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       26 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       27 SCHEM 5 -10 0 MPRFLG 0 ; 
       28 SCHEM 3.75 -8 0 MPRFLG 0 ; 
       29 SCHEM 2.5 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 31.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 51.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 76 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
