SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       fat-cam_int1.5-0 ROOT ; 
       fat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       STATIC-light1.1-0 ROOT ; 
       STATIC-light2.1-0 ROOT ; 
       STATIC-light3.1-0 ROOT ; 
       STATIC-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       medium_fighter_sPATL-mat39.1-0 ; 
       medium_fighter_sPATL-mat40.1-0 ; 
       medium_fighter_sPATL-mat41.1-0 ; 
       new-back1.1-0 ; 
       new-base.1-0 ; 
       new-base1.1-0 ; 
       new-base2.1-0 ; 
       new-bottom.1-0 ; 
       new-caution1.1-0 ; 
       new-caution2.1-0 ; 
       new-front1.1-0 ; 
       new-guns1.1-0 ; 
       new-guns2.1-0 ; 
       new-mat23.1-0 ; 
       new-mat24.1-0 ; 
       new-mat25.1-0 ; 
       new-mat26.1-0 ; 
       new-mat27.1-0 ; 
       new-mat28.1-0 ; 
       new-mat29.1-0 ; 
       new-mat30.1-0 ; 
       new-mat32.1-0 ; 
       new-mat33.1-0 ; 
       new-mat34.1-0 ; 
       new-mat39.1-0 ; 
       new-mat40.1-0 ; 
       new-mat41.1-0 ; 
       new-mat42.1-0 ; 
       new-mat43.1-0 ; 
       new-mat44.1-0 ; 
       new-mat45.1-0 ; 
       new-mat46.1-0 ; 
       new-mat47.1-0 ; 
       new-mat48.1-0 ; 
       new-mat49.1-0 ; 
       new-mat50.1-0 ; 
       new-mat51.1-0 ; 
       new-mat52.1-0 ; 
       new-mat53.1-0 ; 
       new-mat54.1-0 ; 
       new-mat55.1-0 ; 
       new-mat56.1-0 ; 
       new-mat57.1-0 ; 
       new-sides_and_bottom.1-0 ; 
       new-top.1-0 ; 
       new-vents1.1-0 ; 
       remove_anim_edit_nulls-mat20.1-0 ; 
       remove_anim_edit_nulls-mat41.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       remove_anim_edit_nulls-cube1.1-0 ; 
       remove_anim_edit_nulls-cube2.2-0 ; 
       remove_anim_edit_nulls-cube3.2-0 ; 
       remove_anim_edit_nulls-cube4.1-0 ; 
       remove_anim_edit_nulls-cube5.1-0 ; 
       remove_anim_edit_nulls-cube6.1-0 ; 
       remove_anim_edit_nulls-fig08_3_1.4-0 ROOT ; 
       remove_anim_edit_nulls-fuselg.1-0 ; 
       remove_anim_edit_nulls-lslrsal0.1-0 ; 
       remove_anim_edit_nulls-lslrsal1.1-0 ; 
       remove_anim_edit_nulls-lslrsal2.1-0 ; 
       remove_anim_edit_nulls-lslrsal3.1-0 ; 
       remove_anim_edit_nulls-lslrsal4.1-0 ; 
       remove_anim_edit_nulls-lslrsal5.1-0 ; 
       remove_anim_edit_nulls-lslrsal6.2-0 ; 
       remove_anim_edit_nulls-lwepbar.1-0 ; 
       remove_anim_edit_nulls-rslrsal1.1-0 ; 
       remove_anim_edit_nulls-rslrsal2.1-0 ; 
       remove_anim_edit_nulls-rslrsal3.1-0 ; 
       remove_anim_edit_nulls-rslrsal4.1-0 ; 
       remove_anim_edit_nulls-rslrsal5.1-0 ; 
       remove_anim_edit_nulls-rslrsal6.1-0 ; 
       remove_anim_edit_nulls-rwepbar.2-0 ; 
       remove_anim_edit_nulls-slrsal0.1-0 ; 
       remove_anim_edit_nulls-SSal1.1-0 ; 
       remove_anim_edit_nulls-SSal2.1-0 ; 
       remove_anim_edit_nulls-SSal3.1-0 ; 
       remove_anim_edit_nulls-SSal4.1-0 ; 
       remove_anim_edit_nulls-SSal5.1-0 ; 
       remove_anim_edit_nulls-SSal6.1-0 ; 
       remove_anim_edit_nulls-SSar1.1-0 ; 
       remove_anim_edit_nulls-SSar2.1-0 ; 
       remove_anim_edit_nulls-SSar3.1-0 ; 
       remove_anim_edit_nulls-SSar4.1-0 ; 
       remove_anim_edit_nulls-SSar5.1-0 ; 
       remove_anim_edit_nulls-SSar6.1-0 ; 
       remove_anim_edit_nulls-tgun.2-0 ; 
       remove_anim_edit_nulls-twepbas.1-0 ; 
       remove_anim_edit_nulls-wepatt.1-0 ; 
       remove_anim_edit_nulls-wepmnt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/ships/special_ops_ships/fig11ops1/PICTURES/fig11ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fig11ops1-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       medium_fighter_sPATL-t2d34.2-0 ; 
       medium_fighter_sPATL-t2d35.2-0 ; 
       medium_fighter_sPATL-t2d36.2-0 ; 
       new-t2d24.2-0 ; 
       new-t2d27.2-0 ; 
       new-t2d28.2-0 ; 
       new-t2d29.2-0 ; 
       new-t2d31.2-0 ; 
       new-t2d33.2-0 ; 
       new-t2d39.2-0 ; 
       new-t2d40.2-0 ; 
       new-t2d41.2-0 ; 
       new-t2d43.2-0 ; 
       new-t2d44.2-0 ; 
       new-t2d45.2-0 ; 
       new-t2d46.2-0 ; 
       new-t2d47.2-0 ; 
       new-t2d48.2-0 ; 
       new-t2d49.2-0 ; 
       new-t2d50.2-0 ; 
       new-t2d51.2-0 ; 
       new-t2d52.2-0 ; 
       new-t2d53.2-0 ; 
       new-t2d54.2-0 ; 
       new-t2d55.2-0 ; 
       new-t2d56.2-0 ; 
       new-t2d57.2-0 ; 
       new-t2d58.2-0 ; 
       new-t2d6.2-0 ; 
       new-z.2-0 ; 
       remove_anim_edit_nulls-t2d19.2-0 ; 
       remove_anim_edit_nulls-t2d37.2-0 ; 
       STATIC-t2d59.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 15 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 22 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 7 110 ; 
       16 23 110 ; 
       17 23 110 ; 
       18 23 110 ; 
       19 23 110 ; 
       20 23 110 ; 
       21 23 110 ; 
       22 7 110 ; 
       23 7 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       26 11 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 14 110 ; 
       30 16 110 ; 
       31 17 110 ; 
       32 18 110 ; 
       33 19 110 ; 
       34 20 110 ; 
       35 21 110 ; 
       36 37 110 ; 
       37 39 110 ; 
       38 7 110 ; 
       39 38 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 36 300 ; 
       1 37 300 ; 
       2 38 300 ; 
       3 39 300 ; 
       4 40 300 ; 
       5 41 300 ; 
       7 7 300 ; 
       7 44 300 ; 
       7 4 300 ; 
       7 10 300 ; 
       7 43 300 ; 
       7 3 300 ; 
       7 45 300 ; 
       9 19 300 ; 
       10 20 300 ; 
       11 42 300 ; 
       12 21 300 ; 
       13 22 300 ; 
       14 23 300 ; 
       15 6 300 ; 
       15 9 300 ; 
       15 12 300 ; 
       16 18 300 ; 
       17 17 300 ; 
       18 16 300 ; 
       19 15 300 ; 
       20 13 300 ; 
       21 14 300 ; 
       22 5 300 ; 
       22 8 300 ; 
       22 11 300 ; 
       24 25 300 ; 
       25 24 300 ; 
       26 26 300 ; 
       27 27 300 ; 
       28 28 300 ; 
       29 29 300 ; 
       30 30 300 ; 
       31 31 300 ; 
       32 32 300 ; 
       33 33 300 ; 
       34 34 300 ; 
       35 35 300 ; 
       36 47 300 ; 
       37 46 300 ; 
       39 0 300 ; 
       39 1 300 ; 
       39 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       9 5 400 ; 
       10 6 400 ; 
       11 32 400 ; 
       12 7 400 ; 
       14 8 400 ; 
       16 4 400 ; 
       19 3 400 ; 
       21 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 15 401 ; 
       7 16 401 ; 
       8 18 401 ; 
       9 20 401 ; 
       10 14 401 ; 
       11 19 401 ; 
       12 21 401 ; 
       13 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       22 12 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
       43 13 401 ; 
       44 28 401 ; 
       45 17 401 ; 
       46 30 401 ; 
       47 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 52.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 55 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 45 -6 0 MPRFLG 0 ; 
       2 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 MPRFLG 0 ; 
       8 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 30 -6 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 MPRFLG 0 ; 
       14 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 45 -4 0 MPRFLG 0 ; 
       16 SCHEM 25 -6 0 MPRFLG 0 ; 
       17 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 15 -6 0 MPRFLG 0 ; 
       19 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 20 -6 0 MPRFLG 0 ; 
       21 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 18.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 40 -8 0 MPRFLG 0 ; 
       25 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       26 SCHEM 30 -8 0 MPRFLG 0 ; 
       27 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 35 -8 0 MPRFLG 0 ; 
       29 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 25 -8 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 15 -8 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 20 -8 0 MPRFLG 0 ; 
       35 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       36 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       38 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 29 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 52.64839 -6.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 52.64839 -8.13507 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       32 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 52.64839 -8.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 52.64839 -10.13507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
