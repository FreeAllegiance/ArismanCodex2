SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       borg_ship-cam_int1.18-0 ROOT ; 
       borg_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       texture-light1.4-0 ROOT ; 
       texture-light2.3-0 ROOT ; 
       texture-light3.3-0 ROOT ; 
       texture-light4.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       1701d_low-HULL_gray_20.4-0 ; 
       1701d_low-mat1.1-0 ; 
       1701d_low-mat2.1-0 ; 
       1701d_low-mat3.2-0 ; 
       1701d_low-mat4.2-0 ; 
       1701d_low-mat5.2-0 ; 
       1701d_low-mat6.2-0 ; 
       1701d_low-mat7.4-0 ; 
       texture-aft_bottom1.4-0 ; 
       texture-bottom1.4-0 ; 
       texture-mat11.2-0 ; 
       texture-mat8.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       1701d_low-Battle_Bridge.1-0 ; 
       1701d_low-cockpt.1-0 ; 
       1701d_low-Hull.8-0 ROOT ; 
       1701d_low-lthrust.1-0 ; 
       1701d_low-lwepemt.1-0 ; 
       1701d_low-missemt.1-0 ; 
       1701d_low-Port_Core.1-0 ; 
       1701d_low-Port_Engine.1-0 ; 
       1701d_low-rthrust.1-0 ; 
       1701d_low-rwepemt.1-0 ; 
       1701d_low-Saucer.1-0 ; 
       1701d_low-Starbord_Core.1-0 ; 
       1701d_low-Starbord_Engine.1-0 ; 
       1701d_low-trail.1-0 ; 
       1701d_low-turwepemt1.1-0 ; 
       1701d_low-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/extras/PICTURES/ops2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       enterprise_D-texture.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       1701d_low-t2d1.5-0 ; 
       texture-t2d13.1-0 ; 
       texture-t2d14.1-0 ; 
       texture-t2d15.1-0 ; 
       texture-t2d16.1-0 ; 
       texture-t2d2.5-0 ; 
       texture-t2d7.5-0 ; 
       texture-t2d9.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       5 2 110 ; 
       6 7 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       10 2 110 ; 
       11 12 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       4 2 110 ; 
       9 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       2 0 300 ; 
       2 7 300 ; 
       2 9 300 ; 
       2 8 300 ; 
       6 1 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       10 10 300 ; 
       11 2 300 ; 
       12 3 300 ; 
       12 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       10 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       3 3 401 ; 
       5 4 401 ; 
       7 0 401 ; 
       8 6 401 ; 
       9 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 SRT 1 1 1 0 0 0 -7.152557e-007 -1.230041 6.436205 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 MPRFLG 0 ; 
       13 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 50 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 52.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 50 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 52.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
