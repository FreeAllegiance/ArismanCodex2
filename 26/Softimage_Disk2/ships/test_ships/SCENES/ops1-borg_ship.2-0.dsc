SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       borg_ship-cam_int1.21-0 ROOT ; 
       borg_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       borg_ship-light1.6-0 ROOT ; 
       borg_ship-light2.6-0 ROOT ; 
       borg_ship-light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       borg_ship-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       borg_ship-cockpt.1-0 ; 
       borg_ship-lthrust.1-0 ; 
       borg_ship-lwepemt.1-0 ; 
       borg_ship-missemt.1-0 ; 
       borg_ship-rthrust.1-0 ; 
       borg_ship-rwepemt.1-0 ; 
       borg_ship-ship.5-0 ROOT ; 
       borg_ship-smoke.1-0 ; 
       borg_ship-trail.1-0 ; 
       borg_ship-turwepemt1.1-0 ; 
       borg_ship-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/extras/PICTURES/ops1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ops1-borg_ship.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       borg_ship-t2d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 6 110 ; 
       7 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       6 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
