SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       borg_ship-cam_int1.28-0 ROOT ; 
       borg_ship-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       enterprise_D-light1.6-0 ROOT ; 
       enterprise_D-light2.6-0 ROOT ; 
       enterprise_D-light3.6-0 ROOT ; 
       enterprise_D-light4.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       1701d_low-HULL_gray_20.4-0 ; 
       1701d_low-mat7.4-0 ; 
       enterprise_D-aft_bottom1.1-0 ; 
       enterprise_D-bottom1.1-0 ; 
       enterprise_D-mat11.1-0 ; 
       enterprise_D-mat12.1-0 ; 
       enterprise_D-mat13.1-0 ; 
       enterprise_D-mat14.1-0 ; 
       enterprise_D-mat15.1-0 ; 
       enterprise_D-mat16.1-0 ; 
       enterprise_D-mat17.1-0 ; 
       enterprise_D-mat8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       1701d_low-Battle_Bridge.1-0 ; 
       1701d_low-cockpt.1-0 ; 
       1701d_low-Hull.13-0 ROOT ; 
       1701d_low-lsmoke.1-0 ; 
       1701d_low-lthrust.1-0 ; 
       1701d_low-lwepemt.1-0 ; 
       1701d_low-missemt.1-0 ; 
       1701d_low-rsmoke.1-0 ; 
       1701d_low-rthrust.1-0 ; 
       1701d_low-rwepemt.1-0 ; 
       1701d_low-Saucer.1-0 ; 
       1701d_low-Starbord_Core1.1-0 ; 
       1701d_low-Starbord_Core2.1-0 ; 
       1701d_low-Starbord_Engine1.1-0 ; 
       1701d_low-Starbord_Engine2.1-0 ; 
       1701d_low-trail.1-0 ; 
       1701d_low-turwepemt1.1-0 ; 
       1701d_low-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/pete_data2/extras/PICTURES/ops2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ops2-enterprise_D.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       1701d_low-t2d1.8-0 ; 
       enterprise_D-t2d13.3-0 ; 
       enterprise_D-t2d14.3-0 ; 
       enterprise_D-t2d2.4-0 ; 
       enterprise_D-t2d20.1-0 ; 
       enterprise_D-t2d21.1-0 ; 
       enterprise_D-t2d22.1-0 ; 
       enterprise_D-t2d23.1-0 ; 
       enterprise_D-t2d24.1-0 ; 
       enterprise_D-t2d25.1-0 ; 
       enterprise_D-t2d7.4-0 ; 
       enterprise_D-t2d9.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       14 2 110 ; 
       12 14 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       13 2 110 ; 
       11 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 3 300 ; 
       2 2 300 ; 
       14 8 300 ; 
       14 9 300 ; 
       12 10 300 ; 
       10 4 300 ; 
       13 5 300 ; 
       13 6 300 ; 
       11 7 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       10 1 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 11 401 ; 
       5 4 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       1 0 401 ; 
       2 10 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -2 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 55 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 57.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 40 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
