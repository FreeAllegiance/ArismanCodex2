SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       t-cam_int1.12-0 ROOT ; 
       t-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       t-mat1.4-0 ; 
       t-mat2.3-0 ; 
       t-mat3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       t-bumper.4-0 ROOT ; 
       t1-grounder.5-0 ROOT ; 
       t2-final.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       E:/Pete_Data2/extras/PICTURES/CITY-london ; 
       E:/Pete_Data2/extras/PICTURES/map ; 
       E:/Pete_Data2/extras/PICTURES/map2 ; 
       E:/Pete_Data2/extras/PICTURES/map2_light ; 
       E:/Pete_Data2/extras/PICTURES/map_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       t-t.12-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       t-bumps.4-0 ; 
       t-map.4-0 ; 
       t-map_light.5-0 ; 
       t-map2.5-0 ; 
       t-map2light.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       0 0 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 2 400 ; 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 3.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
