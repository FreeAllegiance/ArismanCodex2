SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       single-cam_int1.3-0 ROOT ; 
       single-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       double-map3.1-0 ; 
       double-mat1.1-0 ; 
       single-default1.1-0 ; 
       single-top.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       single-sphere2.3-0 ROOT ; 
       sing_led1-sphere1.2-0 ROOT ; 
       sing_led2-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       F:/Double_wrap_planets/PICTURES/Large_rendermap ; 
       F:/Double_wrap_planets/PICTURES/Large_rendermap_light ; 
       F:/Double_wrap_planets/PICTURES/Nebulae1 ; 
       F:/Double_wrap_planets/PICTURES/Top_rendermap ; 
       F:/Double_wrap_planets/PICTURES/bottom_rendermap ; 
       F:/Double_wrap_planets/PICTURES/galaxy ; 
       F:/Double_wrap_planets/PICTURES/planet.1 ; 
       F:/Double_wrap_planets/PICTURES/planet.6 ; 
       F:/Double_wrap_planets/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mete_sphere-double.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       double-t2d7.1-0 ; 
       double-t2d8.1-0 ; 
       double-t2d9.1-0 ; 
       single-large.1-0 ; 
       single-large_light.1-0 ; 
       single-neb2.3-0 ; 
       single-t2d3.3-0 ; 
       single-t2d5.3-0 ; 
       single-t2d6.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 3 300 ; 
       1 1 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 5 400 ; 
       0 6 400 ; 
       0 7 400 ; 
       0 8 400 ; 
       1 4 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 3 401 ; 
       3 2 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 23.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       5 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
