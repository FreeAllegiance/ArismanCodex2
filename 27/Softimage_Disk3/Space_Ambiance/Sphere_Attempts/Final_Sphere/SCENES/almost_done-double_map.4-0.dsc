SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.15-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       double_map-mat8.1-0 ; 
       double_map-mat9.1-0 ; 
       top_bottom_w_adj_UV-mat1.4-0 ; 
       top_bottom_w_adj_UV-mat2.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       double_map-Mapper1.2-0 ROOT ; 
       top_bottom_w_adj_UV-Mapper.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       F:/Final_Sphere/PICTURES/environtest ; 
       F:/Final_Sphere/PICTURES/lagoon ; 
       F:/Final_Sphere/PICTURES/map ; 
       F:/Final_Sphere/PICTURES/map2 ; 
       F:/Final_Sphere/PICTURES/mapbottom ; 
       F:/Final_Sphere/PICTURES/orion ; 
       F:/Final_Sphere/PICTURES/pieces ; 
       F:/Final_Sphere/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       almost_done-double_map.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       double_map-t2d13.2-0 ; 
       double_map-t2d20.2-0 ; 
       double_map-t2d21.2-0 ; 
       double_map-t2d22.1-0 ; 
       top_bottom_w_adj_UV-Belt.5-0 ; 
       top_bottom_w_adj_UV-Horse_Head.5-0 ; 
       top_bottom_w_adj_UV-orion.5-0 ; 
       top_bottom_w_adj_UV-t2d2.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       1 3 300 ; 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 5 400 ; 
       1 4 400 ; 
       1 6 400 ; 
       1 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 7 401 ; 
       3 3 401 ; 
       0 1 401 ; 
       1 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 8.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 1.25 -6 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
