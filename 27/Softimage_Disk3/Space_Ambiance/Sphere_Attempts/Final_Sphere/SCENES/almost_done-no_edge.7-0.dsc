SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.11-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       no_edge-mat4.1-0 ; 
       no_edge-mat5.1-0 ; 
       top_bottom_w_adj_UV-mat1.3-0 ; 
       top_bottom_w_adj_UV-mat2.3-0 ; 
       top_bottom_w_adj_UV-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       top_bottom_w_adj_UV-Mapper.7-0 ROOT ; 
       top_bottom_w_adj_UV1-Mapper.1-0 ROOT ; 
       top_bottom2-reciever.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Final_Sphere/PICTURES/environtest ; 
       F:/Final_Sphere/PICTURES/lagoon ; 
       F:/Final_Sphere/PICTURES/map ; 
       F:/Final_Sphere/PICTURES/map2 ; 
       F:/Final_Sphere/PICTURES/orion ; 
       F:/Final_Sphere/PICTURES/pieces ; 
       F:/Final_Sphere/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       almost_done-no_edge.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       no_edge-t2d13.1-0 ; 
       no_edge-t2d15.1-0 ; 
       no_edge-t2d16.1-0 ; 
       top_bottom_w_adj_UV-Belt.4-0 ; 
       top_bottom_w_adj_UV-Horse_Head.4-0 ; 
       top_bottom_w_adj_UV-orion.4-0 ; 
       top_bottom_w_adj_UV-t2d12.5-0 ; 
       top_bottom_w_adj_UV-t2d2.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 3 300 ; 
       2 4 300 ; 
       1 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 3 400 ; 
       0 5 400 ; 
       0 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 7 401 ; 
       4 6 401 ; 
       0 1 401 ; 
       1 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1500 1500 1500 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 18.75 0 0 DISPLAY 1 2 SRT 1500 1500 1500 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
