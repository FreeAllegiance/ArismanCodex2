SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.22-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       with_strip-mat10.1-0 ; 
       with_strip-mat6.1-0 ; 
       with_strip-mat7.1-0 ; 
       with_strip-mat8.1-0 ; 
       with_strip-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       with_strip-cyl1.1-0 ; 
       with_strip-Mapper2.1-0 ROOT ; 
       with_strip-Mapper3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Final_Sphere/PICTURES/environtest ; 
       F:/Final_Sphere/PICTURES/lagoon ; 
       F:/Final_Sphere/PICTURES/map ; 
       F:/Final_Sphere/PICTURES/map2 ; 
       F:/Final_Sphere/PICTURES/orion ; 
       F:/Final_Sphere/PICTURES/pieces ; 
       F:/Final_Sphere/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       almost_done-with_strip.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       with_strip-Belt3.1-0 ; 
       with_strip-Horse_Head3.1-0 ; 
       with_strip-orion3.1-0 ; 
       with_strip-t2d19.1-0 ; 
       with_strip-t2d20.1-0 ; 
       with_strip-t2d21.1-0 ; 
       with_strip-t2d22.1-0 ; 
       with_strip-t2d23.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 1 400 ; 
       2 0 400 ; 
       2 2 400 ; 
       2 4 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 5 401 ; 
       0 7 401 ; 
       1 3 401 ; 
       2 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -8 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 0 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 0 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
