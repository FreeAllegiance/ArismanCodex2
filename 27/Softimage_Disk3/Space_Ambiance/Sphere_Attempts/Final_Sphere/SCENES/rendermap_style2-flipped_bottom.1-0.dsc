SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.1-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       flipped_bottom-mat5.1-0 ; 
       top_bottom_w_adj_UV-mat1.1-0 ; 
       top_bottom_w_adj_UV-mat2.1-0 ; 
       top_bottom_w_adj_UV-mat3.1-0 ; 
       top_bottom_w_adj_UV-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       top_bottom_w_adj_UV-Mapper.1-0 ROOT ; 
       top_bottom2-Planet1.1-0 ; 
       top_bottom2-Planet2.1-0 ; 
       top_bottom2-reciever.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Final_Sphere/PICTURES/map ; 
       F:/top-bottom/PICTURES/map_light copy ; 
       F:/Final_Sphere/PICTURES/orion ; 
       F:/Final_Sphere/PICTURES/pieces ; 
       F:/Final_Sphere/PICTURES/planet.1 ; 
       F:/Final_Sphere/PICTURES/planet.13 ; 
       F:/Final_Sphere/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       rendermap_style2-flipped_bottom.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       flipped_bottom-t2d14.1-0 ; 
       top_bottom_w_adj_UV-Belt.1-0 ; 
       top_bottom_w_adj_UV-Horse_Head.1-0 ; 
       top_bottom_w_adj_UV-orion.1-0 ; 
       top_bottom_w_adj_UV-t2d12.1-0 ; 
       top_bottom_w_adj_UV-t2d13.1-0 ; 
       top_bottom_w_adj_UV-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 3 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 2 300 ; 
       1 4 300 ; 
       2 0 300 ; 
       3 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       0 1 400 ; 
       0 3 400 ; 
       1 5 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 6 401 ; 
       3 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 3.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
