SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       flat-cam_int1.1-0 ROOT ; 
       flat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       flat-mat1.1-0 ; 
       flat-mat2.1-0 ; 
       flat-mat3.1-0 ; 
       flat-mat4.1-0 ; 
       flat-mat5.1-0 ; 
       flat-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       flat-null1.1-0 ROOT ; 
       flat-sphere1.1-0 ROOT ; 
       flat-sphere10.1-0 ; 
       flat-sphere9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/environtest ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/map ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       flat-flat.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       flat-t2d1.1-0 ; 
       flat-t2d2.1-0 ; 
       flat-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       2 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 6.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
