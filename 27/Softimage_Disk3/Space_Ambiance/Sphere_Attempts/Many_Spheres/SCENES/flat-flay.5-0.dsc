SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       flay-cam_int1.5-0 ROOT ; 
       flay-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       flay-mat1.3-0 ; 
       flay-mat2.1-0 ; 
       flay-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       flay-null1.3-0 ROOT ; 
       flay-sphere1.4-0 ROOT ; 
       flay-sphere3.1-0 ; 
       flay-sphere4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/environtest ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/map ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/crap/orion ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       flat-flay.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       flay-t2d1.4-0 ; 
       flay-t2d2.1-0 ; 
       flay-t2d3.1-0 ; 
       flay-t2d4.4-0 ; 
       flay-t2d5.2-0 ; 
       flay-t2d6.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 0 110 ; 
       3 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       2 1 300 ; 
       3 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       1 4 400 ; 
       1 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 6.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 13.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
