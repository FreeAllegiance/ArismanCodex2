SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       flat_map-cam_int1.4-0 ROOT ; 
       flat_map-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       flat_map-mat5.2-0 ; 
       flat_map-mat6.1-0 ; 
       flat_map-mat7.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       flat_map-nurbs.2-0 ROOT ; 
       new2-Poly_Map_to.5-0 ROOT ; 
       new3-Poly_Map_to.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/crap/collision ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/environtest ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/Nebs/galaxy ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/map ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/map_light ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/Nebs/neb02 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       flat_map-flat_map.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       flat_map-RenderMap_Me2.3-0 ; 
       flat_map-RenderMap_Me3.2-0 ; 
       flat_map-t2d1.1-0 ; 
       flat_map-t2d2.2-0 ; 
       flat_map-t2d3.2-0 ; 
       flat_map-t2d4.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 0 300 ; 
       2 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       0 5 400 ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 1 401 ; 
       0 0 401 ; 
       2 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 0 0 DISPLAY 0 0 SRT 1500 1500 1500 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 15 -2.125444 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -2.125444 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 15 -4.125443 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -4.125443 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
