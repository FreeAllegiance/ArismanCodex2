SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       geo-cam_int1.3-0 ROOT ; 
       geo-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       geo-hex1.1-0 ; 
       geo-hex2.1-0 ; 
       geo-hex3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       geo_sphere1-geo_sphere.3-0 ROOT ; 
       geo_sphere2-geo_sphere.1-0 ROOT ; 
       geo_sphere3-out.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/map ; 
       F:/Pete_Data3/Sphere_Attempts/Many_Spheres/PICTURES/crap/orion ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       geo-geo.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       geo-t2d1.3-0 ; 
       geo-t2d2.1-0 ; 
       geo-t2d3.1-0 ; 
       geo-t2d4.1-0 ; 
       geo-t2d5.1-0 ; 
       geo-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 2 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       1 4 400 ; 
       1 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 0 -6.000001 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1500 1500 1500 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 0 -8.000002 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 0 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -4.000001 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -8.000002 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
