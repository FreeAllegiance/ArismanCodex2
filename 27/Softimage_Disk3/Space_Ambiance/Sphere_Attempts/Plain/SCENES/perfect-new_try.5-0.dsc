SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.30-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       new_try-mat1.1-0 ; 
       new_try-mat3.1-0 ; 
       new_try-mat4.1-0 ; 
       new_try-mat5.1-0 ; 
       new_try-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       new_try-Panel1.1-0 ; 
       new_try-Panel2.1-0 ; 
       new_try-Space.4-0 ROOT ; 
       new_try1-Space.2-0 ROOT ; 
       new_try2-Space.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       F:/Plain/PICTURES/pieces ; 
       F:/Plain/PICTURES/planet.1 ; 
       F:/Plain/PICTURES/planet.2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       perfect-new_try.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       new_try-t2d1.3-0 ; 
       new_try-t2d3.1-0 ; 
       new_try-t2d4.1-0 ; 
       new_try-t2d5.2-0 ; 
       new_try-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 3 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       4 4 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 0 400 ; 
       3 3 400 ; 
       0 1 400 ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 21.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
