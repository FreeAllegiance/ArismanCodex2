SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       single-cam_int1.1-0 ROOT ; 
       single-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       single-default1.1-0 ; 
       single-map2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       single-sphere2.1-0 ROOT ; 
       sing_led1-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       F:/Single_wrap_planets/PICTURES/Nebulae1 ; 
       F:/Single_wrap_planets/PICTURES/galaxy ; 
       F:/Single_wrap_planets/PICTURES/planet.1 ; 
       F:/Single_wrap_planets/PICTURES/planet.6 ; 
       F:/Single_wrap_planets/PICTURES/rendermap ; 
       F:/Single_wrap_planets/PICTURES/rendermap_light ; 
       F:/Single_wrap_planets/PICTURES/sun.1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mete_sphere-single.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       single-neb2.1-0 ; 
       single-rendermap1.1-0 ; 
       single-t2d3.1-0 ; 
       single-t2d4.1-0 ; 
       single-t2d5.1-0 ; 
       single-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       0 0 400 ; 
       0 2 400 ; 
       0 4 400 ; 
       0 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 16.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
