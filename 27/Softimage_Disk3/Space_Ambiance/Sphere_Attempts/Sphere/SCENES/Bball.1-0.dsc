SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       Bball-roc_n__joc.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       Bball-cam_int1.1-0 ROOT ; 
       Bball-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Bball-Light01.1-0 ROOT ; 
       Bball-Light01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       Bball-ROCK_N__JOCK.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       Bball-roc_n__joc.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Bball.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       Bball-bball.gif.texture.1-0 ; 
       Bball-unnamed.gif.bump.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       1 0 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 0 0 DISPLAY 0 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 61 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
