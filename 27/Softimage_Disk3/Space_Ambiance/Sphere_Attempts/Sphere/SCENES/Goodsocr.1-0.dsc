SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Goodsocr-cam_int1.7-0 ROOT ; 
       Goodsocr-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       Goodsocr-Light01.1-0 ROOT ; 
       Goodsocr-Light02.1-0 ROOT ; 
       Goodsocr-Light02.1-0 ; 
       Goodsocr-Light03.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       Goodsocr-BALL_LEATHER.2-0 ; 
       Goodsocr-BLACK_LEATHER.2-0 ; 
       Goodsocr-Default01.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       Goodsocr-soccerBall.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Goodsocr.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       Goodsocr-stone03.tif.bump.2-0 ; 
       Goodsocr-stone03.tif.bump01.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 1 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 1 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       0 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 0 0 DISPLAY 0 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       3 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
