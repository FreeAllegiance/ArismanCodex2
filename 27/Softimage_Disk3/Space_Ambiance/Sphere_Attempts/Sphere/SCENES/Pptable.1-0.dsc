SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       Pptable-cam_int1.1-0 ROOT ; 
       Pptable-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       Pptable-Light01.1-0 ROOT ; 
       Pptable-Light02.1-0 ROOT ; 
       Pptable-Light03.1-0 ROOT ; 
       Pptable-Light04.1-0 ROOT ; 
       Pptable-Light05.1-0 ROOT ; 
       Pptable-Light06.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       Pptable-BLACK_MATTE.1-0 ; 
       Pptable-BLACK_MATTE01.1-0 ; 
       Pptable-BLACK_MATTE02.1-0 ; 
       Pptable-BLACK_MATTE03.1-0 ; 
       Pptable-BLACK_MATTE04.1-0 ; 
       Pptable-GREEN_MATTE.1-0 ; 
       Pptable-RED_MATTE.1-0 ; 
       Pptable-RED_MATTE01.1-0 ; 
       Pptable-SILVER.1-0 ; 
       Pptable-SILVER01.1-0 ; 
       Pptable-SILVER02.1-0 ; 
       Pptable-SILVER03.1-0 ; 
       Pptable-WHITE_MATTE.1-0 ; 
       Pptable-WHITE_MATTE01.1-0 ; 
       Pptable-WOOD_-_MED._ASH.1-0 ; 
       Pptable-WOOD_-_MED._ASH01.1-0 ; 
       Pptable-WOOD_-_MED._ASH02.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       Pptable-Fence01.1-0 ROOT ; 
       Pptable-leg.1-0 ROOT ; 
       Pptable-leg01.1-0 ROOT ; 
       Pptable-leg02.1-0 ROOT ; 
       Pptable-leg03.1-0 ROOT ; 
       Pptable-NETSUPPO01.1-0 ROOT ; 
       Pptable-NETSUPPO02.1-0 ROOT ; 
       Pptable-PADDLE.1-0 ROOT ; 
       Pptable-PADDLE01.1-0 ROOT ; 
       Pptable-PADDLE02.1-0 ROOT ; 
       Pptable-PADDLE03.1-0 ROOT ; 
       Pptable-PADDLE04.1-0 ROOT ; 
       Pptable-PPTABLE.1-0 ROOT ; 
       Pptable-STRIP.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Pptable.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       Pptable-ashsen.gif.texture.1-0 ; 
       Pptable-ashsen.gif.texture01.1-0 ; 
       Pptable-ashsen.gif.texture02.1-0 ; 
       Pptable-refmap.gif.reflect.1-0 ; 
       Pptable-refmap.gif.reflect01.1-0 ; 
       Pptable-refmap.gif.reflect02.1-0 ; 
       Pptable-refmap.gif.reflect03.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       6 0 300 ; 
       0 1 300 ; 
       4 8 300 ; 
       3 9 300 ; 
       2 10 300 ; 
       1 11 300 ; 
       11 6 300 ; 
       11 14 300 ; 
       11 2 300 ; 
       10 12 300 ; 
       9 3 300 ; 
       8 15 300 ; 
       7 7 300 ; 
       5 4 300 ; 
       13 13 300 ; 
       12 5 300 ; 
       12 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       14 0 401 ; 
       15 1 401 ; 
       16 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 0 0 DISPLAY 0 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       5 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       15 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       16 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
