SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.23-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       sphere-band_1.2-0 ; 
       sphere-band_2.2-0 ; 
       sphere-band_3.2-0 ; 
       sphere-band_4.2-0 ; 
       sphere-Default.5-0 ; 
       sphere-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.19-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Sphere/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Ribbon-sphere.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       sphere-t2d1.15-0 ; 
       sphere-t2d2.14-0 ; 
       sphere-t2d3.12-0 ; 
       sphere-t2d4.10-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 2 401 ; 
       3 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 DISPLAY 1 2 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.768899 -10.49817 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.207347 -10.44087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9.118325 -10.52682 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.57769 -10.69872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.76835 -5.941276 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.3063 1.448534 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.46308 -12.29762 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 3.651451 -12.55689 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9.290232 -12.46952 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.207347 -12.44087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
