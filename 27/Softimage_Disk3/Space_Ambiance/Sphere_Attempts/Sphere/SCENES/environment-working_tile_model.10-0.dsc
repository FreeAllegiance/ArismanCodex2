SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       preuv-cam_int1.51-0 ROOT ; 
       preuv-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       working_tile_model-mat2.5-0 ; 
       working_tile_model-mat3.3-0 ; 
       working_tile_model-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       working_tile_model-sphere6.3-0 ROOT ; 
       working_tile_model1-sphere6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/rendermap ; 
       F:/Sphere/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       environment-working_tile_model.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       working_tile_model-t2d1.3-0 ; 
       working_tile_model-t2d2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       working_tile_model-t3d1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
