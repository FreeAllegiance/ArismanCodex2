SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ball-cam_int1.2-0 ROOT ; 
       ball-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 35     
       ball-BALL_LEATHER.1-0 ; 
       ball-BLACK_LEATHER.1-0 ; 
       ball-Default01.1-0 ; 
       ball-mat1.1-0 ; 
       ball-mat10.1-0 ; 
       ball-mat11.1-0 ; 
       ball-mat12.1-0 ; 
       ball-mat13.1-0 ; 
       ball-mat14.1-0 ; 
       ball-mat15.1-0 ; 
       ball-mat16.1-0 ; 
       ball-mat17.1-0 ; 
       ball-mat18.1-0 ; 
       ball-mat19.1-0 ; 
       ball-mat2.1-0 ; 
       ball-mat20.1-0 ; 
       ball-mat21.1-0 ; 
       ball-mat22.1-0 ; 
       ball-mat23.1-0 ; 
       ball-mat24.1-0 ; 
       ball-mat25.1-0 ; 
       ball-mat26.1-0 ; 
       ball-mat27.1-0 ; 
       ball-mat28.1-0 ; 
       ball-mat29.1-0 ; 
       ball-mat3.1-0 ; 
       ball-mat30.1-0 ; 
       ball-mat31.1-0 ; 
       ball-mat32.1-0 ; 
       ball-mat4.1-0 ; 
       ball-mat5.1-0 ; 
       ball-mat6.1-0 ; 
       ball-mat7.1-0 ; 
       ball-mat8.1-0 ; 
       ball-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       ball-null.2-0 ROOT ; 
       ball-polygon.1-0 ; 
       ball-polygon1.1-0 ; 
       ball-polygon10.1-0 ; 
       ball-polygon11.1-0 ; 
       ball-polygon12.1-0 ; 
       ball-polygon13.1-0 ; 
       ball-polygon14.1-0 ; 
       ball-polygon15.1-0 ; 
       ball-polygon16.1-0 ; 
       ball-polygon17.1-0 ; 
       ball-polygon18.1-0 ; 
       ball-polygon19.1-0 ; 
       ball-polygon2.1-0 ; 
       ball-polygon20.1-0 ; 
       ball-polygon21.1-0 ; 
       ball-polygon22.1-0 ; 
       ball-polygon23.1-0 ; 
       ball-polygon24.1-0 ; 
       ball-polygon25.1-0 ; 
       ball-polygon26.1-0 ; 
       ball-polygon27.1-0 ; 
       ball-polygon28.1-0 ; 
       ball-polygon29.1-0 ; 
       ball-polygon3.1-0 ; 
       ball-polygon30.1-0 ; 
       ball-polygon31.1-0 ; 
       ball-polygon4.1-0 ; 
       ball-polygon5.1-0 ; 
       ball-polygon6.1-0 ; 
       ball-polygon7.1-0 ; 
       ball-polygon8.1-0 ; 
       ball-polygon9.1-0 ; 
       ball-soccerBall.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/SOFT3D_3.7SP1/3d/bin/rsrc/noIcon ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       final_model-ball.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       ball-stone03.tif.bump.1-0 ; 
       ball-stone03.tif.bump01.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       13 0 110 ; 
       24 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       2 14 300 ; 
       13 25 300 ; 
       24 29 300 ; 
       27 30 300 ; 
       28 31 300 ; 
       29 32 300 ; 
       30 33 300 ; 
       31 34 300 ; 
       32 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       33 2 300 ; 
       33 1 300 ; 
       33 0 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       8 10 300 ; 
       9 11 300 ; 
       10 12 300 ; 
       11 13 300 ; 
       12 15 300 ; 
       14 16 300 ; 
       15 17 300 ; 
       16 18 300 ; 
       17 19 300 ; 
       18 20 300 ; 
       19 21 300 ; 
       20 22 300 ; 
       21 23 300 ; 
       22 24 300 ; 
       23 26 300 ; 
       25 27 300 ; 
       26 28 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 41.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 MPRFLG 0 ; 
       27 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 15 -2 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       30 SCHEM 20 -2 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       32 SCHEM 25 -2 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 MPRFLG 0 ; 
       33 SCHEM 82.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 40 -2 0 MPRFLG 0 ; 
       9 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 45 -2 0 MPRFLG 0 ; 
       11 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 50 -2 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 55 -2 0 MPRFLG 0 ; 
       16 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 60 -2 0 MPRFLG 0 ; 
       18 SCHEM 62.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 65 -2 0 MPRFLG 0 ; 
       20 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 70 -2 0 MPRFLG 0 ; 
       22 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 75 -2 0 MPRFLG 0 ; 
       25 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       26 SCHEM 80 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 81.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 81.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 81.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 49 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 51.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 59 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 81.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 81.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
