SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       tile_on_panels-cam_int1.6-0 ROOT ; 
       tile_on_panels-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       tile_on_panels-hex.4-0 ; 
       tile_on_panels-mat1.2-0 ; 
       tile_on_panels-mat2.2-0 ; 
       tile_on_panels-mat3.2-0 ; 
       tile_on_panels-mat4.2-0 ; 
       tile_on_panels-mat5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       geo_sphere-geo_sphere.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Sphere/PICTURES/Type_One/Space ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       geo_sphere-tile_on_panels.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       tile_on_panels-t2d1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -4 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.2909678 -0.04110837 0.1433899 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
