SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.3-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       sphere-mat1.3-0 ; 
       sphere-mat2.2-0 ; 
       sphere-mat3.2-0 ; 
       sphere-mat4.2-0 ; 
       sphere-mat5.1-0 ; 
       sphere-mat6.1-0 ; 
       sphere-mat7.1-0 ; 
       sphere-mat8.1-0 ; 
       sphere-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Sphere/PICTURES/environtest ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       map-sphere.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       sphere-t2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 DISPLAY 1 2 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 0 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
