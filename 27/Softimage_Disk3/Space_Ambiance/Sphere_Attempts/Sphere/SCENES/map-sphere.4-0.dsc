SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.6-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       sphere-Blue.1-0 ; 
       sphere-Brick_red.1-0 ; 
       sphere-Default.1-0 ; 
       sphere-Green.1-0 ; 
       sphere-Magenta.1-0 ; 
       sphere-Orange.1-0 ; 
       sphere-peach.1-0 ; 
       sphere-Purple.1-0 ; 
       sphere-Yellow.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Sphere/PICTURES/environtest ; 
       D:/Sphere/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       map-sphere.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       sphere-t2d1.3-0 ; 
       sphere-t2d2.2-0 ; 
       sphere-t2d3.2-0 ; 
       sphere-t2d4.1-0 ; 
       sphere-t2d5.1-0 ; 
       sphere-t2d6.1-0 ; 
       sphere-t2d7.1-0 ; 
       sphere-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       0 8 300 ; 
       0 0 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 3 300 ; 
       0 7 300 ; 
       0 6 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 5 401 ; 
       1 0 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 7 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -4 0 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5563 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15.0563 1.448534 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 0 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5563 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 0 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
