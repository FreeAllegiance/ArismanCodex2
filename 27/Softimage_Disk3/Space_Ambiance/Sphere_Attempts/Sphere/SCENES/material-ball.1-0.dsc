SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ball-cam_int1.3-0 ROOT ; 
       ball-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       ball-Blue.1-0 ; 
       ball-Blue1.1-0 ; 
       ball-Blue2.1-0 ; 
       ball-Blue3.1-0 ; 
       ball-Blue4.1-0 ; 
       ball-cyan.1-0 ; 
       ball-cyan1.1-0 ; 
       ball-cyan2.1-0 ; 
       ball-cyan3.1-0 ; 
       ball-Green.1-0 ; 
       ball-Green1.1-0 ; 
       ball-Green2.1-0 ; 
       ball-Green3.1-0 ; 
       ball-Green4.1-0 ; 
       ball-Magenta.1-0 ; 
       ball-Magenta1.1-0 ; 
       ball-Magenta2.1-0 ; 
       ball-Magenta3.1-0 ; 
       ball-Red.1-0 ; 
       ball-Red1.1-0 ; 
       ball-Red2.1-0 ; 
       ball-Red3.1-0 ; 
       ball-Red4.1-0 ; 
       ball-White.1-0 ; 
       ball-White1.1-0 ; 
       ball-White2.1-0 ; 
       ball-White3.1-0 ; 
       ball-White4.1-0 ; 
       ball-Yellow.1-0 ; 
       ball-Yellow1.1-0 ; 
       ball-Yellow2.1-0 ; 
       ball-Yellow3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       ball-null.3-0 ROOT ; 
       ball-polygon.1-0 ; 
       ball-polygon1.1-0 ; 
       ball-polygon10.1-0 ; 
       ball-polygon11.1-0 ; 
       ball-polygon12.1-0 ; 
       ball-polygon13.1-0 ; 
       ball-polygon14.1-0 ; 
       ball-polygon15.1-0 ; 
       ball-polygon16.1-0 ; 
       ball-polygon17.1-0 ; 
       ball-polygon18.1-0 ; 
       ball-polygon19.1-0 ; 
       ball-polygon2.1-0 ; 
       ball-polygon20.1-0 ; 
       ball-polygon21.1-0 ; 
       ball-polygon22.1-0 ; 
       ball-polygon23.1-0 ; 
       ball-polygon24.1-0 ; 
       ball-polygon25.1-0 ; 
       ball-polygon26.1-0 ; 
       ball-polygon27.1-0 ; 
       ball-polygon28.1-0 ; 
       ball-polygon29.1-0 ; 
       ball-polygon3.1-0 ; 
       ball-polygon30.1-0 ; 
       ball-polygon31.1-0 ; 
       ball-polygon4.1-0 ; 
       ball-polygon5.1-0 ; 
       ball-polygon6.1-0 ; 
       ball-polygon7.1-0 ; 
       ball-polygon8.1-0 ; 
       ball-polygon9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Sphere/PICTURES/environtest ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       material-ball.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       ball-t2d1.1-0 ; 
       ball-t2d10.1-0 ; 
       ball-t2d11.1-0 ; 
       ball-t2d12.1-0 ; 
       ball-t2d13.1-0 ; 
       ball-t2d14.1-0 ; 
       ball-t2d15.1-0 ; 
       ball-t2d16.1-0 ; 
       ball-t2d17.1-0 ; 
       ball-t2d18.1-0 ; 
       ball-t2d19.1-0 ; 
       ball-t2d2.1-0 ; 
       ball-t2d20.1-0 ; 
       ball-t2d21.1-0 ; 
       ball-t2d22.1-0 ; 
       ball-t2d23.1-0 ; 
       ball-t2d24.1-0 ; 
       ball-t2d25.1-0 ; 
       ball-t2d26.1-0 ; 
       ball-t2d27.1-0 ; 
       ball-t2d28.1-0 ; 
       ball-t2d29.1-0 ; 
       ball-t2d3.1-0 ; 
       ball-t2d30.1-0 ; 
       ball-t2d31.1-0 ; 
       ball-t2d32.1-0 ; 
       ball-t2d4.1-0 ; 
       ball-t2d5.1-0 ; 
       ball-t2d6.1-0 ; 
       ball-t2d7.1-0 ; 
       ball-t2d8.1-0 ; 
       ball-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       13 0 110 ; 
       24 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 0 110 ; 
       31 0 110 ; 
       32 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 0 110 ; 
       17 0 110 ; 
       18 0 110 ; 
       19 0 110 ; 
       20 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       25 0 110 ; 
       26 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 23 300 ; 
       2 18 300 ; 
       13 9 300 ; 
       24 0 300 ; 
       27 28 300 ; 
       28 14 300 ; 
       29 5 300 ; 
       30 24 300 ; 
       31 19 300 ; 
       32 10 300 ; 
       3 1 300 ; 
       4 29 300 ; 
       5 15 300 ; 
       6 6 300 ; 
       7 25 300 ; 
       8 20 300 ; 
       9 11 300 ; 
       10 2 300 ; 
       11 30 300 ; 
       12 16 300 ; 
       14 7 300 ; 
       15 26 300 ; 
       16 21 300 ; 
       17 12 300 ; 
       18 3 300 ; 
       19 31 300 ; 
       20 17 300 ; 
       21 8 300 ; 
       22 27 300 ; 
       23 22 300 ; 
       25 13 300 ; 
       26 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       23 0 401 ; 
       18 11 401 ; 
       9 25 401 ; 
       0 24 401 ; 
       28 23 401 ; 
       14 21 401 ; 
       5 20 401 ; 
       24 19 401 ; 
       19 18 401 ; 
       10 17 401 ; 
       1 16 401 ; 
       29 15 401 ; 
       15 14 401 ; 
       6 13 401 ; 
       25 12 401 ; 
       20 10 401 ; 
       11 9 401 ; 
       2 8 401 ; 
       30 7 401 ; 
       16 6 401 ; 
       7 5 401 ; 
       26 4 401 ; 
       21 3 401 ; 
       12 2 401 ; 
       3 1 401 ; 
       31 31 401 ; 
       17 30 401 ; 
       8 29 401 ; 
       27 28 401 ; 
       22 27 401 ; 
       13 26 401 ; 
       4 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -6 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 5 -6 0 USR MPRFLG 0 ; 
       24 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       27 SCHEM 10 -6 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       29 SCHEM 15 -6 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 20 -6 0 MPRFLG 0 ; 
       32 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 30 -6 0 MPRFLG 0 ; 
       6 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 35 -6 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 40 -6 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 45 -6 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 55 -6 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 60 -6 0 MPRFLG 0 ; 
       19 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 MPRFLG 0 ; 
       21 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 70 -6 0 MPRFLG 0 ; 
       23 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 75 -6 0 MPRFLG 0 ; 
       26 SCHEM 77.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       23 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.5 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       11 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 22.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
