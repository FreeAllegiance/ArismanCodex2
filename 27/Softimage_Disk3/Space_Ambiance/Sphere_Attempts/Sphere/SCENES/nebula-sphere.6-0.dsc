SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.29-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       sphere-band_1.4-0 ; 
       sphere-band_2.4-0 ; 
       sphere-band_3.4-0 ; 
       sphere-band_4.4-0 ; 
       sphere-Default.7-0 ; 
       sphere-mat1.6-0 ; 
       sphere-mat2.4-0 ; 
       sphere-mat3.4-0 ; 
       sphere-mat4.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Sphere/PICTURES/environtest ; 
       D:/Sphere/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       nebula-sphere.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       sphere-t2d1.19-0 ; 
       sphere-t2d2.18-0 ; 
       sphere-t2d3.16-0 ; 
       sphere-t2d4.14-0 ; 
       sphere-t2d5.5-0 ; 
       sphere-t2d6.3-0 ; 
       sphere-t2d7.3-0 ; 
       sphere-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 1 401 ; 
       1 3 401 ; 
       2 2 401 ; 
       3 0 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.768899 -10.49817 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.207347 -10.44087 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9.118325 -10.52682 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.57769 -10.69872 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.3063 1.448534 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 13.24661 -6.234896 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 15.53684 -6.176172 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 18.41431 -6.058724 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.84262 -6.176172 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.46308 -12.29762 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 3.651451 -12.55689 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9.290232 -12.46952 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.207347 -12.44087 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 13.24661 -8.234896 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.53684 -8.176172 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19.41431 -8.058723 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.84262 -8.176172 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
