SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.2-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       sphere-mat1.2-0 ; 
       sphere-mat2.1-0 ; 
       sphere-mat3.1-0 ; 
       sphere-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       sphere-dodeca1.2-0 ROOT ; 
       sphere-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_mat-sphere.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 5 0 0 WIRECOL 2 7 DISPLAY 0 0 SRT 39.94913 39.94913 39.94913 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
