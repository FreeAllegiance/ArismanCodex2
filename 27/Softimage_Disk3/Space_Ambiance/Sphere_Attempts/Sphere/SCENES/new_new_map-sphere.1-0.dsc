SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.34-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       sphere-Default.9-0 ; 
       sphere-mat1.8-0 ; 
       sphere-mat2.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.29-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Sphere/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_new_map-sphere.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       sphere-t2d1.20-0 ; 
       sphere-t2d2.19-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 DISPLAY 1 2 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 11.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 16.3063 1.448534 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
