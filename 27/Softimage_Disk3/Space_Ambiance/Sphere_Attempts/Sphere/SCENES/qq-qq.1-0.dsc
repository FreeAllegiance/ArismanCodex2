SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       qq-cam_int1.1-0 ROOT ; 
       qq-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       qq-mat1.1-0 ; 
       qq-mat10.1-0 ; 
       qq-mat11.1-0 ; 
       qq-mat12.1-0 ; 
       qq-mat13.1-0 ; 
       qq-mat14.1-0 ; 
       qq-mat15.1-0 ; 
       qq-mat16.1-0 ; 
       qq-mat17.1-0 ; 
       qq-mat18.1-0 ; 
       qq-mat19.1-0 ; 
       qq-mat2.1-0 ; 
       qq-mat20.1-0 ; 
       qq-mat21.1-0 ; 
       qq-mat22.1-0 ; 
       qq-mat23.1-0 ; 
       qq-mat24.1-0 ; 
       qq-mat25.1-0 ; 
       qq-mat26.1-0 ; 
       qq-mat27.1-0 ; 
       qq-mat28.1-0 ; 
       qq-mat29.1-0 ; 
       qq-mat3.1-0 ; 
       qq-mat30.1-0 ; 
       qq-mat31.1-0 ; 
       qq-mat32.1-0 ; 
       qq-mat33.1-0 ; 
       qq-mat34.1-0 ; 
       qq-mat35.1-0 ; 
       qq-mat36.1-0 ; 
       qq-mat37.1-0 ; 
       qq-mat38.1-0 ; 
       qq-mat39.1-0 ; 
       qq-mat4.1-0 ; 
       qq-mat40.1-0 ; 
       qq-mat41.1-0 ; 
       qq-mat42.1-0 ; 
       qq-mat43.1-0 ; 
       qq-mat44.1-0 ; 
       qq-mat45.1-0 ; 
       qq-mat46.1-0 ; 
       qq-mat47.1-0 ; 
       qq-mat48.1-0 ; 
       qq-mat49.1-0 ; 
       qq-mat5.1-0 ; 
       qq-mat50.1-0 ; 
       qq-mat51.1-0 ; 
       qq-mat52.1-0 ; 
       qq-mat53.1-0 ; 
       qq-mat54.1-0 ; 
       qq-mat55.1-0 ; 
       qq-mat56.1-0 ; 
       qq-mat57.1-0 ; 
       qq-mat58.1-0 ; 
       qq-mat59.1-0 ; 
       qq-mat6.1-0 ; 
       qq-mat60.1-0 ; 
       qq-mat61.1-0 ; 
       qq-mat62.1-0 ; 
       qq-mat63.1-0 ; 
       qq-mat64.1-0 ; 
       qq-mat7.1-0 ; 
       qq-mat8.1-0 ; 
       qq-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       qq-bmerge1.1-0 ROOT ; 
       qq-null.1-0 ROOT ; 
       qq-polygon.1-0 ; 
       qq-polygon1.1-0 ; 
       qq-polygon10.1-0 ; 
       qq-polygon11.1-0 ; 
       qq-polygon12.1-0 ; 
       qq-polygon13.1-0 ; 
       qq-polygon14.1-0 ; 
       qq-polygon15.1-0 ; 
       qq-polygon16.1-0 ; 
       qq-polygon17.1-0 ; 
       qq-polygon18.1-0 ; 
       qq-polygon19.1-0 ; 
       qq-polygon2.1-0 ; 
       qq-polygon20.1-0 ; 
       qq-polygon21.1-0 ; 
       qq-polygon22.1-0 ; 
       qq-polygon23.1-0 ; 
       qq-polygon24.1-0 ; 
       qq-polygon25.1-0 ; 
       qq-polygon26.1-0 ; 
       qq-polygon27.1-0 ; 
       qq-polygon28.1-0 ; 
       qq-polygon29.1-0 ; 
       qq-polygon3.1-0 ; 
       qq-polygon30.1-0 ; 
       qq-polygon31.1-0 ; 
       qq-polygon4.1-0 ; 
       qq-polygon5.1-0 ; 
       qq-polygon6.1-0 ; 
       qq-polygon7.1-0 ; 
       qq-polygon8.1-0 ; 
       qq-polygon9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       qq-qq.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 1 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 1 110 ; 
       22 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 1 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 1 110 ; 
       32 1 110 ; 
       33 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       3 11 300 ; 
       4 2 300 ; 
       5 3 300 ; 
       6 4 300 ; 
       7 5 300 ; 
       8 6 300 ; 
       9 7 300 ; 
       10 8 300 ; 
       11 9 300 ; 
       12 10 300 ; 
       13 12 300 ; 
       14 22 300 ; 
       15 13 300 ; 
       16 14 300 ; 
       17 15 300 ; 
       18 16 300 ; 
       19 17 300 ; 
       20 18 300 ; 
       21 19 300 ; 
       22 20 300 ; 
       23 21 300 ; 
       24 23 300 ; 
       25 33 300 ; 
       26 24 300 ; 
       27 25 300 ; 
       28 44 300 ; 
       29 55 300 ; 
       30 61 300 ; 
       31 62 300 ; 
       32 63 300 ; 
       33 1 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 38 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 42 300 ; 
       0 43 300 ; 
       0 45 300 ; 
       0 46 300 ; 
       0 47 300 ; 
       0 48 300 ; 
       0 49 300 ; 
       0 50 300 ; 
       0 51 300 ; 
       0 52 300 ; 
       0 53 300 ; 
       0 54 300 ; 
       0 56 300 ; 
       0 57 300 ; 
       0 58 300 ; 
       0 59 300 ; 
       0 60 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 41.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -0.2749288 -0.03613019 0.1402338 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 30 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 32.5 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 40 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 42.5 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 45 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 50 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 60 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 62.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 65 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 67.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 70 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 72.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 75 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 10 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 77.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 80 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 12.5 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 20 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 25 -7.638298 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 82.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.2839766 -0.04110837 0.1433899 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       26 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 24 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 26.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 29 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 31.5 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 41.5 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 46.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 49 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 51.5 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 54 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 59 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 64 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 69 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 71.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 19 -9.638298 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 82.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
