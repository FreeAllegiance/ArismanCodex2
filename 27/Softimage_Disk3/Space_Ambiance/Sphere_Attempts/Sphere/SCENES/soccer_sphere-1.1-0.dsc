SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       1-cam_int1.1-0 ROOT ; 
       1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       1-four1.1-0 ; 
       1-four2.1-0 ; 
       1-four3.1-0 ; 
       1-four4.1-0 ; 
       1-four5.1-0 ; 
       1-four6.1-0 ; 
       1-leftovers.1-0 ; 
       1-leftovers1.1-0 ; 
       1-leftovers2.1-0 ; 
       1-leftovers3.1-0 ; 
       1-leftovers4.1-0 ; 
       1-leftovers5.1-0 ; 
       1-one1.1-0 ; 
       1-one2.1-0 ; 
       1-one3.1-0 ; 
       1-one4.1-0 ; 
       1-one5.1-0 ; 
       1-one6.1-0 ; 
       1-three1.1-0 ; 
       1-three2.1-0 ; 
       1-three3.1-0 ; 
       1-three4.1-0 ; 
       1-three5.1-0 ; 
       1-three6.1-0 ; 
       1-two1.1-0 ; 
       1-two2.1-0 ; 
       1-two3.1-0 ; 
       1-two4.1-0 ; 
       1-two5.1-0 ; 
       1-two6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       geo-geo.8-0 ROOT ; 
       geo4-geo.1-0 ROOT ; 
       geo5-geo.1-0 ROOT ; 
       geo6-geo.1-0 ROOT ; 
       geo7-geo.1-0 ROOT ; 
       geo8-geo.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       1-RENDERMAP.1-0 ; 
       1-RENDERMAP1.1-0 ; 
       1-RENDERMAP2.1-0 ; 
       1-RENDERMAP3.1-0 ; 
       1-RENDERMAP4.1-0 ; 
       1-RENDERMAP5.1-0 ; 
       1-t2d1.1-0 ; 
       1-t2d2.1-0 ; 
       1-t2d3.1-0 ; 
       1-t2d4.1-0 ; 
       1-t2d5.1-0 ; 
       1-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 8 300 ; 
       2 14 300 ; 
       2 26 300 ; 
       2 20 300 ; 
       2 2 300 ; 
       1 7 300 ; 
       1 13 300 ; 
       1 25 300 ; 
       1 19 300 ; 
       1 1 300 ; 
       3 9 300 ; 
       3 15 300 ; 
       3 27 300 ; 
       3 21 300 ; 
       3 3 300 ; 
       4 10 300 ; 
       4 16 300 ; 
       4 28 300 ; 
       4 22 300 ; 
       4 4 300 ; 
       5 11 300 ; 
       5 17 300 ; 
       5 29 300 ; 
       5 23 300 ; 
       5 5 300 ; 
       0 6 300 ; 
       0 12 300 ; 
       0 24 300 ; 
       0 18 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       12 0 401 ; 
       12 6 401 ; 
       13 1 401 ; 
       13 7 401 ; 
       14 2 401 ; 
       14 8 401 ; 
       15 3 401 ; 
       15 9 401 ; 
       16 4 401 ; 
       16 10 401 ; 
       17 5 401 ; 
       17 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 8.75 0 0 WIRECOL 1 7 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -6 0 WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -12 0 WIRECOL 7 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -18 0 WIRECOL 3 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 6.25 -24 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 6.25 -30 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 12.5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 1.25 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 7.5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 7.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1.25 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 7.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 10 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1.25 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 7.5 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -34 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 0 -34 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 0 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 0 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -28 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 0 -28 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
