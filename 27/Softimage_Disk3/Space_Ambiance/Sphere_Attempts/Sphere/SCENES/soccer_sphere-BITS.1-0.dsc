SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       BITS-cam_int1.1-0 ROOT ; 
       BITS-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       BITS-four1.1-0 ; 
       BITS-leftovers.1-0 ; 
       BITS-leftovers1.1-0 ; 
       BITS-leftovers2.1-0 ; 
       BITS-leftovers4.1-0 ; 
       BITS-leftovers5.1-0 ; 
       BITS-leftovers6.1-0 ; 
       BITS-one1.1-0 ; 
       BITS-one2.1-0 ; 
       BITS-one3.1-0 ; 
       BITS-one5.1-0 ; 
       BITS-one7.1-0 ; 
       BITS-three1.1-0 ; 
       BITS-two1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       geo-geo.8-0 ROOT ; 
       geo4-geo.2-0 ROOT ; 
       geo5-geo.2-0 ROOT ; 
       geo7-geo.2-0 ROOT ; 
       geo8-geo.2-0 ROOT ; 
       geo9-geo.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-BITS.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       BITS-RENDERMAP.1-0 ; 
       BITS-RENDERMAP1.1-0 ; 
       BITS-RENDERMAP2.1-0 ; 
       BITS-RENDERMAP4.1-0 ; 
       BITS-RENDERMAP6.1-0 ; 
       BITS-t2d1.1-0 ; 
       BITS-t2d2.1-0 ; 
       BITS-t2d3.1-0 ; 
       BITS-t2d5.1-0 ; 
       BITS-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       2 9 300 ; 
       1 2 300 ; 
       1 8 300 ; 
       3 4 300 ; 
       3 10 300 ; 
       4 5 300 ; 
       5 6 300 ; 
       5 11 300 ; 
       0 1 300 ; 
       0 7 300 ; 
       0 13 300 ; 
       0 12 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       7 5 401 ; 
       8 1 401 ; 
       8 6 401 ; 
       9 2 401 ; 
       9 7 401 ; 
       11 4 401 ; 
       11 9 401 ; 
       10 3 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 8.75 0 0 WIRECOL 1 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.25 -6 0 WIRECOL 2 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 6.25 -18 0 WIRECOL 3 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -24 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 23.75 0 0 WIRECOL 3 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 6.25 -30 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 12.5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.25 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -32 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 18.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.25 -20 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -26 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -34 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 0 -34 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 0 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 0 -22 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
