SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       NEW-cam_int1.1-0 ROOT ; 
       NEW-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       NEW-four1.1-0 ; 
       NEW-leftovers.1-0 ; 
       NEW-leftovers1.1-0 ; 
       NEW-leftovers2.1-0 ; 
       NEW-leftovers4.1-0 ; 
       NEW-leftovers5.1-0 ; 
       NEW-leftovers6.1-0 ; 
       NEW-one1.1-0 ; 
       NEW-one2.1-0 ; 
       NEW-one3.1-0 ; 
       NEW-one5.1-0 ; 
       NEW-one7.1-0 ; 
       NEW-three1.1-0 ; 
       NEW-two1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       geo-geo.8-0 ROOT ; 
       NEW-geo.1-0 ; 
       NEW-geo_1.2-0 ; 
       NEW-geo_2.2-0 ; 
       NEW-geo_3.2-0 ; 
       NEW-geo_4.2-0 ; 
       NEW-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-NEW.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       NEW-RENDERMAP.1-0 ; 
       NEW-RENDERMAP1.1-0 ; 
       NEW-RENDERMAP2.1-0 ; 
       NEW-RENDERMAP4.1-0 ; 
       NEW-RENDERMAP6.1-0 ; 
       NEW-t2d1.1-0 ; 
       NEW-t2d2.1-0 ; 
       NEW-t2d3.1-0 ; 
       NEW-t2d5.1-0 ; 
       NEW-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       5 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       1 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       2 9 300 ; 
       5 2 300 ; 
       5 8 300 ; 
       3 4 300 ; 
       3 10 300 ; 
       4 5 300 ; 
       1 6 300 ; 
       1 11 300 ; 
       0 1 300 ; 
       0 7 300 ; 
       0 13 300 ; 
       0 12 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 0 401 ; 
       7 5 401 ; 
       8 1 401 ; 
       8 6 401 ; 
       9 2 401 ; 
       9 7 401 ; 
       11 4 401 ; 
       11 9 401 ; 
       10 3 401 ; 
       10 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 22.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 41.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 47.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 36.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 40 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 28.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 3.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 13.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 37.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
