SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       BITS-cam_int1.2-0 ROOT ; 
       BITS-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       NEW-leftovers1.1-0 ; 
       NEW-leftovers2.1-0 ; 
       NEW-leftovers4.1-0 ; 
       NEW-leftovers5.1-0 ; 
       NEW-leftovers6.1-0 ; 
       NEW-one2.1-0 ; 
       NEW-one3.1-0 ; 
       NEW-one5.1-0 ; 
       NEW-one7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       NEW-geo.1-0 ; 
       NEW-geo_1.2-0 ; 
       NEW-geo_2.2-0 ; 
       NEW-geo_3.2-0 ; 
       NEW-geo_4.2-0 ; 
       NEW-null1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-NEW.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       NEW-RENDERMAP1.2-0 ; 
       NEW-RENDERMAP2.2-0 ; 
       NEW-RENDERMAP4.2-0 ; 
       NEW-RENDERMAP6.2-0 ; 
       NEW-t2d2.2-0 ; 
       NEW-t2d3.2-0 ; 
       NEW-t2d5.2-0 ; 
       NEW-t2d7.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       4 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       0 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       1 6 300 ; 
       4 0 300 ; 
       4 5 300 ; 
       2 2 300 ; 
       2 7 300 ; 
       3 3 300 ; 
       0 4 300 ; 
       0 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       5 4 401 ; 
       6 1 401 ; 
       6 5 401 ; 
       8 3 401 ; 
       8 7 401 ; 
       7 2 401 ; 
       7 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       5 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 28.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 3.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
