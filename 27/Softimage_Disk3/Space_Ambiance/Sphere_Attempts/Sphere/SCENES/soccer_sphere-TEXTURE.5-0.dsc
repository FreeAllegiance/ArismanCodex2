SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TEXTURE-cam_int1.5-0 ROOT ; 
       TEXTURE-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 7     
       TEXTURE-four1.2-0 ; 
       TEXTURE-leftovers.2-0 ; 
       TEXTURE-leftovers1.1-0 ; 
       TEXTURE-one1.2-0 ; 
       TEXTURE-one2.1-0 ; 
       TEXTURE-three1.2-0 ; 
       TEXTURE-two1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       geo-geo.7-0 ROOT ; 
       geo2-geo.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-TEXTURE.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       TEXTURE-RENDERMAP.4-0 ; 
       TEXTURE-RENDERMAP1.1-0 ; 
       TEXTURE-t2d1.2-0 ; 
       TEXTURE-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       1 4 300 ; 
       0 1 300 ; 
       0 3 300 ; 
       0 6 300 ; 
       0 5 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 0 401 ; 
       3 2 401 ; 
       4 1 401 ; 
       4 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 23.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 18.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
