SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TEXTURE-cam_int1.6-0 ROOT ; 
       TEXTURE-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       TEXTURE-four1.2-0 ; 
       TEXTURE-four2.1-0 ; 
       TEXTURE-leftovers.2-0 ; 
       TEXTURE-leftovers1.2-0 ; 
       TEXTURE-one1.2-0 ; 
       TEXTURE-one2.2-0 ; 
       TEXTURE-three1.2-0 ; 
       TEXTURE-three2.1-0 ; 
       TEXTURE-two1.2-0 ; 
       TEXTURE-two2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       geo-geo.8-0 ROOT ; 
       geo3-geo.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-TEXTURE.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       TEXTURE-RENDERMAP.5-0 ; 
       TEXTURE-RENDERMAP1.2-0 ; 
       TEXTURE-t2d1.3-0 ; 
       TEXTURE-t2d2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 3 300 ; 
       1 5 300 ; 
       1 9 300 ; 
       1 7 300 ; 
       1 1 300 ; 
       0 2 300 ; 
       0 4 300 ; 
       0 8 300 ; 
       0 6 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 0 401 ; 
       4 2 401 ; 
       5 1 401 ; 
       5 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 23.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 18.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
