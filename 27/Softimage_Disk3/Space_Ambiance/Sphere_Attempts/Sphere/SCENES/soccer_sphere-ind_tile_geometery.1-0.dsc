SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       BITS-cam_int1.3-0 ROOT ; 
       BITS-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 9     
       ind_tile_geometery-leftovers1.1-0 ; 
       ind_tile_geometery-leftovers2.1-0 ; 
       ind_tile_geometery-leftovers4.1-0 ; 
       ind_tile_geometery-leftovers5.1-0 ; 
       ind_tile_geometery-leftovers6.1-0 ; 
       ind_tile_geometery-one2.1-0 ; 
       ind_tile_geometery-one3.1-0 ; 
       ind_tile_geometery-one5.1-0 ; 
       ind_tile_geometery-one7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       ind_tile_geometery-geo.1-0 ; 
       ind_tile_geometery-geo_1.2-0 ; 
       ind_tile_geometery-geo_2.2-0 ; 
       ind_tile_geometery-geo_3.2-0 ; 
       ind_tile_geometery-geo_4.2-0 ; 
       ind_tile_geometery-null1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       F:/Sphere/PICTURES/clouds ; 
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-ind_tile_geometery.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ind_tile_geometery-RENDERMAP1.1-0 ; 
       ind_tile_geometery-RENDERMAP2.1-0 ; 
       ind_tile_geometery-RENDERMAP4.1-0 ; 
       ind_tile_geometery-RENDERMAP6.1-0 ; 
       ind_tile_geometery-t2d2.1-0 ; 
       ind_tile_geometery-t2d3.1-0 ; 
       ind_tile_geometery-t2d5.1-0 ; 
       ind_tile_geometery-t2d7.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       4 5 110 ; 
       2 5 110 ; 
       3 5 110 ; 
       0 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       1 6 300 ; 
       4 0 300 ; 
       4 5 300 ; 
       2 2 300 ; 
       2 7 300 ; 
       3 3 300 ; 
       0 4 300 ; 
       0 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       5 0 401 ; 
       5 4 401 ; 
       6 1 401 ; 
       6 5 401 ; 
       8 3 401 ; 
       8 7 401 ; 
       7 2 401 ; 
       7 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 WIRECOL 2 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       0 SCHEM 5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       5 SCHEM 17.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 28.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 3.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
