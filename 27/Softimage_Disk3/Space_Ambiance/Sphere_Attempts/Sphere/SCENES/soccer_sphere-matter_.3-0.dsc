SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       matter_-cam_int1.3-0 ROOT ; 
       matter_-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       matter_-default.2-0 ; 
       matter_-four1.1-0 ; 
       matter_-one1.2-0 ; 
       matter_-three1.2-0 ; 
       matter_-two1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       qq1-bmerge1.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-matter_.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 2 300 ; 
       0 4 300 ; 
       0 3 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -4 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
