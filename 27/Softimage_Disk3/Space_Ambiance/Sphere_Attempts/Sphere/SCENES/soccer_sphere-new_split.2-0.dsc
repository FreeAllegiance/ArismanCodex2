SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       new_split-cam_int1.2-0 ROOT ; 
       new_split-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       new_split-four3.1-0 ; 
       new_split-four4.1-0 ; 
       new_split-four5.1-0 ; 
       new_split-leftovers2.1-0 ; 
       new_split-leftovers3.1-0 ; 
       new_split-leftovers4.1-0 ; 
       new_split-one3.1-0 ; 
       new_split-one4.1-0 ; 
       new_split-one5.1-0 ; 
       new_split-three3.1-0 ; 
       new_split-three4.1-0 ; 
       new_split-three5.1-0 ; 
       new_split-two3.1-0 ; 
       new_split-two4.1-0 ; 
       new_split-two5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       geo4-geo.3-0 ROOT ; 
       geo5-geo.6-0 ROOT ; 
       geo6-geo.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       F:/Sphere/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-new_split.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       new_split-t2d1.1-0 ; 
       new_split-t2d2.1-0 ; 
       new_split-t2d3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       1 7 300 ; 
       1 13 300 ; 
       1 10 300 ; 
       1 1 300 ; 
       2 5 300 ; 
       2 8 300 ; 
       2 14 300 ; 
       2 11 300 ; 
       2 2 300 ; 
       0 3 300 ; 
       0 6 300 ; 
       0 12 300 ; 
       0 9 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 1 401 ; 
       6 0 401 ; 
       8 2 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 -0.6599998 0 0 0 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 -0.6599998 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 37.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
