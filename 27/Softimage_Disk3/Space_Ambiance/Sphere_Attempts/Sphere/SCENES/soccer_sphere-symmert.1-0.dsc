SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       symmert-cam_int1.1-0 ROOT ; 
       symmert-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       symmert-leftovers8.1-0 ; 
       symmert-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       symmert-bmerge1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       soccer_sphere-symmert.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 -0.04217577 0.08211088 0.2915492 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
