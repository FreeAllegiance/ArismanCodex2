SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       complete_tile-cam_int1.3-0 ROOT ; 
       complete_tile-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       complete_tile-bottom1.1-0 ; 
       complete_tile-quad1.1-0 ; 
       complete_tile-quad2.1-0 ; 
       complete_tile-quad3.1-0 ; 
       complete_tile-quad4.1-0 ; 
       complete_tile-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       complete_tile-sphere1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Sphere/PICTURES/newenv ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       take2-complete_tile.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       complete_tile-t2d1.3-0 ; 
       complete_tile-t2d2.3-0 ; 
       complete_tile-t2d3.3-0 ; 
       complete_tile-t2d4.3-0 ; 
       complete_tile-t2d5.3-0 ; 
       complete_tile-t2d6.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       4 5 401 ; 
       5 0 401 ; 
       0 1 401 ; 
       1 2 401 ; 
       2 3 401 ; 
       3 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 20 20 20 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       4 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
