SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.45-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       sphere-Default.14-0 ; 
       sphere-mat1.13-0 ; 
       sphere-mat2.10-0 ; 
       sphere-mat3.9-0 ; 
       sphere-mat4.6-0 ; 
       sphere-mat5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       sphere-dodeca1.39-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Sphere/PICTURES/environtest ; 
       D:/Sphere/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       test_2-sphere.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       sphere-t2d1.28-0 ; 
       sphere-t2d2.26-0 ; 
       sphere-t2d3.23-0 ; 
       sphere-t2d4.19-0 ; 
       sphere-t2d5.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.5233 0.2348956 0 USR DISPLAY 1 2 SRT 17.15 17.15 17.15 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.3063 5.448534 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 13.8063 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.3063 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 18.8063 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.3063 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 23.8063 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 13.8063 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.3063 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 18.8063 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 21.3063 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 23.8063 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 200 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
