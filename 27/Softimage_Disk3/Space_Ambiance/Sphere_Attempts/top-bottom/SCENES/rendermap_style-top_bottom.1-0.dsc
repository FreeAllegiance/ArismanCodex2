SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       TYPE2-cam_int1.8-0 ROOT ; 
       TYPE2-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       top_bottom-mat1.5-0 ; 
       top_bottom-mat2.4-0 ; 
       top_bottom-mat3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       top_bottom-Space.6-0 ROOT ; 
       top_bottom1-Space.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       F:/top-bottom/PICTURES/map ; 
       F:/top-bottom/PICTURES/map_light ; 
       F:/top-bottom/PICTURES/pieces ; 
       F:/top-bottom/PICTURES/strip ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       rendermap_style-top_bottom.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       top_bottom-t2d2.3-0 ; 
       top_bottom-t2d3.3-0 ; 
       top_bottom-t2d4.3-0 ; 
       top_bottom-t2d7.3-0 ; 
       top_bottom-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 1 400 ; 
       0 2 400 ; 
       0 4 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 18.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 30 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
