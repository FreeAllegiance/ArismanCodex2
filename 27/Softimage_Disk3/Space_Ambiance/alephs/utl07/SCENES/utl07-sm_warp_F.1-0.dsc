SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       utl07-utl07.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       Utl07-spot1.1-0 ; 
       Utl07-spot1_3.1-0 ; 
       Utl07-spot1_3_int.1-0 ROOT ; 
       Utl07-spot1_int.1-0 ROOT ; 
       Utl07-spot2.1-0 ; 
       Utl07-spot2_3.1-0 ; 
       Utl07-spot2_3_int.1-0 ROOT ; 
       Utl07-spot2_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 70     
       lg_gate_sAPTL-mat11.1-0 ; 
       lg_gate_sAPTL-mat12.1-0 ; 
       lg_gate_sAPTL-mat13.1-0 ; 
       lg_gate_sAPTL-mat14.1-0 ; 
       lg_gate_sAPTL-mat15.1-0 ; 
       lg_gate_sAPTL-mat16.1-0 ; 
       lg_gate_sAPTL-mat17.1-0 ; 
       lg_gate_sAPTL-mat18.1-0 ; 
       lg_gate_sAPTL-mat19.1-0 ; 
       lg_gate_sAPTL-mat20.1-0 ; 
       lg_gate_sAPTL-mat21.1-0 ; 
       lg_gate_sAPTL-mat22.1-0 ; 
       lg_gate_sAPTL-mat23.1-0 ; 
       lg_gate_sAPTL-mat25.1-0 ; 
       lg_gate_sAPTL-mat26.1-0 ; 
       lg_gate_sAPTL-mat27.1-0 ; 
       lg_gate_sAPTL-mat28.1-0 ; 
       lg_gate_sAPTL-mat29.1-0 ; 
       lg_gate_sAPTL-mat30.1-0 ; 
       lg_gate_sAPTL-mat31.1-0 ; 
       lg_gate_sAPTL-mat32.1-0 ; 
       lg_gate_sAPTL-mat33.1-0 ; 
       lg_gate_sAPTL-mat34.1-0 ; 
       lg_gate_sAPTL-mat35.1-0 ; 
       lg_gate_sAPTL-mat36.1-0 ; 
       lg_gate_sAPTL-mat37.1-0 ; 
       lg_gate_sAPTL-mat38.1-0 ; 
       lg_gate_sAPTL-mat39.1-0 ; 
       lg_gate_sAPTL-mat40.1-0 ; 
       lg_gate_sAPTL-mat41.1-0 ; 
       lg_gate_sAPTL-mat42.1-0 ; 
       lg_gate_sAPTL-mat43.1-0 ; 
       lg_gate_sAPTL-mat44.1-0 ; 
       lg_gate_sAPTL-mat45.1-0 ; 
       lg_gate_sAPTL-mat46.1-0 ; 
       lg_gate_sAPTL-mat47.1-0 ; 
       lg_gate_sAPTL-mat48.1-0 ; 
       lg_gate_sAPTL-mat49.1-0 ; 
       lg_gate_sAPTL-mat5_2.1-0 ; 
       lg_gate_sAPTL-mat50.1-0 ; 
       lg_gate_sAPTL-mat51.1-0 ; 
       lg_gate_sAPTL-mat6.1-0 ; 
       lg_gate_sAPTL-mat7.1-0 ; 
       lg_gate_sAPTL-y1.1-0 ; 
       sm_warp_F-port_red-left.1-0.1-0 ; 
       sm_warp_F-port_red-left.1-10.1-0 ; 
       sm_warp_F-port_red-left.1-11.1-0 ; 
       sm_warp_F-port_red-left.1-12.1-0 ; 
       sm_warp_F-port_red-left.1-13.1-0 ; 
       sm_warp_F-port_red-left.1-14.1-0 ; 
       sm_warp_F-port_red-left.1-15.1-0 ; 
       sm_warp_F-port_red-left.1-16.1-0 ; 
       sm_warp_F-port_red-left.1-17.1-0 ; 
       sm_warp_F-port_red-left.1-18.1-0 ; 
       sm_warp_F-port_red-left.1-19.1-0 ; 
       sm_warp_F-port_red-left.1-20.1-0 ; 
       sm_warp_F-port_red-left.1-21.1-0 ; 
       sm_warp_F-port_red-left.1-22.1-0 ; 
       sm_warp_F-port_red-left.1-23.1-0 ; 
       sm_warp_F-port_red-left.1-24.1-0 ; 
       sm_warp_F-port_red-left.1-25.1-0 ; 
       sm_warp_F-port_red-left.1-4.1-0 ; 
       sm_warp_F-port_red-left.1-5.1-0 ; 
       sm_warp_F-port_red-left.1-7.1-0 ; 
       sm_warp_F-port_red-left.1-8.1-0 ; 
       sm_warp_F-port_red-left.1-9.1-0 ; 
       sm_warp_F-starbord_green-right.1-0.1-0 ; 
       Utl07-mat2.1-0 ; 
       Utl07-mat3.1-0 ; 
       Utl07-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       utl07-antenn.1-0 ; 
       utl07-antenn0.1-0 ; 
       utl07-bantenn.1-0 ; 
       utl07-barmour.1-0 ; 
       utl07-beacon.5-0 ; 
       utl07-fuselg.1-0 ; 
       utl07-fuselg0.5-0 ; 
       utl07-SS0a.1-0 ; 
       utl07-SS0b.1-0 ; 
       utl07-SS0c.1-0 ; 
       utl07-SS0d.1-0 ; 
       utl07-SS1.1-0 ; 
       utl07-SS2.1-0 ; 
       utl07-SS3.1-0 ; 
       utl07-SSa1.1-0 ; 
       utl07-SSa2.1-0 ; 
       utl07-SSa3.1-0 ; 
       utl07-SSa4.1-0 ; 
       utl07-SSa5.1-0 ; 
       utl07-SSb1.1-0 ; 
       utl07-SSb2.1-0 ; 
       utl07-SSb3.1-0 ; 
       utl07-SSb4.1-0 ; 
       utl07-SSb5.1-0 ; 
       utl07-SSc1.1-0 ; 
       utl07-SSc2.1-0 ; 
       utl07-SSc3.1-0 ; 
       utl07-SSc4.1-0 ; 
       utl07-SSc5.1-0 ; 
       utl07-SSd1.1-0 ; 
       utl07-SSd2.1-0 ; 
       utl07-SSd3.1-0 ; 
       utl07-SSd4.1-0 ; 
       utl07-SSd5.1-0 ; 
       utl07-tarmour.1-0 ; 
       utl07-tractr1.1-0 ; 
       utl07-tractr1a.1-0 ; 
       utl07-tractr1b.1-0 ; 
       utl07-tractr2.1-0 ; 
       utl07-tractr2a.1-0 ; 
       utl07-tractr2b.1-0 ; 
       utl07-tractr3.1-0 ; 
       utl07-tractr3a.1-0 ; 
       utl07-tractr3b.1-0 ; 
       utl07-tractr4.1-0 ; 
       utl07-tractr4a.1-0 ; 
       utl07-tractr4b.1-0 ; 
       utl07-utl07.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/alephs/utl07/PICTURES/utl07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       utl07-sm_warp_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 38     
       lg_gate_sAPTL-t2d1.1-0 ; 
       lg_gate_sAPTL-t2d10.1-0 ; 
       lg_gate_sAPTL-t2d11.1-0 ; 
       lg_gate_sAPTL-t2d12.1-0 ; 
       lg_gate_sAPTL-t2d13.1-0 ; 
       lg_gate_sAPTL-t2d14.1-0 ; 
       lg_gate_sAPTL-t2d15.1-0 ; 
       lg_gate_sAPTL-t2d17.1-0 ; 
       lg_gate_sAPTL-t2d18.1-0 ; 
       lg_gate_sAPTL-t2d19.1-0 ; 
       lg_gate_sAPTL-t2d20.1-0 ; 
       lg_gate_sAPTL-t2d21.1-0 ; 
       lg_gate_sAPTL-t2d22.1-0 ; 
       lg_gate_sAPTL-t2d23.1-0 ; 
       lg_gate_sAPTL-t2d24.1-0 ; 
       lg_gate_sAPTL-t2d25.1-0 ; 
       lg_gate_sAPTL-t2d26.1-0 ; 
       lg_gate_sAPTL-t2d27.1-0 ; 
       lg_gate_sAPTL-t2d28.1-0 ; 
       lg_gate_sAPTL-t2d29.1-0 ; 
       lg_gate_sAPTL-t2d30.1-0 ; 
       lg_gate_sAPTL-t2d31.1-0 ; 
       lg_gate_sAPTL-t2d32.1-0 ; 
       lg_gate_sAPTL-t2d33.1-0 ; 
       lg_gate_sAPTL-t2d34.1-0 ; 
       lg_gate_sAPTL-t2d35.1-0 ; 
       lg_gate_sAPTL-t2d36.1-0 ; 
       lg_gate_sAPTL-t2d37.1-0 ; 
       lg_gate_sAPTL-t2d38.1-0 ; 
       lg_gate_sAPTL-t2d39.1-0 ; 
       lg_gate_sAPTL-t2d40.1-0 ; 
       lg_gate_sAPTL-t2d41.1-0 ; 
       lg_gate_sAPTL-t2d42.1-0 ; 
       lg_gate_sAPTL-t2d43.1-0 ; 
       lg_gate_sAPTL-t2d6.1-0 ; 
       lg_gate_sAPTL-t2d7.1-0 ; 
       lg_gate_sAPTL-t2d8.1-0 ; 
       lg_gate_sAPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 34 110 ; 
       12 0 110 ; 
       13 2 110 ; 
       14 7 110 ; 
       15 7 110 ; 
       16 7 110 ; 
       17 7 110 ; 
       18 7 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 8 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 9 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 9 110 ; 
       28 9 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 10 110 ; 
       32 10 110 ; 
       33 10 110 ; 
       0 1 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       4 1 110 ; 
       5 6 110 ; 
       6 47 110 ; 
       34 1 110 ; 
       35 47 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 47 110 ; 
       39 38 110 ; 
       40 38 110 ; 
       41 47 110 ; 
       42 41 110 ; 
       43 41 110 ; 
       44 47 110 ; 
       45 44 110 ; 
       46 44 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       11 61 300 ; 
       12 66 300 ; 
       13 44 300 ; 
       14 51 300 ; 
       15 52 300 ; 
       16 53 300 ; 
       17 54 300 ; 
       18 55 300 ; 
       19 56 300 ; 
       20 57 300 ; 
       21 58 300 ; 
       22 59 300 ; 
       23 60 300 ; 
       24 62 300 ; 
       25 63 300 ; 
       26 64 300 ; 
       27 65 300 ; 
       28 45 300 ; 
       29 46 300 ; 
       30 47 300 ; 
       31 48 300 ; 
       32 49 300 ; 
       33 50 300 ; 
       0 68 300 ; 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 35 300 ; 
       2 38 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 25 300 ; 
       2 43 300 ; 
       2 34 300 ; 
       3 69 300 ; 
       3 6 300 ; 
       3 9 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       3 32 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 29 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       34 67 300 ; 
       34 3 300 ; 
       34 4 300 ; 
       34 5 300 ; 
       34 28 300 ; 
       34 33 300 ; 
       35 13 300 ; 
       35 40 300 ; 
       36 15 300 ; 
       37 14 300 ; 
       38 22 300 ; 
       38 39 300 ; 
       39 24 300 ; 
       40 23 300 ; 
       41 19 300 ; 
       41 37 300 ; 
       42 21 300 ; 
       43 20 300 ; 
       44 16 300 ; 
       44 36 300 ; 
       45 18 300 ; 
       46 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       47 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 3 2110 ; 
       1 2 2110 ; 
       4 7 2110 ; 
       5 6 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 34 401 ; 
       2 35 401 ; 
       4 36 401 ; 
       5 37 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       15 9 401 ; 
       16 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       39 32 401 ; 
       40 33 401 ; 
       42 0 401 ; 
       43 20 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 2.254192 -3.718217 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -0.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM -1.245805 -3.718217 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.254192 -7.718213 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.254192 -5.718217 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM -1.245805 -5.718217 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM -1.245805 -7.718213 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 17.77754 -28.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 17.77754 -38.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 17.77754 -8.620174 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 17.77754 -18.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 21.27754 -52.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 21.27754 -50.62017 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 21.27754 -48.62017 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 21.27754 -24.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 21.27754 -26.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 21.27754 -28.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 21.27754 -30.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 21.27754 -32.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 21.27754 -34.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 21.27754 -36.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 21.27754 -38.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 21.27754 -40.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 21.27754 -42.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 21.27754 -4.620174 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 21.27754 -6.620174 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 21.27754 -8.620174 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 21.27754 -10.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 21.27754 -12.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 21.27754 -14.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 21.27754 -16.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 21.27754 -18.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 21.27754 -20.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 21.27754 -22.62017 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 17.77754 -50.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 14.27754 -48.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 17.77754 -48.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 17.77754 -46.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 17.77754 -44.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 14.27754 -23.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 10.77754 -28.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 17.77754 -52.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 10.77754 -63.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 14.27754 -62.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 14.27754 -64.62018 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 10.77754 -67.62018 0 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 14.27754 -66.62018 0 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 14.27754 -68.62018 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 10.77754 -59.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 14.27754 -58.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 14.27754 -60.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 10.77754 -55.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 14.27754 -54.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 14.27754 -56.62017 0 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 7.27754 -36.62017 0 USR DISPLAY 3 2 SRT 1 1 1 0 8.742289e-008 1.061083e-013 -9.313226e-010 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       67 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14.27754 -60.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 17.77754 -64.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 17.77754 -62.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14.27754 -52.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 17.77754 -56.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 17.77754 -54.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14.27754 -56.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 17.77754 -60.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.77754 -58.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14.27754 -64.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 17.77754 -68.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 17.77754 -66.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.27754 -44.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.27754 -44.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 21.27754 -44.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.27754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.27754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14.27754 -52.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14.27754 -56.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14.27754 -64.87018 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14.27754 -60.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 17.77754 -2.870174 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 17.77754 -2.870174 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.27754 -46.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24.77754 -48.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 24.77754 -12.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 24.77754 -14.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 24.77754 -16.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 24.77754 -18.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 24.77754 -20.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 24.77754 -22.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24.77754 -24.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 24.77754 -26.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 24.77754 -28.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 24.77754 -30.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 24.77754 -32.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24.77754 -34.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24.77754 -36.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24.77754 -38.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24.77754 -40.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24.77754 -42.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 24.77754 -52.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 24.77754 -4.870174 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 24.77754 -6.620174 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 24.77754 -8.620174 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 24.77754 -10.62017 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 24.77754 -50.87017 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 21.27754 -2.870174 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 24.77754 -48.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 24.77754 -48.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.77754 -60.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.27754 -64.87018 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 21.27754 -62.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.77754 -52.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 21.27754 -56.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.27754 -54.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.77754 -56.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.27754 -60.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.27754 -58.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 17.77754 -64.87018 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.27754 -68.87018 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.27754 -66.87018 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24.77754 -44.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24.77754 -50.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 24.77754 -44.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 24.77754 -50.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24.77754 -46.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 24.77754 -48.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 17.77754 -52.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 17.77754 -56.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 17.77754 -64.87018 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 17.77754 -60.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 24.77754 -48.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 24.77754 -48.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 24.77754 -50.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 24.77754 -50.87017 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 10.77754 -2.870174 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 17 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
