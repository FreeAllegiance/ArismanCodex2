SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shpere-cam_int1.15-0 ROOT ; 
       shpere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       shpere-mat1.3-0 ; 
       shpere-mat2.3-0 ; 
       shpere-mat5.3-0 ; 
       shpere-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 2     
       shpere-Static_Cling1.2-0 ; 
       shpere-Static_Cling4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       mapped_Polys-mapped_polys.4-0 ROOT ; 
       nurbs-nurbs.5-0 ROOT ; 
       reciever-reciever.3-0 ROOT ; 
       sphere_poly-sphere-poly.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/build_effect/PICTURES/build1 ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/build_effect/PICTURES/rendermap ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/build_effect/PICTURES/rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       build-shpere.15-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       shpere-t2d1.3-0 ; 
       shpere-t2d2.3-0 ; 
       shpere-t2d3.5-0 ; 
       shpere-t2d4.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       1 1 300 ; 
       0 2 300 ; 
       2 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       1 0 550 ; 
       2 1 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
