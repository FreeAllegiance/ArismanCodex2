SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       chain-cam_int1.4-0 ROOT ; 
       chain-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       chain-inf_light1.4-0 ROOT ; 
       chain-inf_light2.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       chain2-mat2.2-0 ; 
       chain2-mat3.3-0 ; 
       chain2-mat4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       1-first.1-0 ROOT ; 
       2-mid.1-0 ROOT ; 
       3-bgrnd54.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       D:/Pete_Data/Softimage/meteors/Gold_Rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/Gold_Rock/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/Gold_Rock/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/Gold_Rock/PICTURES/map2_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd56-chain2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       chain2-model-3DRock_Metal_gold1.1-2.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       chain2-map1.3-0 ; 
       chain2-map1_light1.2-0 ; 
       chain2-map2.3-0 ; 
       chain2-map2_light.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       chain2-rock2.3-0 ; 
       chain2-t3d2.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 2 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 1 500 ; 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 2 401 ; 
       1 1 401 ; 
       2 3 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 1 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 21 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 18.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.25 0 0 WIRECOL 1 7 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 16 0 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 4.75 0 0 WIRECOL 1 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
