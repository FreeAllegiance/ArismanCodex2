SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.7-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.6-0 ROOT ; 
       Lighting_Template-inf_light2.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       chain-default1.5-0 ; 
       chain-default2.6-0 ; 
       chain-default3.2-0 ; 
       Relight-default3.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       chain-step1.5-0 ROOT ; 
       chain1-step2.6-0 ROOT ; 
       chain2-step3.8-0 ROOT ; 
       chain2-step3_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 10     
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/bgrnd52 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/top_cap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Relight-Relight.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       chain-bottom_of_Craters2.4-0 ; 
       chain-inside_large_crater1.4-0 ; 
       chain-large_crater1.4-0 ; 
       chain-main_features2.4-0 ; 
       chain-map1.2-0 ; 
       chain-map1_light1.7-0 ; 
       chain-map2.7-0 ; 
       chain-map2_light.2-0 ; 
       chain-map7.1-0 ; 
       chain-top_cap.5-0 ; 
       Relight-map2_light.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       chain-marbrevert1.5-0 ; 
       chain-rock2.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       3 3 300 ; 
       0 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 10 400 ; 
       0 2 400 ; 
       0 1 400 ; 
       0 3 400 ; 
       0 0 400 ; 
       1 9 400 ; 
       1 5 400 ; 
       2 7 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 1 500 ; 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 6 401 ; 
       2 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 33.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 21.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 28.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       3 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       10 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
