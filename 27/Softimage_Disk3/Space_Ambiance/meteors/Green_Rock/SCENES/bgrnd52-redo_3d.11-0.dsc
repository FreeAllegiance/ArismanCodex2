SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       reverse_uv-cam_int1.26-0 ROOT ; 
       reverse_uv-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       redo_3d-light1.8-0 ROOT ; 
       redo_3d-light2.6-0 ROOT ; 
       redo_3d-light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       redo_3d-mat5.3-0 ; 
       redo_3d-mat6.2-0 ; 
       reverse_uv-mat4.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       1-1.2-0 ROOT ; 
       2-2.11-0 ROOT ; 
       3-3.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map3 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map3_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map4 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map4_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/primary_rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd52-redo_3d.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       redo_3d-redo_top_global-reverse_uv-3DRock_Lava1.1-1.1-1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       redo_3d-map_3_light.8-0 ; 
       redo_3d-map_4_light.1-0 ; 
       redo_3d-map3.1-0 ; 
       redo_3d-map4.2-0 ; 
       redo_3d-top_caps3.1-0 ; 
       reverse_uv-map_2_light.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       redo_3d-t3d1.8-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 0 300 ; 
       2 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       0 3 401 ; 
       2 5 401 ; 
       2 2 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 1 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
