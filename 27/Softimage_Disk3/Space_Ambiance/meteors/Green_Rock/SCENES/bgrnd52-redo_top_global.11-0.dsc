SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       reverse_uv-cam_int1.13-0 ROOT ; 
       reverse_uv-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       redo_top_global-mat5.2-0 ; 
       reverse_uv-mat4.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       text3-bgrnd52_3.10-0 ROOT ; 
       text5-bgrnd52_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map3 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/map3_light ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/primary_rendermap_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd52-redo_top_global.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       redo_top_global-map_3_light.2-0 ; 
       redo_top_global-map3.3-0 ; 
       redo_top_global-top_caps3.3-0 ; 
       reverse_uv-map_2_light.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 3 401 ; 
       1 1 401 ; 
       0 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 3.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 11.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 WIRECOL 10 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
