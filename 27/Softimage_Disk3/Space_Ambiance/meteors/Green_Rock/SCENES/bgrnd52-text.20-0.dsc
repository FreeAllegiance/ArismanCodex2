SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       text-cam_int1.20-0 ROOT ; 
       text-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       text-inf_light1.20-0 ROOT ; 
       text-inf_light2.20-0 ROOT ; 
       text-inf_light3.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       text-mat1.14-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       text-bgrnd52.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/main_craters ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/moon3 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/moon6 ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/primary_rendermap ; 
       D:/Pete_Data/Softimage/meteors/Green_Rock/PICTURES/smaller_craters ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd52-text.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       text-setup-3DRock_Rough1.1-1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       text-main_craters.10-0 ; 
       text-moon_surface_1.7-0 ; 
       text-moon3_surface1.8-0 ; 
       text-primary_rendermap.5-0 ; 
       text-smaller_craters.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       text-rock1.17-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 0 400 ; 
       0 4 400 ; 
       0 2 400 ; 
       0 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
