SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       3d-cam_int1.6-0 ROOT ; 
       3d-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       3d-OTHERS-seawater.1-0.4-0 ; 
       3d-OTHERS-seawater.1-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 2     
       3d-Static_Cling1.1-0 ; 
       3d-Static_Cling2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       3d-sphere1.6-0 ROOT ; 
       3d1-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mesh-3d.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 2     
       3d-map1.2-0 ; 
       3d-map1_light.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       3d-default.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
       1 1 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
