SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       nurbs-cam_int1.67-0 ROOT ; 
       nurbs-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.16-0 ROOT ; 
       Lighting_Template-inf_light2.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       UV-default6.2-0 ; 
       UV-default7.5-0 ; 
       UV-default8.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       nurbs-nurbs3.41-0 ROOT ; 
       nurbs4-nurbs3.8-0 ROOT ; 
       nurbs5-nurbs3.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/Top-cap ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/reflection ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       relight-UV.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       UV-3D_Projection5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       UV-map3.2-0 ; 
       UV-t2d2.2-0 ; 
       UV-t2d3.6-0 ; 
       UV-t2d4.4-0 ; 
       UV-t2d5.4-0 ; 
       UV-t2d7.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       UV-t3d5.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       2 5 400 ; 
       0 1 400 ; 
       1 2 400 ; 
       1 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 4 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 18.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
