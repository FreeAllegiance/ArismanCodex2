SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.25-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1_1.5-0 ROOT ; 
       Lighting_Template-inf_light2_1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       sphere-default11.8-0 ; 
       sphere-default12.5-0 ; 
       sphere-mat1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       sphere-sphere10.13-0 ROOT ; 
       sphere-sphere13.5-0 ROOT ; 
       sphere2-sphere10.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/MoonBump ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/MoonMap ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/bgrnd55 ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/Water_Rock/PICTURES/map1_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       relight-sphere.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       sphere-3D_Projection7.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       sphere-map4.10-0 ; 
       sphere-map5.6-0 ; 
       sphere-MoonBump1.7-0 ; 
       sphere-MoonMap.7-0 ; 
       sphere-t2d5.6-0 ; 
       sphere-t2d6.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       sphere-ice.4-0 ; 
       sphere-ice1.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 2 300 ; 
       2 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 3 400 ; 
       0 2 400 ; 
       1 5 400 ; 
       2 4 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 1 500 ; 
       2 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 0 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
