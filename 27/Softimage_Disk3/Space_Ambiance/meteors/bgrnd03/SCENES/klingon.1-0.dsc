SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       klingon-BODY01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       klingon-cam_int1.1-0 ROOT ; 
       klingon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       klingon-Light01.1-0 ROOT ; 
       klingon-Light02.1-0 ROOT ; 
       klingon-Light02.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       klingon-CHROME_BLUE_SKY.1-0 ; 
       klingon-CHROME_BLUE_SKY01.1-0 ; 
       klingon-CHROME_CHAOS_SKY.1-0 ; 
       klingon-CHROME_PEARL.1-0 ; 
       klingon-CHROME_PEARL01.1-0 ; 
       klingon-GRAY_SEMIGLOSS.1-0 ; 
       klingon-GRAY_SEMIGLOSS01.1-0 ; 
       klingon-GRAY_SEMIGLOSS02.1-0 ; 
       klingon-GRAY_SEMIGLOSS03.1-0 ; 
       klingon-GRAY_SEMIGLOSS04.1-0 ; 
       klingon-GREEN_GLASS.1-0 ; 
       klingon-GREEN_NEON.1-0 ; 
       klingon-GREEN_NEON01.1-0 ; 
       klingon-ORANGE_PLASTIC.1-0 ; 
       klingon-RED_NEON.1-0 ; 
       klingon-RED_NEON01.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       klingon-BODY01.1-0 ROOT ; 
       klingon-COOLER01.1-0 ROOT ; 
       klingon-coolerfrnt.1-0 ROOT ; 
       klingon-coolerside.1-0 ROOT ; 
       klingon-ENGINE01.1-0 ROOT ; 
       klingon-HEAD01.1-0 ROOT ; 
       klingon-Headwindow.1-0 ROOT ; 
       klingon-necklite-t.1-0 ROOT ; 
       klingon-neck_supp.1-0 ROOT ; 
       klingon-NECK01.1-0 ROOT ; 
       klingon-necklite_s.1-0 ROOT ; 
       klingon-Nosesensor.1-0 ROOT ; 
       klingon-Side_logos.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       klingon.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       klingon-cloud.gif.reflect.1-0 ; 
       klingon-house2_l.tga.reflect.1-0 ; 
       klingon-house2_l.tga.reflect01.1-0 ; 
       klingon-sky.gif.reflect.1-0 ; 
       klingon-sky.gif.reflect01.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       8 0 300 ; 
       3 3 300 ; 
       1 5 300 ; 
       12 13 300 ; 
       7 14 300 ; 
       10 11 300 ; 
       5 6 300 ; 
       11 2 300 ; 
       6 10 300 ; 
       9 7 300 ; 
       2 4 300 ; 
       4 8 300 ; 
       4 1 300 ; 
       4 15 300 ; 
       4 12 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       2 1 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
       3 1 401 ; 
       2 0 401 ; 
       4 2 401 ; 
       1 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 0 0 DISPLAY 0 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       2 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
       5 SCHEM 0 0 0 ; 
       13 SCHEM 0 0 0 ; 
       14 SCHEM 0 0 0 ; 
       11 SCHEM 0 0 0 ; 
       6 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       10 SCHEM 0 0 0 ; 
       7 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
       8 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       15 SCHEM 0 0 0 ; 
       12 SCHEM 0 0 0 ; 
       9 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       0 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       4 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 0 100 0 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
