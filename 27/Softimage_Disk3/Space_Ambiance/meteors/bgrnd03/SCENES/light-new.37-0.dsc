SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.23-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.22-0 ROOT ; 
       Lighting_Template-inf_light2.22-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       new-mat1.10-0 ; 
       new-mat2.10-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       1-skin2.12-0 ROOT ; 
       2-skin2.4-0 ROOT ; 
       new-circle1.1-0 ; 
       new-circle12.1-0 ; 
       new-circle13.1-0 ; 
       new-circle14.1-0 ; 
       new-circle15.1-0 ; 
       new-circle16.1-0 ; 
       new-circle17.1-0 ; 
       new-circle18.1-0 ; 
       new-circle19.1-0 ; 
       new-circle20.1-0 ; 
       new-circle21.1-0 ; 
       new-circle22.1-0 ; 
       new-circle23.1-0 ; 
       new-circle24.1-0 ; 
       new-null2.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/MoonBump ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/MoonMap ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/bgrnd03 ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/bottom_of_features ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/bgrnd03/PICTURES/map1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       light-new.37-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       new-FeatureBumps1.6-0 ; 
       new-FeatureMap1.6-0 ; 
       new-Inside_large_Crater1.11-0 ; 
       new-Large_Crater1.11-0 ; 
       new-MoonBump.11-0 ; 
       new-MoonMap.11-0 ; 
       new-Shell1.11-0 ; 
       new-t2d1.25-0 ; 
       new-t2d2.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       new-rock01.12-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 16 110 ; 
       3 16 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 16 110 ; 
       9 16 110 ; 
       10 16 110 ; 
       11 16 110 ; 
       12 16 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 5 400 ; 
       0 6 400 ; 
       0 3 400 ; 
       0 2 400 ; 
       0 0 400 ; 
       0 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 60 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.1480889 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 40 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 WIRECOL 10 7 DISPLAY 1 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 42.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
