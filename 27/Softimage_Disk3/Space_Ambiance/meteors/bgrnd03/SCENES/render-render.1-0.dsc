SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.27-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.26-0 ROOT ; 
       Lighting_Template-inf_light2.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       render-mat1.1-0 ; 
       render-mat2.1-0 ; 
       render-mat3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       1-skin2.13-0 ROOT ; 
       2-skin2.6-0 ROOT ; 
       render-circle1.1-0 ; 
       render-circle12.1-0 ; 
       render-circle13.1-0 ; 
       render-circle14.1-0 ; 
       render-circle15.1-0 ; 
       render-circle16.1-0 ; 
       render-circle17.1-0 ; 
       render-circle18.1-0 ; 
       render-circle19.1-0 ; 
       render-circle20.1-0 ; 
       render-circle21.1-0 ; 
       render-circle22.1-0 ; 
       render-circle23.1-0 ; 
       render-circle24.1-0 ; 
       render-low.1-0 ROOT ; 
       render-null2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/MoonBump ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/MoonMap ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/bottom_of_features ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/inside_large_crater ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/large crater ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/main_feature ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/main_shell ; 
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/meteors/bgrnd03/PICTURES/map1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       render-render.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       render-FeatureBumps1.1-0 ; 
       render-FeatureMap1.1-0 ; 
       render-Inside_large_Crater1.1-0 ; 
       render-Large_Crater1.1-0 ; 
       render-MoonBump.1-0 ; 
       render-MoonMap.1-0 ; 
       render-Shell1.1-0 ; 
       render-t2d1.1-0 ; 
       render-t2d2.1-0 ; 
       render-t2d3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       render-rock01.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 17 110 ; 
       3 17 110 ; 
       4 17 110 ; 
       5 17 110 ; 
       6 17 110 ; 
       7 17 110 ; 
       8 17 110 ; 
       9 17 110 ; 
       10 17 110 ; 
       11 17 110 ; 
       12 17 110 ; 
       13 17 110 ; 
       14 17 110 ; 
       15 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
       16 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 5 400 ; 
       0 6 400 ; 
       0 3 400 ; 
       0 2 400 ; 
       0 0 400 ; 
       0 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 8 401 ; 
       2 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 65 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 67.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 60 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 62.5 0 0 DISPLAY 0 0 SRT 1.278 0.652 0.8976802 0 0 0 0.294686 -0.01949239 0.09203742 MPRFLG 0 ; 
       17 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0.1480889 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 37.5 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 60 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 55 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 57.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 40 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 45 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 62.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 42.5 -2 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
