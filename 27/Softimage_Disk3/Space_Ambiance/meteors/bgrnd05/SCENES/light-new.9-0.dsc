SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.9-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.9-0 ROOT ; 
       Lighting_Template-inf_light2.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       new-default1.3-0 ; 
       new-default2.2-0 ; 
       new-mat1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       new-low.2-0 ROOT ; 
       new-sphere2.5-0 ROOT ; 
       new1-sphere2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/MoonBump ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/MoonMap ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/bgrnd05 ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/bottom_of_features ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/bgrnd05/PICTURES/map1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       light-new.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       new-FeatureBumps2.4-0 ; 
       new-FeatureMap2.4-0 ; 
       new-Inside_large_Crater2.4-0 ; 
       new-Large_Crater2.4-0 ; 
       new-MoonBump1.4-0 ; 
       new-MoonMap1.4-0 ; 
       new-Shell2.4-0 ; 
       new-t2d4.4-0 ; 
       new-t2d5.4-0 ; 
       new-t2d6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       new-rock2.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       2 1 300 ; 
       0 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 4 400 ; 
       1 5 400 ; 
       1 6 400 ; 
       1 3 400 ; 
       1 2 400 ; 
       1 0 400 ; 
       1 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 8 401 ; 
       2 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 32.5 0 0 DISPLAY 1 2 SRT 1.26 1.26 1.26 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
