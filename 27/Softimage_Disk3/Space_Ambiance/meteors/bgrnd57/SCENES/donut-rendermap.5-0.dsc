SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.5-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       rendermap-inf_light1.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       rendermap-cnostant.1-0 ; 
       rendermap-cnostant1.1-0 ; 
       rendermap-mat1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       base2-low_def_tarus.4-0 ROOT ; 
       bgrnd57-poly.4-0 ROOT ; 
       rendermap-Hi-def_Tarus.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/MoonBump ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/bgrnd57 ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/bottom_of_features ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/main_feature ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/main_shell ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/map1 ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/map1_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       donut-rendermap.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       rendermap-bgrnd57.2-0 ; 
       rendermap-grey.1-0 ; 
       rendermap-inside_large_craters1.1-0 ; 
       rendermap-large_Craters.1-0 ; 
       rendermap-rendermap1.1-0 ; 
       rendermap-shell.1-0 ; 
       rendermap-small_craters1.1-0 ; 
       rendermap-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
       2 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 7 400 ; 
       1 0 400 ; 
       2 3 400 ; 
       2 6 400 ; 
       2 2 400 ; 
       2 1 400 ; 
       2 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 23.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 -1.6464 -0.3397607 -1.883208 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
