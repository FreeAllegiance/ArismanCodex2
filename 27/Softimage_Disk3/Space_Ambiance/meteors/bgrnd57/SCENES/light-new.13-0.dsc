SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       corphq3-cam_int1.18-0 ROOT ; 
       corphq3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.13-0 ROOT ; 
       Lighting_Template-inf_light2.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       new-cnostant2.2-0 ; 
       rendermap-cnostant.1-0 ; 
       rendermap-cnostant1.1-0 ; 
       rendermap-mat1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       base2-low_def_tarus.7-0 ROOT ; 
       bgrnd57-poly.7-0 ROOT ; 
       bgrnd58-low.2-0 ROOT ; 
       rendermap-Hi-def_Tarus.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/MoonBump ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/bgrnd57 ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/inside_large_crater ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/main_feature ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/main_shell ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/map1 ; 
       E:/Pete_Data2/meteors/bgrnd57/PICTURES/map1_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       light-new.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 9     
       new-bgrnd58.2-0 ; 
       rendermap-bgrnd57.4-0 ; 
       rendermap-grey.3-0 ; 
       rendermap-inside_large_craters1.3-0 ; 
       rendermap-large_Craters.3-0 ; 
       rendermap-rendermap1.3-0 ; 
       rendermap-shell.3-0 ; 
       rendermap-small_craters1.3-0 ; 
       rendermap-t2d2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 2 300 ; 
       1 1 300 ; 
       3 3 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 8 400 ; 
       1 1 400 ; 
       3 4 400 ; 
       3 7 400 ; 
       3 3 400 ; 
       3 2 400 ; 
       3 6 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       3 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 18.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 1.268 0 0 -1.6464 -0.3397607 -1.883208 MPRFLG 0 ; 
       2 SCHEM 23.75 0 0 DISPLAY 1 2 SRT 1.27 1.27 1.27 -1.236 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       1 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 40 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
