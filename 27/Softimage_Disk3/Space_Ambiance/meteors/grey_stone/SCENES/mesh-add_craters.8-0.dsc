SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_craters-cam_int1.8-0 ROOT ; 
       add_craters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_craters-inf_light1.8-0 ROOT ; 
       add_craters-inf_light2.8-0 ROOT ; 
       add_craters-inf_light3.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       add_craters-mat1.1-0 ; 
       add_craters-mat2.1-0 ; 
       add_craters-mat3.3-0 ; 
       add_craters-mat4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       3d_global1-map1.3-0 ROOT ; 
       3d_global2-map1.8-0 ROOT ; 
       3d_global3-map1.1-0 ROOT ; 
       add_craters-map1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 9     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map3 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mesh-add_craters.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       add_craters-3D_Cloud1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       add_craters-bottom_of_Craters1.4-0 ; 
       add_craters-bottom_of_Craters2.1-0 ; 
       add_craters-inside_large_crater.5-0 ; 
       add_craters-inside_large_crater1.1-0 ; 
       add_craters-large_crater.5-0 ; 
       add_craters-large_crater1.1-0 ; 
       add_craters-main_features1.4-0 ; 
       add_craters-main_features2.1-0 ; 
       add_craters-map1.1-0 ; 
       add_craters-map1_light.1-0 ; 
       add_craters-map2.1-0 ; 
       add_craters-map2_light.5-0 ; 
       add_craters-map2_light1.1-0 ; 
       add_craters-map3.1-0 ; 
       add_craters-map4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       add_craters-3Dtexture1.1-0 ; 
       add_craters-rock1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       2 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 4 400 ; 
       1 2 400 ; 
       1 6 400 ; 
       1 0 400 ; 
       2 5 400 ; 
       2 3 400 ; 
       2 7 400 ; 
       2 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       3 1 500 ; 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 9 401 ; 
       1 10 401 ; 
       2 11 401 ; 
       2 13 401 ; 
       3 12 401 ; 
       3 14 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 45 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 21.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 36.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 33.75 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 32.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
