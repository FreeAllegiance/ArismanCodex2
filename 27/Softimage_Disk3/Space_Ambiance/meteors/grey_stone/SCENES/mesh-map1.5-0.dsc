SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_craters-cam_int1.14-0 ROOT ; 
       add_craters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_craters-inf_light1.14-0 ROOT ; 
       add_craters-inf_light2.14-0 ROOT ; 
       add_craters-inf_light3.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       add_craters-mat3.4-0 ; 
       map1-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       3d_global2-map1.14-0 ROOT ; 
       3d_global5-map1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map4_light ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       mesh-map1.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       add_craters-bottom_of_Craters1.8-0 ; 
       add_craters-inside_large_crater.9-0 ; 
       add_craters-large_crater.9-0 ; 
       add_craters-main_features1.8-0 ; 
       add_craters-map1.5-0 ; 
       map1-map1_light1.1-0 ; 
       map1-map4_light.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 2 400 ; 
       0 1 400 ; 
       0 3 400 ; 
       0 0 400 ; 
       1 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       1 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 16.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
