SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_craters-cam_int1.23-0 ROOT ; 
       add_craters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_craters-inf_light1.23-0 ROOT ; 
       add_craters-inf_light2.23-0 ROOT ; 
       add_craters-inf_light3.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       map1-default1.3-0 ; 
       map1-mat6.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       3d_global5-map1.4-0 ROOT ; 
       map1-nurbs4.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/top_cap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_rendermap-map1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       map1-bottom_of_Craters2.2-0 ; 
       map1-inside_large_crater1.2-0 ; 
       map1-large_crater1.2-0 ; 
       map1-main_features2.2-0 ; 
       map1-map1_light1.2-0 ; 
       map1-map2.1-0 ; 
       map1-map5.2-0 ; 
       map1-top_cap1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       map1-rock2.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 7 400 ; 
       1 2 400 ; 
       1 1 400 ; 
       1 3 400 ; 
       1 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 5 401 ; 
       0 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       7 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
