SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_craters-cam_int1.24-0 ROOT ; 
       add_craters-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       add_craters-inf_light1.24-0 ROOT ; 
       add_craters-inf_light2.24-0 ROOT ; 
       add_craters-inf_light3.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       map3-default1.1-0 ; 
       map3-mat6.1-0 ; 
       map3-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       3d_global5-map1.5-0 ROOT ; 
       3d_global6-map1.2-0 ROOT ; 
       map3-nurbs4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 10     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map3 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/top_cap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new_rendermap-map3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       map3-bottom_of_Craters2.1-0 ; 
       map3-inside_large_crater1.1-0 ; 
       map3-large_crater1.1-0 ; 
       map3-main_features2.1-0 ; 
       map3-map1_light1.1-0 ; 
       map3-map2.1-0 ; 
       map3-map2_light.1-0 ; 
       map3-map5.1-0 ; 
       map3-map6.1-0 ; 
       map3-top_cap1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       map3-rock2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 2 300 ; 
       0 1 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 9 400 ; 
       2 2 400 ; 
       2 1 400 ; 
       2 3 400 ; 
       2 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       2 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 6 401 ; 
       2 8 401 ; 
       1 5 401 ; 
       0 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 26.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 8.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 26.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       9 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
