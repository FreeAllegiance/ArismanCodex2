SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.5-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.5-0 ROOT ; 
       Lighting_Template-inf_light2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       pre_poly-default1.3-0 ; 
       pre_poly-mat6.2-0 ; 
       pre_poly-mat7.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       3d_global5-map1.9-0 ROOT ; 
       3d_global6-bgrnd53.7-0 ROOT ; 
       pre_poly-nurbs4.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 10     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bgrnd53 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/moon ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/top_cap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       relight-relight.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       pre_poly-bgrnd53.1-0 ; 
       pre_poly-bottom_of_Craters2.5-0 ; 
       pre_poly-inside_large_crater1.5-0 ; 
       pre_poly-large_crater1.5-0 ; 
       pre_poly-main_features2.5-0 ; 
       pre_poly-map1_light1.4-0 ; 
       pre_poly-map2.4-0 ; 
       pre_poly-map5.5-0 ; 
       pre_poly-top_cap1.4-0 ; 
       relight-t2d1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       pre_poly-rock2.5-0 ; 
       relight-rock3.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       1 2 300 ; 
       2 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 5 400 ; 
       0 8 400 ; 
       2 3 400 ; 
       2 2 400 ; 
       2 4 400 ; 
       2 1 400 ; 
       2 9 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 1 500 ; 
       2 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 6 401 ; 
       2 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35.75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 38.25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 30 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 30 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
