SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.20-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.20-0 ROOT ; 
       Lighting_Template-inf_light2.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       test-default10.3-0 ; 
       test-default8.5-0 ; 
       test-default9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       test-nurbs4.3-0 ROOT ; 
       test-sphere8.6-0 ROOT ; 
       test-sphere9.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 11     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/MoonBump ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/MoonMap ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/large_crater ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/top_cap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       relight-test.13-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       test-bottom_of_Craters2.2-0 ; 
       test-inside_large_crater2.2-0 ; 
       test-large_crater2.2-0 ; 
       test-main_features_bump2.2-0 ; 
       test-map10.3-0 ; 
       test-map11.2-0 ; 
       test-MoonBump1.1-0 ; 
       test-MoonMap.1-0 ; 
       test-old_map1_light4.3-0 ; 
       test-t2d1.4-0 ; 
       test-top_cap5.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       test-rock10.2-0 ; 
       test-rock9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 2 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 8 400 ; 
       1 10 400 ; 
       2 9 400 ; 
       0 2 400 ; 
       0 1 400 ; 
       0 3 400 ; 
       0 0 400 ; 
       0 7 400 ; 
       0 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 1 500 ; 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 4 401 ; 
       0 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 26.25 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 33.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 11.25 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 32.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       6 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       1 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
