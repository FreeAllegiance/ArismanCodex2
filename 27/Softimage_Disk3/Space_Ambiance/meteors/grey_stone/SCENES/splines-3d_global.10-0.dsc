SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       3d_global-cam_int1.10-0 ROOT ; 
       3d_global-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       3d_global-inf_light1.6-0 ROOT ; 
       3d_global-inf_light2.6-0 ROOT ; 
       3d_global-inf_light3.6-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       3d_global-mat1.4-0 ; 
       3d_global-mat2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       3d_global-map1.4-0 ROOT ; 
       3d_global1-map1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/grey_stone/PICTURES/map2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       splines-3d_global.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       3d_global-3D_Cloud1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       3d_global-map1.4-0 ; 
       3d_global-map1_light.2-0 ; 
       3d_global-map2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 2     
       3d_global-3Dtexture1.2-0 ; 
       3d_global-rock1.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 1 500 ; 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       1 2 401 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 3.75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
