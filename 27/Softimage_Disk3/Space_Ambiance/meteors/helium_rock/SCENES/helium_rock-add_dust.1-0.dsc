SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_dust-cam_int1.1-0 ROOT ; 
       add_dust-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       add_dust-light1.1-0 ROOT ; 
       add_dust-light2.1-0 ROOT ; 
       add_dust-light3.1-0 ROOT ; 
       add_dust-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       add_dust-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       add_dust-Dusty1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       add_dust-null1.1-0 ROOT ; 
       add_dust-rock.7-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       helium_rock-add_dust.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 4     
       add_dust-bottom_of_craters1.1-0 ; 
       add_dust-craters1.1-0 ; 
       add_dust-main_shell.1-0 ; 
       add_dust-t2d1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       add_dust-rock1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 2 400 ; 
       1 1 400 ; 
       1 0 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 3 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 0 0 DISPLAY 3 2 SRT 1 1 1 -1.44 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       3 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
