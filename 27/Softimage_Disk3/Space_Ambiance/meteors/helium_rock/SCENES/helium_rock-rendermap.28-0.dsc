SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.28-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       rendermap-light1.24-0 ROOT ; 
       rendermap-light2.24-0 ROOT ; 
       rendermap-light3.23-0 ROOT ; 
       rendermap-light4.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       rendermap-mat1.15-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       rendermap-Dusty1.8-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       rendermap-rock.25-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/rendermap ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/sprinkled_helium_Dust ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/veins ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       helium_rock-rendermap.28-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       rendermap-3DRock_Lava1.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       rendermap-bottom_of_craters1.17-0 ; 
       rendermap-craters1.20-0 ; 
       rendermap-inside_large_crater1.3-0 ; 
       rendermap-inside_large_crater2.2-0 ; 
       rendermap-main_shell.17-0 ; 
       rendermap-sprinkled_dust1.13-0 ; 
       rendermap-t2d1.21-0 ; 
       rendermap-veins1.13-0 ; 
       rendermap-very_large_crater1.3-0 ; 
       rendermap-very_large_crater2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       rendermap-rock02.12-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 4 400 ; 
       0 1 400 ; 
       0 0 400 ; 
       0 7 400 ; 
       0 5 400 ; 
       0 8 400 ; 
       0 2 400 ; 
       0 9 400 ; 
       0 3 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 15 0 0 SRT 1 1 1 0 0 0 1.245035 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
