SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       wrap-cam_int1.1-0 ROOT ; 
       wrap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       wrap-default2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       poly2-sphere1.1-0 ROOT ; 
       wrap-skin1.1-0 ROOT ; 
       wrap-sphere1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 6     
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/high_points ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       helium_rock-wrap.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       wrap-bottom_of_craters3.1-0 ; 
       wrap-high_points3.1-0 ; 
       wrap-inside_large_crater3.1-0 ; 
       wrap-large_crater3.1-0 ; 
       wrap-main_Features2.1-0 ; 
       wrap-main_shell3.1-0 ; 
       wrap-rendermap3.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 4 400 ; 
       1 1 400 ; 
       1 5 400 ; 
       1 0 400 ; 
       1 3 400 ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 20 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 22.5 0 0 SRT 0.27 0.27 0.27 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
