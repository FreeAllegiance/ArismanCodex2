SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.21-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1.21-0 ROOT ; 
       Lighting_Template-inf_light2.21-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       Test-Cling_Mat.7-0 ; 
       Test-hh.3-0 ; 
       Test-hh1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       Test-static_cling-Static_Cling1.4-1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 3     
       Test-mapper.12-0 ROOT ; 
       Test1-capper.5-0 ROOT ; 
       Test3-capper.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 14     
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/MoonBump ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/MoonMap ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/map1 ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/map1_light ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/map2 ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/map2_light ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/sprinkled_helium_Dust ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/top-cap ; 
       D:/Pete_Data/Softimage/meteors/helium_rock/PICTURES/veins ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       relight-Test.23-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS NBELEM 1     
       Test-3DRock_Lava2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       Test-bottom_of_craters2.9-0 ; 
       Test-craters2.9-0 ; 
       Test-inside_large_crater3.9-0 ; 
       Test-inside_large_crater4.9-0 ; 
       Test-main_shell1.9-0 ; 
       Test-MoonBump1.7-0 ; 
       Test-MoonMap1.6-0 ; 
       Test-sprinkled_dust2.9-0 ; 
       Test-t2d2.1-0 ; 
       Test-t2d4.4-0 ; 
       Test-t2d6.4-0 ; 
       Test-t2d7.4-0 ; 
       Test-t2d8.3-0 ; 
       Test-veins2.9-0 ; 
       Test-very_large_crater3.9-0 ; 
       Test-very_large_crater4.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       Test-rock3.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 1 300 ; 
       2 2 300 ; 
       0 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 9 400 ; 
       1 11 400 ; 
       2 12 400 ; 
       0 4 400 ; 
       0 1 400 ; 
       0 0 400 ; 
       0 13 400 ; 
       0 7 400 ; 
       0 14 400 ; 
       0 2 400 ; 
       0 15 400 ; 
       0 3 400 ; 
       0 5 400 ; 
       0 6 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       0 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 10 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D CHAPTER TEXTURE3D_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS CHAPTER MODELS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 47.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 50 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 37.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 43.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 17.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 42.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 35 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       12 SCHEM 45 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 35 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE3D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
