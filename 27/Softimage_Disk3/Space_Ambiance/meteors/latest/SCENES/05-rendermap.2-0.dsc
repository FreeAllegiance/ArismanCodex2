SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       rendermap-cam_int1.2-0 ROOT ; 
       rendermap-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       rendermap-inf_light1.2-0 ROOT ; 
       rendermap-light1.2-0 ROOT ; 
       rendermap-light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       rendermap-default2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       rendermap-circle1.1-0 ; 
       rendermap-circle10.1-0 ; 
       rendermap-circle11.1-0 ; 
       rendermap-circle2.1-0 ; 
       rendermap-circle3.1-0 ; 
       rendermap-circle4.1-0 ; 
       rendermap-circle5.1-0 ; 
       rendermap-circle6.1-0 ; 
       rendermap-circle7.1-0 ; 
       rendermap-circle8.1-0 ; 
       rendermap-circle9.1-0 ; 
       rendermap-null1.1-0 ROOT ; 
       rendermap-null2.1-0 ROOT ; 
       rendermap-skin1.2-0 ROOT ; 
       rendermap-spline1.1-0 ; 
       rendermap-spline2.1-0 ; 
       rendermap-spline3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/high_points ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       05-rendermap.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       rendermap-bottom_of_craters3.2-0 ; 
       rendermap-high_points3.2-0 ; 
       rendermap-inside_large_crater3.2-0 ; 
       rendermap-large_crater3.2-0 ; 
       rendermap-main_Features2.2-0 ; 
       rendermap-main_shell3.2-0 ; 
       rendermap-rendermap3.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       0 11 110 ; 
       3 11 110 ; 
       4 11 110 ; 
       5 11 110 ; 
       6 11 110 ; 
       7 11 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       1 11 110 ; 
       2 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       13 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       13 4 400 ; 
       13 1 400 ; 
       13 5 400 ; 
       13 0 400 ; 
       13 3 400 ; 
       13 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 57.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 60 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 62.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       14 SCHEM 50 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 52.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 55 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 38.75 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 52.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 45 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       4 SCHEM 30 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 35 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 37.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 40 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
