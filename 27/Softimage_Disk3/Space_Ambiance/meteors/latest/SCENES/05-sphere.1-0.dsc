SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       sphere-cam_int1.1-0 ROOT ; 
       sphere-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       sphere-inf_light1.1-0 ROOT ; 
       sphere-light1.1-0 ROOT ; 
       sphere-light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       sphere-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       sphere-bgrnd05.1-0 ROOT ; 
       sphere-skin2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 7     
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/bottom_of_Craters ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/high_points ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/inside_large_crater ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/large crater ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/main_feature ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/main_shell ; 
       D:/Pete_Data/Softimage/meteors/latest/PICTURES/rendermap ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       05-sphere.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       sphere-bottom_of_craters1.1-0 ; 
       sphere-high_points1.1-0 ; 
       sphere-inside_large_crater1.1-0 ; 
       sphere-large_crater1.1-0 ; 
       sphere-main_Features.1-0 ; 
       sphere-main_shell1.1-0 ; 
       sphere-rendermap1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 4 400 ; 
       1 1 400 ; 
       1 5 400 ; 
       1 0 400 ; 
       1 3 400 ; 
       1 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 6 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 10 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 20 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 15 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
