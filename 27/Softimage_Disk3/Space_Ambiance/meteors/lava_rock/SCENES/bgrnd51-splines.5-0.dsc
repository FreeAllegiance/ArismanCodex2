SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       splines-cam_int1.5-0 ROOT ; 
       splines-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       splines-circle1.2-0 ROOT ; 
       splines-circle2.2-0 ROOT ; 
       splines-null1.2-0 ROOT ; 
       splines-nurbs1.1-0 ; 
       splines-nurbs10.1-0 ; 
       splines-nurbs11.1-0 ; 
       splines-nurbs11_1.1-0 ; 
       splines-nurbs2.1-0 ; 
       splines-nurbs3.1-0 ; 
       splines-nurbs4.1-0 ; 
       splines-nurbs5.1-0 ; 
       splines-nurbs6.1-0 ; 
       splines-nurbs6_1.1-0 ; 
       splines-nurbs7.1-0 ; 
       splines-nurbs8.1-0 ; 
       splines-nurbs9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd51-splines.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       12 2 110 ; 
       4 2 110 ; 
       15 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       6 2 110 ; 
       11 2 110 ; 
       5 2 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1.002 1.002 1.002 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       15 SCHEM 30 -2 0 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
