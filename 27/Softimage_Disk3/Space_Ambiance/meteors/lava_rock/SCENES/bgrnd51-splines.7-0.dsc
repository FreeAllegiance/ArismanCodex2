SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       splines-cam_int1.7-0 ROOT ; 
       splines-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       splines-circle1.2-0 ROOT ; 
       splines-circle2.2-0 ROOT ; 
       splines-null1.4-0 ROOT ; 
       splines-nurbs1.1-0 ; 
       splines-nurbs10.1-0 ; 
       splines-nurbs11.1-0 ; 
       splines-nurbs11_1.1-0 ; 
       splines-nurbs2.1-0 ; 
       splines-nurbs3.1-0 ; 
       splines-nurbs4.1-0 ; 
       splines-nurbs5.1-0 ; 
       splines-nurbs6.1-0 ; 
       splines-nurbs6_1.1-0 ; 
       splines-nurbs7.1-0 ; 
       splines-nurbs8.1-0 ; 
       splines-nurbs9.1-0 ; 
       splines-skin2.2-0 ROOT ; 
       splines1-skin2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       bgrnd51-splines.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       12 2 110 ; 
       4 2 110 ; 
       15 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       6 2 110 ; 
       11 2 110 ; 
       5 2 110 ; 
       16 5 252 RELDATA CLOSE 0 NB_SPLINE 13 CAP 0 SUBDIVISION 5 IS_PATCH 1 SPL_TYPE 0 NRB_UNIFORMITY 0 NRB_TYPE 2 STEP 3 EndOfRELDATA ; 
       16 5 252 2 ; 
       16 6 252 ; 
       16 6 252 2 ; 
       16 4 252 ; 
       16 4 252 2 ; 
       16 15 252 ; 
       16 15 252 2 ; 
       16 14 252 ; 
       16 14 252 2 ; 
       16 13 252 ; 
       16 13 252 2 ; 
       16 3 252 ; 
       16 3 252 2 ; 
       16 7 252 ; 
       16 7 252 2 ; 
       16 8 252 ; 
       16 8 252 2 ; 
       16 9 252 ; 
       16 9 252 2 ; 
       16 10 252 ; 
       16 10 252 2 ; 
       16 12 252 ; 
       16 12 252 2 ; 
       16 11 252 ; 
       16 11 252 2 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 5 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1.002 1.002 1.002 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 1.570796 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 42.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 40 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
