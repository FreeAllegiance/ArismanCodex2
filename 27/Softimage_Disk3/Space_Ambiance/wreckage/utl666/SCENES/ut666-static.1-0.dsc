SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 37     
       cargoclose_F-mat1_1.1-0 ; 
       cargoclose_F-mat2_1.1-0 ; 
       cargoclose_F-mat3_1.1-0 ; 
       cargoclose_F-mat4_1.1-0 ; 
       cargoclose_F-mat5_1.1-0 ; 
       cargoclose_F-mat6_1.1-0 ; 
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat24.1-0 ; 
       STATIC-mat25.1-0 ; 
       STATIC-mat26.1-0 ; 
       STATIC-mat27.1-0 ; 
       STATIC-mat29.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat33.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat40.1-0 ; 
       STATIC-mat47.1-0 ; 
       STATIC-mat48.1-0 ; 
       STATIC-mat49.1-0 ; 
       STATIC-mat50.1-0 ; 
       STATIC-mat51.1-0 ; 
       STATIC-mat52.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       utl11-blthrust_1.2-0 ; 
       utl11-brthrust_1.5-0 ; 
       utl11-bthrust0_1.1-0 ; 
       utl11-fuselg_1.1-0 ROOT ; 
       utl11-fuselg1_1.3-0 ; 
       utl11-lbtractr1_1.1-0 ; 
       utl11-lbtractr2_1.1-0 ; 
       utl11-ltthrust_1.2-0 ; 
       utl11-rbtractr1_1.1-0 ; 
       utl11-rtthrust_1.2-0 ; 
       utl11-rttractr1_1.1-0 ; 
       utl11-tractr_1.1-0 ; 
       utl11-utl11.1-0 ROOT ; 
       utl11-utl15a_1.2-0 ; 
       utl11-utl15a1_1.1-0 ; 
       utl11-utl15a2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/wreckage/utl666/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ut666-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 35     
       cargoclose_F-t2d1.1-0 ; 
       cargoclose_F-t2d2.1-0 ; 
       cargoclose_F-t2d3.1-0 ; 
       cargoclose_F-t2d4.1-0 ; 
       cargoclose_F-t2d6.1-0 ; 
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d23.1-0 ; 
       STATIC-t2d24.1-0 ; 
       STATIC-t2d25.1-0 ; 
       STATIC-t2d27.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d31.1-0 ; 
       STATIC-t2d39.1-0 ; 
       STATIC-t2d40.1-0 ; 
       STATIC-t2d41.1-0 ; 
       STATIC-t2d42.1-0 ; 
       STATIC-t2d43.1-0 ; 
       STATIC-t2d44.1-0 ; 
       STATIC-t2d45.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 4 110 ; 
       4 12 110 ; 
       5 4 110 ; 
       6 5 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 6 110 ; 
       13 4 110 ; 
       14 4 110 ; 
       15 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 32 300 ; 
       0 33 300 ; 
       1 34 300 ; 
       1 35 300 ; 
       2 29 300 ; 
       2 30 300 ; 
       2 31 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       4 26 300 ; 
       4 28 300 ; 
       4 36 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       5 20 300 ; 
       6 21 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       8 22 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       10 25 300 ; 
       11 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 32 400 ; 
       4 10 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 34 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       13 11 401 ; 
       14 12 401 ; 
       16 13 401 ; 
       17 14 401 ; 
       18 15 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 16 401 ; 
       27 25 401 ; 
       28 24 401 ; 
       30 26 401 ; 
       31 27 401 ; 
       32 28 401 ; 
       33 29 401 ; 
       34 30 401 ; 
       35 31 401 ; 
       36 33 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 37.5 -6 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 28.75 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 20 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 30 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 32.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 28.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 45 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 52.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 56.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
