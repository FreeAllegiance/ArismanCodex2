SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 31     
       STATIC-mat1.1-0 ; 
       STATIC-mat10.1-0 ; 
       STATIC-mat11.1-0 ; 
       STATIC-mat12.1-0 ; 
       STATIC-mat13.1-0 ; 
       STATIC-mat14.1-0 ; 
       STATIC-mat15.1-0 ; 
       STATIC-mat16.1-0 ; 
       STATIC-mat17.1-0 ; 
       STATIC-mat18.1-0 ; 
       STATIC-mat19.1-0 ; 
       STATIC-mat20.1-0 ; 
       STATIC-mat21.1-0 ; 
       STATIC-mat22.1-0 ; 
       STATIC-mat23.1-0 ; 
       STATIC-mat24.1-0 ; 
       STATIC-mat25.1-0 ; 
       STATIC-mat26.1-0 ; 
       STATIC-mat27.1-0 ; 
       STATIC-mat29.1-0 ; 
       STATIC-mat3.1-0 ; 
       STATIC-mat33.1-0 ; 
       STATIC-mat4.1-0 ; 
       STATIC-mat40.1-0 ; 
       STATIC-mat47.1-0 ; 
       STATIC-mat48.1-0 ; 
       STATIC-mat49.1-0 ; 
       STATIC-mat50.1-0 ; 
       STATIC-mat51.1-0 ; 
       STATIC-mat52.1-0 ; 
       STATIC-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 15     
       utl11-blthrust_1.2-0 ; 
       utl11-brthrust_1.5-0 ; 
       utl11-bthrust0_1.1-0 ; 
       utl11-fuselg1_1.3-0 ; 
       utl11-lbtractr1_1.1-0 ; 
       utl11-lbtractr2_1.1-0 ; 
       utl11-ltthrust_1.2-0 ; 
       utl11-rbtractr1_1.1-0 ; 
       utl11-rtthrust_1.2-0 ; 
       utl11-rttractr1_1.1-0 ; 
       utl11-tractr_1.1-0 ; 
       utl11-utl11.1-0 ROOT ; 
       utl11-utl15a_1.2-0 ; 
       utl11-utl15a1_1.1-0 ; 
       utl11-utl15a2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/Space_Ambiance/wreckage/utl667/PICTURES/utl11 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ut667-hull2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       STATIC-t2d1.1-0 ; 
       STATIC-t2d10.1-0 ; 
       STATIC-t2d11.1-0 ; 
       STATIC-t2d12.1-0 ; 
       STATIC-t2d13.1-0 ; 
       STATIC-t2d14.1-0 ; 
       STATIC-t2d15.1-0 ; 
       STATIC-t2d16.1-0 ; 
       STATIC-t2d17.1-0 ; 
       STATIC-t2d18.1-0 ; 
       STATIC-t2d19.1-0 ; 
       STATIC-t2d2.1-0 ; 
       STATIC-t2d20.1-0 ; 
       STATIC-t2d21.1-0 ; 
       STATIC-t2d22.1-0 ; 
       STATIC-t2d23.1-0 ; 
       STATIC-t2d24.1-0 ; 
       STATIC-t2d25.1-0 ; 
       STATIC-t2d27.1-0 ; 
       STATIC-t2d3.1-0 ; 
       STATIC-t2d31.1-0 ; 
       STATIC-t2d39.1-0 ; 
       STATIC-t2d40.1-0 ; 
       STATIC-t2d41.1-0 ; 
       STATIC-t2d42.1-0 ; 
       STATIC-t2d43.1-0 ; 
       STATIC-t2d44.1-0 ; 
       STATIC-t2d45.1-0 ; 
       STATIC-t2d8.1-0 ; 
       STATIC-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 3 110 ; 
       3 11 110 ; 
       4 3 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 5 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 26 300 ; 
       0 27 300 ; 
       1 28 300 ; 
       1 29 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       3 0 300 ; 
       3 20 300 ; 
       3 22 300 ; 
       3 30 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 12 300 ; 
       4 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       7 18 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       9 19 300 ; 
       10 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 27 400 ; 
       3 5 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 29 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       12 10 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 11 401 ; 
       21 20 401 ; 
       22 19 401 ; 
       24 21 401 ; 
       25 22 401 ; 
       26 23 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 28 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 20 -4 0 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 MPRFLG 0 ; 
       9 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 15 -8 0 MPRFLG 0 ; 
       11 SCHEM 28.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 45 -4 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 56.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 56.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
