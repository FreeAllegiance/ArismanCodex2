SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       animation-cam_int1.7-0 ROOT ; 
       animation-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       animation-default.1-0 ; 
       animation-top-n-bottom1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       animation-bit1.2-0 ; 
       animation-bit1_1.2-0 ; 
       animation-cube1.3-0 ; 
       animation-I_beam.5-0 ROOT ; 
       animation-large_cyl.1-0 ; 
       animation-sheet_metal.2-0 ; 
       animation-small_cyl.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wreckage-animation.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 5 110 ; 
       6 4 110 ; 
       5 3 110 ; 
       2 3 110 ; 
       1 3 110 ; 
       0 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       5 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 3.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 0.1350859 -6.268506 -3.667613 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       0 SCHEM 10 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 15 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 300 300 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
