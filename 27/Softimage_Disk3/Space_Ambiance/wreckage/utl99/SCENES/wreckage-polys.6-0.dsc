SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       polys-cam_int1.6-0 ROOT ; 
       polys-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       polys-I_beam.2-0 ROOT ; 
       polys-large_cyl.1-0 ; 
       polys-sheet_metal.2-0 ROOT ; 
       polys-small_cyl.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wreckage-polys.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 2 110 ; 
       3 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 2.5 0 0 SRT 0.30744 0.04072321 1.236357 0 0 0 1.953488 0.2074522 -4.014311 MPRFLG 0 ; 
       0 SCHEM 5 0 0 SRT 1 1 1 -1.343904 1.151917 -0.8028514 0.1350859 -2.481361 -3.667613 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
