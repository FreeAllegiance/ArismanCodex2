SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 16     
       utann_heavy_fighter_land-utann_hvy_fighter_4.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_1_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_15.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_2_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_1_1.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_3_3.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7.11-0 ; 
       utann_heavy_fighter_land-utann_hvy_fighter_4_7_1.11-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.11-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       texture-light1.26-0 ROOT ; 
       texture-light2.26-0 ROOT ; 
       texture-light3.26-0 ROOT ; 
       texture-light4.26-0 ROOT ; 
       texture-light5.26-0 ROOT ; 
       texture-light6.26-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       done_cropping-mat10.3-0 ; 
       texture-default.2-0 ; 
       texture-default1.2-0 ; 
       texture-default2.2-0 ; 
       texture-default3.2-0 ; 
       texture-mat1.2-0 ; 
       texture-mat2.2-0 ; 
       texture-sides1.2-0 ; 
       texture-top-n-bottom1.2-0 ; 
       texture-top1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       done_cropping-afuselg.9-0 ROOT ; 
       done_cropping-cube1.1-0 ; 
       done_cropping-large_cyl.1-0 ; 
       done_cropping-sheet_metal.1-0 ; 
       done_cropping-small_cyl.1-0 ; 
       done_cropping-tetra1.2-0 ; 
       done_cropping-tetra2.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/wreckage/utl99/PICTURES/utl99 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wreckage-texture.26-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       texture-t2d1.3-0 ; 
       texture-t2d10.4-0 ; 
       texture-t2d11.2-0 ; 
       texture-t2d12.2-0 ; 
       texture-t2d13.2-0 ; 
       texture-t2d2.2-0 ; 
       texture-t2d3.2-0 ; 
       texture-t2d4.2-0 ; 
       texture-t2d8.3-0 ; 
       texture-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 3 110 ; 
       3 0 110 ; 
       4 2 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       2 2 300 ; 
       3 1 300 ; 
       3 8 300 ; 
       4 3 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       0 0 300 ; 
       0 7 300 ; 
       0 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 5 401 ; 
       2 6 401 ; 
       3 7 401 ; 
       4 8 401 ; 
       0 4 401 ; 
       5 9 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 0 401 ; 
       9 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 6.25 -4 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 20 -2 0 MPRFLG 0 ; 
       0 SCHEM 16.25 0 0 DISPLAY 1 2 SRT 1 1 1 -0.5809125 0.5698853 0.6261758 -5.272871 -5.431124 -11.9656 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       1 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 20 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 27.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 208.2814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 146.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 147.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 194.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 193.7814 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 296.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 154.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 189.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 229.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 196.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 189.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 154.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 196.6147 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 229.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 180.0314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 219.5314 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 1 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
