SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.4-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       bound-bound1.1-0 ; 
       bound-bound10.1-0 ; 
       bound-bound11.1-0 ; 
       bound-bound12.1-0 ; 
       bound-bound2.1-0 ; 
       bound-bound3.1-0 ; 
       bound-bound4.1-0 ; 
       bound-bound5.3-0 ; 
       bound-bound6.1-0 ; 
       bound-bound7.1-0 ; 
       bound-bound8.1-0 ; 
       bound-bound9.1-0 ; 
       bound-root.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss21a-bound.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       11 12 110 ; 
       3 12 110 ; 
       0 12 110 ; 
       4 12 110 ; 
       5 12 110 ; 
       6 12 110 ; 
       7 12 110 ; 
       10 12 110 ; 
       8 12 110 ; 
       1 12 110 ; 
       2 12 110 ; 
       9 12 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       11 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 16.25 0 0 SRT 1 1 1 0 0 0 8.28805 10.32402 7.412924 MPRFLG 0 ; 
       10 SCHEM 20 -2 0 MPRFLG 0 ; 
       8 SCHEM 15 -2 0 MPRFLG 0 ; 
       1 SCHEM 25 -2 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
