SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       acs06-acs06.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       autofactory_sPTLN-cam_int1.1-0 ROOT ; 
       autofactory_sPTLN-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       autofactory_sPTLN-light1_2.1-0 ROOT ; 
       autofactory_sPTLN-light2_2.1-0 ROOT ; 
       autofactory_sPTLN-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 56     
       autofactory_F-nose_white-center.1-10.1-0 ; 
       autofactory_F-nose_white-center.1-10_1.1-0 ; 
       autofactory_F-nose_white-center.1-11.1-0 ; 
       autofactory_F-nose_white-center.1-12.1-0 ; 
       autofactory_F-nose_white-center.1-13.1-0 ; 
       autofactory_F-nose_white-center.1-14.1-0 ; 
       autofactory_F-nose_white-center.1-15.1-0 ; 
       autofactory_F-nose_white-center.1-16.1-0 ; 
       autofactory_F-nose_white-center.1-17.1-0 ; 
       autofactory_F-nose_white-center.1-18.1-0 ; 
       autofactory_F-nose_white-center.1-19.1-0 ; 
       autofactory_F-nose_white-center.1-20.1-0 ; 
       autofactory_F-nose_white-center.1-21.1-0 ; 
       autofactory_F-nose_white-center.1-22.1-0 ; 
       autofactory_F-nose_white-center.1-23.1-0 ; 
       autofactory_F-nose_white-center.1-24.1-0 ; 
       autofactory_F-nose_white-center.1-25.1-0 ; 
       autofactory_F-nose_white-center.1-26.1-0 ; 
       autofactory_F-nose_white-center.1-27.1-0 ; 
       autofactory_F-nose_white-center.1-28.1-0 ; 
       autofactory_F-nose_white-center.1-5.1-0 ; 
       autofactory_F-nose_white-center.1-6.1-0 ; 
       autofactory_F-nose_white-center.1-7.1-0 ; 
       autofactory_F-nose_white-center.1-8.1-0 ; 
       autofactory_F-nose_white-center.1-9.1-0 ; 
       autofactory_F-nose_white-center.1-9_1.1-0 ; 
       autofactory_F-port_red-left.1-0.1-0 ; 
       autofactory_F-starbord_green-right.1-0.1-0 ; 
       autofactory_sPTLN-mat1.1-0 ; 
       autofactory_sPTLN-mat10.1-0 ; 
       autofactory_sPTLN-mat11.1-0 ; 
       autofactory_sPTLN-mat12.1-0 ; 
       autofactory_sPTLN-mat13.1-0 ; 
       autofactory_sPTLN-mat16.1-0 ; 
       autofactory_sPTLN-mat18.1-0 ; 
       autofactory_sPTLN-mat19.1-0 ; 
       autofactory_sPTLN-mat20.1-0 ; 
       autofactory_sPTLN-mat21.1-0 ; 
       autofactory_sPTLN-mat22.1-0 ; 
       autofactory_sPTLN-mat23.1-0 ; 
       autofactory_sPTLN-mat24.1-0 ; 
       autofactory_sPTLN-mat25.1-0 ; 
       autofactory_sPTLN-mat26.1-0 ; 
       autofactory_sPTLN-mat27.1-0 ; 
       autofactory_sPTLN-mat28.1-0 ; 
       autofactory_sPTLN-mat29.1-0 ; 
       autofactory_sPTLN-mat32.1-0 ; 
       autofactory_sPTLN-mat33.1-0 ; 
       autofactory_sPTLN-mat35.1-0 ; 
       autofactory_sPTLN-mat36.1-0 ; 
       autofactory_sPTLN-mat37.1-0 ; 
       autofactory_sPTLN-mat38.1-0 ; 
       autofactory_sPTLN-mat39.1-0 ; 
       autofactory_sPTLN-mat40.1-0 ; 
       autofactory_sPTLN-mat8.1-0 ; 
       autofactory_sPTLN-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       acs06-acs06.1-0 ROOT ; 
       acs06-bportal.1-0 ; 
       acs06-bturatt.1-0 ; 
       acs06-doccon.1-0 ; 
       acs06-fuselg1.3-0 ; 
       acs06-fuselg2.1-0 ; 
       acs06-lantenn1.1-0 ; 
       acs06-lantenn2.1-0 ; 
       acs06-lantenn3.1-0 ; 
       acs06-llndpad.1-0 ; 
       acs06-platfm.1-0 ; 
       acs06-portal0.1-0 ; 
       acs06-rantenn1.1-0 ; 
       acs06-rantenn2.1-0 ; 
       acs06-rantenn3.1-0 ; 
       acs06-rlndpad.1-0 ; 
       acs06-SSa4.1-0 ; 
       acs06-SSbp1.1-0 ; 
       acs06-SSbp2.1-0 ; 
       acs06-SSl.1-0 ; 
       acs06-SSlp0.1-0 ; 
       acs06-SSlp1.1-0 ; 
       acs06-SSlp2.1-0 ; 
       acs06-SSlp3.1-0 ; 
       acs06-SSlp4.1-0 ; 
       acs06-SSlp5.1-0 ; 
       acs06-SSlpp0.1-0 ; 
       acs06-SSlpp1.1-0 ; 
       acs06-SSlpp2.1-0 ; 
       acs06-SSlpp3.1-0 ; 
       acs06-SSlpp4.1-0 ; 
       acs06-SSlpp5.1-0 ; 
       acs06-SSl_1.1-0 ; 
       acs06-SSr.1-0 ; 
       acs06-SSrp0.1-0 ; 
       acs06-SSrp1.1-0 ; 
       acs06-SSrp2.1-0 ; 
       acs06-SSrp3.1-0 ; 
       acs06-SSrp4.1-0 ; 
       acs06-SSrp5.1-0 ; 
       acs06-SSrpp0.1-0 ; 
       acs06-SSrpp1.1-0 ; 
       acs06-SSrpp2.1-0 ; 
       acs06-SSrpp3.1-0 ; 
       acs06-SSrpp4.1-0 ; 
       acs06-SSrpp5.1-0 ; 
       acs06-SStp1.1-0 ; 
       acs06-SStp2.1-0 ; 
       acs06-tportal.2-0 ; 
       acs06-tractr.1-0 ; 
       acs06-tturatt.5-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/space_station/acs/acs06/PICTURES/acs06 ; 
       D:/Pete_Data/Softimage/space_station/acs/acs06/PICTURES/acs08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs06-autofactory_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       autofactory_sPTLN-t2d10.1-0 ; 
       autofactory_sPTLN-t2d13.1-0 ; 
       autofactory_sPTLN-t2d15.1-0 ; 
       autofactory_sPTLN-t2d16.1-0 ; 
       autofactory_sPTLN-t2d17.1-0 ; 
       autofactory_sPTLN-t2d18.1-0 ; 
       autofactory_sPTLN-t2d19.1-0 ; 
       autofactory_sPTLN-t2d20.1-0 ; 
       autofactory_sPTLN-t2d21.1-0 ; 
       autofactory_sPTLN-t2d22.1-0 ; 
       autofactory_sPTLN-t2d23.1-0 ; 
       autofactory_sPTLN-t2d24.1-0 ; 
       autofactory_sPTLN-t2d25.1-0 ; 
       autofactory_sPTLN-t2d28.1-0 ; 
       autofactory_sPTLN-t2d29.1-0 ; 
       autofactory_sPTLN-t2d31.1-0 ; 
       autofactory_sPTLN-t2d32.1-0 ; 
       autofactory_sPTLN-t2d33.1-0 ; 
       autofactory_sPTLN-t2d34.1-0 ; 
       autofactory_sPTLN-t2d35.1-0 ; 
       autofactory_sPTLN-t2d36.1-0 ; 
       autofactory_sPTLN-t2d7.1-0 ; 
       autofactory_sPTLN-t2d8.1-0 ; 
       autofactory_sPTLN-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       16 13 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 4 110 ; 
       32 7 110 ; 
       20 10 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 10 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 26 110 ; 
       31 26 110 ; 
       33 4 110 ; 
       34 10 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 34 110 ; 
       38 34 110 ; 
       39 34 110 ; 
       40 10 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 40 110 ; 
       46 48 110 ; 
       47 48 110 ; 
       1 11 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       9 10 110 ; 
       10 49 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       15 10 110 ; 
       48 11 110 ; 
       49 4 110 ; 
       50 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       16 27 300 ; 
       17 23 300 ; 
       18 22 300 ; 
       19 24 300 ; 
       32 26 300 ; 
       21 5 300 ; 
       22 6 300 ; 
       23 7 300 ; 
       24 8 300 ; 
       25 9 300 ; 
       27 15 300 ; 
       28 16 300 ; 
       29 17 300 ; 
       30 18 300 ; 
       31 19 300 ; 
       33 0 300 ; 
       35 1 300 ; 
       36 2 300 ; 
       37 25 300 ; 
       38 3 300 ; 
       39 4 300 ; 
       41 10 300 ; 
       42 11 300 ; 
       43 12 300 ; 
       44 13 300 ; 
       45 14 300 ; 
       46 20 300 ; 
       47 21 300 ; 
       1 32 300 ; 
       1 51 300 ; 
       3 40 300 ; 
       4 41 300 ; 
       4 42 300 ; 
       4 43 300 ; 
       4 44 300 ; 
       5 45 300 ; 
       5 48 300 ; 
       5 49 300 ; 
       6 37 300 ; 
       7 34 300 ; 
       8 39 300 ; 
       10 28 300 ; 
       10 46 300 ; 
       10 47 300 ; 
       10 52 300 ; 
       10 53 300 ; 
       12 36 300 ; 
       13 35 300 ; 
       14 38 300 ; 
       48 33 300 ; 
       48 50 300 ; 
       49 54 300 ; 
       49 55 300 ; 
       49 29 300 ; 
       49 30 300 ; 
       49 31 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       29 22 401 ; 
       31 23 401 ; 
       32 0 401 ; 
       33 1 401 ; 
       34 2 401 ; 
       35 3 401 ; 
       36 4 401 ; 
       37 5 401 ; 
       38 6 401 ; 
       39 7 401 ; 
       40 8 401 ; 
       42 9 401 ; 
       43 10 401 ; 
       44 11 401 ; 
       45 12 401 ; 
       46 13 401 ; 
       47 14 401 ; 
       48 15 401 ; 
       49 16 401 ; 
       50 17 401 ; 
       51 18 401 ; 
       52 19 401 ; 
       53 20 401 ; 
       55 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -64.88676 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -66.88676 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 0 -68.88676 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       16 SCHEM 14 -5.886768 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 14 -69.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 14 -71.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 7 -15.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 14 -9.886768 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 14 -33.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 17.5 -37.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 17.5 -35.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 17.5 -33.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 17.5 -31.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 17.5 -29.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 14 -53.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 17.5 -57.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 17.5 -55.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 17.5 -53.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 17.5 -51.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 17.5 -49.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 7 -13.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 14 -23.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 17.5 -27.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 17.5 -25.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 17.5 -23.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 17.5 -21.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 17.5 -19.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 14 -43.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 17.5 -47.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 17.5 -45.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 17.5 -43.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 17.5 -41.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 17.5 -39.88677 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 14 -65.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 14 -67.88677 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 0 -40.88677 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10.5 -70.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7 -75.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7 -63.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 3.5 -40.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 7 -61.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 7 -10.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 10.5 -10.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 14 -11.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 14 -18.88677 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 10.5 -38.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 7 -68.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 7 -6.886768 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 10.5 -6.886768 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 14 -7.886768 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 14 -59.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 10.5 -66.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 7 -38.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 7 -73.88677 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 10.5 -14.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21 -28.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 21 -26.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21 -22.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 21 -20.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 21 -38.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 21 -36.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 21 -34.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21 -32.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21 -30.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21 -48.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21 -46.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 21 -44.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 21 -42.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21 -40.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 21 -58.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 21 -56.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21 -54.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21 -52.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21 -50.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -66.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 17.5 -68.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -72.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 17.5 -70.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21 -24.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -10.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 17.5 -6.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 14 -17.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -68.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -64.13676 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -8.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 10.5 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 10.5 -8.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 17.5 -8.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 17.5 -12.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 10.5 -64.13676 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 7 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 7 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 7 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 7 -4.136768 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 10.5 -62.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 14 -17.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 14 -17.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 10.5 -62.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 10.5 -62.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 14 -64.13676 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 14 -68.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 14 -17.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 14 -17.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 10.5 -16.13677 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -68.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -64.13676 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -8.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 17.5 -4.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -4.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -8.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21 -8.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21 -12.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -64.13676 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -4.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 10.5 -4.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 10.5 -4.136768 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -62.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -17.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 17.5 -17.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -62.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 14 -62.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 17.5 -64.13676 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -68.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -17.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 17.5 -17.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 14 -16.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 14 -16.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 14 -16.13677 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 3.5 -4.136768 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 6 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
