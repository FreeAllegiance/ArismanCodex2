SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       autofactory_F-null1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       autofact_msPTL-light1_1_3.1-0 ROOT ; 
       autofact_msPTL-light10_1_3.1-0 ROOT ; 
       autofact_msPTL-light2_1_3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 63     
       autofactory_F-nose_white-center.1-1.1-0 ; 
       autofactory_F-nose_white-center.1-10.1-0 ; 
       autofactory_F-nose_white-center.1-11.1-0 ; 
       autofactory_F-nose_white-center.1-12.1-0 ; 
       autofactory_F-nose_white-center.1-13.1-0 ; 
       autofactory_F-nose_white-center.1-14.1-0 ; 
       autofactory_F-nose_white-center.1-15.1-0 ; 
       autofactory_F-nose_white-center.1-16.1-0 ; 
       autofactory_F-nose_white-center.1-17.1-0 ; 
       autofactory_F-nose_white-center.1-18.1-0 ; 
       autofactory_F-nose_white-center.1-19.1-0 ; 
       autofactory_F-nose_white-center.1-20.1-0 ; 
       autofactory_F-nose_white-center.1-21.1-0 ; 
       autofactory_F-nose_white-center.1-22.1-0 ; 
       autofactory_F-nose_white-center.1-23.1-0 ; 
       autofactory_F-nose_white-center.1-24.1-0 ; 
       autofactory_F-nose_white-center.1-25.1-0 ; 
       autofactory_F-nose_white-center.1-26.1-0 ; 
       autofactory_F-nose_white-center.1-27.1-0 ; 
       autofactory_F-nose_white-center.1-28.1-0 ; 
       autofactory_F-nose_white-center.1-3.1-0 ; 
       autofactory_F-nose_white-center.1-4.1-0 ; 
       autofactory_F-nose_white-center.1-5.1-0 ; 
       autofactory_F-nose_white-center.1-6.1-0 ; 
       autofactory_F-nose_white-center.1-7.1-0 ; 
       autofactory_F-nose_white-center.1-8.1-0 ; 
       autofactory_F-nose_white-center.1-9.1-0 ; 
       autofactory_F-port_red-left.2-0.1-0 ; 
       autofactory_F-starbord_green-right.1-0.1-0 ; 
       autofact_msPTL-mat1.1-0 ; 
       autofact_msPTL-mat10.1-0 ; 
       autofact_msPTL-mat11.1-0 ; 
       autofact_msPTL-mat12.1-0 ; 
       autofact_msPTL-mat13.1-0 ; 
       autofact_msPTL-mat16.1-0 ; 
       autofact_msPTL-mat17.1-0 ; 
       autofact_msPTL-mat18.1-0 ; 
       autofact_msPTL-mat19.1-0 ; 
       autofact_msPTL-mat2.1-0 ; 
       autofact_msPTL-mat20.1-0 ; 
       autofact_msPTL-mat21.1-0 ; 
       autofact_msPTL-mat22.1-0 ; 
       autofact_msPTL-mat23.1-0 ; 
       autofact_msPTL-mat24.1-0 ; 
       autofact_msPTL-mat25.1-0 ; 
       autofact_msPTL-mat26.1-0 ; 
       autofact_msPTL-mat27.1-0 ; 
       autofact_msPTL-mat28.1-0 ; 
       autofact_msPTL-mat3.1-0 ; 
       autofact_msPTL-mat31.1-0 ; 
       autofact_msPTL-mat32.1-0 ; 
       autofact_msPTL-mat33.1-0 ; 
       autofact_msPTL-mat34.1-0 ; 
       autofact_msPTL-mat35.1-0 ; 
       autofact_msPTL-mat36.1-0 ; 
       autofact_msPTL-mat38.1-0 ; 
       autofact_msPTL-mat39.1-0 ; 
       autofact_msPTL-mat4.1-0 ; 
       autofact_msPTL-mat40.1-0 ; 
       autofact_msPTL-mat41.1-0 ; 
       autofact_msPTL-mat42.1-0 ; 
       autofact_msPTL-mat5.1-0 ; 
       autofact_msPTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       autofactory_F-acs07.4-0 ; 
       autofactory_F-bantenn1.1-0 ; 
       autofactory_F-bantenn2.1-0 ; 
       autofactory_F-bportal.1-0 ; 
       autofactory_F-bturatt.1-0 ; 
       autofactory_F-doccon.2-0 ; 
       autofactory_F-fuselg1.3-0 ; 
       autofactory_F-fuselg2.1-0 ; 
       autofactory_F-lantenn1.1-0 ; 
       autofactory_F-lantenn2.1-0 ; 
       autofactory_F-lantenn3.1-0 ; 
       autofactory_F-llndpad1.1-0 ; 
       autofactory_F-llndpad2.1-0 ; 
       autofactory_F-null1.1-0 ROOT ; 
       autofactory_F-platfm.1-0 ; 
       autofactory_F-portal0.1-0 ; 
       autofactory_F-rantenn1.1-0 ; 
       autofactory_F-rantenn2.1-0 ; 
       autofactory_F-rantenn3.1-0 ; 
       autofactory_F-rlndpad1.1-0 ; 
       autofactory_F-rlndpad2.1-0 ; 
       autofactory_F-SSa4.1-0 ; 
       autofactory_F-SSa5.1-0 ; 
       autofactory_F-SSb1.1-0 ; 
       autofactory_F-SSbp1.1-0 ; 
       autofactory_F-SSbp2.1-0 ; 
       autofactory_F-SSl.1-0 ; 
       autofactory_F-SSlp0.1-0 ; 
       autofactory_F-SSlp1.1-0 ; 
       autofactory_F-SSlp2.1-0 ; 
       autofactory_F-SSlp3.1-0 ; 
       autofactory_F-SSlp4.1-0 ; 
       autofactory_F-SSlp5.1-0 ; 
       autofactory_F-SSlpp0.1-0 ; 
       autofactory_F-SSlpp1.1-0 ; 
       autofactory_F-SSlpp2.1-0 ; 
       autofactory_F-SSlpp3.1-0 ; 
       autofactory_F-SSlpp4.1-0 ; 
       autofactory_F-SSlpp5.1-0 ; 
       autofactory_F-SSr.1-0 ; 
       autofactory_F-SSrp0.1-0 ; 
       autofactory_F-SSrp1.1-0 ; 
       autofactory_F-SSrp2.1-0 ; 
       autofactory_F-SSrp3.1-0 ; 
       autofactory_F-SSrp4.1-0 ; 
       autofactory_F-SSrp5.1-0 ; 
       autofactory_F-SSrpp0.1-0 ; 
       autofactory_F-SSrpp1.1-0 ; 
       autofactory_F-SSrpp2.1-0 ; 
       autofactory_F-SSrpp3.1-0 ; 
       autofactory_F-SSrpp4.1-0 ; 
       autofactory_F-SSrpp5.1-0 ; 
       autofactory_F-SStp1.1-0 ; 
       autofactory_F-SStp2.1-0 ; 
       autofactory_F-strake.1-0 ; 
       autofactory_F-tportal.2-0 ; 
       autofactory_F-tractr.1-0 ; 
       autofactory_F-tturatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs07/PICTURES/acs07 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs07-autofactory_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 32     
       autofact_msPTL-t2d1.1-0 ; 
       autofact_msPTL-t2d10.1-0 ; 
       autofact_msPTL-t2d11.1-0 ; 
       autofact_msPTL-t2d13.1-0 ; 
       autofact_msPTL-t2d14.1-0 ; 
       autofact_msPTL-t2d15.1-0 ; 
       autofact_msPTL-t2d16.1-0 ; 
       autofact_msPTL-t2d17.1-0 ; 
       autofact_msPTL-t2d18.1-0 ; 
       autofact_msPTL-t2d19.1-0 ; 
       autofact_msPTL-t2d2.1-0 ; 
       autofact_msPTL-t2d20.1-0 ; 
       autofact_msPTL-t2d21.1-0 ; 
       autofact_msPTL-t2d22.1-0 ; 
       autofact_msPTL-t2d23.1-0 ; 
       autofact_msPTL-t2d24.1-0 ; 
       autofact_msPTL-t2d25.1-0 ; 
       autofact_msPTL-t2d28.1-0 ; 
       autofact_msPTL-t2d29.1-0 ; 
       autofact_msPTL-t2d3.1-0 ; 
       autofact_msPTL-t2d30.1-0 ; 
       autofact_msPTL-t2d31.1-0 ; 
       autofact_msPTL-t2d32.1-0 ; 
       autofact_msPTL-t2d33.1-0 ; 
       autofact_msPTL-t2d35.1-0 ; 
       autofact_msPTL-t2d36.1-0 ; 
       autofact_msPTL-t2d38.1-0 ; 
       autofact_msPTL-t2d39.1-0 ; 
       autofact_msPTL-t2d40.1-0 ; 
       autofact_msPTL-t2d7.1-0 ; 
       autofact_msPTL-t2d8.1-0 ; 
       autofact_msPTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       21 6 110 ; 
       22 6 110 ; 
       23 2 110 ; 
       24 3 110 ; 
       25 3 110 ; 
       26 9 110 ; 
       27 14 110 ; 
       28 27 110 ; 
       29 27 110 ; 
       30 27 110 ; 
       31 27 110 ; 
       32 27 110 ; 
       33 14 110 ; 
       34 33 110 ; 
       35 33 110 ; 
       36 33 110 ; 
       37 33 110 ; 
       38 33 110 ; 
       39 17 110 ; 
       40 14 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 40 110 ; 
       46 14 110 ; 
       47 46 110 ; 
       48 46 110 ; 
       49 46 110 ; 
       50 46 110 ; 
       51 46 110 ; 
       52 55 110 ; 
       53 55 110 ; 
       0 13 110 ; 
       1 6 110 ; 
       2 1 110 ; 
       3 15 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       14 56 110 ; 
       15 6 110 ; 
       16 6 110 ; 
       17 16 110 ; 
       18 17 110 ; 
       19 14 110 ; 
       20 14 110 ; 
       54 6 110 ; 
       55 15 110 ; 
       56 6 110 ; 
       57 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 20 300 ; 
       22 21 300 ; 
       23 0 300 ; 
       24 25 300 ; 
       25 24 300 ; 
       26 27 300 ; 
       28 5 300 ; 
       29 6 300 ; 
       30 7 300 ; 
       31 8 300 ; 
       32 9 300 ; 
       34 15 300 ; 
       35 16 300 ; 
       36 17 300 ; 
       37 18 300 ; 
       38 19 300 ; 
       39 28 300 ; 
       41 1 300 ; 
       42 2 300 ; 
       43 26 300 ; 
       44 3 300 ; 
       45 4 300 ; 
       47 10 300 ; 
       48 11 300 ; 
       49 12 300 ; 
       50 13 300 ; 
       51 14 300 ; 
       52 22 300 ; 
       53 23 300 ; 
       1 42 300 ; 
       2 41 300 ; 
       3 45 300 ; 
       3 46 300 ; 
       3 54 300 ; 
       5 43 300 ; 
       6 29 300 ; 
       6 38 300 ; 
       6 48 300 ; 
       6 57 300 ; 
       6 56 300 ; 
       6 58 300 ; 
       6 59 300 ; 
       7 33 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       8 36 300 ; 
       9 34 300 ; 
       10 35 300 ; 
       14 61 300 ; 
       14 47 300 ; 
       14 55 300 ; 
       14 60 300 ; 
       16 40 300 ; 
       17 37 300 ; 
       18 39 300 ; 
       54 32 300 ; 
       55 44 300 ; 
       55 52 300 ; 
       55 53 300 ; 
       56 62 300 ; 
       56 30 300 ; 
       56 31 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       13 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       30 30 401 ; 
       31 31 401 ; 
       32 1 401 ; 
       33 2 401 ; 
       34 3 401 ; 
       35 4 401 ; 
       36 5 401 ; 
       37 6 401 ; 
       38 0 401 ; 
       39 7 401 ; 
       40 8 401 ; 
       41 9 401 ; 
       42 11 401 ; 
       43 12 401 ; 
       44 13 401 ; 
       45 14 401 ; 
       46 15 401 ; 
       47 16 401 ; 
       48 10 401 ; 
       49 17 401 ; 
       50 18 401 ; 
       51 20 401 ; 
       52 21 401 ; 
       53 22 401 ; 
       54 23 401 ; 
       55 24 401 ; 
       56 25 401 ; 
       57 19 401 ; 
       58 28 401 ; 
       59 26 401 ; 
       60 27 401 ; 
       62 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 16.31748 -90.0156 0 USR MPRFLG 0 ; 
       0 SCHEM 14.10597 -90.01569 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25.53599 -86.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25.53599 -88.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25.53599 -90.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       21 SCHEM 36.03599 -102.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 36.03599 -104.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 43.03599 -158.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 43.03599 -176.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 43.03599 -178.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 43.03599 -156.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 43.03599 -128.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 46.53599 -132.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 46.53599 -130.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 46.53599 -128.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 46.53599 -126.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 46.53599 -124.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 43.03599 -148.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 46.53599 -152.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 46.53599 -150.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 46.53599 -148.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 46.53599 -146.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 46.53599 -144.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 43.03599 -162.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 43.03599 -138.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 46.53599 -142.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 46.53599 -140.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 46.53599 -138.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 46.53599 -136.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 46.53599 -134.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 43.03599 -118.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 46.53599 -122.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 46.53599 -120.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 46.53599 -118.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 46.53599 -116.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 46.53599 -114.2335 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 43.03599 -172.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 43.03599 -174.2335 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 29.03599 -141.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 36.03599 -158.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 39.53599 -158.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 39.53599 -177.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 36.03599 -168.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 36.03599 -170.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 32.53599 -141.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 36.03599 -164.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 36.03599 -155.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 39.53599 -155.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 43.03599 -154.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 43.03599 -112.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 43.03599 -108.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 25.53599 -141.2335 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 39.53599 -129.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 36.03599 -175.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 36.03599 -161.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 39.53599 -161.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 43.03599 -160.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 43.03599 -106.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 43.03599 -110.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 36.03599 -180.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 39.53599 -173.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 36.03599 -129.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 36.03599 -166.2335 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       29 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 39.53599 -104.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39.53599 -104.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 39.53599 -180.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 39.53599 -164.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 43.03599 -152.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 46.53599 -154.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39.53599 -152.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 43.03599 -158.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 46.53599 -160.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 39.53599 -158.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 43.03599 -156.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39.53599 -156.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39.53599 -170.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 43.03599 -170.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 43.03599 -174.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 43.03599 -174.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 43.03599 -101.274 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 39.53599 -164.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 39.53599 -164.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 39.53599 -164.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 43.03599 -170.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 43.03599 -170.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 43.03599 -174.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 43.03599 -101.274 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 36.03599 -100.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 43.03599 -101.274 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 43.03599 -101.274 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 39.53599 -104.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 46.53599 -158.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50.03599 -142.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 50.03599 -140.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 50.03599 -136.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 50.03599 -134.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50.03599 -132.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 50.03599 -130.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 50.03599 -128.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 50.03599 -126.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 50.03599 -124.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 50.03599 -122.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50.03599 -120.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 50.03599 -118.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 50.03599 -116.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50.03599 -114.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50.03599 -152.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 50.03599 -150.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 50.03599 -148.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50.03599 -146.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 50.03599 -144.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39.53599 -102.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39.53599 -104.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 46.53599 -172.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 46.53599 -174.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 46.53599 -178.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.53599 -176.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 50.03599 -138.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 46.53599 -156.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 46.53599 -162.4835 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 43.03599 -180.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 43.03599 -164.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 46.53599 -152.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 50.03599 -154.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 43.03599 -152.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 46.53599 -158.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 50.03599 -160.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 43.03599 -158.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 46.53599 -156.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 43.03599 -156.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 43.03599 -170.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.53599 -170.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.53599 -174.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.53599 -174.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 46.53599 -101.274 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 43.03599 -164.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 43.03599 -164.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 43.03599 -164.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 46.53599 -170.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.53599 -170.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 46.53599 -174.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 46.53599 -101.274 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 46.53599 -101.274 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 39.53599 -100.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 43.03599 -104.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 43.03599 -104.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 43.03599 -104.4835 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 29.03599 -100.4835 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 6 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
