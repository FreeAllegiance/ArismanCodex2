SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.85-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_color-light1.2-0 ROOT ; 
       add_color-light2.2-0 ROOT ; 
       add_color-light3.2-0 ROOT ; 
       add_color-light4.2-0 ROOT ; 
       add_color-light5.2-0 ROOT ; 
       add_color-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       add_color-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       add_color-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       add_color-cube1.2-0 ; 
       add_color-cube2.1-0 ; 
       add_color-cube3.1-0 ; 
       add_color-cyl1.1-0 ; 
       add_color-cyl2.1-0 ; 
       add_color-cyl2_1.1-0 ; 
       add_color-east_bay_11.5-0 ; 
       add_color-east_bay_antenna1.1-0 ; 
       add_color-east_bay_strut_2.1-0 ; 
       add_color-east_bay_strut2.2-0 ; 
       add_color-garage1A.1-0 ; 
       add_color-garage1B.1-0 ; 
       add_color-garage1C.1-0 ; 
       add_color-garage1D.1-0 ; 
       add_color-garage1E.1-0 ; 
       add_color-landing.1-0 ; 
       add_color-landing_lights1.1-0 ; 
       add_color-landing_lights2.1-0 ; 
       add_color-launch1.1-0 ; 
       add_color-null1.1-0 ROOT ; 
       add_color-south_hull_2.33-0 ; 
       add_color-sphere2.1-0 ; 
       add_color-sphere3.1-0 ; 
       add_color-SS_17.1-0 ; 
       add_color-SS_18.1-0 ; 
       add_color-SS_19.1-0 ; 
       add_color-SS_20.1-0 ; 
       add_color-SS_21.1-0 ; 
       add_color-SS_22.1-0 ; 
       add_color-SS_23.1-0 ; 
       add_color-SS_24.1-0 ; 
       add_color-SS_25.1-0 ; 
       add_color-SS_26.1-0 ; 
       add_color-SS_27.1-0 ; 
       add_color-SS_28.1-0 ; 
       add_color-SS_8.1-0 ; 
       add_color-SS_9.1-0 ; 
       add_color-strobe_set.1-0 ; 
       add_color-turwepemt1.1-0 ; 
       add_color-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-add_color.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       add_color-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 15 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 19 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 15 110 ; 
       20 19 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       32 17 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 20 110 ; 
       36 7 110 ; 
       37 6 110 ; 
       38 20 110 ; 
       39 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 61.25 -8 0 MPRFLG 0 ; 
       1 SCHEM 98.75 -10 0 MPRFLG 0 ; 
       2 SCHEM 97.5 -12 0 MPRFLG 0 ; 
       3 SCHEM 87.5 -10 0 MPRFLG 0 ; 
       4 SCHEM 86.25 -12 0 MPRFLG 0 ; 
       5 SCHEM 91.25 -12 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -14 0 MPRFLG 0 ; 
       7 SCHEM 21.25 -16 0 MPRFLG 0 ; 
       8 SCHEM 46.25 -12 0 MPRFLG 0 ; 
       9 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       10 SCHEM 0 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 10 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 33.75 -18 0 MPRFLG 0 ; 
       17 SCHEM 48.75 -18 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 55 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 80 -12 0 MPRFLG 0 ; 
       22 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 40 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 27.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 30 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 32.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 35 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 37.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 55 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 42.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 45 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 47.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 50 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 52.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 20 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 41.25 -16 0 MPRFLG 0 ; 
       38 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 25 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 95 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
