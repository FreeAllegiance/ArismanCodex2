SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.71-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       acs08_bound-bbound4.1-0 ; 
       acs08_bound-bbound5.2-0 ; 
       acs08_bound-bound.3-0 ; 
       acs08_bound-midbound2.1-0 ; 
       acs08_bound-midbound3.1-0 ; 
       acs08_bound-midbound6.1-0 ; 
       acs08_bound-midbound7.1-0 ; 
       acs08_bound-root.1-0 ROOT ; 
       acs08_bound-tbound1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-bound.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 7 110 ; 
       8 7 110 ; 
       3 7 110 ; 
       4 7 110 ; 
       0 7 110 ; 
       1 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 20 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 99 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
