SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.72-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       bounding_model-bounding_model.1-0 ROOT ; 
       bounding_model-midbound1.3-0 ; 
       bounding_model-midbound2.1-0 ; 
       bounding_model-midbound3.1-0 ; 
       bounding_model-midbound6.1-0 ; 
       bounding_model-midbound7.1-0 ; 
       bounding_model-tbound1.1-0 ; 
       bounding_model-tbound4.1-0 ; 
       bounding_model-tbound5.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-bound.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 0 110 ; 
       8 0 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 20 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 MPRFLG 0 ; 
       0 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 99 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
