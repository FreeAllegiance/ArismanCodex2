SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.51-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       fic_turret-light1.1-0 ROOT ; 
       fic_turret-light2.1-0 ROOT ; 
       fic_turret-light3.1-0 ROOT ; 
       fic_turret-light4.1-0 ROOT ; 
       fic_turret-light5.1-0 ROOT ; 
       fic_turret-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       fic_turret-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       fic_turret-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 33     
       fic_turret-cube1.2-0 ; 
       fic_turret-cube2.1-0 ; 
       fic_turret-cube3.1-0 ; 
       fic_turret-cyl1.1-0 ; 
       fic_turret-cyl2.1-0 ; 
       fic_turret-cyl2_1.1-0 ; 
       fic_turret-east_bay_11.5-0 ; 
       fic_turret-east_bay_antenna1.1-0 ; 
       fic_turret-east_bay_strut_2.1-0 ; 
       fic_turret-east_bay_strut2.2-0 ; 
       fic_turret-landing_lights1.1-0 ; 
       fic_turret-landing_lights2.1-0 ; 
       fic_turret-null1.1-0 ROOT ; 
       fic_turret-south_hull_2.33-0 ; 
       fic_turret-sphere2.1-0 ; 
       fic_turret-sphere3.1-0 ; 
       fic_turret-SS_17.1-0 ; 
       fic_turret-SS_18.1-0 ; 
       fic_turret-SS_19.1-0 ; 
       fic_turret-SS_20.1-0 ; 
       fic_turret-SS_21.1-0 ; 
       fic_turret-SS_22.1-0 ; 
       fic_turret-SS_23.1-0 ; 
       fic_turret-SS_24.1-0 ; 
       fic_turret-SS_25.1-0 ; 
       fic_turret-SS_26.1-0 ; 
       fic_turret-SS_27.1-0 ; 
       fic_turret-SS_28.1-0 ; 
       fic_turret-SS_8.1-0 ; 
       fic_turret-SS_9.1-0 ; 
       fic_turret-strobe_set.1-0 ; 
       fic_turret-turwepemt1.1-0 ; 
       fic_turret-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-fic_turret.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       fic_turret-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 13 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 30 110 ; 
       11 30 110 ; 
       13 12 110 ; 
       14 3 110 ; 
       15 3 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       18 10 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 13 110 ; 
       29 7 110 ; 
       30 6 110 ; 
       31 13 110 ; 
       32 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 107.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 110 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 82.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 71.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 76.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 30 -10 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 8.75 -12 0 MPRFLG 0 ; 
       8 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 MPRFLG 0 ; 
       10 SCHEM 21.25 -14 0 MPRFLG 0 ; 
       11 SCHEM 36.25 -14 0 MPRFLG 0 ; 
       12 SCHEM 48.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       13 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 65 -8 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 15 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 17.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 20 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 22.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 25 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 42.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 30 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 32.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 35 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 37.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 40 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 7.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 28.75 -12 0 MPRFLG 0 ; 
       31 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 12.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
