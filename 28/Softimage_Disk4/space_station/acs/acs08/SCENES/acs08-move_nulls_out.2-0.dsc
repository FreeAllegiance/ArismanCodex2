SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.92-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 12     
       bound-light1.4-0 ROOT ; 
       bound-light2.4-0 ROOT ; 
       bound-light3.4-0 ROOT ; 
       bound-light4.4-0 ROOT ; 
       bound-light5.4-0 ROOT ; 
       bound-light6.4-0 ROOT ; 
       move_nulls_out-light1.2-0 ROOT ; 
       move_nulls_out-light2.2-0 ROOT ; 
       move_nulls_out-light3.2-0 ROOT ; 
       move_nulls_out-light4.2-0 ROOT ; 
       move_nulls_out-light5.2-0 ROOT ; 
       move_nulls_out-light6.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       move_nulls_out-mat12.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       move_nulls_out-Static_Cling1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       move_nulls_out-cube1_1.2-0 ; 
       move_nulls_out-cube2.1-0 ; 
       move_nulls_out-cube3.1-0 ; 
       move_nulls_out-cyl1.1-0 ; 
       move_nulls_out-cyl2.1-0 ; 
       move_nulls_out-cyl2_1.1-0 ; 
       move_nulls_out-east_bay_11.5-0 ; 
       move_nulls_out-east_bay_antenna1.1-0 ; 
       move_nulls_out-east_bay_strut_2.1-0 ; 
       move_nulls_out-east_bay_strut2.2-0 ; 
       move_nulls_out-garage1A.1-0 ; 
       move_nulls_out-garage1B.1-0 ; 
       move_nulls_out-garage1C.1-0 ; 
       move_nulls_out-garage1D.1-0 ; 
       move_nulls_out-garage1E.1-0 ; 
       move_nulls_out-landing.1-0 ; 
       move_nulls_out-landing_lights1.1-0 ; 
       move_nulls_out-landing_lights2.1-0 ; 
       move_nulls_out-launch1.1-0 ; 
       move_nulls_out-null1.2-0 ROOT ; 
       move_nulls_out-south_hull_2_1.33-0 ; 
       move_nulls_out-sphere2.1-0 ; 
       move_nulls_out-sphere3.1-0 ; 
       move_nulls_out-SS_17.1-0 ; 
       move_nulls_out-SS_18.1-0 ; 
       move_nulls_out-SS_19.1-0 ; 
       move_nulls_out-SS_20.1-0 ; 
       move_nulls_out-SS_21.1-0 ; 
       move_nulls_out-SS_22.1-0 ; 
       move_nulls_out-SS_23.1-0 ; 
       move_nulls_out-SS_24.1-0 ; 
       move_nulls_out-SS_25.1-0 ; 
       move_nulls_out-SS_26.1-0 ; 
       move_nulls_out-SS_27.1-0 ; 
       move_nulls_out-SS_28.1-0 ; 
       move_nulls_out-SS_8.1-0 ; 
       move_nulls_out-SS_9.1-0 ; 
       move_nulls_out-strobe_set.1-0 ; 
       move_nulls_out-turwepemt1.1-0 ; 
       move_nulls_out-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-move_nulls_out.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       move_nulls_out-t2d8.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 15 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 19 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 15 110 ; 
       20 19 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       32 17 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 20 110 ; 
       36 7 110 ; 
       37 6 110 ; 
       38 20 110 ; 
       39 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 15 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 17.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       10 SCHEM 5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 65 -10 0 MPRFLG 0 ; 
       2 SCHEM 65 -12 0 MPRFLG 0 ; 
       3 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       4 SCHEM 60 -12 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       6 SCHEM 36.25 -14 0 MPRFLG 0 ; 
       7 SCHEM 20 -16 0 MPRFLG 0 ; 
       8 SCHEM 36.25 -12 0 MPRFLG 0 ; 
       9 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       10 SCHEM 0 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 2.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 7.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 10 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       16 SCHEM 31.25 -18 0 MPRFLG 0 ; 
       17 SCHEM 46.25 -18 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 40 -6 0 MPRFLG 0 ; 
       21 SCHEM 55 -12 0 MPRFLG 0 ; 
       22 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 37.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 25 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 27.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 30 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 32.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 35 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 52.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 40 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 45 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 47.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 50 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 17.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 20 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 38.75 -16 0 MPRFLG 0 ; 
       38 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 22.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 64 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 64 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 99 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
