SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.83-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       remove_back-light1.4-0 ROOT ; 
       remove_back-light2.4-0 ROOT ; 
       remove_back-light3.4-0 ROOT ; 
       remove_back-light4.4-0 ROOT ; 
       remove_back-light5.4-0 ROOT ; 
       remove_back-light6.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       remove_back-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       remove_back-Static_Cling1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       remove_back-cube1.2-0 ; 
       remove_back-cube2.1-0 ; 
       remove_back-cube3.1-0 ; 
       remove_back-cyl1.1-0 ; 
       remove_back-cyl2.1-0 ; 
       remove_back-cyl2_1.1-0 ; 
       remove_back-east_bay_11.5-0 ; 
       remove_back-east_bay_antenna1.1-0 ; 
       remove_back-east_bay_strut_2.1-0 ; 
       remove_back-east_bay_strut2.2-0 ; 
       remove_back-garage1A.1-0 ; 
       remove_back-garage1B.1-0 ; 
       remove_back-garage1C.1-0 ; 
       remove_back-garage1D.1-0 ; 
       remove_back-garage1E.1-0 ; 
       remove_back-landing.1-0 ; 
       remove_back-landing_lights1.1-0 ; 
       remove_back-landing_lights2.1-0 ; 
       remove_back-launch1.1-0 ; 
       remove_back-null1.2-0 ROOT ; 
       remove_back-south_hull_2.33-0 ; 
       remove_back-sphere2.1-0 ; 
       remove_back-sphere3.1-0 ; 
       remove_back-SS_17.1-0 ; 
       remove_back-SS_18.1-0 ; 
       remove_back-SS_19.1-0 ; 
       remove_back-SS_20.1-0 ; 
       remove_back-SS_21.1-0 ; 
       remove_back-SS_22.1-0 ; 
       remove_back-SS_23.1-0 ; 
       remove_back-SS_24.1-0 ; 
       remove_back-SS_25.1-0 ; 
       remove_back-SS_26.1-0 ; 
       remove_back-SS_27.1-0 ; 
       remove_back-SS_28.1-0 ; 
       remove_back-SS_8.1-0 ; 
       remove_back-SS_9.1-0 ; 
       remove_back-strobe_set.1-0 ; 
       remove_back-turwepemt1.1-0 ; 
       remove_back-turwepemt2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-remove_back.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       remove_back-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 20 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 15 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 19 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 15 110 ; 
       20 19 110 ; 
       21 3 110 ; 
       22 3 110 ; 
       23 16 110 ; 
       24 16 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       29 17 110 ; 
       30 17 110 ; 
       31 17 110 ; 
       32 17 110 ; 
       33 17 110 ; 
       34 17 110 ; 
       35 20 110 ; 
       36 7 110 ; 
       37 6 110 ; 
       38 20 110 ; 
       39 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 242.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 245 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 247.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 250 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 252.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 255 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 192.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 228.75 -6 0 MPRFLG 0 ; 
       2 SCHEM 227.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 217.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 216.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 221.25 -8 0 MPRFLG 0 ; 
       6 SCHEM 175 -10 0 MPRFLG 0 ; 
       7 SCHEM 153.75 -12 0 MPRFLG 0 ; 
       8 SCHEM 177.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 180 -6 0 MPRFLG 0 ; 
       10 SCHEM 132.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 135 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 137.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 140 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 142.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 138.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 166.25 -14 0 MPRFLG 0 ; 
       17 SCHEM 181.25 -14 0 MPRFLG 0 ; 
       18 SCHEM 145 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       19 SCHEM 186.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       20 SCHEM 193.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 210 -8 0 MPRFLG 0 ; 
       22 SCHEM 212.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 172.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 160 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 162.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 165 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 167.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 170 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 187.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 175 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 177.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 180 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 182.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 185 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 150 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 152.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 173.75 -12 0 MPRFLG 0 ; 
       38 SCHEM 147.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 157.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 225 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 225 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
