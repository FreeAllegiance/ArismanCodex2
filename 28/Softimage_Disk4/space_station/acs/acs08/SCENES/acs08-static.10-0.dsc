SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.88-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1.8-0 ROOT ; 
       static-light2.8-0 ROOT ; 
       static-light3.8-0 ROOT ; 
       static-light4.8-0 ROOT ; 
       static-light5.8-0 ROOT ; 
       static-light6.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       static-mat12.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       static-Static_Cling1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       static-cube1.2-0 ; 
       static-cube2.1-0 ; 
       static-cube3.1-0 ; 
       static-cyl1.1-0 ; 
       static-cyl2.1-0 ; 
       static-cyl2_1.1-0 ; 
       static-east_bay_11.5-0 ; 
       static-east_bay_antenna1.1-0 ; 
       static-east_bay_strut_2.1-0 ; 
       static-east_bay_strut2.2-0 ; 
       static-null1.4-0 ROOT ; 
       static-south_hull_2.33-0 ; 
       static-sphere2.1-0 ; 
       static-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-static.10-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       static-t2d8.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       11 10 110 ; 
       12 3 110 ; 
       13 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 25 -6 0 MPRFLG 0 ; 
       2 SCHEM 25 -8 0 MPRFLG 0 ; 
       3 SCHEM 18.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 20 -8 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 10 -10 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       8 SCHEM 10 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -6 0 MPRFLG 0 ; 
       10 SCHEM 13.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
