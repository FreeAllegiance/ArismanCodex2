SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.54-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       static-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       static-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 13     
       static-cube1.2-0 ; 
       static-cube2.1-0 ; 
       static-cube3.1-0 ; 
       static-cyl1.1-0 ; 
       static-cyl2.1-0 ; 
       static-cyl2_1.1-0 ; 
       static-east_bay_11.5-0 ; 
       static-east_bay_strut_2.1-0 ; 
       static-east_bay_strut2.2-0 ; 
       static-null1.1-0 ROOT ; 
       static-south_hull_2.33-0 ; 
       static-sphere2.1-0 ; 
       static-sphere3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-static.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       static-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 10 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 7 110 ; 
       7 8 110 ; 
       8 0 110 ; 
       10 9 110 ; 
       11 3 110 ; 
       12 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
