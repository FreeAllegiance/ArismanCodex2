SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.37-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       done_text-light1.1-0 ROOT ; 
       done_text-light2.1-0 ROOT ; 
       done_text-light3.1-0 ROOT ; 
       done_text-light4.1-0 ROOT ; 
       done_text-light5.1-0 ROOT ; 
       done_text-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       done_text-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       done_text-Static_Cling1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 41     
       done_text-cube4.1-0 ROOT ; 
       done_text-cube6.1-0 ROOT ; 
       ss27-cube1.2-0 ; 
       ss27-cube2.1-0 ; 
       ss27-cube3.1-0 ; 
       ss27-cyl1.1-0 ; 
       ss27-cyl2.1-0 ; 
       ss27-cyl2_1.1-0 ; 
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut2.2-0 ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-south_hull_2.32-0 ROOT ; 
       ss27-sphere2.1-0 ; 
       ss27-sphere3.1-0 ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-done_text.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       done_text-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 15 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 2 110 ; 
       8 10 110 ; 
       9 8 110 ; 
       10 11 110 ; 
       11 2 110 ; 
       12 38 110 ; 
       13 38 110 ; 
       14 38 110 ; 
       17 5 110 ; 
       16 5 110 ; 
       18 9 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 13 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 13 110 ; 
       31 14 110 ; 
       32 14 110 ; 
       33 14 110 ; 
       34 14 110 ; 
       35 14 110 ; 
       36 14 110 ; 
       37 15 110 ; 
       38 8 110 ; 
       39 15 110 ; 
       40 8 110 ; 
       6 5 110 ; 
       7 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 10 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 15 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -22 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 96.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 95 -10 0 MPRFLG 0 ; 
       5 SCHEM 85 -8 0 MPRFLG 0 ; 
       8 SCHEM 35 -12 0 MPRFLG 0 ; 
       9 SCHEM 6.25 -14 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 40 -8 0 MPRFLG 0 ; 
       12 SCHEM 33.75 -16 0 MPRFLG 0 ; 
       13 SCHEM 18.75 -16 0 MPRFLG 0 ; 
       14 SCHEM 48.75 -16 0 MPRFLG 0 ; 
       15 SCHEM 53.75 -4 0 SRT 1 1 1 -3.496911e-007 0 0 0 2.087002e-007 0.5968131 MPRFLG 0 ; 
       17 SCHEM 80 -10 0 MPRFLG 0 ; 
       16 SCHEM 77.5 -10 0 MPRFLG 0 ; 
       18 SCHEM 5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 40 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 30 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 35 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 37.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 25 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 12.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 15 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 17.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 20 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 55 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 42.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 45 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 47.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 50 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 52.5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 33.75 -14 0 MPRFLG 0 ; 
       39 SCHEM 0 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 10 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 83.75 -10 0 MPRFLG 0 ; 
       7 SCHEM 88.75 -10 0 MPRFLG 0 ; 
       0 SCHEM 0 -22 0 SRT 0.07222001 0.05024 0.314 0 0 0 0.002414935 7.808809 4.82089 MPRFLG 0 ; 
       1 SCHEM 2.5 -22 0 SRT 0.07222001 0.05024 0.35482 1.570796 0 0 0.00246596 7.399751 4.92292 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
