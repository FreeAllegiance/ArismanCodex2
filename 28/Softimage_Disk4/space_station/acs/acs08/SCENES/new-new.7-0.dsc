SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.7-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       ss27-cube1.2-0 ; 
       ss27-cube2.1-0 ; 
       ss27-cube3.1-0 ; 
       ss27-cyl1.1-0 ; 
       ss27-east_bay_7.7-0 ; 
       ss27-east_bay_antenna.1-0 ; 
       ss27-east_bay_strut_2.1-0 ; 
       ss27-east_bay_strut2.2-0 ; 
       ss27-landing_lights3.1-0 ; 
       ss27-landing_lights4.1-0 ; 
       ss27-landing_lights5.1-0 ; 
       ss27-south_hull_2.6-0 ROOT ; 
       ss27-sphere1.1-0 ; 
       ss27-sphere2.1-0 ; 
       ss27-SS_10.1-0 ; 
       ss27-SS_29.1-0 ; 
       ss27-SS_30.1-0 ; 
       ss27-SS_31.1-0 ; 
       ss27-SS_32.1-0 ; 
       ss27-SS_33.1-0 ; 
       ss27-SS_34.1-0 ; 
       ss27-SS_35.1-0 ; 
       ss27-SS_36.1-0 ; 
       ss27-SS_37.1-0 ; 
       ss27-SS_38.1-0 ; 
       ss27-SS_39.1-0 ; 
       ss27-SS_40.1-0 ; 
       ss27-SS_41.1-0 ; 
       ss27-SS_42.1-0 ; 
       ss27-SS_43.1-0 ; 
       ss27-SS_44.1-0 ; 
       ss27-SS_45.1-0 ; 
       ss27-SS_46.1-0 ; 
       ss27-SS_8.1-0 ; 
       ss27-strobe_set1.1-0 ; 
       ss27-turwepemt1.1-0 ; 
       ss27-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27 ; 
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-new.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 11 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 6 110 ; 
       5 4 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 34 110 ; 
       9 34 110 ; 
       10 34 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 5 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 8 110 ; 
       21 9 110 ; 
       22 9 110 ; 
       23 9 110 ; 
       24 9 110 ; 
       25 9 110 ; 
       26 9 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 10 110 ; 
       30 10 110 ; 
       31 10 110 ; 
       32 10 110 ; 
       33 11 110 ; 
       34 4 110 ; 
       35 11 110 ; 
       36 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 245 -2 0 MPRFLG 0 ; 
       1 SCHEM 272.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 272.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 268.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 241.25 -8 0 MPRFLG 0 ; 
       5 SCHEM 217.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 241.25 -6 0 MPRFLG 0 ; 
       7 SCHEM 241.25 -4 0 MPRFLG 0 ; 
       8 SCHEM 243.75 -12 0 MPRFLG 0 ; 
       9 SCHEM 228.75 -12 0 MPRFLG 0 ; 
       10 SCHEM 258.75 -12 0 MPRFLG 0 ; 
       11 SCHEM 242.5 0 0 SRT 1 1 1 -3.496911e-007 0 0 0 2.087002e-007 0.5968131 MPRFLG 0 ; 
       12 SCHEM 267.5 -6 0 MPRFLG 0 ; 
       13 SCHEM 270 -6 0 MPRFLG 0 ; 
       14 SCHEM 217.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 250 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 237.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 240 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 242.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 245 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 247.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 235 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 222.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 225 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 227.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 230 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 232.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 265 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 252.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 255 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 257.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 260 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 262.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 215 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 243.75 -10 0 MPRFLG 0 ; 
       35 SCHEM 212.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 220 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 7 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
