SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.40-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1.3-0 ROOT ; 
       rotate-light2.3-0 ROOT ; 
       rotate-light3.3-0 ROOT ; 
       rotate-light4.3-0 ROOT ; 
       rotate-light5.3-0 ROOT ; 
       rotate-light6.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       rotate-mat12.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS NBELEM 1     
       rotate-Static_Cling1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       rotate-cube1.2-0 ; 
       rotate-cube2.1-0 ; 
       rotate-cube3.1-0 ; 
       rotate-cyl1.1-0 ; 
       rotate-cyl2.1-0 ; 
       rotate-cyl2_1.1-0 ; 
       rotate-east_bay_7.7-0 ; 
       rotate-east_bay_antenna.1-0 ; 
       rotate-east_bay_strut_2.1-0 ; 
       rotate-east_bay_strut2.2-0 ; 
       rotate-landing_lights3.1-0 ; 
       rotate-landing_lights4.1-0 ; 
       rotate-landing_lights5.1-0 ; 
       rotate-null1.1-0 ROOT ; 
       rotate-south_hull_2.33-0 ; 
       rotate-sphere2.1-0 ; 
       rotate-sphere3.1-0 ; 
       rotate-SS_10.1-0 ; 
       rotate-SS_29.1-0 ; 
       rotate-SS_30.1-0 ; 
       rotate-SS_31.1-0 ; 
       rotate-SS_32.1-0 ; 
       rotate-SS_33.1-0 ; 
       rotate-SS_34.1-0 ; 
       rotate-SS_35.1-0 ; 
       rotate-SS_36.1-0 ; 
       rotate-SS_37.1-0 ; 
       rotate-SS_38.1-0 ; 
       rotate-SS_39.1-0 ; 
       rotate-SS_40.1-0 ; 
       rotate-SS_41.1-0 ; 
       rotate-SS_42.1-0 ; 
       rotate-SS_43.1-0 ; 
       rotate-SS_44.1-0 ; 
       rotate-SS_45.1-0 ; 
       rotate-SS_46.1-0 ; 
       rotate-SS_8.1-0 ; 
       rotate-strobe_set1.1-0 ; 
       rotate-turwepemt1.1-0 ; 
       rotate-turwepemt3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/acs/acs08/PICTURES/acs08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       new-rotate.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 1     
       rotate-t2d8.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 14 110 ; 
       1 0 110 ; 
       2 1 110 ; 
       3 0 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 8 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 0 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       14 13 110 ; 
       15 3 110 ; 
       16 3 110 ; 
       17 7 110 ; 
       18 10 110 ; 
       19 10 110 ; 
       20 10 110 ; 
       21 10 110 ; 
       22 10 110 ; 
       23 10 110 ; 
       24 11 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 11 110 ; 
       28 11 110 ; 
       29 11 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 12 110 ; 
       34 12 110 ; 
       35 12 110 ; 
       36 14 110 ; 
       37 6 110 ; 
       38 14 110 ; 
       39 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER MATERIAL_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 10 -24 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 0 -26 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 96.25 -10 0 MPRFLG 0 ; 
       2 SCHEM 95 -12 0 MPRFLG 0 ; 
       3 SCHEM 85 -10 0 MPRFLG 0 ; 
       4 SCHEM 83.75 -12 0 MPRFLG 0 ; 
       5 SCHEM 88.75 -12 0 MPRFLG 0 ; 
       6 SCHEM 35 -14 0 DISPLAY 1 2 MPRFLG 0 ; 
       7 SCHEM 6.25 -16 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       9 SCHEM 40 -10 0 MPRFLG 0 ; 
       10 SCHEM 33.75 -18 0 MPRFLG 0 ; 
       11 SCHEM 18.75 -18 0 MPRFLG 0 ; 
       12 SCHEM 48.75 -18 0 MPRFLG 0 ; 
       14 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       15 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       16 SCHEM 80 -12 0 MPRFLG 0 ; 
       17 SCHEM 5 -18 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 40 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 27.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 30 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 35 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 25 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 12.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 15 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 17.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 20 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 22.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 55 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 42.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 45 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 47.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 50 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 52.5 -20 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 2.5 -8 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 33.75 -16 0 MPRFLG 0 ; 
       38 SCHEM 0 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 10 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 53.75 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 92.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 92.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIAL_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 60 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
