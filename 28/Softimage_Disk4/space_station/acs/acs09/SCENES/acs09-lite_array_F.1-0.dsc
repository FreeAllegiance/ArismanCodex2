SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       lite_array-cam_int1.1-0 ROOT ; 
       lite_array-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       lite_array-light1_1.1-0 ROOT ; 
       lite_array-light2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 18     
       lite_array-mat1.1-0 ; 
       lite_array-mat10.1-0 ; 
       lite_array-mat11.1-0 ; 
       lite_array-mat12.1-0 ; 
       lite_array-mat13.1-0 ; 
       lite_array-mat14.1-0 ; 
       lite_array-mat15.1-0 ; 
       lite_array-mat16.1-0 ; 
       lite_array-mat17.1-0 ; 
       lite_array-mat18.1-0 ; 
       lite_array-mat3.1-0 ; 
       lite_array-mat4.1-0 ; 
       lite_array-mat5.1-0 ; 
       lite_array-mat6.1-0 ; 
       lite_array-mat7.1-0 ; 
       lite_array-mat8.1-0 ; 
       lite_array-mat9.1-0 ; 
       lite_array_F-mat19.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 20     
       acs09-acs09.1-0 ROOT ; 
       acs09-litebody.1-0 ; 
       lite_array-DECAL-litebody.1-0 ; 
       lite_array-DECAL-litebody1.1-0 ; 
       lite_array-DECAL-litebody10.1-0 ; 
       lite_array-DECAL-litebody11.1-0 ; 
       lite_array-DECAL-litebody12.1-0 ; 
       lite_array-DECAL-litebody13.1-0 ; 
       lite_array-DECAL-litebody14.1-0 ; 
       lite_array-DECAL-litebody15.1-0 ; 
       lite_array-DECAL-litebody16.1-0 ; 
       lite_array-DECAL-litebody2.1-0 ; 
       lite_array-DECAL-litebody3.1-0 ; 
       lite_array-DECAL-litebody4.1-0 ; 
       lite_array-DECAL-litebody5.1-0 ; 
       lite_array-DECAL-litebody6.1-0 ; 
       lite_array-DECAL-litebody7.1-0 ; 
       lite_array-DECAL-litebody8.1-0 ; 
       lite_array-DECAL-litebody9.1-0 ; 
       lite_array-STENCIL-Stencil.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs09/PICTURES/acs09 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs09-lite_array_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       lite_array-t2d10.1-0 ; 
       lite_array-t2d11.1-0 ; 
       lite_array-t2d12.1-0 ; 
       lite_array-t2d13.1-0 ; 
       lite_array-t2d14.1-0 ; 
       lite_array-t2d15.1-0 ; 
       lite_array-t2d16.1-0 ; 
       lite_array-t2d17.1-0 ; 
       lite_array-t2d3.1-0 ; 
       lite_array-t2d4.1-0 ; 
       lite_array-t2d5.1-0 ; 
       lite_array-t2d6.1-0 ; 
       lite_array-t2d7.1-0 ; 
       lite_array-t2d8.1-0 ; 
       lite_array-t2d9.1-0 ; 
       lite_array_F-t2d18.1-0 ; 
       lite_array_F-t2d19.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 19 110 ; 
       2 2 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       2 2 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       3 19 110 ; 
       3 3 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       3 3 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       4 19 110 ; 
       4 4 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       4 4 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       5 19 110 ; 
       5 5 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       5 5 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       6 19 110 ; 
       6 6 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       6 6 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       7 19 110 ; 
       7 7 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       7 7 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       8 19 110 ; 
       8 8 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       8 8 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       9 19 110 ; 
       9 9 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       9 9 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       10 19 110 ; 
       10 10 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       10 10 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       11 19 110 ; 
       11 11 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       11 11 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       12 19 110 ; 
       12 12 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       12 12 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       13 19 110 ; 
       13 13 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       13 13 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       14 19 110 ; 
       14 14 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       14 14 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       15 19 110 ; 
       15 15 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       15 15 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       16 19 110 ; 
       16 16 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       16 16 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       17 19 110 ; 
       17 17 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       17 17 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
       18 19 110 ; 
       18 18 103 RELDATA MIN_LIMIT 0 0 -999 MAX_LIMIT 0 0 999 MIN_ACTIVE 1 1 0 MAX_ACTIVE 1 1 0 DAMPING 0 0.5 EndOfRELDATA ; 
       18 18 102 RELDATA MIN_LIMIT -999 -999 0 MAX_LIMIT 999 999 0 MIN_ACTIVE 0 0 1 MAX_ACTIVE 0 0 1 DAMPING 0 0.5 IS_REF_WORLD 1 IS_AREA_BOX 1 LIMIT_RADIUS 0 EndOfRELDATA ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       1 16 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       1 17 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 14 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 16 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       13 10 401 ; 
       14 11 401 ; 
       15 12 401 ; 
       16 13 401 ; 
       17 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 90 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 92.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 66.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       2 SCHEM 25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 27.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 30 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 40 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 35 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 22.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 20 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 17.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 15 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 12.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 22.5 0 0 DISPLAY 0 0 SRT 2.889999 2.889999 2.889999 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 65 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 45 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 47.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 50 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 52.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 62.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 60 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 57.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 70 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 77.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 55 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 72.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
