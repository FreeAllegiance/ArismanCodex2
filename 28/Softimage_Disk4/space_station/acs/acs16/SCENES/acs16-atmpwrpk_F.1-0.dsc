SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       light_beacon-cam_int1.1-0 ROOT ; 
       light_beacon-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       atmpwrpk_F-light1.1-0 ROOT ; 
       atmpwrpk_F-light2.1-0 ROOT ; 
       atmpwrpk_F-light3.1-0 ROOT ; 
       atmpwrpk_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 5     
       atmpwrpk_F-mat1.1-0 ; 
       atmpwrpk_F-mat2.1-0 ; 
       atmpwrpk_F-mat3.1-0 ; 
       atmpwrpk_F-mat4.1-0 ; 
       atmpwrpk_F-mat5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       acs16-acs16.1-0 ROOT ; 
       acs16-pwrpak.13-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs16/PICTURES/acs16 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs16-atmpwrpk_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 5     
       atmpwrpk_F-t2d1.1-0 ; 
       atmpwrpk_F-t2d3.1-0 ; 
       atmpwrpk_F-t2d4.1-0 ; 
       atmpwrpk_F-t2d5.1-0 ; 
       atmpwrpk_F-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 3 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 4 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 210 1 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
