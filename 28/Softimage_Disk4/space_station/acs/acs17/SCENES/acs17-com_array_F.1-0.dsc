SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       com_array-cam_int1.1-0 ROOT ; 
       com_array-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       com_array-inf_light3_1.1-0 ROOT ; 
       com_array-light1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 11     
       com_array-mat1.1-0 ; 
       com_array-mat10.1-0 ; 
       com_array-mat11.1-0 ; 
       com_array-mat2.1-0 ; 
       com_array-mat3.1-0 ; 
       com_array-mat4.1-0 ; 
       com_array-mat5.1-0 ; 
       com_array-mat6.1-0 ; 
       com_array-mat7.1-0 ; 
       com_array-mat8.1-0 ; 
       com_array-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       acs17-acs17.1-0 ROOT ; 
       acs17-antenna1.1-0 ; 
       acs17-antenna2.1-0 ; 
       acs17-base.1-0 ; 
       acs17-basedish.1-0 ; 
       acs17-dish.1-0 ; 
       acs17-platform.4-0 ; 
       acs17-pod1.1-0 ; 
       acs17-pod4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs17/PICTURES/acs17 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs17-com_array_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 11     
       com_array-t2d1.1-0 ; 
       com_array-t2d10.1-0 ; 
       com_array-t2d11.1-0 ; 
       com_array-t2d2.1-0 ; 
       com_array-t2d3.1-0 ; 
       com_array-t2d4.1-0 ; 
       com_array-t2d5.1-0 ; 
       com_array-t2d6.1-0 ; 
       com_array-t2d7.1-0 ; 
       com_array-t2d8.1-0 ; 
       com_array-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 7 110 ; 
       2 8 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 4 110 ; 
       6 0 110 ; 
       7 6 110 ; 
       8 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 8 300 ; 
       2 9 300 ; 
       3 4 300 ; 
       4 6 300 ; 
       5 10 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       6 3 300 ; 
       7 7 300 ; 
       8 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 2 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 9 401 ; 
       2 10 401 ; 
       3 0 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 16.25 0 0 SRT 1 1 1 1.570796 0 0 0 -2.1792 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 25 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -2 0 MPRFLG 0 ; 
       7 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 3.75 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 15 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 15 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
