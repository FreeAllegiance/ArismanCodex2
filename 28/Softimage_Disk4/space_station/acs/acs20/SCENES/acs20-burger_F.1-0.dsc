SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       burger_F-cam_int1.1-0 ROOT ; 
       burger_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       burger_F-inf_light1.1-0 ROOT ; 
       burger_F-inf_light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       burger_F-mat1.1-0 ; 
       burger_F-mat2.1-0 ; 
       burger_F-mat3.1-0 ; 
       burger_F-mat4.1-0 ; 
       burger_F-mat5.1-0 ; 
       burger_F-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       acs20-acs20.1-0 ROOT ; 
       acs20-base.1-0 ; 
       acs20-bun1.1-0 ; 
       acs20-bun2.1-0 ; 
       acs20-lettuce.1-0 ; 
       acs20-lettuce1.1-0 ; 
       acs20-meat.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs20/PICTURES/acs20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs20-burger_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       burger_F-t2d1.1-0 ; 
       burger_F-t2d2.1-0 ; 
       burger_F-t2d3.1-0 ; 
       burger_F-t2d4.1-0 ; 
       burger_F-t2d5.1-0 ; 
       burger_F-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 0 110 ; 
       3 2 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       6 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 4 300 ; 
       2 0 300 ; 
       3 5 300 ; 
       4 3 300 ; 
       5 1 300 ; 
       6 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 6.881379 -2 0 USR MPRFLG 0 ; 
       2 SCHEM 10 -2 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 10 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 15 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
