SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shield_gen_PTL-cam_int1.1-0 ROOT ; 
       shield_gen_PTL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       shield_gen_PTL-spot1.1-0 ; 
       shield_gen_PTL-spot1_int.1-0 ROOT ; 
       shield_gen_PTL-spot2.1-0 ; 
       shield_gen_PTL-spot2_int.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 10     
       shield_gen_PTL-mat1.1-0 ; 
       shield_gen_PTL-mat10.1-0 ; 
       shield_gen_PTL-mat2.1-0 ; 
       shield_gen_PTL-mat3.1-0 ; 
       shield_gen_PTL-mat4.1-0 ; 
       shield_gen_PTL-mat5.1-0 ; 
       shield_gen_PTL-mat6.1-0 ; 
       shield_gen_PTL-mat7.1-0 ; 
       shield_gen_PTL-mat8.1-0 ; 
       shield_gen_PTL-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       shield_gen_PTL-bar1.1-0 ; 
       shield_gen_PTL-bar2.1-0 ; 
       shield_gen_PTL-genbody.2-0 ; 
       shield_gen_PTL-nullbody.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs21/PICTURES/acs21 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs21-shield_gen_PTL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       shield_gen_PTL-t2d1.1-0 ; 
       shield_gen_PTL-t2d10.1-0 ; 
       shield_gen_PTL-t2d2.1-0 ; 
       shield_gen_PTL-t2d3.1-0 ; 
       shield_gen_PTL-t2d4.1-0 ; 
       shield_gen_PTL-t2d5.1-0 ; 
       shield_gen_PTL-t2d6.1-0 ; 
       shield_gen_PTL-t2d7.1-0 ; 
       shield_gen_PTL-t2d8.1-0 ; 
       shield_gen_PTL-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 5 300 ; 
       0 6 300 ; 
       1 7 300 ; 
       1 8 300 ; 
       1 9 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 1 300 ; 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 3 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 3.5 -24.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -24.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 3.5 -22.5 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 0 -22.5 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7 -19.5 0 MPRFLG 0 ; 
       1 SCHEM 7 -14.5 0 MPRFLG 0 ; 
       2 SCHEM 3.5 -12.5 0 MPRFLG 0 ; 
       3 SCHEM 0 -11.5 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 3.5 -2.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 7 -10.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7 -4.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7 -6.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7 -8.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10.5 -20.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 10.5 -18.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 10.5 -16.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 10.5 -14.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10.5 -12.5 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10.5 -10.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 10.5 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 10.5 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -20.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -18.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -16.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -14.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -12.5 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
