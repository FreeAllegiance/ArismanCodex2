SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       obsv_dom3-cam_int1.1-0 ROOT ; 
       obsv_dom3-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       obsv_dom_F-light1.1-0 ROOT ; 
       obsv_dom_F-light2.1-0 ROOT ; 
       obsv_dom_F-light3.1-0 ROOT ; 
       obsv_dom3-inf_light3.1-0 ROOT ; 
       obsv_dom3-inf_light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 6     
       obsv_dom_F-mat1.1-0 ; 
       obsv_dom_F-mat2.1-0 ; 
       obsv_dom_F-mat3.1-0 ; 
       obsv_dom_F-mat4.1-0 ; 
       obsv_dom_F-mat5.1-0 ; 
       obsv_dom_F-mat6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 1     
       acs23-acs23.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/acs/acs23/PICTURES/acs23 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs23-obsv_dom_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       obsv_dom_F-t2d1.1-0 ; 
       obsv_dom_F-t2d2.1-0 ; 
       obsv_dom_F-t2d3.1-0 ; 
       obsv_dom_F-t2d4.1-0 ; 
       obsv_dom_F-t2d5.1-0 ; 
       obsv_dom_F-t2d6.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 4 401 ; 
       0 5 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       5 3 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 0 0 SRT 1 1 1 0 0 1.570796 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.25 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
