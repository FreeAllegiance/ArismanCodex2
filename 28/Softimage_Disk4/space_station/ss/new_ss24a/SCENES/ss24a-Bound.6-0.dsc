SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.110-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       Bound-mat1_3.1-0 ; 
       Bound-mat40_3.1-0 ; 
       Bound-mat41_3.1-0 ; 
       Bound-mat42_3.1-0 ; 
       Bound-mat43_3.1-0 ; 
       Bound-mat44_3.1-0 ; 
       Bound-mat45_3.1-0 ; 
       Bound-mat46_3.1-0 ; 
       Bound-mat47_3.1-0 ; 
       Bound-mat48_3.1-0 ; 
       Bound-mat49_3.1-0 ; 
       Bound-mat51_3.1-0 ; 
       Bound-mat52_3.1-0 ; 
       Bound-mat53_3.1-0 ; 
       Bound-mat54_3.1-0 ; 
       Bound-mat55_3.1-0 ; 
       Bound-mat56_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       bounding_model1-bound01.1-0 ; 
       bounding_model1-bound02.1-0 ; 
       bounding_model1-bound03.1-0 ; 
       bounding_model1-bound04.1-0 ; 
       bounding_model1-bound05.1-0 ; 
       bounding_model1-bound06.1-0 ; 
       bounding_model1-bounding_model.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-Bound.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 16     
       Bound-t2d38_3.2-0 ; 
       Bound-t2d39_3.2-0 ; 
       Bound-t2d40_3.2-0 ; 
       Bound-t2d41_3.2-0 ; 
       Bound-t2d42_3.2-0 ; 
       Bound-t2d43_3.2-0 ; 
       Bound-t2d44_3.2-0 ; 
       Bound-t2d45_3.2-0 ; 
       Bound-t2d46_3.2-0 ; 
       Bound-t2d47_3.2-0 ; 
       Bound-t2d49_3.2-0 ; 
       Bound-t2d50_3.2-0 ; 
       Bound-t2d51_3.2-0 ; 
       Bound-t2d52_3.2-0 ; 
       Bound-t2d53_3.2-0 ; 
       Bound-t2d54_3.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 0 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 4 401 ; 
       2 6 401 ; 
       3 8 401 ; 
       4 0 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 5 401 ; 
       9 7 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 8.75 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 216.9667 -1.246709 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
