SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.109-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       new-mat2_1.2-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       re_do-mat204.2-0 ; 
       re_do-mat205.2-0 ; 
       re_do-mat206.2-0 ; 
       re_do-mat207.2-0 ; 
       re_do-mat208.2-0 ; 
       re_do-mat209.2-0 ; 
       re_do-mat210.2-0 ; 
       re_do-mat268_1.3-0 ; 
       re_do-mat269.3-0 ; 
       re_do-mat274.2-0 ; 
       re_do-mat275.2-0 ; 
       re_do-mat276.2-0 ; 
       re_do-mat277.2-0 ; 
       re_do-mat278.2-0 ; 
       re_do-mat279.2-0 ; 
       re_do-mat280.2-0 ; 
       re_do-mat281.2-0 ; 
       re_do-mat282.2-0 ; 
       re_do-mat283.3-0 ; 
       re_do-mat284.2-0 ; 
       re_do-mat285.2-0 ; 
       re_do-mat320.2-0 ; 
       re_do-mat321.2-0 ; 
       re_do-mat322.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.1-0 ; 
       ss24a-cyl3_1.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.64-0 ROOT ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-STATIC.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 39     
       new-t2d2_1.17-0 ; 
       plaza8_F-t2d38_1.5-0 ; 
       plaza8_F-t2d39_1.5-0 ; 
       plaza8_F-t2d40_1.5-0 ; 
       plaza8_F-t2d41_1.5-0 ; 
       plaza8_F-t2d42_1.5-0 ; 
       plaza8_F-t2d43_1.5-0 ; 
       plaza8_F-t2d44_1.5-0 ; 
       plaza8_F-t2d45_1.5-0 ; 
       plaza8_F-t2d46_1.5-0 ; 
       plaza8_F-t2d47_1.5-0 ; 
       plaza8_F-t2d49_1.5-0 ; 
       plaza8_F-t2d50_1.5-0 ; 
       plaza8_F-t2d51_1.5-0 ; 
       plaza8_F-t2d52_1.5-0 ; 
       plaza8_F-t2d53_1.5-0 ; 
       plaza8_F-t2d54_1.5-0 ; 
       re_do-t2d196.1-0 ; 
       re_do-t2d197.1-0 ; 
       re_do-t2d198.1-0 ; 
       re_do-t2d199.1-0 ; 
       re_do-t2d200.1-0 ; 
       re_do-t2d201.1-0 ; 
       re_do-t2d261.2-0 ; 
       re_do-t2d268.1-0 ; 
       re_do-t2d269.1-0 ; 
       re_do-t2d270.1-0 ; 
       re_do-t2d271.1-0 ; 
       re_do-t2d272.1-0 ; 
       re_do-t2d273.1-0 ; 
       re_do-t2d274.1-0 ; 
       re_do-t2d275.1-0 ; 
       re_do-t2d276.1-0 ; 
       re_do-t2d277.3-0 ; 
       re_do-t2d278.1-0 ; 
       re_do-t2d279.1-0 ; 
       re_do-t2d280.1-0 ; 
       re_do-t2d281.1-0 ; 
       re_do-t2d282.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 10 110 ; 
       2 10 110 ; 
       3 11 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 0 110 ; 
       7 10 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 26 300 ; 
       1 39 300 ; 
       1 40 300 ; 
       2 37 300 ; 
       2 38 300 ; 
       2 41 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       5 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       6 18 300 ; 
       6 19 300 ; 
       6 20 300 ; 
       6 21 300 ; 
       6 22 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       7 0 300 ; 
       7 25 300 ; 
       7 36 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 12 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 23 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       4 9 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 6 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       19 17 401 ; 
       20 18 401 ; 
       21 19 401 ; 
       22 20 401 ; 
       23 21 401 ; 
       24 22 401 ; 
       27 24 401 ; 
       28 25 401 ; 
       29 26 401 ; 
       30 27 401 ; 
       31 28 401 ; 
       32 29 401 ; 
       33 30 401 ; 
       34 31 401 ; 
       35 32 401 ; 
       36 33 401 ; 
       37 34 401 ; 
       38 35 401 ; 
       39 36 401 ; 
       40 37 401 ; 
       41 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       8 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 10 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
