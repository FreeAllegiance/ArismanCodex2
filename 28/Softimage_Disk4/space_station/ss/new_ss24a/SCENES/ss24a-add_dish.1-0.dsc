SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.33-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       add_dish-light1_1.1-0 ROOT ; 
       add_dish-light2_1.1-0 ROOT ; 
       add_dish-light3_1.1-0 ROOT ; 
       add_dish-light4_1.1-0 ROOT ; 
       add_dish-light5_1.1-0 ROOT ; 
       add_dish-light6_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 79     
       add_dish-mat204.1-0 ; 
       add_dish-mat205.1-0 ; 
       add_dish-mat206.1-0 ; 
       add_dish-mat207.1-0 ; 
       add_dish-mat208.1-0 ; 
       add_dish-mat209.1-0 ; 
       add_dish-mat210.1-0 ; 
       add_dish-mat268_1.1-0 ; 
       add_dish-mat269.1-0 ; 
       add_dish-mat274.1-0 ; 
       add_dish-mat275.1-0 ; 
       add_dish-mat276.1-0 ; 
       add_dish-mat277.1-0 ; 
       add_dish-mat278.1-0 ; 
       add_dish-mat279.1-0 ; 
       add_dish-mat280.1-0 ; 
       add_dish-mat281.1-0 ; 
       add_dish-mat282.1-0 ; 
       add_dish-mat283.1-0 ; 
       add_dish-mat284.1-0 ; 
       add_dish-mat285.1-0 ; 
       edit_strobe_materials-mat287.3-0 ; 
       edit_strobe_materials-mat288.3-0 ; 
       edit_strobe_materials-mat289.3-0 ; 
       edit_strobe_materials-mat290.3-0 ; 
       edit_strobe_materials-mat291.3-0 ; 
       edit_strobe_materials-mat292.3-0 ; 
       edit_strobe_materials-mat293.3-0 ; 
       edit_strobe_materials-mat294.3-0 ; 
       edit_strobe_materials-mat295.3-0 ; 
       edit_strobe_materials-mat296.3-0 ; 
       edit_strobe_materials-mat297.3-0 ; 
       edit_strobe_materials-mat298.3-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       edit_strobe_materials-mat315.3-0 ; 
       edit_strobe_materials-mat316.3-0 ; 
       edit_strobe_materials-mat317.3-0 ; 
       edit_strobe_materials-mat318.3-0 ; 
       edit_strobe_materials-mat319.3-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       rotate-mat270.1-0 ; 
       rotate-mat271.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-beam_lights.1-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-Garage1A.1-0 ; 
       ss24a-Garage1B.1-0 ; 
       ss24a-Garage1C.1-0 ; 
       ss24a-Garage1D.1-0 ; 
       ss24a-Garage1E.1-0 ; 
       ss24a-Garage2A.1-0 ; 
       ss24a-Garage2B.1-0 ; 
       ss24a-Garage2C.1-0 ; 
       ss24a-Garage2D.1-0 ; 
       ss24a-Garage2E.1-0 ; 
       ss24a-Launch1.1-0 ; 
       ss24a-plaza_lights.1-0 ; 
       ss24a-plaza_lights1.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-SS_1.1-0 ; 
       ss24a-SS_13.1-0 ; 
       ss24a-SS_14.1-0 ; 
       ss24a-SS_15.1-0 ; 
       ss24a-SS_16.1-0 ; 
       ss24a-SS_17.1-0 ; 
       ss24a-SS_18.1-0 ; 
       ss24a-SS_19.1-0 ; 
       ss24a-SS_2.1-0 ; 
       ss24a-SS_20.1-0 ; 
       ss24a-SS_21.1-0 ; 
       ss24a-SS_22.1-0 ; 
       ss24a-SS_23.1-0 ; 
       ss24a-SS_24.1-0 ; 
       ss24a-SS_25.1-0 ; 
       ss24a-SS_26.1-0 ; 
       ss24a-SS_27.1-0 ; 
       ss24a-SS_28.1-0 ; 
       ss24a-SS_29.1-0 ; 
       ss24a-SS_30.1-0 ; 
       ss24a-SS_31.1-0 ; 
       ss24a-SS_32.1-0 ; 
       ss24a-SS_33.1-0 ; 
       ss24a-SS_34.1-0 ; 
       ss24a-SS_35.1-0 ; 
       ss24a-SS_36.1-0 ; 
       ss24a-SS_37.1-0 ; 
       ss24a-SS_38.1-0 ; 
       ss24a-SS_39.1-0 ; 
       ss24a-SS_40.1-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.21-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-add_dish.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       add_dish-t2d196.1-0 ; 
       add_dish-t2d197.1-0 ; 
       add_dish-t2d198.1-0 ; 
       add_dish-t2d199.1-0 ; 
       add_dish-t2d200.1-0 ; 
       add_dish-t2d201.1-0 ; 
       add_dish-t2d260_1.1-0 ; 
       add_dish-t2d261.1-0 ; 
       add_dish-t2d268.1-0 ; 
       add_dish-t2d269.1-0 ; 
       add_dish-t2d270.1-0 ; 
       add_dish-t2d271.1-0 ; 
       add_dish-t2d272.1-0 ; 
       add_dish-t2d273.1-0 ; 
       add_dish-t2d274.1-0 ; 
       add_dish-t2d275.1-0 ; 
       add_dish-t2d276.1-0 ; 
       add_dish-t2d277.1-0 ; 
       add_dish-t2d278.1-0 ; 
       add_dish-t2d279.1-0 ; 
       new-t2d2_1.9-0 ; 
       plaza8_F-t2d38_1.4-0 ; 
       plaza8_F-t2d39_1.4-0 ; 
       plaza8_F-t2d40_1.4-0 ; 
       plaza8_F-t2d41_1.4-0 ; 
       plaza8_F-t2d42_1.4-0 ; 
       plaza8_F-t2d43_1.4-0 ; 
       plaza8_F-t2d44_1.4-0 ; 
       plaza8_F-t2d45_1.4-0 ; 
       plaza8_F-t2d46_1.4-0 ; 
       plaza8_F-t2d47_1.4-0 ; 
       plaza8_F-t2d48_1.4-0 ; 
       plaza8_F-t2d49_1.4-0 ; 
       plaza8_F-t2d50_1.4-0 ; 
       plaza8_F-t2d51_1.4-0 ; 
       plaza8_F-t2d52_1.4-0 ; 
       plaza8_F-t2d53_1.4-0 ; 
       plaza8_F-t2d54_1.4-0 ; 
       plaza8_F-t2d55_1.4-0 ; 
       plaza8_F-t2d56_1.4-0 ; 
       plaza8_F-t2d57_1.4-0 ; 
       plaza8_F-t2d58_1.4-0 ; 
       rotate-t2d262.3-0 ; 
       rotate-t2d263.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 57 110 ; 
       1 57 110 ; 
       2 57 110 ; 
       3 24 110 ; 
       4 24 110 ; 
       5 58 110 ; 
       6 24 110 ; 
       22 24 110 ; 
       23 22 110 ; 
       7 24 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 57 110 ; 
       20 57 110 ; 
       21 57 110 ; 
       24 57 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       41 21 110 ; 
       42 21 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 2 110 ; 
       50 2 110 ; 
       51 2 110 ; 
       52 2 110 ; 
       53 2 110 ; 
       54 2 110 ; 
       55 24 110 ; 
       56 55 110 ; 
       58 59 110 ; 
       59 55 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 47 300 ; 
       3 8 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       22 78 300 ; 
       23 77 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       20 21 300 ; 
       21 34 300 ; 
       24 54 300 ; 
       24 7 300 ; 
       24 18 300 ; 
       25 33 300 ; 
       26 22 300 ; 
       27 23 300 ; 
       28 24 300 ; 
       29 25 300 ; 
       30 26 300 ; 
       31 27 300 ; 
       32 28 300 ; 
       33 32 300 ; 
       34 29 300 ; 
       35 30 300 ; 
       36 31 300 ; 
       37 35 300 ; 
       38 36 300 ; 
       39 37 300 ; 
       40 38 300 ; 
       41 39 300 ; 
       42 40 300 ; 
       43 41 300 ; 
       44 42 300 ; 
       45 43 300 ; 
       46 44 300 ; 
       47 45 300 ; 
       48 46 300 ; 
       49 53 300 ; 
       50 52 300 ; 
       51 51 300 ; 
       52 50 300 ; 
       53 49 300 ; 
       54 48 300 ; 
       58 55 300 ; 
       58 66 300 ; 
       58 73 300 ; 
       58 74 300 ; 
       58 75 300 ; 
       58 76 300 ; 
       59 55 300 ; 
       59 56 300 ; 
       59 57 300 ; 
       59 58 300 ; 
       59 59 300 ; 
       59 60 300 ; 
       59 61 300 ; 
       59 62 300 ; 
       59 63 300 ; 
       59 64 300 ; 
       59 65 300 ; 
       59 67 300 ; 
       59 68 300 ; 
       59 69 300 ; 
       59 70 300 ; 
       59 71 300 ; 
       59 72 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       54 20 401 ; 
       56 25 401 ; 
       57 27 401 ; 
       58 29 401 ; 
       59 21 401 ; 
       60 22 401 ; 
       61 23 401 ; 
       62 24 401 ; 
       63 26 401 ; 
       64 28 401 ; 
       65 30 401 ; 
       66 31 401 ; 
       67 32 401 ; 
       68 33 401 ; 
       69 34 401 ; 
       70 35 401 ; 
       71 36 401 ; 
       72 37 401 ; 
       73 38 401 ; 
       74 39 401 ; 
       75 40 401 ; 
       76 41 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       77 42 401 ; 
       78 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 130 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 132.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 135 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 MPRFLG 0 ; 
       2 SCHEM 113.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 40 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 61.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 91.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 MPRFLG 0 ; 
       25 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 65 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 55 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 60 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 95 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 85 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 87.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 90 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 82.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 100 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 102.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 105 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 80 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 107.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 110 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 112.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 115 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 117.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 120 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       56 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       57 SCHEM 61.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       58 SCHEM 5 -8 0 MPRFLG 0 ; 
       59 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 96.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 99 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 104 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 116.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 114 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 109 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 95.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
