SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.79-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       STATIC-light1_1_2.1-0 ROOT ; 
       STATIC-light2_1_2.1-0 ROOT ; 
       STATIC-light3_1_2.1-0 ROOT ; 
       STATIC-light4_1_2.1-0 ROOT ; 
       STATIC-light5_1_2.1-0 ROOT ; 
       STATIC-light6_1_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       bounding_model1-bound10.1-0 ; 
       bounding_model1-bound11.1-0 ; 
       bounding_model1-bound9.1-0 ; 
       bounding_model1-root.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-bound.18-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 3 110 ; 
       2 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 5 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
