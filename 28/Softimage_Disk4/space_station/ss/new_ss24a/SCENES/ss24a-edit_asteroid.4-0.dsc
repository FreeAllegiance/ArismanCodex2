SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.16-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1.16-0 ROOT ; 
       rotate-light2.16-0 ROOT ; 
       rotate-light3.16-0 ROOT ; 
       rotate-light4.16-0 ROOT ; 
       rotate-light5.16-0 ROOT ; 
       rotate-light6.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       edit_asteroid-mat274.1-0 ; 
       edit_asteroid-mat275.1-0 ; 
       edit_asteroid-mat276.1-0 ; 
       edit_asteroid-mat277.1-0 ; 
       edit_asteroid-mat278.1-0 ; 
       edit_asteroid-mat279.1-0 ; 
       edit_asteroid-mat280.1-0 ; 
       edit_asteroid-mat281.1-0 ; 
       edit_asteroid-mat282.1-0 ; 
       edit_asteroid-mat283.1-0 ; 
       edit_asteroid-mat284.2-0 ; 
       edit_asteroid-mat285.1-0 ; 
       new-mat2.2-0 ; 
       plaza8_F-mat1_1.1-0 ; 
       plaza8_F-mat40_1.1-0 ; 
       plaza8_F-mat41_1.1-0 ; 
       plaza8_F-mat42_1.1-0 ; 
       plaza8_F-mat43_1.1-0 ; 
       plaza8_F-mat44_1.1-0 ; 
       plaza8_F-mat45_1.1-0 ; 
       plaza8_F-mat46_1.1-0 ; 
       plaza8_F-mat47_1.1-0 ; 
       plaza8_F-mat48_1.1-0 ; 
       plaza8_F-mat49_1.1-0 ; 
       plaza8_F-mat50_1.1-0 ; 
       plaza8_F-mat51_1.1-0 ; 
       plaza8_F-mat52_1.1-0 ; 
       plaza8_F-mat53_1.1-0 ; 
       plaza8_F-mat54_1.1-0 ; 
       plaza8_F-mat55_1.1-0 ; 
       plaza8_F-mat56_1.1-0 ; 
       plaza8_F-mat57_1.1-0 ; 
       plaza8_F-mat58_1.1-0 ; 
       plaza8_F-mat59_1.1-0 ; 
       plaza8_F-mat60_1.1-0 ; 
       rotate-mat204.1-0 ; 
       rotate-mat205.1-0 ; 
       rotate-mat206.1-0 ; 
       rotate-mat207.1-0 ; 
       rotate-mat208.1-0 ; 
       rotate-mat209.1-0 ; 
       rotate-mat210.1-0 ; 
       rotate-mat268.2-0 ; 
       rotate-mat269.1-0 ; 
       rotate-mat270_1.1-0 ; 
       rotate-mat271_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       edit_asteroid-cyl3.3-0 ROOT ; 
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-cube1.3-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl1.3-0 ; 
       ss24a-fuselg.5-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-fuselg5.1-0 ; 
       ss24a-fuselg6.1-0 ; 
       ss24a-Garage1A.1-0 ; 
       ss24a-Garage1A_1.1-0 ; 
       ss24a-Garage1B.1-0 ; 
       ss24a-Garage1B_1.1-0 ; 
       ss24a-Garage1C.1-0 ; 
       ss24a-Garage1C_1.1-0 ; 
       ss24a-Garage1D.1-0 ; 
       ss24a-Garage1D_1.1-0 ; 
       ss24a-Garage1E.1-0 ; 
       ss24a-Garage1E_1.1-0 ; 
       ss24a-Garage2A.1-0 ; 
       ss24a-Garage2B.1-0 ; 
       ss24a-Garage2C.1-0 ; 
       ss24a-Garage2D.1-0 ; 
       ss24a-Garage2E.1-0 ; 
       ss24a-Launch1.1-0 ; 
       ss24a-Launch1_1.1-0 ; 
       ss24a-Launch2.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_base_1.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-radar_Dish_1.1-0 ; 
       ss24a-skin2.22-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18.24-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a.1-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.10-0 ROOT ; 
       ss24a-ss24a_1_1.3-0 ROOT ; 
       ss24a-tfuselg1.1-0 ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-edit_asteroid.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       edit_asteroid-t2d268.1-0 ; 
       edit_asteroid-t2d269.1-0 ; 
       edit_asteroid-t2d270.1-0 ; 
       edit_asteroid-t2d271.1-0 ; 
       edit_asteroid-t2d272.1-0 ; 
       edit_asteroid-t2d273.1-0 ; 
       edit_asteroid-t2d274.1-0 ; 
       edit_asteroid-t2d275.1-0 ; 
       edit_asteroid-t2d276.1-0 ; 
       edit_asteroid-t2d277.2-0 ; 
       edit_asteroid-t2d278.2-0 ; 
       edit_asteroid-t2d279.1-0 ; 
       new-t2d2_1.5-0 ; 
       plaza8_F-t2d38.2-0 ; 
       plaza8_F-t2d39.2-0 ; 
       plaza8_F-t2d40.2-0 ; 
       plaza8_F-t2d41.2-0 ; 
       plaza8_F-t2d42.2-0 ; 
       plaza8_F-t2d43.2-0 ; 
       plaza8_F-t2d44.2-0 ; 
       plaza8_F-t2d45.2-0 ; 
       plaza8_F-t2d46.2-0 ; 
       plaza8_F-t2d47.2-0 ; 
       plaza8_F-t2d48.2-0 ; 
       plaza8_F-t2d49.2-0 ; 
       plaza8_F-t2d50.2-0 ; 
       plaza8_F-t2d51.2-0 ; 
       plaza8_F-t2d52.2-0 ; 
       plaza8_F-t2d53.2-0 ; 
       plaza8_F-t2d54.2-0 ; 
       plaza8_F-t2d55.2-0 ; 
       plaza8_F-t2d56.2-0 ; 
       plaza8_F-t2d57.2-0 ; 
       plaza8_F-t2d58.2-0 ; 
       rotate-t2d196.1-0 ; 
       rotate-t2d197.1-0 ; 
       rotate-t2d198.1-0 ; 
       rotate-t2d199.1-0 ; 
       rotate-t2d200.1-0 ; 
       rotate-t2d201.1-0 ; 
       rotate-t2d260.5-0 ; 
       rotate-t2d261.1-0 ; 
       rotate-t2d262.1-0 ; 
       rotate-t2d263.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 43 110 ; 
       2 43 110 ; 
       3 36 110 ; 
       4 37 110 ; 
       5 36 110 ; 
       6 44 110 ; 
       7 45 110 ; 
       8 37 110 ; 
       9 37 110 ; 
       10 3 110 ; 
       11 4 110 ; 
       12 36 110 ; 
       13 36 110 ; 
       14 1 110 ; 
       15 37 110 ; 
       16 1 110 ; 
       17 37 110 ; 
       18 1 110 ; 
       19 37 110 ; 
       20 1 110 ; 
       21 37 110 ; 
       22 1 110 ; 
       23 37 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 43 110 ; 
       30 37 110 ; 
       31 37 110 ; 
       32 43 110 ; 
       33 37 110 ; 
       34 32 110 ; 
       35 33 110 ; 
       36 43 110 ; 
       37 42 110 ; 
       38 36 110 ; 
       39 37 110 ; 
       40 38 110 ; 
       41 39 110 ; 
       44 46 110 ; 
       45 47 110 ; 
       46 38 110 ; 
       47 39 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       4 43 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       9 6 300 ; 
       9 7 300 ; 
       9 8 300 ; 
       11 35 300 ; 
       11 36 300 ; 
       11 37 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       11 40 300 ; 
       11 41 300 ; 
       33 45 300 ; 
       35 44 300 ; 
       37 12 300 ; 
       37 42 300 ; 
       37 9 300 ; 
       45 13 300 ; 
       45 24 300 ; 
       45 31 300 ; 
       45 32 300 ; 
       45 33 300 ; 
       45 34 300 ; 
       47 13 300 ; 
       47 14 300 ; 
       47 15 300 ; 
       47 16 300 ; 
       47 17 300 ; 
       47 18 300 ; 
       47 19 300 ; 
       47 20 300 ; 
       47 21 300 ; 
       47 22 300 ; 
       47 23 300 ; 
       47 25 300 ; 
       47 26 300 ; 
       47 27 300 ; 
       47 28 300 ; 
       47 29 300 ; 
       47 30 300 ; 
       0 10 300 ; 
       0 11 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       4 41 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       12 12 401 ; 
       14 17 401 ; 
       15 19 401 ; 
       16 21 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       21 18 401 ; 
       22 20 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       32 31 401 ; 
       33 32 401 ; 
       34 33 401 ; 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 4 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 180 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 182.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 185 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 190 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 192.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 27.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 40 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       8 SCHEM 160 -4 0 MPRFLG 0 ; 
       9 SCHEM 167.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 7.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 120 -6 0 MPRFLG 0 ; 
       12 SCHEM 12.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 15 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 140 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 25 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 142.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 27.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 145 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 30 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 147.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 150 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 35 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 37.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 40 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 42.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 45 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 20 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 152.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 155 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 17.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 136.25 -4 0 MPRFLG 0 ; 
       34 SCHEM 17.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 135 -6 0 MPRFLG 0 ; 
       36 SCHEM 110.3355 -1.947605 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 112.5 -2 0 MPRFLG 0 ; 
       38 SCHEM 3.75 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       40 SCHEM 2.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       42 SCHEM 112.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       43 SCHEM 23.75 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       44 SCHEM 5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 60 -8 0 MPRFLG 0 ; 
       46 SCHEM 5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 80 -6 0 MPRFLG 0 ; 
       0 SCHEM 195 0 0 SRT 4.021627 4.021627 4.021627 0 0 1.570796 16.38504 -6.980423 2.534367 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       12 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 68.87247 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 177.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 197.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 202.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       12 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 170 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 167.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 177.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 200 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 205 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
