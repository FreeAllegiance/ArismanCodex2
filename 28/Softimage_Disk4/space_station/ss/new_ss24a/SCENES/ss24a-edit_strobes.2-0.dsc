SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.31-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       edit_strobes-light1_1.2-0 ROOT ; 
       edit_strobes-light2_1.2-0 ROOT ; 
       edit_strobes-light3_1.2-0 ROOT ; 
       edit_strobes-light4_1.2-0 ROOT ; 
       edit_strobes-light5_1.2-0 ROOT ; 
       edit_strobes-light6_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 77     
       edit_strobes-mat204.1-0 ; 
       edit_strobes-mat205.1-0 ; 
       edit_strobes-mat206.1-0 ; 
       edit_strobes-mat207.1-0 ; 
       edit_strobes-mat208.1-0 ; 
       edit_strobes-mat209.1-0 ; 
       edit_strobes-mat210.1-0 ; 
       edit_strobes-mat268_1.1-0 ; 
       edit_strobes-mat269.1-0 ; 
       edit_strobes-mat274.1-0 ; 
       edit_strobes-mat275.1-0 ; 
       edit_strobes-mat276.1-0 ; 
       edit_strobes-mat277.1-0 ; 
       edit_strobes-mat278.1-0 ; 
       edit_strobes-mat279.1-0 ; 
       edit_strobes-mat280.1-0 ; 
       edit_strobes-mat281.1-0 ; 
       edit_strobes-mat282.1-0 ; 
       edit_strobes-mat283.1-0 ; 
       edit_strobes-mat284.1-0 ; 
       edit_strobes-mat285.1-0 ; 
       edit_strobe_materials-mat287.2-0 ; 
       edit_strobe_materials-mat288.2-0 ; 
       edit_strobe_materials-mat289.2-0 ; 
       edit_strobe_materials-mat290.2-0 ; 
       edit_strobe_materials-mat291.2-0 ; 
       edit_strobe_materials-mat292.2-0 ; 
       edit_strobe_materials-mat293.2-0 ; 
       edit_strobe_materials-mat294.2-0 ; 
       edit_strobe_materials-mat295.2-0 ; 
       edit_strobe_materials-mat296.2-0 ; 
       edit_strobe_materials-mat297.2-0 ; 
       edit_strobe_materials-mat298.2-0 ; 
       edit_strobe_materials-mat299.2-0 ; 
       edit_strobe_materials-mat300.1-0 ; 
       edit_strobe_materials-mat301.1-0 ; 
       edit_strobe_materials-mat302.1-0 ; 
       edit_strobe_materials-mat303.1-0 ; 
       edit_strobe_materials-mat304.1-0 ; 
       edit_strobe_materials-mat305.1-0 ; 
       edit_strobe_materials-mat306.1-0 ; 
       edit_strobe_materials-mat307.1-0 ; 
       edit_strobe_materials-mat308.1-0 ; 
       edit_strobe_materials-mat309.1-0 ; 
       edit_strobe_materials-mat310.1-0 ; 
       edit_strobe_materials-mat311.1-0 ; 
       edit_strobe_materials-mat312.1-0 ; 
       edit_strobe_materials-mat313.2-0 ; 
       edit_strobe_materials-mat314.2-0 ; 
       edit_strobe_materials-mat315.2-0 ; 
       edit_strobe_materials-mat316.2-0 ; 
       edit_strobe_materials-mat317.2-0 ; 
       edit_strobe_materials-mat318.2-0 ; 
       edit_strobe_materials-mat319.2-0 ; 
       new-mat2.5-0 ; 
       plaza8_F-mat1_1.4-0 ; 
       plaza8_F-mat40_1.4-0 ; 
       plaza8_F-mat41_1.4-0 ; 
       plaza8_F-mat42_1.4-0 ; 
       plaza8_F-mat43_1.4-0 ; 
       plaza8_F-mat44_1.4-0 ; 
       plaza8_F-mat45_1.4-0 ; 
       plaza8_F-mat46_1.4-0 ; 
       plaza8_F-mat47_1.4-0 ; 
       plaza8_F-mat48_1.4-0 ; 
       plaza8_F-mat49_1.4-0 ; 
       plaza8_F-mat50_1.4-0 ; 
       plaza8_F-mat51_1.4-0 ; 
       plaza8_F-mat52_1.4-0 ; 
       plaza8_F-mat53_1.4-0 ; 
       plaza8_F-mat54_1.4-0 ; 
       plaza8_F-mat55_1.4-0 ; 
       plaza8_F-mat56_1.4-0 ; 
       plaza8_F-mat57_1.4-0 ; 
       plaza8_F-mat58_1.4-0 ; 
       plaza8_F-mat59_1.4-0 ; 
       plaza8_F-mat60_1.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-beam_lights.1-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-Garage1A.1-0 ; 
       ss24a-Garage1B.1-0 ; 
       ss24a-Garage1C.1-0 ; 
       ss24a-Garage1D.1-0 ; 
       ss24a-Garage1E.1-0 ; 
       ss24a-Garage2A.1-0 ; 
       ss24a-Garage2B.1-0 ; 
       ss24a-Garage2C.1-0 ; 
       ss24a-Garage2D.1-0 ; 
       ss24a-Garage2E.1-0 ; 
       ss24a-Launch1.1-0 ; 
       ss24a-plaza_lights.1-0 ; 
       ss24a-plaza_lights1.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-SS_1.1-0 ; 
       ss24a-SS_13.1-0 ; 
       ss24a-SS_14.1-0 ; 
       ss24a-SS_15.1-0 ; 
       ss24a-SS_16.1-0 ; 
       ss24a-SS_17.1-0 ; 
       ss24a-SS_18.1-0 ; 
       ss24a-SS_19.1-0 ; 
       ss24a-SS_2.1-0 ; 
       ss24a-SS_20.1-0 ; 
       ss24a-SS_21.1-0 ; 
       ss24a-SS_22.1-0 ; 
       ss24a-SS_23.1-0 ; 
       ss24a-SS_24.1-0 ; 
       ss24a-SS_25.1-0 ; 
       ss24a-SS_26.1-0 ; 
       ss24a-SS_27.1-0 ; 
       ss24a-SS_28.1-0 ; 
       ss24a-SS_29.1-0 ; 
       ss24a-SS_30.1-0 ; 
       ss24a-SS_31.1-0 ; 
       ss24a-SS_32.1-0 ; 
       ss24a-SS_33.1-0 ; 
       ss24a-SS_34.1-0 ; 
       ss24a-SS_35.1-0 ; 
       ss24a-SS_36.1-0 ; 
       ss24a-SS_37.1-0 ; 
       ss24a-SS_38.1-0 ; 
       ss24a-SS_39.1-0 ; 
       ss24a-SS_40.1-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.19-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-edit_strobes.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 42     
       edit_strobes-t2d196.1-0 ; 
       edit_strobes-t2d197.1-0 ; 
       edit_strobes-t2d198.1-0 ; 
       edit_strobes-t2d199.1-0 ; 
       edit_strobes-t2d200.1-0 ; 
       edit_strobes-t2d201.1-0 ; 
       edit_strobes-t2d260_1.1-0 ; 
       edit_strobes-t2d261.1-0 ; 
       edit_strobes-t2d268.1-0 ; 
       edit_strobes-t2d269.1-0 ; 
       edit_strobes-t2d270.1-0 ; 
       edit_strobes-t2d271.1-0 ; 
       edit_strobes-t2d272.1-0 ; 
       edit_strobes-t2d273.1-0 ; 
       edit_strobes-t2d274.1-0 ; 
       edit_strobes-t2d275.1-0 ; 
       edit_strobes-t2d276.1-0 ; 
       edit_strobes-t2d277.1-0 ; 
       edit_strobes-t2d278.1-0 ; 
       edit_strobes-t2d279.1-0 ; 
       new-t2d2_1.8-0 ; 
       plaza8_F-t2d38_1.3-0 ; 
       plaza8_F-t2d39_1.3-0 ; 
       plaza8_F-t2d40_1.3-0 ; 
       plaza8_F-t2d41_1.3-0 ; 
       plaza8_F-t2d42_1.3-0 ; 
       plaza8_F-t2d43_1.3-0 ; 
       plaza8_F-t2d44_1.3-0 ; 
       plaza8_F-t2d45_1.3-0 ; 
       plaza8_F-t2d46_1.3-0 ; 
       plaza8_F-t2d47_1.3-0 ; 
       plaza8_F-t2d48_1.3-0 ; 
       plaza8_F-t2d49_1.3-0 ; 
       plaza8_F-t2d50_1.3-0 ; 
       plaza8_F-t2d51_1.3-0 ; 
       plaza8_F-t2d52_1.3-0 ; 
       plaza8_F-t2d53_1.3-0 ; 
       plaza8_F-t2d54_1.3-0 ; 
       plaza8_F-t2d55_1.3-0 ; 
       plaza8_F-t2d56_1.3-0 ; 
       plaza8_F-t2d57_1.3-0 ; 
       plaza8_F-t2d58_1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 55 110 ; 
       1 55 110 ; 
       2 55 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 56 110 ; 
       6 22 110 ; 
       7 22 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 55 110 ; 
       20 55 110 ; 
       21 55 110 ; 
       22 55 110 ; 
       23 20 110 ; 
       24 20 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 21 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       41 21 110 ; 
       42 21 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 2 110 ; 
       48 2 110 ; 
       49 2 110 ; 
       50 2 110 ; 
       51 2 110 ; 
       52 2 110 ; 
       53 22 110 ; 
       54 53 110 ; 
       56 57 110 ; 
       57 53 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 47 300 ; 
       3 8 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       6 12 300 ; 
       6 13 300 ; 
       6 14 300 ; 
       7 15 300 ; 
       7 16 300 ; 
       7 17 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       20 21 300 ; 
       21 34 300 ; 
       22 54 300 ; 
       22 7 300 ; 
       22 18 300 ; 
       23 33 300 ; 
       24 22 300 ; 
       25 23 300 ; 
       26 24 300 ; 
       27 25 300 ; 
       28 26 300 ; 
       29 27 300 ; 
       30 28 300 ; 
       31 32 300 ; 
       32 29 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       35 35 300 ; 
       36 36 300 ; 
       37 37 300 ; 
       38 38 300 ; 
       39 39 300 ; 
       40 40 300 ; 
       41 41 300 ; 
       42 42 300 ; 
       43 43 300 ; 
       44 44 300 ; 
       45 45 300 ; 
       46 46 300 ; 
       47 53 300 ; 
       48 52 300 ; 
       49 51 300 ; 
       50 50 300 ; 
       51 49 300 ; 
       52 48 300 ; 
       56 55 300 ; 
       56 66 300 ; 
       56 73 300 ; 
       56 74 300 ; 
       56 75 300 ; 
       56 76 300 ; 
       57 55 300 ; 
       57 56 300 ; 
       57 57 300 ; 
       57 58 300 ; 
       57 59 300 ; 
       57 60 300 ; 
       57 61 300 ; 
       57 62 300 ; 
       57 63 300 ; 
       57 64 300 ; 
       57 65 300 ; 
       57 67 300 ; 
       57 68 300 ; 
       57 69 300 ; 
       57 70 300 ; 
       57 71 300 ; 
       57 72 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 7 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       54 20 401 ; 
       56 25 401 ; 
       57 27 401 ; 
       58 29 401 ; 
       59 21 401 ; 
       60 22 401 ; 
       61 23 401 ; 
       62 24 401 ; 
       63 26 401 ; 
       64 28 401 ; 
       65 30 401 ; 
       66 31 401 ; 
       67 32 401 ; 
       68 33 401 ; 
       69 34 401 ; 
       70 35 401 ; 
       71 36 401 ; 
       72 37 401 ; 
       73 38 401 ; 
       74 39 401 ; 
       75 40 401 ; 
       76 41 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       9 8 401 ; 
       10 9 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 227.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 230 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 232.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 235 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 237.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 240 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 125 -2 0 MPRFLG 0 ; 
       1 SCHEM 137.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 217.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 106.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 100 -4 0 MPRFLG 0 ; 
       8 SCHEM 75 -6 0 MPRFLG 0 ; 
       9 SCHEM 120 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 122.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 125 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 127.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 130 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 132.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 135 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 137.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 140 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 142.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 117.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 160 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 192.5 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       23 SCHEM 145 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 162.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 152.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 155 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 157.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 160 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 150 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 147.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 170 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 172.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 195 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 185 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 187.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 190 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 192.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 182.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 197.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 200 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 202.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 205 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 180 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 177.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 210 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 212.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 215 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 217.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 220 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 222.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       54 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       55 SCHEM 113.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       56 SCHEM 15 -8 0 MPRFLG 0 ; 
       57 SCHEM 35 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       21 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 152.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 155 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 172.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 207.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 195 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 185 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 187.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 190 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 192.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 182.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 197.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 200 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 202.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 205 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 180 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 177.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 225 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 222.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 210 -6 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       54 SCHEM 115 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 105.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 112.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       20 SCHEM 115 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 112.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
