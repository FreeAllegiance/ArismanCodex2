SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.87-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       refit_bays-light1_1.8-0 ROOT ; 
       refit_bays-light2_1.8-0 ROOT ; 
       refit_bays-light3_1.8-0 ROOT ; 
       refit_bays-light4_1.8-0 ROOT ; 
       refit_bays-light5_1.8-0 ROOT ; 
       refit_bays-light6_1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 57     
       edit_strobe_materials-mat289.4-0 ; 
       edit_strobe_materials-mat300.3-0 ; 
       edit_strobe_materials-mat313.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       edit_strobe_materials-mat315.3-0 ; 
       edit_strobe_materials-mat316.3-0 ; 
       edit_strobe_materials-mat317.3-0 ; 
       edit_strobe_materials-mat318.3-0 ; 
       edit_strobe_materials-mat319.3-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       refit_bays-mat204.1-0 ; 
       refit_bays-mat205.1-0 ; 
       refit_bays-mat206.1-0 ; 
       refit_bays-mat207.1-0 ; 
       refit_bays-mat208.1-0 ; 
       refit_bays-mat209.1-0 ; 
       refit_bays-mat210.1-0 ; 
       refit_bays-mat268_1.1-0 ; 
       refit_bays-mat269.1-0 ; 
       refit_bays-mat270.1-0 ; 
       refit_bays-mat271.1-0 ; 
       refit_bays-mat274.1-0 ; 
       refit_bays-mat275.1-0 ; 
       refit_bays-mat276.1-0 ; 
       refit_bays-mat277.1-0 ; 
       refit_bays-mat278.1-0 ; 
       refit_bays-mat279.1-0 ; 
       refit_bays-mat280.1-0 ; 
       refit_bays-mat281.1-0 ; 
       refit_bays-mat282.1-0 ; 
       refit_bays-mat283.1-0 ; 
       refit_bays-mat284.1-0 ; 
       refit_bays-mat285.1-0 ; 
       refit_bays-mat320.1-0 ; 
       refit_bays-mat321.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-beam_lights.1-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.1-0 ; 
       ss24a-cyl3_1.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-garage1A.1-0 ; 
       ss24a-garage1B.1-0 ; 
       ss24a-garage1C.1-0 ; 
       ss24a-garage1D.1-0 ; 
       ss24a-garage1E.1-0 ; 
       ss24a-garage2A.1-0 ; 
       ss24a-garage2B.1-0 ; 
       ss24a-garage2C.1-0 ; 
       ss24a-garage2D.1-0 ; 
       ss24a-garage2E.1-0 ; 
       ss24a-launch1.1-0 ; 
       ss24a-plaza_lights.1-0 ; 
       ss24a-plaza_lights1.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-SS_1.1-0 ; 
       ss24a-SS_13.1-0 ; 
       ss24a-SS_14.1-0 ; 
       ss24a-SS_15.1-0 ; 
       ss24a-SS_16.1-0 ; 
       ss24a-SS_17.1-0 ; 
       ss24a-SS_18.1-0 ; 
       ss24a-SS_19.1-0 ; 
       ss24a-SS_2.1-0 ; 
       ss24a-SS_20.1-0 ; 
       ss24a-SS_21.1-0 ; 
       ss24a-SS_22.1-0 ; 
       ss24a-SS_23.1-0 ; 
       ss24a-SS_24.1-0 ; 
       ss24a-SS_25.1-0 ; 
       ss24a-SS_26.1-0 ; 
       ss24a-SS_27.1-0 ; 
       ss24a-SS_28.1-0 ; 
       ss24a-SS_29.1-0 ; 
       ss24a-SS_30.1-0 ; 
       ss24a-SS_31.1-0 ; 
       ss24a-SS_32.1-0 ; 
       ss24a-SS_33.1-0 ; 
       ss24a-SS_34.1-0 ; 
       ss24a-SS_35.1-0 ; 
       ss24a-SS_36.1-0 ; 
       ss24a-SS_37.1-0 ; 
       ss24a-SS_38.1-0 ; 
       ss24a-SS_39.1-0 ; 
       ss24a-SS_40.1-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.47-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-refit_bays.8-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       new-t2d2_1.14-0 ; 
       plaza8_F-t2d38_1.5-0 ; 
       plaza8_F-t2d39_1.5-0 ; 
       plaza8_F-t2d40_1.5-0 ; 
       plaza8_F-t2d41_1.5-0 ; 
       plaza8_F-t2d42_1.5-0 ; 
       plaza8_F-t2d43_1.5-0 ; 
       plaza8_F-t2d44_1.5-0 ; 
       plaza8_F-t2d45_1.5-0 ; 
       plaza8_F-t2d46_1.5-0 ; 
       plaza8_F-t2d47_1.5-0 ; 
       plaza8_F-t2d48_1.5-0 ; 
       plaza8_F-t2d49_1.5-0 ; 
       plaza8_F-t2d50_1.5-0 ; 
       plaza8_F-t2d51_1.5-0 ; 
       plaza8_F-t2d52_1.5-0 ; 
       plaza8_F-t2d53_1.5-0 ; 
       plaza8_F-t2d54_1.5-0 ; 
       plaza8_F-t2d55_1.5-0 ; 
       plaza8_F-t2d56_1.5-0 ; 
       plaza8_F-t2d57_1.5-0 ; 
       plaza8_F-t2d58_1.5-0 ; 
       refit_bays-t2d196.1-0 ; 
       refit_bays-t2d197.1-0 ; 
       refit_bays-t2d198.1-0 ; 
       refit_bays-t2d199.1-0 ; 
       refit_bays-t2d200.1-0 ; 
       refit_bays-t2d201.1-0 ; 
       refit_bays-t2d260_1.5-0 ; 
       refit_bays-t2d261.1-0 ; 
       refit_bays-t2d262.1-0 ; 
       refit_bays-t2d263.1-0 ; 
       refit_bays-t2d268.1-0 ; 
       refit_bays-t2d269.1-0 ; 
       refit_bays-t2d270.1-0 ; 
       refit_bays-t2d271.1-0 ; 
       refit_bays-t2d272.1-0 ; 
       refit_bays-t2d273.1-0 ; 
       refit_bays-t2d274.1-0 ; 
       refit_bays-t2d275.1-0 ; 
       refit_bays-t2d276.1-0 ; 
       refit_bays-t2d277.5-0 ; 
       refit_bays-t2d278.2-0 ; 
       refit_bays-t2d279.2-0 ; 
       refit_bays-t2d280.1-0 ; 
       refit_bays-t2d281.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 58 110 ; 
       1 58 110 ; 
       2 58 110 ; 
       3 25 110 ; 
       4 25 110 ; 
       5 25 110 ; 
       6 59 110 ; 
       7 25 110 ; 
       8 25 110 ; 
       9 3 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 58 110 ; 
       21 58 110 ; 
       22 58 110 ; 
       23 25 110 ; 
       24 23 110 ; 
       25 58 110 ; 
       26 21 110 ; 
       27 21 110 ; 
       28 21 110 ; 
       29 21 110 ; 
       30 21 110 ; 
       31 21 110 ; 
       32 21 110 ; 
       33 21 110 ; 
       34 21 110 ; 
       35 21 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 22 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       44 22 110 ; 
       45 22 110 ; 
       46 22 110 ; 
       47 22 110 ; 
       48 22 110 ; 
       49 22 110 ; 
       50 2 110 ; 
       51 2 110 ; 
       52 2 110 ; 
       53 2 110 ; 
       54 2 110 ; 
       55 2 110 ; 
       56 25 110 ; 
       57 56 110 ; 
       59 60 110 ; 
       60 56 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       3 40 300 ; 
       4 55 300 ; 
       4 56 300 ; 
       5 53 300 ; 
       5 54 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       7 46 300 ; 
       7 47 300 ; 
       7 48 300 ; 
       8 49 300 ; 
       8 50 300 ; 
       8 51 300 ; 
       9 32 300 ; 
       9 33 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       9 36 300 ; 
       9 37 300 ; 
       9 38 300 ; 
       22 1 300 ; 
       23 42 300 ; 
       24 41 300 ; 
       25 9 300 ; 
       25 39 300 ; 
       25 52 300 ; 
       26 0 300 ; 
       27 0 300 ; 
       28 0 300 ; 
       29 0 300 ; 
       30 0 300 ; 
       31 0 300 ; 
       32 0 300 ; 
       33 0 300 ; 
       34 0 300 ; 
       35 0 300 ; 
       36 0 300 ; 
       37 0 300 ; 
       38 1 300 ; 
       39 1 300 ; 
       40 1 300 ; 
       41 1 300 ; 
       42 1 300 ; 
       43 1 300 ; 
       44 1 300 ; 
       45 1 300 ; 
       46 1 300 ; 
       47 1 300 ; 
       48 1 300 ; 
       49 1 300 ; 
       50 8 300 ; 
       51 7 300 ; 
       52 6 300 ; 
       53 5 300 ; 
       54 4 300 ; 
       55 3 300 ; 
       59 10 300 ; 
       59 21 300 ; 
       59 28 300 ; 
       59 29 300 ; 
       59 30 300 ; 
       59 31 300 ; 
       60 10 300 ; 
       60 11 300 ; 
       60 12 300 ; 
       60 13 300 ; 
       60 14 300 ; 
       60 15 300 ; 
       60 16 300 ; 
       60 17 300 ; 
       60 18 300 ; 
       60 19 300 ; 
       60 20 300 ; 
       60 22 300 ; 
       60 23 300 ; 
       60 24 300 ; 
       60 25 300 ; 
       60 26 300 ; 
       60 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       11 5 401 ; 
       12 7 401 ; 
       13 9 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 6 401 ; 
       19 8 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       48 37 401 ; 
       49 38 401 ; 
       50 39 401 ; 
       51 40 401 ; 
       52 41 401 ; 
       53 42 401 ; 
       54 43 401 ; 
       55 44 401 ; 
       56 45 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 235 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 237.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 240 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 242.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 245 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 247.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 135 -2 0 MPRFLG 0 ; 
       1 SCHEM 147.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 225 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 116.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 106.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 100 -4 0 MPRFLG 0 ; 
       9 SCHEM 75 -6 0 MPRFLG 0 ; 
       10 SCHEM 130 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 132.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 135 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 137.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 140 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 142.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 145 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 147.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 150 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 152.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 127.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 168.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 200 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 110 -6 0 MPRFLG 0 ; 
       25 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       26 SCHEM 155 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 172.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 162.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 165 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 167.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 170 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 160 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 175 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 157.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 177.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 180 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 182.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 202.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 192.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 195 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 197.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 200 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 190 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 205 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 207.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 210 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 212.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 187.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 185 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 217.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 220 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 222.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 225 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 227.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 230 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       57 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       58 SCHEM 117.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       59 SCHEM 15 -8 0 MPRFLG 0 ; 
       60 SCHEM 35 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 160.24 -12.05101 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 197.3573 -8.447124 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 232.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 230 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 227.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 225 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 222.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 125 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 105.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 12.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 52.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 15 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 17.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 22.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 10 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 7.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 102.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 105 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 117.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 125 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 35 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 37.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 40 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 42.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 45 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 47.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 50 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 12.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 62.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 65 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 15 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 17.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 20 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 22.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 70 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 10 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 7.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 102.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 107.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 105 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 117.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
