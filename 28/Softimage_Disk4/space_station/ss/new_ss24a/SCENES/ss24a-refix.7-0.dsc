SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.7-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1.7-0 ROOT ; 
       rotate-light2.7-0 ROOT ; 
       rotate-light3.7-0 ROOT ; 
       rotate-light4.7-0 ROOT ; 
       rotate-light5.7-0 ROOT ; 
       rotate-light6.7-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       new-mat2.2-0 ; 
       plaza8_F-mat1_1.1-0 ; 
       plaza8_F-mat40_1.1-0 ; 
       plaza8_F-mat41_1.1-0 ; 
       plaza8_F-mat42_1.1-0 ; 
       plaza8_F-mat43_1.1-0 ; 
       plaza8_F-mat44_1.1-0 ; 
       plaza8_F-mat45_1.1-0 ; 
       plaza8_F-mat46_1.1-0 ; 
       plaza8_F-mat47_1.1-0 ; 
       plaza8_F-mat48_1.1-0 ; 
       plaza8_F-mat49_1.1-0 ; 
       plaza8_F-mat50_1.1-0 ; 
       plaza8_F-mat51_1.1-0 ; 
       plaza8_F-mat52_1.1-0 ; 
       plaza8_F-mat53_1.1-0 ; 
       plaza8_F-mat54_1.1-0 ; 
       plaza8_F-mat55_1.1-0 ; 
       plaza8_F-mat56_1.1-0 ; 
       plaza8_F-mat57_1.1-0 ; 
       plaza8_F-mat58_1.1-0 ; 
       plaza8_F-mat59_1.1-0 ; 
       plaza8_F-mat60_1.1-0 ; 
       refix-mat274.1-0 ; 
       refix-mat275.1-0 ; 
       refix-mat276.1-0 ; 
       refix-mat277.1-0 ; 
       refix-mat278.1-0 ; 
       refix-mat279.1-0 ; 
       refix-mat280.1-0 ; 
       refix-mat281.1-0 ; 
       refix-mat282.1-0 ; 
       refix-mat283.1-0 ; 
       rotate-mat204.1-0 ; 
       rotate-mat205.1-0 ; 
       rotate-mat206.1-0 ; 
       rotate-mat207.1-0 ; 
       rotate-mat208.1-0 ; 
       rotate-mat209.1-0 ; 
       rotate-mat210.1-0 ; 
       rotate-mat268.2-0 ; 
       rotate-mat269.1-0 ; 
       rotate-mat270_1.1-0 ; 
       rotate-mat271_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-cube1.3-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl1.3-0 ; 
       ss24a-fuselg.5-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-fuselg5.1-0 ; 
       ss24a-fuselg6.1-0 ; 
       ss24a-Garage1A.1-0 ; 
       ss24a-Garage1A_1.1-0 ; 
       ss24a-Garage1B.1-0 ; 
       ss24a-Garage1B_1.1-0 ; 
       ss24a-Garage1C.1-0 ; 
       ss24a-Garage1C_1.1-0 ; 
       ss24a-Garage1D.1-0 ; 
       ss24a-Garage1D_1.1-0 ; 
       ss24a-Garage1E.1-0 ; 
       ss24a-Garage1E_1.1-0 ; 
       ss24a-Garage2A.1-0 ; 
       ss24a-Garage2B.1-0 ; 
       ss24a-Garage2C.1-0 ; 
       ss24a-Garage2D.1-0 ; 
       ss24a-Garage2E.1-0 ; 
       ss24a-Launch1.1-0 ; 
       ss24a-Launch1_1.1-0 ; 
       ss24a-Launch2.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_base_1.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-radar_Dish_1.1-0 ; 
       ss24a-skin2.22-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18.24-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a.1-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.7-0 ROOT ; 
       ss24a-ss24a_1_1.2-0 ROOT ; 
       ss24a-tfuselg1.1-0 ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-refix.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 42     
       new-t2d2_1.3-0 ; 
       plaza8_F-t2d38.2-0 ; 
       plaza8_F-t2d39.2-0 ; 
       plaza8_F-t2d40.2-0 ; 
       plaza8_F-t2d41.2-0 ; 
       plaza8_F-t2d42.2-0 ; 
       plaza8_F-t2d43.2-0 ; 
       plaza8_F-t2d44.2-0 ; 
       plaza8_F-t2d45.2-0 ; 
       plaza8_F-t2d46.2-0 ; 
       plaza8_F-t2d47.2-0 ; 
       plaza8_F-t2d48.2-0 ; 
       plaza8_F-t2d49.2-0 ; 
       plaza8_F-t2d50.2-0 ; 
       plaza8_F-t2d51.2-0 ; 
       plaza8_F-t2d52.2-0 ; 
       plaza8_F-t2d53.2-0 ; 
       plaza8_F-t2d54.2-0 ; 
       plaza8_F-t2d55.2-0 ; 
       plaza8_F-t2d56.2-0 ; 
       plaza8_F-t2d57.2-0 ; 
       plaza8_F-t2d58.2-0 ; 
       refix-t2d268.2-0 ; 
       refix-t2d269.2-0 ; 
       refix-t2d270.2-0 ; 
       refix-t2d271.1-0 ; 
       refix-t2d272.1-0 ; 
       refix-t2d273.1-0 ; 
       refix-t2d274.1-0 ; 
       refix-t2d275.1-0 ; 
       refix-t2d276.1-0 ; 
       refix-t2d277.1-0 ; 
       rotate-t2d196.1-0 ; 
       rotate-t2d197.1-0 ; 
       rotate-t2d198.1-0 ; 
       rotate-t2d199.1-0 ; 
       rotate-t2d200.1-0 ; 
       rotate-t2d201.1-0 ; 
       rotate-t2d260.3-0 ; 
       rotate-t2d261.1-0 ; 
       rotate-t2d262.1-0 ; 
       rotate-t2d263.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 42 110 ; 
       1 42 110 ; 
       2 35 110 ; 
       3 36 110 ; 
       4 35 110 ; 
       5 43 110 ; 
       6 44 110 ; 
       7 36 110 ; 
       8 36 110 ; 
       9 2 110 ; 
       10 3 110 ; 
       11 35 110 ; 
       12 35 110 ; 
       13 0 110 ; 
       14 36 110 ; 
       15 0 110 ; 
       16 36 110 ; 
       17 0 110 ; 
       18 36 110 ; 
       19 0 110 ; 
       20 36 110 ; 
       21 0 110 ; 
       22 36 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       25 1 110 ; 
       26 1 110 ; 
       27 1 110 ; 
       28 42 110 ; 
       29 36 110 ; 
       30 36 110 ; 
       31 42 110 ; 
       32 36 110 ; 
       33 31 110 ; 
       34 32 110 ; 
       35 42 110 ; 
       36 41 110 ; 
       37 35 110 ; 
       38 36 110 ; 
       39 37 110 ; 
       40 38 110 ; 
       43 45 110 ; 
       44 46 110 ; 
       45 37 110 ; 
       46 38 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 41 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       7 26 300 ; 
       7 27 300 ; 
       7 28 300 ; 
       8 29 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       10 33 300 ; 
       10 34 300 ; 
       10 35 300 ; 
       10 36 300 ; 
       10 37 300 ; 
       10 38 300 ; 
       10 39 300 ; 
       32 43 300 ; 
       34 42 300 ; 
       36 0 300 ; 
       36 40 300 ; 
       36 32 300 ; 
       44 1 300 ; 
       44 12 300 ; 
       44 19 300 ; 
       44 20 300 ; 
       44 21 300 ; 
       44 22 300 ; 
       46 1 300 ; 
       46 2 300 ; 
       46 3 300 ; 
       46 4 300 ; 
       46 5 300 ; 
       46 6 300 ; 
       46 7 300 ; 
       46 8 300 ; 
       46 9 300 ; 
       46 10 300 ; 
       46 11 300 ; 
       46 13 300 ; 
       46 14 300 ; 
       46 15 300 ; 
       46 16 300 ; 
       46 17 300 ; 
       46 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 39 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       4 9 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 6 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       23 22 401 ; 
       24 23 401 ; 
       25 24 401 ; 
       26 25 401 ; 
       27 26 401 ; 
       28 27 401 ; 
       29 28 401 ; 
       30 29 401 ; 
       31 30 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       32 31 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 182.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 185 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 187.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 190 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 192.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 195 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 122.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 160 -4 0 MPRFLG 0 ; 
       8 SCHEM 167.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 120 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 140 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 25 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 142.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 27.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 145 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 30 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 147.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 32.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 150 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 35 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 37.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 40 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 42.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 45 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 20 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 152.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 155 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 136.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 135 -6 0 MPRFLG 0 ; 
       35 SCHEM 8.75 -2 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 111.25 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       37 SCHEM 3.75 -4 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       41 SCHEM 111.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       42 SCHEM 23.75 0 0 WIRECOL 4 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       43 SCHEM 5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 60 -8 0 MPRFLG 0 ; 
       45 SCHEM 5 -6 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 80 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 175 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 68.87247 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 57.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 110 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 55 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 50 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 162.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 157.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 160 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 170 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 165 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 167.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 172.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 132.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 137.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 197.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 175 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 80 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 82.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 72.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 95 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 97.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 100 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 102.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 105 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 107.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 110 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 60 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 55 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 50 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 162.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 157.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 160 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 170 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 165 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 167.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 112.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 115 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 117.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 120 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 172.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 135 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 137.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 200 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
