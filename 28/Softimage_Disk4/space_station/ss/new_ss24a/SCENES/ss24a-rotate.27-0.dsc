SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.72-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1_1.27-0 ROOT ; 
       rotate-light2_1.27-0 ROOT ; 
       rotate-light3_1.27-0 ROOT ; 
       rotate-light4_1.27-0 ROOT ; 
       rotate-light5_1.27-0 ROOT ; 
       rotate-light6_1.27-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 79     
       edit_strobe_materials-mat287.3-0 ; 
       edit_strobe_materials-mat288.3-0 ; 
       edit_strobe_materials-mat289.3-0 ; 
       edit_strobe_materials-mat290.3-0 ; 
       edit_strobe_materials-mat291.3-0 ; 
       edit_strobe_materials-mat292.3-0 ; 
       edit_strobe_materials-mat293.3-0 ; 
       edit_strobe_materials-mat294.3-0 ; 
       edit_strobe_materials-mat295.3-0 ; 
       edit_strobe_materials-mat296.3-0 ; 
       edit_strobe_materials-mat297.3-0 ; 
       edit_strobe_materials-mat298.3-0 ; 
       edit_strobe_materials-mat299.3-0 ; 
       edit_strobe_materials-mat300.2-0 ; 
       edit_strobe_materials-mat301.2-0 ; 
       edit_strobe_materials-mat302.2-0 ; 
       edit_strobe_materials-mat303.2-0 ; 
       edit_strobe_materials-mat304.2-0 ; 
       edit_strobe_materials-mat305.2-0 ; 
       edit_strobe_materials-mat306.2-0 ; 
       edit_strobe_materials-mat307.2-0 ; 
       edit_strobe_materials-mat308.2-0 ; 
       edit_strobe_materials-mat309.2-0 ; 
       edit_strobe_materials-mat310.2-0 ; 
       edit_strobe_materials-mat311.2-0 ; 
       edit_strobe_materials-mat312.2-0 ; 
       edit_strobe_materials-mat313.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       edit_strobe_materials-mat315.3-0 ; 
       edit_strobe_materials-mat316.3-0 ; 
       edit_strobe_materials-mat317.3-0 ; 
       edit_strobe_materials-mat318.3-0 ; 
       edit_strobe_materials-mat319.3-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       rotate-mat204.5-0 ; 
       rotate-mat205.5-0 ; 
       rotate-mat206.5-0 ; 
       rotate-mat207.5-0 ; 
       rotate-mat208.5-0 ; 
       rotate-mat209.5-0 ; 
       rotate-mat210.5-0 ; 
       rotate-mat268_1.4-0 ; 
       rotate-mat269.5-0 ; 
       rotate-mat270.1-0 ; 
       rotate-mat271.1-0 ; 
       rotate-mat274.4-0 ; 
       rotate-mat275.4-0 ; 
       rotate-mat276.4-0 ; 
       rotate-mat277.4-0 ; 
       rotate-mat278.4-0 ; 
       rotate-mat279.4-0 ; 
       rotate-mat280.4-0 ; 
       rotate-mat281.4-0 ; 
       rotate-mat282.4-0 ; 
       rotate-mat283.4-0 ; 
       rotate-mat284.4-0 ; 
       rotate-mat285.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-beam_lights.1-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-garage1A.1-0 ; 
       ss24a-garage1B.1-0 ; 
       ss24a-garage1C.1-0 ; 
       ss24a-garage1D.1-0 ; 
       ss24a-garage1E.1-0 ; 
       ss24a-garage2A.1-0 ; 
       ss24a-garage2B.1-0 ; 
       ss24a-garage2C.1-0 ; 
       ss24a-garage2D.1-0 ; 
       ss24a-garage2E.1-0 ; 
       ss24a-launch1.1-0 ; 
       ss24a-plaza_lights.1-0 ; 
       ss24a-plaza_lights1.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-SS_1.1-0 ; 
       ss24a-SS_13.1-0 ; 
       ss24a-SS_14.1-0 ; 
       ss24a-SS_15.1-0 ; 
       ss24a-SS_16.1-0 ; 
       ss24a-SS_17.1-0 ; 
       ss24a-SS_18.1-0 ; 
       ss24a-SS_19.1-0 ; 
       ss24a-SS_2.1-0 ; 
       ss24a-SS_20.1-0 ; 
       ss24a-SS_21.1-0 ; 
       ss24a-SS_22.1-0 ; 
       ss24a-SS_23.1-0 ; 
       ss24a-SS_24.1-0 ; 
       ss24a-SS_25.1-0 ; 
       ss24a-SS_26.1-0 ; 
       ss24a-SS_27.1-0 ; 
       ss24a-SS_28.1-0 ; 
       ss24a-SS_29.1-0 ; 
       ss24a-SS_30.1-0 ; 
       ss24a-SS_31.1-0 ; 
       ss24a-SS_32.1-0 ; 
       ss24a-SS_33.1-0 ; 
       ss24a-SS_34.1-0 ; 
       ss24a-SS_35.1-0 ; 
       ss24a-SS_36.1-0 ; 
       ss24a-SS_37.1-0 ; 
       ss24a-SS_38.1-0 ; 
       ss24a-SS_39.1-0 ; 
       ss24a-SS_40.1-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.36-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-rotate.27-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       new-t2d2_1.9-0 ; 
       plaza8_F-t2d38_1.4-0 ; 
       plaza8_F-t2d39_1.4-0 ; 
       plaza8_F-t2d40_1.4-0 ; 
       plaza8_F-t2d41_1.4-0 ; 
       plaza8_F-t2d42_1.4-0 ; 
       plaza8_F-t2d43_1.4-0 ; 
       plaza8_F-t2d44_1.4-0 ; 
       plaza8_F-t2d45_1.4-0 ; 
       plaza8_F-t2d46_1.4-0 ; 
       plaza8_F-t2d47_1.4-0 ; 
       plaza8_F-t2d48_1.4-0 ; 
       plaza8_F-t2d49_1.4-0 ; 
       plaza8_F-t2d50_1.4-0 ; 
       plaza8_F-t2d51_1.4-0 ; 
       plaza8_F-t2d52_1.4-0 ; 
       plaza8_F-t2d53_1.4-0 ; 
       plaza8_F-t2d54_1.4-0 ; 
       plaza8_F-t2d55_1.4-0 ; 
       plaza8_F-t2d56_1.4-0 ; 
       plaza8_F-t2d57_1.4-0 ; 
       plaza8_F-t2d58_1.4-0 ; 
       rotate-t2d196.5-0 ; 
       rotate-t2d197.5-0 ; 
       rotate-t2d198.5-0 ; 
       rotate-t2d199.5-0 ; 
       rotate-t2d200.5-0 ; 
       rotate-t2d201.5-0 ; 
       rotate-t2d260_1.4-0 ; 
       rotate-t2d261.5-0 ; 
       rotate-t2d262.3-0 ; 
       rotate-t2d263.3-0 ; 
       rotate-t2d268.5-0 ; 
       rotate-t2d269.5-0 ; 
       rotate-t2d270.5-0 ; 
       rotate-t2d271.5-0 ; 
       rotate-t2d272.5-0 ; 
       rotate-t2d273.5-0 ; 
       rotate-t2d274.5-0 ; 
       rotate-t2d275.5-0 ; 
       rotate-t2d276.5-0 ; 
       rotate-t2d277.4-0 ; 
       rotate-t2d278.4-0 ; 
       rotate-t2d279.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 57 110 ; 
       1 57 110 ; 
       2 57 110 ; 
       3 24 110 ; 
       4 24 110 ; 
       5 58 110 ; 
       6 24 110 ; 
       7 24 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 57 110 ; 
       20 57 110 ; 
       21 57 110 ; 
       22 24 110 ; 
       23 22 110 ; 
       24 57 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       41 21 110 ; 
       42 21 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 2 110 ; 
       50 2 110 ; 
       51 2 110 ; 
       52 2 110 ; 
       53 2 110 ; 
       54 2 110 ; 
       55 24 110 ; 
       56 55 110 ; 
       58 59 110 ; 
       59 55 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 26 300 ; 
       3 64 300 ; 
       4 77 300 ; 
       4 78 300 ; 
       5 67 300 ; 
       5 68 300 ; 
       5 69 300 ; 
       6 70 300 ; 
       6 71 300 ; 
       6 72 300 ; 
       7 73 300 ; 
       7 74 300 ; 
       7 75 300 ; 
       8 56 300 ; 
       8 57 300 ; 
       8 58 300 ; 
       8 59 300 ; 
       8 60 300 ; 
       8 61 300 ; 
       8 62 300 ; 
       20 0 300 ; 
       21 13 300 ; 
       22 66 300 ; 
       23 65 300 ; 
       24 33 300 ; 
       24 63 300 ; 
       24 76 300 ; 
       25 12 300 ; 
       26 1 300 ; 
       27 2 300 ; 
       28 3 300 ; 
       29 4 300 ; 
       30 5 300 ; 
       31 6 300 ; 
       32 7 300 ; 
       33 11 300 ; 
       34 8 300 ; 
       35 9 300 ; 
       36 10 300 ; 
       37 14 300 ; 
       38 15 300 ; 
       39 16 300 ; 
       40 17 300 ; 
       41 18 300 ; 
       42 19 300 ; 
       43 20 300 ; 
       44 21 300 ; 
       45 22 300 ; 
       46 23 300 ; 
       47 24 300 ; 
       48 25 300 ; 
       49 32 300 ; 
       50 31 300 ; 
       51 30 300 ; 
       52 29 300 ; 
       53 28 300 ; 
       54 27 300 ; 
       58 34 300 ; 
       58 45 300 ; 
       58 52 300 ; 
       58 53 300 ; 
       58 54 300 ; 
       58 55 300 ; 
       59 34 300 ; 
       59 35 300 ; 
       59 36 300 ; 
       59 37 300 ; 
       59 38 300 ; 
       59 39 300 ; 
       59 40 300 ; 
       59 41 300 ; 
       59 42 300 ; 
       59 43 300 ; 
       59 44 300 ; 
       59 46 300 ; 
       59 47 300 ; 
       59 48 300 ; 
       59 49 300 ; 
       59 50 300 ; 
       59 51 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       33 0 401 ; 
       35 5 401 ; 
       36 7 401 ; 
       37 9 401 ; 
       38 1 401 ; 
       39 2 401 ; 
       40 3 401 ; 
       41 4 401 ; 
       42 6 401 ; 
       43 8 401 ; 
       44 10 401 ; 
       45 11 401 ; 
       46 12 401 ; 
       47 13 401 ; 
       48 14 401 ; 
       49 15 401 ; 
       50 16 401 ; 
       51 17 401 ; 
       52 18 401 ; 
       53 19 401 ; 
       54 20 401 ; 
       55 21 401 ; 
       57 22 401 ; 
       58 23 401 ; 
       59 24 401 ; 
       60 25 401 ; 
       61 26 401 ; 
       62 27 401 ; 
       63 28 401 ; 
       65 30 401 ; 
       66 31 401 ; 
       67 32 401 ; 
       68 33 401 ; 
       69 34 401 ; 
       70 35 401 ; 
       71 36 401 ; 
       72 37 401 ; 
       73 38 401 ; 
       74 39 401 ; 
       75 40 401 ; 
       76 41 401 ; 
       77 42 401 ; 
       78 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 130 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 132.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 135 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 MPRFLG 0 ; 
       2 SCHEM 113.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM 37.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 40 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 61.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 91.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 MPRFLG 0 ; 
       25 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 65 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 55 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 60 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 95 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 85 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 87.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 90 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 82.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 100 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 102.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 105 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 80 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 107.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 110 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 112.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 115 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 117.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 120 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       56 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       57 SCHEM 61.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       58 SCHEM 5 -8 0 MPRFLG 0 ; 
       59 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 76.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 96.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 99 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 104 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 79 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 76.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 116.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 114 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 109 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 95.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
