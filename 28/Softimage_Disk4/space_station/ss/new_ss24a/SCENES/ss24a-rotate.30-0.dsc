SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.76-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       rotate-light1_1.30-0 ROOT ; 
       rotate-light2_1.30-0 ROOT ; 
       rotate-light3_1.30-0 ROOT ; 
       rotate-light4_1.30-0 ROOT ; 
       rotate-light5_1.30-0 ROOT ; 
       rotate-light6_1.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 55     
       edit_strobe_materials-mat289.4-0 ; 
       edit_strobe_materials-mat300.3-0 ; 
       edit_strobe_materials-mat313.3-0 ; 
       edit_strobe_materials-mat314.3-0 ; 
       edit_strobe_materials-mat315.3-0 ; 
       edit_strobe_materials-mat316.3-0 ; 
       edit_strobe_materials-mat317.3-0 ; 
       edit_strobe_materials-mat318.3-0 ; 
       edit_strobe_materials-mat319.3-0 ; 
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       rotate-mat204.5-0 ; 
       rotate-mat205.5-0 ; 
       rotate-mat206.5-0 ; 
       rotate-mat207.5-0 ; 
       rotate-mat208.5-0 ; 
       rotate-mat209.5-0 ; 
       rotate-mat210.5-0 ; 
       rotate-mat268_1.4-0 ; 
       rotate-mat269.5-0 ; 
       rotate-mat270.1-0 ; 
       rotate-mat271.1-0 ; 
       rotate-mat274.4-0 ; 
       rotate-mat275.4-0 ; 
       rotate-mat276.4-0 ; 
       rotate-mat277.4-0 ; 
       rotate-mat278.4-0 ; 
       rotate-mat279.4-0 ; 
       rotate-mat280.4-0 ; 
       rotate-mat281.4-0 ; 
       rotate-mat282.4-0 ; 
       rotate-mat283.4-0 ; 
       rotate-mat284.4-0 ; 
       rotate-mat285.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       ss24a-bay1.1-0 ; 
       ss24a-bay2.1-0 ; 
       ss24a-beam_lights.1-0 ; 
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-garage1A.1-0 ; 
       ss24a-garage1B.1-0 ; 
       ss24a-garage1C.1-0 ; 
       ss24a-garage1D.1-0 ; 
       ss24a-garage1E.1-0 ; 
       ss24a-garage2A.1-0 ; 
       ss24a-garage2B.1-0 ; 
       ss24a-garage2C.1-0 ; 
       ss24a-garage2D.1-0 ; 
       ss24a-garage2E.1-0 ; 
       ss24a-launch1.1-0 ; 
       ss24a-plaza_lights.1-0 ; 
       ss24a-plaza_lights1.1-0 ; 
       ss24a-radar_base.1-0 ; 
       ss24a-radar_Dish.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-SS_1.1-0 ; 
       ss24a-SS_13.1-0 ; 
       ss24a-SS_14.1-0 ; 
       ss24a-SS_15.1-0 ; 
       ss24a-SS_16.1-0 ; 
       ss24a-SS_17.1-0 ; 
       ss24a-SS_18.1-0 ; 
       ss24a-SS_19.1-0 ; 
       ss24a-SS_2.1-0 ; 
       ss24a-SS_20.1-0 ; 
       ss24a-SS_21.1-0 ; 
       ss24a-SS_22.1-0 ; 
       ss24a-SS_23.1-0 ; 
       ss24a-SS_24.1-0 ; 
       ss24a-SS_25.1-0 ; 
       ss24a-SS_26.1-0 ; 
       ss24a-SS_27.1-0 ; 
       ss24a-SS_28.1-0 ; 
       ss24a-SS_29.1-0 ; 
       ss24a-SS_30.1-0 ; 
       ss24a-SS_31.1-0 ; 
       ss24a-SS_32.1-0 ; 
       ss24a-SS_33.1-0 ; 
       ss24a-SS_34.1-0 ; 
       ss24a-SS_35.1-0 ; 
       ss24a-SS_36.1-0 ; 
       ss24a-SS_37.1-0 ; 
       ss24a-SS_38.1-0 ; 
       ss24a-SS_39.1-0 ; 
       ss24a-SS_40.1-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.39-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-rotate.30-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 44     
       new-t2d2_1.10-0 ; 
       plaza8_F-t2d38_1.5-0 ; 
       plaza8_F-t2d39_1.5-0 ; 
       plaza8_F-t2d40_1.5-0 ; 
       plaza8_F-t2d41_1.5-0 ; 
       plaza8_F-t2d42_1.5-0 ; 
       plaza8_F-t2d43_1.5-0 ; 
       plaza8_F-t2d44_1.5-0 ; 
       plaza8_F-t2d45_1.5-0 ; 
       plaza8_F-t2d46_1.5-0 ; 
       plaza8_F-t2d47_1.5-0 ; 
       plaza8_F-t2d48_1.5-0 ; 
       plaza8_F-t2d49_1.5-0 ; 
       plaza8_F-t2d50_1.5-0 ; 
       plaza8_F-t2d51_1.5-0 ; 
       plaza8_F-t2d52_1.5-0 ; 
       plaza8_F-t2d53_1.5-0 ; 
       plaza8_F-t2d54_1.5-0 ; 
       plaza8_F-t2d55_1.5-0 ; 
       plaza8_F-t2d56_1.5-0 ; 
       plaza8_F-t2d57_1.5-0 ; 
       plaza8_F-t2d58_1.5-0 ; 
       rotate-t2d196.6-0 ; 
       rotate-t2d197.6-0 ; 
       rotate-t2d198.6-0 ; 
       rotate-t2d199.6-0 ; 
       rotate-t2d200.6-0 ; 
       rotate-t2d201.6-0 ; 
       rotate-t2d260_1.5-0 ; 
       rotate-t2d261.6-0 ; 
       rotate-t2d262.4-0 ; 
       rotate-t2d263.4-0 ; 
       rotate-t2d268.6-0 ; 
       rotate-t2d269.6-0 ; 
       rotate-t2d270.6-0 ; 
       rotate-t2d271.6-0 ; 
       rotate-t2d272.6-0 ; 
       rotate-t2d273.6-0 ; 
       rotate-t2d274.6-0 ; 
       rotate-t2d275.6-0 ; 
       rotate-t2d276.6-0 ; 
       rotate-t2d277.5-0 ; 
       rotate-t2d278.5-0 ; 
       rotate-t2d279.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 57 110 ; 
       1 57 110 ; 
       2 57 110 ; 
       3 24 110 ; 
       4 24 110 ; 
       5 58 110 ; 
       6 24 110 ; 
       7 24 110 ; 
       8 3 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 1 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 57 110 ; 
       20 57 110 ; 
       21 57 110 ; 
       22 24 110 ; 
       23 22 110 ; 
       24 57 110 ; 
       25 20 110 ; 
       26 20 110 ; 
       27 20 110 ; 
       28 20 110 ; 
       29 20 110 ; 
       30 20 110 ; 
       31 20 110 ; 
       32 20 110 ; 
       33 20 110 ; 
       34 20 110 ; 
       35 20 110 ; 
       36 20 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 21 110 ; 
       41 21 110 ; 
       42 21 110 ; 
       43 21 110 ; 
       44 21 110 ; 
       45 21 110 ; 
       46 21 110 ; 
       47 21 110 ; 
       48 21 110 ; 
       49 2 110 ; 
       50 2 110 ; 
       51 2 110 ; 
       52 2 110 ; 
       53 2 110 ; 
       54 2 110 ; 
       55 24 110 ; 
       56 55 110 ; 
       58 59 110 ; 
       59 55 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 2 300 ; 
       3 40 300 ; 
       4 53 300 ; 
       4 54 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       6 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       8 34 300 ; 
       8 35 300 ; 
       8 36 300 ; 
       8 37 300 ; 
       8 38 300 ; 
       21 1 300 ; 
       22 42 300 ; 
       23 41 300 ; 
       24 9 300 ; 
       24 39 300 ; 
       24 52 300 ; 
       25 0 300 ; 
       26 0 300 ; 
       27 0 300 ; 
       28 0 300 ; 
       29 0 300 ; 
       30 0 300 ; 
       31 0 300 ; 
       32 0 300 ; 
       33 0 300 ; 
       34 0 300 ; 
       35 0 300 ; 
       36 0 300 ; 
       37 1 300 ; 
       38 1 300 ; 
       39 1 300 ; 
       40 1 300 ; 
       41 1 300 ; 
       42 1 300 ; 
       43 1 300 ; 
       44 1 300 ; 
       45 1 300 ; 
       46 1 300 ; 
       47 1 300 ; 
       48 1 300 ; 
       49 8 300 ; 
       50 7 300 ; 
       51 6 300 ; 
       52 5 300 ; 
       53 4 300 ; 
       54 3 300 ; 
       58 10 300 ; 
       58 21 300 ; 
       58 28 300 ; 
       58 29 300 ; 
       58 30 300 ; 
       58 31 300 ; 
       59 10 300 ; 
       59 11 300 ; 
       59 12 300 ; 
       59 13 300 ; 
       59 14 300 ; 
       59 15 300 ; 
       59 16 300 ; 
       59 17 300 ; 
       59 18 300 ; 
       59 19 300 ; 
       59 20 300 ; 
       59 22 300 ; 
       59 23 300 ; 
       59 24 300 ; 
       59 25 300 ; 
       59 26 300 ; 
       59 27 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       3 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       9 0 401 ; 
       11 5 401 ; 
       12 7 401 ; 
       13 9 401 ; 
       14 1 401 ; 
       15 2 401 ; 
       16 3 401 ; 
       17 4 401 ; 
       18 6 401 ; 
       19 8 401 ; 
       20 10 401 ; 
       21 11 401 ; 
       22 12 401 ; 
       23 13 401 ; 
       24 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       30 20 401 ; 
       31 21 401 ; 
       33 22 401 ; 
       34 23 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 26 401 ; 
       38 27 401 ; 
       39 28 401 ; 
       41 30 401 ; 
       42 31 401 ; 
       43 32 401 ; 
       44 33 401 ; 
       45 34 401 ; 
       46 35 401 ; 
       47 36 401 ; 
       48 37 401 ; 
       49 38 401 ; 
       50 39 401 ; 
       51 40 401 ; 
       52 41 401 ; 
       53 42 401 ; 
       54 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 130 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 132.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 135 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 40 -2 0 MPRFLG 0 ; 
       2 SCHEM 113.75 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 -10 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 22.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 27.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 30 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 32.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 35 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 37.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 40 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 42.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 45 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 20 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 61.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       21 SCHEM 91.25 -2 0 WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       24 SCHEM 10 -2 0 MPRFLG 0 ; 
       25 SCHEM 47.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 65 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 55 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 60 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 52.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 50 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 72.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 95 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 85 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 87.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 90 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 92.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 82.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 97.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 100 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 102.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 105 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 80 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 77.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 107.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 110 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 112.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 115 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 117.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 120 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       56 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       57 SCHEM 61.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       58 SCHEM 5 -8 0 MPRFLG 0 ; 
       59 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 52.74001 -12.05101 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 88.6073 -8.447124 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 119 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 116.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 114 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 111.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 109 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 95.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
