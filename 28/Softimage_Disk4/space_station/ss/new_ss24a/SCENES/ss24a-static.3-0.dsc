SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.35-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       static-light1_1.3-0 ROOT ; 
       static-light2_1.3-0 ROOT ; 
       static-light3_1.3-0 ROOT ; 
       static-light4_1.3-0 ROOT ; 
       static-light5_1.3-0 ROOT ; 
       static-light6_1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 44     
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       static-mat204.2-0 ; 
       static-mat205.2-0 ; 
       static-mat206.2-0 ; 
       static-mat207.2-0 ; 
       static-mat208.2-0 ; 
       static-mat209.2-0 ; 
       static-mat210.2-0 ; 
       static-mat268_1.2-0 ; 
       static-mat269.2-0 ; 
       static-mat274.2-0 ; 
       static-mat275.2-0 ; 
       static-mat276.2-0 ; 
       static-mat277.2-0 ; 
       static-mat278.2-0 ; 
       static-mat279.2-0 ; 
       static-mat280.2-0 ; 
       static-mat281.2-0 ; 
       static-mat282.2-0 ; 
       static-mat283.2-0 ; 
       static-mat284.2-0 ; 
       static-mat285.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss19a_1.1-0 ; 
       ss24a-ss24a.23-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       E:/Pete_Data2/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-static.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 42     
       new-t2d2_1.9-0 ; 
       plaza8_F-t2d38_1.4-0 ; 
       plaza8_F-t2d39_1.4-0 ; 
       plaza8_F-t2d40_1.4-0 ; 
       plaza8_F-t2d41_1.4-0 ; 
       plaza8_F-t2d42_1.4-0 ; 
       plaza8_F-t2d43_1.4-0 ; 
       plaza8_F-t2d44_1.4-0 ; 
       plaza8_F-t2d45_1.4-0 ; 
       plaza8_F-t2d46_1.4-0 ; 
       plaza8_F-t2d47_1.4-0 ; 
       plaza8_F-t2d48_1.4-0 ; 
       plaza8_F-t2d49_1.4-0 ; 
       plaza8_F-t2d50_1.4-0 ; 
       plaza8_F-t2d51_1.4-0 ; 
       plaza8_F-t2d52_1.4-0 ; 
       plaza8_F-t2d53_1.4-0 ; 
       plaza8_F-t2d54_1.4-0 ; 
       plaza8_F-t2d55_1.4-0 ; 
       plaza8_F-t2d56_1.4-0 ; 
       plaza8_F-t2d57_1.4-0 ; 
       plaza8_F-t2d58_1.4-0 ; 
       static-t2d196.2-0 ; 
       static-t2d197.2-0 ; 
       static-t2d198.2-0 ; 
       static-t2d199.2-0 ; 
       static-t2d200.2-0 ; 
       static-t2d201.2-0 ; 
       static-t2d260_1.2-0 ; 
       static-t2d261.2-0 ; 
       static-t2d268.2-0 ; 
       static-t2d269.2-0 ; 
       static-t2d270.2-0 ; 
       static-t2d271.2-0 ; 
       static-t2d272.2-0 ; 
       static-t2d273.2-0 ; 
       static-t2d274.2-0 ; 
       static-t2d275.2-0 ; 
       static-t2d276.2-0 ; 
       static-t2d277.2-0 ; 
       static-t2d278.2-0 ; 
       static-t2d279.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 10 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 0 110 ; 
       6 9 110 ; 
       7 6 110 ; 
       8 7 110 ; 
       10 11 110 ; 
       11 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 31 300 ; 
       1 42 300 ; 
       1 43 300 ; 
       2 32 300 ; 
       2 33 300 ; 
       2 34 300 ; 
       3 35 300 ; 
       3 36 300 ; 
       3 37 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       5 23 300 ; 
       5 24 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 27 300 ; 
       5 28 300 ; 
       5 29 300 ; 
       6 0 300 ; 
       6 30 300 ; 
       6 41 300 ; 
       10 1 300 ; 
       10 12 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       4 9 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 6 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -10 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 10 -2 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 95.1225 -6.188603 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
