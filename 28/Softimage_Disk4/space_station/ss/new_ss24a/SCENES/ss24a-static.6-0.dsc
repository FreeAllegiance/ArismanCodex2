SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.99-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       new-mat2_1.1-0 ; 
       plaza8_F-mat1_1.5-0 ; 
       plaza8_F-mat40_1.5-0 ; 
       plaza8_F-mat41_1.5-0 ; 
       plaza8_F-mat42_1.5-0 ; 
       plaza8_F-mat43_1.5-0 ; 
       plaza8_F-mat44_1.5-0 ; 
       plaza8_F-mat45_1.5-0 ; 
       plaza8_F-mat46_1.5-0 ; 
       plaza8_F-mat47_1.5-0 ; 
       plaza8_F-mat48_1.5-0 ; 
       plaza8_F-mat49_1.5-0 ; 
       plaza8_F-mat50_1.5-0 ; 
       plaza8_F-mat51_1.5-0 ; 
       plaza8_F-mat52_1.5-0 ; 
       plaza8_F-mat53_1.5-0 ; 
       plaza8_F-mat54_1.5-0 ; 
       plaza8_F-mat55_1.5-0 ; 
       plaza8_F-mat56_1.5-0 ; 
       plaza8_F-mat57_1.5-0 ; 
       plaza8_F-mat58_1.5-0 ; 
       plaza8_F-mat59_1.5-0 ; 
       plaza8_F-mat60_1.5-0 ; 
       static-mat204.4-0 ; 
       static-mat205.4-0 ; 
       static-mat206.4-0 ; 
       static-mat207.4-0 ; 
       static-mat208.4-0 ; 
       static-mat209.4-0 ; 
       static-mat210.4-0 ; 
       static-mat268_1.4-0 ; 
       static-mat269.4-0 ; 
       static-mat274.4-0 ; 
       static-mat275.4-0 ; 
       static-mat276.4-0 ; 
       static-mat277.4-0 ; 
       static-mat278.4-0 ; 
       static-mat279.4-0 ; 
       static-mat280.4-0 ; 
       static-mat281.4-0 ; 
       static-mat282.4-0 ; 
       static-mat283.4-0 ; 
       static-mat284.4-0 ; 
       static-mat285.4-0 ; 
       static-mat320.1-0 ; 
       static-mat321.1-0 ; 
       static-mat322.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       ss24a-cube1_1.3-0 ; 
       ss24a-cyl3.1-0 ; 
       ss24a-cyl3_1.3-0 ; 
       ss24a-fuselg_1.5-0 ; 
       ss24a-fuselg_4.1-0 ; 
       ss24a-fuselg_5.1-0 ; 
       ss24a-fuselg1_1.1-0 ; 
       ss24a-skin2_1.22-0 ; 
       ss24a-ss18_1.24-0 ; 
       ss24a-ss24a.55-0 ROOT ; 
       ss24a-tfuselg1_1.1-0 ; 
       ss24a-tfuselg2_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/bgrnd03 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/cwbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/new_ss24a/PICTURES/ss24a ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss24a-static.6-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 45     
       new-t2d2_1.14-0 ; 
       plaza8_F-t2d38_1.5-0 ; 
       plaza8_F-t2d39_1.5-0 ; 
       plaza8_F-t2d40_1.5-0 ; 
       plaza8_F-t2d41_1.5-0 ; 
       plaza8_F-t2d42_1.5-0 ; 
       plaza8_F-t2d43_1.5-0 ; 
       plaza8_F-t2d44_1.5-0 ; 
       plaza8_F-t2d45_1.5-0 ; 
       plaza8_F-t2d46_1.5-0 ; 
       plaza8_F-t2d47_1.5-0 ; 
       plaza8_F-t2d48_1.5-0 ; 
       plaza8_F-t2d49_1.5-0 ; 
       plaza8_F-t2d50_1.5-0 ; 
       plaza8_F-t2d51_1.5-0 ; 
       plaza8_F-t2d52_1.5-0 ; 
       plaza8_F-t2d53_1.5-0 ; 
       plaza8_F-t2d54_1.5-0 ; 
       plaza8_F-t2d55_1.5-0 ; 
       plaza8_F-t2d56_1.5-0 ; 
       plaza8_F-t2d57_1.5-0 ; 
       plaza8_F-t2d58_1.5-0 ; 
       static-t2d196.4-0 ; 
       static-t2d197.4-0 ; 
       static-t2d198.4-0 ; 
       static-t2d199.4-0 ; 
       static-t2d200.4-0 ; 
       static-t2d201.4-0 ; 
       static-t2d260_1.4-0 ; 
       static-t2d261.4-0 ; 
       static-t2d268.4-0 ; 
       static-t2d269.4-0 ; 
       static-t2d270.4-0 ; 
       static-t2d271.4-0 ; 
       static-t2d272.4-0 ; 
       static-t2d273.4-0 ; 
       static-t2d274.4-0 ; 
       static-t2d275.4-0 ; 
       static-t2d276.4-0 ; 
       static-t2d277.4-0 ; 
       static-t2d278.4-0 ; 
       static-t2d279.4-0 ; 
       static-t2d280.1-0 ; 
       static-t2d281.1-0 ; 
       static-t2d282.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 10 110 ; 
       4 7 110 ; 
       5 7 110 ; 
       6 0 110 ; 
       7 9 110 ; 
       8 7 110 ; 
       10 11 110 ; 
       11 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 31 300 ; 
       1 44 300 ; 
       1 45 300 ; 
       2 42 300 ; 
       2 43 300 ; 
       2 46 300 ; 
       3 32 300 ; 
       3 33 300 ; 
       3 34 300 ; 
       4 35 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       5 38 300 ; 
       5 39 300 ; 
       5 40 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       6 25 300 ; 
       6 26 300 ; 
       6 27 300 ; 
       6 28 300 ; 
       6 29 300 ; 
       7 0 300 ; 
       7 30 300 ; 
       7 41 300 ; 
       10 1 300 ; 
       10 12 300 ; 
       10 19 300 ; 
       10 20 300 ; 
       10 21 300 ; 
       10 22 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       11 6 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       11 13 300 ; 
       11 14 300 ; 
       11 15 300 ; 
       11 16 300 ; 
       11 17 300 ; 
       11 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 29 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       2 5 401 ; 
       3 7 401 ; 
       4 9 401 ; 
       5 1 401 ; 
       6 2 401 ; 
       7 3 401 ; 
       8 4 401 ; 
       9 6 401 ; 
       10 8 401 ; 
       11 10 401 ; 
       12 11 401 ; 
       13 12 401 ; 
       14 13 401 ; 
       15 14 401 ; 
       16 15 401 ; 
       17 16 401 ; 
       18 17 401 ; 
       19 18 401 ; 
       20 19 401 ; 
       21 20 401 ; 
       22 21 401 ; 
       24 22 401 ; 
       25 23 401 ; 
       26 24 401 ; 
       27 25 401 ; 
       28 26 401 ; 
       29 27 401 ; 
       30 28 401 ; 
       32 30 401 ; 
       33 31 401 ; 
       34 32 401 ; 
       35 33 401 ; 
       36 34 401 ; 
       37 35 401 ; 
       38 36 401 ; 
       39 37 401 ; 
       40 38 401 ; 
       41 39 401 ; 
       42 40 401 ; 
       43 41 401 ; 
       44 42 401 ; 
       45 43 401 ; 
       46 44 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 40.09258 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 53.53682 -4.94911 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 50.93814 -4.758428 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 37.59258 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 42.59258 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 45.09258 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 40.09258 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 41.34258 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 36.34258 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 58.84258 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 37.59258 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 37.59258 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49.09258 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 142.7638 -7.661158 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 39.09258 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39.09258 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39.09258 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 39.09258 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 39.09258 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 39.09258 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 49.09258 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.59258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.59258 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.59258 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 36.59258 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 41.59258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 41.59258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 41.59258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 44.09258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 44.09258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 44.09258 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 49.09258 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 84.09146 -8.564613 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 84.09146 -8.564613 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 66.16531 -6.292315 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 66.16531 -6.292315 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 84.09146 -8.564613 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 49.09258 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 39.09258 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 39.09258 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 39.09258 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 39.09258 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 39.09258 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 39.09258 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 49.09258 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 41.59258 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 36.59258 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 36.59258 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.59258 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 41.59258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 41.59258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 41.59258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 44.09258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 44.09258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 44.09258 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 49.09258 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 84.09146 -10.56461 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 84.09146 -10.56461 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 66.16531 -8.292315 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 66.16531 -8.292315 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 84.09146 -10.56461 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
