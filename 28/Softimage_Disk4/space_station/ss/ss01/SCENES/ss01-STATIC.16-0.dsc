SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss01-ss01_2.20-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.37-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       dowager_sPtL-inf_light5_1_2.36-0 ROOT ; 
       dowager_sPtL-inf_light5_1_2_2.1-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.36-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2_2.1-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.36-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 64     
       add_frames-front_of_garages1_1.1-0 ; 
       add_frames-nose_white-center.1-13.3-0 ; 
       add_frames-nose_white-center.1-14.3-0 ; 
       add_frames-nose_white-center.1-15.3-0 ; 
       add_frames-nose_white-center.1-5.3-0 ; 
       add_frames-nose_white-center.1-7.3-0 ; 
       add_frames-nose_white-center.1-9.3-0 ; 
       add_frames-side_panels1_1.1-0 ; 
       add_frames-starbord_green-right.1-0.3-0 ; 
       add_frames-starbord_green-right.1-3.3-0 ; 
       add_frames-starbord_green-right.1-4.3-0 ; 
       add_frames-starbord_green-right.1-42.3-0 ; 
       add_frames-starbord_green-right.1-43.3-0 ; 
       add_frames-starbord_green-right.1-44.3-0 ; 
       add_frames-starbord_green-right.1-45.3-0 ; 
       add_frames-starbord_green-right.1-5.3-0 ; 
       add_frames-starbord_green-right.1-50.3-0 ; 
       add_frames-starbord_green-right.1-51.3-0 ; 
       add_frames-starbord_green-right.1-52.3-0 ; 
       add_frames-starbord_green-right.1-53.3-0 ; 
       add_frames-starbord_green-right.1-54.3-0 ; 
       add_frames-starbord_green-right.1-55.3-0 ; 
       add_frames-starbord_green-right.1-56.3-0 ; 
       add_frames-starbord_green-right.1-57.3-0 ; 
       add_frames-starbord_green-right.1-58.3-0 ; 
       add_frames-starbord_green-right.1-59.3-0 ; 
       add_frames-starbord_green-right.1-60.3-0 ; 
       add_frames-starbord_green-right.1-61.3-0 ; 
       dowager_sPtL-mat1_1.1-0 ; 
       dowager_sPtL-mat14_1.1-0 ; 
       dowager_sPtL-mat2_1.1-0 ; 
       dowager_sPtL-mat3_1.1-0 ; 
       dowager_sPtL-mat4_1.1-0 ; 
       dowager_sPtL-mat41.5-0 ; 
       dowager_sPtL-mat41_1.1-0 ; 
       dowager_sPtL-mat42.5-0 ; 
       dowager_sPtL-mat42_1.1-0 ; 
       dowager_sPtL-mat43.5-0 ; 
       dowager_sPtL-mat43_1.1-0 ; 
       dowager_sPtL-mat44.5-0 ; 
       dowager_sPtL-mat44_1.1-0 ; 
       dowager_sPtL-mat51_1.1-0 ; 
       dowager_sPtL-mat52_1.1-0 ; 
       dowager_sPtL-mat53_1.1-0 ; 
       dowager_sPtL-mat55_1.1-0 ; 
       dowager_sPtL-mat57_1.1-0 ; 
       dowager_sPtL-mat60_1.1-0 ; 
       dowager_sPtL-mat62_1.1-0 ; 
       dowager_sPtL-mat65_1.1-0 ; 
       dowager_sPtL-mat66_1.1-0 ; 
       dowager_sPtL-mat67_1.1-0 ; 
       dowager_sPtL-mat7_1.1-0 ; 
       dowager_sPtL-mat86.7-0 ; 
       dowager_sPtL-mat86_1.1-0 ; 
       dowager_sPtL-mat87.5-0 ; 
       dowager_sPtL-mat88.5-0 ; 
       dowager_sPtL-mat89.5-0 ; 
       dowager_sPtL-mat90.7-0 ; 
       dowager_sPtL-mat90_1.1-0 ; 
       dowager_sPtL-mat91.7-0 ; 
       dowager_sPtL-mat91_1.1-0 ; 
       Static-mat100.2-0 ; 
       Static-mat101.2-0 ; 
       Static-mat102.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 62     
       ss01-all_hangars.1-0 ; 
       ss01-bcorrdr.1-0 ; 
       ss01-bcorrdr_3.1-0 ; 
       ss01-bmerge3.2-0 ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr0.1-0 ; 
       ss01-corrdr2_1.2-0 ; 
       ss01-corrdr2_9.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg4.1-0 ; 
       ss01-garage1A.1-0 ; 
       ss01-garage1B.1-0 ; 
       ss01-garage1C.1-0 ; 
       ss01-garage1D.1-0 ; 
       ss01-garage1E.1-0 ; 
       ss01-garage2A.1-0 ; 
       ss01-garage2B.1-0 ; 
       ss01-garage2C.1-0 ; 
       ss01-garage2D.1-0 ; 
       ss01-garage2E.1-0 ; 
       ss01-hangar.1-0 ; 
       ss01-hangar1.1-0 ; 
       ss01-launch1.1-0 ; 
       ss01-launch2.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-platfrm2.1-0 ; 
       ss01-ss01_2.26-0 ROOT ; 
       ss01-SS05a.1-0 ; 
       ss01-SS05b.1-0 ; 
       ss01-SS05c.1-0 ; 
       ss01-SS2b1.1-0 ; 
       ss01-SS2b2.1-0 ; 
       ss01-SS2b3.1-0 ; 
       ss01-SS2b4.1-0 ; 
       ss01-SS2c1.1-0 ; 
       ss01-SS2c2.1-0 ; 
       ss01-SS2c3.1-0 ; 
       ss01-SS2c4.1-0 ; 
       ss01-SS5a1.1-0 ; 
       ss01-SS5a2.1-0 ; 
       ss01-SS5a3.1-0 ; 
       ss01-SS5a4.1-0 ; 
       ss01-SS5b1.1-0 ; 
       ss01-SS5b2.1-0 ; 
       ss01-SS5b3.1-0 ; 
       ss01-SS5b4.1-0 ; 
       ss01-SS5c1.1-0 ; 
       ss01-SS5c2.1-0 ; 
       ss01-SS5c3.1-0 ; 
       ss01-SS5c4.1-0 ; 
       ss01-SSc1.6-0 ; 
       ss01-SSc2.1-0 ; 
       ss01-SSf1.9-0 ; 
       ss01-SSf2.1-0 ; 
       ss01-SSf3.1-0 ; 
       ss01-SSf4.1-0 ; 
       ss01-turwepemt1.1-0 ; 
       ss01-turwepemt2.1-0 ; 
       ss01-turwepemt3.1-0 ; 
       ss01-turwepemt4.1-0 ; 
       ss01-ww.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/ss01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss01-STATIC.16-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_frames-t2d65_1.1-0 ; 
       add_frames-t2d66_1.1-0 ; 
       dowager_sPtL-t2d1_1.1-0 ; 
       dowager_sPtL-t2d11_1.1-0 ; 
       dowager_sPtL-t2d2_1.1-0 ; 
       dowager_sPtL-t2d31.8-0 ; 
       dowager_sPtL-t2d31_1.1-0 ; 
       dowager_sPtL-t2d32.8-0 ; 
       dowager_sPtL-t2d32_1.1-0 ; 
       dowager_sPtL-t2d33.8-0 ; 
       dowager_sPtL-t2d33_1.1-0 ; 
       dowager_sPtL-t2d40.14-0 ; 
       dowager_sPtL-t2d41.14-0 ; 
       dowager_sPtL-t2d42_1.1-0 ; 
       dowager_sPtL-t2d43_1.1-0 ; 
       dowager_sPtL-t2d46_1.1-0 ; 
       dowager_sPtL-t2d5.14-0 ; 
       dowager_sPtL-t2d50_1.1-0 ; 
       dowager_sPtL-t2d51_1.1-0 ; 
       dowager_sPtL-t2d59.11-0 ; 
       dowager_sPtL-t2d59_1.1-0 ; 
       dowager_sPtL-t2d63.11-0 ; 
       dowager_sPtL-t2d63_1.1-0 ; 
       Static-t2d67.3-0 ; 
       Static-t2d68.3-0 ; 
       Static-t2d69.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 5 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 25 110 ; 
       6 25 110 ; 
       8 27 110 ; 
       9 25 110 ; 
       10 25 110 ; 
       25 27 110 ; 
       26 25 110 ; 
       0 27 110 ; 
       2 1 110 ; 
       7 6 110 ; 
       11 21 110 ; 
       12 21 110 ; 
       13 21 110 ; 
       14 21 110 ; 
       15 21 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 22 110 ; 
       20 22 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 0 110 ; 
       61 7 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 2 110 ; 
       31 61 110 ; 
       32 61 110 ; 
       33 61 110 ; 
       34 61 110 ; 
       35 7 110 ; 
       36 7 110 ; 
       37 7 110 ; 
       38 7 110 ; 
       39 28 110 ; 
       40 28 110 ; 
       41 28 110 ; 
       42 28 110 ; 
       43 29 110 ; 
       44 29 110 ; 
       45 29 110 ; 
       46 29 110 ; 
       47 30 110 ; 
       48 30 110 ; 
       49 30 110 ; 
       50 30 110 ; 
       51 27 110 ; 
       52 27 110 ; 
       53 27 110 ; 
       54 27 110 ; 
       55 27 110 ; 
       56 27 110 ; 
       57 27 110 ; 
       58 27 110 ; 
       59 27 110 ; 
       60 27 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 34 300 ; 
       1 36 300 ; 
       1 38 300 ; 
       1 40 300 ; 
       3 61 300 ; 
       3 62 300 ; 
       3 63 300 ; 
       4 29 300 ; 
       6 53 300 ; 
       6 58 300 ; 
       6 60 300 ; 
       8 28 300 ; 
       8 30 300 ; 
       8 31 300 ; 
       9 46 300 ; 
       9 47 300 ; 
       10 48 300 ; 
       10 49 300 ; 
       10 50 300 ; 
       25 32 300 ; 
       25 51 300 ; 
       25 41 300 ; 
       25 42 300 ; 
       25 0 300 ; 
       25 7 300 ; 
       26 43 300 ; 
       26 44 300 ; 
       26 45 300 ; 
       2 33 300 ; 
       2 35 300 ; 
       2 37 300 ; 
       2 39 300 ; 
       7 52 300 ; 
       7 54 300 ; 
       7 55 300 ; 
       7 56 300 ; 
       7 57 300 ; 
       7 59 300 ; 
       31 14 300 ; 
       32 13 300 ; 
       33 11 300 ; 
       34 12 300 ; 
       35 9 300 ; 
       36 10 300 ; 
       37 15 300 ; 
       38 8 300 ; 
       39 24 300 ; 
       40 25 300 ; 
       41 26 300 ; 
       42 27 300 ; 
       43 20 300 ; 
       44 21 300 ; 
       45 22 300 ; 
       46 23 300 ; 
       47 16 300 ; 
       48 17 300 ; 
       49 18 300 ; 
       50 19 300 ; 
       51 4 300 ; 
       52 6 300 ; 
       53 5 300 ; 
       54 1 300 ; 
       55 2 300 ; 
       56 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       27 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       29 3 401 ; 
       30 2 401 ; 
       31 4 401 ; 
       36 6 401 ; 
       38 8 401 ; 
       40 10 401 ; 
       41 11 401 ; 
       42 12 401 ; 
       44 13 401 ; 
       45 14 401 ; 
       47 15 401 ; 
       49 17 401 ; 
       50 18 401 ; 
       51 16 401 ; 
       58 19 401 ; 
       60 21 401 ; 
       61 23 401 ; 
       62 24 401 ; 
       63 25 401 ; 
       35 5 401 ; 
       37 7 401 ; 
       39 9 401 ; 
       57 20 401 ; 
       59 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 0 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 7.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 10 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 12.5 -18 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       3 SCHEM 0 -8 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 13.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 60 -8 0 MPRFLG 0 ; 
       10 SCHEM 55 -8 0 MPRFLG 0 ; 
       25 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       26 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 57.5 -4 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 91.25 -6 0 MPRFLG 0 ; 
       2 SCHEM 38.75 -12 0 MPRFLG 0 ; 
       7 SCHEM 13.75 -10 0 MPRFLG 0 ; 
       11 SCHEM 77.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 80 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 82.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 85 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 87.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 90 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 92.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 95 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 97.5 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 100 -10 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 82.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 95 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 105 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       61 SCHEM 18.75 -12 0 MPRFLG 0 ; 
       28 SCHEM 48.75 -14 0 MPRFLG 0 ; 
       29 SCHEM 28.75 -14 0 MPRFLG 0 ; 
       30 SCHEM 38.75 -14 0 MPRFLG 0 ; 
       31 SCHEM 22.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 15 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 17.5 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 20 -14 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 5 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       36 SCHEM 7.5 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       37 SCHEM 10 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       38 SCHEM 12.5 -12 0 WIRECOL 8 7 MPRFLG 0 ; 
       39 SCHEM 52.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 45 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 47.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 50 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 32.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 25 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 27.5 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 30 -16 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 42.5 -16 0 WIRECOL 8 7 MPRFLG 0 ; 
       48 SCHEM 35 -16 0 WIRECOL 8 7 MPRFLG 0 ; 
       49 SCHEM 37.5 -16 0 WIRECOL 8 7 MPRFLG 0 ; 
       50 SCHEM 40 -16 0 WIRECOL 8 7 MPRFLG 0 ; 
       51 SCHEM 70 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 72.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 75 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 62.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 65 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 67.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 107.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 59.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 62.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 61.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 56.75 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 58.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 56.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 59 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 55.25 -8 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 69 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 71.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 19 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 41.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 36.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 24 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 26.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 29 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 46.5 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 49 -18 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 58.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 61.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 54 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 55.75 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 57.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 56.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 59 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 54.25 -10 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 54 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 24 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 116.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
