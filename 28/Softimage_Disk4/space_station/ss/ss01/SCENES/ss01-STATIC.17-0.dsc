SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.41-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       dowager_sPtL-inf_light5_1_2.40-0 ROOT ; 
       dowager_sPtL-inf_light5_1_2_2.5-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.40-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2_2.5-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.40-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2_2.5-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 36     
       add_frames-front_of_garages1_1.1-0 ; 
       add_frames-side_panels1_1.1-0 ; 
       dowager_sPtL-mat1_1.1-0 ; 
       dowager_sPtL-mat14_1.1-0 ; 
       dowager_sPtL-mat2_1.1-0 ; 
       dowager_sPtL-mat3_1.1-0 ; 
       dowager_sPtL-mat4_1.1-0 ; 
       dowager_sPtL-mat51_1.1-0 ; 
       dowager_sPtL-mat52_1.1-0 ; 
       dowager_sPtL-mat53_1.1-0 ; 
       dowager_sPtL-mat55_1.1-0 ; 
       dowager_sPtL-mat57_1.1-0 ; 
       dowager_sPtL-mat60_1.1-0 ; 
       dowager_sPtL-mat62_1.1-0 ; 
       dowager_sPtL-mat65_1.1-0 ; 
       dowager_sPtL-mat66_1.1-0 ; 
       dowager_sPtL-mat67_1.1-0 ; 
       dowager_sPtL-mat7_1.1-0 ; 
       dowager_sPtL-mat86_1.1-0 ; 
       dowager_sPtL-mat90_1.1-0 ; 
       dowager_sPtL-mat91_1.1-0 ; 
       Static-mat100.2-0 ; 
       Static-mat101.2-0 ; 
       Static-mat102.2-0 ; 
       STATIC-mat86_2.1-0 ; 
       STATIC-mat86_3.1-0 ; 
       STATIC-mat86_4.1-0 ; 
       STATIC-mat86_5.1-0 ; 
       STATIC-mat90_2.1-0 ; 
       STATIC-mat90_3.1-0 ; 
       STATIC-mat90_4.1-0 ; 
       STATIC-mat90_5.1-0 ; 
       STATIC-mat91_2.1-0 ; 
       STATIC-mat91_3.1-0 ; 
       STATIC-mat91_4.1-0 ; 
       STATIC-mat91_5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       ss01-bmerge3.2-0 ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr2_1.2-0 ; 
       ss01-corrdr2_10.1-0 ; 
       ss01-corrdr2_12.1-0 ; 
       ss01-corrdr2_14.1-0 ; 
       ss01-corrdr2_16.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg4.1-0 ; 
       ss01-null2.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-platfrm2.1-0 ; 
       ss01-ss01_2.30-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/ss01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss01-STATIC.17-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_frames-t2d65_1.2-0 ; 
       add_frames-t2d66_1.2-0 ; 
       dowager_sPtL-t2d1_1.1-0 ; 
       dowager_sPtL-t2d11_1.1-0 ; 
       dowager_sPtL-t2d2_1.1-0 ; 
       dowager_sPtL-t2d40.15-0 ; 
       dowager_sPtL-t2d41.15-0 ; 
       dowager_sPtL-t2d42_1.1-0 ; 
       dowager_sPtL-t2d43_1.1-0 ; 
       dowager_sPtL-t2d46_1.1-0 ; 
       dowager_sPtL-t2d5.15-0 ; 
       dowager_sPtL-t2d50_1.1-0 ; 
       dowager_sPtL-t2d51_1.1-0 ; 
       dowager_sPtL-t2d59.11-0 ; 
       dowager_sPtL-t2d63.11-0 ; 
       Static-t2d67.3-0 ; 
       Static-t2d68.3-0 ; 
       Static-t2d69.3-0 ; 
       STATIC-t2d70.2-0 ; 
       STATIC-t2d71.2-0 ; 
       STATIC-t2d72.2-0 ; 
       STATIC-t2d73.2-0 ; 
       STATIC-t2d74.2-0 ; 
       STATIC-t2d75.2-0 ; 
       STATIC-t2d76.1-0 ; 
       STATIC-t2d77.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       10 11 110 ; 
       2 10 110 ; 
       3 10 110 ; 
       7 13 110 ; 
       8 11 110 ; 
       9 11 110 ; 
       11 13 110 ; 
       4 10 110 ; 
       12 11 110 ; 
       5 10 110 ; 
       6 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 21 300 ; 
       0 22 300 ; 
       0 23 300 ; 
       1 3 300 ; 
       2 18 300 ; 
       2 19 300 ; 
       2 20 300 ; 
       3 24 300 ; 
       3 28 300 ; 
       3 32 300 ; 
       7 2 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       11 6 300 ; 
       11 17 300 ; 
       11 7 300 ; 
       11 8 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       4 25 300 ; 
       4 29 300 ; 
       4 33 300 ; 
       12 9 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       5 26 300 ; 
       5 30 300 ; 
       5 34 300 ; 
       6 27 300 ; 
       6 31 300 ; 
       6 35 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       13 9 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 10 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       28 18 401 ; 
       32 19 401 ; 
       29 20 401 ; 
       33 21 401 ; 
       30 22 401 ; 
       34 23 401 ; 
       31 24 401 ; 
       35 25 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 10 -4 0 MPRFLG 0 ; 
       13 SCHEM 13.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 20 -6 0 MPRFLG 0 ; 
       6 SCHEM 25 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 48.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 45.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 48.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
