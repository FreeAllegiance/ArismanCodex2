SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.61-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 66     
       add_frames-front_of_garages1_1.1-0 ; 
       add_frames-side_panels1_1.1-0 ; 
       dowager_sPtL-mat1_1.1-0 ; 
       dowager_sPtL-mat14_1.1-0 ; 
       dowager_sPtL-mat2_1.1-0 ; 
       dowager_sPtL-mat3_1.1-0 ; 
       dowager_sPtL-mat4_1.1-0 ; 
       dowager_sPtL-mat51_1.1-0 ; 
       dowager_sPtL-mat52_1.1-0 ; 
       dowager_sPtL-mat53_1.1-0 ; 
       dowager_sPtL-mat55_1.1-0 ; 
       dowager_sPtL-mat57_1.1-0 ; 
       dowager_sPtL-mat60_1.1-0 ; 
       dowager_sPtL-mat62_1.1-0 ; 
       dowager_sPtL-mat65_1.1-0 ; 
       dowager_sPtL-mat66_1.1-0 ; 
       dowager_sPtL-mat67_1.1-0 ; 
       dowager_sPtL-mat7_1.1-0 ; 
       dowager_sPtL-mat86.7-0 ; 
       dowager_sPtL-mat86_1.1-0 ; 
       dowager_sPtL-mat87.5-0 ; 
       dowager_sPtL-mat88.5-0 ; 
       dowager_sPtL-mat89.5-0 ; 
       dowager_sPtL-mat90.7-0 ; 
       dowager_sPtL-mat90_1.1-0 ; 
       dowager_sPtL-mat91.7-0 ; 
       dowager_sPtL-mat91_1.1-0 ; 
       ROTATE-mat103.1-0 ; 
       ROTATE-mat104.1-0 ; 
       ROTATE-mat105.1-0 ; 
       ROTATE-mat106.1-0 ; 
       ROTATE-mat107.1-0 ; 
       ROTATE-mat108.1-0 ; 
       ROTATE-mat109.1-0 ; 
       ROTATE-mat110.1-0 ; 
       ROTATE-mat111.1-0 ; 
       ROTATE-mat112.1-0 ; 
       ROTATE-mat113.1-0 ; 
       ROTATE-mat114.1-0 ; 
       ROTATE-mat115.1-0 ; 
       ROTATE-mat116.1-0 ; 
       ROTATE-mat117.1-0 ; 
       ROTATE-mat118.1-0 ; 
       ROTATE-mat119.1-0 ; 
       ROTATE-mat120.1-0 ; 
       ROTATE-mat121.1-0 ; 
       ROTATE-mat122.1-0 ; 
       ROTATE-mat123.1-0 ; 
       ROTATE-mat124.1-0 ; 
       ROTATE-mat125.1-0 ; 
       ROTATE-mat126.1-0 ; 
       ROTATE-mat86_2.1-0 ; 
       ROTATE-mat86_3.1-0 ; 
       ROTATE-mat86_4.1-0 ; 
       ROTATE-mat86_5.1-0 ; 
       ROTATE-mat90_2.1-0 ; 
       ROTATE-mat90_3.1-0 ; 
       ROTATE-mat90_4.1-0 ; 
       ROTATE-mat90_5.1-0 ; 
       ROTATE-mat91_2.1-0 ; 
       ROTATE-mat91_3.1-0 ; 
       ROTATE-mat91_4.1-0 ; 
       ROTATE-mat91_5.1-0 ; 
       Static-mat100.2-0 ; 
       Static-mat101.2-0 ; 
       Static-mat102.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       ss01-bmerge3.2-0 ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr2_1.2-0 ; 
       ss01-corrdr2_10.1-0 ; 
       ss01-corrdr2_11.1-0 ; 
       ss01-corrdr2_12.1-0 ; 
       ss01-corrdr2_13.1-0 ; 
       ss01-corrdr2_14.1-0 ; 
       ss01-corrdr2_15.1-0 ; 
       ss01-corrdr2_16.1-0 ; 
       ss01-corrdr2_17.1-0 ; 
       ss01-corrdr2_9.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg4.1-0 ; 
       ss01-null2.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-platfrm2.1-0 ; 
       ss01-ss01_2.40-0 ROOT ; 
       ss01-ww.1-0 ; 
       ss01-ww1.1-0 ; 
       ss01-ww2.1-0 ; 
       ss01-ww3.1-0 ; 
       ss01-ww4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage_Archive/Softimage_Disk4/space_station/ss/ss01/PICTURES/rixbay ; 
       //research/root/federation/shared_art_files/SoftImage_Archive/Softimage_Disk4/space_station/ss/ss01/PICTURES/ss01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss01-STATIC.20-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_frames-t2d65_1.4-0 ; 
       add_frames-t2d66_1.4-0 ; 
       dowager_sPtL-t2d1_1.2-0 ; 
       dowager_sPtL-t2d11_1.1-0 ; 
       dowager_sPtL-t2d2_1.2-0 ; 
       dowager_sPtL-t2d40.17-0 ; 
       dowager_sPtL-t2d41.17-0 ; 
       dowager_sPtL-t2d42_1.1-0 ; 
       dowager_sPtL-t2d43_1.1-0 ; 
       dowager_sPtL-t2d46_1.1-0 ; 
       dowager_sPtL-t2d5.17-0 ; 
       dowager_sPtL-t2d50_1.1-0 ; 
       dowager_sPtL-t2d51_1.1-0 ; 
       dowager_sPtL-t2d59.11-0 ; 
       dowager_sPtL-t2d59_1.1-0 ; 
       dowager_sPtL-t2d63.11-0 ; 
       dowager_sPtL-t2d63_1.1-0 ; 
       ROTATE-t2d59_2.1-0 ; 
       ROTATE-t2d59_3.1-0 ; 
       ROTATE-t2d59_4.1-0 ; 
       ROTATE-t2d59_5.1-0 ; 
       ROTATE-t2d63_2.1-0 ; 
       ROTATE-t2d63_3.1-0 ; 
       ROTATE-t2d63_4.1-0 ; 
       ROTATE-t2d63_5.1-0 ; 
       ROTATE-t2d70.1-0 ; 
       ROTATE-t2d71.1-0 ; 
       ROTATE-t2d72.1-0 ; 
       ROTATE-t2d73.1-0 ; 
       ROTATE-t2d74.1-0 ; 
       ROTATE-t2d75.1-0 ; 
       ROTATE-t2d76.1-0 ; 
       ROTATE-t2d77.1-0 ; 
       Static-t2d67.3-0 ; 
       Static-t2d68.3-0 ; 
       Static-t2d69.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 12 110 ; 
       1 12 110 ; 
       2 15 110 ; 
       3 15 110 ; 
       4 3 110 ; 
       5 15 110 ; 
       6 5 110 ; 
       7 15 110 ; 
       8 7 110 ; 
       9 15 110 ; 
       10 9 110 ; 
       11 2 110 ; 
       12 18 110 ; 
       13 16 110 ; 
       14 16 110 ; 
       15 16 110 ; 
       16 18 110 ; 
       17 16 110 ; 
       19 11 110 ; 
       20 4 110 ; 
       21 6 110 ; 
       22 8 110 ; 
       23 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 63 300 ; 
       0 64 300 ; 
       0 65 300 ; 
       1 3 300 ; 
       2 19 300 ; 
       2 24 300 ; 
       2 26 300 ; 
       3 51 300 ; 
       3 55 300 ; 
       3 59 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 29 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       4 32 300 ; 
       5 52 300 ; 
       5 56 300 ; 
       5 60 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       6 38 300 ; 
       7 53 300 ; 
       7 57 300 ; 
       7 61 300 ; 
       8 39 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       8 42 300 ; 
       8 43 300 ; 
       8 44 300 ; 
       9 54 300 ; 
       9 58 300 ; 
       9 62 300 ; 
       10 45 300 ; 
       10 46 300 ; 
       10 47 300 ; 
       10 48 300 ; 
       10 49 300 ; 
       10 50 300 ; 
       11 18 300 ; 
       11 20 300 ; 
       11 21 300 ; 
       11 22 300 ; 
       11 23 300 ; 
       11 25 300 ; 
       12 2 300 ; 
       12 4 300 ; 
       12 5 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       14 14 300 ; 
       14 15 300 ; 
       14 16 300 ; 
       16 6 300 ; 
       16 17 300 ; 
       16 7 300 ; 
       16 8 300 ; 
       16 0 300 ; 
       16 1 300 ; 
       17 9 300 ; 
       17 10 300 ; 
       17 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       3 3 401 ; 
       4 2 401 ; 
       5 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       13 9 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 10 401 ; 
       23 14 401 ; 
       24 13 401 ; 
       25 16 401 ; 
       26 15 401 ; 
       31 17 401 ; 
       32 21 401 ; 
       37 18 401 ; 
       38 22 401 ; 
       43 19 401 ; 
       44 23 401 ; 
       49 20 401 ; 
       50 24 401 ; 
       55 25 401 ; 
       56 27 401 ; 
       57 29 401 ; 
       58 31 401 ; 
       59 26 401 ; 
       60 28 401 ; 
       61 30 401 ; 
       62 32 401 ; 
       63 33 401 ; 
       64 34 401 ; 
       65 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 20 -6 0 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 MPRFLG 0 ; 
       9 SCHEM 25 -6 0 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 MPRFLG 0 ; 
       11 SCHEM 15 -8 0 MPRFLG 0 ; 
       12 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 20 -4 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 10 -4 0 MPRFLG 0 ; 
       18 SCHEM 13.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 15 -10 0 MPRFLG 0 ; 
       20 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       22 SCHEM 20 -10 0 MPRFLG 0 ; 
       23 SCHEM 25 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 49.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 52.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 51.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 46.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 48.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 45.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 24 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 26.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 48.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 45.75 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 47.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44.25 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 24 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 26.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 24 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 26.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
