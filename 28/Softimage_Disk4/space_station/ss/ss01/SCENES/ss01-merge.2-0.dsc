SDSC3.81
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss01-ss01_2.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.21-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       dowager_sPtL-inf_light5_1_2.20-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.20-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 134     
       add_frames-front_of_garages1.2-0 ; 
       add_frames-nose_white-center.1-10.2-0 ; 
       add_frames-nose_white-center.1-11.2-0 ; 
       add_frames-nose_white-center.1-12.2-0 ; 
       add_frames-nose_white-center.1-13.2-0 ; 
       add_frames-nose_white-center.1-14.2-0 ; 
       add_frames-nose_white-center.1-15.2-0 ; 
       add_frames-nose_white-center.1-5.2-0 ; 
       add_frames-nose_white-center.1-6.2-0 ; 
       add_frames-nose_white-center.1-7.2-0 ; 
       add_frames-nose_white-center.1-9.2-0 ; 
       add_frames-side_panels1.2-0 ; 
       add_frames-starbord_green-right.1-0.2-0 ; 
       add_frames-starbord_green-right.1-11.2-0 ; 
       add_frames-starbord_green-right.1-12.2-0 ; 
       add_frames-starbord_green-right.1-13.2-0 ; 
       add_frames-starbord_green-right.1-14.2-0 ; 
       add_frames-starbord_green-right.1-15.2-0 ; 
       add_frames-starbord_green-right.1-16.2-0 ; 
       add_frames-starbord_green-right.1-17.2-0 ; 
       add_frames-starbord_green-right.1-18.2-0 ; 
       add_frames-starbord_green-right.1-19.2-0 ; 
       add_frames-starbord_green-right.1-20.2-0 ; 
       add_frames-starbord_green-right.1-21.2-0 ; 
       add_frames-starbord_green-right.1-22.2-0 ; 
       add_frames-starbord_green-right.1-23.2-0 ; 
       add_frames-starbord_green-right.1-24.2-0 ; 
       add_frames-starbord_green-right.1-25.2-0 ; 
       add_frames-starbord_green-right.1-26.2-0 ; 
       add_frames-starbord_green-right.1-27.2-0 ; 
       add_frames-starbord_green-right.1-28.2-0 ; 
       add_frames-starbord_green-right.1-29.2-0 ; 
       add_frames-starbord_green-right.1-3.2-0 ; 
       add_frames-starbord_green-right.1-30.2-0 ; 
       add_frames-starbord_green-right.1-31.2-0 ; 
       add_frames-starbord_green-right.1-32.2-0 ; 
       add_frames-starbord_green-right.1-33.2-0 ; 
       add_frames-starbord_green-right.1-34.2-0 ; 
       add_frames-starbord_green-right.1-35.2-0 ; 
       add_frames-starbord_green-right.1-36.2-0 ; 
       add_frames-starbord_green-right.1-37.2-0 ; 
       add_frames-starbord_green-right.1-38.2-0 ; 
       add_frames-starbord_green-right.1-39.2-0 ; 
       add_frames-starbord_green-right.1-4.2-0 ; 
       add_frames-starbord_green-right.1-40.2-0 ; 
       add_frames-starbord_green-right.1-41.2-0 ; 
       add_frames-starbord_green-right.1-42.2-0 ; 
       add_frames-starbord_green-right.1-43.2-0 ; 
       add_frames-starbord_green-right.1-44.2-0 ; 
       add_frames-starbord_green-right.1-45.2-0 ; 
       add_frames-starbord_green-right.1-46.2-0 ; 
       add_frames-starbord_green-right.1-47.2-0 ; 
       add_frames-starbord_green-right.1-48.2-0 ; 
       add_frames-starbord_green-right.1-49.2-0 ; 
       add_frames-starbord_green-right.1-5.2-0 ; 
       add_frames-starbord_green-right.1-50.2-0 ; 
       add_frames-starbord_green-right.1-51.2-0 ; 
       add_frames-starbord_green-right.1-52.2-0 ; 
       add_frames-starbord_green-right.1-53.2-0 ; 
       add_frames-starbord_green-right.1-54.2-0 ; 
       add_frames-starbord_green-right.1-55.2-0 ; 
       add_frames-starbord_green-right.1-56.2-0 ; 
       add_frames-starbord_green-right.1-57.2-0 ; 
       add_frames-starbord_green-right.1-58.2-0 ; 
       add_frames-starbord_green-right.1-59.2-0 ; 
       add_frames-starbord_green-right.1-6.2-0 ; 
       add_frames-starbord_green-right.1-60.2-0 ; 
       add_frames-starbord_green-right.1-61.2-0 ; 
       add_frames-starbord_green-right.1-62.2-0 ; 
       add_frames-starbord_green-right.1-7.2-0 ; 
       add_frames-starbord_green-right.1-8.2-0 ; 
       add_frames-starbord_green-right.1-9.2-0 ; 
       dowager_sPtL-mat1.4-0 ; 
       dowager_sPtL-mat14.4-0 ; 
       dowager_sPtL-mat15.4-0 ; 
       dowager_sPtL-mat16.4-0 ; 
       dowager_sPtL-mat17.4-0 ; 
       dowager_sPtL-mat18.4-0 ; 
       dowager_sPtL-mat19.4-0 ; 
       dowager_sPtL-mat2.4-0 ; 
       dowager_sPtL-mat20.4-0 ; 
       dowager_sPtL-mat21.4-0 ; 
       dowager_sPtL-mat22.4-0 ; 
       dowager_sPtL-mat23.4-0 ; 
       dowager_sPtL-mat3.4-0 ; 
       dowager_sPtL-mat4.6-0 ; 
       dowager_sPtL-mat41.4-0 ; 
       dowager_sPtL-mat42.4-0 ; 
       dowager_sPtL-mat43.4-0 ; 
       dowager_sPtL-mat44.4-0 ; 
       dowager_sPtL-mat47.4-0 ; 
       dowager_sPtL-mat51.6-0 ; 
       dowager_sPtL-mat52.6-0 ; 
       dowager_sPtL-mat53.4-0 ; 
       dowager_sPtL-mat55.4-0 ; 
       dowager_sPtL-mat57.4-0 ; 
       dowager_sPtL-mat60.4-0 ; 
       dowager_sPtL-mat62.4-0 ; 
       dowager_sPtL-mat63.4-0 ; 
       dowager_sPtL-mat64.4-0 ; 
       dowager_sPtL-mat65.4-0 ; 
       dowager_sPtL-mat66.4-0 ; 
       dowager_sPtL-mat67.4-0 ; 
       dowager_sPtL-mat68.4-0 ; 
       dowager_sPtL-mat69.4-0 ; 
       dowager_sPtL-mat7.6-0 ; 
       dowager_sPtL-mat70.4-0 ; 
       dowager_sPtL-mat72.4-0 ; 
       dowager_sPtL-mat73.4-0 ; 
       dowager_sPtL-mat74.4-0 ; 
       dowager_sPtL-mat76.4-0 ; 
       dowager_sPtL-mat77.4-0 ; 
       dowager_sPtL-mat79.4-0 ; 
       dowager_sPtL-mat80.4-0 ; 
       dowager_sPtL-mat81.4-0 ; 
       dowager_sPtL-mat82.4-0 ; 
       dowager_sPtL-mat83.4-0 ; 
       dowager_sPtL-mat84.4-0 ; 
       dowager_sPtL-mat85.4-0 ; 
       dowager_sPtL-mat86.4-0 ; 
       dowager_sPtL-mat87.4-0 ; 
       dowager_sPtL-mat88.4-0 ; 
       dowager_sPtL-mat89.4-0 ; 
       dowager_sPtL-mat90.4-0 ; 
       dowager_sPtL-mat91.4-0 ; 
       dowager_sPtL-mat92.4-0 ; 
       dowager_sPtL-mat93.4-0 ; 
       dowager_sPtL-mat94.4-0 ; 
       dowager_sPtL-mat95.4-0 ; 
       dowager_sPtL-mat96.4-0 ; 
       dowager_sPtL-mat97.4-0 ; 
       merge-mat100.1-0 ; 
       merge-mat101.1-0 ; 
       merge-mat102.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 139     
       merge-bmerge3.2-0 ROOT ; 
       ss01-all_hangars.1-0 ; 
       ss01-antenn1.1-0 ; 
       ss01-antenn2.1-0 ; 
       ss01-antenn3.1-0 ; 
       ss01-bcorrdr.1-0 ; 
       ss01-bturatt0.1-0 ; 
       ss01-bturatt1.1-0 ; 
       ss01-bturatt2.1-0 ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr0.1-0 ; 
       ss01-corrdr1.1-0 ; 
       ss01-corrdr2.1-0 ; 
       ss01-corrdr3.1-0 ; 
       ss01-corrdr4.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg2.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg4.1-0 ; 
       ss01-garage1A.1-0 ; 
       ss01-garage1B.1-0 ; 
       ss01-garage1C.1-0 ; 
       ss01-garage1D.1-0 ; 
       ss01-garage1E.1-0 ; 
       ss01-garage2A.1-0 ; 
       ss01-garage2B.1-0 ; 
       ss01-garage2C.1-0 ; 
       ss01-garage2D.1-0 ; 
       ss01-garage2E.1-0 ; 
       ss01-hangar.1-0 ; 
       ss01-hangar1.1-0 ; 
       ss01-larmour.1-0 ; 
       ss01-launch1.1-0 ; 
       ss01-launch2.1-0 ; 
       ss01-lcontwr.1-0 ; 
       ss01-lndpad0.1-0 ; 
       ss01-lndpad1.1-0 ; 
       ss01-lndpad10.1-0 ; 
       ss01-lndpad11.1-0 ; 
       ss01-lndpad12.1-0 ; 
       ss01-lndpad2.1-0 ; 
       ss01-lndpad3.1-0 ; 
       ss01-lndpad4.1-0 ; 
       ss01-lndpad5.1-0 ; 
       ss01-lndpad6.1-0 ; 
       ss01-lndpad7.1-0 ; 
       ss01-lndpad8.1-0 ; 
       ss01-lndpad9.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-platfrm2.1-0 ; 
       ss01-rarmour.1-0 ; 
       ss01-rcontwr.1-0 ; 
       ss01-ss01_2.14-0 ROOT ; 
       ss01-SS01a.1-0 ; 
       ss01-SS01b.1-0 ; 
       ss01-Ss02a.1-0 ; 
       ss01-SS02b.1-0 ; 
       ss01-SS03a.1-0 ; 
       ss01-SS03b.1-0 ; 
       ss01-SS04a.1-0 ; 
       ss01-SS04b.1-0 ; 
       ss01-SS05a.1-0 ; 
       ss01-SS05b.1-0 ; 
       ss01-SS05c.1-0 ; 
       ss01-SS0p.1-0 ; 
       ss01-SS1a1.1-0 ; 
       ss01-SS1a2.1-0 ; 
       ss01-SS1a3.1-0 ; 
       ss01-SS1a4.1-0 ; 
       ss01-SS1b1.1-0 ; 
       ss01-SS1b2.1-0 ; 
       ss01-SS1b3.1-0 ; 
       ss01-SS1b4.1-0 ; 
       ss01-SS1c1.1-0 ; 
       ss01-SS1c2.1-0 ; 
       ss01-SS1c3.1-0 ; 
       ss01-SS1c4.1-0 ; 
       ss01-SS2a1.1-0 ; 
       ss01-SS2a2.1-0 ; 
       ss01-SS2a3.1-0 ; 
       ss01-SS2a4.1-0 ; 
       ss01-SS2b1.1-0 ; 
       ss01-SS2b2.1-0 ; 
       ss01-SS2b3.1-0 ; 
       ss01-SS2b4.1-0 ; 
       ss01-SS2c1.1-0 ; 
       ss01-SS2c2.1-0 ; 
       ss01-SS2c3.1-0 ; 
       ss01-SS2c4.1-0 ; 
       ss01-SS3a1.1-0 ; 
       ss01-SS3a2.1-0 ; 
       ss01-SS3a3.1-0 ; 
       ss01-SS3a4.1-0 ; 
       ss01-SS3b1.1-0 ; 
       ss01-SS3b2.1-0 ; 
       ss01-SS3b3.1-0 ; 
       ss01-SS3b4.1-0 ; 
       ss01-SS3c1.1-0 ; 
       ss01-SS3c2.1-0 ; 
       ss01-SS3c3.1-0 ; 
       ss01-SS3c4.1-0 ; 
       ss01-SS4a1.1-0 ; 
       ss01-SS4a2.1-0 ; 
       ss01-SS4a3.1-0 ; 
       ss01-SS4a4.1-0 ; 
       ss01-SS4b2.1-0 ; 
       ss01-SS4b3.1-0 ; 
       ss01-SS4b4.1-0 ; 
       ss01-SS4c1.1-0 ; 
       ss01-SS4c2.1-0 ; 
       ss01-SS4c3.1-0 ; 
       ss01-SS4c4.1-0 ; 
       ss01-SS5a1.1-0 ; 
       ss01-SS5a2.1-0 ; 
       ss01-SS5a3.1-0 ; 
       ss01-SS5a4.1-0 ; 
       ss01-SS5b1.1-0 ; 
       ss01-SS5b2.1-0 ; 
       ss01-SS5b3.1-0 ; 
       ss01-SS5b4.1-0 ; 
       ss01-SS5c1.1-0 ; 
       ss01-SS5c2.1-0 ; 
       ss01-SS5c3.1-0 ; 
       ss01-SS5c4.1-0 ; 
       ss01-SSb4b1.1-0 ; 
       ss01-SSc1.6-0 ; 
       ss01-SSc2.1-0 ; 
       ss01-SSf1.9-0 ; 
       ss01-SSf2.1-0 ; 
       ss01-SSf3.1-0 ; 
       ss01-SSf4.1-0 ; 
       ss01-SSp1.1-0 ; 
       ss01-SSp2.1-0 ; 
       ss01-SSp3.9-0 ; 
       ss01-SSp4.1-0 ; 
       ss01-turwepemt1.1-0 ; 
       ss01-turwepemt2.1-0 ; 
       ss01-turwepemt3.1-0 ; 
       ss01-turwepemt4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/ss01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss01-merge.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 40     
       add_frames-t2d65.3-0 ; 
       add_frames-t2d66.3-0 ; 
       dowager_sPtL-t2d1.6-0 ; 
       dowager_sPtL-t2d11.6-0 ; 
       dowager_sPtL-t2d12.6-0 ; 
       dowager_sPtL-t2d13.6-0 ; 
       dowager_sPtL-t2d14.6-0 ; 
       dowager_sPtL-t2d15.6-0 ; 
       dowager_sPtL-t2d16.6-0 ; 
       dowager_sPtL-t2d17.6-0 ; 
       dowager_sPtL-t2d18.6-0 ; 
       dowager_sPtL-t2d2.6-0 ; 
       dowager_sPtL-t2d31.6-0 ; 
       dowager_sPtL-t2d32.6-0 ; 
       dowager_sPtL-t2d33.6-0 ; 
       dowager_sPtL-t2d35.6-0 ; 
       dowager_sPtL-t2d40.8-0 ; 
       dowager_sPtL-t2d41.8-0 ; 
       dowager_sPtL-t2d42.6-0 ; 
       dowager_sPtL-t2d43.6-0 ; 
       dowager_sPtL-t2d46.6-0 ; 
       dowager_sPtL-t2d48.6-0 ; 
       dowager_sPtL-t2d49.6-0 ; 
       dowager_sPtL-t2d5.8-0 ; 
       dowager_sPtL-t2d50.6-0 ; 
       dowager_sPtL-t2d51.6-0 ; 
       dowager_sPtL-t2d52.6-0 ; 
       dowager_sPtL-t2d53.6-0 ; 
       dowager_sPtL-t2d54.6-0 ; 
       dowager_sPtL-t2d55.6-0 ; 
       dowager_sPtL-t2d56.6-0 ; 
       dowager_sPtL-t2d57.6-0 ; 
       dowager_sPtL-t2d59.6-0 ; 
       dowager_sPtL-t2d61.6-0 ; 
       dowager_sPtL-t2d62.6-0 ; 
       dowager_sPtL-t2d63.6-0 ; 
       dowager_sPtL-t2d64.6-0 ; 
       merge-t2d67.2-0 ; 
       merge-t2d68.2-0 ; 
       merge-t2d69.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 52 110 ; 
       2 9 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 10 110 ; 
       6 48 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 15 110 ; 
       10 48 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 52 110 ; 
       16 17 110 ; 
       17 48 110 ; 
       18 48 110 ; 
       19 29 110 ; 
       20 29 110 ; 
       21 29 110 ; 
       22 29 110 ; 
       23 29 110 ; 
       24 30 110 ; 
       25 30 110 ; 
       26 30 110 ; 
       27 30 110 ; 
       28 30 110 ; 
       29 1 110 ; 
       30 1 110 ; 
       31 15 110 ; 
       32 1 110 ; 
       33 1 110 ; 
       34 9 110 ; 
       35 48 110 ; 
       36 35 110 ; 
       37 35 110 ; 
       38 35 110 ; 
       39 35 110 ; 
       40 35 110 ; 
       41 35 110 ; 
       42 35 110 ; 
       43 35 110 ; 
       44 35 110 ; 
       45 35 110 ; 
       46 35 110 ; 
       47 35 110 ; 
       48 52 110 ; 
       49 48 110 ; 
       50 15 110 ; 
       51 9 110 ; 
       53 11 110 ; 
       54 11 110 ; 
       55 12 110 ; 
       56 12 110 ; 
       57 13 110 ; 
       58 13 110 ; 
       59 14 110 ; 
       60 14 110 ; 
       61 5 110 ; 
       62 5 110 ; 
       63 5 110 ; 
       64 48 110 ; 
       65 53 110 ; 
       66 53 110 ; 
       67 53 110 ; 
       68 53 110 ; 
       69 54 110 ; 
       70 54 110 ; 
       71 54 110 ; 
       72 54 110 ; 
       73 11 110 ; 
       74 11 110 ; 
       75 11 110 ; 
       76 11 110 ; 
       77 55 110 ; 
       78 55 110 ; 
       79 55 110 ; 
       80 55 110 ; 
       81 56 110 ; 
       82 56 110 ; 
       83 56 110 ; 
       84 56 110 ; 
       85 12 110 ; 
       86 12 110 ; 
       87 12 110 ; 
       88 12 110 ; 
       89 57 110 ; 
       90 57 110 ; 
       91 57 110 ; 
       92 57 110 ; 
       93 58 110 ; 
       94 58 110 ; 
       95 58 110 ; 
       96 58 110 ; 
       97 13 110 ; 
       98 13 110 ; 
       99 13 110 ; 
       100 13 110 ; 
       101 59 110 ; 
       102 59 110 ; 
       103 59 110 ; 
       104 59 110 ; 
       105 60 110 ; 
       106 60 110 ; 
       107 60 110 ; 
       108 14 110 ; 
       109 14 110 ; 
       110 14 110 ; 
       111 14 110 ; 
       112 61 110 ; 
       113 61 110 ; 
       114 61 110 ; 
       115 61 110 ; 
       116 62 110 ; 
       117 62 110 ; 
       118 62 110 ; 
       119 62 110 ; 
       120 63 110 ; 
       121 63 110 ; 
       122 63 110 ; 
       123 63 110 ; 
       124 60 110 ; 
       125 9 110 ; 
       126 9 110 ; 
       127 15 110 ; 
       128 15 110 ; 
       129 15 110 ; 
       130 15 110 ; 
       131 64 110 ; 
       132 64 110 ; 
       133 64 110 ; 
       134 64 110 ; 
       135 52 110 ; 
       136 52 110 ; 
       137 52 110 ; 
       138 52 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 75 300 ; 
       2 76 300 ; 
       2 77 300 ; 
       2 78 300 ; 
       3 80 300 ; 
       3 81 300 ; 
       3 82 300 ; 
       3 83 300 ; 
       4 74 300 ; 
       5 86 300 ; 
       5 87 300 ; 
       5 88 300 ; 
       5 89 300 ; 
       9 73 300 ; 
       11 113 300 ; 
       11 114 300 ; 
       11 115 300 ; 
       11 116 300 ; 
       11 117 300 ; 
       11 118 300 ; 
       12 119 300 ; 
       12 120 300 ; 
       12 121 300 ; 
       12 122 300 ; 
       12 123 300 ; 
       12 124 300 ; 
       13 110 300 ; 
       13 107 300 ; 
       13 108 300 ; 
       13 109 300 ; 
       13 111 300 ; 
       13 112 300 ; 
       14 125 300 ; 
       14 126 300 ; 
       14 127 300 ; 
       14 128 300 ; 
       14 129 300 ; 
       14 130 300 ; 
       15 72 300 ; 
       15 79 300 ; 
       15 84 300 ; 
       16 90 300 ; 
       16 103 300 ; 
       17 96 300 ; 
       17 97 300 ; 
       18 100 300 ; 
       18 101 300 ; 
       18 102 300 ; 
       31 104 300 ; 
       34 98 300 ; 
       48 85 300 ; 
       48 105 300 ; 
       48 91 300 ; 
       48 92 300 ; 
       48 0 300 ; 
       48 11 300 ; 
       49 93 300 ; 
       49 94 300 ; 
       49 95 300 ; 
       50 106 300 ; 
       51 99 300 ; 
       65 33 300 ; 
       66 34 300 ; 
       67 35 300 ; 
       68 36 300 ; 
       69 53 300 ; 
       70 52 300 ; 
       71 50 300 ; 
       72 51 300 ; 
       73 14 300 ; 
       74 17 300 ; 
       75 65 300 ; 
       76 71 300 ; 
       77 28 300 ; 
       78 29 300 ; 
       79 30 300 ; 
       80 31 300 ; 
       81 49 300 ; 
       82 48 300 ; 
       83 46 300 ; 
       84 47 300 ; 
       85 32 300 ; 
       86 43 300 ; 
       87 54 300 ; 
       88 12 300 ; 
       89 37 300 ; 
       90 38 300 ; 
       91 39 300 ; 
       92 40 300 ; 
       93 45 300 ; 
       94 44 300 ; 
       95 41 300 ; 
       96 42 300 ; 
       97 19 300 ; 
       98 16 300 ; 
       99 13 300 ; 
       100 70 300 ; 
       101 24 300 ; 
       102 25 300 ; 
       103 26 300 ; 
       104 27 300 ; 
       105 22 300 ; 
       106 20 300 ; 
       107 21 300 ; 
       108 18 300 ; 
       109 68 300 ; 
       110 15 300 ; 
       111 69 300 ; 
       112 63 300 ; 
       113 64 300 ; 
       114 66 300 ; 
       115 67 300 ; 
       116 59 300 ; 
       117 60 300 ; 
       118 61 300 ; 
       119 62 300 ; 
       120 55 300 ; 
       121 56 300 ; 
       122 57 300 ; 
       123 58 300 ; 
       124 23 300 ; 
       125 7 300 ; 
       126 10 300 ; 
       127 9 300 ; 
       128 4 300 ; 
       129 5 300 ; 
       130 6 300 ; 
       131 3 300 ; 
       132 2 300 ; 
       133 8 300 ; 
       134 1 300 ; 
       0 131 300 ; 
       0 132 300 ; 
       0 133 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       52 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       11 1 401 ; 
       73 3 401 ; 
       74 4 401 ; 
       76 5 401 ; 
       77 6 401 ; 
       78 7 401 ; 
       79 2 401 ; 
       81 8 401 ; 
       82 9 401 ; 
       83 10 401 ; 
       84 11 401 ; 
       87 12 401 ; 
       88 13 401 ; 
       89 14 401 ; 
       90 15 401 ; 
       91 16 401 ; 
       92 17 401 ; 
       94 18 401 ; 
       95 19 401 ; 
       97 20 401 ; 
       98 22 401 ; 
       99 21 401 ; 
       101 24 401 ; 
       102 25 401 ; 
       103 26 401 ; 
       104 27 401 ; 
       105 23 401 ; 
       106 28 401 ; 
       111 29 401 ; 
       112 30 401 ; 
       117 31 401 ; 
       118 36 401 ; 
       123 32 401 ; 
       124 35 401 ; 
       129 33 401 ; 
       130 34 401 ; 
       132 38 401 ; 
       133 39 401 ; 
       131 37 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 2.5 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -18 0 WIRECOL 7 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 71.25 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 20 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 16.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 291.25 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 88.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 90 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 87.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 23.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 201.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 113.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 158.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 203.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 248.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 27.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 466.2862 58.15258 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 472.653 47.16021 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 325 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 57.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 60 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 62.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 65 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 67.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 70 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 72.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 75 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 77.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 80 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 62.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 75 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 464.4991 56.94917 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 82.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 85 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 343.75 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 345 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 332.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 335 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 357.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 340 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 342.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 330 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 337.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 347.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 350 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 352.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 355 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 238.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 362.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 466.9991 56.94917 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 7.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 200 -4 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       53 SCHEM 116.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 98.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 151.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 161.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 206.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 186.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 251.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 241.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 296.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 276.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 286.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 371.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 120 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 112.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 115 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 117.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 102.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 95 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 97.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 100 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 105 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 107.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 110 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 92.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 155 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 147.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 150 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 152.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 165 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 157.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 160 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 162.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       85 SCHEM 137.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       86 SCHEM 140 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       87 SCHEM 142.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       88 SCHEM 145 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       89 SCHEM 210 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       90 SCHEM 202.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       91 SCHEM 205 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       92 SCHEM 207.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       93 SCHEM 190 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       94 SCHEM 182.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       95 SCHEM 185 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       96 SCHEM 187.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       97 SCHEM 192.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       98 SCHEM 195 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       99 SCHEM 197.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       100 SCHEM 200 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       101 SCHEM 255 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       102 SCHEM 247.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       103 SCHEM 250 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       104 SCHEM 252.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       105 SCHEM 245 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       106 SCHEM 237.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       107 SCHEM 240 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       108 SCHEM 235 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       109 SCHEM 230 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       110 SCHEM 232.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       111 SCHEM 227.5 -12 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       112 SCHEM 300 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       113 SCHEM 292.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       114 SCHEM 295 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       115 SCHEM 297.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       116 SCHEM 280 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       117 SCHEM 272.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       118 SCHEM 275 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       119 SCHEM 277.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       120 SCHEM 290 -14 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       121 SCHEM 282.5 -14 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       122 SCHEM 285 -14 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       123 SCHEM 287.5 -14 0 WIRECOL 8 7 DISPLAY 0 0 MPRFLG 0 ; 
       124 SCHEM 242.5 -14 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       125 SCHEM 35 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       126 SCHEM 37.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       127 SCHEM 47.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       128 SCHEM 0 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       129 SCHEM 2.5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       130 SCHEM 5 -8 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       131 SCHEM 367.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       132 SCHEM 375 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       133 SCHEM 370 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       134 SCHEM 372.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       135 SCHEM 392.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       136 SCHEM 395 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       137 SCHEM 397.5 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       138 SCHEM 400 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 469.434 56.95441 0 USR DISPLAY 1 2 SRT 1 1 1 0 0 0 0 8.711221 -3.677348 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 361 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 372.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 375 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 367.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 0 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 35 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 370 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 37.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 362.5 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 145 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 197.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 105 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 232.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 195 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 107.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 235 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 192.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 237.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 240 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 245 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 242.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 255 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 247.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 250 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 252.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 155 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 147.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 150 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 152.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 137.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 120 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 112.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 115 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 117.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 210 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 202.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 205 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 207.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 185 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 187.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 140 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 182.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 190 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 160 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 162.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 157.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 165 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 97.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 100 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 95 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 102.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 142.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 290 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 282.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 285 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 287.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 280 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 272.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 275 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 277.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 300 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 292.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 110 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 295 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 297.5 -16 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 230 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 227.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 200 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 92.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 55 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 40 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 10 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 30 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 22.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 25 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 27.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 50 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 20 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 12.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 15 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 17.5 -14 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 52.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       85 SCHEM 364 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       86 SCHEM 310 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       87 SCHEM 302.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       88 SCHEM 305 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       89 SCHEM 307.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       90 SCHEM 315 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       91 SCHEM 358 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       92 SCHEM 359.5 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       93 SCHEM 365 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       94 SCHEM 360 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       95 SCHEM 362.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       96 SCHEM 320 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       97 SCHEM 317.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       98 SCHEM 32.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       99 SCHEM 7.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       100 SCHEM 327.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       101 SCHEM 322.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       102 SCHEM 325 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       103 SCHEM 312.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       104 SCHEM 42.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       105 SCHEM 356.5 -8 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       106 SCHEM 45 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       107 SCHEM 212.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       108 SCHEM 215 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       109 SCHEM 217.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       110 SCHEM 225 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       111 SCHEM 220 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       112 SCHEM 222.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       113 SCHEM 135 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       114 SCHEM 122.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       115 SCHEM 125 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       116 SCHEM 127.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       117 SCHEM 130 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       118 SCHEM 132.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       119 SCHEM 180 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       120 SCHEM 167.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       121 SCHEM 170 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       122 SCHEM 172.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       123 SCHEM 175 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       124 SCHEM 177.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       125 SCHEM 270 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       126 SCHEM 257.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       127 SCHEM 260 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       128 SCHEM 262.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       129 SCHEM 265 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       130 SCHEM 267.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       132 SCHEM 477.653 58.15258 0 WIRECOL 1 7 MPRFLG 0 ; 
       133 SCHEM 482.653 58.15258 0 WIRECOL 1 7 MPRFLG 0 ; 
       131 SCHEM 0 -20 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 360 -10 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 361.5 -10 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 50 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 40 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 10 -16 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 22.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 25 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 27.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -16 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 15 -16 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 17.5 -16 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 52.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 302.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 305 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 307.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 315 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 357 -10 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 358.5 -10 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 360 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 362.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 317.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 7.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 32.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 355.5 -10 0 USR WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 322.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 325 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 312.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 42.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 45 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 220 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 222.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 130 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 175 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 265 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 267.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 177.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 132.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 475.153 58.15258 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 480.153 58.15258 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 485.153 58.15258 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 401.5 -6 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 15 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
