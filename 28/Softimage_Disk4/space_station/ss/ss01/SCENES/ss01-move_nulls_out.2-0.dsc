SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss01-ss01_2.25-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       render_version-cam_int1.48-0 ROOT ; 
       render_version-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       dowager_sPtL-inf_light5_1_2.44-0 ROOT ; 
       dowager_sPtL-inf_light5_1_2_2.9-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2.44-0 ROOT ; 
       dowager_sPtL-inf_light6_1_2_2.9-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2.44-0 ROOT ; 
       dowager_sPtL-inf_light8_1_2_2.9-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 112     
       add_frames-front_of_garages1_1.1-0 ; 
       add_frames-nose_white-center.1-13.3-0 ; 
       add_frames-nose_white-center.1-14.3-0 ; 
       add_frames-nose_white-center.1-15.3-0 ; 
       add_frames-nose_white-center.1-5.3-0 ; 
       add_frames-nose_white-center.1-7.3-0 ; 
       add_frames-nose_white-center.1-9.3-0 ; 
       add_frames-side_panels1_1.1-0 ; 
       add_frames-starbord_green-right.1-0.3-0 ; 
       add_frames-starbord_green-right.1-3.3-0 ; 
       add_frames-starbord_green-right.1-4.3-0 ; 
       add_frames-starbord_green-right.1-42.3-0 ; 
       add_frames-starbord_green-right.1-43.3-0 ; 
       add_frames-starbord_green-right.1-44.3-0 ; 
       add_frames-starbord_green-right.1-45.3-0 ; 
       add_frames-starbord_green-right.1-5.3-0 ; 
       dowager_sPtL-mat1_1.1-0 ; 
       dowager_sPtL-mat14_1.1-0 ; 
       dowager_sPtL-mat2_1.1-0 ; 
       dowager_sPtL-mat3_1.1-0 ; 
       dowager_sPtL-mat4_1.1-0 ; 
       dowager_sPtL-mat51_1.1-0 ; 
       dowager_sPtL-mat52_1.1-0 ; 
       dowager_sPtL-mat53_1.1-0 ; 
       dowager_sPtL-mat55_1.1-0 ; 
       dowager_sPtL-mat57_1.1-0 ; 
       dowager_sPtL-mat60_1.1-0 ; 
       dowager_sPtL-mat62_1.1-0 ; 
       dowager_sPtL-mat65_1.1-0 ; 
       dowager_sPtL-mat66_1.1-0 ; 
       dowager_sPtL-mat67_1.1-0 ; 
       dowager_sPtL-mat7_1.1-0 ; 
       dowager_sPtL-mat86.7-0 ; 
       dowager_sPtL-mat86_1.1-0 ; 
       dowager_sPtL-mat87.5-0 ; 
       dowager_sPtL-mat88.5-0 ; 
       dowager_sPtL-mat89.5-0 ; 
       dowager_sPtL-mat90.7-0 ; 
       dowager_sPtL-mat90_1.1-0 ; 
       dowager_sPtL-mat91.7-0 ; 
       dowager_sPtL-mat91_1.1-0 ; 
       ROTATE-mat103.1-0 ; 
       ROTATE-mat104.1-0 ; 
       ROTATE-mat105.1-0 ; 
       ROTATE-mat106.1-0 ; 
       ROTATE-mat107.1-0 ; 
       ROTATE-mat108.1-0 ; 
       ROTATE-mat109.1-0 ; 
       ROTATE-mat110.1-0 ; 
       ROTATE-mat111.1-0 ; 
       ROTATE-mat112.1-0 ; 
       ROTATE-mat113.1-0 ; 
       ROTATE-mat114.1-0 ; 
       ROTATE-mat115.1-0 ; 
       ROTATE-mat116.1-0 ; 
       ROTATE-mat117.1-0 ; 
       ROTATE-mat118.1-0 ; 
       ROTATE-mat119.1-0 ; 
       ROTATE-mat120.1-0 ; 
       ROTATE-mat121.1-0 ; 
       ROTATE-mat122.1-0 ; 
       ROTATE-mat123.1-0 ; 
       ROTATE-mat124.1-0 ; 
       ROTATE-mat125.1-0 ; 
       ROTATE-mat126.1-0 ; 
       ROTATE-mat86_2.1-0 ; 
       ROTATE-mat86_3.1-0 ; 
       ROTATE-mat86_4.1-0 ; 
       ROTATE-mat86_5.1-0 ; 
       ROTATE-mat90_2.1-0 ; 
       ROTATE-mat90_3.1-0 ; 
       ROTATE-mat90_4.1-0 ; 
       ROTATE-mat90_5.1-0 ; 
       ROTATE-mat91_2.1-0 ; 
       ROTATE-mat91_3.1-0 ; 
       ROTATE-mat91_4.1-0 ; 
       ROTATE-mat91_5.1-0 ; 
       ROTATE-starbord_green-right.1-62.1-0 ; 
       ROTATE-starbord_green-right.1-63.1-0 ; 
       ROTATE-starbord_green-right.1-64.1-0 ; 
       ROTATE-starbord_green-right.1-65.1-0 ; 
       ROTATE-starbord_green-right.1-66.1-0 ; 
       ROTATE-starbord_green-right.1-67.1-0 ; 
       ROTATE-starbord_green-right.1-68.1-0 ; 
       ROTATE-starbord_green-right.1-69.1-0 ; 
       ROTATE-starbord_green-right.1-70.1-0 ; 
       ROTATE-starbord_green-right.1-71.1-0 ; 
       ROTATE-starbord_green-right.1-72.1-0 ; 
       ROTATE-starbord_green-right.1-73.1-0 ; 
       ROTATE-starbord_green-right.1-74.1-0 ; 
       ROTATE-starbord_green-right.1-75.1-0 ; 
       ROTATE-starbord_green-right.1-76.1-0 ; 
       ROTATE-starbord_green-right.1-77.1-0 ; 
       ROTATE-starbord_green-right.1-78.1-0 ; 
       ROTATE-starbord_green-right.1-79.1-0 ; 
       ROTATE-starbord_green-right.1-80.1-0 ; 
       ROTATE-starbord_green-right.1-81.1-0 ; 
       ROTATE-starbord_green-right.1-82.1-0 ; 
       ROTATE-starbord_green-right.1-83.1-0 ; 
       ROTATE-starbord_green-right.1-84.1-0 ; 
       ROTATE-starbord_green-right.1-85.1-0 ; 
       ROTATE-starbord_green-right.1-86.1-0 ; 
       ROTATE-starbord_green-right.1-87.1-0 ; 
       ROTATE-starbord_green-right.1-88.1-0 ; 
       ROTATE-starbord_green-right.1-89.1-0 ; 
       ROTATE-starbord_green-right.1-90.1-0 ; 
       ROTATE-starbord_green-right.1-91.1-0 ; 
       ROTATE-starbord_green-right.1-92.1-0 ; 
       ROTATE-starbord_green-right.1-93.1-0 ; 
       Static-mat100.2-0 ; 
       Static-mat101.2-0 ; 
       Static-mat102.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 89     
       ss01-all_hangars.1-0 ; 
       ss01-bmerge3.2-0 ; 
       ss01-contwr1.1-0 ; 
       ss01-corrdr2_1.2-0 ; 
       ss01-corrdr2_10.1-0 ; 
       ss01-corrdr2_11.1-0 ; 
       ss01-corrdr2_12.1-0 ; 
       ss01-corrdr2_13.1-0 ; 
       ss01-corrdr2_14.1-0 ; 
       ss01-corrdr2_15.1-0 ; 
       ss01-corrdr2_16.1-0 ; 
       ss01-corrdr2_17.1-0 ; 
       ss01-corrdr2_9.1-0 ; 
       ss01-fuselg1.1-0 ; 
       ss01-fuselg3.1-0 ; 
       ss01-fuselg4.1-0 ; 
       ss01-garage1A.1-0 ; 
       ss01-garage1B.1-0 ; 
       ss01-garage1C.1-0 ; 
       ss01-garage1D.1-0 ; 
       ss01-garage1E.1-0 ; 
       ss01-garage2A.1-0 ; 
       ss01-garage2B.1-0 ; 
       ss01-garage2C.1-0 ; 
       ss01-garage2D.1-0 ; 
       ss01-garage2E.1-0 ; 
       ss01-hangar.1-0 ; 
       ss01-hangar1.1-0 ; 
       ss01-launch1.1-0 ; 
       ss01-launch2.1-0 ; 
       ss01-null2.1-0 ; 
       ss01-platfrm1.1-0 ; 
       ss01-platfrm2.1-0 ; 
       ss01-ss01_2.32-0 ROOT ; 
       ss01-SS2b1.1-0 ; 
       ss01-SS2b10.1-0 ; 
       ss01-SS2b11.1-0 ; 
       ss01-SS2b12.1-0 ; 
       ss01-SS2b13.1-0 ; 
       ss01-SS2b14.1-0 ; 
       ss01-SS2b15.1-0 ; 
       ss01-SS2b16.1-0 ; 
       ss01-SS2b17.1-0 ; 
       ss01-SS2b18.1-0 ; 
       ss01-SS2b19.1-0 ; 
       ss01-SS2b2.1-0 ; 
       ss01-SS2b20.1-0 ; 
       ss01-SS2b3.1-0 ; 
       ss01-SS2b4.1-0 ; 
       ss01-SS2b5.1-0 ; 
       ss01-SS2b6.1-0 ; 
       ss01-SS2b7.1-0 ; 
       ss01-SS2b8.1-0 ; 
       ss01-SS2b9.1-0 ; 
       ss01-SS2c1.1-0 ; 
       ss01-SS2c10.1-0 ; 
       ss01-SS2c11.1-0 ; 
       ss01-SS2c12.1-0 ; 
       ss01-SS2c13.1-0 ; 
       ss01-SS2c14.1-0 ; 
       ss01-SS2c15.1-0 ; 
       ss01-SS2c16.1-0 ; 
       ss01-SS2c17.1-0 ; 
       ss01-SS2c18.1-0 ; 
       ss01-SS2c19.1-0 ; 
       ss01-SS2c2.1-0 ; 
       ss01-SS2c20.1-0 ; 
       ss01-SS2c3.1-0 ; 
       ss01-SS2c4.1-0 ; 
       ss01-SS2c5.1-0 ; 
       ss01-SS2c6.1-0 ; 
       ss01-SS2c7.1-0 ; 
       ss01-SS2c8.1-0 ; 
       ss01-SS2c9.1-0 ; 
       ss01-SSc1.6-0 ; 
       ss01-SSc2.1-0 ; 
       ss01-SSf1.9-0 ; 
       ss01-SSf2.1-0 ; 
       ss01-SSf3.1-0 ; 
       ss01-SSf4.1-0 ; 
       ss01-turwepemt1.1-0 ; 
       ss01-turwepemt2.1-0 ; 
       ss01-turwepemt3.1-0 ; 
       ss01-turwepemt4.1-0 ; 
       ss01-ww.1-0 ; 
       ss01-ww1.1-0 ; 
       ss01-ww2.1-0 ; 
       ss01-ww3.1-0 ; 
       ss01-ww4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/rixbay ; 
       E:/Pete_Data2/space_station/ss/ss01/PICTURES/ss01 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss01-move_nulls_out.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 36     
       add_frames-t2d65_1.2-0 ; 
       add_frames-t2d66_1.2-0 ; 
       dowager_sPtL-t2d1_1.1-0 ; 
       dowager_sPtL-t2d11_1.1-0 ; 
       dowager_sPtL-t2d2_1.1-0 ; 
       dowager_sPtL-t2d40.15-0 ; 
       dowager_sPtL-t2d41.15-0 ; 
       dowager_sPtL-t2d42_1.1-0 ; 
       dowager_sPtL-t2d43_1.1-0 ; 
       dowager_sPtL-t2d46_1.1-0 ; 
       dowager_sPtL-t2d5.15-0 ; 
       dowager_sPtL-t2d50_1.1-0 ; 
       dowager_sPtL-t2d51_1.1-0 ; 
       dowager_sPtL-t2d59.11-0 ; 
       dowager_sPtL-t2d59_1.1-0 ; 
       dowager_sPtL-t2d63.11-0 ; 
       dowager_sPtL-t2d63_1.1-0 ; 
       ROTATE-t2d59_2.1-0 ; 
       ROTATE-t2d59_3.1-0 ; 
       ROTATE-t2d59_4.1-0 ; 
       ROTATE-t2d59_5.1-0 ; 
       ROTATE-t2d63_2.1-0 ; 
       ROTATE-t2d63_3.1-0 ; 
       ROTATE-t2d63_4.1-0 ; 
       ROTATE-t2d63_5.1-0 ; 
       ROTATE-t2d70.1-0 ; 
       ROTATE-t2d71.1-0 ; 
       ROTATE-t2d72.1-0 ; 
       ROTATE-t2d73.1-0 ; 
       ROTATE-t2d74.1-0 ; 
       ROTATE-t2d75.1-0 ; 
       ROTATE-t2d76.1-0 ; 
       ROTATE-t2d77.1-0 ; 
       Static-t2d67.3-0 ; 
       Static-t2d68.3-0 ; 
       Static-t2d69.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 33 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       3 30 110 ; 
       4 30 110 ; 
       5 4 110 ; 
       6 30 110 ; 
       7 6 110 ; 
       8 30 110 ; 
       9 8 110 ; 
       10 30 110 ; 
       11 10 110 ; 
       12 3 110 ; 
       13 33 110 ; 
       14 31 110 ; 
       15 31 110 ; 
       16 26 110 ; 
       17 26 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       21 27 110 ; 
       22 27 110 ; 
       23 27 110 ; 
       24 27 110 ; 
       25 27 110 ; 
       26 0 110 ; 
       27 0 110 ; 
       28 0 110 ; 
       29 0 110 ; 
       30 31 110 ; 
       31 33 110 ; 
       32 31 110 ; 
       34 84 110 ; 
       35 86 110 ; 
       36 86 110 ; 
       37 86 110 ; 
       38 87 110 ; 
       39 87 110 ; 
       40 87 110 ; 
       41 87 110 ; 
       42 88 110 ; 
       43 88 110 ; 
       44 88 110 ; 
       45 84 110 ; 
       46 88 110 ; 
       47 84 110 ; 
       48 84 110 ; 
       49 85 110 ; 
       50 85 110 ; 
       51 85 110 ; 
       52 85 110 ; 
       53 86 110 ; 
       54 12 110 ; 
       55 7 110 ; 
       56 7 110 ; 
       57 7 110 ; 
       58 9 110 ; 
       59 9 110 ; 
       60 9 110 ; 
       61 9 110 ; 
       62 11 110 ; 
       63 11 110 ; 
       64 11 110 ; 
       65 12 110 ; 
       66 11 110 ; 
       67 12 110 ; 
       68 12 110 ; 
       69 5 110 ; 
       70 5 110 ; 
       71 5 110 ; 
       72 5 110 ; 
       73 7 110 ; 
       74 33 110 ; 
       75 33 110 ; 
       76 33 110 ; 
       77 33 110 ; 
       78 33 110 ; 
       79 33 110 ; 
       80 33 110 ; 
       81 33 110 ; 
       82 33 110 ; 
       83 33 110 ; 
       84 12 110 ; 
       85 5 110 ; 
       86 7 110 ; 
       87 9 110 ; 
       88 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 109 300 ; 
       1 110 300 ; 
       1 111 300 ; 
       2 17 300 ; 
       3 33 300 ; 
       3 38 300 ; 
       3 40 300 ; 
       4 65 300 ; 
       4 69 300 ; 
       4 73 300 ; 
       5 41 300 ; 
       5 42 300 ; 
       5 43 300 ; 
       5 44 300 ; 
       5 45 300 ; 
       5 46 300 ; 
       6 66 300 ; 
       6 70 300 ; 
       6 74 300 ; 
       7 47 300 ; 
       7 48 300 ; 
       7 49 300 ; 
       7 50 300 ; 
       7 51 300 ; 
       7 52 300 ; 
       8 67 300 ; 
       8 71 300 ; 
       8 75 300 ; 
       9 53 300 ; 
       9 54 300 ; 
       9 55 300 ; 
       9 56 300 ; 
       9 57 300 ; 
       9 58 300 ; 
       10 68 300 ; 
       10 72 300 ; 
       10 76 300 ; 
       11 59 300 ; 
       11 60 300 ; 
       11 61 300 ; 
       11 62 300 ; 
       11 63 300 ; 
       11 64 300 ; 
       12 32 300 ; 
       12 34 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 37 300 ; 
       12 39 300 ; 
       13 16 300 ; 
       13 18 300 ; 
       13 19 300 ; 
       14 26 300 ; 
       14 27 300 ; 
       15 28 300 ; 
       15 29 300 ; 
       15 30 300 ; 
       31 20 300 ; 
       31 31 300 ; 
       31 21 300 ; 
       31 22 300 ; 
       31 0 300 ; 
       31 7 300 ; 
       32 23 300 ; 
       32 24 300 ; 
       32 25 300 ; 
       34 14 300 ; 
       35 90 300 ; 
       36 91 300 ; 
       37 92 300 ; 
       38 97 300 ; 
       39 98 300 ; 
       40 99 300 ; 
       41 100 300 ; 
       42 105 300 ; 
       43 106 300 ; 
       44 107 300 ; 
       45 13 300 ; 
       46 108 300 ; 
       47 11 300 ; 
       48 12 300 ; 
       49 81 300 ; 
       50 82 300 ; 
       51 83 300 ; 
       52 84 300 ; 
       53 89 300 ; 
       54 9 300 ; 
       55 86 300 ; 
       56 87 300 ; 
       57 88 300 ; 
       58 93 300 ; 
       59 94 300 ; 
       60 95 300 ; 
       61 96 300 ; 
       62 101 300 ; 
       63 102 300 ; 
       64 103 300 ; 
       65 10 300 ; 
       66 104 300 ; 
       67 15 300 ; 
       68 8 300 ; 
       69 77 300 ; 
       70 78 300 ; 
       71 79 300 ; 
       72 80 300 ; 
       73 85 300 ; 
       74 4 300 ; 
       75 6 300 ; 
       76 5 300 ; 
       77 1 300 ; 
       78 2 300 ; 
       79 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       33 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       7 1 401 ; 
       17 3 401 ; 
       18 2 401 ; 
       19 4 401 ; 
       21 5 401 ; 
       22 6 401 ; 
       24 7 401 ; 
       25 8 401 ; 
       27 9 401 ; 
       29 11 401 ; 
       30 12 401 ; 
       31 10 401 ; 
       37 14 401 ; 
       38 13 401 ; 
       39 16 401 ; 
       40 15 401 ; 
       45 17 401 ; 
       46 21 401 ; 
       51 18 401 ; 
       52 22 401 ; 
       57 19 401 ; 
       58 23 401 ; 
       63 20 401 ; 
       64 24 401 ; 
       69 25 401 ; 
       70 27 401 ; 
       71 29 401 ; 
       72 31 401 ; 
       73 26 401 ; 
       74 28 401 ; 
       75 30 401 ; 
       76 32 401 ; 
       109 33 401 ; 
       110 34 401 ; 
       111 35 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 177.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 170 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 180 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 172.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 182.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 175 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 143.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 83.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 83.75 -8 0 MPRFLG 0 ; 
       6 SCHEM 43.75 -6 0 MPRFLG 0 ; 
       7 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       8 SCHEM 63.75 -6 0 MPRFLG 0 ; 
       9 SCHEM 63.75 -8 0 MPRFLG 0 ; 
       10 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       11 SCHEM 103.75 -8 0 MPRFLG 0 ; 
       12 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       13 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 130 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 132.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 135 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 137.5 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 140 -6 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 142.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 145 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 147.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 150 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 152.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 135 -4 0 WIRECOL 9 7 DISPLAY 3 2 MPRFLG 0 ; 
       27 SCHEM 147.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 155 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 157.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 63.75 -4 0 MPRFLG 0 ; 
       31 SCHEM 60 -2 0 MPRFLG 0 ; 
       32 SCHEM 10 -4 0 MPRFLG 0 ; 
       33 SCHEM 85 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 32.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 50 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 45 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 52.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 72.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 107.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 110 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 105 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM 25 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 112.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 27.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 30 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 87.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 90 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 85 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 92.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 47.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 15 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       55 SCHEM 40 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       56 SCHEM 37.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       57 SCHEM 35 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       58 SCHEM 62.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       59 SCHEM 60 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       60 SCHEM 57.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       61 SCHEM 55 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       62 SCHEM 102.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       63 SCHEM 100 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       64 SCHEM 97.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       65 SCHEM 17.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       66 SCHEM 95 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       67 SCHEM 20 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       68 SCHEM 22.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       69 SCHEM 82.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       70 SCHEM 80 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       71 SCHEM 77.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       72 SCHEM 75 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       73 SCHEM 42.5 -10 0 WIRECOL 8 7 MPRFLG 0 ; 
       74 SCHEM 122.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 125 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 127.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 115 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 117.5 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 120 -2 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 160 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 162.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 165 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 167.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 28.75 -10 0 MPRFLG 0 ; 
       85 SCHEM 88.75 -10 0 MPRFLG 0 ; 
       86 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       87 SCHEM 68.75 -10 0 MPRFLG 0 ; 
       88 SCHEM 108.75 -10 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 93.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 114 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 116.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 119 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 121.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 126.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 124 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 96.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 95 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 90.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 92 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 89 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 94 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 54 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 74 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 114 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 94 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 94 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 94 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 74 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 81.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 79 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 76.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 74 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 89 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 84 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 91.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 41.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 39 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 36.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 34 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 51.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 61.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 59 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 56.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 54 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 64 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 71.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       101 SCHEM 101.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       102 SCHEM 99 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM 96.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       104 SCHEM 94 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       105 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       106 SCHEM 109 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       107 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       108 SCHEM 111.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       109 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       110 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       111 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 92.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 95.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 89.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 91 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 88 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 74 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 114 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 94 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 54 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 74 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 114 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 94 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 94 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 74 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 114 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 114 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 169 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 15 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
