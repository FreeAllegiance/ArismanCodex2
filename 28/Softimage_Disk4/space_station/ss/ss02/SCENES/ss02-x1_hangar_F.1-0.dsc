SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss02-ss02.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 8     
       Utl07-spot1.1-0 ; 
       Utl07-spot1_int.1-0 ROOT ; 
       Utl07-spot2.1-0 ; 
       Utl07-spot2_3.1-0 ; 
       Utl07-spot2_3_int.1-0 ROOT ; 
       Utl07-spot2_int.1-0 ROOT ; 
       x1_hangar_F-light6.1-0 ROOT ; 
       x1_hangar_F-light7.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 68     
       Utl07-mat3.1-0 ; 
       x1_hangar_F-mat10.1-0 ; 
       x1_hangar_F-mat11.1-0 ; 
       x1_hangar_F-mat12.1-0 ; 
       x1_hangar_F-mat13.1-0 ; 
       x1_hangar_F-mat14.1-0 ; 
       x1_hangar_F-mat15.1-0 ; 
       x1_hangar_F-mat16.1-0 ; 
       x1_hangar_F-mat17.1-0 ; 
       x1_hangar_F-mat18.1-0 ; 
       x1_hangar_F-mat19.1-0 ; 
       x1_hangar_F-mat20.1-0 ; 
       x1_hangar_F-mat21.1-0 ; 
       x1_hangar_F-mat22.1-0 ; 
       x1_hangar_F-mat23.1-0 ; 
       x1_hangar_F-mat24.1-0 ; 
       x1_hangar_F-mat25.1-0 ; 
       x1_hangar_F-mat26.1-0 ; 
       x1_hangar_F-mat27.1-0 ; 
       x1_hangar_F-mat28.1-0 ; 
       x1_hangar_F-mat29.1-0 ; 
       x1_hangar_F-mat30.1-0 ; 
       x1_hangar_F-mat31.1-0 ; 
       x1_hangar_F-mat32.1-0 ; 
       x1_hangar_F-mat33.1-0 ; 
       x1_hangar_F-mat34.1-0 ; 
       x1_hangar_F-mat35.1-0 ; 
       x1_hangar_F-mat36.1-0 ; 
       x1_hangar_F-mat37.1-0 ; 
       x1_hangar_F-mat38.1-0 ; 
       x1_hangar_F-mat39.1-0 ; 
       x1_hangar_F-mat4.1-0 ; 
       x1_hangar_F-mat40.1-0 ; 
       x1_hangar_F-mat41.1-0 ; 
       x1_hangar_F-mat42.1-0 ; 
       x1_hangar_F-mat43.1-0 ; 
       x1_hangar_F-mat44.1-0 ; 
       x1_hangar_F-mat45.1-0 ; 
       x1_hangar_F-mat46.1-0 ; 
       x1_hangar_F-mat47.1-0 ; 
       x1_hangar_F-mat48.1-0 ; 
       x1_hangar_F-mat49.1-0 ; 
       x1_hangar_F-mat5.1-0 ; 
       x1_hangar_F-mat50.1-0 ; 
       x1_hangar_F-mat51.1-0 ; 
       x1_hangar_F-mat52.1-0 ; 
       x1_hangar_F-mat53.1-0 ; 
       x1_hangar_F-mat55.1-0 ; 
       x1_hangar_F-mat56.1-0 ; 
       x1_hangar_F-mat57.1-0 ; 
       x1_hangar_F-mat58.1-0 ; 
       x1_hangar_F-mat59.1-0 ; 
       x1_hangar_F-mat6.1-0 ; 
       x1_hangar_F-mat60.1-0 ; 
       x1_hangar_F-mat61.1-0 ; 
       x1_hangar_F-mat62.1-0 ; 
       x1_hangar_F-mat63.1-0 ; 
       x1_hangar_F-mat64.1-0 ; 
       x1_hangar_F-mat65.1-0 ; 
       x1_hangar_F-mat66.1-0 ; 
       x1_hangar_F-mat67.1-0 ; 
       x1_hangar_F-mat68.1-0 ; 
       x1_hangar_F-mat69.1-0 ; 
       x1_hangar_F-mat7.1-0 ; 
       x1_hangar_F-mat70.1-0 ; 
       x1_hangar_F-mat8.1-0 ; 
       x1_hangar_F-mat9.1-0 ; 
       x1_hangar_F-z.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 24     
       ss02-afuselg1.1-0 ; 
       ss02-afuselg2.1-0 ; 
       ss02-alfuselg.1-0 ; 
       ss02-arfuselg.1-0 ; 
       ss02-atantenn.1-0 ; 
       ss02-blndpad.1-0 ; 
       ss02-flantenn.1-0 ; 
       ss02-frantenn.1-0 ; 
       ss02-fuselg.1-0 ; 
       ss02-ss02.1-0 ROOT ; 
       ss02-SS1.2-0 ; 
       ss02-SS10.1-0 ; 
       ss02-SS11.1-0 ; 
       ss02-SS2.2-0 ; 
       ss02-SS3.1-0 ; 
       ss02-SS4.2-0 ; 
       ss02-SS5.1-0 ; 
       ss02-SS6.1-0 ; 
       ss02-SS8.2-0 ; 
       ss02-SS9.3-0 ; 
       ss02-SSl.3-0 ; 
       ss02-SSr.3-0 ; 
       ss02-SSt.3-0 ; 
       ss02-tlndpad.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss02/PICTURES/ss2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss02-x1_hangar_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       x1_hangar_F-t2d1.1-0 ; 
       x1_hangar_F-t2d10.1-0 ; 
       x1_hangar_F-t2d11.1-0 ; 
       x1_hangar_F-t2d12.1-0 ; 
       x1_hangar_F-t2d13.1-0 ; 
       x1_hangar_F-t2d14.1-0 ; 
       x1_hangar_F-t2d15.1-0 ; 
       x1_hangar_F-t2d16.1-0 ; 
       x1_hangar_F-t2d17.1-0 ; 
       x1_hangar_F-t2d18.1-0 ; 
       x1_hangar_F-t2d19.1-0 ; 
       x1_hangar_F-t2d2.1-0 ; 
       x1_hangar_F-t2d20.1-0 ; 
       x1_hangar_F-t2d21.1-0 ; 
       x1_hangar_F-t2d22.1-0 ; 
       x1_hangar_F-t2d23.1-0 ; 
       x1_hangar_F-t2d24.1-0 ; 
       x1_hangar_F-t2d25.1-0 ; 
       x1_hangar_F-t2d26.1-0 ; 
       x1_hangar_F-t2d27.1-0 ; 
       x1_hangar_F-t2d28.1-0 ; 
       x1_hangar_F-t2d29.1-0 ; 
       x1_hangar_F-t2d3.1-0 ; 
       x1_hangar_F-t2d30.1-0 ; 
       x1_hangar_F-t2d31.1-0 ; 
       x1_hangar_F-t2d32.1-0 ; 
       x1_hangar_F-t2d33.1-0 ; 
       x1_hangar_F-t2d34.1-0 ; 
       x1_hangar_F-t2d35.1-0 ; 
       x1_hangar_F-t2d36.1-0 ; 
       x1_hangar_F-t2d37.1-0 ; 
       x1_hangar_F-t2d38.1-0 ; 
       x1_hangar_F-t2d39.1-0 ; 
       x1_hangar_F-t2d4.1-0 ; 
       x1_hangar_F-t2d40.1-0 ; 
       x1_hangar_F-t2d41.1-0 ; 
       x1_hangar_F-t2d42.1-0 ; 
       x1_hangar_F-t2d43.1-0 ; 
       x1_hangar_F-t2d44.1-0 ; 
       x1_hangar_F-t2d45.1-0 ; 
       x1_hangar_F-t2d46.1-0 ; 
       x1_hangar_F-t2d47.1-0 ; 
       x1_hangar_F-t2d5.1-0 ; 
       x1_hangar_F-t2d6.1-0 ; 
       x1_hangar_F-t2d7.1-0 ; 
       x1_hangar_F-t2d8.1-0 ; 
       x1_hangar_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 8 110 ; 
       11 8 110 ; 
       12 8 110 ; 
       13 8 110 ; 
       14 8 110 ; 
       15 8 110 ; 
       16 8 110 ; 
       17 8 110 ; 
       18 8 110 ; 
       19 8 110 ; 
       20 6 110 ; 
       21 7 110 ; 
       22 4 110 ; 
       0 9 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 9 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       8 9 110 ; 
       23 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 51 300 ; 
       11 59 300 ; 
       12 60 300 ; 
       13 55 300 ; 
       14 61 300 ; 
       15 50 300 ; 
       16 62 300 ; 
       17 64 300 ; 
       18 53 300 ; 
       19 54 300 ; 
       20 56 300 ; 
       21 57 300 ; 
       22 58 300 ; 
       0 31 300 ; 
       0 42 300 ; 
       0 52 300 ; 
       0 63 300 ; 
       0 65 300 ; 
       0 29 300 ; 
       0 30 300 ; 
       0 47 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       2 14 300 ; 
       2 15 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       4 0 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       6 32 300 ; 
       6 33 300 ; 
       6 34 300 ; 
       6 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       7 40 300 ; 
       7 41 300 ; 
       7 43 300 ; 
       7 44 300 ; 
       8 66 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       8 9 300 ; 
       8 10 300 ; 
       8 45 300 ; 
       8 46 300 ; 
       8 67 300 ; 
       8 48 300 ; 
       8 49 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 5 2110 ; 
       3 4 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 42 401 ; 
       2 43 401 ; 
       3 44 401 ; 
       4 45 401 ; 
       5 46 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 19 401 ; 
       27 20 401 ; 
       28 21 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
       35 27 401 ; 
       36 28 401 ; 
       37 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
       41 32 401 ; 
       42 0 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       45 36 401 ; 
       46 37 401 ; 
       47 39 401 ; 
       48 40 401 ; 
       49 41 401 ; 
       52 11 401 ; 
       63 22 401 ; 
       65 33 401 ; 
       67 38 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 0 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 10 -12 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 0 -8 0 MPRFLG 0 ; 
       11 SCHEM 30 -8 0 MPRFLG 0 ; 
       12 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 5 -8 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       16 SCHEM 10 -8 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 20 -8 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       20 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 15 -10 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       0 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 35 -8 0 MPRFLG 0 ; 
       2 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       3 SCHEM 40 -8 0 MPRFLG 0 ; 
       4 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 25 -8 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 15 -8 0 MPRFLG 0 ; 
       8 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       9 SCHEM 21.25 -4 0 SRT 1 1 1 0 0 0 0 -0.08429135 -9.001249 MPRFLG 0 ; 
       23 SCHEM 22.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 36.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 39 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 34 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 26.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 41.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 29 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 31.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 36.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 39 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 44 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
