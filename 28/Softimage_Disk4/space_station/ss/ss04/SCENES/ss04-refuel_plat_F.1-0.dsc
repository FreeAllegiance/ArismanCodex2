SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss04-ss04.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       refuel_plat_F-cam_int1.1-0 ROOT ; 
       refuel_plat_F-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       refuel_plat_F-light1.1-0 ROOT ; 
       refuel_plat_F-light1_1.1-0 ROOT ; 
       refuel_plat_F-light2.1-0 ROOT ; 
       refuel_plat_F-light2_1.1-0 ROOT ; 
       refuel_plat_F-light3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 46     
       refuel_plat_F-default10.1-0 ; 
       refuel_plat_F-default13.1-0 ; 
       refuel_plat_F-mat12.1-0 ; 
       refuel_plat_F-mat13.1-0 ; 
       refuel_plat_F-mat14.1-0 ; 
       refuel_plat_F-mat15.1-0 ; 
       refuel_plat_F-mat17.1-0 ; 
       refuel_plat_F-mat18.1-0 ; 
       refuel_plat_F-mat30.1-0 ; 
       refuel_plat_F-mat31.1-0 ; 
       refuel_plat_F-mat32.1-0 ; 
       refuel_plat_F-mat33.1-0 ; 
       refuel_plat_F-mat35.1-0 ; 
       refuel_plat_F-mat36.1-0 ; 
       refuel_plat_F-mat39.1-0 ; 
       refuel_plat_F-mat4.1-0 ; 
       refuel_plat_F-mat48.1-0 ; 
       refuel_plat_F-mat49.1-0 ; 
       refuel_plat_F-mat5.1-0 ; 
       refuel_plat_F-mat52.1-0 ; 
       refuel_plat_F-mat53.1-0 ; 
       refuel_plat_F-mat54.1-0 ; 
       refuel_plat_F-mat55.1-0 ; 
       refuel_plat_F-mat56.1-0 ; 
       refuel_plat_F-mat57.1-0 ; 
       refuel_plat_F-mat58.1-0 ; 
       refuel_plat_F-mat59.1-0 ; 
       refuel_plat_F-mat6.1-0 ; 
       refuel_plat_F-mat60.1-0 ; 
       refuel_plat_F-mat61.1-0 ; 
       refuel_plat_F-mat62.1-0 ; 
       refuel_plat_F-mat63.1-0 ; 
       refuel_plat_F-mat64.1-0 ; 
       refuel_plat_F-mat65.1-0 ; 
       refuel_plat_F-mat66.1-0 ; 
       refuel_plat_F-mat67.1-0 ; 
       refuel_plat_F-mat68.1-0 ; 
       refuel_plat_F-mat69.1-0 ; 
       refuel_plat_F-mat7.1-0 ; 
       refuel_plat_F-mat70.1-0 ; 
       refuel_plat_F-mat71.1-0 ; 
       refuel_plat_F-mat72.1-0 ; 
       refuel_plat_F-mat73.1-0 ; 
       refuel_plat_F-mat74.1-0 ; 
       refuel_plat_F-mat8.1-0 ; 
       refuel_plat_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       ss04-afuselg.1-0 ; 
       ss04-antenn1.1-0 ; 
       ss04-antenn2.1-0 ; 
       ss04-antenn3.1-0 ; 
       ss04-antenn4.1-0 ; 
       ss04-antenn5.1-0 ; 
       ss04-antenn6.1-0 ; 
       ss04-antenn7.1-0 ; 
       ss04-antenn8.1-0 ; 
       ss04-bfuselg1.1-0 ; 
       ss04-bfuselg2.1-0 ; 
       ss04-ffuselg.1-0 ; 
       ss04-fuselg.1-0 ; 
       ss04-lfuselg.1-0 ; 
       ss04-lndpad1.2-0 ; 
       ss04-lndpad2.2-0 ; 
       ss04-mfuselg.1-0 ; 
       ss04-rfuselg.1-0 ; 
       ss04-rlfuselg.1-0 ; 
       ss04-SS0.1-0 ; 
       ss04-ss04.1-0 ROOT ; 
       ss04-SS1.4-0 ; 
       ss04-SS11.2-0 ; 
       ss04-SS12.2-0 ; 
       ss04-SS13.2-0 ; 
       ss04-SS14.2-0 ; 
       ss04-SS15.2-0 ; 
       ss04-SS16.1-0 ; 
       ss04-SS2.4-0 ; 
       ss04-SS3.4-0 ; 
       ss04-SS4.1-0 ; 
       ss04-SS5.3-0 ; 
       ss04-SS6.3-0 ; 
       ss04-SS7.3-0 ; 
       ss04-SS8.3-0 ; 
       ss04-SS9.3-0 ; 
       ss04-tfuselg.1-0 ; 
       ss04-tmfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       D:/Pete_Data/Softimage/space_station/ss/ss04/PICTURES/ss ; 
       D:/Pete_Data/Softimage/space_station/ss/ss04/PICTURES/ss4 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss04-refuel_plat_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       refuel_plat_F-t2d10.1-0 ; 
       refuel_plat_F-t2d11.1-0 ; 
       refuel_plat_F-t2d12.1-0 ; 
       refuel_plat_F-t2d13.1-0 ; 
       refuel_plat_F-t2d14.1-0 ; 
       refuel_plat_F-t2d22.1-0 ; 
       refuel_plat_F-t2d23.1-0 ; 
       refuel_plat_F-t2d24.1-0 ; 
       refuel_plat_F-t2d25.1-0 ; 
       refuel_plat_F-t2d27.1-0 ; 
       refuel_plat_F-t2d28.1-0 ; 
       refuel_plat_F-t2d29.1-0 ; 
       refuel_plat_F-t2d3.1-0 ; 
       refuel_plat_F-t2d30.1-0 ; 
       refuel_plat_F-t2d31.1-0 ; 
       refuel_plat_F-t2d32.1-0 ; 
       refuel_plat_F-t2d33.1-0 ; 
       refuel_plat_F-t2d34.1-0 ; 
       refuel_plat_F-t2d35.1-0 ; 
       refuel_plat_F-t2d4.1-0 ; 
       refuel_plat_F-t2d44.1-0 ; 
       refuel_plat_F-t2d45.1-0 ; 
       refuel_plat_F-t2d46.1-0 ; 
       refuel_plat_F-t2d47.1-0 ; 
       refuel_plat_F-t2d48.1-0 ; 
       refuel_plat_F-t2d49.1-0 ; 
       refuel_plat_F-t2d5.1-0 ; 
       refuel_plat_F-t2d50.1-0 ; 
       refuel_plat_F-t2d53.1-0 ; 
       refuel_plat_F-t2d54.1-0 ; 
       refuel_plat_F-t2d57.1-0 ; 
       refuel_plat_F-t2d58.1-0 ; 
       refuel_plat_F-t2d59.1-0 ; 
       refuel_plat_F-t2d6.1-0 ; 
       refuel_plat_F-t2d60.1-0 ; 
       refuel_plat_F-t2d61.1-0 ; 
       refuel_plat_F-t2d62.1-0 ; 
       refuel_plat_F-t2d63.1-0 ; 
       refuel_plat_F-t2d64.1-0 ; 
       refuel_plat_F-t2d65.1-0 ; 
       refuel_plat_F-t2d66.1-0 ; 
       refuel_plat_F-t2d67.1-0 ; 
       refuel_plat_F-t2d68.1-0 ; 
       refuel_plat_F-t2d69.1-0 ; 
       refuel_plat_F-t2d7.1-0 ; 
       refuel_plat_F-t2d70.1-0 ; 
       refuel_plat_F-t2d71.1-0 ; 
       refuel_plat_F-t2d72.1-0 ; 
       refuel_plat_F-t2d73.1-0 ; 
       refuel_plat_F-t2d74.1-0 ; 
       refuel_plat_F-t2d75.1-0 ; 
       refuel_plat_F-t2d76.1-0 ; 
       refuel_plat_F-t2d77.1-0 ; 
       refuel_plat_F-t2d78.1-0 ; 
       refuel_plat_F-t2d79.1-0 ; 
       refuel_plat_F-t2d8.1-0 ; 
       refuel_plat_F-t2d80.1-0 ; 
       refuel_plat_F-t2d81.1-0 ; 
       refuel_plat_F-t2d82.1-0 ; 
       refuel_plat_F-t2d83.1-0 ; 
       refuel_plat_F-t2d84.1-0 ; 
       refuel_plat_F-t2d85.1-0 ; 
       refuel_plat_F-t2d86.1-0 ; 
       refuel_plat_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       19 14 110 ; 
       21 2 110 ; 
       22 27 110 ; 
       23 27 110 ; 
       24 27 110 ; 
       25 27 110 ; 
       26 27 110 ; 
       27 15 110 ; 
       28 4 110 ; 
       29 6 110 ; 
       30 8 110 ; 
       31 19 110 ; 
       32 19 110 ; 
       33 19 110 ; 
       34 19 110 ; 
       35 19 110 ; 
       0 16 110 ; 
       1 36 110 ; 
       2 1 110 ; 
       3 36 110 ; 
       4 3 110 ; 
       5 36 110 ; 
       6 5 110 ; 
       7 36 110 ; 
       8 7 110 ; 
       9 12 110 ; 
       10 9 110 ; 
       11 16 110 ; 
       12 20 110 ; 
       13 18 110 ; 
       14 36 110 ; 
       15 36 110 ; 
       16 12 110 ; 
       17 18 110 ; 
       18 12 110 ; 
       36 12 110 ; 
       37 36 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       21 37 300 ; 
       22 39 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 42 300 ; 
       26 43 300 ; 
       28 36 300 ; 
       29 35 300 ; 
       30 34 300 ; 
       31 30 300 ; 
       32 29 300 ; 
       33 32 300 ; 
       34 33 300 ; 
       35 31 300 ; 
       0 1 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       1 1 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 28 300 ; 
       2 0 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       3 1 300 ; 
       3 25 300 ; 
       3 26 300 ; 
       3 28 300 ; 
       4 0 300 ; 
       4 23 300 ; 
       4 24 300 ; 
       5 1 300 ; 
       5 25 300 ; 
       5 26 300 ; 
       5 28 300 ; 
       6 0 300 ; 
       6 23 300 ; 
       6 24 300 ; 
       7 1 300 ; 
       7 25 300 ; 
       7 26 300 ; 
       7 28 300 ; 
       8 0 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       9 1 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 12 300 ; 
       9 19 300 ; 
       9 22 300 ; 
       10 12 300 ; 
       11 1 300 ; 
       11 8 300 ; 
       11 9 300 ; 
       11 10 300 ; 
       11 11 300 ; 
       12 1 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 20 300 ; 
       12 21 300 ; 
       13 12 300 ; 
       13 13 300 ; 
       17 12 300 ; 
       17 13 300 ; 
       36 1 300 ; 
       36 2 300 ; 
       36 3 300 ; 
       36 4 300 ; 
       36 5 300 ; 
       36 14 300 ; 
       37 1 300 ; 
       37 15 300 ; 
       37 18 300 ; 
       37 27 300 ; 
       37 38 300 ; 
       37 44 300 ; 
       37 45 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 37 400 ; 
       2 41 400 ; 
       3 28 400 ; 
       4 29 400 ; 
       5 54 400 ; 
       6 59 400 ; 
       7 46 400 ; 
       8 50 400 ; 
       10 20 400 ; 
       13 11 400 ; 
       17 9 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       20 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 30 401 ; 
       2 63 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       13 10 401 ; 
       14 14 401 ; 
       15 12 401 ; 
       16 21 401 ; 
       17 22 401 ; 
       18 19 401 ; 
       19 23 401 ; 
       20 24 401 ; 
       21 25 401 ; 
       22 27 401 ; 
       23 31 401 ; 
       24 32 401 ; 
       25 34 401 ; 
       26 35 401 ; 
       27 26 401 ; 
       28 36 401 ; 
       38 33 401 ; 
       44 44 401 ; 
       45 55 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 50.57242 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 48.07242 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 53.07242 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 45.57242 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 43.07242 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       19 SCHEM 23.07242 -12 0 MPRFLG 0 ; 
       21 SCHEM 5.18206 -14 0 MPRFLG 0 ; 
       22 SCHEM 30.57242 -14 0 MPRFLG 0 ; 
       23 SCHEM 33.07242 -14 0 MPRFLG 0 ; 
       24 SCHEM 35.57242 -14 0 MPRFLG 0 ; 
       25 SCHEM 38.07242 -14 0 MPRFLG 0 ; 
       26 SCHEM 40.57242 -14 0 MPRFLG 0 ; 
       27 SCHEM 35.57242 -12 0 MPRFLG 0 ; 
       28 SCHEM 8.70207 -14 0 MPRFLG 0 ; 
       29 SCHEM 12.15858 -14 0 MPRFLG 0 ; 
       30 SCHEM 15.57211 -14 0 USR MPRFLG 0 ; 
       31 SCHEM 18.07242 -14 0 MPRFLG 0 ; 
       32 SCHEM 20.57242 -14 0 MPRFLG 0 ; 
       33 SCHEM 23.07242 -14 0 MPRFLG 0 ; 
       34 SCHEM 25.57242 -14 0 MPRFLG 0 ; 
       35 SCHEM 28.07242 -14 0 MPRFLG 0 ; 
       0 SCHEM 22.41047 -10 0 USR MPRFLG 0 ; 
       1 SCHEM 5.39607 -10 0 USR MPRFLG 0 ; 
       2 SCHEM 5.18206 -12 0 USR MPRFLG 0 ; 
       3 SCHEM 8.76807 -10 0 USR MPRFLG 0 ; 
       4 SCHEM 8.70207 -12 0 USR MPRFLG 0 ; 
       5 SCHEM 12.21259 -10 0 USR MPRFLG 0 ; 
       6 SCHEM 12.15858 -12 0 USR MPRFLG 0 ; 
       7 SCHEM 15.57242 -10 0 USR MPRFLG 0 ; 
       8 SCHEM 15.52663 -12 0 USR MPRFLG 0 ; 
       9 SCHEM 30.88261 -8 0 USR MPRFLG 0 ; 
       10 SCHEM 30.91454 -10 0 USR MPRFLG 0 ; 
       11 SCHEM 19.71243 -10 0 USR MPRFLG 0 ; 
       12 SCHEM 17.7363 -6 0 MPRFLG 0 ; 
       13 SCHEM 28.11853 -10 0 USR MPRFLG 0 ; 
       14 SCHEM 23.07242 -10 0 MPRFLG 0 ; 
       15 SCHEM 35.57242 -10 0 MPRFLG 0 ; 
       16 SCHEM 19.77845 -8 0 USR MPRFLG 0 ; 
       17 SCHEM 25.33851 -10 0 USR MPRFLG 0 ; 
       18 SCHEM 25.25652 -8 0 USR MPRFLG 0 ; 
       20 SCHEM 17.7363 -4 0 SRT 1 1 1 -1.570796 0 0 0 -18.93432 0 MPRFLG 0 ; 
       36 SCHEM 15.5681 -8 0 USR MPRFLG 0 ; 
       37 SCHEM 2.5 -10 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.68206 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.41047 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 42.07242 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.07242 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 42.07242 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 42.07242 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 32.38261 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 32.38261 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 21.41047 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.41047 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 21.41047 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 21.41047 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 32.41454 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 27.11853 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 42.07242 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.41454 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 32.41454 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 32.41454 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 32.38261 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 32.38261 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 32.41454 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 6.68206 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.68206 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.68206 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 6.68206 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 6.68206 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19.57242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 17.07242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 27.07242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 22.07242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24.57242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14.57211 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 11.15858 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 7.70207 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4.18206 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29.57242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 32.07242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 34.57242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 37.07242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 39.57242 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 1.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 42.07242 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 42.07242 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 42.07242 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 32.38261 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 32.38261 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 21.41047 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 21.41047 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 21.41047 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 21.41047 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24.33851 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 27.11853 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 27.11853 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 0 -135.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 42.07242 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 0 -137.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 0 -139.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 0 -141.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 0 -143.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 29.91454 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 32.41454 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 32.41454 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 32.41454 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 32.38261 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 32.38261 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 32.41454 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 10.20207 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 10.20207 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 6.68206 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 6.68206 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 6.68206 -16 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 6.68206 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 6.68206 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 6.68206 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 6.68206 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 0 -145.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 0 -147.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 0 -149.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 6.68206 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 0 -151.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 0 -153.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 0 -155.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 17.02663 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 0 -157.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 0 -159.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 0 -161.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 17.07211 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 0 -163.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 0 -165.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 0 -167.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 13.65858 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 0 -169.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 0 -171.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 0 -173.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 13.65858 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 0 -175.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 0 -177.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 0 -179.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 42.07242 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 25 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
