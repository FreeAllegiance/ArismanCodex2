SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss05-ss05_2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       sm_gate-cam_int1.1-0 ROOT ; 
       sm_gate-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 6     
       Utl07-spot1.1-0 ; 
       Utl07-spot1_int_2.1-0 ROOT ; 
       Utl07-spot2.1-0 ; 
       Utl07-spot2_3.1-0 ; 
       Utl07-spot2_3_int_2.1-0 ROOT ; 
       Utl07-spot2_int_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       platformx4_F-mat1.1-0 ; 
       platformx4_F-mat11.1-0 ; 
       platformx4_F-mat12.1-0 ; 
       platformx4_F-mat13.1-0 ; 
       platformx4_F-mat2.1-0 ; 
       platformx4_F-mat28.1-0 ; 
       platformx4_F-mat29.1-0 ; 
       platformx4_F-mat3.1-0 ; 
       platformx4_F-mat30.1-0 ; 
       platformx4_F-mat31.1-0 ; 
       platformx4_F-mat32.1-0 ; 
       platformx4_F-mat33.1-0 ; 
       platformx4_F-mat34.1-0 ; 
       platformx4_F-mat38.1-0 ; 
       platformx4_F-mat39.1-0 ; 
       platformx4_F-mat4.1-0 ; 
       platformx4_F-mat40.1-0 ; 
       platformx4_F-mat41.1-0 ; 
       platformx4_F-mat42.1-0 ; 
       platformx4_F-mat43.1-0 ; 
       platformx4_F-mat44.1-0 ; 
       platformx4_F-mat45.1-0 ; 
       platformx4_F-mat46.1-0 ; 
       platformx4_F-mat47.1-0 ; 
       platformx4_F-mat48.1-0 ; 
       platformx4_F-mat49.1-0 ; 
       platformx4_F-mat5.1-0 ; 
       platformx4_F-mat50.1-0 ; 
       platformx4_F-mat51.1-0 ; 
       platformx4_F-mat52.1-0 ; 
       platformx4_F-mat53.1-0 ; 
       platformx4_F-mat54.1-0 ; 
       platformx4_F-mat55.1-0 ; 
       platformx4_F-mat56.1-0 ; 
       platformx4_F-mat57.1-0 ; 
       platformx4_F-mat58.1-0 ; 
       platformx4_F-mat59.1-0 ; 
       platformx4_F-mat6.1-0 ; 
       platformx4_F-mat60.1-0 ; 
       platformx4_F-mat7.1-0 ; 
       platformx4_F-mat8.1-0 ; 
       platformx4_F-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       ss05-bfuselg0.1-0 ; 
       ss05-bfuselg1.1-0 ; 
       ss05-bfuselg2.1-0 ; 
       ss05-bmfuselg.1-0 ; 
       ss05-fuselg.1-0 ; 
       ss05-fuselg3.1-0 ; 
       ss05-fuselg4.1-0 ; 
       ss05-lantenn.1-0 ; 
       ss05-lndpad1.1-0 ; 
       ss05-lndpad2.1-0 ; 
       ss05-lndpad3.1-0 ; 
       ss05-lndpad4.1-0 ; 
       ss05-rantenn.1-0 ; 
       ss05-ss05_2.1-0 ROOT ; 
       ss05-SS1.1-0 ; 
       ss05-SS2.1-0 ; 
       ss05-SSt1.2-0 ; 
       ss05-SSt10.1-0 ; 
       ss05-SSt11.1-0 ; 
       ss05-SSt12.1-0 ; 
       ss05-SSt13.1-0 ; 
       ss05-SSt14.1-0 ; 
       ss05-SSt15.1-0 ; 
       ss05-SSt16.1-0 ; 
       ss05-SSt17.1-0 ; 
       ss05-SSt18.1-0 ; 
       ss05-SSt19.1-0 ; 
       ss05-SSt2.3-0 ; 
       ss05-SSt20.1-0 ; 
       ss05-SSt21.1-0 ; 
       ss05-SSt22.1-0 ; 
       ss05-SSt23.1-0 ; 
       ss05-SSt24.1-0 ; 
       ss05-SSt3.2-0 ; 
       ss05-SSt4.2-0 ; 
       ss05-SSt5.1-0 ; 
       ss05-SSt6.1-0 ; 
       ss05-SSt7.1-0 ; 
       ss05-SSt8.1-0 ; 
       ss05-SSt9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       D:/Pete_Data/Softimage/space_station/ss/ss05/PICTURES/ss ; 
       D:/Pete_Data/Softimage/space_station/ss/ss05/PICTURES/ss4 ; 
       D:/Pete_Data/Softimage/space_station/ss/ss05/PICTURES/ss5 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss05-platformx4_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 29     
       platformx4_F-t2d1.1-0 ; 
       platformx4_F-t2d10.1-0 ; 
       platformx4_F-t2d11.1-0 ; 
       platformx4_F-t2d12.1-0 ; 
       platformx4_F-t2d13.1-0 ; 
       platformx4_F-t2d14.1-0 ; 
       platformx4_F-t2d15.1-0 ; 
       platformx4_F-t2d16.1-0 ; 
       platformx4_F-t2d17.1-0 ; 
       platformx4_F-t2d18.1-0 ; 
       platformx4_F-t2d19.1-0 ; 
       platformx4_F-t2d2.1-0 ; 
       platformx4_F-t2d20.1-0 ; 
       platformx4_F-t2d21.1-0 ; 
       platformx4_F-t2d22.1-0 ; 
       platformx4_F-t2d23.1-0 ; 
       platformx4_F-t2d24.1-0 ; 
       platformx4_F-t2d25.1-0 ; 
       platformx4_F-t2d26.1-0 ; 
       platformx4_F-t2d27.1-0 ; 
       platformx4_F-t2d28.1-0 ; 
       platformx4_F-t2d29.1-0 ; 
       platformx4_F-t2d3.1-0 ; 
       platformx4_F-t2d4.1-0 ; 
       platformx4_F-t2d5.1-0 ; 
       platformx4_F-t2d6.1-0 ; 
       platformx4_F-t2d7.1-0 ; 
       platformx4_F-t2d8.1-0 ; 
       platformx4_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       14 7 110 ; 
       15 12 110 ; 
       16 8 110 ; 
       17 27 110 ; 
       18 27 110 ; 
       19 27 110 ; 
       20 27 110 ; 
       21 27 110 ; 
       22 33 110 ; 
       23 33 110 ; 
       24 33 110 ; 
       25 33 110 ; 
       26 33 110 ; 
       27 9 110 ; 
       28 34 110 ; 
       29 34 110 ; 
       30 34 110 ; 
       31 34 110 ; 
       32 34 110 ; 
       33 10 110 ; 
       34 11 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 16 110 ; 
       39 16 110 ; 
       0 13 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 13 110 ; 
       4 13 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 13 110 ; 
       8 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 13 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       14 14 300 ; 
       15 16 300 ; 
       17 22 300 ; 
       18 23 300 ; 
       19 24 300 ; 
       20 25 300 ; 
       21 27 300 ; 
       22 28 300 ; 
       23 29 300 ; 
       24 30 300 ; 
       25 31 300 ; 
       26 32 300 ; 
       28 33 300 ; 
       29 34 300 ; 
       30 35 300 ; 
       31 36 300 ; 
       32 38 300 ; 
       35 17 300 ; 
       36 18 300 ; 
       37 19 300 ; 
       38 20 300 ; 
       39 21 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       3 0 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 8 300 ; 
       3 9 300 ; 
       3 13 300 ; 
       4 0 300 ; 
       4 4 300 ; 
       4 7 300 ; 
       4 15 300 ; 
       4 26 300 ; 
       4 37 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       4 41 300 ; 
       5 0 300 ; 
       5 1 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       6 3 300 ; 
       7 0 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       12 10 300 ; 
       12 11 300 ; 
       12 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       13 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       0 1 2110 ; 
       2 5 2110 ; 
       3 4 2110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 28 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 0 401 ; 
       5 13 401 ; 
       6 14 401 ; 
       7 11 401 ; 
       8 15 401 ; 
       9 16 401 ; 
       11 17 401 ; 
       12 18 401 ; 
       13 21 401 ; 
       15 22 401 ; 
       26 23 401 ; 
       37 24 401 ; 
       39 25 401 ; 
       40 26 401 ; 
       41 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 70 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 70 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 75 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 72.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 72.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       5 SCHEM 75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       14 SCHEM 2.415019 -6.323053 0 USR MPRFLG 0 ; 
       15 SCHEM 4.865188 -6.123701 0 USR MPRFLG 0 ; 
       16 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 30 -10 0 MPRFLG 0 ; 
       18 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       19 SCHEM 35 -10 0 MPRFLG 0 ; 
       20 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 40 -10 0 MPRFLG 0 ; 
       22 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       23 SCHEM 45 -10 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       25 SCHEM 50 -10 0 MPRFLG 0 ; 
       26 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 35 -8 0 MPRFLG 0 ; 
       28 SCHEM 55 -10 0 MPRFLG 0 ; 
       29 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       30 SCHEM 60 -10 0 MPRFLG 0 ; 
       31 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       32 SCHEM 65 -10 0 MPRFLG 0 ; 
       33 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 60 -8 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 20 -10 0 MPRFLG 0 ; 
       37 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       38 SCHEM 25 -10 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       4 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 15 -6 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 35 -6 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 60 -6 0 MPRFLG 0 ; 
       12 SCHEM 5 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -2 0 SRT 1 1 1 -1.570796 0 0 0 -3.901442 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 21.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 77.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 80 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 82.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 85 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 87.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 90 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 92.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 95 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 97.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 100 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 102.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 105 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 107.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 110 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 112.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 115 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 117.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 120 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 122.5 0 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 28.5 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 28.5 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 28.5 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 28.5 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 31 -0.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 31 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 31 -4.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 31 -6.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 31 -8.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 33.5 -0.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 33.5 -2.5 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 66.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 0 0 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
