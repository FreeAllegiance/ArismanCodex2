SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       x4_hub_fuel_F-ss06.16-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.16-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       x4_hub_fuel_F-light3.16-0 ROOT ; 
       x4_hub_fuel_F-light4.16-0 ROOT ; 
       x4_hub_fuel_F-light5.16-0 ROOT ; 
       x4_hub_fuel_F-light6.16-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       retext-mat100.1-0 ; 
       retext-mat101.1-0 ; 
       retext-mat102.1-0 ; 
       retext-mat103.1-0 ; 
       retext-mat104.1-0 ; 
       retext-mat93.5-0 ; 
       retext-mat94.4-0 ; 
       retext-mat95.3-0 ; 
       retext-mat97.1-0 ; 
       retext-mat98.1-0 ; 
       retext-mat99.1-0 ; 
       sm_509_mod-METALS-GRAPHITE.1-0.2-0 ; 
       x4_hub_fuel_F-mat38.2-0 ; 
       x4_hub_fuel_F-mat67.3-0 ; 
       x4_hub_fuel_F-mat8.6-0 ; 
       x4_hub_fuel_F-mat90.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       x4_hub_fuel_F-fuselg.1-0 ; 
       x4_hub_fuel_F-lndpad1.1-0 ; 
       x4_hub_fuel_F-lndpad11.1-0 ; 
       x4_hub_fuel_F-lndpad12.1-0 ; 
       x4_hub_fuel_F-lndpad13.1-0 ; 
       x4_hub_fuel_F-manteen2.1-0 ; 
       x4_hub_fuel_F-mantenn1.1-0 ; 
       x4_hub_fuel_F-ranteen.1-0 ; 
       x4_hub_fuel_F-ranteen1.1-0 ; 
       x4_hub_fuel_F-ss06.14-0 ROOT ; 
       x4_hub_fuel_F-SS10.1-0 ; 
       x4_hub_fuel_F-SS11.1-0 ; 
       x4_hub_fuel_F-SS12.1-0 ; 
       x4_hub_fuel_F-SS13.1-0 ; 
       x4_hub_fuel_F-SS14.1-0 ; 
       x4_hub_fuel_F-SS15.1-0 ; 
       x4_hub_fuel_F-SS16.1-0 ; 
       x4_hub_fuel_F-SS17.1-0 ; 
       x4_hub_fuel_F-SS18.1-0 ; 
       x4_hub_fuel_F-SS19.1-0 ; 
       x4_hub_fuel_F-SS2.1-0 ; 
       x4_hub_fuel_F-SS20.1-0 ; 
       x4_hub_fuel_F-SS21.1-0 ; 
       x4_hub_fuel_F-SS22.1-0 ; 
       x4_hub_fuel_F-SS23.1-0 ; 
       x4_hub_fuel_F-SS24.1-0 ; 
       x4_hub_fuel_F-SS25.1-0 ; 
       x4_hub_fuel_F-SS26.1-0 ; 
       x4_hub_fuel_F-SS27.1-0 ; 
       x4_hub_fuel_F-SS28.1-0 ; 
       x4_hub_fuel_F-SS3.1-0 ; 
       x4_hub_fuel_F-SS4.1-0 ; 
       x4_hub_fuel_F-SS5.1-0 ; 
       x4_hub_fuel_F-SS6.1-0 ; 
       x4_hub_fuel_F-SS7.1-0 ; 
       x4_hub_fuel_F-SS8.1-0 ; 
       x4_hub_fuel_F-SS9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       E:/Pete_Data2/space_station/ss/ss06/PICTURES/ss06 ; 
       E:/pete_data2/space_station/ss/ss06/PICTURES/ss6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss06-new_retext.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       retext-t2d54.9-0 ; 
       retext-t2d55.8-0 ; 
       retext-t2d56.7-0 ; 
       retext-t2d58.5-0 ; 
       retext-t2d59.5-0 ; 
       retext-t2d60.5-0 ; 
       retext-t2d61.2-0 ; 
       retext-t2d62.2-0 ; 
       retext-t2d63.2-0 ; 
       retext-t2d64.2-0 ; 
       x4_hub_fuel_F-t2d28.4-0 ; 
       x4_hub_fuel_F-t2d53.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 6 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       10 4 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 2 110 ; 
       17 16 110 ; 
       18 16 110 ; 
       19 16 110 ; 
       20 6 110 ; 
       21 16 110 ; 
       22 16 110 ; 
       23 3 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 8 110 ; 
       30 7 110 ; 
       31 1 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       5 12 300 ; 
       6 11 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       11 13 300 ; 
       12 13 300 ; 
       13 13 300 ; 
       14 13 300 ; 
       15 13 300 ; 
       17 13 300 ; 
       18 13 300 ; 
       19 13 300 ; 
       20 13 300 ; 
       21 13 300 ; 
       22 13 300 ; 
       24 13 300 ; 
       25 13 300 ; 
       26 13 300 ; 
       27 13 300 ; 
       28 13 300 ; 
       29 4 300 ; 
       30 15 300 ; 
       32 13 300 ; 
       33 13 300 ; 
       34 13 300 ; 
       35 13 300 ; 
       36 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       5 11 400 ; 
       6 10 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 6 401 ; 
       2 8 401 ; 
       3 9 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 97.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 100 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 102.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 105 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 48.75 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 15 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -4 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 61.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 75 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 -4 0 MPRFLG 0 ; 
       9 SCHEM 48.75 0 0 SRT 1 1 1 -1.570796 0 0 0 -18.96012 0 MPRFLG 0 ; 
       10 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       13 SCHEM 50 -8 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 25 -8 0 MPRFLG 0 ; 
       20 SCHEM 65 -6 0 MPRFLG 0 ; 
       21 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       22 SCHEM 30 -8 0 MPRFLG 0 ; 
       23 SCHEM 40 -6 0 MPRFLG 0 ; 
       24 SCHEM 45 -8 0 MPRFLG 0 ; 
       25 SCHEM 35 -8 0 MPRFLG 0 ; 
       26 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 40 -8 0 MPRFLG 0 ; 
       28 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       29 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       30 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       31 SCHEM 15 -6 0 MPRFLG 0 ; 
       32 SCHEM 20 -8 0 MPRFLG 0 ; 
       33 SCHEM 10 -8 0 MPRFLG 0 ; 
       34 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       35 SCHEM 15 -8 0 MPRFLG 0 ; 
       36 SCHEM 17.5 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 87.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 90 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 92.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 36.03148 -21.73319 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 95 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 80 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 82.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 85 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 87.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 90 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 92.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 77.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 67.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 96.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 2 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
