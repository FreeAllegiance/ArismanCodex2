SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       x4_hub_fuel_F-ss06.14-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.14-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       x4_hub_fuel_F-light3.14-0 ROOT ; 
       x4_hub_fuel_F-light4.14-0 ROOT ; 
       x4_hub_fuel_F-light5.14-0 ROOT ; 
       x4_hub_fuel_F-light6.14-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 14     
       sm_509_mod-METALS-GRAPHITE.1-0.2-0 ; 
       static-mat100.1-0 ; 
       static-mat101.1-0 ; 
       static-mat102.1-0 ; 
       static-mat103.1-0 ; 
       static-mat104.1-0 ; 
       static-mat93.1-0 ; 
       static-mat94.1-0 ; 
       static-mat95.1-0 ; 
       static-mat97.1-0 ; 
       static-mat98.1-0 ; 
       static-mat99.1-0 ; 
       x4_hub_fuel_F-mat38.2-0 ; 
       x4_hub_fuel_F-mat8.6-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       x4_hub_fuel_F-fuselg.1-0 ; 
       x4_hub_fuel_F-manteen2.1-0 ; 
       x4_hub_fuel_F-mantenn1.1-0 ; 
       x4_hub_fuel_F-ranteen.1-0 ; 
       x4_hub_fuel_F-ranteen1.1-0 ; 
       x4_hub_fuel_F-ss06.12-0 ROOT ; 
       x4_hub_fuel_F-SS28.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/pete_data2/space_station/ss/ss06/PICTURES/ss6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss06-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       static-t2d54.1-0 ; 
       static-t2d55.1-0 ; 
       static-t2d56.1-0 ; 
       static-t2d58.1-0 ; 
       static-t2d59.1-0 ; 
       static-t2d60.1-0 ; 
       static-t2d61.1-0 ; 
       static-t2d62.1-0 ; 
       static-t2d63.1-0 ; 
       static-t2d64.1-0 ; 
       x4_hub_fuel_F-t2d28.3-0 ; 
       x4_hub_fuel_F-t2d53.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       4 0 110 ; 
       1 2 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       6 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       1 12 300 ; 
       2 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       6 5 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 11 400 ; 
       2 10 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       11 5 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       1 7 401 ; 
       2 6 401 ; 
       3 8 401 ; 
       4 9 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 10 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 5 0 0 SRT 1 1 1 -1.570796 0 0 0 -18.96012 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 2 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
