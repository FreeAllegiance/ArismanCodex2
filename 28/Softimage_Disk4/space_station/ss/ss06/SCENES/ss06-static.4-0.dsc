SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       x4_hub_fuel_F-ss06.20-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.20-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       x4_hub_fuel_F-light3.20-0 ROOT ; 
       x4_hub_fuel_F-light4.20-0 ROOT ; 
       x4_hub_fuel_F-light5.20-0 ROOT ; 
       x4_hub_fuel_F-light6.20-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       retext-mat100.1-0 ; 
       retext-mat101.1-0 ; 
       retext-mat102.1-0 ; 
       retext-mat103.1-0 ; 
       retext-mat104.1-0 ; 
       retext-mat93.5-0 ; 
       retext-mat94.4-0 ; 
       retext-mat95.3-0 ; 
       retext-mat97.1-0 ; 
       retext-mat98.1-0 ; 
       retext-mat99.1-0 ; 
       x4_hub_fuel_F-mat8.6-0 ; 
       x4_hub_fuel_F-mat90.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       x4_hub_fuel_F-fuselg.1-0 ; 
       x4_hub_fuel_F-ranteen.1-0 ; 
       x4_hub_fuel_F-ranteen1.1-0 ; 
       x4_hub_fuel_F-ss06.18-0 ROOT ; 
       x4_hub_fuel_F-SS28.1-0 ; 
       x4_hub_fuel_F-SS3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss06/PICTURES/ss06 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss06-static.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 10     
       retext-t2d54.10-0 ; 
       retext-t2d55.9-0 ; 
       retext-t2d56.8-0 ; 
       retext-t2d58.6-0 ; 
       retext-t2d59.6-0 ; 
       retext-t2d60.6-0 ; 
       retext-t2d61.3-0 ; 
       retext-t2d62.3-0 ; 
       retext-t2d63.3-0 ; 
       retext-t2d64.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       4 2 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 11 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       4 4 300 ; 
       5 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
       1 6 401 ; 
       2 8 401 ; 
       3 9 401 ; 
       5 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 12.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 15 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 6.25 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 0 0 0 -18.96012 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 1 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
