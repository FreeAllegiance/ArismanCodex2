SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       x4_hub_fuel_F-ss06.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.1-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       x4_hub_fuel_F-light3.1-0 ROOT ; 
       x4_hub_fuel_F-light4.1-0 ROOT ; 
       x4_hub_fuel_F-light5.1-0 ROOT ; 
       x4_hub_fuel_F-light6.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 81     
       sm_509_mod-METALS-GRAPHITE.1-0.1-0 ; 
       x4_hub_fuel_F-mat10.1-0 ; 
       x4_hub_fuel_F-mat11.1-0 ; 
       x4_hub_fuel_F-mat12.1-0 ; 
       x4_hub_fuel_F-mat13.1-0 ; 
       x4_hub_fuel_F-mat14.1-0 ; 
       x4_hub_fuel_F-mat15.1-0 ; 
       x4_hub_fuel_F-mat16.1-0 ; 
       x4_hub_fuel_F-mat17.1-0 ; 
       x4_hub_fuel_F-mat18.1-0 ; 
       x4_hub_fuel_F-mat19.1-0 ; 
       x4_hub_fuel_F-mat20.1-0 ; 
       x4_hub_fuel_F-mat21.1-0 ; 
       x4_hub_fuel_F-mat22.1-0 ; 
       x4_hub_fuel_F-mat23.1-0 ; 
       x4_hub_fuel_F-mat24.1-0 ; 
       x4_hub_fuel_F-mat25.1-0 ; 
       x4_hub_fuel_F-mat26.1-0 ; 
       x4_hub_fuel_F-mat27.1-0 ; 
       x4_hub_fuel_F-mat28.1-0 ; 
       x4_hub_fuel_F-mat29.1-0 ; 
       x4_hub_fuel_F-mat30.1-0 ; 
       x4_hub_fuel_F-mat31.1-0 ; 
       x4_hub_fuel_F-mat32.1-0 ; 
       x4_hub_fuel_F-mat33.1-0 ; 
       x4_hub_fuel_F-mat34.1-0 ; 
       x4_hub_fuel_F-mat35.1-0 ; 
       x4_hub_fuel_F-mat36.1-0 ; 
       x4_hub_fuel_F-mat37.1-0 ; 
       x4_hub_fuel_F-mat38.1-0 ; 
       x4_hub_fuel_F-mat39.1-0 ; 
       x4_hub_fuel_F-mat40.1-0 ; 
       x4_hub_fuel_F-mat41.1-0 ; 
       x4_hub_fuel_F-mat42.1-0 ; 
       x4_hub_fuel_F-mat43.1-0 ; 
       x4_hub_fuel_F-mat44.1-0 ; 
       x4_hub_fuel_F-mat45.1-0 ; 
       x4_hub_fuel_F-mat46.1-0 ; 
       x4_hub_fuel_F-mat47.1-0 ; 
       x4_hub_fuel_F-mat48.1-0 ; 
       x4_hub_fuel_F-mat49.1-0 ; 
       x4_hub_fuel_F-mat50.1-0 ; 
       x4_hub_fuel_F-mat51.1-0 ; 
       x4_hub_fuel_F-mat52.1-0 ; 
       x4_hub_fuel_F-mat53.1-0 ; 
       x4_hub_fuel_F-mat54.1-0 ; 
       x4_hub_fuel_F-mat55.1-0 ; 
       x4_hub_fuel_F-mat56.1-0 ; 
       x4_hub_fuel_F-mat57.1-0 ; 
       x4_hub_fuel_F-mat58.1-0 ; 
       x4_hub_fuel_F-mat59.1-0 ; 
       x4_hub_fuel_F-mat60.1-0 ; 
       x4_hub_fuel_F-mat61.1-0 ; 
       x4_hub_fuel_F-mat62.1-0 ; 
       x4_hub_fuel_F-mat63.1-0 ; 
       x4_hub_fuel_F-mat64.1-0 ; 
       x4_hub_fuel_F-mat67.1-0 ; 
       x4_hub_fuel_F-mat68.1-0 ; 
       x4_hub_fuel_F-mat69.1-0 ; 
       x4_hub_fuel_F-mat70.1-0 ; 
       x4_hub_fuel_F-mat71.1-0 ; 
       x4_hub_fuel_F-mat72.1-0 ; 
       x4_hub_fuel_F-mat73.1-0 ; 
       x4_hub_fuel_F-mat74.1-0 ; 
       x4_hub_fuel_F-mat75.1-0 ; 
       x4_hub_fuel_F-mat76.1-0 ; 
       x4_hub_fuel_F-mat77.1-0 ; 
       x4_hub_fuel_F-mat78.1-0 ; 
       x4_hub_fuel_F-mat79.1-0 ; 
       x4_hub_fuel_F-mat8.1-0 ; 
       x4_hub_fuel_F-mat80.1-0 ; 
       x4_hub_fuel_F-mat81.1-0 ; 
       x4_hub_fuel_F-mat82.1-0 ; 
       x4_hub_fuel_F-mat83.1-0 ; 
       x4_hub_fuel_F-mat84.1-0 ; 
       x4_hub_fuel_F-mat85.1-0 ; 
       x4_hub_fuel_F-mat86.1-0 ; 
       x4_hub_fuel_F-mat9.1-0 ; 
       x4_hub_fuel_F-mat90.1-0 ; 
       x4_hub_fuel_F-mat91.1-0 ; 
       x4_hub_fuel_F-mat92.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 37     
       x4_hub_fuel_F-fuselg.1-0 ; 
       x4_hub_fuel_F-lanteen.1-0 ; 
       x4_hub_fuel_F-lndpad1.1-0 ; 
       x4_hub_fuel_F-lndpad11.1-0 ; 
       x4_hub_fuel_F-lndpad12.1-0 ; 
       x4_hub_fuel_F-lndpad13.1-0 ; 
       x4_hub_fuel_F-manteen2.1-0 ; 
       x4_hub_fuel_F-mantenn1.1-0 ; 
       x4_hub_fuel_F-ranteen.1-0 ; 
       x4_hub_fuel_F-ss06.1-0 ROOT ; 
       x4_hub_fuel_F-SS1.1-0 ; 
       x4_hub_fuel_F-SS10.1-0 ; 
       x4_hub_fuel_F-SS11.1-0 ; 
       x4_hub_fuel_F-SS12.1-0 ; 
       x4_hub_fuel_F-SS13.1-0 ; 
       x4_hub_fuel_F-SS14.1-0 ; 
       x4_hub_fuel_F-SS15.1-0 ; 
       x4_hub_fuel_F-SS16.1-0 ; 
       x4_hub_fuel_F-SS17.1-0 ; 
       x4_hub_fuel_F-SS18.1-0 ; 
       x4_hub_fuel_F-SS19.1-0 ; 
       x4_hub_fuel_F-SS2.1-0 ; 
       x4_hub_fuel_F-SS20.1-0 ; 
       x4_hub_fuel_F-SS21.1-0 ; 
       x4_hub_fuel_F-SS22.1-0 ; 
       x4_hub_fuel_F-SS23.1-0 ; 
       x4_hub_fuel_F-SS24.1-0 ; 
       x4_hub_fuel_F-SS25.1-0 ; 
       x4_hub_fuel_F-SS26.1-0 ; 
       x4_hub_fuel_F-SS27.1-0 ; 
       x4_hub_fuel_F-SS3.1-0 ; 
       x4_hub_fuel_F-SS4.1-0 ; 
       x4_hub_fuel_F-SS5.1-0 ; 
       x4_hub_fuel_F-SS6.1-0 ; 
       x4_hub_fuel_F-SS7.1-0 ; 
       x4_hub_fuel_F-SS8.1-0 ; 
       x4_hub_fuel_F-SS9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 5     
       D:/Pete_Data/Softimage/space_station/ss/ss06/PICTURES/ss ; 
       D:/Pete_Data/Softimage/space_station/ss/ss06/PICTURES/ss10 ; 
       D:/Pete_Data/Softimage/space_station/ss/ss06/PICTURES/ss6 ; 
       D:/Pete_Data/Softimage/space_station/ss/ss06/PICTURES/ssdish ; 
       D:/Pete_Data/Softimage/space_station/ss/ss06/PICTURES/utl6 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss06-x4_hub_fuel_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 51     
       x4_hub_fuel_F-ss6_edge1.1-0 ; 
       x4_hub_fuel_F-t2d1.1-0 ; 
       x4_hub_fuel_F-t2d10.1-0 ; 
       x4_hub_fuel_F-t2d11.1-0 ; 
       x4_hub_fuel_F-t2d12.1-0 ; 
       x4_hub_fuel_F-t2d13.1-0 ; 
       x4_hub_fuel_F-t2d14.1-0 ; 
       x4_hub_fuel_F-t2d15.1-0 ; 
       x4_hub_fuel_F-t2d16.1-0 ; 
       x4_hub_fuel_F-t2d17.1-0 ; 
       x4_hub_fuel_F-t2d18.1-0 ; 
       x4_hub_fuel_F-t2d19.1-0 ; 
       x4_hub_fuel_F-t2d2.1-0 ; 
       x4_hub_fuel_F-t2d20.1-0 ; 
       x4_hub_fuel_F-t2d21.1-0 ; 
       x4_hub_fuel_F-t2d22.1-0 ; 
       x4_hub_fuel_F-t2d23.1-0 ; 
       x4_hub_fuel_F-t2d24.1-0 ; 
       x4_hub_fuel_F-t2d25.1-0 ; 
       x4_hub_fuel_F-t2d27.1-0 ; 
       x4_hub_fuel_F-t2d28.1-0 ; 
       x4_hub_fuel_F-t2d3.1-0 ; 
       x4_hub_fuel_F-t2d30.1-0 ; 
       x4_hub_fuel_F-t2d31.1-0 ; 
       x4_hub_fuel_F-t2d32.1-0 ; 
       x4_hub_fuel_F-t2d33.1-0 ; 
       x4_hub_fuel_F-t2d34.1-0 ; 
       x4_hub_fuel_F-t2d35.1-0 ; 
       x4_hub_fuel_F-t2d36.1-0 ; 
       x4_hub_fuel_F-t2d37.1-0 ; 
       x4_hub_fuel_F-t2d38.1-0 ; 
       x4_hub_fuel_F-t2d39.1-0 ; 
       x4_hub_fuel_F-t2d4.1-0 ; 
       x4_hub_fuel_F-t2d40.1-0 ; 
       x4_hub_fuel_F-t2d42.1-0 ; 
       x4_hub_fuel_F-t2d43.1-0 ; 
       x4_hub_fuel_F-t2d44.1-0 ; 
       x4_hub_fuel_F-t2d45.1-0 ; 
       x4_hub_fuel_F-t2d46.1-0 ; 
       x4_hub_fuel_F-t2d47.1-0 ; 
       x4_hub_fuel_F-t2d48.1-0 ; 
       x4_hub_fuel_F-t2d49.1-0 ; 
       x4_hub_fuel_F-t2d5.1-0 ; 
       x4_hub_fuel_F-t2d50.1-0 ; 
       x4_hub_fuel_F-t2d51.1-0 ; 
       x4_hub_fuel_F-t2d52.1-0 ; 
       x4_hub_fuel_F-t2d53.1-0 ; 
       x4_hub_fuel_F-t2d6.1-0 ; 
       x4_hub_fuel_F-t2d7.1-0 ; 
       x4_hub_fuel_F-t2d8.1-0 ; 
       x4_hub_fuel_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 1 110 ; 
       11 5 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       15 11 110 ; 
       16 11 110 ; 
       17 3 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 7 110 ; 
       22 17 110 ; 
       23 17 110 ; 
       24 4 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 24 110 ; 
       29 24 110 ; 
       30 8 110 ; 
       31 2 110 ; 
       32 31 110 ; 
       33 31 110 ; 
       34 31 110 ; 
       35 31 110 ; 
       36 31 110 ; 
       0 9 110 ; 
       1 0 110 ; 
       2 0 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 0 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 80 300 ; 
       12 72 300 ; 
       13 74 300 ; 
       14 75 300 ; 
       15 73 300 ; 
       16 76 300 ; 
       18 61 300 ; 
       19 63 300 ; 
       20 64 300 ; 
       21 79 300 ; 
       22 62 300 ; 
       23 65 300 ; 
       25 66 300 ; 
       26 70 300 ; 
       27 68 300 ; 
       28 67 300 ; 
       29 71 300 ; 
       30 78 300 ; 
       32 56 300 ; 
       33 57 300 ; 
       34 60 300 ; 
       35 59 300 ; 
       36 58 300 ; 
       0 69 300 ; 
       0 77 300 ; 
       0 1 300 ; 
       0 2 300 ; 
       0 3 300 ; 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       0 20 300 ; 
       0 21 300 ; 
       0 22 300 ; 
       0 23 300 ; 
       0 34 300 ; 
       0 35 300 ; 
       0 36 300 ; 
       0 37 300 ; 
       0 39 300 ; 
       0 40 300 ; 
       0 41 300 ; 
       0 52 300 ; 
       0 53 300 ; 
       0 54 300 ; 
       0 55 300 ; 
       1 24 300 ; 
       1 25 300 ; 
       1 26 300 ; 
       1 27 300 ; 
       1 28 300 ; 
       1 30 300 ; 
       1 31 300 ; 
       1 32 300 ; 
       1 33 300 ; 
       1 38 300 ; 
       6 29 300 ; 
       7 0 300 ; 
       8 42 300 ; 
       8 43 300 ; 
       8 44 300 ; 
       8 45 300 ; 
       8 46 300 ; 
       8 47 300 ; 
       8 48 300 ; 
       8 49 300 ; 
       8 50 300 ; 
       8 51 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       6 46 400 ; 
       7 20 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 12 401 ; 
       2 21 401 ; 
       3 32 401 ; 
       4 42 401 ; 
       5 47 401 ; 
       6 48 401 ; 
       7 0 401 ; 
       8 49 401 ; 
       9 50 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       12 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       17 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       25 17 401 ; 
       26 18 401 ; 
       28 19 401 ; 
       30 22 401 ; 
       31 23 401 ; 
       32 24 401 ; 
       34 25 401 ; 
       35 26 401 ; 
       36 27 401 ; 
       37 28 401 ; 
       38 29 401 ; 
       39 30 401 ; 
       40 31 401 ; 
       41 33 401 ; 
       43 34 401 ; 
       44 35 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 38 401 ; 
       49 39 401 ; 
       51 40 401 ; 
       52 41 401 ; 
       53 43 401 ; 
       54 44 401 ; 
       55 45 401 ; 
       77 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 66.24999 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 63.75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 58.75 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 61.25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       10 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 25 -10 0 MPRFLG 0 ; 
       14 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 30 -10 0 MPRFLG 0 ; 
       16 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 41.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 36.25 -10 0 MPRFLG 0 ; 
       19 SCHEM 38.75 -10 0 MPRFLG 0 ; 
       20 SCHEM 41.25 -10 0 MPRFLG 0 ; 
       21 SCHEM 5 -8 0 MPRFLG 0 ; 
       22 SCHEM 43.75 -10 0 MPRFLG 0 ; 
       23 SCHEM 46.25 -10 0 MPRFLG 0 ; 
       24 SCHEM 53.75 -8 0 MPRFLG 0 ; 
       25 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       26 SCHEM 51.25 -10 0 MPRFLG 0 ; 
       27 SCHEM 53.75 -10 0 MPRFLG 0 ; 
       28 SCHEM 56.25 -10 0 MPRFLG 0 ; 
       29 SCHEM 58.75 -10 0 MPRFLG 0 ; 
       30 SCHEM 33.75 -8 0 MPRFLG 0 ; 
       31 SCHEM 15 -8 0 MPRFLG 0 ; 
       32 SCHEM 10 -10 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 15 -10 0 MPRFLG 0 ; 
       35 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 20 -10 0 MPRFLG 0 ; 
       0 SCHEM 31.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       2 SCHEM 15 -6 0 MPRFLG 0 ; 
       3 SCHEM 41.25 -6 0 MPRFLG 0 ; 
       4 SCHEM 53.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 3.75 -6 0 MPRFLG 0 ; 
       8 SCHEM 33.75 -6 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       9 SCHEM 31.25 -2 0 SRT 1 1 1 -1.570796 0 0 0 -18.96012 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 35.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 19 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 16.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 14 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 35.25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 42.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 37.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 40.25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 45.25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 47.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 55.25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 52.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 50.25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 57.75 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 21.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 29 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 24 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 26.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 31.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 60.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 32.75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 35.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 60.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 61.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 2 0 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
