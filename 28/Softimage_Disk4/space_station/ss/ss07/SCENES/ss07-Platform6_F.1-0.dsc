SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       Platform6_F-ss07.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       one-cam_int1.1-0 ROOT ; 
       one-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       Platform6_F-light10.1-0 ROOT ; 
       Platform6_F-light11.1-0 ROOT ; 
       Platform6_F-light8.1-0 ROOT ; 
       Platform6_F-light9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 72     
       Platform6_F-mat125.1-0 ; 
       Platform6_F-mat126.1-0 ; 
       Platform6_F-mat127.1-0 ; 
       Platform6_F-mat128.1-0 ; 
       Platform6_F-mat129.1-0 ; 
       Platform6_F-mat134.1-0 ; 
       Platform6_F-mat135.1-0 ; 
       Platform6_F-mat136.1-0 ; 
       Platform6_F-mat137.1-0 ; 
       Platform6_F-mat138.1-0 ; 
       Platform6_F-mat139.1-0 ; 
       Platform6_F-mat140.1-0 ; 
       Platform6_F-mat141.1-0 ; 
       Platform6_F-mat142.1-0 ; 
       Platform6_F-mat143.1-0 ; 
       Platform6_F-mat144.1-0 ; 
       Platform6_F-mat145.1-0 ; 
       Platform6_F-mat146.1-0 ; 
       Platform6_F-mat147.1-0 ; 
       Platform6_F-mat148.1-0 ; 
       Platform6_F-mat149.1-0 ; 
       Platform6_F-mat150.1-0 ; 
       Platform6_F-mat151.1-0 ; 
       Platform6_F-mat152.1-0 ; 
       Platform6_F-mat153.1-0 ; 
       Platform6_F-mat154.1-0 ; 
       Platform6_F-mat155.1-0 ; 
       Platform6_F-mat156.1-0 ; 
       Platform6_F-mat157.1-0 ; 
       Platform6_F-mat158.1-0 ; 
       Platform6_F-mat159.1-0 ; 
       Platform6_F-mat160.1-0 ; 
       Platform6_F-mat161.1-0 ; 
       Platform6_F-mat162.1-0 ; 
       Platform6_F-mat163.1-0 ; 
       Platform6_F-mat164.1-0 ; 
       Platform6_F-mat165.1-0 ; 
       Platform6_F-mat166.1-0 ; 
       Platform6_F-mat167.1-0 ; 
       Platform6_F-mat168.1-0 ; 
       Platform6_F-mat169.1-0 ; 
       Platform6_F-mat170.1-0 ; 
       Platform6_F-mat171.1-0 ; 
       Platform6_F-mat172.1-0 ; 
       Platform6_F-mat173.1-0 ; 
       Platform6_F-mat174.1-0 ; 
       Platform6_F-mat175.1-0 ; 
       Platform6_F-mat176.1-0 ; 
       Platform6_F-mat177.1-0 ; 
       Platform6_F-mat178.1-0 ; 
       Platform6_F-mat179.1-0 ; 
       Platform6_F-mat180.1-0 ; 
       Platform6_F-mat181.1-0 ; 
       Platform6_F-mat182.1-0 ; 
       Platform6_F-mat183.1-0 ; 
       Platform6_F-mat184.1-0 ; 
       Platform6_F-mat185.1-0 ; 
       Platform6_F-mat186.1-0 ; 
       Platform6_F-mat187.1-0 ; 
       Platform6_F-mat188.1-0 ; 
       Platform6_F-mat189.1-0 ; 
       Platform6_F-mat190.1-0 ; 
       Platform6_F-mat191.1-0 ; 
       Platform6_F-mat192.1-0 ; 
       Platform6_F-mat193.1-0 ; 
       Platform6_F-mat194.1-0 ; 
       Platform6_F-mat195.1-0 ; 
       Platform6_F-mat196.1-0 ; 
       Platform6_F-mat197.1-0 ; 
       Platform6_F-mat198.1-0 ; 
       Platform6_F-mat199.1-0 ; 
       Platform6_F-mat200.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 73     
       Platform6_F-doccon.1-0 ; 
       Platform6_F-ffuselg.1-0 ; 
       Platform6_F-ffuselg1.1-0 ; 
       Platform6_F-fuselg.2-0 ; 
       Platform6_F-lantenn1.1-0 ; 
       Platform6_F-lantenn2.1-0 ; 
       Platform6_F-lfuselg.1-0 ; 
       Platform6_F-lndpad1.5-0 ; 
       Platform6_F-lndpad2.4-0 ; 
       Platform6_F-lndpad3.4-0 ; 
       Platform6_F-lndpad4.4-0 ; 
       Platform6_F-lndpad5.4-0 ; 
       Platform6_F-lndpad6.4-0 ; 
       Platform6_F-lndpad7.4-0 ; 
       Platform6_F-lndpad8.4-0 ; 
       Platform6_F-rantenn1.1-0 ; 
       Platform6_F-rantenn2.1-0 ; 
       Platform6_F-rfuselg.1-0 ; 
       Platform6_F-ss07.1-0 ROOT ; 
       Platform6_F-SS1.1-0 ; 
       Platform6_F-SS10.1-0 ; 
       Platform6_F-SS11.1-0 ; 
       Platform6_F-SS12.1-0 ; 
       Platform6_F-SS13.1-0 ; 
       Platform6_F-SS14.1-0 ; 
       Platform6_F-SS15.1-0 ; 
       Platform6_F-SS17.1-0 ; 
       Platform6_F-SS18.1-0 ; 
       Platform6_F-SS19.1-0 ; 
       Platform6_F-SS2.1-0 ; 
       Platform6_F-SS20.1-0 ; 
       Platform6_F-SS21.1-0 ; 
       Platform6_F-SS22.1-0 ; 
       Platform6_F-SS23.1-0 ; 
       Platform6_F-SS24.1-0 ; 
       Platform6_F-SS25.1-0 ; 
       Platform6_F-SS26.1-0 ; 
       Platform6_F-SS27.1-0 ; 
       Platform6_F-SS28.1-0 ; 
       Platform6_F-SS29.1-0 ; 
       Platform6_F-SS3.1-0 ; 
       Platform6_F-SS30.1-0 ; 
       Platform6_F-SS31.1-0 ; 
       Platform6_F-SS32.1-0 ; 
       Platform6_F-SS33.1-0 ; 
       Platform6_F-SS34.1-0 ; 
       Platform6_F-SS35.1-0 ; 
       Platform6_F-SS36.1-0 ; 
       Platform6_F-SS37.1-0 ; 
       Platform6_F-SS38.1-0 ; 
       Platform6_F-SS39.1-0 ; 
       Platform6_F-SS4.1-0 ; 
       Platform6_F-SS40.1-0 ; 
       Platform6_F-SS41.1-0 ; 
       Platform6_F-SS42.1-0 ; 
       Platform6_F-SS43.1-0 ; 
       Platform6_F-SS44.1-0 ; 
       Platform6_F-SS45.1-0 ; 
       Platform6_F-SS46.1-0 ; 
       Platform6_F-SS47.1-0 ; 
       Platform6_F-SS48.1-0 ; 
       Platform6_F-SS49.1-0 ; 
       Platform6_F-SS5.1-0 ; 
       Platform6_F-SS6.1-0 ; 
       Platform6_F-SS7.1-0 ; 
       Platform6_F-SS8.1-0 ; 
       Platform6_F-SS9.1-0 ; 
       Platform6_F-SSal.2-0 ; 
       Platform6_F-SSar.2-0 ; 
       Platform6_F-SSfl.2-0 ; 
       Platform6_F-SSfr.2-0 ; 
       Platform6_F-SSlt.1-0 ; 
       Platform6_F-SSrt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 4     
       D:/Pete_Data/Softimage/space_station/ss/ss07/PICTURES/ss ; 
       D:/Pete_Data/Softimage/space_station/ss/ss07/PICTURES/ss07 ; 
       D:/Pete_Data/Softimage/space_station/ss/ss07/PICTURES/ss10 ; 
       D:/Pete_Data/Softimage/space_station/ss/ss07/PICTURES/ssdish ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss07-Platform6_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 24     
       Platform6_F-t2d10.1-0 ; 
       Platform6_F-t2d11.1-0 ; 
       Platform6_F-t2d12.1-0 ; 
       Platform6_F-t2d18.1-0 ; 
       Platform6_F-t2d19.1-0 ; 
       Platform6_F-t2d20.1-0 ; 
       Platform6_F-t2d21.1-0 ; 
       Platform6_F-t2d22.1-0 ; 
       Platform6_F-t2d23.1-0 ; 
       Platform6_F-t2d25.1-0 ; 
       Platform6_F-t2d26.1-0 ; 
       Platform6_F-t2d27.1-0 ; 
       Platform6_F-t2d28.1-0 ; 
       Platform6_F-t2d29.1-0 ; 
       Platform6_F-t2d30.1-0 ; 
       Platform6_F-t2d31.1-0 ; 
       Platform6_F-t2d32.1-0 ; 
       Platform6_F-t2d33.1-0 ; 
       Platform6_F-t2d34.1-0 ; 
       Platform6_F-t2d35.1-0 ; 
       Platform6_F-t2d36.1-0 ; 
       Platform6_F-t2d37.1-0 ; 
       Platform6_F-t2d38.1-0 ; 
       Platform6_F-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       19 7 110 ; 
       20 64 110 ; 
       21 64 110 ; 
       22 64 110 ; 
       23 9 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 19 110 ; 
       30 10 110 ; 
       31 30 110 ; 
       32 30 110 ; 
       33 30 110 ; 
       34 30 110 ; 
       35 30 110 ; 
       36 11 110 ; 
       37 36 110 ; 
       38 36 110 ; 
       39 36 110 ; 
       40 19 110 ; 
       41 36 110 ; 
       42 36 110 ; 
       43 12 110 ; 
       44 43 110 ; 
       45 43 110 ; 
       46 43 110 ; 
       47 43 110 ; 
       48 43 110 ; 
       49 13 110 ; 
       50 49 110 ; 
       51 19 110 ; 
       52 49 110 ; 
       53 49 110 ; 
       54 49 110 ; 
       55 49 110 ; 
       56 14 110 ; 
       57 56 110 ; 
       58 56 110 ; 
       59 56 110 ; 
       60 56 110 ; 
       61 56 110 ; 
       62 19 110 ; 
       63 19 110 ; 
       64 8 110 ; 
       65 64 110 ; 
       66 64 110 ; 
       67 1 110 ; 
       68 1 110 ; 
       69 2 110 ; 
       70 2 110 ; 
       71 4 110 ; 
       72 15 110 ; 
       0 1 110 ; 
       1 3 110 ; 
       2 3 110 ; 
       3 18 110 ; 
       4 6 110 ; 
       5 4 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 3 110 ; 
       12 3 110 ; 
       13 3 110 ; 
       14 3 110 ; 
       15 17 110 ; 
       16 15 110 ; 
       17 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       20 33 300 ; 
       21 32 300 ; 
       22 31 300 ; 
       24 40 300 ; 
       25 39 300 ; 
       26 38 300 ; 
       27 37 300 ; 
       28 36 300 ; 
       29 30 300 ; 
       31 45 300 ; 
       32 44 300 ; 
       33 43 300 ; 
       34 42 300 ; 
       35 41 300 ; 
       37 50 300 ; 
       38 49 300 ; 
       39 48 300 ; 
       40 29 300 ; 
       41 47 300 ; 
       42 46 300 ; 
       44 55 300 ; 
       45 54 300 ; 
       46 53 300 ; 
       47 52 300 ; 
       48 51 300 ; 
       50 60 300 ; 
       51 28 300 ; 
       52 59 300 ; 
       53 58 300 ; 
       54 57 300 ; 
       55 56 300 ; 
       57 65 300 ; 
       58 64 300 ; 
       59 63 300 ; 
       60 62 300 ; 
       61 61 300 ; 
       62 27 300 ; 
       63 26 300 ; 
       65 35 300 ; 
       66 34 300 ; 
       67 69 300 ; 
       68 68 300 ; 
       69 66 300 ; 
       70 67 300 ; 
       71 24 300 ; 
       72 25 300 ; 
       1 5 300 ; 
       1 6 300 ; 
       2 70 300 ; 
       2 71 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 15 300 ; 
       3 16 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       3 20 300 ; 
       3 21 300 ; 
       3 22 300 ; 
       3 23 300 ; 
       4 3 300 ; 
       5 4 300 ; 
       6 0 300 ; 
       6 1 300 ; 
       6 2 300 ; 
       15 10 300 ; 
       16 11 300 ; 
       17 7 300 ; 
       17 8 300 ; 
       17 9 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 3 400 ; 
       2 21 400 ; 
       4 2 400 ; 
       5 19 400 ; 
       15 8 400 ; 
       16 20 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       18 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 23 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       6 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       13 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       20 16 401 ; 
       22 17 401 ; 
       23 18 401 ; 
       71 22 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 0 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -14 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       19 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 35 -12 0 MPRFLG 0 ; 
       21 SCHEM 37.5 -12 0 MPRFLG 0 ; 
       22 SCHEM 40 -12 0 MPRFLG 0 ; 
       23 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -12 0 MPRFLG 0 ; 
       25 SCHEM 45 -12 0 MPRFLG 0 ; 
       26 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       27 SCHEM 50 -12 0 MPRFLG 0 ; 
       28 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       29 SCHEM 17.5 -12 0 MPRFLG 0 ; 
       30 SCHEM 60 -10 0 MPRFLG 0 ; 
       31 SCHEM 55 -12 0 MPRFLG 0 ; 
       32 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       33 SCHEM 60 -12 0 MPRFLG 0 ; 
       34 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       35 SCHEM 65 -12 0 MPRFLG 0 ; 
       36 SCHEM 72.5 -10 0 MPRFLG 0 ; 
       37 SCHEM 67.5 -12 0 MPRFLG 0 ; 
       38 SCHEM 70 -12 0 MPRFLG 0 ; 
       39 SCHEM 72.5 -12 0 MPRFLG 0 ; 
       40 SCHEM 20 -12 0 MPRFLG 0 ; 
       41 SCHEM 75 -12 0 MPRFLG 0 ; 
       42 SCHEM 77.5 -12 0 MPRFLG 0 ; 
       43 SCHEM 85 -10 0 MPRFLG 0 ; 
       44 SCHEM 80 -12 0 MPRFLG 0 ; 
       45 SCHEM 82.5 -12 0 MPRFLG 0 ; 
       46 SCHEM 85 -12 0 MPRFLG 0 ; 
       47 SCHEM 87.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 90 -12 0 MPRFLG 0 ; 
       49 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       50 SCHEM 92.5 -12 0 MPRFLG 0 ; 
       51 SCHEM 22.5 -12 0 MPRFLG 0 ; 
       52 SCHEM 95 -12 0 MPRFLG 0 ; 
       53 SCHEM 97.5 -12 0 MPRFLG 0 ; 
       54 SCHEM 100 -12 0 MPRFLG 0 ; 
       55 SCHEM 102.5 -12 0 MPRFLG 0 ; 
       56 SCHEM 110 -10 0 MPRFLG 0 ; 
       57 SCHEM 105 -12 0 MPRFLG 0 ; 
       58 SCHEM 107.5 -12 0 MPRFLG 0 ; 
       59 SCHEM 110 -12 0 MPRFLG 0 ; 
       60 SCHEM 112.5 -12 0 MPRFLG 0 ; 
       61 SCHEM 115 -12 0 MPRFLG 0 ; 
       62 SCHEM 25 -12 0 MPRFLG 0 ; 
       63 SCHEM 27.5 -12 0 MPRFLG 0 ; 
       64 SCHEM 35 -10 0 MPRFLG 0 ; 
       65 SCHEM 30 -12 0 MPRFLG 0 ; 
       66 SCHEM 32.5 -12 0 MPRFLG 0 ; 
       67 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       68 SCHEM 10 -10 0 MPRFLG 0 ; 
       69 SCHEM 117.5 -10 0 MPRFLG 0 ; 
       70 SCHEM 120 -10 0 MPRFLG 0 ; 
       71 SCHEM 7.5 -12 0 MPRFLG 0 ; 
       72 SCHEM 2.5 -12 0 MPRFLG 0 ; 
       0 SCHEM 15 -10 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 118.75 -8 0 MPRFLG 0 ; 
       3 SCHEM 60 -6 0 MPRFLG 0 ; 
       4 SCHEM 6.25 -10 0 MPRFLG 0 ; 
       5 SCHEM 5 -12 0 MPRFLG 0 ; 
       6 SCHEM 6.25 -8 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       8 SCHEM 35 -8 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 60 -8 0 MPRFLG 0 ; 
       11 SCHEM 72.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 85 -8 0 MPRFLG 0 ; 
       13 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 110 -8 0 MPRFLG 0 ; 
       15 SCHEM 1.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 0 -12 0 MPRFLG 0 ; 
       17 SCHEM 1.25 -8 0 MPRFLG 0 ; 
       18 SCHEM 60 -4 0 SRT 1 1 1 0 -1.570796 0 14.25074 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -1 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 121.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 1.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 26.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 24 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 39 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 34 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 29 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 51.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 49 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 44 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 41.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 64 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 54 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 76.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 74 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 71.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 89 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 84 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 79 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 101.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 99 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 96.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 94 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 91.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 114 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 111.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 109 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 106.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 104 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 116.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 119 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 9 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 11.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 121.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 121.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -1 -14 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 121.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 121.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 121.5 -6 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 3 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
