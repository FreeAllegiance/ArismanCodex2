SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       acs08-acs08.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       cargo_scaled-cam_int1.1-0 ROOT ; 
       cargo_scaled-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       autofactory_F-light1.1-0 ROOT ; 
       autofactory_F-light10.1-0 ROOT ; 
       autofactory_F-light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 62     
       autofactory_F-mat1.1-0 ; 
       autofactory_F-mat10.1-0 ; 
       autofactory_F-mat11.1-0 ; 
       autofactory_F-mat12.1-0 ; 
       autofactory_F-mat13.1-0 ; 
       autofactory_F-mat14.1-0 ; 
       autofactory_F-mat15.1-0 ; 
       autofactory_F-mat16.1-0 ; 
       autofactory_F-mat17.1-0 ; 
       autofactory_F-mat18.1-0 ; 
       autofactory_F-mat19.1-0 ; 
       autofactory_F-mat20.1-0 ; 
       autofactory_F-mat21.1-0 ; 
       autofactory_F-mat23.1-0 ; 
       autofactory_F-mat24.1-0 ; 
       autofactory_F-mat25.1-0 ; 
       autofactory_F-mat26.1-0 ; 
       autofactory_F-mat27.1-0 ; 
       autofactory_F-mat28.1-0 ; 
       autofactory_F-mat29.1-0 ; 
       autofactory_F-mat30.1-0 ; 
       autofactory_F-mat31.1-0 ; 
       autofactory_F-mat32.1-0 ; 
       autofactory_F-mat33large.1-0 ; 
       autofactory_F-mat35large.1-0 ; 
       autofactory_F-mat36.1-0 ; 
       autofactory_F-mat37.1-0 ; 
       autofactory_F-mat38.1-0 ; 
       autofactory_F-mat39.1-0 ; 
       autofactory_F-mat40.1-0 ; 
       autofactory_F-mat5.1-0 ; 
       autofactory_F-mat6.1-0 ; 
       autofactory_F-mat7.1-0 ; 
       autofactory_F-mat8.1-0 ; 
       autofactory_F-mat9.1-0 ; 
       autofactory_F-nose_white-center.1-1.1-0 ; 
       autofactory_F-nose_white-center.1-10.1-0 ; 
       autofactory_F-nose_white-center.1-11.1-0 ; 
       autofactory_F-nose_white-center.1-12.1-0 ; 
       autofactory_F-nose_white-center.1-13.1-0 ; 
       autofactory_F-nose_white-center.1-14.1-0 ; 
       autofactory_F-nose_white-center.1-15.1-0 ; 
       autofactory_F-nose_white-center.1-16.1-0 ; 
       autofactory_F-nose_white-center.1-17.1-0 ; 
       autofactory_F-nose_white-center.1-18.1-0 ; 
       autofactory_F-nose_white-center.1-19.1-0 ; 
       autofactory_F-nose_white-center.1-20.1-0 ; 
       autofactory_F-nose_white-center.1-21.1-0 ; 
       autofactory_F-nose_white-center.1-22.1-0 ; 
       autofactory_F-nose_white-center.1-23.1-0 ; 
       autofactory_F-nose_white-center.1-24.1-0 ; 
       autofactory_F-nose_white-center.1-25.1-0 ; 
       autofactory_F-nose_white-center.1-26.1-0 ; 
       autofactory_F-nose_white-center.1-27.1-0 ; 
       autofactory_F-nose_white-center.1-28.1-0 ; 
       autofactory_F-nose_white-center.1-3.1-0 ; 
       autofactory_F-nose_white-center.1-4.1-0 ; 
       autofactory_F-nose_white-center.1-5.1-0 ; 
       autofactory_F-nose_white-center.1-6.1-0 ; 
       autofactory_F-nose_white-center.1-7.1-0 ; 
       autofactory_F-nose_white-center.1-8.1-0 ; 
       autofactory_F-nose_white-center.1-9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 55     
       acs08-acs08.1-0 ROOT ; 
       acs08-bantenn1.1-0 ; 
       acs08-bantenn2.1-0 ; 
       acs08-bantenn3.1-0 ; 
       acs08-bportal.1-0 ; 
       acs08-bturatt.1-0 ; 
       acs08-doccon.2-0 ; 
       acs08-fuselg1.3-0 ; 
       acs08-fuselg2.1-0 ; 
       acs08-llndpad1.1-0 ; 
       acs08-llndpad2.1-0 ; 
       acs08-platfm.1-0 ; 
       acs08-portal0.1-0 ; 
       acs08-rlndpad1.1-0 ; 
       acs08-rlndpad2.1-0 ; 
       acs08-SSa10.1-0 ; 
       acs08-SSa11.1-0 ; 
       acs08-SSa3.1-0 ; 
       acs08-SSa4.1-0 ; 
       acs08-SSa5.1-0 ; 
       acs08-SSa6.1-0 ; 
       acs08-SSa7.1-0 ; 
       acs08-SSlp0.1-0 ; 
       acs08-SSlp1.1-0 ; 
       acs08-SSlp2.1-0 ; 
       acs08-SSlp3.1-0 ; 
       acs08-SSlp4.1-0 ; 
       acs08-SSlp5.1-0 ; 
       acs08-SSlpp0.1-0 ; 
       acs08-SSlpp1.1-0 ; 
       acs08-SSlpp2.1-0 ; 
       acs08-SSlpp3.1-0 ; 
       acs08-SSlpp4.1-0 ; 
       acs08-SSlpp5.1-0 ; 
       acs08-SSrp0.1-0 ; 
       acs08-SSrp1.1-0 ; 
       acs08-SSrp2.1-0 ; 
       acs08-SSrp3.1-0 ; 
       acs08-SSrp4.1-0 ; 
       acs08-SSrp5.1-0 ; 
       acs08-SSrpp0.1-0 ; 
       acs08-SSrpp1.1-0 ; 
       acs08-SSrpp2.1-0 ; 
       acs08-SSrpp3.1-0 ; 
       acs08-SSrpp4.1-0 ; 
       acs08-SSrpp5.1-0 ; 
       acs08-strake0.1-0 ; 
       acs08-strake1.1-0 ; 
       acs08-strake2.1-0 ; 
       acs08-strake3.1-0 ; 
       acs08-tantenn1.1-0 ; 
       acs08-tantenn2.1-0 ; 
       acs08-tportal.2-0 ; 
       acs08-tractr.1-0 ; 
       acs08-tturatt.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss08/PICTURES/acs08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       acs08-autofactory_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 31     
       autofactory_F-t2d10.1-0 ; 
       autofactory_F-t2d11.1-0 ; 
       autofactory_F-t2d12.1-0 ; 
       autofactory_F-t2d13.1-0 ; 
       autofactory_F-t2d14.1-0 ; 
       autofactory_F-t2d15.1-0 ; 
       autofactory_F-t2d17.1-0 ; 
       autofactory_F-t2d18.1-0 ; 
       autofactory_F-t2d19.1-0 ; 
       autofactory_F-t2d20.1-0 ; 
       autofactory_F-t2d21.1-0 ; 
       autofactory_F-t2d22.1-0 ; 
       autofactory_F-t2d23.1-0 ; 
       autofactory_F-t2d24.1-0 ; 
       autofactory_F-t2d25.1-0 ; 
       autofactory_F-t2d26.1-0 ; 
       autofactory_F-t2d27large.1-0 ; 
       autofactory_F-t2d29.1-0 ; 
       autofactory_F-t2d3.1-0 ; 
       autofactory_F-t2d30.1-0 ; 
       autofactory_F-t2d31.1-0 ; 
       autofactory_F-t2d32.1-0 ; 
       autofactory_F-t2d33.1-0 ; 
       autofactory_F-t2d34.1-0 ; 
       autofactory_F-t2d4.1-0 ; 
       autofactory_F-t2d5.1-0 ; 
       autofactory_F-t2d6.1-0 ; 
       autofactory_F-t2d7.1-0 ; 
       autofactory_F-t2d8.1-0 ; 
       autofactory_F-t2d9.1-0 ; 
       autofactory_F-zzt2d1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       15 4 110 ; 
       16 4 110 ; 
       17 2 110 ; 
       18 7 110 ; 
       19 7 110 ; 
       20 52 110 ; 
       21 52 110 ; 
       22 11 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       27 22 110 ; 
       28 11 110 ; 
       29 28 110 ; 
       30 28 110 ; 
       31 28 110 ; 
       32 28 110 ; 
       33 28 110 ; 
       34 11 110 ; 
       35 34 110 ; 
       36 34 110 ; 
       37 34 110 ; 
       38 34 110 ; 
       39 34 110 ; 
       40 11 110 ; 
       41 40 110 ; 
       42 40 110 ; 
       43 40 110 ; 
       44 40 110 ; 
       45 40 110 ; 
       1 7 110 ; 
       2 1 110 ; 
       3 2 110 ; 
       4 12 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 0 110 ; 
       8 7 110 ; 
       9 11 110 ; 
       10 11 110 ; 
       11 53 110 ; 
       12 7 110 ; 
       13 11 110 ; 
       14 11 110 ; 
       46 7 110 ; 
       47 46 110 ; 
       48 46 110 ; 
       49 46 110 ; 
       50 7 110 ; 
       51 50 110 ; 
       52 12 110 ; 
       53 7 110 ; 
       54 7 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       15 59 300 ; 
       16 60 300 ; 
       17 35 300 ; 
       18 55 300 ; 
       19 56 300 ; 
       20 57 300 ; 
       21 58 300 ; 
       23 40 300 ; 
       24 41 300 ; 
       25 42 300 ; 
       26 43 300 ; 
       27 44 300 ; 
       29 50 300 ; 
       30 51 300 ; 
       31 52 300 ; 
       32 53 300 ; 
       33 54 300 ; 
       35 36 300 ; 
       36 37 300 ; 
       37 61 300 ; 
       38 38 300 ; 
       39 39 300 ; 
       41 45 300 ; 
       42 46 300 ; 
       43 47 300 ; 
       44 48 300 ; 
       45 49 300 ; 
       1 17 300 ; 
       2 16 300 ; 
       3 15 300 ; 
       4 1 300 ; 
       4 20 300 ; 
       4 29 300 ; 
       6 19 300 ; 
       7 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 11 300 ; 
       8 12 300 ; 
       8 13 300 ; 
       8 22 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       11 0 300 ; 
       11 30 300 ; 
       11 23 300 ; 
       11 26 300 ; 
       11 27 300 ; 
       47 4 300 ; 
       48 2 300 ; 
       49 3 300 ; 
       50 10 300 ; 
       50 14 300 ; 
       50 18 300 ; 
       51 9 300 ; 
       52 34 300 ; 
       52 21 300 ; 
       52 28 300 ; 
       53 31 300 ; 
       53 32 300 ; 
       53 33 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 27 401 ; 
       2 28 401 ; 
       3 29 401 ; 
       4 0 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 30 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 16 401 ; 
       24 17 401 ; 
       25 19 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 18 401 ; 
       32 24 401 ; 
       33 25 401 ; 
       34 26 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 3.5 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 -0.5 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM -0.1821793 -2.349965 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM -0.1821793 -4.349966 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM -0.1821793 -6.349966 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       15 SCHEM 20.03599 -63.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 20.03599 -65.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 20.03599 0.4760132 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 13.03599 -73.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 13.03599 -75.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 20.03599 -59.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 20.03599 -61.52399 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 20.03599 -25.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 23.53599 -29.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 23.53599 -27.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 23.53599 -25.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 23.53599 -23.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 23.53599 -21.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 20.03599 -45.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 23.53599 -49.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 23.53599 -47.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 23.53599 -45.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 23.53599 -43.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 23.53599 -41.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 20.03599 -15.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 23.53599 -19.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 23.53599 -17.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 23.53599 -15.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 23.53599 -13.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 23.53599 -11.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 20.03599 -35.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 23.53599 -39.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 23.53599 -37.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 23.53599 -35.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 23.53599 -33.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 23.53599 -31.52399 0 WIRECOL 10 7 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 6.035989 -36.52399 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 -9.536743e-007 0 MPRFLG 0 ; 
       1 SCHEM 13.03599 1.476013 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 16.53599 1.476013 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 20.03599 2.476013 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 16.53599 -64.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 13.03599 -53.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 13.03599 -57.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 9.535989 -36.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 13.03599 -55.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 20.03599 -7.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 20.03599 -9.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 16.53599 -26.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 13.03599 -62.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 20.03599 -5.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 20.03599 -3.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 13.03599 -69.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 16.53599 -67.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 16.53599 -69.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 16.53599 -71.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 13.03599 -1.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 16.53599 -1.523987 0 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 16.53599 -60.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 13.03599 -26.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 13.03599 -51.52399 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20.03599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 20.03599 -61.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 20.03599 -69.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 20.03599 -71.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20.03599 -67.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 13.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 13.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 13.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 13.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 20.03599 -1.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 16.53599 0.2260094 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 13.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.53599 -55.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.53599 -55.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.53599 0.2260094 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 23.53599 2.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 20.03599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.53599 4.226009 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.53599 0.2260094 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.53599 -57.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 20.03599 -61.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 20.03599 -57.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.53599 -55.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20.03599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.53599 -55.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.53599 -55.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 20.03599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 20.03599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 20.03599 -57.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 20.03599 -61.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 20.03599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.53599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.53599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 16.53599 -3.773991 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 20.03599 -57.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 23.53599 0.2260094 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 27.03599 -21.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 27.03599 -19.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 27.03599 -15.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 27.03599 -13.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 27.03599 -31.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 27.03599 -29.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.03599 -27.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 27.03599 -25.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 27.03599 -23.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 27.03599 -41.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 27.03599 -39.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 27.03599 -37.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 27.03599 -35.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 27.03599 -33.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 27.03599 -51.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 27.03599 -49.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 27.03599 -47.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 27.03599 -45.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 27.03599 -43.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 16.53599 -73.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 16.53599 -75.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 23.53599 -59.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 23.53599 -61.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 23.53599 -63.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 23.53599 -65.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 27.03599 -17.77399 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 23.53599 -67.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.53599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.53599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.53599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 20.03599 0.2260094 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.53599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 20.03599 -55.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 20.03599 0.2260094 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 27.03599 2.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 23.53599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20.03599 4.226009 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20.03599 0.2260094 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 20.03599 -57.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 23.53599 -61.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 23.53599 -57.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20.03599 -55.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 23.53599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 20.03599 -55.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 23.53599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 20.03599 -55.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 23.53599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 23.53599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 23.53599 -57.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 23.53599 -61.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 20.03599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 20.03599 -3.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 23.53599 -57.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 23.53599 -61.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 23.53599 -69.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 23.53599 -71.77399 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 23.53599 -1.773991 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 9.535989 4.226009 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 45 16 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
