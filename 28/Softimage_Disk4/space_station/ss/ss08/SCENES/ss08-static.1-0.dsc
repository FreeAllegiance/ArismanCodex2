SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       destroy-Camera1.23-0 ROOT ; 
       utann_heavy_fighter_mod-Camera1.23-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 47     
       add_radar-default2.5-0 ; 
       add_radar-mat1.3-0 ; 
       add_radar-mat10.3-0 ; 
       add_radar-mat107.3-0 ; 
       add_radar-mat11.3-0 ; 
       add_radar-mat110.4-0 ; 
       add_radar-mat111.2-0 ; 
       add_radar-mat12.3-0 ; 
       add_radar-mat13.3-0 ; 
       add_radar-mat14.3-0 ; 
       add_radar-mat15.3-0 ; 
       add_radar-mat16.3-0 ; 
       add_radar-mat17.3-0 ; 
       add_radar-mat18.3-0 ; 
       add_radar-mat19.3-0 ; 
       add_radar-mat2.3-0 ; 
       add_radar-mat20.3-0 ; 
       add_radar-mat21.3-0 ; 
       add_radar-mat22.3-0 ; 
       add_radar-mat23.3-0 ; 
       add_radar-mat24.3-0 ; 
       add_radar-mat25.3-0 ; 
       add_radar-mat26.3-0 ; 
       add_radar-mat27.3-0 ; 
       add_radar-mat28.3-0 ; 
       add_radar-mat29.3-0 ; 
       add_radar-mat3.3-0 ; 
       add_radar-mat30.3-0 ; 
       add_radar-mat31.3-0 ; 
       add_radar-mat32.3-0 ; 
       add_radar-mat33.3-0 ; 
       add_radar-mat34.3-0 ; 
       add_radar-mat35.3-0 ; 
       add_radar-mat36.3-0 ; 
       add_radar-mat37.3-0 ; 
       add_radar-mat38.3-0 ; 
       add_radar-mat39.3-0 ; 
       add_radar-mat4.3-0 ; 
       add_radar-mat40.3-0 ; 
       add_radar-mat41.3-0 ; 
       add_radar-mat42.3-0 ; 
       add_radar-mat43.3-0 ; 
       add_radar-mat5.3-0 ; 
       add_radar-mat6.3-0 ; 
       add_radar-mat7.3-0 ; 
       add_radar-mat8.3-0 ; 
       add_radar-mat9.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       ss08-bfuselg.1-0 ; 
       ss08-bmerge1.2-0 ; 
       ss08-doccon2.1-0 ; 
       ss08-doccon3.1-0 ; 
       ss08-doccon4.1-0 ; 
       ss08-fuselg.1-0 ; 
       ss08-mfuselg0.1-0 ; 
       ss08-mfuselg1.1-0 ; 
       ss08-mfuselg2.1-0 ; 
       ss08-mfuselg3.1-0 ; 
       ss08-mfuselg4.1-0 ; 
       ss08-mfuselg5.1-0 ; 
       ss08-mfuselg6.1-0 ; 
       ss08-mfuselg7.3-0 ; 
       ss08-root.14-0 ROOT ; 
       ss08-tetra1.1-0 ; 
       ss08-tmfuselg.2-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       E:/Pete_Data2/space_station/ss/ss08/PICTURES/ss08 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss08-static.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 46     
       add_radar-t2d1.3-0 ; 
       add_radar-t2d10.3-0 ; 
       add_radar-t2d11.3-0 ; 
       add_radar-t2d12.3-0 ; 
       add_radar-t2d13.3-0 ; 
       add_radar-t2d14.3-0 ; 
       add_radar-t2d15.3-0 ; 
       add_radar-t2d16.3-0 ; 
       add_radar-t2d17.3-0 ; 
       add_radar-t2d18.3-0 ; 
       add_radar-t2d19.3-0 ; 
       add_radar-t2d2.3-0 ; 
       add_radar-t2d20.3-0 ; 
       add_radar-t2d21.3-0 ; 
       add_radar-t2d22.3-0 ; 
       add_radar-t2d23.3-0 ; 
       add_radar-t2d24.3-0 ; 
       add_radar-t2d25.3-0 ; 
       add_radar-t2d26.3-0 ; 
       add_radar-t2d27.3-0 ; 
       add_radar-t2d28.3-0 ; 
       add_radar-t2d29.3-0 ; 
       add_radar-t2d3.3-0 ; 
       add_radar-t2d30.3-0 ; 
       add_radar-t2d31.3-0 ; 
       add_radar-t2d32.3-0 ; 
       add_radar-t2d33.3-0 ; 
       add_radar-t2d34.3-0 ; 
       add_radar-t2d35.3-0 ; 
       add_radar-t2d36.3-0 ; 
       add_radar-t2d37.3-0 ; 
       add_radar-t2d38.3-0 ; 
       add_radar-t2d39.3-0 ; 
       add_radar-t2d4.3-0 ; 
       add_radar-t2d40.3-0 ; 
       add_radar-t2d41.3-0 ; 
       add_radar-t2d42.3-0 ; 
       add_radar-t2d43.3-0 ; 
       add_radar-t2d44.5-0 ; 
       add_radar-t2d45.6-0 ; 
       add_radar-t2d46.4-0 ; 
       add_radar-t2d5.3-0 ; 
       add_radar-t2d6.3-0 ; 
       add_radar-t2d7.3-0 ; 
       add_radar-t2d8.3-0 ; 
       add_radar-t2d9.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 15 110 ; 
       2 8 110 ; 
       3 9 110 ; 
       4 7 110 ; 
       5 14 110 ; 
       6 16 110 ; 
       7 6 110 ; 
       8 6 110 ; 
       9 6 110 ; 
       10 16 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 10 110 ; 
       15 16 110 ; 
       16 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       0 17 300 ; 
       0 18 300 ; 
       0 19 300 ; 
       0 20 300 ; 
       0 21 300 ; 
       0 22 300 ; 
       0 41 300 ; 
       1 5 300 ; 
       1 0 300 ; 
       1 6 300 ; 
       5 1 300 ; 
       5 15 300 ; 
       5 26 300 ; 
       5 10 300 ; 
       5 11 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 16 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       7 4 300 ; 
       7 29 300 ; 
       8 1 300 ; 
       8 37 300 ; 
       8 43 300 ; 
       8 28 300 ; 
       9 1 300 ; 
       9 44 300 ; 
       9 45 300 ; 
       9 27 300 ; 
       11 1 300 ; 
       11 46 300 ; 
       11 25 300 ; 
       11 33 300 ; 
       11 34 300 ; 
       11 40 300 ; 
       12 1 300 ; 
       12 7 300 ; 
       12 24 300 ; 
       12 35 300 ; 
       12 36 300 ; 
       12 38 300 ; 
       13 1 300 ; 
       13 42 300 ; 
       13 8 300 ; 
       13 23 300 ; 
       13 31 300 ; 
       13 32 300 ; 
       13 39 300 ; 
       15 3 300 ; 
       16 1 300 ; 
       16 9 300 ; 
       16 30 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 40 400 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 37 401 ; 
       2 44 401 ; 
       3 38 401 ; 
       4 45 401 ; 
       5 39 401 ; 
       7 1 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       15 0 401 ; 
       16 10 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 15 401 ; 
       21 16 401 ; 
       22 17 401 ; 
       23 18 401 ; 
       24 19 401 ; 
       25 20 401 ; 
       26 11 401 ; 
       27 21 401 ; 
       28 23 401 ; 
       29 24 401 ; 
       30 25 401 ; 
       31 26 401 ; 
       32 27 401 ; 
       33 28 401 ; 
       34 29 401 ; 
       35 30 401 ; 
       36 31 401 ; 
       37 22 401 ; 
       38 32 401 ; 
       39 34 401 ; 
       40 35 401 ; 
       41 36 401 ; 
       42 2 401 ; 
       43 33 401 ; 
       44 41 401 ; 
       45 42 401 ; 
       46 43 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2.5 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 10 -10 0 MPRFLG 0 ; 
       4 SCHEM 5 -10 0 MPRFLG 0 ; 
       5 SCHEM 13.75 -2 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 15 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 15 -8 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 13.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       15 SCHEM 20 -6 0 MPRFLG 0 ; 
       16 SCHEM 12.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 21.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 16.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 11.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 14 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 16.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 19 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 11.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 25 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
