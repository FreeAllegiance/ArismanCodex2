SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       pilon_F-ss10.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon_F-light1.1-0 ROOT ; 
       pilon_F-light2.1-0 ROOT ; 
       pilon_F-light3.1-0 ROOT ; 
       pilon_F-light4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 23     
       pilon_F-mat1.1-0 ; 
       pilon_F-mat15.1-0 ; 
       pilon_F-mat16.1-0 ; 
       pilon_F-mat17.1-0 ; 
       pilon_F-mat18.1-0 ; 
       pilon_F-mat19.1-0 ; 
       pilon_F-mat2.1-0 ; 
       pilon_F-mat20.1-0 ; 
       pilon_F-mat21.1-0 ; 
       pilon_F-mat22.1-0 ; 
       pilon_F-mat23.1-0 ; 
       pilon_F-mat24.1-0 ; 
       pilon_F-mat25.1-0 ; 
       pilon_F-mat26.1-0 ; 
       pilon_F-mat27.1-0 ; 
       pilon_F-mat28.1-0 ; 
       pilon_F-mat29.1-0 ; 
       pilon_F-mat3.1-0 ; 
       pilon_F-mat30.1-0 ; 
       pilon_F-mat4.1-0 ; 
       pilon_F-mat5.1-0 ; 
       pilon_F-mat6.1-0 ; 
       pilon_F-mat7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       pilon_F-bfuselg.1-0 ; 
       pilon_F-doccon.1-0 ; 
       pilon_F-fuselg.1-0 ; 
       pilon_F-SS1.1-0 ; 
       pilon_F-ss10.1-0 ROOT ; 
       pilon_F-SS2.1-0 ; 
       pilon_F-SS3.1-0 ; 
       pilon_F-SS4.1-0 ; 
       pilon_F-tfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss10/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss10-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 14     
       pilon_F-t2d1.1-0 ; 
       pilon_F-t2d11.1-0 ; 
       pilon_F-t2d12.1-0 ; 
       pilon_F-t2d13.1-0 ; 
       pilon_F-t2d14.1-0 ; 
       pilon_F-t2d15.1-0 ; 
       pilon_F-t2d16.1-0 ; 
       pilon_F-t2d17.1-0 ; 
       pilon_F-t2d18.1-0 ; 
       pilon_F-t2d19.1-0 ; 
       pilon_F-t2d2.1-0 ; 
       pilon_F-t2d3.1-0 ; 
       pilon_F-t2d4.1-0 ; 
       pilon_F-t2d5.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       0 2 110 ; 
       1 8 110 ; 
       2 4 110 ; 
       8 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 7 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       7 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       0 18 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       8 0 300 ; 
       8 6 300 ; 
       8 17 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       13 5 401 ; 
       14 6 401 ; 
       15 7 401 ; 
       16 8 401 ; 
       17 0 401 ; 
       18 9 401 ; 
       19 10 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 45 -6 0 MPRFLG 0 ; 
       5 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 50 -6 0 MPRFLG 0 ; 
       7 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 55 -6 0 MPRFLG 0 ; 
       1 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 48.75 -2 0 SRT 1 1 1 -1.570796 0 0 0 -3.955854 0 MPRFLG 0 ; 
       8 SCHEM 42.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 56.5 -4 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
