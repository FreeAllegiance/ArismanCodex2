SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.163-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.14-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bounding_model.24-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOUND-ss100-bios_station.16-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
       0 6 110 ; 
       1 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 7.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       0 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 8.75 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
