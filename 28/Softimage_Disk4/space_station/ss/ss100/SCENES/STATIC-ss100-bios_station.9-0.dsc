SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.120-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       ss100_nulls-cube1_27.1-0 ; 
       ss100_nulls-cube1_29.1-0 ; 
       ss100_nulls-cube1_30.1-0 ; 
       ss100_nulls-cube1_31.1-0 ; 
       ss100_nulls-cube12_25.1-0 ; 
       ss100_nulls-cube12_27.1-0 ; 
       ss100_nulls-cube12_28.1-0 ; 
       ss100_nulls-cube12_29.1-0 ; 
       ss100_nulls-cube13_2_21.1-0 ; 
       ss100_nulls-cube13_2_23.1-0 ; 
       ss100_nulls-cube13_2_24.1-0 ; 
       ss100_nulls-cube13_2_25.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube5_1.5-0 ROOT ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_6.1-0 ; 
       ss100_nulls-east_bay_11_8.1-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru40.1-0 ; 
       ss100_nulls-extru41.1-0 ; 
       ss100_nulls-extru42.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_1.1-0 ; 
       ss100_nulls-landing_lights_2.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2.1-0 ; 
       ss100_nulls-landing_lights2_1.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-null18.1-0 ; 
       ss100_nulls-null19.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss100-bios_station.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       6 2 110 ; 
       7 3 110 ; 
       8 0 110 ; 
       9 1 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 17 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 12 110 ; 
       23 12 110 ; 
       24 12 110 ; 
       25 12 110 ; 
       26 12 110 ; 
       27 12 110 ; 
       28 12 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 12 110 ; 
       34 12 110 ; 
       35 12 110 ; 
       36 12 110 ; 
       37 12 110 ; 
       38 18 110 ; 
       39 19 110 ; 
       40 20 110 ; 
       41 21 110 ; 
       42 20 110 ; 
       43 19 110 ; 
       44 18 110 ; 
       45 21 110 ; 
       46 17 110 ; 
       47 17 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 33.75 -2 0 MPRFLG 0 ; 
       2 SCHEM 93.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 92.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 30 -4 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 MPRFLG 0 ; 
       10 SCHEM 95 -4 0 MPRFLG 0 ; 
       11 SCHEM 40 -4 0 MPRFLG 0 ; 
       12 SCHEM 66.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       14 SCHEM 45 -4 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       16 SCHEM 50 -4 0 MPRFLG 0 ; 
       17 SCHEM 48.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       19 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       20 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       21 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       23 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 85 -4 0 MPRFLG 0 ; 
       26 SCHEM 87.5 -4 0 MPRFLG 0 ; 
       27 SCHEM 90 -4 0 MPRFLG 0 ; 
       28 SCHEM 75 -4 0 MPRFLG 0 ; 
       29 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       30 SCHEM 80 -4 0 MPRFLG 0 ; 
       31 SCHEM 55 -4 0 MPRFLG 0 ; 
       32 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       33 SCHEM 60 -4 0 MPRFLG 0 ; 
       34 SCHEM 65 -4 0 MPRFLG 0 ; 
       35 SCHEM 67.5 -4 0 MPRFLG 0 ; 
       36 SCHEM 70 -4 0 MPRFLG 0 ; 
       37 SCHEM 72.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       41 SCHEM 15 -4 0 MPRFLG 0 ; 
       42 SCHEM 25 -4 0 MPRFLG 0 ; 
       43 SCHEM 5 -4 0 MPRFLG 0 ; 
       44 SCHEM 10 -4 0 MPRFLG 0 ; 
       45 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       46 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       47 SCHEM 20 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
