SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.125-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 107     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bound05.1-0 ; 
       bounding_model-bound06.1-0 ; 
       bounding_model-bounding_model.12-0 ROOT ; 
       ss100_nulls-cube1_27.1-0 ; 
       ss100_nulls-cube1_29.1-0 ; 
       ss100_nulls-cube1_30.1-0 ; 
       ss100_nulls-cube1_31.1-0 ; 
       ss100_nulls-cube12_25.1-0 ; 
       ss100_nulls-cube12_27.1-0 ; 
       ss100_nulls-cube12_28.1-0 ; 
       ss100_nulls-cube12_29.1-0 ; 
       ss100_nulls-cube13_2_21.1-0 ; 
       ss100_nulls-cube13_2_23.1-0 ; 
       ss100_nulls-cube13_2_24.1-0 ; 
       ss100_nulls-cube13_2_25.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube5_1.8-0 ROOT ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_6.1-0 ; 
       ss100_nulls-east_bay_11_8.1-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru40.1-0 ; 
       ss100_nulls-extru41.1-0 ; 
       ss100_nulls-extru42.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-garage1A.1-0 ; 
       ss100_nulls-garage1B.1-0 ; 
       ss100_nulls-garage1C.1-0 ; 
       ss100_nulls-garage1D.1-0 ; 
       ss100_nulls-garage1E.1-0 ; 
       ss100_nulls-garage2A.1-0 ; 
       ss100_nulls-garage2B.1-0 ; 
       ss100_nulls-garage2C.1-0 ; 
       ss100_nulls-garage2D.1-0 ; 
       ss100_nulls-garage2E.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_1.1-0 ; 
       ss100_nulls-landing_lights_2.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2.1-0 ; 
       ss100_nulls-landing_lights2_1.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-launch1.1-0 ; 
       ss100_nulls-launch2.1-0 ; 
       ss100_nulls-null18.1-0 ; 
       ss100_nulls-null19.1-0 ; 
       ss100_nulls-SS_11.1-0 ; 
       ss100_nulls-SS_11_1.1-0 ; 
       ss100_nulls-SS_11_2.1-0 ; 
       ss100_nulls-SS_11_3.1-0 ; 
       ss100_nulls-SS_13.1-0 ; 
       ss100_nulls-SS_13_1.1-0 ; 
       ss100_nulls-SS_13_2.1-0 ; 
       ss100_nulls-SS_13_3.1-0 ; 
       ss100_nulls-SS_15.1-0 ; 
       ss100_nulls-SS_15_1.1-0 ; 
       ss100_nulls-SS_15_2.1-0 ; 
       ss100_nulls-SS_15_3.1-0 ; 
       ss100_nulls-SS_23.1-0 ; 
       ss100_nulls-SS_23_1.1-0 ; 
       ss100_nulls-SS_23_2.1-0 ; 
       ss100_nulls-SS_23_3.1-0 ; 
       ss100_nulls-SS_24.1-0 ; 
       ss100_nulls-SS_24_1.1-0 ; 
       ss100_nulls-SS_24_2.1-0 ; 
       ss100_nulls-SS_24_3.1-0 ; 
       ss100_nulls-SS_26.1-0 ; 
       ss100_nulls-SS_26_1.1-0 ; 
       ss100_nulls-SS_26_2.1-0 ; 
       ss100_nulls-SS_26_3.1-0 ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-SS_56.1-0 ; 
       ss100_nulls-SS_58.1-0 ; 
       ss100_nulls-SS_59.1-0 ; 
       ss100_nulls-SS_60.1-0 ; 
       ss100_nulls-turwepemt2.1-0 ; 
       ss100_nulls-turwepemt2_1.1-0 ; 
       ss100_nulls-turwepemt2_2.1-0 ; 
       ss100_nulls-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       both-ss100-bios_station.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 24 110 ; 
       8 24 110 ; 
       9 24 110 ; 
       5 6 110 ; 
       10 24 110 ; 
       11 7 110 ; 
       12 8 110 ; 
       13 9 110 ; 
       14 10 110 ; 
       15 7 110 ; 
       16 8 110 ; 
       17 9 110 ; 
       18 10 110 ; 
       19 24 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       25 24 110 ; 
       26 24 110 ; 
       27 24 110 ; 
       28 24 110 ; 
       29 19 110 ; 
       30 19 110 ; 
       31 19 110 ; 
       32 19 110 ; 
       33 19 110 ; 
       34 19 110 ; 
       35 19 110 ; 
       36 19 110 ; 
       37 19 110 ; 
       38 19 110 ; 
       39 19 110 ; 
       40 19 110 ; 
       41 19 110 ; 
       42 19 110 ; 
       43 19 110 ; 
       44 19 110 ; 
       45 27 110 ; 
       46 27 110 ; 
       47 27 110 ; 
       48 27 110 ; 
       49 27 110 ; 
       50 26 110 ; 
       51 26 110 ; 
       52 26 110 ; 
       53 26 110 ; 
       54 26 110 ; 
       55 25 110 ; 
       56 26 110 ; 
       57 27 110 ; 
       58 28 110 ; 
       59 27 110 ; 
       60 26 110 ; 
       61 25 110 ; 
       62 28 110 ; 
       63 28 110 ; 
       64 25 110 ; 
       65 24 110 ; 
       66 24 110 ; 
       67 58 110 ; 
       68 57 110 ; 
       69 56 110 ; 
       70 55 110 ; 
       71 55 110 ; 
       72 56 110 ; 
       73 57 110 ; 
       74 58 110 ; 
       75 56 110 ; 
       76 57 110 ; 
       77 55 110 ; 
       78 58 110 ; 
       79 62 110 ; 
       80 60 110 ; 
       81 59 110 ; 
       82 61 110 ; 
       83 62 110 ; 
       84 59 110 ; 
       85 61 110 ; 
       86 60 110 ; 
       87 59 110 ; 
       88 60 110 ; 
       89 61 110 ; 
       90 62 110 ; 
       91 65 110 ; 
       92 65 110 ; 
       93 65 110 ; 
       94 65 110 ; 
       95 66 110 ; 
       96 66 110 ; 
       97 66 110 ; 
       98 66 110 ; 
       99 11 110 ; 
       100 12 110 ; 
       101 13 110 ; 
       102 14 110 ; 
       103 27 110 ; 
       104 26 110 ; 
       105 25 110 ; 
       106 28 110 ; 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 477.6909 -6 0 MPRFLG 0 ; 
       8 SCHEM 482.6909 -6 0 MPRFLG 0 ; 
       9 SCHEM 542.6909 -6 0 MPRFLG 0 ; 
       5 SCHEM 375.519 -1.594696 0 USR MPRFLG 0 ; 
       10 SCHEM 487.6909 -6 0 MPRFLG 0 ; 
       11 SCHEM 476.4409 -8 0 MPRFLG 0 ; 
       12 SCHEM 481.4409 -8 0 MPRFLG 0 ; 
       13 SCHEM 541.4409 -8 0 MPRFLG 0 ; 
       14 SCHEM 486.4409 -8 0 MPRFLG 0 ; 
       15 SCHEM 478.9409 -8 0 MPRFLG 0 ; 
       16 SCHEM 483.9409 -8 0 MPRFLG 0 ; 
       17 SCHEM 543.9409 -8 0 MPRFLG 0 ; 
       18 SCHEM 488.9409 -8 0 MPRFLG 0 ; 
       19 SCHEM 515.1909 -6 0 MPRFLG 0 ; 
       20 SCHEM 491.4409 -8 0 MPRFLG 0 ; 
       21 SCHEM 493.9409 -8 0 MPRFLG 0 ; 
       22 SCHEM 496.4409 -8 0 MPRFLG 0 ; 
       23 SCHEM 498.9409 -8 0 MPRFLG 0 ; 
       24 SCHEM 450.1909 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 395.1909 -6 0 MPRFLG 0 ; 
       26 SCHEM 370.1909 -6 0 MPRFLG 0 ; 
       27 SCHEM 460.1909 -6 0 MPRFLG 0 ; 
       28 SCHEM 425.1909 -6 0 MPRFLG 0 ; 
       29 SCHEM 511.4409 -8 0 MPRFLG 0 ; 
       30 SCHEM 531.4409 -8 0 MPRFLG 0 ; 
       31 SCHEM 501.4409 -8 0 MPRFLG 0 ; 
       32 SCHEM 533.9409 -8 0 MPRFLG 0 ; 
       33 SCHEM 536.4409 -8 0 MPRFLG 0 ; 
       34 SCHEM 538.9409 -8 0 MPRFLG 0 ; 
       35 SCHEM 523.9409 -8 0 MPRFLG 0 ; 
       36 SCHEM 526.4409 -8 0 MPRFLG 0 ; 
       37 SCHEM 528.9409 -8 0 MPRFLG 0 ; 
       38 SCHEM 503.9409 -8 0 MPRFLG 0 ; 
       39 SCHEM 506.4409 -8 0 MPRFLG 0 ; 
       40 SCHEM 508.9409 -8 0 MPRFLG 0 ; 
       41 SCHEM 513.9409 -8 0 MPRFLG 0 ; 
       42 SCHEM 516.4409 -8 0 MPRFLG 0 ; 
       43 SCHEM 518.9409 -8 0 MPRFLG 0 ; 
       44 SCHEM 521.4409 -8 0 MPRFLG 0 ; 
       45 SCHEM 461.4409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 468.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 463.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 473.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       49 SCHEM 471.4409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       50 SCHEM 371.4409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       51 SCHEM 378.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       52 SCHEM 373.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 383.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       54 SCHEM 381.4409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       55 SCHEM 388.9409 -8 0 MPRFLG 0 ; 
       56 SCHEM 358.9409 -8 0 MPRFLG 0 ; 
       57 SCHEM 448.9409 -8 0 MPRFLG 0 ; 
       58 SCHEM 418.9409 -8 0 MPRFLG 0 ; 
       59 SCHEM 456.4409 -8 0 MPRFLG 0 ; 
       60 SCHEM 366.4409 -8 0 MPRFLG 0 ; 
       61 SCHEM 396.4409 -8 0 MPRFLG 0 ; 
       62 SCHEM 426.4409 -8 0 MPRFLG 0 ; 
       63 SCHEM 433.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       64 SCHEM 403.9409 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       65 SCHEM 410.1909 -6 0 MPRFLG 0 ; 
       66 SCHEM 440.1909 -6 0 MPRFLG 0 ; 
       67 SCHEM 421.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 451.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 361.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 391.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 386.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 356.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 446.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 416.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 358.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 448.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 388.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 418.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 428.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 368.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 458.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 398.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 423.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 453.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 393.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 363.9409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 456.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 366.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 396.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 426.4409 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 406.4409 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       92 SCHEM 408.9409 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       93 SCHEM 411.4409 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       94 SCHEM 413.9409 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       95 SCHEM 443.9409 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       96 SCHEM 441.4409 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       97 SCHEM 438.9409 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       98 SCHEM 436.4409 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       99 SCHEM 476.4409 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 481.4409 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       101 SCHEM 541.4409 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       102 SCHEM 486.4409 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM 466.4409 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       104 SCHEM 376.4409 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       105 SCHEM 401.4409 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       106 SCHEM 431.4409 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 363.078 -1.633888 0 MPRFLG 0 ; 
       1 SCHEM 365.578 -1.633888 0 MPRFLG 0 ; 
       2 SCHEM 368.078 -1.633888 0 MPRFLG 0 ; 
       3 SCHEM 370.578 -1.633888 0 MPRFLG 0 ; 
       4 SCHEM 373.078 -1.633888 0 MPRFLG 0 ; 
       6 SCHEM 368.078 0.3661116 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
