SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.3-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       biostation-light1.3-0 ROOT ; 
       biostation-light2.3-0 ROOT ; 
       biostation-light3.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 106     
       biostation-cube1_2.1-0 ; 
       biostation-cube1_2_1.1-0 ; 
       biostation-cube1_4.1-0 ; 
       biostation-cube1_5.1-0 ; 
       biostation-cube12_2.1-0 ; 
       biostation-cube12_2_1.1-0 ; 
       biostation-cube12_4.1-0 ; 
       biostation-cube12_5.1-0 ; 
       biostation-cube13_2.1-0 ; 
       biostation-cube13_2_1.1-0 ; 
       biostation-cube13_4.1-0 ; 
       biostation-cube13_5.1-0 ; 
       biostation-cube20.1-0 ; 
       biostation-cube5.2-0 ; 
       biostation-east_bay_11.3-0 ; 
       biostation-east_bay_11_1.4-0 ; 
       biostation-east_bay_11_2.3-0 ; 
       biostation-east_bay_11_3.3-0 ; 
       biostation-extru11.1-0 ; 
       biostation-extru12.1-0 ; 
       biostation-extru13.1-0 ; 
       biostation-extru15.1-0 ; 
       biostation-extru15_1.1-0 ; 
       biostation-extru22.1-0 ; 
       biostation-extru27.1-0 ; 
       biostation-extru28.1-0 ; 
       biostation-extru29.1-0 ; 
       biostation-extru3.1-0 ; 
       biostation-extru3_1_1.1-0 ; 
       biostation-extru30.1-0 ; 
       biostation-extru31.1-0 ; 
       biostation-extru32.1-0 ; 
       biostation-extru33.1-0 ; 
       biostation-extru9.1-0 ; 
       biostation-garage1A.1-0 ; 
       biostation-garage1B.1-0 ; 
       biostation-garage1C.1-0 ; 
       biostation-garage1D.1-0 ; 
       biostation-garage1E.1-0 ; 
       biostation-garage2A.1-0 ; 
       biostation-garage2B.1-0 ; 
       biostation-garage2C.1-0 ; 
       biostation-garage2D.1-0 ; 
       biostation-garage2E.1-0 ; 
       biostation-landing_lights.1-0 ; 
       biostation-landing_lights_1.1-0 ; 
       biostation-landing_lights_2.1-0 ; 
       biostation-landing_lights_3.1-0 ; 
       biostation-landing_lights2.1-0 ; 
       biostation-landing_lights2_1.1-0 ; 
       biostation-landing_lights2_2.1-0 ; 
       biostation-landing_lights2_3.1-0 ; 
       biostation-launch1.1-0 ; 
       biostation-launch2.1-0 ; 
       biostation-null12.1-0 ; 
       biostation-null13.3-0 ROOT ; 
       biostation-null14.1-0 ; 
       biostation-null15.1-0 ; 
       biostation-null17.1-0 ; 
       biostation-null18.1-0 ; 
       biostation-null19.1-0 ; 
       biostation-null8.1-0 ; 
       biostation-SS_01.1-0 ; 
       biostation-SS_02.1-0 ; 
       biostation-SS_03.1-0 ; 
       biostation-SS_04.1-0 ; 
       biostation-SS_11.1-0 ; 
       biostation-SS_11_1.1-0 ; 
       biostation-SS_11_2.1-0 ; 
       biostation-SS_11_3.1-0 ; 
       biostation-SS_13.1-0 ; 
       biostation-SS_13_1.1-0 ; 
       biostation-SS_13_2.1-0 ; 
       biostation-SS_13_3.1-0 ; 
       biostation-SS_15.1-0 ; 
       biostation-SS_15_1.1-0 ; 
       biostation-SS_15_2.1-0 ; 
       biostation-SS_15_3.1-0 ; 
       biostation-SS_23.1-0 ; 
       biostation-SS_23_1.1-0 ; 
       biostation-SS_23_2.1-0 ; 
       biostation-SS_23_3.1-0 ; 
       biostation-SS_24.1-0 ; 
       biostation-SS_24_1.1-0 ; 
       biostation-SS_24_2.1-0 ; 
       biostation-SS_24_3.1-0 ; 
       biostation-SS_26.1-0 ; 
       biostation-SS_26_1.1-0 ; 
       biostation-SS_26_2.1-0 ; 
       biostation-SS_26_3.1-0 ; 
       biostation-SS_29.1-0 ; 
       biostation-SS_30.1-0 ; 
       biostation-SS_31.1-0 ; 
       biostation-SS_32.1-0 ; 
       biostation-SS_33.1-0 ; 
       biostation-SS_34.1-0 ; 
       biostation-SS_35.1-0 ; 
       biostation-SS_36.1-0 ; 
       biostation-strobe_set.1-0 ; 
       biostation-strobe_set_1.1-0 ; 
       biostation-strobe_set_2.1-0 ; 
       biostation-strobe_set_3.1-0 ; 
       biostation-turwepemt2.1-0 ; 
       biostation-turwepemt2_1.1-0 ; 
       biostation-turwepemt2_2.1-0 ; 
       biostation-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-biostation.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       12 55 110 ; 
       13 55 110 ; 
       56 12 110 ; 
       28 56 110 ; 
       29 56 110 ; 
       30 56 110 ; 
       57 12 110 ; 
       22 57 110 ; 
       31 57 110 ; 
       18 61 110 ; 
       19 61 110 ; 
       20 61 110 ; 
       21 57 110 ; 
       32 57 110 ; 
       23 54 110 ; 
       0 58 110 ; 
       24 54 110 ; 
       25 54 110 ; 
       26 54 110 ; 
       27 56 110 ; 
       4 0 110 ; 
       9 0 110 ; 
       33 61 110 ; 
       54 13 110 ; 
       61 13 110 ; 
       58 13 110 ; 
       1 58 110 ; 
       5 1 110 ; 
       65 5 110 ; 
       8 1 110 ; 
       15 55 110 ; 
       2 58 110 ; 
       6 2 110 ; 
       63 6 110 ; 
       10 2 110 ; 
       3 58 110 ; 
       7 3 110 ; 
       64 7 110 ; 
       11 3 110 ; 
       90 59 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 15 110 ; 
       37 15 110 ; 
       38 15 110 ; 
       46 99 110 ; 
       48 99 110 ; 
       91 59 110 ; 
       92 59 110 ; 
       59 55 110 ; 
       60 55 110 ; 
       94 60 110 ; 
       95 60 110 ; 
       96 60 110 ; 
       97 60 110 ; 
       62 4 110 ; 
       67 46 110 ; 
       72 46 110 ; 
       75 46 110 ; 
       80 48 110 ; 
       83 48 110 ; 
       86 48 110 ; 
       99 15 110 ; 
       102 15 110 ; 
       17 55 110 ; 
       39 17 110 ; 
       40 17 110 ; 
       41 17 110 ; 
       42 17 110 ; 
       43 17 110 ; 
       45 98 110 ; 
       49 98 110 ; 
       68 45 110 ; 
       71 45 110 ; 
       74 45 110 ; 
       79 49 110 ; 
       85 49 110 ; 
       93 59 110 ; 
       87 49 110 ; 
       98 17 110 ; 
       103 17 110 ; 
       16 55 110 ; 
       44 100 110 ; 
       50 100 110 ; 
       53 16 110 ; 
       69 44 110 ; 
       70 44 110 ; 
       76 44 110 ; 
       81 50 110 ; 
       84 50 110 ; 
       88 50 110 ; 
       100 16 110 ; 
       104 16 110 ; 
       14 55 110 ; 
       47 101 110 ; 
       51 101 110 ; 
       52 14 110 ; 
       66 47 110 ; 
       73 47 110 ; 
       77 47 110 ; 
       78 51 110 ; 
       82 51 110 ; 
       89 51 110 ; 
       101 14 110 ; 
       105 14 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1136.566 5.220468 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1136.566 3.220467 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 1118.865 3.412667 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 1121.365 3.412667 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 1123.865 3.412667 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       12 SCHEM 1133.179 -16.02057 0 USR MPRFLG 0 ; 
       13 SCHEM 1153.629 -20.46017 0 USR MPRFLG 0 ; 
       56 SCHEM 1129.389 -17.95831 0 USR MPRFLG 0 ; 
       28 SCHEM 1131.889 -19.9583 0 USR MPRFLG 0 ; 
       29 SCHEM 1126.889 -19.9583 0 USR MPRFLG 0 ; 
       30 SCHEM 1129.389 -19.9583 0 USR MPRFLG 0 ; 
       57 SCHEM 1140.367 -17.89649 0 USR MPRFLG 0 ; 
       22 SCHEM 1142.867 -19.89649 0 USR MPRFLG 0 ; 
       31 SCHEM 1137.867 -19.89649 0 USR MPRFLG 0 ; 
       18 SCHEM 1142.379 -24.46018 0 USR MPRFLG 0 ; 
       19 SCHEM 1137.379 -24.46018 0 USR MPRFLG 0 ; 
       20 SCHEM 1139.879 -24.46018 0 USR MPRFLG 0 ; 
       21 SCHEM 1135.297 -19.93202 0 USR MPRFLG 0 ; 
       32 SCHEM 1140.367 -19.89649 0 USR MPRFLG 0 ; 
       23 SCHEM 1164.879 -24.46018 0 USR MPRFLG 0 ; 
       0 SCHEM 1146.558 -24.20122 0 USR MPRFLG 0 ; 
       24 SCHEM 1172.379 -24.46018 0 USR MPRFLG 0 ; 
       25 SCHEM 1167.379 -24.46018 0 USR MPRFLG 0 ; 
       26 SCHEM 1169.879 -24.46018 0 USR MPRFLG 0 ; 
       27 SCHEM 1124.766 -19.9558 0 USR MPRFLG 0 ; 
       4 SCHEM 1145.308 -26.20122 0 USR MPRFLG 0 ; 
       9 SCHEM 1147.808 -26.20122 0 USR MPRFLG 0 ; 
       33 SCHEM 1134.879 -24.46018 0 USR MPRFLG 0 ; 
       54 SCHEM 1168.629 -22.46017 0 USR MPRFLG 0 ; 
       55 SCHEM 1135.377 -2.719759 0 USR SRT 1 1 1 -1.570796 -3.141593 0 0 31.39535 0 MPRFLG 0 ; 
       61 SCHEM 1138.629 -22.46017 0 USR MPRFLG 0 ; 
       58 SCHEM 1156.545 -22.24988 0 USR MPRFLG 0 ; 
       1 SCHEM 1161.545 -24.24988 0 USR MPRFLG 0 ; 
       5 SCHEM 1160.295 -26.24988 0 USR MPRFLG 0 ; 
       65 SCHEM 1160.295 -28.24988 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1162.795 -26.24988 0 USR MPRFLG 0 ; 
       15 SCHEM 1094.156 -22.75307 0 USR MPRFLG 0 ; 
       2 SCHEM 1151.545 -24.24988 0 USR MPRFLG 0 ; 
       6 SCHEM 1150.295 -26.24988 0 USR MPRFLG 0 ; 
       63 SCHEM 1150.295 -28.24988 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1152.795 -26.24988 0 USR MPRFLG 0 ; 
       3 SCHEM 1156.545 -24.24988 0 USR MPRFLG 0 ; 
       7 SCHEM 1155.295 -26.24988 0 USR MPRFLG 0 ; 
       64 SCHEM 1155.295 -28.24988 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1157.795 -26.24988 0 USR MPRFLG 0 ; 
       90 SCHEM 1155.369 -7.870952 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 1095.406 -24.75307 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 1102.906 -24.75307 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 1097.906 -24.75307 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 1107.906 -24.75307 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 1105.406 -24.75307 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 1082.906 -26.75307 0 USR MPRFLG 0 ; 
       48 SCHEM 1090.406 -26.75307 0 USR MPRFLG 0 ; 
       91 SCHEM 1157.869 -7.870952 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       92 SCHEM 1160.369 -7.870952 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 1159.119 -5.870951 0 USR MPRFLG 0 ; 
       60 SCHEM 1159.513 -9.672518 0 USR MPRFLG 0 ; 
       94 SCHEM 1163.263 -11.67252 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       95 SCHEM 1160.763 -11.67252 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       96 SCHEM 1158.263 -11.67252 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       97 SCHEM 1155.763 -11.67252 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 1145.448 -27.90052 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 1085.406 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 1080.406 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 1082.906 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 1092.906 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 1087.906 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 1090.406 -28.75307 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 1086.656 -24.75307 0 USR MPRFLG 0 ; 
       102 SCHEM 1100.406 -24.75307 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 1098.961 -33.01749 0 USR MPRFLG 0 ; 
       39 SCHEM 1100.211 -35.01749 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 1107.711 -35.01749 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 1102.711 -35.01749 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 1112.711 -35.01749 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 1110.211 -35.01749 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 1087.711 -37.01749 0 USR MPRFLG 0 ; 
       49 SCHEM 1095.211 -37.01749 0 USR MPRFLG 0 ; 
       68 SCHEM 1090.211 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 1085.211 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 1087.711 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 1097.711 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 1092.711 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 1162.869 -7.870952 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       87 SCHEM 1095.211 -39.01749 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 1091.461 -35.01749 0 USR MPRFLG 0 ; 
       103 SCHEM 1105.211 -35.01749 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 1133.461 -32.77 0 USR MPRFLG 0 ; 
       44 SCHEM 1127.211 -36.77 0 USR MPRFLG 0 ; 
       50 SCHEM 1134.711 -36.77 0 USR MPRFLG 0 ; 
       53 SCHEM 1142.211 -34.77 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       69 SCHEM 1129.711 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 1124.711 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 1127.211 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 1137.211 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 1132.211 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 1134.711 -38.77 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       100 SCHEM 1130.961 -34.77 0 USR MPRFLG 0 ; 
       104 SCHEM 1139.711 -34.77 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 1120.495 -25.16468 0 USR MPRFLG 0 ; 
       47 SCHEM 1114.245 -29.16469 0 USR MPRFLG 0 ; 
       51 SCHEM 1121.745 -29.16469 0 USR MPRFLG 0 ; 
       52 SCHEM 1129.245 -27.16468 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       66 SCHEM 1116.745 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 1111.745 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 1114.245 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 1124.245 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 1119.245 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 1121.745 -31.16469 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 1117.995 -27.16468 0 USR MPRFLG 0 ; 
       105 SCHEM 1126.745 -27.16468 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
