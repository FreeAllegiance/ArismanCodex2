SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.6-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       bounding-cube1_6.1-0 ; 
       bounding-cube1_7.1-0 ; 
       bounding-cube1_8.1-0 ; 
       bounding-cube1_9.1-0 ; 
       bounding-null21.1-0 ; 
       bounding-sphere1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bounding.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       4 5 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 1136.566 5.220468 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1136.566 3.220467 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM 1134.326 -0.789907 0 USR DISPLAY 3 2 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 1130.576 -4.789907 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 1134.326 -2.789907 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 1138.076 -4.789907 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 1133.076 -4.789907 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 1135.576 -4.789907 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
