SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.7-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       bounding-cube1_6.1-0 ; 
       bounding-cube1_7.1-0 ; 
       bounding-cube1_8.1-0 ; 
       bounding-cube1_9.1-0 ; 
       bounding-null21.1-0 ROOT ; 
       bounding-sphere1.3-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bounding.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       5 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 0 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 10 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 2.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 5 -4 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141592 1.57853e-024 0 0 0 MPRFLG 0 ; 
       5 SCHEM 5 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
