SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.8-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       bounding-bound01.1-0 ; 
       bounding-bound02.1-0 ; 
       bounding-bound03.3-0 ; 
       bounding-bound04.1-0 ; 
       bounding-bound05.1-0 ; 
       bounding-null21.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-bounding.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       4 5 110 ; 
       1 5 110 ; 
       3 5 110 ; 
       2 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 7.5 0 0 SRT 1 1 1 -1.570796 3.141592 1.57853e-024 0 31.39259 -31.44454 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
