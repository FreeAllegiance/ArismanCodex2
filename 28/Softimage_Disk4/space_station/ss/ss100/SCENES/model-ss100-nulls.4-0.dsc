SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.13-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 106     
       ss100_nulls-cube1_2.1-0 ; 
       ss100_nulls-cube1_2_1.1-0 ; 
       ss100_nulls-cube1_4.1-0 ; 
       ss100_nulls-cube1_5.1-0 ; 
       ss100_nulls-cube12_2.1-0 ; 
       ss100_nulls-cube12_2_1.1-0 ; 
       ss100_nulls-cube12_4.1-0 ; 
       ss100_nulls-cube12_5.1-0 ; 
       ss100_nulls-cube13_2.1-0 ; 
       ss100_nulls-cube13_2_1.1-0 ; 
       ss100_nulls-cube13_4.1-0 ; 
       ss100_nulls-cube13_5.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube5.2-0 ; 
       ss100_nulls-east_bay_11.3-0 ; 
       ss100_nulls-east_bay_11_1.4-0 ; 
       ss100_nulls-east_bay_11_2.3-0 ; 
       ss100_nulls-east_bay_11_3.3-0 ; 
       ss100_nulls-extru11.1-0 ; 
       ss100_nulls-extru12.1-0 ; 
       ss100_nulls-extru13.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru15_1.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru27.1-0 ; 
       ss100_nulls-extru28.1-0 ; 
       ss100_nulls-extru29.1-0 ; 
       ss100_nulls-extru3.1-0 ; 
       ss100_nulls-extru3_1_1.1-0 ; 
       ss100_nulls-extru30.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru32.1-0 ; 
       ss100_nulls-extru33.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-garage1A.1-0 ; 
       ss100_nulls-garage1B.1-0 ; 
       ss100_nulls-garage1C.1-0 ; 
       ss100_nulls-garage1D.1-0 ; 
       ss100_nulls-garage1E.1-0 ; 
       ss100_nulls-garage2A.1-0 ; 
       ss100_nulls-garage2B.1-0 ; 
       ss100_nulls-garage2C.1-0 ; 
       ss100_nulls-garage2D.1-0 ; 
       ss100_nulls-garage2E.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_1.1-0 ; 
       ss100_nulls-landing_lights_2.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2.1-0 ; 
       ss100_nulls-landing_lights2_1.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-launch1.1-0 ; 
       ss100_nulls-launch2.1-0 ; 
       ss100_nulls-null12.1-0 ; 
       ss100_nulls-null13.3-0 ROOT ; 
       ss100_nulls-null14.1-0 ; 
       ss100_nulls-null15.1-0 ; 
       ss100_nulls-null17.1-0 ; 
       ss100_nulls-null18.1-0 ; 
       ss100_nulls-null19.1-0 ; 
       ss100_nulls-null8.1-0 ; 
       ss100_nulls-SS_01.1-0 ; 
       ss100_nulls-SS_02.1-0 ; 
       ss100_nulls-SS_03.1-0 ; 
       ss100_nulls-SS_04.1-0 ; 
       ss100_nulls-SS_11.1-0 ; 
       ss100_nulls-SS_11_1.1-0 ; 
       ss100_nulls-SS_11_2.1-0 ; 
       ss100_nulls-SS_11_3.1-0 ; 
       ss100_nulls-SS_13.1-0 ; 
       ss100_nulls-SS_13_1.1-0 ; 
       ss100_nulls-SS_13_2.1-0 ; 
       ss100_nulls-SS_13_3.1-0 ; 
       ss100_nulls-SS_15.1-0 ; 
       ss100_nulls-SS_15_1.1-0 ; 
       ss100_nulls-SS_15_2.1-0 ; 
       ss100_nulls-SS_15_3.1-0 ; 
       ss100_nulls-SS_23.1-0 ; 
       ss100_nulls-SS_23_1.1-0 ; 
       ss100_nulls-SS_23_2.1-0 ; 
       ss100_nulls-SS_23_3.1-0 ; 
       ss100_nulls-SS_24.1-0 ; 
       ss100_nulls-SS_24_1.1-0 ; 
       ss100_nulls-SS_24_2.1-0 ; 
       ss100_nulls-SS_24_3.1-0 ; 
       ss100_nulls-SS_26.1-0 ; 
       ss100_nulls-SS_26_1.1-0 ; 
       ss100_nulls-SS_26_2.1-0 ; 
       ss100_nulls-SS_26_3.1-0 ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-strobe_set.1-0 ; 
       ss100_nulls-strobe_set_1.1-0 ; 
       ss100_nulls-strobe_set_2.1-0 ; 
       ss100_nulls-strobe_set_3.1-0 ; 
       ss100_nulls-turwepemt2.1-0 ; 
       ss100_nulls-turwepemt2_1.1-0 ; 
       ss100_nulls-turwepemt2_2.1-0 ; 
       ss100_nulls-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss100-nulls.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 58 110 ; 
       1 58 110 ; 
       2 58 110 ; 
       3 58 110 ; 
       4 0 110 ; 
       5 1 110 ; 
       6 2 110 ; 
       7 3 110 ; 
       8 1 110 ; 
       9 0 110 ; 
       10 2 110 ; 
       11 3 110 ; 
       12 55 110 ; 
       13 55 110 ; 
       14 55 110 ; 
       15 55 110 ; 
       16 55 110 ; 
       17 55 110 ; 
       18 61 110 ; 
       19 61 110 ; 
       20 61 110 ; 
       21 57 110 ; 
       22 57 110 ; 
       23 54 110 ; 
       24 54 110 ; 
       25 54 110 ; 
       26 54 110 ; 
       27 56 110 ; 
       28 56 110 ; 
       29 56 110 ; 
       30 56 110 ; 
       31 57 110 ; 
       32 57 110 ; 
       33 61 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 15 110 ; 
       37 15 110 ; 
       38 15 110 ; 
       39 17 110 ; 
       40 17 110 ; 
       41 17 110 ; 
       42 17 110 ; 
       43 17 110 ; 
       44 100 110 ; 
       45 98 110 ; 
       46 99 110 ; 
       47 101 110 ; 
       48 99 110 ; 
       49 98 110 ; 
       50 100 110 ; 
       51 101 110 ; 
       52 14 110 ; 
       53 16 110 ; 
       54 13 110 ; 
       56 12 110 ; 
       57 12 110 ; 
       58 13 110 ; 
       59 55 110 ; 
       60 55 110 ; 
       61 13 110 ; 
       62 4 110 ; 
       63 6 110 ; 
       64 7 110 ; 
       65 5 110 ; 
       66 47 110 ; 
       67 46 110 ; 
       68 45 110 ; 
       69 44 110 ; 
       70 44 110 ; 
       71 45 110 ; 
       72 46 110 ; 
       73 47 110 ; 
       74 45 110 ; 
       75 46 110 ; 
       76 44 110 ; 
       77 47 110 ; 
       78 51 110 ; 
       79 49 110 ; 
       80 48 110 ; 
       81 50 110 ; 
       82 51 110 ; 
       83 48 110 ; 
       84 50 110 ; 
       85 49 110 ; 
       86 48 110 ; 
       87 49 110 ; 
       88 50 110 ; 
       89 51 110 ; 
       90 59 110 ; 
       91 59 110 ; 
       92 59 110 ; 
       93 59 110 ; 
       94 60 110 ; 
       95 60 110 ; 
       96 60 110 ; 
       97 60 110 ; 
       98 17 110 ; 
       99 15 110 ; 
       100 16 110 ; 
       101 14 110 ; 
       102 15 110 ; 
       103 17 110 ; 
       104 16 110 ; 
       105 14 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 66.15198 -50.91391 0 USR MPRFLG 0 ; 
       1 SCHEM 81.13904 -50.96257 0 USR MPRFLG 0 ; 
       2 SCHEM 71.13904 -50.96257 0 USR MPRFLG 0 ; 
       3 SCHEM 76.13904 -50.96257 0 USR MPRFLG 0 ; 
       4 SCHEM 64.90198 -52.91391 0 USR MPRFLG 0 ; 
       5 SCHEM 79.88904 -52.96257 0 USR MPRFLG 0 ; 
       6 SCHEM 69.88904 -52.96257 0 USR MPRFLG 0 ; 
       7 SCHEM 74.88904 -52.96257 0 USR MPRFLG 0 ; 
       8 SCHEM 82.38904 -52.96257 0 USR MPRFLG 0 ; 
       9 SCHEM 67.40198 -52.91391 0 USR MPRFLG 0 ; 
       10 SCHEM 72.38904 -52.96257 0 USR MPRFLG 0 ; 
       11 SCHEM 77.38904 -52.96257 0 USR MPRFLG 0 ; 
       12 SCHEM 52.77295 -42.73326 0 USR MPRFLG 0 ; 
       13 SCHEM 73.22302 -47.17286 0 USR MPRFLG 0 ; 
       14 SCHEM 40.08899 -51.87737 0 USR MPRFLG 0 ; 
       15 SCHEM 13.75 -49.46576 0 USR MPRFLG 0 ; 
       16 SCHEM 53.05505 -59.48269 0 USR MPRFLG 0 ; 
       17 SCHEM 18.55505 -59.73018 0 USR MPRFLG 0 ; 
       18 SCHEM 61.97302 -51.17287 0 USR MPRFLG 0 ; 
       19 SCHEM 56.97302 -51.17287 0 USR MPRFLG 0 ; 
       20 SCHEM 59.47302 -51.17287 0 USR MPRFLG 0 ; 
       21 SCHEM 54.89099 -46.64471 0 USR MPRFLG 0 ; 
       22 SCHEM 62.46094 -46.60918 0 USR MPRFLG 0 ; 
       23 SCHEM 84.47302 -51.17287 0 USR MPRFLG 0 ; 
       24 SCHEM 91.97302 -51.17287 0 USR MPRFLG 0 ; 
       25 SCHEM 86.97302 -51.17287 0 USR MPRFLG 0 ; 
       26 SCHEM 89.47302 -51.17287 0 USR MPRFLG 0 ; 
       27 SCHEM 44.35999 -46.66849 0 USR MPRFLG 0 ; 
       28 SCHEM 51.48303 -46.67099 0 USR MPRFLG 0 ; 
       29 SCHEM 46.48303 -46.67099 0 USR MPRFLG 0 ; 
       30 SCHEM 48.98303 -46.67099 0 USR MPRFLG 0 ; 
       31 SCHEM 57.46094 -46.60918 0 USR MPRFLG 0 ; 
       32 SCHEM 59.96094 -46.60918 0 USR MPRFLG 0 ; 
       33 SCHEM 54.47302 -51.17287 0 USR MPRFLG 0 ; 
       34 SCHEM 15 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 22.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       36 SCHEM 17.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       37 SCHEM 27.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 25 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 19.80505 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       40 SCHEM 27.30505 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       41 SCHEM 22.30505 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 32.30505 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 29.80505 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 46.80505 -63.48269 0 USR MPRFLG 0 ; 
       45 SCHEM 7.305054 -63.73018 0 USR MPRFLG 0 ; 
       46 SCHEM 2.5 -53.46576 0 USR MPRFLG 0 ; 
       47 SCHEM 33.83899 -55.87737 0 USR MPRFLG 0 ; 
       48 SCHEM 10 -53.46576 0 USR MPRFLG 0 ; 
       49 SCHEM 14.80505 -63.73018 0 USR MPRFLG 0 ; 
       50 SCHEM 54.30505 -63.48269 0 USR MPRFLG 0 ; 
       51 SCHEM 41.33899 -55.87737 0 USR MPRFLG 0 ; 
       52 SCHEM 48.83899 -53.87737 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 61.80505 -61.48269 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       54 SCHEM 88.22302 -49.17286 0 USR MPRFLG 0 ; 
       55 SCHEM 35.65516 -4.000001 0 SRT 1 1 1 -1.570796 -3.141593 0 0 31.39535 0 MPRFLG 0 ; 
       56 SCHEM 48.98303 -44.671 0 USR MPRFLG 0 ; 
       57 SCHEM 59.96094 -44.60918 0 USR MPRFLG 0 ; 
       58 SCHEM 76.13904 -48.96257 0 USR MPRFLG 0 ; 
       59 SCHEM 78.71301 -32.58364 0 USR MPRFLG 0 ; 
       60 SCHEM 79.10693 -36.3852 0 USR MPRFLG 0 ; 
       61 SCHEM 58.22302 -49.17286 0 USR MPRFLG 0 ; 
       62 SCHEM 65.04199 -54.61321 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 69.88904 -54.96257 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 74.88904 -54.96257 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 79.88904 -54.96257 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 36.33899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 9.805054 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 49.30505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 44.30505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 4.805054 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 0 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 31.33899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 7.305054 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 2.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 46.80505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 33.83899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 43.83899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 17.30505 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 12.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 56.80505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 38.83899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 7.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 51.80505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 12.30505 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 10 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 14.80505 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 54.30505 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 41.33899 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 74.96301 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       91 SCHEM 77.46301 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       92 SCHEM 79.96301 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       93 SCHEM 82.46301 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       94 SCHEM 82.85693 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       95 SCHEM 80.35693 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       96 SCHEM 77.85693 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       97 SCHEM 75.35693 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       98 SCHEM 11.05505 -61.73018 0 USR MPRFLG 0 ; 
       99 SCHEM 6.25 -51.46576 0 USR MPRFLG 0 ; 
       100 SCHEM 50.55505 -61.48269 0 USR MPRFLG 0 ; 
       101 SCHEM 37.58899 -53.87737 0 USR MPRFLG 0 ; 
       102 SCHEM 20 -51.46576 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM 24.80505 -61.73018 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       104 SCHEM 59.30505 -61.48269 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       105 SCHEM 46.33899 -53.87737 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
