SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.16-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 120     
       ss100_nulls-cube1_10.1-0 ; 
       ss100_nulls-cube1_11.1-0 ; 
       ss100_nulls-cube1_2.1-0 ; 
       ss100_nulls-cube1_2_1.1-0 ; 
       ss100_nulls-cube12_2.1-0 ; 
       ss100_nulls-cube12_2_1.1-0 ; 
       ss100_nulls-cube12_8.1-0 ; 
       ss100_nulls-cube12_9.1-0 ; 
       ss100_nulls-cube13_2_1.1-0 ; 
       ss100_nulls-cube13_2_1_1_1.1-0 ; 
       ss100_nulls-cube13_2_4.1-0 ; 
       ss100_nulls-cube13_2_5.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube21_1.1-0 ; 
       ss100_nulls-cube21_1_1.1-0 ; 
       ss100_nulls-cube21_2.1-0 ; 
       ss100_nulls-cube21_3.1-0 ; 
       ss100_nulls-cube22_1.1-0 ; 
       ss100_nulls-cube22_1_1.1-0 ; 
       ss100_nulls-cube22_2.1-0 ; 
       ss100_nulls-cube22_2_1.1-0 ; 
       ss100_nulls-cube22_3.1-0 ; 
       ss100_nulls-cube22_4.1-0 ; 
       ss100_nulls-cube22_5.1-0 ; 
       ss100_nulls-cube22_6.1-0 ; 
       ss100_nulls-cube5.2-0 ; 
       ss100_nulls-east_bay_11.3-0 ; 
       ss100_nulls-east_bay_11_1.4-0 ; 
       ss100_nulls-east_bay_11_2.3-0 ; 
       ss100_nulls-east_bay_11_3.3-0 ; 
       ss100_nulls-east_bay_11_4.1-0 ; 
       ss100_nulls-east_bay_11_5.1-0 ; 
       ss100_nulls-extru11.1-0 ; 
       ss100_nulls-extru12.1-0 ; 
       ss100_nulls-extru13.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru15_1.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru27.1-0 ; 
       ss100_nulls-extru28.1-0 ; 
       ss100_nulls-extru29.1-0 ; 
       ss100_nulls-extru3.1-0 ; 
       ss100_nulls-extru3_1_1.1-0 ; 
       ss100_nulls-extru30.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru32.1-0 ; 
       ss100_nulls-extru33.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-garage1A.1-0 ; 
       ss100_nulls-garage1B.1-0 ; 
       ss100_nulls-garage1C.1-0 ; 
       ss100_nulls-garage1D.1-0 ; 
       ss100_nulls-garage1E.1-0 ; 
       ss100_nulls-garage2A.1-0 ; 
       ss100_nulls-garage2B.1-0 ; 
       ss100_nulls-garage2C.1-0 ; 
       ss100_nulls-garage2D.1-0 ; 
       ss100_nulls-garage2E.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_1.1-0 ; 
       ss100_nulls-landing_lights_2.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2.1-0 ; 
       ss100_nulls-landing_lights2_1.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-launch1.1-0 ; 
       ss100_nulls-launch2.1-0 ; 
       ss100_nulls-null12.1-0 ; 
       ss100_nulls-null13.4-0 ROOT ; 
       ss100_nulls-null14.1-0 ; 
       ss100_nulls-null15.1-0 ; 
       ss100_nulls-null18.1-0 ; 
       ss100_nulls-null19.1-0 ; 
       ss100_nulls-null22.1-0 ; 
       ss100_nulls-null8.1-0 ; 
       ss100_nulls-SS_01.1-0 ; 
       ss100_nulls-SS_01_1_1.1-0 ; 
       ss100_nulls-SS_11.1-0 ; 
       ss100_nulls-SS_11_1.1-0 ; 
       ss100_nulls-SS_11_2.1-0 ; 
       ss100_nulls-SS_11_3.1-0 ; 
       ss100_nulls-SS_13.1-0 ; 
       ss100_nulls-SS_13_1.1-0 ; 
       ss100_nulls-SS_13_2.1-0 ; 
       ss100_nulls-SS_13_3.1-0 ; 
       ss100_nulls-SS_15.1-0 ; 
       ss100_nulls-SS_15_1.1-0 ; 
       ss100_nulls-SS_15_2.1-0 ; 
       ss100_nulls-SS_15_3.1-0 ; 
       ss100_nulls-SS_23.1-0 ; 
       ss100_nulls-SS_23_1.1-0 ; 
       ss100_nulls-SS_23_2.1-0 ; 
       ss100_nulls-SS_23_3.1-0 ; 
       ss100_nulls-SS_24.1-0 ; 
       ss100_nulls-SS_24_1.1-0 ; 
       ss100_nulls-SS_24_2.1-0 ; 
       ss100_nulls-SS_24_3.1-0 ; 
       ss100_nulls-SS_26.1-0 ; 
       ss100_nulls-SS_26_1.1-0 ; 
       ss100_nulls-SS_26_2.1-0 ; 
       ss100_nulls-SS_26_3.1-0 ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-SS_39.1-0 ; 
       ss100_nulls-SS_40.1-0 ; 
       ss100_nulls-strobe_set.1-0 ; 
       ss100_nulls-strobe_set_1.1-0 ; 
       ss100_nulls-strobe_set_2.1-0 ; 
       ss100_nulls-strobe_set_3.1-0 ; 
       ss100_nulls-turwepemt2.1-0 ; 
       ss100_nulls-turwepemt2_1.1-0 ; 
       ss100_nulls-turwepemt2_2.1-0 ; 
       ss100_nulls-turwepemt2_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ss100-nulls.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 74 110 ; 
       14 2 110 ; 
       17 2 110 ; 
       31 69 110 ; 
       30 69 110 ; 
       20 2 110 ; 
       74 25 110 ; 
       3 74 110 ; 
       5 3 110 ; 
       4 2 110 ; 
       77 5 110 ; 
       9 3 110 ; 
       13 3 110 ; 
       18 3 110 ; 
       19 3 110 ; 
       0 74 110 ; 
       6 0 110 ; 
       110 6 110 ; 
       10 0 110 ; 
       15 0 110 ; 
       21 0 110 ; 
       22 0 110 ; 
       1 74 110 ; 
       7 1 110 ; 
       111 7 110 ; 
       11 1 110 ; 
       16 1 110 ; 
       23 1 110 ; 
       24 1 110 ; 
       8 2 110 ; 
       12 69 110 ; 
       25 69 110 ; 
       26 69 110 ; 
       27 69 110 ; 
       28 69 110 ; 
       29 69 110 ; 
       32 75 110 ; 
       33 75 110 ; 
       34 75 110 ; 
       35 71 110 ; 
       36 71 110 ; 
       37 68 110 ; 
       38 68 110 ; 
       39 68 110 ; 
       40 68 110 ; 
       41 70 110 ; 
       42 70 110 ; 
       43 70 110 ; 
       44 70 110 ; 
       45 71 110 ; 
       46 71 110 ; 
       47 75 110 ; 
       48 27 110 ; 
       49 27 110 ; 
       50 27 110 ; 
       51 27 110 ; 
       52 27 110 ; 
       53 29 110 ; 
       54 29 110 ; 
       55 29 110 ; 
       56 29 110 ; 
       57 29 110 ; 
       58 114 110 ; 
       59 112 110 ; 
       60 113 110 ; 
       61 115 110 ; 
       62 113 110 ; 
       63 112 110 ; 
       64 114 110 ; 
       65 115 110 ; 
       66 26 110 ; 
       67 28 110 ; 
       68 25 110 ; 
       70 12 110 ; 
       71 12 110 ; 
       72 69 110 ; 
       73 69 110 ; 
       75 25 110 ; 
       76 4 110 ; 
       78 61 110 ; 
       79 60 110 ; 
       80 59 110 ; 
       81 58 110 ; 
       82 58 110 ; 
       83 59 110 ; 
       84 60 110 ; 
       85 61 110 ; 
       86 59 110 ; 
       87 60 110 ; 
       88 58 110 ; 
       89 61 110 ; 
       90 65 110 ; 
       91 63 110 ; 
       92 62 110 ; 
       93 64 110 ; 
       94 65 110 ; 
       95 62 110 ; 
       96 64 110 ; 
       97 63 110 ; 
       98 62 110 ; 
       99 63 110 ; 
       100 64 110 ; 
       101 65 110 ; 
       102 72 110 ; 
       103 72 110 ; 
       104 72 110 ; 
       105 72 110 ; 
       106 73 110 ; 
       107 73 110 ; 
       108 73 110 ; 
       109 73 110 ; 
       112 29 110 ; 
       113 27 110 ; 
       114 28 110 ; 
       115 26 110 ; 
       116 27 110 ; 
       117 29 110 ; 
       118 28 110 ; 
       119 26 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2.000001 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 230.924 -54.35346 0 MPRFLG 0 ; 
       14 SCHEM 230.924 -56.35346 0 MPRFLG 0 ; 
       17 SCHEM 233.424 -56.35346 0 MPRFLG 0 ; 
       31 SCHEM 201.0463 -46.00443 0 USR MPRFLG 0 ; 
       30 SCHEM 195.237 -46.32266 0 USR MPRFLG 0 ; 
       20 SCHEM 235.924 -56.35346 0 MPRFLG 0 ; 
       74 SCHEM 249.6739 -52.35346 0 USR MPRFLG 0 ; 
       3 SCHEM 268.424 -54.35346 0 MPRFLG 0 ; 
       5 SCHEM 263.424 -56.35346 0 MPRFLG 0 ; 
       4 SCHEM 225.924 -56.35346 0 MPRFLG 0 ; 
       77 SCHEM 263.424 -58.35346 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 265.924 -56.35346 0 MPRFLG 0 ; 
       13 SCHEM 268.424 -56.35346 0 MPRFLG 0 ; 
       18 SCHEM 270.924 -56.35346 0 MPRFLG 0 ; 
       19 SCHEM 273.424 -56.35346 0 MPRFLG 0 ; 
       0 SCHEM 243.424 -54.35346 0 MPRFLG 0 ; 
       6 SCHEM 238.424 -56.35346 0 MPRFLG 0 ; 
       110 SCHEM 238.424 -58.35346 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 240.924 -56.35346 0 MPRFLG 0 ; 
       15 SCHEM 243.424 -56.35346 0 MPRFLG 0 ; 
       21 SCHEM 245.924 -56.35346 0 MPRFLG 0 ; 
       22 SCHEM 248.424 -56.35346 0 MPRFLG 0 ; 
       1 SCHEM 255.9239 -54.35346 0 MPRFLG 0 ; 
       7 SCHEM 250.9239 -56.35346 0 MPRFLG 0 ; 
       111 SCHEM 250.9239 -58.35346 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 253.4239 -56.35346 0 MPRFLG 0 ; 
       16 SCHEM 255.9239 -56.35346 0 MPRFLG 0 ; 
       23 SCHEM 258.424 -56.35346 0 MPRFLG 0 ; 
       24 SCHEM 260.924 -56.35346 0 MPRFLG 0 ; 
       8 SCHEM 228.424 -56.35346 0 MPRFLG 0 ; 
       12 SCHEM 222.7729 -42.73326 0 USR MPRFLG 0 ; 
       25 SCHEM 243.223 -47.17286 0 USR MPRFLG 0 ; 
       26 SCHEM 210.089 -51.87737 0 USR MPRFLG 0 ; 
       27 SCHEM 183.75 -49.46576 0 USR MPRFLG 0 ; 
       28 SCHEM 223.0551 -59.48269 0 USR MPRFLG 0 ; 
       29 SCHEM 188.5551 -59.73018 0 USR MPRFLG 0 ; 
       32 SCHEM 231.973 -51.17287 0 USR MPRFLG 0 ; 
       33 SCHEM 226.973 -51.17287 0 USR MPRFLG 0 ; 
       34 SCHEM 229.473 -51.17287 0 USR MPRFLG 0 ; 
       35 SCHEM 224.891 -46.64471 0 USR MPRFLG 0 ; 
       36 SCHEM 232.4609 -46.60918 0 USR MPRFLG 0 ; 
       37 SCHEM 254.473 -51.17287 0 USR MPRFLG 0 ; 
       38 SCHEM 261.973 -51.17287 0 USR MPRFLG 0 ; 
       39 SCHEM 256.973 -51.17287 0 USR MPRFLG 0 ; 
       40 SCHEM 259.473 -51.17287 0 USR MPRFLG 0 ; 
       41 SCHEM 214.36 -46.66849 0 USR MPRFLG 0 ; 
       42 SCHEM 221.483 -46.67099 0 USR MPRFLG 0 ; 
       43 SCHEM 216.483 -46.67099 0 USR MPRFLG 0 ; 
       44 SCHEM 218.983 -46.67099 0 USR MPRFLG 0 ; 
       45 SCHEM 227.4609 -46.60918 0 USR MPRFLG 0 ; 
       46 SCHEM 229.9609 -46.60918 0 USR MPRFLG 0 ; 
       47 SCHEM 224.473 -51.17287 0 USR MPRFLG 0 ; 
       48 SCHEM 185 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       49 SCHEM 192.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       50 SCHEM 187.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       51 SCHEM 197.5 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       52 SCHEM 195 -51.46576 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       53 SCHEM 189.8051 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       54 SCHEM 197.3051 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       55 SCHEM 192.3051 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       56 SCHEM 202.3051 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       57 SCHEM 199.8051 -61.73018 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       58 SCHEM 216.8051 -63.48269 0 USR MPRFLG 0 ; 
       59 SCHEM 177.3051 -63.73018 0 USR MPRFLG 0 ; 
       60 SCHEM 172.5 -53.46576 0 USR MPRFLG 0 ; 
       61 SCHEM 203.839 -55.87737 0 USR MPRFLG 0 ; 
       62 SCHEM 180 -53.46576 0 USR MPRFLG 0 ; 
       63 SCHEM 184.8051 -63.73018 0 USR MPRFLG 0 ; 
       64 SCHEM 224.3051 -63.48269 0 USR MPRFLG 0 ; 
       65 SCHEM 211.339 -55.87737 0 USR MPRFLG 0 ; 
       66 SCHEM 218.839 -53.87737 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       67 SCHEM 231.8051 -61.48269 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       68 SCHEM 258.223 -49.17286 0 USR MPRFLG 0 ; 
       69 SCHEM 205.6552 -4.000001 0 SRT 1 1 1 0 0 0 0 31.39535 0 MPRFLG 0 ; 
       70 SCHEM 218.983 -44.671 0 USR MPRFLG 0 ; 
       71 SCHEM 229.9609 -44.60918 0 USR MPRFLG 0 ; 
       72 SCHEM 248.713 -32.58364 0 USR MPRFLG 0 ; 
       73 SCHEM 249.1069 -36.3852 0 USR MPRFLG 0 ; 
       75 SCHEM 228.223 -49.17286 0 USR MPRFLG 0 ; 
       76 SCHEM 225.924 -58.35346 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 206.339 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 175 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 179.8051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 219.3051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 214.3051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 174.8051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 170 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 201.339 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 177.3051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 172.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 216.8051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 203.839 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 213.839 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 187.3051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       92 SCHEM 182.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 226.8051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       94 SCHEM 208.839 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       95 SCHEM 177.5 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       96 SCHEM 221.8051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       97 SCHEM 182.3051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 180 -55.46576 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 184.8051 -65.73018 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       100 SCHEM 224.3051 -65.48269 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 211.339 -57.87737 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       102 SCHEM 244.963 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       103 SCHEM 247.463 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       104 SCHEM 249.963 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       105 SCHEM 252.463 -34.58364 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       106 SCHEM 252.8569 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       107 SCHEM 250.3569 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       108 SCHEM 247.8569 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       109 SCHEM 245.3569 -38.38521 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       112 SCHEM 181.0551 -61.73018 0 USR MPRFLG 0 ; 
       113 SCHEM 176.25 -51.46576 0 USR MPRFLG 0 ; 
       114 SCHEM 220.5551 -61.48269 0 USR MPRFLG 0 ; 
       115 SCHEM 207.589 -53.87737 0 USR MPRFLG 0 ; 
       116 SCHEM 190 -51.46576 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       117 SCHEM 194.8051 -61.73018 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       118 SCHEM 229.3051 -61.48269 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       119 SCHEM 216.339 -53.87737 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
