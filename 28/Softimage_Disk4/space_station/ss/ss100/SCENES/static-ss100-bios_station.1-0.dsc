SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.112-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 34     
       ss100_nulls-cube1_17.1-0 ; 
       ss100_nulls-cube1_26.1-0 ; 
       ss100_nulls-cube1_27.1-0 ; 
       ss100_nulls-cube1_28.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube21_1_16.1-0 ; 
       ss100_nulls-cube21_1_17.1-0 ; 
       ss100_nulls-cube21_1_18.1-0 ; 
       ss100_nulls-cube21_1_7.1-0 ; 
       ss100_nulls-cube22_12.1-0 ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube22_21.1-0 ; 
       ss100_nulls-cube22_22.1-0 ; 
       ss100_nulls-cube22_23.1-0 ; 
       ss100_nulls-cube5.5-0 ROOT ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_6.1-0 ; 
       ss100_nulls-east_bay_11_8.1-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-ss100-bios_station.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 17 110 ; 
       5 1 110 ; 
       6 2 110 ; 
       7 3 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 4 110 ; 
       14 1 110 ; 
       15 2 110 ; 
       16 3 110 ; 
       18 17 110 ; 
       19 17 110 ; 
       20 17 110 ; 
       21 17 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
       26 4 110 ; 
       27 4 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 4 110 ; 
       32 4 110 ; 
       33 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 -8 0 MPRFLG 0 ; 
       1 SCHEM 51.25 -8 0 MPRFLG 0 ; 
       2 SCHEM 56.25 -8 0 MPRFLG 0 ; 
       3 SCHEM 61.25 -8 0 MPRFLG 0 ; 
       4 SCHEM 33.75 -6 0 MPRFLG 0 ; 
       5 SCHEM 50 -10 0 MPRFLG 0 ; 
       6 SCHEM 55 -10 0 MPRFLG 0 ; 
       7 SCHEM 60 -10 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 40 -10 0 MPRFLG 0 ; 
       10 SCHEM 5 -8 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 10 -8 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       14 SCHEM 52.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 57.5 -10 0 MPRFLG 0 ; 
       16 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       17 SCHEM 36.25 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -6 0 MPRFLG 0 ; 
       20 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 65 -6 0 MPRFLG 0 ; 
       22 SCHEM 15 -8 0 MPRFLG 0 ; 
       23 SCHEM 35 -8 0 MPRFLG 0 ; 
       24 SCHEM 42.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 45 -8 0 MPRFLG 0 ; 
       26 SCHEM 47.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       28 SCHEM 30 -8 0 MPRFLG 0 ; 
       29 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       31 SCHEM 20 -8 0 MPRFLG 0 ; 
       32 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       33 SCHEM 25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
