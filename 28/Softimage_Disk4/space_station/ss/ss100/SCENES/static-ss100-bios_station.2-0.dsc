SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.114-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       ss100_nulls-cube1_27.1-0 ; 
       ss100_nulls-cube1_29.1-0 ; 
       ss100_nulls-cube1_30.1-0 ; 
       ss100_nulls-cube1_31.1-0 ; 
       ss100_nulls-cube20.1-0 ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube5_1.1-0 ROOT ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_6.1-0 ; 
       ss100_nulls-east_bay_11_8.1-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       static-ss100-bios_station.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 9 110 ; 
       2 9 110 ; 
       3 9 110 ; 
       4 9 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       8 4 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 9 110 ; 
       13 9 110 ; 
       14 4 110 ; 
       15 4 110 ; 
       16 4 110 ; 
       17 4 110 ; 
       18 4 110 ; 
       19 4 110 ; 
       20 4 110 ; 
       21 4 110 ; 
       22 4 110 ; 
       23 4 110 ; 
       24 4 110 ; 
       25 4 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.92942 6.855342 0 USR MPRFLG 0 ; 
       1 SCHEM 43.55128 6.693624 0 USR MPRFLG 0 ; 
       2 SCHEM 53.23695 7.294713 0 USR MPRFLG 0 ; 
       3 SCHEM 48.16871 6.994169 0 USR MPRFLG 0 ; 
       4 SCHEM 50.68775 -11.19729 0 USR MPRFLG 0 ; 
       5 SCHEM 1.937699 -13.19729 0 MPRFLG 0 ; 
       6 SCHEM 4.4377 -13.19729 0 MPRFLG 0 ; 
       7 SCHEM 6.9377 -13.19729 0 MPRFLG 0 ; 
       8 SCHEM 9.437702 -13.19729 0 MPRFLG 0 ; 
       9 SCHEM 34.61463 13.08168 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 11.37718 7.104258 0 USR MPRFLG 0 ; 
       11 SCHEM 4.560319 -7.094989 0 USR MPRFLG 0 ; 
       12 SCHEM 34.44749 -5.968577 0 USR MPRFLG 0 ; 
       13 SCHEM 22.99504 -1.499116 0 USR MPRFLG 0 ; 
       14 SCHEM 21.93769 -13.19729 0 MPRFLG 0 ; 
       15 SCHEM 41.93775 -13.19729 0 MPRFLG 0 ; 
       16 SCHEM 56.93776 -13.19729 0 MPRFLG 0 ; 
       17 SCHEM 59.43776 -13.19729 0 MPRFLG 0 ; 
       18 SCHEM 61.93779 -13.19729 0 MPRFLG 0 ; 
       19 SCHEM 34.43772 -13.19729 0 MPRFLG 0 ; 
       20 SCHEM 36.93772 -13.19729 0 MPRFLG 0 ; 
       21 SCHEM 39.43772 -13.19729 0 MPRFLG 0 ; 
       22 SCHEM 24.43769 -13.19729 0 MPRFLG 0 ; 
       23 SCHEM 26.93772 -13.19729 0 MPRFLG 0 ; 
       24 SCHEM 29.43772 -13.19729 0 MPRFLG 0 ; 
       25 SCHEM 31.93769 -13.19729 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 4 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
