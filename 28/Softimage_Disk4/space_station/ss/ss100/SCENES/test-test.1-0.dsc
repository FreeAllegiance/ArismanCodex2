SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       test-cam_int1.1-0 ROOT ; 
       test-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       bounding_model-bound01.1-0 ; 
       bounding_model-bounding_model.18-0 ROOT ; 
       bounding_model-garage1A.1-0 ; 
       bounding_model-garage1B.1-0 ; 
       bounding_model-garage1C.1-0 ; 
       bounding_model-garage1D.1-0 ; 
       bounding_model-garage1E.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       test-test.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 1 110 ; 
       3 1 110 ; 
       6 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.787233 -2.695035 0 MPRFLG 0 ; 
       1 SCHEM 9.037233 -0.6950351 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 5.287233 -2.695035 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 7.787233 -2.695035 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 15.28723 -2.695035 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 10.28723 -2.695035 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 12.78723 -2.695035 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
