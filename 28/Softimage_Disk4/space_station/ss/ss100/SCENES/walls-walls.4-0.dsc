SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.67-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 9     
       walls-light1.3-0 ROOT ; 
       walls-light2.3-0 ROOT ; 
       walls-light3.3-0 ROOT ; 
       walls-spot1.1-0 ; 
       walls-spot1_int.4-0 ROOT ; 
       walls-spot2.1-0 ; 
       walls-spot2_int1.3-0 ROOT ; 
       walls-spot3.1-0 ; 
       walls-spot3_int1.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       room-back.1-0 ; 
       room-ceiling.1-0 ; 
       room-floor.1-0 ; 
       room-root.2-0 ROOT ; 
       room-wall.1-0 ; 
       room-wall1.1-0 ; 
       walls-east_bay_11_7.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/chrome3 ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/floor ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       walls-walls.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 3 110 ; 
       4 3 110 ; 
       5 3 110 ; 
       1 3 110 ; 
       0 3 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHTS 
       5 6 2110 ; 
       3 4 2110 ; 
       7 8 2110 ; 
       0 3 2111 ; 
       1 5 2111 ; 
       2 7 2111 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       5 SCHEM 32.5 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       6 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       7 SCHEM 35 -2 0 WIRECOL 7 7 MPRFLG 0 ; 
       8 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       0 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 42.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 -2 0 DISPLAY 1 2 MPRFLG 0 ; 
       4 SCHEM 10 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       0 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 11.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 25 0 0 DISPLAY 0 0 SRT 5 5 5 0 0.7853982 0 0 28.20425 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
