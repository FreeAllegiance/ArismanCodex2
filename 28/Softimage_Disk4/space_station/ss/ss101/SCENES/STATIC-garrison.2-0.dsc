SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.40-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       garrison-mat1.4-0 ; 
       garrison-mat10.1-0 ; 
       garrison-mat11.1-0 ; 
       garrison-mat12.1-0 ; 
       garrison-mat13.3-0 ; 
       garrison-mat14.3-0 ; 
       garrison-mat15.3-0 ; 
       garrison-mat16.2-0 ; 
       garrison-mat17.2-0 ; 
       garrison-mat2.3-0 ; 
       garrison-mat3.2-0 ; 
       garrison-mat33.1-0 ; 
       garrison-mat34.1-0 ; 
       garrison-mat35.3-0 ; 
       garrison-mat36.3-0 ; 
       garrison-mat37.3-0 ; 
       garrison-mat38.3-0 ; 
       garrison-mat39.1-0 ; 
       garrison-mat4.1-0 ; 
       garrison-mat40.1-0 ; 
       garrison-mat41.1-0 ; 
       garrison-mat42.1-0 ; 
       garrison-mat43.1-0 ; 
       garrison-mat44.1-0 ; 
       garrison-mat45.1-0 ; 
       garrison-mat46.1-0 ; 
       garrison-mat47.1-0 ; 
       garrison-mat48.1-0 ; 
       garrison-mat49.1-0 ; 
       garrison-mat5.1-0 ; 
       garrison-mat50.1-0 ; 
       garrison-mat51.1-0 ; 
       garrison-mat52.1-0 ; 
       garrison-mat53.1-0 ; 
       garrison-mat54.1-0 ; 
       garrison-mat55.1-0 ; 
       garrison-mat56.1-0 ; 
       garrison-mat57.1-0 ; 
       garrison-mat58.1-0 ; 
       garrison-mat59.1-0 ; 
       garrison-mat6.1-0 ; 
       garrison-mat61.2-0 ; 
       garrison-mat62.1-0 ; 
       garrison-mat63.1-0 ; 
       garrison-mat64.1-0 ; 
       garrison-mat7.1-0 ; 
       garrison-mat8.1-0 ; 
       garrison-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       garrison-east_bay_11_10.1-0 ; 
       garrison-east_bay_11_18.1-0 ; 
       garrison-east_bay_11_22.1-0 ; 
       garrison-east_bay_11_23.1-0 ; 
       garrison-east_bay_11_24.1-0 ; 
       garrison-east_bay_11_3.3-0 ; 
       garrison-east_bay_12.1-0 ; 
       garrison-east_bay_14.1-0 ; 
       garrison-extru104.1-0 ; 
       garrison-extru105.1-0 ; 
       garrison-extru106.1-0 ; 
       garrison-extru107.1-0 ; 
       garrison-extru108.1-0 ; 
       garrison-extru63.1-0 ; 
       garrison-extru84.1-0 ; 
       garrison-extru92.1-0 ; 
       garrison-extru97.1-0 ; 
       garrison-extru98.1-0 ; 
       garrison-extru99.1-0 ; 
       garrison-landing_lights_1.1-0 ; 
       garrison-landing_lights_11.1-0 ; 
       garrison-landing_lights_12.1-0 ; 
       garrison-landing_lights_5.1-0 ; 
       garrison-landing_lights2_1.1-0 ; 
       garrison-landing_lights2_11.1-0 ; 
       garrison-landing_lights2_12.1-0 ; 
       garrison-landing_lights2_5.1-0 ; 
       garrison-null18.1-0 ; 
       garrison-null19.1-0 ; 
       garrison-null32.1-0 ; 
       garrison-null32_1.1-0 ; 
       garrison-null39.1-0 ; 
       garrison-null42.1-0 ; 
       garrison-null45.26-0 ROOT ; 
       garrison-null48.1-0 ; 
       garrison-null49.1-0 ; 
       garrison-revol1.1-0 ; 
       garrison-revol3.1-0 ; 
       garrison-strobe_set.1-0 ; 
       garrison-strobe_set6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/SS101/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/SS101/PICTURES/ss101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-garrison.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 47     
       garrison-t2d1.4-0 ; 
       garrison-t2d10.1-0 ; 
       garrison-t2d11.1-0 ; 
       garrison-t2d12.1-0 ; 
       garrison-t2d13.6-0 ; 
       garrison-t2d14.6-0 ; 
       garrison-t2d15.5-0 ; 
       garrison-t2d16.5-0 ; 
       garrison-t2d17.4-0 ; 
       garrison-t2d2.3-0 ; 
       garrison-t2d3.2-0 ; 
       garrison-t2d33.1-0 ; 
       garrison-t2d34.4-0 ; 
       garrison-t2d35.4-0 ; 
       garrison-t2d36.4-0 ; 
       garrison-t2d37.4-0 ; 
       garrison-t2d38.2-0 ; 
       garrison-t2d39.2-0 ; 
       garrison-t2d4.2-0 ; 
       garrison-t2d40.2-0 ; 
       garrison-t2d41.2-0 ; 
       garrison-t2d42.2-0 ; 
       garrison-t2d43.2-0 ; 
       garrison-t2d44.2-0 ; 
       garrison-t2d45.2-0 ; 
       garrison-t2d46.2-0 ; 
       garrison-t2d47.1-0 ; 
       garrison-t2d48.1-0 ; 
       garrison-t2d49.1-0 ; 
       garrison-t2d5.3-0 ; 
       garrison-t2d50.1-0 ; 
       garrison-t2d51.1-0 ; 
       garrison-t2d52.1-0 ; 
       garrison-t2d53.1-0 ; 
       garrison-t2d54.1-0 ; 
       garrison-t2d55.1-0 ; 
       garrison-t2d56.1-0 ; 
       garrison-t2d57.1-0 ; 
       garrison-t2d58.1-0 ; 
       garrison-t2d59.1-0 ; 
       garrison-t2d6.3-0 ; 
       garrison-t2d61.2-0 ; 
       garrison-t2d62.1-0 ; 
       garrison-t2d63.1-0 ; 
       garrison-t2d7.1-0 ; 
       garrison-t2d8.1-0 ; 
       garrison-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 36 110 ; 
       1 31 110 ; 
       2 37 110 ; 
       3 2 110 ; 
       4 35 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 4 110 ; 
       8 34 110 ; 
       9 34 110 ; 
       10 34 110 ; 
       11 34 110 ; 
       12 13 110 ; 
       13 37 110 ; 
       14 13 110 ; 
       15 32 110 ; 
       16 32 110 ; 
       17 32 110 ; 
       18 32 110 ; 
       19 38 110 ; 
       20 39 110 ; 
       21 7 110 ; 
       22 6 110 ; 
       23 38 110 ; 
       24 39 110 ; 
       25 7 110 ; 
       26 6 110 ; 
       27 33 110 ; 
       28 33 110 ; 
       29 33 110 ; 
       30 33 110 ; 
       31 36 110 ; 
       32 36 110 ; 
       34 37 110 ; 
       35 37 110 ; 
       36 33 110 ; 
       37 33 110 ; 
       38 5 110 ; 
       39 3 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       10 36 300 ; 
       10 37 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       12 44 300 ; 
       13 41 300 ; 
       14 43 300 ; 
       15 29 300 ; 
       15 40 300 ; 
       16 45 300 ; 
       16 46 300 ; 
       17 47 300 ; 
       17 1 300 ; 
       18 2 300 ; 
       18 3 300 ; 
       33 42 300 ; 
       36 0 300 ; 
       36 9 300 ; 
       36 10 300 ; 
       36 18 300 ; 
       37 16 300 ; 
       37 17 300 ; 
       37 19 300 ; 
       37 20 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 8 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 0 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 10 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 40 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 29 401 ; 
       41 41 401 ; 
       43 42 401 ; 
       44 43 401 ; 
       45 44 401 ; 
       46 45 401 ; 
       47 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 23.75 -4 0 MPRFLG 0 ; 
       1 SCHEM 43.3338 -6.248177 0 USR MPRFLG 0 ; 
       2 SCHEM 48.75 -4 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       4 SCHEM 65.8338 -6.248177 0 USR MPRFLG 0 ; 
       5 SCHEM 23.75 -6 0 MPRFLG 0 ; 
       6 SCHEM 43.3338 -8.248177 0 MPRFLG 0 ; 
       7 SCHEM 65.8338 -8.248177 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 45 -6 0 MPRFLG 0 ; 
       10 SCHEM 40 -6 0 MPRFLG 0 ; 
       11 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 35 -6 0 MPRFLG 0 ; 
       13 SCHEM 33.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 20 -6 0 MPRFLG 0 ; 
       19 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 47.5 -10 0 MPRFLG 0 ; 
       21 SCHEM 64.5838 -10.24818 0 MPRFLG 0 ; 
       22 SCHEM 42.0838 -10.24818 0 MPRFLG 0 ; 
       23 SCHEM 25 -10 0 MPRFLG 0 ; 
       24 SCHEM 50 -10 0 MPRFLG 0 ; 
       25 SCHEM 67.0838 -10.24818 0 MPRFLG 0 ; 
       26 SCHEM 44.5838 -10.24818 0 MPRFLG 0 ; 
       27 SCHEM 5 -2 0 MPRFLG 0 ; 
       28 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       29 SCHEM 10 -2 0 MPRFLG 0 ; 
       30 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       31 SCHEM 41.25 -4 0 USR MPRFLG 0 ; 
       32 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 28.75 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       34 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       35 SCHEM 63.75 -4 0 USR MPRFLG 0 ; 
       36 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       37 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       38 SCHEM 23.75 -8 0 MPRFLG 0 ; 
       39 SCHEM 48.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 46.0838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 46.0838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 46.0838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 46.0838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 46.0838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 44 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 68.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 68.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 68.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 68.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 68.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 56.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 31.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 46.0838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 46.0838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 46.0838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 46.0838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 46.0838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 44 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 66.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 51.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 68.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 68.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 68.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 68.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 68.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 36.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 44 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 39 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 41.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 11.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 31.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 34 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 14 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 16.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
