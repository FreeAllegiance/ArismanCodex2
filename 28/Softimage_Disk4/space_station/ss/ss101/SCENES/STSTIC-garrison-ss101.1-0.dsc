SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.55-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       garrison-mat1.5-0 ; 
       garrison-mat10.1-0 ; 
       garrison-mat11.1-0 ; 
       garrison-mat12.1-0 ; 
       garrison-mat13.3-0 ; 
       garrison-mat14.3-0 ; 
       garrison-mat15.3-0 ; 
       garrison-mat16.2-0 ; 
       garrison-mat17.2-0 ; 
       garrison-mat2.4-0 ; 
       garrison-mat3.3-0 ; 
       garrison-mat33.1-0 ; 
       garrison-mat34.1-0 ; 
       garrison-mat35.3-0 ; 
       garrison-mat36.3-0 ; 
       garrison-mat37.3-0 ; 
       garrison-mat38.3-0 ; 
       garrison-mat39.1-0 ; 
       garrison-mat4.2-0 ; 
       garrison-mat40.1-0 ; 
       garrison-mat41.1-0 ; 
       garrison-mat42.1-0 ; 
       garrison-mat43.1-0 ; 
       garrison-mat44.1-0 ; 
       garrison-mat45.1-0 ; 
       garrison-mat46.1-0 ; 
       garrison-mat47.1-0 ; 
       garrison-mat48.1-0 ; 
       garrison-mat49.1-0 ; 
       garrison-mat5.1-0 ; 
       garrison-mat50.1-0 ; 
       garrison-mat51.1-0 ; 
       garrison-mat52.1-0 ; 
       garrison-mat53.1-0 ; 
       garrison-mat54.1-0 ; 
       garrison-mat55.1-0 ; 
       garrison-mat56.1-0 ; 
       garrison-mat57.1-0 ; 
       garrison-mat58.1-0 ; 
       garrison-mat59.1-0 ; 
       garrison-mat6.1-0 ; 
       garrison-mat61.2-0 ; 
       garrison-mat62.1-0 ; 
       garrison-mat63.1-0 ; 
       garrison-mat64.1-0 ; 
       garrison-mat7.1-0 ; 
       garrison-mat8.1-0 ; 
       garrison-mat9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ss101-east_bay_11_10.1-0 ; 
       ss101-east_bay_11_18.1-0 ; 
       ss101-east_bay_11_22.1-0 ; 
       ss101-east_bay_11_24.1-0 ; 
       ss101-extru104.1-0 ; 
       ss101-extru105.1-0 ; 
       ss101-extru106.1-0 ; 
       ss101-extru107.1-0 ; 
       ss101-extru108.1-0 ; 
       ss101-extru63.1-0 ; 
       ss101-extru84.1-0 ; 
       ss101-extru92.1-0 ; 
       ss101-extru97.1-0 ; 
       ss101-extru98.1-0 ; 
       ss101-extru99.1-0 ; 
       ss101-revol1.1-0 ; 
       ss101-revol3.1-0 ; 
       ss101-ss101.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss101/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss101/PICTURES/ss101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STSTIC-garrison-ss101.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 48     
       garrison-t2d1.10-0 ; 
       garrison-t2d10.6-0 ; 
       garrison-t2d11.6-0 ; 
       garrison-t2d12.6-0 ; 
       garrison-t2d13.11-0 ; 
       garrison-t2d14.11-0 ; 
       garrison-t2d15.10-0 ; 
       garrison-t2d16.10-0 ; 
       garrison-t2d17.9-0 ; 
       garrison-t2d2.9-0 ; 
       garrison-t2d3.8-0 ; 
       garrison-t2d33.6-0 ; 
       garrison-t2d34.9-0 ; 
       garrison-t2d35.9-0 ; 
       garrison-t2d36.9-0 ; 
       garrison-t2d37.9-0 ; 
       garrison-t2d38.7-0 ; 
       garrison-t2d39.7-0 ; 
       garrison-t2d4.8-0 ; 
       garrison-t2d40.7-0 ; 
       garrison-t2d41.7-0 ; 
       garrison-t2d42.7-0 ; 
       garrison-t2d43.7-0 ; 
       garrison-t2d44.7-0 ; 
       garrison-t2d45.7-0 ; 
       garrison-t2d46.7-0 ; 
       garrison-t2d47.6-0 ; 
       garrison-t2d48.6-0 ; 
       garrison-t2d49.6-0 ; 
       garrison-t2d5.8-0 ; 
       garrison-t2d50.6-0 ; 
       garrison-t2d51.6-0 ; 
       garrison-t2d52.6-0 ; 
       garrison-t2d53.6-0 ; 
       garrison-t2d54.6-0 ; 
       garrison-t2d55.6-0 ; 
       garrison-t2d56.6-0 ; 
       garrison-t2d57.6-0 ; 
       garrison-t2d58.6-0 ; 
       garrison-t2d59.6-0 ; 
       garrison-t2d6.8-0 ; 
       garrison-t2d61.7-0 ; 
       garrison-t2d62.6-0 ; 
       garrison-t2d63.6-0 ; 
       garrison-t2d7.6-0 ; 
       garrison-t2d8.6-0 ; 
       garrison-t2d9.6-0 ; 
       garrison_ss101-t2d64.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 17 110 ; 
       1 17 110 ; 
       2 17 110 ; 
       3 17 110 ; 
       4 16 110 ; 
       5 16 110 ; 
       6 16 110 ; 
       7 16 110 ; 
       8 9 110 ; 
       9 17 110 ; 
       10 9 110 ; 
       11 15 110 ; 
       12 15 110 ; 
       13 15 110 ; 
       14 15 110 ; 
       15 17 110 ; 
       16 17 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       3 26 300 ; 
       3 27 300 ; 
       3 28 300 ; 
       3 30 300 ; 
       3 31 300 ; 
       4 32 300 ; 
       4 33 300 ; 
       5 34 300 ; 
       5 35 300 ; 
       6 36 300 ; 
       6 37 300 ; 
       7 38 300 ; 
       7 39 300 ; 
       8 44 300 ; 
       9 41 300 ; 
       10 43 300 ; 
       11 29 300 ; 
       11 40 300 ; 
       12 45 300 ; 
       12 46 300 ; 
       13 47 300 ; 
       13 1 300 ; 
       14 2 300 ; 
       14 3 300 ; 
       15 0 300 ; 
       15 9 300 ; 
       15 10 300 ; 
       15 18 300 ; 
       16 16 300 ; 
       16 17 300 ; 
       16 19 300 ; 
       16 20 300 ; 
       17 42 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       15 47 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 8 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 0 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 10 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 40 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 29 401 ; 
       41 41 401 ; 
       43 42 401 ; 
       44 43 401 ; 
       45 44 401 ; 
       46 45 401 ; 
       47 46 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 15 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 35 -2 0 MPRFLG 0 ; 
       4 SCHEM 25 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 21.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 5 -4 0 MPRFLG 0 ; 
       12 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -4 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       16 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 18.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 34 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 36.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 34 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 24 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 14 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
