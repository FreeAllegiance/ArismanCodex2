SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.37-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 90     
       garrison-mat1.4-0 ; 
       garrison-mat10.1-0 ; 
       garrison-mat11.1-0 ; 
       garrison-mat12.1-0 ; 
       garrison-mat13.3-0 ; 
       garrison-mat14.3-0 ; 
       garrison-mat15.3-0 ; 
       garrison-mat16.2-0 ; 
       garrison-mat17.2-0 ; 
       garrison-mat2.3-0 ; 
       garrison-mat3.2-0 ; 
       garrison-mat33.1-0 ; 
       garrison-mat34.1-0 ; 
       garrison-mat35.3-0 ; 
       garrison-mat36.3-0 ; 
       garrison-mat37.3-0 ; 
       garrison-mat38.3-0 ; 
       garrison-mat39.1-0 ; 
       garrison-mat4.1-0 ; 
       garrison-mat40.1-0 ; 
       garrison-mat41.1-0 ; 
       garrison-mat42.1-0 ; 
       garrison-mat43.1-0 ; 
       garrison-mat44.1-0 ; 
       garrison-mat45.1-0 ; 
       garrison-mat46.1-0 ; 
       garrison-mat47.1-0 ; 
       garrison-mat48.1-0 ; 
       garrison-mat49.1-0 ; 
       garrison-mat5.1-0 ; 
       garrison-mat50.1-0 ; 
       garrison-mat51.1-0 ; 
       garrison-mat52.1-0 ; 
       garrison-mat53.1-0 ; 
       garrison-mat54.1-0 ; 
       garrison-mat55.1-0 ; 
       garrison-mat56.1-0 ; 
       garrison-mat57.1-0 ; 
       garrison-mat58.1-0 ; 
       garrison-mat59.1-0 ; 
       garrison-mat6.1-0 ; 
       garrison-mat60.1-0 ; 
       garrison-mat61.2-0 ; 
       garrison-mat62.1-0 ; 
       garrison-mat63.1-0 ; 
       garrison-mat64.1-0 ; 
       garrison-mat7.1-0 ; 
       garrison-mat8.1-0 ; 
       garrison-mat9.1-0 ; 
       garrison-white_strobe1_50.2-0 ; 
       garrison-white_strobe1_51.2-0 ; 
       garrison-white_strobe1_52.2-0 ; 
       garrison-white_strobe1_53.2-0 ; 
       garrison-white_strobe1_54.2-0 ; 
       garrison-white_strobe1_55.2-0 ; 
       garrison-white_strobe1_56.2-0 ; 
       garrison-white_strobe1_61.2-0 ; 
       garrison-white_strobe1_62.2-0 ; 
       garrison-white_strobe1_63.2-0 ; 
       garrison-white_strobe1_64.2-0 ; 
       garrison-white_strobe1_65.2-0 ; 
       garrison-white_strobe1_66.2-0 ; 
       garrison-white_strobe1_67.1-0 ; 
       garrison-white_strobe1_68.1-0 ; 
       garrison-white_strobe1_69.1-0 ; 
       garrison-white_strobe1_70.1-0 ; 
       garrison-white_strobe1_71.1-0 ; 
       garrison-white_strobe1_72.1-0 ; 
       garrison-white_strobe1_73.1-0 ; 
       garrison-white_strobe1_74.1-0 ; 
       garrison-white_strobe1_75.1-0 ; 
       garrison-white_strobe1_76.1-0 ; 
       garrison-white_strobe1_77.1-0 ; 
       garrison-white_strobe1_78.1-0 ; 
       ordinance-white_strobe1_10.2-0 ; 
       ordinance-white_strobe1_11.2-0 ; 
       ordinance-white_strobe1_12.2-0 ; 
       ordinance-white_strobe1_25.2-0 ; 
       ordinance-white_strobe1_26.2-0 ; 
       ordinance-white_strobe1_27.2-0 ; 
       ordinance-white_strobe1_28.2-0 ; 
       ordinance-white_strobe1_33.2-0 ; 
       ordinance-white_strobe1_34.2-0 ; 
       ordinance-white_strobe1_35.2-0 ; 
       ordinance-white_strobe1_7.2-0 ; 
       ordinance-white_strobe1_8.2-0 ; 
       ordinance-white_strobe1_9.2-0 ; 
       ord_depot-white_strobe1_47.2-0 ; 
       ord_depot-white_strobe1_48.2-0 ; 
       ord_depot-white_strobe1_49.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 94     
       garrison-east_bay_11_10.1-0 ; 
       garrison-east_bay_11_18.1-0 ; 
       garrison-east_bay_11_22.1-0 ; 
       garrison-east_bay_11_23.1-0 ; 
       garrison-east_bay_11_24.1-0 ; 
       garrison-east_bay_11_3.3-0 ; 
       garrison-east_bay_12.1-0 ; 
       garrison-east_bay_14.1-0 ; 
       garrison-extru104.1-0 ; 
       garrison-extru105.1-0 ; 
       garrison-extru106.1-0 ; 
       garrison-extru107.1-0 ; 
       garrison-extru108.1-0 ; 
       garrison-extru63.1-0 ; 
       garrison-extru84.1-0 ; 
       garrison-extru92.1-0 ; 
       garrison-extru97.1-0 ; 
       garrison-extru98.1-0 ; 
       garrison-extru99.1-0 ; 
       garrison-garage2A.1-0 ; 
       garrison-garage2A6.1-0 ; 
       garrison-garage2B.1-0 ; 
       garrison-garage2B6.1-0 ; 
       garrison-garage2C.1-0 ; 
       garrison-garage2C6.1-0 ; 
       garrison-garage2D.1-0 ; 
       garrison-garage2D6.1-0 ; 
       garrison-garage2E.1-0 ; 
       garrison-garage2E6.1-0 ; 
       garrison-landing_lights_1.1-0 ; 
       garrison-landing_lights_11.1-0 ; 
       garrison-landing_lights_12.1-0 ; 
       garrison-landing_lights_5.1-0 ; 
       garrison-landing_lights2_1.1-0 ; 
       garrison-landing_lights2_11.1-0 ; 
       garrison-landing_lights2_12.1-0 ; 
       garrison-landing_lights2_5.1-0 ; 
       garrison-launch2.1-0 ; 
       garrison-launch4.1-0 ; 
       garrison-null18.1-0 ; 
       garrison-null19.1-0 ; 
       garrison-null32.1-0 ; 
       garrison-null32_1.1-0 ; 
       garrison-null39.1-0 ; 
       garrison-null42.1-0 ; 
       garrison-null45.25-0 ROOT ; 
       garrison-null48.1-0 ; 
       garrison-null49.1-0 ; 
       garrison-revol1.1-0 ; 
       garrison-revol3.1-0 ; 
       garrison-SS_01.1-0 ; 
       garrison-SS_01_1.1-0 ; 
       garrison-SS_02.1-0 ; 
       garrison-SS_02_1.1-0 ; 
       garrison-SS_03.1-0 ; 
       garrison-SS_03_1.1-0 ; 
       garrison-SS_05.1-0 ; 
       garrison-SS_05_1.1-0 ; 
       garrison-SS_129.1-0 ; 
       garrison-SS_130.1-0 ; 
       garrison-SS_131.1-0 ; 
       garrison-SS_132.1-0 ; 
       garrison-SS_133.1-0 ; 
       garrison-SS_134.1-0 ; 
       garrison-SS_135.1-0 ; 
       garrison-SS_136.1-0 ; 
       garrison-SS_137.1-0 ; 
       garrison-SS_138.1-0 ; 
       garrison-SS_139.1-0 ; 
       garrison-SS_140.1-0 ; 
       garrison-SS_20.1-0 ; 
       garrison-SS_21.1-0 ; 
       garrison-SS_22.1-0 ; 
       garrison-SS_33.1-0 ; 
       garrison-SS_34.1-0 ; 
       garrison-SS_35.1-0 ; 
       garrison-SS_66.1-0 ; 
       garrison-SS_67.1-0 ; 
       garrison-SS_68.1-0 ; 
       garrison-SS_69.1-0 ; 
       garrison-SS_70.1-0 ; 
       garrison-SS_71.1-0 ; 
       garrison-SS_90.1-0 ; 
       garrison-SS_91.1-0 ; 
       garrison-SS_92.1-0 ; 
       garrison-SS_93.1-0 ; 
       garrison-SS_94.1-0 ; 
       garrison-SS_95.1-0 ; 
       garrison-SS_96.1-0 ; 
       garrison-SS_97.1-0 ; 
       garrison-SS_98.1-0 ; 
       garrison-strobe_set.1-0 ; 
       garrison-strobe_set6.1-0 ; 
       garrison-tetra4.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/SS101/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/SS101/PICTURES/ss101 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-garrison.33-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 48     
       garrison-t2d1.4-0 ; 
       garrison-t2d10.1-0 ; 
       garrison-t2d11.1-0 ; 
       garrison-t2d12.1-0 ; 
       garrison-t2d13.6-0 ; 
       garrison-t2d14.6-0 ; 
       garrison-t2d15.5-0 ; 
       garrison-t2d16.5-0 ; 
       garrison-t2d17.4-0 ; 
       garrison-t2d2.3-0 ; 
       garrison-t2d3.2-0 ; 
       garrison-t2d33.1-0 ; 
       garrison-t2d34.4-0 ; 
       garrison-t2d35.4-0 ; 
       garrison-t2d36.4-0 ; 
       garrison-t2d37.4-0 ; 
       garrison-t2d38.2-0 ; 
       garrison-t2d39.2-0 ; 
       garrison-t2d4.2-0 ; 
       garrison-t2d40.2-0 ; 
       garrison-t2d41.2-0 ; 
       garrison-t2d42.2-0 ; 
       garrison-t2d43.2-0 ; 
       garrison-t2d44.2-0 ; 
       garrison-t2d45.2-0 ; 
       garrison-t2d46.2-0 ; 
       garrison-t2d47.1-0 ; 
       garrison-t2d48.1-0 ; 
       garrison-t2d49.1-0 ; 
       garrison-t2d5.3-0 ; 
       garrison-t2d50.1-0 ; 
       garrison-t2d51.1-0 ; 
       garrison-t2d52.1-0 ; 
       garrison-t2d53.1-0 ; 
       garrison-t2d54.1-0 ; 
       garrison-t2d55.1-0 ; 
       garrison-t2d56.1-0 ; 
       garrison-t2d57.1-0 ; 
       garrison-t2d58.1-0 ; 
       garrison-t2d59.1-0 ; 
       garrison-t2d6.3-0 ; 
       garrison-t2d60.1-0 ; 
       garrison-t2d61.2-0 ; 
       garrison-t2d62.1-0 ; 
       garrison-t2d63.1-0 ; 
       garrison-t2d7.1-0 ; 
       garrison-t2d8.1-0 ; 
       garrison-t2d9.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 48 110 ; 
       1 43 110 ; 
       2 49 110 ; 
       3 2 110 ; 
       4 47 110 ; 
       5 0 110 ; 
       6 1 110 ; 
       7 4 110 ; 
       8 46 110 ; 
       9 46 110 ; 
       10 46 110 ; 
       11 46 110 ; 
       12 13 110 ; 
       13 49 110 ; 
       14 13 110 ; 
       15 44 110 ; 
       16 44 110 ; 
       17 44 110 ; 
       18 44 110 ; 
       19 5 110 ; 
       20 3 110 ; 
       21 5 110 ; 
       22 3 110 ; 
       23 5 110 ; 
       24 3 110 ; 
       25 5 110 ; 
       26 3 110 ; 
       27 5 110 ; 
       28 3 110 ; 
       29 91 110 ; 
       30 92 110 ; 
       31 7 110 ; 
       32 6 110 ; 
       33 91 110 ; 
       34 92 110 ; 
       35 7 110 ; 
       36 6 110 ; 
       37 6 110 ; 
       38 7 110 ; 
       39 45 110 ; 
       40 45 110 ; 
       41 45 110 ; 
       42 45 110 ; 
       43 48 110 ; 
       44 48 110 ; 
       46 49 110 ; 
       47 49 110 ; 
       48 45 110 ; 
       49 45 110 ; 
       50 41 110 ; 
       51 42 110 ; 
       52 41 110 ; 
       53 42 110 ; 
       54 41 110 ; 
       55 42 110 ; 
       56 41 110 ; 
       57 42 110 ; 
       58 30 110 ; 
       59 30 110 ; 
       60 30 110 ; 
       61 34 110 ; 
       62 34 110 ; 
       63 34 110 ; 
       64 35 110 ; 
       65 35 110 ; 
       66 35 110 ; 
       67 31 110 ; 
       68 31 110 ; 
       69 31 110 ; 
       70 39 110 ; 
       71 39 110 ; 
       72 39 110 ; 
       73 40 110 ; 
       74 40 110 ; 
       75 40 110 ; 
       76 29 110 ; 
       77 29 110 ; 
       78 29 110 ; 
       79 33 110 ; 
       80 33 110 ; 
       81 33 110 ; 
       82 36 110 ; 
       83 36 110 ; 
       84 36 110 ; 
       85 32 110 ; 
       86 32 110 ; 
       87 32 110 ; 
       88 40 110 ; 
       89 39 110 ; 
       90 42 110 ; 
       91 5 110 ; 
       92 3 110 ; 
       93 48 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       0 8 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       1 14 300 ; 
       1 15 300 ; 
       2 21 300 ; 
       2 22 300 ; 
       2 23 300 ; 
       2 24 300 ; 
       2 25 300 ; 
       4 26 300 ; 
       4 27 300 ; 
       4 28 300 ; 
       4 30 300 ; 
       4 31 300 ; 
       8 32 300 ; 
       8 33 300 ; 
       9 34 300 ; 
       9 35 300 ; 
       10 36 300 ; 
       10 37 300 ; 
       11 38 300 ; 
       11 39 300 ; 
       12 45 300 ; 
       13 42 300 ; 
       14 44 300 ; 
       15 29 300 ; 
       15 40 300 ; 
       16 46 300 ; 
       16 47 300 ; 
       17 48 300 ; 
       17 1 300 ; 
       18 2 300 ; 
       18 3 300 ; 
       45 43 300 ; 
       48 0 300 ; 
       48 9 300 ; 
       48 10 300 ; 
       48 18 300 ; 
       49 16 300 ; 
       49 17 300 ; 
       49 19 300 ; 
       49 20 300 ; 
       50 79 300 ; 
       51 57 300 ; 
       52 78 300 ; 
       53 58 300 ; 
       54 77 300 ; 
       55 59 300 ; 
       56 80 300 ; 
       57 60 300 ; 
       58 71 300 ; 
       59 72 300 ; 
       60 73 300 ; 
       61 68 300 ; 
       62 69 300 ; 
       63 70 300 ; 
       64 65 300 ; 
       65 66 300 ; 
       66 67 300 ; 
       67 62 300 ; 
       68 63 300 ; 
       69 64 300 ; 
       70 89 300 ; 
       71 88 300 ; 
       72 87 300 ; 
       73 81 300 ; 
       74 82 300 ; 
       75 83 300 ; 
       76 85 300 ; 
       77 86 300 ; 
       78 84 300 ; 
       79 75 300 ; 
       80 76 300 ; 
       81 74 300 ; 
       82 52 300 ; 
       83 53 300 ; 
       84 54 300 ; 
       85 49 300 ; 
       86 50 300 ; 
       87 51 300 ; 
       88 55 300 ; 
       89 56 300 ; 
       90 61 300 ; 
       93 41 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 3 401 ; 
       4 8 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 0 401 ; 
       10 9 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
       17 17 401 ; 
       18 10 401 ; 
       19 19 401 ; 
       20 20 401 ; 
       21 21 401 ; 
       22 22 401 ; 
       23 23 401 ; 
       24 24 401 ; 
       25 25 401 ; 
       26 26 401 ; 
       27 27 401 ; 
       28 28 401 ; 
       29 40 401 ; 
       30 30 401 ; 
       31 31 401 ; 
       32 32 401 ; 
       33 33 401 ; 
       34 34 401 ; 
       35 35 401 ; 
       36 36 401 ; 
       37 37 401 ; 
       38 38 401 ; 
       39 39 401 ; 
       40 29 401 ; 
       41 41 401 ; 
       42 42 401 ; 
       44 43 401 ; 
       45 44 401 ; 
       46 45 401 ; 
       47 46 401 ; 
       48 47 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 70 -4 0 MPRFLG 0 ; 
       1 SCHEM 94.5838 -6.248177 0 USR MPRFLG 0 ; 
       2 SCHEM 130 -4 0 MPRFLG 0 ; 
       3 SCHEM 130 -6 0 MPRFLG 0 ; 
       4 SCHEM 153.3338 -6.248177 0 USR MPRFLG 0 ; 
       5 SCHEM 70 -6 0 MPRFLG 0 ; 
       6 SCHEM 94.5838 -8.248177 0 MPRFLG 0 ; 
       7 SCHEM 153.3338 -8.248177 0 MPRFLG 0 ; 
       8 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 115 -6 0 MPRFLG 0 ; 
       10 SCHEM 110 -6 0 MPRFLG 0 ; 
       11 SCHEM 112.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 105 -6 0 MPRFLG 0 ; 
       13 SCHEM 103.75 -4 0 MPRFLG 0 ; 
       14 SCHEM 102.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 50 -6 0 MPRFLG 0 ; 
       17 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 55 -6 0 MPRFLG 0 ; 
       19 SCHEM 72.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 132.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 137.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 75 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 135 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 82.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 142.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 80 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 140 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 60 -10 0 MPRFLG 0 ; 
       30 SCHEM 120 -10 0 MPRFLG 0 ; 
       31 SCHEM 148.3338 -10.24818 0 MPRFLG 0 ; 
       32 SCHEM 89.5838 -10.24818 0 MPRFLG 0 ; 
       33 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       34 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       35 SCHEM 155.8338 -10.24818 0 MPRFLG 0 ; 
       36 SCHEM 97.0838 -10.24818 0 MPRFLG 0 ; 
       37 SCHEM 102.0838 -10.24818 0 WIRECOL 9 7 MPRFLG 0 ; 
       38 SCHEM 160.8338 -10.24818 0 WIRECOL 9 7 MPRFLG 0 ; 
       39 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       40 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       41 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       42 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       43 SCHEM 92.5 -4 0 USR MPRFLG 0 ; 
       44 SCHEM 51.25 -4 0 MPRFLG 0 ; 
       45 SCHEM 81.25 0 0 SRT 1 1 1 -1.570796 0 0 0 0 0 MPRFLG 0 ; 
       46 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       47 SCHEM 151.25 -4 0 USR MPRFLG 0 ; 
       48 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       49 SCHEM 131.25 -2 0 MPRFLG 0 ; 
       50 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 122.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 117.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 120 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 130 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 125 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 127.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 158.3338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 153.3338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 155.8338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 150.8338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 145.8338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 148.3338 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 12.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 15 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 17.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       73 SCHEM 10 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 7.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       76 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 99.5838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 94.5838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 97.0838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 92.0838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 87.0838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 89.5838 -12.24818 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 2.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       89 SCHEM 20 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       90 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 63.75 -8 0 MPRFLG 0 ; 
       92 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       93 SCHEM 45 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 103.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 103.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 103.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 103.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 103.5838 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 160.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 144 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 144 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 144 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 144 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 144 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 162.3338 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 162.3338 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 162.3338 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 162.3338 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 162.3338 -8.248177 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 106.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 106.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 114 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 109 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 111.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 106.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 161.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 101.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 104 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 91.0838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 86.0838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 88.5838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 98.5838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 93.5838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 96.0838 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 149.8338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 144.8338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 147.3338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 157.3338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 152.3338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 154.8338 -14.24818 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 129 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 124 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 126.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 121.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 116.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 119 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 64 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 54 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 103.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 103.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 103.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 103.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 103.5838 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 160.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 160.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 160.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 160.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 144 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 144 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 144 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 144 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 144 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 162.3338 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 162.3338 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 162.3338 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 162.3338 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 162.3338 -10.24818 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 106.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 106.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 114 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 114 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 109 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 109 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 111.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 46.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 106.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 101.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 104 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 49 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
