SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.2-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 71     
       garrison-bay_top1_10.1-0 ; 
       garrison-bay_top1_11.1-0 ; 
       garrison-inside_bay1_10.1-0 ; 
       garrison-inside_bay1_11.1-0 ; 
       garrison-mat35.1-0 ; 
       garrison-mat36.1-0 ; 
       garrison-mat37.1-0 ; 
       garrison-mat38.1-0 ; 
       garrison-white_strobe1_50.1-0 ; 
       garrison-white_strobe1_51.1-0 ; 
       garrison-white_strobe1_52.1-0 ; 
       garrison-white_strobe1_53.1-0 ; 
       garrison-white_strobe1_54.1-0 ; 
       garrison-white_strobe1_55.1-0 ; 
       ordinance-bay_top1_2.1-0 ; 
       ordinance-bay_top1_4.1-0 ; 
       ordinance-bay_top1_5.1-0 ; 
       ordinance-bay_top1_7.1-0 ; 
       ordinance-bay_top1_8.1-0 ; 
       ordinance-bay_top1_9.1-0 ; 
       ordinance-inside_bay1_2.1-0 ; 
       ordinance-inside_bay1_4.1-0 ; 
       ordinance-inside_bay1_5.1-0 ; 
       ordinance-inside_bay1_7.1-0 ; 
       ordinance-inside_bay1_8.1-0 ; 
       ordinance-inside_bay1_9.1-0 ; 
       ordinance-mat10.1-0 ; 
       ordinance-mat13.1-0 ; 
       ordinance-mat14.1-0 ; 
       ordinance-mat15.1-0 ; 
       ordinance-mat16.1-0 ; 
       ordinance-mat17.1-0 ; 
       ordinance-mat18.1-0 ; 
       ordinance-mat3.1-0 ; 
       ordinance-mat4.1-0 ; 
       ordinance-mat7.1-0 ; 
       ordinance-mat8.1-0 ; 
       ordinance-mat9.1-0 ; 
       ordinance-white_strobe1_10.1-0 ; 
       ordinance-white_strobe1_11.1-0 ; 
       ordinance-white_strobe1_12.1-0 ; 
       ordinance-white_strobe1_13.1-0 ; 
       ordinance-white_strobe1_14.1-0 ; 
       ordinance-white_strobe1_15.1-0 ; 
       ordinance-white_strobe1_16.1-0 ; 
       ordinance-white_strobe1_17.1-0 ; 
       ordinance-white_strobe1_18.1-0 ; 
       ordinance-white_strobe1_25.1-0 ; 
       ordinance-white_strobe1_26.1-0 ; 
       ordinance-white_strobe1_27.1-0 ; 
       ordinance-white_strobe1_28.1-0 ; 
       ordinance-white_strobe1_30.1-0 ; 
       ordinance-white_strobe1_31.1-0 ; 
       ordinance-white_strobe1_32.1-0 ; 
       ordinance-white_strobe1_33.1-0 ; 
       ordinance-white_strobe1_34.1-0 ; 
       ordinance-white_strobe1_35.1-0 ; 
       ordinance-white_strobe1_41.1-0 ; 
       ordinance-white_strobe1_42.1-0 ; 
       ordinance-white_strobe1_43.1-0 ; 
       ordinance-white_strobe1_44.1-0 ; 
       ordinance-white_strobe1_45.1-0 ; 
       ordinance-white_strobe1_46.1-0 ; 
       ordinance-white_strobe1_7.1-0 ; 
       ordinance-white_strobe1_8.1-0 ; 
       ordinance-white_strobe1_9.1-0 ; 
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       ord_depot-white_strobe1_47.1-0 ; 
       ord_depot-white_strobe1_48.1-0 ; 
       ord_depot-white_strobe1_49.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 121     
       garrison-cube1.1-0 ROOT ; 
       garrison-cube12.1-0 ROOT ; 
       garrison-cube13.1-0 ROOT ; 
       garrison-cube14.1-0 ROOT ; 
       garrison-cube2.1-0 ROOT ; 
       garrison-cube5.1-0 ROOT ; 
       garrison-cube6.1-0 ROOT ; 
       garrison-cube7.1-0 ROOT ; 
       garrison-cube8.1-0 ROOT ; 
       garrison-cube9.1-0 ROOT ; 
       garrison-east_bay_11.3-0 ; 
       garrison-east_bay_11_10.1-0 ; 
       garrison-east_bay_11_11.1-0 ; 
       garrison-east_bay_11_3.3-0 ; 
       garrison-east_bay_11_6.1-0 ; 
       garrison-east_bay_11_8.1-0 ; 
       garrison-east_bay_11_9.1-0 ; 
       garrison-east_bay_12.1-0 ; 
       garrison-extru1.1-0 ROOT ; 
       garrison-extru2.1-0 ROOT ; 
       garrison-extru3.1-0 ROOT ; 
       garrison-extru4.1-0 ROOT ; 
       garrison-extru63.1-0 ; 
       garrison-extru69.1-0 ROOT ; 
       garrison-extru70.1-0 ROOT ; 
       garrison-extru77.1-0 ROOT ; 
       garrison-extru78.1-0 ROOT ; 
       garrison-extru79.1-0 ROOT ; 
       garrison-extru80.1-0 ; 
       garrison-extru84.1-0 ; 
       garrison-extru85.1-0 ; 
       garrison-extru86.1-0 ; 
       garrison-extru87.1-0 ; 
       garrison-extru88.1-0 ; 
       garrison-extru89.1-0 ; 
       garrison-extru90.1-0 ; 
       garrison-extru91.1-0 ; 
       garrison-extru92.1-0 ; 
       garrison-extru93.1-0 ; 
       garrison-extru94.1-0 ; 
       garrison-extru95.1-0 ; 
       garrison-extru96.1-0 ; 
       garrison-garage2A.1-0 ; 
       garrison-garage2A1.1-0 ; 
       garrison-garage2B.1-0 ; 
       garrison-garage2B1.1-0 ; 
       garrison-garage2C.1-0 ; 
       garrison-garage2C1.1-0 ; 
       garrison-garage2D.1-0 ; 
       garrison-garage2D1.1-0 ; 
       garrison-garage2E.1-0 ; 
       garrison-garage2E1.1-0 ; 
       garrison-landing_lights_1.1-0 ; 
       garrison-landing_lights_3.1-0 ; 
       garrison-landing_lights_4.1-0 ; 
       garrison-landing_lights_5.1-0 ; 
       garrison-landing_lights2_1.1-0 ; 
       garrison-landing_lights2_3.1-0 ; 
       garrison-landing_lights2_4.1-0 ; 
       garrison-landing_lights2_5.1-0 ; 
       garrison-launch1.1-0 ; 
       garrison-launch2.1-0 ; 
       garrison-null18.1-0 ROOT ; 
       garrison-null18_1.1-0 ROOT ; 
       garrison-null19.1-0 ROOT ; 
       garrison-null32.1-0 ROOT ; 
       garrison-null33.6-0 ; 
       garrison-null34.6-0 ; 
       garrison-null36.6-0 ; 
       garrison-null37.1-0 ; 
       garrison-null39.1-0 ; 
       garrison-null40.2-0 ROOT ; 
       garrison-null41.2-0 ROOT ; 
       garrison-null42.1-0 ; 
       garrison-null43.1-0 ; 
       garrison-null44.1-0 ; 
       garrison-revol1.2-0 ROOT ; 
       garrison-spline1.1-0 ROOT ; 
       garrison-spline2.1-0 ROOT ; 
       garrison-SS_01.1-0 ; 
       garrison-SS_02.1-0 ; 
       garrison-SS_03.1-0 ; 
       garrison-SS_05.1-0 ; 
       garrison-SS_20.1-0 ; 
       garrison-SS_21.1-0 ; 
       garrison-SS_22.1-0 ; 
       garrison-SS_29.1-0 ; 
       garrison-SS_30.1-0 ; 
       garrison-SS_31.1-0 ; 
       garrison-SS_33.1-0 ; 
       garrison-SS_34.1-0 ; 
       garrison-SS_35.1-0 ; 
       garrison-SS_66.1-0 ; 
       garrison-SS_67.1-0 ; 
       garrison-SS_68.1-0 ; 
       garrison-SS_69.1-0 ; 
       garrison-SS_70.1-0 ; 
       garrison-SS_71.1-0 ; 
       garrison-SS_72.1-0 ; 
       garrison-SS_73.1-0 ; 
       garrison-SS_74.1-0 ; 
       garrison-SS_75.1-0 ; 
       garrison-SS_76.1-0 ; 
       garrison-SS_77.1-0 ; 
       garrison-SS_84.1-0 ; 
       garrison-SS_85.1-0 ; 
       garrison-SS_86.1-0 ; 
       garrison-SS_87.1-0 ; 
       garrison-SS_88.1-0 ; 
       garrison-SS_89.1-0 ; 
       garrison-SS_90.1-0 ; 
       garrison-SS_91.1-0 ; 
       garrison-SS_92.1-0 ; 
       garrison-SS_93.1-0 ; 
       garrison-SS_94.1-0 ; 
       garrison-SS_95.1-0 ; 
       garrison-strobe_set.1-0 ; 
       garrison-strobe_set1.1-0 ; 
       garrison-tetra4.3-0 ; 
       ord-extru75.1-0 ROOT ; 
       ord-extru76.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 3     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss103/PICTURES/ss100 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss103/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss103/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       WIP-garrison.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 34     
       garrison-rendermap19.1-0 ; 
       garrison-rendermap20.1-0 ; 
       garrison-rendermap21.1-0 ; 
       garrison-rendermap22.1-0 ; 
       garrison-t2d34.1-0 ; 
       garrison-t2d35.1-0 ; 
       garrison-t2d36.1-0 ; 
       garrison-t2d37.1-0 ; 
       ordinance-rendermap10.1-0 ; 
       ordinance-rendermap13.1-0 ; 
       ordinance-rendermap14.1-0 ; 
       ordinance-rendermap15.1-0 ; 
       ordinance-rendermap16.1-0 ; 
       ordinance-rendermap17.1-0 ; 
       ordinance-rendermap18.1-0 ; 
       ordinance-rendermap3.1-0 ; 
       ordinance-rendermap4.1-0 ; 
       ordinance-rendermap7.1-0 ; 
       ordinance-rendermap8.1-0 ; 
       ordinance-rendermap9.1-0 ; 
       ordinance-t2d10.1-0 ; 
       ordinance-t2d13.1-0 ; 
       ordinance-t2d14.1-0 ; 
       ordinance-t2d15.1-0 ; 
       ordinance-t2d16.1-0 ; 
       ordinance-t2d17.1-0 ; 
       ordinance-t2d18.1-0 ; 
       ordinance-t2d3.1-0 ; 
       ordinance-t2d4.1-0 ; 
       ordinance-t2d7.1-0 ; 
       ordinance-t2d8.1-0 ; 
       ordinance-t2d9.1-0 ; 
       ord_depot-t2d32.1-0 ; 
       ord_depot-t2d33.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       10 11 110 ; 
       11 68 110 ; 
       13 14 110 ; 
       14 66 110 ; 
       15 67 110 ; 
       16 15 110 ; 
       22 76 110 ; 
       69 76 110 ; 
       28 22 110 ; 
       120 119 110 ; 
       70 71 110 ; 
       12 70 110 ; 
       42 13 110 ; 
       43 16 110 ; 
       44 13 110 ; 
       45 16 110 ; 
       46 13 110 ; 
       47 16 110 ; 
       48 13 110 ; 
       49 16 110 ; 
       50 13 110 ; 
       51 16 110 ; 
       52 116 110 ; 
       53 10 110 ; 
       54 117 110 ; 
       56 116 110 ; 
       57 10 110 ; 
       58 117 110 ; 
       60 10 110 ; 
       17 12 110 ; 
       66 71 110 ; 
       67 71 110 ; 
       68 71 110 ; 
       61 17 110 ; 
       59 17 110 ; 
       110 59 110 ; 
       111 59 110 ; 
       112 59 110 ; 
       55 17 110 ; 
       113 55 110 ; 
       114 55 110 ; 
       115 55 110 ; 
       29 22 110 ; 
       30 72 110 ; 
       79 65 110 ; 
       80 65 110 ; 
       81 65 110 ; 
       82 65 110 ; 
       83 62 110 ; 
       84 62 110 ; 
       85 62 110 ; 
       86 63 110 ; 
       87 63 110 ; 
       88 63 110 ; 
       89 64 110 ; 
       90 64 110 ; 
       91 64 110 ; 
       92 52 110 ; 
       93 52 110 ; 
       94 52 110 ; 
       95 56 110 ; 
       96 56 110 ; 
       97 56 110 ; 
       98 53 110 ; 
       99 53 110 ; 
       100 53 110 ; 
       101 57 110 ; 
       102 57 110 ; 
       103 57 110 ; 
       104 54 110 ; 
       105 54 110 ; 
       106 54 110 ; 
       107 58 110 ; 
       108 58 110 ; 
       109 58 110 ; 
       116 13 110 ; 
       117 16 110 ; 
       118 76 110 ; 
       31 72 110 ; 
       32 72 110 ; 
       33 72 110 ; 
       73 76 110 ; 
       34 73 110 ; 
       35 73 110 ; 
       36 73 110 ; 
       37 73 110 ; 
       74 76 110 ; 
       75 76 110 ; 
       38 75 110 ; 
       39 75 110 ; 
       40 75 110 ; 
       41 75 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       10 37 300 ; 
       10 16 300 ; 
       10 22 300 ; 
       10 26 300 ; 
       11 31 300 ; 
       11 19 300 ; 
       11 25 300 ; 
       11 32 300 ; 
       13 35 300 ; 
       13 15 300 ; 
       13 21 300 ; 
       13 36 300 ; 
       14 33 300 ; 
       14 14 300 ; 
       14 20 300 ; 
       14 34 300 ; 
       15 27 300 ; 
       15 17 300 ; 
       15 23 300 ; 
       15 28 300 ; 
       16 29 300 ; 
       16 18 300 ; 
       16 24 300 ; 
       16 30 300 ; 
       119 66 300 ; 
       120 67 300 ; 
       12 4 300 ; 
       12 0 300 ; 
       12 2 300 ; 
       12 5 300 ; 
       17 6 300 ; 
       17 1 300 ; 
       17 3 300 ; 
       17 7 300 ; 
       110 11 300 ; 
       111 12 300 ; 
       112 13 300 ; 
       113 8 300 ; 
       114 9 300 ; 
       115 10 300 ; 
       79 49 300 ; 
       80 48 300 ; 
       81 47 300 ; 
       82 50 300 ; 
       83 70 300 ; 
       84 69 300 ; 
       85 68 300 ; 
       86 53 300 ; 
       87 52 300 ; 
       88 51 300 ; 
       89 54 300 ; 
       90 55 300 ; 
       91 56 300 ; 
       92 64 300 ; 
       93 65 300 ; 
       94 63 300 ; 
       95 39 300 ; 
       96 40 300 ; 
       97 38 300 ; 
       98 45 300 ; 
       99 46 300 ; 
       100 44 300 ; 
       101 42 300 ; 
       102 43 300 ; 
       103 41 300 ; 
       104 60 300 ; 
       105 61 300 ; 
       106 62 300 ; 
       107 57 300 ; 
       108 58 300 ; 
       109 59 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       14 15 401 ; 
       15 17 401 ; 
       16 19 401 ; 
       17 9 401 ; 
       18 11 401 ; 
       19 13 401 ; 
       20 16 401 ; 
       21 18 401 ; 
       22 8 401 ; 
       23 10 401 ; 
       24 12 401 ; 
       25 14 401 ; 
       26 20 401 ; 
       27 21 401 ; 
       28 22 401 ; 
       29 23 401 ; 
       30 24 401 ; 
       31 25 401 ; 
       32 26 401 ; 
       33 27 401 ; 
       34 28 401 ; 
       35 29 401 ; 
       36 30 401 ; 
       37 31 401 ; 
       4 4 401 ; 
       0 0 401 ; 
       66 32 401 ; 
       67 33 401 ; 
       2 1 401 ; 
       5 5 401 ; 
       6 6 401 ; 
       1 2 401 ; 
       3 3 401 ; 
       7 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 187.3667 10.93192 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 187.3667 8.931917 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       78 SCHEM 189.2972 -1.8523 0 USR DISPLAY 0 0 SRT 2.524 2.524 2.039392 0 0 0 0 0 0 MPRFLG 0 ; 
       76 SCHEM 257.9399 -16.9603 0 USR SRT 2.105954 1.646834 2.105954 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 205.4071 -3.23911 0 USR DISPLAY 0 0 SRT 0.8123375 1.706592 2.800065 0 0 -0.7853982 -14.34195 14.41924 0 MPRFLG 0 ; 
       19 SCHEM 196.4713 14.07434 0 USR DISPLAY 0 0 SRT 1.105068 2.321573 3.809085 0 3.141593 0.7853982 19.51015 18.83691 0 MPRFLG 0 ; 
       20 SCHEM 210.4071 -3.23911 0 USR DISPLAY 0 0 SRT 0.8123375 1.706592 2.800065 0 1.509958e-007 2.356194 14.34195 -14.41924 0 MPRFLG 0 ; 
       21 SCHEM 212.9071 -3.23911 0 USR DISPLAY 0 0 SRT 0.8123375 1.706592 2.800065 3.141593 0 0.7853982 -14.34195 -14.41924 0 MPRFLG 0 ; 
       0 SCHEM 197.5 0 0 DISPLAY 0 0 SRT 1.282 1.282 1.282 0 0 0 0 2.746611 13.60004 MPRFLG 0 ; 
       4 SCHEM 247.8842 -1.788511 0 USR DISPLAY 0 0 SRT 1.506796 1.506796 1.506796 3.141593 1.570796 0 16.10221 0.001187341 -7.038498e-007 MPRFLG 0 ; 
       25 SCHEM 192.7668 14.22638 0 USR DISPLAY 0 0 SRT 1.025503 2.154419 2.344414 0 1.509958e-007 -0.8253984 -24.31584 15.52263 0 MPRFLG 0 ; 
       26 SCHEM 201.4713 14.07434 0 USR DISPLAY 0 0 SRT 1.105068 2.321573 3.809085 3.141593 1.509958e-007 0.7853982 -19.51015 -18.83691 0 MPRFLG 0 ; 
       5 SCHEM 202.5 0 0 DISPLAY 0 0 SRT 1.282 1.282 1.282 0 0 0 0 2.746611 -13.60004 MPRFLG 0 ; 
       6 SCHEM 205 0 0 DISPLAY 0 0 SRT 1.282 1.282 1.282 -3.141593 0 0 0 -2.746611 -13.60004 MPRFLG 0 ; 
       10 SCHEM 256.1407 -36.41362 0 MPRFLG 0 ; 
       11 SCHEM 261.1408 -34.41362 0 MPRFLG 0 ; 
       13 SCHEM 246.7972 -31.38915 0 MPRFLG 0 ; 
       14 SCHEM 251.7972 -29.38915 0 MPRFLG 0 ; 
       15 SCHEM 238.7751 -25.04982 0 MPRFLG 0 ; 
       16 SCHEM 233.7751 -27.04982 0 MPRFLG 0 ; 
       27 SCHEM 203.9713 14.07434 0 USR DISPLAY 0 0 SRT 1.105068 2.321573 3.809085 0 1.509958e-007 2.356194 19.51015 -18.83691 0 MPRFLG 0 ; 
       7 SCHEM 253.5449 -1.843977 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1.506796 1.506796 1.506796 -3.141593 0 0 0 -3.228223 -15.98477 MPRFLG 0 ; 
       22 SCHEM 244.1899 -18.9603 0 MPRFLG 0 ; 
       23 SCHEM 206.6833 -7.945899 0 USR DISPLAY 0 0 SRT 1 1 1 1.707694e-007 -0.4859993 -3.141592 -5.312122 0.7656765 -44.68515 MPRFLG 0 ; 
       69 SCHEM 252.9399 -18.9603 0 MPRFLG 0 ; 
       24 SCHEM 228.3941 23.91578 0 USR DISPLAY 0 0 SRT 1 1 1 -1.707696e-007 -2.655591 -3.141592 5.312205 0.7656819 -44.68514 MPRFLG 0 ; 
       8 SCHEM 206.8758 15.43665 0 USR DISPLAY 0 0 SRT 0.7481958 0.7481959 0.7481958 0 0 0 20.89274 0 0 MPRFLG 0 ; 
       9 SCHEM 199.6686 13.26532 0 USR DISPLAY 0 0 SRT 0.7481958 0.7481959 0.7481958 0 3.141593 0 -20.89274 0 0 MPRFLG 0 ; 
       28 SCHEM 242.9399 -20.9603 0 MPRFLG 0 ; 
       119 SCHEM 193.0472 -1.8523 0 USR DISPLAY 0 0 SRT 1 1 1 0 6.357286e-008 -4.805273e-007 -5.59217e-007 -8.467035 2.769063 MPRFLG 0 ; 
       120 SCHEM 191.7972 -3.8523 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 265.2881 -37.89487 0 USR MPRFLG 0 ; 
       12 SCHEM 265.2881 -39.89487 0 MPRFLG 0 ; 
       42 SCHEM 244.2972 -33.38915 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 231.2751 -29.04982 0 WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 249.2972 -33.38915 0 WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 236.2751 -29.04982 0 WIRECOL 9 7 MPRFLG 0 ; 
       46 SCHEM 246.7972 -33.38915 0 WIRECOL 9 7 MPRFLG 0 ; 
       47 SCHEM 233.7751 -29.04982 0 WIRECOL 9 7 MPRFLG 0 ; 
       48 SCHEM 254.2972 -33.38915 0 WIRECOL 9 7 MPRFLG 0 ; 
       49 SCHEM 241.2751 -29.04982 0 WIRECOL 9 7 MPRFLG 0 ; 
       50 SCHEM 251.7972 -33.38915 0 WIRECOL 9 7 MPRFLG 0 ; 
       51 SCHEM 238.7751 -29.04982 0 WIRECOL 9 7 MPRFLG 0 ; 
       52 SCHEM 231.7972 -35.38914 0 MPRFLG 0 ; 
       53 SCHEM 246.1407 -38.41362 0 MPRFLG 0 ; 
       54 SCHEM 218.7751 -31.04982 0 MPRFLG 0 ; 
       56 SCHEM 239.2972 -35.38914 0 MPRFLG 0 ; 
       57 SCHEM 253.6407 -38.41362 0 MPRFLG 0 ; 
       58 SCHEM 226.2751 -31.04982 0 MPRFLG 0 ; 
       60 SCHEM 258.6407 -38.41362 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 260.2881 -41.89487 0 MPRFLG 0 ; 
       62 SCHEM 238.5053 24.40813 0 USR DISPLAY 0 0 SRT 1 1 1 3.258412e-007 3.141593 -3.141592 -5.160648e-006 -9.084629 0.03265271 MPRFLG 0 ; 
       63 SCHEM 233.6063 22.71787 0 USR DISPLAY 0 0 SRT 1 1 1 3.258412e-007 3.141593 -3.141592 -1.089381e-005 8.510309 0.03265274 MPRFLG 0 ; 
       64 SCHEM 228.8488 20.6286 0 USR DISPLAY 0 0 SRT 1 1 1 3.141593 2.098395 -6.283185 -7.280627e-006 -2.578461 0.0326527 MPRFLG 0 ; 
       65 SCHEM 223.9948 18.36758 0 USR DISPLAY 0 0 SRT 1 1 1 3.243628e-007 3.144211 -3.141592 2.823363e-006 29.23683 0.03263943 MPRFLG 0 ; 
       66 SCHEM 251.7972 -27.38915 0 MPRFLG 0 ; 
       67 SCHEM 238.7751 -23.04982 0 USR MPRFLG 0 ; 
       68 SCHEM 261.1408 -32.41363 0 USR MPRFLG 0 ; 
       61 SCHEM 262.7881 -43.89487 0 WIRECOL 9 7 MPRFLG 0 ; 
       59 SCHEM 257.7881 -43.89487 0 MPRFLG 0 ; 
       110 SCHEM 260.2881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       111 SCHEM 255.2881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       112 SCHEM 257.7881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 250.2881 -43.89487 0 MPRFLG 0 ; 
       113 SCHEM 252.7881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       114 SCHEM 247.7881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       115 SCHEM 250.2881 -45.89487 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 245.4399 -20.9603 0 MPRFLG 0 ; 
       71 SCHEM 275.5472 -25.38915 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 196.7174 9.145989 0 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 220.2447 16.36758 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 222.7447 16.36758 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 225.2448 16.36758 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 227.7448 16.36758 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 236.0053 22.40813 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 238.5053 22.40813 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       85 SCHEM 241.0053 22.40813 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       86 SCHEM 231.1063 20.71787 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       87 SCHEM 233.6063 20.71787 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       88 SCHEM 236.1063 20.71787 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       89 SCHEM 231.3488 18.6286 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       90 SCHEM 228.8488 18.6286 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       91 SCHEM 226.3488 18.6286 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       92 SCHEM 229.2972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 231.7972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       94 SCHEM 234.2972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       95 SCHEM 236.7972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       96 SCHEM 239.2972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       97 SCHEM 241.7972 -37.38915 0 WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 243.6407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 246.1407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       100 SCHEM 248.6407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 251.1407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       102 SCHEM 253.6407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       103 SCHEM 256.1407 -40.41362 0 WIRECOL 3 7 MPRFLG 0 ; 
       104 SCHEM 221.2751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       105 SCHEM 216.2751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       106 SCHEM 218.7751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 189.8667 10.93192 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       107 SCHEM 228.7751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       108 SCHEM 223.7751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       109 SCHEM 226.2751 -33.04982 0 WIRECOL 3 7 MPRFLG 0 ; 
       116 SCHEM 235.5472 -33.38915 0 MPRFLG 0 ; 
       117 SCHEM 222.5251 -29.04982 0 MPRFLG 0 ; 
       118 SCHEM 250.4399 -18.9603 0 MPRFLG 0 ; 
       72 SCHEM 200.4673 11.14599 0 USR DISPLAY 0 0 SRT 0.8640001 0.864 0.8640001 0 0.7853981 0 0 17.67004 0 MPRFLG 0 ; 
       31 SCHEM 204.2173 9.145989 0 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 199.2173 9.145989 0 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 201.7173 9.145989 0 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 269.1899 -18.9603 0 MPRFLG 0 ; 
       34 SCHEM 272.9399 -20.9603 0 MPRFLG 0 ; 
       35 SCHEM 267.9399 -20.9603 0 MPRFLG 0 ; 
       36 SCHEM 270.4399 -20.9603 0 MPRFLG 0 ; 
       37 SCHEM 265.4399 -20.9603 0 MPRFLG 0 ; 
       1 SCHEM 255.7185 -1.843985 0 USR WIRECOL 1 7 DISPLAY 0 0 SRT 1.506796 1.506796 1.506796 3.141593 0 0 0 -3.228223 15.98477 MPRFLG 0 ; 
       2 SCHEM 224.6379 -1.35203 0 USR DISPLAY 0 0 SRT 1.743977 1.743977 1.743977 -3.141593 1.570796 1.748456e-007 -18.63681 0.001374237 -8.146409e-007 MPRFLG 0 ; 
       74 SCHEM 247.9399 -18.9603 0 MPRFLG 0 ; 
       3 SCHEM 250.3842 -1.788511 0 USR DISPLAY 0 0 SRT 1.506796 1.506796 1.506796 -3.141593 1.570796 1.748456e-007 -16.10221 0.001187341 -7.038498e-007 MPRFLG 0 ; 
       75 SCHEM 259.1899 -18.9603 0 MPRFLG 0 ; 
       38 SCHEM 262.9399 -20.9603 0 MPRFLG 0 ; 
       39 SCHEM 257.9399 -20.9603 0 MPRFLG 0 ; 
       40 SCHEM 260.4399 -20.9603 0 MPRFLG 0 ; 
       41 SCHEM 255.4399 -20.9603 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       14 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 170 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 160 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 122.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 172.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 162.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 125 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 177.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 175 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 167.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 165 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 130 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 127.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 60 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 55 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 57.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 107.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 105 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 100 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 27.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 22.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 20 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 17.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 145 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 140 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 142.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 137.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 132.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 135 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 52.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 47.5 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 50 -16 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 153.9185 2.395733 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 146.4185 2.395733 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 195 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 192.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 148.9185 2.395733 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 151.4185 2.395733 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 187.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 185 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 182.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 143.9185 0.3957329 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 136.4185 0.3957329 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 138.9185 0.3957329 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 141.4185 0.3957329 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 123.9185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 118.9185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 121.4185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 131.4185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 126.4185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 128.9185 -3.604267 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       8 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 170 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 172.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 160 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 162.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 122.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 125 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 85 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 87.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 117.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 177.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 175 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 167.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 165 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 130 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 127.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 92.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 90 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 153.9185 0.3957329 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 146.4185 0.3957329 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 195 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 192.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 148.9185 0.3957329 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 151.4185 0.3957329 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 143.9185 -1.604267 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 136.4185 -1.604267 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 138.9185 -1.604267 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 141.4185 -1.604267 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
