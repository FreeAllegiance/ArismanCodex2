SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard_ss102-cam_int1.8-0 ROOT ; 
       shipyard_ss102-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_50.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_58.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_62.1-0 ; 
       shipyard_ss102-white_strobe1_63.1-0 ; 
       shipyard_ss102-white_strobe1_64.1-0 ; 
       shipyard_ss102-white_strobe1_65.1-0 ; 
       shipyard_ss102-white_strobe1_66.1-0 ; 
       shipyard_ss102-white_strobe1_67.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       shipyard_ss102-bound01.1-0 ; 
       shipyard_ss102-bound02.1-0 ; 
       shipyard_ss102-bound03.1-0 ; 
       shipyard_ss102-bound04.1-0 ; 
       shipyard_ss102-bounding_model.1-0 ; 
       shipyard_ss102-east_bay_11_1.4-0 ; 
       shipyard_ss102-east_bay_11_11.1-0 ; 
       shipyard_ss102-east_bay_11_12.1-0 ; 
       shipyard_ss102-east_bay_11_8.1-0 ; 
       shipyard_ss102-extru1.2-0 ; 
       shipyard_ss102-extru5.1-0 ; 
       shipyard_ss102-garage1A.1-0 ; 
       shipyard_ss102-garage1B.1-0 ; 
       shipyard_ss102-garage1C.1-0 ; 
       shipyard_ss102-garage1D.1-0 ; 
       shipyard_ss102-garage1E.1-0 ; 
       shipyard_ss102-landing_lights_2.1-0 ; 
       shipyard_ss102-landing_lights2.1-0 ; 
       shipyard_ss102-landing_lights2_4.1-0 ; 
       shipyard_ss102-landing_lights3.1-0 ; 
       shipyard_ss102-launch3.1-0 ; 
       shipyard_ss102-null18.3-0 ; 
       shipyard_ss102-null19.3-0 ; 
       shipyard_ss102-null23.6-0 ROOT ; 
       shipyard_ss102-null26.1-0 ; 
       shipyard_ss102-SS_11_1.1-0 ; 
       shipyard_ss102-SS_11_4.1-0 ; 
       shipyard_ss102-SS_13_2.1-0 ; 
       shipyard_ss102-SS_15_1.1-0 ; 
       shipyard_ss102-SS_15_4.1-0 ; 
       shipyard_ss102-SS_23_2.1-0 ; 
       shipyard_ss102-SS_23_4.1-0 ; 
       shipyard_ss102-SS_24_1.1-0 ; 
       shipyard_ss102-SS_24_4.1-0 ; 
       shipyard_ss102-SS_26.1-0 ; 
       shipyard_ss102-SS_26_4.1-0 ; 
       shipyard_ss102-SS_29.1-0 ; 
       shipyard_ss102-SS_30.1-0 ; 
       shipyard_ss102-SS_31.1-0 ; 
       shipyard_ss102-SS_32.1-0 ; 
       shipyard_ss102-SS_33.1-0 ; 
       shipyard_ss102-SS_34.1-0 ; 
       shipyard_ss102-SS_35.1-0 ; 
       shipyard_ss102-SS_36.1-0 ; 
       shipyard_ss102-SS_55.2-0 ; 
       shipyard_ss102-SS_58.1-0 ; 
       shipyard_ss102-SS_60.1-0 ; 
       shipyard_ss102-SS_61.1-0 ; 
       shipyard_ss102-SS_68.1-0 ; 
       shipyard_ss102-SS_69.1-0 ; 
       shipyard_ss102-SS_70.1-0 ; 
       shipyard_ss102-SS_71.1-0 ; 
       shipyard_ss102-SS_72.1-0 ; 
       shipyard_ss102-SS_73.1-0 ; 
       shipyard_ss102-SS_74.1-0 ; 
       shipyard_ss102-SS_75.1-0 ; 
       shipyard_ss102-SS_76.1-0 ; 
       shipyard_ss102-SS_77.1-0 ; 
       shipyard_ss102-SS_78.1-0 ; 
       shipyard_ss102-strobe_set_1.1-0 ; 
       shipyard_ss102-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-shipyard-ss102.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       shipyard_ss102-t2d49.2-0 ; 
       shipyard_ss102-t2d50.2-0 ; 
       shipyard_ss102-t2d51.2-0 ; 
       shipyard_ss102-t2d58.2-0 ; 
       shipyard_ss102-t2d59.2-0 ; 
       shipyard_ss102-t2d60.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       4 23 110 ; 
       5 8 110 ; 
       6 10 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 23 110 ; 
       10 23 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 59 110 ; 
       17 59 110 ; 
       18 60 110 ; 
       19 60 110 ; 
       20 7 110 ; 
       24 23 110 ; 
       25 16 110 ; 
       26 19 110 ; 
       27 16 110 ; 
       28 16 110 ; 
       29 19 110 ; 
       30 17 110 ; 
       31 18 110 ; 
       32 17 110 ; 
       33 18 110 ; 
       34 17 110 ; 
       35 18 110 ; 
       44 24 110 ; 
       45 19 110 ; 
       50 24 110 ; 
       51 24 110 ; 
       52 24 110 ; 
       53 24 110 ; 
       54 24 110 ; 
       55 24 110 ; 
       56 24 110 ; 
       57 24 110 ; 
       58 24 110 ; 
       59 5 110 ; 
       60 7 110 ; 
       21 23 110 ; 
       22 23 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 21 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       46 22 110 ; 
       47 22 110 ; 
       48 21 110 ; 
       49 21 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 4 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       5 5 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       7 12 300 ; 
       7 1 300 ; 
       7 3 300 ; 
       7 13 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       25 31 300 ; 
       26 27 300 ; 
       27 36 300 ; 
       28 45 300 ; 
       29 29 300 ; 
       30 46 300 ; 
       31 24 300 ; 
       32 47 300 ; 
       33 25 300 ; 
       34 14 300 ; 
       35 26 300 ; 
       44 23 300 ; 
       45 28 300 ; 
       50 35 300 ; 
       51 37 300 ; 
       52 38 300 ; 
       53 39 300 ; 
       54 40 300 ; 
       55 41 300 ; 
       56 42 300 ; 
       57 43 300 ; 
       58 44 300 ; 
       36 18 300 ; 
       37 17 300 ; 
       38 16 300 ; 
       39 15 300 ; 
       40 19 300 ; 
       41 20 300 ; 
       42 21 300 ; 
       43 22 300 ; 
       46 30 300 ; 
       47 32 300 ; 
       48 33 300 ; 
       49 34 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -228.4653 -8.113341 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -228.4653 -10.11334 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -206.925 49.98227 0 WIRECOL 6 7 MPRFLG 0 ; 
       1 SCHEM -204.425 49.98227 0 WIRECOL 6 7 MPRFLG 0 ; 
       2 SCHEM -201.925 49.98227 0 WIRECOL 6 7 MPRFLG 0 ; 
       3 SCHEM -199.425 49.98227 0 WIRECOL 6 7 MPRFLG 0 ; 
       4 SCHEM -203.175 51.98227 0 USR MPRFLG 0 ; 
       5 SCHEM -194.2815 25.00546 0 MPRFLG 0 ; 
       6 SCHEM -163.9138 27.03214 0 USR MPRFLG 0 ; 
       7 SCHEM -163.9138 25.03214 0 MPRFLG 0 ; 
       8 SCHEM -194.2815 27.00546 0 USR MPRFLG 0 ; 
       9 SCHEM -188.6003 30.85949 0 USR MPRFLG 0 ; 
       10 SCHEM -175.2155 31.40101 0 USR MPRFLG 0 ; 
       11 SCHEM -193.0315 23.00546 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM -185.5315 23.00546 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM -190.5315 23.00546 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM -180.5315 23.00546 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM -183.0315 23.00546 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM -205.5315 21.00546 0 MPRFLG 0 ; 
       17 SCHEM -198.0315 21.00546 0 MPRFLG 0 ; 
       18 SCHEM -162.6638 21.03216 0 MPRFLG 0 ; 
       19 SCHEM -170.1638 21.03216 0 MPRFLG 0 ; 
       20 SCHEM -155.1638 23.03214 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM -181.387 36.7088 0 USR SRT 1 1 1 0 0 0 -4.81501e-007 0 0 MPRFLG 0 ; 
       24 SCHEM -215.6465 29.87924 0 USR MPRFLG 0 ; 
       25 SCHEM -203.0315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM -167.6638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM -208.0315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM -205.5315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM -170.1638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM -195.5315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM -160.1638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM -200.5315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM -165.1638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM -198.0315 19.00546 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM -162.6638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM -226.8965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM -172.6638 19.03216 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM -224.3965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM -221.8965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM -219.3965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM -216.8965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM -214.3965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM -211.8965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM -209.3965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM -206.8965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM -204.3965 27.87924 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM -201.7815 23.00546 0 MPRFLG 0 ; 
       60 SCHEM -166.4138 23.03214 0 MPRFLG 0 ; 
       21 SCHEM -166.3302 14.90914 0 USR MPRFLG 0 ; 
       22 SCHEM -178.6095 17.92551 0 USR MPRFLG 0 ; 
       36 SCHEM -170.0802 12.90913 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM -167.5802 12.90913 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM -165.0802 12.90913 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM -162.5802 12.90913 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM -177.3595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM -179.8595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM -182.3595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM -184.8595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM -174.8595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM -172.3595 15.92551 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM -160.0052 12.90237 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM -157.4252 12.93508 0 USR WIRECOL 2 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 507.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 691.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 510.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 693.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 505.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 512.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 515.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 517.8479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 520.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 698.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 701.4046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 703.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 688.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 696.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 485.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 395.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 397.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 400.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 402.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 73.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 78.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 80.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM -38.81696 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 681.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 676.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 678.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 673.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 668.9047 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 671.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 480.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 106.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 428.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 433.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -20.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 475.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -15.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM -10.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM -5.193527 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -26.15214 2.160294 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4.806473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14.80647 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 477.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 487.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 482.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 515.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 517.8479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 520.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 698.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 701.4046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 703.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
