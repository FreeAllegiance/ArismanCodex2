SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard_ss102-cam_int1.42-0 ROOT ; 
       shipyard_ss102-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       shipyard_ss102-mat58.2-0 ; 
       shipyard_ss102-mat59.2-0 ; 
       shipyard_ss102-mat60.2-0 ; 
       shipyard_ss102-mat67.2-0 ; 
       shipyard_ss102-mat68.2-0 ; 
       shipyard_ss102-mat69.2-0 ; 
       shipyard_ss102-mat77.6-0 ; 
       shipyard_ss102-mat78.6-0 ; 
       shipyard_ss102-mat79.4-0 ; 
       shipyard_ss102-mat80.3-0 ; 
       shipyard_ss102-mat81.2-0 ; 
       shipyard_ss102-mat82.4-0 ; 
       shipyard_ss102-mat83.4-0 ; 
       shipyard_ss102-mat84.4-0 ; 
       shipyard_ss102-mat85.4-0 ; 
       shipyard_ss102-mat86.4-0 ; 
       shipyard_ss102-outside1.6-0 ; 
       shipyard_ss102-outside2.4-0 ; 
       shipyard_ss102-white_strobe1_10.2-0 ; 
       shipyard_ss102-white_strobe1_29.2-0 ; 
       shipyard_ss102-white_strobe1_30.2-0 ; 
       shipyard_ss102-white_strobe1_31.2-0 ; 
       shipyard_ss102-white_strobe1_32.2-0 ; 
       shipyard_ss102-white_strobe1_33.2-0 ; 
       shipyard_ss102-white_strobe1_34.2-0 ; 
       shipyard_ss102-white_strobe1_35.2-0 ; 
       shipyard_ss102-white_strobe1_36.2-0 ; 
       shipyard_ss102-white_strobe1_4.2-0 ; 
       shipyard_ss102-white_strobe1_43.2-0 ; 
       shipyard_ss102-white_strobe1_44.2-0 ; 
       shipyard_ss102-white_strobe1_45.2-0 ; 
       shipyard_ss102-white_strobe1_46.2-0 ; 
       shipyard_ss102-white_strobe1_47.2-0 ; 
       shipyard_ss102-white_strobe1_48.2-0 ; 
       shipyard_ss102-white_strobe1_49.2-0 ; 
       shipyard_ss102-white_strobe1_5.2-0 ; 
       shipyard_ss102-white_strobe1_50.2-0 ; 
       shipyard_ss102-white_strobe1_57.2-0 ; 
       shipyard_ss102-white_strobe1_58.2-0 ; 
       shipyard_ss102-white_strobe1_59.2-0 ; 
       shipyard_ss102-white_strobe1_6.2-0 ; 
       shipyard_ss102-white_strobe1_60.2-0 ; 
       shipyard_ss102-white_strobe1_61.2-0 ; 
       shipyard_ss102-white_strobe1_62.2-0 ; 
       shipyard_ss102-white_strobe1_63.2-0 ; 
       shipyard_ss102-white_strobe1_64.2-0 ; 
       shipyard_ss102-white_strobe1_65.2-0 ; 
       shipyard_ss102-white_strobe1_66.2-0 ; 
       shipyard_ss102-white_strobe1_67.2-0 ; 
       shipyard_ss102-white_strobe1_7.2-0 ; 
       shipyard_ss102-white_strobe1_8.2-0 ; 
       shipyard_ss102-white_strobe1_9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       ss102-bay2.1-0 ; 
       ss102-bay3.1-0 ; 
       ss102-east_bay_11_11.1-0 ; 
       ss102-east_bay_11_8.1-0 ; 
       ss102-extru1.2-0 ; 
       ss102-extru8.1-0 ; 
       ss102-garage1A.1-0 ; 
       ss102-garage1B.1-0 ; 
       ss102-garage1C.1-0 ; 
       ss102-garage1D.1-0 ; 
       ss102-garage1E.1-0 ; 
       ss102-garage2A.1-0 ; 
       ss102-garage2B.1-0 ; 
       ss102-garage2C.1-0 ; 
       ss102-garage2D.1-0 ; 
       ss102-garage2E.1-0 ; 
       ss102-garage3A.1-0 ; 
       ss102-garage3B.1-0 ; 
       ss102-garage3C.1-0 ; 
       ss102-garage3D.1-0 ; 
       ss102-garage3E.1-0 ; 
       ss102-launch1.1-0 ; 
       ss102-launch2.1-0 ; 
       ss102-launch3.1-0 ; 
       ss102-null47.1-0 ; 
       ss102-SS_11_1.1-0 ; 
       ss102-SS_11_4.1-0 ; 
       ss102-SS_13_2.1-0 ; 
       ss102-SS_15_1.1-0 ; 
       ss102-SS_15_4.1-0 ; 
       ss102-SS_23_2.1-0 ; 
       ss102-SS_23_4.1-0 ; 
       ss102-SS_24_1.1-0 ; 
       ss102-SS_24_4.1-0 ; 
       ss102-SS_26.1-0 ; 
       ss102-SS_26_4.1-0 ; 
       ss102-SS_29.1-0 ; 
       ss102-SS_30.1-0 ; 
       ss102-SS_31.1-0 ; 
       ss102-SS_32.1-0 ; 
       ss102-SS_33.1-0 ; 
       ss102-SS_34.1-0 ; 
       ss102-SS_35.1-0 ; 
       ss102-SS_36.1-0 ; 
       ss102-SS_55.2-0 ; 
       ss102-SS_58.1-0 ; 
       ss102-SS_60.1-0 ; 
       ss102-SS_61.1-0 ; 
       ss102-SS_68.1-0 ; 
       ss102-SS_69.1-0 ; 
       ss102-SS_70.1-0 ; 
       ss102-SS_71.1-0 ; 
       ss102-SS_72.1-0 ; 
       ss102-SS_73.1-0 ; 
       ss102-SS_74.1-0 ; 
       ss102-SS_75.1-0 ; 
       ss102-SS_76.1-0 ; 
       ss102-SS_77.1-0 ; 
       ss102-SS_78.1-0 ; 
       ss102-ss102.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/ss102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Rotate-shipyard-ss102.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       shipyard_ss102-t2d50.9-0 ; 
       shipyard_ss102-t2d51.9-0 ; 
       shipyard_ss102-t2d59.10-0 ; 
       shipyard_ss102-t2d60.10-0 ; 
       shipyard_ss102-t2d61.16-0 ; 
       shipyard_ss102-t2d62.16-0 ; 
       shipyard_ss102-t2d63.14-0 ; 
       shipyard_ss102-t2d64.12-0 ; 
       shipyard_ss102-t2d65.11-0 ; 
       shipyard_ss102-t2d66.11-0 ; 
       shipyard_ss102-t2d73.5-0 ; 
       shipyard_ss102-t2d74.5-0 ; 
       shipyard_ss102-t2d75.5-0 ; 
       shipyard_ss102-t2d76.5-0 ; 
       shipyard_ss102-t2d77.5-0 ; 
       shipyard_ss102-t2d78.5-0 ; 
       shipyard_ss102-t2d79.5-0 ; 
       shipyard_ss102-t2d80.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 59 110 ; 
       1 59 110 ; 
       2 59 110 ; 
       3 59 110 ; 
       4 59 110 ; 
       5 59 110 ; 
       6 59 110 ; 
       7 59 110 ; 
       8 59 110 ; 
       9 59 110 ; 
       10 59 110 ; 
       11 59 110 ; 
       12 59 110 ; 
       13 59 110 ; 
       14 59 110 ; 
       15 59 110 ; 
       16 59 110 ; 
       17 59 110 ; 
       18 59 110 ; 
       19 59 110 ; 
       20 59 110 ; 
       21 59 110 ; 
       24 59 110 ; 
       25 3 110 ; 
       26 2 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 2 110 ; 
       30 3 110 ; 
       31 2 110 ; 
       32 3 110 ; 
       33 2 110 ; 
       34 3 110 ; 
       35 2 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 24 110 ; 
       39 24 110 ; 
       40 24 110 ; 
       41 24 110 ; 
       42 24 110 ; 
       43 24 110 ; 
       44 24 110 ; 
       45 2 110 ; 
       46 24 110 ; 
       47 24 110 ; 
       48 24 110 ; 
       49 24 110 ; 
       50 24 110 ; 
       51 24 110 ; 
       52 24 110 ; 
       53 24 110 ; 
       54 24 110 ; 
       55 24 110 ; 
       56 24 110 ; 
       57 24 110 ; 
       58 24 110 ; 
       22 59 110 ; 
       23 59 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 6 300 ; 
       4 16 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 17 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       25 35 300 ; 
       26 31 300 ; 
       27 40 300 ; 
       28 49 300 ; 
       29 33 300 ; 
       30 50 300 ; 
       31 28 300 ; 
       32 51 300 ; 
       33 29 300 ; 
       34 18 300 ; 
       35 30 300 ; 
       36 22 300 ; 
       37 21 300 ; 
       38 20 300 ; 
       39 19 300 ; 
       40 23 300 ; 
       41 24 300 ; 
       42 25 300 ; 
       43 26 300 ; 
       44 27 300 ; 
       45 32 300 ; 
       46 34 300 ; 
       47 36 300 ; 
       48 37 300 ; 
       49 38 300 ; 
       50 39 300 ; 
       51 41 300 ; 
       52 42 300 ; 
       53 43 300 ; 
       54 44 300 ; 
       55 45 300 ; 
       56 46 300 ; 
       57 47 300 ; 
       58 48 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 17 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 9 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 11 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
       15 16 401 ; 
       16 4 401 ; 
       17 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999999 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 75 -2 0 MPRFLG 0 ; 
       1 SCHEM 55 -2 0 MPRFLG 0 ; 
       2 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       3 SCHEM 63.75 -2 0 MPRFLG 0 ; 
       4 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 77.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 102.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 105 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 107.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 110 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 112.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 115 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 117.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 120 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 122.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 125 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 127.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       17 SCHEM 130 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       18 SCHEM 132.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       19 SCHEM 135 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       20 SCHEM 137.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 95 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 26.25 -2 0 MPRFLG 0 ; 
       25 SCHEM 62.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 85 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 57.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 60 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 82.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 70 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 92.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 65 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 87.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 67.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 90 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 0 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 2.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 7.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 22.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 20 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 17.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 15 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 80 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 25 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 10 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 12.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 68.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 -4.81501e-007 0 0 MPRFLG 0 ; 
       22 SCHEM 97.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 100 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 96.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 98.59482 2.758616 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 134.0519 2.758616 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 74 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 79 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 69 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 94 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 89 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 91.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 81.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 64 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 61.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 71.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 66.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 98.59482 0.7586164 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 74 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 134.0519 0.7586164 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 79 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 96.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
