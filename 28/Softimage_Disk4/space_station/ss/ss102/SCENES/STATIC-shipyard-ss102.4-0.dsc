SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard_ss102-cam_int1.57-0 ROOT ; 
       shipyard_ss102-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 15     
       shipyard_ss102-mat58.2-0 ; 
       shipyard_ss102-mat59.2-0 ; 
       shipyard_ss102-mat60.2-0 ; 
       shipyard_ss102-mat77.6-0 ; 
       shipyard_ss102-mat78.6-0 ; 
       shipyard_ss102-mat79.4-0 ; 
       shipyard_ss102-mat80.3-0 ; 
       shipyard_ss102-mat81.2-0 ; 
       shipyard_ss102-mat82.4-0 ; 
       shipyard_ss102-mat83.4-0 ; 
       shipyard_ss102-mat84.4-0 ; 
       shipyard_ss102-mat85.4-0 ; 
       shipyard_ss102-mat86.4-0 ; 
       shipyard_ss102-outside1.6-0 ; 
       shipyard_ss102-outside2.4-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 7     
       ss102-bay2.1-0 ; 
       ss102-bay3.1-0 ; 
       ss102-east_bay_11_8.1-0 ; 
       ss102-extru1.2-0 ; 
       ss102-extru8.1-0 ; 
       ss102-null48.1-0 ; 
       ss102-ss102.17-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/ss102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-shipyard-ss102.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       shipyard_ss102-t2d50.13-0 ; 
       shipyard_ss102-t2d51.13-0 ; 
       shipyard_ss102-t2d61.20-0 ; 
       shipyard_ss102-t2d62.20-0 ; 
       shipyard_ss102-t2d63.18-0 ; 
       shipyard_ss102-t2d64.16-0 ; 
       shipyard_ss102-t2d65.15-0 ; 
       shipyard_ss102-t2d66.15-0 ; 
       shipyard_ss102-t2d73.9-0 ; 
       shipyard_ss102-t2d74.9-0 ; 
       shipyard_ss102-t2d75.9-0 ; 
       shipyard_ss102-t2d76.9-0 ; 
       shipyard_ss102-t2d77.9-0 ; 
       shipyard_ss102-t2d78.9-0 ; 
       shipyard_ss102-t2d79.9-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 6 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       3 6 110 ; 
       4 6 110 ; 
       5 6 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 0 300 ; 
       2 1 300 ; 
       2 2 300 ; 
       3 3 300 ; 
       3 13 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       4 8 300 ; 
       4 14 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       4 11 300 ; 
       4 12 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 8 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 7 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 9 401 ; 
       9 11 401 ; 
       10 12 401 ; 
       11 13 401 ; 
       12 14 401 ; 
       13 2 401 ; 
       14 10 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 15 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 -4.81501e-007 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 15.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 20.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 9 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 14 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 6.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 20.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
