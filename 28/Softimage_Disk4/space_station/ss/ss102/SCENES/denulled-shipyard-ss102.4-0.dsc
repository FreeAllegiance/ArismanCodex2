SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.3-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 52     
       shipyard_ss102-mat58.2-0 ; 
       shipyard_ss102-mat59.2-0 ; 
       shipyard_ss102-mat60.2-0 ; 
       shipyard_ss102-mat67.2-0 ; 
       shipyard_ss102-mat68.2-0 ; 
       shipyard_ss102-mat69.2-0 ; 
       shipyard_ss102-mat77.6-0 ; 
       shipyard_ss102-mat78.6-0 ; 
       shipyard_ss102-mat79.4-0 ; 
       shipyard_ss102-mat80.3-0 ; 
       shipyard_ss102-mat81.2-0 ; 
       shipyard_ss102-mat82.4-0 ; 
       shipyard_ss102-mat83.4-0 ; 
       shipyard_ss102-mat84.4-0 ; 
       shipyard_ss102-mat85.4-0 ; 
       shipyard_ss102-mat86.4-0 ; 
       shipyard_ss102-outside1.6-0 ; 
       shipyard_ss102-outside2.4-0 ; 
       shipyard_ss102-white_strobe1_10.2-0 ; 
       shipyard_ss102-white_strobe1_29.2-0 ; 
       shipyard_ss102-white_strobe1_30.2-0 ; 
       shipyard_ss102-white_strobe1_31.2-0 ; 
       shipyard_ss102-white_strobe1_32.2-0 ; 
       shipyard_ss102-white_strobe1_33.2-0 ; 
       shipyard_ss102-white_strobe1_34.2-0 ; 
       shipyard_ss102-white_strobe1_35.2-0 ; 
       shipyard_ss102-white_strobe1_36.2-0 ; 
       shipyard_ss102-white_strobe1_4.2-0 ; 
       shipyard_ss102-white_strobe1_43.2-0 ; 
       shipyard_ss102-white_strobe1_44.2-0 ; 
       shipyard_ss102-white_strobe1_45.2-0 ; 
       shipyard_ss102-white_strobe1_46.2-0 ; 
       shipyard_ss102-white_strobe1_47.2-0 ; 
       shipyard_ss102-white_strobe1_48.2-0 ; 
       shipyard_ss102-white_strobe1_49.2-0 ; 
       shipyard_ss102-white_strobe1_5.2-0 ; 
       shipyard_ss102-white_strobe1_50.2-0 ; 
       shipyard_ss102-white_strobe1_57.2-0 ; 
       shipyard_ss102-white_strobe1_58.2-0 ; 
       shipyard_ss102-white_strobe1_59.2-0 ; 
       shipyard_ss102-white_strobe1_6.2-0 ; 
       shipyard_ss102-white_strobe1_60.2-0 ; 
       shipyard_ss102-white_strobe1_61.2-0 ; 
       shipyard_ss102-white_strobe1_62.2-0 ; 
       shipyard_ss102-white_strobe1_63.2-0 ; 
       shipyard_ss102-white_strobe1_64.2-0 ; 
       shipyard_ss102-white_strobe1_65.2-0 ; 
       shipyard_ss102-white_strobe1_66.2-0 ; 
       shipyard_ss102-white_strobe1_67.2-0 ; 
       shipyard_ss102-white_strobe1_7.2-0 ; 
       shipyard_ss102-white_strobe1_8.2-0 ; 
       shipyard_ss102-white_strobe1_9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 58     
       ss102-bay3.1-0 ; 
       ss102-bay5.1-0 ; 
       ss102-east_bay_11_11.1-0 ; 
       ss102-east_bay_11_8.1-0 ; 
       ss102-extru1.2-0 ; 
       ss102-extru8.1-0 ; 
       ss102-garage1A.1-0 ; 
       ss102-garage1B.1-0 ; 
       ss102-garage1C.1-0 ; 
       ss102-garage1D.1-0 ; 
       ss102-garage1E.1-0 ; 
       ss102-garage3A.1-0 ; 
       ss102-garage3B.1-0 ; 
       ss102-garage3C.1-0 ; 
       ss102-garage3D.1-0 ; 
       ss102-garage3E.1-0 ; 
       ss102-garage5A.1-0 ; 
       ss102-garage5B.1-0 ; 
       ss102-garage5C.1-0 ; 
       ss102-garage5D.1-0 ; 
       ss102-garage5E.1-0 ; 
       ss102-launch3.1-0 ; 
       ss102-null47.1-0 ; 
       ss102-SS_11_1.1-0 ; 
       ss102-SS_11_4.1-0 ; 
       ss102-SS_13_2.1-0 ; 
       ss102-SS_15_1.1-0 ; 
       ss102-SS_15_4.1-0 ; 
       ss102-SS_23_2.1-0 ; 
       ss102-SS_23_4.1-0 ; 
       ss102-SS_24_1.1-0 ; 
       ss102-SS_24_4.1-0 ; 
       ss102-SS_26.1-0 ; 
       ss102-SS_26_4.1-0 ; 
       ss102-SS_29.1-0 ; 
       ss102-SS_30.1-0 ; 
       ss102-SS_31.1-0 ; 
       ss102-SS_32.1-0 ; 
       ss102-SS_33.1-0 ; 
       ss102-SS_34.1-0 ; 
       ss102-SS_35.1-0 ; 
       ss102-SS_36.1-0 ; 
       ss102-SS_55.2-0 ; 
       ss102-SS_58.1-0 ; 
       ss102-SS_60.1-0 ; 
       ss102-SS_61.1-0 ; 
       ss102-SS_68.1-0 ; 
       ss102-SS_69.1-0 ; 
       ss102-SS_70.1-0 ; 
       ss102-SS_71.1-0 ; 
       ss102-SS_72.1-0 ; 
       ss102-SS_73.1-0 ; 
       ss102-SS_74.1-0 ; 
       ss102-SS_75.1-0 ; 
       ss102-SS_76.1-0 ; 
       ss102-SS_77.1-0 ; 
       ss102-SS_78.1-0 ; 
       ss102-ss102.4-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/ss102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       denulled-shipyard-ss102.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       shipyard_ss102-t2d50.8-0 ; 
       shipyard_ss102-t2d51.8-0 ; 
       shipyard_ss102-t2d59.9-0 ; 
       shipyard_ss102-t2d60.9-0 ; 
       shipyard_ss102-t2d61.15-0 ; 
       shipyard_ss102-t2d62.15-0 ; 
       shipyard_ss102-t2d63.13-0 ; 
       shipyard_ss102-t2d64.11-0 ; 
       shipyard_ss102-t2d65.10-0 ; 
       shipyard_ss102-t2d66.10-0 ; 
       shipyard_ss102-t2d73.4-0 ; 
       shipyard_ss102-t2d74.4-0 ; 
       shipyard_ss102-t2d75.4-0 ; 
       shipyard_ss102-t2d76.4-0 ; 
       shipyard_ss102-t2d77.4-0 ; 
       shipyard_ss102-t2d78.4-0 ; 
       shipyard_ss102-t2d79.4-0 ; 
       shipyard_ss102-t2d80.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 57 110 ; 
       1 57 110 ; 
       2 57 110 ; 
       3 57 110 ; 
       4 57 110 ; 
       5 57 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       9 3 110 ; 
       10 3 110 ; 
       11 0 110 ; 
       12 0 110 ; 
       13 0 110 ; 
       14 0 110 ; 
       15 0 110 ; 
       16 1 110 ; 
       17 1 110 ; 
       18 1 110 ; 
       19 1 110 ; 
       20 1 110 ; 
       21 2 110 ; 
       22 57 110 ; 
       23 3 110 ; 
       24 2 110 ; 
       25 3 110 ; 
       26 3 110 ; 
       27 2 110 ; 
       28 3 110 ; 
       29 2 110 ; 
       30 3 110 ; 
       31 2 110 ; 
       32 3 110 ; 
       33 2 110 ; 
       34 22 110 ; 
       35 22 110 ; 
       36 22 110 ; 
       37 22 110 ; 
       38 22 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 2 110 ; 
       44 22 110 ; 
       45 22 110 ; 
       46 22 110 ; 
       47 22 110 ; 
       48 22 110 ; 
       49 22 110 ; 
       50 22 110 ; 
       51 22 110 ; 
       52 22 110 ; 
       53 22 110 ; 
       54 22 110 ; 
       55 22 110 ; 
       56 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 3 300 ; 
       2 4 300 ; 
       2 5 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       4 6 300 ; 
       4 16 300 ; 
       4 7 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       5 11 300 ; 
       5 17 300 ; 
       5 12 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       23 35 300 ; 
       24 31 300 ; 
       25 40 300 ; 
       26 49 300 ; 
       27 33 300 ; 
       28 50 300 ; 
       29 28 300 ; 
       30 51 300 ; 
       31 29 300 ; 
       32 18 300 ; 
       33 30 300 ; 
       34 22 300 ; 
       35 21 300 ; 
       36 20 300 ; 
       37 19 300 ; 
       38 23 300 ; 
       39 24 300 ; 
       40 25 300 ; 
       41 26 300 ; 
       42 27 300 ; 
       43 32 300 ; 
       44 34 300 ; 
       45 36 300 ; 
       46 37 300 ; 
       47 38 300 ; 
       48 39 300 ; 
       49 41 300 ; 
       50 42 300 ; 
       51 43 300 ; 
       52 44 300 ; 
       53 45 300 ; 
       54 46 300 ; 
       55 47 300 ; 
       56 48 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 10 401 ; 
       1 0 401 ; 
       2 1 401 ; 
       3 17 401 ; 
       4 2 401 ; 
       5 3 401 ; 
       6 9 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 11 401 ; 
       12 13 401 ; 
       13 14 401 ; 
       14 15 401 ; 
       15 16 401 ; 
       16 4 401 ; 
       17 12 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 96.15518 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 108.6552 0 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 74.15518 -8.758616 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 49.15518 -8.758616 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 64.15518 -8.758616 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 84.15518 -8.758616 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 51.65518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 56.65518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 54.15518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 61.65518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 59.15518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 91.15518 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 96.15518 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 93.65518 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 101.1552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 98.65518 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 103.6552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 108.6552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 106.1552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 113.6552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 111.1552 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 81.65518 -10.75862 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 7.905186 -8.758616 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 41.65519 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 71.65518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 36.65518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 39.15518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 69.15518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 49.15518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 79.15518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 44.15519 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 74.15518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 46.65519 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 76.65518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM -18.34482 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM -15.84482 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM -13.34482 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM -10.84482 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 4.155185 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 1.655185 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM -0.8448152 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM -3.344815 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 11.65519 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 66.65518 -10.75862 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 6.655186 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 9.155186 -10.75862 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM -8.344814 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM -5.844814 -10.75862 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 14.15519 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 16.65518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 19.15518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 21.65518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 24.15518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 26.65518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 29.15518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 31.65518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 34.15518 -10.75862 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 32.90518 -6.758617 0 USR DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 -4.81501e-007 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 84 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 101.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 85.25 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 107.75 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 84 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 101.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 66.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 96.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 91.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 94 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 89 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 84 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 86.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 61.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 56.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 51.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 59 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 64 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 101.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 101.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 84 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 84 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 84 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 84 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 84 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 85.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 84 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 107.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 101.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
