SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard_ss102-cam_int1.7-0 ROOT ; 
       shipyard_ss102-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_50.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_58.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_62.1-0 ; 
       shipyard_ss102-white_strobe1_63.1-0 ; 
       shipyard_ss102-white_strobe1_64.1-0 ; 
       shipyard_ss102-white_strobe1_65.1-0 ; 
       shipyard_ss102-white_strobe1_66.1-0 ; 
       shipyard_ss102-white_strobe1_67.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bound03.1-0 ; 
       bounding_model-bound04.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       shipyard_ss102-east_bay_11_1.4-0 ; 
       shipyard_ss102-east_bay_11_11.1-0 ; 
       shipyard_ss102-east_bay_11_12.1-0 ; 
       shipyard_ss102-east_bay_11_8.1-0 ; 
       shipyard_ss102-extru1.2-0 ; 
       shipyard_ss102-extru5.1-0 ; 
       shipyard_ss102-garage1A.1-0 ; 
       shipyard_ss102-garage1B.1-0 ; 
       shipyard_ss102-garage1C.1-0 ; 
       shipyard_ss102-garage1D.1-0 ; 
       shipyard_ss102-garage1E.1-0 ; 
       shipyard_ss102-landing_lights_2.1-0 ; 
       shipyard_ss102-landing_lights2.1-0 ; 
       shipyard_ss102-landing_lights2_4.1-0 ; 
       shipyard_ss102-landing_lights3.1-0 ; 
       shipyard_ss102-launch3.1-0 ; 
       shipyard_ss102-null23.5-0 ROOT ; 
       shipyard_ss102-null26.1-0 ROOT ; 
       shipyard_ss102-SS_11_1.1-0 ; 
       shipyard_ss102-SS_11_4.1-0 ; 
       shipyard_ss102-SS_13_2.1-0 ; 
       shipyard_ss102-SS_15_1.1-0 ; 
       shipyard_ss102-SS_15_4.1-0 ; 
       shipyard_ss102-SS_23_2.1-0 ; 
       shipyard_ss102-SS_23_4.1-0 ; 
       shipyard_ss102-SS_24_1.1-0 ; 
       shipyard_ss102-SS_24_4.1-0 ; 
       shipyard_ss102-SS_26.1-0 ; 
       shipyard_ss102-SS_26_4.1-0 ; 
       shipyard_ss102-SS_55.2-0 ; 
       shipyard_ss102-SS_58.1-0 ; 
       shipyard_ss102-SS_70.1-0 ; 
       shipyard_ss102-SS_71.1-0 ; 
       shipyard_ss102-SS_72.1-0 ; 
       shipyard_ss102-SS_73.1-0 ; 
       shipyard_ss102-SS_74.1-0 ; 
       shipyard_ss102-SS_75.1-0 ; 
       shipyard_ss102-SS_76.1-0 ; 
       shipyard_ss102-SS_77.1-0 ; 
       shipyard_ss102-SS_78.1-0 ; 
       shipyard_ss102-strobe_set_1.1-0 ; 
       shipyard_ss102-strobe_set_4.1-0 ; 
       ss100_nulls-null18.3-0 ROOT ; 
       ss100_nulls-null19.3-0 ROOT ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-SS_60.1-0 ; 
       ss100_nulls-SS_61.1-0 ; 
       ss100_nulls-SS_68.1-0 ; 
       ss100_nulls-SS_69.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss102/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-shipyard-ss102.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       shipyard_ss102-t2d49.1-0 ; 
       shipyard_ss102-t2d50.1-0 ; 
       shipyard_ss102-t2d51.1-0 ; 
       shipyard_ss102-t2d58.1-0 ; 
       shipyard_ss102-t2d59.1-0 ; 
       shipyard_ss102-t2d60.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       5 8 110 ; 
       6 10 110 ; 
       7 6 110 ; 
       8 9 110 ; 
       9 21 110 ; 
       10 21 110 ; 
       11 5 110 ; 
       12 5 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 45 110 ; 
       17 45 110 ; 
       18 46 110 ; 
       19 46 110 ; 
       20 7 110 ; 
       23 16 110 ; 
       24 19 110 ; 
       25 16 110 ; 
       26 16 110 ; 
       27 19 110 ; 
       28 17 110 ; 
       29 18 110 ; 
       30 17 110 ; 
       31 18 110 ; 
       32 17 110 ; 
       33 18 110 ; 
       35 19 110 ; 
       45 5 110 ; 
       46 7 110 ; 
       49 47 110 ; 
       50 47 110 ; 
       51 47 110 ; 
       52 47 110 ; 
       53 48 110 ; 
       54 48 110 ; 
       55 48 110 ; 
       56 48 110 ; 
       34 22 110 ; 
       0 4 110 ; 
       1 4 110 ; 
       2 4 110 ; 
       3 4 110 ; 
       57 48 110 ; 
       58 48 110 ; 
       59 47 110 ; 
       60 47 110 ; 
       36 22 110 ; 
       37 22 110 ; 
       38 22 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       42 22 110 ; 
       43 22 110 ; 
       44 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       5 4 300 ; 
       5 0 300 ; 
       5 2 300 ; 
       5 5 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       7 12 300 ; 
       7 1 300 ; 
       7 3 300 ; 
       7 13 300 ; 
       8 6 300 ; 
       8 7 300 ; 
       8 8 300 ; 
       23 31 300 ; 
       24 27 300 ; 
       25 36 300 ; 
       26 45 300 ; 
       27 29 300 ; 
       28 46 300 ; 
       29 24 300 ; 
       30 47 300 ; 
       31 25 300 ; 
       32 14 300 ; 
       33 26 300 ; 
       35 28 300 ; 
       49 18 300 ; 
       50 17 300 ; 
       51 16 300 ; 
       52 15 300 ; 
       53 19 300 ; 
       54 20 300 ; 
       55 21 300 ; 
       56 22 300 ; 
       34 23 300 ; 
       57 30 300 ; 
       58 32 300 ; 
       59 33 300 ; 
       60 34 300 ; 
       36 35 300 ; 
       37 37 300 ; 
       38 38 300 ; 
       39 39 300 ; 
       40 40 300 ; 
       41 41 300 ; 
       42 42 300 ; 
       43 43 300 ; 
       44 44 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -228.4653 -8.113341 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -228.4653 -10.11334 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       5 SCHEM -195.93 39.8421 0 MPRFLG 0 ; 
       6 SCHEM -165.5623 41.86878 0 USR MPRFLG 0 ; 
       7 SCHEM -165.5623 39.86878 0 MPRFLG 0 ; 
       8 SCHEM -195.93 41.8421 0 USR MPRFLG 0 ; 
       9 SCHEM -190.2488 45.69612 0 USR MPRFLG 0 ; 
       10 SCHEM -176.864 46.23764 0 USR MPRFLG 0 ; 
       11 SCHEM -194.68 37.8421 0 WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM -187.18 37.8421 0 WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM -192.18 37.8421 0 WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM -182.18 37.8421 0 WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM -184.68 37.8421 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM -207.18 35.8421 0 MPRFLG 0 ; 
       17 SCHEM -199.68 35.8421 0 MPRFLG 0 ; 
       18 SCHEM -164.3123 35.86879 0 MPRFLG 0 ; 
       19 SCHEM -171.8123 35.86879 0 MPRFLG 0 ; 
       20 SCHEM -156.8123 37.86878 0 WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM -183.0355 51.54543 0 USR SRT 1 1 1 0 0 0 -4.81501e-007 0 0 MPRFLG 0 ; 
       23 SCHEM -204.68 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM -169.3123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM -209.68 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM -207.18 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM -171.8123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM -197.18 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM -161.8123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM -202.18 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM -166.8123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM -199.68 33.8421 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM -164.3123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM -174.3123 33.86879 0 WIRECOL 3 7 MPRFLG 0 ; 
       45 SCHEM -203.43 37.8421 0 MPRFLG 0 ; 
       46 SCHEM -168.0623 37.86878 0 MPRFLG 0 ; 
       47 SCHEM -222.0009 41.41203 0 USR SRT 1 1 1 0 0 0 3.50275e-028 31.39535 0 MPRFLG 0 ; 
       48 SCHEM -218.9353 37.49171 0 USR SRT 1 1 1 0 0 0 -3.517007e-014 -9.809725 1.062176e-007 MPRFLG 0 ; 
       49 SCHEM -225.7509 39.41202 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM -223.2509 39.41202 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM -220.7509 39.41202 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       52 SCHEM -218.2509 39.41202 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       53 SCHEM -217.6853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM -220.1853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM -222.6853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       56 SCHEM -225.1853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       34 SCHEM -228.545 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM -170.8752 52.82001 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -168.3752 52.82001 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM -165.8752 52.82001 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM -163.3752 52.82001 0 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM -167.1252 54.82001 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       57 SCHEM -215.1853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       58 SCHEM -212.6853 35.49171 0 WIRECOL 4 7 MPRFLG 0 ; 
       59 SCHEM -215.6759 39.40526 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM -213.0959 39.43797 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM -226.045 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -223.545 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM -221.045 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM -218.545 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM -217.295 44.71587 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM -216.045 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM -213.545 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM -211.045 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM -208.545 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM -206.045 42.71587 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 507.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 691.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 510.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 693.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 505.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 512.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 515.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 517.8479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 520.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 698.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 701.4046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 703.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 688.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 696.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 485.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 395.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 397.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 400.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 402.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 73.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 78.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 80.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM -38.81696 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 681.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 676.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 678.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 673.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 668.9047 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 671.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 480.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 475.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 477.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 487.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 482.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 106.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 428.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 433.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -20.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -15.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM -10.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM -5.193527 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -26.15214 2.160294 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4.806473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14.80647 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 515.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 517.8479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 520.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 698.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 701.4046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 703.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
