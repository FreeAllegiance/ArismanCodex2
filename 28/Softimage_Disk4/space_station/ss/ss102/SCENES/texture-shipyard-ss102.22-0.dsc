SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard_ss102-cam_int1.30-0 ROOT ; 
       shipyard_ss102-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-mat77.5-0 ; 
       shipyard_ss102-mat78.5-0 ; 
       shipyard_ss102-mat79.3-0 ; 
       shipyard_ss102-mat80.2-0 ; 
       shipyard_ss102-mat81.1-0 ; 
       shipyard_ss102-mat82.3-0 ; 
       shipyard_ss102-mat83.3-0 ; 
       shipyard_ss102-mat84.3-0 ; 
       shipyard_ss102-mat85.3-0 ; 
       shipyard_ss102-mat86.3-0 ; 
       shipyard_ss102-outside1.5-0 ; 
       shipyard_ss102-outside2.3-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_50.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_58.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_62.1-0 ; 
       shipyard_ss102-white_strobe1_63.1-0 ; 
       shipyard_ss102-white_strobe1_64.1-0 ; 
       shipyard_ss102-white_strobe1_65.1-0 ; 
       shipyard_ss102-white_strobe1_66.1-0 ; 
       shipyard_ss102-white_strobe1_67.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 56     
       shipyard_ss102-east_bay_11_1.4-0 ; 
       shipyard_ss102-east_bay_11_11.1-0 ; 
       shipyard_ss102-east_bay_11_12.1-0 ; 
       shipyard_ss102-east_bay_11_8.1-0 ; 
       shipyard_ss102-extru1.2-0 ; 
       shipyard_ss102-extru8.1-0 ; 
       shipyard_ss102-garage1A.1-0 ; 
       shipyard_ss102-garage1B.1-0 ; 
       shipyard_ss102-garage1C.1-0 ; 
       shipyard_ss102-garage1D.1-0 ; 
       shipyard_ss102-garage1E.1-0 ; 
       shipyard_ss102-landing_lights_2.1-0 ; 
       shipyard_ss102-landing_lights2.1-0 ; 
       shipyard_ss102-landing_lights2_4.1-0 ; 
       shipyard_ss102-landing_lights3.1-0 ; 
       shipyard_ss102-launch3.1-0 ; 
       shipyard_ss102-null18.3-0 ; 
       shipyard_ss102-null19.3-0 ; 
       shipyard_ss102-null23.23-0 ROOT ; 
       shipyard_ss102-null26.1-0 ; 
       shipyard_ss102-SS_11_1.1-0 ; 
       shipyard_ss102-SS_11_4.1-0 ; 
       shipyard_ss102-SS_13_2.1-0 ; 
       shipyard_ss102-SS_15_1.1-0 ; 
       shipyard_ss102-SS_15_4.1-0 ; 
       shipyard_ss102-SS_23_2.1-0 ; 
       shipyard_ss102-SS_23_4.1-0 ; 
       shipyard_ss102-SS_24_1.1-0 ; 
       shipyard_ss102-SS_24_4.1-0 ; 
       shipyard_ss102-SS_26.1-0 ; 
       shipyard_ss102-SS_26_4.1-0 ; 
       shipyard_ss102-SS_29.1-0 ; 
       shipyard_ss102-SS_30.1-0 ; 
       shipyard_ss102-SS_31.1-0 ; 
       shipyard_ss102-SS_32.1-0 ; 
       shipyard_ss102-SS_33.1-0 ; 
       shipyard_ss102-SS_34.1-0 ; 
       shipyard_ss102-SS_35.1-0 ; 
       shipyard_ss102-SS_36.1-0 ; 
       shipyard_ss102-SS_55.2-0 ; 
       shipyard_ss102-SS_58.1-0 ; 
       shipyard_ss102-SS_60.1-0 ; 
       shipyard_ss102-SS_61.1-0 ; 
       shipyard_ss102-SS_68.1-0 ; 
       shipyard_ss102-SS_69.1-0 ; 
       shipyard_ss102-SS_70.1-0 ; 
       shipyard_ss102-SS_71.1-0 ; 
       shipyard_ss102-SS_72.1-0 ; 
       shipyard_ss102-SS_73.1-0 ; 
       shipyard_ss102-SS_74.1-0 ; 
       shipyard_ss102-SS_75.1-0 ; 
       shipyard_ss102-SS_76.1-0 ; 
       shipyard_ss102-SS_77.1-0 ; 
       shipyard_ss102-SS_78.1-0 ; 
       shipyard_ss102-strobe_set_1.1-0 ; 
       shipyard_ss102-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss102/PICTURES/ss102 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       texture-shipyard-ss102.22-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 18     
       shipyard_ss102-t2d50.5-0 ; 
       shipyard_ss102-t2d51.5-0 ; 
       shipyard_ss102-t2d59.6-0 ; 
       shipyard_ss102-t2d60.6-0 ; 
       shipyard_ss102-t2d61.12-0 ; 
       shipyard_ss102-t2d62.12-0 ; 
       shipyard_ss102-t2d63.10-0 ; 
       shipyard_ss102-t2d64.8-0 ; 
       shipyard_ss102-t2d65.7-0 ; 
       shipyard_ss102-t2d66.7-0 ; 
       shipyard_ss102-t2d73.1-0 ; 
       shipyard_ss102-t2d74.1-0 ; 
       shipyard_ss102-t2d75.1-0 ; 
       shipyard_ss102-t2d76.1-0 ; 
       shipyard_ss102-t2d77.1-0 ; 
       shipyard_ss102-t2d78.1-0 ; 
       shipyard_ss102-t2d79.1-0 ; 
       shipyard_ss102-t2d80.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 5 110 ; 
       2 1 110 ; 
       3 4 110 ; 
       4 18 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 54 110 ; 
       12 54 110 ; 
       13 55 110 ; 
       14 55 110 ; 
       15 2 110 ; 
       16 18 110 ; 
       17 18 110 ; 
       19 18 110 ; 
       20 11 110 ; 
       21 14 110 ; 
       22 11 110 ; 
       23 11 110 ; 
       24 14 110 ; 
       25 12 110 ; 
       26 13 110 ; 
       27 12 110 ; 
       28 13 110 ; 
       29 12 110 ; 
       30 13 110 ; 
       31 16 110 ; 
       32 16 110 ; 
       33 16 110 ; 
       34 16 110 ; 
       35 17 110 ; 
       36 17 110 ; 
       37 17 110 ; 
       38 17 110 ; 
       39 19 110 ; 
       40 14 110 ; 
       41 17 110 ; 
       42 17 110 ; 
       43 16 110 ; 
       44 16 110 ; 
       45 19 110 ; 
       46 19 110 ; 
       47 19 110 ; 
       48 19 110 ; 
       49 19 110 ; 
       50 19 110 ; 
       51 19 110 ; 
       52 19 110 ; 
       53 19 110 ; 
       54 0 110 ; 
       55 2 110 ; 
       5 18 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 0 300 ; 
       0 2 300 ; 
       0 5 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       2 1 300 ; 
       2 3 300 ; 
       2 13 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       4 14 300 ; 
       4 24 300 ; 
       4 15 300 ; 
       4 16 300 ; 
       4 17 300 ; 
       4 18 300 ; 
       20 43 300 ; 
       21 39 300 ; 
       22 48 300 ; 
       23 57 300 ; 
       24 41 300 ; 
       25 58 300 ; 
       26 36 300 ; 
       27 59 300 ; 
       28 37 300 ; 
       29 26 300 ; 
       30 38 300 ; 
       31 30 300 ; 
       32 29 300 ; 
       33 28 300 ; 
       34 27 300 ; 
       35 31 300 ; 
       36 32 300 ; 
       37 33 300 ; 
       38 34 300 ; 
       39 35 300 ; 
       40 40 300 ; 
       41 42 300 ; 
       42 44 300 ; 
       43 45 300 ; 
       44 46 300 ; 
       45 47 300 ; 
       46 49 300 ; 
       47 50 300 ; 
       48 51 300 ; 
       49 52 300 ; 
       50 53 300 ; 
       51 54 300 ; 
       52 55 300 ; 
       53 56 300 ; 
       5 19 300 ; 
       5 25 300 ; 
       5 20 300 ; 
       5 21 300 ; 
       5 22 300 ; 
       5 23 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 10 401 ; 
       7 0 401 ; 
       8 1 401 ; 
       9 17 401 ; 
       10 2 401 ; 
       11 3 401 ; 
       14 9 401 ; 
       15 5 401 ; 
       16 6 401 ; 
       17 7 401 ; 
       18 8 401 ; 
       19 11 401 ; 
       25 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       24 4 401 ; 
       23 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 75 -6 0 MPRFLG 0 ; 
       1 SCHEM 133.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 130 -6 0 MPRFLG 0 ; 
       3 SCHEM 78.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       6 SCHEM 72.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 77.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 75 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 82.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 80 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 60 -10 0 MPRFLG 0 ; 
       12 SCHEM 67.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 127.5 -10 0 MPRFLG 0 ; 
       14 SCHEM 120 -10 0 MPRFLG 0 ; 
       15 SCHEM 132.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       17 SCHEM 23.75 -2 0 MPRFLG 0 ; 
       18 SCHEM 83.75 0 0 SRT 1 1 1 0 0 0 -4.81501e-007 0 0 MPRFLG 0 ; 
       19 SCHEM 43.75 -2 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 122.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 57.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 60 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 120 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 70 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 130 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 65 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 125 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 67.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 127.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 2.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       32 SCHEM 5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 7.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 10 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 25 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       36 SCHEM 22.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 20 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 17.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 32.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 117.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 27.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 30 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 12.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 15 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 35 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 37.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 40 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 42.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 45 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 63.75 -8 0 MPRFLG 0 ; 
       55 SCHEM 123.75 -8 0 MPRFLG 0 ; 
       5 SCHEM 141.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 135 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 150 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 147.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 101.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 106.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 109 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 111.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 114 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 156.5 -4 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 159 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 161.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 164 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 166.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 104 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 169 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 130 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 125 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 127.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 122.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 117.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 120 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 62.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 57.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 60 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 70 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 65 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 95 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 97.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 147.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 104 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 106.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 109 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 111.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 114 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 101.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 156.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 159 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 161.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 164 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 166.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 169 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 100 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 150 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
