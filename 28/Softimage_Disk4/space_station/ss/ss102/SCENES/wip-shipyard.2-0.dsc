SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       shipyard-cam_int1.2-0 ROOT ; 
       shipyard-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 4     
       shipyard-extru1.2-0 ROOT ; 
       shipyard-extru2.1-0 ROOT ; 
       shipyard-spline1.1-0 ROOT ; 
       shipyard-spline2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-shipyard.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 5 0 0 DISPLAY 0 0 SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 7.5 0 0 DISPLAY 1 2 SRT 0.14 0.14 0.14 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 10 0 0 DISPLAY 0 0 SRT 0.14 0.14 0.14 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
