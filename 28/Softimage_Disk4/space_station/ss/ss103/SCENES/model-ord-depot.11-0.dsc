SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.39-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 104     
       ordinance-bay_top1_2.3-0 ; 
       ordinance-bay_top1_4.3-0 ; 
       ordinance-bay_top1_5.3-0 ; 
       ordinance-bay_top1_7.3-0 ; 
       ordinance-bay_top1_8.3-0 ; 
       ordinance-bay_top1_9.3-0 ; 
       ordinance-inside_bay1_2.3-0 ; 
       ordinance-inside_bay1_4.3-0 ; 
       ordinance-inside_bay1_5.3-0 ; 
       ordinance-inside_bay1_7.3-0 ; 
       ordinance-inside_bay1_8.3-0 ; 
       ordinance-inside_bay1_9.3-0 ; 
       ordinance-mat10.3-0 ; 
       ordinance-mat13.3-0 ; 
       ordinance-mat14.3-0 ; 
       ordinance-mat15.3-0 ; 
       ordinance-mat16.3-0 ; 
       ordinance-mat17.3-0 ; 
       ordinance-mat18.3-0 ; 
       ordinance-mat3.3-0 ; 
       ordinance-mat4.3-0 ; 
       ordinance-mat7.3-0 ; 
       ordinance-mat8.3-0 ; 
       ordinance-mat9.3-0 ; 
       ordinance-white_strobe1_10.3-0 ; 
       ordinance-white_strobe1_11.3-0 ; 
       ordinance-white_strobe1_12.3-0 ; 
       ordinance-white_strobe1_13.3-0 ; 
       ordinance-white_strobe1_14.3-0 ; 
       ordinance-white_strobe1_15.3-0 ; 
       ordinance-white_strobe1_16.3-0 ; 
       ordinance-white_strobe1_17.3-0 ; 
       ordinance-white_strobe1_18.3-0 ; 
       ordinance-white_strobe1_25.4-0 ; 
       ordinance-white_strobe1_26.3-0 ; 
       ordinance-white_strobe1_27.3-0 ; 
       ordinance-white_strobe1_28.3-0 ; 
       ordinance-white_strobe1_30.3-0 ; 
       ordinance-white_strobe1_31.3-0 ; 
       ordinance-white_strobe1_32.3-0 ; 
       ordinance-white_strobe1_33.3-0 ; 
       ordinance-white_strobe1_34.3-0 ; 
       ordinance-white_strobe1_35.3-0 ; 
       ordinance-white_strobe1_41.3-0 ; 
       ordinance-white_strobe1_42.3-0 ; 
       ordinance-white_strobe1_43.3-0 ; 
       ordinance-white_strobe1_44.3-0 ; 
       ordinance-white_strobe1_45.3-0 ; 
       ordinance-white_strobe1_46.3-0 ; 
       ordinance-white_strobe1_7.3-0 ; 
       ordinance-white_strobe1_8.3-0 ; 
       ordinance-white_strobe1_9.3-0 ; 
       ord_depot-mat100.1-0 ; 
       ord_depot-mat101.1-0 ; 
       ord_depot-mat102.1-0 ; 
       ord_depot-mat103.1-0 ; 
       ord_depot-mat104.1-0 ; 
       ord_depot-mat105.1-0 ; 
       ord_depot-mat106.1-0 ; 
       ord_depot-mat107.1-0 ; 
       ord_depot-mat108.1-0 ; 
       ord_depot-mat109.1-0 ; 
       ord_depot-mat110.1-0 ; 
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       ord_depot-mat39.1-0 ; 
       ord_depot-mat40.2-0 ; 
       ord_depot-mat41.2-0 ; 
       ord_depot-mat42.2-0 ; 
       ord_depot-mat44.1-0 ; 
       ord_depot-mat51.1-0 ; 
       ord_depot-mat52.1-0 ; 
       ord_depot-mat54.1-0 ; 
       ord_depot-mat55.1-0 ; 
       ord_depot-mat56.1-0 ; 
       ord_depot-mat57.1-0 ; 
       ord_depot-mat78.1-0 ; 
       ord_depot-mat79.1-0 ; 
       ord_depot-mat80.1-0 ; 
       ord_depot-mat81.1-0 ; 
       ord_depot-mat82.1-0 ; 
       ord_depot-mat83.1-0 ; 
       ord_depot-mat84.1-0 ; 
       ord_depot-mat85.1-0 ; 
       ord_depot-mat86.1-0 ; 
       ord_depot-mat87.1-0 ; 
       ord_depot-mat88.1-0 ; 
       ord_depot-mat89.1-0 ; 
       ord_depot-mat90.1-0 ; 
       ord_depot-mat91.1-0 ; 
       ord_depot-mat92.1-0 ; 
       ord_depot-mat93.1-0 ; 
       ord_depot-mat94.1-0 ; 
       ord_depot-mat95.1-0 ; 
       ord_depot-mat96.1-0 ; 
       ord_depot-mat97.1-0 ; 
       ord_depot-mat98.1-0 ; 
       ord_depot-mat99.1-0 ; 
       ord_depot-sq.1-0 ; 
       ord_depot-sq1.1-0 ; 
       ord_depot-sq2.1-0 ; 
       ord_depot-white_strobe1_47.2-0 ; 
       ord_depot-white_strobe1_48.2-0 ; 
       ord_depot-white_strobe1_49.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 96     
       ord-cube10.1-0 ; 
       ord-cube11.1-0 ; 
       ord-cube12.1-0 ; 
       ord-cube6.1-0 ; 
       ord-cube8.1-0 ; 
       ord-cube9.1-0 ; 
       ord-east_bay_11.3-0 ; 
       ord-east_bay_11_10.1-0 ; 
       ord-east_bay_11_3.3-0 ; 
       ord-east_bay_11_6.1-0 ; 
       ord-east_bay_11_8.1-0 ; 
       ord-east_bay_11_9.1-0 ; 
       ord-extru63.1-0 ; 
       ord-extru69_10.1-0 ; 
       ord-extru69_11.1-0 ; 
       ord-extru69_12.1-0 ; 
       ord-extru69_3.1-0 ; 
       ord-extru69_8.1-0 ; 
       ord-extru69_9.1-0 ; 
       ord-extru75.1-0 ; 
       ord-extru76.1-0 ; 
       ord-extru77.1-0 ; 
       ord-extru78.1-0 ; 
       ord-extru79.1-0 ; 
       ord-extru80.1-0 ; 
       ord-garage2A.1-0 ; 
       ord-garage2A1.1-0 ; 
       ord-garage2B.1-0 ; 
       ord-garage2B1.1-0 ; 
       ord-garage2C.1-0 ; 
       ord-garage2C1.1-0 ; 
       ord-garage2D.1-0 ; 
       ord-garage2D1.1-0 ; 
       ord-garage2E.1-0 ; 
       ord-garage2E1.1-0 ; 
       ord-landing_lights_1.1-0 ; 
       ord-landing_lights_3.1-0 ; 
       ord-landing_lights_4.1-0 ; 
       ord-landing_lights2_1.1-0 ; 
       ord-landing_lights2_3.1-0 ; 
       ord-landing_lights2_4.1-0 ; 
       ord-launch1.1-0 ; 
       ord-null10_1.4-0 ROOT ; 
       ord-null18.1-0 ; 
       ord-null18_1.6-0 ; 
       ord-null19.6-0 ; 
       ord-null32.6-0 ; 
       ord-null33.6-0 ; 
       ord-null34.6-0 ; 
       ord-null36.6-0 ; 
       ord-null39_1.1-0 ; 
       ord-null39_4.1-0 ; 
       ord-null39_5.1-0 ; 
       ord-null41.1-0 ; 
       ord-null42.1-0 ; 
       ord-null43.1-0 ; 
       ord-null44.1-0 ; 
       ord-null46.1-0 ; 
       ord-revol10.1-0 ; 
       ord-revol11.1-0 ; 
       ord-revol2.1-0 ; 
       ord-revol8.1-0 ; 
       ord-SS_01.1-0 ; 
       ord-SS_02.1-0 ; 
       ord-SS_03.1-0 ; 
       ord-SS_05.1-0 ; 
       ord-SS_20.1-0 ; 
       ord-SS_21.1-0 ; 
       ord-SS_22.1-0 ; 
       ord-SS_29.1-0 ; 
       ord-SS_30.1-0 ; 
       ord-SS_31.1-0 ; 
       ord-SS_33.1-0 ; 
       ord-SS_34.1-0 ; 
       ord-SS_35.1-0 ; 
       ord-SS_66.1-0 ; 
       ord-SS_67.1-0 ; 
       ord-SS_68.1-0 ; 
       ord-SS_69.1-0 ; 
       ord-SS_70.1-0 ; 
       ord-SS_71.1-0 ; 
       ord-SS_72.1-0 ; 
       ord-SS_73.1-0 ; 
       ord-SS_74.1-0 ; 
       ord-SS_75.1-0 ; 
       ord-SS_76.1-0 ; 
       ord-SS_77.1-0 ; 
       ord-SS_84.1-0 ; 
       ord-SS_85.1-0 ; 
       ord-SS_86.1-0 ; 
       ord-SS_87.1-0 ; 
       ord-SS_88.1-0 ; 
       ord-SS_89.1-0 ; 
       ord-strobe_set.1-0 ; 
       ord-strobe_set1.1-0 ; 
       ord-tetra4.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss103/PICTURES/ss103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-ord-depot.11-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 50     
       ord_depot-t2d13.1-0 ; 
       ord_depot-t2d14.1-0 ; 
       ord_depot-t2d15.1-0 ; 
       ord_depot-t2d16.1-0 ; 
       ord_depot-t2d17.1-0 ; 
       ord_depot-t2d18.1-0 ; 
       ord_depot-t2d3.2-0 ; 
       ord_depot-t2d32.7-0 ; 
       ord_depot-t2d33.7-0 ; 
       ord_depot-t2d35_1.1-0 ; 
       ord_depot-t2d36.1-0 ; 
       ord_depot-t2d37.1-0 ; 
       ord_depot-t2d38.1-0 ; 
       ord_depot-t2d39.1-0 ; 
       ord_depot-t2d4.2-0 ; 
       ord_depot-t2d40.1-0 ; 
       ord_depot-t2d41.1-0 ; 
       ord_depot-t2d42.1-0 ; 
       ord_depot-t2d43.1-0 ; 
       ord_depot-t2d44.1-0 ; 
       ord_depot-t2d45.1-0 ; 
       ord_depot-t2d46.1-0 ; 
       ord_depot-t2d47.1-0 ; 
       ord_depot-t2d48.1-0 ; 
       ord_depot-t2d49.1-0 ; 
       ord_depot-t2d5.2-0 ; 
       ord_depot-t2d50.1-0 ; 
       ord_depot-t2d51.1-0 ; 
       ord_depot-t2d52.1-0 ; 
       ord_depot-t2d53.1-0 ; 
       ord_depot-t2d54.1-0 ; 
       ord_depot-t2d55.1-0 ; 
       ord_depot-t2d56.1-0 ; 
       ord_depot-t2d57.1-0 ; 
       ord_depot-t2d58.1-0 ; 
       ord_depot-t2d59.1-0 ; 
       ord_depot-t2d60.1-0 ; 
       ord_depot-t2d61.1-0 ; 
       ord_depot-t2d62.1-0 ; 
       ord_depot-t2d63.1-0 ; 
       ord_depot-t2d64.1-0 ; 
       ord_depot-t2d65.1-0 ; 
       ord_depot-t2d66.1-0 ; 
       ord_depot-t2d67.1-0 ; 
       ord_depot-t2d68.1-0 ; 
       ord_depot-t2d69.1-0 ; 
       ord_depot-t2d7.2-0 ; 
       ord_depot-t2d70.1-0 ; 
       ord_depot-t2d71.1-0 ; 
       ord_depot-t2d72.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 55 110 ; 
       1 54 110 ; 
       2 54 110 ; 
       3 55 110 ; 
       4 54 110 ; 
       5 55 110 ; 
       6 7 110 ; 
       7 49 110 ; 
       8 9 110 ; 
       9 47 110 ; 
       10 48 110 ; 
       11 10 110 ; 
       12 60 110 ; 
       13 51 110 ; 
       14 52 110 ; 
       15 52 110 ; 
       16 50 110 ; 
       17 50 110 ; 
       18 51 110 ; 
       19 53 110 ; 
       20 19 110 ; 
       21 53 110 ; 
       22 21 110 ; 
       23 53 110 ; 
       24 23 110 ; 
       25 8 110 ; 
       26 11 110 ; 
       27 8 110 ; 
       28 11 110 ; 
       29 8 110 ; 
       30 11 110 ; 
       31 8 110 ; 
       32 11 110 ; 
       33 8 110 ; 
       34 11 110 ; 
       35 93 110 ; 
       36 6 110 ; 
       37 94 110 ; 
       38 93 110 ; 
       39 6 110 ; 
       40 94 110 ; 
       41 6 110 ; 
       43 42 110 ; 
       44 42 110 ; 
       45 42 110 ; 
       46 42 110 ; 
       47 42 110 ; 
       48 42 110 ; 
       49 42 110 ; 
       50 56 110 ; 
       51 56 110 ; 
       52 56 110 ; 
       53 42 110 ; 
       54 60 110 ; 
       55 60 110 ; 
       56 60 110 ; 
       57 60 110 ; 
       58 57 110 ; 
       59 57 110 ; 
       60 42 110 ; 
       61 57 110 ; 
       62 46 110 ; 
       63 46 110 ; 
       64 46 110 ; 
       65 46 110 ; 
       66 43 110 ; 
       67 43 110 ; 
       68 43 110 ; 
       69 44 110 ; 
       70 44 110 ; 
       71 44 110 ; 
       72 45 110 ; 
       73 45 110 ; 
       74 45 110 ; 
       75 35 110 ; 
       76 35 110 ; 
       77 35 110 ; 
       78 38 110 ; 
       79 38 110 ; 
       80 38 110 ; 
       81 36 110 ; 
       82 36 110 ; 
       83 36 110 ; 
       84 39 110 ; 
       85 39 110 ; 
       86 39 110 ; 
       87 37 110 ; 
       88 37 110 ; 
       89 37 110 ; 
       90 40 110 ; 
       91 40 110 ; 
       92 40 110 ; 
       93 8 110 ; 
       94 11 110 ; 
       95 60 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 55 300 ; 
       1 57 300 ; 
       2 58 300 ; 
       3 53 300 ; 
       4 65 300 ; 
       5 54 300 ; 
       6 23 300 ; 
       6 2 300 ; 
       6 8 300 ; 
       6 12 300 ; 
       7 17 300 ; 
       7 5 300 ; 
       7 11 300 ; 
       7 18 300 ; 
       8 21 300 ; 
       8 1 300 ; 
       8 7 300 ; 
       8 22 300 ; 
       9 19 300 ; 
       9 0 300 ; 
       9 6 300 ; 
       9 20 300 ; 
       10 13 300 ; 
       10 3 300 ; 
       10 9 300 ; 
       10 14 300 ; 
       11 15 300 ; 
       11 4 300 ; 
       11 10 300 ; 
       11 16 300 ; 
       12 56 300 ; 
       13 85 300 ; 
       13 86 300 ; 
       13 87 300 ; 
       13 88 300 ; 
       13 89 300 ; 
       14 100 300 ; 
       14 90 300 ; 
       14 91 300 ; 
       14 92 300 ; 
       14 93 300 ; 
       15 94 300 ; 
       15 95 300 ; 
       15 96 300 ; 
       15 97 300 ; 
       15 52 300 ; 
       16 98 300 ; 
       16 72 300 ; 
       16 73 300 ; 
       16 74 300 ; 
       16 75 300 ; 
       17 76 300 ; 
       17 77 300 ; 
       17 78 300 ; 
       17 79 300 ; 
       17 80 300 ; 
       18 99 300 ; 
       18 81 300 ; 
       18 82 300 ; 
       18 83 300 ; 
       18 84 300 ; 
       19 63 300 ; 
       20 64 300 ; 
       21 59 300 ; 
       22 60 300 ; 
       23 61 300 ; 
       24 62 300 ; 
       58 70 300 ; 
       59 71 300 ; 
       60 66 300 ; 
       60 67 300 ; 
       60 68 300 ; 
       61 69 300 ; 
       62 35 300 ; 
       63 34 300 ; 
       64 33 300 ; 
       65 36 300 ; 
       66 103 300 ; 
       67 102 300 ; 
       68 101 300 ; 
       69 39 300 ; 
       70 38 300 ; 
       71 37 300 ; 
       72 40 300 ; 
       73 41 300 ; 
       74 42 300 ; 
       75 50 300 ; 
       76 51 300 ; 
       77 49 300 ; 
       78 25 300 ; 
       79 26 300 ; 
       80 24 300 ; 
       81 31 300 ; 
       82 32 300 ; 
       83 30 300 ; 
       84 28 300 ; 
       85 29 300 ; 
       86 27 300 ; 
       87 46 300 ; 
       88 47 300 ; 
       89 48 300 ; 
       90 43 300 ; 
       91 44 300 ; 
       92 45 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 49 401 ; 
       52 36 401 ; 
       53 37 401 ; 
       54 38 401 ; 
       55 39 401 ; 
       56 40 401 ; 
       57 41 401 ; 
       58 42 401 ; 
       59 43 401 ; 
       60 44 401 ; 
       61 45 401 ; 
       62 47 401 ; 
       63 7 401 ; 
       64 8 401 ; 
       65 6 401 ; 
       66 48 401 ; 
       67 14 401 ; 
       68 25 401 ; 
       69 46 401 ; 
       70 0 401 ; 
       71 1 401 ; 
       72 2 401 ; 
       73 3 401 ; 
       74 4 401 ; 
       75 5 401 ; 
       76 10 401 ; 
       77 11 401 ; 
       78 12 401 ; 
       79 13 401 ; 
       80 15 401 ; 
       81 17 401 ; 
       82 18 401 ; 
       83 19 401 ; 
       84 20 401 ; 
       85 21 401 ; 
       86 22 401 ; 
       87 23 401 ; 
       88 24 401 ; 
       89 26 401 ; 
       90 28 401 ; 
       91 29 401 ; 
       92 30 401 ; 
       93 31 401 ; 
       94 32 401 ; 
       95 33 401 ; 
       96 34 401 ; 
       97 35 401 ; 
       98 9 401 ; 
       99 16 401 ; 
       100 27 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 155 -6 0 MPRFLG 0 ; 
       1 SCHEM 137.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 140 -6 0 MPRFLG 0 ; 
       3 SCHEM 150 -6 0 MPRFLG 0 ; 
       4 SCHEM 142.5 -6 0 MPRFLG 0 ; 
       5 SCHEM 152.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 30 -6 0 MPRFLG 0 ; 
       7 SCHEM 35 -4 0 MPRFLG 0 ; 
       8 SCHEM 182.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 187.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 245 -4 0 MPRFLG 0 ; 
       11 SCHEM 240 -6 0 MPRFLG 0 ; 
       12 SCHEM 145 -4 0 MPRFLG 0 ; 
       13 SCHEM 105 -8 0 MPRFLG 0 ; 
       14 SCHEM 117.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 130 -8 0 MPRFLG 0 ; 
       16 SCHEM 67.5 -8 0 MPRFLG 0 ; 
       17 SCHEM 80 -8 0 MPRFLG 0 ; 
       18 SCHEM 92.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       20 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       21 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       22 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       23 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       24 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       25 SCHEM 180 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 237.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 185 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 242.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 182.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 240 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 190 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 247.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 187.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 245 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 167.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 20 -8 0 MPRFLG 0 ; 
       37 SCHEM 225 -10 0 MPRFLG 0 ; 
       38 SCHEM 175 -10 0 MPRFLG 0 ; 
       39 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 232.5 -10 0 MPRFLG 0 ; 
       41 SCHEM 32.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 146.25 0 0 SRT 1 1 1 3.141593 0 0 0 0.5329684 0 MPRFLG 0 ; 
       43 SCHEM 287.5 -2 0 MPRFLG 0 ; 
       44 SCHEM 280 -2 0 MPRFLG 0 ; 
       45 SCHEM 272.5 -2 0 MPRFLG 0 ; 
       46 SCHEM 216.25 -2 0 MPRFLG 0 ; 
       47 SCHEM 187.5 -2 0 MPRFLG 0 ; 
       48 SCHEM 245 -2 0 MPRFLG 0 ; 
       49 SCHEM 35 -2 0 MPRFLG 0 ; 
       50 SCHEM 73.75 -6 0 MPRFLG 0 ; 
       51 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       52 SCHEM 123.75 -6 0 MPRFLG 0 ; 
       53 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       54 SCHEM 140 -4 0 MPRFLG 0 ; 
       55 SCHEM 152.5 -4 0 MPRFLG 0 ; 
       56 SCHEM 98.75 -4 0 MPRFLG 0 ; 
       57 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       58 SCHEM 57.5 -6 0 MPRFLG 0 ; 
       59 SCHEM 60 -6 0 MPRFLG 0 ; 
       60 SCHEM 108.75 -2 0 MPRFLG 0 ; 
       61 SCHEM 55 -6 0 MPRFLG 0 ; 
       62 SCHEM 212.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 215 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 217.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 220 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 285 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 287.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 290 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 277.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       70 SCHEM 280 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 282.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       72 SCHEM 275 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 272.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 270 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 165 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 167.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 170 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 172.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 175 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 177.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 17.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 20 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 22.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 25 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 27.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 30 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 227.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 222.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 225 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 235 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 230 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       92 SCHEM 232.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 171.25 -8 0 MPRFLG 0 ; 
       94 SCHEM 228.75 -8 0 MPRFLG 0 ; 
       95 SCHEM 147.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 202.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 192.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 260 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 250 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 205 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 195 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 262.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 252.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 267.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 265 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 257.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 255 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 210 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 207.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 200 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 197.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 177.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 172.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 175 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 30 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 25 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 27.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 17.5 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 20 -12 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 217.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 215 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 212.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 220 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 282.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 280 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 277.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 275 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 272.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 270 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 235 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 230 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 232.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 227.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 222.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 225 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 170 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 165 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 167.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 132.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 150 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 152.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 155 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 145 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 137.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 140 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 7.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 12.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       64 SCHEM 2.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 142.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       66 SCHEM 162.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 157.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 160 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 55 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 57.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 62.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 65 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 67.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 70 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 85 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       78 SCHEM 77.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       79 SCHEM 80 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       80 SCHEM 82.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       81 SCHEM 87.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       82 SCHEM 90 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       83 SCHEM 92.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       84 SCHEM 95 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       85 SCHEM 110 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       86 SCHEM 100 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       87 SCHEM 102.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       88 SCHEM 105 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       89 SCHEM 107.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       90 SCHEM 112.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       91 SCHEM 115 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       92 SCHEM 117.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       93 SCHEM 120 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       94 SCHEM 135 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       95 SCHEM 125 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       96 SCHEM 127.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       97 SCHEM 130 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       98 SCHEM 72.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       99 SCHEM 97.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       100 SCHEM 122.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       101 SCHEM 290 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       102 SCHEM 287.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       103 SCHEM 285 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 57.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 60 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 62.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 65 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 67.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 70 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 142.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 2.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 72.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 85 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 77.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 80 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 157.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 82.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 97.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 87.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 90 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 92.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 95 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 110 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 100 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 102.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 105 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 160 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 107.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 122.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 112.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 115 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 117.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 120 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 135 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 125 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 127.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 130 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 132.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 150 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 152.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 155 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 145 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 137.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 140 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 7.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 55 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 12.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 162.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 292.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
