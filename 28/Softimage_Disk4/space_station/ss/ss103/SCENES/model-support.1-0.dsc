SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       apc-cam_int1.1-0 ROOT ; 
       apc-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 13     
       apc-mat71.1-0 ; 
       apc-mat75.1-0 ; 
       apc-mat77.1-0 ; 
       apc-mat78.1-0 ; 
       apc-mat80.1-0 ; 
       edit_nulls-mat70.1-0 ; 
       recovery_fig24-mat100.1-0 ; 
       recovery_fig24-mat101.1-0 ; 
       recovery_fig24-mat102.1-0 ; 
       recovery_fig24-mat103.1-0 ; 
       recovery_fig24-mat104.1-0 ; 
       recovery_fig24-mat105.1-0 ; 
       recovery_fig24-mat99.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       TEST1-blthrust.2-0 ; 
       TEST1-brthrust.2-0 ; 
       TEST1-canopy_1_1.1-0 ROOT ; 
       TEST1-cockpt.2-0 ; 
       TEST1-cube3.1-0 ; 
       TEST1-cyl1.1-0 ; 
       TEST1-cyl2.1-0 ; 
       TEST1-engine-nib.1-0 ; 
       TEST1-extru2_1.1-0 ; 
       TEST1-l-t-engine4.1-0 ; 
       TEST1-l-t-engine6.1-0 ; 
       TEST1-lsmoke.2-0 ; 
       TEST1-lwepemt.2-0 ; 
       TEST1-missemt.2-0 ; 
       TEST1-null1.1-0 ; 
       TEST1-rsmoke.2-0 ; 
       TEST1-rt-wing4.1-0 ; 
       TEST1-rt-wing5.1-0 ; 
       TEST1-rwepemt.2-0 ; 
       TEST1-SS01.2-0 ; 
       TEST1-SS02.2-0 ; 
       TEST1-SS03.2-0 ; 
       TEST1-SS04.2-0 ; 
       TEST1-SS05.2-0 ; 
       TEST1-SS06.2-0 ; 
       TEST1-tlthrust.2-0 ; 
       TEST1-trail.2-0 ; 
       TEST1-trthrust.2-0 ; 
       TEST1-zz_base_1.9-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss103/PICTURES/fig24 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-support.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 7     
       recovery_fig24-back2.1-0 ; 
       recovery_fig24-Side2.1-0 ; 
       recovery_fig24-t2d16.1-0 ; 
       recovery_fig24-t2d17.1-0 ; 
       recovery_fig24-t2d18.1-0 ; 
       recovery_fig24-t2d19.1-0 ; 
       recovery_fig24-t2d20.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 2 110 ; 
       1 2 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 14 110 ; 
       6 14 110 ; 
       7 2 110 ; 
       8 2 110 ; 
       9 14 110 ; 
       10 14 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       28 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       19 5 300 ; 
       20 0 300 ; 
       28 12 300 ; 
       28 6 300 ; 
       28 7 300 ; 
       28 8 300 ; 
       28 9 300 ; 
       28 10 300 ; 
       28 11 300 ; 
       21 2 300 ; 
       22 1 300 ; 
       23 4 300 ; 
       24 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 0 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 28.44918 -14.50516 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 31.05741 -14.54123 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 31.3695 3.567802 0 USR DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0.3104329 0.5966939 MPRFLG 0 ; 
       3 SCHEM 25.85339 -13.76261 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 29.98589 -1.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 37.4859 -3.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 39.9859 -3.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 27.48589 -1.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 24.98588 -1.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 34.9859 -3.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 32.4859 -3.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 28.51384 -12.88182 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 20.27587 -13.85571 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 21.33377 -13.19791 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 36.2359 -1.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 31.04991 -12.84575 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 42.4859 -1.449623 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 45.06467 -1.406869 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 22.3985 -13.85432 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 33.44975 -12.86331 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 33.46704 -13.57654 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 44.77525 -5.548045 0 USR DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 35.70172 -12.84234 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 35.71901 -13.6185 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 37.99194 -12.80038 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 38.05858 -13.6185 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 28.4383 -13.78367 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 29.76553 -11.7996 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 31.06081 -13.71152 0 USR WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       6 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 7.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 10 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 12.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 103.9704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 108.9704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 111.4704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 116.4704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 113.9704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 106.4704 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 17.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
