SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       ord-cam_int1.70-0 ROOT ; 
       ord-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 60     
       ord_depot-mat100.2-0 ; 
       ord_depot-mat101.2-0 ; 
       ord_depot-mat102.2-0 ; 
       ord_depot-mat103.2-0 ; 
       ord_depot-mat104.2-0 ; 
       ord_depot-mat105.2-0 ; 
       ord_depot-mat106.2-0 ; 
       ord_depot-mat107.2-0 ; 
       ord_depot-mat108.2-0 ; 
       ord_depot-mat109.2-0 ; 
       ord_depot-mat110.2-0 ; 
       ord_depot-mat111.1-0 ; 
       ord_depot-mat33_1.1-0 ; 
       ord_depot-mat34_1.1-0 ; 
       ord_depot-mat39.2-0 ; 
       ord_depot-mat44.2-0 ; 
       ord_depot-mat51.2-0 ; 
       ord_depot-mat52.2-0 ; 
       ord_depot-mat54.2-0 ; 
       ord_depot-mat55.2-0 ; 
       ord_depot-mat55_1.2-0 ; 
       ord_depot-mat55_2.1-0 ; 
       ord_depot-mat55_3.1-0 ; 
       ord_depot-mat56.2-0 ; 
       ord_depot-mat56_1.2-0 ; 
       ord_depot-mat56_2.1-0 ; 
       ord_depot-mat56_3.1-0 ; 
       ord_depot-mat57.2-0 ; 
       ord_depot-mat57_1.2-0 ; 
       ord_depot-mat57_2.1-0 ; 
       ord_depot-mat57_3.1-0 ; 
       ord_depot-mat78.2-0 ; 
       ord_depot-mat79.2-0 ; 
       ord_depot-mat80.2-0 ; 
       ord_depot-mat81.2-0 ; 
       ord_depot-mat82.2-0 ; 
       ord_depot-mat83.2-0 ; 
       ord_depot-mat84.2-0 ; 
       ord_depot-mat85.2-0 ; 
       ord_depot-mat86.2-0 ; 
       ord_depot-mat87.2-0 ; 
       ord_depot-mat88.2-0 ; 
       ord_depot-mat89.2-0 ; 
       ord_depot-mat90.2-0 ; 
       ord_depot-mat91.2-0 ; 
       ord_depot-mat92.2-0 ; 
       ord_depot-mat93.2-0 ; 
       ord_depot-mat94.2-0 ; 
       ord_depot-mat95.2-0 ; 
       ord_depot-mat96.2-0 ; 
       ord_depot-mat97.2-0 ; 
       ord_depot-mat98.2-0 ; 
       ord_depot-mat99.2-0 ; 
       ord_depot-sq.2-0 ; 
       ord_depot-sq1.2-0 ; 
       ord_depot-sq2.2-0 ; 
       ss103_orddepot-mat121.2-0 ; 
       ss103_orddepot-mat133.2-0 ; 
       ss103_orddepot-mat134.2-0 ; 
       ss103_orddepot-mat135.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       ss103-cube10.1-0 ; 
       ss103-cube11.1-0 ; 
       ss103-cube12.1-0 ; 
       ss103-cube6.1-0 ; 
       ss103-cube8.1-0 ; 
       ss103-cube9.1-0 ; 
       ss103-east_bay_11_6_1.1-0 ; 
       ss103-extru63.1-0 ; 
       ss103-extru69_10.1-0 ; 
       ss103-extru69_11.1-0 ; 
       ss103-extru69_12.1-0 ; 
       ss103-extru69_3.1-0 ; 
       ss103-extru69_8.1-0 ; 
       ss103-extru69_9.1-0 ; 
       ss103-extru75.1-0 ; 
       ss103-extru76.1-0 ; 
       ss103-extru77.1-0 ; 
       ss103-extru78.1-0 ; 
       ss103-extru79.1-0 ; 
       ss103-extru80.1-0 ; 
       ss103-landing_bay_1.1-0 ; 
       ss103-landing_bay_2.1-0 ; 
       ss103-newrevolv.1-0 ; 
       ss103-null44.1-0 ; 
       ss103-revol10.1-0 ; 
       ss103-revol11.1-0 ; 
       ss103-revol8.1-0 ; 
       ss103-ss103_1.10-0 ROOT ; 
       ss103-tetra4.3-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss103/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss103/PICTURES/ss103 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss103-STATIC.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 59     
       ord_depot-t2d13.5-0 ; 
       ord_depot-t2d14.5-0 ; 
       ord_depot-t2d15.5-0 ; 
       ord_depot-t2d16.5-0 ; 
       ord_depot-t2d17.5-0 ; 
       ord_depot-t2d18.5-0 ; 
       ord_depot-t2d3.6-0 ; 
       ord_depot-t2d32_1.3-0 ; 
       ord_depot-t2d33_1.3-0 ; 
       ord_depot-t2d35_1.5-0 ; 
       ord_depot-t2d36.5-0 ; 
       ord_depot-t2d37.5-0 ; 
       ord_depot-t2d38.5-0 ; 
       ord_depot-t2d39.5-0 ; 
       ord_depot-t2d40.5-0 ; 
       ord_depot-t2d41.5-0 ; 
       ord_depot-t2d42.5-0 ; 
       ord_depot-t2d43.5-0 ; 
       ord_depot-t2d44.5-0 ; 
       ord_depot-t2d45.5-0 ; 
       ord_depot-t2d46.5-0 ; 
       ord_depot-t2d46_1.6-0 ; 
       ord_depot-t2d46_2.5-0 ; 
       ord_depot-t2d46_3.5-0 ; 
       ord_depot-t2d47.5-0 ; 
       ord_depot-t2d47_1.8-0 ; 
       ord_depot-t2d47_2.7-0 ; 
       ord_depot-t2d47_3.7-0 ; 
       ord_depot-t2d48.5-0 ; 
       ord_depot-t2d48_1.8-0 ; 
       ord_depot-t2d48_2.7-0 ; 
       ord_depot-t2d48_3.7-0 ; 
       ord_depot-t2d49.5-0 ; 
       ord_depot-t2d50.5-0 ; 
       ord_depot-t2d51.5-0 ; 
       ord_depot-t2d52.5-0 ; 
       ord_depot-t2d53.5-0 ; 
       ord_depot-t2d54.5-0 ; 
       ord_depot-t2d55.5-0 ; 
       ord_depot-t2d56.5-0 ; 
       ord_depot-t2d57.5-0 ; 
       ord_depot-t2d58.5-0 ; 
       ord_depot-t2d59.5-0 ; 
       ord_depot-t2d60.5-0 ; 
       ord_depot-t2d61.5-0 ; 
       ord_depot-t2d62.5-0 ; 
       ord_depot-t2d63.5-0 ; 
       ord_depot-t2d64.5-0 ; 
       ord_depot-t2d65.5-0 ; 
       ord_depot-t2d66.5-0 ; 
       ord_depot-t2d67.5-0 ; 
       ord_depot-t2d68.5-0 ; 
       ord_depot-t2d69.5-0 ; 
       ord_depot-t2d7.6-0 ; 
       ord_depot-t2d70.5-0 ; 
       ord_depot-t2d72.5-0 ; 
       ss103_orddepot-t2d80.7-0 ; 
       ss103_orddepot-t2d81.7-0 ; 
       STATIC-t2d82.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 22 110 ; 
       1 22 110 ; 
       2 22 110 ; 
       3 22 110 ; 
       4 22 110 ; 
       5 22 110 ; 
       6 22 110 ; 
       7 22 110 ; 
       8 22 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 22 110 ; 
       14 22 110 ; 
       15 14 110 ; 
       16 22 110 ; 
       17 16 110 ; 
       18 22 110 ; 
       19 18 110 ; 
       20 22 110 ; 
       21 22 110 ; 
       22 27 110 ; 
       23 22 110 ; 
       24 22 110 ; 
       25 22 110 ; 
       26 22 110 ; 
       28 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 3 300 ; 
       1 5 300 ; 
       2 6 300 ; 
       3 1 300 ; 
       4 14 300 ; 
       5 2 300 ; 
       6 22 300 ; 
       6 26 300 ; 
       6 30 300 ; 
       7 4 300 ; 
       8 40 300 ; 
       8 41 300 ; 
       8 42 300 ; 
       8 43 300 ; 
       8 44 300 ; 
       9 55 300 ; 
       9 45 300 ; 
       9 46 300 ; 
       9 47 300 ; 
       9 48 300 ; 
       10 49 300 ; 
       10 50 300 ; 
       10 51 300 ; 
       10 52 300 ; 
       10 0 300 ; 
       11 53 300 ; 
       11 18 300 ; 
       11 19 300 ; 
       11 23 300 ; 
       11 27 300 ; 
       12 31 300 ; 
       12 32 300 ; 
       12 33 300 ; 
       12 34 300 ; 
       12 35 300 ; 
       13 54 300 ; 
       13 36 300 ; 
       13 37 300 ; 
       13 38 300 ; 
       13 39 300 ; 
       14 12 300 ; 
       15 13 300 ; 
       16 7 300 ; 
       17 8 300 ; 
       18 9 300 ; 
       19 10 300 ; 
       20 21 300 ; 
       20 25 300 ; 
       20 29 300 ; 
       21 20 300 ; 
       21 24 300 ; 
       21 28 300 ; 
       22 56 300 ; 
       22 57 300 ; 
       22 58 300 ; 
       22 59 300 ; 
       24 16 300 ; 
       25 17 300 ; 
       26 15 300 ; 
       28 11 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 43 401 ; 
       1 44 401 ; 
       2 45 401 ; 
       3 46 401 ; 
       4 47 401 ; 
       5 48 401 ; 
       6 49 401 ; 
       7 50 401 ; 
       8 51 401 ; 
       9 52 401 ; 
       10 54 401 ; 
       11 55 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 6 401 ; 
       15 53 401 ; 
       16 0 401 ; 
       17 1 401 ; 
       18 2 401 ; 
       19 3 401 ; 
       20 21 401 ; 
       21 22 401 ; 
       22 23 401 ; 
       23 4 401 ; 
       24 25 401 ; 
       25 26 401 ; 
       26 27 401 ; 
       27 5 401 ; 
       28 29 401 ; 
       29 30 401 ; 
       30 31 401 ; 
       31 10 401 ; 
       32 11 401 ; 
       33 12 401 ; 
       34 13 401 ; 
       35 14 401 ; 
       36 16 401 ; 
       37 17 401 ; 
       38 18 401 ; 
       39 19 401 ; 
       40 20 401 ; 
       41 24 401 ; 
       42 28 401 ; 
       43 32 401 ; 
       44 33 401 ; 
       45 35 401 ; 
       46 36 401 ; 
       47 37 401 ; 
       48 38 401 ; 
       49 39 401 ; 
       50 40 401 ; 
       51 41 401 ; 
       52 42 401 ; 
       53 9 401 ; 
       54 15 401 ; 
       55 34 401 ; 
       57 58 401 ; 
       58 56 401 ; 
       59 57 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -4 0 MPRFLG 0 ; 
       1 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       2 SCHEM 35 -4 0 MPRFLG 0 ; 
       3 SCHEM 40 -4 0 MPRFLG 0 ; 
       4 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 60 -4 0 MPRFLG 0 ; 
       7 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       9 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 30 -4 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 15 -4 0 MPRFLG 0 ; 
       13 SCHEM 20 -4 0 MPRFLG 0 ; 
       14 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 5 -4 0 MPRFLG 0 ; 
       17 SCHEM 5 -6 0 MPRFLG 0 ; 
       18 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       19 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 57.5 -4 0 MPRFLG 0 ; 
       21 SCHEM 55 -4 0 MPRFLG 0 ; 
       22 SCHEM 31.25 -2 0 MPRFLG 0 ; 
       23 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       26 SCHEM 10 -4 0 MPRFLG 0 ; 
       27 SCHEM 31.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 50 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 39 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 41.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 44 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 46.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 31.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 49 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 36.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 54 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 56.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 59 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 61.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 21.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 36.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 54 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 56.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 59 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 24 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 26.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 39 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 41.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 44 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 46.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 31.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 6.5 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 49 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 60.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 60.5 -6 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 61.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
