SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.8-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 42     
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 54     
       bioresearch_ss104-bound01.1-0 ; 
       bioresearch_ss104-bounding_model.1-0 ; 
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ; 
       bioresearch_ss104-extru75.1-0 ; 
       bioresearch_ss104-extru76.1-0 ; 
       bioresearch_ss104-garage1A.1-0 ; 
       bioresearch_ss104-garage1B.1-0 ; 
       bioresearch_ss104-garage1C.1-0 ; 
       bioresearch_ss104-garage1D.1-0 ; 
       bioresearch_ss104-garage1E.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-launch3.1-0 ; 
       bioresearch_ss104-null18.5-0 ; 
       bioresearch_ss104-null19.4-0 ; 
       bioresearch_ss104-null26.4-0 ; 
       bioresearch_ss104-null27.5-0 ; 
       bioresearch_ss104-null28.1-0 ROOT ; 
       bioresearch_ss104-sphere1.7-0 ; 
       bioresearch_ss104-sphere2.1-0 ; 
       bioresearch_ss104-sphere5.1-0 ; 
       bioresearch_ss104-SS_11_1.1-0 ; 
       bioresearch_ss104-SS_11_4.1-0 ; 
       bioresearch_ss104-SS_13_2.1-0 ; 
       bioresearch_ss104-SS_15_1.1-0 ; 
       bioresearch_ss104-SS_15_4.1-0 ; 
       bioresearch_ss104-SS_23_2.1-0 ; 
       bioresearch_ss104-SS_23_4.1-0 ; 
       bioresearch_ss104-SS_24_1.1-0 ; 
       bioresearch_ss104-SS_24_4.1-0 ; 
       bioresearch_ss104-SS_26.1-0 ; 
       bioresearch_ss104-SS_26_4.1-0 ; 
       bioresearch_ss104-SS_29.1-0 ; 
       bioresearch_ss104-SS_30.1-0 ; 
       bioresearch_ss104-SS_31.1-0 ; 
       bioresearch_ss104-SS_32.1-0 ; 
       bioresearch_ss104-SS_33.1-0 ; 
       bioresearch_ss104-SS_34.1-0 ; 
       bioresearch_ss104-SS_35.1-0 ; 
       bioresearch_ss104-SS_36.1-0 ; 
       bioresearch_ss104-SS_55.2-0 ; 
       bioresearch_ss104-SS_58.1-0 ; 
       bioresearch_ss104-SS_60.1-0 ; 
       bioresearch_ss104-SS_68.1-0 ; 
       bioresearch_ss104-SS_70.1-0 ; 
       bioresearch_ss104-SS_71.1-0 ; 
       bioresearch_ss104-SS_72.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-bioresearch-ss104.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ord_depot-t2d32.1-0 ; 
       ord_depot-t2d33.1-0 ; 
       shipyard_ss102-t2d49.1-0 ; 
       shipyard_ss102-t2d50.1-0 ; 
       shipyard_ss102-t2d51.1-0 ; 
       shipyard_ss102-t2d58.1-0 ; 
       shipyard_ss102-t2d59.1-0 ; 
       shipyard_ss102-t2d60.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 5 110 ; 
       3 21 110 ; 
       4 3 110 ; 
       5 21 110 ; 
       8 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
       11 2 110 ; 
       12 2 110 ; 
       13 52 110 ; 
       14 52 110 ; 
       15 53 110 ; 
       16 53 110 ; 
       17 4 110 ; 
       21 23 110 ; 
       23 22 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 13 110 ; 
       27 16 110 ; 
       28 13 110 ; 
       29 13 110 ; 
       30 16 110 ; 
       31 14 110 ; 
       32 15 110 ; 
       33 14 110 ; 
       34 15 110 ; 
       35 14 110 ; 
       36 15 110 ; 
       46 16 110 ; 
       52 2 110 ; 
       53 4 110 ; 
       6 23 110 ; 
       7 6 110 ; 
       18 21 110 ; 
       19 21 110 ; 
       20 21 110 ; 
       37 18 110 ; 
       38 18 110 ; 
       39 18 110 ; 
       40 18 110 ; 
       41 19 110 ; 
       42 19 110 ; 
       43 19 110 ; 
       44 19 110 ; 
       45 20 110 ; 
       47 19 110 ; 
       48 18 110 ; 
       49 20 110 ; 
       50 20 110 ; 
       51 20 110 ; 
       1 22 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 6 300 ; 
       2 2 300 ; 
       2 4 300 ; 
       2 7 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       4 14 300 ; 
       4 3 300 ; 
       4 5 300 ; 
       4 15 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 10 300 ; 
       26 33 300 ; 
       27 29 300 ; 
       28 36 300 ; 
       29 39 300 ; 
       30 31 300 ; 
       31 40 300 ; 
       32 26 300 ; 
       33 41 300 ; 
       34 27 300 ; 
       35 16 300 ; 
       36 28 300 ; 
       46 30 300 ; 
       6 0 300 ; 
       7 1 300 ; 
       37 20 300 ; 
       38 19 300 ; 
       39 18 300 ; 
       40 17 300 ; 
       41 21 300 ; 
       42 22 300 ; 
       43 23 300 ; 
       44 24 300 ; 
       45 25 300 ; 
       47 32 300 ; 
       48 34 300 ; 
       49 35 300 ; 
       50 37 300 ; 
       51 38 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5271.777 28.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5271.777 26.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5257.39 31.8527 0 USR MPRFLG 0 ; 
       2 SCHEM 5231.986 10.0558 0 USR MPRFLG 0 ; 
       3 SCHEM 5262.354 12.08247 0 USR MPRFLG 0 ; 
       4 SCHEM 5262.354 10.08248 0 USR MPRFLG 0 ; 
       5 SCHEM 5231.986 12.05579 0 USR MPRFLG 0 ; 
       8 SCHEM 5233.236 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5240.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 5235.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 5245.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       12 SCHEM 5243.236 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       13 SCHEM 5220.736 6.055798 0 USR MPRFLG 0 ; 
       14 SCHEM 5228.236 6.055798 0 USR MPRFLG 0 ; 
       15 SCHEM 5263.604 6.082497 0 USR MPRFLG 0 ; 
       16 SCHEM 5256.104 6.082497 0 USR MPRFLG 0 ; 
       17 SCHEM 5271.104 8.082476 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       21 SCHEM 5256.666 22.91645 0 MPRFLG 0 ; 
       23 SCHEM 5241.371 28.60965 0 USR MPRFLG 0 ; 
       24 SCHEM 5244.46 20.99264 0 USR MPRFLG 0 ; 
       25 SCHEM 5238.919 21.01114 0 USR MPRFLG 0 ; 
       26 SCHEM 5223.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 5258.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 5218.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 5220.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 5256.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 5230.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 5266.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 5225.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 5261.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 5228.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 5263.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 5253.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 5224.486 8.055796 0 USR MPRFLG 0 ; 
       53 SCHEM 5259.854 8.082476 0 USR MPRFLG 0 ; 
       6 SCHEM 5235.799 23.07737 0 USR MPRFLG 0 ; 
       7 SCHEM 5235.799 21.07738 0 USR MPRFLG 0 ; 
       18 SCHEM 5241.984 1.267559 0 USR MPRFLG 0 ; 
       19 SCHEM 5244.53 4.368715 0 USR MPRFLG 0 ; 
       20 SCHEM 5244.921 -1.718512 0 USR MPRFLG 0 ; 
       37 SCHEM 5238.234 -0.732448 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 5240.734 -0.732448 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       39 SCHEM 5243.234 -0.732448 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       40 SCHEM 5245.734 -0.732448 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       41 SCHEM 5245.78 2.368712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 5243.28 2.368712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       43 SCHEM 5240.78 2.368712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 5238.28 2.368712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 5240.264 -4.606065 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 5248.28 2.368712 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       22 SCHEM 5249.681 36.70137 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       48 SCHEM 5248.309 -0.7392086 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 5242.764 -4.606065 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 5245.264 -4.606065 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       51 SCHEM 5247.764 -4.606065 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5256.345 33.88144 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2317.726 -0.7232437 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2322.726 -0.7232437 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5058.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5241.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5060.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5244.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5055.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5063.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5065.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5068.109 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5070.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5249.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5251.666 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5254.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5239.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5246.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5035.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4945.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4948.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4950.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4953.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4623.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4625.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4628.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4630.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4511.444 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 5231.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5226.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5229.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5224.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5219.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5221.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4651.814 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 5030.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4978.868 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4530.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 5025.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4535.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 4540.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 5028.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 5038.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 5033.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 2317.726 -2.723244 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2322.726 -2.723244 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5065.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5068.109 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5070.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5249.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5251.666 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5254.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
