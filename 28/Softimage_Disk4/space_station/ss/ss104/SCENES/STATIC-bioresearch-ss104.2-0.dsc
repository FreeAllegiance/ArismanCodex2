SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.40-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 25     
       bioresearch_ss104-mat77.3-0 ; 
       bioresearch_ss104-mat78.2-0 ; 
       bioresearch_ss104-mat79.1-0 ; 
       bioresearch_ss104-mat80.4-0 ; 
       bioresearch_ss104-mat81.4-0 ; 
       bioresearch_ss104-mat83.4-0 ; 
       bioresearch_ss104-mat84.1-0 ; 
       bioresearch_ss104-mat85.1-0 ; 
       bioresearch_ss104-mat86.1-0 ; 
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ; 
       bioresearch_ss104-extru75.1-0 ; 
       bioresearch_ss104-extru76.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-null27.5-0 ; 
       bioresearch_ss104-sphere1_1.8-0 ROOT ; 
       bioresearch_ss104-sphere2.1-0 ; 
       bioresearch_ss104-sphere6.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bioresearch-ss104.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       bioresearch_ss104-t2d61.6-0 ; 
       bioresearch_ss104-t2d62.4-0 ; 
       bioresearch_ss104-t2d63.4-0 ; 
       bioresearch_ss104-t2d64.3-0 ; 
       bioresearch_ss104-t2d65.3-0 ; 
       bioresearch_ss104-t2d66.3-0 ; 
       bioresearch_ss104-t2d67.2-0 ; 
       bioresearch_ss104-t2d68.2-0 ; 
       bioresearch_ss104-t2d69.2-0 ; 
       ord_depot-t2d32.3-0 ; 
       ord_depot-t2d33.3-0 ; 
       shipyard_ss102-t2d49.6-0 ; 
       shipyard_ss102-t2d50.6-0 ; 
       shipyard_ss102-t2d51.6-0 ; 
       shipyard_ss102-t2d58.6-0 ; 
       shipyard_ss102-t2d59.6-0 ; 
       shipyard_ss102-t2d60.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 10 110 ; 
       2 1 110 ; 
       3 10 110 ; 
       4 11 110 ; 
       5 4 110 ; 
       6 14 110 ; 
       7 14 110 ; 
       8 15 110 ; 
       9 15 110 ; 
       10 11 110 ; 
       12 11 110 ; 
       13 11 110 ; 
       14 0 110 ; 
       15 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 15 300 ; 
       0 11 300 ; 
       0 13 300 ; 
       0 16 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       2 23 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       2 24 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       4 9 300 ; 
       5 10 300 ; 
       11 0 300 ; 
       11 1 300 ; 
       11 2 300 ; 
       12 3 300 ; 
       12 4 300 ; 
       12 5 300 ; 
       13 6 300 ; 
       13 7 300 ; 
       13 8 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 5 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 8.75 -6 0 MPRFLG 0 ; 
       1 SCHEM 13.75 -4 0 MPRFLG 0 ; 
       2 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -4 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 10 -10 0 MPRFLG 0 ; 
       8 SCHEM 15 -10 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       10 SCHEM 11.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 5 -2 0 MPRFLG 0 ; 
       13 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       15 SCHEM 13.75 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 16.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
