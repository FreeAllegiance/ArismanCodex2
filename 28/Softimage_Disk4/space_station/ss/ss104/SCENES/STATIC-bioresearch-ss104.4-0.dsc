SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.47-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 17     
       bioresearch_ss104-mat77.4-0 ; 
       bioresearch_ss104-mat78.3-0 ; 
       bioresearch_ss104-mat79.2-0 ; 
       bioresearch_ss104-mat80.5-0 ; 
       bioresearch_ss104-mat81.5-0 ; 
       bioresearch_ss104-mat83.5-0 ; 
       bioresearch_ss104-mat84.2-0 ; 
       bioresearch_ss104-mat85.2-0 ; 
       bioresearch_ss104-mat86.2-0 ; 
       ord_depot-mat33.2-0 ; 
       ord_depot-mat34.2-0 ; 
       shipyard_ss102-mat58.2-0 ; 
       shipyard_ss102-mat59.2-0 ; 
       shipyard_ss102-mat60.2-0 ; 
       shipyard_ss102-mat67.2-0 ; 
       shipyard_ss102-mat68.2-0 ; 
       shipyard_ss102-mat69.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 8     
       root-east_bay_11_11.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-extru75.1-0 ; 
       root-extru76.1-0 ; 
       root-root.5-0 ROOT ; 
       root-sphere2.1-0 ; 
       root-sphere6.1-0 ; 
       root-ss104.7-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bioresearch-ss104.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       bioresearch_ss104-t2d61.9-0 ; 
       bioresearch_ss104-t2d62.7-0 ; 
       bioresearch_ss104-t2d63.7-0 ; 
       bioresearch_ss104-t2d64.6-0 ; 
       bioresearch_ss104-t2d65.6-0 ; 
       bioresearch_ss104-t2d66.6-0 ; 
       bioresearch_ss104-t2d67.5-0 ; 
       bioresearch_ss104-t2d68.5-0 ; 
       bioresearch_ss104-t2d69.5-0 ; 
       ord_depot-t2d32_1.3-0 ; 
       ord_depot-t2d33_1.3-0 ; 
       shipyard_ss102-t2d49_1.4-0 ; 
       shipyard_ss102-t2d50_1.4-0 ; 
       shipyard_ss102-t2d51_1.4-0 ; 
       shipyard_ss102-t2d58_1.4-0 ; 
       shipyard_ss102-t2d59_1.4-0 ; 
       shipyard_ss102-t2d60_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       2 7 110 ; 
       3 7 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       7 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 5 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -5.530939 0 MPRFLG 0 ; 
       1 SCHEM 17.5 -5.530939 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -5.530939 0 MPRFLG 0 ; 
       3 SCHEM 5 -5.530939 0 MPRFLG 0 ; 
       4 SCHEM 21.89087 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 10 -5.530939 0 MPRFLG 0 ; 
       6 SCHEM 25 -5.530939 0 MPRFLG 0 ; 
       7 SCHEM 22.5 -3.530939 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 42.5 -5.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -5.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -5.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 25 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 35 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 30 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -7.530939 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 42.5 -7.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 37.5 -7.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 40 -7.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 15 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 17.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 35 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 30 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -9.530939 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
