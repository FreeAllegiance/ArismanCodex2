SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.77-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       bioresearch_ss104-Hangar1.1-0 ; 
       bioresearch_ss104-mat80.5-0 ; 
       bioresearch_ss104-mat81.5-0 ; 
       bioresearch_ss104-mat83.5-0 ; 
       bioresearch_ss104-mat84.2-0 ; 
       bioresearch_ss104-mat85.2-0 ; 
       bioresearch_ss104-mat86.2-0 ; 
       bioresearch_ss104-mat87.2-0 ; 
       bioresearch_ss104-mat88.2-0 ; 
       bioresearch_ss104-mat89.2-0 ; 
       ord_depot-mat33.2-0 ; 
       ord_depot-mat34.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 6     
       root-extru75.1-0 ; 
       root-extru76.1-0 ; 
       root-root.20-0 ROOT ; 
       root-sphere2.1-0 ; 
       root-sphere6.1-0 ; 
       root-ss105.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bioresearch-ss104.5-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       bioresearch_ss104-t2d64.7-0 ; 
       bioresearch_ss104-t2d65.7-0 ; 
       bioresearch_ss104-t2d66.7-0 ; 
       bioresearch_ss104-t2d67.6-0 ; 
       bioresearch_ss104-t2d68.6-0 ; 
       bioresearch_ss104-t2d69.6-0 ; 
       bioresearch_ss104-t2d70.5-0 ; 
       bioresearch_ss104-t2d71.5-0 ; 
       bioresearch_ss104-t2d72.5-0 ; 
       bioresearch_ss104-t2d73.2-0 ; 
       ord_depot-t2d32_1.4-0 ; 
       ord_depot-t2d33_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 5 110 ; 
       1 5 110 ; 
       3 5 110 ; 
       4 5 110 ; 
       5 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 10 300 ; 
       1 11 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       4 4 300 ; 
       4 5 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       5 8 300 ; 
       5 9 300 ; 
       5 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 2 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
       10 10 401 ; 
       11 11 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 16.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 10 -4 0 MPRFLG 0 ; 
       4 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 16.25 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 30 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 12.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 15 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 27.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 22.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 7.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 10 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 12.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 15 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 27.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 22.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 30 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
