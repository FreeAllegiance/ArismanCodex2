SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.36-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 51     
       bioresearch_ss104-mat77.3-0 ; 
       bioresearch_ss104-mat78.2-0 ; 
       bioresearch_ss104-mat79.1-0 ; 
       bioresearch_ss104-mat80.4-0 ; 
       bioresearch_ss104-mat81.4-0 ; 
       bioresearch_ss104-mat83.4-0 ; 
       bioresearch_ss104-mat84.1-0 ; 
       bioresearch_ss104-mat85.1-0 ; 
       bioresearch_ss104-mat86.1-0 ; 
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 51     
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ; 
       bioresearch_ss104-extru75.1-0 ; 
       bioresearch_ss104-extru76.1-0 ; 
       bioresearch_ss104-garage1A.1-0 ; 
       bioresearch_ss104-garage1B.1-0 ; 
       bioresearch_ss104-garage1C.1-0 ; 
       bioresearch_ss104-garage1D.1-0 ; 
       bioresearch_ss104-garage1E.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-launch3.1-0 ; 
       bioresearch_ss104-null18.5-0 ; 
       bioresearch_ss104-null19.4-0 ; 
       bioresearch_ss104-null26.4-0 ; 
       bioresearch_ss104-null27.5-0 ; 
       bioresearch_ss104-sphere1_1.7-0 ROOT ; 
       bioresearch_ss104-sphere2.1-0 ; 
       bioresearch_ss104-sphere6.1-0 ; 
       bioresearch_ss104-SS_11_1.1-0 ; 
       bioresearch_ss104-SS_11_4.1-0 ; 
       bioresearch_ss104-SS_13_2.1-0 ; 
       bioresearch_ss104-SS_15_1.1-0 ; 
       bioresearch_ss104-SS_15_4.1-0 ; 
       bioresearch_ss104-SS_23_2.1-0 ; 
       bioresearch_ss104-SS_23_4.1-0 ; 
       bioresearch_ss104-SS_24_1.1-0 ; 
       bioresearch_ss104-SS_24_4.1-0 ; 
       bioresearch_ss104-SS_26.1-0 ; 
       bioresearch_ss104-SS_26_4.1-0 ; 
       bioresearch_ss104-SS_29.1-0 ; 
       bioresearch_ss104-SS_30.1-0 ; 
       bioresearch_ss104-SS_31.1-0 ; 
       bioresearch_ss104-SS_32.1-0 ; 
       bioresearch_ss104-SS_33.1-0 ; 
       bioresearch_ss104-SS_34.1-0 ; 
       bioresearch_ss104-SS_35.1-0 ; 
       bioresearch_ss104-SS_36.1-0 ; 
       bioresearch_ss104-SS_55.2-0 ; 
       bioresearch_ss104-SS_58.1-0 ; 
       bioresearch_ss104-SS_60.1-0 ; 
       bioresearch_ss104-SS_68.1-0 ; 
       bioresearch_ss104-SS_70.1-0 ; 
       bioresearch_ss104-SS_71.1-0 ; 
       bioresearch_ss104-SS_72.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-bioresearch-ss104.28-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       bioresearch_ss104-t2d61.6-0 ; 
       bioresearch_ss104-t2d62.4-0 ; 
       bioresearch_ss104-t2d63.4-0 ; 
       bioresearch_ss104-t2d64.3-0 ; 
       bioresearch_ss104-t2d65.3-0 ; 
       bioresearch_ss104-t2d66.3-0 ; 
       bioresearch_ss104-t2d67.2-0 ; 
       bioresearch_ss104-t2d68.2-0 ; 
       bioresearch_ss104-t2d69.2-0 ; 
       ord_depot-t2d32.3-0 ; 
       ord_depot-t2d33.3-0 ; 
       shipyard_ss102-t2d49.6-0 ; 
       shipyard_ss102-t2d50.6-0 ; 
       shipyard_ss102-t2d51.6-0 ; 
       shipyard_ss102-t2d58.6-0 ; 
       shipyard_ss102-t2d59.6-0 ; 
       shipyard_ss102-t2d60.6-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 19 110 ; 
       2 1 110 ; 
       3 19 110 ; 
       4 20 110 ; 
       5 4 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 0 110 ; 
       10 0 110 ; 
       11 49 110 ; 
       12 49 110 ; 
       13 50 110 ; 
       14 50 110 ; 
       15 2 110 ; 
       16 19 110 ; 
       17 19 110 ; 
       18 19 110 ; 
       19 20 110 ; 
       21 20 110 ; 
       22 20 110 ; 
       23 11 110 ; 
       24 14 110 ; 
       25 11 110 ; 
       26 11 110 ; 
       27 14 110 ; 
       28 12 110 ; 
       29 13 110 ; 
       30 12 110 ; 
       31 13 110 ; 
       32 12 110 ; 
       33 13 110 ; 
       34 16 110 ; 
       35 16 110 ; 
       36 16 110 ; 
       37 16 110 ; 
       38 17 110 ; 
       39 17 110 ; 
       40 17 110 ; 
       41 17 110 ; 
       42 18 110 ; 
       43 14 110 ; 
       44 17 110 ; 
       45 16 110 ; 
       46 18 110 ; 
       47 18 110 ; 
       48 18 110 ; 
       49 0 110 ; 
       50 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 15 300 ; 
       0 11 300 ; 
       0 13 300 ; 
       0 16 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       2 23 300 ; 
       2 12 300 ; 
       2 14 300 ; 
       2 24 300 ; 
       3 17 300 ; 
       3 18 300 ; 
       3 19 300 ; 
       4 9 300 ; 
       5 10 300 ; 
       20 0 300 ; 
       20 1 300 ; 
       20 2 300 ; 
       21 3 300 ; 
       21 4 300 ; 
       21 5 300 ; 
       22 6 300 ; 
       22 7 300 ; 
       22 8 300 ; 
       23 42 300 ; 
       24 38 300 ; 
       25 45 300 ; 
       26 48 300 ; 
       27 40 300 ; 
       28 49 300 ; 
       29 35 300 ; 
       30 50 300 ; 
       31 36 300 ; 
       32 25 300 ; 
       33 37 300 ; 
       34 29 300 ; 
       35 28 300 ; 
       36 27 300 ; 
       37 26 300 ; 
       38 30 300 ; 
       39 31 300 ; 
       40 32 300 ; 
       41 33 300 ; 
       42 34 300 ; 
       43 39 300 ; 
       44 41 300 ; 
       45 43 300 ; 
       46 44 300 ; 
       47 46 300 ; 
       48 47 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 5 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       17 11 401 ; 
       18 12 401 ; 
       19 13 401 ; 
       20 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 111.25 -4 0 MPRFLG 0 ; 
       2 SCHEM 107.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 30 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 35 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 32.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 40 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 37.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       11 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       12 SCHEM 25 -10 0 MPRFLG 0 ; 
       13 SCHEM 105 -10 0 MPRFLG 0 ; 
       14 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 110 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       16 SCHEM 65 -4 0 MPRFLG 0 ; 
       17 SCHEM 77.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 88.75 -4 0 MPRFLG 0 ; 
       19 SCHEM 71.25 -2 0 MPRFLG 0 ; 
       20 SCHEM 72.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 10 -2 0 MPRFLG 0 ; 
       22 SCHEM 132.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 20 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 100 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 15 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 97.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 27.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 107.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 22.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 102.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 25 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 105 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 60 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 65 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       37 SCHEM 67.5 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       38 SCHEM 80 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 77.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 75 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       41 SCHEM 72.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       42 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 95 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       44 SCHEM 82.5 -6 0 WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 70 -6 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 21.25 -8 0 MPRFLG 0 ; 
       50 SCHEM 101.25 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 142.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 140 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 135 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 130 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 112.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 45 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 115 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 50 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 47.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 127.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 122.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 125 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 120 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 117.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 25 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 67.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 65 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 62.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 60 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 80 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 77.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 72.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 85 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 107.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 102.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 105 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 100 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 95 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 97.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 82.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 20 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 70 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 87.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 15 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 90 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 92.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 17.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 27.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       50 SCHEM 22.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 142.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 137.5 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 140 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 7.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 10 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 12.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 135 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 130 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 132.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 2.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 57.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 52.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 55 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 127.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 122.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 125 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
