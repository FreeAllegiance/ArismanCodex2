SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.42-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       bioresearch_ss104-mat77.3-0 ; 
       bioresearch_ss104-mat78.2-0 ; 
       bioresearch_ss104-mat79.1-0 ; 
       bioresearch_ss104-mat80.4-0 ; 
       bioresearch_ss104-mat81.4-0 ; 
       bioresearch_ss104-mat83.4-0 ; 
       bioresearch_ss104-mat84.1-0 ; 
       bioresearch_ss104-mat85.1-0 ; 
       bioresearch_ss104-mat86.1-0 ; 
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       root-east_bay_11_11.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-extru75.1-0 ; 
       root-extru76.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch3.1-0 ; 
       root-root.2-0 ROOT ; 
       root-sphere2.1-0 ; 
       root-sphere6.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_11_4.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_4.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_23_4.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_24_4.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_4.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_55.2-0 ; 
       root-SS_58.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_68.1-0 ; 
       root-SS_70.1-0 ; 
       root-SS_71.1-0 ; 
       root-SS_72.1-0 ; 
       root-ss104.7-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURED-bioresearch-ss104.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       bioresearch_ss104-t2d61.7-0 ; 
       bioresearch_ss104-t2d62.5-0 ; 
       bioresearch_ss104-t2d63.5-0 ; 
       bioresearch_ss104-t2d64.4-0 ; 
       bioresearch_ss104-t2d65.4-0 ; 
       bioresearch_ss104-t2d66.4-0 ; 
       bioresearch_ss104-t2d67.3-0 ; 
       bioresearch_ss104-t2d68.3-0 ; 
       bioresearch_ss104-t2d69.3-0 ; 
       ord_depot-t2d32_1.1-0 ; 
       ord_depot-t2d33_1.1-0 ; 
       shipyard_ss102-t2d49_1.2-0 ; 
       shipyard_ss102-t2d50_1.2-0 ; 
       shipyard_ss102-t2d51_1.2-0 ; 
       shipyard_ss102-t2d58_1.2-0 ; 
       shipyard_ss102-t2d59_1.2-0 ; 
       shipyard_ss102-t2d60_1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 39 110 ; 
       1 39 110 ; 
       2 39 110 ; 
       3 39 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 0 110 ; 
       11 39 110 ; 
       12 39 110 ; 
       13 1 110 ; 
       14 0 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 0 110 ; 
       18 1 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 0 110 ; 
       22 1 110 ; 
       23 0 110 ; 
       24 39 110 ; 
       25 39 110 ; 
       26 39 110 ; 
       27 39 110 ; 
       28 39 110 ; 
       29 39 110 ; 
       30 39 110 ; 
       31 39 110 ; 
       32 39 110 ; 
       33 0 110 ; 
       34 39 110 ; 
       35 39 110 ; 
       36 39 110 ; 
       37 39 110 ; 
       38 39 110 ; 
       39 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       13 34 300 ; 
       14 30 300 ; 
       15 37 300 ; 
       16 40 300 ; 
       17 32 300 ; 
       18 41 300 ; 
       19 27 300 ; 
       20 42 300 ; 
       21 28 300 ; 
       22 17 300 ; 
       23 29 300 ; 
       24 21 300 ; 
       25 20 300 ; 
       26 19 300 ; 
       27 18 300 ; 
       28 22 300 ; 
       29 23 300 ; 
       30 24 300 ; 
       31 25 300 ; 
       32 26 300 ; 
       33 31 300 ; 
       34 33 300 ; 
       35 35 300 ; 
       36 36 300 ; 
       37 38 300 ; 
       38 39 300 ; 
       39 0 300 ; 
       39 1 300 ; 
       39 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 5 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5.098453 2.124552 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5.098453 0.1245524 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 73.62373 -6.357866 0 MPRFLG 0 ; 
       1 SCHEM 13.62374 -6.357866 0 MPRFLG 0 ; 
       2 SCHEM -6.376266 -6.357866 0 MPRFLG 0 ; 
       3 SCHEM -3.876265 -6.357866 0 MPRFLG 0 ; 
       4 SCHEM 16.12373 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 21.12374 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 18.62374 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 26.12374 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 23.62374 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 81.12373 -8.357866 0 WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 36.7646 -0.8269261 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM -1.376265 -6.357866 0 MPRFLG 0 ; 
       12 SCHEM 63.62373 -6.357866 0 MPRFLG 0 ; 
       13 SCHEM 6.123734 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 71.12373 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 1.123734 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 3.623733 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 68.62373 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 13.62374 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 78.62373 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 8.623734 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 73.62373 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 11.12374 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 76.12373 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 28.62374 -6.357866 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 31.12373 -6.357866 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 33.62373 -6.357866 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 36.12373 -6.357866 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 48.62373 -6.357866 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 46.12373 -6.357866 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 43.62373 -6.357866 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 41.12373 -6.357866 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 53.62373 -6.357866 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 66.12374 -8.357866 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 51.12373 -6.357866 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 38.62373 -6.357866 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 56.12373 -6.357866 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 58.62373 -6.357866 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 61.12373 -6.357866 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 37.37373 -4.357865 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 89 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 89 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 89 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 86.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 34 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 41.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 39 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 36.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 54 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 51.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 49 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 46.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 59 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 81.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 76.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 79 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 74 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 69 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 71.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 56.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 11.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 44 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 61.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 6.5 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 64 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 66.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 9 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 19 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 14 -14 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 89 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 89 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 89 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 86.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 34 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 86.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 86.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 86.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
