SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.58-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       bioresearch_ss104-mat77.4-0 ; 
       bioresearch_ss104-mat78.3-0 ; 
       bioresearch_ss104-mat79.2-0 ; 
       bioresearch_ss104-mat80.5-0 ; 
       bioresearch_ss104-mat81.5-0 ; 
       bioresearch_ss104-mat83.5-0 ; 
       bioresearch_ss104-mat84.2-0 ; 
       bioresearch_ss104-mat85.2-0 ; 
       bioresearch_ss104-mat86.2-0 ; 
       ord_depot-mat33.2-0 ; 
       ord_depot-mat34.2-0 ; 
       shipyard_ss102-mat58.2-0 ; 
       shipyard_ss102-mat59.2-0 ; 
       shipyard_ss102-mat60.2-0 ; 
       shipyard_ss102-mat67.2-0 ; 
       shipyard_ss102-mat68.2-0 ; 
       shipyard_ss102-mat69.2-0 ; 
       shipyard_ss102-white_strobe1_10.2-0 ; 
       shipyard_ss102-white_strobe1_29.2-0 ; 
       shipyard_ss102-white_strobe1_30.2-0 ; 
       shipyard_ss102-white_strobe1_31.2-0 ; 
       shipyard_ss102-white_strobe1_32.2-0 ; 
       shipyard_ss102-white_strobe1_33.2-0 ; 
       shipyard_ss102-white_strobe1_34.2-0 ; 
       shipyard_ss102-white_strobe1_35.2-0 ; 
       shipyard_ss102-white_strobe1_36.2-0 ; 
       shipyard_ss102-white_strobe1_4.2-0 ; 
       shipyard_ss102-white_strobe1_43.2-0 ; 
       shipyard_ss102-white_strobe1_44.2-0 ; 
       shipyard_ss102-white_strobe1_45.2-0 ; 
       shipyard_ss102-white_strobe1_46.2-0 ; 
       shipyard_ss102-white_strobe1_47.2-0 ; 
       shipyard_ss102-white_strobe1_48.2-0 ; 
       shipyard_ss102-white_strobe1_49.2-0 ; 
       shipyard_ss102-white_strobe1_5.2-0 ; 
       shipyard_ss102-white_strobe1_57.2-0 ; 
       shipyard_ss102-white_strobe1_59.2-0 ; 
       shipyard_ss102-white_strobe1_6.2-0 ; 
       shipyard_ss102-white_strobe1_60.2-0 ; 
       shipyard_ss102-white_strobe1_61.2-0 ; 
       shipyard_ss102-white_strobe1_7.2-0 ; 
       shipyard_ss102-white_strobe1_8.2-0 ; 
       shipyard_ss102-white_strobe1_9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 40     
       root-east_bay_11_11.1-0 ; 
       root-east_bay_11_8.1-0 ; 
       root-extru75.1-0 ; 
       root-extru76.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-root.9-0 ROOT ; 
       root-sphere2.1-0 ; 
       root-sphere6.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_11_4.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_4.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_23_4.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_24_4.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_4.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_55.2-0 ; 
       root-SS_58.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_68.1-0 ; 
       root-SS_70.1-0 ; 
       root-SS_71.1-0 ; 
       root-SS_72.1-0 ; 
       root-ss104.7-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fix_launch-bioresearch-ss104.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       bioresearch_ss104-t2d61.9-0 ; 
       bioresearch_ss104-t2d62.7-0 ; 
       bioresearch_ss104-t2d63.7-0 ; 
       bioresearch_ss104-t2d64.6-0 ; 
       bioresearch_ss104-t2d65.6-0 ; 
       bioresearch_ss104-t2d66.6-0 ; 
       bioresearch_ss104-t2d67.5-0 ; 
       bioresearch_ss104-t2d68.5-0 ; 
       bioresearch_ss104-t2d69.5-0 ; 
       ord_depot-t2d32_1.3-0 ; 
       ord_depot-t2d33_1.3-0 ; 
       shipyard_ss102-t2d49_1.4-0 ; 
       shipyard_ss102-t2d50_1.4-0 ; 
       shipyard_ss102-t2d51_1.4-0 ; 
       shipyard_ss102-t2d58_1.4-0 ; 
       shipyard_ss102-t2d59_1.4-0 ; 
       shipyard_ss102-t2d60_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 39 110 ; 
       1 39 110 ; 
       2 39 110 ; 
       3 39 110 ; 
       4 10 110 ; 
       5 10 110 ; 
       6 10 110 ; 
       7 10 110 ; 
       8 10 110 ; 
       9 10 110 ; 
       11 39 110 ; 
       12 39 110 ; 
       13 1 110 ; 
       14 0 110 ; 
       15 1 110 ; 
       16 1 110 ; 
       17 0 110 ; 
       18 1 110 ; 
       19 0 110 ; 
       20 1 110 ; 
       21 0 110 ; 
       22 1 110 ; 
       23 0 110 ; 
       24 39 110 ; 
       25 39 110 ; 
       26 39 110 ; 
       27 39 110 ; 
       28 39 110 ; 
       29 39 110 ; 
       30 39 110 ; 
       31 39 110 ; 
       32 39 110 ; 
       33 0 110 ; 
       34 39 110 ; 
       35 39 110 ; 
       36 39 110 ; 
       37 39 110 ; 
       38 39 110 ; 
       39 10 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 14 300 ; 
       0 15 300 ; 
       0 16 300 ; 
       1 11 300 ; 
       1 12 300 ; 
       1 13 300 ; 
       2 9 300 ; 
       3 10 300 ; 
       11 3 300 ; 
       11 4 300 ; 
       11 5 300 ; 
       12 6 300 ; 
       12 7 300 ; 
       12 8 300 ; 
       13 34 300 ; 
       14 30 300 ; 
       15 37 300 ; 
       16 40 300 ; 
       17 32 300 ; 
       18 41 300 ; 
       19 27 300 ; 
       20 42 300 ; 
       21 28 300 ; 
       22 17 300 ; 
       23 29 300 ; 
       24 21 300 ; 
       25 20 300 ; 
       26 19 300 ; 
       27 18 300 ; 
       28 22 300 ; 
       29 23 300 ; 
       30 24 300 ; 
       31 25 300 ; 
       32 26 300 ; 
       33 31 300 ; 
       34 33 300 ; 
       35 35 300 ; 
       36 36 300 ; 
       37 38 300 ; 
       38 39 300 ; 
       39 0 300 ; 
       39 1 300 ; 
       39 2 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 0 401 ; 
       1 1 401 ; 
       2 2 401 ; 
       3 5 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 6 401 ; 
       7 7 401 ; 
       8 8 401 ; 
       9 9 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       12 12 401 ; 
       13 13 401 ; 
       14 14 401 ; 
       15 15 401 ; 
       16 16 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 105 -4 0 MPRFLG 0 ; 
       1 SCHEM 40 -4 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 20 -4 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 USR WIRECOL 9 7 DISPLAY 1 2 MPRFLG 0 ; 
       10 SCHEM 66.98035 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 25 -4 0 MPRFLG 0 ; 
       12 SCHEM 90 -4 0 MPRFLG 0 ; 
       13 SCHEM 35 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 100 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 97.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 42.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 107.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 37.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 102.5 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 40 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 105 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 52.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 55 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 57.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       27 SCHEM 60 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       28 SCHEM 72.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 70 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 67.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       31 SCHEM 65 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       32 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 95 -6 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 80 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 82.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 85 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 70 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 122.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 117.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 120 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 115 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 110 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 112.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 40 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 107.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 102.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 105 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 100 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 95 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 97.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 35 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 30 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 32.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 42.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 37.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 122.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 117.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 120 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 92.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 87.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 90 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 50 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 45 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 47.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 115 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 110 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 112.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
