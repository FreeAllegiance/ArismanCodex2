SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.69-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 38     
       bioresearch_ss104-Hangar1.1-0 ; 
       bioresearch_ss104-mat80.5-0 ; 
       bioresearch_ss104-mat81.5-0 ; 
       bioresearch_ss104-mat83.5-0 ; 
       bioresearch_ss104-mat84.2-0 ; 
       bioresearch_ss104-mat85.2-0 ; 
       bioresearch_ss104-mat86.2-0 ; 
       bioresearch_ss104-mat87.2-0 ; 
       bioresearch_ss104-mat88.2-0 ; 
       bioresearch_ss104-mat89.2-0 ; 
       ord_depot-mat33.2-0 ; 
       ord_depot-mat34.2-0 ; 
       shipyard_ss102-white_strobe1_10.2-0 ; 
       shipyard_ss102-white_strobe1_29.2-0 ; 
       shipyard_ss102-white_strobe1_30.2-0 ; 
       shipyard_ss102-white_strobe1_31.2-0 ; 
       shipyard_ss102-white_strobe1_32.2-0 ; 
       shipyard_ss102-white_strobe1_33.2-0 ; 
       shipyard_ss102-white_strobe1_34.2-0 ; 
       shipyard_ss102-white_strobe1_35.2-0 ; 
       shipyard_ss102-white_strobe1_36.2-0 ; 
       shipyard_ss102-white_strobe1_4.2-0 ; 
       shipyard_ss102-white_strobe1_43.2-0 ; 
       shipyard_ss102-white_strobe1_44.2-0 ; 
       shipyard_ss102-white_strobe1_45.2-0 ; 
       shipyard_ss102-white_strobe1_46.2-0 ; 
       shipyard_ss102-white_strobe1_47.2-0 ; 
       shipyard_ss102-white_strobe1_48.2-0 ; 
       shipyard_ss102-white_strobe1_49.2-0 ; 
       shipyard_ss102-white_strobe1_5.2-0 ; 
       shipyard_ss102-white_strobe1_57.2-0 ; 
       shipyard_ss102-white_strobe1_59.2-0 ; 
       shipyard_ss102-white_strobe1_6.2-0 ; 
       shipyard_ss102-white_strobe1_60.2-0 ; 
       shipyard_ss102-white_strobe1_61.2-0 ; 
       shipyard_ss102-white_strobe1_7.2-0 ; 
       shipyard_ss102-white_strobe1_8.2-0 ; 
       shipyard_ss102-white_strobe1_9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 38     
       root-extru75.1-0 ; 
       root-extru76.1-0 ; 
       root-garage1A.1-0 ; 
       root-garage1B.1-0 ; 
       root-garage1C.1-0 ; 
       root-garage1D.1-0 ; 
       root-garage1E.1-0 ; 
       root-launch1.1-0 ; 
       root-root.16-0 ROOT ; 
       root-sphere2.1-0 ; 
       root-sphere6.1-0 ; 
       root-SS_11_1.1-0 ; 
       root-SS_11_4.1-0 ; 
       root-SS_13_2.1-0 ; 
       root-SS_15_1.1-0 ; 
       root-SS_15_4.1-0 ; 
       root-SS_23_2.1-0 ; 
       root-SS_23_4.1-0 ; 
       root-SS_24_1.1-0 ; 
       root-SS_24_4.1-0 ; 
       root-SS_26.1-0 ; 
       root-SS_26_4.1-0 ; 
       root-SS_29.1-0 ; 
       root-SS_30.1-0 ; 
       root-SS_31.1-0 ; 
       root-SS_32.1-0 ; 
       root-SS_33.1-0 ; 
       root-SS_34.1-0 ; 
       root-SS_35.1-0 ; 
       root-SS_36.1-0 ; 
       root-SS_55.2-0 ; 
       root-SS_58.1-0 ; 
       root-SS_60.1-0 ; 
       root-SS_68.1-0 ; 
       root-SS_70.1-0 ; 
       root-SS_71.1-0 ; 
       root-SS_72.1-0 ; 
       root-ss105.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss104/PICTURES/ss104 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       one_piece-bioresearch-ss104.7-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 12     
       bioresearch_ss104-t2d64.7-0 ; 
       bioresearch_ss104-t2d65.7-0 ; 
       bioresearch_ss104-t2d66.7-0 ; 
       bioresearch_ss104-t2d67.6-0 ; 
       bioresearch_ss104-t2d68.6-0 ; 
       bioresearch_ss104-t2d69.6-0 ; 
       bioresearch_ss104-t2d70.5-0 ; 
       bioresearch_ss104-t2d71.5-0 ; 
       bioresearch_ss104-t2d72.5-0 ; 
       bioresearch_ss104-t2d73.2-0 ; 
       ord_depot-t2d32_1.4-0 ; 
       ord_depot-t2d33_1.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 37 110 ; 
       1 37 110 ; 
       2 8 110 ; 
       3 8 110 ; 
       4 8 110 ; 
       5 8 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       9 37 110 ; 
       10 37 110 ; 
       11 37 110 ; 
       12 37 110 ; 
       13 37 110 ; 
       14 37 110 ; 
       15 37 110 ; 
       16 37 110 ; 
       17 37 110 ; 
       18 37 110 ; 
       19 37 110 ; 
       20 37 110 ; 
       21 37 110 ; 
       22 37 110 ; 
       23 37 110 ; 
       24 37 110 ; 
       25 37 110 ; 
       26 37 110 ; 
       27 37 110 ; 
       28 37 110 ; 
       29 37 110 ; 
       30 37 110 ; 
       31 37 110 ; 
       32 37 110 ; 
       33 37 110 ; 
       34 37 110 ; 
       35 37 110 ; 
       36 37 110 ; 
       37 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 10 300 ; 
       1 11 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 4 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       11 29 300 ; 
       12 25 300 ; 
       13 32 300 ; 
       14 35 300 ; 
       15 27 300 ; 
       16 36 300 ; 
       17 22 300 ; 
       18 37 300 ; 
       19 23 300 ; 
       20 12 300 ; 
       21 24 300 ; 
       22 16 300 ; 
       23 15 300 ; 
       24 14 300 ; 
       25 13 300 ; 
       26 17 300 ; 
       27 18 300 ; 
       28 19 300 ; 
       29 20 300 ; 
       30 21 300 ; 
       31 26 300 ; 
       32 28 300 ; 
       33 30 300 ; 
       34 31 300 ; 
       35 33 300 ; 
       36 34 300 ; 
       37 7 300 ; 
       37 8 300 ; 
       37 9 300 ; 
       37 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 9 401 ; 
       1 2 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       6 5 401 ; 
       10 10 401 ; 
       11 11 401 ; 
       7 6 401 ; 
       8 7 401 ; 
       9 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       1 SCHEM 20 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 10 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       4 SCHEM 7.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 15 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 12.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 2.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 56.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       9 SCHEM 25 -4 0 MPRFLG 0 ; 
       10 SCHEM 82.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 35 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       12 SCHEM 92.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       13 SCHEM 30 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       14 SCHEM 32.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       15 SCHEM 90 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       16 SCHEM 42.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       17 SCHEM 100 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       18 SCHEM 37.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       19 SCHEM 95 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       20 SCHEM 40 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 97.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 45 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 47.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       24 SCHEM 50 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       25 SCHEM 52.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       26 SCHEM 65 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 62.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 60 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       29 SCHEM 57.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       30 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 87.5 -4 0 WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 67.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       33 SCHEM 55 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 72.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 77.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 63.75 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 110 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 27.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 85 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 80 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 82.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 52.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 50 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 45 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 65 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 62.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 60 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 57.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 100 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 95 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 97.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 92.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 87.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 90 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 67.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 35 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 55 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 30 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 77.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 37.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 107.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 102.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 105 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       9 SCHEM 110 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 22.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 27.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 85 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 80 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 82.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 17.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 107.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 102.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 105 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
