SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.5-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_50.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_58.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_62.1-0 ; 
       shipyard_ss102-white_strobe1_63.1-0 ; 
       shipyard_ss102-white_strobe1_64.1-0 ; 
       shipyard_ss102-white_strobe1_65.1-0 ; 
       shipyard_ss102-white_strobe1_66.1-0 ; 
       shipyard_ss102-white_strobe1_67.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 60     
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ; 
       bioresearch_ss104-garage1A.1-0 ; 
       bioresearch_ss104-garage1B.1-0 ; 
       bioresearch_ss104-garage1C.1-0 ; 
       bioresearch_ss104-garage1D.1-0 ; 
       bioresearch_ss104-garage1E.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-launch3.1-0 ; 
       bioresearch_ss104-null27.4-0 ROOT ; 
       bioresearch_ss104-sphere1.5-0 ROOT ; 
       bioresearch_ss104-sphere2.1-0 ; 
       bioresearch_ss104-sphere3.3-0 ROOT ; 
       bioresearch_ss104-sphere4.3-0 ROOT ; 
       bioresearch_ss104-sphere5.1-0 ; 
       bioresearch_ss104-SS_11_1.1-0 ; 
       bioresearch_ss104-SS_11_4.1-0 ; 
       bioresearch_ss104-SS_13_2.1-0 ; 
       bioresearch_ss104-SS_15_1.1-0 ; 
       bioresearch_ss104-SS_15_4.1-0 ; 
       bioresearch_ss104-SS_23_2.1-0 ; 
       bioresearch_ss104-SS_23_4.1-0 ; 
       bioresearch_ss104-SS_24_1.1-0 ; 
       bioresearch_ss104-SS_24_4.1-0 ; 
       bioresearch_ss104-SS_26.1-0 ; 
       bioresearch_ss104-SS_26_4.1-0 ; 
       bioresearch_ss104-SS_58.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
       bioresearch_ss105-sphere1.2-0 ROOT ; 
       shipyard_ss102-null18.3-0 ROOT ; 
       shipyard_ss102-null19.2-0 ROOT ; 
       shipyard_ss102-null26.2-0 ROOT ; 
       shipyard_ss102-SS_29.1-0 ; 
       shipyard_ss102-SS_30.1-0 ; 
       shipyard_ss102-SS_31.1-0 ; 
       shipyard_ss102-SS_32.1-0 ; 
       shipyard_ss102-SS_33.1-0 ; 
       shipyard_ss102-SS_34.1-0 ; 
       shipyard_ss102-SS_35.1-0 ; 
       shipyard_ss102-SS_36.1-0 ; 
       shipyard_ss102-SS_55.2-0 ; 
       shipyard_ss102-SS_60.1-0 ; 
       shipyard_ss102-SS_61.1-0 ; 
       shipyard_ss102-SS_68.1-0 ; 
       shipyard_ss102-SS_69.2-0 ROOT ; 
       shipyard_ss102-SS_70.1-0 ; 
       shipyard_ss102-SS_71.1-0 ; 
       shipyard_ss102-SS_72.1-0 ; 
       shipyard_ss102-SS_73.1-0 ; 
       shipyard_ss102-SS_74.1-0 ; 
       shipyard_ss102-SS_75.1-0 ; 
       shipyard_ss102-SS_76.1-0 ; 
       shipyard_ss102-SS_77.1-0 ; 
       shipyard_ss102-SS_78.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip_v2-bioresearch-ss104.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       shipyard_ss102-t2d49.1-0 ; 
       shipyard_ss102-t2d50.1-0 ; 
       shipyard_ss102-t2d51.1-0 ; 
       shipyard_ss102-t2d58.1-0 ; 
       shipyard_ss102-t2d59.1-0 ; 
       shipyard_ss102-t2d60.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       1 14 110 ; 
       2 1 110 ; 
       3 14 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 32 110 ; 
       10 32 110 ; 
       11 33 110 ; 
       12 33 110 ; 
       13 2 110 ; 
       16 15 110 ; 
       19 15 110 ; 
       20 9 110 ; 
       21 12 110 ; 
       22 9 110 ; 
       23 9 110 ; 
       24 12 110 ; 
       25 10 110 ; 
       26 11 110 ; 
       27 10 110 ; 
       28 11 110 ; 
       29 10 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 0 110 ; 
       33 2 110 ; 
       38 35 110 ; 
       39 35 110 ; 
       40 35 110 ; 
       41 35 110 ; 
       42 36 110 ; 
       43 36 110 ; 
       44 36 110 ; 
       45 36 110 ; 
       46 37 110 ; 
       47 36 110 ; 
       48 36 110 ; 
       49 35 110 ; 
       51 37 110 ; 
       52 37 110 ; 
       53 37 110 ; 
       54 37 110 ; 
       55 37 110 ; 
       56 37 110 ; 
       57 37 110 ; 
       58 37 110 ; 
       59 37 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 0 300 ; 
       0 2 300 ; 
       0 5 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       2 1 300 ; 
       2 3 300 ; 
       2 13 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       20 31 300 ; 
       21 27 300 ; 
       22 36 300 ; 
       23 45 300 ; 
       24 29 300 ; 
       25 46 300 ; 
       26 24 300 ; 
       27 47 300 ; 
       28 25 300 ; 
       29 14 300 ; 
       30 26 300 ; 
       31 28 300 ; 
       38 18 300 ; 
       39 17 300 ; 
       40 16 300 ; 
       41 15 300 ; 
       42 19 300 ; 
       43 20 300 ; 
       44 21 300 ; 
       45 22 300 ; 
       46 23 300 ; 
       47 30 300 ; 
       48 32 300 ; 
       49 33 300 ; 
       50 34 300 ; 
       51 35 300 ; 
       52 37 300 ; 
       53 38 300 ; 
       54 39 300 ; 
       55 40 300 ; 
       56 41 300 ; 
       57 42 300 ; 
       58 43 300 ; 
       59 44 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 656.025 7.107316 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 656.025 5.107316 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 681.7251 10.0558 0 USR MPRFLG 0 ; 
       1 SCHEM 712.0928 12.08247 0 USR MPRFLG 0 ; 
       2 SCHEM 712.0928 10.08248 0 USR MPRFLG 0 ; 
       3 SCHEM 681.7251 12.05579 0 USR MPRFLG 0 ; 
       4 SCHEM 682.9751 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       5 SCHEM 690.4751 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 685.4751 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 695.4751 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 692.9751 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 670.4751 6.055798 0 USR MPRFLG 0 ; 
       10 SCHEM 677.9751 6.055798 0 USR MPRFLG 0 ; 
       11 SCHEM 713.3428 6.082497 0 USR MPRFLG 0 ; 
       12 SCHEM 705.8428 6.082497 0 USR MPRFLG 0 ; 
       13 SCHEM 720.8428 8.082476 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       14 SCHEM 706.4046 22.91645 0 SRT 1 1 1 0 0 1.570796 0 0 0 MPRFLG 0 ; 
       15 SCHEM 691.3113 23.17121 0 USR SRT 2.44014 5.986466 4.925478 0 0 0 0 -7.86681 0 MPRFLG 0 ; 
       16 SCHEM 694.1992 20.99264 0 USR MPRFLG 0 ; 
       17 SCHEM 725.8428 23.17121 0 USR DISPLAY 0 0 SRT 3.363563 4.560029 1.620401 0 0 0 21.5662 -4.542652 0 MPRFLG 0 ; 
       18 SCHEM 728.3428 23.17121 0 USR DISPLAY 0 0 SRT 3.363563 4.560029 1.620401 0 0 0 21.5662 -4.542652 0 MPRFLG 0 ; 
       19 SCHEM 688.6578 21.01114 0 USR MPRFLG 0 ; 
       20 SCHEM 672.9751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       21 SCHEM 708.3428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 667.9751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 670.4751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 705.8428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 680.4751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 715.8428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 675.4751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 710.8428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 677.9751 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 713.3428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 703.3428 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 674.2251 8.055796 0 USR MPRFLG 0 ; 
       33 SCHEM 709.5928 8.082476 0 USR MPRFLG 0 ; 
       34 SCHEM 723.3428 23.17121 0 USR DISPLAY 0 0 SRT 2.394208 5.873782 4.832765 0 0 0 0 -7.86681 0 MPRFLG 0 ; 
       35 SCHEM 709.6764 -0.04052886 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 31.39535 0 MPRFLG 0 ; 
       36 SCHEM 697.3971 2.975847 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -5.684342e-014 -9.809725 1.062176e-007 MPRFLG 0 ; 
       37 SCHEM 660.3601 14.92957 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 705.9264 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 708.4264 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 710.9264 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 713.4264 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 698.6471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 696.1471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 693.6471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 691.1471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 649.1101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 701.1471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 703.6471 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 716.0014 -2.047296 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 718.5814 -2.014589 0 USR WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.6195 -37.37888 0.01068692 MPRFLG 0 ; 
       51 SCHEM 651.6101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 654.1101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 656.6101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 659.1101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 661.6101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 664.1101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 666.6101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 669.1101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 671.6101 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 507.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 691.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 510.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 693.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 505.3479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 512.8479 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 515.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 517.8479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 520.3479 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 698.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 701.4046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 703.9046 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 688.9046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 696.4046 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 485.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 395.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 397.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 400.3479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 402.8479 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 73.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 75.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 78.16489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 80.66489 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM -38.81696 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 681.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 676.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 678.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 673.9046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 668.9047 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 671.4046 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 101.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 480.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 106.5529 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 428.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 433.6066 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM -20.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 475.3479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM -15.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM -10.19353 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM -5.193527 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM -26.15214 2.160294 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 2.306473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4.806473 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 14.80647 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 477.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 487.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 482.8479 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 515.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 517.8479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 520.3479 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 698.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 701.4046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 703.9046 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
