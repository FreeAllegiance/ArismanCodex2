SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.7-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 50     
       ord_depot-mat33.1-0 ; 
       ord_depot-mat34.1-0 ; 
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_50.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_58.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_62.1-0 ; 
       shipyard_ss102-white_strobe1_63.1-0 ; 
       shipyard_ss102-white_strobe1_64.1-0 ; 
       shipyard_ss102-white_strobe1_65.1-0 ; 
       shipyard_ss102-white_strobe1_66.1-0 ; 
       shipyard_ss102-white_strobe1_67.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       bioresearch_ss104-cube1.1-0 ROOT ; 
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ; 
       bioresearch_ss104-garage1A.1-0 ; 
       bioresearch_ss104-garage1B.1-0 ; 
       bioresearch_ss104-garage1C.1-0 ; 
       bioresearch_ss104-garage1D.1-0 ; 
       bioresearch_ss104-garage1E.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-launch3.1-0 ; 
       bioresearch_ss104-null27.5-0 ; 
       bioresearch_ss104-sphere1.7-0 ROOT ; 
       bioresearch_ss104-sphere2.1-0 ; 
       bioresearch_ss104-sphere3.5-0 ROOT ; 
       bioresearch_ss104-sphere4.5-0 ROOT ; 
       bioresearch_ss104-sphere5.1-0 ; 
       bioresearch_ss104-SS_11_1.1-0 ; 
       bioresearch_ss104-SS_11_4.1-0 ; 
       bioresearch_ss104-SS_13_2.1-0 ; 
       bioresearch_ss104-SS_15_1.1-0 ; 
       bioresearch_ss104-SS_15_4.1-0 ; 
       bioresearch_ss104-SS_23_2.1-0 ; 
       bioresearch_ss104-SS_23_4.1-0 ; 
       bioresearch_ss104-SS_24_1.1-0 ; 
       bioresearch_ss104-SS_24_4.1-0 ; 
       bioresearch_ss104-SS_26.1-0 ; 
       bioresearch_ss104-SS_26_4.1-0 ; 
       bioresearch_ss104-SS_58.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
       bioresearch_ss105-sphere1.4-0 ROOT ; 
       ord-extru75.1-0 ROOT ; 
       ord-extru76.1-0 ; 
       shipyard_ss102-null18.5-0 ROOT ; 
       shipyard_ss102-null19.4-0 ROOT ; 
       shipyard_ss102-null26.4-0 ROOT ; 
       shipyard_ss102-SS_29.1-0 ; 
       shipyard_ss102-SS_30.1-0 ; 
       shipyard_ss102-SS_31.1-0 ; 
       shipyard_ss102-SS_32.1-0 ; 
       shipyard_ss102-SS_33.1-0 ; 
       shipyard_ss102-SS_34.1-0 ; 
       shipyard_ss102-SS_35.1-0 ; 
       shipyard_ss102-SS_36.1-0 ; 
       shipyard_ss102-SS_55.2-0 ; 
       shipyard_ss102-SS_60.1-0 ; 
       shipyard_ss102-SS_61.1-0 ; 
       shipyard_ss102-SS_68.1-0 ; 
       shipyard_ss102-SS_69.4-0 ROOT ; 
       shipyard_ss102-SS_70.1-0 ; 
       shipyard_ss102-SS_71.1-0 ; 
       shipyard_ss102-SS_72.1-0 ; 
       shipyard_ss102-SS_73.1-0 ; 
       shipyard_ss102-SS_74.1-0 ; 
       shipyard_ss102-SS_75.1-0 ; 
       shipyard_ss102-SS_76.1-0 ; 
       shipyard_ss102-SS_77.1-0 ; 
       shipyard_ss102-SS_78.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip_v2-bioresearch-ss104.3-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       ord_depot-t2d32.1-0 ; 
       ord_depot-t2d33.1-0 ; 
       shipyard_ss102-t2d49.1-0 ; 
       shipyard_ss102-t2d50.1-0 ; 
       shipyard_ss102-t2d51.1-0 ; 
       shipyard_ss102-t2d58.1-0 ; 
       shipyard_ss102-t2d59.1-0 ; 
       shipyard_ss102-t2d60.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       37 36 110 ; 
       1 4 110 ; 
       2 15 110 ; 
       3 2 110 ; 
       4 15 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 33 110 ; 
       11 33 110 ; 
       12 34 110 ; 
       13 34 110 ; 
       14 3 110 ; 
       15 16 110 ; 
       17 16 110 ; 
       20 16 110 ; 
       21 10 110 ; 
       22 13 110 ; 
       23 10 110 ; 
       24 10 110 ; 
       25 13 110 ; 
       26 11 110 ; 
       27 12 110 ; 
       28 11 110 ; 
       29 12 110 ; 
       30 11 110 ; 
       31 12 110 ; 
       32 13 110 ; 
       33 1 110 ; 
       34 3 110 ; 
       41 38 110 ; 
       42 38 110 ; 
       43 38 110 ; 
       44 38 110 ; 
       45 39 110 ; 
       46 39 110 ; 
       47 39 110 ; 
       48 39 110 ; 
       49 40 110 ; 
       50 39 110 ; 
       51 39 110 ; 
       52 38 110 ; 
       54 40 110 ; 
       55 40 110 ; 
       56 40 110 ; 
       57 40 110 ; 
       58 40 110 ; 
       59 40 110 ; 
       60 40 110 ; 
       61 40 110 ; 
       62 40 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       36 0 300 ; 
       37 1 300 ; 
       1 6 300 ; 
       1 2 300 ; 
       1 4 300 ; 
       1 7 300 ; 
       2 11 300 ; 
       2 12 300 ; 
       2 13 300 ; 
       3 14 300 ; 
       3 3 300 ; 
       3 5 300 ; 
       3 15 300 ; 
       4 8 300 ; 
       4 9 300 ; 
       4 10 300 ; 
       21 33 300 ; 
       22 29 300 ; 
       23 38 300 ; 
       24 47 300 ; 
       25 31 300 ; 
       26 48 300 ; 
       27 26 300 ; 
       28 49 300 ; 
       29 27 300 ; 
       30 16 300 ; 
       31 28 300 ; 
       32 30 300 ; 
       41 20 300 ; 
       42 19 300 ; 
       43 18 300 ; 
       44 17 300 ; 
       45 21 300 ; 
       46 22 300 ; 
       47 23 300 ; 
       48 24 300 ; 
       49 25 300 ; 
       50 32 300 ; 
       51 34 300 ; 
       52 35 300 ; 
       53 36 300 ; 
       54 37 300 ; 
       55 39 300 ; 
       56 40 300 ; 
       57 41 300 ; 
       58 42 300 ; 
       59 43 300 ; 
       60 44 300 ; 
       61 45 300 ; 
       62 46 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
       12 6 401 ; 
       13 7 401 ; 
       0 0 401 ; 
       1 1 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5271.777 28.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5271.777 26.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5257.39 31.8527 0 USR DISPLAY 1 2 SRT 1.584072 1.584072 0.6146197 0 0 0 0 12.21476 0 MPRFLG 0 ; 
       36 SCHEM 5279.711 28.71724 0 USR SRT 0.5460001 0.5460001 0.5460001 -1.509958e-007 0 3.141593 3.89288e-006 -59.13184 10.08531 MPRFLG 0 ; 
       37 SCHEM 5279.711 26.71725 0 USR MPRFLG 0 ; 
       1 SCHEM 5231.986 10.0558 0 USR MPRFLG 0 ; 
       2 SCHEM 5262.354 12.08247 0 USR MPRFLG 0 ; 
       3 SCHEM 5262.354 10.08248 0 USR MPRFLG 0 ; 
       4 SCHEM 5231.986 12.05579 0 USR MPRFLG 0 ; 
       5 SCHEM 5233.236 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       6 SCHEM 5240.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       7 SCHEM 5235.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       8 SCHEM 5245.736 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       9 SCHEM 5243.236 8.055796 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       10 SCHEM 5220.736 6.055798 0 USR MPRFLG 0 ; 
       11 SCHEM 5228.236 6.055798 0 USR MPRFLG 0 ; 
       12 SCHEM 5263.604 6.082497 0 USR MPRFLG 0 ; 
       13 SCHEM 5256.104 6.082497 0 USR MPRFLG 0 ; 
       14 SCHEM 5271.104 8.082476 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       15 SCHEM 5256.666 22.91645 0 MPRFLG 0 ; 
       16 SCHEM 5241.572 23.17121 0 USR SRT 2.44014 5.986466 4.925478 0 0 0 0 -7.86681 0 MPRFLG 0 ; 
       17 SCHEM 5244.46 20.99264 0 USR MPRFLG 0 ; 
       18 SCHEM 5276.104 23.17121 0 USR DISPLAY 0 0 SRT 3.363563 4.560029 1.620401 0 0 0 21.5662 -4.542652 0 MPRFLG 0 ; 
       19 SCHEM 5278.604 23.17121 0 USR DISPLAY 0 0 SRT 3.363563 4.560029 1.620401 0 0 0 21.5662 -4.542652 0 MPRFLG 0 ; 
       20 SCHEM 5238.919 21.01114 0 USR MPRFLG 0 ; 
       21 SCHEM 5223.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       22 SCHEM 5258.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       23 SCHEM 5218.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       24 SCHEM 5220.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       25 SCHEM 5256.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       26 SCHEM 5230.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       27 SCHEM 5266.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       28 SCHEM 5225.736 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       29 SCHEM 5261.104 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       30 SCHEM 5228.236 4.055795 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       31 SCHEM 5263.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       32 SCHEM 5253.604 4.082494 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 5224.486 8.055796 0 USR MPRFLG 0 ; 
       34 SCHEM 5259.854 8.082476 0 USR MPRFLG 0 ; 
       35 SCHEM 5273.604 23.17121 0 USR DISPLAY 0 0 SRT 2.394208 5.873782 4.832765 0 0 0 0 -7.86681 0 MPRFLG 0 ; 
       38 SCHEM 5259.938 -0.04052886 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 31.39535 0 MPRFLG 0 ; 
       39 SCHEM 5247.658 2.975847 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -5.684342e-014 -9.809725 1.062176e-007 MPRFLG 0 ; 
       40 SCHEM 5210.621 14.92957 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       41 SCHEM 5256.188 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 5258.688 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 5261.188 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 5263.688 -2.040536 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 5248.908 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 5246.408 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 5243.908 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 5241.408 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 5199.371 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 5251.408 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 5253.908 0.9758446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 5266.263 -2.047296 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 5268.843 -2.014589 0 USR WIRECOL 2 7 DISPLAY 0 0 SRT 1 1 1 0 0 0 -15.6195 -37.37888 0.01068692 MPRFLG 0 ; 
       54 SCHEM 5201.871 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 5204.371 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 5206.871 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 5209.371 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 5211.871 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       59 SCHEM 5214.371 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 5216.871 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 5219.371 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 5221.871 12.92958 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 5058.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5241.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5060.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5244.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5055.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5063.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5065.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5068.109 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5070.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5249.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5251.666 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5254.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5239.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5246.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5035.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4945.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4948.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4950.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4953.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4623.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4625.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4628.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4630.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4511.444 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 5231.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5226.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5229.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5224.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 5219.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5221.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4651.814 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 5030.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 4656.814 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4978.868 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 4983.868 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 4530.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5025.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 4535.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 4540.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4545.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4524.109 2.160294 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4552.568 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4552.568 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4555.068 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4565.068 18.73772 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 5028.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 5038.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 5033.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 2317.726 -0.7232437 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 2322.726 -0.7232437 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       2 SCHEM 5065.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5068.109 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5070.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5249.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 5251.666 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 5254.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       0 SCHEM 2317.726 -2.723244 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 2322.726 -2.723244 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
