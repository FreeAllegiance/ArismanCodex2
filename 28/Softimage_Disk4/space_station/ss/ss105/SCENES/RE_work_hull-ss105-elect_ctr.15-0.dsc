SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.61-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 84     
       ss105-cube25.13-0 ; 
       ss105-east_bay_11_10.1-0 ; 
       ss105-east_bay_11_6.1-0 ; 
       ss105-east_bay_11_8.1-0 ; 
       ss105-east_bay_11_9.1-0 ; 
       ss105-extru32_1.1-0 ; 
       ss105-extru32_2.1-0 ; 
       ss105-extru32_3.1-0 ; 
       ss105-extru32_4.1-0 ; 
       ss105-extru39_10.1-0 ; 
       ss105-extru39_11.1-0 ; 
       ss105-extru39_8.1-0 ; 
       ss105-extru39_9.1-0 ; 
       ss105-extru63.1-0 ; 
       ss105-extru64.1-0 ; 
       ss105-extru69.1-0 ; 
       ss105-extru70.1-0 ; 
       ss105-extru71.1-0 ; 
       ss105-extru72.1-0 ; 
       ss105-extru73.1-0 ; 
       ss105-extru74.1-0 ; 
       ss105-garage1A.1-0 ; 
       ss105-garage1B.1-0 ; 
       ss105-garage1C.1-0 ; 
       ss105-garage1D.1-0 ; 
       ss105-garage1E.1-0 ; 
       ss105-garage2A.1-0 ; 
       ss105-garage2B.1-0 ; 
       ss105-garage2C.1-0 ; 
       ss105-garage2D.1-0 ; 
       ss105-garage2E.1-0 ; 
       ss105-launch1.1-0 ; 
       ss105-launch2.1-0 ; 
       ss105-newHull.1-0 ; 
       ss105-sphere14.1-0 ; 
       ss105-sphere15.1-0 ; 
       ss105-sphere16.1-0 ; 
       ss105-sphere17.1-0 ; 
       ss105-SS_01.1-0 ; 
       ss105-SS_02.1-0 ; 
       ss105-SS_03.1-0 ; 
       ss105-SS_04.1-0 ; 
       ss105-SS_05.1-0 ; 
       ss105-SS_20.1-0 ; 
       ss105-SS_21.1-0 ; 
       ss105-SS_22.1-0 ; 
       ss105-SS_23_4.1-0 ; 
       ss105-SS_29.1-0 ; 
       ss105-SS_30.1-0 ; 
       ss105-SS_31.1-0 ; 
       ss105-SS_32.1-0 ; 
       ss105-SS_33.1-0 ; 
       ss105-SS_34.1-0 ; 
       ss105-SS_35.1-0 ; 
       ss105-SS_36.1-0 ; 
       ss105-SS_60.1-0 ; 
       ss105-SS_61.1-0 ; 
       ss105-SS_62.1-0 ; 
       ss105-SS_63.1-0 ; 
       ss105-SS_64.1-0 ; 
       ss105-SS_65.1-0 ; 
       ss105-SS_66.1-0 ; 
       ss105-SS_67.1-0 ; 
       ss105-SS_68.1-0 ; 
       ss105-SS_69.1-0 ; 
       ss105-SS_70.1-0 ; 
       ss105-SS_71.1-0 ; 
       ss105-SS_72.1-0 ; 
       ss105-SS_73.1-0 ; 
       ss105-SS_74.1-0 ; 
       ss105-SS_75.1-0 ; 
       ss105-SS_76.1-0 ; 
       ss105-SS_77.1-0 ; 
       ss105-SS_78.1-0 ; 
       ss105-SS_79.1-0 ; 
       ss105-SS_80.1-0 ; 
       ss105-SS_81.1-0 ; 
       ss105-SS_82.1-0 ; 
       ss105-SS_83.1-0 ; 
       ss105-ss105.7-0 ROOT ; 
       ss105-tetra10.1-0 ; 
       ss105-tetra11.1-0 ; 
       ss105-tetra12.1-0 ; 
       ss105-tetra13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       RE_work_hull-ss105-elect_ctr.15-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 79 110 ; 
       1 79 110 ; 
       2 79 110 ; 
       3 79 110 ; 
       4 79 110 ; 
       5 79 110 ; 
       6 79 110 ; 
       7 79 110 ; 
       8 79 110 ; 
       9 79 110 ; 
       10 79 110 ; 
       11 79 110 ; 
       12 79 110 ; 
       13 79 110 ; 
       14 79 110 ; 
       15 79 110 ; 
       16 79 110 ; 
       17 79 110 ; 
       18 79 110 ; 
       19 79 110 ; 
       20 79 110 ; 
       21 79 110 ; 
       22 79 110 ; 
       23 79 110 ; 
       24 79 110 ; 
       25 79 110 ; 
       26 79 110 ; 
       27 79 110 ; 
       28 79 110 ; 
       29 79 110 ; 
       30 79 110 ; 
       31 79 110 ; 
       32 79 110 ; 
       34 11 110 ; 
       35 12 110 ; 
       36 9 110 ; 
       37 10 110 ; 
       38 79 110 ; 
       39 79 110 ; 
       40 79 110 ; 
       41 79 110 ; 
       42 79 110 ; 
       43 79 110 ; 
       44 79 110 ; 
       45 79 110 ; 
       46 79 110 ; 
       47 79 110 ; 
       48 79 110 ; 
       49 79 110 ; 
       50 79 110 ; 
       51 79 110 ; 
       52 79 110 ; 
       53 79 110 ; 
       54 79 110 ; 
       55 2 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       61 3 110 ; 
       62 3 110 ; 
       63 3 110 ; 
       64 3 110 ; 
       65 3 110 ; 
       66 3 110 ; 
       67 1 110 ; 
       68 1 110 ; 
       69 1 110 ; 
       70 1 110 ; 
       71 1 110 ; 
       72 1 110 ; 
       73 4 110 ; 
       74 4 110 ; 
       75 4 110 ; 
       76 4 110 ; 
       77 4 110 ; 
       78 4 110 ; 
       80 11 110 ; 
       81 12 110 ; 
       82 9 110 ; 
       83 10 110 ; 
       33 79 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 6.25 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 42.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 85 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 210 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 20 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 252.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 255 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 257.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 260 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 150 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 162.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 125 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 137.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 180 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 172.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 195 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 187.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 232.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 225 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 240 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 247.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 267.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 270 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 272.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 275 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 277.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 280 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 282.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 285 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 287.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 290 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 262.5 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 265 -2 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 120 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 132.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 145 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 157.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 117.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 107.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 110 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 112.5 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 115 -2 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 105 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 97.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 100 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 102.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 62.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 55 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 57.5 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 60 -2 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 72.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 65 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 67.5 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 70 -2 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 87.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 82.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 85 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 80 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 75 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 77.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 212.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 207.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 210 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 205 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 200 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 202.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM 45 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM 40 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM 42.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM 37.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM 32.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM 35 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM 22.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM 17.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM 20 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM 15 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM 10 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM 12.5 -4 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 146.25 0 0 DISPLAY 3 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       80 SCHEM 122.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 135 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 147.5 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 160 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 2.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
