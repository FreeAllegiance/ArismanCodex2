SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.68-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 84     
       ss105-cube25.13-0 ; 
       ss105-east_bay_11_10.1-0 ; 
       ss105-east_bay_11_6.1-0 ; 
       ss105-east_bay_11_8.1-0 ; 
       ss105-east_bay_11_9.1-0 ; 
       ss105-extru32_1.1-0 ; 
       ss105-extru32_2.1-0 ; 
       ss105-extru32_3.1-0 ; 
       ss105-extru32_4.1-0 ; 
       ss105-extru39_10.1-0 ; 
       ss105-extru39_11.1-0 ; 
       ss105-extru39_8.1-0 ; 
       ss105-extru39_9.1-0 ; 
       ss105-extru63.1-0 ; 
       ss105-extru64.1-0 ; 
       ss105-extru69.1-0 ; 
       ss105-extru70.1-0 ; 
       ss105-extru71.1-0 ; 
       ss105-extru72.1-0 ; 
       ss105-extru73.1-0 ; 
       ss105-extru74.1-0 ; 
       ss105-garage1A.1-0 ; 
       ss105-garage1B.1-0 ; 
       ss105-garage1C.1-0 ; 
       ss105-garage1D.1-0 ; 
       ss105-garage1E.1-0 ; 
       ss105-garage2A.1-0 ; 
       ss105-garage2B.1-0 ; 
       ss105-garage2C.1-0 ; 
       ss105-garage2D.1-0 ; 
       ss105-garage2E.1-0 ; 
       ss105-launch1.1-0 ; 
       ss105-launch2.1-0 ; 
       ss105-newHull.1-0 ; 
       ss105-sphere14.1-0 ; 
       ss105-sphere15.1-0 ; 
       ss105-sphere16.1-0 ; 
       ss105-sphere17.1-0 ; 
       ss105-SS_01.1-0 ; 
       ss105-SS_02.1-0 ; 
       ss105-SS_03.1-0 ; 
       ss105-SS_04.1-0 ; 
       ss105-SS_05.1-0 ; 
       ss105-SS_20.1-0 ; 
       ss105-SS_21.1-0 ; 
       ss105-SS_22.1-0 ; 
       ss105-SS_23_4.1-0 ; 
       ss105-SS_29.1-0 ; 
       ss105-SS_30.1-0 ; 
       ss105-SS_31.1-0 ; 
       ss105-SS_32.1-0 ; 
       ss105-SS_33.1-0 ; 
       ss105-SS_34.1-0 ; 
       ss105-SS_35.1-0 ; 
       ss105-SS_36.1-0 ; 
       ss105-SS_60.1-0 ; 
       ss105-SS_61.1-0 ; 
       ss105-SS_62.1-0 ; 
       ss105-SS_63.1-0 ; 
       ss105-SS_64.1-0 ; 
       ss105-SS_65.1-0 ; 
       ss105-SS_66.1-0 ; 
       ss105-SS_67.1-0 ; 
       ss105-SS_68.1-0 ; 
       ss105-SS_69.1-0 ; 
       ss105-SS_70.1-0 ; 
       ss105-SS_71.1-0 ; 
       ss105-SS_72.1-0 ; 
       ss105-SS_73.1-0 ; 
       ss105-SS_74.1-0 ; 
       ss105-SS_75.1-0 ; 
       ss105-SS_76.1-0 ; 
       ss105-SS_77.1-0 ; 
       ss105-SS_78.1-0 ; 
       ss105-SS_79.1-0 ; 
       ss105-SS_80.1-0 ; 
       ss105-SS_81.1-0 ; 
       ss105-SS_82.1-0 ; 
       ss105-SS_83.1-0 ; 
       ss105-ss105.11-0 ROOT ; 
       ss105-tetra10.1-0 ; 
       ss105-tetra11.1-0 ; 
       ss105-tetra12.1-0 ; 
       ss105-tetra13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ROTATE-ss105-elect_ctr.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 79 110 ; 
       1 79 110 ; 
       2 79 110 ; 
       3 79 110 ; 
       4 79 110 ; 
       5 79 110 ; 
       6 79 110 ; 
       7 79 110 ; 
       8 79 110 ; 
       9 79 110 ; 
       10 79 110 ; 
       11 79 110 ; 
       12 79 110 ; 
       13 79 110 ; 
       14 79 110 ; 
       15 79 110 ; 
       16 79 110 ; 
       17 79 110 ; 
       18 79 110 ; 
       19 79 110 ; 
       20 79 110 ; 
       21 79 110 ; 
       22 79 110 ; 
       23 79 110 ; 
       24 79 110 ; 
       25 79 110 ; 
       26 79 110 ; 
       27 79 110 ; 
       28 79 110 ; 
       29 79 110 ; 
       30 79 110 ; 
       31 79 110 ; 
       32 79 110 ; 
       33 79 110 ; 
       34 11 110 ; 
       35 12 110 ; 
       36 9 110 ; 
       37 10 110 ; 
       38 79 110 ; 
       39 79 110 ; 
       40 79 110 ; 
       41 79 110 ; 
       42 79 110 ; 
       43 79 110 ; 
       44 79 110 ; 
       45 79 110 ; 
       46 79 110 ; 
       47 79 110 ; 
       48 79 110 ; 
       49 79 110 ; 
       50 79 110 ; 
       51 79 110 ; 
       52 79 110 ; 
       53 79 110 ; 
       54 79 110 ; 
       55 2 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       61 3 110 ; 
       62 3 110 ; 
       63 3 110 ; 
       64 3 110 ; 
       65 3 110 ; 
       66 3 110 ; 
       67 1 110 ; 
       68 1 110 ; 
       69 1 110 ; 
       70 1 110 ; 
       71 1 110 ; 
       72 1 110 ; 
       73 4 110 ; 
       74 4 110 ; 
       75 4 110 ; 
       76 4 110 ; 
       77 4 110 ; 
       78 4 110 ; 
       80 11 110 ; 
       81 12 110 ; 
       82 9 110 ; 
       83 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 85.68861 -4.185699 0 USR MPRFLG 0 ; 
       1 SCHEM 59.97376 -53.65053 0 USR MPRFLG 0 ; 
       2 SCHEM 94.97378 -53.65053 0 USR MPRFLG 0 ; 
       3 SCHEM 67.92704 -20.09227 0 USR MPRFLG 0 ; 
       4 SCHEM 44.97377 -53.65053 0 USR MPRFLG 0 ; 
       5 SCHEM 112.6482 -22.42676 0 USR MPRFLG 0 ; 
       6 SCHEM 115.1482 -22.42676 0 USR MPRFLG 0 ; 
       7 SCHEM 117.6482 -22.42676 0 USR MPRFLG 0 ; 
       8 SCHEM 120.1482 -22.42676 0 USR MPRFLG 0 ; 
       9 SCHEM 106.25 -2 0 MPRFLG 0 ; 
       10 SCHEM 111.25 -2 0 MPRFLG 0 ; 
       11 SCHEM 96.25 -2 0 MPRFLG 0 ; 
       12 SCHEM 101.25 -2 0 MPRFLG 0 ; 
       13 SCHEM 80.1482 -22.42676 0 USR MPRFLG 0 ; 
       14 SCHEM 77.6482 -22.42676 0 USR MPRFLG 0 ; 
       15 SCHEM 85.1482 -22.42676 0 USR MPRFLG 0 ; 
       16 SCHEM 82.6482 -22.42676 0 USR MPRFLG 0 ; 
       17 SCHEM 105.1482 -22.42676 0 USR MPRFLG 0 ; 
       18 SCHEM 102.6482 -22.42676 0 USR MPRFLG 0 ; 
       19 SCHEM 107.6482 -22.42676 0 USR MPRFLG 0 ; 
       20 SCHEM 110.1482 -22.42676 0 USR MPRFLG 0 ; 
       21 SCHEM 127.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       22 SCHEM 130.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 132.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 135.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 137.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 140.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 142.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 145.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 147.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 150.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 122.6482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 125.1482 -22.42676 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 33.72377 -53.65053 0 USR MPRFLG 0 ; 
       34 SCHEM 95 -4 0 MPRFLG 0 ; 
       35 SCHEM 100 -4 0 MPRFLG 0 ; 
       36 SCHEM 105 -4 0 MPRFLG 0 ; 
       37 SCHEM 110 -4 0 MPRFLG 0 ; 
       38 SCHEM 123.7238 -53.65053 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 113.7238 -53.65053 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 116.2238 -53.65053 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 118.7238 -53.65053 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 121.2238 -53.65053 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 111.2238 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       44 SCHEM 103.7238 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       45 SCHEM 106.2238 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       46 SCHEM 108.7238 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       47 SCHEM 76.22376 -53.65053 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       48 SCHEM 68.72375 -53.65053 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       49 SCHEM 71.22375 -53.65053 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       50 SCHEM 73.72376 -53.65053 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       51 SCHEM 86.22378 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       52 SCHEM 78.72376 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       53 SCHEM 81.22378 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       54 SCHEM 83.72378 -53.65053 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       55 SCHEM 101.2238 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 96.22378 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 98.72378 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 93.72378 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       59 SCHEM 88.72378 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       60 SCHEM 91.22378 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       61 SCHEM 74.17704 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       62 SCHEM 69.17704 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       63 SCHEM 71.67704 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       64 SCHEM 66.67704 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       65 SCHEM 61.67704 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 64.17705 -22.09227 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 66.22375 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 61.22377 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       69 SCHEM 63.72376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       70 SCHEM 58.72376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       71 SCHEM 53.72375 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       72 SCHEM 56.22376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       73 SCHEM 51.22375 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 46.22377 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 48.72376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 43.72376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 38.72377 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 41.22376 -55.65053 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 95 0 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       80 SCHEM 97.5 -4 0 MPRFLG 0 ; 
       81 SCHEM 102.5 -4 0 MPRFLG 0 ; 
       82 SCHEM 107.5 -4 0 MPRFLG 0 ; 
       83 SCHEM 112.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
