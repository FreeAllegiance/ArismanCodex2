SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.38-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 36     
       elect_ctr-cube25.1-0 ; 
       elect_ctr-east_bay_11_10.1-0 ; 
       elect_ctr-east_bay_11_6.1-0 ; 
       elect_ctr-east_bay_11_8.1-0 ; 
       elect_ctr-east_bay_11_9.1-0 ; 
       elect_ctr-extru32_1.1-0 ; 
       elect_ctr-extru32_2.1-0 ; 
       elect_ctr-extru32_3.1-0 ; 
       elect_ctr-extru32_4.1-0 ; 
       elect_ctr-extru39_10.1-0 ; 
       elect_ctr-extru39_11.1-0 ; 
       elect_ctr-extru39_8.1-0 ; 
       elect_ctr-extru39_9.1-0 ; 
       elect_ctr-extru63.1-0 ; 
       elect_ctr-extru64.1-0 ; 
       elect_ctr-extru69.1-0 ; 
       elect_ctr-extru70.1-0 ; 
       elect_ctr-extru71.1-0 ; 
       elect_ctr-extru72.1-0 ; 
       elect_ctr-extru73.1-0 ; 
       elect_ctr-extru74.1-0 ; 
       elect_ctr-null24.1-0 ; 
       elect_ctr-null27.1-0 ; 
       elect_ctr-null28.1-0 ; 
       elect_ctr-null30.1-0 ; 
       elect_ctr-null31.24-0 ROOT ; 
       elect_ctr-sphere14.1-0 ; 
       elect_ctr-sphere15.1-0 ; 
       elect_ctr-sphere16.1-0 ; 
       elect_ctr-sphere17.1-0 ; 
       elect_ctr-sphere2.2-0 ; 
       elect_ctr-tetra10.1-0 ; 
       elect_ctr-tetra11.1-0 ; 
       elect_ctr-tetra12.1-0 ; 
       elect_ctr-tetra13.1-0 ; 
       elect_ctr-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss105-elect_ctr.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 30 110 ; 
       1 21 110 ; 
       2 21 110 ; 
       3 21 110 ; 
       4 21 110 ; 
       5 24 110 ; 
       6 24 110 ; 
       7 24 110 ; 
       8 24 110 ; 
       9 22 110 ; 
       10 22 110 ; 
       11 22 110 ; 
       12 22 110 ; 
       13 23 110 ; 
       14 13 110 ; 
       15 23 110 ; 
       16 15 110 ; 
       17 23 110 ; 
       18 17 110 ; 
       19 23 110 ; 
       20 19 110 ; 
       21 25 110 ; 
       22 30 110 ; 
       23 30 110 ; 
       24 30 110 ; 
       26 31 110 ; 
       27 32 110 ; 
       28 33 110 ; 
       29 34 110 ; 
       30 25 110 ; 
       31 11 110 ; 
       32 12 110 ; 
       33 9 110 ; 
       34 10 110 ; 
       35 30 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 45 -4 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       6 SCHEM 35 -6 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 40 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       15 SCHEM 25 -6 0 MPRFLG 0 ; 
       16 SCHEM 25 -8 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 27.5 -8 0 MPRFLG 0 ; 
       19 SCHEM 30 -6 0 MPRFLG 0 ; 
       20 SCHEM 30 -8 0 MPRFLG 0 ; 
       21 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       22 SCHEM 16.25 -4 0 MPRFLG 0 ; 
       23 SCHEM 26.25 -4 0 MPRFLG 0 ; 
       24 SCHEM 36.25 -4 0 MPRFLG 0 ; 
       25 SCHEM 23.75 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       27 SCHEM 15 -10 0 MPRFLG 0 ; 
       28 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       29 SCHEM 20 -10 0 MPRFLG 0 ; 
       30 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       32 SCHEM 15 -8 0 MPRFLG 0 ; 
       33 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 20 -8 0 MPRFLG 0 ; 
       35 SCHEM 42.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
