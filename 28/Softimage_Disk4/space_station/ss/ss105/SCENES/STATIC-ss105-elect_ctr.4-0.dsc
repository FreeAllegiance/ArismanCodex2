SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.65-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 27     
       ss105-cube25.13-0 ; 
       ss105-east_bay_11_10.1-0 ; 
       ss105-east_bay_11_6.1-0 ; 
       ss105-east_bay_11_8.1-0 ; 
       ss105-east_bay_11_9.1-0 ; 
       ss105-extru32_1.1-0 ; 
       ss105-extru32_2.1-0 ; 
       ss105-extru32_3.1-0 ; 
       ss105-extru32_4.1-0 ; 
       ss105-extru39_10.1-0 ; 
       ss105-extru39_11.1-0 ; 
       ss105-extru39_8.1-0 ; 
       ss105-extru39_9.1-0 ; 
       ss105-extru63.1-0 ; 
       ss105-extru64.1-0 ; 
       ss105-extru69.1-0 ; 
       ss105-extru70.1-0 ; 
       ss105-extru71.1-0 ; 
       ss105-extru72.1-0 ; 
       ss105-extru73.1-0 ; 
       ss105-extru74.1-0 ; 
       ss105-newHull.1-0 ; 
       ss105-sphere14.1-0 ; 
       ss105-sphere15.1-0 ; 
       ss105-sphere16.1-0 ; 
       ss105-sphere17.1-0 ; 
       ss105-ss105.10-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss105-elect_ctr.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 26 110 ; 
       2 26 110 ; 
       3 26 110 ; 
       4 26 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       14 26 110 ; 
       15 26 110 ; 
       16 26 110 ; 
       17 26 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       22 11 110 ; 
       23 12 110 ; 
       24 9 110 ; 
       25 10 110 ; 
       21 26 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 10 -6 0 MPRFLG 0 ; 
       3 SCHEM 32.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 5 -6 0 MPRFLG 0 ; 
       5 SCHEM 45 -6 0 MPRFLG 0 ; 
       6 SCHEM 47.5 -6 0 MPRFLG 0 ; 
       7 SCHEM 50 -6 0 MPRFLG 0 ; 
       8 SCHEM 52.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       10 SCHEM 20 -6 0 MPRFLG 0 ; 
       11 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       12 SCHEM 15 -6 0 MPRFLG 0 ; 
       13 SCHEM 25 -6 0 MPRFLG 0 ; 
       14 SCHEM 22.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 30 -6 0 MPRFLG 0 ; 
       16 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       17 SCHEM 37.5 -6 0 MPRFLG 0 ; 
       18 SCHEM 35 -6 0 MPRFLG 0 ; 
       19 SCHEM 40 -6 0 MPRFLG 0 ; 
       20 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       22 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       23 SCHEM 15 -8 0 MPRFLG 0 ; 
       24 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       25 SCHEM 20 -8 0 MPRFLG 0 ; 
       26 SCHEM 26.25 -4 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       21 SCHEM 0 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
