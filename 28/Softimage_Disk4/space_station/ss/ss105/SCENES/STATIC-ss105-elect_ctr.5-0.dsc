SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.71-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 26     
       ss105-cube25.13-0 ; 
       ss105-east_bay_11_10.1-0 ; 
       ss105-east_bay_11_6.1-0 ; 
       ss105-east_bay_11_8.1-0 ; 
       ss105-east_bay_11_9.1-0 ; 
       ss105-extru32_1.1-0 ; 
       ss105-extru32_2.1-0 ; 
       ss105-extru32_3.1-0 ; 
       ss105-extru32_4.1-0 ; 
       ss105-extru39_10.1-0 ; 
       ss105-extru39_11.1-0 ; 
       ss105-extru39_8.1-0 ; 
       ss105-extru39_9.1-0 ; 
       ss105-extru63.1-0 ; 
       ss105-extru64.1-0 ; 
       ss105-extru69.1-0 ; 
       ss105-extru70.1-0 ; 
       ss105-extru71.1-0 ; 
       ss105-extru72.1-0 ; 
       ss105-extru73.1-0 ; 
       ss105-extru74.1-0 ; 
       ss105-sphere14.1-0 ; 
       ss105-sphere15.1-0 ; 
       ss105-sphere16.1-0 ; 
       ss105-sphere17.1-0 ; 
       ss105-ss105.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-ss105-elect_ctr.5-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 25 110 ; 
       1 25 110 ; 
       2 25 110 ; 
       3 25 110 ; 
       4 25 110 ; 
       5 25 110 ; 
       6 25 110 ; 
       7 25 110 ; 
       8 25 110 ; 
       9 25 110 ; 
       10 25 110 ; 
       11 25 110 ; 
       12 25 110 ; 
       13 25 110 ; 
       14 25 110 ; 
       15 25 110 ; 
       16 25 110 ; 
       17 25 110 ; 
       18 25 110 ; 
       19 25 110 ; 
       20 25 110 ; 
       21 11 110 ; 
       22 12 110 ; 
       23 9 110 ; 
       24 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 20 -2 0 MPRFLG 0 ; 
       1 SCHEM 5 -2 0 MPRFLG 0 ; 
       2 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 42.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 45 -2 0 MPRFLG 0 ; 
       8 SCHEM 50 -2 0 MPRFLG 0 ; 
       9 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       11 SCHEM 32.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 40 -2 0 MPRFLG 0 ; 
       13 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       16 SCHEM 15 -2 0 MPRFLG 0 ; 
       17 SCHEM 27.5 -2 0 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 MPRFLG 0 ; 
       19 SCHEM 30 -2 0 MPRFLG 0 ; 
       20 SCHEM 35 -2 0 MPRFLG 0 ; 
       21 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 40 -4 0 MPRFLG 0 ; 
       23 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       24 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       25 SCHEM 27.5 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
