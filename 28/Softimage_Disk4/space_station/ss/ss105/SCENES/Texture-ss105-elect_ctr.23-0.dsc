SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.25-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 104     
       elect_ctr-cube25.1-0 ; 
       elect_ctr-east_bay_11.3-0 ; 
       elect_ctr-east_bay_11_1.4-0 ; 
       elect_ctr-east_bay_11_2.3-0 ; 
       elect_ctr-east_bay_11_3.3-0 ; 
       elect_ctr-east_bay_11_6.1-0 ; 
       elect_ctr-east_bay_11_7.1-0 ; 
       elect_ctr-extru32_1.1-0 ; 
       elect_ctr-extru32_2.1-0 ; 
       elect_ctr-extru32_3.1-0 ; 
       elect_ctr-extru32_4.1-0 ; 
       elect_ctr-extru39_10.1-0 ; 
       elect_ctr-extru39_11.1-0 ; 
       elect_ctr-extru39_8.1-0 ; 
       elect_ctr-extru39_9.1-0 ; 
       elect_ctr-extru63.1-0 ; 
       elect_ctr-extru64.1-0 ; 
       elect_ctr-extru69.1-0 ; 
       elect_ctr-extru70.1-0 ; 
       elect_ctr-extru71.1-0 ; 
       elect_ctr-extru72.1-0 ; 
       elect_ctr-extru73.1-0 ; 
       elect_ctr-extru74.1-0 ; 
       elect_ctr-garage1A.1-0 ; 
       elect_ctr-garage1B.1-0 ; 
       elect_ctr-garage1C.1-0 ; 
       elect_ctr-garage1D.1-0 ; 
       elect_ctr-garage1E.1-0 ; 
       elect_ctr-garage2A.1-0 ; 
       elect_ctr-garage2B.1-0 ; 
       elect_ctr-garage2C.1-0 ; 
       elect_ctr-garage2D.1-0 ; 
       elect_ctr-garage2E.1-0 ; 
       elect_ctr-landing_lights.1-0 ; 
       elect_ctr-landing_lights_1.1-0 ; 
       elect_ctr-landing_lights_2.1-0 ; 
       elect_ctr-landing_lights_3.1-0 ; 
       elect_ctr-landing_lights2.1-0 ; 
       elect_ctr-landing_lights2_1.1-0 ; 
       elect_ctr-landing_lights2_2.1-0 ; 
       elect_ctr-landing_lights2_3.1-0 ; 
       elect_ctr-launch1.1-0 ; 
       elect_ctr-launch2.1-0 ; 
       elect_ctr-null18.1-0 ; 
       elect_ctr-null19.1-0 ; 
       elect_ctr-null19_1.1-0 ; 
       elect_ctr-null27.1-0 ; 
       elect_ctr-null28.1-0 ; 
       elect_ctr-null30.1-0 ; 
       elect_ctr-null31.17-0 ROOT ; 
       elect_ctr-null32.1-0 ; 
       elect_ctr-sphere14.1-0 ; 
       elect_ctr-sphere15.1-0 ; 
       elect_ctr-sphere16.1-0 ; 
       elect_ctr-sphere17.1-0 ; 
       elect_ctr-sphere2.2-0 ; 
       elect_ctr-SS_01.1-0 ; 
       elect_ctr-SS_02.1-0 ; 
       elect_ctr-SS_03.1-0 ; 
       elect_ctr-SS_04.1-0 ; 
       elect_ctr-SS_05.1-0 ; 
       elect_ctr-SS_20.1-0 ; 
       elect_ctr-SS_21.1-0 ; 
       elect_ctr-SS_22.1-0 ; 
       elect_ctr-SS_23_4.1-0 ; 
       elect_ctr-SS_29.1-0 ; 
       elect_ctr-SS_30.1-0 ; 
       elect_ctr-SS_31.1-0 ; 
       elect_ctr-SS_32.1-0 ; 
       elect_ctr-SS_33.1-0 ; 
       elect_ctr-SS_34.1-0 ; 
       elect_ctr-SS_35.1-0 ; 
       elect_ctr-SS_36.1-0 ; 
       elect_ctr-SS_60.1-0 ; 
       elect_ctr-SS_61.1-0 ; 
       elect_ctr-SS_62.1-0 ; 
       elect_ctr-SS_63.1-0 ; 
       elect_ctr-SS_64.1-0 ; 
       elect_ctr-SS_65.1-0 ; 
       elect_ctr-SS_66.1-0 ; 
       elect_ctr-SS_67.1-0 ; 
       elect_ctr-SS_68.1-0 ; 
       elect_ctr-SS_69.1-0 ; 
       elect_ctr-SS_70.1-0 ; 
       elect_ctr-SS_71.1-0 ; 
       elect_ctr-SS_72.1-0 ; 
       elect_ctr-SS_73.1-0 ; 
       elect_ctr-SS_74.1-0 ; 
       elect_ctr-SS_75.1-0 ; 
       elect_ctr-SS_76.1-0 ; 
       elect_ctr-SS_77.1-0 ; 
       elect_ctr-SS_78.1-0 ; 
       elect_ctr-SS_79.1-0 ; 
       elect_ctr-SS_80.1-0 ; 
       elect_ctr-SS_81.1-0 ; 
       elect_ctr-SS_82.1-0 ; 
       elect_ctr-SS_83.1-0 ; 
       elect_ctr-strobe_set.1-0 ; 
       elect_ctr-strobe_set_1.1-0 ; 
       elect_ctr-tetra10.1-0 ; 
       elect_ctr-tetra11.1-0 ; 
       elect_ctr-tetra12.1-0 ; 
       elect_ctr-tetra13.1-0 ; 
       elect_ctr-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture-ss105-elect_ctr.23-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 55 110 ; 
       1 49 110 ; 
       2 49 110 ; 
       3 49 110 ; 
       4 49 110 ; 
       5 55 110 ; 
       6 55 110 ; 
       7 48 110 ; 
       8 48 110 ; 
       9 48 110 ; 
       10 48 110 ; 
       11 46 110 ; 
       12 46 110 ; 
       13 46 110 ; 
       14 46 110 ; 
       15 47 110 ; 
       16 15 110 ; 
       17 47 110 ; 
       18 17 110 ; 
       19 47 110 ; 
       20 19 110 ; 
       21 47 110 ; 
       22 21 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 4 110 ; 
       31 4 110 ; 
       32 4 110 ; 
       33 3 110 ; 
       34 97 110 ; 
       35 98 110 ; 
       36 1 110 ; 
       37 98 110 ; 
       38 97 110 ; 
       39 3 110 ; 
       40 1 110 ; 
       41 1 110 ; 
       42 3 110 ; 
       43 49 110 ; 
       44 49 110 ; 
       45 49 110 ; 
       46 55 110 ; 
       47 55 110 ; 
       48 55 110 ; 
       50 49 110 ; 
       51 99 110 ; 
       52 100 110 ; 
       53 101 110 ; 
       54 102 110 ; 
       55 49 110 ; 
       56 50 110 ; 
       57 50 110 ; 
       58 50 110 ; 
       59 50 110 ; 
       60 50 110 ; 
       61 45 110 ; 
       62 45 110 ; 
       63 45 110 ; 
       64 45 110 ; 
       65 43 110 ; 
       66 43 110 ; 
       67 43 110 ; 
       68 43 110 ; 
       69 44 110 ; 
       70 44 110 ; 
       71 44 110 ; 
       72 44 110 ; 
       73 35 110 ; 
       74 35 110 ; 
       75 35 110 ; 
       76 37 110 ; 
       77 37 110 ; 
       78 37 110 ; 
       79 34 110 ; 
       80 34 110 ; 
       81 34 110 ; 
       82 38 110 ; 
       83 38 110 ; 
       84 38 110 ; 
       85 36 110 ; 
       86 36 110 ; 
       87 36 110 ; 
       88 40 110 ; 
       89 40 110 ; 
       90 40 110 ; 
       91 33 110 ; 
       92 33 110 ; 
       93 33 110 ; 
       94 39 110 ; 
       95 39 110 ; 
       96 39 110 ; 
       97 4 110 ; 
       98 2 110 ; 
       99 13 110 ; 
       100 14 110 ; 
       101 11 110 ; 
       102 12 110 ; 
       103 55 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 633.75 -8 0 MPRFLG 0 ; 
       1 SCHEM 462.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 395 -6 0 MPRFLG 0 ; 
       3 SCHEM 417.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 440 -6 0 MPRFLG 0 ; 
       5 SCHEM 505 -8 0 MPRFLG 0 ; 
       6 SCHEM 507.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 620 -10 0 MPRFLG 0 ; 
       8 SCHEM 622.5 -10 0 MPRFLG 0 ; 
       9 SCHEM 625 -10 0 MPRFLG 0 ; 
       10 SCHEM 627.5 -10 0 MPRFLG 0 ; 
       11 SCHEM 540 -10 0 MPRFLG 0 ; 
       12 SCHEM 552.5 -10 0 MPRFLG 0 ; 
       13 SCHEM 515 -10 0 MPRFLG 0 ; 
       14 SCHEM 527.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 566.25 -10 0 MPRFLG 0 ; 
       16 SCHEM 562.5 -12 0 MPRFLG 0 ; 
       17 SCHEM 581.25 -10 0 MPRFLG 0 ; 
       18 SCHEM 577.5 -12 0 MPRFLG 0 ; 
       19 SCHEM 596.25 -10 0 MPRFLG 0 ; 
       20 SCHEM 592.5 -12 0 MPRFLG 0 ; 
       21 SCHEM 611.25 -10 0 MPRFLG 0 ; 
       22 SCHEM 607.5 -12 0 MPRFLG 0 ; 
       23 SCHEM 407.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 382.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 385 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 387.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 390 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 452.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 427.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 430 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 432.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 435 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 422.5 -8 0 MPRFLG 0 ; 
       34 SCHEM 447.5 -10 0 MPRFLG 0 ; 
       35 SCHEM 402.5 -10 0 MPRFLG 0 ; 
       36 SCHEM 467.5 -8 0 MPRFLG 0 ; 
       37 SCHEM 395 -10 0 MPRFLG 0 ; 
       38 SCHEM 440 -10 0 MPRFLG 0 ; 
       39 SCHEM 412.5 -8 0 MPRFLG 0 ; 
       40 SCHEM 457.5 -8 0 MPRFLG 0 ; 
       41 SCHEM 462.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       42 SCHEM 417.5 -8 0 WIRECOL 9 7 MPRFLG 0 ; 
       43 SCHEM 376.25 -6 0 MPRFLG 0 ; 
       44 SCHEM 476.25 -6 0 MPRFLG 0 ; 
       45 SCHEM 486.25 -6 0 MPRFLG 0 ; 
       46 SCHEM 533.75 -8 0 MPRFLG 0 ; 
       47 SCHEM 588.75 -8 0 MPRFLG 0 ; 
       48 SCHEM 623.75 -8 0 MPRFLG 0 ; 
       49 SCHEM 507.5 -4 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       50 SCHEM 497.5 -6 0 MPRFLG 0 ; 
       51 SCHEM 510 -14 0 MPRFLG 0 ; 
       52 SCHEM 522.5 -14 0 MPRFLG 0 ; 
       53 SCHEM 535 -14 0 MPRFLG 0 ; 
       54 SCHEM 547.5 -14 0 MPRFLG 0 ; 
       55 SCHEM 573.75 -6 0 MPRFLG 0 ; 
       56 SCHEM 502.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 492.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 495 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 497.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 500 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 490 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 482.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 485 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 487.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 380 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       66 SCHEM 372.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       67 SCHEM 375 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 377.5 -8 0 WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 480 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       70 SCHEM 472.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       71 SCHEM 475 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 477.5 -8 0 WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 405 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       74 SCHEM 400 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       75 SCHEM 402.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 397.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 392.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 395 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 450 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 445 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 447.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 442.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 437.5 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 440 -12 0 WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 470 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 465 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 467.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 460 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 455 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 457.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 425 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       92 SCHEM 420 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 422.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       94 SCHEM 415 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       95 SCHEM 410 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       96 SCHEM 412.5 -10 0 WIRECOL 3 7 MPRFLG 0 ; 
       97 SCHEM 443.75 -8 0 MPRFLG 0 ; 
       98 SCHEM 398.75 -8 0 MPRFLG 0 ; 
       99 SCHEM 511.25 -12 0 MPRFLG 0 ; 
       100 SCHEM 523.75 -12 0 MPRFLG 0 ; 
       101 SCHEM 536.25 -12 0 MPRFLG 0 ; 
       102 SCHEM 548.75 -12 0 MPRFLG 0 ; 
       103 SCHEM 630 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
