SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.31-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 107     
       elect_ctr-cube25.1-0 ; 
       elect_ctr-east_bay_11.3-0 ; 
       elect_ctr-east_bay_11_1.4-0 ; 
       elect_ctr-east_bay_11_10.1-0 ; 
       elect_ctr-east_bay_11_2.3-0 ; 
       elect_ctr-east_bay_11_3.3-0 ; 
       elect_ctr-east_bay_11_6.1-0 ; 
       elect_ctr-east_bay_11_8.1-0 ; 
       elect_ctr-east_bay_11_9.1-0 ; 
       elect_ctr-extru32_1.1-0 ; 
       elect_ctr-extru32_2.1-0 ; 
       elect_ctr-extru32_3.1-0 ; 
       elect_ctr-extru32_4.1-0 ; 
       elect_ctr-extru39_10.1-0 ; 
       elect_ctr-extru39_11.1-0 ; 
       elect_ctr-extru39_8.1-0 ; 
       elect_ctr-extru39_9.1-0 ; 
       elect_ctr-extru63.1-0 ; 
       elect_ctr-extru64.1-0 ; 
       elect_ctr-extru69.1-0 ; 
       elect_ctr-extru70.1-0 ; 
       elect_ctr-extru71.1-0 ; 
       elect_ctr-extru72.1-0 ; 
       elect_ctr-extru73.1-0 ; 
       elect_ctr-extru74.1-0 ; 
       elect_ctr-garage1A.1-0 ; 
       elect_ctr-garage1B.1-0 ; 
       elect_ctr-garage1C.1-0 ; 
       elect_ctr-garage1D.1-0 ; 
       elect_ctr-garage1E.1-0 ; 
       elect_ctr-garage2A.1-0 ; 
       elect_ctr-garage2B.1-0 ; 
       elect_ctr-garage2C.1-0 ; 
       elect_ctr-garage2D.1-0 ; 
       elect_ctr-garage2E.1-0 ; 
       elect_ctr-landing_lights.1-0 ; 
       elect_ctr-landing_lights_1.1-0 ; 
       elect_ctr-landing_lights_2.1-0 ; 
       elect_ctr-landing_lights_3.1-0 ; 
       elect_ctr-landing_lights2.1-0 ; 
       elect_ctr-landing_lights2_1.1-0 ; 
       elect_ctr-landing_lights2_2.1-0 ; 
       elect_ctr-landing_lights2_3.1-0 ; 
       elect_ctr-launch1.1-0 ; 
       elect_ctr-launch2.1-0 ; 
       elect_ctr-null18.1-0 ; 
       elect_ctr-null19.1-0 ; 
       elect_ctr-null19_1.1-0 ; 
       elect_ctr-null24.1-0 ; 
       elect_ctr-null27.1-0 ; 
       elect_ctr-null28.1-0 ; 
       elect_ctr-null30.1-0 ; 
       elect_ctr-null31.20-0 ROOT ; 
       elect_ctr-null32.1-0 ; 
       elect_ctr-sphere14.1-0 ; 
       elect_ctr-sphere15.1-0 ; 
       elect_ctr-sphere16.1-0 ; 
       elect_ctr-sphere17.1-0 ; 
       elect_ctr-sphere2.2-0 ; 
       elect_ctr-SS_01.1-0 ; 
       elect_ctr-SS_02.1-0 ; 
       elect_ctr-SS_03.1-0 ; 
       elect_ctr-SS_04.1-0 ; 
       elect_ctr-SS_05.1-0 ; 
       elect_ctr-SS_20.1-0 ; 
       elect_ctr-SS_21.1-0 ; 
       elect_ctr-SS_22.1-0 ; 
       elect_ctr-SS_23_4.1-0 ; 
       elect_ctr-SS_29.1-0 ; 
       elect_ctr-SS_30.1-0 ; 
       elect_ctr-SS_31.1-0 ; 
       elect_ctr-SS_32.1-0 ; 
       elect_ctr-SS_33.1-0 ; 
       elect_ctr-SS_34.1-0 ; 
       elect_ctr-SS_35.1-0 ; 
       elect_ctr-SS_36.1-0 ; 
       elect_ctr-SS_60.1-0 ; 
       elect_ctr-SS_61.1-0 ; 
       elect_ctr-SS_62.1-0 ; 
       elect_ctr-SS_63.1-0 ; 
       elect_ctr-SS_64.1-0 ; 
       elect_ctr-SS_65.1-0 ; 
       elect_ctr-SS_66.1-0 ; 
       elect_ctr-SS_67.1-0 ; 
       elect_ctr-SS_68.1-0 ; 
       elect_ctr-SS_69.1-0 ; 
       elect_ctr-SS_70.1-0 ; 
       elect_ctr-SS_71.1-0 ; 
       elect_ctr-SS_72.1-0 ; 
       elect_ctr-SS_73.1-0 ; 
       elect_ctr-SS_74.1-0 ; 
       elect_ctr-SS_75.1-0 ; 
       elect_ctr-SS_76.1-0 ; 
       elect_ctr-SS_77.1-0 ; 
       elect_ctr-SS_78.1-0 ; 
       elect_ctr-SS_79.1-0 ; 
       elect_ctr-SS_80.1-0 ; 
       elect_ctr-SS_81.1-0 ; 
       elect_ctr-SS_82.1-0 ; 
       elect_ctr-SS_83.1-0 ; 
       elect_ctr-strobe_set.1-0 ; 
       elect_ctr-strobe_set_1.1-0 ; 
       elect_ctr-tetra10.1-0 ; 
       elect_ctr-tetra11.1-0 ; 
       elect_ctr-tetra12.1-0 ; 
       elect_ctr-tetra13.1-0 ; 
       elect_ctr-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Texture_bays-ss105-elect_ctr.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 58 110 ; 
       1 52 110 ; 
       2 52 110 ; 
       3 48 110 ; 
       4 52 110 ; 
       5 52 110 ; 
       6 48 110 ; 
       7 48 110 ; 
       8 48 110 ; 
       9 51 110 ; 
       10 51 110 ; 
       11 51 110 ; 
       12 51 110 ; 
       13 49 110 ; 
       14 49 110 ; 
       15 49 110 ; 
       16 49 110 ; 
       17 50 110 ; 
       18 17 110 ; 
       19 50 110 ; 
       20 19 110 ; 
       21 50 110 ; 
       22 21 110 ; 
       23 50 110 ; 
       24 23 110 ; 
       25 2 110 ; 
       26 2 110 ; 
       27 2 110 ; 
       28 2 110 ; 
       29 2 110 ; 
       30 5 110 ; 
       31 5 110 ; 
       32 5 110 ; 
       33 5 110 ; 
       34 5 110 ; 
       35 4 110 ; 
       36 100 110 ; 
       37 101 110 ; 
       38 1 110 ; 
       39 101 110 ; 
       40 100 110 ; 
       41 4 110 ; 
       42 1 110 ; 
       43 1 110 ; 
       44 4 110 ; 
       45 52 110 ; 
       46 52 110 ; 
       47 52 110 ; 
       48 52 110 ; 
       49 58 110 ; 
       50 58 110 ; 
       51 58 110 ; 
       53 52 110 ; 
       54 102 110 ; 
       55 103 110 ; 
       56 104 110 ; 
       57 105 110 ; 
       58 52 110 ; 
       59 53 110 ; 
       60 53 110 ; 
       61 53 110 ; 
       62 53 110 ; 
       63 53 110 ; 
       64 47 110 ; 
       65 47 110 ; 
       66 47 110 ; 
       67 47 110 ; 
       68 45 110 ; 
       69 45 110 ; 
       70 45 110 ; 
       71 45 110 ; 
       72 46 110 ; 
       73 46 110 ; 
       74 46 110 ; 
       75 46 110 ; 
       76 37 110 ; 
       77 37 110 ; 
       78 37 110 ; 
       79 39 110 ; 
       80 39 110 ; 
       81 39 110 ; 
       82 36 110 ; 
       83 36 110 ; 
       84 36 110 ; 
       85 40 110 ; 
       86 40 110 ; 
       87 40 110 ; 
       88 38 110 ; 
       89 38 110 ; 
       90 38 110 ; 
       91 42 110 ; 
       92 42 110 ; 
       93 42 110 ; 
       94 35 110 ; 
       95 35 110 ; 
       96 35 110 ; 
       97 41 110 ; 
       98 41 110 ; 
       99 41 110 ; 
       100 5 110 ; 
       101 2 110 ; 
       102 15 110 ; 
       103 16 110 ; 
       104 13 110 ; 
       105 14 110 ; 
       106 58 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 286.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 120 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 25 -8 0 MPRFLG 0 ; 
       4 SCHEM 75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 97.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 10 -8 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 272.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 275 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 277.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 280 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 192.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 205 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 167.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 180 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 218.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 215 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 233.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 230 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 248.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 245 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 263.75 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 260 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 65 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 40 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 42.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 45 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 47.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 110 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 85 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 87.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 90 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 92.5 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 80 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 105 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 60 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 125 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 52.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 97.5 -10 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 70 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 115 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 120 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 75 -8 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 33.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 133.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 143.75 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 13.75 -6 0 MPRFLG 0 ; 
       49 SCHEM 186.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 241.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 276.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 147.5 -4 0 DISPLAY 0 0 SRT 1 1 1 -1.570796 -3.141593 0 0 0 0 MPRFLG 0 ; 
       53 SCHEM 155 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 162.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       55 SCHEM 175 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       56 SCHEM 187.5 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       57 SCHEM 200 -14 0 DISPLAY 0 0 MPRFLG 0 ; 
       58 SCHEM 228.75 -6 0 DISPLAY 1 2 MPRFLG 0 ; 
       59 SCHEM 160 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       60 SCHEM 150 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       61 SCHEM 152.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       62 SCHEM 155 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       63 SCHEM 157.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       64 SCHEM 147.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       65 SCHEM 140 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 142.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 145 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       68 SCHEM 37.5 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       69 SCHEM 30 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       70 SCHEM 32.5 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       71 SCHEM 35 -8 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       72 SCHEM 137.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       73 SCHEM 130 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       74 SCHEM 132.5 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       75 SCHEM 135 -8 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       76 SCHEM 62.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       77 SCHEM 57.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 60 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 55 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 50 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 52.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       82 SCHEM 107.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       83 SCHEM 102.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       84 SCHEM 105 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       85 SCHEM 100 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       86 SCHEM 95 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       87 SCHEM 97.5 -12 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       88 SCHEM 127.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       89 SCHEM 122.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       90 SCHEM 125 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       91 SCHEM 117.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       92 SCHEM 112.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       93 SCHEM 115 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       94 SCHEM 82.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       95 SCHEM 77.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       96 SCHEM 80 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       97 SCHEM 72.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       98 SCHEM 67.5 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       99 SCHEM 70 -10 0 WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       100 SCHEM 101.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       101 SCHEM 56.25 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       102 SCHEM 163.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       103 SCHEM 176.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       104 SCHEM 188.75 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       105 SCHEM 201.25 -12 0 DISPLAY 0 0 MPRFLG 0 ; 
       106 SCHEM 282.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
