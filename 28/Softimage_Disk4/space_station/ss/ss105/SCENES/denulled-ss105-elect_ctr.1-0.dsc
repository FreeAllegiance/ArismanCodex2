SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.42-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 85     
       ss105-cube25.1-0 ; 
       ss105-east_bay_11_10.1-0 ; 
       ss105-east_bay_11_6.1-0 ; 
       ss105-east_bay_11_8.1-0 ; 
       ss105-east_bay_11_9.1-0 ; 
       ss105-extru32_1.1-0 ; 
       ss105-extru32_2.1-0 ; 
       ss105-extru32_3.1-0 ; 
       ss105-extru32_4.1-0 ; 
       ss105-extru39_10.1-0 ; 
       ss105-extru39_11.1-0 ; 
       ss105-extru39_8.1-0 ; 
       ss105-extru39_9.1-0 ; 
       ss105-extru63.1-0 ; 
       ss105-extru64.1-0 ; 
       ss105-extru69.1-0 ; 
       ss105-extru70.1-0 ; 
       ss105-extru71.1-0 ; 
       ss105-extru72.1-0 ; 
       ss105-extru73.1-0 ; 
       ss105-extru74.1-0 ; 
       ss105-garage1A.1-0 ; 
       ss105-garage1B.1-0 ; 
       ss105-garage1C.1-0 ; 
       ss105-garage1D.1-0 ; 
       ss105-garage1E.1-0 ; 
       ss105-garage2A.1-0 ; 
       ss105-garage2B.1-0 ; 
       ss105-garage2C.1-0 ; 
       ss105-garage2D.1-0 ; 
       ss105-garage2E.1-0 ; 
       ss105-launch1.1-0 ; 
       ss105-launch2.1-0 ; 
       ss105-sphere14.1-0 ; 
       ss105-sphere15.1-0 ; 
       ss105-sphere16.1-0 ; 
       ss105-sphere17.1-0 ; 
       ss105-sphere2.2-0 ; 
       ss105-SS_01.1-0 ; 
       ss105-SS_02.1-0 ; 
       ss105-SS_03.1-0 ; 
       ss105-SS_04.1-0 ; 
       ss105-SS_05.1-0 ; 
       ss105-SS_20.1-0 ; 
       ss105-SS_21.1-0 ; 
       ss105-SS_22.1-0 ; 
       ss105-SS_23_4.1-0 ; 
       ss105-SS_29.1-0 ; 
       ss105-SS_30.1-0 ; 
       ss105-SS_31.1-0 ; 
       ss105-SS_32.1-0 ; 
       ss105-SS_33.1-0 ; 
       ss105-SS_34.1-0 ; 
       ss105-SS_35.1-0 ; 
       ss105-SS_36.1-0 ; 
       ss105-SS_60.1-0 ; 
       ss105-SS_61.1-0 ; 
       ss105-SS_62.1-0 ; 
       ss105-SS_63.1-0 ; 
       ss105-SS_64.1-0 ; 
       ss105-SS_65.1-0 ; 
       ss105-SS_66.1-0 ; 
       ss105-SS_67.1-0 ; 
       ss105-SS_68.1-0 ; 
       ss105-SS_69.1-0 ; 
       ss105-SS_70.1-0 ; 
       ss105-SS_71.1-0 ; 
       ss105-SS_72.1-0 ; 
       ss105-SS_73.1-0 ; 
       ss105-SS_74.1-0 ; 
       ss105-SS_75.1-0 ; 
       ss105-SS_76.1-0 ; 
       ss105-SS_77.1-0 ; 
       ss105-SS_78.1-0 ; 
       ss105-SS_79.1-0 ; 
       ss105-SS_80.1-0 ; 
       ss105-SS_81.1-0 ; 
       ss105-SS_82.1-0 ; 
       ss105-SS_83.1-0 ; 
       ss105-ss105.1-0 ROOT ; 
       ss105-tetra10.1-0 ; 
       ss105-tetra11.1-0 ; 
       ss105-tetra12.1-0 ; 
       ss105-tetra13.1-0 ; 
       ss105-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss105/PICTURES/ss105 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       denulled-ss105-elect_ctr.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 37 110 ; 
       1 79 110 ; 
       2 79 110 ; 
       3 79 110 ; 
       4 79 110 ; 
       5 79 110 ; 
       6 79 110 ; 
       7 79 110 ; 
       8 79 110 ; 
       9 79 110 ; 
       10 79 110 ; 
       11 79 110 ; 
       12 79 110 ; 
       13 79 110 ; 
       14 79 110 ; 
       15 79 110 ; 
       16 79 110 ; 
       17 79 110 ; 
       18 79 110 ; 
       19 79 110 ; 
       20 79 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 2 110 ; 
       24 2 110 ; 
       25 2 110 ; 
       26 3 110 ; 
       27 3 110 ; 
       28 3 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       31 1 110 ; 
       32 4 110 ; 
       33 11 110 ; 
       34 12 110 ; 
       35 9 110 ; 
       36 10 110 ; 
       37 79 110 ; 
       38 79 110 ; 
       39 79 110 ; 
       40 79 110 ; 
       41 79 110 ; 
       42 79 110 ; 
       43 79 110 ; 
       44 79 110 ; 
       45 79 110 ; 
       46 79 110 ; 
       47 79 110 ; 
       48 79 110 ; 
       49 79 110 ; 
       50 79 110 ; 
       51 79 110 ; 
       52 79 110 ; 
       53 79 110 ; 
       54 79 110 ; 
       55 2 110 ; 
       56 2 110 ; 
       57 2 110 ; 
       58 2 110 ; 
       59 2 110 ; 
       60 2 110 ; 
       61 3 110 ; 
       62 3 110 ; 
       63 3 110 ; 
       64 3 110 ; 
       65 3 110 ; 
       66 3 110 ; 
       67 1 110 ; 
       68 1 110 ; 
       69 1 110 ; 
       70 1 110 ; 
       71 1 110 ; 
       72 1 110 ; 
       73 4 110 ; 
       74 4 110 ; 
       75 4 110 ; 
       76 4 110 ; 
       77 4 110 ; 
       78 4 110 ; 
       80 11 110 ; 
       81 12 110 ; 
       82 9 110 ; 
       83 10 110 ; 
       84 37 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -6.17559 9.301508 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM -6.17559 7.301508 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -36.05392 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM -58.55392 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM -11.05391 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 43.94609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM -76.05392 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 73.94611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 76.44611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 78.94611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 86.44611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 65.1961 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 70.19611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 27.69609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 60.19611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 83.94609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 81.44611 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 91.44609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 88.94609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 96.44609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 93.94609 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 98.94608 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 101.4461 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 1.446086 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM -23.55391 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM -21.05391 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM -18.55391 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM -16.05392 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 56.4461 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 31.44609 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 33.94609 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 36.4461 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 38.9461 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM -58.55392 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM -76.05392 -3.617883 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       79 SCHEM 8.946081 0.3821161 0 USR DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       33 SCHEM 26.44609 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 58.9461 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 63.94611 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 68.94611 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM -37.30392 -1.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 23.94608 -1.617883 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 13.94608 -1.617883 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 16.44608 -1.617883 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 18.94608 -1.617883 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 21.44608 -1.617883 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 11.44608 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 3.946082 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 6.446085 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 8.946081 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM -41.05392 -1.617883 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM -48.55391 -1.617883 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM -46.05392 -1.617883 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM -43.55392 -1.617883 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM -26.05391 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM -33.55392 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM -31.05392 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM -28.55391 -1.617883 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM -1.05391 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM -6.053913 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM -3.553913 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM -8.553913 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM -13.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM -11.05391 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       61 SCHEM 53.9461 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       62 SCHEM 48.9461 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       63 SCHEM 51.4461 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       64 SCHEM 46.44609 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       65 SCHEM 41.44609 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       66 SCHEM 43.94609 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       67 SCHEM -51.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       68 SCHEM -56.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       69 SCHEM -53.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       70 SCHEM -61.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       71 SCHEM -66.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       72 SCHEM -63.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       73 SCHEM -68.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       74 SCHEM -73.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       75 SCHEM -71.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       76 SCHEM -78.55392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       77 SCHEM -83.55391 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       78 SCHEM -81.05392 -3.617883 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       80 SCHEM 28.94609 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       81 SCHEM 61.44611 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       82 SCHEM 66.44611 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       83 SCHEM 71.44611 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
       84 SCHEM -38.55392 -3.617883 0 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
