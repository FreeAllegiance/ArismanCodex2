SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.1-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 106     
       elect-bound01.3-0 ; 
       elect-root.1-0 ROOT ; 
       elect_ctr-cube25.1-0 ; 
       elect_ctr-east_bay_11.3-0 ; 
       elect_ctr-east_bay_11_1.4-0 ; 
       elect_ctr-east_bay_11_2.3-0 ; 
       elect_ctr-east_bay_11_3.3-0 ; 
       elect_ctr-east_bay_11_6.1-0 ; 
       elect_ctr-east_bay_11_7.1-0 ; 
       elect_ctr-extru32.1-0 ; 
       elect_ctr-extru32_1.1-0 ; 
       elect_ctr-extru34.1-0 ; 
       elect_ctr-extru34_1.1-0 ; 
       elect_ctr-extru39_1.1-0 ; 
       elect_ctr-extru39_2.1-0 ; 
       elect_ctr-extru52.1-0 ; 
       elect_ctr-extru53.1-0 ; 
       elect_ctr-extru54.1-0 ; 
       elect_ctr-extru54_1.1-0 ; 
       elect_ctr-extru55.1-0 ; 
       elect_ctr-extru56.1-0 ; 
       elect_ctr-extru57.1-0 ; 
       elect_ctr-extru58.1-0 ; 
       elect_ctr-extru61.1-0 ; 
       elect_ctr-extru62.1-0 ; 
       elect_ctr-garage1A.1-0 ; 
       elect_ctr-garage1B.1-0 ; 
       elect_ctr-garage1C.1-0 ; 
       elect_ctr-garage1D.1-0 ; 
       elect_ctr-garage1E.1-0 ; 
       elect_ctr-garage2A.1-0 ; 
       elect_ctr-garage2B.1-0 ; 
       elect_ctr-garage2C.1-0 ; 
       elect_ctr-garage2D.1-0 ; 
       elect_ctr-garage2E.1-0 ; 
       elect_ctr-landing_lights.1-0 ; 
       elect_ctr-landing_lights_1.1-0 ; 
       elect_ctr-landing_lights_2.1-0 ; 
       elect_ctr-landing_lights_3.1-0 ; 
       elect_ctr-landing_lights2.1-0 ; 
       elect_ctr-landing_lights2_1.1-0 ; 
       elect_ctr-landing_lights2_2.1-0 ; 
       elect_ctr-landing_lights2_3.1-0 ; 
       elect_ctr-launch1.1-0 ; 
       elect_ctr-launch2.1-0 ; 
       elect_ctr-null18.1-0 ; 
       elect_ctr-null19.1-0 ; 
       elect_ctr-null19_1.1-0 ; 
       elect_ctr-null27.1-0 ; 
       elect_ctr-null28.1-0 ; 
       elect_ctr-null30.1-0 ; 
       elect_ctr-null31.1-0 ROOT ; 
       elect_ctr-null32.1-0 ; 
       elect_ctr-sphere2.2-0 ; 
       elect_ctr-sphere5.1-0 ; 
       elect_ctr-sphere5_1.1-0 ; 
       elect_ctr-sphere7.1-0 ; 
       elect_ctr-sphere8.1-0 ; 
       elect_ctr-SS_01.1-0 ; 
       elect_ctr-SS_02.1-0 ; 
       elect_ctr-SS_03.1-0 ; 
       elect_ctr-SS_04.1-0 ; 
       elect_ctr-SS_05.1-0 ; 
       elect_ctr-SS_20.1-0 ; 
       elect_ctr-SS_21.1-0 ; 
       elect_ctr-SS_22.1-0 ; 
       elect_ctr-SS_23_4.1-0 ; 
       elect_ctr-SS_29.1-0 ; 
       elect_ctr-SS_30.1-0 ; 
       elect_ctr-SS_31.1-0 ; 
       elect_ctr-SS_32.1-0 ; 
       elect_ctr-SS_33.1-0 ; 
       elect_ctr-SS_34.1-0 ; 
       elect_ctr-SS_35.1-0 ; 
       elect_ctr-SS_36.1-0 ; 
       elect_ctr-SS_60.1-0 ; 
       elect_ctr-SS_61.1-0 ; 
       elect_ctr-SS_62.1-0 ; 
       elect_ctr-SS_63.1-0 ; 
       elect_ctr-SS_64.1-0 ; 
       elect_ctr-SS_65.1-0 ; 
       elect_ctr-SS_66.1-0 ; 
       elect_ctr-SS_67.1-0 ; 
       elect_ctr-SS_68.1-0 ; 
       elect_ctr-SS_69.1-0 ; 
       elect_ctr-SS_70.1-0 ; 
       elect_ctr-SS_71.1-0 ; 
       elect_ctr-SS_72.1-0 ; 
       elect_ctr-SS_73.1-0 ; 
       elect_ctr-SS_74.1-0 ; 
       elect_ctr-SS_75.1-0 ; 
       elect_ctr-SS_76.1-0 ; 
       elect_ctr-SS_77.1-0 ; 
       elect_ctr-SS_78.1-0 ; 
       elect_ctr-SS_79.1-0 ; 
       elect_ctr-SS_80.1-0 ; 
       elect_ctr-SS_81.1-0 ; 
       elect_ctr-SS_82.1-0 ; 
       elect_ctr-SS_83.1-0 ; 
       elect_ctr-strobe_set.1-0 ; 
       elect_ctr-strobe_set_1.1-0 ; 
       elect_ctr-tetra1.1-0 ; 
       elect_ctr-tetra1_1.1-0 ; 
       elect_ctr-tetra2.1-0 ; 
       elect_ctr-tetra3.1-0 ; 
       elect_ctr-tetra4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss105/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss105/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-elect_ctr.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 53 110 ; 
       3 51 110 ; 
       4 51 110 ; 
       5 51 110 ; 
       6 51 110 ; 
       7 53 110 ; 
       8 53 110 ; 
       9 50 110 ; 
       10 50 110 ; 
       11 49 110 ; 
       12 49 110 ; 
       13 48 110 ; 
       14 48 110 ; 
       15 48 110 ; 
       16 48 110 ; 
       17 11 110 ; 
       18 12 110 ; 
       19 49 110 ; 
       20 19 110 ; 
       21 49 110 ; 
       22 21 110 ; 
       23 50 110 ; 
       24 50 110 ; 
       25 4 110 ; 
       26 4 110 ; 
       27 4 110 ; 
       28 4 110 ; 
       29 4 110 ; 
       30 6 110 ; 
       31 6 110 ; 
       32 6 110 ; 
       33 6 110 ; 
       34 6 110 ; 
       35 5 110 ; 
       36 99 110 ; 
       37 100 110 ; 
       38 3 110 ; 
       39 100 110 ; 
       40 99 110 ; 
       41 5 110 ; 
       42 3 110 ; 
       43 3 110 ; 
       44 5 110 ; 
       45 51 110 ; 
       46 51 110 ; 
       47 51 110 ; 
       48 53 110 ; 
       49 53 110 ; 
       50 53 110 ; 
       52 51 110 ; 
       53 51 110 ; 
       54 103 110 ; 
       55 104 110 ; 
       56 101 110 ; 
       57 102 110 ; 
       58 52 110 ; 
       59 52 110 ; 
       60 52 110 ; 
       61 52 110 ; 
       62 52 110 ; 
       63 47 110 ; 
       64 47 110 ; 
       65 47 110 ; 
       66 47 110 ; 
       67 45 110 ; 
       68 45 110 ; 
       69 45 110 ; 
       70 45 110 ; 
       71 46 110 ; 
       72 46 110 ; 
       73 46 110 ; 
       74 46 110 ; 
       75 37 110 ; 
       76 37 110 ; 
       77 37 110 ; 
       78 39 110 ; 
       79 39 110 ; 
       80 39 110 ; 
       81 36 110 ; 
       82 36 110 ; 
       83 36 110 ; 
       84 40 110 ; 
       85 40 110 ; 
       86 40 110 ; 
       87 38 110 ; 
       88 38 110 ; 
       89 38 110 ; 
       90 42 110 ; 
       91 42 110 ; 
       92 42 110 ; 
       93 35 110 ; 
       94 35 110 ; 
       95 35 110 ; 
       96 41 110 ; 
       97 41 110 ; 
       98 41 110 ; 
       99 6 110 ; 
       100 4 110 ; 
       101 14 110 ; 
       102 13 110 ; 
       103 15 110 ; 
       104 16 110 ; 
       105 53 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2186.29 48.12609 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2186.29 46.12609 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2196.363 45.79469 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2196.998 47.65995 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -27.40373 MPRFLG 0 ; 
       2 SCHEM 2214.215 50.42273 0 USR MPRFLG 0 ; 
       3 SCHEM 2240.89 38.96848 0 USR MPRFLG 0 ; 
       4 SCHEM 2211.811 38.59404 0 USR MPRFLG 0 ; 
       5 SCHEM 2241.335 31.098 0 USR MPRFLG 0 ; 
       6 SCHEM 2212.526 30.5383 0 USR MPRFLG 0 ; 
       7 SCHEM 2236.715 50.42273 0 USR MPRFLG 0 ; 
       8 SCHEM 2239.215 50.42273 0 USR MPRFLG 0 ; 
       9 SCHEM 2216.715 48.42273 0 USR MPRFLG 0 ; 
       10 SCHEM 2224.215 48.42273 0 USR MPRFLG 0 ; 
       11 SCHEM 2226.715 48.42273 0 USR MPRFLG 0 ; 
       12 SCHEM 2234.215 48.42273 0 USR MPRFLG 0 ; 
       13 SCHEM 2211.715 48.42273 0 USR MPRFLG 0 ; 
       14 SCHEM 2204.215 48.42273 0 USR MPRFLG 0 ; 
       15 SCHEM 2206.715 48.42273 0 USR MPRFLG 0 ; 
       16 SCHEM 2209.215 48.42273 0 USR MPRFLG 0 ; 
       17 SCHEM 2226.715 46.42272 0 USR MPRFLG 0 ; 
       18 SCHEM 2234.215 46.42272 0 USR MPRFLG 0 ; 
       19 SCHEM 2229.215 48.42273 0 USR MPRFLG 0 ; 
       20 SCHEM 2229.215 46.42272 0 USR MPRFLG 0 ; 
       21 SCHEM 2231.715 48.42273 0 USR MPRFLG 0 ; 
       22 SCHEM 2231.715 46.42272 0 USR MPRFLG 0 ; 
       23 SCHEM 2219.215 48.42273 0 USR MPRFLG 0 ; 
       24 SCHEM 2221.715 48.42273 0 USR MPRFLG 0 ; 
       25 SCHEM 2209.288 35.70854 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 2214.247 35.70854 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 2211.788 35.70854 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 2219.247 35.70854 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 2216.747 35.70854 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       30 SCHEM 2210.003 27.65282 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       31 SCHEM 2214.962 27.65282 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 2212.503 27.65282 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       33 SCHEM 2219.962 27.65282 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 2217.462 27.65282 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       35 SCHEM 2235.085 27.098 0 USR MPRFLG 0 ; 
       36 SCHEM 2203.586 25.65282 0 USR MPRFLG 0 ; 
       37 SCHEM 2202.871 33.70854 0 USR MPRFLG 0 ; 
       38 SCHEM 2234.64 34.96848 0 USR MPRFLG 0 ; 
       39 SCHEM 2210.371 33.70854 0 USR MPRFLG 0 ; 
       40 SCHEM 2211.086 25.65282 0 USR MPRFLG 0 ; 
       41 SCHEM 2242.585 27.098 0 USR MPRFLG 0 ; 
       42 SCHEM 2242.14 34.96848 0 USR MPRFLG 0 ; 
       43 SCHEM 2249.64 36.96848 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       44 SCHEM 2250.085 29.098 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 2225.512 20.72652 0 USR MPRFLG 0 ; 
       46 SCHEM 2225.906 16.92496 0 USR MPRFLG 0 ; 
       47 SCHEM 2225.911 14.21651 0 USR MPRFLG 0 ; 
       48 SCHEM 2207.965 50.42273 0 USR MPRFLG 0 ; 
       49 SCHEM 2230.465 50.42273 0 USR MPRFLG 0 ; 
       50 SCHEM 2220.465 50.42273 0 USR MPRFLG 0 ; 
       51 SCHEM 2221.715 54.42273 0 USR SRT 1 1 1 0 0 0 0 0 -27.40373 MPRFLG 0 ; 
       52 SCHEM 2225.797 24.44735 0 USR MPRFLG 0 ; 
       53 SCHEM 2221.715 52.42273 0 USR MPRFLG 0 ; 
       54 SCHEM 2206.715 44.42273 0 USR MPRFLG 0 ; 
       55 SCHEM 2209.215 44.42273 0 USR MPRFLG 0 ; 
       56 SCHEM 2204.215 44.42273 0 USR MPRFLG 0 ; 
       57 SCHEM 2211.715 44.42273 0 USR MPRFLG 0 ; 
       58 SCHEM 2222.047 22.44735 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 2224.547 22.44735 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 2227.047 22.44735 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 2229.547 22.44735 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       62 SCHEM 2231.855 22.42166 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       63 SCHEM 2222.161 12.2165 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 2224.661 12.2165 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 2227.161 12.2165 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 2229.661 12.2165 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       67 SCHEM 2221.762 18.72652 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       68 SCHEM 2224.262 18.72652 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       69 SCHEM 2226.762 18.72652 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       70 SCHEM 2229.262 18.72652 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       71 SCHEM 2229.656 14.92495 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       72 SCHEM 2227.156 14.92495 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       73 SCHEM 2224.656 14.92495 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       74 SCHEM 2222.156 14.92495 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       75 SCHEM 2200.371 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       76 SCHEM 2202.871 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 2205.371 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       78 SCHEM 2207.871 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       79 SCHEM 2210.371 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       80 SCHEM 2212.871 31.70854 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       81 SCHEM 2201.086 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       82 SCHEM 2203.586 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       83 SCHEM 2206.086 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       84 SCHEM 2208.586 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       85 SCHEM 2211.086 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       86 SCHEM 2213.586 23.65282 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       87 SCHEM 2232.14 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       88 SCHEM 2234.64 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       89 SCHEM 2237.14 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       90 SCHEM 2239.64 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       91 SCHEM 2242.14 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       92 SCHEM 2244.64 32.96846 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       93 SCHEM 2232.585 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       94 SCHEM 2235.085 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       95 SCHEM 2237.585 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       96 SCHEM 2240.085 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       97 SCHEM 2242.585 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 2245.085 25.098 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 2207.336 27.65282 0 USR MPRFLG 0 ; 
       100 SCHEM 2206.621 35.70854 0 USR MPRFLG 0 ; 
       101 SCHEM 2204.215 46.42272 0 USR MPRFLG 0 ; 
       102 SCHEM 2211.715 46.42272 0 USR MPRFLG 0 ; 
       103 SCHEM 2206.715 46.42272 0 USR MPRFLG 0 ; 
       104 SCHEM 2209.215 46.42272 0 USR MPRFLG 0 ; 
       105 SCHEM 2225.378 50.98804 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
