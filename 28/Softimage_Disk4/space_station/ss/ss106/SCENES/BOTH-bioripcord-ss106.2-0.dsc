SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.30-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 63     
       bounding_model-bound01.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       ss106-cube20.2-0 ; 
       ss106-cube22_2_2.1-0 ; 
       ss106-cube22_2_24.1-0 ; 
       ss106-cube22_2_25.1-0 ; 
       ss106-cube22_2_28.1-0 ; 
       ss106-cube23.1-0 ; 
       ss106-cube24.1-0 ; 
       ss106-cube5.2-0 ; 
       ss106-east_bay_11_10.1-0 ; 
       ss106-east_bay_11_9.1-0 ; 
       ss106-extru22.1-0 ; 
       ss106-extru31.1-0 ; 
       ss106-extru34.1-0 ; 
       ss106-extru35.1-0 ; 
       ss106-extru36.1-0 ; 
       ss106-extru37.1-0 ; 
       ss106-extru38.1-0 ; 
       ss106-extru39.1-0 ; 
       ss106-extru40.1-0 ; 
       ss106-extru41.1-0 ; 
       ss106-extru42.1-0 ; 
       ss106-extru9.1-0 ; 
       ss106-garage1A.1-0 ; 
       ss106-garage1B.1-0 ; 
       ss106-garage1C.1-0 ; 
       ss106-garage1D.1-0 ; 
       ss106-garage1E.1-0 ; 
       ss106-launch3.1-0 ; 
       ss106-sphere8.5-0 ; 
       ss106-sphere9_1.1-0 ; 
       ss106-SS_11_1.1-0 ; 
       ss106-SS_11_4.1-0 ; 
       ss106-SS_13_2.1-0 ; 
       ss106-SS_15_1.1-0 ; 
       ss106-SS_15_4.1-0 ; 
       ss106-SS_23_2.1-0 ; 
       ss106-SS_23_4.1-0 ; 
       ss106-SS_24_1.1-0 ; 
       ss106-SS_24_4.1-0 ; 
       ss106-SS_26.1-0 ; 
       ss106-SS_26_4.1-0 ; 
       ss106-SS_29.1-0 ; 
       ss106-SS_30.1-0 ; 
       ss106-SS_31.1-0 ; 
       ss106-SS_32.1-0 ; 
       ss106-SS_33.1-0 ; 
       ss106-SS_34.1-0 ; 
       ss106-SS_35.1-0 ; 
       ss106-SS_36.1-0 ; 
       ss106-SS_55.2-0 ; 
       ss106-SS_58.1-0 ; 
       ss106-SS_70.1-0 ; 
       ss106-SS_71.1-0 ; 
       ss106-SS_72.1-0 ; 
       ss106-SS_73.1-0 ; 
       ss106-SS_74.1-0 ; 
       ss106-SS_75.1-0 ; 
       ss106-SS_76.1-0 ; 
       ss106-SS_77.1-0 ; 
       ss106-SS_78.1-0 ; 
       ss106-ss106.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss106/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-bioripcord-ss106.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       2 62 110 ; 
       3 62 110 ; 
       4 62 110 ; 
       5 62 110 ; 
       6 62 110 ; 
       7 62 110 ; 
       8 62 110 ; 
       9 62 110 ; 
       10 62 110 ; 
       11 62 110 ; 
       12 62 110 ; 
       13 62 110 ; 
       14 62 110 ; 
       15 62 110 ; 
       16 62 110 ; 
       17 62 110 ; 
       18 62 110 ; 
       19 62 110 ; 
       20 62 110 ; 
       21 62 110 ; 
       22 62 110 ; 
       23 62 110 ; 
       24 10 110 ; 
       25 10 110 ; 
       26 10 110 ; 
       27 10 110 ; 
       28 10 110 ; 
       29 11 110 ; 
       30 62 110 ; 
       31 62 110 ; 
       32 10 110 ; 
       33 11 110 ; 
       34 10 110 ; 
       35 10 110 ; 
       36 11 110 ; 
       37 10 110 ; 
       38 11 110 ; 
       39 10 110 ; 
       40 11 110 ; 
       41 10 110 ; 
       42 11 110 ; 
       43 62 110 ; 
       44 62 110 ; 
       45 62 110 ; 
       46 62 110 ; 
       47 62 110 ; 
       48 62 110 ; 
       49 62 110 ; 
       50 62 110 ; 
       51 62 110 ; 
       52 11 110 ; 
       53 62 110 ; 
       54 62 110 ; 
       55 62 110 ; 
       56 62 110 ; 
       57 62 110 ; 
       58 62 110 ; 
       59 62 110 ; 
       60 62 110 ; 
       61 62 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 18.1559 30.20494 0 USR MPRFLG 0 ; 
       1 SCHEM 18.1559 28.20496 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 53.73529 25.43027 0 WIRECOL 6 7 MPRFLG 0 ; 
       1 SCHEM 53.73529 27.43027 0 USR WIRECOL 6 7 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 30.82569 14.37256 0 MPRFLG 0 ; 
       3 SCHEM -9.17431 14.37256 0 MPRFLG 0 ; 
       4 SCHEM -6.674309 14.37256 0 MPRFLG 0 ; 
       5 SCHEM -1.674309 14.37256 0 MPRFLG 0 ; 
       6 SCHEM 28.32569 14.37256 0 MPRFLG 0 ; 
       7 SCHEM -4.174309 14.37256 0 MPRFLG 0 ; 
       8 SCHEM -11.67431 14.37256 0 MPRFLG 0 ; 
       9 SCHEM 65.82569 14.37256 0 MPRFLG 0 ; 
       10 SCHEM 13.32569 14.37256 0 MPRFLG 0 ; 
       11 SCHEM 115.8257 14.37256 0 MPRFLG 0 ; 
       12 SCHEM 43.32569 14.37256 0 MPRFLG 0 ; 
       13 SCHEM 33.32569 14.37256 0 MPRFLG 0 ; 
       14 SCHEM 45.82569 14.37256 0 MPRFLG 0 ; 
       15 SCHEM 58.32569 14.37256 0 MPRFLG 0 ; 
       16 SCHEM 60.82569 14.37256 0 MPRFLG 0 ; 
       17 SCHEM 50.82569 14.37256 0 MPRFLG 0 ; 
       18 SCHEM 53.32569 14.37256 0 MPRFLG 0 ; 
       19 SCHEM 55.82569 14.37256 0 MPRFLG 0 ; 
       20 SCHEM 35.82569 14.37256 0 MPRFLG 0 ; 
       21 SCHEM 38.32569 14.37256 0 MPRFLG 0 ; 
       22 SCHEM 40.82569 14.37256 0 MPRFLG 0 ; 
       23 SCHEM 48.32569 14.37256 0 MPRFLG 0 ; 
       24 SCHEM 15.82569 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 20.82569 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 18.32569 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 25.82569 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 23.32569 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 123.3257 12.37256 0 WIRECOL 9 7 MPRFLG 0 ; 
       62 SCHEM 60.29228 23.35933 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       30 SCHEM 63.32569 14.37256 0 MPRFLG 0 ; 
       31 SCHEM 68.32569 14.37256 0 MPRFLG 0 ; 
       32 SCHEM 5.825691 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       33 SCHEM 113.3257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       34 SCHEM 0.8256915 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       35 SCHEM 3.32569 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       36 SCHEM 110.8257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       37 SCHEM 13.32569 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       38 SCHEM 120.8257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       39 SCHEM 8.32569 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       40 SCHEM 115.8257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       41 SCHEM 10.82569 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       42 SCHEM 118.3257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       43 SCHEM 105.8257 14.37256 0 WIRECOL 2 7 MPRFLG 0 ; 
       44 SCHEM 125.8257 14.37256 0 WIRECOL 2 7 MPRFLG 0 ; 
       45 SCHEM 128.3257 14.37256 0 WIRECOL 2 7 MPRFLG 0 ; 
       46 SCHEM 130.8257 14.37256 0 WIRECOL 2 7 MPRFLG 0 ; 
       47 SCHEM 103.3257 14.37256 0 WIRECOL 4 7 MPRFLG 0 ; 
       48 SCHEM 100.8257 14.37256 0 WIRECOL 4 7 MPRFLG 0 ; 
       49 SCHEM 98.32569 14.37256 0 WIRECOL 4 7 MPRFLG 0 ; 
       50 SCHEM 95.82569 14.37256 0 WIRECOL 4 7 MPRFLG 0 ; 
       51 SCHEM 70.82569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       52 SCHEM 108.3257 12.37256 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 73.32569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       54 SCHEM 75.82569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       55 SCHEM 78.32569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       56 SCHEM 80.82569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       57 SCHEM 83.32569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       58 SCHEM 88.32569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       59 SCHEM 90.82569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       60 SCHEM 85.82569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
       61 SCHEM 93.32569 14.37256 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
