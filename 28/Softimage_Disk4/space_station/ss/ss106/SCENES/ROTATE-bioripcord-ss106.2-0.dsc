SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.38-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 50     
       bioripcord_ss106-cube20.2-0 ; 
       bioripcord_ss106-cube22_2_2.1-0 ; 
       bioripcord_ss106-cube22_2_24.1-0 ; 
       bioripcord_ss106-cube22_2_25.1-0 ; 
       bioripcord_ss106-cube22_2_28.1-0 ; 
       bioripcord_ss106-cube23.1-0 ; 
       bioripcord_ss106-cube24.1-0 ; 
       bioripcord_ss106-cube5.2-0 ; 
       bioripcord_ss106-extru22.1-0 ; 
       bioripcord_ss106-extru31.1-0 ; 
       bioripcord_ss106-extru34.1-0 ; 
       bioripcord_ss106-extru35.1-0 ; 
       bioripcord_ss106-extru36.1-0 ; 
       bioripcord_ss106-extru37.1-0 ; 
       bioripcord_ss106-extru38.1-0 ; 
       bioripcord_ss106-extru39.1-0 ; 
       bioripcord_ss106-extru40.1-0 ; 
       bioripcord_ss106-extru41.1-0 ; 
       bioripcord_ss106-extru42.1-0 ; 
       bioripcord_ss106-extru9.1-0 ; 
       bioripcord_ss106-null12.1-0 ; 
       bioripcord_ss106-null13.2-0 ; 
       bioripcord_ss106-null14.1-0 ; 
       bioripcord_ss106-null18.3-0 ; 
       bioripcord_ss106-null19.3-0 ; 
       bioripcord_ss106-null23.1-0 ; 
       bioripcord_ss106-null24.1-0 ; 
       bioripcord_ss106-null26.4-0 ; 
       bioripcord_ss106-null28.23-0 ROOT ; 
       bioripcord_ss106-null8.1-0 ; 
       bioripcord_ss106-sphere8.5-0 ; 
       bioripcord_ss106-sphere9_1.1-0 ; 
       bioripcord_ss106-SS_29.1-0 ; 
       bioripcord_ss106-SS_30.1-0 ; 
       bioripcord_ss106-SS_31.1-0 ; 
       bioripcord_ss106-SS_32.1-0 ; 
       bioripcord_ss106-SS_33.1-0 ; 
       bioripcord_ss106-SS_34.1-0 ; 
       bioripcord_ss106-SS_35.1-0 ; 
       bioripcord_ss106-SS_36.1-0 ; 
       bioripcord_ss106-SS_55.2-0 ; 
       bioripcord_ss106-SS_70.1-0 ; 
       bioripcord_ss106-SS_71.1-0 ; 
       bioripcord_ss106-SS_72.1-0 ; 
       bioripcord_ss106-SS_73.1-0 ; 
       bioripcord_ss106-SS_74.1-0 ; 
       bioripcord_ss106-SS_75.1-0 ; 
       bioripcord_ss106-SS_76.1-0 ; 
       bioripcord_ss106-SS_77.1-0 ; 
       bioripcord_ss106-SS_78.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ROTATE-bioripcord-ss106.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 26 110 ; 
       1 25 110 ; 
       2 25 110 ; 
       3 25 110 ; 
       4 25 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 21 110 ; 
       8 20 110 ; 
       9 22 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 29 110 ; 
       14 29 110 ; 
       15 29 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 29 110 ; 
       20 21 110 ; 
       21 26 110 ; 
       22 0 110 ; 
       23 28 110 ; 
       24 28 110 ; 
       25 0 110 ; 
       26 28 110 ; 
       27 28 110 ; 
       29 20 110 ; 
       30 28 110 ; 
       31 30 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       34 23 110 ; 
       35 23 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 24 110 ; 
       39 24 110 ; 
       40 27 110 ; 
       41 27 110 ; 
       42 27 110 ; 
       43 27 110 ; 
       44 27 110 ; 
       45 27 110 ; 
       46 27 110 ; 
       47 27 110 ; 
       48 27 110 ; 
       49 27 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999988 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 5 -8 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 MPRFLG 0 ; 
       5 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -10 0 MPRFLG 0 ; 
       7 SCHEM 42.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 25 -8 0 MPRFLG 0 ; 
       11 SCHEM 37.5 -8 0 MPRFLG 0 ; 
       12 SCHEM 40 -8 0 MPRFLG 0 ; 
       13 SCHEM 30 -10 0 MPRFLG 0 ; 
       14 SCHEM 32.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 35 -10 0 MPRFLG 0 ; 
       16 SCHEM 15 -8 0 MPRFLG 0 ; 
       17 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       18 SCHEM 20 -8 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       20 SCHEM 31.25 -6 0 MPRFLG 0 ; 
       21 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       22 SCHEM 16.25 -6 0 MPRFLG 0 ; 
       23 SCHEM 86.25 -2 0 MPRFLG 0 ; 
       24 SCHEM 76.25 -2 0 MPRFLG 0 ; 
       25 SCHEM 6.25 -6 0 MPRFLG 0 ; 
       26 SCHEM 22.5 -2 0 MPRFLG 0 ; 
       27 SCHEM 58.75 -2 0 MPRFLG 0 ; 
       28 SCHEM 46.25 0 0 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 31.25 -8 0 MPRFLG 0 ; 
       30 SCHEM 45 -2 0 MPRFLG 0 ; 
       31 SCHEM 43.19592 -3.778371 0 USR MPRFLG 0 ; 
       32 SCHEM 82.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       33 SCHEM 85 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       34 SCHEM 87.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       35 SCHEM 90 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       36 SCHEM 80 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       37 SCHEM 77.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       38 SCHEM 75 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       39 SCHEM 72.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       40 SCHEM 47.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 50 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 52.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 55 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 57.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 60 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 65 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 67.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       48 SCHEM 62.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       49 SCHEM 70 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
