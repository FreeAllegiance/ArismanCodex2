SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.35-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 29     
       bioripcord_ss106-cube20.2-0 ; 
       bioripcord_ss106-cube22_2_2.1-0 ; 
       bioripcord_ss106-cube22_2_24.1-0 ; 
       bioripcord_ss106-cube22_2_25.1-0 ; 
       bioripcord_ss106-cube22_2_28.1-0 ; 
       bioripcord_ss106-cube23.1-0 ; 
       bioripcord_ss106-cube24.1-0 ; 
       bioripcord_ss106-cube5.2-0 ; 
       bioripcord_ss106-extru22.1-0 ; 
       bioripcord_ss106-extru31.1-0 ; 
       bioripcord_ss106-extru34.1-0 ; 
       bioripcord_ss106-extru35.1-0 ; 
       bioripcord_ss106-extru36.1-0 ; 
       bioripcord_ss106-extru37.1-0 ; 
       bioripcord_ss106-extru38.1-0 ; 
       bioripcord_ss106-extru39.1-0 ; 
       bioripcord_ss106-extru40.1-0 ; 
       bioripcord_ss106-extru41.1-0 ; 
       bioripcord_ss106-extru42.1-0 ; 
       bioripcord_ss106-extru9.1-0 ; 
       bioripcord_ss106-null12.1-0 ; 
       bioripcord_ss106-null13.2-0 ; 
       bioripcord_ss106-null14.1-0 ; 
       bioripcord_ss106-null23.1-0 ; 
       bioripcord_ss106-null24.1-0 ; 
       bioripcord_ss106-null28.21-0 ROOT ; 
       bioripcord_ss106-null8.1-0 ; 
       bioripcord_ss106-sphere8.5-0 ; 
       bioripcord_ss106-sphere9_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-bioripcord-ss106.7-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 24 110 ; 
       1 23 110 ; 
       2 23 110 ; 
       3 23 110 ; 
       4 23 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 21 110 ; 
       8 20 110 ; 
       9 22 110 ; 
       10 20 110 ; 
       11 20 110 ; 
       12 20 110 ; 
       13 26 110 ; 
       14 26 110 ; 
       15 26 110 ; 
       16 22 110 ; 
       17 22 110 ; 
       18 22 110 ; 
       19 26 110 ; 
       20 21 110 ; 
       21 24 110 ; 
       22 0 110 ; 
       23 0 110 ; 
       24 25 110 ; 
       26 20 110 ; 
       27 25 110 ; 
       28 27 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999988 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 163.75 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM 155 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM 157.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 160 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 162.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 160 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM 155 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 195 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 175 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 165 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 177.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 190 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 192.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 182.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 185 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 187.5 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 167.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 170 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 172.5 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 180 -10 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 183.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 185 -4 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 168.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 158.75 -6 0 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 175 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 176.25 0 0 DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 15.24941 0 MPRFLG 0 ; 
       26 SCHEM 183.75 -8 0 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 197.5 -2 0 DISPLAY 2 2 MPRFLG 0 ; 
       28 SCHEM 195.6959 -3.778371 0 USR DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
