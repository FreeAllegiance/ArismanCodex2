SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.26-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       bioripcord_ss106-cube20.2-0 ; 
       bioripcord_ss106-cube22_2_2.1-0 ; 
       bioripcord_ss106-cube22_2_24.1-0 ; 
       bioripcord_ss106-cube22_2_25.1-0 ; 
       bioripcord_ss106-cube22_2_28.1-0 ; 
       bioripcord_ss106-cube23.1-0 ; 
       bioripcord_ss106-cube24.1-0 ; 
       bioripcord_ss106-cube5.2-0 ; 
       bioripcord_ss106-east_bay_11_1.4-0 ; 
       bioripcord_ss106-east_bay_11_10.1-0 ; 
       bioripcord_ss106-east_bay_11_12.1-0 ; 
       bioripcord_ss106-east_bay_11_9.1-0 ; 
       bioripcord_ss106-extru22.1-0 ; 
       bioripcord_ss106-extru31.1-0 ; 
       bioripcord_ss106-extru34.1-0 ; 
       bioripcord_ss106-extru35.1-0 ; 
       bioripcord_ss106-extru36.1-0 ; 
       bioripcord_ss106-extru37.1-0 ; 
       bioripcord_ss106-extru38.1-0 ; 
       bioripcord_ss106-extru39.1-0 ; 
       bioripcord_ss106-extru40.1-0 ; 
       bioripcord_ss106-extru41.1-0 ; 
       bioripcord_ss106-extru42.1-0 ; 
       bioripcord_ss106-extru9.1-0 ; 
       bioripcord_ss106-garage1A.1-0 ; 
       bioripcord_ss106-garage1B.1-0 ; 
       bioripcord_ss106-garage1C.1-0 ; 
       bioripcord_ss106-garage1D.1-0 ; 
       bioripcord_ss106-garage1E.1-0 ; 
       bioripcord_ss106-landing_lights_2.1-0 ; 
       bioripcord_ss106-landing_lights2.1-0 ; 
       bioripcord_ss106-landing_lights2_4.1-0 ; 
       bioripcord_ss106-landing_lights3.1-0 ; 
       bioripcord_ss106-launch3.1-0 ; 
       bioripcord_ss106-null12.1-0 ; 
       bioripcord_ss106-null13.2-0 ; 
       bioripcord_ss106-null14.1-0 ; 
       bioripcord_ss106-null18.3-0 ; 
       bioripcord_ss106-null19.3-0 ; 
       bioripcord_ss106-null23.1-0 ; 
       bioripcord_ss106-null24.1-0 ; 
       bioripcord_ss106-null24_1.2-0 ; 
       bioripcord_ss106-null26.4-0 ; 
       bioripcord_ss106-null28.17-0 ROOT ; 
       bioripcord_ss106-null8.1-0 ; 
       bioripcord_ss106-sphere8.5-0 ; 
       bioripcord_ss106-sphere9_1.1-0 ; 
       bioripcord_ss106-SS_11_1.1-0 ; 
       bioripcord_ss106-SS_11_4.1-0 ; 
       bioripcord_ss106-SS_13_2.1-0 ; 
       bioripcord_ss106-SS_15_1.1-0 ; 
       bioripcord_ss106-SS_15_4.1-0 ; 
       bioripcord_ss106-SS_23_2.1-0 ; 
       bioripcord_ss106-SS_23_4.1-0 ; 
       bioripcord_ss106-SS_24_1.1-0 ; 
       bioripcord_ss106-SS_24_4.1-0 ; 
       bioripcord_ss106-SS_26.1-0 ; 
       bioripcord_ss106-SS_26_4.1-0 ; 
       bioripcord_ss106-SS_29.1-0 ; 
       bioripcord_ss106-SS_30.1-0 ; 
       bioripcord_ss106-SS_31.1-0 ; 
       bioripcord_ss106-SS_32.1-0 ; 
       bioripcord_ss106-SS_33.1-0 ; 
       bioripcord_ss106-SS_34.1-0 ; 
       bioripcord_ss106-SS_35.1-0 ; 
       bioripcord_ss106-SS_36.1-0 ; 
       bioripcord_ss106-SS_55.2-0 ; 
       bioripcord_ss106-SS_58.1-0 ; 
       bioripcord_ss106-SS_70.1-0 ; 
       bioripcord_ss106-SS_71.1-0 ; 
       bioripcord_ss106-SS_72.1-0 ; 
       bioripcord_ss106-SS_73.1-0 ; 
       bioripcord_ss106-SS_74.1-0 ; 
       bioripcord_ss106-SS_75.1-0 ; 
       bioripcord_ss106-SS_76.1-0 ; 
       bioripcord_ss106-SS_77.1-0 ; 
       bioripcord_ss106-SS_78.1-0 ; 
       bioripcord_ss106-strobe_set_1.1-0 ; 
       bioripcord_ss106-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-bioripcord-ss106.19-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       46 45 110 ; 
       0 40 110 ; 
       1 39 110 ; 
       2 39 110 ; 
       3 39 110 ; 
       4 39 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 35 110 ; 
       8 41 110 ; 
       9 41 110 ; 
       10 41 110 ; 
       11 41 110 ; 
       12 34 110 ; 
       13 36 110 ; 
       14 34 110 ; 
       15 34 110 ; 
       16 34 110 ; 
       17 44 110 ; 
       18 44 110 ; 
       19 44 110 ; 
       20 36 110 ; 
       21 36 110 ; 
       22 36 110 ; 
       23 44 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 77 110 ; 
       30 77 110 ; 
       31 78 110 ; 
       32 78 110 ; 
       33 10 110 ; 
       34 35 110 ; 
       35 40 110 ; 
       36 0 110 ; 
       37 43 110 ; 
       38 43 110 ; 
       39 0 110 ; 
       40 43 110 ; 
       41 43 110 ; 
       42 43 110 ; 
       44 34 110 ; 
       45 43 110 ; 
       47 29 110 ; 
       48 32 110 ; 
       49 29 110 ; 
       50 29 110 ; 
       51 32 110 ; 
       52 30 110 ; 
       53 31 110 ; 
       54 30 110 ; 
       55 31 110 ; 
       56 30 110 ; 
       57 31 110 ; 
       58 37 110 ; 
       59 37 110 ; 
       60 37 110 ; 
       61 37 110 ; 
       62 38 110 ; 
       63 38 110 ; 
       64 38 110 ; 
       65 38 110 ; 
       66 42 110 ; 
       67 32 110 ; 
       68 42 110 ; 
       69 42 110 ; 
       70 42 110 ; 
       71 42 110 ; 
       72 42 110 ; 
       73 42 110 ; 
       74 42 110 ; 
       75 42 110 ; 
       76 42 110 ; 
       77 8 110 ; 
       78 10 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999988 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       46 SCHEM 90.69585 -7.778359 0 USR MPRFLG 0 ; 
       0 SCHEM 58.75 -7.999988 0 MPRFLG 0 ; 
       1 SCHEM 50 -11.99999 0 MPRFLG 0 ; 
       2 SCHEM 52.5 -11.99999 0 MPRFLG 0 ; 
       3 SCHEM 55 -11.99999 0 MPRFLG 0 ; 
       4 SCHEM 57.5 -11.99999 0 MPRFLG 0 ; 
       5 SCHEM 55 -13.99999 0 MPRFLG 0 ; 
       6 SCHEM 50 -13.99999 0 MPRFLG 0 ; 
       7 SCHEM 90 -9.999989 0 MPRFLG 0 ; 
       8 SCHEM 12.5 -7.999988 0 MPRFLG 0 ; 
       9 SCHEM 47.5 -7.999988 0 MPRFLG 0 ; 
       10 SCHEM 35 -7.999988 0 MPRFLG 0 ; 
       11 SCHEM 45 -7.999988 0 MPRFLG 0 ; 
       12 SCHEM 70 -11.99999 0 MPRFLG 0 ; 
       13 SCHEM 60 -11.99999 0 MPRFLG 0 ; 
       14 SCHEM 72.5 -11.99999 0 MPRFLG 0 ; 
       15 SCHEM 85 -11.99999 0 MPRFLG 0 ; 
       16 SCHEM 87.5 -11.99999 0 MPRFLG 0 ; 
       17 SCHEM 77.5 -13.99999 0 MPRFLG 0 ; 
       18 SCHEM 80 -13.99999 0 MPRFLG 0 ; 
       19 SCHEM 82.5 -13.99999 0 MPRFLG 0 ; 
       20 SCHEM 62.5 -11.99999 0 MPRFLG 0 ; 
       21 SCHEM 65 -11.99999 0 MPRFLG 0 ; 
       22 SCHEM 67.5 -11.99999 0 MPRFLG 0 ; 
       23 SCHEM 75 -13.99999 0 MPRFLG 0 ; 
       24 SCHEM 15 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 20 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 17.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 25 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 22.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 2.5 -11.99999 0 MPRFLG 0 ; 
       30 SCHEM 10 -11.99999 0 MPRFLG 0 ; 
       31 SCHEM 37.5 -11.99999 0 MPRFLG 0 ; 
       32 SCHEM 30 -11.99999 0 MPRFLG 0 ; 
       33 SCHEM 42.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       34 SCHEM 78.75 -9.999989 0 MPRFLG 0 ; 
       35 SCHEM 80 -7.999988 0 MPRFLG 0 ; 
       36 SCHEM 63.75 -9.999989 0 MPRFLG 0 ; 
       37 SCHEM 133.75 -5.999988 0 MPRFLG 0 ; 
       38 SCHEM 123.75 -5.999988 0 MPRFLG 0 ; 
       39 SCHEM 53.75 -9.999989 0 MPRFLG 0 ; 
       40 SCHEM 70 -5.999988 0 MPRFLG 0 ; 
       41 SCHEM 23.75 -5.999988 0 MPRFLG 0 ; 
       42 SCHEM 106.25 -5.999988 0 MPRFLG 0 ; 
       43 SCHEM 68.75 -3.999988 0 SRT 1 1 1 0 0 0 0 15.24941 0 MPRFLG 0 ; 
       44 SCHEM 78.75 -11.99999 0 MPRFLG 0 ; 
       45 SCHEM 92.5 -5.999988 0 MPRFLG 0 ; 
       47 SCHEM 5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 32.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 0 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 2.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 30 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 12.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 40 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 7.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 35 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 10 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 37.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       58 SCHEM 130 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 132.5 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 135 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 137.5 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 127.5 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 125 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 122.5 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 120 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       66 SCHEM 95 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 27.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       68 SCHEM 97.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 100 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 102.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 105 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 107.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 112.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 115 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 110 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 117.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       77 SCHEM 6.25 -9.999989 0 MPRFLG 0 ; 
       78 SCHEM 33.75 -9.999989 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
