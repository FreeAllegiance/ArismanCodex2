SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.31-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 77     
       bioripcord_ss106-cube20.2-0 ; 
       bioripcord_ss106-cube22_2_2.1-0 ; 
       bioripcord_ss106-cube22_2_24.1-0 ; 
       bioripcord_ss106-cube22_2_25.1-0 ; 
       bioripcord_ss106-cube22_2_28.1-0 ; 
       bioripcord_ss106-cube23.1-0 ; 
       bioripcord_ss106-cube24.1-0 ; 
       bioripcord_ss106-cube5.2-0 ; 
       bioripcord_ss106-east_bay_11_1.4-0 ; 
       bioripcord_ss106-east_bay_11_12.1-0 ; 
       bioripcord_ss106-extru22.1-0 ; 
       bioripcord_ss106-extru31.1-0 ; 
       bioripcord_ss106-extru34.1-0 ; 
       bioripcord_ss106-extru35.1-0 ; 
       bioripcord_ss106-extru36.1-0 ; 
       bioripcord_ss106-extru37.1-0 ; 
       bioripcord_ss106-extru38.1-0 ; 
       bioripcord_ss106-extru39.1-0 ; 
       bioripcord_ss106-extru40.1-0 ; 
       bioripcord_ss106-extru41.1-0 ; 
       bioripcord_ss106-extru42.1-0 ; 
       bioripcord_ss106-extru9.1-0 ; 
       bioripcord_ss106-garage1A.1-0 ; 
       bioripcord_ss106-garage1B.1-0 ; 
       bioripcord_ss106-garage1C.1-0 ; 
       bioripcord_ss106-garage1D.1-0 ; 
       bioripcord_ss106-garage1E.1-0 ; 
       bioripcord_ss106-landing_lights_2.1-0 ; 
       bioripcord_ss106-landing_lights2.1-0 ; 
       bioripcord_ss106-landing_lights2_4.1-0 ; 
       bioripcord_ss106-landing_lights3.1-0 ; 
       bioripcord_ss106-launch3.1-0 ; 
       bioripcord_ss106-null12.1-0 ; 
       bioripcord_ss106-null13.2-0 ; 
       bioripcord_ss106-null14.1-0 ; 
       bioripcord_ss106-null18.3-0 ; 
       bioripcord_ss106-null19.3-0 ; 
       bioripcord_ss106-null23.1-0 ; 
       bioripcord_ss106-null24.1-0 ; 
       bioripcord_ss106-null24_1.2-0 ; 
       bioripcord_ss106-null26.4-0 ; 
       bioripcord_ss106-null28.19-0 ROOT ; 
       bioripcord_ss106-null8.1-0 ; 
       bioripcord_ss106-sphere8.5-0 ; 
       bioripcord_ss106-sphere9_1.1-0 ; 
       bioripcord_ss106-SS_11_1.1-0 ; 
       bioripcord_ss106-SS_11_4.1-0 ; 
       bioripcord_ss106-SS_13_2.1-0 ; 
       bioripcord_ss106-SS_15_1.1-0 ; 
       bioripcord_ss106-SS_15_4.1-0 ; 
       bioripcord_ss106-SS_23_2.1-0 ; 
       bioripcord_ss106-SS_23_4.1-0 ; 
       bioripcord_ss106-SS_24_1.1-0 ; 
       bioripcord_ss106-SS_24_4.1-0 ; 
       bioripcord_ss106-SS_26.1-0 ; 
       bioripcord_ss106-SS_26_4.1-0 ; 
       bioripcord_ss106-SS_29.1-0 ; 
       bioripcord_ss106-SS_30.1-0 ; 
       bioripcord_ss106-SS_31.1-0 ; 
       bioripcord_ss106-SS_32.1-0 ; 
       bioripcord_ss106-SS_33.1-0 ; 
       bioripcord_ss106-SS_34.1-0 ; 
       bioripcord_ss106-SS_35.1-0 ; 
       bioripcord_ss106-SS_36.1-0 ; 
       bioripcord_ss106-SS_55.2-0 ; 
       bioripcord_ss106-SS_58.1-0 ; 
       bioripcord_ss106-SS_70.1-0 ; 
       bioripcord_ss106-SS_71.1-0 ; 
       bioripcord_ss106-SS_72.1-0 ; 
       bioripcord_ss106-SS_73.1-0 ; 
       bioripcord_ss106-SS_74.1-0 ; 
       bioripcord_ss106-SS_75.1-0 ; 
       bioripcord_ss106-SS_76.1-0 ; 
       bioripcord_ss106-SS_77.1-0 ; 
       bioripcord_ss106-SS_78.1-0 ; 
       bioripcord_ss106-strobe_set_1.1-0 ; 
       bioripcord_ss106-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-bioripcord-ss106.20-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 38 110 ; 
       1 37 110 ; 
       2 37 110 ; 
       3 37 110 ; 
       4 37 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 33 110 ; 
       8 39 110 ; 
       9 39 110 ; 
       10 32 110 ; 
       11 34 110 ; 
       12 32 110 ; 
       13 32 110 ; 
       14 32 110 ; 
       15 42 110 ; 
       16 42 110 ; 
       17 42 110 ; 
       18 34 110 ; 
       19 34 110 ; 
       20 34 110 ; 
       21 42 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 75 110 ; 
       28 75 110 ; 
       29 76 110 ; 
       30 76 110 ; 
       31 9 110 ; 
       32 33 110 ; 
       33 38 110 ; 
       34 0 110 ; 
       35 41 110 ; 
       36 41 110 ; 
       37 0 110 ; 
       38 41 110 ; 
       39 41 110 ; 
       40 41 110 ; 
       42 32 110 ; 
       43 41 110 ; 
       44 43 110 ; 
       45 27 110 ; 
       46 30 110 ; 
       47 27 110 ; 
       48 27 110 ; 
       49 30 110 ; 
       50 28 110 ; 
       51 29 110 ; 
       52 28 110 ; 
       53 29 110 ; 
       54 28 110 ; 
       55 29 110 ; 
       56 35 110 ; 
       57 35 110 ; 
       58 35 110 ; 
       59 35 110 ; 
       60 36 110 ; 
       61 36 110 ; 
       62 36 110 ; 
       63 36 110 ; 
       64 40 110 ; 
       65 30 110 ; 
       66 40 110 ; 
       67 40 110 ; 
       68 40 110 ; 
       69 40 110 ; 
       70 40 110 ; 
       71 40 110 ; 
       72 40 110 ; 
       73 40 110 ; 
       74 40 110 ; 
       75 8 110 ; 
       76 9 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999988 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 336.25 -7.999988 0 MPRFLG 0 ; 
       1 SCHEM 327.5 -11.99999 0 MPRFLG 0 ; 
       2 SCHEM 330 -11.99999 0 MPRFLG 0 ; 
       3 SCHEM 332.5 -11.99999 0 MPRFLG 0 ; 
       4 SCHEM 335 -11.99999 0 MPRFLG 0 ; 
       5 SCHEM 332.5 -13.99999 0 MPRFLG 0 ; 
       6 SCHEM 327.5 -13.99999 0 MPRFLG 0 ; 
       7 SCHEM 367.5 -9.999989 0 MPRFLG 0 ; 
       8 SCHEM 290 -7.999988 0 MPRFLG 0 ; 
       9 SCHEM 312.5 -7.999988 0 MPRFLG 0 ; 
       10 SCHEM 347.5 -11.99999 0 MPRFLG 0 ; 
       11 SCHEM 337.5 -11.99999 0 MPRFLG 0 ; 
       12 SCHEM 350 -11.99999 0 MPRFLG 0 ; 
       13 SCHEM 362.5 -11.99999 0 MPRFLG 0 ; 
       14 SCHEM 365 -11.99999 0 MPRFLG 0 ; 
       15 SCHEM 355 -13.99999 0 MPRFLG 0 ; 
       16 SCHEM 357.5 -13.99999 0 MPRFLG 0 ; 
       17 SCHEM 360 -13.99999 0 MPRFLG 0 ; 
       18 SCHEM 340 -11.99999 0 MPRFLG 0 ; 
       19 SCHEM 342.5 -11.99999 0 MPRFLG 0 ; 
       20 SCHEM 345 -11.99999 0 MPRFLG 0 ; 
       21 SCHEM 352.5 -13.99999 0 MPRFLG 0 ; 
       22 SCHEM 292.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       23 SCHEM 297.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       24 SCHEM 295 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 302.5 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 300 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 280 -11.99999 0 MPRFLG 0 ; 
       28 SCHEM 287.5 -11.99999 0 MPRFLG 0 ; 
       29 SCHEM 315 -11.99999 0 MPRFLG 0 ; 
       30 SCHEM 307.5 -11.99999 0 MPRFLG 0 ; 
       31 SCHEM 320 -9.999989 0 WIRECOL 9 7 MPRFLG 0 ; 
       32 SCHEM 356.25 -9.999989 0 MPRFLG 0 ; 
       33 SCHEM 357.5 -7.999988 0 MPRFLG 0 ; 
       34 SCHEM 341.25 -9.999989 0 MPRFLG 0 ; 
       35 SCHEM 411.25 -5.999988 0 MPRFLG 0 ; 
       36 SCHEM 401.25 -5.999988 0 MPRFLG 0 ; 
       37 SCHEM 331.25 -9.999989 0 MPRFLG 0 ; 
       38 SCHEM 347.5 -5.999988 0 MPRFLG 0 ; 
       39 SCHEM 301.25 -5.999988 0 MPRFLG 0 ; 
       40 SCHEM 383.75 -5.999988 0 MPRFLG 0 ; 
       41 SCHEM 346.25 -3.999988 0 SRT 1 1 1 0 0 0 0 15.24941 0 MPRFLG 0 ; 
       42 SCHEM 356.25 -11.99999 0 MPRFLG 0 ; 
       43 SCHEM 370 -5.999988 0 MPRFLG 0 ; 
       44 SCHEM 368.1959 -7.778359 0 USR MPRFLG 0 ; 
       45 SCHEM 282.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       46 SCHEM 310 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       47 SCHEM 277.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 280 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 307.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 290 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 317.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 285 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 312.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 287.5 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 315 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 407.5 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       57 SCHEM 410 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       58 SCHEM 412.5 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 415 -7.999988 0 WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 405 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       61 SCHEM 402.5 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       62 SCHEM 400 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 397.5 -7.999988 0 WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 372.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       65 SCHEM 305 -13.99999 0 WIRECOL 3 7 MPRFLG 0 ; 
       66 SCHEM 375 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       67 SCHEM 377.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 380 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 382.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 385 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 390 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 392.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 387.5 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 395 -7.999988 0 WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 283.75 -9.999989 0 MPRFLG 0 ; 
       76 SCHEM 311.25 -9.999989 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
