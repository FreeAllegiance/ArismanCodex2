SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.29-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 61     
       ss106-cube20.2-0 ; 
       ss106-cube22_2_2.1-0 ; 
       ss106-cube22_2_24.1-0 ; 
       ss106-cube22_2_25.1-0 ; 
       ss106-cube22_2_28.1-0 ; 
       ss106-cube23.1-0 ; 
       ss106-cube24.1-0 ; 
       ss106-cube5.2-0 ; 
       ss106-east_bay_11_10.1-0 ; 
       ss106-east_bay_11_9.1-0 ; 
       ss106-extru22.1-0 ; 
       ss106-extru31.1-0 ; 
       ss106-extru34.1-0 ; 
       ss106-extru35.1-0 ; 
       ss106-extru36.1-0 ; 
       ss106-extru37.1-0 ; 
       ss106-extru38.1-0 ; 
       ss106-extru39.1-0 ; 
       ss106-extru40.1-0 ; 
       ss106-extru41.1-0 ; 
       ss106-extru42.1-0 ; 
       ss106-extru9.1-0 ; 
       ss106-garage1A.1-0 ; 
       ss106-garage1B.1-0 ; 
       ss106-garage1C.1-0 ; 
       ss106-garage1D.1-0 ; 
       ss106-garage1E.1-0 ; 
       ss106-launch3.1-0 ; 
       ss106-sphere8.5-0 ; 
       ss106-sphere9_1.1-0 ; 
       ss106-SS_11_1.1-0 ; 
       ss106-SS_11_4.1-0 ; 
       ss106-SS_13_2.1-0 ; 
       ss106-SS_15_1.1-0 ; 
       ss106-SS_15_4.1-0 ; 
       ss106-SS_23_2.1-0 ; 
       ss106-SS_23_4.1-0 ; 
       ss106-SS_24_1.1-0 ; 
       ss106-SS_24_4.1-0 ; 
       ss106-SS_26.1-0 ; 
       ss106-SS_26_4.1-0 ; 
       ss106-SS_29.1-0 ; 
       ss106-SS_30.1-0 ; 
       ss106-SS_31.1-0 ; 
       ss106-SS_32.1-0 ; 
       ss106-SS_33.1-0 ; 
       ss106-SS_34.1-0 ; 
       ss106-SS_35.1-0 ; 
       ss106-SS_36.1-0 ; 
       ss106-SS_55.2-0 ; 
       ss106-SS_58.1-0 ; 
       ss106-SS_70.1-0 ; 
       ss106-SS_71.1-0 ; 
       ss106-SS_72.1-0 ; 
       ss106-SS_73.1-0 ; 
       ss106-SS_74.1-0 ; 
       ss106-SS_75.1-0 ; 
       ss106-SS_76.1-0 ; 
       ss106-SS_77.1-0 ; 
       ss106-SS_78.1-0 ; 
       ss106-ss106.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss106/PICTURES/ss106 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       denulled-bioripcord-ss106.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 60 110 ; 
       1 60 110 ; 
       2 60 110 ; 
       3 60 110 ; 
       4 60 110 ; 
       5 60 110 ; 
       6 60 110 ; 
       7 60 110 ; 
       8 60 110 ; 
       9 60 110 ; 
       10 60 110 ; 
       11 60 110 ; 
       12 60 110 ; 
       13 60 110 ; 
       14 60 110 ; 
       15 60 110 ; 
       16 60 110 ; 
       17 60 110 ; 
       18 60 110 ; 
       19 60 110 ; 
       20 60 110 ; 
       21 60 110 ; 
       22 8 110 ; 
       23 8 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 9 110 ; 
       28 60 110 ; 
       29 60 110 ; 
       30 8 110 ; 
       31 9 110 ; 
       32 8 110 ; 
       33 8 110 ; 
       34 9 110 ; 
       35 8 110 ; 
       36 9 110 ; 
       37 8 110 ; 
       38 9 110 ; 
       39 8 110 ; 
       40 9 110 ; 
       41 60 110 ; 
       42 60 110 ; 
       43 60 110 ; 
       44 60 110 ; 
       45 60 110 ; 
       46 60 110 ; 
       47 60 110 ; 
       48 60 110 ; 
       49 60 110 ; 
       50 9 110 ; 
       51 60 110 ; 
       52 60 110 ; 
       53 60 110 ; 
       54 60 110 ; 
       55 60 110 ; 
       56 60 110 ; 
       57 60 110 ; 
       58 60 110 ; 
       59 60 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -1.999988 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 36.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       1 SCHEM -3.388908 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       2 SCHEM -0.8889084 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       3 SCHEM 4.111092 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       4 SCHEM 34.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       5 SCHEM 1.611092 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       6 SCHEM -5.888908 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       7 SCHEM 71.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       8 SCHEM 19.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       9 SCHEM 121.6111 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       10 SCHEM 49.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       11 SCHEM 39.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       12 SCHEM 51.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       13 SCHEM 64.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       14 SCHEM 66.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       15 SCHEM 56.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       16 SCHEM 59.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       17 SCHEM 61.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       18 SCHEM 41.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       19 SCHEM 44.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       20 SCHEM 46.61109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       21 SCHEM 54.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       22 SCHEM 21.61109 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       23 SCHEM 26.61109 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       24 SCHEM 24.11109 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       25 SCHEM 31.61109 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       26 SCHEM 29.11109 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       27 SCHEM 129.1111 -37.24838 0 WIRECOL 9 7 DISPLAY 2 2 MPRFLG 0 ; 
       60 SCHEM 66.07768 -26.26161 0 USR DISPLAY 3 2 SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 69.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       29 SCHEM 74.11109 -35.24838 0 DISPLAY 2 2 MPRFLG 0 ; 
       30 SCHEM 11.61109 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       31 SCHEM 119.1111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       32 SCHEM 6.611092 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       33 SCHEM 9.111092 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       34 SCHEM 116.6111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       35 SCHEM 19.11109 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       36 SCHEM 126.6111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       37 SCHEM 14.11109 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       38 SCHEM 121.6111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       39 SCHEM 16.61109 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       40 SCHEM 124.1111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       41 SCHEM 111.6111 -35.24838 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       42 SCHEM 131.6111 -35.24838 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       43 SCHEM 134.1111 -35.24838 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       44 SCHEM 136.6111 -35.24838 0 WIRECOL 2 7 DISPLAY 2 2 MPRFLG 0 ; 
       45 SCHEM 109.1111 -35.24838 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       46 SCHEM 106.6111 -35.24838 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       47 SCHEM 104.1111 -35.24838 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       48 SCHEM 101.6111 -35.24838 0 WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       49 SCHEM 76.61109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       50 SCHEM 114.1111 -37.24838 0 WIRECOL 3 7 DISPLAY 2 2 MPRFLG 0 ; 
       51 SCHEM 79.11109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       52 SCHEM 81.61109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       53 SCHEM 84.11109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       54 SCHEM 86.61109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       55 SCHEM 89.11109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       56 SCHEM 94.11109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       57 SCHEM 96.61109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       58 SCHEM 91.61109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
       59 SCHEM 99.11109 -35.24838 0 WIRECOL 1 7 DISPLAY 2 2 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
