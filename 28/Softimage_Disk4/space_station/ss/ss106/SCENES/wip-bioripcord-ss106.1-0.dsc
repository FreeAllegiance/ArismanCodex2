SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       bioresearch_ss104-cam_int1.1-0 ROOT ; 
       bioresearch_ss104-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 40     
       shipyard_ss102-bay_top1_1.1-0 ; 
       shipyard_ss102-bay_top1_6.1-0 ; 
       shipyard_ss102-inside_bay1_1.1-0 ; 
       shipyard_ss102-inside_bay1_6.1-0 ; 
       shipyard_ss102-mat47.1-0 ; 
       shipyard_ss102-mat48.1-0 ; 
       shipyard_ss102-mat58.1-0 ; 
       shipyard_ss102-mat59.1-0 ; 
       shipyard_ss102-mat60.1-0 ; 
       shipyard_ss102-mat67.1-0 ; 
       shipyard_ss102-mat68.1-0 ; 
       shipyard_ss102-mat69.1-0 ; 
       shipyard_ss102-mat75.1-0 ; 
       shipyard_ss102-mat76.1-0 ; 
       shipyard_ss102-white_strobe1_10.1-0 ; 
       shipyard_ss102-white_strobe1_29.1-0 ; 
       shipyard_ss102-white_strobe1_30.1-0 ; 
       shipyard_ss102-white_strobe1_31.1-0 ; 
       shipyard_ss102-white_strobe1_32.1-0 ; 
       shipyard_ss102-white_strobe1_33.1-0 ; 
       shipyard_ss102-white_strobe1_34.1-0 ; 
       shipyard_ss102-white_strobe1_35.1-0 ; 
       shipyard_ss102-white_strobe1_36.1-0 ; 
       shipyard_ss102-white_strobe1_4.1-0 ; 
       shipyard_ss102-white_strobe1_43.1-0 ; 
       shipyard_ss102-white_strobe1_44.1-0 ; 
       shipyard_ss102-white_strobe1_45.1-0 ; 
       shipyard_ss102-white_strobe1_46.1-0 ; 
       shipyard_ss102-white_strobe1_47.1-0 ; 
       shipyard_ss102-white_strobe1_48.1-0 ; 
       shipyard_ss102-white_strobe1_49.1-0 ; 
       shipyard_ss102-white_strobe1_5.1-0 ; 
       shipyard_ss102-white_strobe1_57.1-0 ; 
       shipyard_ss102-white_strobe1_59.1-0 ; 
       shipyard_ss102-white_strobe1_6.1-0 ; 
       shipyard_ss102-white_strobe1_60.1-0 ; 
       shipyard_ss102-white_strobe1_61.1-0 ; 
       shipyard_ss102-white_strobe1_7.1-0 ; 
       shipyard_ss102-white_strobe1_8.1-0 ; 
       shipyard_ss102-white_strobe1_9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 48     
       bioresearch_ss104-east_bay_11_1.4-0 ; 
       bioresearch_ss104-east_bay_11_11.1-0 ROOT ; 
       bioresearch_ss104-east_bay_11_12.1-0 ; 
       bioresearch_ss104-east_bay_11_8.1-0 ROOT ; 
       bioresearch_ss104-garage1A.1-0 ; 
       bioresearch_ss104-garage1B.1-0 ; 
       bioresearch_ss104-garage1C.1-0 ; 
       bioresearch_ss104-garage1D.1-0 ; 
       bioresearch_ss104-garage1E.1-0 ; 
       bioresearch_ss104-landing_lights_2.1-0 ; 
       bioresearch_ss104-landing_lights2.1-0 ; 
       bioresearch_ss104-landing_lights2_4.1-0 ; 
       bioresearch_ss104-landing_lights3.1-0 ; 
       bioresearch_ss104-launch3.1-0 ; 
       bioresearch_ss104-null18.1-0 ROOT ; 
       bioresearch_ss104-null19.1-0 ROOT ; 
       bioresearch_ss104-null26.1-0 ROOT ; 
       bioresearch_ss104-SS_11_1.1-0 ; 
       bioresearch_ss104-SS_11_4.1-0 ; 
       bioresearch_ss104-SS_13_2.1-0 ; 
       bioresearch_ss104-SS_15_1.1-0 ; 
       bioresearch_ss104-SS_15_4.1-0 ; 
       bioresearch_ss104-SS_23_2.1-0 ; 
       bioresearch_ss104-SS_23_4.1-0 ; 
       bioresearch_ss104-SS_24_1.1-0 ; 
       bioresearch_ss104-SS_24_4.1-0 ; 
       bioresearch_ss104-SS_26.1-0 ; 
       bioresearch_ss104-SS_26_4.1-0 ; 
       bioresearch_ss104-SS_29.1-0 ; 
       bioresearch_ss104-SS_30.1-0 ; 
       bioresearch_ss104-SS_31.1-0 ; 
       bioresearch_ss104-SS_32.1-0 ; 
       bioresearch_ss104-SS_33.1-0 ; 
       bioresearch_ss104-SS_34.1-0 ; 
       bioresearch_ss104-SS_35.1-0 ; 
       bioresearch_ss104-SS_36.1-0 ; 
       bioresearch_ss104-SS_55.2-0 ; 
       bioresearch_ss104-SS_58.1-0 ; 
       bioresearch_ss104-SS_60.1-0 ; 
       bioresearch_ss104-SS_68.1-0 ; 
       bioresearch_ss104-SS_70.1-0 ; 
       bioresearch_ss104-SS_71.1-0 ; 
       bioresearch_ss104-SS_72.1-0 ; 
       bioresearch_ss104-strobe_set_1.1-0 ; 
       bioresearch_ss104-strobe_set_4.1-0 ; 
       bioripcord_ss106-sphere10.1-0 ROOT ; 
       bioripcord_ss106-sphere8.1-0 ROOT ; 
       bioripcord_ss106-sphere9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-bioripcord-ss106.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 6     
       shipyard_ss102-t2d49.1-0 ; 
       shipyard_ss102-t2d50.1-0 ; 
       shipyard_ss102-t2d51.1-0 ; 
       shipyard_ss102-t2d58.1-0 ; 
       shipyard_ss102-t2d59.1-0 ; 
       shipyard_ss102-t2d60.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 3 110 ; 
       2 1 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 0 110 ; 
       8 0 110 ; 
       9 43 110 ; 
       10 43 110 ; 
       11 44 110 ; 
       12 44 110 ; 
       13 2 110 ; 
       17 9 110 ; 
       18 12 110 ; 
       19 9 110 ; 
       20 9 110 ; 
       21 12 110 ; 
       22 10 110 ; 
       23 11 110 ; 
       24 10 110 ; 
       25 11 110 ; 
       26 10 110 ; 
       27 11 110 ; 
       37 12 110 ; 
       43 0 110 ; 
       44 2 110 ; 
       28 14 110 ; 
       29 14 110 ; 
       30 14 110 ; 
       31 14 110 ; 
       32 15 110 ; 
       33 15 110 ; 
       34 15 110 ; 
       35 15 110 ; 
       36 16 110 ; 
       38 15 110 ; 
       39 14 110 ; 
       40 16 110 ; 
       41 16 110 ; 
       42 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 0 300 ; 
       0 2 300 ; 
       0 5 300 ; 
       1 9 300 ; 
       1 10 300 ; 
       1 11 300 ; 
       2 12 300 ; 
       2 1 300 ; 
       2 3 300 ; 
       2 13 300 ; 
       3 6 300 ; 
       3 7 300 ; 
       3 8 300 ; 
       17 31 300 ; 
       18 27 300 ; 
       19 34 300 ; 
       20 37 300 ; 
       21 29 300 ; 
       22 38 300 ; 
       23 24 300 ; 
       24 39 300 ; 
       25 25 300 ; 
       26 14 300 ; 
       27 26 300 ; 
       37 28 300 ; 
       28 18 300 ; 
       29 17 300 ; 
       30 16 300 ; 
       31 15 300 ; 
       32 19 300 ; 
       33 20 300 ; 
       34 21 300 ; 
       35 22 300 ; 
       36 23 300 ; 
       38 30 300 ; 
       39 32 300 ; 
       40 33 300 ; 
       41 35 300 ; 
       42 36 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 0 401 ; 
       7 1 401 ; 
       8 2 401 ; 
       9 3 401 ; 
       10 4 401 ; 
       11 5 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5271.777 28.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5271.777 26.28717 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       47 SCHEM 5276.777 28.28717 0 DISPLAY 0 0 SRT 1 0.04047264 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 5234.636 0.9043514 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5262.781 9.431116 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 3.141593 1.570796 0.07473154 -0.2847596 0.04076761 MPRFLG 0 ; 
       2 SCHEM 5262.781 7.431127 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 5234.636 2.904341 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 6.283185 1.570796 0.0747355 20.00688 0.003460124 MPRFLG 0 ; 
       4 SCHEM 5235.886 -1.095654 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5243.386 -1.095654 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5238.386 -1.095654 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 5248.386 -1.095654 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 5245.886 -1.095654 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5223.386 -3.095651 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5230.886 -3.095651 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5264.031 3.431143 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 5256.531 3.431143 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 5271.531 5.431121 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 5274.277 28.28717 0 DISPLAY 1 2 SRT 1 0.04047264 1 0 0 0 0 0 0 MPRFLG 0 ; 
       45 SCHEM 5279.277 28.28717 0 DISPLAY 0 0 SRT 1 0.04047264 1 0 0 0 0 0 0 MPRFLG 0 ; 
       17 SCHEM 5225.886 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 5259.031 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 5220.886 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 5223.386 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 5256.531 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       22 SCHEM 5233.386 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       23 SCHEM 5266.531 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 5228.386 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 5261.531 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 5230.886 -5.095654 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 5264.031 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 5254.031 1.43114 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 5227.136 -1.095654 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 5260.281 5.431121 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 5264.05 -1.55485 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.643968e-014 52.24363 0 MPRFLG 0 ; 
       15 SCHEM 5266.853 -5.466955 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 2.119865e-014 11.03855 1.062176e-007 MPRFLG 0 ; 
       16 SCHEM 5256.125 -4.369867 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.386255e-022 20.84828 0 MPRFLG 0 ; 
       28 SCHEM 5260.3 -3.554857 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 5262.8 -3.554857 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5265.3 -3.554857 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 5267.8 -3.554857 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 5268.103 -7.466958 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 5265.603 -7.466958 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 5263.103 -7.466958 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 5260.603 -7.466958 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 5251.468 -7.25742 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 5270.603 -7.466958 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 5270.375 -3.561618 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 5253.968 -7.25742 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 5256.468 -7.25742 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 5258.968 -7.25742 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 5058.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 5241.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 5060.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 5244.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 5055.609 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 5063.109 -1.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 5065.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 5068.109 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 5070.609 0.552352 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 5249.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 5251.666 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 5254.166 0.552351 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5239.166 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5246.666 -1.447649 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5035.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4945.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4948.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4950.609 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4953.109 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 4623.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4625.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4628.426 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 4630.926 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4511.444 -0.5 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 5231.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 5226.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 5229.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 5224.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 5219.166 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 5221.666 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 4651.814 22.91645 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 5030.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 4978.868 18.71841 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 4530.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 5025.609 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 4535.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 4540.068 16.19025 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 5028.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 5038.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 5033.109 -7.447648 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 5065.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 5068.109 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 5070.609 -1.447648 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 5249.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 5251.666 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5254.166 -1.447649 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
