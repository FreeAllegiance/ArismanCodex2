SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.2-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       bioripcord_ss106-light1.1-0 ROOT ; 
       bioripcord_ss106-light2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 125     
       bioresearch_ss104-null18.2-0 ROOT ; 
       bioresearch_ss104-null19.2-0 ROOT ; 
       bioresearch_ss104-null26.3-0 ROOT ; 
       bioresearch_ss104-SS_29.1-0 ; 
       bioresearch_ss104-SS_30.1-0 ; 
       bioresearch_ss104-SS_31.1-0 ; 
       bioresearch_ss104-SS_32.1-0 ; 
       bioresearch_ss104-SS_33.1-0 ; 
       bioresearch_ss104-SS_34.1-0 ; 
       bioresearch_ss104-SS_35.1-0 ; 
       bioresearch_ss104-SS_36.1-0 ; 
       bioresearch_ss104-SS_55.2-0 ; 
       bioresearch_ss104-SS_60.1-0 ; 
       bioresearch_ss104-SS_68.1-0 ; 
       bioresearch_ss104-SS_70.1-0 ; 
       bioresearch_ss104-SS_71.1-0 ; 
       bioresearch_ss104-SS_72.1-0 ; 
       bioresearch_ss104-SS_73.1-0 ; 
       bioresearch_ss104-SS_74.1-0 ; 
       bioresearch_ss104-SS_75.1-0 ; 
       bioresearch_ss104-SS_76.1-0 ; 
       bioresearch_ss104-SS_77.1-0 ; 
       bioresearch_ss104-SS_78.1-0 ; 
       bioripcord_ss106-east_bay_11_1.4-0 ; 
       bioripcord_ss106-east_bay_11_11.1-0 ; 
       bioripcord_ss106-east_bay_11_12.1-0 ; 
       bioripcord_ss106-east_bay_11_8.1-0 ; 
       bioripcord_ss106-garage1A.1-0 ; 
       bioripcord_ss106-garage1B.1-0 ; 
       bioripcord_ss106-garage1C.1-0 ; 
       bioripcord_ss106-garage1D.1-0 ; 
       bioripcord_ss106-garage1E.1-0 ; 
       bioripcord_ss106-landing_lights_2.1-0 ; 
       bioripcord_ss106-landing_lights2.1-0 ; 
       bioripcord_ss106-landing_lights2_4.1-0 ; 
       bioripcord_ss106-landing_lights3.1-0 ; 
       bioripcord_ss106-launch3.1-0 ; 
       bioripcord_ss106-null27.2-0 ROOT ; 
       bioripcord_ss106-sphere8.4-0 ROOT ; 
       bioripcord_ss106-SS_11_1.1-0 ; 
       bioripcord_ss106-SS_11_4.1-0 ; 
       bioripcord_ss106-SS_13_2.1-0 ; 
       bioripcord_ss106-SS_15_1.1-0 ; 
       bioripcord_ss106-SS_15_4.1-0 ; 
       bioripcord_ss106-SS_23_2.1-0 ; 
       bioripcord_ss106-SS_23_4.1-0 ; 
       bioripcord_ss106-SS_24_1.1-0 ; 
       bioripcord_ss106-SS_24_4.1-0 ; 
       bioripcord_ss106-SS_26.1-0 ; 
       bioripcord_ss106-SS_26_4.1-0 ; 
       bioripcord_ss106-SS_58.1-0 ; 
       bioripcord_ss106-strobe_set_1.1-0 ; 
       bioripcord_ss106-strobe_set_4.1-0 ; 
       ss100_nulls-cube20.2-0 ROOT ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube23.1-0 ; 
       ss100_nulls-cube24.1-0 ; 
       ss100_nulls-cube5.2-0 ; 
       ss100_nulls-east_bay_11.3-0 ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_2.3-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru40.1-0 ; 
       ss100_nulls-extru41.1-0 ; 
       ss100_nulls-extru42.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-launch1.1-0 ; 
       ss100_nulls-launch2.1-0 ; 
       ss100_nulls-null12.1-0 ; 
       ss100_nulls-null13.2-0 ROOT ; 
       ss100_nulls-null14.1-0 ; 
       ss100_nulls-null15.1-0 ROOT ; 
       ss100_nulls-null18.2-0 ROOT ; 
       ss100_nulls-null19.2-0 ROOT ; 
       ss100_nulls-null23.1-0 ; 
       ss100_nulls-null24.1-0 ROOT ; 
       ss100_nulls-null8.1-0 ; 
       ss100_nulls-SS_11.1-0 ; 
       ss100_nulls-SS_11_3.1-0 ; 
       ss100_nulls-SS_13.1-0 ; 
       ss100_nulls-SS_13_3.1-0 ; 
       ss100_nulls-SS_15_2.1-0 ; 
       ss100_nulls-SS_15_3.1-0 ; 
       ss100_nulls-SS_23.1-0 ; 
       ss100_nulls-SS_23_3.1-0 ; 
       ss100_nulls-SS_24.1-0 ; 
       ss100_nulls-SS_24_2.1-0 ; 
       ss100_nulls-SS_26_2.1-0 ; 
       ss100_nulls-SS_26_3.1-0 ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-strobe_set_2.1-0 ; 
       ss100_nulls-strobe_set_3.1-0 ; 
       ss100_nulls-turwepemt2_2.1-0 ; 
       ss100_nulls-turwepemt2_3.1-0 ; 
       ss100_nulls1-null18.1-0 ROOT ; 
       ss100_nulls1-SS_29.1-0 ; 
       ss100_nulls1-SS_30.1-0 ; 
       ss100_nulls1-SS_31.1-0 ; 
       ss100_nulls1-SS_32.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss104/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-bioripcord-ss106.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       58 56 110 ; 
       23 26 110 ; 
       24 37 110 ; 
       25 24 110 ; 
       26 37 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 23 110 ; 
       30 23 110 ; 
       31 23 110 ; 
       32 51 110 ; 
       33 51 110 ; 
       34 52 110 ; 
       35 52 110 ; 
       36 25 110 ; 
       59 54 110 ; 
       124 120 110 ; 
       123 120 110 ; 
       39 32 110 ; 
       40 35 110 ; 
       41 32 110 ; 
       42 32 110 ; 
       43 35 110 ; 
       44 33 110 ; 
       45 34 110 ; 
       46 33 110 ; 
       47 34 110 ; 
       48 33 110 ; 
       49 34 110 ; 
       50 35 110 ; 
       51 23 110 ; 
       52 25 110 ; 
       122 120 110 ; 
       121 120 110 ; 
       21 2 110 ; 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 1 110 ; 
       17 2 110 ; 
       13 0 110 ; 
       18 2 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       22 2 110 ; 
       54 93 110 ; 
       55 93 110 ; 
       56 93 110 ; 
       57 93 110 ; 
       60 88 110 ; 
       61 94 110 ; 
       62 94 110 ; 
       63 94 110 ; 
       64 94 110 ; 
       65 90 110 ; 
       66 87 110 ; 
       67 89 110 ; 
       68 87 110 ; 
       69 87 110 ; 
       70 87 110 ; 
       71 95 110 ; 
       72 95 110 ; 
       73 95 110 ; 
       74 89 110 ; 
       75 89 110 ; 
       76 89 110 ; 
       77 90 110 ; 
       78 90 110 ; 
       79 90 110 ; 
       80 95 110 ; 
       81 116 110 ; 
       82 117 110 ; 
       83 116 110 ; 
       84 117 110 ; 
       85 61 110 ; 
       86 63 110 ; 
       87 88 110 ; 
       89 53 110 ; 
       93 53 110 ; 
       95 87 110 ; 
       96 82 110 ; 
       97 81 110 ; 
       98 81 110 ; 
       99 82 110 ; 
       100 81 110 ; 
       101 82 110 ; 
       102 84 110 ; 
       103 83 110 ; 
       104 84 110 ; 
       105 83 110 ; 
       106 83 110 ; 
       107 84 110 ; 
       108 91 110 ; 
       109 91 110 ; 
       110 91 110 ; 
       111 91 110 ; 
       112 92 110 ; 
       113 92 110 ; 
       114 92 110 ; 
       115 92 110 ; 
       116 63 110 ; 
       117 61 110 ; 
       118 63 110 ; 
       119 61 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5238.292 14.65863 0 USR MPRFLG 0 ; 
       1 SCHEM 5238.292 12.65863 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5359.238 14.65863 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 5361.738 14.65863 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       58 SCHEM 5364.238 14.65863 0 MPRFLG 0 ; 
       23 SCHEM 5305.644 -53.54779 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 5333.789 -45.02101 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 5333.789 -47.021 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 5305.644 -51.54781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 5306.894 -55.54781 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 5314.394 -55.54781 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 5309.394 -55.54781 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5319.394 -55.54781 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 5316.894 -55.54781 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 5294.394 -57.54781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 5301.894 -57.54781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 5335.039 -51.021 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 5327.539 -51.021 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 5342.539 -49.02101 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 5336.571 1.627643 0 USR SRT 11.64537 0.4713185 11.64537 0 0 0 0 0 0 MPRFLG 0 ; 
       59 SCHEM 5366.738 14.65863 0 MPRFLG 0 ; 
       37 SCHEM 5352.785 -26.16497 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 1.570796 0 0 -1.726289 0 MPRFLG 0 ; 
       120 SCHEM 5288.186 -16.57483 0 USR DISPLAY 0 0 SRT 0.8770324 0.8770324 0.8770324 -3.335192e-020 0 0 1.062526e-006 -48.84949 -1.02364e-006 MPRFLG 0 ; 
       124 SCHEM 5291.936 -18.57483 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       123 SCHEM 5289.436 -18.57483 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 5296.894 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 5330.039 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 5291.894 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 5294.394 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 5327.539 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 5304.394 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 5337.539 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 5299.394 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 5332.539 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 5301.894 -59.54781 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 5335.039 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 5325.039 -53.02101 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 5298.144 -55.54781 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 5331.289 -49.02101 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       122 SCHEM 5286.936 -18.57483 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       121 SCHEM 5284.436 -18.57483 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 5370.784 -13.91263 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 5297.8 7.126568 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.643968e-014 52.24363 0 MPRFLG 0 ; 
       1 SCHEM 5300.603 3.214463 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 2.119865e-014 11.03855 1.062176e-007 MPRFLG 0 ; 
       2 SCHEM 5362.826 -9.222984 0 USR SRT 1 1 1 0 0 0 -2.386255e-022 20.84828 0 MPRFLG 0 ; 
       3 SCHEM 5294.05 5.126563 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5296.55 5.126563 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5299.05 5.126563 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5301.55 5.126563 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 5301.853 1.21446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 5299.353 1.21446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5296.853 1.21446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5294.353 1.21446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5358.169 -12.11054 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5304.353 1.21446 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 5368.033 -12.12275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 5304.125 5.119802 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 5370.533 -12.12275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 5360.669 -12.11054 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5363.169 -12.11054 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5365.669 -12.11054 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5373.033 -12.12275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5375.533 -12.12275 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5375.784 -13.91263 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       53 SCHEM 5330.702 -4.88484 0 USR SRT 1.354 1.270594 1.354 -3.335192e-020 2.661841e-023 0 1.062526e-006 -3.537966 -1.02364e-006 MPRFLG 0 ; 
       54 SCHEM 5316.952 -8.884841 0 USR MPRFLG 0 ; 
       55 SCHEM 5319.452 -8.884841 0 USR MPRFLG 0 ; 
       56 SCHEM 5321.952 -8.884841 0 USR MPRFLG 0 ; 
       57 SCHEM 5324.452 -8.884841 0 USR MPRFLG 0 ; 
       60 SCHEM 5356.738 -4.486528 0 USR MPRFLG 0 ; 
       61 SCHEM 5317.798 -13.24664 0 USR MPRFLG 0 ; 
       62 SCHEM 5351.548 -13.24664 0 USR MPRFLG 0 ; 
       63 SCHEM 5337.798 -13.24664 0 USR MPRFLG 0 ; 
       64 SCHEM 5349.048 -13.24664 0 USR MPRFLG 0 ; 
       65 SCHEM 5315.683 -35.75615 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       66 SCHEM 5336.738 -6.486531 0 USR MPRFLG 0 ; 
       67 SCHEM 5326.952 -8.884841 0 USR MPRFLG 0 ; 
       68 SCHEM 5339.238 -6.486531 0 USR MPRFLG 0 ; 
       69 SCHEM 5351.738 -6.486531 0 USR MPRFLG 0 ; 
       70 SCHEM 5354.238 -6.486531 0 USR MPRFLG 0 ; 
       71 SCHEM 5344.238 -8.486533 0 USR MPRFLG 0 ; 
       72 SCHEM 5346.738 -8.486533 0 USR MPRFLG 0 ; 
       73 SCHEM 5349.238 -8.486533 0 USR MPRFLG 0 ; 
       74 SCHEM 5329.452 -8.884841 0 USR MPRFLG 0 ; 
       75 SCHEM 5331.952 -8.884841 0 USR MPRFLG 0 ; 
       76 SCHEM 5334.452 -8.884841 0 USR MPRFLG 0 ; 
       77 SCHEM 5318.183 -35.75615 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       78 SCHEM 5320.683 -35.75615 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 5323.183 -35.75615 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 5341.738 -8.486533 0 USR MPRFLG 0 ; 
       81 SCHEM 5331.548 -17.24663 0 USR MPRFLG 0 ; 
       82 SCHEM 5311.548 -17.24663 0 USR MPRFLG 0 ; 
       83 SCHEM 5339.048 -17.24663 0 USR MPRFLG 0 ; 
       84 SCHEM 5319.048 -17.24663 0 USR MPRFLG 0 ; 
       85 SCHEM 5326.548 -15.24664 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       86 SCHEM 5346.548 -15.24664 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       87 SCHEM 5345.488 -4.486528 0 USR MPRFLG 0 ; 
       88 SCHEM 5346.738 -2.48653 0 USR SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       89 SCHEM 5330.702 -6.884842 0 USR MPRFLG 0 ; 
       90 SCHEM 5319.433 -33.75614 0 USR DISPLAY 0 0 SRT 1.045418 1.045418 1.045418 -3.335192e-020 2.661841e-023 0 1.062526e-006 -44.22925 -1.02364e-006 MPRFLG 0 ; 
       91 SCHEM 5288.234 -12.73283 0 USR DISPLAY 0 0 SRT 1.265879 1.265879 1.265879 -3.335192e-020 2.661841e-023 0 1.062526e-006 -49.46897 -1.02364e-006 MPRFLG 0 ; 
       92 SCHEM 5289.458 -6.903888 0 USR DISPLAY 3 2 SRT 13.25445 13.1829 6.12104 0 0 0 1.062526e-006 -0.7228203 -9.174224e-007 MPRFLG 0 ; 
       93 SCHEM 5320.702 -6.884842 0 USR MPRFLG 0 ; 
       94 SCHEM 5330.298 -11.24664 0 USR SRT 1 1 1 -3.019916e-007 1.570796 2.622683e-007 1.062526e-006 1.698984 -1.02364e-006 MPRFLG 0 ; 
       95 SCHEM 5345.488 -6.486531 0 USR MPRFLG 0 ; 
       96 SCHEM 5314.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       97 SCHEM 5334.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 5329.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 5309.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       100 SCHEM 5331.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 5311.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       102 SCHEM 5321.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       103 SCHEM 5341.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       104 SCHEM 5316.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       105 SCHEM 5336.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       106 SCHEM 5339.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       107 SCHEM 5319.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       108 SCHEM 5284.484 -14.73283 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       109 SCHEM 5286.984 -14.73283 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       110 SCHEM 5289.484 -14.73283 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       111 SCHEM 5291.984 -14.73283 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       112 SCHEM 5293.208 -8.903888 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       113 SCHEM 5290.708 -8.903888 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       114 SCHEM 5288.208 -8.903888 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       115 SCHEM 5285.708 -8.903888 0 USR WIRECOL 4 7 DISPLAY 2 2 MPRFLG 0 ; 
       116 SCHEM 5335.298 -15.24664 0 USR MPRFLG 0 ; 
       117 SCHEM 5315.298 -15.24664 0 USR MPRFLG 0 ; 
       118 SCHEM 5344.048 -15.24664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       119 SCHEM 5324.048 -15.24664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
