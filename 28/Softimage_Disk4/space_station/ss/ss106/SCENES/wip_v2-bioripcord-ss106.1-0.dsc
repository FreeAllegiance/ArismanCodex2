SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.3-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       bioripcord_ss106-light1.2-0 ROOT ; 
       bioripcord_ss106-light2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 126     
       bioresearch_ss104-null18.3-0 ROOT ; 
       bioresearch_ss104-null19.3-0 ROOT ; 
       bioresearch_ss104-null26.4-0 ROOT ; 
       bioresearch_ss104-SS_29.1-0 ; 
       bioresearch_ss104-SS_30.1-0 ; 
       bioresearch_ss104-SS_31.1-0 ; 
       bioresearch_ss104-SS_32.1-0 ; 
       bioresearch_ss104-SS_33.1-0 ; 
       bioresearch_ss104-SS_34.1-0 ; 
       bioresearch_ss104-SS_35.1-0 ; 
       bioresearch_ss104-SS_36.1-0 ; 
       bioresearch_ss104-SS_55.2-0 ; 
       bioresearch_ss104-SS_60.1-0 ; 
       bioresearch_ss104-SS_68.1-0 ; 
       bioresearch_ss104-SS_70.1-0 ; 
       bioresearch_ss104-SS_71.1-0 ; 
       bioresearch_ss104-SS_72.1-0 ; 
       bioresearch_ss104-SS_73.1-0 ; 
       bioresearch_ss104-SS_74.1-0 ; 
       bioresearch_ss104-SS_75.1-0 ; 
       bioresearch_ss104-SS_76.1-0 ; 
       bioresearch_ss104-SS_77.1-0 ; 
       bioresearch_ss104-SS_78.1-0 ; 
       bioripcord_ss106-east_bay_11_1.4-0 ; 
       bioripcord_ss106-east_bay_11_11.1-0 ; 
       bioripcord_ss106-east_bay_11_12.1-0 ; 
       bioripcord_ss106-east_bay_11_8.1-0 ; 
       bioripcord_ss106-garage1A.1-0 ; 
       bioripcord_ss106-garage1B.1-0 ; 
       bioripcord_ss106-garage1C.1-0 ; 
       bioripcord_ss106-garage1D.1-0 ; 
       bioripcord_ss106-garage1E.1-0 ; 
       bioripcord_ss106-landing_lights_2.1-0 ; 
       bioripcord_ss106-landing_lights2.1-0 ; 
       bioripcord_ss106-landing_lights2_4.1-0 ; 
       bioripcord_ss106-landing_lights3.1-0 ; 
       bioripcord_ss106-launch3.1-0 ; 
       bioripcord_ss106-null27.3-0 ROOT ; 
       bioripcord_ss106-sphere8.5-0 ROOT ; 
       bioripcord_ss106-sphere9.1-0 ; 
       bioripcord_ss106-SS_11_1.1-0 ; 
       bioripcord_ss106-SS_11_4.1-0 ; 
       bioripcord_ss106-SS_13_2.1-0 ; 
       bioripcord_ss106-SS_15_1.1-0 ; 
       bioripcord_ss106-SS_15_4.1-0 ; 
       bioripcord_ss106-SS_23_2.1-0 ; 
       bioripcord_ss106-SS_23_4.1-0 ; 
       bioripcord_ss106-SS_24_1.1-0 ; 
       bioripcord_ss106-SS_24_4.1-0 ; 
       bioripcord_ss106-SS_26.1-0 ; 
       bioripcord_ss106-SS_26_4.1-0 ; 
       bioripcord_ss106-SS_58.1-0 ; 
       bioripcord_ss106-strobe_set_1.1-0 ; 
       bioripcord_ss106-strobe_set_4.1-0 ; 
       ss100_nulls-cube20.3-0 ROOT ; 
       ss100_nulls-cube22_2_2.1-0 ; 
       ss100_nulls-cube22_2_24.1-0 ; 
       ss100_nulls-cube22_2_25.1-0 ; 
       ss100_nulls-cube22_2_28.1-0 ; 
       ss100_nulls-cube23.1-0 ; 
       ss100_nulls-cube24.1-0 ; 
       ss100_nulls-cube5.2-0 ; 
       ss100_nulls-east_bay_11.3-0 ; 
       ss100_nulls-east_bay_11_10.1-0 ; 
       ss100_nulls-east_bay_11_2.3-0 ; 
       ss100_nulls-east_bay_11_9.1-0 ; 
       ss100_nulls-extru15.1-0 ; 
       ss100_nulls-extru22.1-0 ; 
       ss100_nulls-extru31.1-0 ; 
       ss100_nulls-extru34.1-0 ; 
       ss100_nulls-extru35.1-0 ; 
       ss100_nulls-extru36.1-0 ; 
       ss100_nulls-extru37.1-0 ; 
       ss100_nulls-extru38.1-0 ; 
       ss100_nulls-extru39.1-0 ; 
       ss100_nulls-extru40.1-0 ; 
       ss100_nulls-extru41.1-0 ; 
       ss100_nulls-extru42.1-0 ; 
       ss100_nulls-extru43.1-0 ; 
       ss100_nulls-extru44.1-0 ; 
       ss100_nulls-extru45.1-0 ; 
       ss100_nulls-extru9.1-0 ; 
       ss100_nulls-landing_lights.1-0 ; 
       ss100_nulls-landing_lights_3.1-0 ; 
       ss100_nulls-landing_lights2_2.1-0 ; 
       ss100_nulls-landing_lights2_3.1-0 ; 
       ss100_nulls-launch1.1-0 ; 
       ss100_nulls-launch2.1-0 ; 
       ss100_nulls-null12.1-0 ; 
       ss100_nulls-null13.3-0 ROOT ; 
       ss100_nulls-null14.1-0 ; 
       ss100_nulls-null15.2-0 ROOT ; 
       ss100_nulls-null18.3-0 ROOT ; 
       ss100_nulls-null19.3-0 ROOT ; 
       ss100_nulls-null23.1-0 ; 
       ss100_nulls-null24.2-0 ROOT ; 
       ss100_nulls-null8.1-0 ; 
       ss100_nulls-SS_11.1-0 ; 
       ss100_nulls-SS_11_3.1-0 ; 
       ss100_nulls-SS_13.1-0 ; 
       ss100_nulls-SS_13_3.1-0 ; 
       ss100_nulls-SS_15_2.1-0 ; 
       ss100_nulls-SS_15_3.1-0 ; 
       ss100_nulls-SS_23.1-0 ; 
       ss100_nulls-SS_23_3.1-0 ; 
       ss100_nulls-SS_24.1-0 ; 
       ss100_nulls-SS_24_2.1-0 ; 
       ss100_nulls-SS_26_2.1-0 ; 
       ss100_nulls-SS_26_3.1-0 ; 
       ss100_nulls-SS_29.1-0 ; 
       ss100_nulls-SS_30.1-0 ; 
       ss100_nulls-SS_31.1-0 ; 
       ss100_nulls-SS_32.1-0 ; 
       ss100_nulls-SS_33.1-0 ; 
       ss100_nulls-SS_34.1-0 ; 
       ss100_nulls-SS_35.1-0 ; 
       ss100_nulls-SS_36.1-0 ; 
       ss100_nulls-strobe_set_2.1-0 ; 
       ss100_nulls-strobe_set_3.1-0 ; 
       ss100_nulls-turwepemt2_2.1-0 ; 
       ss100_nulls-turwepemt2_3.1-0 ; 
       ss100_nulls1-null18.2-0 ROOT ; 
       ss100_nulls1-SS_29.1-0 ; 
       ss100_nulls1-SS_30.1-0 ; 
       ss100_nulls1-SS_31.1-0 ; 
       ss100_nulls1-SS_32.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip_v2-bioripcord-ss106.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 0 110 ; 
       4 0 110 ; 
       5 0 110 ; 
       6 0 110 ; 
       7 1 110 ; 
       8 1 110 ; 
       9 1 110 ; 
       10 1 110 ; 
       11 2 110 ; 
       12 1 110 ; 
       13 0 110 ; 
       14 2 110 ; 
       15 2 110 ; 
       16 2 110 ; 
       17 2 110 ; 
       18 2 110 ; 
       19 2 110 ; 
       20 2 110 ; 
       21 2 110 ; 
       22 2 110 ; 
       23 26 110 ; 
       24 37 110 ; 
       25 24 110 ; 
       26 37 110 ; 
       27 23 110 ; 
       28 23 110 ; 
       29 23 110 ; 
       30 23 110 ; 
       31 23 110 ; 
       32 52 110 ; 
       33 52 110 ; 
       34 53 110 ; 
       35 53 110 ; 
       36 25 110 ; 
       40 32 110 ; 
       41 35 110 ; 
       42 32 110 ; 
       43 32 110 ; 
       44 35 110 ; 
       45 33 110 ; 
       46 34 110 ; 
       47 33 110 ; 
       48 34 110 ; 
       49 33 110 ; 
       50 34 110 ; 
       51 35 110 ; 
       52 23 110 ; 
       53 25 110 ; 
       55 94 110 ; 
       56 94 110 ; 
       57 94 110 ; 
       58 94 110 ; 
       59 57 110 ; 
       60 55 110 ; 
       61 89 110 ; 
       62 95 110 ; 
       63 95 110 ; 
       64 95 110 ; 
       65 95 110 ; 
       66 91 110 ; 
       67 88 110 ; 
       68 90 110 ; 
       69 88 110 ; 
       70 88 110 ; 
       71 88 110 ; 
       72 96 110 ; 
       73 96 110 ; 
       74 96 110 ; 
       75 90 110 ; 
       76 90 110 ; 
       77 90 110 ; 
       78 91 110 ; 
       79 91 110 ; 
       80 91 110 ; 
       81 96 110 ; 
       82 117 110 ; 
       83 118 110 ; 
       84 117 110 ; 
       85 118 110 ; 
       86 62 110 ; 
       87 64 110 ; 
       88 89 110 ; 
       90 54 110 ; 
       94 54 110 ; 
       96 88 110 ; 
       97 83 110 ; 
       98 82 110 ; 
       99 82 110 ; 
       100 83 110 ; 
       101 82 110 ; 
       102 83 110 ; 
       103 85 110 ; 
       104 84 110 ; 
       105 85 110 ; 
       106 84 110 ; 
       107 84 110 ; 
       108 85 110 ; 
       109 92 110 ; 
       110 92 110 ; 
       111 92 110 ; 
       112 92 110 ; 
       113 93 110 ; 
       114 93 110 ; 
       115 93 110 ; 
       116 93 110 ; 
       117 64 110 ; 
       118 62 110 ; 
       119 64 110 ; 
       120 62 110 ; 
       122 121 110 ; 
       123 121 110 ; 
       124 121 110 ; 
       125 121 110 ; 
       39 38 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 5669.851 21.86298 0 USR MPRFLG 0 ; 
       1 SCHEM 5669.851 19.86299 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 5725.314 -66.4613 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 5727.814 -66.4613 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5663.876 -73.99332 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 -2.643968e-014 52.24363 0 MPRFLG 0 ; 
       1 SCHEM 5666.679 -77.9054 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 2.119865e-014 11.03855 1.062176e-007 MPRFLG 0 ; 
       2 SCHEM 5732.702 -12.39048 0 USR SRT 1 1 1 0 0 0 -2.386255e-022 20.84828 0 MPRFLG 0 ; 
       3 SCHEM 5660.126 -75.99331 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 5662.626 -75.99331 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 5665.126 -75.99331 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 5667.626 -75.99331 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 5667.929 -79.9054 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 5665.429 -79.9054 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 5662.929 -79.9054 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5660.429 -79.9054 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5728.045 -15.27803 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 5670.429 -79.9054 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 5670.201 -76.00008 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 5730.545 -15.27803 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 5733.045 -15.27803 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5735.545 -15.27803 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 5737.909 -15.29024 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 5740.409 -15.29024 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 5742.909 -15.29024 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 5745.409 -15.29024 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 5740.66 -17.08012 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 5745.66 -17.08012 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 5672.033 -85.77238 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       24 SCHEM 5700.178 -77.24561 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       25 SCHEM 5700.178 -79.24559 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       26 SCHEM 5672.033 -83.7724 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 5673.283 -87.7724 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 5680.783 -87.7724 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 5675.783 -87.7724 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 5685.783 -87.7724 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 5683.283 -87.7724 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 5660.783 -89.7724 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 5668.283 -89.7724 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 5701.428 -83.24559 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 5693.928 -83.24559 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 5708.928 -81.24561 0 USR WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 5719.174 -58.38957 0 USR DISPLAY 0 0 SRT 1 1 1 1.570796 1.570796 0 0 -1.726289 0 MPRFLG 0 ; 
       38 SCHEM 5706.723 8.346695 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       40 SCHEM 5663.283 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 5696.428 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 5658.283 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       43 SCHEM 5660.783 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 5693.928 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       45 SCHEM 5670.783 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 5703.928 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       47 SCHEM 5665.783 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       48 SCHEM 5698.928 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       49 SCHEM 5668.283 -91.7724 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       50 SCHEM 5701.428 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       51 SCHEM 5691.428 -85.24561 0 USR WIRECOL 3 7 DISPLAY 0 0 MPRFLG 0 ; 
       52 SCHEM 5664.533 -87.7724 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       53 SCHEM 5697.678 -81.24561 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       54 SCHEM 5698.202 -4.88484 0 USR SRT 1 1 1 -3.335192e-020 2.661841e-023 0 1.062526e-006 -3.537966 -1.02364e-006 MPRFLG 0 ; 
       55 SCHEM 5684.452 -8.884841 0 USR MPRFLG 0 ; 
       56 SCHEM 5686.952 -8.884841 0 USR MPRFLG 0 ; 
       57 SCHEM 5689.452 -8.884841 0 USR MPRFLG 0 ; 
       58 SCHEM 5691.952 -8.884841 0 USR MPRFLG 0 ; 
       59 SCHEM 5682.549 -11.57583 0 USR MPRFLG 0 ; 
       60 SCHEM 5680.675 -11.21146 0 USR MPRFLG 0 ; 
       61 SCHEM 5724.238 -4.486528 0 USR MPRFLG 0 ; 
       62 SCHEM 5685.298 -13.24664 0 USR MPRFLG 0 ; 
       63 SCHEM 5719.048 -13.24664 0 USR MPRFLG 0 ; 
       64 SCHEM 5705.298 -13.24664 0 USR MPRFLG 0 ; 
       65 SCHEM 5716.548 -13.24664 0 USR DISPLAY 1 2 MPRFLG 0 ; 
       66 SCHEM 5682.072 -78.59267 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       67 SCHEM 5704.238 -6.486531 0 USR MPRFLG 0 ; 
       68 SCHEM 5694.452 -8.884841 0 USR MPRFLG 0 ; 
       69 SCHEM 5706.738 -6.486531 0 USR MPRFLG 0 ; 
       70 SCHEM 5719.238 -6.486531 0 USR MPRFLG 0 ; 
       71 SCHEM 5721.738 -6.486531 0 USR MPRFLG 0 ; 
       72 SCHEM 5711.738 -8.486533 0 USR MPRFLG 0 ; 
       73 SCHEM 5714.238 -8.486533 0 USR MPRFLG 0 ; 
       74 SCHEM 5716.738 -8.486533 0 USR MPRFLG 0 ; 
       75 SCHEM 5696.952 -8.884841 0 USR MPRFLG 0 ; 
       76 SCHEM 5699.452 -8.884841 0 USR MPRFLG 0 ; 
       77 SCHEM 5701.952 -8.884841 0 USR MPRFLG 0 ; 
       78 SCHEM 5684.572 -78.59267 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       79 SCHEM 5687.072 -78.59267 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       80 SCHEM 5689.572 -78.59267 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       81 SCHEM 5709.238 -8.486533 0 USR MPRFLG 0 ; 
       82 SCHEM 5699.048 -17.24663 0 USR MPRFLG 0 ; 
       83 SCHEM 5679.048 -17.24663 0 USR MPRFLG 0 ; 
       84 SCHEM 5706.548 -17.24663 0 USR MPRFLG 0 ; 
       85 SCHEM 5686.548 -17.24663 0 USR MPRFLG 0 ; 
       86 SCHEM 5694.048 -15.24664 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       87 SCHEM 5714.048 -15.24664 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       88 SCHEM 5712.988 -4.486528 0 USR MPRFLG 0 ; 
       89 SCHEM 5714.238 -2.48653 0 USR SRT 1 1 1 3.141593 0 0 0 0 0 MPRFLG 0 ; 
       90 SCHEM 5698.202 -6.884842 0 USR MPRFLG 0 ; 
       91 SCHEM 5685.822 -76.59267 0 USR DISPLAY 0 0 SRT 1.045418 1.045418 1.045418 -3.335192e-020 2.661841e-023 0 1.062526e-006 -44.22925 -1.02364e-006 MPRFLG 0 ; 
       92 SCHEM 5736.292 -7.193474 0 USR SRT 1 1 1 -3.335192e-020 2.661841e-023 0 1.062526e-006 0.6804352 -1.02364e-006 MPRFLG 0 ; 
       93 SCHEM 5734.827 -1.9976 0 USR SRT 1 1 1 0 0 0 1.062526e-006 -0.7228203 -9.174224e-007 MPRFLG 0 ; 
       94 SCHEM 5688.202 -6.884842 0 USR MPRFLG 0 ; 
       95 SCHEM 5697.798 -11.24664 0 USR SRT 1 1 1 -3.019916e-007 1.570796 2.622683e-007 1.062526e-006 1.698984 -1.02364e-006 MPRFLG 0 ; 
       96 SCHEM 5712.988 -6.486531 0 USR MPRFLG 0 ; 
       97 SCHEM 5681.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       98 SCHEM 5701.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       99 SCHEM 5696.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       100 SCHEM 5676.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       101 SCHEM 5699.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       102 SCHEM 5679.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       103 SCHEM 5689.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       104 SCHEM 5709.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       105 SCHEM 5684.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       106 SCHEM 5704.048 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       107 SCHEM 5706.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       108 SCHEM 5686.548 -19.24663 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       109 SCHEM 5732.542 -9.193473 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       110 SCHEM 5735.042 -9.193473 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       111 SCHEM 5737.542 -9.193473 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       112 SCHEM 5740.042 -9.193473 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       113 SCHEM 5738.577 -3.9976 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       114 SCHEM 5736.077 -3.9976 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       115 SCHEM 5733.577 -3.9976 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       116 SCHEM 5731.077 -3.9976 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       117 SCHEM 5702.798 -15.24664 0 USR MPRFLG 0 ; 
       118 SCHEM 5682.798 -15.24664 0 USR MPRFLG 0 ; 
       119 SCHEM 5711.548 -15.24664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       120 SCHEM 5691.548 -15.24664 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       121 SCHEM 5654.262 -97.69466 0 USR DISPLAY 0 0 SRT 0.8770324 0.8770324 0.8770324 -3.335192e-020 0 0 1.062526e-006 -48.84949 -1.02364e-006 MPRFLG 0 ; 
       122 SCHEM 5650.512 -99.69466 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       123 SCHEM 5653.012 -99.69466 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       124 SCHEM 5655.512 -99.69466 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       125 SCHEM 5658.012 -99.69466 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 5706.723 6.346694 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
