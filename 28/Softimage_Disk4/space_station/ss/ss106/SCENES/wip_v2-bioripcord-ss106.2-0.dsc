SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.4-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 79     
       bioripcord_ss106-cube20.2-0 ; 
       bioripcord_ss106-cube22_2_2.1-0 ; 
       bioripcord_ss106-cube22_2_24.1-0 ; 
       bioripcord_ss106-cube22_2_25.1-0 ; 
       bioripcord_ss106-cube22_2_28.1-0 ; 
       bioripcord_ss106-cube23.1-0 ; 
       bioripcord_ss106-cube24.1-0 ; 
       bioripcord_ss106-cube5.2-0 ; 
       bioripcord_ss106-east_bay_11_1.4-0 ; 
       bioripcord_ss106-east_bay_11_10.1-0 ; 
       bioripcord_ss106-east_bay_11_12.1-0 ; 
       bioripcord_ss106-east_bay_11_9.1-0 ; 
       bioripcord_ss106-extru22.1-0 ; 
       bioripcord_ss106-extru31.1-0 ; 
       bioripcord_ss106-extru34.1-0 ; 
       bioripcord_ss106-extru35.1-0 ; 
       bioripcord_ss106-extru36.1-0 ; 
       bioripcord_ss106-extru37.1-0 ; 
       bioripcord_ss106-extru38.1-0 ; 
       bioripcord_ss106-extru39.1-0 ; 
       bioripcord_ss106-extru40.1-0 ; 
       bioripcord_ss106-extru41.1-0 ; 
       bioripcord_ss106-extru42.1-0 ; 
       bioripcord_ss106-extru9.1-0 ; 
       bioripcord_ss106-garage1A.1-0 ; 
       bioripcord_ss106-garage1B.1-0 ; 
       bioripcord_ss106-garage1C.1-0 ; 
       bioripcord_ss106-garage1D.1-0 ; 
       bioripcord_ss106-garage1E.1-0 ; 
       bioripcord_ss106-landing_lights_2.1-0 ; 
       bioripcord_ss106-landing_lights2.1-0 ; 
       bioripcord_ss106-landing_lights2_4.1-0 ; 
       bioripcord_ss106-landing_lights3.1-0 ; 
       bioripcord_ss106-launch3.1-0 ; 
       bioripcord_ss106-null12.1-0 ; 
       bioripcord_ss106-null13.2-0 ; 
       bioripcord_ss106-null14.1-0 ; 
       bioripcord_ss106-null18.3-0 ; 
       bioripcord_ss106-null19.3-0 ; 
       bioripcord_ss106-null23.1-0 ; 
       bioripcord_ss106-null24.1-0 ; 
       bioripcord_ss106-null24_1.2-0 ; 
       bioripcord_ss106-null26.4-0 ; 
       bioripcord_ss106-null28.1-0 ROOT ; 
       bioripcord_ss106-null8.1-0 ; 
       bioripcord_ss106-sphere8.5-0 ; 
       bioripcord_ss106-sphere9.1-0 ; 
       bioripcord_ss106-SS_11_1.1-0 ; 
       bioripcord_ss106-SS_11_4.1-0 ; 
       bioripcord_ss106-SS_13_2.1-0 ; 
       bioripcord_ss106-SS_15_1.1-0 ; 
       bioripcord_ss106-SS_15_4.1-0 ; 
       bioripcord_ss106-SS_23_2.1-0 ; 
       bioripcord_ss106-SS_23_4.1-0 ; 
       bioripcord_ss106-SS_24_1.1-0 ; 
       bioripcord_ss106-SS_24_4.1-0 ; 
       bioripcord_ss106-SS_26.1-0 ; 
       bioripcord_ss106-SS_26_4.1-0 ; 
       bioripcord_ss106-SS_29.1-0 ; 
       bioripcord_ss106-SS_30.1-0 ; 
       bioripcord_ss106-SS_31.1-0 ; 
       bioripcord_ss106-SS_32.1-0 ; 
       bioripcord_ss106-SS_33.1-0 ; 
       bioripcord_ss106-SS_34.1-0 ; 
       bioripcord_ss106-SS_35.1-0 ; 
       bioripcord_ss106-SS_36.1-0 ; 
       bioripcord_ss106-SS_55.2-0 ; 
       bioripcord_ss106-SS_58.1-0 ; 
       bioripcord_ss106-SS_70.1-0 ; 
       bioripcord_ss106-SS_71.1-0 ; 
       bioripcord_ss106-SS_72.1-0 ; 
       bioripcord_ss106-SS_73.1-0 ; 
       bioripcord_ss106-SS_74.1-0 ; 
       bioripcord_ss106-SS_75.1-0 ; 
       bioripcord_ss106-SS_76.1-0 ; 
       bioripcord_ss106-SS_77.1-0 ; 
       bioripcord_ss106-SS_78.1-0 ; 
       bioripcord_ss106-strobe_set_1.1-0 ; 
       bioripcord_ss106-strobe_set_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/biosbay ; 
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss100/PICTURES/ss100 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip_v2-bioripcord-ss106.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       40 43 110 ; 
       42 43 110 ; 
       66 42 110 ; 
       68 42 110 ; 
       69 42 110 ; 
       70 42 110 ; 
       71 42 110 ; 
       72 42 110 ; 
       73 42 110 ; 
       74 42 110 ; 
       75 42 110 ; 
       76 42 110 ; 
       8 41 110 ; 
       10 41 110 ; 
       24 8 110 ; 
       25 8 110 ; 
       26 8 110 ; 
       27 8 110 ; 
       28 8 110 ; 
       29 77 110 ; 
       30 77 110 ; 
       31 78 110 ; 
       32 78 110 ; 
       33 10 110 ; 
       45 43 110 ; 
       46 45 110 ; 
       47 29 110 ; 
       48 32 110 ; 
       49 29 110 ; 
       50 29 110 ; 
       51 32 110 ; 
       52 30 110 ; 
       53 31 110 ; 
       54 30 110 ; 
       55 31 110 ; 
       56 30 110 ; 
       57 31 110 ; 
       67 32 110 ; 
       77 8 110 ; 
       78 10 110 ; 
       9 41 110 ; 
       11 41 110 ; 
       37 43 110 ; 
       38 43 110 ; 
       41 43 110 ; 
       58 37 110 ; 
       59 37 110 ; 
       60 37 110 ; 
       61 37 110 ; 
       62 38 110 ; 
       63 38 110 ; 
       64 38 110 ; 
       65 38 110 ; 
       0 40 110 ; 
       1 39 110 ; 
       2 39 110 ; 
       3 39 110 ; 
       4 39 110 ; 
       5 3 110 ; 
       6 1 110 ; 
       7 35 110 ; 
       12 34 110 ; 
       13 36 110 ; 
       14 34 110 ; 
       15 34 110 ; 
       16 34 110 ; 
       17 44 110 ; 
       18 44 110 ; 
       19 44 110 ; 
       20 36 110 ; 
       21 36 110 ; 
       22 36 110 ; 
       23 44 110 ; 
       34 35 110 ; 
       35 40 110 ; 
       36 0 110 ; 
       39 0 110 ; 
       44 34 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 6079.794 3.575599 0 USR MPRFLG 0 ; 
       1 SCHEM 6079.794 1.575611 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       40 SCHEM 6112.646 4.330431 0 USR MPRFLG 0 ; 
       42 SCHEM 6141.4 -11.37262 0 USR MPRFLG 0 ; 
       66 SCHEM 6136.743 -14.26017 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       68 SCHEM 6139.243 -14.26017 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       69 SCHEM 6141.743 -14.26017 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       70 SCHEM 6144.243 -14.26017 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       71 SCHEM 6146.607 -14.27238 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       72 SCHEM 6149.107 -14.27238 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       73 SCHEM 6151.607 -14.27238 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       74 SCHEM 6154.107 -14.27238 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       75 SCHEM 6149.358 -16.06226 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       76 SCHEM 6154.358 -16.06226 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 6096.064 -15.87027 0 USR MPRFLG 0 ; 
       10 SCHEM 6114.325 -17.86989 0 USR MPRFLG 0 ; 
       24 SCHEM 6097.314 -17.8703 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       25 SCHEM 6104.814 -17.8703 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       26 SCHEM 6099.814 -17.8703 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       27 SCHEM 6109.814 -17.8703 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       28 SCHEM 6107.314 -17.8703 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       29 SCHEM 6084.814 -19.8703 0 USR MPRFLG 0 ; 
       30 SCHEM 6092.314 -19.8703 0 USR MPRFLG 0 ; 
       31 SCHEM 6115.575 -21.86989 0 USR MPRFLG 0 ; 
       32 SCHEM 6108.075 -21.86989 0 USR MPRFLG 0 ; 
       33 SCHEM 6123.075 -19.86991 0 USR WIRECOL 9 7 MPRFLG 0 ; 
       45 SCHEM 6115.42 9.364552 0 USR MPRFLG 0 ; 
       46 SCHEM 6115.42 7.36455 0 MPRFLG 0 ; 
       47 SCHEM 6087.314 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       48 SCHEM 6110.575 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       49 SCHEM 6082.314 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       50 SCHEM 6084.814 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       51 SCHEM 6108.075 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       52 SCHEM 6094.814 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       53 SCHEM 6118.075 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       54 SCHEM 6089.814 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       55 SCHEM 6113.075 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       56 SCHEM 6092.314 -21.8703 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       57 SCHEM 6115.575 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       67 SCHEM 6105.575 -23.86991 0 USR WIRECOL 3 7 MPRFLG 0 ; 
       77 SCHEM 6088.564 -17.8703 0 USR MPRFLG 0 ; 
       78 SCHEM 6111.825 -19.86991 0 USR MPRFLG 0 ; 
       43 SCHEM 6116.164 16.55794 0 USR SRT 1 1 1 0 0 0 0 15.24941 0 MPRFLG 0 ; 
       9 SCHEM 6122.082 -14.79815 0 USR MPRFLG 0 ; 
       11 SCHEM 6119.388 -14.79815 0 USR MPRFLG 0 ; 
       37 SCHEM 6144.99 -6.175618 0 USR MPRFLG 0 ; 
       38 SCHEM 6143.525 -0.979744 0 USR MPRFLG 0 ; 
       41 SCHEM 6107.809 -10.66655 0 USR MPRFLG 0 ; 
       58 SCHEM 6141.24 -8.175616 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       59 SCHEM 6143.74 -8.175616 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       60 SCHEM 6146.24 -8.175616 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       61 SCHEM 6148.74 -8.175616 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       62 SCHEM 6147.275 -2.979744 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       63 SCHEM 6144.775 -2.979744 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       64 SCHEM 6142.275 -2.979744 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       65 SCHEM 6139.775 -2.979744 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       0 SCHEM 6102.472 -1.33543 0 USR MPRFLG 0 ; 
       1 SCHEM 6088.722 -5.335431 0 USR MPRFLG 0 ; 
       2 SCHEM 6091.222 -5.335431 0 USR MPRFLG 0 ; 
       3 SCHEM 6093.722 -5.335431 0 USR MPRFLG 0 ; 
       4 SCHEM 6096.222 -5.335431 0 USR MPRFLG 0 ; 
       5 SCHEM 6093.722 -7.335431 0 USR MPRFLG 0 ; 
       6 SCHEM 6088.722 -7.335431 0 USR MPRFLG 0 ; 
       7 SCHEM 6130.112 -3.337734 0 USR MPRFLG 0 ; 
       12 SCHEM 6110.112 -5.337735 0 USR MPRFLG 0 ; 
       13 SCHEM 6098.722 -5.335431 0 USR MPRFLG 0 ; 
       14 SCHEM 6112.612 -5.337735 0 USR MPRFLG 0 ; 
       15 SCHEM 6125.112 -5.337735 0 USR MPRFLG 0 ; 
       16 SCHEM 6127.612 -5.337735 0 USR MPRFLG 0 ; 
       17 SCHEM 6117.612 -7.33775 0 USR MPRFLG 0 ; 
       18 SCHEM 6120.112 -7.33775 0 USR MPRFLG 0 ; 
       19 SCHEM 6122.612 -7.33775 0 USR MPRFLG 0 ; 
       20 SCHEM 6101.222 -5.335431 0 USR MPRFLG 0 ; 
       21 SCHEM 6103.722 -5.335431 0 USR MPRFLG 0 ; 
       22 SCHEM 6106.222 -5.335431 0 USR MPRFLG 0 ; 
       23 SCHEM 6115.112 -7.33775 0 USR MPRFLG 0 ; 
       34 SCHEM 6118.862 -3.337734 0 USR MPRFLG 0 ; 
       35 SCHEM 6120.112 -1.337734 0 USR MPRFLG 0 ; 
       36 SCHEM 6102.472 -3.335445 0 USR MPRFLG 0 ; 
       39 SCHEM 6092.472 -3.335445 0 USR MPRFLG 0 ; 
       44 SCHEM 6118.862 -5.337735 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
