SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.32-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       bounding_model-bound01.1-0 ; 
       bounding_model-bound02.1-0 ; 
       bounding_model-bounding_model.1-0 ROOT ; 
       ss107-cube14.1-0 ; 
       ss107-cube15.1-0 ; 
       ss107-cube21.1-0 ; 
       ss107-cube27.1-0 ; 
       ss107-cube28.1-0 ; 
       ss107-cube29.1-0 ; 
       ss107-cube30.1-0 ; 
       ss107-cyl13.1-0 ; 
       ss107-cyl14.1-0 ; 
       ss107-cyl15.1-0 ; 
       ss107-cyl7.1-0 ; 
       ss107-SS_01.1-0 ; 
       ss107-SS_02.1-0 ; 
       ss107-SS_03.1-0 ; 
       ss107-SS_04.1-0 ; 
       ss107-SS_29.1-0 ; 
       ss107-SS_30.1-0 ; 
       ss107-SS_31.1-0 ; 
       ss107-SS_32.1-0 ; 
       ss107-SS_33.1-0 ; 
       ss107-SS_34.1-0 ; 
       ss107-SS_35.1-0 ; 
       ss107-SS_36.1-0 ; 
       ss107-ss107.1-0 ROOT ; 
       ss107-tetra10.1-0 ; 
       ss107-tetra12.1-0 ; 
       ss107-tetra13.1-0 ; 
       ss107-tetra14.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss107/PICTURES/ss107 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       BOTH-ss107-mining.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 26 110 ; 
       4 26 110 ; 
       0 2 110 ; 
       1 2 110 ; 
       5 26 110 ; 
       6 26 110 ; 
       7 26 110 ; 
       8 26 110 ; 
       9 26 110 ; 
       10 26 110 ; 
       11 26 110 ; 
       12 26 110 ; 
       13 26 110 ; 
       14 26 110 ; 
       15 26 110 ; 
       16 26 110 ; 
       17 26 110 ; 
       18 26 110 ; 
       19 26 110 ; 
       20 26 110 ; 
       21 26 110 ; 
       22 26 110 ; 
       23 26 110 ; 
       24 26 110 ; 
       25 26 110 ; 
       27 26 110 ; 
       28 26 110 ; 
       29 26 110 ; 
       30 26 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2615.194 9.446912 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2615.194 7.446913 0 USR DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 2633.071 0.3151581 0 MPRFLG 0 ; 
       4 SCHEM 2665.571 0.3151581 0 MPRFLG 0 ; 
       0 SCHEM 2640.446 5.151909 0 MPRFLG 0 ; 
       1 SCHEM 2642.946 5.151909 0 MPRFLG 0 ; 
       2 SCHEM 2641.696 7.151916 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 2663.071 0.3151581 0 MPRFLG 0 ; 
       6 SCHEM 2640.571 0.3151581 0 MPRFLG 0 ; 
       7 SCHEM 2645.571 0.3151581 0 MPRFLG 0 ; 
       8 SCHEM 2653.071 0.3151581 0 MPRFLG 0 ; 
       9 SCHEM 2658.071 0.3151581 0 MPRFLG 0 ; 
       26 SCHEM 2633.071 2.315158 0 USR SRT 1 1 1 -1.570796 3.141593 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 2648.071 0.3151581 0 MPRFLG 0 ; 
       11 SCHEM 2650.571 0.3151581 0 MPRFLG 0 ; 
       12 SCHEM 2655.571 0.3151581 0 MPRFLG 0 ; 
       13 SCHEM 2660.571 0.3151581 0 MPRFLG 0 ; 
       14 SCHEM 2600.571 0.3151581 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 2603.071 0.3151581 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2605.571 0.3151581 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2608.071 0.3151581 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2620.571 0.3151581 0 WIRECOL 2 7 MPRFLG 0 ; 
       19 SCHEM 2623.071 0.3151581 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 2625.571 0.3151581 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 2628.071 0.3151581 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 2618.071 0.3151581 0 WIRECOL 4 7 MPRFLG 0 ; 
       23 SCHEM 2615.571 0.3151581 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 2613.071 0.3151581 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2610.571 0.3151581 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 2630.571 0.3151581 0 MPRFLG 0 ; 
       28 SCHEM 2635.571 0.3151581 0 MPRFLG 0 ; 
       29 SCHEM 2638.071 0.3151581 0 MPRFLG 0 ; 
       30 SCHEM 2643.071 0.3151581 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
