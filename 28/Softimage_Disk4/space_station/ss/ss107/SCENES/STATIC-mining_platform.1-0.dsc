SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.30-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       mining_platform-cube14.1-0 ; 
       mining_platform-cube15.1-0 ; 
       mining_platform-cube21.1-0 ; 
       mining_platform-cube27.1-0 ; 
       mining_platform-cube28.1-0 ; 
       mining_platform-cube29.1-0 ; 
       mining_platform-cube30.1-0 ; 
       mining_platform-cube7.22-0 ROOT ; 
       mining_platform-cyl13.1-0 ; 
       mining_platform-cyl14.1-0 ; 
       mining_platform-cyl15.1-0 ; 
       mining_platform-cyl7.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss107/PICTURES/ss107 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       STATIC-mining_platform.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       10 7 110 ; 
       6 10 110 ; 
       3 7 110 ; 
       8 7 110 ; 
       4 8 110 ; 
       9 7 110 ; 
       5 9 110 ; 
       11 7 110 ; 
       2 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 -2 0 MPRFLG 0 ; 
       1 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       10 SCHEM 15 -2 0 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 MPRFLG 0 ; 
       3 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       7 SCHEM 10 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       8 SCHEM 10 -2 0 MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       9 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       2 SCHEM 17.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
