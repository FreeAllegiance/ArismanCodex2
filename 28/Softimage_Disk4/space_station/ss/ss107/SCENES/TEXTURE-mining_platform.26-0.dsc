SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.28-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       mining_platform-cube14.1-0 ; 
       mining_platform-cube15.1-0 ; 
       mining_platform-cube21.1-0 ; 
       mining_platform-cube27.1-0 ; 
       mining_platform-cube28.1-0 ; 
       mining_platform-cube29.1-0 ; 
       mining_platform-cube30.1-0 ; 
       mining_platform-cube7.21-0 ROOT ; 
       mining_platform-cyl13.1-0 ; 
       mining_platform-cyl14.1-0 ; 
       mining_platform-cyl15.1-0 ; 
       mining_platform-cyl7.1-0 ; 
       mining_platform-null18.1-0 ; 
       mining_platform-null19.1-0 ; 
       mining_platform-null32.1-0 ; 
       mining_platform-SS_01.1-0 ; 
       mining_platform-SS_02.1-0 ; 
       mining_platform-SS_03.1-0 ; 
       mining_platform-SS_04.1-0 ; 
       mining_platform-SS_29.1-0 ; 
       mining_platform-SS_30.1-0 ; 
       mining_platform-SS_31.1-0 ; 
       mining_platform-SS_32.1-0 ; 
       mining_platform-SS_33.1-0 ; 
       mining_platform-SS_34.1-0 ; 
       mining_platform-SS_35.1-0 ; 
       mining_platform-SS_36.1-0 ; 
       mining_platform-tetra10.1-0 ; 
       mining_platform-tetra12.1-0 ; 
       mining_platform-tetra13.1-0 ; 
       mining_platform-tetra14.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       //research/root/federation/shared_art_files/SoftImage/space_station/ss/ss107/PICTURES/ss107 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       TEXTURE-mining_platform.26-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 7 110 ; 
       1 7 110 ; 
       28 0 110 ; 
       10 7 110 ; 
       6 10 110 ; 
       3 7 110 ; 
       29 3 110 ; 
       30 3 110 ; 
       8 7 110 ; 
       12 7 110 ; 
       13 7 110 ; 
       14 7 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 13 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
       27 0 110 ; 
       4 8 110 ; 
       9 7 110 ; 
       5 9 110 ; 
       11 7 110 ; 
       2 11 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 38.75 -2 0 MPRFLG 0 ; 
       1 SCHEM 12.5 -2 0 MPRFLG 0 ; 
       28 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       10 SCHEM 72.5 -2 0 MPRFLG 0 ; 
       6 SCHEM 70 -4 0 MPRFLG 0 ; 
       3 SCHEM 48.75 -2 0 MPRFLG 0 ; 
       7 SCHEM 46.25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 45 -4 0 MPRFLG 0 ; 
       30 SCHEM 47.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       12 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       13 SCHEM 18.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 6.25 -2 0 MPRFLG 0 ; 
       15 SCHEM 2.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 7.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 10 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 25 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 27.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 30 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 32.5 -4 0 WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 22.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 20 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 17.5 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 15 -4 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 35 -4 0 MPRFLG 0 ; 
       4 SCHEM 55 -4 0 MPRFLG 0 ; 
       9 SCHEM 65 -2 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 80 -2 0 USR MPRFLG 0 ; 
       2 SCHEM 77.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
