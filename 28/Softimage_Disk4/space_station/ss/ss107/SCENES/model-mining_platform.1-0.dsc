SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.2-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 31     
       mining_platform-cube14.1-0 ; 
       mining_platform-cube15.1-0 ; 
       mining_platform-cube16.1-0 ; 
       mining_platform-cube17.1-0 ; 
       mining_platform-cube18.1-0 ; 
       mining_platform-cube19.1-0 ; 
       mining_platform-cube7.2-0 ROOT ; 
       mining_platform-cube8.1-0 ; 
       mining_platform-cyl2.1-0 ; 
       mining_platform-cyl3.1-0 ; 
       mining_platform-cyl4.1-0 ; 
       mining_platform-cyl5.1-0 ; 
       mining_platform-null18.1-0 ; 
       mining_platform-null19.1-0 ; 
       mining_platform-null32.1-0 ; 
       mining_platform-SS_01.1-0 ; 
       mining_platform-SS_02.1-0 ; 
       mining_platform-SS_03.1-0 ; 
       mining_platform-SS_04.1-0 ; 
       mining_platform-SS_29.1-0 ; 
       mining_platform-SS_30.1-0 ; 
       mining_platform-SS_31.1-0 ; 
       mining_platform-SS_32.1-0 ; 
       mining_platform-SS_33.1-0 ; 
       mining_platform-SS_34.1-0 ; 
       mining_platform-SS_35.1-0 ; 
       mining_platform-SS_36.1-0 ; 
       mining_platform-tetra10.1-0 ; 
       mining_platform-tetra11.1-0 ; 
       mining_platform-tetra12.1-0 ; 
       mining_platform-tetra13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       model-mining_platform.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       9 6 110 ; 
       3 9 110 ; 
       8 6 110 ; 
       7 8 110 ; 
       10 6 110 ; 
       4 10 110 ; 
       11 6 110 ; 
       27 0 110 ; 
       1 6 110 ; 
       2 6 110 ; 
       5 11 110 ; 
       29 2 110 ; 
       28 0 110 ; 
       0 6 110 ; 
       30 2 110 ; 
       12 6 110 ; 
       13 6 110 ; 
       14 6 110 ; 
       15 14 110 ; 
       16 14 110 ; 
       17 14 110 ; 
       18 14 110 ; 
       19 12 110 ; 
       20 12 110 ; 
       21 12 110 ; 
       22 12 110 ; 
       23 13 110 ; 
       24 13 110 ; 
       25 13 110 ; 
       26 13 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2431.474 70.41506 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2431.474 68.41506 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       9 SCHEM 2426.051 53.64879 0 USR MPRFLG 0 ; 
       3 SCHEM 2426.051 51.64879 0 MPRFLG 0 ; 
       8 SCHEM 2423.598 53.64371 0 MPRFLG 0 ; 
       7 SCHEM 2423.598 51.64371 0 MPRFLG 0 ; 
       10 SCHEM 2428.551 53.64879 0 USR MPRFLG 0 ; 
       4 SCHEM 2428.551 51.64879 0 MPRFLG 0 ; 
       11 SCHEM 2431.051 53.64879 0 USR MPRFLG 0 ; 
       27 SCHEM 2433.598 51.64371 0 MPRFLG 0 ; 
       1 SCHEM 2437.242 53.64371 0 USR MPRFLG 0 ; 
       2 SCHEM 2439.848 53.64371 0 MPRFLG 0 ; 
       5 SCHEM 2431.051 51.64879 0 MPRFLG 0 ; 
       29 SCHEM 2438.598 51.64371 0 MPRFLG 0 ; 
       28 SCHEM 2436.098 51.64371 0 MPRFLG 0 ; 
       0 SCHEM 2434.848 53.64371 0 MPRFLG 0 ; 
       30 SCHEM 2441.098 51.64371 0 MPRFLG 0 ; 
       12 SCHEM 2447.009 48.89911 0 USR MPRFLG 0 ; 
       13 SCHEM 2447.009 52.15878 0 USR MPRFLG 0 ; 
       14 SCHEM 2446.971 55.07062 0 USR MPRFLG 0 ; 
       15 SCHEM 2443.221 53.07063 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 2445.721 53.07063 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 2448.221 53.07063 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 2450.721 53.07063 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 2443.259 46.89911 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       20 SCHEM 2445.759 46.89911 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       21 SCHEM 2448.259 46.89911 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       22 SCHEM 2450.759 46.89911 0 USR WIRECOL 2 7 MPRFLG 0 ; 
       23 SCHEM 2450.759 50.15878 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       24 SCHEM 2448.259 50.15878 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 2445.759 50.15878 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 2443.259 50.15878 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       6 SCHEM 2440.697 60.19904 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
