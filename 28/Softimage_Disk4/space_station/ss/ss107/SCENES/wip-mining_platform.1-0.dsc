SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       star_base-cam_int1.1-0 ROOT ; 
       star_base-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 47     
       elect-bound01.3-0 ; 
       elect-root.1-0 ROOT ; 
       mining_platform-cube10.1-0 ; 
       mining_platform-cube11.1-0 ; 
       mining_platform-cube12.1-0 ; 
       mining_platform-cube13.1-0 ROOT ; 
       mining_platform-cube14.1-0 ROOT ; 
       mining_platform-cube15.1-0 ROOT ; 
       mining_platform-cube7.1-0 ROOT ; 
       mining_platform-cube8.1-0 ; 
       mining_platform-cube9.1-0 ROOT ; 
       mining_platform-cyl1.1-0 ROOT ; 
       mining_platform-cyl2.1-0 ; 
       mining_platform-cyl3.1-0 ; 
       mining_platform-cyl4.1-0 ; 
       mining_platform-cyl5.1-0 ; 
       mining_platform-east_bay_11_6.1-0 ROOT ; 
       mining_platform-extru32.1-0 ; 
       mining_platform-extru32_1.1-0 ; 
       mining_platform-extru61.1-0 ; 
       mining_platform-extru62.1-0 ; 
       mining_platform-null18.1-0 ROOT ; 
       mining_platform-null19.1-0 ROOT ; 
       mining_platform-null19_1.1-0 ROOT ; 
       mining_platform-null30.1-0 ROOT ; 
       mining_platform-null32.1-0 ROOT ; 
       mining_platform-SS_01.1-0 ; 
       mining_platform-SS_02.1-0 ; 
       mining_platform-SS_03.1-0 ; 
       mining_platform-SS_04.1-0 ; 
       mining_platform-SS_05.1-0 ; 
       mining_platform-SS_20.1-0 ; 
       mining_platform-SS_21.1-0 ; 
       mining_platform-SS_22.1-0 ; 
       mining_platform-SS_23_4.1-0 ; 
       mining_platform-SS_29.1-0 ; 
       mining_platform-SS_30.1-0 ; 
       mining_platform-SS_31.1-0 ; 
       mining_platform-SS_32.1-0 ; 
       mining_platform-SS_33.1-0 ; 
       mining_platform-SS_34.1-0 ; 
       mining_platform-SS_35.1-0 ; 
       mining_platform-SS_36.1-0 ; 
       mining_platform-tetra10.1-0 ROOT ; 
       mining_platform-tetra11.1-0 ROOT ; 
       mining_platform-tetra5.1-0 ; 
       mining_platform-tetra9.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss107/PICTURES/ss27 ; 
       //research/root/federation/Shared_Art_Files/SoftImage/space_station/ss/ss107/PICTURES/ss27inside ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       wip-mining_platform.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       12 8 110 ; 
       9 12 110 ; 
       45 8 110 ; 
       13 8 110 ; 
       2 13 110 ; 
       14 8 110 ; 
       3 14 110 ; 
       17 24 110 ; 
       15 8 110 ; 
       4 15 110 ; 
       18 24 110 ; 
       19 24 110 ; 
       20 24 110 ; 
       26 25 110 ; 
       27 25 110 ; 
       28 25 110 ; 
       29 25 110 ; 
       30 25 110 ; 
       31 23 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       34 23 110 ; 
       35 21 110 ; 
       36 21 110 ; 
       37 21 110 ; 
       38 21 110 ; 
       39 22 110 ; 
       40 22 110 ; 
       41 22 110 ; 
       42 22 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 2431.474 70.41506 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 2431.474 68.41506 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2417.438 51.68975 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 2478.555 70.41506 0 DISPLAY 0 0 SRT 4.399951 2.428773 4.399951 0 -0.7853982 0 -24.48872 9.553715 36.27618 MPRFLG 0 ; 
       12 SCHEM 2507.729 69.635 0 MPRFLG 0 ; 
       9 SCHEM 2507.729 67.635 0 MPRFLG 0 ; 
       1 SCHEM 2418.073 53.55501 0 USR DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 -27.40373 MPRFLG 0 ; 
       10 SCHEM 2485.192 71.635 0 DISPLAY 0 0 SRT 1.348 0.4017041 7.144401 0 0 0 0 -29.95187 0 MPRFLG 0 ; 
       45 SCHEM 2510.229 69.635 0 MPRFLG 0 ; 
       43 SCHEM 2530.229 71.635 0 SRT 0.73 0.9636003 0.73 -3.141592 0.8907998 7.449195e-007 11.52865 -1.294626 -35.03054 MPRFLG 0 ; 
       7 SCHEM 2537.729 71.635 0 DISPLAY 1 2 SRT 2.518 1.112956 2.518 0 0 0 0 -11.95541 0 MPRFLG 0 ; 
       13 SCHEM 2520.229 69.635 0 MPRFLG 0 ; 
       2 SCHEM 2520.229 67.635 0 MPRFLG 0 ; 
       14 SCHEM 2522.729 69.635 0 MPRFLG 0 ; 
       16 SCHEM 2450.634 56.35884 0 USR DISPLAY 0 0 SRT 1 1 1 3.59772e-007 -1.329447e-007 -3.000182e-007 -0.0002534293 -45.19073 0.03713927 MPRFLG 0 ; 
       3 SCHEM 2522.729 67.635 0 MPRFLG 0 ; 
       17 SCHEM 2488.008 64.45891 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 2525.229 69.635 0 MPRFLG 0 ; 
       4 SCHEM 2525.229 67.635 0 MPRFLG 0 ; 
       5 SCHEM 2505.229 71.635 0 SRT 1.564579 0.44232 0.9403726 0 0 0 0 -3.791639 26.69875 MPRFLG 0 ; 
       18 SCHEM 2495.508 64.45891 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       46 SCHEM 2527.729 71.635 0 SRT 0.73 0.9636003 0.73 6.278329e-007 -0.8907998 3.141592 -11.52865 -1.294626 35.03054 MPRFLG 0 ; 
       19 SCHEM 2490.508 64.45891 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       44 SCHEM 2532.729 71.635 0 SRT 0.73 0.9636003 0.73 -6.278329e-007 0.8907998 3.141592 -11.52865 -1.294626 -35.03054 MPRFLG 0 ; 
       6 SCHEM 2535.229 71.635 0 SRT 1.564579 0.44232 0.9403726 0 0 0 0 -3.791639 -26.69875 MPRFLG 0 ; 
       20 SCHEM 2493.008 64.45891 0 USR DISPLAY 0 0 MPRFLG 0 ; 
       21 SCHEM 2449.427 29.16148 0 USR DISPLAY 0 0 SRT 1 1 1 -3.141592 3.141593 3.141592 1.772811e-005 26.46317 1.945753e-005 MPRFLG 0 ; 
       22 SCHEM 2449.821 25.35991 0 USR DISPLAY 0 0 SRT 1 1 1 -3.141592 2.330195 3.141593 -6.53983e-006 -48.01463 -7.071587e-006 MPRFLG 0 ; 
       23 SCHEM 2449.826 22.65147 0 USR DISPLAY 0 0 SRT 1 1 1 -3.141592 2.326195 3.141593 4.920452e-009 -27.92893 1.116184e-007 MPRFLG 0 ; 
       24 SCHEM 2491.758 66.45891 0 USR DISPLAY 0 0 SRT 0.7179999 0.7179999 0.7179999 3.59772e-007 -1.604785e-007 -3.000182e-007 7.336559e-007 -12.25361 -6.091418e-008 MPRFLG 0 ; 
       25 SCHEM 2449.712 32.88231 0 USR DISPLAY 0 0 SRT 1 1 1 -3.141592 3.138975 3.141592 9.600471e-006 1.5196 1.053702e-005 MPRFLG 0 ; 
       26 SCHEM 2445.962 30.88231 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       27 SCHEM 2448.462 30.88231 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       28 SCHEM 2450.962 30.88231 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       29 SCHEM 2453.462 30.88231 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       30 SCHEM 2455.77 30.85662 0 USR WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       31 SCHEM 2446.076 20.65145 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       32 SCHEM 2448.576 20.65145 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       33 SCHEM 2451.076 20.65145 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       34 SCHEM 2453.576 20.65145 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       35 SCHEM 2445.677 27.16148 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       36 SCHEM 2448.177 27.16148 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 2450.677 27.16148 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       38 SCHEM 2453.177 27.16148 0 USR WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       39 SCHEM 2453.571 23.35991 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 2451.071 23.35991 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 2448.571 23.35991 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       42 SCHEM 2446.071 23.35991 0 USR WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 2516.479 71.635 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
