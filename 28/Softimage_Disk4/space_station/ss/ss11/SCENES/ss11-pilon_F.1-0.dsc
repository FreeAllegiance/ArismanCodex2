SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss11-ss11.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light1_5.1-0 ROOT ; 
       pilon10-light2_5.1-0 ROOT ; 
       pilon10-light3_5.1-0 ROOT ; 
       pilon10-light4_5.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       pilon_F-mat20.1-0 ; 
       pilon_F-mat21.1-0 ; 
       pilon_F-mat22.1-0 ; 
       pilon_F-mat23.1-0 ; 
       pilon_F-mat24.1-0 ; 
       pilon_F-mat25.1-0 ; 
       pilon_F-mat26.1-0 ; 
       pilon_F-mat27.1-0 ; 
       pilon_F-mat28.1-0 ; 
       pilon_F-mat29.1-0 ; 
       pilon_F-mat30.1-0 ; 
       pilon_F-mat31.1-0 ; 
       pilon_F-mat32.1-0 ; 
       pilon_F-mat40.1-0 ; 
       pilon_F-mat41.1-0 ; 
       pilon_F-mat42.1-0 ; 
       pilon_F-mat43.1-0 ; 
       pilon_F-mat44.1-0 ; 
       pilon_F-mat45.1-0 ; 
       pilon_F-mat46.1-0 ; 
       pilon_F-mat47.1-0 ; 
       pilon_F-mat48.1-0 ; 
       pilon_F-mat49.1-0 ; 
       pilon_F-mat50.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       ss11-doccon.1-0 ; 
       ss11-fuselg.1-0 ; 
       ss11-SS1.1-0 ; 
       ss11-ss11.1-0 ROOT ; 
       ss11-SS2.1-0 ; 
       ss11-SS3.1-0 ; 
       ss11-SS4.1-0 ; 
       ss11-tfuselg.1-0 ; 
       ss11-tfuselg1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss11/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss11-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       pilon_F-t2d15.1-0 ; 
       pilon_F-t2d16.1-0 ; 
       pilon_F-t2d17.1-0 ; 
       pilon_F-t2d18.1-0 ; 
       pilon_F-t2d19.1-0 ; 
       pilon_F-t2d20.1-0 ; 
       pilon_F-t2d21.1-0 ; 
       pilon_F-t2d22.1-0 ; 
       pilon_F-t2d23.1-0 ; 
       pilon_F-t2d24.1-0 ; 
       pilon_F-t2d25.1-0 ; 
       pilon_F-t2d26.1-0 ; 
       pilon_F-t2d27.1-0 ; 
       pilon_F-t2d28.1-0 ; 
       pilon_F-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       4 1 110 ; 
       5 1 110 ; 
       6 1 110 ; 
       0 7 110 ; 
       1 3 110 ; 
       7 1 110 ; 
       8 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 13 300 ; 
       4 14 300 ; 
       5 15 300 ; 
       6 16 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       7 6 300 ; 
       7 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       7 12 300 ; 
       8 17 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       8 21 300 ; 
       8 22 300 ; 
       8 23 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       19 10 401 ; 
       20 11 401 ; 
       21 12 401 ; 
       22 13 401 ; 
       23 14 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 5 -4 0 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       5 SCHEM 10 -4 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 27.5 0 0 USR MPRFLG 0 ; 
       1 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       3 SCHEM 8.75 0 0 SRT 1 1 1 -1.570796 0 0 0 -5.826557 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 29 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 14 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 16.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 2 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
