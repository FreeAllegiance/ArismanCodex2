SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss12-ss12.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.2-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light1_12.2-0 ROOT ; 
       pilon10-light2_12.2-0 ROOT ; 
       pilon10-light3_12.2-0 ROOT ; 
       pilon10-light4_12.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 24     
       pilon_F-mat40.2-0 ; 
       pilon_F-mat41.2-0 ; 
       pilon_F-mat42.2-0 ; 
       pilon_F-mat43.2-0 ; 
       pilon_F-mat44.2-0 ; 
       pilon_F-mat45.2-0 ; 
       pilon_F-mat46.2-0 ; 
       pilon_F-mat47.2-0 ; 
       pilon_F-obj5.2-0 ; 
       pilon_F-obj6.2-0 ; 
       pilon_F-obj7.2-0 ; 
       pilon11-mat20.2-0 ; 
       pilon11-mat21.2-0 ; 
       pilon11-mat22.2-0 ; 
       pilon11-mat23.2-0 ; 
       pilon11-mat25.2-0 ; 
       pilon11-mat33.2-0 ; 
       pilon11-mat34.2-0 ; 
       pilon11-mat38.2-0 ; 
       pilon11-mat39.2-0 ; 
       pilon11-obj1.2-0 ; 
       pilon11-obj2.2-0 ; 
       pilon11-obj3.2-0 ; 
       pilon11-obj4.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 9     
       pilon_F-afuselg3.2-0 ROOT ; 
       ss12-afuselg.1-0 ; 
       ss12-doccon.1-0 ; 
       ss12-fuselg.1-0 ; 
       ss12-SS1.1-0 ; 
       ss12-ss12.2-0 ROOT ; 
       ss12-SS2.1-0 ; 
       ss12-SS3.1-0 ; 
       ss12-SS4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss12/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss12-pilon_F.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 15     
       pilon_F-t2d30.2-0 ; 
       pilon_F-t2d31.2-0 ; 
       pilon_F-t2d32.2-0 ; 
       pilon_F-t2d33.2-0 ; 
       pilon_F-t2d34.2-0 ; 
       pilon11-t2d15.2-0 ; 
       pilon11-t2d16.2-0 ; 
       pilon11-t2d17.2-0 ; 
       pilon11-t2d18.2-0 ; 
       pilon11-t2d19.2-0 ; 
       pilon11-t2d25.2-0 ; 
       pilon11-t2d26.2-0 ; 
       pilon11-t2d27.2-0 ; 
       pilon11-t2d28.2-0 ; 
       pilon11-t2d29.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       4 3 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 3 110 ; 
       1 3 110 ; 
       2 1 110 ; 
       3 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       0 5 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 6 300 ; 
       0 7 300 ; 
       4 0 300 ; 
       6 1 300 ; 
       7 2 300 ; 
       8 3 300 ; 
       1 16 300 ; 
       1 17 300 ; 
       1 22 300 ; 
       1 21 300 ; 
       1 20 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       3 11 300 ; 
       3 12 300 ; 
       3 13 300 ; 
       3 14 300 ; 
       3 23 300 ; 
       3 15 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       5 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       6 3 401 ; 
       7 4 401 ; 
       8 0 401 ; 
       9 1 401 ; 
       10 2 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 9 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       20 12 401 ; 
       21 11 401 ; 
       22 10 401 ; 
       23 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 27.5 0 0 SRT 1 0.9999999 0.9999999 -0.7853979 0 0 -0.002563477 0.002856459 5.368074 MPRFLG 0 ; 
       4 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 10 -4 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 15 -4 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       5 SCHEM 8.75 0 0 SRT 1 1 1 2.356194 0 0 0 0.3664084 5.716208 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 29 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 29 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 16.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 30 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
