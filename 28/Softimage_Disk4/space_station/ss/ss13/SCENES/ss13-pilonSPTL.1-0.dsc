SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss13-ss13.2-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.2-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light3_11.2-0 ROOT ; 
       pilon10-light3_2.2-0 ROOT ; 
       pilon10-light4_11.2-0 ROOT ; 
       pilon10-light4_2.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       pilon-mat40.2-0 ; 
       pilon-mat41.2-0 ; 
       pilon-mat42.2-0 ; 
       pilon-mat43.2-0 ; 
       pilon-mat44.2-0 ; 
       pilon-mat45.2-0 ; 
       pilon11-mat20.2-0 ; 
       pilon11-mat21.2-0 ; 
       pilon11-mat22.2-0 ; 
       pilon11-mat23.2-0 ; 
       pilon11-mat24.2-0 ; 
       pilon11-mat25.2-0 ; 
       pilon11-mat26.2-0 ; 
       pilon11-mat27.2-0 ; 
       pilon11-mat28.2-0 ; 
       pilon11-mat29.2-0 ; 
       pilon11-mat30.2-0 ; 
       pilon11-mat31.2-0 ; 
       pilon11-mat32.2-0 ; 
       pilon11-mat33.1-0 ; 
       pilon11-mat34.1-0 ; 
       pilon11-mat35.1-0 ; 
       pilon11-mat36.1-0 ; 
       pilon11-mat37.1-0 ; 
       pilon11-mat38.1-0 ; 
       pilon11-mat39.1-0 ; 
       pilonSPTL-mat46.1-0 ; 
       pilonSPTL-mat47.1-0 ; 
       pilonSPTL-mat48.1-0 ; 
       pilonSPTL-mat49.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       ss13-bfuselg.1-0 ; 
       ss13-doccon.1-0 ; 
       ss13-fuselg.1-0 ; 
       ss13-SS1.1-0 ; 
       ss13-ss13.2-0 ROOT ; 
       ss13-SS2.1-0 ; 
       ss13-SS3.1-0 ; 
       ss13-SS4.1-0 ; 
       ss13-tfuselg1.1-0 ; 
       ss13-tfuselg2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss13/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss13-pilonSPTL.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       pilon-t2d30.2-0 ; 
       pilon-t2d31.2-0 ; 
       pilon-t2d32.2-0 ; 
       pilon-t2d33.2-0 ; 
       pilon-t2d34.2-0 ; 
       pilon11-t2d15.2-0 ; 
       pilon11-t2d16.2-0 ; 
       pilon11-t2d17.2-0 ; 
       pilon11-t2d18.2-0 ; 
       pilon11-t2d19.2-0 ; 
       pilon11-t2d20.2-0 ; 
       pilon11-t2d21.2-0 ; 
       pilon11-t2d22.2-0 ; 
       pilon11-t2d23.2-0 ; 
       pilon11-t2d24.2-0 ; 
       pilon11-t2d25.1-0 ; 
       pilon11-t2d26.1-0 ; 
       pilon11-t2d27.1-0 ; 
       pilon11-t2d28.1-0 ; 
       pilon11-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       3 2 110 ; 
       5 2 110 ; 
       6 8 110 ; 
       7 8 110 ; 
       0 2 110 ; 
       1 9 110 ; 
       2 4 110 ; 
       8 2 110 ; 
       9 8 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 26 300 ; 
       5 27 300 ; 
       6 28 300 ; 
       7 29 300 ; 
       0 19 300 ; 
       0 20 300 ; 
       0 21 300 ; 
       0 22 300 ; 
       0 23 300 ; 
       0 24 300 ; 
       0 25 300 ; 
       2 6 300 ; 
       2 7 300 ; 
       2 8 300 ; 
       2 9 300 ; 
       2 10 300 ; 
       2 11 300 ; 
       8 0 300 ; 
       8 1 300 ; 
       8 2 300 ; 
       8 3 300 ; 
       8 4 300 ; 
       8 5 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
       9 17 300 ; 
       9 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       4 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       7 5 401 ; 
       8 6 401 ; 
       9 7 401 ; 
       10 8 401 ; 
       11 9 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       21 15 401 ; 
       22 16 401 ; 
       23 17 401 ; 
       24 18 401 ; 
       25 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 15.90516 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 18.40516 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 20.90516 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 23.40516 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       3 SCHEM 12.72002 -4 0 MPRFLG 0 ; 
       5 SCHEM 10.22002 -4 0 MPRFLG 0 ; 
       6 SCHEM 10.97014 -6.174942 0 USR MPRFLG 0 ; 
       7 SCHEM 13.40516 -6.174942 0 USR MPRFLG 0 ; 
       0 SCHEM 2.720021 -4 0 USR MPRFLG 0 ; 
       1 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 3.970021 -2 0 USR MPRFLG 0 ; 
       4 SCHEM 3.970021 0 0 SRT 1 1 1 0 0 0 0 -4.983999 0 MPRFLG 0 ; 
       8 SCHEM 5.220021 -4 0 USR MPRFLG 0 ; 
       9 SCHEM 2.5 -6 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 14.22002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 1.720021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 11.72002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9.97014 -8.174942 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 12.40516 -8.174942 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 14.22002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 14.22002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 14.22002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 14.22002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 14.22002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.720021 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 1.720021 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 1.720021 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 1.720021 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 1.720021 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.72002 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 11 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
