SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss13-ss13.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light3_11.1-0 ROOT ; 
       pilon10-light3_2.1-0 ROOT ; 
       pilon10-light4_11.1-0 ROOT ; 
       pilon10-light4_2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 30     
       pilon-mat40.1-0 ; 
       pilon-mat41.1-0 ; 
       pilon-mat42.1-0 ; 
       pilon-mat43.1-0 ; 
       pilon-mat44.1-0 ; 
       pilon-mat45.1-0 ; 
       pilon_F-mat46.1-0 ; 
       pilon_F-mat47.1-0 ; 
       pilon_F-mat48.1-0 ; 
       pilon_F-mat49.1-0 ; 
       pilon_F-mat50.1-0 ; 
       pilon_F-mat51.1-0 ; 
       pilon_F-mat52.1-0 ; 
       pilon_F-mat53.1-0 ; 
       pilon_F-mat54.1-0 ; 
       pilon_F-mat55.1-0 ; 
       pilon_F-mat56.1-0 ; 
       pilon11-mat20.1-0 ; 
       pilon11-mat21.1-0 ; 
       pilon11-mat22.1-0 ; 
       pilon11-mat23.1-0 ; 
       pilon11-mat24.1-0 ; 
       pilon11-mat25.1-0 ; 
       pilon11-mat26.1-0 ; 
       pilon11-mat27.1-0 ; 
       pilon11-mat28.1-0 ; 
       pilon11-mat29.1-0 ; 
       pilon11-mat30.1-0 ; 
       pilon11-mat31.1-0 ; 
       pilon11-mat32.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       ss13-doccon.1-0 ; 
       ss13-fuselg.1-0 ; 
       ss13-SS1.1-0 ; 
       ss13-ss13.1-0 ROOT ; 
       ss13-SS2.1-0 ; 
       ss13-SS3.1-0 ; 
       ss13-SS4.1-0 ; 
       ss13-tfuselg1.1-0 ; 
       ss13-tfuselg2.1-0 ; 
       ss13-tfuselg4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss13/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss13-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       pilon-t2d30.1-0 ; 
       pilon-t2d31.1-0 ; 
       pilon-t2d32.1-0 ; 
       pilon-t2d33.1-0 ; 
       pilon-t2d34.1-0 ; 
       pilon_F-t2d35.1-0 ; 
       pilon_F-t2d36.1-0 ; 
       pilon_F-t2d37.1-0 ; 
       pilon_F-t2d38.1-0 ; 
       pilon_F-t2d39.1-0 ; 
       pilon11-t2d15.1-0 ; 
       pilon11-t2d16.1-0 ; 
       pilon11-t2d17.1-0 ; 
       pilon11-t2d18.1-0 ; 
       pilon11-t2d19.1-0 ; 
       pilon11-t2d20.1-0 ; 
       pilon11-t2d21.1-0 ; 
       pilon11-t2d22.1-0 ; 
       pilon11-t2d23.1-0 ; 
       pilon11-t2d24.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       2 1 110 ; 
       4 1 110 ; 
       5 7 110 ; 
       6 7 110 ; 
       0 8 110 ; 
       1 3 110 ; 
       7 1 110 ; 
       8 7 110 ; 
       9 1 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 6 300 ; 
       4 7 300 ; 
       5 8 300 ; 
       6 9 300 ; 
       1 17 300 ; 
       1 18 300 ; 
       1 19 300 ; 
       1 20 300 ; 
       1 21 300 ; 
       1 22 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       7 2 300 ; 
       7 3 300 ; 
       7 4 300 ; 
       7 5 300 ; 
       8 23 300 ; 
       8 24 300 ; 
       8 25 300 ; 
       8 26 300 ; 
       8 27 300 ; 
       8 28 300 ; 
       8 29 300 ; 
       9 10 300 ; 
       9 11 300 ; 
       9 12 300 ; 
       9 13 300 ; 
       9 14 300 ; 
       9 15 300 ; 
       9 16 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       3 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       12 5 401 ; 
       13 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       18 10 401 ; 
       19 11 401 ; 
       20 12 401 ; 
       21 13 401 ; 
       22 14 401 ; 
       25 15 401 ; 
       26 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 17.72002 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 20.22002 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 22.72002 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 25.22002 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 12.72002 -4 0 MPRFLG 0 ; 
       4 SCHEM 10.22002 -4 0 MPRFLG 0 ; 
       5 SCHEM 10.97014 -6.174942 0 USR MPRFLG 0 ; 
       6 SCHEM 13.40516 -6.174942 0 USR MPRFLG 0 ; 
       0 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       1 SCHEM 3.970021 -2 0 USR MPRFLG 0 ; 
       3 SCHEM 3.970021 0 0 SRT 1 1 1 -1.570796 0 0 0 -5.407094 0 MPRFLG 0 ; 
       7 SCHEM 5.220021 -4 0 USR MPRFLG 0 ; 
       8 SCHEM 2.5 -6 0 USR MPRFLG 0 ; 
       9 SCHEM 15.22002 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 14.90516 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 11.72002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 9.220021 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 9.97014 -8.174942 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 12.40516 -8.174942 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 15.22002 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 16.72002 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 14.90516 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 15.22002 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 15.22002 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15.22002 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 15.22002 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 15.22002 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 16.72002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 16.72002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 16.72002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 16.72002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 16.72002 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 11.72002 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 11 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
