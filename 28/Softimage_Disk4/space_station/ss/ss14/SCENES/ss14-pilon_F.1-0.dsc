SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss14-ss14.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 3     
       pilon_F-light3_1.1-0 ROOT ; 
       pilon_F-light4.1-0 ROOT ; 
       pilon_F-light4_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 29     
       pilon_F-mat20.1-0 ; 
       pilon_F-mat21.1-0 ; 
       pilon_F-mat22.1-0 ; 
       pilon_F-mat23.1-0 ; 
       pilon_F-mat24.1-0 ; 
       pilon_F-mat25.1-0 ; 
       pilon_F-mat26.1-0 ; 
       pilon_F-mat27.1-0 ; 
       pilon_F-mat28.1-0 ; 
       pilon_F-mat29.1-0 ; 
       pilon_F-mat3_1.1-0 ; 
       pilon_F-mat3_2.1-0 ; 
       pilon_F-mat4_1.1-0 ; 
       pilon_F-mat4_2.1-0 ; 
       pilon_F-mat5_1.1-0 ; 
       pilon_F-mat5_2.1-0 ; 
       pilon_F-mat6_1.1-0 ; 
       pilon_F-mat6_2.1-0 ; 
       pilon_F-mat7_1.1-0 ; 
       pilon_F-mat7_2.1-0 ; 
       pilon_F-z.1-0 ; 
       pilon_F-z1.1-0 ; 
       pilon_F-z2.1-0 ; 
       pilon_F-z3.1-0 ; 
       pilon10-mat15.1-0 ; 
       pilon10-mat16.1-0 ; 
       pilon10-mat17.1-0 ; 
       pilon10-mat18.1-0 ; 
       pilon10-mat19.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 11     
       ss14-doccon1.1-0 ; 
       ss14-doccon2.1-0 ; 
       ss14-fuselg.1-0 ; 
       ss14-mfuselg.1-0 ; 
       ss14-sphere1.1-0 ; 
       ss14-sphere2.1-0 ; 
       ss14-sphere3.1-0 ; 
       ss14-sphere4.1-0 ; 
       ss14-ss14.1-0 ROOT ; 
       ss14-tfuselg.1-0 ; 
       ss14-tfuselg5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss14/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss14-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 20     
       pilon_F-t2d1_1.1-0 ; 
       pilon_F-t2d1_2.1-0 ; 
       pilon_F-t2d15.1-0 ; 
       pilon_F-t2d16.1-0 ; 
       pilon_F-t2d17.1-0 ; 
       pilon_F-t2d18.1-0 ; 
       pilon_F-t2d19.1-0 ; 
       pilon_F-t2d2_1.1-0 ; 
       pilon_F-t2d2_2.1-0 ; 
       pilon_F-t2d20.1-0 ; 
       pilon_F-t2d3_1.1-0 ; 
       pilon_F-t2d3_2.1-0 ; 
       pilon_F-t2d4_1.1-0 ; 
       pilon_F-t2d4_2.1-0 ; 
       pilon_F-t2d5_1.1-0 ; 
       pilon_F-t2d5_2.1-0 ; 
       pilon10-t2d11.1-0 ; 
       pilon10-t2d12.1-0 ; 
       pilon10-t2d13.1-0 ; 
       pilon10-t2d14.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 110 ; 
       1 3 110 ; 
       2 8 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 2 110 ; 
       6 2 110 ; 
       7 2 110 ; 
       9 2 110 ; 
       10 2 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       2 24 300 ; 
       2 25 300 ; 
       2 26 300 ; 
       2 27 300 ; 
       2 28 300 ; 
       3 0 300 ; 
       3 1 300 ; 
       3 2 300 ; 
       3 3 300 ; 
       3 4 300 ; 
       3 5 300 ; 
       4 6 300 ; 
       5 7 300 ; 
       6 8 300 ; 
       7 9 300 ; 
       9 20 300 ; 
       9 21 300 ; 
       9 10 300 ; 
       9 12 300 ; 
       9 14 300 ; 
       9 16 300 ; 
       9 18 300 ; 
       10 22 300 ; 
       10 23 300 ; 
       10 11 300 ; 
       10 13 300 ; 
       10 15 300 ; 
       10 17 300 ; 
       10 19 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 2 401 ; 
       1 3 401 ; 
       2 4 401 ; 
       3 5 401 ; 
       4 6 401 ; 
       5 9 401 ; 
       10 0 401 ; 
       11 1 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 10 401 ; 
       15 11 401 ; 
       16 12 401 ; 
       17 13 401 ; 
       18 14 401 ; 
       19 15 401 ; 
       25 16 401 ; 
       26 17 401 ; 
       27 18 401 ; 
       28 19 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       2 SCHEM 5 -2 0 USR MPRFLG 0 ; 
       3 SCHEM 7.5 -4 0 USR MPRFLG 0 ; 
       4 SCHEM 10 -4 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       6 SCHEM 15 -4 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 5 0 0 USR SRT 1 1 1 -1.570796 0 0 0 -5.786036 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -4 0 USR MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 9 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 9 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 20 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 14 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 22 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
