SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss15-ss15.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light3_6.1-0 ROOT ; 
       pilon10-light3_7.1-0 ROOT ; 
       pilon10-light4_6.1-0 ROOT ; 
       pilon10-light4_7.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 39     
       pilon_F-mat40.1-0 ; 
       pilon_F-mat41.1-0 ; 
       pilon_F-mat42.1-0 ; 
       pilon_F-mat43.1-0 ; 
       pilon_F-mat44.1-0 ; 
       pilon_F-mat45.1-0 ; 
       pilon_F-mat53.1-0 ; 
       pilon_F-mat54.1-0 ; 
       pilon_F-mat55.1-0 ; 
       pilon_F-mat56.1-0 ; 
       pilon_F-mat57.1-0 ; 
       pilon_F-mat58.1-0 ; 
       pilon_F-mat59.1-0 ; 
       pilon_F-mat60.1-0 ; 
       pilon_F-mat61.1-0 ; 
       pilon_F-mat62.1-0 ; 
       pilon_F-mat63.1-0 ; 
       pilon_F-mat64.1-0 ; 
       pilon_F-mat65.1-0 ; 
       pilon_F-mat66.1-0 ; 
       pilon_F-mat67.1-0 ; 
       pilon_F-mat68.1-0 ; 
       pilon_F-mat69.1-0 ; 
       pilon_F-mat70.1-0 ; 
       pilon_F-mat71.1-0 ; 
       pilon_F-mat72.1-0 ; 
       pilon_F-mat73.1-0 ; 
       pilon_F-mat74.1-0 ; 
       pilon_F-mat75.1-0 ; 
       pilon_F-mat76.1-0 ; 
       pilon_F-mat77.1-0 ; 
       pilon_F-mat78.1-0 ; 
       pilon_F-mat79.1-0 ; 
       pilon11-mat20.1-0 ; 
       pilon11-mat21.1-0 ; 
       pilon11-mat22.1-0 ; 
       pilon11-mat23.1-0 ; 
       pilon11-mat24.1-0 ; 
       pilon11-mat25.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 14     
       ss15-bfuselg.1-0 ; 
       ss15-bfuselg1.1-0 ; 
       ss15-doccon1.1-0 ; 
       ss15-doccon2.1-0 ; 
       ss15-fuselg.1-0 ; 
       ss15-lfuselg.1-0 ; 
       ss15-rfuselg.1-0 ; 
       ss15-SS1.1-0 ; 
       ss15-ss15.1-0 ROOT ; 
       ss15-SS2.1-0 ; 
       ss15-SS3.1-0 ; 
       ss15-SS4.1-0 ; 
       ss15-SS5.1-0 ; 
       ss15-SS6.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss15/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss15-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 25     
       pilon_F-t2d30.1-0 ; 
       pilon_F-t2d31.1-0 ; 
       pilon_F-t2d32.1-0 ; 
       pilon_F-t2d33.1-0 ; 
       pilon_F-t2d34.1-0 ; 
       pilon_F-t2d40.1-0 ; 
       pilon_F-t2d41.1-0 ; 
       pilon_F-t2d42.1-0 ; 
       pilon_F-t2d43.1-0 ; 
       pilon_F-t2d44.1-0 ; 
       pilon_F-t2d45.1-0 ; 
       pilon_F-t2d46.1-0 ; 
       pilon_F-t2d47.1-0 ; 
       pilon_F-t2d48.1-0 ; 
       pilon_F-t2d49.1-0 ; 
       pilon_F-t2d50.1-0 ; 
       pilon_F-t2d51.1-0 ; 
       pilon_F-t2d52.1-0 ; 
       pilon_F-t2d53.1-0 ; 
       pilon_F-t2d54.1-0 ; 
       pilon11-t2d15.1-0 ; 
       pilon11-t2d16.1-0 ; 
       pilon11-t2d17.1-0 ; 
       pilon11-t2d18.1-0 ; 
       pilon11-t2d19.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 4 110 ; 
       9 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 1 110 ; 
       13 1 110 ; 
       0 1 110 ; 
       1 4 110 ; 
       2 5 110 ; 
       3 6 110 ; 
       4 8 110 ; 
       5 4 110 ; 
       6 4 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 20 300 ; 
       9 21 300 ; 
       10 22 300 ; 
       11 23 300 ; 
       12 24 300 ; 
       13 25 300 ; 
       0 26 300 ; 
       0 27 300 ; 
       0 28 300 ; 
       0 29 300 ; 
       0 30 300 ; 
       0 31 300 ; 
       0 32 300 ; 
       1 0 300 ; 
       1 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
       1 4 300 ; 
       1 5 300 ; 
       4 33 300 ; 
       4 34 300 ; 
       4 35 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       5 13 300 ; 
       5 14 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       5 19 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       6 8 300 ; 
       6 9 300 ; 
       6 10 300 ; 
       6 11 300 ; 
       6 12 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       8 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 0 401 ; 
       2 1 401 ; 
       3 2 401 ; 
       4 3 401 ; 
       5 4 401 ; 
       8 5 401 ; 
       9 6 401 ; 
       10 7 401 ; 
       11 8 401 ; 
       12 9 401 ; 
       15 10 401 ; 
       16 11 401 ; 
       17 12 401 ; 
       18 13 401 ; 
       19 14 401 ; 
       28 15 401 ; 
       29 16 401 ; 
       30 17 401 ; 
       31 18 401 ; 
       32 19 401 ; 
       34 20 401 ; 
       35 21 401 ; 
       36 22 401 ; 
       37 23 401 ; 
       38 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 22.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 30 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       7 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 15 -4 0 MPRFLG 0 ; 
       10 SCHEM 20 -4 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 MPRFLG 0 ; 
       13 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       0 SCHEM 10 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 USR MPRFLG 0 ; 
       2 SCHEM 5 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -5.957375 0 MPRFLG 0 ; 
       4 SCHEM 5 -2 0 USR MPRFLG 0 ; 
       5 SCHEM 5 -4 0 USR MPRFLG 0 ; 
       6 SCHEM 2.5 -3.957375 0 USR MPRFLG 0 ; 
       8 SCHEM 5 0 0 SRT 1 1 1 -1.570796 0 0 0 -5.455351 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -5.957375 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 6.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 11.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 14 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 16.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 6.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 21.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 11.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -7.957375 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -7.957375 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -7.957375 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -7.957375 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -7.957375 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 6.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 16.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
