SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       ss16-ss16.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       pilon1-cam_int1.1-0 ROOT ; 
       pilon1-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       pilon10-light3_5.1-0 ROOT ; 
       pilon10-light3_7_1.1-0 ROOT ; 
       pilon10-light4_5.1-0 ROOT ; 
       pilon10-light4_7_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 48     
       pilon_F-mat100.1-0 ; 
       pilon_F-mat101.1-0 ; 
       pilon_F-mat67.1-0 ; 
       pilon_F-mat68.1-0 ; 
       pilon_F-mat69.1-0 ; 
       pilon_F-mat70.1-0 ; 
       pilon_F-mat71.1-0 ; 
       pilon_F-mat72.1-0 ; 
       pilon_F-mat73.1-0 ; 
       pilon_F-mat74.1-0 ; 
       pilon_F-mat75.1-0 ; 
       pilon_F-mat76.1-0 ; 
       pilon_F-mat77.1-0 ; 
       pilon_F-mat78.1-0 ; 
       pilon_F-mat79.1-0 ; 
       pilon_F-mat80.1-0 ; 
       pilon_F-mat81.1-0 ; 
       pilon_F-mat82.1-0 ; 
       pilon_F-mat83.1-0 ; 
       pilon_F-mat84.1-0 ; 
       pilon_F-mat85.1-0 ; 
       pilon_F-mat86.1-0 ; 
       pilon_F-mat87.1-0 ; 
       pilon_F-mat88.1-0 ; 
       pilon_F-mat89.1-0 ; 
       pilon_F-mat90.1-0 ; 
       pilon_F-mat91.1-0 ; 
       pilon_F-mat92.1-0 ; 
       pilon_F-mat93.1-0 ; 
       pilon_F-mat94.1-0 ; 
       pilon_F-mat95.1-0 ; 
       pilon_F-mat96.1-0 ; 
       pilon_F-mat97.1-0 ; 
       pilon_F-mat98.1-0 ; 
       pilon_F-mat99.1-0 ; 
       pilon11-mat20.1-0 ; 
       pilon11-mat21.1-0 ; 
       pilon11-mat22.1-0 ; 
       pilon11-mat23.1-0 ; 
       pilon11-mat24.1-0 ; 
       pilon11-mat25.1-0 ; 
       pilon11-mat33.1-0 ; 
       pilon11-mat34.1-0 ; 
       pilon11-mat35.1-0 ; 
       pilon11-mat36.1-0 ; 
       pilon11-mat37.1-0 ; 
       pilon11-mat38.1-0 ; 
       pilon11-mat39.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 18     
       ss16-bfuselg.1-0 ; 
       ss16-doccon1.1-0 ; 
       ss16-doccon2.1-0 ; 
       ss16-doccon3.1-0 ; 
       ss16-mfuselg1.1-0 ; 
       ss16-mfuselg2.1-0 ; 
       ss16-rfuselg.1-0 ; 
       ss16-rfuselg2.1-0 ; 
       ss16-SS1.1-0 ; 
       ss16-ss16.1-0 ROOT ; 
       ss16-SS2.1-0 ; 
       ss16-SS3.1-0 ; 
       ss16-SS4.1-0 ; 
       ss16-SS5.1-0 ; 
       ss16-SS6.1-0 ; 
       ss16-SS7.1-0 ; 
       ss16-SS8.1-0 ; 
       ss16-tfuselg.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       D:/Pete_Data/Softimage/space_station/ss/ss16/PICTURES/ss10 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ss16-pilon_F.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 30     
       pilon_F-t2d50.1-0 ; 
       pilon_F-t2d51.1-0 ; 
       pilon_F-t2d52.1-0 ; 
       pilon_F-t2d53.1-0 ; 
       pilon_F-t2d54.1-0 ; 
       pilon_F-t2d55.1-0 ; 
       pilon_F-t2d56.1-0 ; 
       pilon_F-t2d57.1-0 ; 
       pilon_F-t2d58.1-0 ; 
       pilon_F-t2d59.1-0 ; 
       pilon_F-t2d60.1-0 ; 
       pilon_F-t2d61.1-0 ; 
       pilon_F-t2d62.1-0 ; 
       pilon_F-t2d63.1-0 ; 
       pilon_F-t2d64.1-0 ; 
       pilon_F-t2d65.1-0 ; 
       pilon_F-t2d66.1-0 ; 
       pilon_F-t2d67.1-0 ; 
       pilon_F-t2d68.1-0 ; 
       pilon_F-t2d69.1-0 ; 
       pilon11-t2d15.1-0 ; 
       pilon11-t2d16.1-0 ; 
       pilon11-t2d17.1-0 ; 
       pilon11-t2d18.1-0 ; 
       pilon11-t2d19.1-0 ; 
       pilon11-t2d25.1-0 ; 
       pilon11-t2d26.1-0 ; 
       pilon11-t2d27.1-0 ; 
       pilon11-t2d28.1-0 ; 
       pilon11-t2d29.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 4 110 ; 
       10 4 110 ; 
       11 4 110 ; 
       12 4 110 ; 
       13 5 110 ; 
       14 5 110 ; 
       15 5 110 ; 
       16 5 110 ; 
       0 5 110 ; 
       1 17 110 ; 
       2 7 110 ; 
       3 6 110 ; 
       4 9 110 ; 
       5 4 110 ; 
       6 4 110 ; 
       7 4 110 ; 
       17 5 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       8 22 300 ; 
       10 23 300 ; 
       11 24 300 ; 
       12 25 300 ; 
       13 26 300 ; 
       14 27 300 ; 
       15 28 300 ; 
       16 29 300 ; 
       0 8 300 ; 
       0 9 300 ; 
       0 10 300 ; 
       0 11 300 ; 
       0 12 300 ; 
       0 13 300 ; 
       0 14 300 ; 
       4 35 300 ; 
       4 36 300 ; 
       4 37 300 ; 
       4 38 300 ; 
       4 39 300 ; 
       4 40 300 ; 
       5 2 300 ; 
       5 3 300 ; 
       5 4 300 ; 
       5 5 300 ; 
       5 6 300 ; 
       5 7 300 ; 
       6 41 300 ; 
       6 42 300 ; 
       6 43 300 ; 
       6 44 300 ; 
       6 45 300 ; 
       6 46 300 ; 
       6 47 300 ; 
       7 30 300 ; 
       7 31 300 ; 
       7 32 300 ; 
       7 33 300 ; 
       7 34 300 ; 
       7 0 300 ; 
       7 1 300 ; 
       17 15 300 ; 
       17 16 300 ; 
       17 17 300 ; 
       17 18 300 ; 
       17 19 300 ; 
       17 20 300 ; 
       17 21 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       9 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 18 401 ; 
       1 19 401 ; 
       3 0 401 ; 
       4 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       12 7 401 ; 
       13 8 401 ; 
       14 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       32 15 401 ; 
       33 16 401 ; 
       34 17 401 ; 
       36 20 401 ; 
       37 21 401 ; 
       38 22 401 ; 
       39 23 401 ; 
       40 24 401 ; 
       43 25 401 ; 
       44 26 401 ; 
       45 27 401 ; 
       46 28 401 ; 
       47 29 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 32.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 35 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 37.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 40 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 25 -4 0 MPRFLG 0 ; 
       10 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       11 SCHEM 22.5 -4 0 MPRFLG 0 ; 
       12 SCHEM 20 -4 0 MPRFLG 0 ; 
       13 SCHEM 10 -6 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       15 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       16 SCHEM 15 -6 0 MPRFLG 0 ; 
       0 SCHEM 5 -6 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       2 SCHEM 30 -6 0 MPRFLG 0 ; 
       3 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       4 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       5 SCHEM 11.25 -4 0 MPRFLG 0 ; 
       6 SCHEM 2.5 -4 0 MPRFLG 0 ; 
       7 SCHEM 30 -4 0 MPRFLG 0 ; 
       9 SCHEM 16.25 0 0 SRT 1 1 1 -1.570796 0 0 0 -5.302129 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -6 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 24 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 21.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 19 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 9 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM 11.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM 16.5 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM 14 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 32.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 31.5 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       43 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       44 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       45 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       46 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       47 SCHEM 4 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 19 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 9 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 32.5 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 31.5 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 4 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 31.5 -2 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 1 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
